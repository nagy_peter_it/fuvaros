unit Koltnyil;

interface

uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, DBTables, DB, StdCtrls, Buttons, Mask, DBCtrls, ExtCtrls,
  ADODB,DateUtils;

type
  TKoltnyilDlg = class(TForm)
    BitBtn4: TBitBtn;
    BitBtn6: TBitBtn;
    Label3: TLabel;
    M4: TMaskEdit;
    BitBtn1: TBitBtn;
    Label1: TLabel;
    MD1: TMaskEdit;
    Label2: TLabel;
    MD2: TMaskEdit;
    BitBtn2: TBitBtn;
    BitBtn3: TBitBtn;
    Label4: TLabel;
    M1: TMaskEdit;
    BitBtn5: TBitBtn;
    M0: TMaskEdit;
    Query1: TADOQuery;
    procedure FormCreate(Sender: TObject);
    procedure BitBtn4Click(Sender: TObject);
    procedure BitBtn6Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure MD1Exit(Sender: TObject);
    procedure MD2Exit(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure BitBtn5Click(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure M1Click(Sender: TObject);
    procedure M1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
     jaratkod		: string;
  public
  end;

var
  KoltnyilDlg: TKoltnyilDlg;

implementation

uses
	Egyeb, Koltlis, J_SQL, Kozos, J_VALASZTO;
{$R *.DFM}

procedure TKoltnyilDlg.FormCreate(Sender: TObject);
begin
 	M4.Text 		:= '';
  jaratkod    := '';
  MD1.Text		:= copy(EgyebDlg.MaiDatum,1,8)+'01.';
  MD2.Text		:= EgyebDlg.MaiDatum;
end;

procedure TKoltnyilDlg.BitBtn4Click(Sender: TObject);
var
  kedat		: string;
  vedat		: string;
begin
	{A lista �ssze�ll�t�sa a sz�r�si elemek alapj�n}
  kedat	:= MD1.Text;
  vedat	:= MD2.Text;
	if MD1.Text = '' then begin
  	kedat := copy(EgyebDlg.MaiDatum,1,4)+'.01.01.';
  end;
	if MD2.Text = '' then begin
  	vedat := EgyebDlg.MaiDatum;;
  end;
  if not Jodatum(kedat) then begin
		NoticeKi('Rossz d�tum megad�sa!');
     MD1.SetFocus;
     Exit;
  end;
  if not Jodatum(vedat) then begin
		NoticeKi('Rossz d�tum megad�sa!');
     MD2.SetFocus;
     Exit;
  end;

	{A lista �ssze�ll�t�sa a sz�r�si elemek alapj�n}
  Application.CreateForm(TKoltlisDlg, KoltlisDlg);
  KoltlisDlg.Tolt(M4.Text, kedat, vedat, MD1.Text, MD2.Text, jaratkod);
  if KoltlisDlg.vanadat	then begin
     KoltlisDlg.Rep.Preview;
  end else begin
		NoticeKi('Nincs a megadott felt�teleknek megfelel� t�tel!');
  end;
  KoltlisDlg.Destroy;
end;

procedure TKoltnyilDlg.BitBtn6Click(Sender: TObject);
begin
	Close;
end;

procedure TKoltnyilDlg.BitBtn1Click(Sender: TObject);
begin
	GepkocsiValaszto(M4);
end;

procedure TKoltnyilDlg.MD1Exit(Sender: TObject);
begin
	if ( ( MD1.Text <> '' ) and ( not Jodatum2(MD1 ) ) ) then begin
		NoticeKi('Hib�s d�tum megad�sa!');
		MD1.Text := '';
		MD1.Setfocus;
	end;
  EgyebDlg.ElsoNapEllenor(MD1, MD2);
end;

procedure TKoltnyilDlg.MD2Exit(Sender: TObject);
begin
	if ( ( MD2.Text <> '' ) and ( not Jodatum2(MD2 ) ) ) then begin
		NoticeKi('Hib�s d�tum megad�sa!');
		MD2.Text := '';
		MD2.Setfocus;
	end;
end;

procedure TKoltnyilDlg.BitBtn2Click(Sender: TObject);
begin
	// EgyebDlg.Calendarbe(MD1);
  // EgyebDlg.ElsoNapEllenor(MD1, MD2);
  EgyebDlg.CalendarBe_idoszak(MD1, MD2);
end;

procedure TKoltnyilDlg.BitBtn3Click(Sender: TObject);
begin
	EgyebDlg.Calendarbe(MD2);
  EgyebDlg.UtolsoNap(MD2);
end;

procedure TKoltnyilDlg.BitBtn5Click(Sender: TObject);
begin
	JaratValaszto(M0, M1);
  	jaratkod	:= M0.Text;
end;

procedure TKoltnyilDlg.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
	EgyebDlg.FormReturn(Key);
end;

procedure TKoltnyilDlg.M1Click(Sender: TObject);
begin
	M1.SelectAll;
end;

procedure TKoltnyilDlg.M1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
	if Key = VK_RETURN then begin
		{Megkeress�k a megadott sz�m� j�ratot}
     EgyebDlg.SetADOQueryDatabase(Query1);
     if Query_Run(Query1, 'SELECT JA_KOD, JA_ORSZ, JA_ALKOD FROM JARAT WHERE JA_ALKOD = '+
     	IntToStr(StrToIntDef(M1.Text,0)) ,true) then begin
     	M1.Text 	:= '';
     	if Query1.RecordCount > 0 then begin
  			M1.Text 	:= GetJaratszam(Query1.FieldByname('JA_KOD').AsString);
     		jaratkod	:= Query1.FieldByname('JA_KOD').AsString;
        end;
		end;
     Query1.Close;
	end;
end;

end.


