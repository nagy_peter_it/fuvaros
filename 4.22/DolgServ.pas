unit DolgServ;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, DB, ADODB, Buttons;

type
  TFDolgServ = class(TForm)
    PanelDolgServ: TPanel;
    EditP1: TEdit;
    ButtonP1: TButton;
    ADOQuery1: TADOQuery;
    ADOQuery1dvnev: TStringField;
    ADOQuery1dknev1: TStringField;
    Panel1: TPanel;
    Label1: TLabel;
    BitKilep: TBitBtn;
    SQuery1: TADOQuery;
    ADOCommand1: TADOCommand;
    procedure ButtonP1Click(Sender: TObject);
    procedure BitKilepClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FDolgServ: TFDolgServ;

implementation

uses Egyeb,J_SQL, Kozos;

{$R *.dfm}

procedure TFDolgServ.ButtonP1Click(Sender: TObject);
var
  skod: integer;
  sskod,nev,s,proc,sql: string;
begin
  if EditP1.Text='' then exit;
  skod:=StrToIntDef(EditP1.Text,0);
  sskod:=IntToStr(skod);
  nev:= Query_Select('DOLGOZO','DO_SKOD',sskod,'DO_NAME');
  if nev<>'' then
  begin
    // van m�r ilyen k�d a t�zsben
    ShowMessage('Van m�r ilyen k�d� dolgoz� a t�rzsben: '+nev);
    exit;
  end;

  if EgyebDlg.ADOConnectionServantes.Connected then
    EgyebDlg.ADOConnectionServantes.Close;

  if not EgyebDlg.VanServantes then begin
       NoticeKi('A servantes megnyit�s nem siker�lt!');
       Exit;
       end;

  sskod:= StringOfChar('0',5-length(sskod))+sskod;
  ADOQuery1.Close;
  ADOQuery1.Parameters[0].Value:=sskod;
  ADOQuery1.Open;
  ADOQuery1.First;
  nev:=Trim(Trim(ADOQuery1dvnev.AsString)+' '+Trim(ADOQuery1dknev1.AsString));
  ADOQuery1.Close;
  if nev='' then
  begin
    ShowMessage('Nincs ilyen k�d� dolgoz� a Serventes t�rzsben!');
    exit;
  end;
  s:='Mehet az �tv�tel?'+#13+#10+'Dolgoz�: '+nev;
  if (MessageBox(0, Pchar(s), '', MB_ICONQUESTION or MB_YESNO or MB_DEFBUTTON2) in [idNo]) then
    exit;

  if EgyebDlg.TESZTPRG then
    proc:='DOLG_FUVAROSBA_TESZT '
  else
    proc:='DOLG_FUVAROSBA ';

	sql:='EXEC '+proc+''''+sskod+''' ';
  // ADOCommand1.CommandText:=sql;
  Try
    // ADOCommand1.Execute;
    Command_Run(ADOCommand1, sql, true);   // 20150218 NagyP
    ShowMessage('Sikeres �tv�tel!')
  Except
    ShowMessage('Az �tv�tel nem siker�lt!');
  End;

    {
	sql:='EXEC '+proc+sskod;
	if Query_Run(SQuery1,sql, FALSE) then
    ShowMessage('Sikeres �tv�tel!')
  else
    ShowMessage('Az �tv�tel nem siker�lt!');
 }
 if EgyebDlg.ADOConnectionServantes.Connected then
    EgyebDlg.ADOConnectionServantes.Close;

end;

procedure TFDolgServ.BitKilepClick(Sender: TObject);
begin
  close;
end;

end.
