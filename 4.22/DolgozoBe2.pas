unit DolgozoBe2;

interface

uses
	Windows, SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
	Forms, Dialogs, StdCtrls, Buttons, Mask, DB, DBTables, J_ALFORM, ADODB,
  ExtCtrls, Grids, ComCtrls, Shellapi, FileCtrl, DBGrids, DBCtrls,
  ExtDlgs, Menus,jpeg, Math, Clipbrd, PictureUtils;

const
    MEZOSZAM	= 10;
    SGTel_Telefonkod_oszlop = 6;
    SGTel_Elofizeteskod_oszlop = 7;

type
	TDolgozobe2Dlg = class(TJ_AlformDlg)
    Panel1: TPanel;
    BitElkuld: TBitBtn;
    BitKilep: TBitBtn;
    Panel2: TPanel;
    Label2: TLabel;
    Label8: TLabel;
    Label3: TLabel;
    Label6: TLabel;
    M1: TMaskEdit;
    M2: TMaskEdit;
    M3: TMaskEdit;
    BitBtn1: TBitBtn;
    M30: TMaskEdit;
    CBKUL: TCheckBox;
    CBNem: TCheckBox;
    BitBtn7: TBitBtn;
    Query1: TADOQuery;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
	 GroupBox1: TGroupBox;
    SG1: TStringGrid;
    TabSheet2: TTabSheet;
    GroupBox3: TGroupBox;
    Panel4: TPanel;
    BitBtn4: TBitBtn;
    SGTel: TStringGrid;
    Panel3: TPanel;
    Label1: TLabel;
    M11A: TMaskEdit;
    BitBtn2: TBitBtn;
    Label4: TLabel;
    M11B: TMaskEdit;
    ButtonMod: TBitBtn;
    GroupBox2: TGroupBox;
    SB1: TScrollBox;
    LOrig: TLabel;
    MOrig: TMaskEdit;
    ButtonTor: TBitBtn;
    BitBtn5: TBitBtn;
    TabSheet3: TTabSheet;
    Label10: TLabel;
    MM1: TMaskEdit;
    Label11: TLabel;
    MM2: TMaskEdit;
    Label12: TLabel;
    MM3: TMaskEdit;
    Label13: TLabel;
    MM5: TMaskEdit;
    BitBtn9: TBitBtn;
    Label14: TLabel;
	 MM4: TMaskEdit;
    Label15: TLabel;
    MM6: TMaskEdit;
    Label16: TLabel;
    MM7: TMaskEdit;
    MM8: TMaskEdit;
    MM9: TMaskEdit;
    TabSheet4: TTabSheet;
    Panel5: TPanel;
    BitBtn10: TBitBtn;
    BitBtn12: TBitBtn;
    ADOQuery1: TADOQuery;
    ADOQuery1SZ_FOKOD: TStringField;
    ADOQuery1SZ_ALKOD: TStringField;
    ADOQuery1SZ_MENEV: TStringField;
    ADOQuery1SZ_LEIRO: TStringField;
    ADOQuery1SZ_KODHO: TBCDField;
    ADOQuery1SZ_NEVHO: TBCDField;
    ADOQuery1SZ_ERVNY: TIntegerField;
    Label17: TLabel;
    MaskEdit1: TMaskEdit;
    Label18: TLabel;
    MaskEdit2: TMaskEdit;
    CB_A: TCheckBox;
    CB_B: TCheckBox;
    CB_C: TCheckBox;
    CB_D: TCheckBox;
    CB_E: TCheckBox;
    PopupMenu2: TPopupMenu;
    PM2_1: TMenuItem;
    PM2_3: TMenuItem;
    MenuItem7: TMenuItem;
    PM2_4: TMenuItem;
    Image2: TImage;
    OpenPictureDialog1: TOpenPictureDialog;
    Image4: TImage;
    DOLGFOTO: TADODataSet;
    DOLGFOTODO_KOD: TStringField;
    DOLGFOTODO_FOTO: TBlobField;
    N1: TMenuItem;
    PM2_2: TMenuItem;
    BitBtn13: TBitBtn;
    M1_: TMaskEdit;
    Label19: TLabel;
    BitBtn19: TBitBtn;
    SQuery1: TADOQuery;
    SGCSOP: TStringGrid;
    GroupBox4: TGroupBox;
    Label20: TLabel;
    meEgyebTel: TMaskEdit;
    BitBtn3: TBitBtn;
    BitBtn11: TBitBtn;
    meTELKOD: TMaskEdit;
    meELOFIZKOD: TMaskEdit;
    BitBtn14: TBitBtn;
    Forgats1: TMenuItem;
    N2: TMenuItem;
    Forgatsbalra1: TMenuItem;
    Forgatsfejtetre1: TMenuItem;
    BitBtn16: TBitBtn;
    BitBtn17: TBitBtn;
    Label21: TLabel;
    meIrodaiTel: TMaskEdit;
    meBelsoMellek: TMaskEdit;
    Label22: TLabel;
    chkICLOUD: TCheckBox;
    chkKozosLista: TCheckBox;
    Label23: TLabel;
    cbBeosztas: TComboBox;
    meVCDADD: TMaskEdit;
    Label24: TLabel;
    Label5: TLabel;
    M13: TMaskEdit;
    BitBtn15: TBitBtn;
    TabSheet5: TTabSheet;
    Label7: TLabel;
    M12: TMaskEdit;
    BitBtn6: TBitBtn;
    Label9: TLabel;
    M12B: TMaskEdit;
    BitBtn8: TBitBtn;
    meUtolsoMunkanap: TMaskEdit;
    BitBtn18: TBitBtn;
    Label25: TLabel;
    Panel6: TPanel;
    CB6: TCheckBox;
    CB5: TCheckBox;
    Label26: TLabel;
    cbTavozasOka: TComboBox;
    CBAR: TCheckBox;
	procedure BitKilepClick(Sender: TObject);
	procedure FormCreate(Sender: TObject);
	procedure Tolto(cim : string; teko : string); override;
	procedure BitElkuldClick(Sender: TObject);
  procedure MaskEditClick(Sender: TObject);
    procedure Modosit(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure BitBtn1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure M5Exit(Sender: TObject);
    procedure BitBtn6Click(Sender: TObject);
    procedure BitBtn7Click(Sender: TObject);
    procedure KilepDat(Sender: TObject);
    procedure ButtonModClick(Sender: TObject);
    procedure ButtonTorClick(Sender: TObject);
    procedure GridChange(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SG1MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
	 procedure TablaBeiro;
    procedure BitBtn4Click(Sender: TObject);
   // procedure ButtonUjClick(Sender: TObject);
 	  procedure TelefonTablaToltes;
 	  procedure CsoportTablaToltes;
    procedure BitBtn5Click(Sender: TObject);
    procedure BitBtn8Click(Sender: TObject);
    procedure BitBtn9Click(Sender: TObject);
    procedure BitBtn10Click(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
    procedure BitBtn12Click(Sender: TObject);
    procedure CB_AClick(Sender: TObject);
    procedure PM2_1Click(Sender: TObject);
    procedure PM2_4Click(Sender: TObject);
    procedure OpenPic2;
    procedure PM2_3Click(Sender: TObject);
    procedure DOLGFOTOAfterInsert(DataSet: TDataSet);
    procedure Image2Click(Sender: TObject);
    procedure DOLGFOTOBeforePost(DataSet: TDataSet);
    procedure PM2_2Click(Sender: TObject);
    procedure BitBtn13Click(Sender: TObject);
    procedure M2Enter(Sender: TObject);
    procedure MM1Enter(Sender: TObject);
    procedure M1_Enter(Sender: TObject);
    procedure BitBtn19Click(Sender: TObject);
    procedure DolgCsopFrissit;
    procedure BitBtn11Click(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure SGTelDblClick(Sender: TObject);
    procedure BitBtn14Click(Sender: TObject);
    procedure Forgats1Click(Sender: TObject);
    procedure Forgatsbalra1Click(Sender: TObject);
    procedure Forgatsfejtetre1Click(Sender: TObject);
    procedure BitBtn15Click(Sender: TObject);
    procedure BitBtn16Click(Sender: TObject);
    procedure BitBtn17Click(Sender: TObject);
    procedure BitBtn18Click(Sender: TObject);
	private
     modosult 	: boolean;
     cimkek	: array[1..MEZOSZAM] of TLabel;
     mezok		: array[1..MEZOSZAM] of TMaskEdit;
	   mezonevek	: array[1..MEZOSZAM] of string;
     dszerv: array of string;
     docsdat,docskod,docsnev:string;
     CEG_JS: boolean;
	public
    // procedure ImgResize(RefBitmap, DescBitmap:TBitmap; ujXmeret,ujYmeret:Integer);
    // procedure RotateBitmap(Bmp: TBitmap; Degs: Integer; AdjustSize: Boolean;  BkColor: TColor = clNone);
    // procedure CropBitmap(InBitmap : TBitmap; HonnanX, HonnanY, HovaX, HovaY, W, H :Integer; BackgroundColor: TColor);
    procedure Forgatas(fok: integer);
    procedure ProcessAndSaveImage2(LoadedExt: string);
    procedure Image2Mentes;

    function Elkuldes : Boolean;
	end;

var
	Dolgozobe2Dlg: TDolgozobe2Dlg;
  kep1,kep2,kep3:string;

implementation

uses
 Egyeb,	J_VALASZTO,  J_SQL, Kozos, Kozos_Local, J_FOFORMUJ,
  StrUtils, Kamera, DolgCsopBe, Telefonbe2, Telelofizetbe, TelkiegCsoportosbe, VCARDParams;

{$R *.DFM}

procedure TDolgozobe2Dlg.BitKilepClick(Sender: TObject);
begin
	if modosult then begin
		if NoticeKi('M�dosultak az adatok . Ki akar l�pni ment�s n�lk�l?', NOT_QUESTION ) = 0 then begin
			ret_kod := '';
			Close;
     	end;
  	end else begin
		ret_kod := '';
		Close;
  	end;
end;

procedure TDolgozobe2Dlg.Forgats1Click(Sender: TObject);
begin
   Forgatas(90);
end;

procedure TDolgozobe2Dlg.Forgatsbalra1Click(Sender: TObject);
begin
   Forgatas(270);
end;

procedure TDolgozobe2Dlg.Forgatsfejtetre1Click(Sender: TObject);
begin
   Forgatas(180);
end;

procedure TDolgozobe2Dlg.Forgatas(fok: integer);
var
  bmp1: TBitMap;
  jpg: TJPegImage;
begin
  jpg:=TJPegImage.Create;
  bmp1:=TBitmap.Create;
  try
    jpg.Assign(Image2.Picture); // JPG van benne
    bmp1.Assign(jpg);
    RotateBitmap(bmp1, fok, False);  // n�gyzetes a k�p, nem kell �tm�retezni (nem m�k�d�tt j�l...)
    // debug...bmp1.SaveToFile('D:\Nagyp\X\x.bmp');
    jpg.Assign(bmp1);
    Image2.Picture.Assign(jpg);
    // ment�s is
    Image2Mentes;
  finally
    if jpg <> nil then jpg.Free;
    if bmp1 <> nil then bmp1.free;
    end;  // try-finally
end;

procedure TDolgozobe2Dlg.FormCreate(Sender: TObject);
var
	i : integer;
  elotag: string;
begin
  	EgyebDlg.SetADOQueryDatabase(Query1);
	ret_kod 	:= '';
  	modosult 	:= false;
  	// A hat�rid�k felt�lt�se
  	SG1.Cells[0,0]	:= 'Ssz.';
  	SG1.Cells[1,0]	:= 'K�d';
  	SG1.Cells[2,0]	:= 'Megnevez�s';
  	SG1.Cells[3,0]	:= 'Megjegyz�s';
	  SG1.Cells[4,0]	:= 'Flag';
  	SG1.Cells[5,0]	:= 'D�tum';

  	Query_Run(Query1, 'SELECT * FROM SZOTAR WHERE SZ_FOKOD = ''127'' ORDER BY SZ_ALKOD ');
  	if Query1.RecordCount > 0 then begin
  		SG1.RowCount := Query1.RecordCount+1;
     	i := 1;
     	while not Query1.Eof do begin
        	SG1.Cells[0,i]	:= IntToStr(i);
        	SG1.Cells[1,i]	:= Query1.FieldByName('SZ_ALKOD').AsString;
        	SG1.Cells[2,i]	:= Query1.FieldByName('SZ_MENEV').AsString;
        	SG1.Cells[4,i]	:= 'Nem';
        	SG1.Cells[5,i]	:= '';
        	Query1.Next;
        	Inc(i);
     	end;
  	end;
  	SGTel.Cells[0,0]	:= 'Els�dleges';
  	SGTel.Cells[1,0]	:= 'Telefonsz�m';
  	SGTel.Cells[2,0]	:= 'K�sz�l�k';
  	SGTel.Cells[3,0]	:= '';  // 'Mobilnet';
  	SGTel.Cells[4,0]	:= ''; // 'Roaming';
  	SGTel.Cells[5,0]	:= 'H�v�ssz�r�s';
    SGTel.Cells[6,0]	:= 'TKID'; // telefon k�d
    SGTel.Cells[7,0]	:= 'TEID'; // el�fizet�s k�d

	  SGTel.ColWidths[0]	:= 80;
	  SGTel.ColWidths[1]	:= 120;
	  SGTel.ColWidths[2]	:= 150;
	  SGTel.ColWidths[3]	:= 0;
	  SGTel.ColWidths[4]	:= 0;
	  SGTel.ColWidths[5]	:= 75;
    SGTel.ColWidths[6]	:= 0;  // rejtve
    SGTel.ColWidths[7]	:= 0;  // rejtve

    SGCSOP.Cells[0,0]	:= 'D�tum -t�l';
  	SGCSOP.Cells[1,0]	:= 'Dolgoz�i csoport';
    SGCSOP.ColWidths[0]	:= 160;
	  SGCSOP.ColWidths[1]	:= 250;

   // A kieg�sz�t� mez�k l�trehoz�sa
   for i := 1 to MEZOSZAM do begin
   	cimkek[i] 			:= TLabel.Create(SB1);
       cimkek[i].Parent    := SB1;
       cimkek[i].Name		:= 'CIMKE'+IntToStr(i);
       cimkek[i].Caption	:= 'CIMKE'+IntToStr(i);
       cimkek[i].Top		:= MOrig.Top + (i-1)* ( MOrig.Height + 10 ) + 4;
       cimkek[i].Width		:= LOrig.Width;
       cimkek[i].Left		:= LOrig.Left;
       cimkek[i].Height	:= LOrig.Height;
       cimkek[i].AutoSize	:= LOrig.AutoSize;
       cimkek[i].Visible	:= false;

       mezonevek[i]		:= '';

		mezok[i] 			:= TMaskEdit.Create(SB1);
       mezok[i].Parent    	:= SB1;
       mezok[i].Name		:= 'MEZO'+IntToStr(i);
       mezok[i].Text		:= '';
       mezok[i].Top		:= MOrig.Top + (i-1)* ( MOrig.Height + 10 );
       mezok[i].Width		:= SB1.Width - MOrig.Left - 50;
       mezok[i].Left		:= MOrig.Left;
       mezok[i].Height		:= MOrig.Height;
       mezok[i].MaxLength	:= MOrig.MaxLength;
       mezok[i].Visible	:= false;
       mezok[i].OnChange	:= Modosit;
   end;
   // Be�ll�tjuk a kieg�sz�t� mez�k tartalm�t
   i	:= 0;
  	Query_Run(Query1, 'SELECT * FROM SZOTAR WHERE SZ_FOKOD = ''360'' ORDER BY SZ_ALKOD ');
  	if Query1.RecordCount > 0 then begin
  		while ( ( not Query1.eof ) and (i < MEZOSZAM) ) do begin
        	Inc(i);
           cimkek[i].Caption	:= Query1.FieldByName('SZ_MENEV').AsString;
           mezonevek[i]	   	:= Trim('DM_'+Query1.FieldByName('SZ_ALKOD').AsString);
           cimkek[i].Visible	:= true;
           mezok[i].Visible   	:= true;
        	Query1.Next;
     	end;
  	end;

    /////////////////
   elotag		:=   EgyebDlg.Read_SZGrid('CEG', '310');
   if Length(elotag)=3  then  // pl. 131
      elotag:=copy(elotag,3,1);
   CEG_JS:= (elotag='1');
   M12.ReadOnly:=CEG_JS;       // csak JS nem �rhat
   M2.ReadOnly:=CEG_JS;        // a "n�v" is ide tartozik
   MM1.ReadOnly:=CEG_JS;
   MM2.ReadOnly:=CEG_JS;
   MM3.ReadOnly:=CEG_JS;
   MM4.ReadOnly:=CEG_JS;
   MM5.ReadOnly:=CEG_JS;
   MM6.ReadOnly:=CEG_JS;
   MM7.ReadOnly:=CEG_JS;
   MM8.ReadOnly:=CEG_JS;
   MM9.ReadOnly:=CEG_JS;

   SzotarTolt_CsakLista(cbBeosztas, '471');
   SzotarTolt_CsakLista(cbTavozasOka, '490');
end;

procedure TDolgozobe2Dlg.Tolto(cim : string; teko:string);
var
	i		: integer;
   sorsz	: integer;
begin
	SetMaskEdits([M1, M3]);
	ret_kod 		:= teko;
  if (CEG_JS)and(ret_kod='') then exit;  // csak a Servantesben vihet� fel �j dolgoz�
	Caption 		:= cim;
	M1.Text 		:= teko;
	M2.Text 		:= '';
	if ret_kod <> '' then begin		{M�dos�t�s}
		if Query_Run (Query1, 'SELECT * FROM DOLGOZO WHERE DO_KOD = '''+teko+''' ',true) then begin
			{docsdat:=Query1.FieldByName('DO_CSDAT').AsString;
      docskod:=Query1.FieldByName('DO_CSKOD').AsString;
      docsnev:=Query1.FieldByName('DO_CSNEV').AsString;}

			M1_.Text 		:= Query1.FieldByName('DO_SKOD').AsString;
			M2.Text 		:= Query1.FieldByName('DO_NAME').AsString;
			M3.Text 		:= Query1.FieldByName('DO_RENDSZ').AsString;
			M30.Text 		:= Query1.FieldByName('DO_JOKAT').AsString;
      CB_A.Checked:=pos('A',Query1.FieldByName('DO_JOKAT').AsString)>0;
      CB_b.Checked:=pos('B',Query1.FieldByName('DO_JOKAT').AsString)>0;
      CB_c.Checked:=pos('C',Query1.FieldByName('DO_JOKAT').AsString)>0;
      CB_d.Checked:=pos('D',Query1.FieldByName('DO_JOKAT').AsString)>0;
      CB_e.Checked:=pos('E',Query1.FieldByName('DO_JOKAT').AsString)>0;

			M12.Text 		:= Query1.FieldByName('DO_BELEP').AsString;
			M12B.Text 		:= Query1.FieldByName('DO_KILEP').AsString;
      meUtolsoMunkanap.Text:= Query1.FieldByName('DO_KITERV').AsString;
      cbTavozasOka.ItemIndex:= cbTavozasOka.Items.IndexOf(Query1.FieldByName('DO_KIOKA').AsString);
			M13.Text 		:= Query1.FieldByName('DO_EMAIL').AsString;
      meEgyebTel.Text:= Query1.FieldByName('DO_EGYEBTEL').AsString;
      meIrodaiTel.Text:= Query1.FieldByName('DO_IRODAITEL').AsString;
      meBelsoMellek.Text:= Query1.FieldByName('DO_BELSOMELLEK').AsString;
      meVCDADD.Text:= Query1.FieldByName('DO_VCDADD').AsString;
      cbBeosztas.Text:= Query1.FieldByName('DO_BEOSZTAS').AsString;
      chkKozosLista.Checked		:= Query1.FieldByName('DO_KOZOSLISTA').AsInteger > 0;
			CB5.Checked		:= Query1.FieldByName('DO_CHMOZ').AsInteger > 0;
			CB6.Checked		:= Query1.FieldByName('DO_ALKAL').AsInteger > 0;
			CBKUL.Checked	:= Query1.FieldByName('DO_KULSO').AsInteger > 0;
			CBNEM.Checked	:= Query1.FieldByName('DO_NEMZE').AsInteger > 0;
			CBAR.Checked	:= Query1.FieldByName('DO_ARHIV').AsInteger > 0;
      chkICLOUD.Checked	:= Query1.FieldByName('DO_ICLOUDMAIL').AsString = '1';
			// A szem�lyi adatok bet�lt�se
			MM1.Text		:= Query1.FieldByName('DO_TAJSZ').AsString;
			MM2.Text		:= Query1.FieldByName('DO_DFEOR').AsString;
			MM3.Text		:= Query1.FieldByName('DO_ADOSZ').AsString;
			MM4.Text		:= Query1.FieldByName('DO_SZULH').AsString;
			MM5.Text		:= Query1.FieldByName('DO_SZIDO').AsString;
			MM6.Text		:= Query1.FieldByName('DO_ANYAN').AsString;
			MM7.Text		:= Query1.FieldByName('DO_LAIRS').AsString;
			MM8.Text		:= Query1.FieldByName('DO_LATEL').AsString;
			MM9.Text		:= Query1.FieldByName('DO_LACIM').AsString;
			MaskEdit1.Text		:= Query1.FieldByName('DO_BERKOD').AsString;
			MaskEdit2.Text		:= Query1.FieldByName('DO_SZABI').AsString;

			// A hat�rid�k felvitele
			if Query_Run (EgyebDlg.QueryKOZOS, 'SELECT * FROM DOLGOZOERV WHERE DE_DOKOD = '''+M1.Text+''' ',true) then begin
				while not EgyebDlg.QueryKozos.Eof do begin
					sorsz	:= SG1.Cols[1].IndexOf(EgyebDlg.QueryKozos.FieldByName('DE_ALKOD').AsString);
              		if sorsz > 0 then begin
                 		SG1.Cells[3,sorsz]	:= EgyebDlg.QueryKozos.FieldByName('DE_MEGJE').AsString;
						SG1.Cells[4,sorsz]	:= 'Igen';
                 		SG1.Cells[5,sorsz]	:= EgyebDlg.QueryKozos.FieldByName('DE_DATUM').AsString;
              		end;
           		EgyebDlg.QueryKozos.Next;
           	end;
			end;
			TelefonTablaToltes;
     	CsoportTablaToltes;
		end;
		// A kieg�sz�t� adatok t�lt�se
		if Query_Run (Query1, 'SELECT * FROM DOLGOZOMEG WHERE DM_DOKOD = '''+teko+''' ',true) then begin
			try
			for i := 1 to MEZOSZAM do begin
				if mezonevek[i] <> '' then begin
					mezok[i].Text := Query1.FieldByName(mezonevek[i]).AsString;
				end;
			end;
			except
			end;
		end;
    //// Foto
    DOLGFOTO.Close;
    DOLGFOTO.Parameters[0].Value:=ret_kod;
    DOLGFOTO.Open;
    DOLGFOTO.First;
    if DOLGFOTODO_FOTO.BlobSize>0 then
      OpenPic2;
	end else begin
		{A k�vetkez� dolgoz�k�d meg�llap�t�sa}
		if Query_Run (Query1, 'SELECT MAX(DO_KOD) MAXI FROM DOLGOZO ',true) then begin
			i := StrToIntDef(Query1.FieldByName('MAXI').AsString,0)+1;
			M1.Text		:= Format('%3.3d',[i]);
		end;
	end;
	modosult 	:= false;
end;

procedure TDolgozobe2Dlg.BitElkuldClick(Sender: TObject);
begin
  if Elkuldes then
  	Close;
end;

procedure TDolgozobe2Dlg.MaskEditClick(Sender: TObject);
begin
	with Sender as TMaskEdit do begin
  		SelectAll;
  	end;
end;

procedure TDolgozobe2Dlg.Modosit(Sender: TObject);
begin
	modosult := true;
end;

procedure TDolgozobe2Dlg.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
	EgyebDlg.FormReturn(Key);
end;

procedure TDolgozobe2Dlg.BitBtn11Click(Sender: TObject);
begin
  meTELKOD.text:= '';
  TelefonValaszto2(meTELKOD, True);
  SetTelefonDolgozo(meTELKOD.Text, M1.text);  // a kiv�lasztott �j telefon
  TelefonTablaToltes;  // a t�bl�zat friss�t�s
end;

procedure TDolgozobe2Dlg.BitBtn1Click(Sender: TObject);
begin
	GepkocsiValaszto(M3, GK_TRAKTOR);
  if PageControl1.ActivePageIndex= 3 then  // Dolgoz�i csop.
  begin
     Elkuldes ;
     PageControl1.OnChange(self);
  end;
end;

procedure TDolgozobe2Dlg.BitBtn3Click(Sender: TObject);
begin
  meELOFIZKOD.text:= '';
  TelElofizetValaszto2(meELOFIZKOD, True);
  SetElofizetesDolgozo(meELOFIZKOD.Text, M1.text);  // a kiv�lasztott el�fizet�s
  TelefonTablaToltes;  // a t�bl�zat friss�t�s
end;

procedure TDolgozobe2Dlg.FormShow(Sender: TObject);
begin
	M2.SetFocus;
	TablaBeiro;
   PageControl1.ActivePageIndex	:= 0;
end;

procedure TDolgozobe2Dlg.M5Exit(Sender: TObject);
begin
	DatumExit(Sender, false );
end;

procedure TDolgozobe2Dlg.BitBtn6Click(Sender: TObject);
begin
	EgyebDlg.Calendarbe(M12);
end;

procedure TDolgozobe2Dlg.BitBtn7Click(Sender: TObject);
begin
	M3.Text	:= '';
end;

procedure TDolgozobe2Dlg.KilepDat(Sender: TObject);
begin
	EgyebDlg.Calendarbe(M11A);
end;

procedure TDolgozobe2Dlg.ButtonModClick(Sender: TObject);
begin
	// A megadott adatokat beteszi a t�bl�zat megfelel� sor�ba
  	if M11A.Text <> '' then begin
     	if DatumExit( M11A, true ) then begin
        	SG1.Cells[3,SG1.Row] := M11B.Text;
        	SG1.Cells[4,SG1.Row] := 'Igen';
        	SG1.Cells[5,SG1.Row] := M11A.Text;
     		modosult  := true;
     	end;
  	end;
end;

procedure TDolgozobe2Dlg.ButtonTorClick(Sender: TObject);
begin
	// Kit�r�lj�k a sor tartalm�t
	// A garancia adatok t�rl�se
	if NoticeKi('Val�ban t�r�lni k�v�nja a kijel�lt �rv�nyess�gi adatot?', NOT_QUESTION) = 0 then begin
       SG1.Cells[3,SG1.Row] 	:= '';
       SG1.Cells[4,SG1.Row] 	:= 'Nem';
       SG1.Cells[5,SG1.Row] 	:= '';
       modosult  				:= true;
   end;
end;

procedure TDolgozobe2Dlg.GridChange(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
	TablaBeiro;
end;

procedure TDolgozobe2Dlg.SG1MouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
	TablaBeiro;
end;

procedure TDolgozobe2Dlg.SGTelDblClick(Sender: TObject);
var
	i : integer;
  TKID, TEID: string;
begin
   TKID:= SGTel.Cells[SGTel_Telefonkod_oszlop ,SGTel.Row];
   TEID:= SGTel.Cells[SGTel_Elofizeteskod_oszlop ,SGTel.Row];
   if (TKID <> '') and (TKID <> '0') then begin  // ha van telefonk�sz�l�k, akkor azt jelen�tj�k meg
    	Application.CreateForm(TTelefonbe2Dlg, Telefonbe2Dlg);
      Telefonbe2Dlg.megtekint:= true;
		  Telefonbe2Dlg.Tolto('Telefonk�sz�l�k megtekint�se', TKID);
		  Telefonbe2Dlg.ShowModal;
		  Telefonbe2Dlg.Destroy;
      end
   else if TEID <> '' then begin  // ha van el�fizet�s, akkor azt jelen�tj�k meg
    	Application.CreateForm(TTelElofizetBeDlg, TelElofizetBeDlg);
      TelElofizetBeDlg.megtekint:= true;
		  TelElofizetBeDlg.Tolto('Telefon el�fizet�s megtekint�se', TEID);
		  TelElofizetBeDlg.ShowModal;
		  TelElofizetBeDlg.Destroy;
      end;
end;

procedure TDolgozobe2Dlg.TablaBeiro;
begin
 	M11B.Text	:= SG1.Cells[3,SG1.Row];
	M11A.Text	:= SG1.Cells[5,SG1.Row];
end;


procedure TDolgozobe2Dlg.BitBtn4Click(Sender: TObject);
var
	i : integer;
  TKID, TEID: string;
begin
   if NoticeKi('Val�ban t�r�lni k�v�nja a kijel�lt �sszerendel�st?', NOT_QUESTION) <> 0 then begin
     	Exit;
      end;

   TKID:= SGTel.Cells[SGTel_Telefonkod_oszlop ,SGTel.Row];
   TEID:= SGTel.Cells[SGTel_Elofizeteskod_oszlop ,SGTel.Row];
   if TKID <> '' then begin  	// Ha ki van t�ltve a telefon ID
      SetTelefonDolgozo (TKID, '');
      end
   else if TEID <> '' then begin  	// Ha ki van t�ltve az el�fizet�s ID
  		SetElofizetesDolgozo (TEID, '');
      end;

   	// A sor t�rl�se
    GridTorles(SGTel, SGTel.Row);
   	if (SGTel.Cells[SGTel_Telefonkod_oszlop ,SGTel.Row] = '') and (SGTel.Cells[SGTel_Elofizeteskod_oszlop ,SGTel.Row] = '') then begin
       	BitBtn4.Enabled	:= false;
       end;
end;

{
procedure TDolgozobe2Dlg.ButtonUjClick(Sender: TObject);
var
	sorsz	: integer;
begin
	// A telefonk�nyvi adatok beolvas�sa
	Application.CreateForm(TTelefonbeDlg, TelefonbeDlg);
   TelefonbeDlg.Tolto('�j telefon felvitele', '');
   TelefonbeDlg.M5.Text	:= M1.Text;
   TelefonbeDlg.M6.Text	:= M2.Text;
   TelefonbeDlg.BitBtn1.Enabled	:= false;
   TelefonbeDlg.ShowModal;
   if TelefonbeDlg.ret_kod <> '' then begin
   	// A telefonos adatok berak�sa a t�bl�zatba �s a dolgoz�i adatb�zisba
		SoforTelefonFrissites(M1.Text);
       TelefonTablaToltes;
       // R�ugrunk a megfelel� sorra
       sorsz	:= SGTel.Cols[2].IndexOf(TelefonbeDlg.ret_kod);
       if sorsz < 0 then begin
       	sorsz	:= 0;
       end;
       SGTel.Row	:= sorsz;
   end;
   TelefonbeDlg.Destroy;
end;
  }

procedure TDolgozobe2Dlg.TelefonTablaToltes;
const
  IgenString = 'Igen';
  NemString = '-';
var
	i : integer;
  S, Elsodleges, Hivasszures, Roaming: string;
begin
   // A telefonsz�mok felt�lt�se
   SGTel.RowCount	:= 2;
   SGTel.Rows[1].Clear;
   BitBtn4.Enabled	:= false;

   // El�fizet�s + esetleg telefon
   S:='select TE_TKID TKID, TE_DOKOD, TE_DOELS, TE_TELSZAM, k.SZ_MENEV KESZULEK, TE_HIVSZUR, convert(varchar,TE_ID) TE_ID ';
   S:=S+' from TELELOFIZETES e ';
   S:=S+' left outer join ';
   S:=S+'  (select convert(varchar, TK_ID) TK_ID, TK_DOKOD, sz2.SZ_MENEV ';
   S:=S+'   from TELKESZULEK, SZOTAR sz2 ';
   S:=S+'   where sz2.SZ_ALKOD = TK_TIPUSKOD and sz2.SZ_FOKOD=146 and TK_STATUSKOD = 1) k ';
   S:=S+'   on TE_TKID = k.TK_ID ';
   // S:=S+' where sz.SZ_ALKOD = TE_MOBILNETKOD and sz.SZ_FOKOD=143 ';
   S:=S+' where TE_STATUSKOD = 1 ';
   S:=S+' and ((TE_DOKOD = '''+M1.Text+''') or (TK_DOKOD = '''+M1.Text+''')) ';
   S:=S+' union ';
   // Telefon mag�ban, ahol nincs el�fizet�s
   S:=S+' select convert(varchar, TK_ID), TK_DOKOD, 0, '''', sz.SZ_MENEV, '''', '''' ';  // a dolgoz�hoz rendelt k�sz�l�k
   S:=S+' from SZOTAR sz, TELKESZULEK k ';
   S:=S+' where sz.SZ_ALKOD = TK_TIPUSKOD and sz.SZ_FOKOD=146 ';
   S:=S+' and TK_ID not in (select TE_TKID from TELELOFIZETES where TE_TKID is not null) '; // ami nincs el�fizet�shez rendelve!
   S:=S+' and TK_STATUSKOD = 1 ';
   S:=S+' and TK_DOKOD = '''+M1.Text+''' ';
   S:=S+' ORDER BY TE_DOELS DESC, TE_TELSZAM ASC';

   if Query_Run (EgyebDlg.QueryKOZOS, S,true) then begin
       if EgyebDlg.QueryKOZOS.RecordCount > 0 then with EgyebDlg.QueryKOZOS do begin
           SGTel.RowCount	:= RecordCount + 1;
           i := 1;
           while not Eof do begin
               if FieldByName('TE_DOELS').AsInteger=1 then Elsodleges:=IgenString else Elsodleges:= NemString;
               // if FieldByName('TE_ROAMING').AsInteger=1 then Roaming:=IgenString else Roaming:= NemString;
               if FieldByName('TE_HIVSZUR').AsInteger=1 then Hivasszures:=IgenString else Hivasszures:= NemString;
               SGTel.Cells[0, i]	:= Elsodleges;
               SGTel.Cells[1, i]	:= FieldByName('TE_TELSZAM').AsString;
               SGTel.Cells[2, i]	:= FieldByName('KESZULEK').AsString;
               SGTel.Cells[3, i]	:= ''; // FieldByName('MOBILNET').AsString;
	      			 SGTel.Cells[4, i]	:= ''; // Roaming;
	      			 SGTel.Cells[5, i]	:= Hivasszures;
               SGTel.Cells[6, i]	:= FieldByName('TKID').AsString;
               SGTel.Cells[7, i]	:= FieldByName('TE_ID').AsString;
               BitBtn4.Enabled		:= true;
               Next;
               Inc(i);
           end;
       end;
   end;
end;

procedure TDolgozobe2Dlg.CsoportTablaToltes;
var
	i : integer;
  S: string;
begin
   SGCSOP.RowCount	:= 1; // csak a fejl�c
   // SGCSOP.Rows[1].Clear;
   // BitBtn4.Enabled	:= false;
   S:='SELECT DC_DATTOL, DC_CSNEV FROM DOLGOZOCSOP WHERE DC_DOKOD = '''+M1.Text+''' ';
   S:=S+' ORDER BY 1';
   if Query_Run (EgyebDlg.QueryKOZOS, S,true) then begin
       if EgyebDlg.QueryKOZOS.RecordCount > 0 then begin
           SGCSOP.RowCount	:= EgyebDlg.QueryKOZOS.RecordCount + 1;
           SGCSOP.FixedRows:= 1;
           i := 1;
           while not EgyebDlg.QueryKOZOS.Eof do begin
               SGCSOP.Cells[0, i]	:= EgyebDlg.QueryKOZOS.FieldByName('DC_DATTOL').AsString;
               SGCSOP.Cells[1, i]	:= EgyebDlg.QueryKOZOS.FieldByName('DC_CSNEV').AsString;
               EgyebDlg.QueryKOZOS.Next;
               Inc(i);
           end;
       end;
   end;
end;


procedure TDolgozobe2Dlg.BitBtn5Click(Sender: TObject);
var
	dir0	: string;
	dir		: string;
begin
	// A dolgoz�hoz tartoz� dokumentumok megjelen�t�se
	dir0	:= EgyebDlg.Read_SZGrid('999', '720');
	if dir0 = '' then begin
		// Be kell olvasni a kezd�k�nyvt�rat
		dir	:= '';
		if SelectDirectory('A dolgoz�k k�nyvt�ra ...', '', dir ) then begin
			dir0	:= dir;
			// A k�nyvt�r be�r�sa a sz�t�rba
			if Length(dir0) > 120 then begin
				NoticeKi('T�l hossz� k�nyvt�rn�v : '+dir0);
				Exit;
			end;
			Query_Run(EgyebDlg.QueryKOZOS, 'UPDATE SZOTAR SET SZ_MENEV = '''+dir0+''' '+
				' WHERE SZ_FOKOD = ''999'' AND SZ_ALKOD = ''720'' ', TRUE);
			EgyebDlg.Fill_SZGrid;
		end else begin
			NoticeKi('Hiba a dolgoz�i k�nyvt�r megad�s�n�l!');
			Exit;
		end;
	end else begin
		if not DirectoryExists(dir0) then begin
			NoticeKi('Nem l�tez� alapk�nyvt�r : '+dir0);
			Exit;
		end;
	end;
	// A DIR0 egy l�tez� k�nyvt�r
	if ( ( copy(dir0, Length(dir0), 0) <> '\' ) and ( copy(dir0, Length(dir0), 0) <> '/' ) ) then begin
		dir0	:= dir0 + '\';
	end;
	// A n�v hozz�rak�sa a dir0 -hoz
	dir0	:= dir0 + M2.text;
	if not DirectoryExists(dir0) then begin
    if not CreateDir(dir0) then
  		NoticeKi('Nem l�tez�  dolgoz�i k�nyvt�r : "'+dir0+'"');
		Exit;
	end;

	if not DirectoryExists(dir0+'\Figyelmeztet�sek') then begin
    if not CreateDir(dir0+'\Figyelmeztet�sek') then
    begin
		  NoticeKi('Nem l�tez�  Figyelmeztet�sek k�nyvt�r : "'+dir0+'\Figyelmeztet�sek'+'"');
		 // Exit;
    end;
	end;

	// Nyissuk meg a k�nyvt�rat
	ShellExecute(Application.Handle,'open',PChar(dir0), '', '', SW_SHOWNORMAL );
end;

procedure TDolgozobe2Dlg.BitBtn8Click(Sender: TObject);
begin
	EgyebDlg.Calendarbe(M12B);
end;

procedure TDolgozobe2Dlg.BitBtn9Click(Sender: TObject);
begin
	EgyebDlg.Calendarbe(MM5);
end;


procedure TDolgozobe2Dlg.BitBtn10Click(Sender: TObject);
var
	sorsz	: integer;
begin
	Application.CreateForm(TDolgCsopbeDlg, DolgCsopbeDlg);
  DolgCsopbeDlg.DOKOD:= M1.Text;
   DolgCsopbeDlg.Tolto('�j csoport besorol�s felvitele', '');
   DolgCsopbeDlg.ShowModal;
   if DolgCsopbeDlg.ret_kod <> '' then begin
     CsoportTablaToltes;
     end;
   DolgCsopbeDlg.Destroy;
  DolgCsopFrissit;  // DO_CSKOD-ot update-eli, ha kell
end;

procedure TDolgozobe2Dlg.PageControl1Change(Sender: TObject);
var
  i:integer;
begin
  // az�rt kell menteni, hogy legyen DO_NAME a DOLGOZOCSOP adatsorban. �tgondoland�.
  if PageControl1.ActivePageIndex= 3 then begin // Dolgoz�i csop.
     if not Elkuldes then begin
       NoticeKi('A ment�s nem siker�lt!');
       end;
   {
     ADOQuery1.Close;
     ADOQuery1.Open;
     ADOQuery1.First;
     i:=0;
     SetLength(dszerv,ADOQuery1.RecordCount);
     DBGrid1.Columns[1].PickList.Clear;
     while not ADOQuery1.Eof do
     begin
      DBGrid1.Columns[1].PickList.Add(ADOQuery1SZ_MENEV.Value);
      dszerv[i]:=ADOQuery1SZ_ALKOD.Value;
      inc(i);
      ADOQuery1.Next;
     end;
     ADOQuery1.Close;

     TDOLGCSOP.Close;
     TDOLGCSOP.Parameters[0].Value:=M1.Text;
     TDOLGCSOP.Open;
     TDOLGCSOP.First;
     }
  end;
end;

procedure TDolgozobe2Dlg.BitBtn12Click(Sender: TObject);
var
	i : integer;
begin
  if SGCSOP.Row>0 then begin // fejl�cet nem t�rl�nk
    if NoticeKi('Val�ban t�r�lni k�v�nja a kijel�lt sort?', NOT_QUESTION) <> 0 then begin
        Exit;
        end;
    Query_Run ( Query1, 'delete from dolgozocsop where DC_DOKOD='+M1.Text+' and DC_DATTOL= '''+SGCSOP.Cells[0,SGCSOP.Row]+'''',true) ;
    CsoportTablaToltes;
    DolgCsopFrissit;  // DO_CSKOD-ot update-eli, ha kell
    end;
end;

procedure TDolgozobe2Dlg.DolgCsopFrissit;
var
   ActCSKOD, StoredCSKOD, ActCSNEV, S: string;
begin
 // a mai d�tum alapj�n �rv�nyes csoport k�d
  S:='select DC_CSKOD from DOLGOZOCSOP where DC_DOKOD='''+M1.Text+''' and DC_DATTOL= '+
     '(select max(DC_DATTOL) from DOLGOZOCSOP where DC_DOKOD='''+M1.Text+''' and DC_DATTOL<='''+EgyebDlg.MaiDatum+''')';

  ActCSKOD:= Query_SelectString('DOLGOZOCSOP', S); // kihaszn�ljuk, hogy az els� adatsorb�l veszi az adatot
  StoredCSKOD:= Query_Select('DOLGOZO', 'DO_KOD', M1.Text, 'DO_CSKOD');

  if StoredCSKOD <> ActCSKOD then begin
      ActCSNEV:=Query_SelectString('SZOTAR', 'select SZ_MENEV from SZOTAR where SZ_FOKOD=''400'' and SZ_ALKOD='''+ActCSKOD+'''');
      Query_Update (Query1, 'DOLGOZO',
        [
        'DO_CSDAT', 		''''+''+'''',  // ezt m�r nem haszn�ljuk
        'DO_CSKOD', 	''''+ActCSKOD+'''',
        'DO_CSNEV', 	''''+ActCSNEV+''''
        ], ' WHERE DO_KOD = '''+M1.Text+''' ' );
      end;
end;

procedure TDolgozobe2Dlg.CB_AClick(Sender: TObject);
var
  jog:string;
begin
  jog:='';
  if CB_A.Checked then jog:=jog+'A,';
  if CB_b.Checked then jog:=jog+'B,';
  if CB_c.Checked then jog:=jog+'C,';
  if CB_d.Checked then jog:=jog+'D,';
  if CB_e.Checked then jog:=jog+'E,';
  //jog:=copy(jog,1,length(jog)-1);
  M30.Text:=jog;
end;

procedure TDolgozobe2Dlg.PM2_1Click(Sender: TObject);
var
  LoadedExt: string;
begin
  if OpenPictureDialog1.Execute then begin
    Image2.Picture.LoadFromFile(OpenPictureDialog1.FileName);
    LoadedExt:=ExtractFileExt(OpenPictureDialog1.FileName);
    ProcessAndSaveImage2(LoadedExt);
    end; // if OpenPictureDialog1
end;

procedure TDolgozobe2Dlg.ProcessAndSaveImage2(LoadedExt: string);
var
  bmp1:TBitmap;
  jpg:TJPegImage;
  uw, uh, cropx, cropy:integer;
  eredetiwidth, eredetiheight, ujwidth, ujheight, ujkezdox, ujkezdoy: integer;
begin
    // Image2.Invalidate;
    jpg:=TJPegImage.Create;
    bmp1:=TBitmap.Create;
    try
      if LoadedExt='.bmp' then
            jpg.Assign(Image2.Picture.Bitmap)
      else
            jpg.Assign(Image2.Picture);
      bmp1.Assign(jpg);
      // if (bmp1.Width*bmp1.Height)>(640*640) then begin // �tm�retez�s, ha lehet
      if true then begin // �tm�retez�s mindenk�pp
        if bmp1.Width/bmp1.Height <= 1 then begin
          uw:=bmp1.Width;
          if bmp1.Width>640 then
            uw:=640;
          uh:=trunc( uw/bmp1.Width*bmp1.Height);
          cropx:= 0;
          cropy:= trunc((uh-uw)/2);
          end
        else begin
          uh:=bmp1.Height;
          if bmp1.Height>640 then
            uh:=640;
          uw:=trunc( uh/bmp1.Height*bmp1.Width);
          cropx:= trunc((uw-uh)/2);
          cropy:= 0;
          end;

        ImgResize(bmp1,Image4.Picture.Bitmap,uw,uh);   // az ar�nyok meg�rz�s�vel adtuk meg uw-t �s uh-t,
        // Image4.Picture.Bitmap.SaveToFile('D:\Nagyp\X\x_'+intToStr(uw)+'_'+intToStr(uh)+'.bmp');
        if (uw>=640) and (uh>=640) then begin  // tiszt�n v�g�s lesz
          CropBitmap(Image4.Picture.Bitmap, cropx, cropy, 0, 0, 640, 640, clBtnFace);  // �s itt lev�gjuk 640x640-esre
          end
        else begin
          eredetiwidth:=JPG.width;
          eredetiheight:=JPG.height;
          if eredetiwidth <> eredetiheight then begin
            if eredetiwidth > eredetiheight then begin
              ujwidth:=eredetiwidth;
              ujheight:=eredetiwidth;
              ujkezdox:= 0;
              ujkezdoy:=floor((ujheight-eredetiheight)/2);
              end;
            if eredetiwidth < eredetiheight then begin
              ujwidth:=eredetiheight;
              ujheight:=eredetiheight;
              ujkezdox:=floor((ujwidth-eredetiwidth)/2);
              ujkezdoy:=0;
              end;
            bmp1:=TBitmap.Create;
            try
              bmp1.Assign(jpg);
              CropBitmap(bmp1, 0, 0, ujkezdox, ujkezdoy, ujheight, ujwidth, clBtnFace);
              // bmp1.SaveToFile('D:\Nagyp\X\x.bmp');
              Image4.Picture.Assign(bmp1);
            finally
              bmp1.Free;
              end;  // try-finally
            end;  // if nem n�gyzetes
          end;  // else, vagyis kip�tolni kell
        // Image4.Picture.Bitmap.SaveToFile('D:\Nagyp\X\x_640_640.bmp');
        Image4.Picture.Width;
        jpg.Assign(Image4.Picture.Bitmap);
        end;
      Image4.Picture:=nil;
      Image2.Picture.Assign(JPG);
      Image2Mentes;

    finally
      if jpg <> nil then jpg.Free;
      if bmp1 <> nil then bmp1.Free;
      end;  // try-finally
end;

{procedure TDolgozobe2Dlg.CropBitmap(InBitmap : TBitmap; HonnanX, HonnanY, HovaX, HovaY, W, H :Integer; BackgroundColor: TColor);
var
  bmp1: TBitmap;
begin
  bmp1:=TBitmap.Create;
  try
    bmp1.PixelFormat:=pf24bit;
    bmp1.Width:=W; // a kimeneti m�ret
    bmp1.Height:=H; // a kimeneti m�ret
    bmp1.Canvas.Brush.Color := BackgroundColor;
    bmp1.Canvas.FillRect(Rect(0, 0, H, W));
    BitBlt(bmp1.Canvas.Handle, HovaX, HovaY, W, H, InBitmap.Canvas.Handle, HonnanX, HonnanY, SRCCOPY);
    bmp1.Width :=W;
    bmp1.Height:=H;
    InBitmap.Assign(bmp1);
  finally
    if bmp1 <> nil then bmp1.Free;
    end;  // try-finally

end;
 }
procedure TDolgozobe2Dlg.Image2Mentes;
var
  ms: TMemoryStream;
  jpg:TJPegImage;
begin
  ms:= TMemoryStream.Create;
  jpg:= TJPegImage.Create;
  try
      jpg.Assign(Image2.Picture);
      jpg.SaveToStream(ms);
      ms.Position := 0;
      Application.ProcessMessages;
      if DOLGFOTO.Eof then
        DOLGFOTO.Append
      else
        DOLGFOTO.Edit;
      DOLGFOTODO_FOTO.LoadFromStream(ms);
      DOLGFOTO.Post;
  except
      if ms <> nil then ms.Free;
      if jpg <> nil then jpg.Free;
      end;  // try-except
end;
{
procedure TDolgozobe2Dlg.ImgResize(RefBitmap, DescBitmap: TBitmap; ujXmeret,
  ujYmeret: Integer);
var
  XRatio, YRatio: Double;
  dx, dy: Double;          // (x,y) ??? (l,k) ??
  x, y: Double;            // (x,y) ?? pDestBitmap(c,r) ??? pRefBitmap ????
  c1, c2, c3, c4: Integer;  // Bilinear Eq.: f = c1 + c2*x + c3*y + c4*x*y
  k, l: Integer;            // (l,k) ?? pRefBitmap ?????(x,y)
  r, c: Integer;            // (c,r) ?? pDestBitmap ?????(x,y)
  b: Integer;
  pRef: array of PByteArray;
  pDest: PByteArray;
begin
  RefBitmap.PixelFormat:=pf24Bit;
  DescBitmap.PixelFormat:=pf24Bit;
  DescBitmap.Width:=ujXmeret;
  DescBitmap.Height:=ujYmeret;
  XRatio := DescBitmap.Width / RefBitmap.Width;
  YRatio := DescBitmap.Height / RefBitmap.Height;
  // ????pRefBitmap????????
  SetLength(pRef, RefBitmap.Height);
  for k := Low(pRef) to High(pRef) do
    pRef[k] := RefBitmap.ScanLine[k];
  // bilinear ??
  for r := 0 to DescBitmap.Height - 1 do
  begin
    pDest := DescBitmap.ScanLine[r];
    for c := 0 to DescBitmap.Width - 1 do
    begin
      if XRatio < 1 then x := c / XRatio + XRatio
      else              x := c / XRatio;
      if YRatio < 1 then y := r / YRatio + YRatio
      else              y := r / YRatio;
      l := Floor(x);
      k := Floor(y);
      dx := x - l;
      dy := y - k;
      // ???BGR????
      for b := 0 to 2 do
      begin
        c1 := pRef[k][l * 3 + b];
        if l = RefBitmap.Width - 1 then
          c2 := 0
        else
          c2 := pRef[k][(l + 1) * 3 + b] - pRef[k][l * 3 + b];
        if k = RefBitmap.Height - 1 then
          c3 := 0
        else
          c3 := pRef[k + 1][l * 3 + b] - pRef[k][l * 3 + b];
        if (l = RefBitmap.Width - 1) or (k = RefBitmap.Height - 1) then
          c4 := 0
        else
          c4 := pRef[k][l * 3 + b] + pRef[k + 1][(l + 1) * 3 + b] -
                pRef[k][(l + 1) * 3 + b] - pRef[k + 1][l * 3 + b];
        pDest[c * 3 + b] := Byte(Trunc(c1 + c2 * dx + c3 * dy + c4 * dx * dy));
      end;
    end;
  end;

end;

procedure TDolgozobe2Dlg.RotateBitmap(Bmp: TBitmap; Degs: Integer; AdjustSize: Boolean;
  BkColor: TColor = clNone);
var
  Tmp: TGPBitmap;
  Matrix: TGPMatrix;
  C: Single;
  S: Single;
  NewSize: TSize;
  Graphs: TGPGraphics;
  P: TGPPointF;
  DrawX, DrawY: Cardinal;
begin
  Tmp := TGPBitmap.Create(Bmp.Handle, Bmp.Palette);
  Matrix := TGPMatrix.Create;
  try
    Matrix.RotateAt(Degs, MakePoint(0.5 * Bmp.Width, 0.5 * Bmp.Height));
    if AdjustSize then begin
      C := Cos(DegToRad(Degs));
      S := Sin(DegToRad(Degs));
      NewSize.cx := Round(Bmp.Width * Abs(C) + Bmp.Height * Abs(S));
      NewSize.cy := Round(Bmp.Width * Abs(S) + Bmp.Height * Abs(C));
      Bmp.Width := NewSize.cx;
      Bmp.Height := NewSize.cy;
      end;
    Graphs := TGPGraphics.Create(Bmp.Canvas.Handle);
    try
      Graphs.Clear(ColorRefToARGB(ColorToRGB(BkColor)));
      Graphs.SetTransform(Matrix);
      Graphs.DrawImage(Tmp, (Cardinal(Bmp.Width) - Tmp.GetWidth) div 2, (Cardinal(Bmp.Height) - Tmp.GetHeight) div 2);
      // Graphs.DrawImage(Tmp, 0, 0);  // a der�ksz�g� forgat�sokn�l nekem (0,0)-b�l indul a rajz
      // DrawX:= abs(Cardinal(Bmp.Width) - Tmp.GetWidth) div 2;
      // if DrawX<0 then DrawX:=0;
      // DrawY:= abs(Cardinal(Bmp.Height) - Tmp.GetHeight) div 2;
      // if DrawY<0 then DrawY:=0;
      // Graphs.DrawImage(Tmp, DrawX, DrawY);
    finally
      Graphs.Free;
    end;
  finally
    Matrix.Free;
    Tmp.Free;
  end;
end;
}
{procedure TDolgozobe2Dlg.RotateImage(MyImage: TImage);  // most csak egyf�l�t tud, ha kell akkor t�bbsz�r forgatnak
var
  PaintBox1: TPaintBox;
  Stream: IStream;
  BlobStream: TMemoryStream;
  GPImage: TGPImage;
  bmp1: TBitMap;
  jpg: TJPegImage;
  MyCanvas: TCanvas;
begin
  BlobStream := TMemoryStream.Create;
  jpg:=TJPegImage.Create;
  jpg.Assign(MyImage.Picture); // JPG van benne
  bmp1:=TBitmap.Create;
  bmp1.Assign(jpg);
  bmp1.SaveToStream(BlobStream);
  // BlobStream.Position := 0;
  try
    Stream := TStreamAdapter.Create(BlobStream);
    GPImage:= TGPImage.Create(Stream);
    GPImage.RotateFlip(Rotate90FlipNone);
  finally
    if BlobStream <> nil then BlobStream.Free;
    if bmp1 <> nil then bmp1.free;
    end;  // try-finally
  // with TGPGraphics.Create(PaintBox1.Canvas.Handle) do

  *** vagy lehet Save(iStream ....
  MyCanvas:= TCanvas.Create;
  with TGPGraphics.Create(MyCanvas.Handle) do
  try
    DrawImage(GPImage, 0, 0);

    ****************
      ms: TMemoryStream;
  JPG: TJPEGImage;
begin
  JPG:=TJPEGImage.Create;
  ms:=TMemoryStream.Create;

  try
    DOLGFOTODO_FOTO.SaveToStream(ms);
    ms.Position := 0;
    JPG.LoadFromStream(ms);
    Image2.Picture.Assign(JPG);
  finally
     JPG.Free;
     ms.Free;
  end;
   ********************

  finally
    Free;
    end; // try-finally
  MyImage.Invalidate;  // redraw
end;
}

{
procedure RotateBitmap90CW(b1,b2:TBitmap);
var
  x,y:integer;
  p:array[0..2] of TPoint;
begin
  x:=b1.Width;
  y:=b1.Height;
  b2.Width:=y;
  b2.Height:=x;
  p[0].X:=y;
  p[0].Y:=0;
  p[1].X:=y;
  p[1].Y:=x;
  p[2].X:=0;
  p[2].Y:=0;
  PlgBlt(b2.Canvas.Handle,p,b1.Canvas.Handle,0,0,x,y,0,0,0);
end;
 }
procedure TDolgozobe2Dlg.PM2_4Click(Sender: TObject);
begin
  if DOLGFOTODO_FOTO.BlobSize>0 then
    kep2:=EgyebDlg.TempPath+'tmp_2.jpg';
    DOLGFOTODO_FOTO.SaveToFile(kep2);
    ShellExecute(Handle, 'open', PChar(kep2), nil, PChar(ExtractFilePath(kep2)), SW_SHOWDEFAULT)
end;

procedure TDolgozobe2Dlg.OpenPic2;
var
  ms: TMemoryStream;
  JPG: TJPEGImage;
  bmp1: TBitMap;
  eredetiwidth, eredetiheight, ujwidth, ujheight, ujkezdox, ujkezdoy: integer;
begin
  JPG:=TJPEGImage.Create;
  ms:=TMemoryStream.Create;
  try
    DOLGFOTODO_FOTO.SaveToStream(ms);
    ms.Position := 0;
    JPG.LoadFromStream(ms);
    // Image2.Picture.Assign(JPG);
    // ha nem n�gyzetes a k�p, akkor "kieg�sz�tj�k" - nem v�gjuk, mert van ahol belev�gn�nk az arcba
    eredetiwidth:=JPG.width;
    eredetiheight:=JPG.height;
    if eredetiwidth <> eredetiheight then begin
      if eredetiwidth > eredetiheight then begin
          ujwidth:=eredetiwidth;
          ujheight:=eredetiwidth;
          ujkezdox:= 0;
          ujkezdoy:=floor((ujheight-eredetiheight)/2);
          end;
      if eredetiwidth < eredetiheight then begin
          ujwidth:=eredetiheight;
          ujheight:=eredetiheight;
          ujkezdox:=floor((ujwidth-eredetiwidth)/2);
          ujkezdoy:=0;
          end;
      bmp1:=TBitmap.Create;
      try
        bmp1.Assign(jpg);
        CropBitmap(bmp1, 0, 0, ujkezdox, ujkezdoy, ujheight, ujwidth, clBtnFace);
        // bmp1.SaveToFile('D:\Nagyp\X\x.bmp');
        jpg.Assign(bmp1);
      finally
        bmp1.Free;
        end;  // try-finally
      end;  // if nem n�gyzetes
    Image2.Picture.Assign(JPG);
  finally
     JPG.Free;
     ms.Free;
  end;
end;

{ File-ba ment�s megold�s
procedure TDolgozobe2Dlg.OpenPic2;
begin
  kep2:=EgyebDlg.TempPath+'tmp_2.jpg';
  //kep2:=LMDSysInfo1.TempPath+'tmp_2.jpg';
  DOLGFOTODO_FOTO.SaveToFile(kep2);
  try
    Image2.Picture.LoadFromFile(kep2);
  except
    on EInvalidGraphic do
      Image2.Picture.Graphic := nil;
  end;
end;
}

procedure TDolgozobe2Dlg.PM2_3Click(Sender: TObject);
begin
  if DOLGFOTODO_FOTO.BlobSize=0 then abort;
  if Application.MessageBox(pchar('T�rl�d a k�pet?'),
        'T�rl�s',MB_YESNO + MB_ICONQUESTION + MB_DEFBUTTON2)=IDYES then
  begin
   if not (DOLGFOTO.State in [dsInsert,dsEdit]) then
     DOLGFOTO.Edit;
   DOLGFOTODO_FOTO.Clear;
   Image2.Picture:=nil;
   DOLGFOTO.Post;
  end;

end;

procedure TDolgozobe2Dlg.DOLGFOTOAfterInsert(DataSet: TDataSet);
begin
  DOLGFOTODO_KOD.Value:=ret_kod  ;
end;

procedure TDolgozobe2Dlg.Image2Click(Sender: TObject);
begin
  Try
  if FKamera.Visible and Clipboard.HasFormat(CF_BITMAP) then
     PM2_2.OnClick(self);
  Except
  End;
  if DOLGFOTODO_FOTO.BlobSize=0 then
     PM2_1.OnClick(self);

end;

procedure TDolgozobe2Dlg.DOLGFOTOBeforePost(DataSet: TDataSet);
begin
//  if DOLGFOTODO_FOTO.BlobSize=0 then
//    DOLGFOTO.Cancel;
end;


procedure TDolgozobe2Dlg.PM2_2Click(Sender: TObject);
var
  bmp1:TBitmap;
  jpg:TJPegImage;
  kit:string;
  uw,uh:integer;
begin
  Image2.Picture.LoadFromClipboardFormat(CF_BITMAP, ClipBoard.GetAsHandle(cf_Bitmap), 0);
  ProcessAndSaveImage2('.bmp');
end;

procedure TDolgozobe2Dlg.BitBtn13Click(Sender: TObject);
begin
  Application.CreateForm(TFKamera, FKamera);
  FKamera.Show;
end;

procedure TDolgozobe2Dlg.BitBtn14Click(Sender: TObject);
var
  TEID: string;
begin
   TEID:= SGTel.Cells[SGTel_Elofizeteskod_oszlop ,SGTel.Row];
   if TEID = '' then begin  	// nincs kit�ltve az el�fizet�s ID
      NoticeKi('Nincs telefonsz�m kijel�lve');
     	Exit;
      end;
   SetElsodlegesSzam (M1.Text, TEID);
   TelefonTablaToltes;
end;

procedure TDolgozobe2Dlg.BitBtn15Click(Sender: TObject);
begin
  if ret_kod = '' then begin
    NoticeKi('El�bb mentse el a dolgoz�t ("Elk�ld�s")!');
    Exit;
    end;
  // EgyebDlg.UzenetKuldesDolgozonak(ret_kod);
  TruckpitUzenetKuldes(ret_kod, '', '', ''); // Truckpit
end;


procedure TDolgozobe2Dlg.BitBtn16Click(Sender: TObject);
begin
  Application.CreateForm(TTelkiegCsoportosbeDlg, TelkiegCsoportosbeDlg);
  TelkiegCsoportosbeDlg.Tolto(M1.Text);  // DOKOD
  TelkiegCsoportosbeDlg.ShowModal;
  TelkiegCsoportosbeDlg.Free;
end;

procedure TDolgozobe2Dlg.BitBtn17Click(Sender: TObject);
begin
 // Application.CreateForm(TVCARDParamsDlg, VCARDParamsDlg);
 // VCARDParamsDlg.VCardTolto('DOLGOZO', M1.Text, M2.Text);
 // VCARDParamsDlg.ShowModal;
 // VCARDParamsDlg.Free;
end;

procedure TDolgozobe2Dlg.BitBtn18Click(Sender: TObject);
begin
   EgyebDlg.Calendarbe(meUtolsoMunkanap);
end;

function TDolgozobe2Dlg.Elkuldes: Boolean;
var
	i 		: integer;
	ujkod	: integer;
	jogos	: string;
	joger	: string;
	utlev	: string;
	utler	: string;
	nejsz	: string;
	nejer	: string;
	adrsz	: string;
	adrer	: string;
	sqlstr: string;
	mbkat	: string;
	mb1		: string;
  gkkat : string;
  docsop, udocsop,cskod  : string;
  Darab, ElsodlegesSzamCount: integer;
begin
  Result:=False;
	if M1.Text = '' then begin
		NoticeKi('K�d megad�sa k�telez�!');
		M1.Setfocus;
		Exit;
	end;
	if M2.Text = '' then begin
		NoticeKi('A n�v megad�sa k�telez�!');
		M2.Setfocus;
		Exit;
	end;
	if (M12.Text = '')and(not CBKUL.Checked) then begin
		NoticeKi('A bel�p�s d�tum�nak megad�sa k�telez�!');
		M12.Setfocus;
		Exit;
	end;
	if (MM3.Text<>'*')and(Length(MM3.Text)<>10) then begin
       if not MM3.Readonly then begin    // Csak akkor figyelmeztess�nk,ha tud bele �rni
           NoticeKi('Az ad�sz�m megad�sa k�telez� (10 karakter). Ha nincs, akkor �rjon be egy (*) csillagot!');
           PageControl1.ActivePageIndex := 2;
           MM3.Setfocus;
           Exit;
       end;
	end;

  ElsodlegesSzamCount:= GetElsodlegesSzamCount(M1.Text);  // -1, ha egy�ltal�n nincs telefonja
	if (ElsodlegesSzamCount = 0) or (ElsodlegesSzamCount > 1) then begin
		NoticeKi('Nincs megadva egy�rtelm� els�dleges telefonsz�m! K�rem v�lassza ki a megfelel� sz�mot.');
		Exit;
	end;

  SzotarBovito('471', cbBeosztas.Text); // felveszi a sz�t�rba, ha m�g nem l�tezik

	if ret_kod = '' then begin
		{�j rekord felvitele}
		if Query_Run (Query1, 'SELECT * FROM DOLGOZO WHERE DO_KOD = '''+M1.Text+''' ',true) then begin
			if Query1.RecordCount > 0 then begin
				NoticeKi('L�tez� k�d!');
				M1.Setfocus;
     			Exit;
        	end;
     	end;
     	Query_Run ( Query1, 'INSERT INTO DOLGOZO (DO_KOD) VALUES (''' +M1.Text+ ''' )',true);
     	Query_Run ( Query1, 'INSERT INTO DOLGOZOMEG (DM_DOKOD) VALUES (''' +M1.Text+ ''' )',true);
     	ret_kod := M1.Text;
  	end;

   jogos	:= '';
   joger	:= '';
   utlev	:= '';
   utler	:= '';
	 nejsz	:= '';
   nejer	:= '';
   adrsz	:= '';
   adrer	:= '';
   for i := 1 to SG1.RowCount - 1 do begin
    	if SG1.Cells[1,i] = '101' then begin
       	jogos	:= SG1.Cells[3,i];
           joger	:= SG1.Cells[5,i];
       end;
    	if SG1.Cells[1,i] = '102' then begin
       	utlev	:= SG1.Cells[3,i];
           utler	:= SG1.Cells[5,i];
       end;
    	if SG1.Cells[1,i] = '103' then begin
			nejsz	:= SG1.Cells[3,i];
			nejer	:= SG1.Cells[5,i];
		end;
		if SG1.Cells[1,i] = '104' then begin
			adrsz	:= SG1.Cells[3,i];
			adrer	:= SG1.Cells[5,i];
		end;
	end;

  if (not CBAR.Checked) and (SGCSOP.RowCount=1) then  begin
    	if NoticeKi('A dolgoz�i csoport megad�sa k�telez�!. K�v�nja most megadni?', NOT_QUESTION) = 0 then begin
        PageControl1.ActivePageIndex:=3;
				BitBtn10.Setfocus;
     		Exit;
        end;
    end;

	// A mozg�b�r kateg�ria meg�llap�t�sa
	mbkat	:= '';
	mb1		:= M12.Text;
	if 	mb1 <> '' then begin
		i := StrToIntDef(EgyebDlg.EvSzam,0) - StrToIntDef(copy(mb1,1,4),0);
		if copy(mb1,6,5) = '01.01' then begin
			i := i + 1;
		end;
		if i < 0 then begin
			i := 0;
		end;
		mbkat	:= IntToStr(i);
	end;
  gkkat:= Query_Select('GEPKOCSI', 'GK_RESZ', M3.Text, 'GK_GKKAT');
  if gkkat<> '' then begin
    {
    // kivettem, hogy ne f�ggj�n egym�st�l egy a k�t besorol�s. 2017-04-03
    Query_Run (Query1, 'select * from szotar where sz_fokod=420 and sz_menev like '''+'%'+gkkat+';%''',true)  ;
    cskod:=Query1.FieldByName('SZ_ALKOD').AsString ;
    if (cskod<>'') and (cskod<>docskod) then      // a rendsz�m m�sik gk.kateg�ri�ba tartozik, ez�rt a dolgoz�i kateg�ri�t is meg kell v�ltoztatni.
    begin
      Query_Run (Query1, 'select * from szotar where sz_fokod=400 and sz_alkod='+cskod,true)  ;
      docsnev:=Query1.FieldByName('SZ_MENEV').AsString ;
      docskod:=cskod;
      docsdat:=EgyebDlg.MaiDatum;
      // megn�zz�k hogy nincs-e m�g ilyen
      Darab:= StrToInt(Query_SelectString('DOLGOZOCSOP', 'select count(*) from dolgozocsop where DC_DOKOD='''+M1.Text+''' and DC_DATTOL='''+docsdat+''''));
      if Darab=0 then
          Query_Insert (Query1, 'DOLGOZOCSOP',
              [
              'DC_DOKOD', ''''+M1.Text+'''',
              'DC_DONEV', ''''+M2.text+'''',
              'DC_CSKOD', ''''+docskod+'''',
              'DC_CSNEV', ''''+docsnev+'''',
              'DC_DATTOL',''''+docsdat+''''
              ]);
      end;
      }
    end;

	{A r�gi rekord m�dos�t�sa}
	Query_Update (Query1, 'DOLGOZO',
		[
		'DO_NAME', 		''''+M2.Text+'''',
		'DO_SKOD', 		''''+M1_.Text+'''',
		'DO_RENDSZ', 	''''+M3.Text+'''',
		'DO_GKKAT',     ''''+gkkat+'''',
		'DO_JOKAT', 	''''+M30.Text+'''',
		'DO_JOGOS', 	''''+copy(jogos,1,11)+'''',
		'DO_JOGER', 	''''+joger+'''',
		'DO_UTLEV', 	''''+copy(utlev,1,11)+'''',
		'DO_UTLER', 	''''+utler+'''',
		'DO_NEJSZ', 	''''+copy(nejsz,1,6)+'''',
		'DO_NEJER', 	''''+nejer+'''',
		'DO_ADRSZ', 	''''+copy(adrsz,1,6)+'''',
		'DO_ADRER', 	''''+adrer+'''',
		'DO_BELEP', 	''''+M12.Text+'''',
		'DO_KILEP', 	''''+M12B.Text+'''',
		'DO_KITERV', 	''''+meUtolsoMunkanap.Text+'''',
		'DO_KIOKA', 	''''+cbTavozasOka.Text+'''',
		'DO_EMAIL', 	''''+M13.Text+'''',
    'DO_EGYEBTEL', 	''''+meEgyebTel.Text+'''',
    'DO_IRODAITEL', 	''''+meIrodaiTel.Text+'''',
    'DO_BELSOMELLEK', 	''''+meBelsoMellek.Text+'''',
    'DO_VCDADD', 	''''+meVCDADD.Text+'''',
    'DO_BEOSZTAS', 	''''+cbBeosztas.Text+'''',
		'DO_KOZOSLISTA', 	BoolToIntStr(chkKozosLista.Checked),
		'DO_MBKAT', 	''''+mbkat+'''',
		'DO_KULSO', 	BoolToIntStr(CBKUL.Checked),
		'DO_NEMZE', 	BoolToIntStr(CBNEM.Checked),
//		'DO_ARHIV', 	BoolToIntStr(CBAR.Checked),
		'DO_ARHIV', 	BoolToIntStr(M12B.Text<>EmptyStr),
		'DO_CHMOZ', 	BoolToIntStr(CB5.Checked),
		'DO_ALKAL', 	BoolToIntStr(CB6.Checked),
		'DO_ICLOUDMAIL', 	BoolToIntStr(chkICLOUD.Checked),
		'DO_BERKOD', 	''''+MaskEdit1.Text+'''',
		'DO_SZABI', 	IntToStr(StrToIntDef(MaskEdit2.Text,0)),
	 //	'DO_SZABI', 	StrToIntDef(MaskEdit2.Text,0),
		'DO_TAJSZ', 	''''+MM1.Text+'''',
		'DO_DFEOR', 	''''+MM2.Text+'''',
		'DO_ADOSZ', 	''''+MM3.Text+'''',
		'DO_SZULH', 	''''+MM4.Text+'''',
		'DO_SZIDO', 	''''+MM5.Text+'''',
		'DO_ANYAN', 	''''+MM6.Text+'''',
		'DO_LAIRS', 	''''+MM7.Text+'''',
		'DO_LATEL', 	''''+MM8.Text+'''',
		'DO_LACIM', 	''''+MM9.Text+''''
		// 'DO_CSDAT', 	''''+docsdat+'''',
		// 'DO_CSKOD', 	''''+docskod+'''',
		// 'DO_CSNEV', 	''''+docsnev+''''
		], ' WHERE DO_KOD = '''+ret_kod+''' ' );

	// A garancia t�bla t�lt�se
	Query_Run ( Query1, 'DELETE FROM DOLGOZOERV WHERE DE_DOKOD = '''+M1.Text+''' ',true) ;
	ujkod	:= GetNextCode('DOLGOZOERV', 'DE_DEKOD');
	for i := 1 to SG1.RowCount -1 do begin
		if SG1.Cells[4,i] = 'Igen' then begin
			// Besz�runk a t�bl�ba
			Query_Insert (Query1, 'DOLGOZOERV',
				[
				'DE_DEKOD', IntToStr(ujkod),
				'DE_DOKOD', ''''+M1.Text+'''',
				'DE_ALKOD', ''''+SG1.Cells[1,i]+'''',
				'DE_MEGJE', ''''+SG1.Cells[3,i]+'''',
				'DE_DATUM', ''''+SG1.Cells[5,i]+''''
				]);
			Inc(ujkod);
		end;
  	end;
   // A g�pkocsi t�bl�n a sof�r oszlop friss�t�se
   SofornevFrissites(M2.Text, M3.Text);
   // A kieg�sz�t� adatok friss�t�se
   sqlstr := 'UPDATE DOLGOZOMEG SET ';
   for i := 1 to MEZOSZAM do begin
   	if mezonevek[i] <> '' then begin
       	sqlstr	:= sqlstr + ' ' + mezonevek[i] + ' = ''' + mezok[i].Text +''', ';
       end;
   end;
   sqlstr := sqlstr + 'DM_DOKOD = '''+ret_kod+''' WHERE DM_DOKOD = '''+ret_kod+''' ' ;
  	if not Query_Run ( Query1, sqlstr, false) then begin
		NoticeKi('Figyelem! A kieg�sz�t� mez�k t�rol�sa nem siker�lt!');
   end;
   Result:=True;
end;

procedure TDolgozobe2Dlg.M2Enter(Sender: TObject);
begin
//  if EgyebDlg.v_formatum=0  then // JS
//  M30.SetFocus;
end;

procedure TDolgozobe2Dlg.MM1Enter(Sender: TObject);
begin
//  if EgyebDlg.v_formatum=0  then // JS
//  MaskEdit2.SetFocus;

end;

procedure TDolgozobe2Dlg.M1_Enter(Sender: TObject);
begin
  if M1_.Text<>'' then
    M1.SetFocus;
end;

{ ez lett volna az �j, de nem j�
procedure TDolgozobeDlg.BitBtn19Click(Sender: TObject);
var
  OldCursor: TCursor;
begin
  if NoticeKi('Indulhat a Servantes adatok �tv�tele?', NOT_QUESTION ) = 0 then begin
      OldCursor:= Screen.Cursor;  // elmentj�k
      Screen.Cursor	:= crHourGlass;
      EgyebDlg.DolgServantes;
      Tolto(Caption,M1.Text);   // megjelen�tj�k az adatokat
      Screen.Cursor	:= OldCursor;
      end;
end;
}

procedure TDolgozobe2Dlg.BitBtn19Click(Sender: TObject);
var
  svkod, proc, sql :string;
begin
  if EgyebDlg.KONYVELES_ATAD_FORMA = 'SERVANTES' then begin
       if M1_.Text='' then begin
           exit;
       end;

       if EgyebDlg.TESZTPRG then begin
           proc:='DOLG_FUVAROSBA_TESZT '
       end else begin
           proc:='DOLG_FUVAROSBA ';
       end;

      svkod:= Stringofchar('0',5-length(Trim(M1_.Text)))+Trim(M1_.Text);

       if not EgyebDlg.Vanservantes then begin
           NoticeKi('A Servantes megnyit�s nem siker�lt!');
           Exit;
       end;
       SQuery1.Connection:=EgyebDlg.ADOConnectionServantes;
      sql:='EXEC '+proc+''''+svkod+'''';
      if not Query_Run(SQuery1, sql, FALSE) then begin
           // nem siker�lt
           ShowMessage('A friss�t�s nem siker�lt!');
           EgyebDlg.ADOConnectionServantes.Close;
       end else begin
           EgyebDlg.ADOConnectionServantes.Close;
           Tolto(Caption,M1.Text);
           ShowMessage('K�sz!');
       end;
      end
  else begin
      NoticeKi('A Kulcs B�r programb�l a dolgoz�k adatait a "Dolgoz�k karbantart�sa" ablakon a "B�rprogram adatok �tv�tele" gombbal lehet �tvenni!');
      end;
end;


end.
