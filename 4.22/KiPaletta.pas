unit KiPaletta;

interface

uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls, Egyeb,
  Forms, Dialogs, DBTables, DB, StdCtrls, Buttons, Mask, DBCtrls,
  ExtCtrls,DateUtils, Data.Win.ADODB;

type
  TKiPalettaDlg = class(TForm)
    BitBtn4: TBitBtn;
    BitBtn6: TBitBtn;
    Label3: TLabel;
    M1: TMaskEdit;
    Label1: TLabel;
    BitBtn2: TBitBtn;
    CB1: TCheckBox;
    CB11: TComboBox;
    Query1: TADOQuery;
    procedure BitBtn4Click(Sender: TObject);
    procedure BitBtn6Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure M1Exit(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
		helylist		: TStringList;
  public
  end;

var
  KiPalettaDlg: TKiPalettaDlg;

implementation

uses
	J_Sql, KiPalettaList, Kozos;
{$R *.DFM}

procedure TKiPalettaDlg.BitBtn4Click(Sender: TObject);
begin
	 {A lista �ssze�ll�t�sa a sz�r�si elemek alapj�n}
   Screen.Cursor	:= crHourGlass;
   Application.CreateForm(TKiPalettaListDlg, KiPalettaListDlg);
   KiPalettaListDlg.Tolt(M1.Text, helylist[CB11.ItemIndex], CB1.Checked);
   Screen.Cursor	:= crDefault;
   if KiPalettaListDlg.vanadat	then begin
     	KiPalettaListDlg.Rep.Preview;
   end else begin
	  	NoticeKi('Nincs a felt�teleknek megfelel� t�tel!');
   end;
   KiPalettaListDlg.Destroy;
end;

procedure TKiPalettaDlg.BitBtn6Click(Sender: TObject);
begin
	Close;
end;

procedure TKiPalettaDlg.FormCreate(Sender: TObject);
begin
   // A helysz�nek felt�lt�se
 	EgyebDlg.SetADOQueryDatabase(Query1);
  	helylist 	:= TStringList.Create;
   CB11.Clear;
   helylist.Add('');
   CB11.Items.Add('Minden hely');
   Query_Run (Query1, 'SELECT * FROM SZOTAR WHERE SZ_FOKOD = ''140'' ',true);
   while not Query1.Eof do begin
       helylist.Add('0'+Query1.FieldByName('SZ_ALKOD').AsString);
       CB11.Items.Add(Query1.FieldByName('SZ_MENEV').AsString);
       Query1.Next;
   end;
   Query_Run (Query1, 'SELECT DISTINCT PV_HEKO1 AS HEKOD, PV_HETI1 AS HETIP FROM PALVALT UNION SELECT DISTINCT PV_HEKO2 AS HEKOD, PV_HETI2 AS HETIP FROM PALVALT ORDER BY 1');
   while not Query1.Eof do begin
       if ( ( Trim(Query1.FieldByName('HEKOD').AsString) <> '' ) and (Trim(Query1.FieldByName('HETIP').AsString) <> '0') ) then begin
           helylist.Add('1'+Query1.FieldByName('HEKOD').AsString);
           CB11.Items.Add(Query1.FieldByName('HEKOD').AsString);
       end;
       Query1.Next;
   end;
  	CB11.ItemIndex  := 0;
   M1.Text         := EgyebDlg.MaiDatum;
end;

procedure TKiPalettaDlg.BitBtn2Click(Sender: TObject);
begin
	EgyebDlg.Calendarbe(M1);
end;

procedure TKiPalettaDlg.M1Exit(Sender: TObject);
begin
	if ( ( M1.Text <> '' ) and ( not Jodatum2(M1 ) ) ) then begin
		NoticeKi('Hib�s d�tum megad�sa!');
		M1.Text := '';
		M1.Setfocus;
	end;
end;

end.


