   unit PontokBe;

interface

uses
	SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
	Forms, Dialogs, StdCtrls, Buttons, Mask, DB, DBTables, Egyeb, J_SQL,
   VevoUjfm, ExtCtrls, Kozos, ADODB,J_Alform;

type
	TPontokBeDlg = class(TJ_AlformDlg)
	BitElkuld: TBitBtn;
	BitKilep: TBitBtn;
	Label2: TLabel;
    M1: TMaskEdit;
	Label12: TLabel;
    Label1: TLabel;
    CCsoport: TComboBox;
    Label4: TLabel;
    Query1: TADOQuery;
    Query2: TADOQuery;
    M0: TMaskEdit;
    Label3: TLabel;
    CDolgozo: TComboBox;
    M4: TMaskEdit;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    M3: TMaskEdit;
    BitBtn1: TBitBtn;
    Label8: TLabel;
    Query3: TADOQuery;
	procedure BitKilepClick(Sender: TObject);
	procedure FormCreate(Sender: TObject);
	procedure Tolto(cim : string; teko:string); override;
	procedure BitElkuldClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormDestroy(Sender: TObject);
    procedure M3KeyPress(Sender: TObject; var Key: Char);
    procedure M3Exit(Sender: TObject);
    procedure DolgozoTolto;
    procedure CCsoportChange(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    function  Elkuld : boolean;
    procedure FormShow(Sender: TObject);
    procedure CDolgozoChange(Sender: TObject);
	private
       listacsop   : TStringLIst;
       listadolg   : TStringLIst;
       datum       : string;
	public
	end;

var
	PontokBeDlg: TPontokBeDlg;

implementation

uses
	Kozos_Local, Math;
{$R *.DFM}

procedure TPontokBeDlg.BitKilepClick(Sender: TObject);
begin
   Close;
end;

procedure TPontokBeDlg.FormCreate(Sender: TObject);
begin
	EgyebDlg.SetADOQueryDatabase(Query1);
	EgyebDlg.SetADOQueryDatabase(Query2);
   EgyebDlg.SetADOQueryDatabase(Query3);
	ret_kod 	:= '';
	listacsop	:= TStringList.Create;
   listadolg   := TStringList.Create;
   datum           := copy(EgyebDlg.MaiDatum, 1, 8)+'01.';
   datum           := DatumHoznap(datum, -1, true);
   datum           := copy(datum, 1, 8)+'01.';
   Label8.Caption  := copy(datum, 1, 5) + ' ' + FormatSettings.LongMonthNames[StrToIntDef(copy(datum, 6,2), 1)] ;
   Query_Run (Query2, 'SELECT DISTINCT DC_CSKOD, DC_CSNEV FROM PONTOZO, DOLGOZOCSOP WHERE PO_ALKOD = DC_CSKOD AND PO_FEKOD = '''+EgyebDlg.user_code+''' ');
   while not Query2.Eof do begin
       listacsop.Add(Query2.FieldByName('DC_CSKOD').AsString);
       CCsoport.Items.Add(Query2.FieldByName('DC_CSNEV').AsString);
       Query2.Next;
   end;
   if CCsoport.Items.Count > 0 then begin
       CCsoport.ItemIndex  := 0;
       DolgozoTolto;
   end else begin
       BitElkuld.Hide;
       BitBtn1.Hide;
   end;
   CCsoport.Enabled    := false;
end;

procedure TPontokBeDlg.Tolto(cim : string; teko:string);
begin
	ret_kod 		:= teko;
	Caption 		:= cim;
	M0.Text 		:= teko;
   M1.Text         := EgyebDlg.user_name;
   SetMaskEdits([M0, M1]);
	if ret_kod <> '' then begin		{M�dos�t�s}
		if Query_Run (Query2, 'SELECT * FROM PONTOK WHERE PP_PPKOD = '+teko,true) then begin

   		// CCsoport.ItemIndex 	:= Max(0, listacsop.IndexOf(Query2.FieldByName('PP_ALKOD').AsString));
			// CDolgozo.ItemIndex 	:= Max(0, listadolg.IndexOf(Query2.FieldByName('PP_DOKOD').AsString));

      // mivel a DolgozoTolto pont azokat teszi bele, akiknek m�g nincs pontjuk,
      // lecser�lj�k a list�kat, hogy csak a m�dos�tand� adatsor elemeit tartalmazz�k
      listacsop.Clear;
      CCsoport.Items.Clear;
      Query_Run (Query3, 'SELECT DC_CSKOD, DC_CSNEV FROM DOLGOZOCSOP WHERE DC_CSKOD = '''+Query2.FieldByName('PP_ALKOD').AsString+''' ');
      if not Query3.Eof then begin
         listacsop.Add(Query3.FieldByName('DC_CSKOD').AsString);
         CCsoport.Items.Add(Query3.FieldByName('DC_CSNEV').AsString);
      end;  // if

      listadolg.Clear;
      CDolgozo.Items.Clear;
      Query_Run (Query3, 'SELECT DO_NAME, DO_KOD FROM DOLGOZO WHERE DO_KOD = '''+Query2.FieldByName('PP_DOKOD').AsString+''' ');
      if not Query3.Eof then begin
           listadolg.Add(Query3.FieldByName('DO_KOD').AsString);
           CDolgozo.Items.Add(Query3.FieldByName('DO_NAME').AsString);
      end;  // if

      // -------------------------------------------------------------
			CCsoport.ItemIndex 	:= 0;  // az egyetlen elem
			CDolgozo.ItemIndex 	:= 0;  // az egyetlen elem

      datum               := Query2.FieldByName('PP_DATUM').AsString;
      Label8.Caption      := copy(datum, 1, 5) + ' ' + Formatsettings.LongMonthNames[StrToIntDef(copy(datum, 6,2), 1)] ;
			M3.Text 		    := Format('%.2f', [StringSzam(Query2.FieldByName('PP_UPONT').AsString)]);
			M4.Text 		    := Query2.FieldByName('PP_MEGJE').AsString;
		end;

    CCsoport.Enabled := false;
    CDolgozo.Enabled := false;
    BitElkuld.Show;
    BitBtn1.Hide;
	end;
end;

procedure TPontokBeDlg.BitElkuldClick(Sender: TObject);
begin
   if Elkuld then begin
       Close;
   end;
end;

procedure TPontokBeDlg.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
	EgyebDlg.FormReturn(Key);
end;

procedure TPontokBeDlg.FormDestroy(Sender: TObject);
begin
   listacsop.Free;
   listadolg.Free;
end;

procedure TPontokBeDlg.M3KeyPress(Sender: TObject; var Key: Char);
begin
	with Sender as TMaskEdit do begin
		Key := EgyebDlg.Jochar(Text,Key,12,2);
	end;
end;

procedure TPontokBeDlg.M3Exit(Sender: TObject);
begin
	SzamExit(Sender);
end;

procedure TPontokBeDlg.DolgozoTolto;
var
   S: string;
begin
   BitElkuld.Show;
   BitBtn1.Show;
   listadolg.Clear;
   CDolgozo.Items.Clear;
   S:='SELECT DISTINCT DO_NAME, DO_KOD FROM DOLGOZO, DOLGOZOCSOP WHERE DO_KOD = DC_DOKOD AND DO_ARHIV = 0 ';
   S:=S+ ' AND DO_KOD NOT IN (SELECT PP_DOKOD FROM PONTOK WHERE PP_DATUM = '''+datum+''' AND PP_FEKOD = '''+EgyebDlg.user_code+''' ) ';
   S:=S+ ' AND DO_KOD IN (SELECT PO_DOKOD FROM PONTOZO WHERE PO_FEKOD = '''+EgyebDlg.user_code+''' ) ';
   S:=S+ ' AND convert(int,DO_KOD) not in (select JE_DOKOD from JELSZO where JE_FEKOD = '+EgyebDlg.user_code+' AND JE_DOKOD <> '''' )';  // saj�t magunkat nem l�tjuk
   S:=S+ ' AND DO_BELEP<'''+datum+''' ';     // az az�ta bel�pett dolgoz�k sem kellenek
   S:=S+ ' ORDER BY DO_NAME ';
   Query_Run (Query2, S);
   while not Query2.Eof do begin
       if Query2.FieldByName('DO_NAME').AsString <> '' then begin
           listadolg.Add(Query2.FieldByName('DO_KOD').AsString);
           CDolgozo.Items.Add(Query2.FieldByName('DO_NAME').AsString);
       end;
       Query2.Next;
   end;
   if CDolgozo.Items.Count > 0 then begin
       CDolgozo.ItemIndex  := 0;
       CDolgozoChange(Self);
   end else begin
       BitElkuld.Hide;
       BitBtn1.Hide;
   end;
end;

procedure TPontokBeDlg.CCsoportChange(Sender: TObject);
begin
   DolgozoTolto;
end;

procedure TPontokBeDlg.CDolgozoChange(Sender: TObject);
begin
   // A dolgoz�i csoport be�ll�t�sa
   Query_Run (Query3, 'SELECT DC_CSKOD, DC_DOKOD FROM DOLGOZOCSOP WHERE DC_DOKOD = '''+listadolg[CDolgozo.ItemIndex]+''' ORDER BY DC_DATTOL');
   if Query3.RecordCount > 0  then begin
       Query3.Last;
       CCsoport.ItemIndex 	:= Max(0, listacsop.IndexOf(Query3.FieldByName('DC_CSKOD').AsString));
   end;
end;

procedure TPontokBeDlg.BitBtn1Click(Sender: TObject);
begin
   // Elk�ld�s �s tov�bbl�p�s
   ret_kod := '';
   If Elkuld then begin
       DolgozoTolto;
       M3.Text := '';
       M4.Text := '';
       M3.SetFocus;
   end;
end;

function TPontokBeDlg.Elkuld : boolean;
begin
   Result  := false;
	if ret_kod = '' then begin
       // Megl�v� ellen�rz�se
		Query_Run ( Query2, 'SELECT PP_PPKOD FROM PONTOK WHERE PP_DATUM = '''+datum+''' '+
               ' AND PP_FEKOD = '''+EgyebDlg.user_code+''' '+
               ' AND PP_DOKOD = '''+listadolg[CDolgozo.ItemIndex]+''' ',true);
       if Query2.RecordCount > 0 then begin
           NoticeKi('L�tez� bejegyz�s!');
           Exit;
       end;
		{�j rekord felvitele}
		ret_kod := IntToStr(GetNextStrCode('PONTOK', 'PP_PPKOD', 1, 5,True));
		Query_Run ( Query2, 'INSERT INTO PONTOK (PP_PPKOD) VALUES ('+ret_kod+' )',true);
	end;
	{A r�gi rekord m�dos�t�sa}
   Query_Update( Query2, 'PONTOK',
       [
       'PP_ALKOD', ''''+ listacsop[CCsoport.ItemIndex] +'''',
       'PP_DATUM', ''''+ datum +'''',
       'PP_DOKOD', ''''+ listadolg[CDolgozo.ItemIndex] +'''',
       'PP_DONEV', ''''+ CDolgozo.Text +'''',
       'PP_MEGJE', ''''+ M4.Text +'''',
       'PP_FEKOD', ''''+ EgyebDlg.user_code +'''',
       'PP_UPONT', SqlSzamString( StrinGSzam(M3.Text) , 12, 2)
       ], ' WHERE PP_PPKOD = '+ret_kod );
   Result  := true;
end;
procedure TPontokBeDlg.FormShow(Sender: TObject);
begin
   if ret_kod <> '' then begin
       M3.SetFocus;
   end;
end;

end.

