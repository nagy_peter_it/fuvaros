unit Arfolyamtolto2;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, InvokeRegistry, Rio, SOAPHTTPClient, Mnb, StdCtrls, ExtCtrls,
  ComCtrls, DB, ADODB;

type
  TFArfolyamtolto2 = class(TForm)
    RateMemo: TMemo;
    Panel1: TPanel;
    Button1: TButton;
    Button2: TButton;
    Rio: THTTPRIO;
    DateTimePicker1: TDateTimePicker;
    Label1: TLabel;
    Label2: TLabel;
    Query1: TADOQuery;
    Button4: TButton;
    Button3: TButton;
    procedure FormShow(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure DateTimePicker1Change(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    { Private declarations }
  valutanemek: string;
  kilepes: boolean;
  public
    { Public declarations }
  end;

  Procedure GetCurrentExchangeRates(aList:TStrings);
  Function ExchangeRatesFromXML(Const aXML:AnsiString):TStringList;

var
  FArfolyamtolto2: TFArfolyamtolto2;

implementation

uses Egyeb, Fomenu,J_SQL;

{$R *.dfm}
Procedure GetCurrentExchangeRates(aList:TStrings);
Begin
  aList.Assign(ExchangeRatesFromXML((FArfolyamtolto2.Rio as
    MNBArfolyamServiceSoap).GetCurrentExchangeRates));
End;

Function GetCurrentExchangeRates2(aList:TStrings): integer;
Begin
  Result:=0;
  aList.Assign(ExchangeRatesFromXML((FArfolyamtolto2.Rio as
    MNBArfolyamServiceSoap).GetCurrentExchangeRates));
  Result:=aList.Count;
End;

Procedure GetExchangeRates(startDate: string; endDate: string; currencyNames: string;aList:TStrings );
Begin
  aList.Assign(ExchangeRatesFromXML((FArfolyamtolto2.Rio as
    MNBArfolyamServiceSoap).GetExchangeRates(startDate,endDate,currencyNames)));
End;

Function GetExchangeRates2(startDate: string; endDate: string; currencyNames: string;aList:TStrings ): integer;
Begin
  Result:=0;
  aList.Assign(ExchangeRatesFromXML((FArfolyamtolto2.Rio as
    MNBArfolyamServiceSoap).GetExchangeRates(startDate,endDate,currencyNames)));
  Result:=aList.Count;
End;

Function ExchangeRatesFromXML(Const aXML:AnsiString):TStringList;
Var S,WS:TSysCharSet;
    I:Integer;
begin
  Result:=TStringList.Create;
  S:=['<','>'];
  WS:=[];

  Result.BeginUpdate;
  ExtractStrings(S,WS,PChar(aXML),Result);
  Result.EndUpdate;

  if Result.Count=1 then
  begin
    Result.Delete(0);
    exit;
  end;
  Result.Delete(1);
  Result.Delete(0);

  Result.BeginUpdate;
  For I:=Result.Count-1 Downto 0 do If Result[I][1]='/' Then Result.Delete(I);
  For I:=0 To Result.Count-1 do If (I mod 2)=0 Then Result[I]:=Copy(Result[I],12,3);
  
  For I:=Result.Count-1 Downto 0 do
    Begin
      If (I mod 2)=0 Then
        Begin
          Result[I]:=Result[I]+'='+Result[I+1];
          Result.Delete(I+1);
        End;
    End;
  //Result.Add(DateTimeToStr(now)) ;
  Result.EndUpdate;
  FArfolyamtolto2.valutanemek:='';
  For I:=0 to Result.Count-1 do
  Begin
    FArfolyamtolto2.valutanemek:=FArfolyamtolto2.valutanemek+copy(Result[i],1,3)+',';
  End;
end;

procedure TFArfolyamtolto2.FormShow(Sender: TObject);
begin
  if kilepes then
  begin
    FArfolyamtolto2.Close;
   // FArfolyamtolto2.Destroy;
    exit;
  end;
 // Rio.WSDLLocation:=ExtractFilePath(Application.ExeName)+'mnb.wsdl';
  DateTimePicker1.Date:=date;
  Button2.OnClick(self);
end;

procedure TFArfolyamtolto2.Button1Click(Sender: TObject);
var
  itemno:integer;
begin
  //GetCurrentExchangeRates(RateMemo.Lines);
  itemno:= GetCurrentExchangeRates2(RateMemo.Lines);
//  ShowMessage(IntToStr(itemno));
end;

procedure TFArfolyamtolto2.Button2Click(Sender: TObject);
var
  itemno:integer;
begin
  itemno:= GetExchangeRates2(DateToStr(DateTimePicker1.Date),DateToStr(DateTimePicker1.Date),valutanemek, RateMemo.Lines);
  Label2.Visible:=itemno=0;
end;
procedure TFArfolyamtolto2.FormCreate(Sender: TObject);
var
  fn:string;
begin
 Try
  fn:=ExtractFilePath(Application.ExeName)+'mnb.wsdl';
  if not FileExists(fn)  then
    ShowMessage('Hi�nyz� f�jl: '+fn);
  Rio.WSDLLocation:=fn;
  GetCurrentExchangeRates2(RateMemo.Lines);
 Except
  ShowMessage('Probl�ma az MNB WEB Service el�r�s�n�l!');
  kilepes:=True;
 End;
end;

procedure TFArfolyamtolto2.DateTimePicker1Change(Sender: TObject);
begin
  Button2.OnClick(self);
end;

procedure TFArfolyamtolto2.Button3Click(Sender: TObject);
var
  i, kod,egyseg: integer;
  vnem, sdatum,segyseg: string;
  arf: double;
begin
  if RateMemo.Lines.Count=0 then
    exit;

  sdatum:=DateToStr( DateTimePicker1.date);
  for i:=0 to RateMemo.Lines.Count-1 do
  begin
    vnem:=copy(RateMemo.Lines[i],1,3);
    arf:=StrToFloat(StringReplace( copy(RateMemo.Lines[i],5,10),',','.',[]));
    ////// t�rol�s
    egyseg:=1;
    segyseg:=Query_Select('ARFOLYAM','AR_VALNEM',vnem,'AR_EGYSEG');
    if segyseg<>'' then egyseg:=StrToInt(segyseg);
    Query_Run(Query1,'select * from arfolyam where ar_valnem='+#39+vnem+#39+' and ar_datum='+#39+sdatum+#39);
    if Query1.RecordCount<>0 then  // van m�r ilyen
      Continue;

		kod	:= GetNextCode('ARFOLYAM', 'AR_KOD', 1, 5);
		Query_Insert (Query1, 'ARFOLYAM',
						[
						'AR_KOD', 	 IntToStr(kod),
						'AR_EGYSEG', IntToStr(egyseg),
						'AR_VALNEM', ''''+vnem+'''',
						'AR_ERTEK', SqlSzamString(arf,12,4),
						'AR_DATUM', ''''+sdatum+'''',
						'AR_MEGJE', ''''+'MNB WEB Service'+''''
						] );
  end;
  if FArfolyamtolto2.Showing then
    ShowMessage('K�sz!');
end;

procedure TFArfolyamtolto2.Button4Click(Sender: TObject);
var
  i:integer;
begin
  for i:=0 to 2 do
  begin
    DateTimePicker1.Date:=Date-i;
    DateTimePicker1.OnChange(self);
    Button3.OnClick(self);
  end;
end;

procedure TFArfolyamtolto2.FormActivate(Sender: TObject);
begin
  if kilepes then
  begin
    FArfolyamtolto2.Close;
    exit;
  end;
end;

procedure TFArfolyamtolto2.FormDestroy(Sender: TObject);
begin
  Rio.Free;
end;

end.
