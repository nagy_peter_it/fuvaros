unit FogyasztSzamolo;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls,
  Kozos, KozosTipusok, Kozos_Local, Data.DB, Data.Win.ADODB;

type
  E_res= array[1..4] of string;
  TDoubleValidAdat = record
      HUFValue: double;
      EURValue: double;
      IsValid: boolean;
      VanAdat: boolean;
      end;

  TFogyasztSzamoloDlg = class(TForm)
    Query1: TADOQuery;
    Query5: TADOQuery;
    Query3: TADOQuery;
    Query2: TADOQuery;
    QueryAlap: TADOQuery;
    Query6: TADOQuery;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
  public
    function Jarat_Egyeb_km(jakod: string): TDoubleValidAdat;
 	  function Jarat_Egyeb_ktg(jaratkod : string ): TDoubleValidAdat;
  end;

var
  FogyasztSzamoloDlg: TFogyasztSzamoloDlg;

implementation

uses
	Egyeb, J_SQL, J_Fogyaszt, StrUtils;
{$R *.dfm}

function TFogyasztSzamoloDlg.Jarat_Egyeb_km(jakod: string): TDoubleValidAdat;
var
  mbdb, okm: integer;
  oktgEUR, oktg: double;
  ejakod,ejasza,ejaev: string;
  jaratok,fajta, S: string;
  res: E_res;
  KtgRecord: TDoubleValidAdat;
begin
  //mbkod:=StrToIntDef( Query_Select('JARATMEGBIZAS','JM_JAKOD',jakod,'JM_MBKOD'),-1);
  Result.HUFValue:=0;
  Result.EURValue:=0;
  Result.IsValid:= True;

  S:='SELECT DISTINCT ms_jakod,ms_jasza,ms_fajta, substring(MS_DATUM,1,4) ev FROM MEGSEGED, MEGBIZAS where ms_mbkod=mb_mbkod and mb_jakod='+jakod +' and ms_fajko<>0';
  S:=S+' union ';
  S:=S+' select DISTINCT ja_kod, JA_ORSZ+''-''+convert(varchar,convert(int, ja_alkod)), ''alj�rat'', substring(JA_JKEZD,1,4) from JARAT WHERE JA_FOJAR='+jakod;

//	if Query_Run(QueryAlap, 'SELECT DISTINCT ms_jakod,ms_jasza,ms_fajta FROM MEGSEGED where ms_mbkod='+IntToStr(mbkod)+' and ms_fajko<>0') then
	if Query_Run(QueryAlap, S) then
  begin
    okm:=0;
    oktg:=0;
    oktgEUR:=0;
    jaratok:='';
    while not QueryAlap.EOF do    // MEGSEGED
    begin
      fajta:=QueryAlap.Fieldbyname('MS_FAJTA').AsString ;
      ejakod:=QueryAlap.Fieldbyname('MS_JAKOD').AsString ;
      ejasza:=QueryAlap.Fieldbyname('MS_JASZA').AsString ;
      ejaev:=QueryAlap.Fieldbyname('ev').AsString ;
      if ejakod<>'' then begin
        if fajta= 'alj�rat' then begin
          mbdb:= 1;
          end
        else begin
          Query_Run(Query6, 'select DISTINCT mb_jasza from MEGSEGED, megbizas where ms_mbkod=mb_mbkod and ms_jasza='''+ejasza+''' '+
                ' and substring(MS_DATUM,1,4)='''+ejaev+'''');
          mbdb:= Query6.RecordCount;
          end;
      	Query_Run(Query6, 'SELECT JA_OSSZKM FROM JARAT where ja_kod='''+ejakod+'''');
        KtgRecord:= Jarat_Egyeb_ktg(ejakod);
        if mbdb>0 then begin
          okm:= okm+ Trunc(Query6.Fieldbyname('JA_OSSZKM').AsInteger / mbdb);
          oktg:= oktg+ KtgRecord.HUFValue / mbdb;
          oktgEUR:= oktgEUR+ KtgRecord.EURValue / mbdb;
          if not(KtgRecord.IsValid) then
              res[4]:='0'; // ha egy is invalid, akkor az eg�sz eredm�ny az
          jaratok:=jaratok+ejasza+IfThen(mbdb>1,'/'+IntToStr(mbdb))+' '+fajta+',' ;
          end
        else begin
          okm:= okm+ Trunc(Query6.Fieldbyname('JA_OSSZKM').AsInteger);
          oktg:= oktg+ Trunc(KtgRecord.HUFValue);
          oktgEUR:= oktgEUR+ Trunc(KtgRecord.EURValue);
          if not(KtgRecord.IsValid) then
              Result.IsValid:= False; // ha egy is invalid, akkor az eg�sz eredm�ny az
          jaratok:=jaratok+ejasza+' '+fajta+',' ;
        end;
      end;
      QueryAlap.Next;
    end;
  end;
  Result.HUFValue:=oktg;
  Result.EURValue:=oktgEUR;
  Result.IsValid:= True;
end;

function	TFogyasztSzamoloDlg.Jarat_Egyeb_ktg(jaratkod : string ): TDoubleValidAdat;
var
	ertek, szossz, feldij, felra1, uzkolt, uzmegtak, loc_HUFokolts, loc_EURokolts	: double;
	sqlstr, sof1, jojarat, voltsofor, jaratsz: string;
	i, tipus			: integer;
  ossz,ossz2, EURossz, TenyFogyasztas: double;
  selkm1, selkm2, sextra1, sedij1, sedij2, minimumkt, maximumkt: double;
  siker: boolean;

begin
  Result.IsValid:=True;  // am�g nem �rv�nytelen�tj�k
  selkm1		:= 0;
  selkm2		:= 0;
  sedij1		:= 0;
  sedij2		:= 0;
  loc_HUFokolts:= 0;
  loc_EURokolts:= 0;
  ossz      := 0;
  ossz2     := 0;
  EURossz      := 0;
  Result.vanadat 	:= false;
  feldij		:= StringSzam(EgyebDlg.Read_SZGrid('CEG', '301'));
  minimumkt	:= StringSzam(EgyebDlg.Read_SZGrid('999', '742'));
  maximumkt	:= StringSzam(EgyebDlg.Read_SZGrid('999', '743'));
  // El�sz�r lesz�rj�k a j�ratokat a megadott felt�telek alapj�n
  sqlstr	:= 'SELECT * FROM JARAT WHERE JA_KOD = '''+jaratkod+''' ';
  siker:= Query_Run (Query1, sqlstr ,true);
  if siker then begin
    while not Query1.EOF do begin
      jaratsz			:= Query1.FieldByName('JA_KOD').AsString;
      jojarat	:= GetJaratszamFromDb(Query1);
      Result.vanadat 	:= true;
      // A j�rathoz tartoz� fogyaszt�si adatok kisz�m�t�sa

      FogyasztasDlg.ClearAllList;
      FogyasztasDlg.JaratBetoltes(jaratkod);
      FogyasztasDlg.CreateTankLista;
      FogyasztasDlg.CreateAdBlueLista;
      FogyasztasDlg.Szakaszolo;

      // A dolgoz�i adatok bet�tele
      voltsofor	:= '';
      Query_Run(Query5, 'SELECT * FROM JARSOFOR WHERE JS_JAKOD = '''+jaratsz+''' ORDER BY JS_SORSZ ');
      if Query5.RecordCount > 0 then begin
        while not Query5.Eof do begin
          sof1		:= Query_Select('DOLGOZO', 'DO_KOD', Query5.FieldByname('JS_SOFOR').AsString , 'DO_NAME');
          if sof1 <> '' then begin
            // Betessz�k a sof�r fogyaszt�si adatokat k�l�n sorba
            uzkolt 	:= 0;
            if Pos (Query5.FieldByname('JS_SOFOR').AsString, voltsofor) < 1 then begin
              voltsofor	:= voltsofor + Query5.FieldByname('JS_SOFOR').AsString + ';';
              if  Round(Query1.FieldByname('JA_OSSZKM').AsFloat ) > 0  then begin
                uzmegtak := FogyasztasDlg.GetSoforMegtakaritas(Query5.FieldByname('JS_SOFOR').AsString);
                // Az �zemanyag megtakar�t�s k�lts�g�nek kisz�m�t�sa �s megjelen�t�se
                if uzmegtak <> 0 then begin
                  uzkolt	 				   	:= -1 * uzmegtak * GetApehUzemanyag(Query1.FieldByName('JA_JKEZD').AsString, 1);
                end;
                ossz  		:= ossz + uzkolt;
                ossz2  		:= ossz2 + uzkolt;
                EURossz   := EURossz + GetValueInOtherCur(uzkolt, Query1.FieldByName('JA_JKEZD').AsString, 'HUF', 'EUR' );
              end;
            end;
            selkm1	:= StringSzam(Query5.FieldByname('JS_SZAKM').AsString);
            sedij1	:= StringSzam(Query5.FieldByname('JS_JADIJ').AsString);
            sextra1	:= StringSzam(Query5.FieldByname('JS_EXDIJ').AsString);
            felra1  := StringSzam(Query5.FieldByname('JS_FELRA').AsString);
            ertek:=  (selkm1 * sedij1 + sextra1 + felra1*feldij);
            ossz  								:= ossz - ertek;
            ossz2  								:= ossz2- ertek;
            EURossz   := EURossz - GetValueInOtherCur(ertek, Query1.FieldByName('JA_JKEZD').AsString, 'HUF', 'EUR' );
          end;
          Query5.Next;
        end;
      end;


     // Bet�ltj�k a k�lts�geket a Query1_-be
     if Query_Run (Query3,'SELECT * FROM KOLTSEG WHERE KS_JAKOD = '''+jaratsz+''' '+
      ' AND KS_TETAN = 0 '+
      ' ORDER BY KS_RENDSZ DESC, KS_DATUM' ,true) then begin
      while not Query3.EOF do begin
        tipus 	:= StrToIntdef(Query3.FieldByName('KS_TIPUS').AsString,0);
        // Nett� �rt�ket sz�molunk
        ertek	:= -1 * Query3.FieldByname('KS_OSSZEG').AsFloat * Query3.FieldByname('KS_ARFOLY').AsFloat + Query3.FieldByname('KS_AFAERT').AsFloat;
        if ( ( tipus <> 0 ) and ( tipus <> 3 ) and ( tipus <> 2 ) ) then begin		// Az �zemanyag k�lts�get + Ad blue -t + h�t� tankol�st nem vonjuk le, mert azt ut�na sz�moljuk
          ossz	:= ossz + ertek;
          ossz2	:= ossz2 + ertek;
          EURossz   := EURossz + GetValueInOtherCur(ertek, Query3.FieldByName('KS_DATUM').AsString, 'HUF', 'EUR' );
        end;
        Query3.Next;
      end;
     end;

    // A g�pkocsi �zemanyag k�lts�g�nek berak�sa
    // Ha nincs ilyen adat, ezt ki kellene hagyni!!!
    ossz  	:= ossz - FogyasztasDlg.GetKoltseg;
    ossz2  	:= ossz2- FogyasztasDlg.GetKoltseg;
    EURossz := EURossz - GetValueInOtherCur(FogyasztasDlg.GetKoltseg, Query1.FieldByName('JA_JKEZD').AsString, 'HUF', 'EUR' );

    // A g�pkocsi Ad-blue k�lts�g�nek berak�sa -> Ha nincs ilyen adat, ezt ki kell hagyni!!!
    Query_Run (Query5, 'SELECT * FROM KOLTSEG WHERE KS_RENDSZ = '''+Query1.FieldByname('JA_RENDSZ').AsString+''' AND KS_TIPUS = 2 ' );
    if Query5.RecordCount > 0 then begin
      ossz := ossz - FogyasztasDlg.GetBlueKoltseg;
      ossz2:= ossz2- FogyasztasDlg.GetBlueKoltseg;
      EURossz   := EURossz - GetValueInOtherCur(FogyasztasDlg.GetBlueKoltseg, Query1.FieldByName('JA_JKEZD').AsString, 'HUF', 'EUR' );
    end;

    // A h�t� tankol�sok berak�sa
    Query_Run (Query2,'SELECT * FROM JARPOTOS WHERE JP_JAKOD = '''+jaratsz+''' ',true);
    while not Query2.EOF do begin
      if HutoPotkocsi(Query2.FieldByname('JP_POREN').AsString) then begin
        szossz	:= GetHutoOsszeg(Query2.FieldByname('JP_POREN').AsString, Query2.FieldByname('JP_UZEM1').AsString, Query2.FieldByname('JP_UZEM2').AsString);
        ossz  								:= ossz - szossz;
        ossz2  								:= ossz2- szossz;
        EURossz   := EURossz - GetValueInOtherCur(szossz, Query1.FieldByName('JA_JKEZD').AsString, 'HUF', 'EUR' );

      end;
      Query2.Next;
    end;
    Query2.Close;

     // K�lts�g �sszesen bet�lt�se
//     if OssznyilDlg.CheckBox2.Checked then
//        ossz:=ossz2;
     loc_HUFokolts	:= loc_HUFokolts + ossz;
     loc_EURokolts	:= loc_EURokolts + EURossz;


     // ofuvar	:= ofuvar + ertek;

     // ??? Az alj�ratok fogyaszt�s�t nem kell hozz�adni a tot�l sorhoz!
     TenyFogyasztas:= FogyasztasDlg.GetTenyFogyasztas;
     if not(FogyasztasDlg.ErvenyesTenyFogyasztas) then begin // �rv�nytelen �rt�k, pl. -1 = nincs minden szakaszhoz z�r� telitank
          // ervenytelen_fogyasztas_grandtotal:= true;
          Result.IsValid:=False;
          end;

      // oertek			:= oertek + ertek + ossz;
      // Az EUR k�lts�g kisz�m�t�sa a j�rat indul� d�tum alapj�n
      //ossz			:= 0;
      Query1.Next;
    end;
    Query1.EnableControls;
  end;
  Result.HUFValue:=-1*loc_HUFokolts;
  Result.EURValue:=-1*loc_EURokolts;
end;

procedure TFogyasztSzamoloDlg.FormCreate(Sender: TObject);
begin
   //  EgyebDlg.SetADOQueryDatabase(Query1);
   // EgyebDlg.SetADOQueryDatabase(Query2);
   // EgyebDlg.SetADOQueryDatabase(Query3);
   // EgyebDlg.SetADOQueryDatabase(Query5);
   // EgyebDlg.SetADOQueryDatabase(Query6);
   // EgyebDlg.SetADOQueryDatabase(QueryAlap);
    Application.CreateForm(TFogyasztasDlg, FogyasztasDlg);
end;

procedure TFogyasztSzamoloDlg.FormDestroy(Sender: TObject);
begin
  FogyasztasDlg.Release;
end;




end.
