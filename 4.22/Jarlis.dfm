object JarlisDlg: TJarlisDlg
  Left = 318
  Top = 183
  Width = 944
  Height = 812
  HorzScrollBar.Range = 1200
  VertScrollBar.Range = 2000
  Caption = 'JarlisDlg'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clBlack
  Font.Height = -13
  Font.Name = 'Courier New'
  Font.Style = []
  OldCreateOrder = True
  Scaled = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 16
  object QRLabel1: TQRLabel
    Left = 86
    Top = 128
    Width = 10
    Height = 10
    Size.Values = (
      26.458333333333330000
      227.541666666666700000
      338.666666666666700000
      26.458333333333330000)
    XLColumn = 0
    Alignment = taCenter
    AlignToBand = False
    AutoSize = False
    Caption = 'Fizet'#233'si m'#243'd'
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -9
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
    Transparent = True
    ExportAs = exptText
    WrapStyle = BreakOnSpaces
    FontSize = 7
  end
  object QRLabel13: TQRLabel
    Left = 42
    Top = 145
    Width = 10
    Height = 10
    Size.Values = (
      26.458333333333330000
      111.125000000000000000
      383.645833333333300000
      26.458333333333330000)
    XLColumn = 0
    Alignment = taCenter
    AlignToBand = False
    AutoSize = False
    Caption = 'Menetlev'#233'l'
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -13
    Font.Name = 'Times New Roman'
    Font.Style = [fsBold]
    ParentFont = False
    Transparent = True
    ExportAs = exptText
    WrapStyle = BreakOnSpaces
    FontSize = 10
  end
  object Rep: TQuickRep
    Left = 0
    Top = 0
    Width = 794
    Height = 1123
    BeforePrint = RepBeforePrint
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Arial'
    Font.Style = []
    Functions.Strings = (
      'PAGENUMBER'
      'COLUMNNUMBER'
      'REPORTTITLE')
    Functions.DATA = (
      '0'
      '0'
      #39#39)
    OnNeedData = RepNeedData
    Options = [FirstPageHeader, LastPageFooter]
    Page.Columns = 1
    Page.Orientation = poPortrait
    Page.PaperSize = A4
    Page.Continuous = False
    Page.Values = (
      100.000000000000000000
      2970.000000000000000000
      100.000000000000000000
      2100.000000000000000000
      100.000000000000000000
      100.000000000000000000
      0.000000000000000000)
    PrinterSettings.Copies = 1
    PrinterSettings.OutputBin = Auto
    PrinterSettings.Duplex = False
    PrinterSettings.FirstPage = 0
    PrinterSettings.LastPage = 0
    PrinterSettings.UseStandardprinter = False
    PrinterSettings.UseCustomBinCode = False
    PrinterSettings.CustomBinCode = 0
    PrinterSettings.ExtendedDuplex = 0
    PrinterSettings.UseCustomPaperCode = False
    PrinterSettings.CustomPaperCode = 0
    PrinterSettings.PrintMetaFile = False
    PrinterSettings.PrintQuality = 0
    PrinterSettings.Collate = 0
    PrinterSettings.ColorOption = 0
    PrintIfEmpty = True
    ReportTitle = 'J'#225'rat k'#246'lts'#233'gek list'#225'ja'
    ShowProgress = False
    SnapToGrid = True
    Units = MM
    Zoom = 100
    PrevFormStyle = fsNormal
    PreviewInitialState = wsNormal
    PreviewWidth = 500
    PreviewHeight = 500
    PrevInitialZoom = qrZoomToFit
    PreviewDefaultSaveType = stQRP
    PreviewLeft = 0
    PreviewTop = 0
    object QRBand3: TQRBand
      Left = 38
      Top = 38
      Width = 718
      Height = 145
      AlignToBottom = False
      TransparentBand = False
      ForceNewColumn = False
      ForceNewPage = False
      Size.Values = (
        383.645833333333300000
        1899.708333333333000000)
      PreCaluculateBandHeight = False
      KeepOnOnePage = False
      BandType = rbPageHeader
      object QRShape5: TQRShape
        Left = 91
        Top = 117
        Width = 450
        Height = 25
        Size.Values = (
          66.145833333333320000
          240.770833333333300000
          309.562500000000000000
          1190.625000000000000000)
        XLColumn = 0
        Brush.Color = clSilver
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape8: TQRShape
        Left = 624
        Top = 117
        Width = 85
        Height = 25
        Size.Values = (
          66.145833333333320000
          1651.000000000000000000
          309.562500000000000000
          224.895833333333300000)
        XLColumn = 0
        Brush.Color = clSilver
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape3: TQRShape
        Left = 11
        Top = 117
        Width = 80
        Height = 25
        Size.Values = (
          66.145833333333320000
          29.104166666666670000
          309.562500000000000000
          211.666666666666700000)
        XLColumn = 0
        Brush.Color = clSilver
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRLSzamla: TQRLabel
        Left = 8
        Top = 29
        Width = 705
        Height = 27
        Size.Values = (
          71.437500000000000000
          21.166666666666670000
          76.729166666666680000
          1865.312500000000000000)
        XLColumn = 0
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = 'J'#225'rat k'#246'lts'#233'gek list'#225'ja'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -23
        Font.Name = 'Arial'
        Font.Style = [fsBold, fsItalic]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 17
      end
      object QRLabel16: TQRLabel
        Left = 147
        Top = 78
        Width = 530
        Height = 16
        Size.Values = (
          42.333333333333330000
          388.937500000000000000
          206.375000000000000000
          1402.291666666667000000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'QRLabel16'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 10
      end
      object QRLabel6: TQRLabel
        Left = 589
        Top = 2
        Width = 80
        Height = 16
        Size.Values = (
          42.333333333333330000
          1558.395833333333000000
          5.291666666666667000
          211.666666666666700000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'D'#225'tum :'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 10
      end
      object QRSysData1: TQRSysData
        Left = 634
        Top = 2
        Width = 80
        Height = 16
        Size.Values = (
          42.333333333333330000
          1677.458333333333000000
          5.291666666666667000
          211.666666666666700000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Color = clWhite
        Data = qrsDate
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Text = ''
        Transparent = True
        ExportAs = exptText
        FontSize = 10
      end
      object QRLabel7: TQRLabel
        Left = 589
        Top = 18
        Width = 80
        Height = 16
        Size.Values = (
          42.333333333333330000
          1558.395833333333000000
          47.625000000000000000
          211.666666666666700000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Lap   :'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 10
      end
      object QRSysData4: TQRSysData
        Left = 632
        Top = 18
        Width = 80
        Height = 16
        Size.Values = (
          42.333333333333330000
          1672.166666666667000000
          47.625000000000000000
          211.666666666666700000)
        XLColumn = 0
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Color = clWhite
        Data = qrsPageNumber
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Text = ''
        Transparent = True
        ExportAs = exptText
        FontSize = 10
      end
      object QRLabel8: TQRLabel
        Left = 18
        Top = 79
        Width = 120
        Height = 16
        Size.Values = (
          42.333333333333330000
          47.625000000000000000
          209.020833333333300000
          317.500000000000000000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'G'#233'pj'#225'rm'#369' + sof'#337'r : '
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 10
      end
      object QRLabel4: TQRLabel
        Left = 18
        Top = 121
        Width = 55
        Height = 16
        Size.Values = (
          42.333333333333330000
          47.625000000000000000
          320.145833333333300000
          145.520833333333300000)
        XLColumn = 0
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = 'Kelt'
        Color = clSilver
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 10
      end
      object QRLabel12: TQRLabel
        Left = 96
        Top = 121
        Width = 393
        Height = 16
        Size.Values = (
          42.333333333333340000
          254.000000000000000000
          320.145833333333400000
          1039.812500000000000000)
        XLColumn = 0
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = 'Le'#237'r'#225's'
        Color = clSilver
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 10
      end
      object QRLabel24: TQRLabel
        Left = 631
        Top = 121
        Width = 74
        Height = 16
        Size.Values = (
          42.333333333333330000
          1669.520833333333000000
          320.145833333333300000
          195.791666666666700000)
        XLColumn = 0
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = #201'rt'#233'k (Ft)'
        Color = clSilver
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 10
      end
      object QRLabel2: TQRLabel
        Left = 18
        Top = 59
        Width = 120
        Height = 16
        Size.Values = (
          42.333333333333330000
          47.625000000000000000
          156.104166666666700000
          317.500000000000000000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'J'#225'rat : '
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 10
      end
      object QRLabel5: TQRLabel
        Left = 147
        Top = 59
        Width = 530
        Height = 16
        Size.Values = (
          42.333333333333330000
          388.937500000000000000
          156.104166666666700000
          1402.291666666667000000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'QRLabel5'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 10
      end
      object QRShape1: TQRShape
        Left = 541
        Top = 117
        Width = 85
        Height = 25
        Size.Values = (
          66.145833333333320000
          1431.395833333333000000
          309.562500000000000000
          224.895833333333300000)
        XLColumn = 0
        Brush.Color = clSilver
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRLabel9: TQRLabel
        Left = 551
        Top = 121
        Width = 66
        Height = 16
        Size.Values = (
          42.333333333333330000
          1457.854166666667000000
          320.145833333333300000
          174.625000000000000000)
        XLColumn = 0
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = #193'tlag (l)'
        Color = clSilver
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 10
      end
      object QRShape2: TQRShape
        Left = 496
        Top = 117
        Width = 46
        Height = 25
        Size.Values = (
          66.145833333333340000
          1312.333333333333000000
          309.562500000000000000
          121.708333333333400000)
        XLColumn = 0
        Brush.Color = clSilver
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRLabel3: TQRLabel
        Left = 503
        Top = 121
        Width = 32
        Height = 16
        Size.Values = (
          42.333333333333330000
          1330.854166666667000000
          320.145833333333300000
          84.666666666666670000)
        XLColumn = 0
        Alignment = taCenter
        AlignToBand = False
        Caption = 'Orsz.'
        Color = clSilver
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 10
      end
    end
    object QRBand1: TQRBand
      Left = 38
      Top = 183
      Width = 718
      Height = 20
      AlignToBottom = False
      BeforePrint = QRBand1BeforePrint
      TransparentBand = False
      ForceNewColumn = False
      ForceNewPage = False
      Size.Values = (
        52.916666666666670000
        1899.708333333333000000)
      PreCaluculateBandHeight = False
      KeepOnOnePage = False
      BandType = rbDetail
      object QSA1: TQRShape
        Left = 91
        Top = 0
        Width = 618
        Height = 19
        Size.Values = (
          50.800000000000000000
          241.300000000000000000
          0.000000000000000000
          1634.066666666667000000)
        XLColumn = 0
        Brush.Color = clWindow
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QS2: TQRLabel
        Left = 15
        Top = 1
        Width = 74
        Height = 16
        Size.Values = (
          42.333333333333330000
          39.687500000000000000
          2.645833333333333000
          195.791666666666700000)
        XLColumn = 0
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = 'QS2'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 10
      end
      object QS3: TQRLabel
        Left = 104
        Top = 2
        Width = 385
        Height = 16
        Size.Values = (
          42.333333333333340000
          275.166666666666700000
          5.291666666666667000
          1018.645833333333000000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'K'#246'lts'#233'gek ...'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 10
      end
      object QS4: TQRLabel
        Left = 632
        Top = 2
        Width = 73
        Height = 16
        Size.Values = (
          42.333333333333340000
          1672.166666666667000000
          4.233333333333333000
          192.616666666666700000)
        XLColumn = 0
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = '9995556,2'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 10
      end
      object QS30: TQRLabel
        Left = 544
        Top = 2
        Width = 81
        Height = 16
        Size.Values = (
          42.333333333333340000
          1439.333333333333000000
          4.233333333333333000
          213.783333333333300000)
        XLColumn = 0
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = '9995556,2'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 10
      end
      object QS31: TQRLabel
        Left = 498
        Top = 1
        Width = 42
        Height = 16
        Size.Values = (
          42.333333333333340000
          1317.625000000000000000
          2.645833333333333000
          111.125000000000000000)
        XLColumn = 0
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = 'QS31'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 10
      end
    end
  end
  object SG1: TStringGrid
    Left = 180
    Top = 304
    Width = 320
    Height = 120
    ColCount = 7
    FixedCols = 0
    FixedRows = 0
    TabOrder = 2
  end
  object Query1: TADOQuery
    Tag = 1
    Parameters = <>
    Left = 319
    Top = 255
  end
  object Query2: TADOQuery
    Tag = 1
    Parameters = <>
    Left = 359
    Top = 255
  end
  object Query3: TADOQuery
    Tag = 1
    Parameters = <>
    Left = 399
    Top = 255
  end
end
