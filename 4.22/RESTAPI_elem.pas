﻿unit RESTAPI_elem;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, IPPeerClient, Vcl.Grids,
  REST.Response.Adapter, REST.Client, Data.Bind.Components,
  Data.Bind.ObjectScope, Data.DB, Vcl.StdCtrls,
  json, Data.DBXJSON, Vcl.ExtCtrls, Vcl.Menus; // IdURI

type
   TRESTAPIDisplayMode = (dmNew, dmModify);

type
  TRESTAPIElemDlg = class(TForm)
    GridPanel2: TGridPanel;
    Button4: TButton;
    SGE: TStringGrid;
    PopupMenu1: TPopupMenu;
    UzenetTop1: TMenuItem;
    lbReadOnlyCells: TListBox;
    procedure FormCreate(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure SendRequest;
    procedure UzenetTop1Click(Sender: TObject);
    procedure SGESelectCell(Sender: TObject; ACol, ARow: Integer;
      var CanSelect: Boolean);
  private
    FRequestResource: string;
    FRequestMethod: string;
    FResourceDisplayName: string;
    FDisplayMode: TRESTAPIDisplayMode;

  public
    procedure SetRequestResource(Res: string);
    procedure SetRequestMethod(AMethod: string);
    procedure SetResourceDisplayName(const DisplayName: string);
    procedure SetDisplayMode(const DisplayMode: TRESTAPIDisplayMode);
  end;

const
  const_CRLF = AnsiString(#13#10); // (chr(13)+chr(10)) as string;
  RESTAPI_true = 'Igen';
  RESTAPI_false = 'Nem';

var
  RESTAPIElemDlg: TRESTAPIElemDlg;

implementation

{$R *.dfm}

uses
   UzenetTop, RESTAPI_Kozos;

procedure TRESTAPIElemDlg.Button4Click(Sender: TObject);
begin
   SendRequest;
end;


procedure TRESTAPIElemDlg.SendRequest;
var
  i: integer;
  BodyKey, BodyValue, S, Res: string;
  JSONResult: TJSONResult;
begin
  RESTAPIKozosDlg.RESTAPIParamsList.Clear;
  for i:=1 to SGE.RowCount-1 do begin  // 0. sor a táblázat fejléc
     if (FDisplayMode = dmNew) or (SGE.Cells[4, i]='false') then begin  // ha "insert", akkor minden mező kell. Ha "update", akkor csak a nem kulcsmezők
        BodyKey:= SGE.Cells[2, i];
        if SGE.Cells[3, i]='boolean' then begin
          if SGE.Cells[1, i] = RESTAPI_true then
             BodyValue:='true'
          else BodyValue:='false';
          end
        else begin
          BodyValue:= SGE.Cells[1, i];
          end;
        RESTAPIKozosDlg.RESTAPIParamsList.Add(BodyKey, BodyValue);
        end;  // if
      end;  // if
  Screen.Cursor:= crHourGlass;
  try
   try
   JSONResult:= RESTAPIKozosDlg.ExecRequest(FRequestResource, FRequestMethod, RESTAPIKozosDlg.RESTAPIParamsList);
   if JSONResult.ErrorString <> '' then begin
        S:='Hiba a mentés során! Kód: '+ JSONResult.ErrorString;
        MessageDlg( S, mtError,[mbOK],0);
        // NoticeKi(S);
        end
    else begin
        ModalResult:= mrOK;  // close form
        end;
   except
    on E: Exception do begin
        // itt majd NoticeKi
        MessageDlg( E.Message, mtError,[mbOK],0);
        end; // on E
      end;  // try-except
  finally
    Screen.Cursor:= crDefault;;
    end;  // try-finally
end;

procedure TRESTAPIElemDlg.FormCreate(Sender: TObject);
begin
   SGE.ColWidths[0]:=142;
   SGE.ColWidths[1]:=320;
   SGE.ColWidths[2]:=0;  // elrejt
   SGE.ColWidths[3]:=0;  // elrejt
   SGE.ColWidths[4]:=0;  // elrejt
end;

procedure TRESTAPIElemDlg.SetRequestResource(Res: string);
begin
    FRequestResource:= Res;
end;

procedure TRESTAPIElemDlg.SetResourceDisplayName(const DisplayName: string);
begin
   FResourceDisplayName:= DisplayName;
   Caption:= FResourceDisplayName;
end;

procedure TRESTAPIElemDlg.SGESelectCell(Sender: TObject; ACol, ARow: Integer;
  var CanSelect: Boolean);
var
  myRect: TGridRect;
begin
 if ACol <> 1 then begin  // nem a szerkeszthető oszlop
     CanSelect:= False;
     myRect.Left := 1;
     myRect.Right := 1;
     myRect.Top := ARow+1;
     myRect.Bottom := ARow+1;
     SGE.Selection := myRect;
     end
  else begin   // szerkeszthető oszlop: ez a sor szerkeszthető?
   if lbReadOnlyCells.Items.IndexOf(IntToStr(ARow)+'|'+IntToStr(ACol)) >=0 then begin // benne van
      CanSelect:= False;
      end;
   end;  // else
end;

procedure TRESTAPIElemDlg.SetDisplayMode(const DisplayMode: TRESTAPIDisplayMode);
begin
   FDisplayMode:= DisplayMode;
end;

procedure TRESTAPIElemDlg.SetRequestMethod(AMethod: string);
begin
   FRequestMethod:= AMethod;
end;


procedure TRESTAPIElemDlg.UzenetTop1Click(Sender: TObject);
begin
   UzenetTopDlg.Show;
end;

end.
