object JNavOnline: TJNavOnline
  Left = 0
  Top = 0
  BorderIcons = []
  BorderStyle = bsSingle
  Caption = 'NAV Online sz'#225'mla kezel'#233's'
  ClientHeight = 599
  ClientWidth = 1206
  Color = clBtnFace
  Font.Charset = EASTEUROPE_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Arial'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 16
  object Panel1: TPanel
    Left = 0
    Top = 64
    Width = 1206
    Height = 65
    Align = alTop
    Font.Charset = EASTEUROPE_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Arial'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    object Label1: TLabel
      Left = 24
      Top = 10
      Width = 84
      Height = 16
      Caption = 'Sz'#225'mlasz'#225'm :'
    end
    object Label3: TLabel
      Left = 24
      Top = 39
      Width = 74
      Height = 16
      Caption = 'Transact ID :'
    end
    object Label4: TLabel
      Left = 384
      Top = 10
      Width = 53
      Height = 16
      Caption = 'St'#225'tusz :'
    end
    object MSzam: TMaskEdit
      Left = 134
      Top = 6
      Width = 131
      Height = 24
      Color = clLime
      ReadOnly = True
      TabOrder = 0
      Text = ''
    end
    object Panel4: TPanel
      Left = 720
      Top = 1
      Width = 485
      Height = 63
      Align = alRight
      Caption = 'Panel4'
      ShowCaption = False
      TabOrder = 1
      object Label2: TLabel
        Left = 18
        Top = 5
        Width = 91
        Height = 16
        Caption = 'Eddigi XML-ek :'
      end
      object CBXml: TComboBox
        Left = 19
        Top = 27
        Width = 454
        Height = 24
        Style = csDropDownList
        TabOrder = 0
        OnChange = CBXmlChange
      end
    end
    object MTrid: TMaskEdit
      Left = 134
      Top = 35
      Width = 203
      Height = 24
      Color = clLime
      ReadOnly = True
      TabOrder = 2
      Text = ''
    end
    object MStat: TMaskEdit
      Left = 448
      Top = 6
      Width = 209
      Height = 24
      Color = clLime
      ReadOnly = True
      TabOrder = 3
      Text = ''
    end
  end
  object NOMemo: TMemo
    Left = 0
    Top = 129
    Width = 1206
    Height = 412
    Align = alClient
    ScrollBars = ssBoth
    TabOrder = 4
  end
  object MemoKap: TMemo
    Left = 232
    Top = 161
    Width = 241
    Height = 112
    TabOrder = 0
    Visible = False
  end
  object MemoAll: TMemo
    Left = 528
    Top = 161
    Width = 225
    Height = 112
    TabOrder = 1
    Visible = False
  end
  object Panel2: TPanel
    Left = 0
    Top = 541
    Width = 1206
    Height = 58
    Align = alBottom
    Caption = 'Panel2'
    ShowCaption = False
    TabOrder = 3
    object BitOK: TBitBtn
      Left = 606
      Top = 16
      Width = 130
      Height = 25
      Caption = 'OK'
      TabOrder = 0
      OnClick = BitOKClick
    end
    object BitLezar: TBitBtn
      Left = 870
      Top = 16
      Width = 130
      Height = 25
      Caption = 'Lez'#225'r'#225's k'#233'zzel'
      TabOrder = 1
      OnClick = BitLezarClick
    end
    object BitStat: TBitBtn
      Left = 78
      Top = 14
      Width = 130
      Height = 25
      Caption = 'St'#225'tusz lek'#233'rdez'#233's'
      TabOrder = 2
      OnClick = BitStatClick
    end
    object BitInv: TBitBtn
      Left = 335
      Top = 14
      Width = 130
      Height = 25
      Caption = 'Sz'#225'mla lek'#233'rdez'#233's'
      TabOrder = 3
      OnClick = BitInvClick
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 0
    Width = 1206
    Height = 64
    Align = alTop
    Caption = 'NAV TESZT  TER'#220'LET'
    Color = clRed
    Font.Charset = EASTEUROPE_CHARSET
    Font.Color = clYellow
    Font.Height = -32
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentBackground = False
    ParentFont = False
    TabOrder = 5
  end
  object FLB: TFileListBox
    Left = 803
    Top = 200
    Width = 233
    Height = 169
    Mask = '*.XML'
    TabOrder = 6
    Visible = False
  end
  object IdHTTP1: TIdHTTP
    AllowCookies = True
    ProxyParams.BasicAuthentication = False
    ProxyParams.ProxyPort = 0
    Request.ContentLength = -1
    Request.ContentRangeEnd = -1
    Request.ContentRangeStart = -1
    Request.ContentRangeInstanceLength = -1
    Request.Accept = 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8'
    Request.BasicAuthentication = False
    Request.UserAgent = 'Mozilla/3.0 (compatible; Indy Library)'
    Request.Ranges.Units = 'bytes'
    Request.Ranges = <>
    HTTPOptions = [hoForceEncodeParams]
    Left = 888
    Top = 8
  end
  object Timer1: TTimer
    Interval = 500
    OnTimer = Timer1Timer
    Left = 160
    Top = 160
  end
end
