unit Nevnap;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ADODB, jpeg, ExtCtrls, StdCtrls;

type
  TFNevnap = class(TForm)
    TDolgozo: TADOQuery;
    TDolgozoje_fenev: TStringField;
    Panel1: TPanel;
    Panel2: TPanel;
    Image1: TImage;
    Label1: TLabel;
    Label2: TLabel;
    Image2: TImage;
    procedure Image1Click(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
  public
    { Public declarations }
    function Nevnap_dat(datum:Tdate):string;
    function Nevnap(ukod:string):Boolean;
  end;

  type
	TNameDayRecType=record
  	day: integer;
    name1, name2: string
  end;

const
	NameDayArray: array[0..364] of TNameDayRecType=(
    (day: 1; name1:  'Fruzsina'; name2:  ''),
    (day: 2; name1:  '�bel'; name2:  ''),
    (day: 3; name1:  'Genov�va'; name2:  'Benj�min'),
    (day: 4; name1:  'Titusz'; name2:  'Leona'),
    (day: 5; name1:  'Simon'; name2:  ''),
    (day: 6; name1:  'Boldizs�r'; name2:  ''),
    (day: 7; name1:  'Attila'; name2:  'Ram�na'),
    (day: 8; name1:  'Gy�ngyv�r'; name2:  ''),
    (day: 9; name1:  'Marcell'; name2:  ''),
    (day: 10; name1:  'Mel�nia'; name2:  ''),
    (day: 11; name1:  '�gota'; name2:  ''),
    (day: 12; name1:  'Ern�'; name2:  ''),
    (day: 13; name1:  'Veronika'; name2:  ''),
    (day: 14; name1:  'B�dog'; name2:  ''),
    (day: 15; name1:  'L�r�nt'; name2:  'L�r�nd'),
    (day: 16; name1:  'Guszt�v'; name2:  ''),
    (day: 17; name1:  'Antal'; name2:  'Ant�nia'),
    (day: 18; name1:  'Piroska'; name2:  ''),
    (day: 19; name1:  'S�ra'; name2:  'M�ri�'),
    (day: 20; name1:  'F�bi�n'; name2:  'Sebesty�n'),
    (day: 21; name1:  '�gnes'; name2:  ''),
    (day: 22; name1:  'Vince'; name2:  'Art�r'),
    (day: 23; name1:  'Zelma'; name2:  'Rajmund'),
    (day: 24; name1:  'Tim�t'; name2:  ''),
    (day: 25; name1:  'P�l'; name2:  ''),
    (day: 26; name1:  'Vanda'; name2:  'Paula'),
    (day: 27; name1:  'Angelika'; name2:  ''),
    (day: 28; name1:  'K�roly'; name2:  'Karola'),
    (day: 29; name1:  'Ad�l'; name2:  ''),
    (day: 30; name1:  'Martina'; name2:  'Gerda'),
    (day: 31; name1:  'Marcella'; name2:  ''),
    (day: 32; name1:  'Ign�c'; name2:  ''),
    (day: 33; name1:  'Karolina'; name2:  'Aida'),
    (day: 34; name1:  'Bal�zs'; name2:  ''),
    (day: 35; name1:  'R�hel'; name2:  'Csenge'),
    (day: 36; name1:  '�gota'; name2:  'Ingrid'),
    (day: 37; name1:  'Dorottya'; name2:  'D�ra'),
    (day: 38; name1:  'T�dor'; name2:  'R�me�'),
    (day: 39; name1:  'Aranka'; name2:  ''),
    (day: 40; name1:  'Abig�l'; name2:  'Alex'),
    (day: 41; name1:  'Elvira'; name2:  ''),
    (day: 42; name1:  'Bertold'; name2:  'Marietta'),
    (day: 43; name1:  'L�dia'; name2:  'L�via'),
    (day: 44; name1:  'Ella'; name2:  'Linda'),
    (day: 45; name1:  'B�lint'; name2:  'Valentin'),
    (day: 46; name1:  'Kolos'; name2:  'Georgina'),
    (day: 47; name1:  'Julianna'; name2:  'Lilla'),
    (day: 48; name1:  'Don�t'; name2:  ''),
    (day: 49; name1:  'Bernadett'; name2:  ''),
    (day: 50; name1:  'Zsuzsanna'; name2:  ''),
    (day: 51; name1:  'Alad�r'; name2:  '�lmos'),
    (day: 52; name1:  'Eleon�ra'; name2:  ''),
    (day: 53; name1:  'Gerzson'; name2:  ''),
    (day: 54; name1:  'Alfr�d'; name2:  ''),
    (day: 55; name1:  'M�ty�s'; name2:  ''),
    (day: 56; name1:  'G�za'; name2:  ''),
    (day: 57; name1:  'Edina'; name2:  ''),
    (day: 58; name1:  '�kos'; name2:  'B�tor'),
    (day: 59; name1:  'Elem�r'; name2:  ''),
    (day: 60; name1:  'Albin'; name2:  ''),
    (day: 61; name1:  'Lujza'; name2:  ''),
    (day: 62; name1:  'Korn�lia'; name2:  ''),
    (day: 63; name1:  'K�zm�r'; name2:  ''),
    (day: 64; name1:  'Adorj�n'; name2:  'Adri�n'),
    (day: 65; name1:  'Leon�ra'; name2:  'Inez'),
    (day: 66; name1:  'Tam�s'; name2:  ''),
    (day: 67; name1:  'Zolt�n'; name2:  ''),
    (day: 68; name1:  'Franciska'; name2:  'Fanni'),
    (day: 69; name1:  'Ildik�'; name2:  ''),
    (day: 70; name1:  'Szil�rd'; name2:  ''),
    (day: 71; name1:  'Gergely'; name2:  ''),
    (day: 72; name1:  'Kriszti�n'; name2:  'Ajtony'),
    (day: 73; name1:  'Matild'; name2:  ''),
    (day: 74; name1:  'Krist�f'; name2:  ''),
    (day: 75; name1:  'Henrietta'; name2:  ''),
    (day: 76; name1:  'Gertr�d'; name2:  'Patrik'),
    (day: 77; name1:  'S�ndor'; name2:  'Ede'),
    (day: 78; name1:  'J�zsef'; name2:  'B�nk'),
    (day: 79; name1:  'Klaudia'; name2:  ''),
    (day: 80; name1:  'Benedek'; name2:  ''),
    (day: 81; name1:  'Be�ta'; name2:  'Izolda'),
    (day: 82; name1:  'Em�ke'; name2:  ''),
    (day: 83; name1:  'G�bor'; name2:  'Karina'),
    (day: 84; name1:  'Ir�n'; name2:  '�risz'),
    (day: 85; name1:  'Em�nuel'; name2:  ''),
    (day: 86; name1:  'Hajnalka'; name2:  ''),
    (day: 87; name1:  'Gedeon'; name2:  'Johanna'),
    (day: 88; name1:  'Auguszta'; name2:  ''),
    (day: 89; name1:  'Zal�n'; name2:  ''),
    (day: 90; name1:  '�rp�d'; name2:  ''),
    (day: 91; name1:  'Hug�'; name2:  ''),
    (day: 92; name1:  '�ron'; name2:  ''),
    (day: 93; name1:  'Buda'; name2:  'Rich�rd'),
    (day: 94; name1:  'Izidor'; name2:  ''),
    (day: 95; name1:  'Vince'; name2:  ''),
    (day: 96; name1:  'Vilmos'; name2:  'B�borka'),
    (day: 97; name1:  'Herman'; name2:  ''),
    (day: 98; name1:  'D�nes'; name2:  ''),
    (day: 99; name1:  'Erhard'; name2:  ''),
    (day: 100; name1:  'Zsolt'; name2:  ''),
    (day: 101; name1:  'Le�'; name2:  'Szaniszl�'),
    (day: 102; name1:  'Gyula'; name2:  ''),
    (day: 103; name1:  'Ida'; name2:  ''),
    (day: 104; name1:  'Tibor'; name2:  ''),
    (day: 105; name1:  'Anaszt�zia'; name2:  'Tas'),
    (day: 106; name1:  'Csongor'; name2:  ''),
    (day: 107; name1:  'Rudolf'; name2:  ''),
    (day: 108; name1:  'Andrea'; name2:  'Ilma'),
    (day: 109; name1:  'Emma'; name2:  ''),
    (day: 110; name1:  'Tivadar'; name2:  ''),
    (day: 111; name1:  'Konr�d'; name2:  ''),
    (day: 112; name1:  'Csilla'; name2:  'No�mi'),
    (day: 113; name1:  'B�la'; name2:  ''),
    (day: 114; name1:  'Gy�rgy'; name2:  ''),
    (day: 115; name1:  'M�rk'; name2:  ''),
    (day: 116; name1:  'Ervin'; name2:  ''),
    (day: 117; name1:  'Zita'; name2:  'Mariann'),
    (day: 118; name1:  'Val�ria'; name2:  ''),
    (day: 119; name1:  'P�ter'; name2:  ''),
    (day: 120; name1:  'Katalin'; name2:  'Kitti'),
    (day: 121; name1:  'F�l�p'; name2:  'Jakab'),
    (day: 122; name1:  'Zsigmond'; name2:  ''),
    (day: 123; name1:  'T�mea'; name2:  'Irma'),
    (day: 124; name1:  'M�nika'; name2:  'Fl�ri�n'),
    (day: 125; name1:  'Gy�rgyi'; name2:  ''),
    (day: 126; name1:  'Ivett'; name2:  'Frida'),
    (day: 127; name1:  'Gizella'; name2:  ''),
    (day: 128; name1:  'Mih�ly'; name2:  ''),
    (day: 129; name1:  'Gergely'; name2:  ''),
    (day: 130; name1:  '�rmin'; name2:  'P�lma'),
    (day: 131; name1:  'Ferenc'; name2:  ''),
    (day: 132; name1:  'Pongr�c'; name2:  ''),
    (day: 133; name1:  'Szerv�c'; name2:  'Imola'),
    (day: 134; name1:  'Bonif�c'; name2:  ''),
    (day: 135; name1:  'Zs�fia'; name2:  'Szonja'),
    (day: 136; name1:  'M�zes'; name2:  'Botond'),
    (day: 137; name1:  'Paszk�l'; name2:  ''),
    (day: 138; name1:  'Erik'; name2:  'Alexandra'),
    (day: 139; name1:  'Iv�'; name2:  'Mil�n'),
    (day: 140; name1:  'Bern�t'; name2:  'Fel�cia'),
    (day: 141; name1:  'Konstantin'; name2:  ''),
    (day: 142; name1:  'J�lia'; name2:  'Rita'),
    (day: 143; name1:  'Dezs�'; name2:  ''),
    (day: 144; name1:  'Eliza'; name2:  'Eszter'),
    (day: 145; name1:  'Orb�n'; name2:  ''),
    (day: 146; name1:  'F�l�p'; name2:  'Evelin'),
    (day: 147; name1:  'Hella'; name2:  ''),
    (day: 148; name1:  'Emil'; name2:  'Csan�d'),
    (day: 149; name1:  'Magdolna'; name2:  ''),
    (day: 150; name1:  'Janka'; name2:  'Zsanett'),
    (day: 151; name1:  'Ang�la'; name2:  'Petronella'),
    (day: 152; name1:  'T�nde'; name2:  ''),
    (day: 153; name1:  'K�rmen'; name2:  'Anita'),
    (day: 154; name1:  'Klotild'; name2:  ''),
    (day: 155; name1:  'Bulcs�'; name2:  ''),
    (day: 156; name1:  'Fatime'; name2:  ''),
    (day: 157; name1:  'Norbert'; name2:  'Cintia'),
    (day: 158; name1:  'R�bert'; name2:  ''),
    (day: 159; name1:  'Med�rd'; name2:  ''),
    (day: 160; name1:  'F�lix'; name2:  ''),
    (day: 161; name1:  'Margit'; name2:  'Gr�ta'),
    (day: 162; name1:  'Barnab�s'; name2:  ''),
    (day: 163; name1:  'Vill�'; name2:  ''),
    (day: 164; name1:  'Antal'; name2:  'Anett'),
    (day: 165; name1:  'Vazul'; name2:  ''),
    (day: 166; name1:  'Jol�n'; name2:  'Vid'),
    (day: 167; name1:  'Jusztin'; name2:  ''),
    (day: 168; name1:  'Laura'; name2:  'Alida'),
    (day: 169; name1:  'Arnold'; name2:  'Levente'),
    (day: 170; name1:  'Gy�rf�s'; name2:  ''),
    (day: 171; name1:  'Rafael'; name2:  ''),
    (day: 172; name1:  'Alajos'; name2:  'Leila'),
    (day: 173; name1:  'Paulina'; name2:  ''),
    (day: 174; name1:  'Zolt�n'; name2:  ''),
    (day: 175; name1:  'Iv�n'; name2:  ''),
    (day: 176; name1:  'Vilmos'; name2:  ''),
    (day: 177; name1:  'J�nos'; name2:  'P�l'),
    (day: 178; name1:  'L�szl�'; name2:  ''),
    (day: 179; name1:  'Levente'; name2:  'Ir�n'),
    (day: 180; name1:  'P�ter'; name2:  'P�l'),
    (day: 181; name1:  'P�l'; name2:  ''),
    (day: 182; name1:  'Tiham�r'; name2:  'Annam�ria'),
    (day: 183; name1:  'Ott�'; name2:  ''),
    (day: 184; name1:  'Korn�l'; name2:  'Soma'),
    (day: 185; name1:  'Ulrik'; name2:  ''),
    (day: 186; name1:  'Emese'; name2:  'Sarolta'),
    (day: 187; name1:  'Csaba'; name2:  ''),
    (day: 188; name1:  'Apoll�nia'; name2:  ''),
    (day: 189; name1:  'Ell�k'; name2:  ''),
    (day: 190; name1:  'Lukr�cia'; name2:  ''),
    (day: 191; name1:  'Am�lia'; name2:  ''),
    (day: 192; name1:  'N�ra'; name2:  'Lili'),
    (day: 193; name1:  'Izabella'; name2:  'Dalma'),
    (day: 194; name1:  'Jen�'; name2:  ''),
    (day: 195; name1:  '�rs'; name2:  'Stella'),
    (day: 196; name1:  'Roland'; name2:  'Henrik'),
    (day: 197; name1:  'Valter'; name2:  ''),
    (day: 198; name1:  'Endre'; name2:  'Elek'),
    (day: 199; name1:  'Frigyes'; name2:  ''),
    (day: 200; name1:  'Em�lia'; name2:  ''),
    (day: 201; name1:  'Ill�s'; name2:  ''),
    (day: 202; name1:  'D�niel'; name2:  'Daniella'),
    (day: 203; name1:  'Magdolna'; name2:  ''),
    (day: 204; name1:  'Lenke'; name2:  ''),
    (day: 205; name1:  'Kinga'; name2:  'Kincs�'),
    (day: 206; name1:  'Krist�f'; name2:  'Jakab'),
    (day: 207; name1:  'Anna'; name2:  'Anik�'),
    (day: 208; name1:  'Olga'; name2:  'Lili�na'),
    (day: 209; name1:  'Szabolcs'; name2:  ''),
    (day: 210; name1:  'M�rta'; name2:  'Fl�ra'),
    (day: 211; name1:  'Judit'; name2:  'X�nia'),
    (day: 212; name1:  'Oszk�r'; name2:  ''),
    (day: 213; name1:  'Bogl�rka'; name2:  ''),
    (day: 214; name1:  'Lehel'; name2:  ''),
    (day: 215; name1:  'Hermina'; name2:  ''),
    (day: 216; name1:  'Domonkos'; name2:  'Dominika'),
    (day: 217; name1:  'Krisztina'; name2:  ''),
    (day: 218; name1:  'Berta'; name2:  'Bettina'),
    (day: 219; name1:  'Ibolya'; name2:  ''),
    (day: 220; name1:  'L�szl�'; name2:  ''),
    (day: 221; name1:  'Em�d'; name2:  ''),
    (day: 222; name1:  'L�rinc'; name2:  ''),
    (day: 223; name1:  'Zsuzsanna'; name2:  'Tiborc'),
    (day: 224; name1:  'Kl�ra'; name2:  ''),
    (day: 225; name1:  'Ipoly'; name2:  ''),
    (day: 226; name1:  'Marcell'; name2:  ''),
    (day: 227; name1:  'M�ria'; name2:  ''),
    (day: 228; name1:  '�brah�m'; name2:  ''),
    (day: 229; name1:  'J�cint'; name2:  ''),
    (day: 230; name1:  'Ilona'; name2:  ''),
    (day: 231; name1:  'Huba'; name2:  ''),
    (day: 232; name1:  'Vajk'; name2:  ''),
    (day: 233; name1:  'S�muel'; name2:  'Hajna'),
    (day: 234; name1:  'Menyh�rt'; name2:  'Mirjam'),
    (day: 235; name1:  'Bence'; name2:  ''),
    (day: 236; name1:  'Bertalan'; name2:  ''),
    (day: 237; name1:  'Lajos'; name2:  'Patr�cia'),
    (day: 238; name1:  'Izs�'; name2:  ''),
    (day: 239; name1:  'G�sp�r'; name2:  ''),
    (day: 240; name1:  '�goston'; name2:  ''),
    (day: 241; name1:  'Beatrix'; name2:  'Erna'),
    (day: 242; name1:  'R�zsa'; name2:  ''),
    (day: 243; name1:  'Erika'; name2:  'Bella'),
    (day: 244; name1:  'Egyed'; name2:  'Egon'),
    (day: 245; name1:  'Rebeka'; name2:  'Dorina'),
    (day: 246; name1:  'Hilda'; name2:  ''),
    (day: 247; name1:  'Roz�lia'; name2:  ''),
    (day: 248; name1:  'Viktor'; name2:  'L�rinc'),
    (day: 249; name1:  'Zakari�s'; name2:  ''),
    (day: 250; name1:  'Regina'; name2:  ''),
    (day: 251; name1:  'M�ria'; name2:  'Adrienn'),
    (day: 252; name1:  '�d�m'; name2:  ''),
    (day: 253; name1:  'Nikolett'; name2:  'Hunor'),
    (day: 254; name1:  'Teod�ra'; name2:  ''),
    (day: 255; name1:  'M�ria'; name2:  ''),
    (day: 256; name1:  'Korn�l'; name2:  ''),
    (day: 257; name1:  'Szer�na'; name2:  'Rox�na'),
    (day: 258; name1:  'Enik�'; name2:  'Melitta'),
    (day: 259; name1:  'Edit'; name2:  ''),
    (day: 260; name1:  'Zs�fia'; name2:  ''),
    (day: 261; name1:  'Di�na'; name2:  ''),
    (day: 262; name1:  'Vilhelmina'; name2:  ''),
    (day: 263; name1:  'Friderika'; name2:  ''),
    (day: 264; name1:  'M�t�,Mira'; name2:  'Mirella'),
    (day: 265; name1:  'M�ric'; name2:  ''),
    (day: 266; name1:  'Tekla'; name2:  ''),
    (day: 267; name1:  'Gell�rt'; name2:  'Merc�desz'),
    (day: 268; name1:  'Eufrozina'; name2:  'Kende'),
    (day: 269; name1:  'Jusztina'; name2:  ''),
    (day: 270; name1:  'Adalbert'; name2:  ''),
    (day: 271; name1:  'Vencel'; name2:  ''),
    (day: 272; name1:  'Mih�ly'; name2:  ''),
    (day: 273; name1:  'Jeromos'; name2:  ''),
    (day: 274; name1:  'Malvin'; name2:  ''),
    (day: 275; name1:  'Petra'; name2:  ''),
    (day: 276; name1:  'Helga'; name2:  ''),
    (day: 277; name1:  'Ferenc'; name2:  ''),
    (day: 278; name1:  'Aur�l'; name2:  ''),
    (day: 279; name1:  'Br�n�'; name2:  'Ren�ta'),
    (day: 280; name1:  'Am�lia'; name2:  ''),
    (day: 281; name1:  'Kopp�ny'; name2:  ''),
    (day: 282; name1:  'D�nes'; name2:  ''),
    (day: 283; name1:  'Gedeon'; name2:  ''),
    (day: 284; name1:  'Brigitta'; name2:  'Gitta'),
    (day: 285; name1:  'Miksa'; name2:  ''),
    (day: 286; name1:  'K�lm�n'; name2:  'Ede'),
    (day: 287; name1:  'Hel�n'; name2:  ''),
    (day: 288; name1:  'Ter�z'; name2:  ''),
    (day: 289; name1:  'G�l'; name2:  ''),
    (day: 290; name1:  'Hedvig'; name2:  ''),
    (day: 291; name1:  'Luk�cs'; name2:  ''),
    (day: 292; name1:  'N�ndor'; name2:  ''),
    (day: 293; name1:  'Vendel'; name2:  ''),
    (day: 294; name1:  'Orsolya'; name2:  ''),
    (day: 295; name1:  'El�d'; name2:  ''),
    (day: 296; name1:  'Gy�ngyi'; name2:  ''),
    (day: 297; name1:  'Salamon'; name2:  ''),
    (day: 298; name1:  'Blanka'; name2:  'Bianka'),
    (day: 299; name1:  'D�m�t�r'; name2:  ''),
    (day: 300; name1:  'Szabina'; name2:  ''),
    (day: 301; name1:  'Simon'; name2:  'Szimonetta'),
    (day: 302; name1:  'N�rcisz'; name2:  ''),
    (day: 303; name1:  'Alfonz'; name2:  ''),
    (day: 304; name1:  'Farkas'; name2:  ''),
    (day: 305; name1:  'Marianna'; name2:  ''),
    (day: 306; name1:  'Achilles'; name2:  ''),
    (day: 307; name1:  'Gy�z�'; name2:  ''),
    (day: 308; name1:  'K�roly'; name2:  ''),
    (day: 309; name1:  'Imre'; name2:  ''),
    (day: 310; name1:  'L�n�rd'; name2:  ''),
    (day: 311; name1:  'Rezs�'; name2:  ''),
    (day: 312; name1:  'Zsombor'; name2:  ''),
    (day: 313; name1:  'Tivadar'; name2:  ''),
    (day: 314; name1:  'R�ka'; name2:  ''),
    (day: 315; name1:  'M�rton'; name2:  ''),
    (day: 316; name1:  'J�n�s'; name2:  'Ren�t�'),
    (day: 317; name1:  'Szilvia'; name2:  ''),
    (day: 318; name1:  'Aliz'; name2:  ''),
    (day: 319; name1:  'Albert'; name2:  'Lip�t'),
    (day: 320; name1:  '�d�n'; name2:  ''),
    (day: 321; name1:  'Hortenzia'; name2:  'Gerg�'),
    (day: 322; name1:  'Jen�'; name2:  ''),
    (day: 323; name1:  'Erzs�bet'; name2:  'Zs�ka'),
    (day: 324; name1:  'Jol�n'; name2:  ''),
    (day: 325; name1:  'Oliv�r'; name2:  ''),
    (day: 326; name1:  'Cec�lia'; name2:  ''),
    (day: 327; name1:  'Kelemen'; name2:  'Klementina'),
    (day: 328; name1:  'Emma'; name2:  ''),
    (day: 329; name1:  'Katalin'; name2:  ''),
    (day: 330; name1:  'Vir�g'; name2:  ''),
    (day: 331; name1:  'Virgil'; name2:  ''),
    (day: 332; name1:  'Stef�nia'; name2:  ''),
    (day: 333; name1:  'Taksony'; name2:  ''),
    (day: 334; name1:  'Andr�s'; name2:  'Andor'),
    (day: 335; name1:  'Elza'; name2:  ''),
    (day: 336; name1:  'Melinda'; name2:  'Bertalan'),
    (day: 337; name1:  'Ferenc'; name2:  'Ol�via'),
    (day: 338; name1:  'Borb�la'; name2:  'Barbara'),
    (day: 339; name1:  'Vilma'; name2:  ''),
    (day: 340; name1:  'Mikl�s'; name2:  ''),
    (day: 341; name1:  'Ambrus'; name2:  ''),
    (day: 342; name1:  'M�ria'; name2:  ''),
    (day: 343; name1:  'Nat�lia'; name2:  ''),
    (day: 344; name1:  'Judit'; name2:  ''),
    (day: 345; name1:  '�rp�d'; name2:  ''),
    (day: 346; name1:  'Gabriella'; name2:  ''),
    (day: 347; name1:  'Luca'; name2:  'Ot�lia'),
    (day: 348; name1:  'Szil�rda'; name2:  ''),
    (day: 349; name1:  'Val�r'; name2:  ''),
    (day: 350; name1:  'Etelka'; name2:  'Aletta'),
    (day: 351; name1:  'L�z�r'; name2:  'Olimpia'),
    (day: 352; name1:  'Auguszta'; name2:  ''),
    (day: 353; name1:  'Viola'; name2:  ''),
    (day: 354; name1:  'Teofil'; name2:  ''),
    (day: 355; name1:  'Tam�s'; name2:  ''),
    (day: 356; name1:  'Z�n�'; name2:  ''),
    (day: 357; name1:  'Vikt�ria'; name2:  ''),
    (day: 358; name1:  '�d�m'; name2:  '�va'),
    (day: 359; name1:  'Eug�nia'; name2:  ''),
    (day: 360; name1:  'Istv�n'; name2:  ''),
    (day: 361; name1:  'J�nos'; name2:  ''),
    (day: 362; name1:  'Kamilia'; name2:  ''),
    (day: 363; name1:  'Tam�s'; name2:  'Tamara'),
    (day: 364; name1:  'D�vid'; name2:  ''),
    (day: 365; name1:  'Szilveszter'; name2:  '')
  );


var
  FNevnap: TFNevnap;

implementation

uses DateUtils, Egyeb;

{$R *.dfm}

{ TFNevnap }

function TFNevnap.Nevnap(ukod: string): Boolean;
var
  nev:string;
  i:integer;
  ism:boolean;
begin
  TDolgozo.Close;
  TDolgozo.Parameters[0].Value:=ukod;
  TDolgozo.Open;
  TDolgozo.First;
  Result:=False;
  if TDolgozo.Eof then exit;

  nev:=Trim(TDolgozoje_fenev.Value);
  ism:=True;
  while ism do
  begin
    i:=pos(' ',nev);
    if i>0 then
      nev:=Trim(copy(nev,i+1,100))
    else
      ism:=False;
  end;
  Label2.Caption:=nev;
  if pos(nev,Nevnap_dat(date))>0   then
  Result:=True;
end;

function TFNevnap.Nevnap_dat(datum: Tdate): string;
var
  napNo:integer;
  year, month, day: word;
  nevnap,nevnap1,nevnap2:string;
begin
  DecodeDate(datum, year, month, day);
  napNo:=Trunc( datum-EncodeDate(Year, 1, 1));
  nevnap:='';
  if IsLeapYear(year) then     // sz�k��v
  begin
    if (month=2)and(day=29) then
      nevnap:='Sz�k�nap';
    if ((month=2) and (day>=24)) or (month>2) then
      napNo:=napNo-1;
  end;
  if not ((month=2)and(day=29)) then
  begin
 // ShowMessage(IntToStr(napNo));
    nevnap1:=NameDayArray[napNo].name1;
    nevnap2:=NameDayArray[napNo].name2;
    nevnap:=nevnap1;
    if nevnap2<>'' then
      nevnap:=nevnap+', '+nevnap2;
  end;
  Result:=nevnap;
end;

procedure TFNevnap.Image1Click(Sender: TObject);
begin
  close;
end;

procedure TFNevnap.FormKeyPress(Sender: TObject; var Key: Char);
begin
 close;
end;

end.
