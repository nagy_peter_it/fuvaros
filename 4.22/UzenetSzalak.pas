unit UzenetSzalak;

interface

uses WinTypes, WinProcs, Classes, Graphics, Forms, Controls, Buttons,
	StdCtrls, ExtCtrls, Sysutils, Vcl.Mask, Vcl.Grids, Vcl.ValEdit, Generics.Collections,
  Vcl.Menus, ChatControl, Data.DBXJSON, ClipBrd, KozosTipusok;

type
	TUzenetSzalakDlg = class(TForm)
	Panel1: TPanel;
    Label8: TLabel;
    GridPanel2: TGridPanel;
    BitBtn1: TBitBtn;
    pnlMessageInfo: TPanel;
    BitBtn2: TBitBtn;
    cbSMSSzalak: TComboBox;
    Button1: TButton;
    Timer1: TTimer;
    procedure FormCreate(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure CimzettNevekFeltolt;
    procedure SzalakFrissit;
    procedure SzalMutat(Telefonszam: string);
    procedure SzalFrissit;
    procedure Button1Click(Sender: TObject);
    procedure cbSMSSzalakChange(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
	private
    // APIKEY, APIPassword, SendingPhone, AuthToken: string;
    SzalDict: TDictionary<string,string>;
    CimzettNevek: TDictionary<string,string>;
	public

	end;

var
	UzenetSzalakDlg: TUzenetSzalakDlg;
  ChatControl1: TChatControl;
  KivalasztottKontakt, KivalasztottNev: string;
implementation
uses
	Egyeb, J_VALASZTO, Math, SMSKuld, J_SQL, Kozos, UzenetSzerkeszto;

{$R *.DFM}

procedure TUzenetSzalakDlg.BitBtn1Click(Sender: TObject);
var
   DisplayString, Telefonszam: string;
begin
   Screen.Cursor	:= crHourGlass;
   DisplayString:= cbSMSSzalak.Items[cbSMSSzalak.ItemIndex];
   Telefonszam:= SzalDict[DisplayString];
   // Application.CreateForm(TUzenetSzerkesztoDlg, UzenetSzerkesztoDlg);
   UzenetSzerkesztoDlg.Reset;
   UzenetSzerkesztoDlg.AddCimzett(Telefonszam, CimzettNevek[Telefonszam], cimzett);
   Screen.Cursor	:= crDefault;
   UzenetSzerkesztoDlg.EagleSMSTipus:= technikai;
   UzenetSzerkesztoDlg.ShowModal;
   // UzenetSzerkesztoDlg.Free;
   SzalFrissit;
end;

procedure TUzenetSzalakDlg.BitBtn2Click(Sender: TObject);
begin
 Close;
end;

procedure TUzenetSzalakDlg.Button1Click(Sender: TObject);
begin
  SzalFrissit;
end;

procedure TUzenetSzalakDlg.cbSMSSzalakChange(Sender: TObject);
begin
   SzalFrissit;

end;

procedure TUzenetSzalakDlg.SzalFrissit;
var
  DisplayString, Telefonszam, JSON: string;
begin
  if (cbSMSSzalak.Items.Count>0) and (cbSMSSzalak.ItemIndex <> -1) then begin
    DisplayString:= cbSMSSzalak.Items[cbSMSSzalak.ItemIndex];
    Telefonszam:=SzalDict[DisplayString];
    Telefonszam:=StringReplace(Telefonszam, '"','', [rfReplaceAll]);
    // --------------------------------- //
    SMSKuldDlg.RefreshConversation(Telefonszam);  // MySMS: ez a HTTP lek�rdez�s
    // --------------------------------- //
    Screen.Cursor	:= crHourGlass;
    SzalMutat(Telefonszam);
    Screen.Cursor	:= crDefault;
    end;  // if
end;

procedure TUzenetSzalakDlg.FormCreate(Sender: TObject);
var
  JSON: string;
begin
  CimzettNevek:=TDictionary<string,string>.Create;
  SzalDict:=  TDictionary<string,string>.Create;
  ChatControl1:= TChatControl.Create(UzenetSzalakDlg);
  ChatControl1.Parent:= UzenetSzalakDlg;
  ChatControl1.Align:=alClient;
  CimzettNevekFeltolt;
  SzalakFrissit;
end;

procedure TUzenetSzalakDlg.FormDestroy(Sender: TObject);
begin
   if CimzettNevek <> nil then CimzettNevek.Free;
   if SzalDict<>nil then SzalDict.Free;
end;

procedure TUzenetSzalakDlg.CimzettNevekFeltolt;
// a form�z�sok miatt nem tudjuk visszaolvasni az SMS sz�mb�l a tulajt, ez�rt k�sz�t�nk egy lookup t�bl�zatot
var
  S, KOD, Telefonszam, Emailkontakt, Tulaj: string;
begin
  // -------- dolgoz� telefonsz�mok �s email-ek  ------------
  S:= 'select do_kod, do_name from dolgozo where DO_ARHIV=0';
  Query_Run(EgyebDlg.Query1, S, True);
  with EgyebDlg.Query1 do begin
    while not Eof do begin
        KOD:= Fields[0].AsString;
        Tulaj:= Fields[1].AsString;
        Telefonszam:= EgyebDlg.GetDolgozoSMSKontakt(KOD);
        if copy(Telefonszam,1,1)='+' then begin  // telefonsz�m-szer�, de lehet hogy nem magyar
          Telefonszam:= '"'+Telefonszam+'"';
          if not(CimzettNevek.ContainsKey (Telefonszam)) then
            CimzettNevek.Add(Telefonszam, Tulaj);
          end; // if
        Emailkontakt:= EgyebDlg.GetDolgozoEmailKontakt(KOD);
        if EgyebDlg.ErvenyesEmailKontakt(Emailkontakt) then begin
          Emailkontakt:= '"'+Emailkontakt+'"';
          if not(CimzettNevek.ContainsKey (Emailkontakt)) then
            CimzettNevek.Add(Emailkontakt, Tulaj);
          end;  // if
        Next;
        end;  // while
    // Nagy P�ter k�l�n
    if not(CimzettNevek.ContainsKey ('"+36209588785"')) then
        CimzettNevek.Add('"+36209588785"', 'Nagy P�ter');
        CimzettNevek.Add('"nagy.peter@js.hu"', 'Nagy P�ter');
    end;  // with

  S:= 'select v_kod, v_nev from vevo where VE_MOBIL is not null and (VE_MOBIL<>'''') ';
  Query_Run(EgyebDlg.Query1, S, True);
  with EgyebDlg.Query1 do begin
    while not Eof do begin
        KOD:= Fields[0].AsString;
        Tulaj:= Fields[1].AsString;
        Telefonszam:= EgyebDlg.GetPartnerSMSKontakt(KOD);
        if not(CimzettNevek.ContainsKey (Telefonszam)) then
          CimzettNevek.Add(Telefonszam, Tulaj);
        Emailkontakt:= EgyebDlg.GetPartnerEmailKontakt(KOD);
        if EgyebDlg.ErvenyesEmailKontakt(Emailkontakt) then begin
          Emailkontakt:= '"'+Emailkontakt+'"';
          if not(CimzettNevek.ContainsKey (Emailkontakt)) then
            CimzettNevek.Add(Emailkontakt, Tulaj);
          end;  // if
        Next;
        end;  // while
    end;  // with
end;


procedure TUzenetSzalakDlg.Timer1Timer(Sender: TObject);
begin
 // SzalFrissit;
end;

procedure TUzenetSzalakDlg.SzalakFrissit;
var
  retvalue, HibaUzenetek, HibaString, Telefonszam, JSDate, LegutobbiUzenet, Tulajdonos, UzenetekSzama: string;
  SzalakArray : TArray<string>;  // a sorrendez�shez
  S, KeyName: string;
  Hibakod: integer;
  VoltHiba: boolean;
begin

   cbSMSSzalak.Items.Clear;
   SzalDict.Clear;
   S:= 'select SZ_TELEF, SZ_DARAB, SZ_UTODA from SMSSZALAK';
   Query_Run(EgyebDlg.Query1, S, True);
   with EgyebDlg.Query1 do begin
      while not eof do begin
         Telefonszam:= '"'+FieldByName('SZ_TELEF').AsString+'"';
         // UzenetekSzama:= FieldByName('SZ_DARAB').AsInteger;
         // LegutobbiUzenet:= JavaScriptTimestampToFuvaros(StrToInt64(JSDate));
         if CimzettNevek.ContainsKey (Telefonszam) then
            Tulajdonos:= CimzettNevek[Telefonszam]
         else Tulajdonos:= '???';
         SzalDict.Add(Tulajdonos+' <'+Telefonszam+'>', Telefonszam);
         Next;
         end;  // while
      // sorrendezz�k n�v szerint �s �gy tessz�k a combo-ba
      SzalakArray := SzalDict.Keys.ToArray;
      TArray.Sort<string>(SzalakArray);
      for KeyName in SzalakArray do begin
          cbSMSSzalak.Items.Add(KeyName);
          end; // for
      end; // with
end;

procedure TUzenetSzalakDlg.SzalMutat(Telefonszam: string);
var
  retvalue, HibaUzenetek, HibaString, Elkuldve_JSDate, Elkuldve, S: string;
  UzenetSzoveg: String;
  incoming: boolean;
  Hibakod: integer;
  MessageSide: TUser;
  VoltHiba: boolean;

begin
   ChatControl1.Clear;
   S:= 'select SU_BEJOVO, SU_UZENE, SU_KULDI from SMSUZENETEK where SU_TELEF='''+Telefonszam+'''';
   Query_Run(EgyebDlg.Query1, S, True);
   with EgyebDlg.Query1 do begin
      while not eof do begin
         UzenetSzoveg:= FieldByName('SU_UZENE').AsString;
         Elkuldve:= FieldByName('SU_KULDI').AsString;
         if FieldByName('SU_BEJOVO').AsInteger = 1 then
             MessageSide:=User2
         else MessageSide:=User1;  // bal oldal a k�ld�s, jobb a v�lasz (az iroda szemsz�g�b�l)
         ChatControl1.Say(MessageSide, 'timestamp:'+Elkuldve);
         ChatControl1.Say(MessageSide, UzenetSzoveg);
         Next;
         end;  // while
      end;  // with
   ChatControl1.SetFocus;
end;


end.
