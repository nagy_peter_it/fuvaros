﻿unit KulcsDolgozoOlvaso;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, DB, ADODB, Buttons, Nvv.IO.CSV.Delphi.NvvCSVClasses,
  Vcl.Mask, Generics.Collections;

type
  TKulcsDolgozoOlvasoDlg = class(TForm)
    PanelDolgServ: TPanel;
    Query1: TADOQuery;
    Panel2: TPanel;
    Memo1: TMemo;
    OpenDialog1: TOpenDialog;
    GridPanel1: TGridPanel;
    BtnImport: TBitBtn;
    BitBtn6: TBitBtn;
    Query2: TADOQuery;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure OpenCSVSor(CSVSor: string);
    function GetCSVElem(i: integer): string;
    function ParseLine (Cimke: string): boolean;
    procedure CheckElem (Hanyadik: integer; VartErtek: string);
    function GetElemIndex (OszlopFej: string): integer;
    procedure BtnImportClick(Sender: TObject);
    procedure BitBtn6Click(Sender: TObject);
    function GetKulcsCSVName(): string;
    procedure Beolvas_dolgozo;
    procedure Beolvas_szabadsag;
    procedure Beolvas_jogviszony;
    procedure UpdateDolgozo(const SKod, Nev, Adoszam, TAJ, SzuletesiHely, SzuletesiDatum, AnyjaNeve,
                                                Iranyito, Varos, Lakcim, NemMegnevezes: string; var UjDolgozo: boolean);
    procedure UpdateDolgozo_jogviszony(const SKod, Nev, BelepDatum, KilepDatum, FEOR: string);
    procedure UpdateSzabadsag(const dokod, donev: string; const ev, alapszabi, potszabi: integer);
    function SkipLineUntilPattern (i: integer; Pattern: string): boolean;
    function MyStrToInt (S, Cimke: string): integer;
    procedure LegyenDokuMappa(Nev: string);
  private
    kilepes: boolean;
    feldolgozott_dokodlista: TStringList;
    F1: textfile;
    SummaSummarum: TStringList;
  public
    { Public declarations }
  end;


const
  FormatExceptionDesc = 'A KULCS állomány formátuma megváltozott, kérem értesítse a Fuvaros fejlesztőket!';

var
  KulcsDolgozoOlvasoDlg: TKulcsDolgozoOlvasoDlg;
  CSVReader: TnvvCSVStringReader;
  FejlecIndex: TDictionary<string,integer>;
  UjDolgozok: TStringList;
implementation

uses Egyeb,J_SQL, Kozos;

{$R *.dfm}


procedure TKulcsDolgozoOlvasoDlg.OpenCSVSor(CSVSor: string);
const
   FieldSeparator = 59; // ';'
   QuoteChar = 34; // '"'
begin
    with CSVReader do begin
       FieldCount_AutoDetect:= True;
       HeaderPresent:= False;
       UseFieldQuoting:= True;
       FieldSeparatorCharCode:= FieldSeparator;
       QuoteCharCode := QuoteChar;
       Close;
       // SetDataString(CSVSor);
       DataString:= CSVSor;
       Open;
       end;  // with
end;

function TKulcsDolgozoOlvasoDlg.GetCSVElem(i: integer): string;
begin
    Result:= Trim(CSVReader.Fields[i-1].Value);  // index starts with 0
end;

function TKulcsDolgozoOlvasoDlg.ParseLine (Cimke: string): boolean;
var
  sor: string;
begin
  Result:= False;
  if not Eof(F1) then begin
    ReadLn(F1, sor);
    if trim(sor) <> '' then begin
      OpenCSVSor(sor);
      Result:= True;
      end;
    end
  else Exception.Create(FormatExceptionDesc+ ' - '+Cimke);
end;

function TKulcsDolgozoOlvasoDlg.SkipLineUntilPattern (i: integer; Pattern: string): boolean;
var
  sor: string;
  found: boolean;
begin
  if GetCSVElem(i) = Pattern then Result:= True
  else begin
    found:= False;
    while not Eof(F1) and not found do begin
      ReadLn(F1, sor);
      OpenCSVSor(sor);
      if GetCSVElem(i) = Pattern then
          found:= True;
      end;
    if found then Result:= True
    else Result:= False;  // EOF miatt léptünk ki
    end;  // else
end;

procedure TKulcsDolgozoOlvasoDlg.CheckElem (Hanyadik: integer; VartErtek: string);
var
   S: string;
begin
   S:= GetCSVElem(Hanyadik);
   if S <> VartErtek then
      raise Exception.Create(FormatExceptionDesc+ ' - várt érték: '+VartErtek);
end;

function TKulcsDolgozoOlvasoDlg.GetElemIndex (OszlopFej: string): integer;
var
   S: string;
   i: integer;
   Found: boolean;
begin
   i:=1;
   Found:= False;
   while (i <= CSVReader.FieldCount) and not Found do begin
      if GetCSVElem(i)=OszlopFej then
        Found:= True
      else
        Inc(i);
      end;  // while
   if Found then
      Result:= i
   else
      raise Exception.Create(FormatExceptionDesc+ ' - ismeretlen fejléc: '+OszlopFej);
end;

function TKulcsDolgozoOlvasoDlg.MyStrToInt (S, Cimke: string): integer;
begin
   if not JoEgesz(S) then
      raise Exception.Create(FormatExceptionDesc+ ' - Cimke - nem egész szám: '+S)
   else Result:= StrToInt(S);
end;

procedure TKulcsDolgozoOlvasoDlg.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
   if CSVReader <> nil then CSVReader.Free;
   if feldolgozott_dokodlista <> nil then feldolgozott_dokodlista.Free;
   if SummaSummarum <> nil then SummaSummarum.Free;
   if UjDolgozok <> nil then UjDolgozok.Free;
end;

procedure TKulcsDolgozoOlvasoDlg.FormCreate(Sender: TObject);
begin
   CSVReader:= TnvvCSVStringReader.Create;
   feldolgozott_dokodlista:= TStringList.Create;
   feldolgozott_dokodlista.Delimiter:=',';
   SummaSummarum:= TStringList.Create;
   EgyebDlg.SetADOQueryDatabase(Query1);
   EgyebDlg.SetADOQueryDatabase(Query2);
   FejlecIndex:= TDictionary<string,integer>.Create;
   UjDolgozok:= TStringList.Create;
end;


procedure TKulcsDolgozoOlvasoDlg.Beolvas_dolgozo;

var
  SKod, Nev, Adoszam, TAJ, SzuletesiHely, SzuletesiDatum, AnyjaNeve, Iranyito, Varos, Lakcim, TablePrefix, S: string;
  NemMegnevezes, feldolgozott_dokod: string;  // innen tudjuk, hogy kit nem talált a friss listában --> azok kiléptek
  VanAdat, IfUjDolgozo: boolean;
  i, UjCount, i_Ev: integer;
  OsszesCount, UnknownCount: integer;

  function GetElem(N: string): string;
    begin
       Result:= Query1.FieldByname(N).AsString;
    end;

begin
  i_Ev:= StrToInt(copy(EgyebDlg.Maidatum,1,4));
  TablePrefix := EgyebDlg.Read_SZGrid('382', '1') + '.';    // pl. [sql\SQL2005]
  // az 1 az utolsó év, a 2 az előző stb. - de lehet hogy 2018-ban nem így lesz
  TablePrefix := TablePrefix + Format(EgyebDlg.Read_SZGrid('382', 'BERDB'), [IntToStr(i_Ev)]);  // pl. [sql\SQL2005].ber32_2016_ceg_jsspeedfuvaroz1
  TablePrefix := TablePrefix + '.dbo.';
  S := 'exec KulcsSelectDolgozok ''' + TablePrefix + ''' ';
  Query_run(Query1, S);
  // ha eddig eljutott akkor jó a mezőszerkezet
  i:=0;
  UjCount:=0;
  feldolgozott_dokodlista.Clear;
  OsszesCount:= Query1.recordCount;
	while not Query1.Eof do begin
		// Application.ProcessMessages;
    Inc(i);
    SKod:= GetElem('Kod');
    Nev:= Trim(GetElem('Elonev')+' '+GetElem('Vezeteknev')+' '+GetElem('Keresztnev'));
    Nev:= StringReplace(Nev, '  ', ' ', [rfReplaceAll]);  // az esetleges belső többszörös szóközök ellen
    Nev:= TitleCase(Nev);  // egységesen csak az elsõ karakter nagybetû
    NemMegnevezes:= GetElem('NemMegnevezes');
    Adoszam:= GetElem('Adoszam');
    TAJ:= GetElem('TajSzam');
    SzuletesiHely:= GetElem('SzuletesiHely');
    SzuletesiDatum:= KulcsDatum_to_Fuvaros(GetElem('SzuletesiDatum'));
    AnyjaNeve:= GetElem('AnyjaNeve');
    Iranyito:= GetElem('CimIrsz');
    Varos:= GetElem('CimVaros');
   // Memo1.Lines.Add('  = 7 = '+formatdatetime('HH:mm:ss.zzz', Now));
    Lakcim:= GetElem('CimKoztNev')+' '+GetElem('CimHazszam')+' '+GetElem('CimEpulet')+' '+
        GetElem('CimEmelet')+' '+GetElem('CimLepcsHaz')+' '+GetElem('CimAjto');
    // Memo1.Lines.Add('  = 8 = '+formatdatetime('HH:mm:ss.zzz', Now));
    UpdateDolgozo( SKod, Nev, Adoszam, TAJ, SzuletesiHely, SzuletesiDatum, AnyjaNeve, Iranyito, Varos, Lakcim, NemMegnevezes, IfUjDolgozo);
    // *******
    // Inc(UpdateCount);
    // *******
    // Memo1.Lines.Add('  = 9 = '+formatdatetime('HH:mm:ss.zzz', Now));
    if IfUjDolgozo then begin
      Inc(UjCount);
      UjDolgozok.Add(Nev+' ('+SzuletesiDatum+')');
      end;
    // Memo1.Lines.Add('  = 10 = '+formatdatetime('HH:mm:ss.zzz', Now));
    Query1.Next;
    end;  // while
  SummaSummarum.Add('Dolgozó adatok, összesen frissítve: '+IntToStr(OsszesCount)+ ' dolgozó.');
  if UjCount>0 then begin
     SummaSummarum.Add('Új dolgozók: '+IntToStr(UjCount)+ ' dolgozó.');
     for i:=0 to UjDolgozok.Count-1 do
       SummaSummarum.Add('    '+UjDolgozok[i]);
     end;  // if

  // a nem feldolgozottak
  // ezt azért nem kell, mert a kilépett dolgozók benne lesznek a "jogviszony" állományban is, és dátum
  // alapján majd archív-ba állítjuk őket.

  // Query_Update (Query1, 'DOLGOZO',
	//		[
	//		'DO_KILEP', ''''+EgyebDlg.Maidatum+'''',
	//		'DO_ARHIV', ''''+'1'+''''
	//		], ' WHERE DO_ARHIV=''0'' AND DO_KOD NOT IN ('+feldolgozott_dokodlista.DelimitedText +')');

end;



procedure TKulcsDolgozoOlvasoDlg.Beolvas_szabadsag;
var
  dokod, S, TablePrefix: string;
  ev, szabi, i, alapszabi, potszabi: integer;
  DolgozoNev, SzulDatum, SKod: string;
  VanAdat: boolean;
  UpdateCount, UnknownCount: integer;

  function GetElem(N: string): string;
    begin
       Result:= Query1.FieldByname(N).AsString;
    end;

begin
  ev:= StrToInt(copy(EgyebDlg.Maidatum,1,4));
  TablePrefix := EgyebDlg.Read_SZGrid('382', '1') + '.';    // pl. [sql\SQL2005]
  // az 1 az utolsó év, a 2 az előző stb. - de lehet hogy 2018-ban nem így lesz
  TablePrefix := TablePrefix + Format(EgyebDlg.Read_SZGrid('382', 'BERDB'), [IntToStr(ev)]);  // pl. [sql\SQL2005].ber32_2016_ceg_jsspeedfuvaroz1
  TablePrefix := TablePrefix + '.dbo.';
  S := 'exec KulcsSelectSzabadsagok ''' + TablePrefix + ''' ';
  Query_run(Query1, S);

  i:=0;
  UpdateCount:=0;
  UnknownCount:=0;
  kilepes:= false;
  // Memo1.Lines.Clear;
	while not Query1.Eof do begin
		// Application.ProcessMessages;
    Inc(i);
    SKod:= GetElem('DolgozoKod');
    S:= 'select DO_KOD, DO_NAME from DOLGOZO where DO_SKOD='+SKod;
    Query_Run (Query2, S);
    // Memo1.Lines.Add('  = 8.2 = '+formatdatetime('HH:mm:ss.zzz', Now));
    dokod:= Query2.Fields[0].AsString;
    DolgozoNev:= Query2.Fields[1].AsString;
    // szabi:= StrToInt(GetElem('OsszesSzabadsag'));
    alapszabi:= StrToInt(GetElem('AlapSzabadsag'));
    potszabi:= StrToInt(GetElem('PotSzabadsag'));
    UpdateSzabadsag(dokod, DolgozoNev, ev, alapszabi, potszabi);
    Inc(UpdateCount);
    // Memo1.Lines.Add('Szabadság frissítve: '+DolgozoNev+ ' ('+SzulDatum+') Évi '+IntToStr(szabi)+' nap');
    Query1.Next;
    end;  // while
  SummaSummarum.Add('Szabadság adatok, összesen frissítve: '+IntToStr(UpdateCount)+ ' dolgozó.');
end;



procedure TKulcsDolgozoOlvasoDlg.Beolvas_jogviszony;

var
  SKod, Nev, BelepDatum, KilepDatum, FEOR, TablePrefix, S: string;
  feldolgozott_dokod: string;  // innen tudjuk, hogy kit nem talált a friss listában --> azok kiléptek
  VanAdat: boolean;
  i, i_Ev: integer;
  UpdateCount, UnknownCount: integer;

  function GetElem(N: string): string;
    begin
       Result:= Query1.FieldByname(N).AsString;
    end;

begin
  i_Ev:= StrToInt(copy(EgyebDlg.Maidatum,1,4));
  TablePrefix := EgyebDlg.Read_SZGrid('382', '1') + '.';    // pl. [sql\SQL2005]
  // az 1 az utolsó év, a 2 az előző stb. - de lehet hogy 2018-ban nem így lesz
  TablePrefix := TablePrefix + Format(EgyebDlg.Read_SZGrid('382', 'BERDB'), [IntToStr(i_Ev)]);  // pl. [sql\SQL2005].ber32_2016_ceg_jsspeedfuvaroz1
  TablePrefix := TablePrefix + '.dbo.';
  S := 'exec KulcsSelectJogviszonyok ''' + TablePrefix + ''' ';
  Query_run(Query1, S);

  i:=0;
  UpdateCount:=0;
  UnknownCount:=0;
  while not Query1.Eof do begin
		// Application.ProcessMessages;
    Inc(i);
    SKod:= GetElem('DolgozoKod');
    Nev:= GetElem('DolgozoElonev')+' '+GetElem('DolgozoVezetekNev')+' '+GetElem('DolgozoKeresztNev');
    Nev:= StringReplace(Nev, '  ', ' ', [rfReplaceAll]);  // az esetleges belső többszörös szóközök ellen
    Nev:= TitleCase(Nev);
    BelepDatum:= KulcsDatum_to_Fuvaros(GetElem('BelepesDatum'));
    KilepDatum:= KulcsDatum_to_Fuvaros(GetElem('KilepesDatum'));
    FEOR:= GetElem('kFEORKod')+' '+GetElem('kFEORNev');
    UpdateDolgozo_jogviszony( SKod, Nev, BelepDatum, KilepDatum, FEOR);
    Inc(UpdateCount);
    Query1.Next;
    end;  // while
  SummaSummarum.Add('Jogviszony adatok, összesen frissítve: '+IntToStr(UpdateCount)+ ' jogviszony.');
  if UnknownCount>0 then
    SummaSummarum.Add('*** Ismeretlen dolgozó: '+IntToStr(UnknownCount)+ ' db.');
end;


procedure TKulcsDolgozoOlvasoDlg.UpdateDolgozo(const SKod, Nev, Adoszam, TAJ, SzuletesiHely, SzuletesiDatum, AnyjaNeve,
                                                Iranyito, Varos, Lakcim, NemMegnevezes: string; var UjDolgozo: boolean);
var
  dokod, S: string;
  UpdateCount, UnknownCount: integer;
begin
  // Memo1.Lines.Add('  = 8.1 = '+formatdatetime('HH:mm:ss.zzz', Now));
  S:= 'select DO_KOD from DOLGOZO where DO_SKOD='+SKod;
  Query_Run (Query2, S);
  // Memo1.Lines.Add('  = 8.2 = '+formatdatetime('HH:mm:ss.zzz', Now));
  dokod:= Query2.Fields[0].AsString;
  // Memo1.Lines.Add('  = 8.3 = '+formatdatetime('HH:mm:ss.zzz', Now));
  if dokod <> '' then begin  // megtaláltuk
    feldolgozott_dokodlista.Add(dokod);
    // Memo1.Lines.Add('  = 8.4 = '+formatdatetime('HH:mm:ss.zzz', Now));
    Query_Update (Query2, 'DOLGOZO',
			[
			// 'DO_DFEOR', ''''+'XX'+'''', // ezt most nem tudjuk
			'DO_TAJSZ', ''''+TAJ+'''',
			'DO_ADOSZ', ''''+Adoszam+'''',
			'DO_SZULH', ''''+SzuletesiHely+'''',
			'DO_NAME', ''''+Nev+'''',
			'DO_SZIDO', ''''+SzuletesiDatum+'''',
			'DO_ANYAN', ''''+AnyjaNeve+'''',
			'DO_NEME', ''''+NemMegnevezes+'''',
			// 'DO_KILEP', ''''+'XX'+'''',   // ezt most nem tudjuk
			// 'DO_ARHIV', ''''+'XX'+'''',   // ezt most nem tudjuk
			// 'DO_BELEP', ''''+'XX'+'''',   // ezt most nem tudjuk
			'DO_LAIRS', ''''+Iranyito+'''',
			'DO_LATEL', ''''+Varos+'''',
			'DO_LACIM', ''''+Lakcim+''''
			], ' WHERE DO_KOD='''+dokod+'''');
      // Memo1.Lines.Add('  = 8.5 = '+formatdatetime('HH:mm:ss.zzz', Now));
      Memo1.Lines.Add('Dolgozó frissítve: '+Nev+ ' ('+SzuletesiHely+', '+SzuletesiDatum+')');
      UjDolgozo:= False;
     end
  else begin  // nem találtuk
    // dokod:= Query_SelectString('DOLGOZO', 'select max(DO_KOD)+1 from DOLGOZO');
    dokod:= Query_SelectString('DOLGOZO', 'select max(convert(int, DO_KOD))+1 from DOLGOZO');
    feldolgozott_dokodlista.Add(dokod);
    Query_Insert (Query2, 'DOLGOZO',
			[
       'DO_KOD', ''''+dokod+'''',
       'DO_SKOD', ''''+SKod+'''',
			// 'DO_DFEOR', ''''+'XX'+'''', // ezt most nem tudjuk
      'DO_RENDSZ', ''''+''+'''',
			'DO_TAJSZ', ''''+TAJ+'''',
			'DO_ADOSZ', ''''+Adoszam+'''',
			'DO_SZULH', ''''+SzuletesiHely+'''',
			'DO_NAME', ''''+Nev+'''',
			'DO_SZIDO', ''''+SzuletesiDatum+'''',
			'DO_ANYAN', ''''+AnyjaNeve+'''',
      'DO_NEME', ''''+NemMegnevezes+'''',
			'DO_KILEP', ''''+''+'''',
			'DO_ARHIV', ''''+'0'+'''',
      'DO_KULSO', ''''+'0'+'''',
			'DO_BELEP', ''''+ EgyebDlg.Maidatum+'''',  // nem kapjuk meg a KULCS-ból
			'DO_LAIRS', ''''+Iranyito+'''',
			'DO_LATEL', ''''+Varos+'''',
			'DO_LACIM', ''''+Lakcim+''''
			]);
      Query_Insert (Query2, 'DOLGOZOMEG',
			[
       'DM_DOKOD', ''''+dokod+''''
			]);
      LegyenDokuMappa(Nev);
      Memo1.Lines.Add('Dolgozó felvéve: '+Nev+ ' ('+SzuletesiHely+', '+SzuletesiDatum+')');
      UjDolgozo:= True;
     end; // else
end;

procedure TKulcsDolgozoOlvasoDlg.LegyenDokuMappa(Nev: string);
var
  dir0: string;
begin
  dir0:= EgyebDlg.Read_SZGrid('999', '720');
  if ( ( copy(dir0, Length(dir0), 0) <> '\' ) and ( copy(dir0, Length(dir0), 0) <> '/' ) ) then
		dir0	:= dir0 + '\';
  CreateDir(dir0+Nev);
end;

procedure TKulcsDolgozoOlvasoDlg.UpdateDolgozo_jogviszony(const SKod, Nev, BelepDatum, KilepDatum, FEOR: string);
var
  dokod, archivflag, ervenyesflag, AdatResz, S: string;
begin
  // lassú... dokod:= Query_Select('DOLGOZO', 'DO_SKOD', SKod, 'DO_KOD');
  S:= 'select DO_KOD from DOLGOZO where DO_SKOD='+SKod;
  Query_Run (Query2, S);
  dokod:= Query2.Fields[0].AsString;

  if dokod <> '' then begin  // megtaláltuk
    // feldolgozott_dokodlista.Add(dokod);
    Query_Update (Query2, 'DOLGOZO',
			[
			'DO_BELEP', ''''+BelepDatum+'''',
			'DO_KILEP', ''''+KilepDatum+'''',
			'DO_DFEOR', ''''+FEOR+''''
			], ' WHERE DO_KOD='''+dokod+'''');

    // ha már kilépett, beállítjuk archívnak
    // elvileg lehet olyan dolgozó, ahol a DO_KILEP múltbeli, és mégsem DO_ARHIV - ha nem futtattunk beolvasást
    // itt kihasználjuk azt, hogy a jogviszonyok időrendben követik egymást a beolvasás során - ha több van,
    // akkor az utolsó az érvényes
    if (KilepDatum = '') or (KilepDatum >= EgyebDlg.maidatum) then begin // még nem lépett ki
        archivflag:= '0';
        ervenyesflag:='1';
        end
    else begin
        archivflag:= '1';
        ervenyesflag:='0';
        end;
    Query_Update (Query2, 'DOLGOZO',
			[
			'DO_ARHIV', ''''+archivflag+''''
			], ' WHERE DO_KOD='''+dokod+'''');
    // ------ a kapcsolódó felhasználó letiltása, ha kell  ****** //
     Query_Update (Query2, 'JELSZO',
			[
			'JE_VALID', ''''+ervenyesflag+''''
			], ' WHERE JE_DOKOD='''+dokod+'''');
    // ----------------------------------- //
    AdatResz:= 'Belép: '+BelepDatum;
    if KilepDatum <> '' then
       AdatResz:= AdatResz+ ', kilép: '+KilepDatum;
    Memo1.Lines.Add('Dolgozó jogviszony frissítve: '+Nev+ ' ('+'Dolgozó kód: '+dokod+') '+AdatResz);
    end
  else begin
    // itt nem veszünk fel dolgozót, az újakat az UpdateDolgozo-ban már felvettük
     Memo1.Lines.Add('*** Ismeretlen dolgozó: '+Nev+ ' ('+'Bérprogram kód: '+SKod+')');
     end;  // else
end;


procedure TKulcsDolgozoOlvasoDlg.UpdateSzabadsag(const dokod, donev: string; const ev, alapszabi, potszabi: integer);
var
  darab, S: string;
begin
  // darab:= Query_SelectString('DOLGOZO', 'select count(sb_dokod) from SZABADSAG WHERE sb_dokod='''+dokod+''' and sb_ev='''+IntToStr(ev)+'''');
  S:= 'select count(sb_dokod) from SZABADSAG WHERE sb_dokod='''+dokod+''' and sb_ev='''+IntToStr(ev)+'''';
  Query_Run (Query2, S);
  darab:= Query2.Fields[0].AsString;

  if darab = '1' then begin  // megtaláltuk
    Query_Update (Query2, 'SZABADSAG',
			[
			'sb_szabi', ''''+IntToStr(alapszabi)+'''',
			'sb_eszab', ''''+IntToStr(potszabi)+''''
			], ' WHERE sb_dokod='''+dokod+''' and sb_ev='''+IntToStr(ev)+'''');
     end
  else begin  // nem találtuk
    Query_Insert (Query2, 'SZABADSAG',
			[
      'sb_dokod', ''''+dokod+'''',
      'sb_ev', ''''+IntToStr(ev)+'''',
      'sb_szabi', ''''+IntToStr(alapszabi)+'''',
			'sb_eszab', ''''+IntToStr(potszabi)+''''
			]);
     end; // else
  Memo1.Lines.Add('Szabadság frissítve - '+donev+ ' '+IntToStr(ev)+': '+IntToStr(alapszabi)+'+'+IntToStr(potszabi)+' nap');
end;

procedure TKulcsDolgozoOlvasoDlg.btnImportClick(Sender: TObject);
const
  // Szepsor =  '************************************';
  Szepsor =  '------------------------------------';
  // Szepdarab = '***  ';
  Szepdarab = '---  ';
var
  i: integer;
begin
  // btnImport.Hide;
  SummaSummarum.Clear;
  Memo1.Lines.Clear;  // hogy egyben látsszon mindhárom lista
  Memo1.Lines.Add(Szepsor);
  Memo1.Lines.Add(Szepdarab+ 'DOLGOZÓK BEOLVASÁSA');
  Memo1.Lines.Add(Szepsor);
  Beolvas_dolgozo;
  Memo1.Lines.Add(Szepsor);
  Memo1.Lines.Add(Szepdarab+ 'JOGVISZONYOK BEOLVASÁSA');
  Memo1.Lines.Add(Szepsor);
  Beolvas_jogviszony;
  Memo1.Lines.Add(Szepsor);
  Memo1.Lines.Add(Szepdarab+ 'SZABADSÁGOK BEOLVASÁSA');
  Memo1.Lines.Add(Szepsor);
  Beolvas_szabadsag;
  Memo1.Lines.Add('---------------------------------------------------------------');
  Memo1.Lines.AddStrings(SummaSummarum);
  Memo1.Lines.Add('---------------------------------------------------------------');
  // Elmentjük a beolvasás logját
  EgyebDlg.AppendLog('KULCS_IMPORT', '###################################################');
  EgyebDlg.AppendLog('KULCS_IMPORT', 'Felhasználó: '+EgyebDlg.user_name);
  EgyebDlg.AppendLog('KULCS_IMPORT', '###################################################');
  for i:=0 to Memo1.Lines.Count-1 do
      EgyebDlg.AppendLog('KULCS_IMPORT', Memo1.Lines[i]);
  EgyebDlg.AppendLog('KULCS_IMPORT', '');
end;

function TKulcsDolgozoOlvasoDlg.GetKulcsCSVName(): string;
begin
	// Az állomány kiválasztása
	OpenDialog1.Filter		:= 'KULCS mentett állományok (*.CSV)|*.CSV';
	OpenDialog1.DefaultExt  := 'CSV';
	OpenDialog1.InitialDir	:= EgyebDlg.Read_SZGrid('999','755'); // KULCS dolgozó kapcsolat könyvtára
	if OpenDialog1.Execute then begin
		Result:= OpenDialog1.FileName;
	end;
end;

procedure TKulcsDolgozoOlvasoDlg.BitBtn6Click(Sender: TObject);
begin
	if not kilepes then begin
		kilepes	:= true;
		// BtnImport.Show;
	end else begin
		Close;
	end;
end;

{procedure TKulcsDolgozoOlvasoDlg.Beolvas_dolgozo;
type
   TFejlecArray = array[0..16] of string;

const
  cFejlecmezok: TFejlecArray = ('Kód','Előnév','Vezetéknév','Keresztnév','Adószám','TAJ szám','Születési hely','Születési dátum',
         'Anyja neve','Irányítószám','Város','Közterület név','Házszám','Épület','Emelet','Lépcsőház','Ajtó');

var
  SKod, Nev, Adoszam, TAJ, SzuletesiHely, SzuletesiDatum, AnyjaNeve, Iranyito, Varos, Lakcim: string;
  feldolgozott_dokod: string;  // innen tudjuk, hogy kit nem talált a friss listában --> azok kiléptek
  VanAdat, IfUjDolgozo: boolean;
  i, UjCount: integer;

  procedure FejlecFeldolgozas (OszlopLista: TFejlecArray);
    var
     i, oszlopindex: integer;
     Fej: string;
    begin
     FejlecIndex.Clear;
     for i:=0 to Length(OszlopLista)-1 do begin
        Fej:= OszlopLista[i];
        oszlopindex:= GetElemIndex(Fej);
        FejlecIndex.Add(Fej, oszlopindex);
        end;  // for
    end;

begin
	AssignFile(F1, M1.Text);
	Reset(F1);
	i	:= 0;
  ParseLine('címsor');
  CheckElem(1, 'Dolgozók listája');
  ParseLine('');  // üres sor, ugorjunk
  ParseLine('cég adatok');
  CheckElem(1, 'Cég neve:');
  // CheckElem(4, 'J&S');  -- ezt esetleg szótárból
  ParseLine('fejléc');
   // a különböző blokkokban más lehet a struktúra (!!), betold 1-1 oszlopot a .csv-be
  FejlecFeldolgozas(cFejlecmezok);

  // ha eddig eljutott akkor jó a mezőszerkezet
  i:=0;
  UjCount:=0;
  feldolgozott_dokodlista.Clear;
	while (not Eof(F1) and not kilepes) do begin
		Application.ProcessMessages;
    Inc(i);
		VanAdat:= ParseLine('adatsor'+ IntToStr(i));   // ha üres sort talál, VanAdat false lesz
    if not VanAdat then begin
       VanAdat:= SkipLineUntilPattern(1, 'Kód');  // a fejléc soron áll
       if VanAdat then // ha megvolt a fejléc
         FejlecFeldolgozas(cFejlecmezok);
       if VanAdat then // ha megvolt a fejléc
         VanAdat:= ParseLine('adatsor'+ IntToStr(i));   // beolvassa a következő adatsort
       end;
    if VanAdat then begin
      SKod:= GetCSVElem(FejlecIndex['Kód']);
      Nev:= Trim(GetCSVElem(FejlecIndex['Előnév'])+' '+GetCSVElem(FejlecIndex['Vezetéknév'])+' '+GetCSVElem(FejlecIndex['Keresztnév']));
      Nev:= StringReplace(Nev, '  ', ' ', [rfReplaceAll]);  // az esetleges belső többszörös szóközök ellen
      Nev:= TitleCase(Nev);  // egységesen csak az elsõ karakter nagybetû
      Adoszam:= GetCSVElem(FejlecIndex['Adószám']);
      TAJ:= GetCSVElem(FejlecIndex['TAJ szám']);
      SzuletesiHely:= GetCSVElem(FejlecIndex['Születési hely']);
      SzuletesiDatum:= KulcsDatum_to_Fuvaros(GetCSVElem(FejlecIndex['Születési dátum']));
      AnyjaNeve:= GetCSVElem(FejlecIndex['Anyja neve']);
      Iranyito:= GetCSVElem(FejlecIndex['Irányítószám']);
      Varos:= GetCSVElem(FejlecIndex['Város']);
      Lakcim:= GetCSVElem(FejlecIndex['Közterület név'])+' '+GetCSVElem(FejlecIndex['Házszám'])+' '+GetCSVElem(FejlecIndex['Épület'])+' '+
          GetCSVElem(FejlecIndex['Emelet'])+' '+GetCSVElem(FejlecIndex['Lépcsőház'])+' '+GetCSVElem(FejlecIndex['Ajtó']);
      UpdateDolgozo( SKod, Nev, Adoszam, TAJ, SzuletesiHely, SzuletesiDatum, AnyjaNeve, Iranyito, Varos, Lakcim, IfUjDolgozo);
      if IfUjDolgozo then begin
        Inc(UjCount);
        UjDolgozok.Add(Nev+' ('+SzuletesiDatum+')');
        end;
      end  // if VanAdat
    else begin
      kilepes:= True;  // az adatok utáni első üres soron
      end;
    end;  // while
 	CloseFile(F1);
  SummaSummarum.Add('Dolgozó adatok, összesen frissítve: '+IntToStr(UpdateCount)+ ' dolgozó.');
  if UjCount>0 then begin
     SummaSummarum.Add('Új dolgozók: '+IntToStr(UjCount)+ ' dolgozó.');
     for i:=0 to UjDolgozok.Count do
       SummaSummarum.Add('    '+UjDolgozok[i]);
     end;  // if


end;
}

{procedure TKulcsDolgozoOlvasoDlg.Beolvas_szabadsag;
type
   TFejlecArray = array[0..1] of string;

const
  cFejlecmezok: TFejlecArray = ('(Szabadságok napban)','Tárgyéviszámított');
  cAdatOffset = 2;  // ennyivel van jobbra az adat a fejéchez képest
var
  dokod, S: string;
  ev, szabi, i, alapszabi, potszabi: integer;
  DolgozoNev, SzulDatum: string;
  VanAdat: boolean;

   procedure FejlecFeldolgozas (OszlopLista: TFejlecArray);
    var
     i, oszlopindex: integer;
     Fej: string;
    begin
     FejlecIndex.Clear;
     for i:=0 to Length(OszlopLista)-1 do begin
        Fej:= OszlopLista[i];
        oszlopindex:= GetElemIndex(Fej);
        FejlecIndex.Add(Fej, oszlopindex);
        end;  // for
    end;
begin
	AssignFile(F1, M2.Text);
	Reset(F1);
	i	:= 0;
  ParseLine('cég adatok');
  CheckElem(1, 'Munkáltató neve:');
  // CheckElem(3, 'J&S');  -- ezt esetleg szótárból
  ParseLine('');  // üres sor, ugorjunk
  ParseLine('cég adatok');
  CheckElem(1, 'Székhelyének címe:');
  ParseLine('');  // üres sor, ugorjunk
  ParseLine('cég adatok');
  CheckElem(1, 'Szabadság lista');
  ParseLine('év');
  S:= GetCSVElem(1);  // elvileg "2015. év" a tartalom
  ev:= MyStrToInt(copy(S,1,4), 'evszam');
  i:=0;
  UpdateCount:=0;
  UnknownCount:=0;
  kilepes:= false;
  // Memo1.Lines.Clear;
	while (not Eof(F1) and not kilepes) do begin
		Application.ProcessMessages;
    Inc(i);
		VanAdat:= ParseLine('dolgozo'+ IntToStr(i));
    if VanAdat then
      VanAdat:= SkipLineUntilPattern(1, 'Munkavállaló:');
    if VanAdat then begin
      CheckElem(1, 'Munkavállaló:');
      S:= GetCSVElem(2);  // elvileg " Gipsz Jakab   (szül.:1975.07.24.)" a tartalom
      if S='' then   // ezért bocs, nem konzisztens a csv tartalma...
         S:= GetCSVElem(3);
      DolgozoNev:= Trim(EgyebDlg.SepFrontToken('(', S));  // "Gipsz Jakab"
      DolgozoNev:= StringReplace(DolgozoNev, '  ', ' ', [rfReplaceAll]);  // a belső többszörös szóközök ellen
      DolgozoNev:= TitleCase(DolgozoNev);
      S:= Trim(EgyebDlg.SepRest('(', S));  // "szül.:1975.07.24.)" a tartalom
      S:= Trim(EgyebDlg.SepRest('szül.:', S));  // "1975.07.24.)" a tartalom
      SzulDatum:= copy(S, 1, 11);  // "1975.07.24." a tartalom
      ParseLine('blokk fejlec');
      FejlecFeldolgozas(cFejlecmezok);

      CheckElem(1, '(Szabadságok napban)');  // fejléc sor ellenőrzése
      // CheckElem(6+OffsetJobbra, 'Tárgyéviszámított');  // nem kell
      ParseLine('');  // üres sor, ugorjunk
      ParseLine('alapszabadsag');
      try
        CheckElem(1, 'Alapszabadság');  // fejléc sor ellenőrzése
        S:= GetCSVElem(FejlecIndex['Tárgyéviszámított']+cAdatOffset);
        alapszabi:= MyStrToInt(S, 'dolgozo'+ IntToStr(i)+' alapszabi');
      except  // ha nem talált "Alapszabadság" sort - pl. gyakornok
        alapszabi:= 0;
        end;  // try-except
      // itt a következő üres sorig olvassuk a felsorolt szabadságokat, attól függően hogy az illető
      // dolgozónak milyen fajták vannak
      potszabi:= 0;
      ParseLine('potszabadsag');
      S:= GetCSVElem(1);
      while S <> '' do begin
         if not Pos('szabadság', S)>0 then
            raise Exception.Create(FormatExceptionDesc+ ' - szabadság sort vártunk, ezt találtuk: '+S);
         if S='Életkor utáni pótszabadság' then begin  // csak ezt kell hozzáadni az alapszabadsághoz
           S:= GetCSVElem(FejlecIndex['Tárgyéviszámított']+cAdatOffset);
           potszabi:= potszabi + MyStrToInt(S, 'dolgozo'+ IntToStr(i)+' eletkorszabi');
           end;
         ParseLine('potszabadsag');
         S:= GetCSVElem(1);
         end;  // while

      szabi:= alapszabi + potszabi;

      // ParseLine('');  // üres sor, ugorjunk

      dokod:= Query_Select2('DOLGOZO', 'DO_NAME', 'DO_SZIDO', DolgozoNev, SzulDatum, 'DO_KOD');
      if dokod <> '0' then begin
         UpdateSzabadsag(dokod, ev, szabi);
         Inc(UpdateCount);
         Memo1.Lines.Add('Szabadság frissítve: '+DolgozoNev+ ' ('+SzulDatum+') Évi '+IntToStr(szabi)+' nap');
         end
      else begin
         Inc(UnknownCount);
         Memo1.Lines.Add('*** Ismeretlen dolgozó: '+DolgozoNev+ ' ('+SzulDatum+')');
         end;
      end  // if VanAdat
    else begin
      kilepes:= True;  // az adatok utáni első üres soron
      end;
    end;  // while
 	CloseFile(F1);
  SummaSummarum.Add('Szabadság adatok, összesen frissítve: '+IntToStr(UpdateCount)+ ' dolgozó.');
  if UnknownCount>0 then
    SummaSummarum.Add('*** Ismeretlen dolgozó: '+IntToStr(UnknownCount)+ ' db.');
end;
}


{procedure TKulcsDolgozoOlvasoDlg.Beolvas_jogviszony;
type
  TFejlecArray = array[0..5] of string;

const
  cFejlecmezok: TFejlecArray = ('Dolgozó kód','Dolgozó vezetéknév','Dolgozó keresztnév','Belépés dátuma','Kilépés dátuma','FEOR');

var
  SKod, Nev, BelepDatum, KilepDatum, FEOR: string;
  feldolgozott_dokod: string;  // innen tudjuk, hogy kit nem talált a friss listában --> azok kiléptek
  VanAdat: boolean;
  i: integer;

  procedure FejlecFeldolgozas (OszlopLista: TFejlecArray);
    var
     i, oszlopindex: integer;
     Fej: string;
    begin
     FejlecIndex.Clear;
     for i:=0 to Length(OszlopLista)-1 do begin
        Fej:= OszlopLista[i];
        oszlopindex:= GetElemIndex(Fej);
        FejlecIndex.Add(Fej, oszlopindex);
        end;  // for
    end;

begin
	AssignFile(F1, M3.Text);
	Reset(F1);
	i	:= 0;
  ParseLine('címsor');
  CheckElem(1, 'Jogviszonyok listája');
  ParseLine('');  // hónap, ugorjunk
  ParseLine('cég adatok');
  CheckElem(1, 'Cég neve:');
  // CheckElem(4, 'J&S');  -- ezt esetleg szótárból
  ParseLine('fejléc');
  FejlecFeldolgozas(cFejlecmezok);
  // ha eddig eljutott akkor jó a mezőszerkezet
  i:=0;
  UpdateCount:=0;
  UnknownCount:=0;
  kilepes:= false;
  // Memo1.Lines.Clear;
  // feldolgozott_dokodlista.Clear;
	while (not Eof(F1) and not kilepes) do begin
		Application.ProcessMessages;
    Inc(i);
		VanAdat:= ParseLine('adatsor'+ IntToStr(i));
    if VanAdat and not JoEgesz(GetCSVElem(1)) then  // van hogy nincs üre sor az oldalak között, hanem egyből a ""Készült 2015.12.16-án..." sor jön
       VanAdat:= False;
    if not VanAdat then begin
       VanAdat:= SkipLineUntilPattern(1, 'Dolgozó kód');  // a fejléc soron áll
       if VanAdat then // ha megvolt a fejléc
         FejlecFeldolgozas(cFejlecmezok);
       if VanAdat then // ha megvolt a fejléc
         VanAdat:= ParseLine('adatsor'+ IntToStr(i));   // beolvassa a következő adatsort
       end;
    if VanAdat then begin
      SKod:= GetCSVElem(FejlecIndex['Dolgozó kód']);
      Nev:= GetCSVElem(FejlecIndex['Dolgozó vezetéknév'])+' '+GetCSVElem(FejlecIndex['Dolgozó keresztnév']);
      Nev:= StringReplace(Nev, '  ', ' ', [rfReplaceAll]);  // az esetleges belső többszörös szóközök ellen
      Nev:= TitleCase(Nev);
      BelepDatum:= KulcsDatum_to_Fuvaros(GetCSVElem(FejlecIndex['Belépés dátuma']));
      KilepDatum:= KulcsDatum_to_Fuvaros(GetCSVElem(FejlecIndex['Kilépés dátuma']));
      // FEOR:= GetCSVElem(8);
      FEOR:= GetCSVElem(FejlecIndex['FEOR']);
      UpdateDolgozo_jogviszony( SKod, Nev, BelepDatum, KilepDatum, FEOR);
      end  // if VanAdat
    else begin
      kilepes:= True;  // az adatok utáni első üres soron
      end;
    end;  // while
 	CloseFile(F1);
  SummaSummarum.Add('Jogviszony adatok, összesen frissítve: '+IntToStr(UpdateCount)+ ' jogviszony.');
  if UnknownCount>0 then
    SummaSummarum.Add('*** Ismeretlen dolgozó: '+IntToStr(UnknownCount)+ ' db.');
end;
}

end.
