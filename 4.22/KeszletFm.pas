unit KeszletFm;

interface

uses
	SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
	StdCtrls, Forms, ExtCtrls, Dialogs, J_FOFORMUJ;

type
	TKeszletFmDlg = class(TJ_FOFORMUJDLG)
	  procedure FormCreate(Sender: TObject);override;
    procedure Adatlap(cim, kod : string);override;
	end;

var
	KeszletFmDlg : TKeszletFmDlg;

implementation

uses
	Egyeb, J_SQL,Keszletbe;

{$R *.DFM}

procedure TKeszletFmDlg.FormCreate(Sender: TObject);
begin
	Inherited FormCreate(Sender);
	AddSzuromezoRovid('KM_RAKNEV', 'KMOZGAS');
	AddSzuromezoRovid('KM_TNEV', 'KMOZGAS');
	AddSzuromezoRovid('KM_RENDSZ', 'KMOZGAS');
	AddSzuromezoRovid('KM_MNEM', 'KMOZGAS');
	AddSzuromezoRovid('KM_KIBE', 'KMOZGAS');
	FelTolto('�j mozg�s felvitele ', 'Mozg�s adatok m�dos�t�sa ','Mozg�sok list�ja', 'KM_ID', 'Select * from KMOZGAS ', 'KMOZGAS', QUERY_ADAT_TAG );
	width				:= DLG_WIDTH;
	height			:= DLG_HEIGHT;
end;

procedure TKeszletFmDlg.Adatlap(cim, kod : string);
begin
  Application.CreateForm(TKeszletbeDlg, AlForm);
//	Application.CreateForm(TArfolybeDlg, AlForm);
	Inherited Adatlap(cim, kod );

	ModLocate (Query1, FOMEZO, Query1.FieldByName(FOMEZO).AsString );

end;

end.

