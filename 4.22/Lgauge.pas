unit Lgauge;

interface

uses
	SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
	Forms, Dialogs, Gauges, ExtCtrls, StdCtrls;

type
	TFormGaugeDlg = class(TForm)
	Label1: TLabel;
	Panel1: TPanel;
	ListaGauge: TGauge;
	Button1: TButton;
    Label2: TLabel;
	procedure Button1Click(Sender: TObject);
	private
		idotartam	: TDateTime;
	public
		kilepes : boolean;
  	procedure GaugeNyit(cim : string; szoveg : string; vangomb : boolean = true);
  	procedure GaugeZar;
    procedure SetMax( max : integer);
	  procedure Pozicio( pos : integer);

	end;

var
	FormGaugeDlg: TFormGaugeDlg;

implementation

{$R *.DFM}

procedure TFormGaugeDlg.Button1Click(Sender: TObject);
begin
	kilepes := true;
end;

procedure TFormGaugeDlg.GaugeNyit(cim : string; szoveg : string; vangomb : boolean = true);
begin
	Label1.Caption  		:= szoveg;
	Label1.Update;
	Caption  				:= cim;
	kilepes 				:= false;
	idotartam				:= now;
	ListaGauge.Progress 	:= 0;
	if not vangomb then begin
		Button1.Visible		:= false;
		FormGaugeDlg.Height	:= 120;
	end;
	Show;
end;

procedure TFormGaugeDlg.Pozicio( pos : integer);
var
  msg		: TMSG;
begin
//  Application.ProcessMessages;
	ListaGauge.Progress := pos;
	Button1.Update;
	Label1.Update;
	Label2.Caption	:= FormatDateTime('NN.SS', now-idotartam);
	Label2.Update;
	if PeekMessage( msg, FormGaugeDlg.Button1.Handle,WM_LBUTTONDOWN, WM_LBUTTONDOWN, PM_REMOVE ) then begin
		FormGaugeDlg.kilepes := true;
	end;
end;

procedure TFormGaugeDlg.SetMax( max : integer);
begin
	ListaGauge.MaxValue := max;
end;

procedure TFormGaugeDlg.GaugeZar;
begin
	FormGaugeDlg.Close;
end;

end.
