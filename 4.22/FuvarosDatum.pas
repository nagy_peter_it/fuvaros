unit FuvarosDatum;
{******************************************************************************
   Aloszt�ly a t�vols�g meghat�roz�s�hoz
*******************************************************************************}

interface

uses
   Classes, SysUtils, Variants,  Vcl.ExtCtrls, Kozos;

type
   TFuvarosDatum = Class (TObject)
      private
         FDateTimeValue: TDateTime;
      public
         // constructor Create(AOwner: TComponent); override;
         // destructor Destroy; override;
         class function FuvarosDatumToDateTime(const AFuvarosDatum: string): TDateTime;
         class function DateTimeToFuvarosDatum(const ADT: TDateTime): string;
         procedure SetFuvarosDatum(const AFuvarosDatum: string);
         procedure SetDateTime(const ADT: TDateTime);
         function GetFuvarosDatum: string;
         function GetDateTime: TDateTime;
      end;
implementation

// "Fuvaros" d�tum form�tum: 2018.01.04. 00:10:31 (vagy lehet m�sodperc n�lk�l is)
class function TFuvarosDatum.FuvarosDatumToDateTime(const AFuvarosDatum: string): TDateTime;
var
  DatumS, IdoS: string;
begin
   DatumS:= copy(AFuvarosDatum,1,11);
   IdoS:= copy(AFuvarosDatum,13,8);
   Result:= DateTimeStrToDateTime(DatumS, IdoS);
end;

class function TFuvarosDatum.DateTimeToFuvarosDatum(const ADT: TDateTime): string;
begin
   Result:= FormatDateTime('yyyy.mm.dd. HH:mm:ss', ADT);
end;

procedure TFuvarosDatum.SetDateTime(const ADT: TDateTime);
begin
   FDateTimeValue:= ADT;
end;

function TFuvarosDatum.GetDateTime: TDateTime;
begin
   Result:= FDateTimeValue;
end;

procedure TFuvarosDatum.SetFuvarosDatum(const AFuvarosDatum: string);
begin
   FDateTimeValue:= FuvarosDatumToDateTime(AFuvarosDatum);
end;

function TFuvarosDatum.GetFuvarosDatum: string;
var
  DatumS, IdoS: string;
begin
   Result:= DateTimeToFuvarosDatum(FDateTimeValue);
end;

end.

