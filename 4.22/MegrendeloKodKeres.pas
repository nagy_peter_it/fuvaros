unit MegrendeloKodKeres;

interface

uses
	SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
	Forms, Dialogs, StdCtrls, Buttons, Mask, DB, DBTables, J_ALFORM, ADODB;

type
  TMegrendeloRecord = record
      ID: integer;
      KOD: string;
      REF: string;
      end;  // record

type
	TMegrendeloKodKeresDlg = class(TJ_AlformDlg)
	BitElkuld: TBitBtn;
	BitKilep: TBitBtn;
	 Query1: TADOQuery;
	 Query2: TADOQuery;
    Label8: TLabel;
    cbTerulet: TComboBox;
    Label1: TLabel;
    meKod: TMaskEdit;
    Label2: TLabel;
    meRef: TMaskEdit;
    M1: TMaskEdit;
    chkWebshop: TCheckBox;
	 procedure BitKilepClick(Sender: TObject);
	 procedure FormCreate(Sender: TObject);
	 procedure BitElkuldClick(Sender: TObject);
	 procedure MaskEditClick(Sender: TObject);
   procedure FormDestroy(Sender: TObject);
   function GetTeruletElveszettKod(TeruletKod: string): TMegrendeloRecord;
   function GetUjMegrendeloSorszam(TeruletKod, Datum: string): integer;
   procedure SzotarTolt_Egyeb1(Combo : TCustomComboBox; alkod : string; kodlist : TStringList);
   // function ReferenciaMentes: boolean;
	private
    lista_terulet: TStringList;
	public
	end;

var
	MegrendeloKodKeresDlg: TMegrendeloKodKeresDlg;

implementation

uses
 Egyeb, J_SQL, Kozos, Clipbrd, MegrendeloKodMagyaraz;

{$R *.DFM}


function TMegrendeloKodKeresDlg.GetTeruletElveszettKod(TeruletKod: string): TMegrendeloRecord;
var
  S, MKOD, REF, KeresKonyvtar, FileName: string;
  Talalt: boolean;
begin
   KeresKonyvtar:= EgyebDlg.Read_SZGrid('999', '760');
   S:= 'select MR_ID, MR_DATUM, MR_TERULET, MR_SORSZAM, MR_REF from MEGRENDELO where MR_NEMHASZNALT=0 and MR_WEBSHOP=0 and MR_TERULET='''+TeruletKod+'''';
   S:= S+' and MR_KINEK = '+EgyebDlg.user_code;
   if Query_Run(Query2, S) then begin
    Talalt:= False;
    Result.KOD:= '';
		while not Query2.EOF and not Talalt do begin
			MKOD:= FormatMegrendeloKod(Query2.FieldByname('MR_DATUM').AsString, Query2.FieldByname('MR_TERULET').AsString, Query2.FieldByname('MR_SORSZAM').AsInteger);
      REF:= Query2.FieldByname('MR_REF').AsString;
			FileName:= KeresKonyvtar+'\'+MKOD+'.pdf';
      if not FileExists(FileName) then begin
        Talalt:= True;
        Result.ID:= Query2.FieldByname('MR_ID').AsInteger;
        Result.KOD:= MKOD;
        Result.REF:= REF;
        end;  // if
			Query2.Next;
      end;  // while
		end;  // if
end;

function TMegrendeloKodKeresDlg.GetUjMegrendeloSorszam(TeruletKod, Datum: string): integer;
var
  S, MyResult: string;
begin
   S:= 'select max(MR_SORSZAM) from MEGRENDELO ';
   S:=S+ ' where MR_TERULET='''+TeruletKod+''' AND MR_DATUM='''+Datum+'''';
   Query_Run(Query2, S);
   if Query2.Fields[0].IsNull then begin  // erre a napra m�g nem volt
       Result:=1;
       end
   else begin
      Result:=Query2.Fields[0].AsInteger +1;
      end;
end;

procedure TMegrendeloKodKeresDlg.BitKilepClick(Sender: TObject);
begin
  // if ReferenciaMentes then begin
  ret_kod := '';
  Close;
end;



procedure TMegrendeloKodKeresDlg.FormCreate(Sender: TObject);
begin
	EgyebDlg.SeTADOQueryDatabase(Query1);
	EgyebDlg.SeTADOQueryDatabase(Query2);
  lista_terulet:= TStringList.Create;
  // SzotarTolt(cbTERULET, '148' , lista_terulet, false, false );
  SzotarTolt_Egyeb1(cbTERULET, '148' , lista_terulet); // megsz�ri az SZ_EGYEB1 �rt�ke alapj�n
	ret_kod:= '';
end;

procedure TMegrendeloKodKeresDlg.FormDestroy(Sender: TObject);
begin
  if lista_terulet <> nil then lista_terulet.Free;
end;

procedure TMegrendeloKodKeresDlg.BitElkuldClick(Sender: TObject);
var
  UjKodSorszam, UjRKID, ElveszettID: integer;
  TeruletKod, S, UjKod, ElveszettKod, ElveszettRef: string;
  MegrendeloRecord: TMegrendeloRecord;
begin
	if cbTerulet.Text = '' then begin
		NoticeKi('Ter�let megad�sa k�telez�!');
		cbTerulet.Setfocus;
		Exit;
	  end;
	if meRef.Text = '' then begin
		NoticeKi('A megrendel�s t�rgy�nak megad�sa k�telez�!');
		meRef.Setfocus;
		Exit;
	  end;
  TeruletKod:= lista_terulet[cbTerulet.ItemIndex];
  MegrendeloRecord:= GetTeruletElveszettKod(TeruletKod);
  ElveszettID:= MegrendeloRecord.ID;
  ElveszettKod:= MegrendeloRecord.Kod;
  ElveszettRef:= MegrendeloRecord.Ref;
  if ElveszettKod = '' then begin
    UjKodSorszam:= GetUjMegrendeloSorszam(TeruletKod, EgyebDlg.MaiDatum);
    UjRKID:= Query_Insert_Identity (Query1, 'MEGRENDELO',
        [
        'MR_TERULET',  ''''+TeruletKod+'''',
        'MR_DATUM', ''''+EgyebDlg.MaiDatum+'''',
        'MR_SORSZAM', IntToStr(UjKodSorszam),
        'MR_REF',  ''''+meRef.Text+'''',
        'MR_NEMHASZNALT', '0',
        'MR_WEBSHOP', BoolToIntStr(chkWebshop.Checked),
        'MR_KINEK', ''''+EgyebDlg.user_code+'''',
        'MR_KIADI', ''''+formatdatetime('yyyy.mm.dd. HH:mm:ss', Now)+''''
        ], 'MR_ID' );
       {if MR_ID>=0 then begin  // ha nem volt sikeres, �gyis kap SQL hiba ablakot
        ret_kod:= IntToStr(UjRKID);
        M1.Text:= ret_kod;
        end;}
    UjKod:= FormatMegrendeloKod(EgyebDlg.MaiDatum, TeruletKod, UjKodSorszam);
    meKod.Text:= UjKod;
    M1.Text:= IntToStr(UjRKID);
    Clipboard.Clear;
	  Clipboard.AsText:= UjKod;
	  end // minden rendben
  else begin
    S:= 'Fel nem haszn�lt megrendel� k�dot tal�ltam: '+ElveszettKod+'(el�z�leg "'+ElveszettRef+'").';
    S:= S+' Szeretn�d ezt haszn�lni?';
    if NoticeKi(S, NOT_QUESTION) = 0 then begin   // Igen, haszn�ln�
      meKod.Text:= ElveszettKod;
      M1.Text:= IntToStr(ElveszettID);
      // meRef.Text:= ElveszettRef; a most be�rt referencia maradjon
      Query_Update (Query1, 'MEGRENDELO',
        [
        'MR_REF',  ''''+meRef.Text+''''
        ], ' WHERE MR_ID='+M1.Text);
      Clipboard.Clear;
	    Clipboard.AsText:= ElveszettKod;
      end // if
    else begin  // nem haszn�lja, meg kell magyar�zni hogy mi volt az
      Application.CreateForm(TMegrendeloKodMagyarazDlg, MegrendeloKodMagyarazDlg);
      MegrendeloKodMagyarazDlg.Tolto('', IntToStr(ElveszettID));
      MegrendeloKodMagyarazDlg.ShowModal;
      MegrendeloKodMagyarazDlg.Free;
      end;
    end; // else = volt elveszett k�d
end;

{
function TMegrendeloKodKeresDlg.ReferenciaMentes: boolean;
var
  S: string;
begin
	if meRef.Text = '' then begin
		NoticeKi('�ru, szolg�ltat�s megad�sa k�telez�!');
		meRef.Setfocus;
		Result:= False;
	  end
  else begin
    if Query_Update (Query1, 'MEGRENDELO',
        [
        'MR_REF',  ''''+meRef.Text+''''
        ], ' WHERE MR_ID='+M1.Text) then
        Result:=True
    else begin
       Result:= False;  // nem siker�lt az update
       NoticeKi('A ment�s nem siker�lt!');
       end;
	  end
end;
}

procedure TMegrendeloKodKeresDlg.MaskEditClick(Sender: TObject);
begin
	with Sender as TMaskEdit do begin
  	SelectAll;
  end;
end;


procedure TMegrendeloKodKeresDlg.SzotarTolt_Egyeb1(Combo : TCustomComboBox; alkod : string; kodlist : TStringList);
var
  S: string;
begin
	Combo.Clear;
	kodlist.Clear;
  S:= 'SELECT SZ_ALKOD, SZ_MENEV, SZ_ERVNY FROM SZOTAR WHERE SZ_FOKOD='''+alkod+''' '+
		' AND SZ_ERVNY = 1 '+
    ' and (SZ_EGYEB1 & POWER(2,0)) > 0 '+   // 0. bit: jelenjen meg ebben az ablakban
    ' ORDER BY SZ_MENEV';

	if Query_Run(Query2, S) then begin
		while not Query2.EOF do begin
			Combo.Items.Add(Query2.FieldByName('SZ_MENEV').AsString);
			kodlist.Add(Query2.FieldByName('SZ_ALKOD').AsString);
			Query2.Next;
	   	end;  // while
		if Combo.Items.Count > 0 then begin
			Combo.ItemIndex	:= 0;
		  end; // if
  	end; // if
end;



end.




