﻿unit KoltsegImport2;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Grids, Vcl.Buttons,
  Vcl.Mask, Vcl.ExtCtrls, StrUtils, Data.DB, Data.Win.ADODB;

type
  TKoltsegImport2Dlg = class(TForm)
    GroupBox2: TGroupBox;
    Label1: TLabel;
    CB1: TComboBox;
    Label2: TLabel;
    M0: TMaskEdit;
    BitBtn1: TBitBtn;
    SG1: TStringGrid;
    Memo1: TMemo;
    BitBtn2: TBitBtn;
    BitKilep: TBitBtn;
    OpenDialog1: TOpenDialog;
    Label3: TLabel;
    QueryAl: TADOQuery;
    QueryAl2: TADOQuery;
    QRsz: TADOQuery;
    chkUjDEAutopalya: TCheckBox;
    procedure BitKilepClick(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    function  GetMezoSorszam( meznev : string; hibamutat: boolean = False) : integer;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure OlvasasEtoll;
    procedure OlvasasHugo;
    procedure OlvasasAutoDe;
    procedure OlvasasShell;
    procedure OlvasasAutoAT;
    procedure OlvasasBp;
    procedure Elokeszito;
    procedure KoltsegGyujtoBeiro( fimod, megje, ktipstr : string );
    function  GetDatum(dstr : string) : string;
    function  GetBeodat( fimod : string ) : string;
    procedure MezonevKorrigalo;
    function  VanRendszam( rsz : string) : boolean;
    procedure NevCSere(SGbe : TStringGrid ; mirol, mire : string);
  private
    dir        : string;
    fnev       : string;
    rec_szam   : integer;
    mezolista	: TStringList;
    mezonevek	: TStringList;
    leiras     : string;
    valnem     : string;
    ret_kod    : integer;
    fname      : string;
    rsz        : integer;
    F          : Textfile;
    rendsz     : string;
    datum      : string;
    ido        : string;
    osszeg     : double;
    km         : double;
    menny      : double;
    sor        : string;
    orsz       : string;
    tipus      : string;
    sorsz      : integer;
  public

  end;

var
  KoltsegImport2Dlg: TKoltsegImport2Dlg;
  // FejlecIndex: TDictionary<string,integer>;
implementation

{$R *.dfm}

uses
   Kozos, Egyeb, J_Sql, LGauge, J_Excel, J_Export;

procedure TKoltsegImport2Dlg.FormCreate(Sender: TObject);
begin
	EgyebDlg.SeTADOQueryDatabase(QueryAl);
	EgyebDlg.SeTADOQueryDatabase(QRsz);
	EgyebDlg.SeTADOQueryDatabase(QueryAl2);
   mezolista	    := TStringList.Create;
   mezonevek	    := TStringList.Create;
   CB1.ItemIndex   := 5;
   SetMaskEdits([M0]);
   Query_Run(QRsz, 'SELECT GK_RESZ FROM GEPKOCSI ORDER BY 1 ');
   // FejlecIndex:= TDictionary<string,integer>.Create;
end;

procedure TKoltsegImport2Dlg.BitBtn1Click(Sender: TObject);
begin
   // A könyvtár beolvasása
   OpenLocalIni(1);
   case CB1.Itemindex of
       0:  dir	:= ReadLocalIni('DIRECTORIES', 'DIR_AUTODE', '');       // Autópálya DE
       1:  dir	:= ReadLocalIni('DIRECTORIES', 'DIR_SHELL', '');        // SHELL
       2:  dir	:= ReadLocalIni('DIRECTORIES', 'DIR_EUROTOLL', '');     // EUROTOLL
       3:  dir	:= ReadLocalIni('DIRECTORIES', 'DIR_HUGO', '');         // HU-GO
       4:  dir	:= ReadLocalIni('DIRECTORIES', 'DIR_AUTOAT', '');       // AT autópálya
       5:  dir	:= ReadLocalIni('DIRECTORIES', 'DIR_BP', '');           // BP
   end;
   CloseLocalIni;
   OpenDialog1.InitialDir  := dir;
   if OpenDialog1.Execute then begin
       dir     := ExtractFilePath( OpenDialog1.FileName);
       M0.Text := ExtractFileName( OpenDialog1.FileName);
       // A könyvtár elmentése
       OpenLocalIni(1);
       case CB1.Itemindex of
           0: WriteLocalIni('DIRECTORIES', 'DIR_AUTODE', dir);         // Autópálya DE
           1: WriteLocalIni('DIRECTORIES', 'DIR_SHELL', dir);          // SHELL
           2: WriteLocalIni('DIRECTORIES', 'DIR_EUROTOLL', dir);       // EUROTOLL
           3: WriteLocalIni('DIRECTORIES', 'DIR_HUGO', dir);           // HU-GO
           4: WriteLocalIni('DIRECTORIES', 'DIR_AUTOAT', dir);         // AT autópálya
           5: WriteLocalIni('DIRECTORIES', 'DIR_BP', dir);             // BP
       end;
       CloseLocalIni;
   end else begin
       dir     := '';
       M0.Text := '';
   end;
end;

procedure TKoltsegImport2Dlg.BitBtn2Click(Sender: TObject);
begin
   if M0.Text = '' then begin
       NoticeKi('Hiányzó bemeneti állomány!');
       Exit;
   end;
   // Ellenőrizzük, hogy az állomány beolvasásra került-e már.
   Query_Run(QueryAl, 'SELECT KS_IMPFN FROM KOLTSEG_GYUJTO WHERE KS_IMPFN = '''+M0.Text+''' ');
   if QueryAl.Recordcount > 0 then begin
       NoticeKi('Ez az állomány már importálásra került.');
       Exit;
   end;
   fnev            := dir+'hiba_'+ M0.Text;
   rec_szam        := Sorszamolo(dir+M0.Text);
   if rec_szam > 0 then begin
       Screen.Cursor       := crHourGlass;
       BitBtn2.Hide;                   // A végrehajtás gombot örökre letiltjuk
       BitBtn1.Enabled     := false;
       BitKilep.Enabled    := false;
       CB1.Enabled         := false;
       case CB1.Itemindex of
           0:  OlvasasAutoDE;      // Autópálya DE
           1:  OlvasasShell;       // SHELL
           2:  OlvasasEtoll;       // EUROTOLL
           3:  OlvasasHugo;        // HU_GO
           4:  OlvasasAutoAT;      // AT autópálya
           5:  OlvasasBp;          // Bp
       end;
       BitKilep.Enabled    := true;
       Screen.Cursor:=crDefault;
   end else begin
       NoticeKi('Az állomány megnyitása nem sikerült! Lehet hogy használatban van!');
   end;
end;

procedure TKoltsegImport2Dlg.BitKilepClick(Sender: TObject);
begin
   Close;
end;

procedure TKoltsegImport2Dlg.OlvasasEtoll;
var
   ertek       : double;
	EX		    : TJExcel;
   fn          : string;
   Exdlg       : TExportDlg;
begin
   // Ha excel táblát kapunk akkor előszőr átalakítjuk txt-re
   if Pos('.xls', M0.Text) > 0 then begin
       // Megnyitás, beolvasás, kiírás
       EX 	:= TJexcel.Create(Self);
       if not EX.OpenXlsFile(dir + M0.Text) then begin
           NoticeKi('Nem sikerült megnyitnom az Excel állományt!');
           EX.Free;  // nem kell már
           Exit;
       end;
       SG1.Visible         := false;
       if not EX.ReadFullXls(SG1) then begin
           NoticeKi('Nem sikerült beolvasnom az Excel állomány 2. lapját!');
           EX.Free;  // nem kell már
           Exit;
       end;
       // Az oszlopnevek cseréje a csv beolvasáshoz
       NevCSere(SG1, 'Pays', 'Pays immatriculation');
       NevCSere(SG1, 'Période', 'Horodatage de sortie');
       // ------- 2018.04.09.  Adri szerint egy ideje más az adattartalom, a Montant TTC mezőben van a nettó összeg
       // NevCSere(SG1, 'Montant HT', 'Euros HT');
       NevCSere(SG1, 'Montant TTC', 'Euros HT');
       NevCSere(SG1, 'Pays de la SCA', 'Pays');
       // Az állomány kiírása
       Application.CreateForm(TExportDlg, Exdlg);
       fn  := copy(M0.Text, 1, Pos('.xls', M0.Text)-1)+'_T.CSV';
       Exdlg.gridexport    := true;
       Exdlg.grid          := SG1;
       Exdlg.elvalaszto    := ';';
	    Exdlg.SilentFile(dir+fn);
	    Exdlg.Destroy;

       EX.Free;  // nem kell már
       M0.Text         := fn;
       SG1.Visible     := true;
       BitBtn2Click(Self);
       Exit;
   end;

   Elokeszito;

   // Az első sor beolvasása -> ez lesz a mezőnevek listája
   ReadLn(F, sor);
   StringParse(sor, ';', mezonevek);
   MezonevKorrigalo;
   Memo1.Lines.Clear;
   Memo1.Lines.Add('Az EUROTOLL adatok feldolgozása elkezdődött ( '+FormatDateTime('YYYY.MM.DD. HH.NN.SS', now)+' )');
   Memo1.Lines.Add('');
   Memo1.Lines.Add('A beolvasott rekordok száma : '+IntToStr(rec_szam));
   if ( ( GetMezoSorszam( 'Immatriculation') < 0 ) or ( GetMezoSorszam( 'Horodatage de sortie' ) < 0 ) or ( GetMezoSorszam( 'Euros HT' ) < 0 ) or ( GetMezoSorszam( 'Pays' ) < 0 ) ) then begin
       NoticeKi('Egy fontos mező hiányzik az állományból : Immatriculation; Horodatage de sortie; Euros HT; Pays');
       Exit;
   end;
   // A táblázat feltöltése
   while not Eof(F) do begin
       Application.ProcessMessages;
       ReadLn(F, sor);
       if copy(sor, 1, 10) = ';;;;;;;;;;' then begin
           Continue;
       end;
       mezolista.Clear;
       StringParse(sor, ';', mezolista);
       // Mezők ellenőrzése
       rendsz  := mezolista[GetMezoSorszam( 'Immatriculation' )];
       rendsz  := Trim(StringReplace(rendsz, 'VIRT', '', [rfReplaceAll]));
       rendsz  := LeftStr(rendsz,3)+'-'+RightStr(rendsz, 3);
       if rendsz = '-' then begin
           EllenListAppend('A rendszám mező üres ! '+mezolista[GetMezoSorszam( 'Horodatage de sortie')]+';'+mezolista[GetMezoSorszam('Euros HT')]);
           Continue;
       end;
       if not Vanrendszam(rendsz) then begin
           EllenListAppend('Hiányzó rendszám :'+mezolista[GetMezoSorszam( 'Horodatage de sortie')]+';'+mezolista[GetMezoSorszam('Euros HT')]+';'+rendsz);
           Continue;
       end;
       datum   := copy(mezolista[GetMezoSorszam( 'Horodatage de sortie')],1,10)+'.';
       if not Jodatum(datum) then begin
           EllenListAppend('Érvénytelen dátum ! '+mezolista[GetMezoSorszam( 'Horodatage de sortie' )]+';'+mezolista[GetMezoSorszam('Euros HT')]);
           Continue;
       end;
       ido     := Trim(copy(mezolista[GetMezoSorszam( 'Horodatage de sortie')],12,6));
       if not Joido(ido) then begin
           ido := '';
       end;
       orsz  := Trim(mezolista[GetMezoSorszam('Pays')]);
       if orsz='ESPAGNE'      then orsz:='ES' else
       if orsz='FRANCE'       then orsz:='FR' else
       if orsz='ALLEMAGNE'    then orsz:='DE' else
       if orsz='AUTRICHE'     then orsz:='AT' else
       if orsz='PORTUGAL'     then orsz:='PT' else
                                   orsz:='??';
       valnem              := 'EUR';
       tipus               := '1';
       leiras              := 'Autopálya';
       osszeg              := StringSzam( mezolista[GetMezoSorszam('Euros HT')]);
       SG1.Cells[0,rsz]    := IntToStr(rsz);
       SG1.Cells[1,rsz]    := rendsz;
       SG1.Cells[2,rsz]    := datum;
       SG1.Cells[3,rsz]    := ido;
       SG1.Cells[4,rsz]    := orsz;
       SG1.Cells[5,rsz]    := Format('%.2f', [osszeg]);
       SG1.Cells[7,rsz]    := valnem;
       SG1.Cells[8,rsz]    := tipus;
       SG1.Cells[9,rsz]    := leiras;
       ertek               := 0;
       SG1.Cells[10,rsz]   := Format('%.2f', [ertek]);     // Mennyiség
       SG1.Cells[11,rsz]   := Format('%.2f', [ertek]);     // ÁFA %
       rsz                 := rsz + 1;
   end;
   SG1.RowCount    := rsz;
   CloseFile(f);
   KoltsegGyujtoBeiro('ETOLL', 'Autópálya díj import EUROTOLL', '5');
end;

procedure TKoltsegImport2Dlg.OlvasasHugo;
var
   ertek       : double;
   datumstr    : string;
begin
   Elokeszito;
   // Az első sor beolvasása -> ez lesz a mezőnevek listája
   ReadLn(F, sor);
   StringParse(sor, ';', mezonevek);
   MezonevKorrigalo;
   Memo1.Lines.Clear;
   Memo1.Lines.Add('A HU-GO adatok feldolgozása elkezdődött ( '+FormatDateTime('YYYY.MM.DD. HH.NN.SS', now)+' )');
   Memo1.Lines.Add('');
   Memo1.Lines.Add('A beolvasott rekordok száma : '+IntToStr(rec_szam));
   if ( ( GetMezoSorszam( 'Rendszám') < 0 ) or ( GetMezoSorszam( 'Költség') < 0 ) or ( GetMezoSorszam( 'Használat ideje') < 0 ) ) then begin
       NoticeKi('Egy fontos mező hiányzik az állományból : Rendszám; Költség; Használat ideje');
       Exit;
   end;
   // A táblázat feltöltése
   while not Eof(F) do begin
       Application.ProcessMessages;
       ReadLn(F, sor);
       if copy(sor, 1, 3) <> 'OBU' then begin    // MINDEN SORNAK OBU -val kell kezdődnie
           Continue;
       end;
       mezolista.Clear;
       StringParse(sor, ';', mezolista);
       // Mezők ellenőrzése
       rendsz  := mezolista[GetMezoSorszam( 'Rendszám')];
       rendsz  := Trim(StringReplace(rendsz, 'VIRT', '', [rfReplaceAll]));
       rendsz  := LeftStr(rendsz,3)+'-'+RightStr(rendsz, 3);
       if rendsz = '-' then begin
           EllenListAppend('A rendszám mező üres ! '+mezolista[GetMezoSorszam( 'Használat ideje')]+';'+mezolista[GetMezoSorszam('Költség')]);
           Continue;
       end;
       if not Vanrendszam(rendsz) then begin
           EllenListAppend('Hiányzó rendszám :'+mezolista[GetMezoSorszam( 'Használat ideje')]+';'+mezolista[GetMezoSorszam('Költség')]+';'+rendsz);
           Continue;
       end;
       datumstr    := mezolista[GetMezoSorszam( 'Használat ideje')];
       if copy(datumstr, 1, 1) = '"' then begin
           datumstr    := copy(datumstr, 2, Length(datumstr)-2);
       end;
       datum       := copy(datumstr,1,10)+'.';
       if not Jodatum(datum) then begin
           EllenListAppend('Érvénytelen dátum ! '+mezolista[GetMezoSorszam( 'Használat ideje')]+';'+mezolista[GetMezoSorszam('Költség')]);
           Continue;
       end;
       ido     := Trim(copy(mezolista[GetMezoSorszam( 'Használat ideje')],12,6));
       if not Joido(ido) then begin
           ido := '';
       end;
       orsz                := 'HU';
       valnem              := 'HUF';
       tipus               := '1';
       leiras              := 'HU-GO';
       osszeg              := StringSzam( mezolista[GetMezoSorszam('Költség')]);
       if osszeg = 0 then begin
           EllenListAppend('Összeg = 0 !'+mezolista[GetMezoSorszam( 'Használat ideje')]+';'+rendsz);
           Continue;
       end;
       SG1.Cells[0,rsz]    := IntToStr(rsz);
       SG1.Cells[1,rsz]    := rendsz;
       SG1.Cells[2,rsz]    := datum;
       SG1.Cells[3,rsz]    := ido;
       SG1.Cells[4,rsz]    := orsz;
       SG1.Cells[5,rsz]    := Format('%.2f', [osszeg/1.27]);       // A nettó összeget tároljuk
       SG1.Cells[7,rsz]    := valnem;
       SG1.Cells[8,rsz]    := tipus;
       SG1.Cells[9,rsz]    := leiras;
       ertek               := 0;
       SG1.Cells[10,rsz]   := Format('%.2f', [ertek]);     // Mennyiség
       SG1.Cells[11,rsz]   := Format('%.2f', [ertek]);     // ÁFA %
       rsz                 := rsz + 1;
   end;
   SG1.RowCount    := rsz;
   CloseFile(f);
   KoltsegGyujtoBeiro ('HU-GO', 'HU-GO import', '7');
end;

procedure TKoltsegImport2Dlg.OlvasasAutoAT;
var
   ertek           : double;
   datumstr        : string;
   rendszam_str    : string;
   koltseg_str     : string;
   datumido_str    : string;
begin
   Elokeszito;
   // Az első sor beolvasása -> ez lesz a mezőnevek listája
   ReadLn(F, sor);
   StringParse(sor, ';', mezonevek);
   MezonevKorrigalo;
   Memo1.Lines.Clear;
   Memo1.Lines.Add('Az AT autópálya adatok feldolgozása elkezdődött ( '+FormatDateTime('YYYY.MM.DD. HH.NN.SS', now)+' )');
   Memo1.Lines.Add('');
   rendszam_str    := 'Kfz-Kennzeichen';
   koltseg_str     := 'Netto';
   datumido_str    := 'Datum / Zeit';

   Memo1.Lines.Add('A beolvasott rekordok száma : '+IntToStr(rec_szam));
   if ( ( GetMezoSorszam( rendszam_str ) < 0 ) or ( GetMezoSorszam( koltseg_str ) < 0 ) or ( GetMezoSorszam( datumido_str ) < 0 ) ) then begin
       NoticeKi('Egy fontos mező hiányzik az állományból : '+rendszam_str+'; '+koltseg_str+'; '+datumido_str);
       Exit;
   end;
   // A táblázat feltöltése
   while not Eof(F) do begin
       Application.ProcessMessages;
       ReadLn(F, sor);
       if copy(sor, 1, 5) = 'Datum' then begin    // ismétlik néha a fejlécet !!!!!
           Continue;
       end;
       mezolista.Clear;
       StringParse(sor, ';', mezolista);
       // Mezők ellenőrzése
       rendsz  := mezolista[GetMezoSorszam( rendszam_str )];
       rendsz  := Trim(StringReplace(rendsz, 'VIRT', '', [rfReplaceAll]));
       rendsz  := LeftStr(rendsz,3)+'-'+RightStr(rendsz, 3);
       if rendsz = '-' then begin
           EllenListAppend('A rendszám mező üres ! '+mezolista[GetMezoSorszam( datumido_str )]+';'+mezolista[GetMezoSorszam(koltseg_str)]);
           Continue;
       end;
       if not Vanrendszam(rendsz) then begin
           EllenListAppend('Hiányzó rendszám :'+mezolista[GetMezoSorszam( datumido_str )]+';'+mezolista[GetMezoSorszam(koltseg_str)]+';'+rendsz);
           Continue;
       end;
       datumstr    := mezolista[GetMezoSorszam( datumido_str )];
       datum       := copy(datumstr,7,4)+'.'+copy(datumstr,4,2)+'.'+copy(datumstr,1,2)+'.';
       if not Jodatum(datum) then begin
           EllenListAppend('Érvénytelen dátum ! '+mezolista[GetMezoSorszam( datumido_str )]+';'+mezolista[GetMezoSorszam('Költség')]);
           Continue;
       end;
       ido     := Trim(copy(mezolista[GetMezoSorszam( datumido_str )],12,8));
       if not Joido(ido) then begin
           ido := '';
       end;
       ido     := copy(ido, 1, 5);         // Le kell vágnunk, mert az adatbázisban csak 5 van
       orsz                := 'AT';
       valnem              := 'EUR';
       tipus               := '1';
       leiras              := 'Autópálya';
       osszeg              := StringSzam( mezolista[GetMezoSorszam(koltseg_str)]);
       if osszeg = 0 then begin
           EllenListAppend('Összeg = 0 !'+mezolista[GetMezoSorszam( datumido_str )]+';'+rendsz);
           Continue;
       end;
       SG1.Cells[0,rsz]    := IntToStr(rsz);
       SG1.Cells[1,rsz]    := rendsz;
       SG1.Cells[2,rsz]    := datum;
       SG1.Cells[3,rsz]    := ido;
       SG1.Cells[4,rsz]    := orsz;
       SG1.Cells[5,rsz]    := Format('%.2f', [osszeg]);       // A nettó összeget tároljuk
       SG1.Cells[7,rsz]    := valnem;
       SG1.Cells[8,rsz]    := tipus;
       SG1.Cells[9,rsz]    := leiras;
       ertek               := 0;
       SG1.Cells[10,rsz]   := Format('%.2f', [ertek]);     // Mennyiség
       SG1.Cells[11,rsz]   := Format('%.2f', [ertek]);     // ÁFA %
       rsz                 := rsz + 1;
   end;
   SG1.RowCount    := rsz;
   CloseFile(f);
   KoltsegGyujtoBeiro ('ASFIN', 'Autópálya díj import AT', '2');
end;

procedure TKoltsegImport2Dlg.OlvasasAutoDe;
var
   ertek   : double;
   KmOszlop, OsszegOszlop: integer;
begin
   if chkUjDEAutopalya.Checked then begin
      KmOszlop:= 16;
      OsszegOszlop:= 17;
      end
   else begin
      KmOszlop:= 19;
      OsszegOszlop:= 20;
      end;
   Elokeszito;
   // Az első sor beolvasása -> ez lesz a mezőnevek listája
   ReadLn(F, sor);
   StringParse(sor, ';', mezonevek);
   MezonevKorrigalo;
   Memo1.Lines.Clear;
   Memo1.Lines.Add('Az Autópálya DE adatok feldolgozása elkezdődött ( '+FormatDateTime('YYYY.MM.DD. HH.NN.SS', now)+' )');
   Memo1.Lines.Add('');
   Memo1.Lines.Add('A beolvasott rekordok száma : '+IntToStr(rec_szam));
   // Az állomány olvasása, a táblázat feltöltése
   while not Eof(F) do begin
       Application.ProcessMessages;
       ReadLn(F, sor);
       mezolista.Clear;
       StringParse(sor, ';', mezolista);
       if ( ( mezolista.Count < 5 ) or ( Length(sor) < 30  ) ) then begin
           Continue;
       end;
       // Mezők ellenőrzése
       rendsz  := mezolista[1];
       rendsz  := Trim(StringReplace(rendsz, 'VIRT', '', [rfReplaceAll]));
       rendsz  := LeftStr(rendsz,3)+'-'+RightStr(rendsz, 3);
       if rendsz = '-' then begin
           EllenListAppend('A rendszám mező üres ! '+mezolista[3]+';'+mezolista[2]);
           Continue;
       end;
       if not Vanrendszam(rendsz) then begin
           EllenListAppend('Hiányzó rendszám :'+mezolista[3]+';'+mezolista[2]+';'+rendsz);
           Continue;
       end;
       datum   := GetDatum(mezolista[2]);
       if not Jodatum(datum) then begin
           EllenListAppend('Érvénytelen dátum ! '+mezolista[3]+';'+mezolista[2]);
           Continue;
       end;
       ido                 := RightStr('0000'+mezolista[3], 5);
       orsz                := 'DE';
       valnem              := 'EUR';
       tipus               := '1';
       osszeg              := StringSzam( mezolista[OsszegOszlop]);
       leiras              := 'Autópálya';
       km                  := StringSzam( mezolista[KmOszlop]);
       SG1.Cells[0,rsz]    := IntToStr(rsz);
       SG1.Cells[1,rsz]    := rendsz;
       SG1.Cells[2,rsz]    := datum;
       SG1.Cells[3,rsz]    := ido;
       SG1.Cells[4,rsz]    := orsz;
       SG1.Cells[5,rsz]    := Format('%.2f', [osszeg]);
       SG1.Cells[6,rsz]    := Format('%.2f', [km]);
       SG1.Cells[7,rsz]    := valnem;
       SG1.Cells[8,rsz]    := tipus;
       SG1.Cells[9,rsz]    := leiras;
       ertek               := 0;
       SG1.Cells[10,rsz]   := Format('%.2f', [ertek]);     // Mennyiség
       SG1.Cells[11,rsz]   := Format('%.2f', [ertek]);     // ÁFA %
       rsz                 := rsz + 1;
   end;
   SG1.RowCount    := rsz;
   CloseFile(f);
   KoltsegGyujtoBeiro('AUTDE', 'Autópálya díj import DE', '0');
end;

procedure TKoltsegImport2Dlg.OlvasasShell;
var
   i           : integer;
   termkod     : string;
	EX		    : TJExcel;
   fn          : string;
   Exdlg       : TExportDlg;
   idoert      : double;
   idostr      : string;
   idosec      : integer;
   RendszamOszlop, OrszagOszlop, VallalatOszlop, DatumOszlop, IdoOszlop, ToltoAllomasOszlop: integer;
   PenznemOszlop, TermekOszlop, KilometerOszlop, NettoOsszegOszlop, MennyisegOszlop, AFAOszlop: integer;
   Sorszam: integer;

begin
   // Ha excel táblát kapunk akkor előszőr átalakítjuk txt-re
   if Pos('.xls', M0.Text) > 0 then begin
       // Megnyitás, beolvasás, kiírás
       EX 	:= TJexcel.Create(Self);
       if not EX.OpenXlsFile(dir + M0.Text) then begin
           NoticeKi('Nem sikerült megnyitnom az Excel állományt!');
           EX.Free;  // nem kell már
           Exit;
       end;
       SG1.Visible         := false;
       if not EX.ReadFullXls(SG1) then begin
           NoticeKi('Nem sikerült beolvasnom az Excel állomány 2. lapját!');
           EX.Free;  // nem kell már
           Exit;
       end;
       // Az állomány kiírása
       Application.CreateForm(TExportDlg, Exdlg);
       fn  := copy(M0.Text, 1, Pos('.xls', M0.Text)-1)+'_T.CSV';
       Exdlg.gridexport    := true;
       Exdlg.grid          := SG1;
       // Az időpontok átírása
       for i := 1 to SG1.RowCount - 1 do begin
  	        idoert  := StringSzam(SG1.Cells[6, i]);
           idostr  := '';
           if idoert > 0 then begin
               idosec          := Trunc( idoert * 24 *3600 ) ;
               idostr          := Format('%2.2d:',[ idosec div 3600 ]);
               idosec          := idosec - ( idosec div 3600 )*3600;
               idostr          := idostr + Format('%2.2d',[ idosec div 60]);
           end;
           SG1.Cells[6, i] := idostr;
       end;

       Exdlg.elvalaszto    := ';';
	    Exdlg.SilentFile(dir+fn);
	    Exdlg.Destroy;

       EX.Free;  // nem kell már
       M0.Text         := fn;
       SG1.Visible     := true;
       BitBtn2Click(Self);
       Exit;
   end;

   Elokeszito;
   // Az első sor beolvasása -> ez lesz a mezőnevek listája
   ReadLn(F, sor);
   StringParse(sor, ';', mezonevek);
   MezonevKorrigalo;
   // a szótárban tárolt fejléc szövegek alapján megkeressük az oszlop számát a beolvasott állományban
   RendszamOszlop:= GetMezoSorszam (EgyebDlg.Read_SZGrid('472','Rendszam'), True); // 16
   OrszagOszlop:= GetMezoSorszam (EgyebDlg.Read_SZGrid('472','Orszag'), True); // 2
   VallalatOszlop:= GetMezoSorszam (EgyebDlg.Read_SZGrid('472','Vallalat'), True); // 3
   DatumOszlop:= GetMezoSorszam (EgyebDlg.Read_SZGrid('472','Datum'), True); // 5
   IdoOszlop:= GetMezoSorszam (EgyebDlg.Read_SZGrid('472','Ido'), True); // 6
   ToltoAllomasOszlop:= GetMezoSorszam (EgyebDlg.Read_SZGrid('472','ToltoAllomas'), True); // 4
   PenznemOszlop:= GetMezoSorszam (EgyebDlg.Read_SZGrid('472','Penznem'), True); // 37
   TermekOszlop:= GetMezoSorszam (EgyebDlg.Read_SZGrid('472','Termek'), True); // 19
   KilometerOszlop:= GetMezoSorszam (EgyebDlg.Read_SZGrid('472','Kilometer'), True); // 17
   NettoOsszegOszlop:= GetMezoSorszam (EgyebDlg.Read_SZGrid('472','NettoOsszeg'), True); // 41
   MennyisegOszlop:= GetMezoSorszam (EgyebDlg.Read_SZGrid('472','Mennyiseg'), True); // 20
   AFAOszlop:= GetMezoSorszam (EgyebDlg.Read_SZGrid('472','AFA'), True); // 25

   if (RendszamOszlop = -1) or (OrszagOszlop = -1) or (VallalatOszlop = -1) or (DatumOszlop = -1)
    or (IdoOszlop = -1) or (ToltoAllomasOszlop = -1) or (PenznemOszlop = -1) or (TermekOszlop = -1)
    or (KilometerOszlop = -1) or (NettoOsszegOszlop = -1) or (MennyisegOszlop = -1) or (AFAOszlop = -1) then begin
        Exit; // function
        end;

   Memo1.Lines.Clear;
   Memo1.Lines.Add('SHELL adatok feldolgozása elkezdődött ( '+FormatDateTime('YYYY.MM.DD. HH.NN.SS', now)+' )');
   Memo1.Lines.Add('');
   Memo1.Lines.Add('A beolvasott rekordok száma : '+IntToStr(rec_szam));
   // Az állomány olvasása, a táblázat feltöltése
   Sorszam:=1; // az első adatsor a 2. lesz
   while not Eof(F) do begin
       Application.ProcessMessages;
       ReadLn(F, sor);
       Inc(Sorszam);
       mezolista.Clear;
       StringParse(sor, ';', mezolista);
       for i := 0 to mezolista.Count - 1 do begin
           if copy(mezolista[i], 1, 1) = '"' then begin
               mezolista[i] := copy(mezolista[i], 2, Length(mezolista[i])-2);
           end;
       end;
       // Mezők ellenőrzése
       // rendsz  := mezolista[16];
       rendsz  := mezolista[RendszamOszlop];
       rendsz  := Trim(StringReplace(rendsz, 'VIRT', '', [rfReplaceAll]));
       rendsz  := LeftStr(rendsz,3)+'-'+RightStr(rendsz, 3);
       if rendsz = '-' then begin
           // EllenListAppend('A rendszám mező üres ! '+mezolista[3]+';'+mezolista[2]);
           EllenListAppend('A rendszám mező üres (adatsor: '+IntToStr(Sorszam)+')');
           Continue;
       end;
       if not Vanrendszam(rendsz) then begin
           // EllenListAppend('Hiányzó rendszám :'+mezolista[3]+';'+mezolista[2]+';'+rendsz);
           EllenListAppend('Hiányzó rendszám (adatsor: '+IntToStr(Sorszam)+');'+rendsz);
           Continue;
       end;

       datum   := GetDatum(mezolista[5]);
       if not Jodatum(datum) then begin
           // EllenListAppend('Érvénytelen dátum ! '+mezolista[3]+';'+mezolista[2]);
           EllenListAppend('Érvénytelen dátum (adatsor: '+IntToStr(Sorszam)+')');
           Continue;
       end;
       // ido                 := copy(mezolista[6], 1, 5);
       ido                 := copy(mezolista[IdoOszlop], 1, 5);
       // orsz                := Trim(mezolista[1]);
       orsz                := Trim(mezolista[OrszagOszlop]);
       if orsz='EU' then begin
           if Trim(mezolista[VallalatOszlop])<>'' then begin
               orsz:=Trim(mezolista[VallalatOszlop]);
           end;
       end;
       valnem              := copy(Trim(mezolista[PenznemOszlop]), 1, 3);
       termkod             := Trim(mezolista[TermekOszlop]);
       if Length(termkod)=1 then begin
           termkod         := '00'+termkod;
       end;
       if Length(termkod)=2 then begin
           termkod         := '0'+termkod;
       end;
       tipus               := copy(EgyebDlg.Read_SZGrid( '142', termkod ),1,1);
       leiras              := copy(EgyebDlg.Read_SZGrid( '142', termkod ),3,30);
       if  tipus = '' then begin
           tipus           := '1';
           leiras          := '?'+termkod+'?';
       end;
       osszeg              := StringSzam( mezolista[NettoOsszegOszlop]);
       km                  := StringSzam( mezolista[KilometerOszlop]);
       menny               := StringSzam( mezolista[MennyisegOszlop]);
       SG1.Cells[0,rsz]    := IntToStr(rsz);
       SG1.Cells[1,rsz]    := rendsz;
       SG1.Cells[2,rsz]    := datum;
       SG1.Cells[3,rsz]    := ido;
       SG1.Cells[4,rsz]    := orsz;
       SG1.Cells[5,rsz]    := Format('%.2f', [osszeg]);
       SG1.Cells[6,rsz]    := Format('%.2f', [km]);
       SG1.Cells[7,rsz]    := valnem;
       SG1.Cells[8,rsz]    := tipus;
       SG1.Cells[9,rsz]    := leiras;
       SG1.Cells[10,rsz]   := Format('%.2f', [menny]);     // Mennyiség
       SG1.Cells[11,rsz]   := Format('%.2f', [StringSzam( mezolista[AFAOszlop])]);     // ÁFA %
       rsz                 := rsz + 1;
   end;
   SG1.RowCount    := rsz;
   CloseFile(f);
   KoltsegGyujtoBeiro('SHELL', 'SHELL import', '3');
end;

procedure TKoltsegImport2Dlg.OlvasasBp;
var
   i           : integer;
	EX		    : TJExcel;
   fn          : string;
   Exdlg       : TExportDlg;
begin

   // Ha excel táblát kapunk akkor előszőr átalakítjuk txt-re
   if Pos('.xls', M0.Text) > 0 then begin
       // Megnyitás, beolvasás, kiírás
       EX 	:= TJexcel.Create(Self);
       if not EX.OpenXlsFile(dir + M0.Text) then begin
           NoticeKi('Nem sikerült megnyitnom az Excel állományt!');
           EX.Free;  // nem kell már
           Exit;
       end;
       SG1.Visible         := false;
       if not EX.ReadFullXls(SG1) then begin
           NoticeKi('Nem sikerült beolvasnom az Excel állomány 2. lapját!');
           EX.Free;  // nem kell már
           Exit;
       end;
       // Az állomány kiírása
       Application.CreateForm(TExportDlg, Exdlg);
       fn  := copy(M0.Text, 1, Pos('.xls', M0.Text)-1)+'_T.CSV';
       Exdlg.gridexport    := true;
       Exdlg.grid          := SG1;
       // Az időpontok átírása
       for i := 1 to SG1.RowCount - 1 do begin
           rendsz  := Copy(SG1.Cells[3, i], 1, Pos(' ', SG1.Cells[3, i]) -1);
           SG1.Cells[3, i] := rendsz;
       end;

       Exdlg.elvalaszto    := ';';
	    Exdlg.SilentFile(dir+fn);
	    Exdlg.Destroy;

       EX.Free;  // nem kell már
       M0.Text         := fn;
       SG1.Visible     := true;
       BitBtn2Click(Self);
       Exit;
   end;

   Elokeszito;
   // Az első sor beolvasása -> ez lesz a mezőnevek listája
   ReadLn(F, sor);
   StringParse(sor, ';', mezonevek);
   while ( ( Trim(mezonevek[0]) = '' ) and (not Eof(F)) ) do begin
       ReadLn(F, sor);
       StringParse(sor, ';', mezonevek);
   end;
   MezonevKorrigalo;
   Memo1.Lines.Clear;
   Memo1.Lines.Add('BP adatok feldolgozása elkezdődött ( '+FormatDateTime('YYYY.MM.DD. HH.NN.SS', now)+' )');
   Memo1.Lines.Add('');
   Memo1.Lines.Add('A beolvasott rekordok száma : '+IntToStr(rec_szam));
   // Az állomány olvasása, a táblázat feltöltése
   while not Eof(F) do begin
       Application.ProcessMessages;
       ReadLn(F, sor);
       mezolista.Clear;
       StringParse(sor, ';', mezolista);
       // Mezők ellenőrzése
       rendsz  := mezolista[3];
       if not Vanrendszam(rendsz) then begin
           EllenListAppend('Hiányzó rendszám :'+mezolista[3]+';'+mezolista[2]+';'+rendsz);
           Continue;
       end;

       datum   := GetDatum(copy(mezolista[7], 1, 10));
       if not Jodatum(datum) then begin
           EllenListAppend('Érvénytelen dátum ! '+mezolista[3]+';'+mezolista[2]);
           Continue;
       end;
       ido                 := copy(mezolista[7], 13, 5);
       orsz                := '';
       valnem              := 'EUR';
       if Trim(UpperCase(mezolista[10])) = 'DIESEL' then begin
           tipus   := '0';
           leiras  := 'Üzemanyag költség';
       end else begin
           if UpperCase(Copy(mezolista[10], 1, 7)) = 'AD BLUE' then begin
               tipus   := '2';
               leiras  := 'AD Blue';
           end else begin
               tipus   := '1';         // Egyéb költség
               leiras  := 'Egyéb költség';
           end;
       end;
       osszeg              := StringSzam( mezolista[13]);
       km                  := StringSzam( mezolista[5]);
       menny               := StringSzam( mezolista[11]);
       SG1.Cells[0,rsz]    := IntToStr(rsz);
       SG1.Cells[1,rsz]    := rendsz;
       SG1.Cells[2,rsz]    := datum;
       SG1.Cells[3,rsz]    := ido;
       SG1.Cells[4,rsz]    := orsz;
       SG1.Cells[5,rsz]    := Format('%.2f', [osszeg]);
       SG1.Cells[6,rsz]    := Format('%.2f', [km]);
       SG1.Cells[7,rsz]    := valnem;
       SG1.Cells[8,rsz]    := tipus;
       SG1.Cells[9,rsz]    := leiras;
       SG1.Cells[10,rsz]   := Format('%.2f', [menny]);     // Mennyiség
       SG1.Cells[11,rsz]   := Format('%.0f', [StringSzam( mezolista[14])*100/StringSzam( mezolista[13])]);     // ÁFA %
       SG1.Cells[12,rsz]   := Format('%.2f', [StringSzam( mezolista[14])]);                                    // ÁFA érték
       rsz                 := rsz + 1;
   end;
   SG1.RowCount    := rsz;
   CloseFile(f);
   KoltsegGyujtoBeiro('BP', 'BP import', '8');
end;

procedure TKoltsegImport2Dlg.FormDestroy(Sender: TObject);
begin
   mezolista.Free;
   mezonevek.Free;
   // if FejlecIndex <> nil then FejlecIndex.Free;
end;

function TKoltsegImport2Dlg.GetMezoSorszam( meznev : string; hibamutat: boolean = False) : integer;
var
   msz     : integer;
   felt    : boolean;
begin
   Result  := -1;
   msz     := 0;
   while msz < mezonevek.Count do begin
       felt    := (UpperCase(meznev) = UpperCase(mezonevek[msz]));
       if felt then begin
           Result  := msz;
           msz     := mezonevek.Count;
       end;
       Inc(msz);
   end; // while
   if (Result = -1) and hibamutat then
          NoticeKi('Változott a fejléc sor! Hiányzó mező: '+meznev);
end;

procedure TKoltsegImport2Dlg.MezonevKorrigalo;
var
   msz     : integer;
begin
   for msz := 0 to mezonevek.Count -1 do begin
       if copy(mezonevek[msz], 1, 1) = '"' then begin
           mezonevek[msz] := copy(mezonevek[msz], 2, length(mezonevek[msz])-2)
       end;
       if copy(mezonevek[msz], 1, 1) = '''' then begin
           mezonevek[msz] := copy(mezonevek[msz], 2, length(mezonevek[msz])-2)
       end;
   end;
end;

procedure TKoltsegImport2Dlg.KoltsegGyujtoBeiro( fimod, megje, ktipstr : string );
var
   rsz         : integer;
   sorsz2      : integer;
   regilog     : integer;
   beodat      : string;
   afaert      : double;
begin
   Memo1.Lines.Add('');
   sorsz2  := Memo1.Lines.Add('A feldolgozott rekordok száma : ');
   ret_kod	:= GetNextCode('KOLTSEG_GYUJTO', 'KS_KTKOD', 1, 0);
   FormGaugeDlg    := TFormGaugeDlg.Create(Self);
   FormGaugeDlg.GaugeNyit('Adatok tárolása ... ' , '', true);
   regilog := EgyebDlg.log_level;
   EgyebDlg.log_level  := 0;
   sorsz   := 0;
   beodat  := GetBeodat(fimod);
//   SaveGridToFile(SG1, 'tabla01.csv');
   for rsz := 1 to SG1.RowCount - 1 do begin
       Application.ProcessMessages;
       if FormGaugeDlg.kilepes then begin
           if NoticeKi('Valóban megszakítja a feldolgozási műveletet?', NOT_QUESTION) = 0 then begin
               // Töröljük az eddig felvitt elemeket
               Query_Run(QueryAl, 'DELETE FROM KOLTSEG_GYUJTO WHERE KS_IMPFN = '''+M0.Text+''' ');
               Break;
           end else begin
               FormGaugeDlg.kilepes := false;
           end;
       end;
       if ( ( rsz - 1 ) mod 100 ) = 0 then begin
           Memo1.Lines[sorsz2]  := 'A feldolgozott rekordok száma : '+IntToStr(rsz)+'/'+IntToStr(SG1.RowCount - 1);
              FormGaugeDlg.Pozicio ( ( sorsz * 100 ) div (SG1.RowCount - 1)) ;
       end;
       Inc(sorsz);
       // Ha ki van töltve a 12-es mező, akkor ez kerül az ÁFA értékbe, különben számolunk
       afaert  := StringSzam(SG1.Cells[5,rsz])*StringSzam(SG1.Cells[11,rsz])/100;
       if StringSzam(SG1.Cells[12,rsz]) > 0 then begin
           afaert  := StringSzam(SG1.Cells[12,rsz]);
       end;

       if Query_Insert( QueryAl, 'KOLTSEG_GYUJTO', [
           'KS_KTKOD',   IntToStr( ret_kod),
           'KS_RENDSZ',  ''''+SG1.Cells[1,rsz]+'''',
           'KS_DATUM',   ''''+Copy(SG1.Cells[2,rsz], 1, 11)+'''',
           'KS_IDO',     ''''+Copy(SG1.Cells[3,rsz], 1, 5)+'''',
           'KS_ORSZA',   ''''+Copy(SG1.Cells[4,rsz], 1, 2)+'''',
           'KS_LEIRAS',  ''''+SG1.Cells[9,rsz]+'''',
           'KS_VALNEM',  ''''+Copy(SG1.Cells[7,rsz], 1, 3)+'''',
           'KS_FIMOD',   ''''+fimod+'''',
           'KS_KMORA',   SqlSzamString(StringSzam(SG1.Cells[6,rsz]),12,2),
           'KS_OSSZEG',  SqlSzamString(StringSzam(SG1.Cells[5,rsz]),14,4),
           'KS_UZMENY',  SqlSzamString(StringSzam(SG1.Cells[10,rsz]),12,2),
           'KS_ARFOLY',  SqlSzamString(0,14,4),
           'KS_ERTEK',   SqlSzamString(0,14,4),
           'KS_MEGJ',    ''''+megje+'''',
           'KS_TIPUS',   IntToStr(StrToIntDef( SG1.Cells[8,rsz] , 0)),
           'KS_JAKOD',   ''''+''+'''',
           'KS_JARAT',   ''''+''+'''',
           'KS_TELES',   ''''+'N'+'''',
           'KS_AFASZ',   SqlSzamString(StringSzam(SG1.Cells[11,rsz]),12,2),
           'KS_AFAERT',  SqlSzamString(afaert,12,2),
           'KS_AFAKOD',   ''''+''+'''',
           'KS_KTIP',    ktipstr,
           'KS_IMPFN',   ''''+M0.Text+'''',
           'KS_BEODAT',   ''''+beodat+'''',
           'KS_SOR',     ''''+IntToStr(ret_kod)+SG1.Cells[1,rsz]+SG1.Cells[2,rsz]+SG1.Cells[3,rsz]+'0'+SG1.Cells[5,rsz]+''''                                //
           ]) then begin
           inc(ret_kod);
       end else begin
           // A rossz sql kiírása
           EllenListAppend('A rekord felírás nem sikerült : '+SG1.Cells[1,rsz]+','+SG1.Cells[2,rsz]+','+SG1.Cells[3,rsz]+','+Format('%.2f', [StringSzam(SG1.Cells[5,rsz])]));
       end;
   end;
   EgyebDlg.log_level  := regilog;
   if not FormGaugeDlg.kilepes then begin
       Memo1.Lines[sorsz2]  := 'A feldolgozott rekordok száma : '+IntToStr(rsz-1)+'/'+IntToStr(SG1.RowCount - 1);
   end else begin
       Memo1.Lines[sorsz2]  := 'A műveletet a felhasználó megszakította!';
       BitBtn2.Show;
       BitBtn1.Enabled     := true;
       BitKilep.Enabled    := true;
       CB1.Enabled         := true;
   end;
   FormGaugeDlg.Free;
   if EgyebDlg.Ellenlista.Count > 0 then begin
       if FileExists(fnev) then begin
           DeleteFile(fnev);
       end;
       EgyebDlg.Ellenlista.SaveToFile(fnev);
   end;
   Memo1.Lines.Add('');
   Memo1.Lines.Add('Az adatok feldolgozása befejeződött ( '+FormatDateTime('YYYY.MM.DD. HH.NN.SS', now)+' )')
end;

procedure TKoltsegImport2Dlg.Elokeszito;
begin
    // A műveletek előkészítése
   fname   := dir + M0.Text;
   rsz     := rec_szam;
	AssignFile(F, fname);
	Reset(F);
	mezonevek.Clear;
   // A táblázat kezdetének beállítása
   SG1.ColCount        := 13;
   SG1.RowCount        := 2;
   SG1.Rows[1].Clear;
   SG1.Cells[0,0]      := 'SSZ';
   SG1.Cells[1,0]      := 'RENDSZAM';
   SG1.Cells[2,0]      := 'DATUM';
   SG1.Cells[3,0]      := 'IDO';
   SG1.Cells[4,0]      := 'ORSZAG';
   SG1.Cells[5,0]      := 'OSSZEG';
   SG1.Cells[6,0]      := 'KM';
   SG1.Cells[7,0]      := 'VNEM';
   SG1.Cells[8,0]      := 'Típus';
   SG1.Cells[9,0]      := 'Leírás';
   SG1.Cells[10,0]     := 'Menny';
   SG1.Cells[11,0]     := 'ÁFA %';
   SG1.Cells[12,0]     := 'ÁFA érték';
   SG1.ColWidths[1]    := 100;
   SG1.ColWidths[2]    := 100;
   SG1.ColWidths[3]    := 100;
   SG1.ColWidths[4]    := 100;
   SG1.ColWidths[5]    := 100;
   SG1.ColWidths[6]    := 80;
   SG1.ColWidths[7]    := 50;
   SG1.ColWidths[8]    := 100;
   SG1.ColWidths[9]    := 200;
   SG1.ColWidths[10]   := 100;
   SG1.ColWidths[11]   := 100;
   SG1.ColWidths[12]   := 100;
   SG1.RowCount        := 2 + rec_szam;
   rsz                 := 1;
   EgyebDlg.Ellenlista.Clear;
end;

function TKoltsegImport2Dlg.GetDatum(dstr : string) : string;
var
   dat : string;
begin
   Result  := '';
   dat     := dstr;
   if Length(dat) >= 10 then begin
       // Teljes dátum
       if copy(dat, 3, 1) = '.' then begin
           // NN.HH.ÉÉÉÉ alakú
           dat := copy(dat, 7, 4)+'.'+copy(dat, 4, 2)+':'+copy(dat, 1, 2);
       end;
   end;
   while Pos(' ', dat) > 0 do begin
       dat := copy(dat, 1, Pos(' ', dat)-1)+copy(dat, Pos(' ', dat)+1, 9999);
   end;
   Result  := dat;
end;

function TKoltsegImport2Dlg.GetBeodat( fimod : string ) : string;
var
   beodat  : string;
   folytat : boolean;
begin
   // A beolvasás dátumának meghatározása -> ez egyedi lesz a megadott költségimport állományra
   beodat  := EgyebDlg.MaiDatum;
   folytat := true;
   while folytat do begin
       Query_Run( QueryAl, 'SELECT * FROM KOLTSEG_GYUJTO WHERE KS_BEODAT = '''+beodat+''' AND KS_FIMOD = '''+fimod+''' ');
       if QueryAl.RecordCount = 0 then begin
           // Jó lesz a megadott dátum
           folytat := false;
       end else begin
           // Továbblépünk a napokkal
           beodat  := Datumhoznap(beodat, 1, true);
       end;
   end;
   Result  := beodat;
end;

function TKoltsegImport2Dlg.VanRendszam( rsz : string) : boolean;
begin
    // A rendszám ellenőrzése
    Result := QRSz.Locate('GK_RESZ', rsz, []);
end;

procedure  TKoltsegImport2Dlg.NevCSere(SGbe : TStringGrid ; mirol, mire : string);
var
   ii  : integer;
begin
   for ii := 0 to SGbe.ColCOunt-1 do begin
       if SGbe.Cells[ii, 0] = mirol then begin
           SGbe.Cells[ii, 0] := mire;
       end;
   end;
end;

end.
