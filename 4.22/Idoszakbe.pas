unit Idoszakbe;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, Mask;

type
  TIdoszakbeDlg = class(TForm)
    Label6: TLabel;
    Mig: TMaskEdit;
    BitBtn10: TBitBtn;
    BitElkuld: TBitBtn;
    BitKilep: TBitBtn;
    Label1: TLabel;
    Mtol: TMaskEdit;
    BitBtn1: TBitBtn;
    procedure BitBtn10Click(Sender: TObject);
    procedure MigExit(Sender: TObject);
    procedure MtolExit(Sender: TObject);
    procedure BitElkuldClick(Sender: TObject);
    procedure BitKilepClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure MtolKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
  public
  		ret_datumtol, ret_datumig: string;
  end;

var
  IdoszakbeDlg: TIdoszakbeDlg;

implementation

uses
	Egyeb, Kozos;
{$R *.dfm}

procedure TIdoszakbeDlg.BitBtn10Click(Sender: TObject);
begin
	// EgyebDlg.Calendarbe(Mtol);
  EgyebDlg.CalendarBe_idoszak(Mtol, Mig);
end;


procedure TIdoszakbeDlg.MigExit(Sender: TObject);
begin
	DatumExit(Mig, false);
end;

procedure TIdoszakbeDlg.BitElkuldClick(Sender: TObject);
begin
  if (trim(Mtol.Text)= '') then begin
  		NoticeKi('A kezd� d�tumot meg kell adni!');
	  	Mtol.SetFocus;
		  Exit;
    end;
  if (trim(Mig.Text)= '') then begin
  		NoticeKi('A z�r� d�tumot meg kell adni!');
	  	Mig.SetFocus;
		  Exit;
    end;

	if (not DatumExit(Mtol, true))and(not DatumExit(Mtol, true)) then begin
   	  Exit;
      end;
	if (not DatumExit(Mig, true))and(not DatumExit(Mig, true)) then begin
   	  Exit;
      end;
   ret_datumtol	:= trim(Mtol.Text);
   ret_datumig	:= trim(Mig.Text);
   Close;
end;

procedure TIdoszakbeDlg.BitKilepClick(Sender: TObject);
begin
   ret_datumtol	:= '';
   ret_datumig	:= '';
   Close;
end;

procedure TIdoszakbeDlg.FormCreate(Sender: TObject);
begin
   ret_datumtol	:= '';
   ret_datumig	:= '';
end;

procedure TIdoszakbeDlg.MtolExit(Sender: TObject);
begin
	DatumExit(Mtol, false);
end;


procedure TIdoszakbeDlg.BitBtn1Click(Sender: TObject);
begin
	EgyebDlg.Calendarbe(Mig, True);

end;

procedure TIdoszakbeDlg.FormShow(Sender: TObject);
begin
  if Mtol.Text='' then
   ActiveControl:=Mtol;
end;

procedure TIdoszakbeDlg.MtolKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
	EgyebDlg.FormReturn(Key);
end;

end.
