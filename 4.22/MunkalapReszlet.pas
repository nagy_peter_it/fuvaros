unit MunkalapReszlet;

interface

uses
  J_ALFORM, Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Buttons, Vcl.Mask,
  Data.DB, Data.Win.ADODB;

type
  TMunkalapReszletDlg = class(TJ_AlformDlg)
    MU_IGENY: TMaskEdit;
    Label6: TLabel;
    MU_MSZAM: TMaskEdit;
    Label1: TLabel;
    MU_RENSZ: TMaskEdit;
    Label2: TLabel;
    MU_DATUM: TMaskEdit;
    Label3: TLabel;
    MU_IDO: TMaskEdit;
    Label4: TLabel;
    MU_FENNM: TMaskEdit;
    Label5: TLabel;
    MU_MEGJE: TMaskEdit;
    BitKilep: TBitBtn;
    MU_NETTO: TMaskEdit;
    Label7: TLabel;
    Query1: TADOQuery;
  	procedure Tolto(cim : string; teko : string); override;
    procedure BitKilepClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  MunkalapReszletDlg: TMunkalapReszletDlg;

implementation

uses
	Kozos, Egyeb, J_SQL;

{$R *.dfm}


procedure TMunkalapReszletDlg.BitKilepClick(Sender: TObject);
begin
   Close;
end;


procedure TMunkalapReszletDlg.Tolto(cim : string; teko:string);
var
  S: string;
begin
  S:= 'SELECT MU_MSZAM, MU_RENSZ, MU_DATUM, MU_IDO, MU_NETTO, MU_IGENY, MU_FENNM, MU_MEGJE ';
  S:= S+ 'FROM SZERVIZMUNKALAP WHERE MU_MSZAM = '''+teko+'''  ';
  Query_Run (Query1, S ,true);
  MU_MSZAM.Text:=Query1.FieldByName('MU_MSZAM').AsString;
  MU_RENSZ.Text:=Query1.FieldByName('MU_RENSZ').AsString;
  MU_DATUM.Text:=Query1.FieldByName('MU_DATUM').AsString;
  MU_IDO.Text:=Query1.FieldByName('MU_IDO').AsString;
  MU_NETTO.Text:=Query1.FieldByName('MU_NETTO').AsString;
  MU_IGENY.Text:=Query1.FieldByName('MU_IGENY').AsString;
  MU_FENNM.Text:=Query1.FieldByName('MU_FENNM').AsString;
  MU_MEGJE.Text:=Query1.FieldByName('MU_MEGJE').AsString;
end;

end.
