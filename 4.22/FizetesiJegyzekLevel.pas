unit FizetesiJegyzekLevel;

interface

uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, DBTables, DB, StdCtrls, Buttons, Mask, DBCtrls,
  ExtCtrls, ADODB, FileCtrl, StrUtils, Grids, J_DataModule, Vcl.ImgList;

type
  TComboImages = (Semmi, IgenNem, Figyelem);
  TFizetesiJegyzekLevelDlg = class(TForm)
    Panel2: TPanel;
    SG1: TStringGrid;
    Query1: TADOQuery;
    GridPanel1: TGridPanel;
    BtnExport: TBitBtn;
    BitBtn6: TBitBtn;
    InfoPanel: TPanel;
    Button1: TButton;
    Button2: TButton;
    IgenNemList: TImageList;
	 procedure FormCreate(Sender: TObject);
   procedure AddGridCombo(iColNumber: integer; iRowNumber: Integer; ListItems: array of string; ComboImages: TComboImages);
   procedure AlignGridCombos(row: integer);
   procedure AlignGridCombo(iColNumber, iRowNumber:integer);
   procedure SetGridCombo(iColNumber, iRowNumber:integer; iIndex: integer);
   function GetGridCombo(iColNumber, iRowNumber:integer): integer;
   function GetMailPart(const SzotarFokod: string; const Part: string; const Valtozok: array of string): string;
   function Behelyettesit(const S: string; const Valtozok: array of string): string;
   // procedure IgenNemDraw(Control: TWinControl; Index: Integer;
   //   Rect: TRect; State: TOwnerDrawState);
   // procedure AddGridCheckBox(iColNumber: integer; iRowNumber: Integer);
   // procedure SetGridCheckBoxAlignment(iColNumber, iRowNumber:integer; bChecked :Boolean);
	 procedure BitBtn6Click(Sender: TObject);
	 procedure BtnExportClick(Sender: TObject);
	 procedure Feldolgozas;
   procedure FillGrid;
   //  procedure SG1DrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure SG1TopLeftChanged(Sender: TObject);
    procedure SG1FixedCellClick(Sender: TObject; ACol, ARow: Integer);
    procedure SG1MouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
    procedure SG1SelectCell(Sender: TObject; ACol, ARow: Integer;
      var CanSelect: Boolean);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);


  private
		kilepes		: boolean;
		JSzamla    	: TJSzamla;
    ElsoLevelNap, HanyNaponta, LevelDarabszam: integer;
    HatarNap: string;
    HintLatestRow, HintLatestCol : integer;
  public
  end;

const
  // Ujsor = chr(13)+chr(10);
  Ujsor = '<BR>';
  NyelvArray: array [0..2] of string = ('HUN','ENG','GER');
  micsiSemmi = 0;
  micsiKuld = 1;

var
  FizetesiJegyzekLevelDlg: TFizetesiJegyzekLevelDlg;

implementation

uses
	Egyeb, Kozos, KozosTipusok, J_SQL, J_Excel, HianyVevobe, Windows;
{$R *.DFM}

procedure TFizetesiJegyzekLevelDlg.FormCreate(Sender: TObject);
var
  S: string;
  Cur: TCursor;
begin
  EgyebDlg.SeTADOQueryDatabase(Query1);
  Cur:= Screen.Cursor;
  Screen.Cursor:=crHourGlass;
  FillGrid;
  Screen.Cursor:=Cur;
end;

procedure TFizetesiJegyzekLevelDlg.FillGrid;
const
  const_fizjegy_kiterjesztes = '.pdf';
var
  S, Info, LegutobbiDatum: string;
  i, KuldottLevelDb: integer;
  Kuldheto: boolean;
  FN_from, FN_to, Res, SoforNeve: string;
  talalat: integer;
  SearchRec: TSearchRec;
begin
  talalat:= FindFirst(EgyebDlg.FIZETESI_JEGYZEK_BEMENET+'\*'+const_fizjegy_kiterjesztes, faAnyFile, SearchRec);
  if talalat = 0 then begin
    SG1.DefaultRowHeight:=26;  // A combo-k miatt a 24 nem el�g
    SG1.RowCount:=2;
    SG1.FixedRows:=1;
    SG1.ColWidths[0]:=500;
    SG1.ColWidths[1]:=80;
    SG1.Cells[0,0]:='Dokumentum';
    SG1.Cells[1,0]:='K�ldend�';
    end;  // if
  i:=0;
  try
      while talalat = 0 do begin
        Inc(i);
        if SG1.RowCount < i+1 then  // a FixedRows=1 miatt musz�j 2 sorral ind�tani
           SG1.RowCount:= SG1.RowCount +1;
        SG1.Cells[0,i]:= SearchRec.Name;
        AddGridCombo(1, i, ['Nem', 'Igen'], IgenNem); // ilyen sorrendben vannak az ImageList-ben is
        talalat:= FindNext(SearchRec);
        end;  // while
      InfoPanel.Caption:=IntToStr(i)+' dokumentum';
      SysUtils.FindClose(SearchRec);  // valami Winapi-s met�dust gondolt a ford�t�
      for i:=0 to SG1.RowCount-1 do begin
        SetGridCombo(1, i, micsiKuld); // "Igen"-eket �ll�tunk be
        end;  // for
  except
      on E: Exception do begin
          NoticeKi(E.Message+' in line '+IntToStr(i));
          end;
      end;  // try-except
end;

procedure TFizetesiJegyzekLevelDlg.AddGridCombo(iColNumber: integer; iRowNumber: Integer; ListItems: array of string; ComboImages: TComboImages);
var
  NewCombo :TComboBox;
  i: integer;
begin
  NewCombo := TComboBox.Create(SG1);
  NewCombo.Parent := SG1; // Place string grid on one panel
  NewCombo.Style:= csDropDownList;  // text not editable
  NewCombo.Visible := False;
  NewCombo.Color :=  clWhite;
  i	:= 0;
 	while i <= High(ListItems) do begin
    NewCombo.Items.Add(ListItems[i]);
    Inc(i);
   	end;  // while
  NewCombo.Tag:= 100 * iRowNumber + iColNumber;  // assuming less than 100 columns

  SG1.Objects[iColNumber, iRowNumber] := NewCombo;
  AlignGridCombo(iColNumber, iRowNumber); // Calling align function
end;

procedure TFizetesiJegyzekLevelDlg.AlignGridCombos(row: integer);
begin
   AlignGridCombo(1, row);
end;

procedure TFizetesiJegyzekLevelDlg.AlignGridCombo(iColNumber, iRowNumber: integer);
var
  ActCombo :TComboBox;
  Rect:TRect;
  LeftGap: integer;
begin
  ActCombo := (SG1.Objects[iColNumber, iRowNumber] as TComboBox);
  if ActCombo <> nil then begin
    Rect := SG1.CellRect(iColNumber,iRowNumber);
    if Rect.Height>0 then begin   // within the canvas at all
      LeftGap := 2;  // gap from cell's left border
      ActCombo.Left  := SG1.Left + Rect.Left + LeftGap;
      ActCombo.Top   := SG1.Top + Rect.Top+2;
      ActCombo.Width := Rect.Right-Rect.Left - LeftGap;  // to fill cell
      ActCombo.Height := Rect.Bottom-Rect.Top;
      ActCombo.Visible := True;
      end
    else begin
      ActCombo.Visible := False;
      end;
    // ActCombo.ItemIndex:=iIndex;
    end;  // if
end;

procedure TFizetesiJegyzekLevelDlg.SetGridCombo(iColNumber, iRowNumber:integer; iIndex: integer);
var
  NewCombo :TComboBox;
begin
  NewCombo := (SG1.Objects[iColNumber, iRowNumber] as TComboBox);
  if NewCombo <> nil then begin
    NewCombo.ItemIndex:=iIndex;
    end;  // if
end;

function TFizetesiJegyzekLevelDlg.GetGridCombo(iColNumber, iRowNumber:integer): integer;
var
  NewCombo :TComboBox;
begin
  NewCombo := (SG1.Objects[iColNumber, iRowNumber] as TComboBox);
  if NewCombo <> nil then begin
    Result:= NewCombo.ItemIndex;
    end;  // if
end;

procedure TFizetesiJegyzekLevelDlg.SG1FixedCellClick(Sender: TObject; ACol,
  ARow: Integer);
begin
  SG1TopLeftChanged(Sender); // Col resize?
end;

procedure TFizetesiJegyzekLevelDlg.SG1MouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
var
   P : TPoint;
   gc: TGridCoord;
begin
   gc := TStringGrid(Sender).MouseCoord(x, y);
   P.X := X;
   P.Y := Y;
   if  (gc.x <> HintLatestCol) or (gc.y <> HintLatestRow) then begin
      HintLatestCol := gc.x;
      HintLatestRow := gc.y;
      // ha van hozz� �rv�nyes adat cella
      if (HintLatestCol>=1) and (HintLatestCol<=SG1.ColCount-1) and (HintLatestRow>=0) and (HintLatestRow<=SG1.RowCount-1) then begin
          SG1.Hint := SG1.Cells[HintLatestCol, HintLatestRow];  // cell content
          // SG1.ShowHint:=True;
          Application.HintColor := clInfobk;
          Application.ActivateHint(P);
          end;  // if
      end;  // if
end;

procedure TFizetesiJegyzekLevelDlg.SG1SelectCell(Sender: TObject; ACol, ARow: Integer;
  var CanSelect: Boolean);
begin
  {if ACol in [999] then   // esetleg k�s�bbi m�dos�t�shoza szerkeszthet� oszlopok
      CanSelect:= True
  else CanSelect:= False;
  }
  CanSelect:= False;
end;

procedure TFizetesiJegyzekLevelDlg.SG1TopLeftChanged(Sender: TObject);
var
  row: integer;
begin
  for row := 0 to SG1.RowCount - 1 do begin    // itt kezel�nk mindenf�le g�rget�st
     AlignGridCombos(row);
     end;  // for
end;


procedure TFizetesiJegyzekLevelDlg.BitBtn6Click(Sender: TObject);
begin
	if not kilepes then begin
		kilepes	:= true;
	  end
  else begin
		Close;
	  end;
end;

procedure TFizetesiJegyzekLevelDlg.BtnExportClick(Sender: TObject);
var
  fnev,elotag: string;
  Cur: TCursor;
begin
	BtnExport.Visible	:= false;
  if NoticeKi('A k�ldhet� dokumentumokat most elk�ldj�k. Folytatja?', NOT_QUESTION) <> 0 then begin
       Exit;  // ha nem "igen" a v�lasz
       end;
  Cur:= Screen.Cursor;
  Screen.Cursor:=crHourGlass;
	Feldolgozas;
  Screen.Cursor:=Cur;
  // NoticeKi('A levelet elk�ldt�k, megtekinthet�k az Outlook "Elk�ld�tt �zenetek" mapp�j�ban.');
	BtnExport.Visible	:= true;
end;

procedure TFizetesiJegyzekLevelDlg.Button1Click(Sender: TObject);
const
   MicsiCol = 1;
var
  i: integer;
begin
  if NoticeKi('Biztosan minden sorban �t szeretn� �ll�tani a "K�ldhet�" mez� �rt�k�t?', NOT_QUESTION) <> 0 then
     exit ;
  for i:=0 to SG1.RowCount-1 do begin
    SetGridCombo(MicsiCol, i, micsiKuld); // a felhaszn�l� �ltal v�lasztott Nem, Igen vagy �gyv�dnek
    end;  // for
end;

procedure TFizetesiJegyzekLevelDlg.Button2Click(Sender: TObject);
const
   MicsiCol = 1;
var
  i: integer;
begin
  if NoticeKi('Biztosan minden sorban �t szeretn� �ll�tani a "K�ldhet�" mez� �rt�k�t?', NOT_QUESTION) <> 0 then
     exit ;
  for i:=0 to SG1.RowCount-1 do begin
    SetGridCombo(MicsiCol, i, micsiSemmi); // a felhaszn�l� �ltal v�lasztott Nem, Igen vagy �gyv�dnek
    end;  // for
end;

procedure TFizetesiJegyzekLevelDlg.Feldolgozas;
var
  row, i, NyelvIndex, Micsi: integer;
  Cimzett, MailSubject, MailBody, FN_from, FN_to, S: string;
  Filename, SzotarFokod, Nyelv: string;
  Cimzettek, AttachmentList, UresSL: TStringList;
  Siker: boolean;
  Valtozok: array of string;

  function GetFokod(NyelvIndex: integer): string;
    begin
      case NyelvIndex of        //  ['HUN','ENG','GER']
         0: Result:='466';
         end;  // case
    end;

begin
  Cimzettek:=TStringList.Create;
  UresSL:=TStringList.Create;
  AttachmentList:=TStringList.Create;
  try
   SzotarFokod:= GetFokod(0);
   MailSubject:= GetMailPart(SzotarFokod, 'C1', Valtozok);  // c�m
   MailBody:= GetMailPart(SzotarFokod, 'F', Valtozok);   // fejsorok
   AttachmentList.Clear;
   row:= 1;  // fejl�c nem kell
   while row <= SG1.RowCount - 1 do begin
      Micsi:= GetGridCombo(1,row); // a felhaszn�l� �ltal v�lasztott Nem vagy Igen
      if Micsi <> micsiSemmi then begin  // K�ldhet�
         Filename:= SG1.Cells[0,row];
         AttachmentList.Add(EgyebDlg.FIZETESI_JEGYZEK_BEMENET+'\'+Filename);
         end;  // if
      Inc(row);
      end; // while
   if AttachmentList.Count > 0 then begin  // ha legal�bb egy k�ldhet�t tal�lt
      MailBody:= MailBody+ UjSor+ GetMailPart(SzotarFokod, 'L', Valtozok);   // l�bsorok
      Cimzettek.Clear;
      Cimzett:= GetMailPart(SzotarFokod, 'cimzett', Valtozok);
      Cimzettek.Add(Cimzett);
      Siker:= SendOutlookHTMLMmail(Cimzettek, UresSL, AttachmentList, MailSubject, MailBody, True, uzleti);
      if Siker then begin  // napl�zunk
          S:='Dokumentum elk�ldve a '+Cimzett+' c�mre: ';
          for i:=0 to AttachmentList.Count-1 do begin
              Query_Log_Str(S + AttachmentList[i], 0, true);
              FN_from:= AttachmentList[i];
              FN_to:= EgyebDlg.FIZETESI_JEGYZEK_ELKULDVE+'\'+ ExtractFileName(AttachmentList[i]);
              if MoveFile(PChar(FN_from), PChar(FN_to)) then
                  Query_Log_Str('Elmozgatva: '+FN_from+' -> '+FN_to, 0, true)
              else
                  Query_Log_Str('!!! Sikertelen file mozgat�s: '+FN_from+' -> '+FN_to, 0, true)
              end;
          end  // if
      else begin
          NoticeKi('Sikertelen k�ld�s!');
          end; // else
      end; // if AttachmentList.Count
  finally
     if Cimzettek <> nil then Cimzettek.Free;
     if UresSL <> nil then UresSL.Free;
     if AttachmentList <> nil then AttachmentList.Free;
     end;  // try-finally
end;

function TFizetesiJegyzekLevelDlg.GetMailPart(const SzotarFokod: string; const Part: string; const Valtozok: array of string): string;
var
  Res, Sor: string;
  i: integer;

  function MaxSorok(Part: string): integer;  // ennyi sorb�l �llhat egy blokk (fejsorok, l�bsorok)
    begin
       if Part='F' then Result:=12;
       if Part='L' then Result:=22;
    end;
begin
  if length(Part)>1 then begin  // egy sort szeretn�nk csak
      Res:= EgyebDlg.Read_SZGrid(SzotarFokod, Part);
      end // if
  else begin  // t�bb sorb�l �ll
    Res:='';
    for i:=MaxSorok(Part) downto 1 do begin  // k�nnyebb �gy a blokk v�gi �res sorokat kisz�rni
      Sor:=Trim(EgyebDlg.Read_SZGrid(SzotarFokod, Part + FormatFloat('00',i))); // k�t jegyre 0-val
      if (Sor<>'') or (Res<>'') then
        Res:=Sor+UjSor+Res;
      end;  // for
    end;  // else
  Res:=Behelyettesit(Res, Valtozok);
  Result:= Res;
end;

function TFizetesiJegyzekLevelDlg.Behelyettesit(const S: string; const Valtozok: array of string): string;
var
  i: integer;
  R, v_ertek, v_nev: string;
begin
  i	:= 0;
  R:=S;  // init
 	while i <= High(Valtozok) do begin
    v_nev:= Valtozok[i];
    v_ertek:= Valtozok[i+1];
    R:=StringReplace(R, '['+v_nev+']', v_ertek, [rfReplaceAll]);  // '[PARTNER]' --> 'UPC Kft'
    i := i+2;
  	end;  // while
  Result:= R;
end;

end.



