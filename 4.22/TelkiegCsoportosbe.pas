unit TelkiegCsoportosbe;

interface

uses
	SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
	Forms, Dialogs, StdCtrls, Buttons, Mask, DB, DBTables, J_ALFORM, ADODB,
  Vcl.Grids, Vcl.ExtCtrls, Vcl.ComCtrls, Vcl.TabNotBk;

type
	TTelkiegCsoportosbeDlg = class(TJ_AlformDlg)
    Panel1: TPanel;
    Panel2: TPanel;
    Label6: TLabel;
    M5: TMaskEdit;
    M6: TMaskEdit;
    Query1: TADOQuery;
    Query2: TADOQuery;
    SGTelKieg: TStringGrid;
    GridPanel1: TGridPanel;
    BitBtn10: TBitBtn;
    BitBtn12: TBitBtn;
    BitKilep: TBitBtn;
    meTELKIEGKOD: TMaskEdit;
	 procedure BitKilepClick(Sender: TObject);
	 procedure FormCreate(Sender: TObject);
	 procedure MaskEditClick(Sender: TObject);
	 procedure Tolto(DOKOD : string);
    procedure BitBtn10Click(Sender: TObject);
    procedure BitBtn12Click(Sender: TObject);
    procedure M6Click(Sender: TObject);
	private
    lista_tipus, lista_statusz: TStringList;
    procedure KiegeszitokTablaToltes;
	public
	end;

const
  SG_kod_oszlop = 0;
var
	TelkiegCsoportosbeDlg: TTelkiegCsoportosbeDlg;

implementation

uses
 Egyeb, J_SQL, Kozos,  J_VALASZTO;

{$R *.DFM}

procedure TTelkiegCsoportosbeDlg.BitKilepClick(Sender: TObject);
begin
	Close;
end;

procedure TTelkiegCsoportosbeDlg.FormCreate(Sender: TObject);
var
  i: integer;
begin
	EgyebDlg.SeTADOQueryDatabase(Query1);
	EgyebDlg.SeTADOQueryDatabase(Query2);

  SGTelKieg.Cells[0,0]	:= 'K�d';
	SGTelKieg.Cells[1,0]	:= 'T�pus';
	SGTelKieg.Cells[2,0]	:= 'Megjegyz�s';
  SGTelKieg.ColWidths[0]	:= 50;
  SGTelKieg.ColWidths[1]	:= 220;
  SGTelKieg.ColWidths[2]	:= 300;
end;

procedure TTelkiegCsoportosbeDlg.Tolto(DOKOD : string);
var
  S: string;
begin
	SetMaskEdits([M5, M6]);
	M5.Text		:= DOKOD;
  M6.Text		:= Query_Select('DOLGOZO', 'DO_KOD', M5.Text, 'DO_NAME');

  KiegeszitokTablaToltes;
end;

procedure TTelkiegCsoportosbeDlg.KiegeszitokTablaToltes;
var
  i: integer;
begin
  SGTelKieg.RowCount := 1;  // alaphelyzet: csak a fejl�c
  Query_Run(Query1, 'select TI_ID SORSZAM, sz1.SZ_MENEV TIPUS, TI_MEGJE MEGJEGYZES from '+
        ' TELKIEG left outer join SZOTAR sz1 on sz1.SZ_ALKOD = TI_TIPUSKOD and sz1.SZ_FOKOD = ''153''  '+
        ' where TI_DOKOD= '+M5.text);
	if Query1.RecordCount > 0 then begin
		SGTelKieg.RowCount := Query1.RecordCount+1;
		i := 1;
		while not Query1.Eof do begin
			SGTelKieg.Cells[0,i]	:= Query1.FieldByName('SORSZAM').AsString;
			SGTelKieg.Cells[1,i]	:= Query1.FieldByName('TIPUS').AsString;
      SGTelKieg.Cells[2,i]	:= Query1.FieldByName('MEGJEGYZES').AsString;
			Query1.Next;
			Inc(i);
  		end;  // while
  	end; // if
end;


procedure TTelkiegCsoportosbeDlg.M6Click(Sender: TObject);
begin
   EgyebDlg.DolgozoFotoMutato(M5.Text, M6);
end;

procedure TTelkiegCsoportosbeDlg.MaskEditClick(Sender: TObject);
begin
	with Sender as TMaskEdit do begin
  	SelectAll;
  end;
end;

procedure TTelkiegCsoportosbeDlg.BitBtn10Click(Sender: TObject);
begin
  meTELKIEGKOD.text:= '';
  TelKiegValaszto(meTELKIEGKOD, True);
  if meTELKIEGKOD.text<> '' then begin
    Query_Update (Query1, 'TELKIEG',
        [
        'TI_DOKOD', ''''+M5.Text+''''
        ], ' WHERE TI_ID='+meTELKIEGKOD.Text);
    KiegeszitokTablaToltes;  // a t�bl�zat friss�t�se
    end;  // if
end;

procedure TTelkiegCsoportosbeDlg.BitBtn12Click(Sender: TObject);
var
	i : integer;
  TIID: string;
begin
   if NoticeKi('Val�ban t�r�lni k�v�nja a kijel�lt �sszerendel�st?', NOT_QUESTION) <> 0 then begin
     	Exit;
      end;
   TIID:= SGTelKieg.Cells[SG_kod_oszlop, SGTelKieg.Row];
   if TIID <> '' then begin  	// Ha ki van t�ltve az ID
      Query_Update (Query1, 'TELKIEG',
      [
      'TI_DOKOD', 'null'
      ], ' WHERE TI_ID='+TIID);
      end;
 KiegeszitokTablaToltes;  // a t�bl�zat friss�t�se
end;

end.







