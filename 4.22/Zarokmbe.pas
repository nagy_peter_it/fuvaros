unit Zarokmbe;

interface

uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, DBTables, DB, StdCtrls, Buttons, Mask, DBCtrls,
  J_ALFORM, ExtCtrls, ADODB;

type
  TZarokmbeDlg = class(TJ_AlformDlg)
    BitBtn6: TBitBtn;
    Label3: TLabel;
    M1: TMaskEdit;
    BitBtn1: TBitBtn;
    Label1: TLabel;
    M2: TMaskEdit;
    BitBtn2: TBitBtn;
    Label2: TLabel;
    M3: TMaskEdit;
    BitElkuld: TBitBtn;
    Query1: TADOQuery;
    M4: TMaskEdit;
    Label16: TLabel;
    M0: TMaskEdit;
    Label4: TLabel;
    Query2: TADOQuery;
    CheckBox1: TCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure BitBtn6Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure M2Exit(Sender: TObject);
    procedure M3Exit(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure M3KeyPress(Sender: TObject; var Key: Char);
    procedure M3Click(Sender: TObject);
    procedure BitElkuldClick(Sender: TObject);
    procedure Tolto(cim : string; teko : string);override;
    procedure M3KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure M4KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
  public
  end;

var
  ZarokmbeDlg: TZarokmbeDlg;

implementation

uses
	J_VALASZTO, Egyeb, Kozos, J_SQL, Kozos_Local,ZarokmFm, Windows, Math;
{$R *.DFM}

procedure TZarokmbeDlg.FormCreate(Sender: TObject);
begin
 	EgyebDlg.SetADOQueryDatabase(Query1);
 	EgyebDlg.SetADOQueryDatabase(Query2);
	ret_kod 	:= '';
end;

procedure TZarokmbeDlg.BitBtn6Click(Sender: TObject);
begin
	Close;
end;

procedure TZarokmbeDlg.BitBtn1Click(Sender: TObject);
begin
	GepkocsiValaszto(M1, GK_TRAKTOR);
  BitBtn2.SetFocus;
end;

procedure TZarokmbeDlg.M2Exit(Sender: TObject);
begin
	DatumExit(M2, false);
   if (M2.Text <> '')and(not CheckBox1.Checked) then begin
  		M2.Text	:= GetLastDay(M2.Text);
   end;
end;

procedure TZarokmbeDlg.M3Exit(Sender: TObject);
var
 hozza: integer;
begin
	with Sender as TMaskEdit do begin
  		Text := StrSzamStr(Text,12,0);
  end;
  hozza:=StrToIntDef( Query_Select('GEPKOCSI','GK_RESZ',M1.Text,'GK_HOZZA'),0);
  if hozza>0 then
  begin
    M3.Text:=IntToStr(strtointdef(M3.Text,0)+hozza);
  end;
end;

procedure TZarokmbeDlg.BitBtn2Click(Sender: TObject);
begin
	EgyebDlg.Calendarbe(M2);
   // A h�nap utols� napj�ra �llunk
   if not CheckBox1.Checked then
   M2.Text	:= GetLastDay(M2.Text);
   M3.SetFocus;
end;

procedure TZarokmbeDlg.M3KeyPress(Sender: TObject; var Key: Char);
begin
  	with Sender as TMaskEdit do begin
  		Key := EgyebDlg.Jochar(Text,Key,12,0);
  	end;
end;

procedure TZarokmbeDlg.M3Click(Sender: TObject);
begin
	with Sender as TMaskEdit do begin
  		SelectAll;
  	end;
end;

procedure TZarokmbeDlg.BitElkuldClick(Sender: TObject);
var
  ureskm: string;
  kedat		: string;
  vedat		: string;
begin
	 if not DatumExit(M2, true) then begin
   	Exit;
   end;
   Query_Run(Query2, 'SELECT GK_GKKAT FROM GEPKOCSI WHERE GK_RESZ ='''+M1.Text+'''' );

   kedat:=copy(M2.Text,1,8)+'01.';
   vedat:=GetLastDay(M2.Text);
   Query_Run(Query1, 'SELECT sum(cast( ja_uresk as integer)) OSSZ_U_KM  FROM jarat WHERE ja_rendsz ='''+M1.Text+''''+
   ' and JA_JKEZD >= '''+kedat+''' AND JA_JKEZD <= '''+vedat+'''  AND JA_FAZIS <> 2' );

   ureskm:=Query1.FieldByName('OSSZ_U_KM').AsString   ;
   if not CheckBox1.Checked then
   M2.Text	:= GetLastDay(M2.Text);
	 if ret_kod = '' then begin
  		{�j rekord felvitele}
     	//ujkodszam	:= GetNextCode('ZAROKM', 'ZK_ZKKOD', 1, 0);
     	//ret_kod		:= IntToStr(ujkodszam);
     	//Query_Run ( Query1, 'INSERT INTO ZAROKM (ZK_ZKKOD) VALUES (' + ret_kod + ' )',true);
     	Query_Run ( Query1, 'INSERT INTO ZAROKM (ZK_RENSZ,ZK_DATUM,ZK_GKKAT,ZK_KMORA,ZK_MEGJE) VALUES ('+
      ''''+M1.Text+''','''+M2.Text+''','''+Query2.FieldByName('GK_GKKAT').AsString+''','+SqlSzamString(StringSzam(M3.Text), 12, 0)+','''+M4.Text+''' )',true);

    	Query_Run(Query1, 'SELECT MAX(ZK_ZKKOD) MAXKOD FROM ZAROKM');
      ret_kod:= Query1.FieldByname('MAXKOD').AsString ;

   end
   else
   begin
 	 {A r�gi rekord m�dos�t�sa}
   Query_Update (Query1, 'ZAROKM',
    	[
     	'ZK_RENSZ', '''' + M1.Text + '''',
     	'ZK_DATUM', '''' + M2.Text + '''',
      'ZK_GKKAT',''''+Query2.FieldByName('GK_GKKAT').AsString+'''',
     	'ZK_KMORA', SqlSzamString(StringSzam(M3.Text), 12, 0),
     	'ZK_URESKM', SqlSzamString(StringSzam(ureskm), 12, 0),
      'ZK_MEGJE', '''' + M4.Text + ''''
     	], 	' WHERE ZK_ZKKOD = '+ret_kod);
   end;

   ZarokmFmDlg.idopont:=M2.Text;
   ZarokmFmDlg.ret_kod:=ret_kod;
   Close;

end;

procedure TZarokmbeDlg.Tolto(cim : string; teko:string);
begin
	ret_kod 	:= teko;
	Caption 	:= cim;
   M0.Text		:= teko;
	M1.Text 	:= '';
	M2.Text 	:=ZarokmFmDlg.idopont;            //'';
	M3.Text 	:= '';
   M4.Text		:= '';
	if ret_kod <> '' then begin		{M�dos�t�s}
     	if Query_Run (Query1, 'SELECT * FROM ZAROKM WHERE ZK_ZKKOD = '+teko,true) then begin
			M1.Text 	:= Query1.FieldByName('ZK_RENSZ').AsString;
			M2.Text 	:= Query1.FieldByName('ZK_DATUM').AsString;
			M3.Text 	:= Query1.FieldByName('ZK_KMORA').AsString;
           M4.Text 	:= Query1.FieldByName('ZK_MEGJE').AsString;
		end;
	end;
  ZarokmFmDlg.ret_kod:=ret_kod;
end;

procedure TZarokmbeDlg.M3KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key=vk_return then
    M4.SetFocus;
end;

procedure TZarokmbeDlg.M4KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key=vk_return then
    BitElkuld.SetFocus;

end;

end.


