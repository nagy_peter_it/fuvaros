unit Egyeb;

interface

uses
	SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
	Forms, 	Dialogs, DB, DBTables, IniFiles, Buttons, StdCtrls, ExtCtrls,
	Mask, quickrpt, Qrctrls, printers, variants,
  	Menus, 	Grids, FileList, dbgrids, Kozos, J_SQL, Calend1, qrprntr,
  	ADODB, 	OleCtrls, SHDocVw, HTTPApp, HTTPProd, ShellApi, MsHtml,
  	IdBaseComponent, IdComponent, IdTCPConnection, IdTCPClient, IdHTTP, DateUtils,
    Data.DBXJSON, KozosTipusok, idFTP;
type
  szamnev = array[0..9] of string[13];

  T_TeljesDijSzamitasRec = record
           teljesdij_EUR: double;
           alvallalkozo_EUR: double;
           teljesdij_leiras: string;
           EURKm: double;
           EURKmAtallas: double;
           AlEURKm: double;
           EURKm_leiras: string;
           end;  // record

   TUtvonalInfo = record
            Kilometer: integer;
            Atallas: integer;
            Leiras: string;
            Hibauzenet: string;
            end;  // record

   TFelrakLerak = record
            FelOrszag: string;
            FelIranyito: string;
            LeOrszag: string;
            LeIranyito: string;
            FelDatum: string;
            Ervenyes: boolean;
       end;  // record

  TElofizetesInfo = record
            dokod: string;
            megjegyzes: string;
       end;  // record

  TElofizetesInfoMulti = record
            dokodlista: string;
            dolgozonevlista: string;
            megjegyzes: string;
       end;  // record

  TMegbizasValtozasAkcio = (mbUJ, mbMODOSIT, mbTOROL);

  TEgyebDlg = class(TForm)
    SzuroGrid: TStringGrid;
	 SzGrid: TStringGrid;
    IndexGrid: TStringGrid;
    Query1: TADOQuery;
    Query2: TADOQuery;
    Query3: TADOQuery;
	 QueryKozos: TADOQuery;
    Querykoz2: TADOQuery;
    QueryAlap: TADOQuery;
    Queryal2: TADOQuery;
    ADOConnectionKozos: TADOConnection;
	 ADOConnectionAlap: TADOConnection;
	 QueryAl3: TADOQuery;
	 SGNavcenter: TStringGrid;
    IdHTTP1: TIdHTTP;
    QueryVevo: TADOQuery;
    QuerySzfej: TADOQuery;
    QuerySzfej2: TADOQuery;
    ADOConnectionNavCenter: TADOConnection;
    ADOQuery1: TADOQuery;
    ADODataSet1: TADODataSet;
    TSZFEJ: TADODataSet;
    TSZFEJSA_KOD: TStringField;
    TSZFEJSA_DATUM: TStringField;
    TSZFEJSA_KIDAT: TStringField;
    TSZFEJSA_ESDAT: TStringField;
    TSZFEJSA_TEDAT: TStringField;
    TSZFEJSA_VEVOKOD: TStringField;
    TSZFEJSA_VEVONEV: TStringField;
    TSZFEJSA_VEVAROS: TStringField;
    TSZFEJSA_VEIRSZ: TStringField;
    TSZFEJSA_VECIM: TStringField;
    TSZFEJSA_VEADO: TStringField;
    TSZFEJSA_VEBANK: TStringField;
    TSZFEJSA_VEKEDV: TBCDField;
    TSZFEJSA_OSSZEG: TBCDField;
    TSZFEJSA_AFA: TBCDField;
    TSZFEJSA_FIMOD: TStringField;
    TSZFEJSA_MEGJ: TStringField;
    TSZFEJSA_FLAG: TStringField;
    TSZFEJSA_MELL1: TStringField;
    TSZFEJSA_MELL2: TStringField;
    TSZFEJSA_MELL3: TStringField;
    TSZFEJSA_MELL4: TStringField;
    TSZFEJSA_JARAT: TStringField;
    TSZFEJSA_RSZ: TStringField;
    TSZFEJSA_POZICIO: TStringField;
    TSZFEJSA_PELDANY: TBCDField;
    TSZFEJSA_TIPUS: TBCDField;
    TSZFEJSA_UJKOD: TBCDField;
    TSZFEJSA_RESZE: TIntegerField;
    TSZFEJSA_NEMET: TIntegerField;
    TSZFEJSA_FEJL1: TStringField;
    TSZFEJSA_FEJL2: TStringField;
    TSZFEJSA_FEJL3: TStringField;
    TSZFEJSA_FEJL4: TStringField;
    TSZFEJSA_FEJL5: TStringField;
    TSZFEJSA_FEJL6: TStringField;
    TSZFEJSA_MELL5: TStringField;
    TSZFEJSA_HELYE: TIntegerField;
    TSZFEJSA_SORRE: TIntegerField;
    TSZFEJSA_FOKON: TStringField;
    TSZFEJSA_FEJL7: TStringField;
    TSZFEJSA_EGYA1: TStringField;
    TSZFEJSA_EGYA2: TStringField;
    TSZFEJSA_EGYA3: TStringField;
    TSZFEJSA_SAEUR: TIntegerField;
    TSZFEJSA_MEGRE: TStringField;
    TSZFEJSA_ATAD: TIntegerField;
    TSZFEJSA_ATDAT: TStringField;
    TSZFEJSA_ATNEV: TStringField;
    TSZFEJSA_ORSZA: TStringField;
    TSZFEJSA_MELL6: TStringField;
    TSZFEJSA_MELL7: TStringField;
    TSZFEJSA_ALLST: TStringField;
    TSZFEJSA_EUADO: TStringField;
    TSZFEJSA_TTTT: TStringField;
    TSZFEJSA_VEFEJ: TStringField;
    TSZFEJSA_VALNEM: TStringField;
    TSZFEJSA_VALOSS: TBCDField;
    TSZFEJSA_VALAFA: TBCDField;
    TSZSOR: TADODataSet;
    TSZSORSS_KOD: TStringField;
    TSZSORSS_SOR: TIntegerField;
    TSZSORSS_TERKOD: TStringField;
    TSZSORSS_TERNEV: TStringField;
    TSZSORSS_CIKKSZ: TStringField;
    TSZSORSS_ITJSZJ: TStringField;
    TSZSORSS_MEGY: TStringField;
    TSZSORSS_AFA: TBCDField;
    TSZSORSS_EGYAR: TBCDField;
    TSZSORSS_KEDV: TBCDField;
    TSZSORSS_VALNEM: TStringField;
    TSZSORSS_VALERT: TBCDField;
    TSZSORSS_VALARF: TBCDField;
    TSZSORSS_AFAKOD: TStringField;
    TSZSORSS_LEIRAS: TStringField;
    TSZSORSS_KULNEV: TStringField;
    TSZSORSS_KULLEI: TStringField;
    TSZSORSS_DARAB: TBCDField;
    TSZSORSS_KULME: TStringField;
    TSZSORSS_ARDAT: TStringField;
    TSZSORSS_ARFOL: TBCDField;
    TSZSORSS_UJKOD: TBCDField;
    TSZSORSS_NEMEU: TIntegerField;
    SQLDATE: TADOCommand;
    SQLAlap: TADOCommand;
    SQLKozos: TADOCommand;
    QueryAlap2: TADOQuery;
    QueryKozos2: TADOQuery;
    ADOConnectionServantes: TADOConnection;
    QueryServantes: TADOQuery;
    StoredProcServantes: TADOStoredProc;
    SQuery1: TADOQuery;
    QService: TADOQuery;
    QServiceSI_GKTIP: TStringField;
    QServiceSI_GKKAT: TStringField;
    QServiceSI_JELLEG: TStringField;
    QServiceSI_SERV1: TIntegerField;
    QServiceSI_SERVT: TIntegerField;
    QServiceSI_JELZ1: TIntegerField;
    QServiceSI_JELZT: TIntegerField;
    QServiceSI_KOD: TIntegerField;
    QService2: TADOQuery;
    QService2SH_KOD: TIntegerField;
    QService2SH_RENSZ: TStringField;
    QService2SH_DATUM: TStringField;
    QService2SH_SERVKM: TIntegerField;
    QService2SH_SERVINT: TIntegerField;
    QService2SH_SERVKM2: TIntegerField;
    QService2SH_JELZES: TIntegerField;
    QService2SH_MEGJ: TStringField;
    QService2SH_JELLEG: TStringField;
    ADOConnectionAutonom: TADOConnection;
    ADOConnectionTransaction: TADOConnection;
    _ppmUgras_proba: TPopupMenu;
    elefonelfizetsek1: TMenuItem;
    elefonszmlk1: TMenuItem;
    ADOConnectionKulcs: TADOConnection;
    Panel1: TPanel;
	function 	Jochar(szoveg:string; betu:Char; egesz,tizedes : integer) : Char;
	procedure 	FormCreate(Sender: TObject);
	procedure 	FormReturn( bill : WORD );
	procedure 	FileMutato( filename, cim, st1, st2, st3, st4 : string; orient : integer);
	procedure 	FileMutato2( filename, cim : string; fejlec, lablec : TStringList; orient : integer);
	function 	JoSzazalek(MaskEd : TMaskEdit) : Boolean;
	function 	VanAdat : boolean;
	function 	GetSzuroStr( szkod : string ) : string;
	procedure 	SetSzuroStr( szkod, szurostr : string );
	function 	GetTempFileName (ext : string = '') : string;
	procedure 	SeTADOQueryDatabase(sourcequery : TADOQuery );
	procedure 	SetJogok(dlgtag : integer; BUJB, BMOD, BTOR, BPRI, BNEZ : TBitBtn);
	function 	SQL_Tolto(Forras : TADOQuery; alapstr, orderstr : string; dlgtag : integer) : boolean;
	function 	RekordTorles(Forras : TADOQuery; alapstr, orderstr : string; dlgtag : integer;
					torlostr : string) : boolean;
	function CalendarBe( edit : TMaskEdit; MasodikDatum: boolean = False ) : boolean;
  function CalendarBe_idoszak(edit1, edit2: TMaskEdit): boolean;
	function 	Read_SZGrid( fokod, alkod : string) : string;
	procedure 	Fill_SZGrid;
  function 	Read_Szotar(fokod, alkod : string) : string;
  function Read_SzotarTechnikaiErtek(fokod, alkod : string): integer;
  procedure Write_Szotar(fokod, alkod, ertek: string);
  	procedure 	FormDestroy(Sender: TObject);
	function 	GetServerDate : TDateTime;
  	procedure  	Fill_IndexGrid;
  	procedure	Save_IndexGrid;
	procedure 	Fill_FieldGrid;
	procedure 	Save_FieldGrid;
  function 	ArfolyamErtek( valnem : string; datestr : string; kell : boolean; elozonap: boolean=False) : double;
  // function 	ArfolyamErtek( valnem : string; datestr : string; elozonap: boolean) : double;
	procedure 	ElsoNapEllenor(ma1, ma2 : TMaskEdit);
  procedure UtolsoNap(MD: TMaskEdit);
	function 	DateTimeToNum( datstr, timestr : string) : double;
	procedure 	DateTimeDifference( datstr1, timestr1, datstr2, timestr2 : string; var day, hour : integer);
	function 	GepkocsiErvEllenor(napok, kmek : integer): integer ;
	function 	DolgozoErvEllenor(napok, kmek : integer): integer ;
	function 	VevoErvEllenor(napok, kmek : integer): integer ;
	function 	HaviZaroKmEllenor(ev : string): integer ;
	function 	PozicioszamEllenor(ev : string): integer ;
  function  hutospotEllenor : integer;
  function  SzervizEllenor : integer;
	procedure 	Ervenyesseg(sql, mez1, mez2, szoveg : string);
  procedure JaratIdotartam;
	function 	JaratAtlag(jaratkod: string) : double;
	procedure 	WriteRowColor;
	procedure 	AlapToltes;
  procedure AlapadatToltes;
	procedure 	Arfolyambetoltes;
	procedure 	UzemanyagarBetoltes;
	procedure 	SqlInit;
	function 	GetFieldStr( szkod : string ) : string;
	procedure 	SetFieldStr( szkod, fieldstr : string );
  procedure WebBrowser1NavigateComplete2(Sender: TObject;
	   const pDisp: IDispatch; var URL: OleVariant);
  function GoogleMap(rendszam,rajt,cel,fel_le: string; teszt:Boolean): Boolean;
  function HutosGk(rendsz:string) : Boolean;
  function Szamla_Arf_szamol(ujkod, arfolyam: double) : Boolean;
  function UzemoraEllenorzes(jaratszam,rendszam,km: string): Boolean;
	function 	JaratKoltseg(jaratkod: string) : double;
  function  GetAtlagFogyasztas(kod: string) : double;
  function HetvegeVagyUnnep(datum: TDate) : Boolean;
  function WriteLogFile(FName:string; Text:string):Boolean;
  function AppendLog(LogName, LogText: string): Boolean;
  function Szamla_szam_datum_ell(szszam,szdatum: string; gyujto:boolean):Boolean;
  function Jarat_Egyeb_km(jakod: string): integer;
  function Szamlaszam_ell(kezd_dat: string = '2013.01.01.'): string;
  procedure PontozasEllenor;
  function Jarat_MBfajtak(jakod: string): string;
  function Jaratszam(jakod: string): string;
  function FormIsLoaded(sType : TClass): boolean;
  procedure DolgServantes;
  procedure Lejartszamlak(vevokod: string; kellhanincs: boolean; kmoral: string ='N');
   function VanServantes : boolean;
  function SepFrontToken(Sep, Miben: string): string;
  function SepRest(Sep, Miben: string): string;
  function GetSepListItem(Sep, Miben: string; PP:integer): string;  // first item: 1
  function SepListLength(Sep, Miben: string): integer;
  function Megbizas_valtozas(MBKOD: integer; MB_EUDIJ: double; Akcio: TMegbizasValtozasAkcio): TUtvonalInfo;
  procedure BeginTrans_ALAPKOZOS;
  procedure CommitTrans_ALAPKOZOS;
  procedure RollbackTrans_ALAPKOZOS;
  function ModifAktivalva(DevCode: string): boolean;
  function GetDolgozoSMSKontakt(DOKOD: string): string;
  function GetDolgozoEmailKontakt(DOKOD: string): string;
  function GetDolgozoKontakt(DOKOD: string; ForceSMS: boolean): string;
  function GetDolgozoICloudKontakt(const DOKOD: string): string;
  function ErvenyesEmailKontakt(kontakt: string): boolean;
  function ErvenyesSMSEagleKontakt(kontakt: string): boolean;
  function GetPartnerSMSKontakt(VEKOD: string): string;
  function GetPartnerEmailKontakt(VEKOD: string): string;
  function JSONArray_DuplikacioTorol(ArrayBe: string): string;
  function StringLista_DuplikacioTorol(ListaBe, Sep: string): string;
  function GetDolgozoSMSEagleCimzett(DOKOD: string; ForceSMS: boolean): string;
  function GetElofizetesInfo(Telefonszam: string): TElofizetesInfo;
  function GetElofizetesInfo_Idosoros(const Telefonszam, Mikor: string): TElofizetesInfo;
  function GetElofizetesInfo_Idoszak(const Telefonszam, DatumTol, DatumIg: string): TElofizetesInfoMulti;
  procedure DolgozoFotoMutato(DOKOD: string; Control: TControl);
  function ConnectKulcs : boolean;
  function DisconnectKulcs : boolean;
  procedure Fill_Localfields;
  procedure DelFilesFromDir(Directory, FileMask: string; DelSubDirs: Boolean);
  function FTP_Upload(const AediFileName, AftpHost, AftpUser, AftpPassword, AftpDir: string; const AftpPort: integer): string;
  procedure MegbizasDebug(const mbkod, comment: string);
  procedure DuplaTankolasKezeles(const Rendszam: string; const Km: integer);
  function FuvarosMost: string;
  function GetFelettesEmail(TERULET: string): TStringList;
  function MerciPozicioJelentesKuld (MBKODLIST: TStringList; dokod: string; var Szoveg: string): string;
  function MerciPozicioJelentesHTML (MBKOD: integer): string;
  function IsMegbizasStandTailer(const MBKOD: integer; var AruLeiras: string): boolean;
  function StoreMegbizasLevel(const MBKOD, ACimzett, ASzoveg: string): boolean;
  function Mintailleszt(const AKeresMinta, AMiben: string): boolean;
  function NoEkezet(const AInput: string): string;
  function GetTruckpitAliasList(const DOKOD: string): TTruckpitAliasInfoArray;
	public
		CurDispatch			: IDispatch; {save the interface globally }
		globalini			: string;
		localini			: string;
		EvSzam				: string;
		DBEvSzam				: string;
     	MaiDatum			: string;
		kezdofej			: boolean;
		TempPath			: string;
    DocumentPath : string;  // X:\9_Fuvaros
    PartnerFolder: string;  // Partnerek
    ExePath			: string;
     	HelpPath			: string;
     	// DataPath			: string;		// A k�z�st t�blater�let helye
     	// AlapPath			: string;		// Az egyedi t�blater�let helye
   	BackUpPath			: string;		//
		TempList			: string;
    NaploPath : string;
		Focim				: string;
   	Alcim				: string;
    JS_ALNYOMAT   : string;
		vanvonal			: boolean;
		user_name			: string;
		user_code			: string;
    user_dokod			: string;
		user_super			: boolean;
    // user_kellinfoablak: boolean;
    Nyomtathato			: boolean;		// Ha �rt�ke FALSE, a Folista nem nyomtat
		Saveable			: boolean;
		VoltNyomtatas		: boolean;
		Szamlanyom			: boolean;		// Csak egyetlen p�ld�ny nyomtathat�
		CtrlGomb			: boolean;		// A v�gleges�tett elemek m�dos�t�s�hoz sz�ks�ges CTRL-F11 jelz�je
		CtrlGomb2			: boolean;		// A p�ld�nysz�m vissza�ll�t�s�hoz CTRL-F12 jelz�je
		log_level			: integer;		// Logol�si szint (0 - semmi; 1 - csak insert/update; 2 - select is)
		log_memory  	: integer;		// Szabad mem�ria logol�si szint (0 - nem; 1 - igen)
		Valutanem			: string;
		Arfolyam			: double;
		ellenlista			: TStringList;
		Peldanyszam			: integer;
		Kelllistagomb		: boolean;		// A FOLISTA k�perny�n megjelenjen-e egy plusz nyom�gomb
		ListaGomb			: string;		// Az �j nyom�gomb caption-je
		Kelllistagomb2		: boolean;     // A FOLISTA k�perny�n megjelenjen-e egy m�sodik nyom�gomb
		ListaGomb2			: string;      // Az �j nyom�gomb caption-je
		Kelllistagomb3		: boolean;     // A FOLISTA k�perny�n megjelenjen-e egy harmadik nyom�gomb
		ListaGomb3			: string;      // Az �j nyom�gomb caption-je
		ListaValasz 		: integer;     // Melyik �j nyom�gomb lett megnyomva
		v_nemzetkozi	  	: boolean;		// Sz�r�s nemzetk�zi dolgoz�kra
		v_kulsosok		  	: boolean;		// Sz�r�s k�ls�s dolgoz�kra
  		v_archivak		  	: boolean;		// Sz�r�s k�ls�s dolgoz�kra
  		v_nemzetkozi2	  	: boolean;		// Sz�r�s nemzetk�zi g�pkocsikra
  		v_kulsosok2		  	: boolean;		// Sz�r�s k�ls�s g�pkocsikra
		v_archivak2		  	: boolean;		// Sz�r�s k�ls�s dolgoz�kra
    v_megbizas_default_ful: integer; // melyik f�l legyen alap�rtelmezett
		combo_autocomplete_delay: integer;		// Milyen gyorsan kell g�pelni (ms)
		folista_zoom		: integer;		// A list�k megjelen�t�si % -a
		row_color			: integer;		// A t�bl�zat kiemel�seinek sz�ne
		Export_Available  	: boolean;		// Exportol�si lehet�s�g a panelokra
		v_alap_db			: string;
		v_koz_db			: string;
		v_nav_db			: string;
		v_evszam			: string;
		keresheto			: boolean;
		megbizaskod			: string;
		kilepes			  	: boolean;
		felrakas_str		: string;
		lerakas_str			: string;
		userini			  	: string;
		klikkeles			  : TNotifyEvent;
		v_formatum			: integer;		// A form�tum sz�ma a c�g sz�tv�laszt�shoz
		v_szamlaformatum: integer;		// A form�tum sz�ma a sz�mla k�lalalk m�dos�t�s�hoz
		import_dat			: string;		// Adatb�zis importhoz a forr�s dat neve
		import_koz			: string;		// Adatb�zis importhoz a forr�s koz neve
		torles_jakod		: string;
		VANLANID			  : integer;		// Kell haszn�lni a fuvart�bla adatokat : 0 - nem; 1 - igen
		parameter_str		: string;		// Param�terek �tad�s�ra haszn�lt mez� (pl. CSATOLMANYFM-n�l)
    MATRIXAPIKEY    : string;
    UJ_DK_SZAMLA_TOL: string;
    MobilAppTelszam, MobilAppJelszo: string;  // Truckpit el�r�shez
    TimerTruckpitTelszam, TimerTruckpitJelszo: string;  // Fuvar_Timer Truckpit el�r�shez

    MERCEDES_POZICIO_CIMZETTEK: string; // nekik kell k�ldeni a W�rth-i j�ratr�l helyzetjelent�st
    MERCEDES_POZICIO_TARGY: string; // a email t�rgya
    LEJARTSZAMLA_LEVEL_ENABLED: boolean;
    figy_megbizas_lejartszamla: boolean;  // legyen-e lej�rt sz�mla figyel�s a megb�z�s r�gz�t�skor
    figy_megbizas_uzemanyag_ar: boolean;  // legyen-e �zemanyag �r figyel�s a megb�z�s r�gz�t�skor
    fizlist_csehtran: boolean; // legyen-e a fizet�si list�n a "cseh tarnzit" �sszeg
    fizlist_lejart_okmany: boolean; // legyen-e a fizet�si list�n a "lej�rt okm�nyok" sor
    szamla_devizanem_valtas_engedelyez: boolean;  // eredetileg HUF sz�ml�t lehet-e EUR-k�nt kinyomtatni
    KONYVELES_ATAD_FORMA: string;
    DOLGOZO_ATAD_FORMA, DOLGOZO_ATAD_PREFIX: string;
    MEGENGEDO_SZAMLA_ARFOLYAM_ELLENORZES: boolean;
    SMS_KULDES_FORMA, SMS_KULDES_CHARSET, TELEPFELKOD, SMSEAGLE_DOMAIN: string;
    SMS_DIRECT_SENDING_LIMIT: integer;
    SMS_DIRECT_SENDING_IMMEDIATE: boolean;
    TANKOLAS_KM_FIGYELEMBE_VETEL: boolean;
    TRUCKPIT_SMS_PARHUZAMOSAN: boolean;
    SMSEAGLE_IP, SMSEAGLE_USER, SMSEAGLE_PASSWORD: string;
    ALAPERTELMEZETT_KERESESI_MOD: integer;
    GYORSHAJTAS_KULDES_KEZDETE: string;
    GPS_KALKULACIOK_HASZNALATA: boolean;
    UTVONAL_KULSO_BONGESZOVEL: boolean;
    MINDEN_KULSO_BONGESZOVEL: boolean;
    UZENET_VALASZ_SZAM: boolean;  // lehessen-e kiv�lasztani �zenet k�ld�skor v�lasz sz�mot
    EKAER_FIGYELMEZTETES_ENABLED: boolean;
    MEGBIZAS_DEBUG_ENABLED: boolean;
    MOZGOBER_NAPTARI_EVRE: boolean;  // ha True, egy teljes itt t�lt�tt �v ut�n lesz csak 1 �ves mozg�b�r. Ha valaki 2016 szeptemberben j�n, akkor csak 2018 janu�rt�l fog 1 �ves mozg�b�rt kapni, eg�sz 2018-ban. �v k�zben nincs v�lt�s.
                                     // ha False, 12 itt t�lt�tt h�nap ut�n m�r j�r az 1 �ves mozg�b�r
    SMS_ROUTING_HASZNALATA: boolean;  // v�laszthatunk az SMSEagle SIM k�rty�i k�z�tt
    UTSZAKASZ_MEGJELENITES_HASZNALATA: boolean;
    SIESSEN_SEBESSEG, ELKESETT_SEBESSEG: integer;
    SQL_TIMEOUT: integer;
    FIZETESI_JEGYZEK_BEMENET, FIZETESI_JEGYZEK_ELKULDVE: string;
    SMS_MASOLAT_ICLOUD_HASZNALATA: boolean;  // a dolgoz�i SMS-eket a megfelel� iCloud c�mre is kik�ldj�k-e
    ICLOUD_EMAIL_PREFIX, ICLOUD_EMAIL_POSTFIX: string;  // az iCloud c�m k�pz�s�hez haszn�lt pre- �s postfix, pl. 'js'  -->  js209588785@icloud.com
    IRODAI_CSOPORT_LISTA: string;  // azon dolgoz�i csoportok list�ja, amelybe irodai dolgoz�k tartoznak
    SMTP_HOST: string;
    TRUCKPIT_BASE_URL: string;
    S3_BASE_URL: string;
    FieldGrid			  : TStringGrid;
    NavC_Kapcs_OK   : Boolean;
    TESZTDB,TESZTDAT:string;
    nemetfejlec, AUTOSZAMLA: Boolean;
    stralap: boolean;
    MBujtetel: boolean;
    TIZEDESJEL:string;
	  GK_HUTOS: string;
    ATLAG_KM_ORA: integer;
    TESZTPRG: boolean;
    DATUMIDOELL: boolean;

    function IsFormOpen(const FormName : string): Boolean;
    procedure SetMaidatum;
    function  JaratkoltsegHUF(jakod: string): double;
    function  JaratkoltsegEUR(jakod: string): double;
    function Szamlaertek2jar_megb(sujkod,sakod: string): boolean;    // �sszeg, sz�mlasz�m vissza�r�sa a Viszonyba �s a megb�z�sba.
    function GkTipus(rendsz: string): string;
    function TeljesDijSzamitas_mag(MBKOD: integer; datum, vevokod: string; OsszKM, AtallasKm: double;
              Fuvardij, EgyebDij, Aldij: double; FuvarDeviza, EgyebDeviza, Aldeviza: string) :T_TeljesDijSzamitasRec;
    function GetCsoportFuvardij(MBKOD: integer): double;
    function GetCsoportAlvallakozoiDij(MBKOD: integer): double;
    function UtvonalSzamitas_megbizas(MBKOD: integer): TUtvonalInfo;
    function UtvonalSzamitas_megseged(MS_ID1, MS_ID2: integer): TUtvonalInfo;
    function UtvonalSzamitas_mag(SQL, CsoportInfo: string; MBKOD: integer): TUtvonalInfo;
    function UtvonalSzamitas_mag_v2(SQL, CsoportInfo: string; MBKOD: integer): TUtvonalInfo;
    function UtvonalSzamitas_mag_v3(SQL, CsoportInfo: string; MBKOD: integer): TUtvonalInfo;
    function UtvonalSzamitas_simple(Lat1, Long1, Lat2, Long2: double): integer;
    function Cimsorrend_beallit(CimLista: TList): boolean;
    function GetFelrakoLerako(MBKOD: integer): TFelrakLerak;
    function GetFuvarosSzin(Szinkod: string): TColor;
    function SzovegFilter(S, MitCsinal: string): string;
    // procedure UzenetKuldesDolgozonak(dokod: string);
    function Telephely_e(const Nev, Orszag, ZIP, Varos, UtcaHazszam: string): boolean;
    function RakodoNevFormat(const EredetiNev: string): string;
    procedure MyShellExecute(const CommandLine: string);
    function GetMobilAppTelszam: string;
    function GetMobilAppJelszo: string;
  private
    function GetReszmegbizasCsop(MBKODLista: TStringList): integer; overload;
    function GetReszmegbizasCsop(MBKOD: integer): integer; overload;
    function GetReszmegbizasCsop_elemek(CSOP: integer): TStringList;
    function Reszmegbizas_add(CSOP, MBKOD: integer): string;
    function Reszmegbizas_remove(MBKOD: integer): string;
    procedure Utinfo_csoportos_update(MBLista: TStringList; ActMBKOD: integer; UTINFO: TUtvonalInfo; MB_EUDIJ: double);

end;

// Glob�lis v�ltoz�k
var
	EgyebDlg: TEgyebDlg;


const
 // NULLA = 0;
	ID_ANYAGTIPUS 		= '100';
	URESSTRING     		= '                                                                                                    ' +
						  '                                                                                                    ';
	VONALSTRING     	= '----------------------------------------------------------------------------------------------------' +
						  '----------------------------------------------------------------------------------------------------';
	Szamok: szamnev 	= ('nulla','egy','kett�','h�rom','n�gy','�t','hat','h�t','nyolc','kilenc');
	Tizek: szamnev 		= ('nulla','tizen','huszon','harminc','negyven','�tven','hatvan','hetven','nyolcvan','kilencven');
	Ezrek: szamnev 		= ('','ezer','milli�','milli�rd','billi�','billi�rd','trilli�','trilli�rd','ezertrilli�','ezertrilli�rd');
	NO_MENU_SHOW		= FALSE;  		// A men�k l�that�ak legyenek ha nem enged�lyezettek -> enable = false k�l�nben visible = false
{	DAT_TABLAK			= 'ALJARAT, ALSEGED, CEGADAT, JARAT, JARPOTOS, JARSOFOR, KOLTSEG, MEGBIZAS, MEGBIZAS2, MEGLEVEL, MEGSEGED, MEGSEGED2, MEGTETEL, '+
						  'SABLON, SULYOK, SZFEJ, SZFEJ2, SZSOR, SZSOR2, T_GEPK, T_KOLT, T_POTK, TEHER, VISZONY, ZAROKM, CMRDAT, FUVARTABLA,'+
						  'CSATOLMANY, JARATMEGBIZAS,JARATMEGSEGED, MBCSATOL, TAVOLLET, KOLTSEG_GYUJTO, KMOZGAS, KESZLET, PALETTA, PALVALT, '+
                         'JARATKOLTSEG, MEGBIZASRESZ, UTOLSOKOD, NVCKIVETEL, FILEMUTAT, URLPUFFER, RENDKIVULI_ESEMENY, SMS_TECH, '+
                         ' SMSUZENETEK, SMSSZALAK, SZERVIZMUNKALAP, TELELOFIZETES, TELKESZULEK, TELHIST, TELSZAMLAK ';

	KOZ_TABLAK			= 'ALVALLAL, ARFOLYAM, DIALOGS, DOLGOZO, DOLGOZOERV, DOLGOZOMEG, DOLGOZOCSOP, DOLGOZOFOTO, FELRAKO, GARANCIA, GEPKOCSI,  KATINTER,'+
						  'LIZING, MEZOK, POZICIOK, SHELL, SZOTAR, TELEFON, TERMEK, UZEMANYAG, UZEMERT, UZENET, VEVO, VEVODIJ, '+
						  'ALGEPKOCSI, TETANAR, KMCHANGE, DKVKARTYA, UZEMFIX, GK_KATEG, NAPOK, SZABADSAG';
 }
 // bet�rendben, egyben
 DAT_TABLAK			= 'ALJARAT, ALGEPKOCSI, ALSEGED, ALVALLAL, ARFOLYAM, ATJELENT, CEGADAT, CMRDAT, CSATOLMANY, '+
              'DIALOGS, DKVKARTYA, DOLGOZO, DOLGOZOERV, DOLGOZOMEG, DOLGOZOCSOP, DOLGOZOFOTO, '+
              'FELRAKO, FILEMUTAT, FUVARTABLA, GARANCIA, GEPKOCSI, GK_KATEG, GKSZAKASZ, GKSZAKASZ_HIST, '+
              'JARAT, JARATKOLTSEG, JARATMEGBIZAS, JARATMEGSEGED, JARPOTOS, JARSOFOR, '+
              'KATINTER, KESZLET, KMCHANGE, KMOZGAS, KOLTSEG, KOLTSEG_GYUJTO, KUTALLAPOT, LIZING, '+
              'MBCSATOL, MEGBIZAS, MEGBIZAS2, MEGBIZASRESZ, MEGLEVEL, MEGRENDELO, MEGSEGED, MEGSEGED2, MEGTETEL, MEZOK, '+
              'NAPOK, NVCKIVETEL, PALETTA, PALVALT, PONTOK, PONTOZO, POZICIOK, RENDKIVULI_ESEMENY, RENDSZERKOD, '+
						  'SABLON, SERVICE, SERVICE_HIS, SHELL, SMS_TECH, SMSUZENETEK, SMSSZALAK, SULYOK, '+
              'SZABADSAG, SZAMLALEVEL, SZERVIZMUNKALAP, SZFEJ, SZFEJ2, SZSOR, SZSOR2, SZOTAR, '+
              'TAVOLLET, TELELOFIZETES, TELKESZULEK, TELHIST, TELKIEG, TELSZAMLAK, TEHER, TERMEK, TETANAR, '+
              'UNNEP, URLPUFFER, UTEMEZETT, UTOLSOKOD, UZEMANYAG, UZEMERT, UZEMFIX, UZENET, '+
              'VEVO, VEVODIJ, VEVOIRAT, VISZONY, ZAROKM';
 KOZ_TABLAK		= '';

	SW_VERZIO		   	= '4.22.7';      	// Verzi�sz�m

	QUERY_KOZOS_TAG		= 0;
	QUERY_ADAT_TAG		= 1;
  QUERY_AUTONOM_TAG		= 2;
  QUERY_TRANSACTION_TAG	= 3;
   PLUSZNAP= 10;
  //  JSTELEPFELKOD       = '24680';      // A telephely felrak� k�dja  --> SZ�T�RBA!!!!!!
   ATEGOTIP   : string = '[3,5],[3,5B],[6],[6B],[4,5],[15]';

implementation

uses
//	Arfolybe,
	Folista, ArfolyamTolto , Kozos_Local, J_DataFunc, Lgauge, J_Fogyaszt, Email,
  Info_, Terkep, Terkep2, Math, ADOInt, Lejartszamla, GoogleDistance, Googlepoints, GeoAddress,
  DolgozoFotoMutat, System.RegularExpressions, UzenetSzerkeszto, GeoRoutines;
{$R *.DFM}

procedure TEgyebDlg.PontozasEllenor;
var
   moddat      : string;
   vanrec      : boolean;
   db          : integer;
begin
   moddat := copy(EgyebDlg.MaiDatum, 1, 8)+'01.';
   moddat := DatumHoznap(moddat, -1, true);
   moddat := copy(moddat, 1, 8)+'01.';
   EllenlistTorol;
   EllenListAppend('', false);
   EllenListAppend(copy(moddat, 1, 5) + ' ' + FormatSettings.LongMonthNames[StrToIntDef(copy(moddat, 6,2), 1)]+' h�napban nem pontozott dolgoz�k:', false);
   EllenListAppend('=================================================', false);
   // Pontoz�sok ellen�rz�se
   vanrec := false;
   db := 1;
   // if Query_Run (QueryKozos, 'SELECT DISTINCT DC_DOKOD, DO_NAME FROM PONTOZO, DOLGOZOCSOP, DOLGOZO '+
   //    ' WHERE DC_CSKOD = PO_ALKOD AND DO_KOD = DC_DOKOD AND DO_ARHIV = 0 AND PO_FEKOD = '''+user_code+''' '+
   if Query_Run (QueryKozos, 'SELECT DISTINCT PO_DOKOD, DO_NAME FROM PONTOZO, DOLGOZO '+
       ' WHERE PO_DOKOD=DO_KOD AND DO_ARHIV = 0 AND PO_FEKOD = '''+user_code+''' '+
       ' AND PO_DOKOD NOT IN (SELECT PP_DOKOD FROM PONTOK WHERE PP_DATUM = '''+moddat+''' '+
       ' AND PP_FEKOD = '''+user_code+''' )'+
       ' AND DO_BELEP<'''+moddat+''' '+    // csak az azel�tt bel�pett dolgoz�kat
       ' AND convert(int,DO_KOD) not in (select isnull(JE_DOKOD,0) from JELSZO where JE_FEKOD = '+EgyebDlg.user_code+')'+  // saj�t magunkat nem l�tjuk
       ' ORDER BY DO_NAME', true) then begin
       while not QueryKozos.Eof do begin
           EllenListAppend(Format('%6d. ', [db])+QueryKozos.FieldByName('DO_NAME').AsString, false);
           Inc(db);
           QueryKozos.Next;
           vanrec := true;
       end;
   end;
   if vanrec then begin
       EllenListMutat('Pontoz�si hi�nyoss�gok', false);
   end;
   EllenListtorol;
end;

procedure TEgyebDlg.FormCreate(Sender: TObject);
var
	Inifn			: TIniFile;
begin
	log_level		:= 0;
  log_memory	:= 0;
	EvSzam			:= FormatDateTime('yyyy',Date);
	Nyomtathato	:= true;
	CtrlGomb	  := false;
	kilepes			:= false;


  ExePath 		:= ExtractFilePath(Application.ExeName);
	HelpPath 		:= ExtractFilePath(Application.ExeName)+'HELP\';
	if not DirecToryExists(ExePath+'INI') then begin
		ForceDirectories(ExePath+'INI');
	end;

	{ 2018.02.26. kikommenteztem, szerintem ez egy r�gi migr�ci�.
  localini		:= 'C:\FUVARINI\L_' + ChangeFileExt( ExtractFileName(Application.ExeName), '.INI' );
	if not FileExists(localini) then begin
		localini	:= GetEnvironmentVariable('TEMP') + '\L_' + ChangeFileExt( ExtractFileName(Application.ExeName), '.INI' );
	end;
	globalini		:= ExePath+'INI\'+ChangeFileExt( ExtractFileName(Application.ExeName), '.INI' );
	if not fileExists(globalini) then begin
		// A r�gi ini �tm�sol�sa az �j helyre
		CopyFile(TempPath + ChangeFileExt( ExtractFileName(Application.ExeName), '.INI' ), globalini);
	end;
  }
  globalini		:= ExePath+'INI\'+ChangeFileExt( ExtractFileName(Application.ExeName), '.INI' );
	Inifn 				:= TIniFile.Create( globalini );
  DocumentPath:= IncludeTrailingPathDelimiter(Inifn.ReadString( 'GENERAL', 'DOCUMENT_ROOT',''));
  PartnerFolder:= Inifn.ReadString( 'GENERAL', 'PARTNER_DIR', 'Partnerek');
  if DocumentPath = '' then begin
    NoticeKi('Hi�nyz� alapbe�ll�t�s! (DocumentPath)', NOT_ERROR);
    Application.Terminate;
    end;
  // DataPath	    := '';
	// TempPath 		:= ExtractFilePath(Application.ExeName);
  TempPath:= DocumentPath;  // mindenf�l�t ide csin�ljon ezent�l

	ellenlista		:= TStringList.Create;
	if not DirecToryExists(TempPath+'LIST') then begin
		ForceDirectories(TempPath+'LIST');
	end;
	TempList:= TempPath+'LIST\';						// Az ideiglenes lista �llom�nyok helye

 	if not DirecToryExists(DocumentPath+'LOG') then begin
		ForceDirectories(DocumentPath+'LOG');
	end;
  NaploPath := DocumentPath+'LOG\';						// A napl� �llom�nyok helye

	kezdofej 				:= true;
	user_name 				:= '';
	user_code 				:= '';
	user_super				:= false;
  ALAPERTELMEZETT_KERESESI_MOD:= 1;  // a r�gi m�d
	VoltNyomtatas			:= false;
	SzamlaNyom				:= false;
	Saveable   				:= false;
	megbizaskod				:= '';
	SzuroGrid.RowCount 		:= 1;
	SzuroGrid.Cells[0,0]	:= '';
	SzuroGrid.Cells[1,0]	:= '';
 	RegisterPreviewClass(TQRMDIPreviewInterface);	// A Quickreport View beregisztr�l�sa
	// A f�c�mek beolvas�sa

	FOCIM 				:= Inifn.ReadString( 'GENERAL', 'LABEL','');
	ALCIM 				:= Inifn.ReadString( 'GENERAL', 'LABEL2','');
  JS_ALNYOMAT   := Inifn.ReadString( 'SZAMLA', 'JS_ALNYOMAT','1');
  UJ_DK_SZAMLA_TOL:= Inifn.ReadString( 'SZAMLA', 'UJ_DK_SZAMLA_TOL','1');
	TESZTDB 			:= Inifn.ReadString( 'IMPORT', 'TESZTDB','');
	TESZTDAT 			:= Inifn.ReadString( 'IMPORT', 'TESZTDAT','');
	log_level			:= StrToIntDef(Inifn.ReadString( 'GENERAL', 'DEBUG','0'),0);
  log_memory			:= StrToIntDef(Inifn.ReadString( 'GENERAL', 'LOG_FREE_MEM','0'),0);

	VANLANID			:= StrToIntDef(Inifn.ReadString( 'GENERAL', 'VAN_LANID','0'),0);
  MATRIXAPIKEY  := Inifn.ReadString( 'GENERAL', 'MATRIXAPIKEY','');
  LEJARTSZAMLA_LEVEL_ENABLED := (Inifn.ReadString( 'FUNKCIOK', 'LEJARTSZAMLA_LEVEL','1') = '1');
  figy_megbizas_lejartszamla := (Inifn.ReadString( 'FUNKCIOK', 'FIGY_MEGBIZAS_LEJARTSZAMLA','1') = '1');
  figy_megbizas_uzemanyag_ar := (Inifn.ReadString( 'FUNKCIOK', 'FIGY_MEGBIZAS_UZEMANYAG_AR','1') = '1');
  fizlist_csehtran := (Inifn.ReadString( 'FUNKCIOK', 'FIZLIST_CSEHTRAN','1') = '1');
  szamla_devizanem_valtas_engedelyez := (Inifn.ReadString( 'FUNKCIOK', 'SZAMLA_DEVIZANEM_VALTAS_ENGEDELYEZ','1') = '1');
  fizlist_lejart_okmany := (Inifn.ReadString( 'FUNKCIOK', 'FIZLIST_LEJART_OKMANY','1') = '1');
  MEGENGEDO_SZAMLA_ARFOLYAM_ELLENORZES := (Inifn.ReadString( 'FUNKCIOK', 'MEGENGEDO_SZAMLA_ARFOLYAM_ELLENORZES','1') = '1');
  KONYVELES_ATAD_FORMA := Inifn.ReadString( 'EXPORT', 'KONYVELES_ATAD_FORMA','SERVANTES');
  DOLGOZO_ATAD_FORMA := Inifn.ReadString( 'EXPORT', 'DOLGOZO_ATAD_FORMA','SERVANTES');
  DOLGOZO_ATAD_PREFIX := Inifn.ReadString( 'EXPORT', 'DOLGOZO_ATAD_PREFIX','JS');
  SMS_KULDES_FORMA := Inifn.ReadString( 'FUNKCIOK', 'SMS_KULDES_FORMA','TELENORMAIL'); // hogyan k�ld�nk SMS-t a sof�rnek
  SMS_KULDES_CHARSET := Inifn.ReadString( 'FUNKCIOK', 'SMS_KULDES_CHARSET','ASCII'); // k�ld�nk-e Unicode SMS-t
  SMSEAGLE_DOMAIN := Inifn.ReadString( 'FUNKCIOK', 'SMSEAGLE_DOMAIN',''); // alapb�l az IP c�me
  SMS_DIRECT_SENDING_LIMIT := StrToInt(Inifn.ReadString( 'FUNKCIOK', 'SMS_DIRECT_SENDING_LIMIT','1000')); // h�ny karaktert�l ne haszn�lja a MailTo-t
  SMS_DIRECT_SENDING_IMMEDIATE := (Inifn.ReadString( 'FUNKCIOK', 'SMS_DIRECT_SENDING_IMMEDIATE','0') = '1'); // El is k�ldje, vagy csak megjelen�tse
  TANKOLAS_KM_FIGYELEMBE_VETEL := (Inifn.ReadString( 'FUNKCIOK', 'TANKOLAS_KM_FIGYELEMBE_VETEL','0') = '1'); // A k�lts�g feldolgoz�sn�l a telepi tankol�s km-�t is egyeztesse-e
  TRUCKPIT_SMS_PARHUZAMOSAN := (Inifn.ReadString( 'FUNKCIOK', 'TRUCKPIT_SMS_PARHUZAMOSAN','0') = '1'); // Az �t�ll�s idej�n menjen ki SMS-ben is az �zenet
  GYORSHAJTAS_KULDES_KEZDETE := Inifn.ReadString( 'GYORSHAJTAS', 'GYORSHAJTAS_KULDES_KEZDETE','2100.01.01.'); // E nap el�tti gyorshajt�sokat nem k�ldj�k ki (ut�lag, ugye)
  GPS_KALKULACIOK_HASZNALATA := (Inifn.ReadString( 'FUNKCIOK', 'GPS_KALKULACIOK_HASZNALATA','0') = '1'); // Sz�moljunk-e a felrak�-lerak� GPS poz�ci�kkal
  SMS_ROUTING_HASZNALATA := (Inifn.ReadString( 'FUNKCIOK', 'SMS_ROUTING_HASZNALATA','0') = '1'); // v�laszthatunk az SMSEagle SIM k�rty�i k�z�tt
  UTVONAL_KULSO_BONGESZOVEL := (Inifn.ReadString( 'FUNKCIOK', 'UTVONAL_KULSO_BONGESZOVEL','0') = '1'); // FTerkep2 helyett k�ls� b�ng�sz� mutatja az �tvonalat (a TWebBrowser-nek kompatibilit�si gondjai vannak)
  MINDEN_KULSO_BONGESZOVEL := (Inifn.ReadString( 'FUNKCIOK', 'MINDEN_KULSO_BONGESZOVEL','0') = '1'); // FTerkep2 helyett k�ls� b�ng�sz� mutat mindent (a TWebBrowser-nek kompatibilit�si gondjai vannak)
  UZENET_VALASZ_SZAM := (Inifn.ReadString( 'FUNKCIOK', 'UZENET_VALASZ_SZAM','0') = '1'); // �zenet k�ld�sn�l lehessen v�lasz sz�mot megadni
  MOZGOBER_NAPTARI_EVRE := (Inifn.ReadString( 'FUNKCIOK', 'MOZGOBER_NAPTARI_EVRE','1') = '1'); //
  EKAER_FIGYELMEZTETES_ENABLED := (Inifn.ReadString( 'FUNKCIOK', 'EKAER_FIGYELMEZTETES_ENABLED','1') = '1');
  MEGBIZAS_DEBUG_ENABLED := (Inifn.ReadString( 'FUNKCIOK', 'MEGBIZAS_DEBUG_ENABLED','0') = '1');
  UTSZAKASZ_MEGJELENITES_HASZNALATA := (Inifn.ReadString( 'FUNKCIOK', 'UTSZAKASZ_MEGJELENITES_HASZNALATA','0') = '1'); // megjelenjen-e az "�tszakaszok"
  SMS_MASOLAT_ICLOUD_HASZNALATA := (Inifn.ReadString( 'FUNKCIOK', 'SMS_MASOLAT_ICLOUD_HASZNALATA','0') = '1'); // a dolgoz�i SMS-eket a megfelel� iCloud c�mre is kik�ldj�k
  ICLOUD_EMAIL_PREFIX := Inifn.ReadString( 'FUNKCIOK', 'ICLOUD_EMAIL_PREFIX',''); // az iCloud c�m k�pz�s�hez haszn�lt prefix, pl. 'js'  -->  js209588785@icloud.com
  ICLOUD_EMAIL_POSTFIX := Inifn.ReadString( 'FUNKCIOK', 'ICLOUD_EMAIL_POSTFIX','@icloud.com');
  FIZETESI_JEGYZEK_BEMENET := Inifn.ReadString( 'FIZETESI_JEGYZEK', 'BEMENET',''); // hol keress�k a k�ldend� Kulcs Fizet�si Jegyz�k dokumentumokat
  FIZETESI_JEGYZEK_ELKULDVE := Inifn.ReadString( 'FIZETESI_JEGYZEK', 'ELKULDVE',''); // hov� mozgassuk az elk�ld�tt Kulcs Fizet�si Jegyz�k dokumentumokat
  SQL_TIMEOUT:= StrToIntDef(Inifn.ReadString('GENERAL', 'SQL_TIMEOUT', '30'), 30); // SQL timeout m�sodpercben

  SMSEAGLE_IP:= Inifn.ReadString('SMSEAGLE', 'SMSEAGLE_IP','');
  SMSEAGLE_USER:= DecodeString(Inifn.ReadString('SMSEAGLE', 'SMSEAGLE_USER',''));
  SMSEAGLE_PASSWORD:= DecodeString(Inifn.ReadString('SMSEAGLE', 'SMSEAGLE_PASSWORD',''));

  TimerTruckpitTelszam:= DecodeString(Inifn.ReadString('TRUCKPIT', 'FUVARTIMER_TELSZAM',''));
  TimerTruckpitJelszo:= DecodeString(Inifn.ReadString('TRUCKPIT', 'FUVARTIMER_JELSZO',''));
  TRUCKPIT_BASE_URL:= Inifn.ReadString('TRUCKPIT', 'TRUCKPIT_BASE_URL','https://truckpit.novel.hu');  // https://soforbackend.herokuapp.com
  S3_BASE_URL:= Inifn.ReadString('TRUCKPIT', 'S3_BASE_URL','');  //

  MERCEDES_POZICIO_CIMZETTEK:= Inifn.ReadString('POZICIO_KULDES', 'WORTH','');
  MERCEDES_POZICIO_TARGY:= Inifn.ReadString('POZICIO_KULDES', 'WORTH_TARGY','');
  SMTP_HOST:= Inifn.ReadString('EMAIL', 'SMTP','sbs.jsspeed.local');

	Inifn.Free;
	FieldGrid			:= TStringGrid.Create(self);
   FieldGrid.ColCount	:= 2;
   FieldGrid.RowCount	:= 1;



end;

function 	TEgyebDlg.Jochar(szoveg:string; betu:Char; egesz,tizedes : integer) : Char;
begin
	Jochar := Chr(0);
	if ( ( ( betu >= '0' ) and ( betu <= '9' ) ) or ( betu = FormatSettings.DecimalSeparator )
		or ( betu < Chr(32) ) or ( betu = '-' ) or ( betu = '*' ) ) then begin
		Jochar := betu;
		if ( ( betu = FormatSettings.DecimalSeparator ) and ( pos(FormatSettings.DecimalSeparator,szoveg)>0  ) ) then
			Jochar := Chr(0);
	end;
end;

function TEgyebDlg.JoSzazalek(MaskEd : TMaskEdit) : Boolean;
var
	kedv : double;
begin
	JoSzazalek := true;
	if MaskEd.Text <> '' then begin
		kedv := StringSzam(MaskEd.Text);
		if ( ( kedv < 0 ) or ( kedv > 100 ) ) then begin
			NoticeKi('Hib�s % megad�sa!', NOT_ERROR);
			MaskEd.Text := '';
			MaskEd.SetFocus;
			JoSzazalek := false;
	  	end else begin
			MaskEd.Text := SzamString(kedv,6,2);
		end;
	end;
end;

procedure 	TEgyebDlg.FormReturn(bill : WORD);
begin
  if Screen.ActiveControl.ClassName<>'TDBGrid' then
	if bill = VK_RETURN then begin
    	PostMessage(Screen.ActiveForm.Handle, WM_KEYDOWN, VK_TAB, 0);
    	PostMessage(Screen.ActiveForm.Handle, WM_KEYUP, VK_TAB, 0);
 	end;
end;

procedure 	TEgyebDlg.FileMutato( filename, cim, st1, st2, st3, st4 : string; orient : integer);
var
	lista	: TStringlist;
begin
	lista := TStringList.Create;
  	if st1 <> '' then begin
  		lista.Add(st1);
  	end;
  	if st2 <> '' then begin
  		lista.Add(st2);
  	end;
  	if st3 <> '' then begin
  		lista.Add(st3);
  	end;
  	if st4 <> '' then begin
  		lista.Add(st4);
  	end;
	FileMutato2( filename, cim , lista, nil , orient );
	lista.Free;
end;

procedure 	TEgyebDlg.FileMutato2( filename, cim : string; fejlec, lablec : TStringList; orient : integer);
begin
	Application.CreateForm(TFileListDlg, FileListDlg);
	FileListDlg.repfilename 	:= filename;
 	FileListDlg.focim			:= cim;
 	FileListDlg.fejlecek.Assign(fejlec);
  	if not Assigned(lablec) then begin
  		lablec	:= TStringList.Create;
  	end;
 	FileListDlg.lablecek.Assign(lablec);
	FileListDlg.cegnev				:= Read_SZGrid( '999', '101' );
	FileListDlg.Kezdofej			:= Kezdofej;
	FileListDlg.Rep.Page.Orientation := poPortrait;
 	FileListDlg.Vanvonal 		:= VanVonal;
 	if orient <> 0 then begin
		FileListDlg.Rep.Page.Orientation := poLandScape;
 	end;
	FileListDlg.Rep.Preview;
 	FileListDlg.Destroy;
 	Kezdofej	:= true;
	VanVonal	:= true;
end;

procedure TEgyebDlg.Fill_Localfields;
begin
 	// OpenLocalIni(0);
  OpenLocalIni(1);  // az INI k�nyvt�rban lev� cuccot nyissa meg
	v_nemzetkozi			:= StrToIntDef(ReadLocalIni('GENERAL', 'NEMZETKOZI', '0'),0) > 0;
	v_kulsosok  			:= StrToIntDef(ReadLocalIni('GENERAL', 'KULSOSOK', '0'),0) > 0;
	v_archivak  			:= StrToIntDef(ReadLocalIni('GENERAL', 'ARCHIVAK', '0'),0) > 0;
	v_nemzetkozi2			:= StrToIntDef(ReadLocalIni('GENERAL', 'NEMZETKOZI2', '0'),0) > 0;
	v_kulsosok2 			:= StrToIntDef(ReadLocalIni('GENERAL', 'KULSOSOK2', '0'),0) > 0;
	v_archivak2 			:= StrToIntDef(ReadLocalIni('GENERAL', 'ARCHIVAK2', '0'),0) > 0;
	folista_zoom			:= StrToIntDef(ReadLocalIni('GENERAL', 'ZOOMFACTOR', '0'),100);
	combo_autocomplete_delay:= StrToIntDef(ReadLocalIni('GENERAL', 'COMBO_AUTOCOMPLETE_DELAY', '500'),500);
  v_megbizas_default_ful:= StrToIntDef(ReadLocalIni('GENERAL', 'MEGBIZAS_FUL', '1'),1);
end;

{******************************************************************************
F�ggv�ny neve : VanAdat

Funci�        : 	Megkeresi az adatb�zist. Elso l�p�sben beolvassa  az
						'exename'.cfg �llom�nyt, �s abban keres egy PATH -t.
                 Ha nincs �llom�ny, akkor az exe k�nyvt�r f�l�tti DAT
                 lesz az adatb�zis k�nyvt�r.
				  Mindk�t helyen a SZOTAR.DBF �llom�nyt nyitja meg.

Param�terek   : 	-

Visszaadott �.: 	siker�lt vagy nem true - false

Hiba�rt�kek	: 	-

******************************************************************************}
function TEgyebDlg.VanAdat : boolean;
{***********************************}
var
	lgstr   		: string;
  	kellog			: integer;
  	alap_user		: string;
  	alap_pswd		: string;
  	kozos_user		: string;
  	kozos_pswd		: string;
	alap_str2		: string;
	kozos_str2		: string;
	Inifn		   	: TIniFile;
begin
	Result	:= false;
   // Az alap param�terek beolvas�sa a f� ini-b�l
 	Inifn 					:= TIniFile.Create( globalini );
	lgstr					:= Inifn.ReadString( 'GENERAL', 'LOGIN_STR',  '');
	kellog					:= StrToIntDef(Inifn.ReadString( 'GENERAL', 'KELL_LOG',  '0'),0);
   alap_user				:= DecodeString(Inifn.ReadString( 'GENERAL', 'ALAP_USER',  ''));
	alap_pswd				:= DecodeString(Inifn.ReadString( 'GENERAL', 'ALAP_PSWD',  ''));
	kozos_user				:= DecodeString(Inifn.ReadString( 'GENERAL', 'KOZOS_USER', ''));
	kozos_pswd				:= DecodeString(Inifn.ReadString( 'GENERAL', 'KOZOS_PSWD', ''));
	import_dat				:= Inifn.ReadString( 'IMPORT', 'IMPORT_DAT',  '');
	import_koz				:= Inifn.ReadString( 'IMPORT', 'IMPORT_KOZ',  '');
	Inifn.Free;

	{OpenLocalIni(0);
	v_nemzetkozi			:= StrToIntDef(ReadLocalIni('GENERAL', 'NEMZETKOZI', '0'),0) > 0;
	v_kulsosok  			:= StrToIntDef(ReadLocalIni('GENERAL', 'KULSOSOK', '0'),0) > 0;
	v_archivak  			:= StrToIntDef(ReadLocalIni('GENERAL', 'ARCHIVAK', '0'),0) > 0;
	v_nemzetkozi2			:= StrToIntDef(ReadLocalIni('GENERAL', 'NEMZETKOZI2', '0'),0) > 0;
	v_kulsosok2 			:= StrToIntDef(ReadLocalIni('GENERAL', 'KULSOSOK2', '0'),0) > 0;
	v_archivak2 			:= StrToIntDef(ReadLocalIni('GENERAL', 'ARCHIVAK2', '0'),0) > 0;
	folista_zoom			:= StrToIntDef(ReadLocalIni('GENERAL', 'ZOOMFACTOR', '0'),100);
	combo_autocomplete_delay:= StrToIntDef(ReadLocalIni('GENERAL', 'COMBO_AUTOCOMPLETE_DELAY', '500'),500);
  }

	if lgstr = '' then begin
		lgstr			   	:= ReadLocalIni('GENERAL', 'LOGIN_STR',  '');
       kellog			   	:= StrToIntDef(ReadLocalIni('GENERAL', 'KELL_LOG',  '0'),0);
       alap_user		   	:= DecodeString(ReadLocalIni('GENERAL', 'ALAP_USER',  ''));
       alap_pswd		   	:= DecodeString(ReadLocalIni('GENERAL', 'ALAP_PSWD',  ''));
       kozos_user		   	:= DecodeString(ReadLocalIni('GENERAL', 'KOZOS_USER', ''));
       kozos_pswd		   	:= DecodeString(ReadLocalIni('GENERAL', 'KOZOS_PSWD', ''));
   end;
	CloseLocalIni;
   try
		AdoConnectionAlap.Close;
		AdoConnectionKozos.Close;
    AdoConnectionAutonom.Close;
    AdoConnectionTransaction.Close;
		ADOConnectionNavCenter.Close;
		alap_str2	:= '';
		if alap_user <> '' then begin
			alap_str2 	:= ';User ID= '+alap_user+';Password='+alap_pswd;
		end;
		kozos_str2	:= '';
		if kozos_user <> '' then begin
			kozos_str2 	:= ';User ID= '+kozos_user+';Password='+kozos_pswd;
		end;
		AdoConnectionAlap.ConnectionString		:= lgstr+alap_str2;
    AdoConnectionAutonom.ConnectionString		:= lgstr+alap_str2;
    AdoConnectionTransaction.ConnectionString		:= lgstr+alap_str2;
		AdoConnectionKozos.ConnectionString		:= lgstr+kozos_str2;
		ADOConnectionNavCenter.ConnectionString		:= lgstr+kozos_str2;
		AdoConnectionAlap.DefaultDatabase		:= v_alap_db;
    AdoConnectionAutonom.DefaultDatabase		:= v_alap_db;
    AdoConnectionTransaction.DefaultDatabase		:= v_alap_db;
		AdoConnectionKozos.DefaultDatabase		:= v_koz_db;
		ADOConnectionNavCenter.DefaultDatabase		:= v_nav_db;
		AdoConnectionAlap.LoginPrompt			:= false;
    AdoConnectionAutonom.LoginPrompt			:= false;
		if kellog > 0 then begin
			AdoConnectionAlap.LoginPrompt		:= true;
		end;

		AdoConnectionKozos.LoginPrompt		:= false;
		if kellog > 0 then begin
			AdoConnectionKozos.LoginPrompt		:= true;
		end;

		ADOConnectionNavCenter.LoginPrompt		:= false;
		if kellog > 0 then begin
			ADOConnectionNavCenter.LoginPrompt		:= true;
		end;

    ADOConnectionKozos.Connected;
    ADOConnectionAlap.Connected;
    AdoConnectionAutonom.Connected;
    AdoConnectionTransaction.Connected;

		AdoConnectionAlap.Open;
 		AdoConnectionAutonom.Open;
    AdoConnectionTransaction.Open;
		AdoConnectionKozos.Open;
    Try
  		//ADOConnectionNavCenter.Open;
      NavC_Kapcs_OK:=True;
    Except
      NavC_Kapcs_OK:=False;
    End;
	except
		NoticeKi('Nem siker�lt az adatb�zis kapcsol�d�s!');
		Exit;
	end;
	SeTADOQueryDatabase(Query1);
	SeTADOQueryDatabase(Query2);
	SeTADOQueryDatabase(Query3);
  SeTADOQueryDatabase(QueryKozos);
  SeTADOQueryDatabase(QueryKozos2);
	SeTADOQueryDatabase(QueryKoz2);
	SeTADOQueryDatabase(QueryAlap);
	SeTADOQueryDatabase(QueryAlap2);
	SeTADOQueryDatabase(QueryAl2);
	SeTADOQueryDatabase(QueryAl3);
	SeTADOQueryDatabase(QueryVevo);
	SeTADOQueryDatabase(QuerySzfej);
	SeTADOQueryDatabase(QuerySzfej2);
  MaiDatum        := GetServerDateTime;
	Fill_SZGrid;
	Result 			:= true;
	Peldanyszam		:= StrToIntDef(Read_SZGrid('999','555'),0);
	if Peldanyszam < 1 then begin
		Peldanyszam	:= 3;
	end;
	row_color		:= StrToIntDef(Read_SZGrid( '999', '711' ),0);
	if row_color = 0 then begin
		row_color	:= clAqua;
	end;
	EgyebDlg.felrakas_str	:= EgyebDlg.Read_SZGrid( '999', '712' );
	EgyebDlg.lerakas_str    := EgyebDlg.Read_SZGrid( '999', '713' );
  BackUpPath	    := Read_SZGrid( '999', '747' );
  TELEPFELKOD := Read_SZGrid( '999', '759' ); // 32665: a telephely felrak� k�dja
  IRODAI_CSOPORT_LISTA := Read_SZGrid( '470', 'IRODACSOPLISTA');

  SIESSEN_SEBESSEG:= StrToIntDef(Read_SZGrid( '381', 'SIESSEN'), 70);
  ELKESETT_SEBESSEG:= StrToIntDef(Read_SZGrid( '381', 'ELKESETT'), 80);

end;

function 	TEgyebDlg.GetSzuroStr( szkod : string ) : string;
var
	sorszam	: integer;
begin
	Result		:= '';
 	sorszam		:= SzuroGrid.Cols[0].IndexOf(szkod);
	if sorszam > -1 then begin
 		Result 	:= SzuroGrid.Cells[1,sorszam];
 	end;
end;

procedure 	TEgyebDlg.SetSzuroStr( szkod, szurostr : string );
var
	sorszam	: integer;
begin
  sorszam		:= SzuroGrid.Cols[0].IndexOf(szkod);
  if sorszam > -1 then begin
 		SzuroGrid.Cells[1,sorszam] := szurostr;
  end else begin
 		SzuroGrid.RowCount := SzuroGrid.RowCount + 1;
 		SzuroGrid.Cells[0,SzuroGrid.RowCount - 1] := szkod;
 		SzuroGrid.Cells[1,SzuroGrid.RowCount - 1] := szurostr;
  end;
end;


function 	TEgyebDlg.GetTempFileName (ext : string = '') : string;
begin
	Randomize;
	Result := TempList + 'T'+IntToStr(Random(999999))+'.LST';
	if ext <> '' then begin
		Result := ChangeFileExt(Result, ext);
	end;
end;

procedure TEgyebDlg.SeTADOQueryDatabase(sourcequery : TADOQuery );
{*****************************************************************************}
begin
	if Sourcequery.Tag = QUERY_KOZOS_TAG then
		SourceQuery.Connection:= ADOCOnnectionKozos
	else if Sourcequery.Tag = QUERY_ADAT_TAG then
  	SourceQuery.Connection:= ADOCOnnectionAlap
	else if Sourcequery.Tag = QUERY_AUTONOM_TAG then
    SourceQuery.Connection:= ADOConnectionAutonom
  else if Sourcequery.Tag = QUERY_TRANSACTION_TAG then
    SourceQuery.Connection:= ADOConnectionTransaction;
end;

procedure TEgyebDlg.SetJogok(dlgtag : integer; BUJB, BMOD, BTOR, BPRI, BNEZ : TBitBtn);
{*******************************************************************************}
begin
  // Jogosults�gok lekezel�se
  if NO_MENU_SHOW then begin
	  case GetRightTag(dlgtag) of
		 RG_NORIGHT:
			begin
			   BPRI.Enabled := false;
			   BUJB.Enabled := false;
			   BMOD.Enabled := false;
			   BTOR.Enabled := false;
			   BNEZ.Enabled := false;
			end;
		 RG_READONLY:
			begin
			   BUJB.Enabled := false;
			   BMOD.Enabled := false;
			   BTOR.Enabled := false;
			end;
		 RG_MODIFY:
			begin
			   BUJB.Enabled := false;
			   BTOR.Enabled := false;
		  end;
	  end;
  end else begin
	  case GetRightTag(dlgtag) of
		 RG_NORIGHT:
			begin
			   BPRI.Visible := false;
			   BUJB.Visible := false;
			   BMOD.Visible := false;
			   BTOR.Visible := false;
			   BNEZ.Enabled := false;
			end;
		 RG_READONLY:
			begin
			   BUJB.Visible := false;
			   BMOD.Visible := false;
			   BTOR.Visible := false;
			end;
		 RG_MODIFY:
			begin
			   BUJB.Visible := false;
			   BTOR.Visible := false;
		  end;
	  end;
  end;
end;

function TEgyebDlg.SQL_Tolto(Forras : TADOQuery; alapstr, orderstr : string; dlgtag : integer) : boolean;
{****************************************************************************************************}
var
	regilist	: TStringList;
  ujstr		: string;
  ujstr2	: string;
begin
	Result 	:= true;
	regilist := TStringList.Create;
	regilist.Assign(Forras.Sql);
	try
       ujstr	:= alapstr;
       if GetSzuroStr(IntToStr(dlgtag)) <> '' then begin
           ujstr2	:= '';
               if pos ('GROUP BY', UpperCase(ujstr) ) > 0 then begin
           ujstr2	:= ' '+copy(ujstr,pos ('GROUP BY', UpperCase(ujstr) ), 255);
              ujstr		:= copy(ujstr,1, pos ('GROUP BY', UpperCase(ujstr) )-1);
               end;
           if Pos( 'WHERE', UpperCase(ujstr) ) > 0 then begin
               ujstr := ujstr + ' AND ' +GetSzuroStr(IntToStr(dlgtag)) + ' ';
           end else begin
               ujstr := ujstr + ' WHERE ' +GetSzuroStr(IntToStr(dlgtag)) + ' ';
           end;
               if ujstr2 <>'' then begin
               ujstr := ujstr + ujstr2;
               end;
       end;
  		ujstr := ujstr + orderstr;
	 	Query_Run(Forras, ujstr );
     	Forras.First;
  	except
		Forras.Sql.Assign(regilist);
  		Forras.Open;
  		regilist.Free;
     	Result := false;
  	end;
  regilist.Free;
  Forras.First;
end;


function TEgyebDlg.RekordTorles(Forras : TADOQuery; alapstr, orderstr : string; dlgtag : integer;
				torlostr : string ) : boolean;
{**********************************************************************************************}
begin
	Result	:= true;
	if NoticeKi( TORLOTEXT, NOT_QUESTION ) = 0 then begin
		// Rekord t�rl�se
     	Query2.Close;
		Query2.Connection 	 := Forras.Connection;
    	if not Query_Run(Query2, torlostr) then begin
    		Exit;
		end;
		Forras.Delete;
		if Forras.EOF then begin
     		// Meg kell nezni, hogy a sz�r�s t�rl�se ut�n sem marad-e rekord
			if GetSzuroStr(IntToStr(dlgtag)) <> '' then begin
				NoticeKi('Megsz�ntetj�k a sz�r�si felt�teleket!', NOT_MESSAGE);
        		SetSzuroStr(IntToStr(dlgtag),'');
				if SQL_Tolto(Forras,alapstr, orderstr, dlgtag) then begin
					Result := ( Forras.RecordCount > 0 );
           		Exit;
				end;
			end;
			// �res adatb�zis eset�n form bez�r�sa
			Result := false;
		end;
	end;
end;

function TEgyebDlg.CalendarBe( edit : TMaskEdit; MasodikDatum: boolean = False ) : boolean;
var
	datestr	: string;
begin
  {Beolvassa a kiv�lasztott d�tumot egy t�mbb�l}
  	Result	:= false;
	datestr := edit.Text;
  	if datestr = '' then begin
  		datestr := MaiDatum;
  	end;
  	Application.CreateForm(TCalendarDlg, CalendarDlg);
  	CalendarDlg.Tolt(datestr);
  	CalendarDlg.ShowModal;
  	if CalendarDlg.ret_date <> '' then begin
   	Result		:= true;
    if MasodikDatum then
      edit.Text 	:= CalendarDlg.ret_date2  // a h�nap kiv�laszt�s�ra a h�nap v�g�t fogja betenni
    else
  		edit.Text 	:= CalendarDlg.ret_date;
  	end;
  	CalendarDlg.Destroy;
end;

function TEgyebDlg.CalendarBe_idoszak(edit1, edit2: TMaskEdit): boolean;
// itt megoldjuk a h�nap �s h�t v�laszt�st
var
	datestr	: string;
begin
  {Beolvassa a kiv�lasztott d�tumot egy t�mbb�l}
  	Result	:= false;
	datestr := edit1.Text;
  	if datestr = '' then begin
  		datestr := MaiDatum;
  	end;
  	Application.CreateForm(TCalendarDlg, CalendarDlg);
  	CalendarDlg.Tolt(datestr);
  	CalendarDlg.ShowModal;
  	if CalendarDlg.ret_date <> '' then begin
   	  Result		:= true;
  		edit1.Text 	:= CalendarDlg.ret_date;
  		edit2.Text 	:= CalendarDlg.ret_date2;
      // EgyebDlg.ElsoNapEllenor(M2, M3);
  	end;
  	CalendarDlg.Destroy;
end;


procedure TEgyebDlg.Fill_SZGrid;
var
	recszam	: integer;
begin
	recszam	:= 0;
	if Query_Run(Query1, 'Select * from SZOTAR WHERE SZ_ERVNY > 0 AND SZ_FOKOD <> ''112'' ') then begin
		SzGrid.RowCount 	:= Query1.RecordCount;
		recszam				:= 0;
		while not Query1.EOF do begin
      		SzGrid.Cells[0, recszam] := '#'+Query1.Fieldbyname('SZ_FOKOD').AsString+'#'+Query1.Fieldbyname('SZ_ALKOD').AsString+'#';
      		SzGrid.Cells[1, recszam] := Query1.Fieldbyname('SZ_MENEV').AsString;
      		Query1.Next;
        	Inc(recszam);
     	end;
	end;
	if Query_Run(QueryAlap, 'SELECT * FROM CEGADAT') then begin
		SzGrid.RowCount 	:= SzGrid.RowCount + QueryAlap.RecordCount;
     	while not QueryAlap.EOF do begin
      		SzGrid.Cells[0, recszam] := '#CEG#'+QueryAlap.Fieldbyname('S_ALKOD').AsString+'#';
      		SzGrid.Cells[1, recszam] := QueryAlap.Fieldbyname('S_NEV').AsString;
      		QueryAlap.Next;
        	Inc(recszam);
     	end;
	end;
end;

function TEgyebDlg.Read_SZGrid( fokod, alkod : string) : string;
var
	recszam	: integer;
begin
	Result 	:= '';
  	recszam	:= SzGrid.Cols[0].IndexOf('#'+fokod+'#'+alkod+'#');
	if recszam > -1 then begin
		Result := SzGrid.Cells[1, recszam];
  	end;
end;

procedure TEgyebDlg.FormDestroy(Sender: TObject);
begin
	Save_IndexGrid;
	Save_FieldGrid;
	ellenlista.Free;
	// OpenLocalIni(0);
  OpenLocalIni(1);  // az INI k�nyvt�rban lev� cuccot nyissa meg
	WriteLocalIni('GENERAL', 'NEMZETKOZI', BoolToStr01(v_nemzetkozi));
	WriteLocalIni('GENERAL', 'KULSOSOK', BoolToStr01(v_kulsosok));
	WriteLocalIni('GENERAL', 'ARCHIVAK', BoolToStr01(v_archivak));
	WriteLocalIni('GENERAL', 'NEMZETKOZI2', BoolToStr01(v_nemzetkozi2));
	WriteLocalIni('GENERAL', 'KULSOSOK2', BoolToStr01(v_kulsosok2));
	WriteLocalIni('GENERAL', 'ARCHIVAK2', BoolToStr01(v_archivak2));
	WriteLocalIni('GENERAL', 'ZOOMFACTOR', IntToStr(folista_zoom));
	WriteLocalIni('GENERAL', 'ROW_COLOR', IntToStr(row_color));
	CloseLocalIni;
	// Kil�p�s a bel�p�s d�tum�nak �s idej�nek �r�t�se
	Query_Run(QueryKozos,  'UPDATE JELSZO SET JE_BEDAT = '''', JE_BEIDO = '''', JE_BEMAX = 0  WHERE JE_FEKOD = ''' +user_code+ ''' ' , FALSE);
	Query_Update(QueryAlap, 'MEGBIZAS',
		[
		'MB_AFKOD', ''''+''+'''',
		'MB_AFNEV', ''''+''+'''',
		'MB_AFLAG', IntToStr(StrToINtDef(FormatDateTime('nnss', now),0))
		], ' WHERE MB_AFKOD = '''+user_code+'''' );
end;


(******************************************************************************
F�ggv�ny neve :  GetServerDate

Le�r�s        :  Visszaadja az ideiglenes �llom�nyt tartalmaz� sz�m�t�g�p
                 idej�t. Ha a file a szerveren van, akkor a szerver id�t.

Param�terek   :  -

Output        :  az aktu�lis id�
******************************************************************************)
function TEgyebDlg.GetServerDate : TDateTime;
(*******************************************)
var
  fname 		: string;
  fhandle 		: Integer;
begin
	Result 	:= Now;
  	fname 	:= GetTempFileName;
	fhandle 	:= FileCreate(fname);
	if fhandle > 0 then begin
  	{ Az �llom�ny d�tum�nak lek�r�se}
     Result := FileDateToDateTime(FileGetDate(fhandle));
     FileClose(fhandle);
  	DeleteFile(PWideChar(fname));
  end;
end;

procedure TEgyebDlg.Fill_IndexGrid;
var
	list	: TStringLIst;
	i		: integer;
	str	: string;
begin
	list	:= TStringList.Create;
	OpenLocalIni(1);
	ReadLocalIniSectionValues('INDEXES', list);   // pl. 163=3;Megb�z�sok list�ja (4.19.29);TRUE;10;0
	CloseLocalIni;
	{A lista elemeinek feldolgoz�sa}
	IndexGrid.RowCount 	:= list.Count;
	for i := 0 to list.Count - 1 do begin
     	str	:= list[i];
     	IndexGrid.Cells[0,i]	:= copy(str,1,Pos('=', str) - 1);
       str	:= copy(str,Pos('=', str ) + 1, 256);
     	IndexGrid.Cells[1,i]	:= copy(str,1,Pos(';', str) - 1);
       str	:= copy(str,Pos(';', str ) + 1, 256);
     	IndexGrid.Cells[2,i]	:= copy(str,1,Pos(';', str) - 1);
       str	:= copy(str,Pos(';', str ) + 1, 256);
     	IndexGrid.Cells[3,i]	:= copy(str,1,Pos(';', str) - 1);
       str	:= copy(str,Pos(';', str ) + 1, 256);
     	IndexGrid.Cells[4,i]	:= copy(str,1,Pos(';', str) - 1);
       str	:= copy(str,Pos(';', str ) + 1, 256);
     	IndexGrid.Cells[5,i]	:= str;
  	end;
  	list.Free;
end;

procedure TEgyebDlg.Save_IndexGrid;
var
	Ini	: TIniFile  ;
	i		: integer;
begin
	Ini := TIniFile.Create( userini );
	try
		for i := 0 to IndexGrid.RowCount - 1 do begin
			if IndexGrid.Cells[0,i] <> '' then begin
				Ini.WriteString( 'INDEXES', IndexGrid.Cells[0,i], IndexGrid.Cells[1,i] + ';' +IndexGrid.Cells[2,i]+ ';' +
				IndexGrid.Cells[3,i] + ';' +IndexGrid.Cells[4,i] + ';' +IndexGrid.Cells[5,i]);
			end;
		end;
	except
  	end;
 	Ini.Free;
end;

procedure TEgyebDlg.Fill_FieldGrid;
var
	list	: TStringLIst;
	i		: integer;
	Inifn	: TIniFile;
   aktrow	: integer;
begin
	list	:= TStringList.Create;
	OpenLocalIni(1);
	ReadLocalIniSectionValues('FIELDS', list);
	CloseLocalIni;
	{A lista elemeinek feldolgoz�sa}
	FieldGrid.RowCount 	:= list.Count;
	for i := 0 to list.Count - 1 do begin
     	FieldGrid.Cells[0,i]	:= copy(list[i],1,Pos('=', list[i]) - 1);
     	FieldGrid.Cells[1,i]	:= copy(list[i],Pos('=', list[i]) + 1, 799);
  	end;
	// A global ini beolvas�sa - adminisztr�tori be�ll�t�sok
	Inifn  := TIniFile.Create( globalini );
	IniFn.ReadSectionValues( 'FIELDS', list );
	Inifn.Free;
	{A lista elemeinek feldolgoz�sa}
   aktrow	:= FieldGrid.RowCount;
	FieldGrid.RowCount 	:= FieldGrid.RowCount + list.Count;
	for i := 0 to list.Count - 1 do begin
     	FieldGrid.Cells[0,aktrow + i]	:= copy(list[i],1,Pos('=', list[i]) - 1);
     	FieldGrid.Cells[1,aktrow + i]	:= copy(list[i],Pos('=', list[i]) + 1, 799);
  	end;
  	list.Free;
end;

procedure TEgyebDlg.Save_FieldGrid;
var
	Ini		: TIniFile  ;
	i		: integer;
begin
	Ini := TIniFile.Create( userini );
	try
		for i := 0 to FieldGrid.RowCount - 1 do begin
			if ( ( FieldGrid.Cells[0,i] <> '' ) and (Pos('#000#',FieldGrid.Cells[0,i]) = 0 ) ) then begin
				Ini.WriteString( 'FIELDS', FieldGrid.Cells[0,i], FieldGrid.Cells[1,i]);
			end;
		end;
	except
  	end;
 	Ini.Free;
end;

{******************************************************************************
F�ggv�ny neve : 	ArfolyamErtek

Funci�        : 	Visszaadja a megadott napon �rv�nyben l�v� megadott valutanem
						�rfolyam�t

Param�terek   : 	valnem		- a valutanem k�dja
                 datestr  	- a keresett d�tum
                 kell:  - ha hi�nyzik az �rfolyam -> k�rd�s + kit�lt�s. Ez ki volt kommentezve.
Visszaadott �.:  az �rfolyam �rt�ke

****************************************************************************** }

function 	TEgyebDlg.ArfolyamErtek( valnem : string; datestr : string; kell : boolean; elozonap: boolean=False) : double;
// function 	TEgyebDlg.ArfolyamErtek( valnem : string; datestr : string; elozonap: boolean) : double;
var
  ev, ho, nap : integer;
  tmpDate, originaldate: TDateTime;

begin
	Result	:= 0;
  // v�gtelen ciklusba futhatna, �s egy�bk�nt is �rtelmetlen
  if valnem = '' then begin
		Result	:= 0;
    Exit;
    end;
	{Forintra mindig 1 -et adjon vissza}
  if valnem = 'HUF' then begin
		Result	:= 1;
     Exit;
  end;
	{Megkeresi az aznapi �rfolyamot}
  if Query_Run(QueryKozos, 'SELECT * FROM ARFOLYAM WHERE AR_VALNEM = '''+
  	valnem+''' AND AR_DATUM = '''+datestr+''' ',true) then begin
     if Querykozos.RecordCount > 0 then begin       // van �rfolyam
     	Result := QueryKozos.FieldByname('AR_ERTEK').AsFloat;
     end else begin                                 // NINCS �rfolyam
     	{Megn�zz�k, hogy h�tv�g�re esett-e}
  		ev    := StrToIntDef(copy(datestr,1,4),0);
  		ho    := StrToIntDef(copy(datestr,6,2),0);
  		nap   := StrToIntDef(copy(datestr,9,2),0);
  		if ( ( ev = 0 ) or ( ho = 0 ) or ( nap = 0 ) ) then begin
     		Exit;
  		end;
  		tmpDate := EncodeDate(ev, ho, nap);
      originaldate:= tmpDate;  // innen indulunk visszafel� az id�ben, ha vissza kell keresni
      if (not elozonap)and(not HetvegeVagyUnnep(tmpDate)) then
      begin
        Result:=0;
        Exit;
      end;
      // r�gebbi �rfolyam keres�se
      Query_Run(QueryKozos, 'SELECT * FROM ARFOLYAM WHERE AR_VALNEM = '''+ valnem+''' AND AR_DATUM <= '''+datestr+''' ',true);
      if Querykozos.RecordCount > 0 then begin       // egy�ltal�n van r�gebbi �rfolyam
        while (originaldate-tmpDate) < 10  do begin  // v�gtelenciklus ellen, na meg ne is haszn�ljunk ennyire r�gi �rfolyamot
            QueryKozos.Close;
            tmpDate:=tmpDate-1;
            if not HetvegeVagyUnnep(tmpDate) then  begin
              datestr:= DateToStr(tmpDate) ;
              // Result := ArfolyamErtek(valnem, datestr, kell, elozonap);
              Result := ArfolyamErtek(valnem, datestr, kell, elozonap);
              if (Result=0)and(elozonap) then
                // ha 0 �s elozonap, akkor megn�zi az el�z�nap �rfolyam�t
                date
              else
                Break;
              end;  // if
            end;  // while
        end;  // if

{    	if DayOfWeek(tmpDate) = 1 then begin      // vas�rnap
    			QueryKozos.Close;
        	Result := ArfolyamErtek(valnem, DatumHozNap(datestr,-2,false), kell);
     	end else begin
        	if DayOfWeek(tmpDate) = 7 then begin   // szombat
     			    QueryKozos.Close;
		        	Result := ArfolyamErtek(valnem, DatumHozNap(datestr,-1,false), kell);
				  end else begin
           	if kell then begin
                 //Hi�nyzik az �rfolyam -> k�rd�s + kit�lt�s

                 Application.CreateForm(TArfolybeDlg, ArfolybeDlg);
                 ArfolybeDlg.Tolto('�j �rfolyam felvitele','');
                 ArfolybeDlg.MaskEdit2.Text		:= '1';
                 afasor := ArfolybeDlg.afalist.IndexOf(valnem);
                 if afasor < 0 then begin
                    afasor := 0;
                 end;
                 ArfolybeDlg.AfaCombo.ItemIndex	:= afasor;
                 ArfolybeDlg.MaskEdit10.Text		:= datestr;
                 ArfolybeDlg.MaskEdit2.Enabled	:= false;
                 ArfolybeDlg.MaskEdit10.Enabled	:= false;
                 ArfolybeDlg.AfaCombo.Enabled	:= false;
                 ArfolybeDlg.ShowModal;
                 if ArfolybeDlg.ret_kod <> '' then begin
                    Result	:= StringSzam(ArfolybeDlg.ret_arfoly);
                 end;
                 ArfolybeDlg.Destroy;

            end;
     	    end;
     	end;  }


  	 end;
     QueryKozos.Close;
  end;
end;

{******************************************************************************
F�ggv�ny neve : ElsoNapEllenor

Funci�        : 	Ellen�rzi, hogy az input valamelyik h�nap els� napja e, �s ha
                 igen, a kimeneti mez�be beteszi a h�nap utols� napj�t.

Param�terek   : 	ma1	- a bemeneti mez�
                 ma2  	- a kimeneti mez�
Visszaadott �.:	-
****************************************************************************** }
procedure TEgyebDlg.ElsoNapEllenor(ma1, ma2 : TMaskEdit);
	var  nap : integer;
begin
	nap := StrToIntDef(copy(ma1.Text,9,2),0);
	if nap <> 1 then begin
		Exit;
	end;
	{Megkeress�k az utols� napot}
	nap := 31;
	ma2.Text := copy(ma1.Text,1,8) + Format('%2.2d.',[nap]);
	while not Jodatum2(ma2) do begin
		Dec(nap);
		ma2.Text := copy(ma1.Text,1,8) + Format('%2.2d.',[nap]);
	end;
end;

procedure TEgyebDlg.UtolsoNap(MD: TMaskEdit);
begin
  // itt egyben ki-be lehet kapcsolni
	// MD.Text:=DateToStr(EndOfTheMonth(StrToDate(MD.Text)));
end;

function 	TEgyebDlg.DateTimeToNum( datstr, timestr : string) : double;
var
  ev, ho, nap 	: integer;
  ora, perc, sec : integer;
begin
	Result 	:= 0;
	ev 		:= StrToIntDef(copy(datstr,1,4),0);
	ho 		:= StrToIntDef(copy(datstr,6,2),0);
	nap 		:= StrToIntDef(copy(datstr,9,2),0);
	if ( ( ev = 0 ) or ( ho = 0 ) or (nap=0) ) then begin
		Exit;
	end;

	ora 	:= StrToIntDef(copy(timestr,1,2),0);
	perc 	:= StrToIntDef(copy(timestr,4,2),0);
	sec 	:= StrToIntDef(copy(timestr,6,2),0);

	try
		Result 	:= EncodeDate(ev, ho, nap) + EncodeTime(ora, perc, sec, 0);
	except
		Result 	:= 0;
	end;
end;

procedure TEgyebDlg.DateTimeDifference( datstr1, timestr1, datstr2, timestr2 : string; var day, hour : integer);
var
	td1, td2	: TDateTime;
  kuld		: TDateTime;
begin
	day 		:= 0;
  hour		:= 0;
	td1 		:= EgyebDlg.DateTimeToNum(datstr1, timestr1);
	td2 		:= EgyebDlg.DateTimeToNum(datstr2, timestr2);
  if ( (td1 = 0) or (td2 = 0) ) then begin
	Exit;
  end;
	kuld		:= td2 - td1;
  if ( kuld < 0 ) then begin
	Exit;
  end;
  day		:= Round(Int(kuld));
  hour		:= Round(Int((Frac(kuld)+0.000001) * 24));	{Felfel� kerek�ti az id�t}
end;

function TEgyebDlg.GepkocsiErvEllenor(napok, kmek : integer): integer;
{************************************************************}
var
	st			: string;
	elonap,count   	: integer;
	elokm		: integer;
	moddat		: string;
	gepkm		: integer;
	st2			: string;
  db: integer;
begin
  count:=0;
	EllenListAppend('', false);
	EllenListAppend('M�r lej�rt g�pkocsi elemek :', false);
	EllenListAppend('============================', false);
	EllenListAppend(Format('Be�ll�tott param�terek : nap = %d;  km = %d', [napok, kmek]), false);
	elonap	:= napok;
	elokm	:= kmek;
	if elonap < 1 then begin
		elonap	:= StrToIntDef(Read_SZGrid('999', '641'),0); {A napok sz�ma}
	end;
	if elokm < 1 then begin
		elokm 	:= StrToIntDef(Read_SZGrid('999', '651'),0); {A km-ek sz�ma}
	end;
	// A mai napig lej�rt elemek list�z�sa
	moddat	:= MaiDatum;
	// Garanci�k ellen�rz�se
	if Query_Run (QueryKozos, 'SELECT DISTINCT GA_ALKOD FROM GARANCIA ORDER BY GA_ALKOD ', true) then begin
		while not QueryKozos.Eof do begin
			Query_Run (QueryKoz2, 'SELECT GA_RENSZ, GA_DATUM, GK_KIVON FROM GARANCIA, GEPKOCSI WHERE GA_ALKOD = '''+QueryKozos.FieldByName('GA_ALKOD').AsString+''' '+
				' AND GA_DATUM < '''+moddat+''' AND GA_RENSZ = GK_RESZ AND  GK_ARHIV < 1  ORDER BY GA_DATUM ', true);
//				' AND GA_DATUM < '''+moddat+''' AND GA_RENSZ = GK_RESZ AND  GK_ARHIV < 1 AND GK_KIVON = 0 ORDER BY GA_DATUM ', true);
			if QueryKoz2.RecordCount > 0 then begin
				st := Read_SZGrid( '125',QueryKozos.FieldByName('GA_ALKOD').AsString ) + ' : ';
        db:=0;
				while not QueryKoz2.Eof do begin
          if (QueryKoz2.FieldByName('GK_KIVON').AsString='1')and(QueryKozos.FieldByName('GA_ALKOD').AsString<>'280') then
          begin
            Querykoz2.Next;
            Continue;
          end;
					st	:= st + QueryKoz2.FieldByName('GA_RENSZ').AsString
						+ '('+QueryKoz2.FieldByName('GA_DATUM').AsString+ '); ';
          inc(db);
					QueryKoz2.Next;
				end;
        if db>0 then
        begin
  				EllenListAppend(st, false);
          inc(count);
        end;
			end;
			QueryKozos.Next;
		end;
	end;
	// A lej�rt lizingek ellen�rz�se
	st2	:= '';
	if Query_Run (QueryKozos, 'SELECT * FROM GEPKOCSI WHERE GK_LIVAN = 1 ORDER BY GK_RESZ ', true) then begin
		while not QueryKozos.Eof do begin
			st	:= GetLastLizingDate( QueryKozos.FieldByName('GK_LIDAT').AsString, QueryKozos.FieldByName('GK_LIHON').AsString, QueryKozos.FieldByName('GK_LIPER').AsInteger);
			st	:= copy(st,1,4)+'.'+copy(st,5,2)+copy(QueryKozos.FieldByName('GK_LIDAT').AsString, 8,5);
			if st < moddat then begin
				st2	:= st2 + QueryKozos.FieldByName('GK_RESZ').AsString + '('+st+'); ';
			end;
			QueryKozos.Next;
		end;
	end;
	if st2	<> '' then begin
		EllenListAppend('Lej�rt lizingek : '+st2, false);
        inc(count);
	end;

	{A garancia km ellen�rz�se}
	st2	:= '';
//	if Query_Run (QueryKozos, 'SELECT * FROM GEPKOCSI WHERE GK_GARFI > 0 AND GK_ARHIV < 1 AND GK_KIVON = 0 ', true) then begin
	if Query_Run (QueryKozos, 'SELECT * FROM GEPKOCSI WHERE GK_GARFI > 0 AND GK_ARHIV < 1 ', true) then begin
		while not QueryKozos.EOF do begin
			{T�telenk�nt ellen�rz�s}
			if Query_Run (QueryAl2, 'SELECT MAX(KS_KMORA) MAXERT FROM KOLTSEG WHERE KS_RENDSZ = '''+
				QueryKozos.FieldByname('GK_RESZ').AsString+''' ', true) then begin
				gepkm	:= StrToIntDef(QueryAl2.FieldByname('MAXERT').AsString,0);
				if  gepkm >= StrToIntDef(Querykozos.FieldByname('GK_GARKM').AsString,0) then begin
					st2 := st2 + Querykozos.FieldByname('GK_RESZ').AsString + '('+Querykozos.FieldByname('GK_GARKM').AsString+'); ';
//					EllenListAppend('Lej�rt garancia km : '+ Querykozos.FieldByname('GK_RESZ').AsString, false);
				end;
			end;
			QueryKozos.Next;
		end;
		QueryKozos.Close;
	end;
	if st2 <> '' then begin
		EllenListAppend('Lej�rt garancia km : '+ st2, false);
        inc(count);
	end;

	EllenListAppend('', false);
	EllenListAppend('Lej�r� g�pkocsi elemek :', false);
	EllenListAppend('========================', false);
	moddat	:= DatumHozNap(MaiDatum, elonap, false);
	// Garanci�k ellen�rz�se
	if Query_Run (QueryKozos, 'SELECT DISTINCT GA_ALKOD FROM GARANCIA ORDER BY GA_ALKOD ', true) then begin
		while not QueryKozos.Eof do begin
			Query_Run (QueryKoz2, 'SELECT GA_RENSZ, GA_DATUM, GK_KIVON FROM GARANCIA, GEPKOCSI WHERE GA_ALKOD = '''+ QueryKozos.FieldByName('GA_ALKOD').AsString+''' '+
				' AND GA_DATUM < '''+moddat+ ''' '+ ' AND GA_DATUM >= '''+MaiDatum+''' AND GA_RENSZ = GK_RESZ AND  GK_ARHIV < 1  ORDER BY GA_DATUM ', true);
//				' AND GA_DATUM < '''+moddat+ ''' '+ ' AND GA_DATUM >= '''+MaiDatum+''' AND GA_RENSZ = GK_RESZ AND  GK_ARHIV < 1 AND GK_KIVON = 0 ORDER BY GA_DATUM ', true);
			if QueryKoz2.RecordCount > 0 then begin
				st := Read_SZGrid( '125',QueryKozos.FieldByName('GA_ALKOD').AsString ) + ' : ';
				while not QueryKoz2.Eof do begin
          if (QueryKoz2.FieldByName('GK_KIVON').AsString='1')and(QueryKozos.FieldByName('GA_ALKOD').AsString<>'280') then begin
            Querykoz2.Next;
            Continue;
            end;
					st	:= st + QueryKoz2.FieldByName('GA_RENSZ').AsString + '('+QueryKoz2.FieldByName('GA_DATUM').AsString+ '); ';
					QueryKoz2.Next;
				end;
				EllenListAppend(st, false);
        inc(count);
			end;
			QueryKozos.Next;
		end;
	end;
	// A lej�r� lizingek ellen�rz�se
	st2	:= '';

	if Query_Run (QueryKozos, 'SELECT * FROM GEPKOCSI WHERE GK_LIVAN = 1 ORDER BY GK_RESZ ', true) then begin
		while not QueryKozos.Eof do begin
			st	:= GetLastLizingDate( QueryKozos.FieldByName('GK_LIDAT').AsString, QueryKozos.FieldByName('GK_LIHON').AsString, QueryKozos.FieldByName('GK_LIPER').AsInteger);
			st	:= copy(st,1,4)+'.'+copy(st,5,2)+copy(QueryKozos.FieldByName('GK_LIDAT').AsString, 8,5);
			if ( ( st < moddat ) and (st >= MaiDatum) ) then begin
				st2	:= st2 + QueryKozos.FieldByName('GK_RESZ').AsString + '('+st+'); ';
			end;
			QueryKozos.Next;
		end;
	end;
	if st2	<> '' then begin
		EllenListAppend('Lej�r� lizingek : '+st2, false);
        inc(count);
	end;

	{A garancia km ellen�rz�se}
	st2	:= '';
//	if Query_Run (QueryKozos, 'SELECT * FROM GEPKOCSI WHERE GK_GARFI > 0 AND GK_ARHIV < 1 AND GK_KIVON = 0 ', true) then begin
	if Query_Run (QueryKozos, 'SELECT * FROM GEPKOCSI WHERE GK_GARFI > 0 AND GK_ARHIV < 1  ', true) then begin
		while not QueryKozos.EOF do begin
			{T�telenk�nt ellen�rz�s}
			if Query_Run (QueryAl2, 'SELECT MAX(KS_KMORA) MAXERT FROM KOLTSEG WHERE KS_RENDSZ = '''+
				QueryKozos.FieldByname('GK_RESZ').AsString+''' ', true) then begin
				gepkm	:= StrToIntDef(QueryAl2.FieldByname('MAXERT').AsString,0);
				if ( ( gepkm < StrToIntDef(Querykozos.FieldByname('GK_GARKM').AsString,0)) and (( gepkm + elokm ) >= StrToIntDef(Querykozos.FieldByname('GK_GARKM').AsString,0) ) ) then begin
					st2 := st2 + Querykozos.FieldByname('GK_RESZ').AsString + '('+Querykozos.FieldByname('GK_GARKM').AsString+'); ';
				end;
			end;
			QueryKozos.Next;
		end;
		QueryKozos.Close;
	end;
	if st2 <> '' then begin
		EllenListAppend('Lej�r� garancia km : '+ st2, false);
        inc(count);
	end;
  Result:=count;
end;

function TEgyebDlg.DolgozoErvEllenor(napok, kmek : integer): integer;
{************************************************************}
var
	st			: string;
	elonap,count   	: integer;
	moddat,dokod		: string;
begin
  count:=0;
	EllenListAppend('', false);
	EllenListAppend('M�r lej�rt dolgoz�i elemek :', false);
	EllenListAppend('============================', false);
	EllenListAppend(Format('Be�ll�tott param�terek : nap = %d', [napok]), false);
	elonap	:= napok;
	if elonap < 1 then begin
		elonap	:= StrToIntDef(Read_SZGrid('999', '641'),0); {A napok sz�ma}
	end;
	// A mai napig lej�rt elemek list�z�sa
	moddat	:= MaiDatum;
	{SHELL k�rtya ellen�rz�s}
	//if Query_Run (QueryKozos, 'SELECT * FROM SHELL WHERE SH_ERDAT <= '''+moddat+''' ORDER BY SH_ERDAT ', true) then begin
	if Query_Run (QueryKozos, 'SELECT * FROM SHELL,GEPKOCSI WHERE substring(SH_SHNEV,1,7)=GK_RESZ and GK_ARHIV=0 and SH_ERDAT <= '''+moddat+''' ORDER BY SH_ERDAT ', true) then begin
		if QueryKozos.RecordCount > 0 then begin
			st := '';
			while not QueryKozos.EOF do begin
				st := st + QueryKozos.FieldByname('SH_SHNEV').AsString+'('+QueryKozos.FieldByname('SH_ERDAT').AsString+'); ';
				QueryKozos.Next;
			end;
			EllenListAppend('Lej�rt SHELL k�rtya : '+ st, false);
        inc(count);
		end;
		QueryKozos.Close;
	end;
	{DKV k�rtya ellen�rz�s}
	//if Query_Run (QueryKozos, 'SELECT * FROM DKVKARTYA WHERE DK_ERDAT <= '''+moddat+''' ORDER BY DK_ERDAT ', true) then begin
	if Query_Run (QueryKozos, 'SELECT * FROM DKVKARTYA,GEPKOCSI WHERE substring(DK_DKNEV,1,7)=GK_RESZ and GK_ARHIV=0 and DK_ERDAT <= '''+moddat+''' ORDER BY DK_ERDAT ', true) then begin
		if QueryKozos.RecordCount > 0 then begin
			st := '';
			while not QueryKozos.EOF do begin
				st := st + QueryKozos.FieldByname('DK_DKNEV').AsString+'('+QueryKozos.FieldByname('DK_ERDAT').AsString+'); ';
				QueryKozos.Next;
			end;
			EllenListAppend('Lej�rt DKV k�rtya : '+ st, false);
        inc(count);
		end;
		QueryKozos.Close;
	end;
	// Dolgoz�i hat�rid�k ellen�rz�se
	// if Query_Run (QueryKozos, 'SELECT DISTINCT DE_ALKOD FROM DOLGOZOERV ORDER BY DE_ALKOD ', true) then begin
  // ... a sz�t�r alapj�n, hogy a letiltottak/t�r�ltek ne jelenjenek meg
  if Query_Run (QueryKozos, 'SELECT DISTINCT SZ_ALKOD ALKOD FROM SZOTAR where SZ_FOKOD=''127'' and SZ_ERVNY=''1'' ORDER BY 1 ', true) then begin
		while not QueryKozos.Eof do begin
			Query_Run (QueryKoz2, 'SELECT DO_NAME, DE_DATUM,DE_DOKOD FROM DOLGOZOERV, DOLGOZO WHERE DE_ALKOD = '''+QueryKozos.FieldByName('ALKOD').AsString+''' '+
				' AND DE_DATUM < '''+moddat+''' AND DE_DOKOD = DO_KOD AND  DO_ARHIV < 1 ORDER BY DE_DATUM ', true);
			if QueryKoz2.RecordCount > 0 then begin
				st := Read_SZGrid( '127',QueryKozos.FieldByName('ALKOD').AsString ) + ' : ';
				while not QueryKoz2.Eof do begin
					st	:= st + QueryKoz2.FieldByName('DO_NAME').AsString
						+ '('+QueryKoz2.FieldByName('DE_DATUM').AsString+ '); ';
          dokod:=QueryKoz2.FieldByName('DE_DOKOD').AsString  ;
					QueryKoz2.Next;
				end;
				EllenListAppend(st, false);
        inc(count);
			end;
			QueryKozos.Next;
		end;
	end;

	EllenListAppend('', false);
	EllenListAppend('Lej�r� dolgoz�i elemek :', false);
	EllenListAppend('========================', false);
	moddat	:= DatumHozNap(MaiDatum, elonap, false);
	{SHELL k�rtya ellen�rz�s}
	if Query_Run (QueryKozos, 'SELECT * FROM SHELL WHERE SH_ERDAT <= '''+moddat+''' '+
		' AND SH_ERDAT > '''+MaiDatum+''' '+
		' ORDER BY SH_ERDAT ', true) then begin
		if QueryKozos.RecordCount > 0 then begin
			st	:= '';
			while not QueryKozos.EOF do begin
				st := st + QueryKozos.FieldByname('SH_SHNEV').AsString+'('+QueryKozos.FieldByname('SH_ERDAT').AsString+'); ';
				QueryKozos.Next;
			end;
			EllenListAppend('Lej�r� SHELL k�rtya : '+ st, false);
        inc(count);
		end;
		QueryKozos.Close;
	end;
	{DKV k�rtya ellen�rz�s}
	if Query_Run (QueryKozos, 'SELECT * FROM DKVKARTYA WHERE DK_ERDAT <= '''+moddat+'''  AND DK_ERDAT > '''+MaiDatum+''' '+
		' ORDER BY DK_ERDAT ', true) then begin
		if QueryKozos.RecordCount > 0 then begin
			st	:= '';
			while not QueryKozos.EOF do begin
				st := st + QueryKozos.FieldByname('DK_DKNEV').AsString+'('+QueryKozos.FieldByname('DK_ERDAT').AsString+'); ';
				QueryKozos.Next;
			end;
			EllenListAppend('Lej�r� DKV k�rtya : '+ st, false);
        inc(count);
		end;
		QueryKozos.Close;
	end;
	// Dolgoz�i hat�rid�k ellen�rz�se
	// if Query_Run (QueryKozos, 'SELECT DISTINCT DE_ALKOD FROM DOLGOZOERV ORDER BY DE_ALKOD ', true) then begin
    if Query_Run (QueryKozos, 'SELECT DISTINCT SZ_ALKOD ALKOD FROM SZOTAR where SZ_FOKOD=''127'' and SZ_ERVNY=''1'' ORDER BY 1 ', true) then begin
		while not QueryKozos.Eof do begin
			Query_Run (QueryKoz2, 'SELECT DO_NAME, DE_DATUM FROM DOLGOZOERV, DOLGOZO WHERE DE_ALKOD = '''+QueryKozos.FieldByName('ALKOD').AsString+''' '+
				' AND DE_DATUM < '''+moddat+''' '+
				' AND DE_DATUM >= '''+MaiDatum+''' '+
				' AND DE_DOKOD = DO_KOD AND  DO_ARHIV < 1 ORDER BY DE_DATUM ', true);
			if QueryKoz2.RecordCount > 0 then begin
				st := Read_SZGrid( '127',QueryKozos.FieldByName('ALKOD').AsString ) + ' : ';
				while not QueryKoz2.Eof do begin
					st	:= st + QueryKoz2.FieldByName('DO_NAME').AsString + '('+QueryKoz2.FieldByName('DE_DATUM').AsString+ '); ';
					QueryKoz2.Next;
				end;
				EllenListAppend(st, false);
        inc(count);
			end;
			QueryKozos.Next;
		end;
	end;
  Result:=count;
end;

procedure TEgyebDlg.Ervenyesseg(sql, mez1, mez2, szoveg : string);
var
	st : string;
begin
	if Query_Run (Query1, sql, true) then begin
		if Query1.RecordCount > 0 then begin
			st := '';
		 while not Query1.EOF do begin
			st := st + Query1.FieldByName(mez1).AsString + ' ('+ Query1.FieldByName(mez2).AsString + '), ';
			Query1.Next;
		 end;
		 EllenListAppend(szoveg+ st, false);
	  end;
	end;
end;

function TEgyebDlg.JaratAtlag(jaratkod: string) : double;
{********************************************************}
var
	Fogyi22	: TFogyasztasDlg;
begin
   Result  := 0;
	if Query_Run (QueryAlap, 'SELECT * FROM JARAT WHERE JA_KOD = '''+jaratkod+''' ', true) then begin
		Application.CreateForm(TFogyasztasDlg, Fogyi22);
		Fogyi22.ClearAllList;
		Fogyi22.JaratBetoltes(jaratkod);
		Fogyi22.CreateTankLista;
		Fogyi22.CreateAdBlueLista;
		Fogyi22.Szakaszolo;
		if Fogyi22.GetOsszKm <> 0 then begin
			Result	:= Fogyi22.GetTenyFogyasztas * 100/Fogyi22.GetOsszKm;
		end;
		Fogyi22.Destroy;
	end;
end;

procedure TEgyebDlg.AlapToltes;
var
	Inifn				: TIniFile;
begin
	Inifn 		:= TIniFile.Create( globalini );
	v_alap_db	:= Inifn.ReadString( 'GENERAL', 'ALAP_DB', '');
	v_koz_db	:= Inifn.ReadString( 'GENERAL', 'KOZOS_DB', '');
	v_nav_db	:= Inifn.ReadString( 'GENERAL', 'NAV_DB', 'NavCenter2SQL');
	v_formatum	:= StrToIntdef(Inifn.ReadString( 'GENERAL', 'FORMATUM', '0'),0);
	v_szamlaformatum:= StrToIntdef(Inifn.ReadString( 'GENERAL', 'SZAMLAFORMATUM', '0'),0);
//	if (StrToIntdef(v_evszam,0) = 0)and(TESZTDB='') then begin
	if (StrToIntdef(v_evszam,0) = 0) then begin
		// Csak akkor olvassuk be az INI �vsz�mot, ha el�tte lenull�ztuk
		v_evszam	:= Inifn.ReadString( 'GENERAL', 'EVSZAM', '');
	end;
	Inifn.Free;
(*
	OpenLocalIni ;
	v_alap_db	:= ReadLocalIni('GENERAL', 'ALAP_DB', '');
	v_koz_db	:= ReadLocalIni('GENERAL', 'KOZOS_DB', '');
	v_evszam	:= ReadLocalIni('GENERAL', 'EVSZAM', '');
	CloseLocalIni;
*)
	if v_evszam = '' then begin
		v_evszam	:= evszam;
	end;
	if v_alap_db = '' then begin
		v_alap_db 	:= 'DAT';
	end;
	if v_koz_db = '' then begin
		v_koz_db 	:= 'KOZ';
	end;
	v_alap_db	:= v_alap_db + v_evszam;
	v_koz_db	:= v_koz_db  + v_evszam;

  if TESZTDB<>'' then
    v_evszam:=EvSzam;

  TESZTPRG:= (pos('TESZT',EgyebDlg.v_alap_db)>0);

end;

procedure TEgyebDlg.AlapadatToltes;
begin
  MobilAppTelszam:= GetMobilAppTelszam;
  MobilAppJelszo:= GetMobilAppJelszo;
end;

procedure TEgyebDlg.SqlInit;
var
	sn1			: string;
	sn2			: string;
//	tesztkod	: integer;
	i			: integer;
	tmpDate  	: TDateTime;
	kellszotar	: boolean;
   hossz, db       : integer;
   s           : string;
   strdate     : string;
begin
//	teszt kod    := StrToIntDef(Read_Szgrid('999', '	919'), 0);
  ///////////////////
	GK_HUTOS    := Read_SZGrid( '999', '922' );
   ATLAG_KM_ORA:=70;
   s           :=''',''';
   GK_HUTOS    := StringReplace(GK_HUTOS,',',s,[rfReplaceAll]);
   hossz       := length(GK_HUTOS);
   GK_HUTOS    := copy(GK_HUTOS,3,hossz-4);


   if not Query_Run(QueryKozos2, 'SELECT SZ_EGYEB2 FROM SZOTAR', false) then begin
       Query_Run(QueryKozos2, 'ALTER TABLE SZOTAR ADD SZ_EGYEB2 INT DEFAULT 0 ');
       end;

 	Query_Run(QueryKozos2, 'SELECT * FROM SZOTAR WHERE SZ_FOKOD = ''000'' AND SZ_ALKOD = ''156'' ', FALSE);
	if QueryKozos2.RecordCount < 1 then begin
 		Query_Run(QueryKozos2,  'insert into szotar (SZ_FOKOD, SZ_ALKOD, SZ_MENEV, SZ_LEIRO, SZ_ERVNY) VALUES (''000'', ''156'', ''Alap�rtelmezett v�lasz SMS sz�m'', '''', 1) ', FALSE);
    end;


 	Query_Run(QueryKozos2, 'SELECT * FROM SZOTAR WHERE SZ_FOKOD = ''000'' AND SZ_ALKOD = ''341'' ', FALSE);
	if QueryKozos2.RecordCount < 1 then begin
 		Query_Run(QueryKozos2,  'insert into szotar (SZ_FOKOD, SZ_ALKOD, SZ_MENEV, SZ_LEIRO, SZ_ERVNY) VALUES (''000'', ''341'', ''Csoport �zenet cc (dolgoz� k�d lista)'', '''', 1) ', FALSE);
    end;

   // P�tkocsi �zem�ra kimutat�s
   Query_Run(QueryKozos2, 'SELECT * FROM DIALOGS WHERE DL_DLKOD = 610', false);
   if QueryKozos2.RecordCount < 1 then begin
       Query_Run(QueryKozos2, 'insert into DIALOGS (DL_DLNEV, DL_DLKOD, DL_LEIRS) VALUES (''POTUZEMORA'', 610, ''P�tkocsi �zem�ra kimutat�s'')', FALSE);
       end;  // if

 	Query_Run(QueryKozos2, 'SELECT * FROM SZOTAR WHERE SZ_FOKOD = ''000'' AND SZ_ALKOD = ''157'' ', FALSE);
	if QueryKozos2.RecordCount < 1 then begin
 		Query_Run(QueryKozos2,  'insert into szotar (SZ_FOKOD, SZ_ALKOD, SZ_MENEV, SZ_LEIRO, SZ_ERVNY) VALUES (''000'', ''157'', ''Adatkapcsolati be�ll�t�sok'', '''', 1) ', FALSE);
 		Query_Run(QueryKozos2,  'insert into szotar (SZ_FOKOD, SZ_ALKOD, SZ_MENEV, SZ_LEIRO, SZ_ERVNY) VALUES (''157'', ''STAND_TRAILER'', ''%stand trailer%'', ''Stand trailer pattern'', 1) ', FALSE);
 		Query_Run(QueryKozos2,  'insert into szotar (SZ_FOKOD, SZ_ALKOD, SZ_MENEV, SZ_LEIRO, SZ_ERVNY) VALUES (''157'', ''STAND_TRAILER_FAP'', ''Stand trailer'', ''Stand trailer konstans az FAP �llom�nyban'', 1) ', FALSE);
    end;

 	Query_Run(QueryKozos2, 'SELECT * FROM SZOTAR WHERE SZ_FOKOD = ''000'' AND SZ_ALKOD = ''157'' ', FALSE);
	if QueryKozos2.RecordCount < 1 then begin
 		Query_Run(QueryKozos2,  'insert into szotar (SZ_FOKOD, SZ_ALKOD, SZ_MENEV, SZ_LEIRO, SZ_ERVNY) VALUES (''000'', ''157'', ''Adatkapcsolati be�ll�t�sok'', '''', 1) ', FALSE);
 		Query_Run(QueryKozos2,  'insert into szotar (SZ_FOKOD, SZ_ALKOD, SZ_MENEV, SZ_LEIRO, SZ_ERVNY) VALUES (''157'', ''STAND_TRAILER'', ''%stand trailer%'', ''Stand trailer pattern'', 1) ', FALSE);
 		Query_Run(QueryKozos2,  'insert into szotar (SZ_FOKOD, SZ_ALKOD, SZ_MENEV, SZ_LEIRO, SZ_ERVNY) VALUES (''157'', ''STAND_TRAILER_FAP'', ''Stand trailer'', ''Stand trailer konstans az FAP �llom�nyban'', 1) ', FALSE);
    end;

 	Query_Run(QueryKozos2, 'SELECT * FROM SZOTAR WHERE SZ_FOKOD = ''157'' AND SZ_ALKOD = ''POZKULD_KIVETEL'' ', FALSE);
	if QueryKozos2.RecordCount < 1 then begin
 		Query_Run(QueryKozos2,  'insert into szotar (SZ_FOKOD, SZ_ALKOD, SZ_MENEV, SZ_LEIRO, SZ_ERVNY) VALUES (''157'', ''POZKULD_KIVETEL'', '''', ''Megb�z�s k�d lista, amit nem kell k�ldeni'', 1) ', FALSE);
    end;

  // NAV adatok az egyedi sz�ml�khoz -> Teszt (T) �s �les (�)
  if not Query_Run(QueryAlap, 'SELECT SA_TSTAT FROM SZFEJ ', false) then begin
       Query_Run(QueryAlap, 'ALTER TABLE SZFEJ ADD SA_TSTAT VARCHAR(20)');
       Query_Run(QueryAlap, 'ALTER TABLE SZFEJ ADD SA_TTRID VARCHAR(20)');
       Query_Run(QueryAlap, 'ALTER TABLE SZFEJ ADD SA_ESTAT VARCHAR(20)');
       Query_Run(QueryAlap, 'ALTER TABLE SZFEJ ADD SA_ETRID VARCHAR(20)');
   end;

   if not Query_Run(QueryAlap, 'SELECT SA_STORN FROM SZFEJ ', false) then begin
       Query_Run(QueryAlap, 'ALTER TABLE SZFEJ ADD SA_STORN VARCHAR(20)');
       Query_Run(QueryAlap, 'UPDATE SZFEJ SET SA_STORN = ''STORNO'' WHERE SA_FLAG = ''3'' ');
   end;

   // NAV adatok az �sszes�tett sz�ml�khoz -> Teszt (T) �s �les (�)
   if not Query_Run(QueryAlap, 'SELECT SA_TSTAT FROM SZFEJ2 ', false) then begin
       Query_Run(QueryAlap, 'ALTER TABLE SZFEJ2 ADD SA_TSTAT VARCHAR(20)');
       Query_Run(QueryAlap, 'ALTER TABLE SZFEJ2 ADD SA_TTRID VARCHAR(20)');
       Query_Run(QueryAlap, 'ALTER TABLE SZFEJ2 ADD SA_ESTAT VARCHAR(20)');
       Query_Run(QueryAlap, 'ALTER TABLE SZFEJ2 ADD SA_ETRID VARCHAR(20)');
   end;

   if not Query_Run(QueryAlap, 'SELECT SA_ORSZ2 FROM SZFEJ ', false) then begin
       Query_Run(QueryAlap, 'ALTER TABLE SZFEJ ADD SA_ORSZ2 VARCHAR(3)');
       Query_Run(QueryAlap, 'UPDATE SZFEJ SET SA_ORSZ2 = (select V_ORSZ from VEVO where V_KOD = SA_VEVOKOD) ');
   end;

   if not Query_Run(QueryAlap, 'SELECT SA_STORN FROM SZFEJ2 ', false) then begin
       Query_Run(QueryAlap, 'ALTER TABLE SZFEJ2 ADD SA_STORN VARCHAR(20)');
       Query_Run(QueryAlap, 'UPDATE SZFEJ2 SET SA_STORN = ''STORNO'' WHERE SA_FLAG = ''3'' ');
   end;

   if not Query_Run(QueryAlap, 'SELECT SA_ORSZ2 FROM SZFEJ2 ', false) then begin
       Query_Run(QueryAlap, 'ALTER TABLE SZFEJ2 ADD SA_ORSZ2 VARCHAR(3)');
       Query_Run(QueryAlap, 'UPDATE SZFEJ2 SET SA_ORSZ2 = (select V_ORSZ from VEVO where V_KOD = SA_VEVOKOD) ');
   end;

   if not Query_Run(QueryAlap, 'SELECT JE_COCKPITJSZ FROM JELSZO ', false) then begin
       Query_Run(QueryAlap, 'ALTER TABLE JELSZO ADD JE_COCKPITJSZ VARCHAR(30)');
       Query_Run(QueryAlap, 'ALTER TABLE JELSZO ADD JE_COCKPITTEL VARCHAR(15)');
   end;

 	Query_Run(QueryKozos2, 'SELECT * FROM SZOTAR WHERE SZ_FOKOD = ''000'' AND SZ_ALKOD = ''991'' ', FALSE);
	  if QueryKozos2.RecordCount < 1 then begin
   		Query_Run(QueryKozos2,  'insert into szotar (SZ_FOKOD, SZ_ALKOD, SZ_MENEV, SZ_LEIRO, SZ_ERVNY) VALUES (''000'', ''991'', ''Egy�b be�ll�t�sok'', '''', 1) ', FALSE);
   		Query_Run(QueryKozos2,  'insert into szotar (SZ_FOKOD, SZ_ALKOD, SZ_MENEV, SZ_LEIRO, SZ_ERVNY) VALUES (''991'', ''MBDIJ_LIMIT'', ''4000'', ''Ezen �sszeg felett figyelmeztet, ha EUR a devizanem.'', 1) ', FALSE);
      end;

   if not Query_Run(QueryAlap, 'SELECT GK_NOPOS FROM GEPKOCSI ', false) then begin
       Query_Run(QueryAlap, 'ALTER TABLE GEPKOCSI ADD GK_NOPOS INT NULL');
       Query_Run(QueryAlap, 'UPDATE GEPKOCSI SET GK_NOPOS = 0 where GK_NOPOS is null ');
   end;

   if not Query_Run(QueryAlap, 'SELECT TK_MDM FROM telkeszulek ', false) then begin
       Query_Run(QueryAlap, 'ALTER TABLE telkeszulek ADD TK_MDM VARCHAR(100)');  // b�rmif�le comment belef�r
       end;
   // Ezek csak akkor kellenek ha m�r minden�tt ez a progi van fent DK - is
   (*
   // R�szrak� t�rl�se a g�pkocsi adatokb�l
   if Query_Run(QueryKozos2, 'SELECT GK_RESZR FROM GEPKOCSI', false) then begin
       Query_Run(QueryAlap, 'ALTER TABLE GEPKOCSI DROP COLUMN GK_RESZR ');
   end;

   *)

   // PontozasEllenor;

	// ***************************************************************************
	// Itt vannak azok, amiknek maradniuk kell!!!
	// ***************************************************************************

	if kellszotar then begin	// Ker�ltek �j elemek a sz�t�rba, �jra be kell olvasni
		Fill_SZGrid;
	end;

	// A g�pkocsi lizing lej�rt�nak figyel�se
	Query_Run(QueryKozos2,  'SELECT GK_KOD, GK_LIDAT , GK_LIHON, GK_LIPER FROM GEPKOCSI WHERE GK_LIVAN = 1 ');
	while not QueryKozos2.Eof do begin
	  {	i	 := 	StrToIntDef(copy(QueryKozos2.FieldByName('GK_LIDAT').AsString,1,4),0) * 12 + StrToIntDef(copy(QueryKozos2.FieldByName('GK_LIDAT').AsString,6,2),0)+
			 StrToIntDef(QueryKozos2.FieldByName('GK_LIHON').AsString,0) - 1;
		if ( i mod 12 ) = 0 then begin
			sn1	:= IntToStr(( i div 12 ) - 1)+'.12.31.';
		end else begin
			sn1	:= IntToStr(i div 12)+Format('.%2.2d.31.', [i mod 12]);
		end;}
    sn1	:= GetLastLizingDate( QueryKozos2.FieldByName('GK_LIDAT').AsString, QueryKozos2.FieldByName('GK_LIHON').AsString, QueryKozos2.FieldByName('GK_LIPER').AsInteger);
    sn1	:= sn1 + '.31.';

		if MaiDatum > sn1 then begin
			Query_Run(QueryKoz2,  'UPDATE GEPKOCSI SET GK_LIVAN = 2 WHERE GK_KOD = '''+QueryKozos2.FieldByName('GK_KOD').AsString+''' ');
		end;
		QueryKozos2.Next;
	end;

	// A bel�p�s d�tum�nak �s idej�nek besz�r�sa az adatb�zisba
	i	:= log_level;
	log_level	:= 0;
	Query_Run(QueryKozos2,  'UPDATE JELSZO SET JE_BEDAT = '''+FormatDateTime('YYYY.MM.DD.', now)+''', '+
		' JE_BEIDO = '''+FormatDateTime('HH:NN', now)+''', JE_BEMAX = ' +SqlSzamString(now, 14,4) +', JE_SWVER= '''+SW_VERZIO+''''+
		' WHERE JE_FEKOD = ''' +user_code+ ''' ' , FALSE);
	log_level	:= i;

//	if user_super then begin

   // Az �rfolyamok let�lt�se kezd�skor
   // kivettem, NagyP 2016-06-24
	 // 	Arfolyambetoltes;

		// Az �rfolyam hi�nyok ellen�rz�se
		EllenListTorol;
		if GetRightTag(551) <> RG_NORIGHT then begin
			sn1	:= DatumHoznap(Maidatum, -21, true);
			Query_Run(QueryKozos2, 'SELECT * FROM ARFOLYAM WHERE AR_DATUM > '''+sn1+''' AND AR_VALNEM = ''EUR'' ', FALSE);
			sn2	:= ';';
			while not QueryKozos2.Eof do begin
				sn2 := sn2 + QueryKozos2.FieldByName('AR_DATUM').AsString+';';
				QueryKozos2.Next;
			end;
			while sn1 < MaiDatum do begin
				sn1 := DatumHoznap(sn1, 1, true);
				if sn1 <> MaiDatum then begin
					if Pos(';'+sn1+';', sn2) = 0 then begin
						// Hi�nyzik az �rfolyam, ellen�rizz�k a h�tv�g�t
						tmpDate := DateTimeToNum( sn1 , '');
            if not HetvegeVagyUnnep(tmpDate) then    begin
						//if ( ( DayOfWeek(tmpDate) <> 1 ) and ( DayOfWeek(tmpDate) <> 7 ) ) then begin
							// NEM vas�rnap vagy szombat
							EllenListAppend('Hi�nyz� �rfolyam : '+sn1, false);
						end;
					end;
				end;
			end;
		end;
		if Ellenlista.Count > 0 then begin
			EllenListMutat('Hi�nyz� �rfolyamok list�ja', false);
		end;

		// Az APEH szerinti �zemanyag�rak bet�lt�se
    // -- ezt kiveszem, m�r r�gen nem m�k�dik a HTML oldal parse-ol�s�val

    {   strdate := Read_SZGrid( '888', '310' );
       if strdate < MaiDatum then begin
           UzemanyagarBetoltes;
		    Query_Run(QueryKozos,  'DELETE FROM SZOTAR WHERE SZ_FOKOD = ''888'' AND SZ_ALKOD = ''310'' ', FALSE);
		    Query_Run(QueryKozos,  'INSERT INTO SZOTAR ( SZ_FOKOD, SZ_ALKOD, SZ_MENEV, SZ_LEIRO, SZ_ERVNY ) VALUES '+
				'( ''888'', ''310'', '''+MaiDatum+''', ''�zemanyag ellen�rz�s d�tuma '', 1 ) ', FALSE);
       end;
       }

//	end;

	// Az export�l�s eld�nt�se
	Export_Available	:= false;
	{�rv�nyess�gek ellen�rz�se}
	if GetRightTag(560) <> RG_NORIGHT then begin
		Export_Available	:= true;
	end;

	// A USER INI �llom�ny kezel�se
	userini		:= ExePath+'INI\FUVAR_'+user_code+'.INI';
	if not fileExists(userini) then begin
		// A r�gi ini �tm�sol�sa az �j helyre
		CopyFile(localini, userini);
	end;
	Fill_IndexGrid;			// Beolvassa az index sorrendeket a localini -b�l
  Fill_FieldGrid;         // Beolvassa a mezoszur�sek be�ll�t�s�t
  Fill_Localfields;
	// Ellen�rizz�k a dolgoz�khoz tartoz� mez�k megl�t�t
	DolgozoMezoEllenor;
end;

procedure TEgyebDlg.Arfolyambetoltes;
begin
	Application.CreateForm(TArfolyamToltoDlg, ArfolyamToltoDlg);
	if ArfolyamToltoDlg.Tolto2 then begin
		ArfolyamToltoDlg.Tarolas;
		Query_Run(QueryKozos,  'DELETE FROM SZOTAR WHERE SZ_FOKOD = ''888'' AND SZ_ALKOD = ''100'' ', FALSE);
		Query_Run(QueryKozos,  'INSERT INTO SZOTAR ( SZ_FOKOD, SZ_ALKOD, SZ_MENEV, SZ_LEIRO, SZ_ERVNY ) VALUES '+
				'( ''888'', ''100'', '''+MaiDatum+''', ''Az utols� �rfolyamellen�rz�s d�tuma '', 1 ) ', FALSE);
	end;
//	ArfolyamToltoDlg.ShowModal;
	ArfolyamToltoDlg.Destroy;
end;

procedure TEgyebDlg.UzemanyagarBetoltes;
var
	str			: string;
	sor			: string;
	pozic		: integer;
	fazis		    : integer;
	honap,ehonap		: integer;
	osszeg		  : double;
	logstr		  : string;
	kezdat		  : string;
	vegdat		  : string;
   osszeghossz :integer;
begin
	try
		logstr		:= 'APEH �zemanyag�rak : ';
 //		str			:= IDHttp1.Get('http://www.apeh.hu/uzemanyag/uzemanyagarak');
    str			:= IDHttp1.Get('http://nav.gov.hu/magyar_oldalak/nav/szolgaltatasok/uzemanyag/uzemanyagarak/benzinar_2013.html?query=%C3%BCzemanyag');
		fazis		:= 0;
		pozic		:= Pos('<', str);
		honap		:= 0;
    osszeghossz:=3;
		while pozic > 0 do begin
			sor		:= copy(str, 1, pozic);
   //   Clipboard.Clear;
   //   Clipboard.AsText:=sor;
      sor:=StringReplace(sor,'á','',[]);    //   b>Gázolaj   <
      sor:=StringReplace(sor,'�','',[rfReplaceAll	]);    //   b>Gázolaj   <
			//if ( ( fazis = 0 ) and ( Pos( '>'+EgyebDlg.EvSzam+'.', sor ) > 0 ) ) then begin       // KG Nem mind�g tal�l �rat a h�naphoz;
			if ( ( Pos( '>'+EgyebDlg.EvSzam+'.', sor ) > 0 ) ) then begin
        //if (honap>0)and(ehonap>honap)and(fazis>0) then
        //  ShowMessage('Nem olvashat� adat: '+EvSzam+'.'+IntToStr(honap));
				honap	:= -1;
				fazis	:= 1;
				// A h�nap kider�t�se
				if Pos('december', sor) > 0 then begin
					honap	:= 12;
				end;
				if Pos('november', sor) > 0 then begin
					honap	:= 11;
				end;
				if Pos('okt', sor) > 0 then begin
					honap	:= 10;
				end;
				if Pos('szeptember', sor) > 0 then begin
					honap	:= 9;
				end;
				if Pos('augusztus', sor) > 0 then begin
					honap	:= 8;
				end;
				if Pos('lius', sor) > 0 then begin
					honap	:= 7;
				end;
				if Pos('nius', sor) > 0 then begin
					honap	:= 6;
				end;
				if Pos('jus', sor) > 0 then begin
					honap	:= 5;
				end;
				if Pos('prilis', sor) > 0 then begin
					honap	:= 4;
				end;
				if Pos('rcius', sor) > 0 then begin
					honap	:= 3;
				end;
				if Pos('febru', sor) > 0 then begin
					honap	:= 2;
				end;
				if Pos('janu', sor) > 0 then begin
					honap	:= 1;
				end;
				if honap	 = -1 then begin
					fazis	:= 0;
				end;
			end;
//			if ( ( fazis = 1 ) and (Pos('zolaj<', sor) > 0 ) )then begin
			if ( ( fazis = 1 ) and (Pos('Gzolaj', sor) > 0 ) )then begin
				fazis	:= 2;
        ehonap:=honap;
			end;
//			if ( ( fazis = 2 ) and ((copy(sor,1,7) = 'p align')or(copy(sor,1,7)='strong>')and(pos(copy(sor,8,1),'123456789')>0)) ) then begin
			if ( fazis = 2 ) then begin
       sor:=StringReplace(sor,'p align="left">','',[]);
       sor:=StringReplace(sor,'strong>','',[]);
       sor:=StringReplace(sor,'b>','',[]);
			 if pos(copy(sor,1,1),'123456789')>0 then begin
				fazis	:= 0;
				// Az �sszeg meghat�roz�sa
		{		if Pos('>', sor) > 0 then begin
					sor := copy(sor, Pos('>', sor)+1, 999);
				end;
				if Pos(' ', sor) > 0 then begin
					sor := copy(sor, 1, Pos(' ', sor)-1);
        end
        else begin
					sor := copy(sor, 1, osszeghossz);
				end;  }
				sor := copy(sor, 1, osszeghossz);
				osszeg	:= StringSzam(sor);
				logstr	:= logstr + IntToStr(honap)+'('+sor+'), ';
				// Az adatb�zis ellen�rz�se
				kezdat	:= EgyebDlg.EvSzam + '.'+Format('%2.2d', [honap])+'.01.';
				vegdat	:= FormatDateTime('YYYY.MM.DD.', Endofthemonth(GetDateFromStr(kezdat)));
				Query_Run(QueryKozos,  'SELECT UA_ERTEK FROM UZEMANYAG WHERE UA_DATUM = '''+kezdat+''' '+
					' AND UA_DATKI = '''+vegdat+''' ');
				if QueryKozos.RecordCount < 1 then begin
					// Nem l�tezik a bejegyz�s, meg kell csin�lni
					Query_Insert (QueryKozos, 'UZEMANYAG',
						[
						'UA_UAKOD', IntToStr(GetNextCode('UZEMANYAG', 'UA_UAKOD', 0, 0)),
						'UA_ERTEK', SqlSzamString(osszeg,12,4),
						'UA_DATUM', ''''+kezdat+'''',
						'UA_DATKI', ''''+vegdat+''''
						]);
					logstr	:= logstr + '(+), ';
				end else begin
					if StringSzam(QueryKozos.FieldByName('UA_ERTEK').AsString) <> osszeg then begin
						NoticeKi('Elt�r� APEH �zemanyag �r : '+Evszam+'.'+IntToStr(honap)+'. '+
							QueryKozos.FieldByName('UA_ERTEK').AsString+' <> '+Format('%f', [osszeg]));
						logstr	:= logstr + '(-), ';
					end;
				end;
       end;
			end;
			str 	:= copy(str, pozic+1, 999999);
			pozic	:= Pos('<', str);
		end;
//		HibaKiiro(logstr);
	except
		HibaKiiro('Az APEH �zemanyag�rak bet�lt�se nem siker�lt!');
	end;
end;


procedure TEgyebDlg.WriteRowColor;
begin
	Query_Run(QueryKozos,  'DELETE FROM SZOTAR WHERE SZ_FOKOD = ''999'' AND SZ_ALKOD = ''711'' ', FALSE);
	Query_Run(QueryKozos,  'INSERT INTO SZOTAR ( SZ_FOKOD, SZ_ALKOD, SZ_MENEV, SZ_LEIRO, SZ_ERVNY ) VALUES '+
			'( ''999'', ''711'', '''+IntToStr(row_color)+''', ''A kiemelt sorok sz�ne '', 1 ) ', FALSE);
end;

procedure TEgyebDlg.WebBrowser1NavigateComplete2(Sender: TObject;
  const pDisp: IDispatch; var URL: OleVariant);
begin
  if CurDispatch = nil then
	 CurDispatch := pDisp; { save for comparison }
end;

function 	TEgyebDlg.GetFieldStr( szkod : string ) : string;
var
	sorszam	: integer;
begin
	Result		:= '';
 	sorszam		:= FieldGrid.Cols[0].IndexOf(szkod);
	if sorszam > -1 then begin
 		Result 	:= FieldGrid.Cells[1,sorszam];
 	end;
end;

procedure 	TEgyebDlg.SetFieldStr( szkod, fieldstr : string );
var
	sorszam	: integer;
begin
  sorszam		:= FieldGrid.Cols[0].IndexOf(szkod);
  if sorszam > -1 then begin
 		FieldGrid.Cells[1,sorszam] 					:= fieldstr;
  end else begin
 		FieldGrid.RowCount 							:= FieldGrid.RowCount + 1;
 		FieldGrid.Cells[0,FieldGrid.RowCount - 1] 	:= szkod;
 		FieldGrid.Cells[1,FieldGrid.RowCount - 1] 	:= fieldstr;
  end;
end;


function TEgyebDlg.VevoErvEllenor(napok, kmek: integer): integer;
var
	st			: string;
	elonap,count   	: integer;
	moddat		: string;
begin
  count:=0;
	EllenListAppend('', false);
	EllenListAppend('M�r lej�rt vev�1 elemek :', false);
	EllenListAppend('============================', false);
	EllenListAppend(Format('Be�ll�tott param�terek : nap = %d;  ', [napok]), false);
	elonap	:= napok;
	if elonap < 1 then begin
		elonap	:= StrToIntDef(Read_SZGrid('999', '641'),0); {A napok sz�ma}
	end;
	// A mai napig lej�rt elemek list�z�sa
	moddat	:= MaiDatum;
	// Vev�i hat�rid�k ellen�rz�se
	if Query_Run (QueryKozos, 'SELECT DISTINCT VI_VEKOD FROM VEVOIRAT ORDER BY VI_VEKOD ', true) then begin
		while not QueryKozos.Eof do begin
			st:='SELECT V_NEV, VI_LEDAT, VI_DONEV FROM VEVOIRAT, VEVO WHERE VI_VEKOD = '''+QueryKozos.FieldByName('VI_VEKOD').AsString+''' '+
				' AND VI_LEDATS < '''+moddat+''' AND V_KOD = VI_VEKOD AND  VI_LEELL = '''+'I'+''' ORDER BY VI_LEDAT ';
			Query_Run (QueryKoz2,st, true);
      st:='';
			if QueryKoz2.RecordCount > 0 then begin
				while not QueryKoz2.Eof do begin
					st:=QueryKoz2.FieldByName('V_NEV').AsString+' ('+QueryKoz2.FieldByName('VI_LEDAT').AsString+ ')�('+ QueryKoz2.FieldByName('VI_DONEV').AsString+')';
  				EllenListAppend(st, false);
          Inc(count);
					QueryKoz2.Next;
				end;
			 //	EllenListAppend(st, false);
			end;
			QueryKozos.Next;
		end;
	end;

	EllenListAppend('', false);
	EllenListAppend('Lej�r� vev�i elemek :', false);
	EllenListAppend('========================', false);
	moddat	:= DatumHozNap(MaiDatum, elonap, false);
	// Vev�i hat�rid�k ellen�rz�se
	if Query_Run (QueryKozos, 'SELECT DISTINCT VI_VEKOD FROM VEVOIRAT ORDER BY VI_VEKOD ', true) then begin
		while not QueryKozos.Eof do begin
			st:='SELECT V_NEV, VI_LEDAT, VI_DONEV FROM VEVOIRAT, VEVO WHERE VI_VEKOD = '''+QueryKozos.FieldByName('VI_VEKOD').AsString+''' '+
				' AND VI_LEDATS < '''+moddat+''' AND VI_LEDATS >= '''+MaiDatum+''' AND V_KOD = VI_VEKOD AND  VI_LEELL = '''+'I'+''' ORDER BY VI_LEDAT ';
			Query_Run (QueryKoz2,st, true);
      st:='';
			if QueryKoz2.RecordCount > 0 then begin
				while not QueryKoz2.Eof do begin
					st:=QueryKoz2.FieldByName('V_NEV').AsString+' ('+QueryKoz2.FieldByName('VI_LEDAT').AsString+ ')�('+ QueryKoz2.FieldByName('VI_DONEV').AsString+')';
  				EllenListAppend(st, false);
          Inc(count);
					QueryKoz2.Next;
				end;
				//EllenListAppend(st, false);
			end;
			QueryKozos.Next;
		end;
	end;
  Result:=count;
end;

function TEgyebDlg.GoogleMap(rendszam,rajt,cel,fel_le: string; teszt:Boolean): Boolean;
begin
  if (rendszam=EmptyStr)and(rajt='')and(cel='')  and not teszt then
  begin
    ShowMessage('Nincs rendsz�m, vagy egy�b adat!');
   exit;
  end;
 { if (not ADOConnectionNavCenter.Connected) then
  begin
    ShowMessage('Nincs adatb�ziskapcsolat! (NavCenter)');
   exit;
  end; }
  if FTerkep = nil then Application.CreateForm(TFTerkep, FTerkep);  // egyszer-egyszer el�fordult, hogy Access Violation-t adott
//  Application.CreateForm(TFTerkep, FTerkep);
  FTerkep.Rendszam:=rendszam;
  FTerkep.Caption:=rendszam;
  if teszt then
    FTerkep.Visible:=False;

  FTerkep.Edit1.Text:=cel;
  FTerkep.Edit2.Text:=rajt;
  FTerkep.fel_le:=fel_le;
  FTerkep2.Edit1.Text:=rajt;
  FTerkep2.Edit2.Text:=cel;
  FTerkep.Panel2.Visible:= (cel<>'');
  FTerkep.CheckBoxTraffic.Visible:=not FTerkep.Panel2.Visible;
  //FTerkep.Show;
  FTerkep.TESZT:=teszt;
  if not teszt then
    FTerkep.ShowModal
  else
    FTerkep.OnActivate(self);
 // FTerkep.Free;
end;

function TEgyebDlg.HaviZaroKmEllenor(ev: string): integer;
var
	st,ho,forh,eho			: string;
	count,i,maxkm ,utho 	: integer;
  km,ekm: real;
begin
   count   :=0;
	EllenListAppend('', false);
	EllenListAppend('Havi z�r� km ellen�rz�se :', false);
	EllenListAppend('============================', false);
	EllenListAppend('', false);
   maxkm   :=30000;
   utho    :=MonthOf(date);
   if utho=1 then begin // ha janu�r, akkor az el�z� �vet n�zem
       ev      :=IntToStr( strtoint(ev)-1);
       utho    :=12;
   end else begin
       utho    :=utho-1;
   end;
	if Query_Run (QueryKozos, 'SELECT  gk_resz,gk_forhe,gk_sforh FROM gepkocsi where (gk_kulso=0)and(gk_arhiv=0)and(gk_kivon=0)and(gk_potos<>1)and(LEN(gk_resz)>4) ORDER BY gk_resz ', true) then begin
	    while not QueryKozos.Eof do begin
//     for i:=1 to 12 do begin
     km:=0;
     for i:=1 to  utho do begin
      if i<10 then
        ho:=ev+'.0'+IntToStr(i)
      else
        ho:=ev+'.'+IntToStr(i) ;

      ekm:=km;
      if i=1 then
      begin
        eho:=IntToStr(StrToInt(ev)-1)+'.12';
  			st:='SELECT ZK_KMORA,ZK_RENSZ  FROM ZAROKM WHERE ZK_RENSZ = '''+QueryKozos.FieldByName('GK_RESZ').AsString+''' AND SUBSTRING(ZK_DATUM,1,7)='''+eho+'''';
	  		Query_Run (QueryAlap,st, true);
        ekm:= QueryAlap.FieldByName('ZK_KMORA').AsFloat;
      end;
			st:='SELECT ZK_KMORA,ZK_RENSZ  FROM ZAROKM WHERE ZK_RENSZ = '''+QueryKozos.FieldByName('GK_RESZ').AsString+''' AND SUBSTRING(ZK_DATUM,1,7)='''+ho+'''';
			Query_Run (QueryAlap,st, true);
      km:= QueryAlap.FieldByName('ZK_KMORA').AsFloat;
			if (km= 0) then begin
        forh:=copy(QueryKozos.FieldByName('GK_FORHE').AsString,1,7) ;
        if  (QueryKozos.FieldByName('GK_SFORH').AsString<>'') then forh:=copy(QueryKozos.FieldByName('GK_SFORH').AsString,1,7);
        if (forh<>'')and(forh<=ho) then
        begin
					st:=QueryKozos.FieldByName('gk_resz').AsString+'  '+ho+'  Hi�nyz� z�r� km';
  				EllenListAppend(st, false);
          Inc(count);
        end;
			end;
			if (km<>0)and(km<ekm)and(i>1) then begin
					st:=QueryKozos.FieldByName('gk_resz').AsString+'  '+ho+'  El�z� km: '+FloatToStr(ekm)+'>  Mostani km: '+FloatToStr(km)  ;
  				EllenListAppend(st, false);
          Inc(count);
			end;
			if (km<>0)and(ekm<>0)and(km>ekm+maxkm)and(i>1) then begin
					st:=QueryKozos.FieldByName('gk_resz').AsString+'  '+ho+'  El�z� km: '+FloatToStr(ekm)+'  Mostani km: '+FloatToStr(km)+'  Elt�r�s: '+FloatToStr(km-ekm)  ;
  				EllenListAppend(st, false);
          Inc(count);
			end;
     end;
		 QueryKozos.Next;
   	 //EllenListAppend('', false);
		end;
	end;
  Result:=count;
end;

function TEgyebDlg.PozicioszamEllenor(ev: string): integer;
var
	st	 	: string;
	count 	: integer;
begin
  count:=0;
	EllenListAppend('', false);
	EllenListAppend('Poz�ci�sz�m ellen�rz�se (hi�nyz� poz�ci�sz�mok):', false);
	EllenListAppend('================================================', false);
	EllenListAppend('', false);

	st:='SELECT  MB_MBKOD,MB_VEKOD,MB_DATUM, MB_VENEV FROM MEGBIZAS,'+GetTableFullName(EgyebDlg.v_koz_db,'VEVO')+
//  '.dbo.vevo where (mb_vekod=v_kod)and(ve_pkell=1)AND(mb_pozic='''''+')and(mb_nopoz<>1)and(SUBSTRING(mb_datum,1,4)='''+ev+''') order by mb_datum';
  ' where (mb_vekod=v_kod)and(ve_pkell=1)AND(mb_pozic='''''+')and(mb_nopoz<>1)and(SUBSTRING(mb_datum,1,4)='''+ev+''') order by mb_datum';

//  Clipboard.Clear;
//  Clipboard.AsText:=st;
	if Query_Run (QueryAlap,st, true) then
  begin
    QueryAlap.RecordCount;
	  while not QueryAlap.Eof do
    begin
		 st:=QueryAlap.FieldByName('mb_mbkod').AsString+'  '+QueryAlap.FieldByName('mb_datum').AsString+'  '+QueryAlap.FieldByName('mb_venev').AsString+'  ';
//  	 EllenListAppend(st, false);

     Query_Run(Queryal2, 'SELECT DISTINCT MS_MBKOD, MS_SORSZ, MS_FAJKO, MS_FAJTA,  MS_FELNEV, MS_FELTEL, MS_FELCIM, MS_FELDAT, MS_FELIDO, '+
     'MS_FETDAT, MS_FETIDO, MS_LERNEV, MS_LERTEL, MS_LERCIM, MS_LERDAT, MS_LERIDO, MS_LETDAT,MS_LETIDO, MS_FELARU, MS_FSULY, MS_FEPAL, MS_LSULY,'+
     ' MS_LEPAL, MS_FELSE1, MS_FELSE2, MS_FELSE3, MS_FELSE4,MS_FELSE5, MS_LERSE1, MS_LERSE2, MS_LERSE3, MS_LERSE4, MS_LERSE5, MS_ORSZA, MS_FELIR,'+
     ' MS_LELIR, MS_FORSZ,MS_RENSZ from MEGSEGED WHERE MS_MBKOD = '+QueryAlap.FieldByName('mb_mbkod').AsString+' AND MS_FAJTA='''+'norm�l''') ;
//     Query_Run(Queryal2, 'SELECT * FROM MEGSEGED WHERE MS_MBKOD = '+QueryAlap.FieldByName('mb_mbkod').AsString+' AND MS_FAJTA='''+'norm�l''') ;
     Queryal2.First;
     if not Queryal2.Eof then
    	 EllenListAppend(st, false);
     while not Queryal2.Eof do
     begin
        st:='    ('+Queryal2.FieldByName('ms_felnev').AsString+'  '+Queryal2.FieldByName('ms_forsz').AsString+'  '+Queryal2.FieldByName('ms_feltel').AsString+'  '+Queryal2.FieldByName('ms_feldat').AsString+') - ('
                   +Queryal2.FieldByName('ms_lernev').AsString+'  '+Queryal2.FieldByName('ms_orsza').AsString+'  '+Queryal2.FieldByName('ms_lertel').AsString+'  '+Queryal2.FieldByName('ms_lerdat').AsString+')';
        EllenListAppend(st,False);
        Queryal2.Next;
     end;
     Inc(count);
		 QueryAlap.Next;
		end;
	end;
  Result:=count;
end;

function TEgyebDlg.HutosGk(rendsz: string): Boolean;
var
    HUTOTIP,gkkat: string;
begin
  Result:=False;
	HUTOTIP:= EgyebDlg.Read_SZGrid( '999', '922' );
 	gkkat	:= ','+Query_Select('GEPKOCSI', 'GK_RESZ', rendsz, 'GK_GKKAT')+',';
  if pos(gkkat,HUTOTIP)<>0 then
  begin
    Result:=True;
  end;
end;

function TEgyebDlg.Szamla_Arf_szamol(ujkod, arfolyam: double): Boolean;
var
  val:Boolean;
  eHUF,aHUF,eEUR,aEUR :double;
  db:integer;
begin
  Result:=False;
  eHUF:=0;
  aHUF:=0;
  eEUR:=0 ;
  aEUR:=0;
  db:=0;
  TSZFEJ.Close;
  TSZFEJ.Parameters[0].Value:=ujkod;
  TSZFEJ.Open;
  TSZFEJ.First;

  if TSZFEJ.Eof then
    exit;         // Nincs ilyen sz�mla
  if (TSZFEJSA_KOD.Value<>EmptyStr)and(TSZFEJSA_FLAG.Value='0') then
    exit;         // van sz�mlasz�m

  //val:=TSZFEJSA_VALNEM.Value='EUR';

  TSZSOR.Close;
  TSZSOR.Parameters[0].Value:=ujkod;
  TSZSOR.Open;
  TSZSOR.First;
  Try
   while not TSZSOR.Eof do
   begin
    if TSZSORSS_DARAB.Value<>0 then
    begin
      if TSZSORSS_VALNEM.Value='HUF' then
      begin
        TSZSOR.Edit;
        TSZSORSS_ARFOL.Value:=arfolyam;
        TSZSOR.Post;
        inc(db);
      end ;
      if TSZSORSS_VALNEM.Value='EUR' then
      begin
        TSZSOR.Edit;
        TSZSORSS_VALARF.Value:=arfolyam;
        TSZSORSS_ARFOL.Value:=arfolyam;
//        TSZSORSS_EGYAR.Value:=RoundTo( TSZSORSS_VALERT.Value/TSZSORSS_DARAB.Value*arfolyam,2);
        TSZSORSS_EGYAR.Value:=RoundTo( TSZSORSS_VALERT.Value*arfolyam,-2);
        TSZSOR.Post;
        inc(db);
      end;
      eHUF:=eHUF+(TSZSORSS_EGYAR.Value*TSZSORSS_DARAB.Value);
      aHUF:=aHUF+(TSZSORSS_EGYAR.Value*TSZSORSS_DARAB.Value*TSZSORSS_AFA.Value/100);
      eEUR:=eEUR+(TSZSORSS_VALERT.Value*TSZSORSS_DARAB.Value);
      aEUR:=aEUR+(TSZSORSS_VALERT.Value*TSZSORSS_DARAB.Value*TSZSORSS_AFA.Value/100);
    end;
    TSZSOR.Next;
   end;
   if db>0 then
   begin
    TSZFEJ.Edit;
    TSZFEJSA_OSSZEG.Value:=RoundTo(eHUF,-2);
    TSZFEJSA_AFA.Value:=RoundTo(aHUF,-2);
    TSZFEJSA_VALOSS.Value:=RoundTo(eEUR,-2);
    TSZFEJSA_VALAFA.Value:=RoundTo(aEUR,-2);
    TSZFEJ.Post;
   end;
  Except
  End;
  TSZFEJ.Close;
  TSZSOR.Close;
  Result:=True;
end;

function TEgyebDlg.UzemoraEllenorzes(jaratszam, rendszam,km: string): Boolean;
begin
  QueryAlap.Close;
  QueryAlap.SQL.Clear;
  QueryAlap.SQL.Add('select * from jarpotos where JP_JAKOD='''+jaratszam+''' and JP_POREN='''+rendszam+''' and JP_UZEM1<='+km+' and JP_UZEM2>='+km);
  QueryAlap.Open;
  QueryAlap.First;
  Result:=not QueryAlap.Eof;
  QueryAlap.Close;
end;

function TEgyebDlg.JaratKoltseg(jaratkod: string): double;
var
  rensz,gkkat,skm,sertek,skttol,sktgig:string;
  km: integer;
  ertek,kttol,ktgig,ft_km,atlag: double;
begin
  Result:=0;
  rensz:=Query_Select('JARAT','JA_KOD',jaratkod,'JA_RENDSZ');
  skm:=Query_Select('JARAT','JA_KOD',jaratkod,'JA_OSSZKM');
  if rensz=EmptyStr then exit;
  km:= StrToIntDef( skm,0);
  if km<=1 then exit;
  gkkat:=Query_Select('GEPKOCSI','GK_RESZ',rensz,'GK_GKKAT');

  ertek:=0;
	Query_Run(QueryAlap, 'select * from koltseg where ks_jakod='''+jaratkod+'''');
  while not QueryAlap.Eof do
  begin
    if (QueryAlap.Fieldbyname('KS_TIPUS').Value<>0) and  (QueryAlap.Fieldbyname('KS_TIPUS').Value<>2) and  (QueryAlap.Fieldbyname('KS_TIPUS').Value<>3) then  // �zema., adblue
    begin
      ertek:=ertek+QueryAlap.Fieldbyname('KS_ERTEK').Value ;
    end;
    if (QueryAlap.Fieldbyname('KS_TIPUS').Value=0) then  // �zema.
    begin
       atlag:=GetAtlagFogyasztas(QueryAlap.Fieldbyname('KS_KTKOD').Value);
       if atlag>0 then
       begin
          ertek:=ertek+QueryAlap.Fieldbyname('KS_ERTEK').Value / QueryAlap.Fieldbyname('KS_UZMENY').Value * atlag * km / 100;
       end;
    end;
    QueryAlap.Next;
  end;
  QueryAlap.Close;

  //ertek:=StrToFloatDef(sertek,0);

  if ertek=0 then exit;

  skttol:=Query_Select('KATINTER','KI_GKKAT',gkkat,'KI_KTTOL');
  sktgig:=Query_Select('KATINTER','KI_GKKAT',gkkat,'KI_KTGIG');
  kttol:=StrToFloatDef(skttol,0);
  ktgig:=StrToFloatDef(sktgig,0);

  ft_km:=RoundTo( ertek/km ,0);
  if (ft_km<kttol) or (ft_km>ktgig) then
    Result:=ft_km;

end;

function TEgyebDlg.GetAtlagFogyasztas(kod: string): double;
var
  rensz,tipus,datum: string;
  kkmora,vkmora, km, uzmeny: double;
begin
  Result:=0;
	Query_Run(Queryal2, 'select * from koltseg where ks_ktkod='''+kod+'''');
  tipus:=Queryal2.Fieldbyname('KS_TIPUS').AsString;
  rensz:=Queryal2.Fieldbyname('KS_RENDSZ').AsString;
  datum:=Queryal2.Fieldbyname('KS_DATUM').AsString;
  vkmora:=Queryal2.Fieldbyname('KS_KMORA').AsFloat;

	Query_Run(Queryal2, 'select * from koltseg where ks_rendsz='''+rensz+''' and ks_tipus='+tipus+' and ks_datum<'''+datum+''' order by ks_datum');
  Queryal2.First;
  kkmora:=Queryal2.Fieldbyname('KS_KMORA').AsFloat;
  km:=vkmora-kkmora;
  if km<=0 then exit;

	Query_Run(Queryal2, 'select sum(ks_uzmeny) as uzme from koltseg where ks_rendsz='''+rensz+''' and ks_tipus='+tipus+' and ks_datum<'''+datum+'''');
  uzmeny:=Queryal2.Fieldbyname('uzme').AsFloat;
  Queryal2.Close;
  if uzmeny<=0 then exit;

  Result:=RoundTo( uzmeny/km*100,-1);


end;

procedure TEgyebDlg.JaratIdotartam;
var
  ora,min: integer;
  kdat,vdat,kido,vido, S: string;
begin
//	Query_Run(Queryal2, 'select * from jarat where JA_JKEZD<>''''  and JA_JVEGE<>'''' and (JA_JAORA='''' or JA_JAORA is null) ');
	Query_Run(Queryal2, 'select * from jarat where JA_JKEZD<>''''  and JA_JVEGE<>'''' and JA_JAORA is null ');
  while not Queryal2.Eof do begin
    kdat:=Queryal2.Fieldbyname('JA_JKEZD').AsString;
    vdat:=Queryal2.Fieldbyname('JA_JVEGE').AsString;
    kido:=Queryal2.Fieldbyname('JA_KEIDO').AsString;
    vido:=Queryal2.Fieldbyname('JA_VEIDO').AsString;
    if kido='24:00' then kido:='23:59';
    if vido='24:00' then vido:='23:59';

    ora:= MinutesBetween(StrToDateTime(kdat+' '+kido),StrToDateTime(kdat+' '+vido)) div 60         ;
    min:= MinutesBetween(StrToDateTime(kdat+' '+kido),StrToDateTime(kdat+' '+vido)) mod 60         ;
    if min>=30 then ora:=ora+1;

    Try
		  // SQLAlap.CommandText:= 'UPDATE jarat SET JA_JAORA = '+IntToStr(ora)+' WHERE JA_KOD='+Queryal2.Fieldbyname('JA_KOD').AsString ;
      // SQLAlap.Execute;

      S:='UPDATE jarat SET JA_JAORA = '+IntToStr(ora)+' WHERE JA_KOD='+Queryal2.Fieldbyname('JA_KOD').AsString ;
      Command_Run(SQLAlap, S, true);

    Except
    End;
    Queryal2.Next;
  end;
  Queryal2.Close;

end;

function TEgyebDlg.HetvegeVagyUnnep(datum: TDate): Boolean;
begin
   Result:=False;
   if DayOfWeek(datum) = 1 then
   begin
       Result:=True;      { vas�rnap }
       exit;
   end;
   if DayOfWeek(datum) = 7 then
   begin
       Result:=True;      { szombat  }
       Exit;
   end;
   if Query_Run(QueryKozos, 'Select COUNT(na_unnep) NAPSZAM from NAPOK WHERE na_orsza=''HU'' and na_unnep=''1'' and na_datum='''+FormatDateTime('YYYY.MM.DD', datum)+''' ') then begin
       if StrToIntDef(QueryKozos.FieldByName('NAPSZAM').AsString, 0) > 0 then begin
           Result:=True;
       end;
   end;
end;



function TEgyebDlg.WriteLogFile(FName, Text: string): Boolean;
var
  LogFile: TextFile;
begin
  // prepares log file
  AssignFile (LogFile, Fname);
  if FileExists (FName) then
    Append (LogFile) // open existing file
  else
    Rewrite (LogFile); // create a new one

  // write to the file and show error
  Writeln (LogFile,Text);

  // close the file
  CloseFile (LogFile);
  Result:=True;
end;

function TEgyebDlg.AppendLog(LogName, LogText: string): Boolean;
var
  TeljesFN: string;
begin
// ExtractFilePath(Application.ExeName);
if not DirecToryExists(DocumentPath+'LOG') then begin
		ForceDirectories(DocumentPath+'LOG');
	end;
TeljesFN:=DocumentPath+'LOG'+'\'+UpperCase(LogName)+'.TXT';
Result:=WriteLogFile(TeljesFN,DateTimeToStr(Now)+' '+LogText);
end;


function TEgyebDlg.Szamla_szam_datum_ell(szszam, szdatum: string; gyujto:boolean): Boolean;
begin
// Van e kisebb sz�mlasz�m nagyobb d�tummal?
  Result:=True;
  if (szszam='')or(szdatum='') then
    exit;

  if gyujto then
    Query_Run(QueryAlap, 'select sa_kod,sa_kidat from SZFEJ2 where sa_kod<>'''''+' and sa_kod<'''+szszam+''' and sa_kidat>'''+szdatum+'''')
  else
    Query_Run(QueryAlap, 'select sa_kod,sa_kidat from SZFEJ  where sa_resze=0 and sa_kod<>'''''+' and sa_kod<'''+szszam+''' and sa_kidat>'''+szdatum+'''');

  if QueryAlap.RecordCount>0 then  // HIBA  ; van k�s�bbi sz�mla
  begin
    Result:=False;
  end;
  QueryAlap.Close;
end;

function TEgyebDlg.Jarat_Egyeb_km(jakod: string): integer;
var
  mbkod, mbdb, okm: integer;
  ejakod: string;
begin
   mbkod:=StrToIntDef( Query_Select('JARATMEGBIZAS','JM_JAKOD',jakod,'JM_MBKOD'),0);
   if Query_Run(QueryAlap, 'SELECT DISTINCT ms_jakod FROM MEGSEGED where ms_mbkod='+IntToStr(mbkod)+' and ms_fajko<>0') then begin
       okm:=0;
       while not QueryAlap.EOF do begin   // MEGSEGED
           ejakod:=QueryAlap.Fieldbyname('MS_JAKOD').AsString ;
    	    Query_Run(Queryal2, 'SELECT VI_JAKOD FROM VISZONY where VI_JAKOD='''+ejakod+'''');
           mbdb:= Queryal2.RecordCount;
    	    Query_Run(Queryal2, 'SELECT JA_OSSZKM FROM JARAT where ja_kod='''+ejakod+'''');
           okm:= okm+ Trunc(QueryAl2.Fieldbyname('JA_OSSZKM').AsInteger / mbdb);
           QueryAlap.Next;
       end;
   end;
   Result  := okm;
end;

function TEgyebDlg.IsFormOpen(const FormName: string): Boolean;
var
  i: Integer;
begin
  Result := False;
  for i := Screen.FormCount - 1 DownTo 0 do
    if (Screen.Forms[i].Name = FormName) then
    begin
      Result := True;
      Break;
    end;
end;

function TEgyebDlg.Szamlaszam_ell(kezd_dat: string): string;
var
  hiba,sszam: string;
  szam,k,v,i: integer;
begin
  hiba:='';
  Result:=hiba;
  kezd_dat:=copy(kezd_dat,1,4);
  QueryAlap.SQL.Clear;
  QueryAlap.SQL.Add('select DISTINCT sa_kod from szfej where (sa_kod<>'''''+')and(sa_datum like '''+kezd_dat+'%'''+') order by sa_kod');
  QueryAlap.Open;
  QueryAlap.Last;
  if QueryAlap.Eof then
    exit;
  v:=QueryAlap.Fieldbyname('SA_KOD').AsInteger;
  QueryAlap.First;
  szam:=QueryAlap.Fieldbyname('SA_KOD').AsInteger;
  k:=szam;
  for i:=k to v do
  begin
    if i=QueryAlap.Fieldbyname('SA_KOD').AsInteger then  //OK
    begin
      QueryAlap.Next;
    end
    else
    begin
      hiba:=hiba+','+IntToStr(i);
    end;
  end;
  QueryAlap.Close;
  Result:=hiba;
end;

procedure TEgyebDlg.SetMaidatum;
begin
  //MaiDatum    := SQLDATE.Execute.Fields[0].Value;
  MaiDatum        := GetServerDateTime;
end;

function TEgyebDlg.JaratkoltsegHUF(jakod: string): double;
var
  ertek: double;
begin
  ertek:=0;
  Query_Run(QueryAlap,'select * from koltseg where ks_jakod='''+jakod+'''');
  while not QueryAlap.Eof do
  begin
    ertek:=ertek+QueryAlap.Fieldbyname('KS_ERTEK').Value;
    QueryAlap.Next;
  end;
  QueryAlap.Close;
  Result:=ertek;
end;              

function TEgyebDlg.JaratkoltsegEUR(jakod: string): double;
var
  ertek: double;
begin
  ertek:=0;
  Query_Run(QueryAlap,'select * from koltseg where ks_jakod='''+jakod+'''');
  while not QueryAlap.Eof do
  begin
    if QueryAlap.Fieldbyname('KS_VALNEM').Value = 'EUR' then
      ertek:=ertek+QueryAlap.Fieldbyname('KS_OSSZEG').Value
    else
      ertek:=ertek+ GetValueInOtherCur(QueryAlap.Fieldbyname('KS_OSSZEG').Value,QueryAlap.FieldByName('KS_DATUM').AsString,QueryAlap.Fieldbyname('KS_VALNEM').Value,'EUR' );

    QueryAlap.Next;
  end;
  QueryAlap.Close;
  Result:=ertek;
end;

function TEgyebDlg.Szamlaertek2jar_megb(sujkod,sakod: string): boolean;
var
  arany, belf, kulf, osszeg: double;
  valnem, jakod,vikod: string;
  S: string;
begin
    Result:=False;
    if sujkod='' then exit;
    if sakod ='' then exit;
    arany:=0;
    //sakod:=Query_Select('SZFEJ','SA_UJKOD',sujkod,'SA_KOD') ;
    valnem:=Query_Select('SZFEJ','SA_UJKOD',sujkod,'SA_VALNEM') ;
    osszeg:=StrToFloat( Query_Select('SZFEJ','SA_UJKOD',sujkod,'SA_VALOSS')) ;
    jakod:= Query_Select('VISZONY','VI_UJKOD',sujkod,'VI_JAKOD');
    vikod:= Query_Select('VISZONY','VI_UJKOD',sujkod,'VI_VIKOD');
    if (StrToFloatDef( Query_Select('VISZONY','VI_UJKOD',sujkod,'VI_FDIMP'),0) <>0) and
       (StrToFloatDef( Query_Select('VISZONY','VI_UJKOD',sujkod,'VI_FDKOV'),0) <>0) then      // belf.-k�lf. megoszt�s van
    begin
    	arany 			:= StringSzam(EgyebDlg.Read_SZGrid('999','601'));
    end;
    if arany>0 then begin
		  belf 	:= (osszeg*arany/100);
		  kulf 	:= (osszeg*(100-arany)/100);
    end else begin
       belf:=osszeg;
       kulf:=0;
    end;
    Try
      // a Viszonyba vissza�rjuk az �sszeget is. H�tha v�ltozott.

      // A logol�s miatt Query_Run legyen [NagyP 2015-02-02]
     	// SQLAlap.CommandText:= 'UPDATE VISZONY SET VI_SAKOD='''+sakod+''',VI_VALNEM='''+valnem+''',VI_FDEXP='+SqlSzamString(osszeg,12,2)+',VI_FDIMP='+SqlSzamString(belf,12,2)+
      // ',VI_FDKOV='+SqlSzamString(kulf,12,2)+' WHERE VI_UJKOD='+sujkod ;
      // SQLAlap.Execute;

      S:='UPDATE VISZONY SET VI_SAKOD='''+sakod+''',VI_VALNEM='''+valnem+''',VI_FDEXP='+SqlSzamString(osszeg,12,2)+',VI_FDIMP='+SqlSzamString(belf,12,2)+
      ',VI_FDKOV='+SqlSzamString(kulf,12,2)+' WHERE VI_UJKOD='+sujkod ;
      Query_Run(QueryAlap, S, true);

      // Megbiz�sba is beirjuk: Sz�mlasz�m, �sszeg

      // A logol�s miatt Query_Run legyen [NagyP 2015-02-02]
	  	// SQLAlap.CommandText:='UPDATE MEGBIZAS SET MB_SAKO2='''+sakod+''',MB_SAVAL='''+valnem+''',MB_SAOSS='+SqlSzamString(osszeg,12,2)+
      // ' WHERE MB_JAKOD='''+jakod+''' and MB_VIKOD='''+vikod+'''';
      // SQLAlap.Execute;
      if (jakod<>'') and (vikod<>'') then begin   // ne tegye, ha a VISZONYb�l nem j�tt �rt�k
        S:='UPDATE MEGBIZAS SET MB_SAKO2='''+sakod+''',MB_SAVAL='''+valnem+''',MB_SAOSS='+SqlSzamString(osszeg,12,2)+
        ' WHERE MB_JAKOD='''+jakod+''' and MB_VIKOD='''+vikod+'''';
        Query_Run(QueryAlap, S, true);
        end;

      Result:=True;
    Except
      on E: exception do begin
         HibaKiiro('Szamlaertek2jar_megb: '+E.Message+' at "'+S+'"');
         Result:=False;
         end;
    End;
end;

function TEgyebDlg.hutospotEllenor: integer;
var
    HUTOTIP,st: string;
    count, km, ukm, kulonbseg, i,skm: integer;
begin
	HUTOTIP:= EgyebDlg.Read_SZGrid( '999', '922' );
  kulonbseg:=300;

  count:=0;
	EllenListAppend('', false);
	EllenListAppend('H�t�s p�tkocsik ellen�rz�se :', false);
	EllenListAppend('================================================', false);
	EllenListAppend('', false);

	st:='SELECT  gk_resz,gk_ut_km,gk_ut_da,gk_hskm,gk_hsdat FROM gepkocsi where  gk_kulso=0 and gk_arhiv=0 and (gk_forki='''''+' or gk_forki is null) and (gk_gkkat='''+'H'''+' or gk_gkkat='''+'HV'''+')  order by gk_resz';

//  Clipboard.Clear;
//  Clipboard.AsText:=st;
	if Query_Run (QueryKozos,st, true) then
  begin
    QueryKozos.RecordCount;
	  while not QueryKozos.Eof do
    begin
		 st:=QueryKozos.FieldByName('gk_resz').AsString+'  '+QueryKozos.FieldByName('gk_ut_da').AsString+'  '+QueryKozos.FieldByName('gk_ut_km').AsString+' �.�ra ';
     km:= QueryKozos.FieldByName('gk_ut_km').AsInteger;
     if km<=500 then
     begin
       if 500-kulonbseg<=km then
       begin
         EllenListAppend(st+'(500)', false);
         Inc(count);
       end;
     end
     else
     begin
       ukm:=km;
       i:=0;
       while ukm>3000 do
       begin
        ukm:=ukm-3000;
        inc(i);
       end;
       if ukm+kulonbseg>=3000 then
       begin
         skm:=(i+1)*3000  ;
         if  StrToIntDef( QueryKozos.FieldByName('gk_hskm').AsString,0) < skm then
         begin
           EllenListAppend(st+'('+IntToStr(skm)+')', false);
           Inc(count);
         end;
       end;
     end;
		 QueryKozos.Next;
		end;
	end;
  QueryKozos.Close;

  //////////////////////////////////
	EllenListAppend('', false);
	EllenListAppend('', false);
	EllenListAppend('Szerviz ellen�rz�se :', false);
	EllenListAppend('================================================', false);
	EllenListAppend('', false);

	st:='SELECT  gk_resz,gk_ut_km,gk_ut_da,gk_hskm,gk_hsdat FROM gepkocsi '+
   'where  gk_kulso=0 and gk_arhiv=0 and (gk_forki='''''+' or gk_forki is null) and (CHARINDEX('''+'['''+'+'+'gk_gkkat'+'+'''+']'''+','''+ATEGOTIP+''')>0 )  order by gk_resz';

//  Clipboard.Clear;
//  Clipboard.AsText:=st;
  kulonbseg:=5000;
	if Query_Run (QueryKozos,st, true) then
  begin
    QueryKozos.RecordCount;
	  while not QueryKozos.Eof do
    begin
		   st:=QueryKozos.FieldByName('gk_resz').AsString+'  '+QueryKozos.FieldByName('gk_ut_da').AsString+'  '+QueryKozos.FieldByName('gk_ut_km').AsString+' km ';
       km:= QueryKozos.FieldByName('gk_ut_km').AsInteger;
       ukm:=km;
       i:=0;
       while ukm>50000 do
       begin
        ukm:=ukm-50000;
        inc(i);
       end;
       if ukm+kulonbseg>=50000 then
       begin
         skm:=(i+1)*50000  ;
         if  StrToIntDef( QueryKozos.FieldByName('gk_hskm').AsString,0) < skm then
         begin
           EllenListAppend(st+'('+IntToStr(skm)+')', false);
           Inc(count);
         end;
       end;
		   QueryKozos.Next;
    end;
	end;
  //////////////////////////////////
  QueryKozos.Close;
  Result:=count;
end;

function TEgyebDlg.Jarat_MBfajtak(jakod: string): string;
var
  fajko, fajta, S: string;
begin
  Result:='';
  if jakod='' then exit;
  fajko:= Query_Select('JARAT','JA_KOD',jakod,'JA_FAJKO');
  if fajko='0' then
  begin
    Result:='Norm�l';
    exit
  end;
  if Query_Run(QueryAlap2,'select * from jaratmegseged where jg_jakod='''+jakod+''' ') then
  begin
    if not QueryAlap2.Eof then Result:=IntToStr(QueryAlap2.RecordCount)+'_';      // darab
    while not QueryAlap2.Eof do begin
      // ez null-ra '0'-t ad vissza
      // fajta:=Query_Select2('MEGSEGED','MS_MBKOD','MS_MSKOD',QueryAlap2.fieldbyname('JG_MBKOD').AsString,QueryAlap2.fieldbyname('JG_MSKOD').AsString,'MS_FAJTA');
      S:= 'select MS_FAJTA from MEGSEGED where MS_MBKOD='+QueryAlap2.fieldbyname('JG_MBKOD').AsString+
          ' and MS_MSKOD= '+QueryAlap2.fieldbyname('JG_MSKOD').AsString;
      fajta:=Query_SelectString('MEGSEGED', S);
      if (fajta<> '') and ((Pos(fajta, Result))=0) then  // nincs m�g ilyen
         Result:=Result+fajta+', ';
      QueryAlap2.Next;
    end;
    QueryAlap2.Close;
  end;
end;

function TEgyebDlg.Jaratszam(jakod: string): string;
var
  orsz, jasza: string;
begin
  Result:='';
  orsz:= Query_Select('JARAT','JA_KOD',jakod,'JA_ORSZ');
  jasza:= Query_Select('JARAT','JA_KOD',jakod,'JA_ALKOD');
  if (orsz<>'')and(jasza<>'') then
    Result:=orsz+'-'+jasza;
end;

function TEgyebDlg.FormIsLoaded(sType: TClass): boolean;
var
  i : integer;
begin
  Result := false;
  for i := 0 to screen.FormCount - 1 do
    if (screen.Forms[i].ClassType = sType) then
    begin
      Result := True;
      exit;
    end;
end;

function TEgyebDlg.GkTipus(rendsz: string): string;
begin
  Result:= Query_Select('GEPKOCSI','GK_RESZ',rendsz,'GK_GKKAT');
end;

function TEgyebDlg.SepFrontToken(Sep, Miben: string): string;
var  P:integer;
begin
     P:=Pos(Sep, Miben);
     if P>0 then
        SepFrontToken:=Copy(Miben, 1, P-1)
     else
        SepFrontToken:=Miben;
end;

function TEgyebDlg.SepRest(Sep, Miben: string): string;
var  P:integer;
begin
     P:=Pos(Sep, Miben);
     if P>0 then
        SepRest:=Copy(Miben, P+Length(Sep), Length(Miben)-Length(Sep)-P+1)
     else
        SepRest:='';
end;

function TEgyebDlg.GetSepListItem(Sep, Miben: string; PP:integer): string;
var H, T, Res: string;
    P: integer;
begin
    T:=Miben;
    P:=PP;
    while (P>1) do begin  // first item: 1
         T:=SepRest(Sep,T);
         P:=P-1;
        end;
    Res:=SepFrontToken(Sep,T);
    Result:=Res;
end;

function TEgyebDlg.SepListLength(Sep, Miben: string): integer;
var H, T, Res: string;
    P: integer;
begin
    T:=Miben;
    P:=0;
    repeat
       T:=SepRest(Sep,T);
       Inc(P);
       until T='';
    Result:=P;
end;


procedure TEgyebDlg.DolgServantes;
const
  SQLSep = '^';
var
  DBSema, uido, sql, Naploba, MyTimestamp, masnap: string;
  AktSor: string;
  siker: integer;
begin
	// uido	:= Read_SZGrid( '999', '925' );   // ez pufferelve van, nem friss inf�!
  uido	:= Query_Select2('SZOTAR', 'SZ_FOKOD', 'SZ_ALKOD', '999', '925', 'SZ_MENEV');
  if (uido='') or (uido='0') then exit;    // ha nincs megadva, akkor nincs �tv�tel
  masnap:=DatumHozNap(uido, 1, true); // az utols� futtat�s + 1 nap, form�tum: '2015.03.01. 13:52:12'
  if FormatDateTime('yyyy.mm.dd. HH:mm:ss', Now) < masnap then exit;  // egy napig ne futtassuk

  {if TESZTPRG then
       DBSema:='DAT_TESZT.szami'
  else
       DBSema:='DAT2013.szami';
       }
  DBSema:=v_alap_db;    // ALAP_DB + EVSZAM az .ini-b�l

  if copy(DBSema,1,2)='DK' then DBSema:=DBSema+'.dbo'    //  bocs.
  else DBSema:=DBSema+'.szami';

  if ADOConnectionServantes.Connected then begin
      ADOConnectionServantes.Close;
      end;  // if
  if not VanServantes then begin
	    NoticeKi('Nem siker�lt a SERVANTES adatb�zis kapcsol�d�s!');
      Exit;
      end;  // if
  with StoredProcServantes do begin    // 'DOLG_FUVAROSBA_MIND';
      Close;
      ProcedureName := 'DOLG_FUVAROSBA_MIND2';  // recordset-ben adja vissza az SQL sorokat
      with parameters do begin
        Clear;
        CreateParameter('@RETURN_VALUE', ftInteger, pdReturnValue, 0, 0);
        CreateParameter('@SemaNev', ftString, pdInput, 255, DBSema);
        CreateParameter('@UtolsoExport', ftString, pdInput, 50, uido);
        // CreateParameter('@Naploba', ftString, pdOutput, 500000, '');
        end;  // with parameters
     // ExecProc;
     Open;
     siker := parameters.ParamByName('@RETURN_VALUE').Value;
     Query_Log_Str('DOLG_FUVAROSBA_MIND2('+DBSema+', '+uido+') v�grehajtva, eredm�ny sorok sz�ma: '+IntToStr(RecordCount), 0, true);     // napl�z�s (a fut�s hossz�hoz 0-t �runk)
     while not eof do begin
       AktSor:=Fields[0].AsString;
       Query_Log_Str(AktSor, 0, true);     // napl�z�s (a fut�s hossz�hoz 0-t �runk)
       Next;
     end;  // while
     // if not varisnull(Parameters.ParamByName('@Naploba').Value) then
     //  Naploba := Parameters.ParamByName('@Naploba').Value
     // else  Naploba := '';
     end;  // with

  if siker=0 then begin
      MyTimestamp:= FormatDateTime('yyyy.mm.dd. HH:mm:ss', Now);
      Query_Run(QueryKozos2,  'UPDATE SZOTAR SET SZ_MENEV='''+MyTimestamp+''' where SZ_FOKOD='''+'999'''+' and SZ_ALKOD='''+'925''', FALSE);
      end;
  // A t�rolt elj�r�s �ltal visszaadott napl�zand� sorok feldolgoz�sa. Sor szepar�tor: "^"
  // while Naploba>'' do begin
  //   AktSor:=SepFrontToken(SQLSep, Naploba);  // az els� elem
  //   Naploba:=SepRest(SQLSep, Naploba);      // az els� els� elem leemel�s�vel kapott marad�k lista
  //   Query_Log_Str(AktSor, 0, true);     // napl�z�s (a fut�s hossz�hoz 0-t �runk)
  //   end;  // while
 if ADOConnectionServantes.Connected then
    ADOConnectionServantes.Close;
end;


{procedure TEgyebDlg.DolgServantes;
var
  most: TDateTime;
  uido,fname,sid, proc,sql: string;
  db: integer;
  // utdat,utido: string;
begin
	fname   := EgyebDlg.TempList + 'DolgFromServ.txt';
   most    :=now;
	uido	:= Read_SZGrid( '999', '925' );
   if uido='' then exit;    // ha nincs megadva, akkor nincs �tv�tel
   //ido:=StrToDateTimeDef(uido,date);
   //uido:=DateToStr(ido) ;
   db:=0;
   if TESZTPRG then
       proc:='DOLG_FUVAROSBA_TESZT '
   else
       proc:='DOLG_FUVAROSBA ';

       if ADOConnectionServantes.Connected then begin
           ADOConnectionServantes.Close;
       end;
       if not VanServantes then begin
		    NoticeKi('Nem siker�lt a SERVANTES adatb�zis kapcsol�d�s!');
           Exit;
       end;

      QueryServantes.Close;
      QueryServantes.SQL.Clear;
      // NagyP 2015-01-06
      // Az id�pont ellen�rz�s hib�s volt �gy.
      // utdat:=copy(uido,1,10);
      // utido:=Trim(copy(uido,13,8));
      // sql:='select dolgazon from bdolgsza where convert(varchar(10),lastdate,102)>='''+utdat+''' and convert(varchar(10),lastdate,108)>'+' convert(varchar(10),cast('''+'2014.01.01 '+utido+''' as datetime),108)'  ;
      sql:='select distinct dolgazon from bdolgsza where convert(varchar(10),lastdate,102)+''. ''+convert(varchar(10),lastdate,108)>'''+uido+'''' ;

      QueryServantes.SQL.Add(sql);
      QueryServantes.Open;
      QueryServantes.First;
    //  ShowMessage('open');
      while not QueryServantes.Eof do
      begin
        sid:=QueryServantes.fieldbyname('DOLGAZON').AsString;
        StoredProcServantes.Close;
        StoredProcServantes.Parameters[0].Value:=QueryServantes.fieldbyname('DOLGAZON').AsString;
        try
           if not Query_Run(SQuery1,  'EXEC '+proc+''''+sid+'''', FALSE) then Raise Exception.Create('');
           EgyebDlg.WriteLogFile(fname,DateTimeToStr(Now)+' SZA '+QueryServantes.fieldbyname('DOLGAZON').AsString);
        except
           EgyebDlg.WriteLogFile(fname,DateTimeToStr(Now)+' HIBA!!! '+sid);
           end;  // try-except

        inc(db);
        QueryServantes.Next;
      end;
      QueryServantes.Close;
      QueryServantes.SQL.Clear;
      // NagyP 2015-01-06
      // sql:='select dolgazon from bdolgjv where convert(varchar(10),lastdate,102)>='''+utdat+''' and convert(varchar(10),lastdate,108)>'+' convert(varchar(10),cast('''+'2014.01.01 '+utido+''' as datetime),108)'  ;
      sql:='select distinct dolgazon from bdolgjv where convert(varchar(10),lastdate,102)+''. ''+convert(varchar(10),lastdate,108)>'''+uido+'''' ;
      QueryServantes.SQL.Add(sql);

      QueryServantes.Open;
      QueryServantes.First;
      sid:='';
      while not QueryServantes.Eof do
      begin
        sid:=QueryServantes.fieldbyname('DOLGAZON').AsString;
        StoredProcServantes.Close;
        StoredProcServantes.Parameters[0].Value:=QueryServantes.fieldbyname('DOLGAZON').AsString;
        //StoredProcServantes.ExecProc;
        try
           if not Query_Run(SQuery1,  'EXEC '+proc+''''+sid+'''', FALSE) then Raise Exception.Create('');
           EgyebDlg.WriteLogFile(fname,DateTimeToStr(Now)+' JV  '+QueryServantes.fieldbyname('DOLGAZON').AsString);
        except
           EgyebDlg.WriteLogFile(fname,DateTimeToStr(Now)+' HIBA!!! '+sid);
           end;  // try-except
        inc(db);
        QueryServantes.Next;
      end;
      QueryServantes.Close;
      if db>0 then
         Query_Run(QueryKozos2,  'UPDATE SZOTAR SET  SZ_MENEV='''+DateTimeToStr(most)+''' where SZ_FOKOD='''+'999'''+' and SZ_ALKOD='''+'925''', FALSE);


 if ADOConnectionServantes.Connected then
    ADOConnectionServantes.Close;
end;
}
procedure TEgyebDlg.Lejartszamlak(vevokod: string; kellhanincs: boolean; kmoral: string ='N');
var
  fm,mmoral: integer;
begin
    if vevokod='' then exit;
    Application.CreateForm(TFLejartszamla, FLejartszamla);

    mmoral:=StrToIntDef(kmoral,-9999);
    FLejartszamla.Query2.Close;
    FLejartszamla.Query2.Parameters[0].Value:=vevokod;
    FLejartszamla.Query2.Open;
    FLejartszamla.Query2.First;
    if FLejartszamla.Query2.Eof then begin   // m�g nem volt kifizetett sz�ml�ja
      fm:=100;
      // FLejartszamla.Query2.Close;
      // FLejartszamla.free;
      // NoticeKi('A partnernek m�g nincs kifizetett sz�ml�ja!');
      // Exit;
      end
    else begin
      fm:=FLejartszamla.Query2moral.AsInteger;
      end;
    FLejartszamla.Close;

    FLejartszamla.Query1.Close;
    FLejartszamla.Query1.Parameters[0].Value:=vevokod;
    FLejartszamla.Query1.Open;
    FLejartszamla.Query1.First;
    FLejartszamla.Panel3.Visible:=not FLejartszamla.Query1.Eof;
    FLejartszamla.BitSzamla.Visible:=not FLejartszamla.Query1.Eof;
    if FLejartszamla.Query1.Eof then begin
       FLejartszamla.Height:=FLejartszamla.Height-FLejartszamla.Panel3.Height;
       if (not kellhanincs)or(kellhanincs and(fm<mmoral)) then begin
           FLejartszamla.Query1.Close;
           FLejartszamla.Free;
           Exit;
       end;
   end;

   FLejartszamla.ShowModal;
   FLejartszamla.Query1.Close;
   FLejartszamla.Free;
end;

function TEgyebDlg.SzervizEllenor: integer;
var
    st, orakm  : string;
    count      : integer;
    eshkod,utkm: integer;
    egkrensz,utkmdat: string;
begin
  QService2.Close;QService2.Open;
  QService2.First;
  eshkod:=0;
  count:=0;
  egkrensz:='';
  while not QService2.Eof do
  begin
    if QService2SH_KOD.AsInteger<>eshkod then
    begin
  	  EllenListAppend('', false);
      EllenListAppend(QService2SH_JELLEG.Value+': ',false);
	    EllenListAppend('================================================', false);
      eshkod:=QService2SH_KOD.AsInteger;
    end;

	  st:='SELECT  gk_ut_km,gk_ut_da FROM gepkocsi where gk_resz='''+QService2SH_RENSZ.Value+'''';
    Query_Run (QueryKozos,st, true);
    utkm:=QueryKozos.FieldByName('gk_ut_km').AsInteger;
    utkmdat:=QueryKozos.FieldByName('gk_ut_da').AsString;
    if HutosGk(QService2SH_RENSZ.Value) then
      orakm:=' �.�ra '
    else
      orakm:=' km ';

    if (QService2SH_SERVKM2.AsInteger>0)and(utkm+QService2SH_JELZES.Value > QService2SH_SERVKM2.Value) then
    begin
		  st:=QService2SH_RENSZ.Value+'  '+utkmdat+'  '+IntToStr(utkm)+orakm+' ('+QService2SH_SERVKM2.AsString+')';
      if utkm > QService2SH_SERVKM2.Value then st:=st+' !!!';
      EllenListAppend(st, false);
      Inc(count);
    end;

    egkrensz:=QService2SH_RENSZ.Value;
    while (not QService2.Eof)and(QService2SH_KOD.value=eshkod)and(QService2SH_RENSZ.value=egkrensz) do
    begin
       QService2.Next;
    end;
  end;
  QService2.Close;
  Result:=count;

	EllenListAppend('V�ge============================================', false);
end;

function TEgyebDlg.VanServantes : boolean;
var
	Inifn			: TIniFile;
   serv_conn       : string;
   serv_db         : string;
   serv_user       : string;
   serv_pass       : string;
begin
   Result  := false;
   try
       Inifn 		:= TIniFile.Create( EgyebDlg.globalini );
       serv_conn   := Inifn.ReadString( 'GENERAL', 'SERV_CONNECTION','');
       serv_db     := Inifn.ReadString( 'GENERAL', 'SERV_DB','SERVANTES');
       serv_user   := DecodeString(Inifn.ReadString( 'GENERAL', 'SERV_USER',''));
       serv_pass   := DecodeString(Inifn.ReadString( 'GENERAL', 'SERV_PASSWORD',''));
       Inifn.Free;
       if serv_conn = '' then begin
           NoticeKi('Az ini-ben ellen�rizze a SERV_CONNECTION; SERV_USER; SERV_PASSWORD be�ll�t�sokat !');
           Exit;
       end;
       if serv_user <> '' then begin
           serv_conn 	:= serv_conn + ';User ID= ' + serv_user;
       end;
       if serv_pass <> '' then begin
           serv_conn 	:= serv_conn + ';Password=' + serv_pass;
       end;
       ADOConnectionServantes.ConnectionString		:= serv_conn;
       ADOConnectionServantes.DefaultDatabase		:= serv_db;
       ADOConnectionServantes.LoginPrompt			:= false;
       ADOConnectionServantes.Open;
       if not ADOConnectionServantes.Connected then begin
           Exit;
       end;
       result  := true;
   except

   end;
end;

function TEgyebDlg.ConnectKulcs : boolean;
var
	Inifn			: TIniFile;
   kulcs_conn       : string;
   kulcs_db         : string;
   kulcs_user       : string;
   kulcs_pass       : string;
begin
   Result  := false;
   try
       Inifn 		:= TIniFile.Create( EgyebDlg.globalini );
       kulcs_conn   := Inifn.ReadString( 'GENERAL', 'KULCS_CONNECTION','');
       kulcs_db     := Inifn.ReadString( 'GENERAL', 'KULCS_DB','');
       kulcs_user   := DecodeString(Inifn.ReadString( 'GENERAL', 'KULCS_USER',''));
       kulcs_pass   := DecodeString(Inifn.ReadString( 'GENERAL', 'KULCS_PASSWORD',''));
       Inifn.Free;
       if kulcs_conn = '' then begin
           NoticeKi('Az ini-ben ellen�rizze a KULCS_CONNECTION; KULCS_USER; KULCS_PASSWORD be�ll�t�sokat !');
           Exit;
       end;
       if kulcs_user <> '' then begin
           kulcs_conn 	:= kulcs_conn + ';User ID= ' + kulcs_user;
       end;
       if kulcs_pass <> '' then begin
           kulcs_conn 	:= kulcs_conn + ';Password=' + kulcs_pass;
       end;
       ADOConnectionkulcs.ConnectionString:= kulcs_conn;
       if kulcs_db <> '' then
         ADOConnectionkulcs.DefaultDatabase:= kulcs_db;
       ADOConnectionkulcs.LoginPrompt:= false;
       ADOConnectionkulcs.Open;
       if not ADOConnectionkulcs.Connected then begin
           Exit;
       end;
       result  := true;
   except

   end;
end;


function TEgyebDlg.DisconnectKulcs : boolean;
begin
   ADOConnectionkulcs.Connected:= False;
end;

function TEgyebDlg.TeljesDijSzamitas_mag(MBKOD: integer; datum, vevokod: string; OsszKM, AtallasKm: double;
              Fuvardij, EgyebDij, Aldij: double; FuvarDeviza, EgyebDeviza, Aldeviza: string) :T_TeljesDijSzamitasRec;
var
   OutRecord: T_TeljesDijSzamitasRec;
   fuvardij_EUR, egyebdij_EUR, OsszKM2, uapotlek, TobbiFuvardijEUR, TobbiAlDijEUR: double;
   MBKODLista: TStringList;
   CSOP, i: integer;
   csendes_mod: boolean;
begin
   if ((FuvarDeviza<>'EUR') or (EgyebDeviza<>'EUR') or (AlDeviza<>'EUR')) and (EgyebDlg.ArfolyamErtek('EUR', datum, true, true)=0) then begin // nincs j� EUR �rfolyam
      OutRecord.teljesdij_EUR:=0;
      OutRecord.teljesdij_leiras:='�rv�nytelen EUR �rfolyam a '+datum+' d�tumon.';
      OutRecord.EURKm:=0;
      OutRecord.EURKmAtallas:=0;
      end
   else begin
     TobbiFuvardijEUR:=GetCsoportFuvardij(MBKOD);  // 0, ha nincs csoport
     TobbiAlDijEUR:=GetCsoportAlvallakozoiDij(MBKOD);  // 0, ha nincs csoport
     OutRecord.teljesdij_EUR:=0;
     // --------- fuvard�j --------- //
     if FuvarDeviza='EUR' then begin
        OutRecord.teljesdij_EUR:=OutRecord.teljesdij_EUR + Fuvardij;
        end
     else begin
        fuvardij_EUR:= Fuvardij * EgyebDlg.ArfolyamErtek(FuvarDeviza, datum, true, true) / EgyebDlg.ArfolyamErtek('EUR', datum, true, true); // kereszt�rfolyammal
        OutRecord.teljesdij_EUR:=OutRecord.teljesdij_EUR + fuvardij_EUR;
        end;
     // --------- egy�b d�j --------- //
     if (EgyebDij>0) and (Trim(EgyebDeviza)<>'') then begin
       if EgyebDeviza='EUR' then begin
          OutRecord.teljesdij_EUR:=OutRecord.teljesdij_EUR + EgyebDij;
          end
       else begin
          egyebdij_EUR:= EgyebDij * EgyebDlg.ArfolyamErtek(EgyebDeviza, datum, true, true) / EgyebDlg.ArfolyamErtek('EUR', datum, true, true); // kereszt�rfolyammal
          OutRecord.teljesdij_EUR:= OutRecord.teljesdij_EUR + egyebdij_EUR;
          end;  // else
       end;  // if
     // --------- alv�llalkoz�i d�j EUR --------- //
     if (Aldij>0) and (Trim(AlDeviza)<>'') then begin
       if AlDeviza='EUR' then begin
          OutRecord.alvallalkozo_EUR:=Aldij;
          end
       else begin
          OutRecord.alvallalkozo_EUR:= Aldij * EgyebDlg.ArfolyamErtek(AlDeviza, datum, true, true) / EgyebDlg.ArfolyamErtek('EUR', datum, true, true); // kereszt�rfolyammal
          end;  // else
       end;  // if
     // --------- �zemanyag p�tl�k --------- //
     if figy_megbizas_uzemanyag_ar then csendes_mod:=False
     else csendes_mod:=true;
     uapotlek:= GetUzemanyagPotlekSz(vevokod, GetApehUzemanyag(datum, 0, true, csendes_mod), datum, False);  // kor�bbi is j�, ha nincs m�s
     OutRecord.teljesdij_EUR:=OutRecord.teljesdij_EUR * (1+uapotlek/100);
     // ---------- EUR / km ---------------- //
     if OsszKM > 0 then begin
       OsszKM2:=OsszKM + AtallasKm;
       OutRecord.EURKmAtallas:=(OutRecord.teljesdij_EUR + TobbiFuvardijEUR) / OsszKM2;  // �t�ll�ssal sz�molva
       OutRecord.EURKm:=(OutRecord.teljesdij_EUR + TobbiFuvardijEUR) / OsszKM;    // �t�ll�s n�lk�l
       OutRecord.AlEURKm:=(OutRecord.alvallalkozo_EUR + TobbiAlDijEUR) / OsszKM;    // �t�ll�s n�lk�l
       if TobbiFuvardijEUR>0 then
           OutRecord.EURKm_leiras:='�sszes�tett teljes fuvard�j: '+FormatFloat('0.00', OutRecord.teljesdij_EUR + TobbiFuvardijEUR)+' '
       else OutRecord.EURKm_leiras:='';  // nincs k�l�n inf�
       end
     else begin
       OutRecord.EURKm:=0;
       OutRecord.EURKmAtallas:=0;
       OutRecord.AlEURKm:=0;
       end;  // else
     // --------- a csoport elemeiben friss�tj�k az EURKM, EURKMU mez�ket ------- //
     CSOP:= GetReszmegbizasCsop(MBKOD);
     if CSOP>0 then begin
        MBKODLista:= GetReszmegbizasCsop_elemek(CSOP);
        for i:= 0 to MBKODLista.Count-1 do begin
          if MBKODLista[i] <> IntToStr(MBKOD) then begin  // az aktu�lis megb�z�st ne friss�ts�k
            Query_Update(QueryAlap2, 'MEGBIZAS',
             [
              'MB_EURKM',  SqlSzamString(OutRecord.EURKm, 12, 3),
              'MB_EURKMU',  SqlSzamString(OutRecord.EURKmAtallas, 12, 3),
              'MB_ALEURKM',  SqlSzamString(OutRecord.AlEURKm, 12, 3)
              ], ' WHERE MB_MBKOD = '+MBKODLista[i]);
             end;  // if
           end;  // for
        if MBKODLista<>nil then MBKODLista.Free;
        end;  // if CSOP
     end;  // else
   Result:= OutRecord;
end;

// a t�bbi megb�z�s teljes fuvard�ja EUR-ban
function TEgyebDlg.GetCsoportFuvardij(MBKOD: integer): double;
var
   CSOP: integer;
   MBKODLista: TStringList;
   S: string;
begin
   CSOP:= GetReszmegbizasCsop(MBKOD);
   if CSOP>0 then begin
      MBKODLista:= GetReszmegbizasCsop_elemek(CSOP);
      MBKODLista.Delimiter:=',';
      S:='select sum(MB_EUDIJ) from megbizas where MB_MBKOD in ('+ MBKODLista.DelimitedText+') ';
      S:=S+ 'and MB_MBKOD<>'+IntToStr(MBKOD);  // �t mag�t ne sz�moljuk bele
      Query_run(QueryAlap2, S);
      Result:=QueryAlap2.Fields[0].AsFloat;
      end
   else begin  // nincs csoport
      Result:=0
      end;
end;

// a t�bbi megb�z�s alv�llalkoz�i d�ja EUR-ban
function TEgyebDlg.GetCsoportAlvallakozoiDij(MBKOD: integer): double;
var
   CSOP: integer;
   MBKODLista: TStringList;
   S: string;
begin
   CSOP:= GetReszmegbizasCsop(MBKOD);
   if CSOP>0 then begin
      MBKODLista:= GetReszmegbizasCsop_elemek(CSOP);
      MBKODLista.Delimiter:=',';
      S:='select sum(MB_ALEUR) from megbizas where MB_MBKOD in ('+ MBKODLista.DelimitedText+') ';
      S:=S+ 'and MB_MBKOD<>'+IntToStr(MBKOD);  // �t mag�t ne sz�moljuk bele
      Query_run(QueryAlap2, S);
      Result:=QueryAlap2.Fields[0].AsFloat;
      end
   else begin  // nincs csoport
      Result:=0
      end;
end;

// a visszat�r�si �rt�k MBKOD friss�tett �tvonal inform�ci�ja lesz
function TEgyebDlg.Megbizas_valtozas(MBKOD: integer; MB_EUDIJ: double; Akcio: TMegbizasValtozasAkcio): TUtvonalInfo;
var
   S, TOL, IG, RENSZ, VISZO, CSOP, GKKAT: string;
   TOL_elott, IG_utan, Hibauzenet, HibaHely: string;
   MBLista_elotte, MBLista_utana: TStringList;    //MBKODCsak
   i, CSOP_ELOTTE, CSOP_UTANA, UresKMSzazalek: integer;
   UTINFO, UTINFO_temp: TUtvonalInfo;
   Hibas_tol_ig: boolean;
   LEHETCSOP: boolean;  // a megb�z�st kiz�rtuk-e a csoportba sorol�sb�l
begin
  CSOP_ELOTTE:=0;
  CSOP_UTANA:=0;
  // A f�ggv�ny fogja inicializ�lni
  // MBLista_elotte:=TStringList.Create;
  MBLista_utana:=TStringList.Create;
  MBLista_utana.Sorted:=True;
  MBLista_utana.Duplicates:=dupIgnore;
  // MBKODCsak:=TStringList.Create;
  // MBKODCsak.Add(IntToStr(MBKOD));
  LEHETCSOP:=  ( Query_Select('MEGBIZAS', 'MB_MBKOD', IntToStr(MBKOD), 'MB_NOCSOP') <> '1' ); // a megb�z�st nem z�rtuk ki a csoportba sorol�sb�l
  HibaHely:='01';
  if LEHETCSOP then begin
    if (Akcio = mbUJ) or (Akcio = mbMODOSIT) then begin
     // Megb�z�s id�szak meghat�roz�sa: els� felrak�st�l utols� lerak�sig
      // S:='select min(MS_FETED2+MS_FETEI2) TOL, max(MS_LETED2+MS_LETEI2) IG ';
      // S:='select min(MS_FELDAT+MS_FELIDO) TOL, max(MS_LERDAT+MS_LERIDO) IG ';  // az "id�ablak" eleje!!!
      // 2016.03.07. - ha ki van t�ltve a "t�ny" id�, akkor azzal sz�molunk
      S:='select min(case when DATALENGTH(MS_FETDAT+MS_FETIDO)=16 then MS_FETDAT+MS_FETIDO else MS_FELDAT+MS_FELIDO end) TOL, ';
      S:=S+' max(case when DATALENGTH(MS_LETDAT+MS_LETIDO)=16 then MS_LETDAT+MS_LETIDO else MS_LERDAT+MS_LERIDO end) IG ';  // az "id�ablak" eleje!!!
      // ----------------------------------
      S:=S+' from MEGSEGED where MS_FAJTA=''norm�l'' and MS_MBKOD = '+IntToStr(MBKOD);
      S:=S+' and DATALENGTH(MS_FELDAT+MS_FELIDO)=16 '; // csak a teljesen kit�lt�tteket vegye figyelembe
      S:=S+' and DATALENGTH(MS_LERDAT+MS_LERIDO)=16 '; // csak a teljesen kit�lt�tteket vegye figyelembe
      Query_run(QueryAlap, S);
      with QueryAlap do begin

        Hibas_tol_ig:= false;
        if eof then begin
          Hibas_tol_ig:= true;
          end
        else begin
          TOL:=FieldByName('TOL').AsString;
          IG:=FieldByName('IG').AsString;
          if (length(TOL) < 15) or (length(IG) < 15) then   // nincs teljes d�tum-id�pont, ezzel ne dolgozzunk
             Hibas_tol_ig:= true;
          end; // else
        if Hibas_tol_ig then begin
          UTINFO.Hibauzenet:='Nincs "norm�l" felrak�/lerak� megadva a megb�z�shoz!';
          UTINFO.Kilometer:=0;
          UTINFO.Atallas:=0;
          UTINFO.Leiras:='';
          Result:=UTINFO;
          Exit;
          end;
        end;  // with
      TOL_elott:=DatumHozNap(TOL, -30, True);       // TOL - 30 nap
      IG_utan:=DatumHozNap(IG, 30, True);       // IG + 30 nap
      HibaHely:='02';
      // Megb�z�s rendsz�m, ir�ny, jateg�ria
      S:='select MB_VISZO, MB_RENSZ, MB_GKKAT from MEGBIZAS where MB_MBKOD = '+IntToStr(MBKOD);
      Query_run(QueryAlap, S);
      with QueryAlap do begin
        if not eof then begin
          RENSZ:=FieldByName('MB_RENSZ').AsString;
          VISZO:=Trim(FieldByName('MB_VISZO').AsString);
          GKKAT:=FieldByName('MB_GKKAT').AsString;
          end
        else begin
          UTINFO.Hibauzenet:='Nem l�tez� megb�z�s!';
          UTINFO.Kilometer:=0;
          UTINFO.Atallas:=0;
          UTINFO.Leiras:='';
          Result:=UTINFO;
          Exit;
          end;
        end;  // with
      HibaHely:='03';
      if RENSZ<>'' then begin  // rendsz�m n�lk�l nincs �rtelme (alv�llalkoz�i j�ratok!)
        S:='select MS_MBKOD from ';
        // S:=S+' (select MS_MBKOD, min(MS_FETED2+MS_FETEI2) MTOL, max(MS_LETED2+MS_LETEI2) MIG ';
        // S:=S+' (select MS_MBKOD, min(MS_FELDAT+MS_FELIDO) MTOL, max(MS_LERDAT+MS_LERIDO) MIG ';  // az "id�ablak" eleje!!!
        // 2016.03.07. - ha ki van t�ltve a "t�ny" id�, akkor azzal sz�molunk
        S:=S+' (select MS_MBKOD, min(case when DATALENGTH(MS_FETDAT+MS_FETIDO)=16 then MS_FETDAT+MS_FETIDO else MS_FELDAT+MS_FELIDO end) MTOL, ';  // az "id�ablak" eleje!!!
        S:=S+' max(case when DATALENGTH(MS_LETDAT+MS_LETIDO)=16 then MS_LETDAT+MS_LETIDO else MS_LERDAT+MS_LERIDO end) MIG ';  // az "id�ablak" eleje!!!
        // ----------------------------------------------------------------------

        S:=S+' from MEGSEGED where MS_RENSZ='''+RENSZ+''' AND MS_FAJTA=''norm�l'' ';
        S:=S+' and MS_FELDAT>='''+TOL_elott+''' AND MS_LERDAT<='''+IG_utan+''' ';  // az egy�ltal�n sz�ba j�het�kre egy sz�k�t�s
        // az id�pontok miatt nem kell, egy id�ben nem lehet export �s import
        // S:=S+' and MS_EXSTR='''+VISZO+''' '; // export, import               // VISZO: lehet ak�r "export, import, k�lf�ld," ??
        S:=S+' group by MS_MBKOD) a , MEGBIZAS ';
        S:=S+' where a.MS_MBKOD = MB_MBKOD ';
        S:=S+' and MTOL < '''+IG+''' and MIG > '''+TOL+''' ';  // MTOL<IG and MIG>TOL
        S:=S+' and MS_MBKOD <> '+IntToStr(MBKOD);  // nem �nmaga
        S:=S+' and isnull(MB_NOCSOP, 0) = 0 ';  // ------ 2017.11.20. csoportb�l val� kivon�s lehet�s�ge ---- //
        // a k�t VISZO megegyezik, vagy egyik tartalmazza a m�sikat. A "van k�z�s elem�k" lek�rdez�st csak t�rolt elj�r�ssal vagy kliens oldali ciklussal tudn�m
        // megoldani, az lass� lenne.
        S:=S+' and ((ltrim(rtrim(MB_VISZO))='''+VISZO+''') or (PatIndex(''%''+ltrim(rtrim(MB_VISZO))+''%'', '''+VISZO+''')>0 ) '+
              ' or (PatIndex(''%'+VISZO+'%'', ltrim(rtrim(MB_VISZO)))>0 ))';

        Query_run(QueryAlap, S);
        if QueryAlap.RecordCount > 20 then begin  // pr�b�ljuk elkapni a gyan�san nagy elem� csoportokat
            HibaKiiro('Gyan�san sok r�szmegb�z�s erre: '+S);
            end;
        while not QueryAlap.eof do begin
           MBLista_utana.Add(QueryAlap.FieldByName('MS_MBKOD').AsString);
           QueryAlap.Next;
           end;  // while
        end;  // if RENSZ
      HibaHely:='04';
      // Azonos j�rathoz kapcsol�d� megb�z�sok
      S:='select MS_MBKOD from ';
      // S:=S+' (select MS_MBKOD, min(MS_FELDAT+MS_FELIDO) MTOL, max(MS_LERDAT+MS_LERIDO) MIG ';
      // 2016.03.07. - ha ki van t�ltve a "t�ny" id�, akkor azzal sz�molunk
      S:=S+' (select MS_MBKOD, min(case when DATALENGTH(MS_FETDAT+MS_FETIDO)=16 then MS_FETDAT+MS_FETIDO else MS_FELDAT+MS_FELIDO end) MTOL,  ';
      S:=S+' max(case when DATALENGTH(MS_LETDAT+MS_LETIDO)=16 then MS_LETDAT+MS_LETIDO else MS_LERDAT+MS_LERIDO end) MIG ';
      // ------------------------------------------------------------
      S:=S+' from MEGSEGED, MEGBIZAS mb1, MEGBIZAS mb2 where MS_MBKOD = mb2.MB_MBKOD AND MS_FAJTA=''norm�l'' ';
      S:=S+' and MS_FELDAT>='''+TOL_elott+''' AND MS_LERDAT<='''+IG_utan+''' ';  // az egy�ltal�n sz�ba j�het�kre egy sz�k�t�s
      // S:=S+' and MS_EXSTR='''+VISZO+''' '; // export, import
      S:=S+' and mb1.MB_JAKOD=mb2.MB_JAKOD ';  // azonos j�rathoz kapcsol�dnak
      S:=S+' and mb1.MB_MBKOD <> mb2.MB_MBKOD '; // hogy ne lehessen ugyanaz
      S:=S+' and rtrim(mb1.MB_JAKOD)<>'''' ';
      S:=S+' and mb1.MB_MBKOD='+IntToStr(MBKOD);
      S:=S+' and isnull(mb2.MB_NOCSOP, 0) = 0 ';  // ------ 2017.11.20. csoportb�l val� kivon�s lehet�s�ge ---- //
      S:=S+' group by MS_MBKOD) a , MEGBIZAS ';
      S:=S+' where a.MS_MBKOD = MB_MBKOD ';
      S:=S+' and MTOL < '''+IG+''' and MIG > '''+TOL+''' ';  // MTOL<IG and MIG>TOL
      S:=S+' and MS_MBKOD <> '+IntToStr(MBKOD);  // nem �nmaga
      // a k�t VISZO megegyezik, vagy egyik tartalmazza a m�sikat. A "van k�z�s elem�k" lek�rdez�st csak t�rolt elj�r�ssal vagy kliens oldali ciklussal tudn�m
        // megoldani, az lass� lenne.
      S:=S+' and ((ltrim(rtrim(MB_VISZO))='''+VISZO+''') or (PatIndex(''%''+ltrim(rtrim(MB_VISZO))+''%'', '''+VISZO+''')>0 ) '+
              ' or (PatIndex(''%'+VISZO+'%'', ltrim(rtrim(MB_VISZO)))>0 ))';
      Query_run(QueryAlap, S);
      if QueryAlap.RecordCount > 20 then begin  // pr�b�ljuk elkapni a gyan�san nagy elem� csoportokat
          HibaKiiro('Gyan�san sok r�szmegb�z�s erre: '+S);
          end;
      while not QueryAlap.eof do begin
         MBLista_utana.Add(QueryAlap.FieldByName('MS_MBKOD').AsString);
         QueryAlap.Next;
         end;  // while
      CSOP_UTANA:= GetReszmegbizasCsop(MBLista_utana);
      end;  // UJ vagy MODOSIT
     HibaHely:='05';
     end;  // if LEHETCSOP - csak ekkor kellett a j�v�beli csoport besorol�s
   if (Akcio = mbTOROL) or (Akcio= mbMODOSIT) then begin
      // CSOP_ELOTTE:= GetReszmegbizasCsop(MBKODCsak);
      CSOP_ELOTTE:= GetReszmegbizasCsop(MBKOD);
      MBLista_elotte:= GetReszmegbizasCsop_elemek(CSOP_ELOTTE);
      end;  // TOROL vagy MODOSIT
   HibaHely:='06';
   // ha nem hoztuk m�g l�tre, akkor most (�resen)
   if MBLista_elotte=nil then begin
      MBLista_elotte:=TStringList.Create;
      end;  // ig
   HibaHely:='07';
   BeginTrans_ALAPKOZOS; // eddig nem volt m�dos�t�s, el�g itt kezdeni a tranzakci�t
    try  // finally
     try  // except
     // ------- ki�rt�kel�s ---------//
     if (CSOP_ELOTTE=0) and ((CSOP_UTANA=0) and (MBLista_utana.Count=0)) then begin  // nincs csoport
        UTINFO:=UtvonalSzamitas_megbizas(MBKOD);
        end;
     HibaHely:='08';
     if (CSOP_ELOTTE=CSOP_UTANA) and (CSOP_UTANA>0) then begin  // a csoport �sszet�tele nem v�ltozott: az�rt az �tvonal v�ltozhatott
        MBLista_utana.Add(IntToStr(MBKOD));
        UTINFO:=UtvonalSzamitas_megbizas(MBKOD);  // �jrasz�m�tjuk a csoportot, el�g az els� elemre, mind ugyanaz lesz
        Utinfo_csoportos_update(MBLista_utana, MBKOD, UTINFO, MB_EUDIJ);  // minden elemre
        end;
     HibaHely:='09';
     if (CSOP_ELOTTE>0) and (CSOP_UTANA=0) then begin  // (kiker�lt a csoportb�l)
          Reszmegbizas_remove(MBKOD);
          ListaElemTorol(MBLista_elotte, IntToStr(MBKOD)); // m�r nem tagja a csoportnak
          if MBLista_elotte.Count>0 then begin  // ha maradt valaki: �jrasz�m�tjuk a csoportot
            UTINFO_temp:=UtvonalSzamitas_megbizas(StrToInt(MBLista_elotte[0]));  // el�g az els� elemre, mind ugyanaz lesz
            Utinfo_csoportos_update(MBLista_elotte, MBKOD, UTINFO_temp, 0);  // minden elemre
            end;  // if
          UTINFO:=UtvonalSzamitas_megbizas(MBKOD); // az aktu�lis elemre nem t�roljuk, hanem visszaadjuk a h�v�nak
          end;
     HibaHely:='10';
     if (CSOP_ELOTTE=0) and ((CSOP_UTANA>0) or (MBLista_utana.Count>0)) then begin // (beker�lt a csoportba)
          if CSOP_UTANA=0 then begin  // �j csoport
            CSOP_UTANA:=GetNextCode('MEGBIZASRESZ', 'MR_CSOP');
             for i:= 0 to MBLista_utana.Count-1 do begin  // mindenkit hozz�adunk az �j csoporthoz
                Reszmegbizas_add(CSOP_UTANA, StrToInt(MBLista_utana[i]));
                end;  // for
            end;  //if
          Reszmegbizas_add(CSOP_UTANA, MBKOD);
          MBLista_utana.Add(IntToStr(MBKOD));
          UTINFO:=UtvonalSzamitas_megbizas(MBKOD);  // �jrasz�m�tjuk a csoportot, el�g az els� elemre, mind ugyanaz lesz
          Utinfo_csoportos_update(MBLista_utana, MBKOD, UTINFO, MB_EUDIJ);  // minden elemre
          end;
     HibaHely:='11';
      if (CSOP_ELOTTE>0) and (CSOP_UTANA>0) and (CSOP_ELOTTE<>CSOP_UTANA) then begin // (egyik csoportb�l ki, a m�sikba be, pl. v�ltozott a megb�z�s d�tuma vagy a rendsz�m
          Reszmegbizas_remove(MBKOD);
          ListaElemTorol(MBLista_elotte, IntToStr(MBKOD)); // m�r nem tagja a csoportnak
          if MBLista_elotte.Count>0 then begin  // ha maradt valaki: �jrasz�m�tjuk a csoportot
            UTINFO_temp:=UtvonalSzamitas_megbizas(StrToInt(MBLista_elotte[0]));  // el�g az els� elemre, mind ugyanaz lesz
            Utinfo_csoportos_update(MBLista_elotte, MBKOD, UTINFO_temp, 0);  // minden elemre
            end;  // if
          Reszmegbizas_add(CSOP_UTANA, MBKOD);
          MBLista_utana.Add(IntToStr(MBKOD));
          UTINFO:=UtvonalSzamitas_megbizas(MBKOD);  // �jrasz�m�tjuk a csoportot, el�g az els� elemre, mind ugyanaz lesz
          Utinfo_csoportos_update(MBLista_utana, MBKOD, UTINFO, MB_EUDIJ);  // minden elemre
          end;
     HibaHely:='12';
      Result:=UTINFO; // az aktu�lis elemre vonatkoz�t visszaadjuk a h�v�nak
      CommitTrans_ALAPKOZOS;
    except
       on E: Exception do begin
          RollbackTrans_ALAPKOZOS;
          Hibauzenet:= 'Hiba a "Megbizas_valtozas" elj�r�sban ('+HibaHely+'): '+E.Message;
          NoticeKi(Hibauzenet);
          Hibakiiro(Hibauzenet);
          UTINFO.Hibauzenet:=Hibauzenet;
          UTINFO.Kilometer:=0;
          UTINFO.Atallas:=0;
          UTINFO.Leiras:='';
          Result:=UTINFO;
          end;  // on E
      end;  // try-except
  finally
   if MBLista_elotte<>nil then MBLista_elotte.Free;
   if MBLista_utana<>nil then MBLista_utana.Free;
    // if MBKODCsak<>nil then MBKODCsak.Free;
    end;  // try-finally
end;


function TEgyebDlg.GetReszmegbizasCsop(MBKOD: integer): integer;
var
   MBKODCsak: TStringList;
begin
  MBKODCsak:=TStringList.Create;
  try
    MBKODCsak.Add(IntToStr(MBKOD));
    Result:= GetReszmegbizasCsop(MBKODCsak);
  finally
   if MBKODCsak<>nil then MBKODCsak.Free;
   end;  // try-finally
end;


function TEgyebDlg.GetReszmegbizasCsop(MBKODLista: TStringList): integer;
var
  S: string;
begin
  if MBKODLista.Count>0 then begin
    MBKODLista.Delimiter:=',';
    S:='select distinct MR_CSOP from MEGBIZASRESZ where MR_MBKOD in ('+ MBKODLista.DelimitedText+')';
    Query_run(QueryAlap2, S);
    case QueryAlap2.RecordCount of
      0: begin  // egyik�k sincs csoportba sorolva
        // Result:=GetNewID('MEGBIZASRESZ');
        Result:=0;
        end;
      1: begin  // pontosan egy csoport van
        Result:=QueryAlap2.FieldByName('MR_CSOP').AsInteger;
        end;
      else begin  // egyn�l t�bb csoport van
        // ezzel nem tudunk mit kezdeni, adjuk vissza az els� csoportot
        // Result:=-1;
        Result:=QueryAlap2.FieldByName('MR_CSOP').AsInteger;
        end;
      end;  // case
    end // if
  else begin
    Result:=0;
    end;
end;

function TEgyebDlg.GetReszmegbizasCsop_elemek(CSOP: integer): TStringList;
var
  S: string;
begin
    Result:= TStringList.Create;
    S:='select MR_MBKOD from MEGBIZASRESZ where MR_CSOP='+IntToStr(CSOP);
    Query_run(QueryAlap2, S);
    while not QueryAlap2.eof do begin
        Result.Add(QueryAlap2.FieldByName('MR_MBKOD').AsString);
        QueryAlap2.Next;
        end;  // while
end;

function TEgyebDlg.Reszmegbizas_add(CSOP, MBKOD: integer): string;
var
  S: string;
  i: integer;
begin
  S:='select count(*) from MEGBIZASRESZ where MR_MBKOD='+IntToStr(MBKOD)+' and MR_CSOP='+IntToStr(CSOP);
  i:=StrToInt(Query_SelectString('MEGBIZASRESZ', S));
  if i=0 then begin
      Query_Insert (QueryAlap2, 'MEGBIZASRESZ',
						[
						'MR_CSOP', IntToStr(CSOP),
						'MR_MBKOD', IntToStr(MBKOD)
						]);
      end;
end;

function TEgyebDlg.Reszmegbizas_remove(MBKOD: integer): string;
var
  S: string;
  i: integer;
begin
  SQLFuttat('MEGBIZASRESZ', 'delete from MEGBIZASRESZ where MR_MBKOD='+IntToStr(MBKOD));
end;

procedure TEgyebDlg.Utinfo_csoportos_update(MBLista: TStringList; ActMBKOD: integer; UTINFO: TUtvonalInfo; MB_EUDIJ: double);
var
    i: integer;
    CsoportEUDIJ, EURKM, EURKMU: double;
    S: string;
begin
   // mivel �k egy csoport, az EURKM �s EURKMU �rt�k�k is megegyezik
   MBLista.Delimiter:=',';
   S:='select sum(MB_EUDIJ) from megbizas where MB_MBKOD in ('+ MBLista.DelimitedText+') ';
   S:=S+ 'and MB_MBKOD<>'+IntToStr(ActMBKOD);  // �t mag�t ne sz�moljuk bele
   Query_run(QueryAlap2, S);
   if not QueryAlap2.Eof then
      CsoportEUDIJ:=QueryAlap2.Fields[0].AsFloat
   else CsoportEUDIJ:=0;
   CsoportEUDIJ:=CsoportEUDIJ + MB_EUDIJ;  // a k�perny�n szerepl�, mentetlen �rt�k
   if UTINFO.Kilometer>0 then begin
       EURKM:=CsoportEUDIJ / UTINFO.Kilometer;    // �t�ll�s n�lk�l
       EURKMU:=CsoportEUDIJ / (UTINFO.Kilometer + UTINFO.Atallas);  // �t�ll�ssal sz�molva
       end
   else begin
       EURKM:=0;    // �rv�nytelen
       EURKMU:=0;  // �rv�nytelen
       end;
   for i:= 0 to MBLista.Count-1 do begin
      Query_Update(QueryAlap2, 'MEGBIZAS',
       [
       'MB_EURKM',  SqlSzamString(EURKM, 12, 3),
       'MB_EURKMU',  SqlSzamString(EURKMU, 12, 3),
       'MB_TAVKM',  SqlSzamString(UTINFO.Kilometer, 12, 2),
       'MB_ATAKM',  SqlSzamString(UTINFO.Atallas, 12, 2),
       'MB_TAVMI',  ''''+UTINFO.Leiras+''''
       ], ' WHERE MB_MBKOD = '+MBLista[i]);
      end;  // for

end;


{function TEgyebDlg.GetElozoMegseged(MBKOD: integer): integer;
var
  S, TOL, RENSZ, UTOLSO_LERAKO: string;
begin
    S:='select MS_RENSZ from MEGSEGED where MS_FAJTA=''norm�l'' and MS_MBKOD = '+ MBKOD;
    Query_run(QueryAlap2, S);
    RENSZ:= QueryAlap2.FieldByName('MS_RENSZ').AsString;

    S:='select min(MS_FETED2+MS_FETEI2) TOL, max(MS_LETED2+MS_LETEI2) IG
    S:=S+' from MEGSEGED where MS_FAJTA=''norm�l'' and MS_MBKOD = '+ MBKOD;
    Query_run(QueryAlap2, S);
    if not QueryAlap2.Eof then begin
       TOL:= QueryAlap2.FieldByName('TOL').AsString;
       S:='select max(MS_LETED2+MS_LETEI2) UTOLSO_LERAKO from MEGSEGED ';
       S:=S+'where MS_RENSZ='''+RENSZ+''' and MS_LETED2+MS_LETEI2<='''+TOL+'''';
       Query_run(QueryAlap2, S);
       UTOLSO_LERAKO:= QueryAlap2.FieldByName('UTOLSO_LERAKO').AsString;

       S:='select MS_ID from MEGSEGED where MS_RENSZ='''+RENSZ+''' and and MS_LETED2+MS_LETEI2='''+UTOLSO_LERAKO+'''';
       Query_run(QueryAlap2, S);
       Result:= QueryAlap2.FieldByName('MS_ID').AsInteger;
       end;
end;
}

function TEgyebDlg.UtvonalSzamitas_megbizas(MBKOD: integer): TUtvonalInfo;
var
   S, FromString, GKKAT, CsoportInfo: string;
   UresKMSzazalek: double;
   CSOP: integer;
   MBLista: TStringList;
begin
     CSOP:= GetReszmegbizasCsop(MBKOD);
     if CSOP>0 then begin  // ha r�szmegb�z�s, akkor a teljes csoportra k�rdezz�k le az �tvonalpontokat
       FromString:=' from MEGSEGED, MEGBIZASRESZ where MS_FAJTA=''norm�l'' and MS_MBKOD = MR_MBKOD and MR_CSOP='+IntToStr(CSOP);
       MBLista:= GetReszmegbizasCsop_elemek(CSOP);
       try
         if MBLista.Count>1 then begin
           MBLista.Delimiter:=',';
           CsoportInfo:='�sszes megb�z�s: '+MBLista.DelimitedText+'.';   // vessz�vel felsorolva
           // CsoportInfo:=CsoportInfo+'Teljes fuvard�j: 590 EUR.'  // Ezt nem lehet ide tenni, mert nem tudjuk friss�teni ha v�ltozik.
    			 CsoportInfo:=CsoportInfo+' Teljes �tvonal: ';
           end
         else begin  // egy elem� csoport
           CsoportInfo:=''
           end;
       finally
         if MBLista<>nil then MBLista.Free;
         end;  // try-finally
       end
     else begin
       FromString:=' from MEGSEGED where MS_FAJTA=''norm�l'' and MS_MBKOD = '+IntToStr(MBKOD);
       CsoportInfo:='';
       end;  // else

     if ModifAktivalva('001_MtrxV3') then begin   // �j c�msorrendez�s, list�s ut�feldolgoz�ssal (t�bb mez� is van)
       S:='select distinct MS_FORSZ +'' ''+ MS_FELIR+'' ''+ MS_FELTEL CIM, MS_FELCIM UTCA, MS_FELTEL VAROS, MS_FELDAT+MS_FELIDO MIKOR1, ';
       S:=S+ ' MS_FELDAT2+MS_FELIDO2 MIKOR2, ''felrak'' MI, MS_SORSZ ';
       S:=S+ FromString;
       S:=S+' union all ';
       S:=S+' select distinct MS_ORSZA +'' ''+ MS_LELIR+'' ''+ MS_LERTEL CIM, MS_LERCIM UTCA, MS_LERTEL VAROS, MS_LERDAT+MS_LERIDO MIKOR1, ';
       S:=S+' MS_LERDAT2 + MS_LERIDO2 MIKOR2, ''lerak'', MS_SORSZ ';
       S:=S+ FromString;
       S:=S+' order by mikor1 ';
       Result:=UtvonalSzamitas_mag_v3(S, CsoportInfo, MBKOD);
       end
     else begin  // r�gi c�msorrendez�s
       S:='select distinct MS_FORSZ +'' ''+ MS_FELIR+'' ''+ MS_FELTEL CIM, MS_FELCIM UTCA, MS_FELTEL VAROS, MS_FELDAT+MS_FELIDO MIKOR, ''felrak'' MI ';
       S:=S+ FromString;
       S:=S+' union all ';
       S:=S+' select distinct MS_ORSZA +'' ''+ MS_LELIR+'' ''+ MS_LERTEL CIM, MS_LERCIM UTCA, MS_LERTEL VAROS, MS_LERDAT+MS_LERIDO MIKOR, ''lerak'' ';
       S:=S+ FromString;
       S:=S+' order by mikor ';
       // Result:=UtvonalSzamitas_mag(S, CsoportInfo, MBKOD);
       Result:=UtvonalSzamitas_mag_v2(S, CsoportInfo, MBKOD);
       end;

     // -------------------------------------------------------------------------------- //
     // Az �t�ll�s sz�m�t�si algoritmus (a csoport szerkezet figyelembe v�tel�vel) annyira bonyolult lett,
     // hogy haszn�lhatatlanra lass�tan� a megb�z�s m�dos�t�st. Egyel�re marad az �resfut�s becsl�s alap� �rt�k.
     GKKAT:= Query_Select('MEGBIZAS','MB_MBKOD', IntToStr(MBKOD),'MB_GKKAT');
     S:=Read_SZGrid('128', GKKAT);
     if trim(S)<>'' then
        UresKMSzazalek:= StrToInt(S)
     else UresKMSzazalek:=0;
     Result.Atallas:= Round(Result.Kilometer * UresKMSzazalek /100);
     // ---------------------------------------------------------------------------------- //
end;

function TEgyebDlg.UtvonalSzamitas_megseged(MS_ID1, MS_ID2: integer): TUtvonalInfo;
var
   S: string;
   CSOP: integer;
begin
   if ModifAktivalva('001_MtrxV3') then begin   // �j c�msorrendez�s, list�s ut�feldolgoz�ssal (t�bb mez� is van)
     S:='select distinct MS_FORSZ +'' ''+ MS_FELIR+'' ''+ MS_FELTEL CIM, MS_FELCIM UTCA, MS_FELTEL VAROS, MS_FELDAT+MS_FELIDO MIKOR1, ';
     S:=S+ ' MS_FELDAT2+MS_FELIDO2 MIKOR2, ''felrak'' MI, MS_SORSZ ';
     S:=S+ 'from MEGSEGED where MS_MBKOD = '+IntToStr(MS_ID1);
     S:=S+' union all ';
     S:=S+' select distinct MS_ORSZA +'' ''+ MS_LELIR+'' ''+ MS_LERTEL CIM, MS_LERCIM UTCA, MS_LERTEL VAROS, MS_LERDAT+MS_LERIDO MIKOR1, ';
     S:=S+' MS_LERDAT2 + MS_LERIDO2 MIKOR2, ''lerak'', MS_SORSZ ';
     S:=S+'from MEGSEGED where MS_MBKOD = '+IntToStr(MS_ID2);
     S:=S+' order by mikor1 ';
     Result:=UtvonalSzamitas_mag_v3(S, '', 0); // nincs CsoportInfo
     end
   else begin
     S:='select distinct MS_FORSZ +'' ''+ MS_FELIR+'' ''+ MS_FELTEL CIM, MS_FELCIM UTCA, MS_FELTEL VAROS, MS_FELDAT+MS_FELIDO MIKOR, ''felrak'' MI ';
     S:=S+ 'from MEGSEGED where MS_MBKOD = '+IntToStr(MS_ID1);
     S:=S+' union all ';
     S:=S+' select distinct MS_ORSZA +'' ''+ MS_LELIR+'' ''+ MS_LERTEL CIM, MS_LERCIM UTCA, MS_LERTEL VAROS, MS_LERDAT+MS_LERIDO MIKOR, ''lerak'' ';
     S:=S+'from MEGSEGED where MS_MBKOD = '+IntToStr(MS_ID2);
     S:=S+' order by mikor ';
     // Result:=UtvonalSzamitas_mag(S, '', 0); // nincs CsoportInfo
     Result:=UtvonalSzamitas_mag_v2(S, '', 0); // nincs CsoportInfo
     end
end;

function TEgyebDlg.UtvonalSzamitas_mag(SQL, CsoportInfo: string; MBKOD: integer): TUtvonalInfo;
var
   S, aktcim, aktutca, aktvaros, elozocim: string;
   GDist: TGoogleDistance;
   RInfo: TRouteInfo;
   UTINFO: TUtvonalInfo;
   // JSON: string;
   RouteLength, AktPont, AddResult: integer;
   RouteDesc, RetMessage, TAVMI, HibaHely, Hibauzenet: string;
begin
 // az eg�sz �tvonalsz�m�t�s kikapcsolhat�, ha �resen hagyjuk az .ini-ben a MATRIXAPIKEY-t
 if Trim(MATRIXAPIKEY)<>'' then begin
   HibaHely:='01';
   GDist:=TGoogleDistance.Create(Self);
   try
    try
     Query_run(QueryAlap, SQL);
     HibaHely:='02';
     with QueryAlap do begin
        elozocim:='';
        AktPont:=0;
        AddResult:=0;  // OK
        while (not eof) and (AddResult=0) do begin
            aktcim:=FieldByName('CIM').AsString;
            aktutca:=FieldByName('UTCA').AsString;
            aktvaros:=FieldByName('VAROS').AsString;
            Inc(AktPont);
            if (aktcim+' '+aktutca) <> elozocim then begin   // csak akkor kell, ha nem az el�z� �tvonalpont (pl. lerak, majd ugyanott felrak)
               if elozocim='' then begin // els� pont
                  AddResult:=GDist.Sources.AddPointAddr(aktcim, aktutca, aktvaros);  // az els� pont csak kiindul�si
                  end
               else begin
                  if AktPont=RecordCount then begin
                     AddResult:=GDist.Destinations.AddPointAddr(aktcim, aktutca, aktvaros);  // az utols� pont csak c�lpont
                     end
                  else begin
                    AddResult:=GDist.Sources.AddPointAddr(aktcim, aktutca, aktvaros);   // a k�ztes pontok kiindul�si �s c�lpontok is egyben
                    AddResult:=GDist.Destinations.AddPointAddr(aktcim, aktutca, aktvaros);
                    end;  // else
                  end;  // else
               end;
            elozocim:=aktcim+' '+aktutca;
            Next;
            end;  // while
        Close;
        end;  // with
      HibaHely:='03';
      if AddResult=0 then begin  // a pontok hozz�ad�sa sikeres volt
        RetMessage:=GDist.CalculateDistance(True, MBKOD);
        if RetMessage='' then begin
            HibaHely:='04.1';
            RInfo:=GDist.GetRouteInfo(RDM_All);
            UTINFO.Kilometer:=RInfo.RouteLength;
            // UTINFO.Leiras:= CsoportInfo + RInfo.RouteDesc;
            TAVMI:= CsoportInfo + RInfo.RouteDesc;
            if Length(TAVMI) > 500 then   // 500 karakteres az MB_TAVMI mez�
               TAVMI:=copy(TAVMI,1,497)+'...';
            UTINFO.Leiras:= TAVMI;
            UTINFO.Hibauzenet:='';
            end
        else begin
            HibaHely:='04.2';
            if Length('Ismeretlen c�m: '+RetMessage) > 500 then   // 500 karakteres az MB_TAVMI mez�
               RetMessage:=copy(RetMessage,1,497-Length('Ismeretlen c�m: '))+'...';
            UTINFO.Hibauzenet:='Ismeretlen c�m: '+RetMessage;  // ismeretlen pontok
            UTINFO.Kilometer:=0;
            UTINFO.Leiras:=UTINFO.Hibauzenet; // el kell majd t�rolni az MB_TAVMI-ben, hogy l�tsszon a sikertelens�g oka.
            end;
        end
      else begin // a pontok hozz�ad�sa sikertelen volt
          HibaHely:='05';
          UTINFO.Hibauzenet:='T�l sok �tvonalpont (kapcsolt megb�z�sok)!';
          UTINFO.Kilometer:=0;
          UTINFO.Leiras:=UTINFO.Hibauzenet; // el kell majd t�rolni az MB_TAVMI-ben, hogy l�tsszon a sikertelens�g oka.
          end;
     except
      on E: exception do begin
          Hibauzenet:= 'Hiba az "UtvonalSzamitas_mag" elj�r�sban ('+HibaHely+'): '+E.Message;
          NoticeKi(Hibauzenet);
          Hibakiiro(Hibauzenet);
          UTINFO.Hibauzenet:=Hibauzenet;
          UTINFO.Kilometer:=0;
          UTINFO.Leiras:=Hibauzenet;
          end; // on E
         end;  // try-except
   finally
      if GDist<>nil then GDist.Free;
   end;  // try-finally
  end  // if MATRIXAPIKEY
 else begin
    UTINFO.Hibauzenet:='�thossz sz�m�t�s inakt�v!';
    UTINFO.Kilometer:=0;
    UTINFO.Leiras:='';
   end;  // else
 Result:= UTINFO;
end;

function TEgyebDlg.UtvonalSzamitas_mag_v2(SQL, CsoportInfo: string; MBKOD: integer): TUtvonalInfo;
var
   S, aktcim, aktutca, aktvaros, kovetkezocim, kovetkezoutca, kovetkezovaros : string;
   GDist: TGoogleDistance;
   RInfo: TRouteInfo;
   UTINFO: TUtvonalInfo;
   // JSON: string;
   RouteLength, AktPont, AddResult: integer;
   RouteDesc, RetMessage, TAVMI, Marker, Hibauzenet: string;
   SzakaszOK, ElsoSzakasz: boolean;
   RouteDescMode: TRouteDescMode;
begin
 // az eg�sz �tvonalsz�m�t�s kikapcsolhat�, ha �resen hagyjuk az .ini-ben a MATRIXAPIKEY-t
 if Trim(MATRIXAPIKEY)<>'' then begin
   Marker:='01';
   SzakaszOK:= True;
   ElsoSzakasz:= True;
   UTINFO.Kilometer := 0;
   UTINFO.Leiras := '';
   GDist:=TGoogleDistance.Create(Self);
   try
    try
     Query_run(QueryAlap, SQL);
     Marker:='02';
     with QueryAlap do begin
        AddResult:=0;  // OK
        aktcim:=FieldByName('CIM').AsString;
        aktutca:=FieldByName('UTCA').AsString;
        aktvaros:=FieldByName('VAROS').AsString;
        while (not eof) and (AddResult=0) and (SzakaszOK) do begin
            Next;
            kovetkezocim:=FieldByName('CIM').AsString;
            kovetkezoutca:=FieldByName('UTCA').AsString;
            kovetkezovaros:=FieldByName('VAROS').AsString;
            if kovetkezocim <> aktcim then begin   // csak akkor kell, ha nem az el�z� �tvonalpont (pl. lerak, majd ugyanott felrak)
               GDist.Sources.ClearPoints;
               GDist.Destinations.ClearPoints;
               AddResult:=GDist.Sources.AddPointAddr(aktcim, aktutca, aktvaros);
               AddResult:=GDist.Destinations.AddPointAddr(kovetkezocim, kovetkezoutca, kovetkezovaros);
               Marker:='03';
               if AddResult=0 then begin  // a pontok hozz�ad�sa sikeres volt
                  RetMessage:=GDist.CalculateDistance(True, MBKOD);
                  if RetMessage='' then begin
                     Marker:='04.1';
                     if ElsoSzakasz then begin
                        RouteDescMode:= RDM_All; // az els� szakaszb�l kell a kiindul�s �s a c�l is
                        ElsoSzakasz:=False;
                        end
                     else begin
                        RouteDescMode:= RDM_DestOnly;  // a m�sodik szakaszt�l csak a c�l kell
                        end;
                     RInfo:=GDist.GetRouteInfo (RouteDescMode);
                     UTINFO.Kilometer := UTINFO.Kilometer + RInfo.RouteLength;
                     UTINFO.Leiras := UTINFO.Leiras + RInfo.RouteDesc;
                     // UTINFO.Hibauzenet:='';
                     end
                  else begin  // ismeretlen pontok
                     Marker:='04.2';
                     SzakaszOK:= False;  // a ciklust megszak�tjuk, nincs �rtelme tov�bb sz�molni
                     UTINFO.Hibauzenet:= RetMessage;  // ismeretlen pontok
                     UTINFO.Kilometer:=0;
                     UTINFO.Leiras:=UTINFO.Hibauzenet; // el kell majd t�rolni az MB_TAVMI-ben, hogy l�tsszon a sikertelens�g oka.
                      end; // else
                  end  // AddResult=0
               else begin // a pontok hozz�ad�sa sikertelen volt
                  Marker:='05';
                  UTINFO.Hibauzenet:='Sikertelen �tvonalpont hozz�ad�s!';
                  UTINFO.Kilometer:=0;
                  UTINFO.Leiras:=UTINFO.Hibauzenet; // el kell majd t�rolni az MB_TAVMI-ben, hogy l�tsszon a sikertelens�g oka.
                  end;  // else
               end;
            aktcim:= kovetkezocim;
            aktutca:= kovetkezoutca;
            aktvaros:= kovetkezovaros;
            end;  // while
        Close;
        end;  // with

     UTINFO.Leiras:= CsoportInfo + UTINFO.Leiras;
     if Length(UTINFO.Leiras) > 500 then   // 500 karakteres az MB_TAVMI mez�
         UTINFO.Leiras:=copy(UTINFO.Leiras,1,497)+'...';

     if Length('Ismeretlen c�m: '+UTINFO.Hibauzenet) > 500 then   // 500 karakteres az MB_TAVMI mez�
         UTINFO.Hibauzenet:=copy(UTINFO.Hibauzenet,1,497-Length('Ismeretlen c�m: '))+'...';

     except
      on E: exception do begin
          Hibauzenet:= 'Hiba az "UtvonalSzamitas_mag_v2" elj�r�sban ('+Marker+'): '+E.Message;
          NoticeKi(Hibauzenet);
          Hibakiiro(Hibauzenet);
          UTINFO.Hibauzenet:=Hibauzenet;
          UTINFO.Kilometer:=0;
          UTINFO.Leiras:=Hibauzenet;
          end; // on E
         end;  // try-except
   finally
      if GDist<>nil then GDist.Free;
      end;  // try-finally
   end  // if MATRIXAPIKEY
  else begin
    UTINFO.Hibauzenet:='�thossz sz�m�t�s inakt�v!';
    UTINFO.Kilometer:=0;
    UTINFO.Leiras:='';
   end;  // else
 Result:= UTINFO;
end;

function TEgyebDlg.UtvonalSzamitas_mag_v3(SQL, CsoportInfo: string; MBKOD: integer): TUtvonalInfo;
var
   S, aktcim, aktutca, aktvaros, kovetkezocim, kovetkezoutca, kovetkezovaros : string;
   aktido1, aktido2, aktesemeny: string;
   GDist: TGoogleDistance;
   RInfo: TRouteInfo;
   UTINFO: TUtvonalInfo;
   // JSON: string;
   RouteLength, AktPont, AddResult, aktsorszam, TenylegesSzakaszok: integer;
   RouteDesc, RetMessage, TAVMI, Marker, Hibauzenet: string;
   SzakaszOK, ElsoSzakasz: boolean;
   RouteDescMode: TRouteDescMode;
   AktGeoAddress: TGeoAddress;
   // CimLista: TAddressList;
   CimLista: TList;
begin
 // az eg�sz �tvonalsz�m�t�s kikapcsolhat�, ha �resen hagyjuk az .ini-ben a MATRIXAPIKEY-t
 if Trim(MATRIXAPIKEY)<>'' then begin
   Marker:='01';
   // a c�meket list�ba t�ltj�k, �jrasorrendezz�k, majd a list�b�l dolgozzuk fel.
   CimLista:= TList.Create;
   Query_run(QueryAlap, SQL);
   with QueryAlap do begin
     while not eof do begin
        aktcim:=FieldByName('CIM').AsString;
        aktvaros:=FieldByName('VAROS').AsString;
        aktutca:=FieldByName('UTCA').AsString;
        aktido1:=FieldByName('MIKOR1').AsString;
        aktido2:=FieldByName('MIKOR2').AsString;
        aktesemeny:=FieldByName('MI').AsString;
        aktsorszam:=FieldByName('MS_SORSZ').AsInteger;
        AktGeoAddress:= TGeoAddress.Create(aktcim, aktvaros, aktutca, aktido1, aktido2, aktesemeny, aktsorszam);
        CimLista.Add(AktGeoAddress);
        Next;
        end;  // while
     Close;
     end;  // with

   if not Cimsorrend_beallit(CimLista) then begin  // valami gubanc volt
      raise Exception.Create('C�msorrend feldolgoz�s hiba');
      end;

   SzakaszOK:= True;
   ElsoSzakasz:= True;
   TenylegesSzakaszok:= 0;
   UTINFO.Kilometer := 0;
   UTINFO.Leiras := '';
   GDist:=TGoogleDistance.Create(Self);
   try
    try
     AktPont:= 0; // az 1. elem
     Marker:='02';
     AddResult:=0;  // OK
     if CimLista.Count > 0 then begin  // ha egy�ltal�n van eleme
       aktcim:= TGeoAddress(CimLista[AktPont]).FullAddress;
       aktvaros:= TGeoAddress(CimLista[AktPont]).CityOnly;
       aktutca:= TGeoAddress(CimLista[AktPont]).StreetNumber;
       end;
     // CimLista.Count-2 , mert a ciklusmagban van m�g egy Inc(AktPont);
     while (AktPont <= CimLista.Count-2) and (AddResult=0) and (SzakaszOK) do begin
            Inc(AktPont);
            kovetkezocim:= TGeoAddress(CimLista[AktPont]).FullAddress;
            kovetkezovaros:= TGeoAddress(CimLista[AktPont]).CityOnly;
            kovetkezoutca:= TGeoAddress(CimLista[AktPont]).StreetNumber;
            if kovetkezocim <> aktcim then begin   // csak akkor kell, ha nem az el�z� �tvonalpont (pl. lerak, majd ugyanott felrak)
               Inc(TenylegesSzakaszok);
               GDist.Sources.ClearPoints;
               GDist.Destinations.ClearPoints;
               AddResult:=GDist.Sources.AddPointAddr(aktcim, aktutca, aktvaros);
               AddResult:=GDist.Destinations.AddPointAddr(kovetkezocim, kovetkezoutca, kovetkezovaros);
               Marker:='03';
               if AddResult=0 then begin  // a pontok hozz�ad�sa sikeres volt
                  RetMessage:=GDist.CalculateDistance(True, MBKOD);
                  if RetMessage='' then begin
                     Marker:='04.1';
                     if ElsoSzakasz then begin
                        RouteDescMode:= RDM_All; // az els� szakaszb�l kell a kiindul�s �s a c�l is
                        ElsoSzakasz:=False;
                        end
                     else begin
                        RouteDescMode:= RDM_DestOnly;  // a m�sodik szakaszt�l csak a c�l kell
                        end;
                     RInfo:=GDist.GetRouteInfo (RouteDescMode);
                     UTINFO.Kilometer := UTINFO.Kilometer + RInfo.RouteLength;
                     UTINFO.Leiras := UTINFO.Leiras + RInfo.RouteDesc;
                     // UTINFO.Hibauzenet:='';
                     end
                  else begin  // ismeretlen pontok
                     Marker:='04.2';
                     SzakaszOK:= False;  // a ciklust megszak�tjuk, nincs �rtelme tov�bb sz�molni
                     UTINFO.Hibauzenet:= RetMessage;  // ismeretlen pontok
                     UTINFO.Kilometer:=0;
                     UTINFO.Leiras:=UTINFO.Hibauzenet; // el kell majd t�rolni az MB_TAVMI-ben, hogy l�tsszon a sikertelens�g oka.
                      end; // else
                  end  // AddResult=0
               else begin // a pontok hozz�ad�sa sikertelen volt
                  Marker:='05';
                  UTINFO.Hibauzenet:='Sikertelen �tvonalpont hozz�ad�s!';
                  UTINFO.Kilometer:=0;
                  UTINFO.Leiras:=UTINFO.Hibauzenet; // el kell majd t�rolni az MB_TAVMI-ben, hogy l�tsszon a sikertelens�g oka.
                  end;  // else
               end;  // if kovetkezocim <> aktcim
            aktcim:= kovetkezocim;
            aktutca:= kovetkezoutca;
            aktvaros:= kovetkezovaros;
            end;  // while

     if TenylegesSzakaszok = 0 then begin  // nem is volt "CalculateDistance" �s "GetRouteInfo"
        UTINFO.Kilometer:=0;
        UTINFO.Leiras:= 'Nincsenek �tvonalpontok!';
        end;

     UTINFO.Leiras:= CsoportInfo + UTINFO.Leiras;
     if Length(UTINFO.Leiras) > 500 then   // 500 karakteres az MB_TAVMI mez�
         UTINFO.Leiras:=copy(UTINFO.Leiras,1,497)+'...';

     if Length('Ismeretlen c�m: '+UTINFO.Hibauzenet) > 500 then   // 500 karakteres az MB_TAVMI mez�
         UTINFO.Hibauzenet:=copy(UTINFO.Hibauzenet,1,497-Length('Ismeretlen c�m: '))+'...';

     except
      on E: exception do begin
          Hibauzenet:= 'Hiba az "UtvonalSzamitas_mag_v3" elj�r�sban ('+Marker+'): '+E.Message;
          NoticeKi(Hibauzenet);
          Hibakiiro(Hibauzenet);
          UTINFO.Hibauzenet:=Hibauzenet;
          UTINFO.Kilometer:=0;
          UTINFO.Leiras:=Hibauzenet;
          end; // on E
         end;  // try-except
   finally
      if GDist<>nil then GDist.Free;
      if AktGeoAddress <> nil then AktGeoAddress.Free;
      if CimLista<>nil then CimLista.Free;
      end;  // try-finally
   end  // if MATRIXAPIKEY
  else begin
    UTINFO.Hibauzenet:='�thossz sz�m�t�s inakt�v!';
    UTINFO.Kilometer:=0;
    UTINFO.Leiras:='';
   end;  // else
 Result:= UTINFO;
end;

function TEgyebDlg.UtvonalSzamitas_simple(Lat1, Long1, Lat2, Long2: double): integer;
var
   GDist: TGoogleDistance;
   RInfo: TRouteInfo;
begin
 // az eg�sz �tvonalsz�m�t�s kikapcsolhat�, ha �resen hagyjuk az .ini-ben a MATRIXAPIKEY-t
 if Trim(MATRIXAPIKEY)<>'' then begin
   GDist:=TGoogleDistance.Create(Self);
   try
    try
     GDist.Sources.AddPointCoord(Lat1, Long1, '');
     GDist.Destinations.AddPointCoord(Lat2, Long2, '');
     GDist.CalculateDistance(False, 0);
     RInfo:=GDist.GetRouteInfo (RDM_DestOnly);
     Result:= RInfo.RouteLength;
   except
     Result:= -1;
     end;  // try-except
   finally
      if GDist<>nil then GDist.Free;
      end;  // try-finally
   end;  // if
end;


// A k�perny�n a felrak�s/lerak�s sorok id�rendben vannak, de mit jelent ez?
// Felrak�s2 nem el�zheti meg Felrak�s1-et.
// Felrak�s2 megel�zheti a Lerak�s1-et.
// Lerak�s2 megel�zheti Lerak�s1-et.
// --> csak a felrak�sokra kell el�rni, hogy teljes�lj�n a k�perny� szerinti sorrend
function TEgyebDlg.Cimsorrend_beallit(CimLista: TList): boolean;
var
   Pont1, Pont2: integer;
   Hibauzenet: string;
begin
   try
     for Pont1:=0 to CimLista.Count-1 do begin
       for Pont2:=Pont1+1 to CimLista.Count-1 do begin
          if (TGeoAddress(CimLista[Pont1]).FuvarosEvent = 'felrak') and (TGeoAddress(CimLista[Pont2]).FuvarosEvent = 'felrak')
             and (TGeoAddress(CimLista[Pont1]).OrderNumber > TGeoAddress(CimLista[Pont2]).OrderNumber) then begin
                // nem megcser�lni kell �ket!
                // CimLista.Exchange(Pont1, Pont2);
                // az alacsonyabb sorsz�m� felrak�st a magasabb sorsz�m� el� hozzuk fel
                CimLista.Move(Pont2, Pont1);
                end;  // nem megfelel� sorrend
          end;  // for
       end;  // for

      Result:= True;
    except
      on E: exception do begin
          Hibauzenet:= 'Hiba a "Cimsorrend_beallit" elj�r�sban: '+E.Message;
          NoticeKi(Hibauzenet);
          Hibakiiro(Hibauzenet);
          Result:= False;
          end; // on E
         end;  // try-except
end;

function TEgyebDlg.GetFelrakoLerako(MBKOD: integer): TFelrakLerak;
var
   S: string;
   TOL, IG: string;
   Siker: boolean;
begin
   Siker:=True;
   // Megb�z�s id�szak meghat�roz�sa: els� felrak�st�l utols� lerak�sig
   S:='select min(MS_FELDAT+MS_FELIDO) TOL, max(MS_LERDAT+MS_LERIDO) IG ';
   S:=S+' from MEGSEGED where MS_FAJTA=''norm�l'' and MS_MBKOD = '+IntToStr(MBKOD);
   Query_run(QueryAlap, S);
   if QueryAlap.RecordCount > 0 then begin
     TOL:=QueryAlap.FieldByName('TOL').AsString;
     IG:=QueryAlap.FieldByName('IG').AsString;
     Result.FelDatum:=copy(TOL,1,11); // csak a nap kell
     end
   else begin
     Siker:=False;
     end;
   if Siker then begin
     S:='select MS_FORSZ ORSZAG, MS_FELIR IRANYITO ';
     S:=S+' from MEGSEGED where MS_FAJTA=''norm�l'' and MS_MBKOD ='+IntToStr(MBKOD)+' and MS_FELDAT+MS_FELIDO='''+TOL+'''';
     Query_run(QueryAlap, S);
     if QueryAlap.RecordCount > 0 then begin
       Result.FelOrszag:=QueryAlap.FieldByName('ORSZAG').AsString;
       Result.FelIranyito:=QueryAlap.FieldByName('IRANYITO').AsString;
       end
     else begin
       Siker:=False;
       end;
     end;  // if Siker
   if Siker then begin
     S:='select MS_ORSZA ORSZAG, MS_LELIR IRANYITO ';
     S:=S+' from MEGSEGED where MS_FAJTA=''norm�l'' and MS_MBKOD ='+IntToStr(MBKOD)+' and MS_LERDAT+MS_LERIDO='''+IG+'''';
     Query_run(QueryAlap, S);
     if QueryAlap.RecordCount > 0 then begin
       Result.LeOrszag:=QueryAlap.FieldByName('ORSZAG').AsString;
       Result.LeIranyito:=QueryAlap.FieldByName('IRANYITO').AsString;
       end
     else begin
       Siker:=False;
       end;
     end;  // if Siker
   Result.Ervenyes:=Siker;
end;

procedure TEgyebDlg.BeginTrans_ALAPKOZOS;
var
    S: string;
begin
  // S:='BEGIN TRANSACTION '+'USER'+user_code+'_'+FormatDateTime('yyyymmdd_HHmmss',Now);
  // Command_Run(SQLAlap, S, true);
  // S:='BEGIN TRANSACTION '+'USER'+user_code+'_'+FormatDateTime('yyyymmdd_HHmmss',Now);
  // Command_Run(SQLKozos, S, true);
end;

procedure TEgyebDlg.CommitTrans_ALAPKOZOS;
begin
  // while ADOConnectionAlap.InTransaction do  // az esetleges egym�sba �gyazott tranzakci�k miatt
  //   Command_Run(SQLAlap, 'COMMIT TRANSACTION', true);
  // while ADOConnectionKozos.InTransaction do  // az esetleges egym�sba �gyazott tranzakci�k miatt
  //   Command_Run(SQLKozos, 'COMMIT TRANSACTION', true);
end;

procedure TEgyebDlg.RollbackTrans_ALAPKOZOS;
begin
  // ------- ADOConnectionAlap.CommitTransaction;
  // if ADOConnectionAlap.InTransaction then
  //    Command_Run(SQLAlap, 'ROLLBACK TRANSACTION', true);
  // if ADOConnectionKozos.InTransaction then
  //    Command_Run(SQLKozos, 'ROLLBACK TRANSACTION', true);
end;

function TEgyebDlg.ModifAktivalva(DevCode: string): boolean;
var
  SzotarAdat: string;
begin
  SzotarAdat:= Query_Select2('SZOTAR', 'SZ_FOKOD', 'SZ_ALKOD', '990', DevCode, 'SZ_MENEV');
  Result:= (SzotarAdat = '1');
end;

function TEgyebDlg.GetDolgozoSMSKontakt(DOKOD: string): string;
var
   TEID, DolgozoSelect, S: string;
begin
   TEID:= GetElsodlegesSzam(DOKOD, True);
   if TEID = '' then begin  // nincs els�dleges sz�ma
      TEID:= GetBarmelyikSzam(DOKOD);
      end;  // if
   if EgyebDlg.SMS_KULDES_FORMA='MYSMS' then
       S:= '"'+Query_select('TELELOFIZETES', 'TE_ID', TEID, 'TE_TELSZAM')+'"';
   if EgyebDlg.SMS_KULDES_FORMA='SMSEAGLE' then
       S:= Query_select('TELELOFIZETES', 'TE_ID', TEID, 'TE_TELSZAM');
   Result:= S;
end;

function TEgyebDlg.GetDolgozoEmailKontakt(DOKOD: string): string;
var
   TEID, DolgozoSelect, S: string;
begin
   if EgyebDlg.SMS_KULDES_FORMA='MYSMS' then
       S:= '';  // MySMS m�dban nem kezel�nk email c�met
   if EgyebDlg.SMS_KULDES_FORMA='SMSEAGLE' then
       S:= Query_select('DOLGOZO', 'DO_KOD', DOKOD, 'DO_EMAIL');
   Result:= S;
end;

function TEgyebDlg.GetDolgozoKontakt(DOKOD: string; ForceSMS: boolean): string;
var
  smskontakt, emailkontakt: string;
begin
  if not ForceSMS then begin
    emailkontakt:= trim(GetDolgozoEmailKontakt(DOKOD));  // DOKOD-b�l
    end
  else begin
     emailkontakt:= '';
     end;
  if ErvenyesEmailKontakt(emailkontakt) then begin  // az email priorit�st �lvez (ingyenes)
      Result:=emailkontakt;
      end
  else begin
      smskontakt:= trim(GetDolgozoSMSKontakt(DOKOD));  // DOKOD-b�l
      Result:=smskontakt;
      end;  // else
end;

function TEgyebDlg.GetDolgozoICloudKontakt(const DOKOD: string): string;
var
  Telefonszam, LehetICLOUD: string;
begin
   Result:= '';
   LehetICLOUD:= Query_Select('DOLGOZO', 'DO_KOD', DOKOD, 'DO_ICLOUDMAIL');
   if LehetICLOUD = '1' then begin
     Telefonszam:= trim(GetDolgozoSMSKontakt(DOKOD));  // +36209588785
     Result:= ICLOUD_EMAIL_PREFIX+ copy(Telefonszam,4,999) + ICLOUD_EMAIL_POSTFIX;
     end;
end;

function TEgyebDlg.ErvenyesEmailKontakt(kontakt: string): boolean;
var
  KukacPos, PontPos: integer;
begin
  KukacPos:= Pos('@',kontakt);
  PontPos:= Pos('.',kontakt);
  Result:= (KukacPos > 0) and (PontPos > 0);  // lehetne m�g cizell�lni
end;

function TEgyebDlg.ErvenyesSMSEagleKontakt(kontakt: string): boolean;
var
  ElejeStimmel, NemEmail: boolean;
begin
  ElejeStimmel:= (Length(kontakt) >= 12) and (copy(kontakt,1,1) = '+');
  NemEmail:= (pos('@', kontakt) <=0 );
  Result:= ElejeStimmel and NemEmail;
end;


function TEgyebDlg.GetDolgozoSMSEagleCimzett(DOKOD: string; ForceSMS: boolean): string;
var
  DONAME, Megjelenitett_nev, kontakt: string;
begin
  DONAME:= Query_Select('DOLGOZO', 'DO_KOD', DOKOD, 'DO_NAME');
  kontakt:= trim(GetDolgozoKontakt(DOKOD, ForceSMS));
  if ErvenyesSMSEagleKontakt(kontakt) then
     Result:= '"'+DONAME + '" <'+kontakt+'@'+SMSEAGLE_DOMAIN+'>'
  else Result:= '"'+DONAME + '" <'+kontakt+'>';
end;

{function TEgyebDlg.GetDolgozoSMSKontakt(DOKOD: string): string;
var
   ecim, telef: string;
begin
  ecim:= Query_Select('DOLGOZO', 'DO_KOD', DOKOD, 'DO_EMAIL');
  if EgyebDlg.SMS_KULDES_FORMA='TELENORMAIL' then begin   // v�ltozatlan form�ban list�ba f�zz�k
    // Result:= ecim;
    Result:= '';  // nem t�mogatjuk innen az email k�ld�st
    end;

  if EgyebDlg.SMS_KULDES_FORMA='MYSMS' then begin
    if (Pos('@telenormail.hu', ecim)>0) or (Pos('@pannonmail.hu', ecim)>0) then begin  // r�gi form�tum
      ecim:=EgyebDlg.SepFrontToken('@', ecim);  // levessz�k az elej�t (a telefonsz�mot)
      Result:= '"+3620'+ecim+'"';
      end
    else if (Pos('@', ecim)>0) or (trim(ecim)='') then begin  // b�rmi m�s email vagy �res: a telefonsz�m
      telef:= Query_Select('DOLGOZO', 'DO_KOD', DOKOD, 'DO_TELEF');
      telef:= EgyebDlg.SepFrontToken(';', telef);  // ha lista, akkor az els� sz�mot vessz�k le
      Result:= '"'+telef+'"';
      end
    else if (copy(ecim,1,1)='+') and JoEgesz(copy(ecim,2,999)) then begin  // telefonsz�m forma: +36201234567
      Result:= '"'+ecim+'"';
      end;
    end;
end;
}

function TEgyebDlg.GetPartnerSMSKontakt(VEKOD: string): string;
var
   mobilszam, S: string;
begin
  mobilszam:= Query_Select('VEVO', 'V_KOD', VEKOD, 'VE_MOBIL');  // forma lehet tagolt is, pl. +36 20/123-4567
  mobilszam:= JoTelefonszam(mobilszam);      // --> forma: +36201234567
   if EgyebDlg.SMS_KULDES_FORMA='MYSMS' then
       S:= '"' + mobilszam + '"';
   if EgyebDlg.SMS_KULDES_FORMA='SMSEAGLE' then
       S:= mobilszam;
   Result:= S;
end;

function TEgyebDlg.GetPartnerEmailKontakt(VEKOD: string): string;
var
   kontakt, S: string;
begin
  kontakt:= Query_Select('VEVO', 'V_KOD', VEKOD, 'VE_EMAIL');
   if EgyebDlg.SMS_KULDES_FORMA='MYSMS' then
       S:= '';  // MySMS m�dban nem kezel�nk email c�met
   if EgyebDlg.SMS_KULDES_FORMA='SMSEAGLE' then
       S:= kontakt;
   Result:= S;
end;

function TEgyebDlg.GetElofizetesInfo(Telefonszam: string): TElofizetesInfo;
var
  DOKOD, S, TKID, Tech, TE_MEGJE: string;
begin
  S:= 'select TE_DOKOD, TE_MEGJE, TE_TKID from TELELOFIZETES where TE_TELSZAM = '''+Telefonszam+'''';
  Query_Run(Query3, S);
  DOKOD:= Query3.FieldByName('TE_DOKOD').AsString;
  TE_MEGJE:= Query3.FieldByName('TE_MEGJE').AsString;
  if DOKOD <> '' then begin
     Result.dokod:= DOKOD;
     Result.megjegyzes:= Query3.FieldByName('TE_MEGJE').AsString;
     end
  else begin
     TKID:=Query3.FieldByName('TE_TKID').AsString;
     if TKID <> '' then begin
       S:= 'select TK_DOKOD, TK_TECH, TK_MEGJE from TELKESZULEK where TK_ID = '+TKID;
       Query_Run(Query3, S);
       Result.dokod:= Query3.FieldByName('TK_DOKOD').AsString;
       Tech:= Query3.FieldByName('TK_TECH').AsString;
       // if Tech = '1' then
       // megjegyz�s NEM CSAK a technikai telefonokhoz
       Result.megjegyzes:= Query3.FieldByName('TK_MEGJE').AsString;
       if Result.megjegyzes = '' then
            Result.megjegyzes:= TE_MEGJE;  // ha nincs a telefonn�l semmi
       end
     else begin // senki�
       Result.dokod:= '';
       Result.megjegyzes:= TE_MEGJE;
       end; // else
     end; // else
end;

function TEgyebDlg.GetElofizetesInfo_Idosoros(const Telefonszam, Mikor: string): TElofizetesInfo;
var
  S: string;
begin
  S:= 'select DOKOD, MEGJEGYZES from ( '+
       ' select TELSZAM, MEDDIG, DOKOD, MEGJEGYZES, ROW_NUMBER() OVER(ORDER BY TELSZAM, MEDDIG DESC) AS SORSZAM from ( '+
       ' select TE_TELSZAM TELSZAM, TH_MEDDIG MEDDIG, DO_KOD DOKOD, TE_MEGJE MEGJEGYZES'+
       ' from telhist, telelofizetes, dolgozo '+
       ' where DO_KOD = TH_DOKOD and TH_KATEG=''SIM'' and TH_KOD = TE_ID  '+
       ' union '+
       ' select TE_TELSZAM, CONVERT(varchar, getdate(), 102)+''.'', TE_DOKOD, TE_MEGJE '+
       ' from TELELOFIZETES where TE_STATUSKOD=1 and isnull(TE_DOKOD, '''') <> '''' '+
       ' union '+
       ' select TE_TELSZAM, CONVERT(varchar, getdate(), 102)+''.'', TK_DOKOD, TK_MEGJE '+
		   ' from TELELOFIZETES, TELKESZULEK where TE_TKID = TK_ID and TE_STATUSKOD=1 and isnull(TK_DOKOD, '''') <> '''' '+
       ' union '+
       ' select TE_TELSZAM, CONVERT(varchar, getdate(), 102)+''.'', '''', TE_MEGJE '+
       ' from TELELOFIZETES where TE_STATUSKOD=1 '+
       ' and isnull(TE_DOKOD, '''') = ''''  and isnull(TE_TKID, '''') = '''' '+
       '  ) a where TELSZAM= '''+Telefonszam+''' and MEDDIG<='''+Mikor+''') b '+
       ' where  SORSZAM = 1 ';  // a legut�bbi
  Query_Run(Query3, S);
  if not Query3.Eof then begin
     Result.dokod:= Query3.FieldByName('DOKOD').AsString;
     Result.megjegyzes:= Query3.FieldByName('MEGJEGYZES').AsString;
     end  // if
  else begin
     Result.dokod:= '';
     Result.megjegyzes:= '';
     end; // else
end;

function TEgyebDlg.GetElofizetesInfo_Idoszak(const Telefonszam, DatumTol, DatumIg: string): TElofizetesInfoMulti;
var
  S: string;
begin
  //   Command_Run(SQLAlap, S, True);

  {S:= 'IF OBJECT_ID(''tempdb..#temp'') IS NOT NULL DROP TABLE #temp ';
  S:= S+' ; ' + 'select TELSZAM, ''           '' METTOL, MEDDIG, DOKOD, ROW_NUMBER() OVER(ORDER BY TELSZAM, MEDDIG) AS SORSZAM, MEGJEGYZES '+
      ' into #temp from ( '+
       ' select TE_TELSZAM TELSZAM, TH_MEDDIG MEDDIG, DO_KOD DOKOD, TE_MEGJE MEGJEGYZES'+
       ' from telhist, telelofizetes, dolgozo '+
       ' where DO_KOD = TH_DOKOD and TH_KATEG=''SIM'' and TH_KOD = TE_ID  '+
       ' union '+
       ' select TE_TELSZAM, CONVERT(varchar, getdate(), 102)+''.'', TE_DOKOD, TE_MEGJE '+
       ' from TELELOFIZETES where TE_STATUSKOD=1 and '+
       ' ((isnull(TE_DOKOD, '''') <> '''' ) or (TE_MEGJE <> '''')) '+
       // isnull(TE_DOKOD, '''') <> '''' '+
       ' union '+
       ' select TE_TELSZAM, CONVERT(varchar, getdate(), 102)+''.'', TK_DOKOD, TK_MEGJE '+
		   ' from TELELOFIZETES, TELKESZULEK where TE_TKID = TK_ID and TE_STATUSKOD=1 and '+
       ' ((isnull(TK_DOKOD, '''') <> '''' ) or (TK_MEGJE <> '''')) '+
       // isnull(TK_DOKOD, '''') <> '''' '+
       ' union '+
       ' select TE_TELSZAM, CONVERT(varchar, getdate(), 102)+''.'', '''', TE_MEGJE '+
       ' from TELELOFIZETES where TE_STATUSKOD=1 '+
       ' and isnull(TE_DOKOD, '''') = ''''  and isnull(TE_TKID, '''') = '''' '+
       '  ) a where TELSZAM= '''+Telefonszam+''' ';
  S:= S+' ; ' + 'update #temp set METTOL= (select MEDDIG from  #temp t2 where t2.SORSZAM= #temp.SORSZAM-1 ) where SORSZAM>=2 ';
  S:= S+' ; ' + 'update #temp set METTOL=''1900.01.01.'' where SORSZAM=1';
  Command_Run(SQLAlap, S, True);
  S:= 'select MEDDIG, DOKOD, DO_NAME, SORSZAM, MEGJEGYZES '+
      ' from #temp left join dolgozo on DO_KOD = DOKOD '+
      ' where  (MEDDIG between '''+DatumTol+''' and '''+DatumIg+''' )'+
      ' OR (SORSZAM= (select min(SORSZAM) from #temp where MEDDIG>'''+DatumIg+''') and METTOL <> '''+DatumIg+''' )';
   }

  S:= 'exec szami.SelectElofizetesInfo '''+Telefonszam+''','''+DatumTol+''','''+DatumIg+'''';
  Query_Run(QueryAlap, S);
  while not QueryAlap.Eof do begin
     Result.dokodlista:= Felsorolashoz(Result.dokodlista, QueryAlap.FieldByName('DOKOD').AsString, ',', False);
     Result.dolgozonevlista:= Felsorolashoz(Result.dolgozonevlista, QueryAlap.FieldByName('DO_NAME').AsString, ', ', False);
     Result.megjegyzes:= Felsorolashoz(Result.dolgozonevlista, QueryAlap.FieldByName('MEGJEGYZES').AsString, ', ', False);
     QueryAlap.Next;
     end;  // while
end;

function TEgyebDlg.JSONArray_DuplikacioTorol(ArrayBe: string): string;
var
  MyStringList: TStringList;
  jsArr: TJSONArray;
  // vJSONScenario, vJSONValue: TJSONValue;
  Elem, S: string;
  i: integer;
begin
  MyStringList:= TStringList.Create;
  try
    MyStringList.Sorted:=True;
    MyStringList.Duplicates:=dupIgnore;
    // ---------- JSON parse + list�ba --------- //
    jsArr := TJSONObject.ParseJSONValue(ArrayBe) as TJsonArray;
    for i := 0 to jsArr.Size - 1 do begin
       Elem:= (jsArr.Get(i) as TJSONString).Value;
       MyStringList.Add(Elem);
       end;  // for
    // ---------- ki�r�s --------- //
    S:= '';
    for i:=0 to MyStringList.Count-1 do begin
     Elem:= '"'+MyStringList[i]+'"';
     if S = '' then S:= '['+ Elem
     else S:= S + ' , '+Elem;
     end;  // for
    S:= S+']';
    Result:= S;
  finally
    if MyStringList<>nil then MyStringList.Free;
    end;  // try-finally
end;

function TEgyebDlg.StringLista_DuplikacioTorol(ListaBe, Sep: string): string;
 var
  MyStringList: TStringList;
  // vJSONScenario, vJSONValue: TJSONValue;
  Lista, Elem, S: string;
  i: integer;
begin
  MyStringList:= TStringList.Create;
  try
    MyStringList.Sorted:=True;
    MyStringList.Duplicates:=dupIgnore;
    // ---------- elemek list�ba --------- //
    Lista:= ListaBe;
    while Lista <> '' do begin
      Elem:= Trim(EgyebDlg.SepFrontToken(Sep, Lista));
      Lista:= Trim(EgyebDlg.SepRest(Sep, Lista));
      MyStringList.Add(Elem);
      end; // while
    // ---------- ki�r�s --------- //
    S:= '';
    for i:=0 to MyStringList.Count-1 do begin
     Elem:= MyStringList[i];
     if S = '' then S:= Elem
     else S:= S + Sep+' '+Elem;
     end;  // for
    Result:= S;
  finally
    if MyStringList<>nil then MyStringList.Free;
    end;  // try-finally
end;

procedure TEgyebDlg.DolgozoFotoMutato(DOKOD: string; Control: TControl);
var
  APoint: TPoint;
begin
   if DOKOD <> '' then begin
     Application.CreateForm(TDolgozoFotoMutatDlg, DolgozoFotoMutatDlg);
     try
       if DolgozoFotoMutatDlg.OpenPic(DOKOD) then begin
         APoint := Control.ClientToScreen(Point(0, Control.ClientHeight));
         DolgozoFotoMutatDlg.Left:= APoint.X - floor (DolgozoFotoMutatDlg.Width/3);  // "f�l�" ny�ljon
         DolgozoFotoMutatDlg.Top:= APoint.Y - floor (DolgozoFotoMutatDlg.Height/2);  // "f�l�" ny�ljon
         DolgozoFotoMutatDlg.ShowModal;
         end
       else begin
         NoticeKi('A dolgoz�hoz nincs k�p megadva!');
         end;
     finally
       DolgozoFotoMutatDlg.Release;
       end;  // try-finally
     end;  // if
end;

function TEgyebDlg.GetFuvarosSzin(Szinkod: string): TColor;
var
  SzinHexa: string;
  MyColor: TColor;
begin
  SzinHexa:= Read_SZGrid('961', Szinkod);
  Result:= StrToInt('$'+SzinHexa);
end;

function TEgyebDlg.SzovegFilter(S, MitCsinal: string): string;
var
  Res: string;
begin
   if MitCsinal='sz�m_per_sz�m' then begin
      // Az SMS-ben kik�ld�tt azonos�t�kat a telefon telefonsz�m hivatkoz�sk�nt mutatja,
      // �s egy ilyen esetben kihagyta a /6-ot a k�zep�b�l. Ennek elker�l�s�re a "/" el� �s m�g�
      // tesz�nk egy sz�k�zt, ha sz�m a szomsz�dja.
      Res:=S;
      Res:= TRegEx.Replace(Res, '(\d)(\/)', '\1 \2');  // illeszkedik a sz�mjegy + "/" mint�ra
      Res:= TRegEx.Replace(Res, '(\/)(\d)', '\1 \2');  // illeszkedik a "/" + sz�mjegy mint�ra
      end;  // 'sz�m_per_sz�m'
   Result:= Res;
end;

{
// helyette: Kozos.TruckpitUzenetKuldes
procedure TEgyebDlg.UzenetKuldesDolgozonak(dokod: string);
begin
  // UzenetSzerkesztoDlg:= TUzenetSzerkesztoDlg.Create(nil);
  // try
  UzenetSzerkesztoDlg.Reset;
  UzenetSzerkesztoDlg.AddDolgozoCimzett(dokod, cimzett, False);
  UzenetSzerkesztoDlg.SetSzoveg('');
  UzenetSzerkesztoDlg.EagleSMSTipus:= technikai;
  UzenetSzerkesztoDlg.ShowModal;
  // finally
  //  if UzenetSzerkesztoDlg <> nil then UzenetSzerkesztoDlg.Release;
  //  end;  // try-finally
end;
 }
procedure TEgyebDlg.MegbizasDebug(const mbkod, comment: string);
var
  S, TimeS: string;
begin
   if MEGBIZAS_DEBUG_ENABLED then begin
     TimeS:= FormatDateTime('yyyy.mm.dd. HH:mm', Now);
     S:= 'insert into debug (d1, d2, d3, d4, d5, d6) '+
            'select ''MEGBIZAS DEBUG'', '+
            ''''+TimeS+'_'+comment+''', '+
            'MB_MBKOD, MB_JAKOD, MB_JASZA, MB_VIKOD from MEGBIZAS where MB_MBKOD='+mbkod;
     Command_Run(SQLAlap, S, FALSE);
     end;  // if
end;

// a Read_SZGrid a program indul�sakori �llapotot mutatja, ez real-time
function TEgyebDlg.Read_Szotar(fokod, alkod : string): string;
begin
   Query_Run(QueryAlap2,  'select SZ_MENEV from SZOTAR where SZ_FOKOD='''+fokod+''' and SZ_ALKOD='''+alkod+'''', FALSE);
   Result:= QueryAlap2.Fields[0].AsString;
end;

function TEgyebDlg.Read_SzotarTechnikaiErtek(fokod, alkod : string): integer;
begin
   Query_Run(QueryAlap2,  'select SZ_EGYEB1 from SZOTAR where SZ_FOKOD='''+fokod+''' and SZ_ALKOD='''+alkod+'''', FALSE);
   Result:= QueryAlap2.Fields[0].AsInteger;
end;

procedure TEgyebDlg.Write_Szotar(fokod, alkod, ertek: string);
var
  S: string;
begin
   Query_Run(QueryAlap2,  'select SZ_MENEV from SZOTAR where SZ_FOKOD='''+fokod+''' and SZ_ALKOD='''+alkod+'''', FALSE);
   if QueryAlap2.Eof then begin
      Query_Run(QueryAlap2, 'INSERT INTO SZOTAR (SZ_FOKOD, SZ_ALKOD, SZ_MENEV, SZ_EGYEB1, SZ_ERVNY) '+
          ' values('''+fokod+''', '''+alkod+''', '''+ertek+''', 0, 1)', FALSE);
      end
   else begin
      Query_Run(QueryAlap2, 'UPDATE SZOTAR SET SZ_MENEV='''+ertek+''' where SZ_FOKOD='''+fokod+''' and SZ_ALKOD='''+alkod+'''', FALSE);
      end;

end;

function TEgyebDlg.FuvarosMost: string;
begin
  Result:= FormatDateTime('yyyy.mm.dd. HH:mm:ss', Now);
end;

procedure TEgyebDlg.DelFilesFromDir(Directory, FileMask: string; DelSubDirs: Boolean);
var
  SourceLst: string;
  FOS: TSHFileOpStruct;
begin
  FillChar(FOS, SizeOf(FOS), 0);
  FOS.Wnd := Application.MainForm.Handle;
  FOS.wFunc := FO_DELETE;
  SourceLst := Directory + '\' + FileMask + #0;
  FOS.pFrom := PChar(SourceLst);
  if not DelSubDirs then
    FOS.fFlags := FOS.fFlags OR FOF_FILESONLY;
  // Remove the next line if you want a confirmation dialog box
  FOS.fFlags := FOS.fFlags OR FOF_NOCONFIRMATION;
  // Add the next line for a "silent operation" (no progress box)
  FOS.fFlags := FOS.fFlags OR FOF_SILENT;
  SHFileOperation(FOS);
end;

function TEgyebDlg.FTP_Upload(const AediFileName, AftpHost, AftpUser, AftpPassword, AftpDir: string; const AftpPort: integer): string;
var
  ftpUpStream: TFileStream;
  myftp: TIdFTP;
  localsize, remotesize: integer;
  myfile: string;
begin
  ftpUpStream:= TFileStream.create(AediFileName, fmopenread);
  localsize:= ftpUpStream.Size;
  myftp:= TIdFTP.create(self);
  try
   try
    with myftp do begin
      Host:= AftpHost;
      Username:= AftpUser;
      Password:= AftpPassword;
      end;
    //Connect FTP server and Use PASV mode
    myftp.Connect(AftpHost, AftpPort);
    myftp.Passive:= true;
    if AftpDir <> '' then
      myftp.ChangeDir(AftpDir);
    myfile:= ExtractFileName(AediFileName);
    myftp.Put(ftpUpStream, myFile, false);
    remotesize:= myftp.size(myfile);
    if (remotesize = localsize) then
      result:= ''
    else result:= 'A felt�lt�s nem siker�lt!';
   except
    on E: Exception do begin
      result:='Hiba a felt�lt�s sor�n: '+E.Message;
      end;  // on E
    end;  // try-except
  finally
    ftpUpStream.Free;
    //Disconnect to Quit();
    myftp.Quit;
    myftp.Free;
    end; // try-finally
end;

procedure TEgyebDlg.DuplaTankolasKezeles(const Rendszam: string; const Km: integer);
// az azonos rendsz�m + km t�telek k�z�l csak a legutols� lesz tele, a t�bbi nem
var
   S: string;
begin
   S:= 'update koltseg '+
      ' set KS_TELE = case when SORSZAM = 1 then 1 else 0 end, '+
      '	KS_TELES = case when SORSZAM = 1 then ''I'' else ''N'' end '+
      ' FROM '+
      ' koltseg inner join (select KS_KTKOD, ROW_NUMBER() OVER(ORDER BY KS_DATUM+KS_IDO, KS_KTKOD DESC) AS SORSZAM '+
      '		from koltseg WHERE KS_RENDSZ = '''+Rendszam+''' AND KS_KTIP = 1 AND KS_TIPUS = 0  '+
      '  AND KS_KMORA  = '+IntToStr(Km)+') a on a.KS_KTKOD = koltseg.KS_KTKOD ';
   Command_Run(SQLAlap, S, True);
end;


function TEgyebDlg.GetFelettesEmail(TERULET: string): TStringList;
var
   S, DOKOD, FelettesLista, GeneralLista, TeljesLista, EmailLista: string;
begin
   Result:= TStringList.Create;
   FelettesLista:= EgyebDlg.Read_SZGrid('149', TERULET);  // a ter�let felettese
   GeneralLista:= EgyebDlg.Read_SZGrid('149', '0');      // minden ter�let felettese
   TeljesLista:=GeneralLista+','+FelettesLista;
   // EmailLista:='';
   while TeljesLista <> '' do begin
      DOKOD:= Trim(EgyebDlg.SepFrontToken(',', TeljesLista));
      TeljesLista:= Trim(EgyebDlg.SepRest(',', TeljesLista));
      // EmailLista:= Felsorolashoz(EmailLista, trim(EgyebDlg.GetDolgozoEmailKontakt(DOKOD)), ',', False);
      S:= trim(EgyebDlg.GetDolgozoEmailKontakt(DOKOD));
      Result.Add(S);
      end; // while
end;

{function TEgyebDlg.GetFelettesEmail(TERULET: string): string;
var
   S, DOKOD, FelettesLista, GeneralLista, TeljesLista, EmailLista: string;
begin
   FelettesLista:= EgyebDlg.Read_SZGrid('149', TERULET);  // a ter�let felettese
   GeneralLista:= EgyebDlg.Read_SZGrid('149', '0');      // minden ter�let felettese
   TeljesLista:=GeneralLista+','+FelettesLista;
   EmailLista:='';
   while TeljesLista <> '' do begin
      DOKOD:= Trim(EgyebDlg.SepFrontToken(',', TeljesLista));
      TeljesLista:= Trim(EgyebDlg.SepRest(',', TeljesLista));
      EmailLista:= Felsorolashoz(EmailLista, trim(EgyebDlg.GetDolgozoEmailKontakt(DOKOD)), ',', False);
      end; // while
   Result:= EmailLista;
end;
 }


function TEgyebDlg.RakodoNevFormat(const EredetiNev: string): string;
var
  S: string;
begin
   S:= StringReplace(EredetiNev,'&','_and_',[rfReplaceAll]);
   Result:= S;
end;

function TEgyebDlg.Telephely_e(const Nev, Orszag, ZIP, Varos, UtcaHazszam: string): boolean;
var
  S, fekod, telephelykod: string;
	Query1: TADOQuery;
begin
   S:= 'select FF_FEKOD from FELRAKO '+
      ' where FF_FELNEV='''+Nev+''' and FF_ORSZA='''+Orszag+''' and FF_FELIR='''+ZIP+''''+
      ' and FF_FELTEL='''+AposztrofCsere(Varos)+''' and FF_FELCIM='''+AposztrofCsere(UtcaHazszam)+'''';
   Query_run(Query3, S);
   Result:= False;
   if not Query3.Eof then begin
      fekod:= Query3.Fields[0].AsString;
      telephelykod:= Read_SZGrid('999', '759');
      Result:= (fekod = telephelykod);
      end;
end;

procedure TEgyebDlg.MyShellExecute(const CommandLine: string);
var
  S: string;
begin
    // S:= StringEncodeAsUTF8(CommandLine);
    // S:= '"'+CommandLine+'"';
    S:= CommandLine;
    ShellExecute(Application.Handle,'open',PChar(S), '', '', SW_SHOWNORMAL);
end;

function TEgyebDlg.MerciPozicioJelentesKuld (MBKODLIST: TStringList; dokod: string; var Szoveg: string): string;
var
  S, Uzenet, Title, cimzettek, cimzett, Res: string;
  i: integer;
  cimzettlista, cclista, UresSL: TStringlist;
  Siker: boolean;
begin
    cimzettlista:=TStringlist.Create;
    UresSL:=TStringList.Create;
    cclista:=TStringList.Create;
    try
     try
      if dokod='' then begin
        cimzettek:= MERCEDES_POZICIO_CIMZETTEK;
        end  // if
      else begin  // van kit�ltve dolgoz� k�d
        cimzettek:= GetDolgozoEmailKontakt(DOKOD);
        cimzettek:= cimzettek + ';nagy.peter@js.hu'; // l�tni szeretn�m
        end; // else
      while cimzettek <> '' do begin
        cimzett:= Trim(EgyebDlg.SepFrontToken(',', cimzettek));
        cimzettek:= Trim(EgyebDlg.SepRest(',', cimzettek));
        cimzettlista.Add(cimzett);
        end; // while
      Uzenet:= '';
      Title:= MERCEDES_POZICIO_TARGY + ' '+FormatDateTime('dd/mm/yyyy', now);
      for i:=0 to MBKODLIST.Count-1 do begin
          Uzenet:= Uzenet + MerciPozicioJelentesHTML(StrToInt(MBKODLIST[i]))+
              const_HTML_Ujsor +
              '----------------------------------------------------------------------------------------------------------'+
              const_HTML_Ujsor;
          end;
      Uzenet:= Uzenet +  const_HTML_Ujsor +
        '<i><small>Automatisch generierte J&S Systemnachricht, bitte antworten Sie nicht! '+
        ' This is an automatically generated email, please do not reply! </small></i>';

      SetLength(LevelMellekletLista, 0);
      Res:= EmailDlg.SendLevel(cimzettlista, '', Title, Uzenet, True, 'Fuvaros �zenetk�zpont',
                   LevelMellekletLista, LevelMellekletNevek, LevelMellekletTipusok, 'multipart/mixed', 'text/html');
      // Siker:= SendOutlookHTMLMmail(cimzettlista, cclista, UresSL, Title, Uzenet, False, technikai);  // el�sz�r csak jelen�tse meg
      // WriteLogFile(kutaslogfn, 'NapiJelentes elk�ldve. Sz�veg: '+Uzenet+' '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
      Szoveg:= Uzenet;
      Result:= Res;
    except
      on E: Exception do begin
         Result:= 'MerciPozicioJelentesKuld hiba: '+E.Message;
         end;
      end;  // try-except
  finally
      if cimzettlista <> nil then cimzettlista.Free;
      if UresSL <> nil then UresSL.Free;
      if cclista <> nil then cclista.Free;
      end;  // try-finally
end;

function TEgyebDlg.MerciPozicioJelentesHTML (MBKOD: integer): string;
const
  WorthLat = 49.043511; // coordinates of W�rth am Rhein
  WorthLong = 8.283188;
var
  S, S2, S3, Uzenet: string;
  WorthDistance: integer;

begin
  S:= 'select '+
    ' max(case when MS_SORSZ=1 and MS_TIPUS=0 then ERKEZES_IDO else '''' end) MOVAR_ERKEZES, '+
    ' max(case when MS_SORSZ=1 and MS_TIPUS=0 then INDULAS_IDO else '''' end) MOVAR_INDULAS, '+
    ' max(case when MS_SORSZ=1 and MS_TIPUS=1 then ERKEZES_IDO else '''' end) BRECLAV_ERKEZES, '+
    ' max(case when MS_SORSZ=3 and MS_TIPUS=0 then INDULAS_IDO else '''' end) BRECLAV_INDULAS, '+
    ' max(case when MS_SORSZ=2 and MS_TIPUS=1 then ERKEZES_IDO else '''' end) WORTH_ERKEZES  '+
    ' from  '+
    ' (select MS_SORSZ, MS_ID, MS_TIPUS, '+
   '    case when MS_TIPUS=0 then '+
   '    case when datalength(MS_FERKIDO)>=5 then MS_FERKDAT +'' ''+ MS_FERKIDO else isnull(MS_FPOSOK, '''') end '+
   '    else '+
   '       case when datalength(MS_LERKIDO)>=5 then MS_LERKDAT +'' ''+ MS_LERKIDO else isnull(MS_LPOSOK, '''') end '+
   '    end ERKEZES_IDO, '+
   '    case when MS_TIPUS=0 then '+
   '    	case when datalength(MS_FETIDO)>=5 then MS_FETDAT +'' ''+ MS_FETIDO else isnull(MS_INDULOK, '''') end '+
   '    else  '+
   '       case when datalength(MS_LETIDO)>=5 then MS_LETDAT +'' ''+ MS_LETIDO else isnull(MS_INDULOK, '''') end  '+
   '    end INDULAS_IDO,  '+
   '    case when MS_TIPUS=0 then MS_TEFTEL else MS_TENTEL end VAROS  '+
   '    from MEGSEGED where MS_MBKOD = ' + IntToStr(MBKOD)+
   '    ) x1 ';

  Query_Run(Query3, S);
  S2:= 'select MB_RENSZ, MB_POTSZ, MB_BORDNO1+''-''+MB_BORDNO2 BORDERO, MB_KEDAT from MEGBIZAS where MB_MBKOD=' + IntToStr(MBKOD);
  Query_Run(Query2, S2);
  if not Query2.eof then begin
      Uzenet:= 'Bordero number: '+Query2.FieldByName('BORDERO').AsString +
        ' Truck number: '+Query2.FieldByName('MB_RENSZ').AsString +
        ' Trailer Number: '+Query2.FieldByName('MB_POTSZ').AsString +
        ' Departure date: '+ FuvarosTimestampToNemet(Query2.FieldByName('MB_KEDAT').AsString)+const_HTML_Ujsor;
      Uzenet:= Uzenet +'Arrival Mosonmagyar�v�r: '+ FuvarosTimestampToNemet(Query3.FieldByName('MOVAR_ERKEZES').AsString)+const_HTML_Ujsor;
      Uzenet:= Uzenet +'Departure Mosonmagyar�v�r: '+ FuvarosTimestampToNemet(Query3.FieldByName('MOVAR_INDULAS').AsString)+const_HTML_Ujsor;
      Uzenet:= Uzenet + const_HTML_Ujsor;
      Uzenet:= Uzenet +'Arrival Breclav: '+ FuvarosTimestampToNemet(Query3.FieldByName('BRECLAV_ERKEZES').AsString)+const_HTML_Ujsor;
      Uzenet:= Uzenet +'Departure Breclav: '+ FuvarosTimestampToNemet(Query3.FieldByName('BRECLAV_INDULAS').AsString)+const_HTML_Ujsor;
      Uzenet:= Uzenet + const_HTML_Ujsor;
      Uzenet:= Uzenet +'Arrival W�rth Am Rhein: '+ FuvarosTimestampToNemet(Query3.FieldByName('WORTH_ERKEZES').AsString)+const_HTML_Ujsor;
      Uzenet:= Uzenet + const_HTML_Ujsor;
      Uzenet:= Uzenet +'<b>Time: '+ FormatDateTime('dd/mm/yyyy HH:mm', now)+ '</b>' + const_HTML_Ujsor;
      S3:= 'select PO_GEKOD, PO_SZELE, PO_HOSZA, PO_DAT from POZICIOK where PO_RENSZ=''' + Query2.FieldByName('MB_RENSZ').AsString+'''';
      Query_Run(QueryAl3, S3);
      Uzenet:= Uzenet +'<b>Actual position: '+ QueryAl3.FieldByName('PO_GEKOD').AsString+ '</b> <i><small>('+
          QueryAl3.FieldByName('PO_DAT').AsString+ ') </i></small>'+
          GeoRoutines.GetGooglePlaceLink(QueryAl3.FieldByName('PO_SZELE').AsFloat, QueryAl3.FieldByName('PO_HOSZA').AsFloat) + const_HTML_Ujsor;
      if (Query3.FieldByName('BRECLAV_INDULAS').AsString <> '') and (Query3.FieldByName('WORTH_ERKEZES').AsString = '') then begin
         WorthDistance:= EgyebDlg.UtvonalSzamitas_simple(QueryAl3.FieldByName('PO_SZELE').AsFloat,
                QueryAl3.FieldByName('PO_HOSZA').AsFloat, WorthLat, WorthLong);
         // if (WorthDistance>=0) and (WorthDistance<=4000) then  // a furcsa eredm�nyeket nem k�rj�k.
         Uzenet:= Uzenet +'<b>Distance to W�rth Am Rhein: '+ IntToStr(WorthDistance)+ ' kilometers </b>' + const_HTML_Ujsor;
         end; // if
    end
 else begin
    Uzenet:= IntToStr(MBKOD)+'. megb�z�s: nincs adat. ';
    end;
 Result:= Uzenet;
 end;

function TEgyebDlg.IsMegbizasStandTailer(const MBKOD: integer; var AruLeiras: string): boolean;
var
  S, Res, TrailerPattern: string;
begin
  TrailerPattern:= Read_SZGrid('157', 'STAND_TRAILER'); // '%stand trailer%'
  // S:= 'select case when exists (select MS_FELARU from megseged where MS_MBKOD='+IntToStr(MBKOD)+' and MS_FELARU like '''+TrailerPattern+''') then 1 else 0 end ';
  S:= 'select max(MS_FELARU) from megseged where MS_MBKOD='+IntToStr(MBKOD)+' and MS_FELARU like '''+TrailerPattern+'''';
  AruLeiras:= Query_SelectString('', S);
  Result:= (AruLeiras <> '');
end;

function TEgyebDlg.GetMobilAppTelszam: string;
var
  TelSzam: string;
begin
 TelSzam:= GetDolgozoSMSKontakt(EgyebDlg.user_dokod);
 if TelSzam = '' then begin
      TelSzam:= Query_select('JELSZO', 'JE_FEKOD', EgyebDlg.user_code, 'JE_COCKPITTEL'); // k�ls�s�knek
      end;
 // ----------------------
 if copy(TelSzam,1,1)= '+' then TelSzam:= copy(TelSzam,2,9999); // egyel�re
 // ----------------------
 Result:= TelSzam;
end;

function TEgyebDlg.GetMobilAppJelszo: string;
begin
   Result:= DecodeString(Query_select('JELSZO', 'JE_FEKOD', EgyebDlg.user_code, 'JE_COCKPITJSZ'));
end;

function TEgyebDlg.StoreMegbizasLevel(const MBKOD, ACimzett, ASzoveg: string): boolean;
var
  emailkod: string;
begin
  emailkod := IntToStr(GetNextCode('MEGLEVEL', 'ML_MLKOD', 1, 0));
  Result:= Query_Insert (QueryAl3, 'MEGLEVEL',
     [
     'ML_MLKOD', emailkod,
     'ML_MBKOD', MBKOD,
     'ML_FEKOD', ''''+EgyebDlg.user_code+'''',
     'ML_FENEV', ''''+EgyebDlg.user_name+'''',
     'ML_DATUM', ''''+EgyebDlg.MaiDatum+'''',
     'ML_IDOPT', ''''+FormatDateTime('hh:nn',now)+'''',
     'ML_EMCIM', ''''+copy(ACimzett,1,128)+'''',
     'ML_SZOV1', ''''+copy(ASzoveg,1,512)+'''',
     'ML_SZOV2', ''''+copy(ASzoveg,513,512)+''''
    ] );
end;

function TEgyebDlg.Mintailleszt(const AKeresMinta, AMiben: string): boolean;
var
  NoEkezetMinta, NoEkezetMiben: string;
begin
  NoEkezetMinta:= NoEkezet(LowerCase(AKeresMinta));
  NoEkezetMiben:= NoEkezet(LowerCase(AMiben));
  Result:= (Pos(NoEkezetMinta, NoEkezetMiben) > 0);
end;

function TEgyebDlg.NoEkezet(const AInput: string): string;
var
  S: string;
begin
   S:= AInput;
   S:= StringReplace(S,'�','a',[rfReplaceAll]);
   S:= StringReplace(S,'�','i',[rfReplaceAll]);
   S:= StringReplace(S,'�','u',[rfReplaceAll]);
   S:= StringReplace(S,'�','o',[rfReplaceAll]);
   S:= StringReplace(S,'�','u',[rfReplaceAll]);
   S:= StringReplace(S,'�','o',[rfReplaceAll]);
   S:= StringReplace(S,'�','u',[rfReplaceAll]);
   S:= StringReplace(S,'�','o',[rfReplaceAll]);
   S:= StringReplace(S,'�','e',[rfReplaceAll]);

   S:= StringReplace(S,'�','a',[rfReplaceAll]);
   S:= StringReplace(S,'�','i',[rfReplaceAll]);
   S:= StringReplace(S,'�','u',[rfReplaceAll]);
   S:= StringReplace(S,'�','o',[rfReplaceAll]);
   S:= StringReplace(S,'�','u',[rfReplaceAll]);
   S:= StringReplace(S,'�','o',[rfReplaceAll]);
   S:= StringReplace(S,'�','u',[rfReplaceAll]);
   S:= StringReplace(S,'�','o',[rfReplaceAll]);
   S:= StringReplace(S,'�','e',[rfReplaceAll]);
   Result:= S;
end;

function TEgyebDlg.GetTruckpitAliasList(const DOKOD: string): TTruckpitAliasInfoArray;
var
  S: string;
  InfoRecord: TTruckpitAliasInfo;
  Res: TTruckpitAliasInfoArray;
begin
   Setlength(Res, 0);  // nem tudom kell-e
   S:= 'select TT_XUID, TT_NAME, TT_TEL, TT_JELSZO, TA_ENABLED '+
       ' from TRUCKPIT_TECHUSER '+
       ' left join TRUCKPIT_ALIAS on TA_XUID = TT_XUID '+
       ' where TA_FEKOD = '''+DOKOD+'''';
   Query_run(Query3, S);
   while not Query3.Eof do begin
      InfoRecord.AliasXUID:= Query3.FieldbyName('TT_XUID').AsString;
      InfoRecord.AliasNev:= Query3.FieldbyName('TT_NAME').AsString;
      InfoRecord.AliasTelefon:= Query3.FieldbyName('TT_TEL').AsString;
      InfoRecord.AliasJelszo:= DecodeString(Query3.FieldbyName('TT_JELSZO').AsString);
      InfoRecord.Enabled:= (Query3.FieldbyName('TA_ENABLED').AsString = '1');
      Setlength(Res, Length(Res)+1);
      Res[Length(Res)-1]:= InfoRecord;
      Query3.Next;
      end; // while
   Result:= Res;
end;

end.

