object ComboFormDlg: TComboFormDlg
  Left = 0
  Top = 0
  BorderIcons = []
  Caption = 'A hi'#225'ny oka'
  ClientHeight = 25
  ClientWidth = 253
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object cbCombo: TComboBox
    Left = 0
    Top = 0
    Width = 253
    Height = 24
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Arial'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    OnChange = cbComboChange
    ExplicitLeft = 8
    ExplicitTop = 2
    ExplicitWidth = 234
  end
end
