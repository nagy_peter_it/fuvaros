unit TelefonCsere;

interface

uses
	SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
	Forms, Dialogs, StdCtrls, Buttons, Mask, DB, DBTables, J_ALFORM, ADODB,
  Vcl.Grids, Vcl.ExtCtrls;

type
	TTelefonCsereDlg = class(TJ_AlformDlg)
	BitElkuld: TBitBtn;
	BitKilep: TBitBtn;
	 Query1: TADOQuery;
    GroupBox1: TGroupBox;
    Label3: TLabel;
    Label1: TLabel;
    Label9: TLabel;
    Label7: TLabel;
    meKiadas: TMaskEdit;
    BitBtn8: TBitBtn;
    meMegjegyzes: TMemo;
    meIMEI: TMaskEdit;
    cbTipus: TComboBox;
    GroupBox2: TGroupBox;
    chkRegiLegyenArchiv: TCheckBox;
    Label2: TLabel;
    meRegiIMEI: TMaskEdit;
	 procedure BitKilepClick(Sender: TObject);
	 procedure FormCreate(Sender: TObject);
	 procedure BitElkuldClick(Sender: TObject);
	 procedure MaskEditClick(Sender: TObject);
    procedure BitBtn8Click(Sender: TObject);
    procedure FormDestroy(Sender: TObject);

	private
    lista_tipus: TStringList;
    function TelefonCsere: string;
	public
    RegiTelKod: integer;
	end;

const
  SG_Elofizeteskod_oszlop = 5;
var
	TelefonCsereDlg: TTelefonCsereDlg;

implementation

uses
 Egyeb, J_SQL, Kozos,  J_VALASZTO;

{$R *.DFM}

procedure TTelefonCsereDlg.BitKilepClick(Sender: TObject);
begin
	ret_kod := '';
	Close;
end;

procedure TTelefonCsereDlg.FormCreate(Sender: TObject);
begin
	EgyebDlg.SeTADOQueryDatabase(Query1);
  lista_tipus	:= TStringList.Create;
  SzotarTolt(CBTipus, '146' , lista_tipus, false, false, true );
  CBTipus.ItemIndex  := lista_tipus.IndexOf(LegutobbFelvittTelefonTipus);
  meKiadas.Text:= EgyebDlg.Maidatum;
	ret_kod:= '';
end;

procedure TTelefonCsereDlg.FormDestroy(Sender: TObject);
begin
  if lista_tipus <> nil then lista_tipus.Free;
end;

procedure TTelefonCsereDlg.BitElkuldClick(Sender: TObject);
var
  S: string;
begin
  Screen.Cursor	:= crHourGlass;
  S:= TelefonCsere;
  if S='' then Close
  else NoticeKi('Hiba a v�grehajt�s sor�n: '+S);
	Screen.Cursor	:= crDefault;
end;

function TTelefonCsereDlg.TelefonCsere: string;
var
  UjTkID: integer;
  RegiTelDOKOD, S, S2: string;
  SQLArray: TStringList;
begin
 if trim(meIMEI.Text) = '' then begin
    // NoticeKi('Az �j IMEI nem lehet �res!');
    Result:= 'Az �j IMEI nem lehet �res!';
    Exit;  // procedure
    end;
 S := Query_Select('TELKESZULEK', 'TK_IMEI', meIMEI.Text, 'TK_ID');
 if S <>'' then UjTkID:= StrToInt(S)
 else UjTkID:= 0;
 if UjTkID <> 0 then begin
  	if NoticeKi('A megadott IMEI m�r l�tezik a rendszerben, azt fogjuk haszn�lni (telefon k�dja: '+IntToStr(UjTkID)+'). Rendben?', NOT_QUESTION) <> 0 then begin
        // ha nem fogadta el
        Exit;  // procedure
        end
    else begin  // elfogadta
        Query_Log_Str('Telefon csere: l�tez� IMEI elfogadva('+meIMEI.Text+'), TK_ID='+IntToStr(UjTkID), 0, true);  // napl�zzuk
        end;
    end
 else begin
   UjTkID:= Query_Insert_Identity (Query1, 'TELKESZULEK',
        [
        'TK_STATUSKOD',  ''''+'1'+'''', // haszn�latban
        'TK_TIPUSKOD', ''''+lista_tipus[cbTipus.ItemIndex]+'''',
        'TK_IMEI', ''''+meIMEI.Text+'''',
        // 'TK_DOKOD', ''''+M5.Text+'''', majd k�l�n be�ll�tjuk
        'TK_TECH', BoolToIntStr(False),
        // 'TK_APPLEID', ''''+meAppleID.Text+'''',
        // 'TK_APPLEJELSZO', ''''+meAppleJelszo.Text+'''',
        'TK_KIADDAT', ''''+meKiadas.Text+'''',
        'TK_MEGJE', ''''+meMegjegyzes.Text+''''
        ], 'TK_ID' );
   end;
  if UjTkID>=0 then begin  // onnan indulunk, hogy felvitt�k az �j k�sz�l�ket
    ret_kod:= IntToStr(UjTkID);
    SQLArray:= TStringList.Create;
    try
      RegiTelDOKOD:= Query_select('TELKESZULEK', 'TK_ID', IntToStr(RegiTelKod), 'TK_DOKOD'); // a tulajdonos
      // el�fizet�sek "�tcsatol�sa" az �j telefonra
      S:='update TELELOFIZETES set TE_TKID='+IntToStr(UjTkID)+' where TE_TKID= '+IntToStr(RegiTelKod);  // TELHIST bejegyz�s nem kell, nem v�ltozik a tulaj
      SQLArray.Add(S);
      // dolgoz� hozz�rendel�se az �j telefonhoz
      S:='update TELKESZULEK set TK_DOKOD='''+RegiTelDOKOD+''' where TK_ID= '+IntToStr(UjTkID);
      SQLArray.Add(S);
      // r�gi telefonhoz ne legyen dolgoz� + ha kell akkor arch�v legyen
      if chkRegiLegyenArchiv.Checked then S2:= ', TK_STATUSKOD = 2'
      else S2:='';
      S:='update TELKESZULEK set TK_DOKOD='''' '+S2+' where TK_ID= '+IntToStr(RegiTelKod);
      SQLArray.Add(S);
      // r�gi telefon �s dolgoz� �sszerendel�st a hist-be
      S:= 'select count(*) from TELHIST where TH_KATEG=''TELEFON'' and TH_DOKOD='+RegiTelDOKOD +
        ' and TH_MEDDIG='''+EgyebDlg.MaiDatum+''' and TH_KOD= '+IntToStr(RegiTelKod);
      if Query_SelectString('TELHIST', S) = '0' then begin  // csak akkor, ha nem l�tezett m�g
        S:='insert into telhist (TH_MEDDIG, TH_DOKOD, TH_KATEG, TH_KOD )';
        S:= S+ ' values ('''+EgyebDlg.MaiDatum+''', '+RegiTelDOKOD+', ''TELEFON'', '+IntToStr(RegiTelKod)+')';
        SQLArray.Add(S);
        end;  // if nem volt m�g
      // ---------- v�grehajt�s ------------- //
      Result:= ExecuteSQLTransaction(SQLArray);
    finally
      if SQLArray<>nil then SQLArray.Free;
      end;  // try-finally
    end  // if van UjTkID
  else begin
    Result:= 'Az �j k�sz�l�k r�gz�t�se nem siker�lt!';
    end;
end;

procedure TTelefonCsereDlg.MaskEditClick(Sender: TObject);
begin
	with Sender as TMaskEdit do begin
  	SelectAll;
  end;
end;

procedure TTelefonCsereDlg.BitBtn8Click(Sender: TObject);
begin
	EgyebDlg.Calendarbe(meKiadas);
end;

end.



























