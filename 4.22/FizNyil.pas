unit Fiznyil;

interface

uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, DBTables, DB, StdCtrls, Buttons, Mask, DBCtrls, 
  ExtCtrls, ADODB,DateUtils,Shellapi;

type
  TFiznyilDlg = class(TForm)
    BitBtn4: TBitBtn;
    BitBtn6: TBitBtn;
    Label1: TLabel;
    M2: TMaskEdit;
    Label2: TLabel;
    M3: TMaskEdit;
    BitBtn2: TBitBtn;
	 BitBtn3: TBitBtn;
    Label4: TLabel;
    M1: TMaskEdit;
    BitBtn5: TBitBtn;
    M0: TMaskEdit;
    BtnExport: TBitBtn;
    Query1: TADOQuery;
    BitBtn28: TBitBtn;
    CheckBox1: TCheckBox;
    CheckBox2: TCheckBox;
    CheckBox3: TCheckBox;
	 procedure FormCreate(Sender: TObject);
    procedure BitBtn4Click(Sender: TObject);
	 procedure BitBtn6Click(Sender: TObject);
	 procedure M2Exit(Sender: TObject);
	 procedure M3Exit(Sender: TObject);
	 procedure BitBtn2Click(Sender: TObject);
	 procedure BitBtn3Click(Sender: TObject);
	 procedure BitBtn5Click(Sender: TObject);
	 procedure BtnExportClick(Sender: TObject);
	 procedure BitBtn28Click(Sender: TObject);
    procedure M1Change(Sender: TObject);
  private
	  dolgozo		: string;
  public
  end;

var
  FiznyilDlg: TFiznyilDlg;
  UseFtKm: boolean;  // CheckBox2 helyett

implementation

uses
	J_VALASZTO, FizLis2, Egyeb, Kozos, J_EXCEL, FizExport, J_SQL, Lgauge,
  Windows;
{$R *.DFM}

procedure TFiznyilDlg.FormCreate(Sender: TObject);
begin
	M1.Text 	:= '';
 	M2.Text 	:= copy(EgyebDlg.MaiDatum,1,8)+'01.';
  	M3.Text 	:= EgyebDlg.MaiDatum;
  	dolgozo		:= '';
	SetMaskEdits([M1]);
	EgyebDlg.SetADOQueryDatabase(Query1);
  if EgyebDlg.v_formatum = 3 then
    UseFtKm:= False
  else UseFtKm:= True;

end;

procedure TFiznyilDlg.BitBtn4Click(Sender: TObject);
begin
	if M1.Text = '' then begin
		dolgozo 	:= '';
	end;
	{A lista �ssze�ll�t�sa a sz�r�si elemek alapj�n}
	if M2.Text = '' then begin
		M2.Text := copy(EgyebDlg.MaiDatum,1,8)+'01.';
	end;
	if M3.Text = '' then begin
		M3.Text := EgyebDlg.MaiDatum;;
	end;
	if not Jodatum2(M2) then begin
		NoticeKi('Rossz d�tum megad�sa!');
		M2.SetFocus;
		Exit;
	end;
	if not Jodatum2(M3) then begin
		NoticeKi('Rossz d�tum megad�sa!');
		M3.SetFocus;
		Exit;
	end;

	{A lista �ssze�ll�t�sa a sz�r�si elemek alapj�n}
	Screen.Cursor := crHourglass;
	Application.CreateForm(TFizlisDlg, FizlisDlg);

	// FizlisDlg.Tolt(dolgozo, M2.Text, M3.Text, M1.Text, CheckBox2.Checked, CheckBox3.Checked, EgyebDlg.fizlist_csehtran);
  FizlisDlg.Tolt(dolgozo, M2.Text, M3.Text, M1.Text, UseFtKm, CheckBox3.Checked, EgyebDlg.fizlist_csehtran);
	Screen.Cursor := crDefault;
	if FizlisDlg.vanadat	then begin
		FizlisDlg.Rep.Preview;
	end else begin
		NoticeKi('Nincs a megadott felt�teleknek megfelel� t�tel!');
	end;
	FizlisDlg.Destroy;
end;

procedure TFiznyilDlg.BitBtn6Click(Sender: TObject);
begin
	Close;
end;

procedure TFiznyilDlg.M2Exit(Sender: TObject);
begin
	if ( ( M2.Text <> '' ) and ( not Jodatum2(M2 ) ) ) then begin
		NoticeKi('Hib�s d�tum megad�sa!');
		M2.Text := '';
		M2.Setfocus;
	end;
  EgyebDlg.ElsoNapEllenor(M2, M3);
end;

procedure TFiznyilDlg.M3Exit(Sender: TObject);
begin
	if ( ( M3.Text <> '' ) and ( not Jodatum2(M3 ) ) ) then begin
		NoticeKi('Hib�s d�tum megad�sa!');
		M3.Text := '';
		M3.Setfocus;
	end;
end;

procedure TFiznyilDlg.BitBtn2Click(Sender: TObject);
begin
	// EgyebDlg.Calendarbe(M2);
  // EgyebDlg.ElsoNapEllenor(M2, M3);
  EgyebDlg.CalendarBe_idoszak(M2, M3);
end;

procedure TFiznyilDlg.BitBtn3Click(Sender: TObject);
begin
	EgyebDlg.Calendarbe(M3);
  EgyebDlg.UtolsoNap(M3);
  // M3.Text:=DateToStr(EndOfTheMonth(StrToDate(M3.Text)));
end;

procedure TFiznyilDlg.BitBtn5Click(Sender: TObject);
begin
	DolgozoValaszto(M0, M1);
	dolgozo	:= M0.Text;
end;

procedure TFiznyilDlg.BtnExportClick(Sender: TObject);
var
	recno	: integer;
   fnev    : string;
   kilep   : boolean;
begin
	// Az adatok export�l�sa
   kilep   := false;
	if M2.Text = '' then begin
		M2.Text := copy(EgyebDlg.MaiDatum,1,8)+'01.';
	end;
	if M3.Text = '' then begin
		M3.Text := EgyebDlg.MaiDatum;;
	end;
	if not Jodatum2(M2) then begin
		NoticeKi('Rossz d�tum megad�sa!');
		M2.SetFocus;
		Exit;
	end;
	if not Jodatum2(M3) then begin
		NoticeKi('Rossz d�tum megad�sa!');
		M3.SetFocus;
		Exit;
	end;
	if M1.Text = '' then begin
		if NoticeKi('Val�ban megcsin�lja a kimutat�st minden dolgoz�ra?', NOT_QUESTION) <> 0 then begin
			Exit;
		end;
		// Minden dolgoz�ra kell a kimutat�s
		Application.CreateForm(TFormGaugeDlg, FormGaugeDlg);
		FormGaugeDlg.GaugeNyit('Fizet�si adatok �sszegy�jt�se... ' , '', true);
		Query_Run(Query1, 'SELECT * FROM DOLGOZO WHERE DO_ARHIV = 0');
		recno	:= 1;
       fnev:= EgyebDlg.TempList + M2.Text + '_' + M3.Text+'.csv';
       if FileExists(fnev) then begin
           DeleteFile(pchar(fnev))   ;
       end;
       EgyebDlg.WriteLogFile(fnev,'N�v;kateg�ria;megtakar�t�s;�sszes.fiz;futott km; megtakar�t�s/km');
		while ( ( not Query1.Eof ) and (not FormGaugeDlg.kilepes) ) do begin
			Application.ProcessMessages;
			FormGaugeDlg.Pozicio ( ( recno * 100 ) div Query1.RecordCount ) ;
			Inc(recno);
			Application.CreateForm(TFizExportDlg, FizExportDlg);
           // FizExportDlg.vanftkm    := CheckBox2.Checked;
           FizExportDlg.vanftkm    := UseFtKm;
           FizExportDlg.jarbmeg    := CheckBox3.Checked;
			FizExportDlg.kelluzenet	:= false;
           FizExportDlg.csakgyujtes:=CheckBox1.Checked;
           FizExportDlg.fncsv:=fnev;;
			FizExportDlg.Tolt(Query1.FieldByName('DO_KOD').AsString, M2.Text, M3.Text, Query1.FieldByName('DO_NAME').AsString);
			FizExportDlg.BtnExportClick(Self);
			// 	FizExportDlg.ShowModal;
           kilep   := fizexportDlg.kilepes;
			FizExportDlg.Destroy;
			Query1.Next;
		end;
       if ( ( FileExists(fnev) ) and ( not kilep ) )then begin
           ShellExecute(Application.Handle,'open',PChar(fnev), '', '', SW_SHOWNORMAL );
       end;
       FormGaugeDlg.Destroy;
       Exit;
   end;

   {A lista �ssze�ll�t�sa a sz�r�si elemek alapj�n}
   Screen.Cursor := crHourglass;
   Application.CreateForm(TFizExportDlg, FizExportDlg);
   // FizExportDlg.vanftkm:= CheckBox2.Checked;
   FizExportDlg.vanftkm:= UseFtKm;
   FizExportDlg.jarbmeg:= CheckBox3.Checked;
   fnev:= EgyebDlg.TempList + M2.Text + '_' + M3.Text+'.csv';
   FizExportDlg.fncsv:=fnev;
   if FileExists(fnev) then begin
       DeleteFile(pchar(fnev))   ;
   end;
   FizExportDlg.Tolt(dolgozo, M2.Text, M3.Text, M1.Text);
   EgyebDlg.WriteLogFile(fnev,'N�v;kateg�ria;megtakar�t�s;�sszes.fiz;futott km; megtakar�t�s/km');
   Screen.Cursor := crDefault;
   FizExportDlg.ShowModal;
   kilep   := FizExportDlg.kilepes;
   FizExportDlg.Destroy;
   if ( ( FileExists(fnev) ) and (not kilep) ) then begin
       ShellExecute(Application.Handle,'open',PChar(fnev), '', '', SW_SHOWNORMAL );
   end;
end;

procedure TFiznyilDlg.BitBtn28Click(Sender: TObject);
begin
	M0.Text		:= '';
	M1.Text		:= '';
end;

procedure TFiznyilDlg.M1Change(Sender: TObject);
begin
  CheckBox1.Enabled:= M1.Text='';
  if M1.Text<>'' then CheckBox1.Checked:=False;
end;

end.


