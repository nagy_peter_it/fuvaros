unit szures;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ADODB, ExtCtrls, Kozos, KozosTipusok;

type
  TSzuresDlg = class(TForm)
    ListBox1: TListBox;
    ListBox3: TListBox;
    Panel1: TPanel;
    ButtonKilep: TBitBtn;
    ButtonKuld: TBitBtn;
    BitBtn1: TBitBtn;
    ButtonTor: TBitBtn;
    BitBtn2: TBitBtn;
    ButtonUj: TBitBtn;
    procedure ButtonKilepClick(Sender: TObject);
    procedure ButtonUjClick(Sender: TObject);
    procedure ButtonTorClick(Sender: TObject);
    procedure ButtonKuldClick(Sender: TObject);
    procedure Tolt(vQuery : TADOQuery; formtag : string; FixSzuresNev, FixSzuresSQL: TStringList);
    function SQLDarabol(S: string): TStringPar;
    procedure FormCreate(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
  private
    SzuresQuery	: TADOQuery;
  public
    SzuresStr		: string;
    kilepes		 	: boolean;
  end;

var
  SzuresDlg: TSzuresDlg;

implementation

{$R *.DFM}

uses
	Egyeb, Szurbe;

procedure TSzuresDlg.ButtonKilepClick(Sender: TObject);
begin
	kilepes := true;
	Close;
end;

procedure TSzuresDlg.ButtonUjClick(Sender: TObject);
begin
	Application.CreateForm(TSzurbeDlg, SzurbeDlg);
 	SzurbeDlg.querymas 	:= SzuresQuery;
 	SzurbeDlg.Tolt('');
 	SzurbeDlg.ShowModal;
 	if SzurbeDlg.ret_str <> '' then begin
 		// Van szur�si felt�tel
    	ListBox1.Items.Add(SzurbeDlg.ret_str);
    	ListBox3.Items.Add(SzurbeDlg.ret_str2);
 		ListBox1.ItemIndex := ListBox1.Items.Count - 1;
 		ListBox3.ItemIndex := ListBox3.Items.Count - 1;
    	BitBtn1.Show;
    	BitBtn2.Show;
    	ButtonTor.Show;
 	end;
 	SzurbeDlg.Destroy;
end;

procedure TSzuresDlg.ButtonTorClick(Sender: TObject);
var
	index		: integer;
begin
   if	NoticeKi( 'Val�ban t�rli a kijel�lt elemet? ', NOT_QUESTION ) = 0 then begin
  		index := ListBox3.ItemIndex;
  		ListBox1.Items.Delete(index);
  		ListBox3.Items.Delete(index);
     	if index > 0 then begin
     		Dec(index);
     	end;
  		if ListBox1.Items.Count < 1 then begin
        BitBtn1.Hide;
        BitBtn2.Hide;
  			ButtonTor.Hide;
     	end else begin
     		ListBox1.ItemIndex := index;
     		ListBox3.ItemIndex := index;
	  		end;
  	end;
end;

procedure TSzuresDlg.ButtonKuldClick(Sender: TObject);
var
	i : integer;
begin
	SzuresStr := '';
 	if listBox1.Items.Count > 0 then begin
 		SzuresStr := listBox1.Items[0];
    	for i := 1 to listBox1.Items.Count - 1 do begin
 			SzuresStr := SzuresStr + ' and ' +listBox1.Items[i];
    	end;
 	end;
 	kilepes := false;
 	Close;
end;

function TSzuresDlg.SQLDarabol(S: string): TStringPar;
begin
   // kezelj�k az "oszthatatlan", a fix sz�r�s �ltal betett elemeket. Ezeket /*begin*/ �s /*end*/ k�z�tt tal�ljuk
   S:=Trim(S);  // biztos ami biztos
   If copy(S,1,length(fix_szures_blokk_eleje))=fix_szures_blokk_eleje then begin
     Result.Elso:= EgyebDlg.SepFrontToken(fix_szures_blokk_vege, S) + fix_szures_blokk_vege;
     Result.Masodik:= Trim(EgyebDlg.SepRest(fix_szures_blokk_vege, S));  // a marad�k
     if EgyebDlg.SepFrontToken('and', Result.Masodik)='' then  // v�rhat�an 'and' a k�vetkez�
        Result.Masodik:= Trim(EgyebDlg.SepRest('and', Result.Masodik));
     end
   else begin
     Result.Elso:= EgyebDlg.SepFrontToken('and', S);   // 'and' szepar�torral has�tjuk list�ba
     Result.Masodik:= Trim(EgyebDlg.SepRest('and', S));  // a marad�k
     end; // else
end;

procedure TSzuresDlg.Tolt(vQuery : TADOQuery; formtag : string; FixSzuresNev, FixSzuresSQL: TStringList);
var
	str, mezostr, konvertaltstr, MezoNev, DisplayNev, FixSzuresNevElem: string;
  mezoszam	: integer;
  StringPar: TStringPar;

  function GetFixSzuresNev(str: string): string;
    var
       i: integer;
    begin
      i:= FixSzuresSQL.IndexOf(str);
      if i>=0 then
        Result:= FixSzuresNev[i]
      else Result:=''; // nem tal�lt
    end;

begin
	SzuresQuery := vQuery;		// Ennek a query-nek az elemeit kell sz�rni
	SzuresStr 	:= EgyebDlg.GetSzuroStr(formtag);
  	ListBox1.Items.Clear;
    StringPar:= SQLDarabol(SzuresStr);
    str:= StringPar.Elso;
    SzuresStr:= StringPar.Masodik;
  	while (str <> '') do begin
     	ListBox1.Items.Add(str);  // az eredeti forma
      FixSzuresNevElem:= GetFixSzuresNev(str);
      if FixSzuresNevElem <> '' then begin
          konvertaltstr:= fix_szures_jelzo + FixSzuresNevElem;  // ezt jelen�tj�k meg
          end
      else begin // nem fix sz�r�sr�l besz�l�nk
        konvertaltstr:=str;
        // egy operandus�, r�gi st�lus� SQL: mez�n�vvel kezd�dik
        mezostr := str;
        if Pos(' ', str) > 0 then begin
          mezostr 		:= copy(mezostr, 1, Pos(' ', str)-1);
            for mezoszam 	:= 0 to SzuresQuery.FieldCount -  1 do begin
              if SzuresQuery.Fields[mezoszam].FieldName = mezostr then begin
                konvertaltstr := SzuresQuery.Fields[mezoszam].DisplayName + copy(str, Pos(' ', str), 200);
                end;  // if
              end;  // for
          end;  // if
        // �j st�lus� SQL: mez�nevek z�r�jelek k�z�tt
        for mezoszam 	:= 0 to SzuresQuery.FieldCount -  1 do begin
           MezoNev:= SzuresQuery.Fields[mezoszam].FieldName;
           DisplayNev:= SzuresQuery.Fields[mezoszam].DisplayName;
           konvertaltstr:= StringReplace(konvertaltstr, '('+MezoNev+')', '('+DisplayNev+')', [rfReplaceAll]);
           end;  // for
        end;  // else
      ListBox3.Items.Add(konvertaltstr);
      StringPar:= SQLDarabol(SzuresStr);
      str:= StringPar.Elso;
      SzuresStr:= StringPar.Masodik;
  	  end;  // while

  	if ListBox1.Items.Count > 0 then begin
  		ListBox1.ItemIndex := 0;
  		ListBox3.ItemIndex := 0;
  	end else begin
  		ButtonTor.Hide;
      BitBtn1.Hide;
      BitBtn2.Hide;
  	end;
end;

{procedure TSzuresDlg.Tolt(vQuery : TADOQuery; formtag : string);
var
	str 		: string;
  mezostr  : string;
  mezoszam	: integer;
begin
	SzuresQuery := vQuery;		// Ennek a query-nek az elemeit kell sz�rni
	SzuresStr 	:= EgyebDlg.GetSzuroStr(formtag);
  	ListBox1.Items.Clear;;
  	while Pos('and',SzuresStr) > 0 do begin
  		str 			:= copy(SzuresStr,1,Pos('and',SzuresStr) - 1);
  		SzuresStr 	:= copy(SzuresStr,Pos('and',SzuresStr) +4, 9999);
     	ListBox1.Items.Add(str);
     	mezostr := str;
     	if Pos(' ', str) > 0 then begin
     		mezostr 		:= copy(mezostr, 1, Pos(' ', str)-1);
        	for mezoszam 	:= 0 to SzuresQuery.FieldCount -  1 do begin
        		if SzuresQuery.Fields[mezoszam].FieldName = mezostr then begin
					    mezostr := SzuresQuery.Fields[mezoszam].DisplayName + copy(str, Pos(' ', str), 200);
              end;  // if
            end;  // for
        end;  // if
     ListBox3.Items.Add(mezostr);
  	end;
  	if Length( SzuresStr ) > 0 then begin
  		str 			:= SzuresStr;
  		ListBox1.Items.Add(SzuresStr);
     	mezostr := SzuresStr;
     	if Pos(' ', SzuresStr) > 0 then begin
     		mezostr 		:= copy(mezostr, 1, Pos(' ', SzuresStr)-1);
        	for mezoszam 	:= 0 to SzuresQuery.FieldCount -  1 do begin
        		if SzuresQuery.Fields[mezoszam].FieldName = mezostr then begin
					mezostr := SzuresQuery.Fields[mezoszam].DisplayName + copy(SzuresStr, Pos(' ', str), 200);
           	end;
        	end;
     	end;
  		ListBox3.Items.Add(mezostr);
  	end;
  	if ListBox1.Items.Count > 0 then begin
  		ListBox1.ItemIndex := 0;
  		ListBox3.ItemIndex := 0;
  	end else begin
  		ButtonTor.Hide;
      BitBtn1.Hide;
      BitBtn2.Hide;
  	end;
end;
}

procedure TSzuresDlg.FormCreate(Sender: TObject);
begin
	kilepes := true;
end;


procedure TSzuresDlg.BitBtn2Click(Sender: TObject);
var
  SzuresSor, SzuresCimke: string;
begin
  SzuresCimke:= ListBox3.Items[Listbox3.Itemindex];
  SzuresSor:= ListBox1.Items[Listbox3.Itemindex];
  If copy(SzuresCimke,1,length(fix_szures_jelzo))=fix_szures_jelzo then begin
    NoticeKi('Ez a sor nem szerkeszthet�!');
    end
  else begin
    Application.CreateForm(TSzurbeDlg, SzurbeDlg);
    SzurbeDlg.querymas 	:= SzuresQuery;
    // SzurbeDlg.Tolt(ListBox1.Items[Listbox1.Itemindex]);
    SzurbeDlg.Tolt(SzuresSor);   // NP 2015-07-24
    SzurbeDlg.ShowModal;
    if SzurbeDlg.ret_str <> '' then begin
      // Van szur�si felt�tel
        ListBox1.Items[Listbox3.Itemindex] := SzurbeDlg.ret_str;
        ListBox3.Items[Listbox3.Itemindex] := SzurbeDlg.ret_str2;
    end;
    SzurbeDlg.Destroy;
    end;  // else
end;

procedure TSzuresDlg.BitBtn1Click(Sender: TObject);
begin
   if	NoticeKi( 'Val�ban t�rli az �sszes elemet? ', NOT_QUESTION ) = 0 then begin
      ListBox1.Items.Clear;
      ListBox3.Items.Clear;
     // ListBox1.Refresh;
      Application.ProcessMessages;
      BitBtn1.Hide;
      BitBtn2.Hide;
  		ButtonTor.Hide;
   end;
end;

end.
