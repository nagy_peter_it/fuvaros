unit FelrakoFm;

interface

uses
	SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
	StdCtrls, Forms, ExtCtrls, Dialogs, J_FOFORMUJ;

type
	TFelrakoFmDlg = class(TJ_FOFORMUJDLG)
	  procedure FormCreate(Sender: TObject);override;
	 procedure Adatlap(cim, kod : string);override;
	end;

var
	FelrakoFmDlg : TFelrakoFmDlg;

implementation

uses
	Egyeb, J_SQL, Felrakobe;

{$R *.DFM}

procedure TFelrakoFmDlg.FormCreate(Sender: TObject);
begin
	Inherited FormCreate(Sender);
  AddSzuromezoRovid('FF_FELNEV', '');
  AddSzuromezoRovid('FF_FELTEL', '');
  AddSzuromezoRovid('FF_ORSZA', '');
  AddSzuromezoRovid('FF_SZELE', '', '1');
  AddSzuromezoRovid('FF_HOSZA', '', '1');
	FelTolto('�j felrak�/lerak� felvitele ', 'Felrak�/lerak� adatok m�dos�t�sa ',
			'Felrak�k/lerak�k list�ja', 'FF_FEKOD', 'SELECT * FROM FELRAKO ','FELRAKO', QUERY_KOZOS_TAG );
	width	:= DLG_WIDTH;
	height	:= DLG_HEIGHT;
  vedettElemek.Add(EgyebDlg.Read_SZGrid('999', '759')); // A hazai telephely felrak� k�dja
end;

procedure TFelrakoFmDlg.Adatlap(cim, kod : string);
begin
	Application.CreateForm(TFelrakobeDlg, AlForm);
	Inherited Adatlap(cim, kod );
end;

end.

