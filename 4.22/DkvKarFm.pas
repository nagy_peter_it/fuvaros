unit DkvKarFm;

interface

uses
	SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
	StdCtrls, Forms, ExtCtrls, Dialogs, J_FOFORMUJ;

type
	TDkvKarFmDlg = class(TJ_FOFORMUJDLG)
	  procedure FormCreate(Sender: TObject);override;
	 procedure Adatlap(cim, kod : string);override;
	end;

var
	DkvKarFmDlg : TDkvKarFmDlg;

implementation

uses
	Egyeb, J_SQL, DkvKarbe;

{$R *.DFM}

procedure TDkvKarFmDlg.FormCreate(Sender: TObject);
begin
	Inherited FormCreate(Sender);
  AddSzuromezoRovid( 'DK_ARHIV', '' ); // m�r nem haszn�ljuk az adatt�bla param�tert
  AddSzuromezoRovid( 'DO_NAME', '' );
	FelTolto('�j DKV k�rtya felvitele ', 'DKV k�rtya adatok m�dos�t�sa ',
			'DKV k�rty�k list�ja', 'DK_KSZAM', 'SELECT * FROM DKVKARTYA LEFT OUTER JOIN DOLGOZO ON DK_TULAJ = DO_KOD ','DKVKARTYA', QUERY_KOZOS_TAG );
	nevmezo	:= 'DK_DKNEV';
	width	:= DLG_WIDTH;
	height	:= DLG_HEIGHT;
end;

procedure TDkvKarFmDlg.Adatlap(cim, kod : string);
begin
	Application.CreateForm(TDKVKarBeDlg, AlForm);
	Inherited Adatlap(cim, kod );
end;

end.

