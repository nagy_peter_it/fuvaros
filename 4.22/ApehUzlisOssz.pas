unit ApehUzlisOssz;

interface

uses
  WinTypes, WinProcs, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, QuickRpt, QRExpr, Qrctrls, ExtCtrls, printers, DB, DBTables, Egyeb, Forgalom, J_SQL,
  Grids, Kozos, ADODB ;

type
  TApehUzlisOsszDlg = class(TForm)
    Report: TQuickRep;
    QRBand1: TQRBand;
    QRLabel1: TQRLabel;
    QRLabel13: TQRLabel;
    QRLabel14: TQRLabel;
    Q6: TQRLabel;
    Q7: TQRLabel;
    Q2: TQRLabel;
    Q5: TQRLabel;
    Q4: TQRLabel;
    Q1: TQRLabel;
    QRGroup1: TQRGroup;
    QRBand3: TQRBand;
    QRLabel3: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel6: TQRLabel;
    QRLabel33: TQRLabel;
    QQ1: TQRLabel;
    QQ5: TQRLabel;
    QQ6: TQRLabel;
    QQ7: TQRLabel;
    QQ4: TQRLabel;
    QRLabel31: TQRLabel;
    QRSysData3: TQRSysData;
    QRSysData5: TQRSysData;
    QRLabel29: TQRLabel;
    Q8: TQRLabel;
    QRLabel2: TQRLabel;
    QRLabel7: TQRLabel;
    QRR2: TQRLabel;
    QRR3: TQRLabel;
    QRR4: TQRLabel;
    QRR5: TQRLabel;
    QRShape2: TQRShape;
    QRShape1: TQRShape;
    QRR1: TQRLabel;
    procedure FormCreate(Sender: TObject);
    procedure QRBand1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ReportBeforePrint(Sender: TCustomQuickRep;
      var PrintReport: Boolean);
    procedure ReportNeedData(Sender: TObject; var MoreData: Boolean);
    procedure	Tolt(sg : TStringGrid; idoszak : string);
    procedure QRBand3BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
  	private
   	grid			: TStringGrid;
       ossz1			: double;
       ossz2			: double;
       ossz3			: double;
       ossz4			: double;
       ossz5			: double;
  public
       rekordszam		: integer;
  end;

var
  ApehUzlisOsszDlg: TApehUzlisOsszDlg;

implementation

{$R *.DFM}

procedure TApehUzlisOsszDlg.FormCreate(Sender: TObject);
begin
   ossz1		:= 0;
   ossz2		:= 0;
   ossz3		:= 0;
   ossz4		:= 0;
   ossz5		:= 0;
end;

procedure TApehUzlisOsszDlg.QRBand1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
begin
	Q1.Caption		:= IntToStr(rekordSzam+1);
   Q2.Caption		:= Grid.Cells[0, rekordszam];
   Q4.Caption		:= Grid.Cells[1, rekordszam];
   Q5.Caption		:= Grid.Cells[2, rekordszam];
   Q6.Caption		:= Grid.Cells[3, rekordszam];
   Q7.Caption		:= Grid.Cells[4, rekordszam];
   if Grid.Cells[5, rekordszam] <> '-1' then begin
       Q8.Caption		:= Grid.Cells[5, rekordszam];
       ossz1			:= ossz1 + StringSzam(Q4.Caption);
       ossz2			:= ossz2 + StringSzam(Q5.Caption);
       ossz3			:= ossz3 + StringSzam(Q6.Caption);
       ossz4			:= ossz4 + StringSzam(Q7.Caption);
       ossz5			:= ossz4 + ossz1 * StringSzam(Q8.Caption);
   end else begin
       Q8.Caption		:= 'Hi�nyos!';
   end;
   inc(Rekordszam);
end;

procedure TApehUzlisOsszDlg.ReportBeforePrint(Sender: TCustomQuickRep;
      var PrintReport: Boolean);
begin
	RekordSzam	:= 0;
   ossz1		:= 0;
   ossz2		:= 0;
   ossz3		:= 0;
   ossz4		:= 0;
   ossz5		:= 0;
end;

procedure TApehUzlisOsszDlg.ReportNeedData(Sender: TObject;
  var MoreData: Boolean);
begin
	Moredata := Rekordszam < grid.RowCount;
end;

procedure	TApehUzlisOsszDlg.Tolt(sg : TStringGrid; idoszak : string);
begin
	grid				:= sg;
   QrLabel33.Caption	:= idoszak;
end;

procedure TApehUzlisOsszDlg.QRBand3BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
	QRR1.Caption	:= Format('%.0f', [ossz1]);
	QRR2.Caption	:= Format('%.0f', [ossz2]);
	QRR3.Caption	:= Format('%.2f', [ossz3]);
	QRR4.Caption	:= Format('%.2f', [ossz4]);
   if ossz1 = 0 then begin
		QRR5.Caption	:= '0.00';
   end else begin
		QRR5.Caption	:= Format('%.2f', [ossz5 / ossz1]);
   end;
end;

end.
