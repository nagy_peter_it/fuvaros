unit FUMEGLI;

interface

uses
  WinTypes, WinProcs, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, QuickRpt, QRExpr, Qrctrls, ExtCtrls, printers, DB, DBTables, Egyeb, J_SQL,
  ADODB, jpeg, grimgctrl,StrUtils, Vcl.Imaging.pngimage ;

type
  TFumegliDlg = class(TForm)
    Rep: TQuickRep;
    QRBand1: TQRBand;
    QRLabel37: TQRLabel;
    QRLabel69: TQRLabel;
	 QRLabel71: TQRLabel;
    QRShape33: TQRShape;
    QRLabel1: TQRLabel;
    QRLabel20: TQRLabel;
    QRLabel21: TQRLabel;
    QRLabel22: TQRLabel;
    Query1: TADOQuery;
	 Query3: TADOQuery;
    ChildBand1: TQRChildBand;
    ChildBand2: TQRChildBand;
    ChildBand3: TQRChildBand;
    Image2: TQRImage;
    QLAB1: TQRLabel;
    QLAB2: TQRLabel;
    QLAB3: TQRLabel;
    QLAB4: TQRLabel;
    QLAB5: TQRLabel;
    QRMemo1: TQRMemo;
    QRMemo2: TQRMemo;
    QRMemo3: TQRMemo;
    QRShape1: TQRShape;
    QRLabel12: TQRLabel;
    QRLabel13: TQRLabel;
    QRLabel31: TQRLabel;
    QRLabel32: TQRLabel;
    QL01: TQRLabel;
    QL02: TQRLabel;
    QL03: TQRLabel;
    QL05: TQRLabel;
    QL06: TQRLabel;
    QL07: TQRLabel;
    QL08: TQRLabel;
    QL09: TQRLabel;
    QL04: TQRLabel;
    QL11: TQRLabel;
    QL12: TQRLabel;
    QL13: TQRLabel;
    QL14: TQRLabel;
    QL15: TQRLabel;
    QL16: TQRLabel;
    QL17: TQRLabel;
    QL18: TQRLabel;
    QL19: TQRLabel;
    QL21: TQRLabel;
    Query22: TADOQuery;
    QL00: TQRLabel;
    QL09A: TQRLabel;
    QRLabel34: TQRLabel;
    QL19A: TQRLabel;
    QRLabel33: TQRLabel;
    QRLabel35: TQRLabel;
    QRLabel36: TQRLabel;
    QRImage2: TQRImage;
    QRLabel38: TQRLabel;
    QRBand4: TQRBand;
    Image1: TQRImage;
    QRBand3: TQRBand;
    QRImage1: TQRImage;
    QRLabel99: TQRLabel;
    QRLabel80: TQRLabel;
    QRShape8: TQRShape;
    QRLabel15: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel9: TQRLabel;
    QRLabel10: TQRLabel;
    QRLabel16: TQRLabel;
    QRLabel17: TQRLabel;
    QRLabel18: TQRLabel;
    QRLabel19: TQRLabel;
    QRLabel23: TQRLabel;
    QFej1: TQRLabel;
    QFEJ2: TQRLabel;
    QFEJ3: TQRLabel;
    QFEJ4: TQRLabel;
    QFEJ5: TQRLabel;
    QRLabel3: TQRLabel;
    QRLabel2: TQRLabel;
    QRGroup1: TQRGroup;
    QRMemoEKAER: TQRMemo;
    QRBand5: TQRBand;
    Image11: TQRImage;
    QRBand2: TQRBand;
    QRLabel14: TQRLabel;
    QRLabel24: TQRLabel;
    QRLabel25: TQRLabel;
    QRLabel26: TQRLabel;
    QRLabel27: TQRLabel;
    QRLabel28: TQRLabel;
    QRLabel29: TQRLabel;
    QRLabel30: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel6: TQRLabel;
    QRLabel7: TQRLabel;
    QRLabel11: TQRLabel;
    QRLabel39: TQRLabel;
    QRLabel40: TQRLabel;
    ChildBand4: TQRChildBand;
    Image3: TQRImage;
    QRImage4: TQRImage;
    QRLabel41: TQRLabel;
    QLEKAER: TQRLabel;
    QRLabel98: TQRLabel;
	 procedure FormCreate(Sender: TObject);
	 procedure	Tolt(szkod	: string);
	 procedure QRBand1BeforePrint(Sender: TQRCustomBand;
	   var PrintBand: Boolean);
	 procedure ChildBand2BeforePrint(Sender: TQRCustomBand;
	   var PrintBand: Boolean);
    procedure ChildBand3BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure RepBeforePrint(Sender: TCustomQuickRep;
      var PrintReport: Boolean);
    procedure QRBand3BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBand4BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBand5BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    function AddMemoLines(QRMemo: TQRMemo; Query: TADOQuery; FieldPrefix: string; Mettol, Meddig: integer): integer;
    function AddMemoLinesSzotar(QRMemo: TQRMemo; Fokod, NYELV: string; Mettol, Meddig: integer): integer;
    procedure Kep_betolt(Image: TQRImage; Filenev: string);
    procedure ChildBand4BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure CimkeBeallit(UNYELV: string);
    procedure InitCimkeArray;
  private
		// kellkep	: boolean;
    VanMelleklet1, VanMelleklet2: boolean;
		sorszam	: integer;
    LabelArray: array of TQRLabel;
  public
    STORNO: boolean;
    VALTOZAT: integer;
  end;

var
  FumegliDlg: TFumegliDlg;

implementation

uses Kozos;
{$R *.DFM}

procedure TFumegliDlg.FormCreate(Sender: TObject);
begin
	EgyebDlg.SetADOQueryDatabase(Query1);
	EgyebDlg.SetADOQueryDatabase(Query22);
	EgyebDlg.SetADOQueryDatabase(Query3);
  InitCimkeArray;
end;

procedure	TFumegliDlg.Tolt(szkod	: string);
const
   MEGB_Filenev = 'MEGB_';
var
   Sorok: integer;
   UNYELV, AJ_V_KOD, SzamStr: string;
begin
  UNYELV:='HUN';  // alap�rtelmezett
	{T�lt�s}
	if FileExists(EgyebDlg.ExePath+'FU_FEJ.BMP') then
  begin
		Image1.Picture.LoadFromFile(EgyebDlg.ExePath+'FU_FEJ.BMP');
    QRBand4.Height:= Image1.Height;
    Image1.Top:=0;
    Image1.Left:=Trunc( (QRBand4.Width-Image1.Width)/2)   ;
    QRLabel2.Enabled:=False;
	end
  else
  begin
    QRLabel2.Enabled:=True;
  end;

	if FileExists(EgyebDlg.ExePath+'FU_LAB.BMP') then
  begin
		Image11.Picture.LoadFromFile(EgyebDlg.ExePath+'FU_LAB.BMP');
    QRBand5.Enabled:=True;
	end
  else
  begin
    QRBand5.Enabled:=False;
  end;

//	Query_Run(Query2,'SELECT * FROM ALSEGED WHERE AS_JAKOD = '''+szkod+''' ORDER BY AS_SORSZ', true);
	Query_Run(Query22, 'SELECT DISTINCT JM_SORSZ, MS_TEFNEV, MS_TEFTEL, MS_TEFL1, MS_TEFL2, MS_TEFL3, MS_TEFL4, '+
		' MS_TEFOR, MS_TEFIR, MS_TENOR, MS_TENIR, MS_FEPAL, MS_LEPAL, MS_FSULY, MS_LSULY, '+
		' MS_TEFL5, MS_TEFCIM, MS_FELIDO, MS_FELARU, MS_TENNEV, MS_TENTEL, MS_TENYL1, MS_TENYL2, MS_TENYL3, MS_TENYL4, '+
//		' MS_TENYL5, MS_FELDAT, MS_LERDAT, MS_LERIDO, MS_TENCIM, MS_LDARU,MS_FDARU,MS_FELIDO2,MS_LERIDO2,MS_LERDAT2,MS_FELDAT2,MS_CSPAL,MS_FCPAL '+
//		' MS_TENYL5, MS_FELDAT, MS_LERDAT, MS_LERIDO, MS_TENCIM, MS_DARU,MS_FELIDO2,MS_LERIDO2,MS_LERDAT2,MS_FELDAT2,MS_CSPAL,MS_FCPAL,MS_MBKOD,MS_SORSZ,MS_TIPUS '+
//		' MS_TENYL5, MS_FELDAT, MS_LERDAT, MS_LERIDO, MS_TENCIM, MS_DARU,MS_FELIDO2,MS_LERIDO2,MS_LERDAT2,MS_FELDAT2,MS_CSPAL,MS_FCPAL,MS_MBKOD,MS_SORSZ '+
		' MS_TENYL5, MS_FELDAT, MS_LERDAT, MS_LERIDO, MS_TENCIM,MS_FELIDO2,MS_LERIDO2,MS_LERDAT2,MS_FELDAT2,MS_CSPAL,MS_FCPAL,MS_MBKOD,MS_SORSZ, '+
    ' MS_EKAER '+
		' FROM JARATMEGBIZAS, MEGSEGED WHERE MS_MBKOD = JM_MBKOD AND MS_SORSZ = JM_ORIGS AND JM_JAKOD = '''+szkod+''' '+
		' AND MS_FAJKO = 0 ' + 	// csak a norm�l szakaszok jelenjenek itt meg
		' ORDER BY JM_SORSZ' );

	Query_Run(Query1,'SELECT * FROM ALJARAT WHERE AJ_JAKOD = '''+szkod+''' ', true);
	if Query1.RecordCount > 0 then begin
    AJ_V_KOD:=Query1.FieldByName('AJ_V_KOD').AsString;
    UNYELV:=Query_Select('VEVO', 'V_KOD', AJ_V_KOD, 'VE_UNYELV');      // �gyint�z�si nyelv
    if UNYELV='' then UNYELV:=Query_Select('VEVO', 'V_KOD', AJ_V_KOD, 'VE_NYELV');  // a sz�ml�z�si nyelv
    if UNYELV='' then UNYELV:='HUN';  // ha valami�rt egyiket sem tal�lja, akkor HUF

		QrLabel2.Caption	:= EgyebDlg.Read_SZGrid('CEG', '101');
		QrLabel23.Caption	:= Query1.FieldByName('AJ_ALNEV').AsString;
		QrLabel18.Caption	:= Query1.FieldByName('AJ_ALCIM').AsString;
		QrLabel16.Caption	:= Query1.FieldByName('AJ_ALTEL').AsString;
		QrLabel9.Caption	:= Query1.FieldByName('AJ_ALFAX').AsString;
		QrLabel3.Caption	:= Query1.FieldByName('AJ_POMEG').AsString;
		{J�ratsz�m kider�t�se}
		QrLabel80.Caption	:= '';
		Query_Run(Query3,'SELECT * FROM JARAT WHERE JA_KOD = '''+szkod+''' ', true);
		if Query3.RecordCount > 0 then begin
			QrLabel80.Caption	:= GetJaratszam(Query3.FieldByName('JA_KOD').AsString);
		end;
		QrLabel24.Caption	:= Query1.FieldByName('AJ_ALREN').AsString;
		QrLabel40.Caption	:= Query1.FieldByName('AJ_POTOS').AsString;
		QrLabel26.Caption	:= Query1.FieldByName('AJ_ALTIP').AsString;
		QrLabel28.Caption	:= Query1.FieldByName('AJ_FUDIJ').AsString + ' ' +
							   Query1.FieldByName('AJ_FUVAL').AsString;
		QrLabel4.Caption	:= Query1.FieldByName('AJ_FUMEG').AsString;
		QrLabel30.Caption	:= Query1.FieldByName('AJ_MEMO01').AsString;
		QrLabel5.Caption	:= Query1.FieldByName('AJ_MEMO02').AsString;
		QrLabel6.Caption	:= Query1.FieldByName('AJ_MEMO03').AsString;
		QrLabel7.Caption	:= Query1.FieldByName('AJ_MEMO04').AsString;
		QrLabel11.Caption	:= Query1.FieldByName('AJ_MEMO05').AsString;
		{Az oldal �sszeh�z�sa, ha vannak �res sorok}
		if QrLabel11.Caption = '' then begin
			QrBand2.Height		:= QrLabel11.Top;
			QRLabel11.Enabled	:= false;
			if QrLabel7.Caption = '' then begin
				QrBand2.Height		:= QrLabel7.Top;
				QRLabel7.Enabled	:= false;
				if QrLabel6.Caption = '' then begin
					QrBand2.Height		:= QrLabel6.Top;
					QRLabel6.Enabled	:= false;
				end;
			end;
		end;
		QFej1.Caption	:= Query1.FieldByName('AJ_FEJS1').AsString;
		QFej2.Caption	:= Query1.FieldByName('AJ_FEJS2').AsString;
		QFej3.Caption	:= Query1.FieldByName('AJ_FEJS3').AsString;
		QFej4.Caption	:= Query1.FieldByName('AJ_FEJS4').AsString;
		QFej5.Caption	:= Query1.FieldByName('AJ_FEJS5').AsString;
		QLab1.Caption	:= Query1.FieldByName('AJ_LABS1').AsString;
		QLab2.Caption	:= Query1.FieldByName('AJ_LABS2').AsString;
		QLab3.Caption	:= Query1.FieldByName('AJ_LABS3').AsString;
		QLab4.Caption	:= Query1.FieldByName('AJ_LABS4').AsString;
		QLab5.Caption	:= Query1.FieldByName('AJ_LABS5').AsString;

    Sorok:= AddMemoLines(QRMemo1, Query1, 'AJ_MEMO', 11, 16);
    QRMemo1.Height:=Sorok*14;      // Magass�g: soronk�nt 14
    QRMemo2.Top:= QRMemo1.Top + QRMemo1.Height + 8;

    Sorok:= AddMemoLines(QRMemo2, Query1, 'AJ_MEMO', 21, 23);
    QRMemo2.Height:=Sorok*19;      // Magass�g: soronk�nt 19
    QRShape1.Top:= QRMemo2.Top + QRMemo2.Height + 4;
    QRMemo3.Top:= QRMemo2.Top + QRMemo2.Height + 8;

    Sorok:= AddMemoLines(QRMemo3, Query1, 'AJ_MEMO', 31, 33);
    QRMemo3.Height:=Sorok*14;      // Magass�g: soronk�nt 14

    QLAB1.Top:= QRMemo2.Top + QRMemo2.Height + 4;
    QLAB2.Top:= QLAB1.Top+16;
    QLAB3.Top:= QLAB2.Top+16;
    QLAB4.Top:= QLAB3.Top+16;
    QLAB5.Top:= QLAB4.Top+16;

	end;
  // EKAER sz�veg
  QRMemoEKAER.Top:= QLAB5.Top+16+8; // egy kis sz�net is legyen
  Sorok:= AddMemoLinesSzotar(QRMemoEKAER, '371', UNYELV, 1, 12);
  QRMemoEKAER.Height:=Sorok*16;      // Magass�g: soronk�nt 16
  ChildBand2.Height:=QRMemoEKAER.Top+ QRMemoEKAER.Height+ 4;
	// A mell�klet bet�lt�se
  Kep_betolt(Image2, MEGB_Filenev+UNYELV+'1');
  Kep_betolt(Image3, MEGB_Filenev+UNYELV+'2');
//   if (Image2.Visible=False) and (Image3.Visible=False) then kellkep	:= false;  // ha egyik sem l�tszik
  CimkeBeallit(UNYELV);  // magyar vagy idegen nyelv� feliratok be�ll�t�sa
  if (not STORNO) and (VALTOZAT>0) then begin
    if UNYELV='ENG' then begin
        SzamStr:= AngolSorszam(VALTOZAT);
        end
    else begin
        SzamStr:= IntToStr(VALTOZAT)+'.';
        end; // else
    QRLabel99.Caption:=QRLabel99.Caption+'  ('+SzamStr+' '+QRLabel98.Caption+')';  // 98: rejtett label, az automata felt�lt�s miatt lett �gy
    end;  // if


  if Image2.Visible then begin
      VanMelleklet1:= true;
      Childband3.Height:=Image2.Top + Image2.Height + 2;
      end
  else VanMelleklet1:=False;  // a ChildBand enged�lyez�shez
  if Image3.Visible then begin
      VanMelleklet2:= true;
      Childband4.Height:=Image3.Top + Image3.Height + 2;
      end
  else VanMelleklet2:=False;  // a ChildBand enged�lyez�shez
end;

procedure TFumegliDlg.CimkeBeallit(UNYELV: string);
var
  S, ID, Cimke: string;
  IDI: integer;
begin
  S:= 'SELECT SZ_ALKOD, SZ_MENEV FROM SZOTAR WHERE SZ_FOKOD = ''373'' ';
  S:=S+ ' AND substring(SZ_ALKOD,1,3)='''+UNYELV+'''';
  Query_Run(Query3, S, true);
  with Query3 do begin
    while not Eof do begin
      ID:= FieldByName('SZ_ALKOD').AsString;
      if (length(ID)>=4) and (copy(ID, 4,1)<>'M') then begin  // a "mail" param�terekkel se foglalkozzunk
        ID:= copy(ID, 4, length(ID)-3);  // 'HUN15' --> '15'
        Cimke:= FieldByName('SZ_MENEV').AsString;
        if JoEgesz(ID) then begin
          IDI:= StrToInt(ID);
          if LabelArray[IDI]<> nil then
             LabelArray[IDI].Caption:= Cimke;
          {try  // ne lehessen AccessViolation a hivatkoz�skor!
             LabelArray[IDI].Caption:= Cimke;
          except
            end;  // try-except
            }
          end; // if JoEgesz
        end;  // if Length OK
      Next;
      end;  // while
    end;  // with
end;

procedure TFumegliDlg.InitCimkeArray;
const
   hossz = 100;
var
   i: integer;
begin
   // jobb ha ford�t�si id�ben kider�l, ha valamelyik nem l�tezik.
   SetLength(LabelArray, hossz);
   LabelArray[1]:=QRLabel1;
   LabelArray[10]:=QRLabel10;
   LabelArray[12]:=QRLabel12;
   LabelArray[14]:=QRLabel14;
   LabelArray[15]:=QRLabel15;
   LabelArray[17]:=QRLabel17;
   LabelArray[19]:=QRLabel19;
   LabelArray[20]:=QRLabel20;
   LabelArray[21]:=QRLabel21;
   LabelArray[22]:=QRLabel22;
   LabelArray[25]:=QRLabel25;
   LabelArray[27]:=QRLabel27;
   LabelArray[29]:=QRLabel29;
   LabelArray[3]:=QRLabel3;
   LabelArray[33]:=QRLabel33;
   LabelArray[34]:=QRLabel34;
   LabelArray[35]:=QRLabel35;
   LabelArray[36]:=QRLabel36;
   LabelArray[37]:=QRLabel37;
   LabelArray[38]:=QRLabel38;
   LabelArray[39]:=QRLabel39;
   LabelArray[8]:=QRLabel8;
   LabelArray[98]:=QRLabel98;
   LabelArray[99]:=QRLabel99;
end;

procedure TFumegliDlg.Kep_betolt(Image: TQRImage; Filenev: string);
var
  IsLoaded: boolean;
begin
  IsLoaded:=False;
	if FileExists (EgyebDlg.ExePath + Filenev + '.png') then begin
		Image.Picture.LoadFromFile(EgyebDlg.ExePath + Filenev + '.png');
    IsLoaded:=True;
	end else begin
  	if FileExists (EgyebDlg.ExePath + Filenev + '.bmp') then begin
    	Image.Picture.LoadFromFile(EgyebDlg.ExePath + Filenev + '.bmp');
      IsLoaded:=True;
		  end;
    end;
  if IsLoaded then Image.Visible:=True else Image.Visible:=False;
end;

function TFumegliDlg.AddMemoLines(QRMemo: TQRMemo; Query: TADOQuery; FieldPrefix: string; Mettol, Meddig: integer): integer;
var
   i, sorok: integer;
   S: string;
begin
  Sorok:=0;
	QRMemo.Lines.Clear;
  for i:=Mettol to Meddig do begin
     S:=Query.FieldByName(FieldPrefix+IntToStr(i)).AsString;
     if trim(S)<>'' then begin
        QRMemo.Lines.Add(S);
        Inc(Sorok);
        end;  // if
     end;  // for
  Result:= sorok;
end;

function TFumegliDlg.AddMemoLinesSzotar(QRMemo: TQRMemo; Fokod, NYELV: string; Mettol, Meddig: integer): integer;
var
   i, sorok: integer;
   S: string;
begin
  Sorok:=0;
	QRMemo.Lines.Clear;
  for i:=Mettol to Meddig do begin
     S:=EgyebDlg.Read_SZGrid(Fokod, NYELV+IntToStr(i));
     if trim(S)<>'' then begin
        QRMemo.Lines.Add(S);
        Inc(Sorok);
        end;  // if
     end;  // for
  Result:= sorok;
end;

procedure TFumegliDlg.QRBand1BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
	kulonbseg	: integer;
  	szorzo		: integer;
begin
	// Az �rt�kek felt�lt�se
	QL00.Caption  	:= IntToStr(sorszam);
	Inc(sorszam);
	QL01.Caption  	:= Query22.FieldByName('MS_TEFNEV').AsString;
	QL02.Caption  	:= copy ( Query22.FieldByName('MS_TEFOR').AsString+'-'+Query22.FieldByName('MS_TEFIR').AsString+' '+Query22.FieldByName('MS_TEFTEL').AsString, 1, 30 );
	QL03.Caption  	:= Query22.FieldByName('MS_TEFCIM').AsString;
	QL04.Caption  	:= Query22.FieldByName('MS_TEFL1').AsString;
	QL05.Caption  	:= Query22.FieldByName('MS_TEFL2').AsString;
	QL06.Caption  	:= Query22.FieldByName('MS_TEFL3').AsString;
	QL07.Caption  	:= Query22.FieldByName('MS_TEFL4').AsString;
	QL08.Caption  	:= Query22.FieldByName('MS_TEFL5').AsString;
  if Query22.FieldByName('MS_FELIDO2').AsString<>'' then
  	QL09.Caption  	:= Query22.FieldByName('MS_FELDAT').AsString+' '+Query22.FieldByName('MS_FELIDO').AsString+'-'+Query22.FieldByName('MS_FELDAT2').AsString+' '+Query22.FieldByName('MS_FELIDO2').AsString
  else
  	QL09.Caption  	:= Query22.FieldByName('MS_FELDAT').AsString+' '+Query22.FieldByName('MS_FELIDO').AsString;
	//QL09A.Caption  	:= Query22.FieldByName('MS_FEPAL').AsString+' pal.  '+Query22.FieldByName('MS_FSULY').AsString+' kg';
	QL09A.Caption  	:= Query22.FieldByName('MS_FEPAL').AsString+IfThen(Query22.FieldByName('MS_CSPAL').AsInteger=1,'  ('+Query22.FieldByName('MS_FCPAL').AsString+'db CSERE) pal.  ',' pal.  ')+Query22.FieldByName('MS_FSULY').AsString+' kg';
	QL11.Caption  	:= Query22.FieldByName('MS_TENNEV').AsString;
	QL12.Caption  	:= copy ( Query22.FieldByName('MS_TENOR').AsString+'-'+Query22.FieldByName('MS_TENIR').AsString+' '+Query22.FieldByName('MS_TENTEL').AsString, 1, 30 );
	QL13.Caption  	:= Query22.FieldByName('MS_TENCIM').AsString;
	QL14.Caption  	:= Query22.FieldByName('MS_TENYL1').AsString;
	QL15.Caption  	:= Query22.FieldByName('MS_TENYL2').AsString;
	QL16.Caption  	:= Query22.FieldByName('MS_TENYL3').AsString;
	QL17.Caption  	:= Query22.FieldByName('MS_TENYL4').AsString;
	QL18.Caption  	:= Query22.FieldByName('MS_TENYL5').AsString;
  if Query22.FieldByName('MS_LERIDO2').AsString<>'' then
  	QL19.Caption  	:= Query22.FieldByName('MS_LERDAT').AsString+' '+Query22.FieldByName('MS_LERIDO').AsString+'-'+Query22.FieldByName('MS_LERDAT2').AsString+' '+Query22.FieldByName('MS_LERIDO2').AsString
  else
  	QL19.Caption  	:= Query22.FieldByName('MS_LERDAT').AsString+' '+Query22.FieldByName('MS_LERIDO').AsString;
	QL19A.Caption  	:= Query22.FieldByName('MS_LEPAL').AsString+' pal.  '+Query22.FieldByName('MS_LSULY').AsString+' kg';

	QL21.Caption  	:= Query22.FieldByName('MS_FELARU').AsString;
  QLEKAER.Caption := Query22.FieldByName('MS_EKAER').AsString;

  if Query22.FieldByName('MS_MBKOD').AsString <> '' then begin
  	Query_Run(Query3,'SELECT MS_DARU FROM MEGSEGED WHERE MS_MBKOD ='+Query22.FieldByName('MS_MBKOD').AsString+' and MS_SORSZ='+Query22.FieldByName('MS_SORSZ').AsString+' and MS_TIPUS=0', true);
    QRLabel36.Enabled:=(Query3.FieldByName('MS_DARU').AsInteger=1);
	  Query_Run(Query3,'SELECT MS_DARU FROM MEGSEGED WHERE MS_MBKOD ='+Query22.FieldByName('MS_MBKOD').AsString+' and MS_SORSZ='+Query22.FieldByName('MS_SORSZ').AsString+' and MS_TIPUS=1', true);
    QRLabel35.Enabled:=(Query3.FieldByName('MS_DARU').AsInteger=1);
    end
  else begin
    QRLabel36.Enabled:= False;
    QRLabel35.Enabled:= False;
    end;

  //QRLabel35.Enabled:=(Query22.FieldByName('MS_LDARU').Value=1);
  //QRLabel36.Enabled:=(Query22.FieldByName('MS_FDARU').Value=1);

	// Az �res sorok kiiktat�sa
	kulonbseg		:= QL02.Top - QL01.Top;
	szorzo			:= 0;
	if  ( ( Query22.FieldByname('MS_TEFL5').AsString = '' ) and
		( Query22.FieldByname('MS_TENYL5').AsString = '' ) ) then begin
		szorzo	:= 1;
		if  ( ( Query22.FieldByname('MS_TEFL4').AsString = '' ) and
			( Query22.FieldByname('MS_TENYL4').AsString = '' ) ) then begin
			szorzo	:= 2;
			if  ( ( Query22.FieldByname('MS_TEFL3').AsString = '' ) and
				( Query22.FieldByname('MS_TENYL3').AsString = '' ) ) then begin
				szorzo	:= 3;
				if  ( ( Query22.FieldByname('MS_TEFL2').AsString = '' ) and
					( Query22.FieldByname('MS_TENYL2').AsString = '' ) ) then begin
					szorzo	:= 4;
					if ( 	( Query22.FieldByname('MS_TEFL1').AsString = '' ) and
						( Query22.FieldByname('MS_TENYL1').AsString = '' ) ) then begin
						szorzo	:= 5;
					end;
				end;
			end;
		end;
	end;

	QRLabel1.Top	:= QL01.Top + (8-szorzo) * kulonbseg;
	QrLabel22.Top	:= QL01.Top + (8-szorzo) * kulonbseg;
	QL09.Top		  := QRLabel1.Top;
	QL19.Top		  := QRLabel1.Top;

	QrLabel34.Top	:= QL01.Top + (9-szorzo) * kulonbseg;
	QL09A.Top		  := QrLabel34.Top;
  QrLabel33.Top	:= QrLabel34.Top;
  QrLabel35.Top	:= QrLabel33.Top;
  QrLabel36.Top	:= QrLabel33.Top;
	QL19A.Top		  := QrLabel34.Top;

	QrLabel20.Top	:= QL01.Top + (10-szorzo) * kulonbseg;
	QL21.Top		  := QrLabel20.Top;

  if QLEKAER.Caption <> '' then begin
    QRLabel41.Enabled	:= True;
	  QLEKAER.Enabled		:= True;
	  QRLabel41.Top	:= QrLabel20.Top + 16;  // EKAER sor
	  QLEKAER.Top		:= QRLabel41.Top;
  	QrShape33.Top	:= QRLabel41.Top + 16;
    end
  else begin   // nincs EKAER adat: feljebb cs�szik az elv�laszt� vonal
	  QRLabel41.Enabled	:= False;
	  QLEKAER.Enabled		:= False;
  	QrShape33.Top	:= QrLabel20.Top + 16;
    end;
  QRBand1.Height:= QrShape33.Top + 5;
end;

procedure TFumegliDlg.ChildBand2BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  QRImage2.Enabled:=STORNO;
	// Ide j�n a fizet�si felt�telek be�r�sa, ha van!!!
	PrintBand	:= true;

  // ezt �t kell szervezni, m�s is van ezen a band-en
	{ if ( Query1.FieldByName('AJ_FIZU1').AsString + Query1.FieldByName('AJ_FIZU2').AsString +
		 Query1.FieldByName('AJ_FIZU3').AsString = '' ) then begin
		PrintBand	:= false;
		Exit;
	end;}

  // QRLabel41.Caption := EgyebDlg.Read_SZGrid('XXX', 'XXXX');  sajnos nem f�r a sz�t�rba
	// Az �rt�kek ki�r�sa
	QrLabel13.Caption	:= Query1.FieldByName('AJ_FIZU1').AsString +
		Format('%.0f', [StringSzam(Query1.FieldByName('AJ_FUDIJ').AsString)*StringSzam(Query1.FieldByName('AJ_FISZ1').AsString) / 100])	+
		' ' + Query1.FieldByName('AJ_FUVAL').AsString;
	QrLabel31.Caption	:= Query1.FieldByName('AJ_FIZU2').AsString +
		Format('%.0f', [StringSzam(Query1.FieldByName('AJ_FUDIJ').AsString)*StringSzam(Query1.FieldByName('AJ_FISZ2').AsString) / 100])	+
		' ' + Query1.FieldByName('AJ_FUVAL').AsString;
	QrLabel32.Caption	:= Query1.FieldByName('AJ_FIZU3').AsString +
		Format('%.0f', [StringSzam(Query1.FieldByName('AJ_FUDIJ').AsString)*StringSzam(Query1.FieldByName('AJ_FISZ3').AsString) / 100])	+
		' ' + Query1.FieldByName('AJ_FUVAL').AsString;
	// A band magass�g be�ll�t�sa
	if Query1.FieldByName('AJ_FIZU3').AsString = '' then begin
		ChildBand1.Height	:= QrLabel32.Top; //ChildBand1.Height - QrLabel32.Height;
		QrLabel32.Caption	:= '';
		QrLabel32.Enabled	:= false;
		if Query1.FieldByName('AJ_FIZU2').AsString = '' then begin
			ChildBand1.Height	:= QrLabel31.Top; //ChildBand1.Height - QrLabel32.Height;
			QrLabel31.Caption	:= '';
			QrLabel31.Enabled	:= false;
		end;
	end;
end;

procedure TFumegliDlg.ChildBand3BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
	PrintBand	:= VanMelleklet1;
end;

procedure TFumegliDlg.ChildBand4BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  PrintBand	:= VanMelleklet2;
end;

procedure TFumegliDlg.RepBeforePrint(Sender: TCustomQuickRep;
  var PrintReport: Boolean);
begin
	sorszam	:= 1;
end;

procedure TFumegliDlg.QRBand3BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  QRImage1.Enabled:=STORNO;
end;

procedure TFumegliDlg.QRBand4BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
//  PrintBand:= Rep.PageNumber=1;
end;

procedure TFumegliDlg.QRBand5BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
//  PrintBand:= Rep.PageNumber=1;
end;

end.
