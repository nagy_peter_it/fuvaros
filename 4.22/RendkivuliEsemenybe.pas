unit RendkivuliEsemenybe;

interface

uses
	SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
	Forms, Dialogs, StdCtrls, Buttons, Mask, DB, DBTables, J_ALFORM, ADODB,
  Vcl.CheckLst;

type
	TRendkivuliEsemenybeDlg = class(TJ_AlformDlg)
	BitElkuld: TBitBtn;
	BitKilep: TBitBtn;
    Label3: TLabel;
    Label2: TLabel;
    M1: TMaskEdit;
	 Query1: TADOQuery;
	 Query2: TADOQuery;
	 Label9: TLabel;
    ebDATUM: TMaskEdit;
    BitBtn8: TBitBtn;
    ebESEMENY: TMemo;
    Label8: TLabel;
    cbTerulet: TComboBox;
    Label13: TLabel;
    ebRENDSZAM: TMaskEdit;
    BitBtn4: TBitBtn;
    BitBtn7: TBitBtn;
    ebIDOPT: TMaskEdit;
    Label1: TLabel;
    cbStatusz: TComboBox;
    CLIST: TCheckListBox;
    Label79: TLabel;
    ebKoltseg: TMaskEdit;
    Label4: TLabel;
    Label5: TLabel;
    M33: TMaskEdit;
    M34: TMaskEdit;
    BitBtn11: TBitBtn;
    BitBtn12: TBitBtn;
	 procedure BitKilepClick(Sender: TObject);
	 procedure FormCreate(Sender: TObject);
	 procedure BitElkuldClick(Sender: TObject);
	 procedure MaskEditClick(Sender: TObject);
	 procedure Tolto(cim, kod : string); override;
    procedure BitBtn8Click(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure BitBtn7Click(Sender: TObject);
    procedure BitBtn4Click(Sender: TObject);
    procedure ebIDOPTExit(Sender: TObject);
    procedure BitBtn11Click(Sender: TObject);
    procedure BitBtn12Click(Sender: TObject);
	private
    lista_terulet, lista_statusz: TStringList;
	public
	end;

var
	RendkivuliEsemenybeDlg: TRendkivuliEsemenybeDlg;

implementation

uses
 Egyeb, J_SQL, Kozos,  J_VALASZTO;

{$R *.DFM}

procedure TRendkivuliEsemenybeDlg.BitKilepClick(Sender: TObject);
begin
	ret_kod := '';
	Close;
end;

procedure TRendkivuliEsemenybeDlg.ebIDOPTExit(Sender: TObject);
begin
   IdoExit(Sender, false);
end;

procedure TRendkivuliEsemenybeDlg.FormCreate(Sender: TObject);
begin
	EgyebDlg.SeTADOQueryDatabase(Query1);
	EgyebDlg.SeTADOQueryDatabase(Query2);
  lista_terulet:= TStringList.Create;
  lista_statusz:= TStringList.Create;
  SzotarTolt(cbTERULET, '148' , lista_terulet, false, false );
  CLIST.Clear;
	CLIST.Items:= cbTERULET.Items;
  SzotarTolt(cbStatusz, '379' , lista_statusz, false, false );
  cbStatusz.ItemIndex:=0;
	ret_kod:= '';
end;

procedure TRendkivuliEsemenybeDlg.FormDestroy(Sender: TObject);
begin
  if lista_terulet <> nil then lista_terulet.Free;
  if lista_statusz <> nil then lista_statusz.Free;
end;

procedure TRendkivuliEsemenybeDlg.Tolto(cim, kod : string);
begin
  if megtekint then BitElkuld.Enabled:= False;
	SetMaskEdits([M1, ebRENDSZAM]);
	Caption 		:= cim;
	ret_kod  := kod;
	M1.Text		:= ret_kod;
	if ret_kod <> '' then begin
		if Query_Run (Query1, 'SELECT * FROM RENDKIVULI_ESEMENY WHERE RK_ID = '+ret_kod ,true) then begin
      ComboSet (cbTerulet, Trim(Query1.FieldByName('RK_TERULETKOD').AsString), lista_terulet);
      ComboSet (cbStatusz, Trim(Query1.FieldByName('RK_STATUSZID').AsString), lista_statusz);
			ebRENDSZAM.Text		:= Query1.FieldByname('RK_RENDSZAM').AsString;
			ebDATUM.Text	:= Query1.FieldByname('RK_DATUM').AsString;
      M33.Text:= Query1.FieldByname('RK_INTEZOKOD').AsString;
      M34.Text:= Query_Select('DOLGOZO', 'DO_KOD',  M33.Text, 'DO_NAME');
      ebKoltseg.Text:= Query1.FieldByname('RK_KOLTSEG').AsString;
			ebIDOPT.Text		:= Query1.FieldByname('RK_IDOPT').AsString;
 			ebESEMENY.Text	:= Query1.FieldByname('RK_ESEMENY').AsString;
      CLIST.Visible:= False; // itt m�r nem m�dos�thatjuk
      Label79.Visible:= False;
		end; // if
	end;  // if
end;

procedure TRendkivuliEsemenybeDlg.BitElkuldClick(Sender: TObject);
var
  UjRKID, i: integer;
  TovabbiTeruletek: string;
begin

	if cbTerulet.Text = '' then begin
		NoticeKi('Ter�let megad�sa k�telez�!');
		cbTerulet.Setfocus;
		Exit;
	  end;
	if ebEsemeny.Text = '' then begin
		NoticeKi('Esem�ny r�szletek megad�sa k�telez�!');
		ebEsemeny.Setfocus;
		Exit;
	  end;
	if ebDatum.Text = '' then begin
		NoticeKi('D�tum megad�sa k�telez�!');
		ebDatum.Setfocus;
		Exit;
	  end;

	if ret_kod = '' then begin  // �j adatsor
     TovabbiTeruletek:= '';
     for i := 0 to CLIST.Items.Count - 1 do begin
        if CLIST.Checked[i] then
         TovabbiTeruletek:= Felsorolashoz(TovabbiTeruletek, lista_terulet[i], ',', False);
        end;  // for
    UjRKID:= Query_Insert_Identity (Query1, 'RENDKIVULI_ESEMENY',
      [
      'RK_TERULETKOD',  ''''+lista_terulet[cbTerulet.ItemIndex]+'''',
      'RK_STATUSZID',  ''''+lista_statusz[cbStatusz.ItemIndex]+'''',
      'RK_RENDSZAM', ''''+ebRENDSZAM.Text+'''',
      'RK_DATUM', ''''+ebDATUM.Text+'''',
      'RK_IDOPT', ''''+ebIDOPT.Text+'''',
      'RK_ESEMENY', ''''+ebESEMENY.Text+'''',
      'RK_FEKOD', ''''+EgyebDlg.user_code+'''',
      'RK_KOLTSEG', SqlSzamString(StringSzam(ebKoltseg.Text),12,2),
      'RK_INTEZOKOD',''''+M33.Text+'''',
      'RK_TOVABBITERULET',''''+TovabbiTeruletek+'''',
      'RK_MODUSER', ''''+EgyebDlg.user_code+'''',
      'RK_MODTIME', ''''+formatdatetime('yyyy.mm.dd. HH:mm:ss', Now)+''''
      ], 'RK_ID' );
     if UjRKID>=0 then begin  // ha nem volt sikeres, �gyis kap SQL hiba ablakot
      ret_kod:= IntToStr(UjRKID);
      M1.Text:= ret_kod;
      end;
     end
  else begin
     Query_Update (Query1, 'RENDKIVULI_ESEMENY',
      [
      'RK_TERULETKOD',  ''''+lista_terulet[cbTerulet.ItemIndex]+'''',
      'RK_STATUSZID',  ''''+lista_statusz[cbStatusz.ItemIndex]+'''',
      'RK_RENDSZAM', ''''+ebRENDSZAM.Text+'''',
      'RK_DATUM', ''''+ebDATUM.Text+'''',
      'RK_IDOPT', ''''+ebIDOPT.Text+'''',
      'RK_ESEMENY', ''''+ebESEMENY.Text+'''',
      // 'RK_FEKOD', ''''+EgyebDlg.user_code+'''',  -- itt az eredeti r�gz�t� maradjon
      'RK_KOLTSEG', SqlSzamString(StringSzam(ebKoltseg.Text),12,2),
      'RK_INTEZOKOD',''''+M33.Text+'''',
      'RK_MODUSER', ''''+EgyebDlg.user_code+'''',
      'RK_MODTIME', ''''+formatdatetime('yyyy.mm.dd. HH:mm:ss', Now)+''''
      ], ' WHERE RK_ID='+ret_kod);
      end;  // else

	Close;

end;

procedure TRendkivuliEsemenybeDlg.MaskEditClick(Sender: TObject);
begin
	with Sender as TMaskEdit do begin
  	SelectAll;
  end;
end;


procedure TRendkivuliEsemenybeDlg.BitBtn11Click(Sender: TObject);
begin
	DolgozoValaszto(M33, M34);
end;

procedure TRendkivuliEsemenybeDlg.BitBtn12Click(Sender: TObject);
begin
	M33.Text	:= '';
	M34.Text	:= '';
end;

procedure TRendkivuliEsemenybeDlg.BitBtn4Click(Sender: TObject);
begin
  GepkocsiValaszto(ebRENDSZAM);
end;

procedure TRendkivuliEsemenybeDlg.BitBtn7Click(Sender: TObject);
begin
  ebRENDSZAM.Text	:= '';
end;

procedure TRendkivuliEsemenybeDlg.BitBtn8Click(Sender: TObject);
begin
	EgyebDlg.Calendarbe(ebDATUM);
end;

end.




