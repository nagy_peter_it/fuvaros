unit Termekbe;

interface

uses
	SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
	Forms, Dialogs, StdCtrls, Buttons, Mask, DB, DBTables, J_ALFORM, ADODB;

type
	TTermekbeDlg = class(TJ_AlformDlg)
	BitElkuld: TBitBtn;
	BitKilep: TBitBtn;
	Label1: TLabel;
	Label2: TLabel;
	Label3: TLabel;
	Label4: TLabel;
	Label6: TLabel;
	Label7: TLabel;
	MaskEdit1: TMaskEdit;
	MaskEdit2: TMaskEdit;
	MaskEdit3: TMaskEdit;
	MaskEdit4: TMaskEdit;
	MaskEdit7: TMaskEdit;
	Label8: TLabel;
	Label9: TLabel;
	MaskEdit5: TMaskEdit;
	Label12: TLabel;
  MaskEdit10: TMaskEdit;
    AFACombo: TComboBox;
    Query1: TADOQuery;
    Label5: TLabel;
    ComboBox1: TComboBox;
    CheckBox1: TCheckBox;
    Label10: TLabel;
	procedure BitKilepClick(Sender: TObject);
	procedure FormCreate(Sender: TObject);
	procedure Tolto(cim : string; teko : string); override;
	procedure BitElkuldClick(Sender: TObject);
	procedure FormDestroy(Sender: TObject);
  procedure MaskEditClick(Sender: TObject);
  procedure MaskEditExit(Sender: TObject);
	procedure MaskEditKeyPress(Sender: TObject; var Key: Char);
    procedure Modosit(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
	private
		afalist		: TStringList;
     modosult 	: boolean;
	public
	end;

var
	TermekbeDlg: TTermekbeDlg;

implementation

uses
	Egyeb, J_SQL, Kozos, Math;
{$R *.DFM}

procedure TTermekbeDlg.BitKilepClick(Sender: TObject);
begin
	if modosult then begin
  		if NoticeKi('M�dosultak az adatok . Ki akar l�pni ment�s n�lk�l?', NOT_QUESTION ) = 0 then begin
			ret_kod := '';
			Close;
     	end;
  	end else begin
		ret_kod := '';
		Close;
  	end;
end;

procedure TTermekbeDlg.FormCreate(Sender: TObject);
begin
  	EgyebDlg.SetAdoQueryDatabase(Query1);
	SetMaskEdits([MaskEdit1]);
	ret_kod 	:= '';
  	modosult 	:= false;
  	afalist 		:= TStringList.Create;
	SzotarTolt(AFACombo, '101' , afalist, false );
  	AFACombo.ItemIndex := 0;
end;

procedure TTermekbeDlg.Tolto(cim : string; teko:string);
var
  afasor	: integer;
begin
	ret_kod 	:= teko;
	Caption 		:= cim;
	MaskEdit1.Text := '';
	MaskEdit2.Text := '';
	MaskEdit3.Text := '';
	MaskEdit4.Text := '';
	MaskEdit5.Text := '';
	MaskEdit7.Text := '';
	MaskEdit10.Text := '';
	if ret_kod <> '' then begin		{M�dos�t�s}
			if Query_Run(Query1, 'SELECT * FROM TERMEK WHERE T_TERKOD = '''+ret_kod+''' ') then begin
				MaskEdit1.Text := Query1.FieldByName('T_TERKOD').AsString;
				MaskEdit2.Text := Query1.FieldByName('T_TERNEV').AsString;
				MaskEdit3.Text := Query1.FieldByName('T_LEIRAS').AsString;
				MaskEdit4.Text := Query1.FieldByName('T_CIKKSZAM').AsString;
				MaskEdit5.Text := Query1.FieldByName('T_ITJSZJ').AsString;
        afasor := afalist.IndexOf(Query1.FieldByName('T_AFA').AsString);
        if afasor < 0 then begin
           	afasor := 0;
        end;
        AfaCombo.ItemIndex  := afasor;
				MaskEdit7.Text := SzamString(Query1.FieldByName('T_EGYAR').AsFloat,12,0);
				MaskEdit10.Text := Query1.FieldByName('T_MEGJ').AsString;
        ComboBox1.ItemIndex:= ComboBox1.Items.IndexOf(Query1.FieldByName('T_ME').AsString) ;
			 //	ComboBox1.Text := Query1.FieldByName('T_ME').AsString;
			  CheckBox1.Checked:= StrToIntDef(Query1.FieldByName('T_KKEZ').AsString, 0) > 0 ;
			end;
	end;
  modosult 	:= false;
end;

procedure TTermekbeDlg.BitElkuldClick(Sender: TObject);
begin
	if MaskEdit2.Text = '' then begin
		NoticeKi('Hi�nyz� megnevez�s!');
		MaskEdit2.SetFocus;
     Exit;
  end;
	if CheckBox1.Checked and (ComboBox1.Text='') then begin
		NoticeKi('Hi�nyz� mennyis�gi egys�g!');
		ComboBox1.SetFocus;
     Exit;
  end;
  if ret_kod = '' then begin
     {�j term�krekord felvitele}
     ret_kod	:= Format('%4.4d',[GetNextCode('TERMEK', 'T_TERKOD', 0, 4)]) ;
     Query_Run(Query1, 'INSERT INTO TERMEK (T_TERKOD) VALUES ('''+ret_kod+''' )');
     MaskEdit1.Text := ret_kod;
  end;
	Query_Update ( Query1, 'TERMEK',
  	[
    	'T_TERNEV', 	'''' + MaskEdit2.Text + '''',
    	'T_LEIRAS', 	'''' + MaskEdit3.Text + '''',
    	'T_CIKKSZAM', 	'''' + MaskEdit4.Text + '''',
    	'T_ITJSZJ', 	'''' + MaskEdit5.Text + '''',
    	'T_AFA', 		'''' + afalist[Afacombo.ItemIndex] + '''',
    	'T_EGYAR',		SqlSzamString(StringSzam(MaskEdit7.Text),12,0),
    	'T_ME', 		'''' + ComboBox1.Text + '''',
    	'T_KKEZ',  '''' +	IntToStr( IfThen(CheckBox1.Checked,1,0))+ ''''	 ,
    	'T_MEGJ', 		'''' + MaskEdit10.Text + ''''
     ], ' WHERE T_TERKOD = '''+ret_kod+''' ');
  Close;
end;

procedure TTermekbeDlg.FormDestroy(Sender: TObject);
begin
  afalist.Free;
end;

procedure TTermekbeDlg.MaskEditClick(Sender: TObject);
begin
	with Sender as TMaskEdit do begin
  	SelectAll;
  end;
end;

procedure TTermekbeDlg.MaskEditExit(Sender: TObject);
begin
	SzamExit(Sender);
end;

procedure TTermekbeDlg.MaskEditKeyPress(Sender: TObject; var Key: Char);
begin
  with Sender as TMaskEdit do begin
  	Key := EgyebDlg.Jochar(Text,Key,12,0);
  end;
end;

procedure TTermekbeDlg.Modosit(Sender: TObject);
begin
	modosult := true;
end;

procedure TTermekbeDlg.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
	EgyebDlg.FormReturn(Key);
end;

end.
