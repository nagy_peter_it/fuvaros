unit terhelesbe;

interface

uses
	SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
	Forms, Dialogs, StdCtrls, Buttons, Mask, DB, DBTables, J_ALFORM, ADODB;

type
	TTerhelesbeDlg = class(TJ_AlformDlg)
	BitElkuld: TBitBtn;
	BitKilep: TBitBtn;
	Label2: TLabel;
    M1: TMaskEdit;
	Label8: TLabel;
    M2: TMaskEdit;
    Label3: TLabel;
    M4: TMaskEdit;
    Label7: TLabel;
    M3: TMaskEdit;
    BitBtn1: TBitBtn;
    BitBtn10: TBitBtn;
    Label13: TLabel;
    M0: TMaskEdit;
    Query1: TADOQuery;
	procedure BitKilepClick(Sender: TObject);
	procedure FormCreate(Sender: TObject);
	procedure Tolto(cim : string; teko:string); override;
	procedure BitElkuldClick(Sender: TObject);
  procedure MaskEditClick(Sender: TObject);
    procedure Modosit(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure M4Exit(Sender: TObject);
    procedure MaskKeyPress(Sender: TObject; var Key: Char);
    procedure BitBtn1Click(Sender: TObject);
    procedure M2Change(Sender: TObject);
    procedure M2Exit(Sender: TObject);
    procedure BitBtn10Click(Sender: TObject);
	private
     modosult 	: boolean;
	public
	end;

var
	TerhelesbeDlg: TTerhelesbeDlg;

implementation

uses
	Egyeb, J_SQL, Kozos, J_VALASZTO;

{$R *.DFM}

procedure TTerhelesbeDlg.BitKilepClick(Sender: TObject);
begin
	if modosult then begin
  	if NoticeKi('M�dosultak az adatok . Ki akar l�pni ment�s n�lk�l?', NOT_QUESTION) = 0 then begin
			ret_kod := '';
			Close;
     end;
  end else begin
		ret_kod := '';
		Close;
  end;
end;

procedure TTerhelesbeDlg.FormCreate(Sender: TObject);
begin
	EgyebDlg.SetADOQueryDatabase(Query1);
	SetMaskEdits([M0, M1]);
	ret_kod 	:= '';
  	modosult := false;
end;

procedure TTerhelesbeDlg.Tolto(cim : string; teko:string);
begin
	ret_kod 		:= teko;
	Caption 			:= cim;
	M0.Text 			:= '';
	M1.Text 			:= '';
	M2.Text 			:= '';
	M3.Text 			:= '';
	M4.Text 			:= '';
	if ret_kod <> '' then begin		{M�dos�t�s}
     	if Query_Run (Query1, 'SELECT * FROM TEHER WHERE TH_THKOD = '''+teko+''' ',true) then begin
			M0.Text 	:= Query1.FieldByName('TH_DOKOD').AsString;
			M1.Text 	:= Query1.FieldByName('TH_DONEV').AsString;
			M2.Text 	:= Query1.FieldByName('TH_DATUM').AsString;
			M3.Text 	:= SzamString(Query1.FieldByName('TH_OSSZEG').AsFloat,12,2);
			M4.Text 	:= Query1.FieldByName('TH_MEGJ').AsString;
		end;
  	end;
  	modosult 	:= false;
end;

procedure TTerhelesbeDlg.BitElkuldClick(Sender: TObject);
begin
	if M0.Text = '' then begin
		NoticeKi('Dolgoz� megad�sa k�telez�!');
     	Exit;
  	end;

	if ( ( M2.Text = '' ) or ( not Jodatum2( M2 ) ) )then begin
		NoticeKi('D�tum megad�sa k�telez�!');
		M2.Setfocus;
     	Exit;
  	end;

	if ret_kod = '' then begin
  		{�j rekord felvitele}
		ret_kod	:= Format('%5.5d',[GetNextCode('TEHER', 'TH_THKOD', 0, 5)]);
		Query_Run ( Query1, 'INSERT INTO TEHER (TH_THKOD) VALUES (''' +ret_kod+ ''' )',true);
	end;
	Query_Update( Query1, 'TEHER', [
		'TH_DOKOD', 	''''+M0.Text+'''',
		'TH_DONEV', 	''''+M1.Text+'''',
		'TH_DATUM', 	''''+M2.Text+'''',
		'TH_OSSZEG', 	SqlSzamString(StringSzam(M3.Text),12,4),
		'TH_MEGJ',		''''+M4.Text+''''
		] , ' WHERE TH_THKOD = '''+ret_kod+''' ' );
	Close;
end;

procedure TTerhelesbeDlg.MaskEditClick(Sender: TObject);
begin
	with Sender as TMaskEdit do begin
	SelectAll;
  end;
end;

procedure TTerhelesbeDlg.Modosit(Sender: TObject);
begin
	modosult := true;
end;

procedure TTerhelesbeDlg.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
	EgyebDlg.FormReturn(Key);
end;

procedure TTerhelesbeDlg.M4Exit(Sender: TObject);
begin
	SzamExit(Sender);
end;

procedure TTerhelesbeDlg.MaskKeyPress(Sender: TObject; var Key: Char);
begin
  with Sender as TMaskEdit do begin
  	Key := EgyebDlg.Jochar(Text,Key,12,Tag);
  end;
end;

procedure TTerhelesbeDlg.BitBtn1Click(Sender: TObject);
begin
	DolgozoValaszto(M0, M1);
  M2.Setfocus;
end;

procedure TTerhelesbeDlg.M2Change(Sender: TObject);
begin
	Modosit(Sender);
end;

procedure TTerhelesbeDlg.M2Exit(Sender: TObject);
begin
	DatumExit(Sender, false);
end;

procedure TTerhelesbeDlg.BitBtn10Click(Sender: TObject);
begin
	EgyebDlg.Calendarbe(M2);
end;

end.
