unit HTTPRutinok;

interface

uses Classes, SysUtils, Variants, IdSSLOpenSSL, IdHTTP;

  function HttpGet(url: String): string;
  function HttpGetBinary(url: String; responseStream : TMemoryStream): string;
  function HttpPost(url, RequestContentType: String; Params: TStringStream): string;

implementation

function HttpGet(url: String): string;
var
  responseStream : TMemoryStream;
  html: string;
  HTTP: TIdHTTP;
  LHandler: TIdSSLIOHandlerSocketOpenSSL;
begin
  try
      try
        LHandler := TIdSSLIOHandlerSocketOpenSSL.Create(nil);
        responseStream := TMemoryStream.Create;
        HTTP := TIdHTTP.Create(nil);
        HTTP.IOHandler:=LHandler;
        HTTP.HTTPOptions := [hoForceEncodeParams];
        HTTP.Get(url, responseStream);
        SetString(html, PAnsiChar(responseStream.Memory), responseStream.Size);
        result := html;
      except
        on E: Exception do
            result := 'Hiba a HTTP lekérdezésben: '+E.Message;
      end;
    finally
      try
        HTTP.Disconnect;
        HTTP.Free;
        LHandler.Free;
      except
      end;
    end;
end;

function HttpGetBinary(url: String; responseStream : TMemoryStream): string;
var
  html: string;
  HTTP: TIdHTTP;
  LHandler: TIdSSLIOHandlerSocketOpenSSL;
begin
  try
      try
        LHandler := TIdSSLIOHandlerSocketOpenSSL.Create(nil);
        HTTP := TIdHTTP.Create(nil);
        HTTP.IOHandler:=LHandler;
        HTTP.HTTPOptions := [hoForceEncodeParams];
        // ------- 2018-10-26 --------

        // HTTP.HTTPOptions := [];
        // esetleg ezt be?
        // HTTP.SetRequestParams(TStrings.Create(), TIdTextEncoding.Default);
        // ---------------------------
        HTTP.Get(url, responseStream);
        result:= '';
      except
        on E: Exception do
            result := 'Hiba a HTTP lekérdezésben: '+E.Message;
      end;
    finally
      try
        HTTP.Disconnect;
        HTTP.Free;
        LHandler.Free;
      except
      end;
    end;
end;

function HttpPost(url, RequestContentType: String; Params: TStringStream): String;
var
  responseStream : TMemoryStream;
  html: string;
  HTTP: TIdHTTP;
  LHandler: TIdSSLIOHandlerSocketOpenSSL;
begin
  try
      try
        LHandler := TIdSSLIOHandlerSocketOpenSSL.Create(nil);
        responseStream := TMemoryStream.Create;
        HTTP := TIdHTTP.Create(nil);
        HTTP.IOHandler:=LHandler;
        HTTP.HTTPOptions := [hoForceEncodeParams];
        HTTP.Request.ContentType := RequestContentType;
        HTTP.Request.Charset := 'utf-8';
        HTTP.Post(url, Params, responseStream);
        SetString(html, PAnsiChar(responseStream.Memory), responseStream.Size);
        result := html;
      except
        on E: Exception do
            result := 'Hiba a HTTP Post-ban: '+E.Message;
      end;
    finally
      try
        HTTP.Disconnect;
        HTTP.Free;
        LHandler.Free;
      except
      end;
    end;
end;

end.
