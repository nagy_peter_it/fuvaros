unit GepkocsiBe;

interface

uses
	SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
	Forms, Dialogs, StdCtrls, Buttons, Mask, DB, DBTables, J_ALFORM,
	TabNotBk, ComCtrls, Grids, ADODB, ExtCtrls, DBGrids, Shellapi, FileCtrl,
  DBCtrls;

type
	TGepkocsibeDlg = class(TJ_AlformDlg)
    TBN1: TTabbedNotebook;
    GroupBox1: TGroupBox;
    SG1: TStringGrid;
    Query1: TADOQuery;
    Panel1: TPanel;
    BitElkuld: TBitBtn;
    BitKilep: TBitBtn;
    Panel2: TPanel;
    Label2: TLabel;
	 Label8: TLabel;
    Label1: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
	 Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label13: TLabel;
    Label14: TLabel;
	 Label15: TLabel;
	 Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
	 Label19: TLabel;
    Label20: TLabel;
    Label21: TLabel;
	 Label22: TLabel;
    Label23: TLabel;
	 Label24: TLabel;
    Label25: TLabel;
    Label29: TLabel;
    Label30: TLabel;
	 Label35: TLabel;
    M1: TMaskEdit;
    M2: TMaskEdit;
    M3: TMaskEdit;
    M5: TMaskEdit;
    MaskEdit1: TMaskEdit;
    MaskEdit2: TMaskEdit;
	 MaskEdit3: TMaskEdit;
    M51: TMaskEdit;
    M52: TMaskEdit;
    M53: TMaskEdit;
    M10: TMaskEdit;
    BitBtn2: TBitBtn;
    M8: TMaskEdit;
    M9: TMaskEdit;
	 M11: TMaskEdit;
    BitBtn1: TBitBtn;
    MaskEdit4: TMaskEdit;
	 CheckBox3: TCheckBox;
    CheckBox4: TCheckBox;
    Panel3: TPanel;
    ButtonMod: TBitBtn;
    ButtonTor: TBitBtn;
	 M11A: TMaskEdit;
	 BitBtn3: TBitBtn;
    CBKateg: TComboBox;
    Panel4: TPanel;
    Label3: TLabel;
    Label32: TLabel;
    Label12: TLabel;
    Label26: TLabel;
    Label27: TLabel;
	 Label28: TLabel;
    Label31: TLabel;
	 Label33: TLabel;
    Label34: TLabel;
    M27: TMaskEdit;
    M12: TMaskEdit;
    M21: TMaskEdit;
    M22: TMaskEdit;
    M23: TMaskEdit;
    M26: TMaskEdit;
	 M24: TMaskEdit;
    M25: TMaskEdit;
    M20A: TMaskEdit;
    MaskEdit5: TMaskEdit;
	 BitBtn26: TBitBtn;
    MaskEdit6: TMaskEdit;
    CheckBox1: TCheckBox;
	 MaskEdit7: TMaskEdit;
    MaskEdit9: TMaskEdit;
    BitBtn37: TBitBtn;
	 Label36: TLabel;
    M26A: TMaskEdit;
    Label37: TLabel;
    MaskEdit8: TMaskEdit;
    Label38: TLabel;
	 MaskEdit10: TMaskEdit;
	 Label39: TLabel;
    Label40: TLabel;
    CheckBox2: TCheckBox;
    Label41: TLabel;
    Label42: TLabel;
    CBLiceg: TComboBox;
    MaskEdit11: TMaskEdit;
    BitBtn5: TBitBtn;
    Label43: TLabel;
    MaskEdit12: TMaskEdit;
    Label45: TLabel;
    MaskEdit13: TMaskEdit;
	 Label46: TLabel;
	 MaskEdit14: TMaskEdit;
    CBLiVnem: TComboBox;
    CBLiPeriod: TComboBox;
    Label47: TLabel;
    Label48: TLabel;
    MaskEdit15: TMaskEdit;
    GroupBox3: TGroupBox;
    Panel6: TPanel;
	 BitBtn30: TBitBtn;
    BitBtn31: TBitBtn;
    BitBtn32: TBitBtn;
    BitBtn21: TBitBtn;
	 DBGrid1: TDBGrid;
    QueryLizing: TADOQuery;
	 DataSource1: TDataSource;
    QueryLizingLZ_LZKOD: TIntegerField;
    QueryLizingLZ_RENSZ: TStringField;
    QueryLizingLZ_DATUM: TStringField;
    QueryLizingLZ_ERTEK: TBCDField;
    QueryLizingLZ_VANEM: TStringField;
    QueryLizingLZ_MEGJE: TStringField;
	 QueryLizingLZ_BEDAT: TStringField;
    Panel5: TPanel;
    GroupBox4: TGroupBox;
    Panel7: TPanel;
    ButtonUj2: TBitBtn;
    BitBtn42: TBitBtn;
    GroupBox2: TGroupBox;
    Panel8: TPanel;
    ButtonUj: TBitBtn;
    BitBtn4: TBitBtn;
    SG2: TStringGrid;
    QueryKmValt: TADOQuery;
    DS_KmValt: TDataSource;
    DBGrid2: TDBGrid;
    BitBtn41: TBitBtn;
    GroupBox5: TGroupBox;
    MaskEdit17: TMaskEdit;
    MaskEdit16: TMaskEdit;
    Label49: TLabel;
    MaskEdit18: TMaskEdit;
    CBVetnem: TComboBox;
    Label50: TLabel;
    CBEuro: TComboBox;
    MaskEdit19: TMaskEdit;
    BitBtn6: TBitBtn;
    CheckBox7: TCheckBox;
    QueryKmValtKC_KCKOD: TIntegerField;
    QueryKmValtKC_RENSZ: TStringField;
    QueryKmValtKC_DATUM: TStringField;
    QueryKmValtKC_KMOR1: TBCDField;
    QueryKmValtKC_KMOR2: TBCDField;
    QueryKmValtKC_MEGJE: TStringField;
    BitBtn7: TBitBtn;
    Label51: TLabel;
    M11S: TMaskEdit;
    BitBtn8: TBitBtn;
    Query2: TADOQuery;
    Label52: TLabel;
    MaskEdit20: TMaskEdit;
    MaskEdit21: TMaskEdit;
    Label53: TLabel;
    M11K: TMaskEdit;
    BitBtn9: TBitBtn;
    DBGrid3: TDBGrid;
    ADOQuery1: TADOQuery;
    DataSource2: TDataSource;
    ADOQuery1KM_ID: TAutoIncField;
    ADOQuery1KM_DATUM: TStringField;
    ADOQuery1KM_RAKKOD: TStringField;
    ADOQuery1KM_RAKNEV: TStringField;
    ADOQuery1KM_TKOD: TStringField;
    ADOQuery1KM_TNEV: TStringField;
    ADOQuery1KM_ME: TStringField;
    ADOQuery1KM_GKRESZ: TStringField;
    ADOQuery1KM_GKKAT: TStringField;
    ADOQuery1KM_GKEUROB: TIntegerField;
    ADOQuery1KM_KMORA: TIntegerField;
    ADOQuery1KM_MENNY: TBCDField;
    ADOQuery1KM_EMENNY: TBCDField;
    ADOQuery1KM_KIBE: TStringField;
    ADOQuery1KM_MNEM: TStringField;
    ADOQuery1KM_MEGJ: TStringField;
    ADOQuery1KM_MODDAT: TDateTimeField;
    ADOQuery1KM_USER: TStringField;
    Label54: TLabel;
    CBszin: TComboBox;
    GroupBox6: TGroupBox;
    Label55: TLabel;
    Edit1: TEdit;
    Label56: TLabel;
    Edit2: TEdit;
    Label57: TLabel;
    Edit3: TEdit;
    RadioGroup1: TRadioGroup;
    Query3: TADOQuery;
    Query3SI_GKTIP: TStringField;
    Query3SI_GKKAT: TStringField;
    Query3SI_JELLEG: TStringField;
    Query3SI_SERV1: TIntegerField;
    Query3SI_SERVT: TIntegerField;
    Query3SI_JELZ1: TIntegerField;
    Query3SI_JELZT: TIntegerField;
    Service: TADODataSet;
    SJelleg: TADOQuery;
    SJellegsi_jelleg: TStringField;
    TabControl1: TTabControl;
    DSService: TDataSource;
    DBGrid4: TDBGrid;
    ServiceSH_KOD: TIntegerField;
    ServiceSH_RENSZ: TStringField;
    ServiceSH_DATUM: TStringField;
    ServiceSH_SERVKM: TIntegerField;
    ServiceSH_MEGJ: TStringField;
    Panel9: TPanel;
    DBNavigator1: TDBNavigator;
    BitBtn12: TBitBtn;
    BitBtn13: TBitBtn;
    BitBtn14: TBitBtn;
    ServiceSH_SERVINT: TIntegerField;
    ServiceSH_SERVKM2: TIntegerField;
    ServiceSH_JELZES: TIntegerField;
    SJellegsi_kod: TIntegerField;
    MEdit: TMaskEdit;
    ServiceSH_JELLEG: TStringField;
    MaskEdit23: TMaskEdit;
    Label58: TLabel;
    Query4: TADOQuery;
    GroupBox7: TGroupBox;
    CBKUL: TCheckBox;
    CB2: TCheckBox;
    cbEladasra: TCheckBox;
    CheckBox6: TCheckBox;
    cbNoKimut: TCheckBox;
    MaskEdit22: TMaskEdit;
    BitBtn10: TBitBtn;
    cbNemTermelo: TCheckBox;
	procedure BitKilepClick(Sender: TObject);
	procedure FormCreate(Sender: TObject);
	procedure Tolto(cim : string; teko : string); override;
	procedure BitElkuldClick(Sender: TObject);
  procedure MaskEditClick(Sender: TObject);
    procedure Modosit(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
	   Shift: TShiftState);
    procedure MaskKeyPress(Sender: TObject; var Key: Char);
    procedure MaskExit(Sender: TObject);
    procedure MClick(Sender: TObject);
	 procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure MDatumExit(Sender: TObject);
    procedure BitBtn26Click(Sender: TObject);
	 procedure BitBtn37Click(Sender: TObject);
    procedure ButtonModClick(Sender: TObject);
	 procedure ButtonTorClick(Sender: TObject);
	 procedure BitBtn3Click(Sender: TObject);
	 procedure SG1Click(Sender: TObject);
	 procedure MezoFrissito;
	 procedure SG1MouseDown(Sender: TObject; Button: TMouseButton;
	   Shift: TShiftState; X, Y: Integer);
	 procedure M11AExit(Sender: TObject);
	 procedure Sofortolto(kod : string);
	 procedure ButtonUjClick(Sender: TObject);
	 procedure BitBtn4Click(Sender: TObject);
	 procedure CheckBox2Click(Sender: TObject);
	 procedure BitBtn5Click(Sender: TObject);
	 procedure MaskEdit11Exit(Sender: TObject);
	 procedure BitBtn30Click(Sender: TObject);
	 procedure LizingTolto;
	 procedure KmValtTolto;
	 procedure BitBtn31Click(Sender: TObject);
	 procedure BitBtn32Click(Sender: TObject);
	 procedure BitBtn21Click(Sender: TObject);
    procedure BitBtn6Click(Sender: TObject);
    procedure ButtonUj2Click(Sender: TObject);
    procedure BitBtn41Click(Sender: TObject);
    procedure BitBtn42Click(Sender: TObject);
    procedure BitBtn7Click(Sender: TObject);
    procedure BitBtn8Click(Sender: TObject);
    procedure BitBtn9Click(Sender: TObject);
    procedure TBN1Change(Sender: TObject; NewTab: Integer;
      var AllowChange: Boolean);
    procedure BitBtn10Click(Sender: TObject);
    procedure CheckBox6Click(Sender: TObject);
    procedure RadioGroup1Click(Sender: TObject);
    procedure TabControl1Change(Sender: TObject);
    procedure ServiceAfterInsert(DataSet: TDataSet);
    procedure ServiceCalcFields(DataSet: TDataSet);
    procedure BitBtn12Click(Sender: TObject);
    procedure BitBtn14Click(Sender: TObject);
    procedure DBGrid4KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBGrid4KeyPress(Sender: TObject; var Key: Char);
    procedure ServiceSH_SERVINTChange(Sender: TField);
    procedure ServiceSH_SERVKMChange(Sender: TField);
    procedure ServiceAfterPost(DataSet: TDataSet);
    procedure ServiceBeforePost(DataSet: TDataSet);
    procedure DBGrid4ColEnter(Sender: TObject);
    procedure DBGrid4DblClick(Sender: TObject);
    procedure SetPeriodLabel;
    procedure CBLiPeriodChange(Sender: TObject);
	private
	  modosult 		: boolean;
	  lista_kateg	: TStringList;
	  lista_liceg   : TStringList;
	  vnemlist, vvetlist, szinlist, periodlist: TStringList;
	public
	end;

var
	GepkocsibeDlg: TGepkocsibeDlg;
  SERVTIP: array of integer;
 // SERVTIPNEV: array of string;
 // STNEV: string;
  SZERVINT, JELZES : integer;


implementation

uses
	GepkocsiUjFm, Kozos, Egyeb, J_SQL, DolgozoUjFm, Lizingbe, LizingList, Kozos_Local, KmValtbe,
  Terkep, Windows;

{$R *.DFM}

procedure TGepkocsibeDlg.BitKilepClick(Sender: TObject);
begin
	if modosult then begin
	if NoticeKi('M�dosultak az adatok . Ki akar l�pni ment�s n�lk�l?', NOT_QUESTION ) = 0 then begin
			ret_kod := '';
			Close;
	  end;
  end else begin
		ret_kod := '';
		Close;
  end;
end;

procedure TGepkocsibeDlg.FormCreate(Sender: TObject);
var
	i 	: integer;
begin
	BitBtn7.Visible 	:= EgyebDlg.NavC_Kapcs_OK;
	ret_kod 	:= '';
	EgyebDlg.SetADOQueryDatabase(Query1);
	EgyebDlg.SetADOQueryDatabase(Query2);
	EgyebDlg.SetADOQueryDatabase(QueryLizing);
	EgyebDlg.SetADOQueryDatabase(QueryKmValt);
	//CPotClick(Sender);
	// A garanci�k felt�lt�se
	SG1.Cells[0,0]	:= 'Ssz.';
	SG1.Cells[1,0]	:= 'K�d';
	SG1.Cells[2,0]	:= 'Megnevez�s';
	SG1.Cells[3,0]	:= 'Flag';
	SG1.Cells[4,0]	:= 'D�tum';
	SG2.Cells[0,0]	:= 'Ssz.';
	SG2.Cells[1,0]	:= 'S.k�d';
	SG2.Cells[2,0]	:= 'Bel�p�s ideje';
	SG2.Cells[3,0]	:= 'Sof�r neve';
	SG2.ColWidths[0]	:=30;
	SG2.ColWidths[1]	:=35;
	SG2.ColWidths[2]	:=80;
	SG2.ColWidths[3]	:=180;

	Query_Run(Query1, 'SELECT * FROM SZOTAR WHERE SZ_FOKOD = ''125'' ORDER BY SZ_ALKOD ');
	if Query1.RecordCount > 0 then begin
		SG1.RowCount := Query1.RecordCount+1;
		i := 1;
		while not Query1.Eof do begin
			SG1.Cells[0,i]	:= IntToStr(i);
			SG1.Cells[1,i]	:= Query1.FieldByName('SZ_ALKOD').AsString;
			SG1.Cells[2,i]	:= Query1.FieldByName('SZ_MENEV').AsString;
			SG1.Cells[3,i]	:= 'Nem';
			SG1.Cells[4,i]	:= '';
			Query1.Next;
			Inc(i);
		end;
	end;
	modosult 	:= false;
	lista_kateg	:= TStringList.Create;
	SzotarTolt(CBKateg, '340' , lista_kateg, false, true );
	CBKateg.ItemIndex  := 0;

	szinlist	:= TStringList.Create;
	SzotarTolt(CBszin, '115' , szinlist, false, false );
	CBszin.ItemIndex  := 1;

	lista_liceg	:= TStringList.Create;
	SzotarTolt(CBLiceg, '380' , lista_liceg, false, true );
	CBLiceg.ItemIndex  := 0;
	vnemlist := TStringList.Create;
	SzotarTolt(CBLivnem, '110' , vnemlist, true, false );
	{ ezt csin�lja a "SzotarTolt"
  CBLivnem.Items.Clear;
	for i := 0 to vnemlist.Count - 1 do begin
		CBLivnem.Items.Add(vnemlist[i]);
	end;}
	periodlist := TStringList.Create;
  SzotarTolt(CBLiPeriod, '240' , periodlist, false, false );
	vvetlist := TStringList.Create;
	SzotarTolt(CBVetnem, '110' , vvetlist, true, false );
	{CBVetnem.Items.Clear;
	for i := 0 to vvetlist.Count - 1 do begin
		CBVetnem.Items.Add(vvetlist[i]);
	end;}
	CheckBox2Click(Sender);
end;

procedure TGepkocsibeDlg.Tolto(cim : string; teko:string);
var
	sorsz 	: integer;
	pozic 	: RPozicio;
  HUTOTIP : string;
  i       : integer;
begin
	HUTOTIP:= EgyebDlg.Read_SZGrid( '999', '922' );
	// A t�li norm�k nem m�dos�that�ak
	SetMaskEdits([MaskEdit5, M5, M52]);
	// A km v�lt�s tilt�sa a Barnuln�l
	ret_kod 		:= teko;
	Caption 		:= cim;
	M1.Text 		:= teko;
	CBKateg.Enabled	:= true;
	if ret_kod <> '' then begin		{M�dos�t�s}
//		CBKateg.Enabled	:= false;
		if Query_Run (Query1, 'SELECT * FROM GEPKOCSI WHERE GK_KOD = '''+teko+''' ',true) then begin
			M2.Text    			:= Query1.FieldByName('GK_RESZ').AsString;
			M3.Text    			:= Query1.FieldByName('GK_TIPUS').AsString;
			M5.Text    			:= SzamString(Query1.FieldByName('GK_NORMA').AsFloat,6,2);
			M51.Text 			:= SzamString(Query1.FieldByName('GK_NORNY').AsFloat,6,2);
			M52.Text 			:= SzamString(Query1.FieldByName('GK_APETE').AsFloat,6,2);
			M53.Text 			:= SzamString(Query1.FieldByName('GK_APENY').AsFloat,6,2);
			MaskEdit1.Text 		:= SzamString(Query1.FieldByName('GK_DIJ1').AsFloat,10,2);
			MaskEdit2.Text 		:= SzamString(Query1.FieldByName('GK_DIJ2').AsFloat,10,2);
			MaskEdit3.Text 		:= SzamString(Query1.FieldByName('GK_SULYL').AsFloat,10,2);
			MaskEdit4.Text 		:= SzamString(Query1.FieldByName('GK_SULYA').AsFloat,10,2);
			M27.Text 			:= Query1.FieldByName('GK_MEGJ').AsString;
			M26A.Text 			:= Query1.FieldByName('GK_HOZZA').AsString;
			CB2.Checked			:= Query1.FieldByName('GK_KLIMA').AsInteger > 0;
			cbEladasra.Checked:= Query1.FieldByName('GK_ELADO').AsInteger > 0;
 			cbNemTermelo.Checked:= Query1.FieldByName('GK_NEMTERM').AsInteger > 0;
			M8.Text    			:= Query1.FieldByName('GK_ALVAZ').AsString;
			M9.Text    			:= Query1.FieldByName('GK_MOTOR').AsString;
			M10.Text 			:= Query1.FieldByName('GK_EVJAR').AsString;
			M11.Text 			:= Query1.FieldByName('GK_FORHE').AsString;
			M11S.Text 			:= Query1.FieldByName('GK_SFORH').AsString;
			M11K.Text 			:= Query1.FieldByName('GK_FORKI').AsString;
			M12.Text 			:= Query1.FieldByName('GK_FEHER').AsString;
			M20A.Text 			:= SzamString(Query1.FieldByName('GK_OSULY').AsFloat,9,2);
			M21.Text 			:= SzamString(Query1.FieldByName('GK_RAKSU').AsFloat,9,2);
			M22.Text 			:= SzamString(Query1.FieldByName('GK_TERAK').AsFloat,9,2);
			M23.Text 			:= SzamString(Query1.FieldByName('GK_RAME1').AsFloat,9,2);
			M24.Text 			:= SzamString(Query1.FieldByName('GK_RAME2').AsFloat,9,2);
			M25.Text 			:= SzamString(Query1.FieldByName('GK_RAME3').AsFloat,9,2);
			M26.Text 			:= SzamString(Query1.FieldByName('GK_EUPAL').AsFloat,4,0);
			CBKUL.Checked		:= Query1.FieldByName('GK_KULSO').AsInteger > 0;
			//CPot.Checked		:= Query1.FieldByName('GK_POTOS').AsInteger > 0;
      //GroupBox7.Visible:=RadioGroup1.ItemIndex=1;
      //GroupBox7.Visible:=CPot.Checked;
		//	ComboBox1.Text:= Query1.FieldByName('GK_HSKM').AsString;
		//	MaskEdit23.Text		:= Query1.FieldByName('GK_HSDAT').AsString;
			CheckBox1.Checked	:= Query1.FieldByName('GK_GARFI').AsInteger > 0;
			CheckBox3.Checked	:= Query1.FieldByName('GK_NEMZE').AsInteger > 0;
			CheckBox4.Checked	:= Query1.FieldByName('GK_ARHIV').AsInteger > 0;
      cbNoKimut.Checked	:= Query1.FieldByName('GK_NOKIM').AsInteger > 0;
			CheckBox6.Checked	:= Query1.FieldByName('GK_KIVON').AsInteger > 0;
			MaskEdit22.Text 	:= Query1.FieldByName('GK_IFORK').AsString;
      MaskEdit22.Enabled:=CheckBox6.Checked;
      BitBtn10.Enabled  :=CheckBox6.Checked;
			MaskEdit7.Text 		:= SzamString(Query1.FieldByName('GK_GARKM').AsFloat,10,0);
			MaskEdit9.Text 		:= Query1.FieldByName('GK_GARNO').AsString;
			MaskEdit8.Text 		:= SzamString(Query1.FieldByName('GK_TANKL').AsFloat,10,2);
			MaskEdit10.Text  	:= SzamString(Query1.FieldByName('GK_TARTL').AsFloat,10,2);
      MaskEdit20.Text   :=Query1.FieldByName('GK_UT_DA').AsString;
      if pos(Query1.FieldByName('GK_GKKAT').AsString,HUTOTIP)=0 then
        MaskEdit21.Text   :=Query1.FieldByName('GK_UT_KM').AsString+' km'
      else
        MaskEdit21.Text   :=Query1.FieldByName('GK_UT_KM').AsString+' �ra';
			ComboSet (CBKateg, 	 Trim(Query1.FieldByName('GK_GKKAT').AsString), lista_kateg);

			RadioGroup1.ItemIndex:= Query1.FieldByName('GK_POTOS').AsInteger;
      RadioGroup1.OnClick(self);

			// A lizing adatok megad�sa
			CheckBox2.Checked	:= false;
			if StrToIntDef(Query1.FieldByName('GK_LIVAN').AsString,0) > 0 then begin
				CheckBox2.Checked	:= true;
			end;
			if StrToIntDef(Query1.FieldByName('GK_LIVAN').AsString,0) = 2 then begin
				CheckBox7.Checked	:= true;
			end;
			CheckBox2Click(Self);
			ComboSet (CBLiceg, 	 Trim(Query1.FieldByName('GK_LICEG').AsString), lista_liceg);
			MaskEdit11.Text    	:= Query1.FieldByName('GK_LIDAT').AsString;
			MaskEdit12.Text    	:= Query1.FieldByName('GK_LIHON').AsString;
			MaskEdit13.Text    	:= Query1.FieldByName('GK_LISZE').AsString;
			MaskEdit14.Text    	:= Query1.FieldByName('GK_LIOSZ').AsString;
			MaskEdit15.Text    	:= Query1.FieldByName('GK_LIMEG').AsString;
			MaskEdit18.Text		:= Query1.FieldByName('GK_VETAR').AsString;
			MaskEdit23.Text		:= Query1.FieldByName('GK_M_ERT').AsString;

			Edit1.Text		:= Query1.FieldByName('GK_OBUID'  ).AsString;
			Edit2.Text		:= Query1.FieldByName('GK_OBUPIN' ).AsString;
			Edit3.Text		:= Query1.FieldByName('GK_OBUGYSZ').AsString;

			ComboSet (CBLivnem,  Trim(Query1.FieldByName('GK_LINEM').AsString), vnemlist);
			ComboSet (CBVetnem,  Trim(Query1.FieldByName('GK_VETVN').AsString), vvetlist);
			ComboSet (CBLiPeriod,  Trim(Query1.FieldByName('GK_LIPER').AsString), periodlist);
      SetPeriodLabel;
			{ A hozz�rendelt g�pkocsi be�ll�t�sa}
			MaskEdit6.Text		:= Format('%3.3d',[StrToIntDef(Query1.FieldByName('GK_POKOD').AsString,0)]);
			if MaskEdit6.Text <> '' then begin
				if Query_Run (EgyebDlg.QueryKOZOS, 'SELECT * FROM GEPKOCSI WHERE GK_KOD = '''+MaskEdit6.Text+''' ',true) then begin
					MaskEdit5.Text	:= EgyebDlg.QueryKOZOS.FieldByname('GK_RESZ').AsString;
				end;
			end;
			CBEuro.ItemIndex	:= StrToIntDef(Query1.FieldByName('GK_EUROB').AsString, 0);
			CBszin.ItemIndex	:= CBszin.Items.IndexOf(Query1.FieldByName('GK_GSZIN').AsString);
  //    CBszin.Text:=Query1.FieldByName('GK_GSZIN').AsString;
			SetMaskEdits([M1, M2]);
			// A garanci�k felvitele
			if Query_Run (EgyebDlg.QueryKOZOS, 'SELECT * FROM GARANCIA WHERE GA_RENSZ = '''+M2.Text+''' ',true) then begin
				while not EgyebDlg.QueryKozos.Eof do begin
					sorsz	:= SG1.Cols[1].IndexOf(EgyebDlg.QueryKozos.FieldByName('GA_ALKOD').AsString);
					if sorsz > 0 then begin
						SG1.Cells[3,sorsz]	:= 'Igen';
						SG1.Cells[4,sorsz]	:= EgyebDlg.QueryKozos.FieldByName('GA_DATUM').AsString;
					end;
					EgyebDlg.QueryKozos.Next;
				end;
			end;

		end;
		LizingTolto;
		KmValtTolto;
    //A SHELL k�rtya bejegyz�s ellen�rz�se
    for i := 1 to SG1.RowCount -1 do begin
           if StrToIntDef(SG1.Cells[1,i], 0) = 175 then begin
               // A SHELL k�rty�n�l a k�rtyasz�m beolvas�sa
               Query_Run(Query4, 'SELECT SH_KSZAM, SH_ERDAT FROM SHELL WHERE SH_RENSZ = '''+M2.Text+''' ');
               if Query4.RecordCount > 0 then begin
                   SG1.Cells[2,i]	:= SG1.Cells[2,i] + ' (' +Query4.FieldByName('SH_KSZAM').AsString+')';
			        SG1.Cells[3,i]	:= 'Igen';
			        SG1.Cells[4,i]	:= Query4.FieldByName('SH_ERDAT').AsString;
               end;
           end;
       end;
	end else begin
		{�j felviteln�l gener�ljon egy �j azonos�t�t}
		if Query_Run (EgyebDlg.QueryKOZOS, 'SELECT MAX(GK_KOD) MAXKOD FROM GEPKOCSI ', true) then begin
			M1.Text := Format('%3.3d',[Round(StringSzam(EgyebDlg.QueryKOZOS.FieldByname('MAXKOD').AsString)) + 1]);
		end;
		SetMaskEdits([M1]);
		CBKateg.Enabled	:= true;
	end;
	modosult 	:= false;
	TBN1.PageIndex	:= 0;
	MezoFrissito;
	Sofortolto('');
	// A poz�ci� ki�r�sa
	GetLocation(M2.Text, pozic);
	  MaskEdit17.Text	:= pozic.location;
	  MaskEdit16.Text	:= pozic.datum;
	  MaskEdit19.Text	:= 'Seb:'+IntToStr(pozic.sebesseg)+'   '+pozic.digit;
  /////////////////////////////
  {
	Query_Run(Query2, 'SELECT KS_DATUM,KS_KMORA FROM KOLTSEG WHERE KS_TIPUS= ''0'' and KS_RENDSZ='''+M2.Text+''' ORDER BY KS_DATUM DESC ');
  Query2.First;
  MaskEdit20.Text:=Query2.FieldByName('KS_DATUM').AsString;
  MaskEdit21.Text:=Query2.FieldByName('KS_KMORA').AsString+' km';
   }
  ///////////////////////////////////
 { Query3.Close;
  Query3.Parameters[0].Value:=lista_kateg[CBKateg.ItemIndex];
  Query3.Open;
  Query3.First ;
  GroupBox7.Visible:= (Query3SI_SERVT.Value>0);

  if Query3SI_SERVT.Value>0 then
  begin
  GroupBox7.Caption:=Query3SI_JELLEG.Value+' - '+Query3SI_GKTIP.Value;
  ComboBox1.Items.Clear;
  if Query3SI_SERV1.Value>0 then
  begin
    ComboBox1.Items.Add(Query3SI_SERV1.AsString);
  end;
  for i:=1 to 20 do
  begin
      ComboBox1.Items.Add(IntToStr(i*Query3SI_SERVT.Value)) ;
  end;
  end ;

  Query3.Close;      }
end;

procedure TGepkocsibeDlg.SetPeriodLabel;
begin
  Label47.Caption := ' / '+CBLiPeriod.Text;
end;

procedure TGepkocsibeDlg.BitElkuldClick(Sender: TObject);
var
	i 			: integer;
	ujkod		: integer;
	likod		: string;
	dir0	: string;
begin
	if M1.Text = '' then begin
		NoticeKi('K�d megad�sa k�telez�!');
		M1.Setfocus;
		Exit;
  end;

	if CheckBox1.Checked then begin
		if StringSzam(MaskEdit7.Text) = 0 then begin
			NoticeKi('Garancia lej�rat km nincs kit�ltve!');
			MaskEdit7.Setfocus;
			Exit;
		end;
	end;

  // Ha p�t, akkor az �ns�lyt ki kell t�lteni.
  if (RadioGroup1.ItemIndex=1) and (StringSzam(M20A.Text)=0) then
  //if (CPot.Checked) and (StringSzam(M20A.Text)=0) then
  begin
			NoticeKi('Az �ns�ly nincs kit�ltve!');
			M20A.Setfocus;
			Exit;
  end;

  // Tankm�ret kit�lt�s ell.
  if (MaskEdit8.Text='')or(MaskEdit10.Text='')or(StringSzam(MaskEdit8.Text)=0)or(StringSzam(MaskEdit10.Text)=0) then
  begin
      //dir0:=lista_kateg[CBKateg.ItemIndex];
//    if (not CBKUL.Checked)and((not CPot.Checked)or(Pos(lista_kateg[CBKateg.ItemIndex],'H,HV')>0)) then          // nem k�ls�, nem p�t vagy p�t, de h�t�s
//      if (not CBKUL.Checked)and((not CPot.Checked)or(Pos(''''+lista_kateg[CBKateg.ItemIndex]+'''',EgyebDlg.GK_HUTOS)>0)) then          // nem k�ls�, nem p�t vagy p�t, de h�t�s
      if (not CBKUL.Checked)and((RadioGroup1.ItemIndex=2)or(Pos(''''+lista_kateg[CBKateg.ItemIndex]+'''',EgyebDlg.GK_HUTOS)>0)) then          // nem k�ls�, nem p�t vagy p�t, de h�t�s
      begin
				NoticeKi('A tankm�ret, �s a tartal�k kit�lt�se k�telez�!');
        TBN1.PageIndex:=1;
				MaskEdit8.Setfocus;
				Exit;
      end;
  end;
  ////////////////////////////////
	if ret_kod = '' then begin
		{�j rekord felvitele}
		if Query_Run (Query1, 	'SELECT GK_KOD FROM GEPKOCSI WHERE GK_KOD = '''+M1.Text+''' ',true) then begin
			if Query1.RecordCount > 0 then begin
				NoticeKi('L�tez� k�d!');
				M1.Setfocus;
				Exit;
			end;
		end;
		if Query_Run (Query1, 	'SELECT GK_RESZ FROM GEPKOCSI WHERE GK_RESZ = '''+M2.Text+''' ',true) then begin
			if Query1.RecordCount > 0 then begin
				NoticeKi('L�tez� rendsz�m!');
				M2.Setfocus;
				Exit;
			end;
		end;

		Query_Run ( Query1, 'INSERT INTO GEPKOCSI (GK_KOD) VALUES (''' +M1.Text+ ''' )',true);
		ret_kod := M1.Text;
		// Felvissz�k az �j kateg�ri�t a kateg�ria t�bl�ba
		Query_Run ( Query1, 'INSERT INTO GK_KATEG ( KA_RENSZ, KA_GKKAT, KA_DATUM ) VALUES (''' +M2.Text+ ''', '''+
			lista_kateg[CBKateg.ItemIndex]+''','''' )',true);

    // K�nyvt�rak l�trehoz�sa
  	dir0	:= EgyebDlg.Read_SZGrid('999', '721');
	  if (dir0 <> '') and (Sysutils.DirectoryExists(dir0)) then
    begin
    	// A DIR0 egy l�tez� k�nyvt�r
	    if ( ( copy(dir0, Length(dir0), 0) <> '\' ) and ( copy(dir0, Length(dir0), 0) <> '/' ) ) then begin
		    dir0	:= dir0 + '\';
	    end;
    	// A n�v hozz�rak�sa a dir0 -hoz
    	dir0	:= dir0 + M2.text;
	    if not Sysutils.DirectoryExists(dir0) then begin
        if not CreateDir(dir0) then
        begin
		    //  NoticeKi('Nem l�tez�  g�pkocsi k�nyvt�r : "'+dir0+'"');
		    //  Exit;
        end;
	    end;
    	if (not Sysutils.DirectoryExists(dir0+'\Arch�v')) then begin
        if not CreateDir(dir0+'\Arch�v') then
        begin
		     // NoticeKi('Nem l�tez�  Arch�v k�nyvt�r : "'+dir0+'\Arch�v'+'"');
		    // Exit;
        end;
    	end;
    	if not Sysutils.DirectoryExists(dir0+'\Foto') then begin
        if not CreateDir(dir0+'\Foto') then
        begin
		    //  NoticeKi('Nem l�tez�  Foto k�nyvt�r : "'+dir0+'\Foto'+'"');
    		 // Exit;
        end;
    	end;
	  end;
  end;
	{A r�gi rekord m�dos�t�sa}
	Query_Update (Query1, 'GEPKOCSI',
		[
		'GK_RESZ', 	''''+M2.Text+'''',
		'GK_TIPUS', ''''+M3.Text+'''',
		'GK_GKKAT', ''''+lista_kateg[CBKateg.ItemIndex]+'''',
		'GK_KLIMA', BoolToIntStr(CB2.Checked),
		'GK_ELADO', BoolToIntStr(cbEladasra.Checked),
		'GK_NEMTERM', BoolToIntStr(cbNemTermelo.Checked),
		'GK_NORMA', SqlSzamString(StringSzam(M5.Text),6,2),
		'GK_NORNY', SqlSzamString(StringSzam(M51.Text),6,2),
		'GK_APETE', SqlSzamString(StringSzam(M52.Text),6,2),
		'GK_APENY', SqlSzamString(StringSzam(M53.Text),6,2),
		'GK_DIJ1', 	SqlSzamString(StringSzam(MaskEdit1.Text),10,2),
		'GK_DIJ2', 	SqlSzamString(StringSzam(MaskEdit2.Text),10,2),
		'GK_SULYL', SqlSzamString(StringSzam(MaskEdit3.Text),10,2),
		'GK_SULYA', SqlSzamString(StringSzam(MaskEdit4.Text),10,2),
		'GK_MEGJ', ''''+M27.Text+'''',
		'GK_OSULY', SqlSzamString(StringSzam(M20A.Text),9,2),
		'GK_RAKSU', SqlSzamString(StringSzam(M21.Text),9,2),
		'GK_TERAK', SqlSzamString(StringSzam(M22.Text),9,2),
		'GK_RAME1', SqlSzamString(StringSzam(M23.Text),9,2),
		'GK_RAME2', SqlSzamString(StringSzam(M24.Text),9,2),
		'GK_RAME3', SqlSzamString(StringSzam(M25.Text),9,2),
		'GK_EUPAL', SqlSzamString(StringSzam(M26.Text),4,0),
		'GK_HOZZA', SqlSzamString(StringSzam(M26A.Text),8,0),
		'GK_TANKL', SqlSzamString(StringSzam(MaskEdit8.Text),10,2),
		'GK_TARTL', SqlSzamString(StringSzam(MaskEdit10.Text),10,2),
		'GK_ALVAZ', ''''+M8.Text+'''',
		'GK_MOTOR', ''''+M9.Text+'''',
		'GK_EVJAR', ''''+copy(M10.Text,1,4)+'''',
		'GK_FORHE', ''''+M11.Text+'''',
		'GK_SFORH', ''''+M11S.Text+'''',
		'GK_FORKI', ''''+M11K.Text+'''',
		'GK_FEHER', ''''+M12.Text+'''',
		'GK_POKOD', ''''+MaskEdit6.Text+'''',
		'GK_KULSO', BoolToIntStr(CBKUL.Checked),
		//'GK_POTOS', BoolToIntStr(CPot.Checked),
		'GK_POTOS',  IntToStr( RadioGroup1.ItemIndex),
		'GK_GARKM', SqlSzamString(StringSzam(MaskEdit7.Text),10,2),
		'GK_GARFI', BoolToIntStr(CheckBox1.Checked),
		'GK_GARNO', ''''+MaskEdit9.Text+'''',
		'GK_LIVAN', '0',
		'GK_EUROB', IntToStr(CBEuro.ItemIndex),
		'GK_NEMZE', BoolToIntStr(CheckBox3.Checked),
 		'GK_NOKIM', BoolToIntStr(cbNoKimut.Checked),
		'GK_ARHIV', BoolToIntStr(M11K.Text<> ''),
		'GK_KIVON', BoolToIntStr(CheckBox6.Checked),
		'GK_IFORK', ''''+MaskEdit22.Text+'''',
		'GK_GSZIN', ''''+CBszin.Text+'''',
		'GK_OBUID', ''''+Edit1.Text+'''',
		'GK_OBUPIN', ''''+Edit2.Text+'''',
		'GK_OBUGYSZ', ''''+Edit3.Text+''''
		//'GK_HSKM', ''''+ComboBox1.Text+'''',
		//'GK_HSDAT', ''''+MaskEdit23.Text+''''
		],' WHERE GK_KOD = '''+ret_kod+''' ' );
	likod := '1';
	if CheckBox7.Checked then begin
		likod := '2';
	end;
	if CheckBox2.Checked then begin
		Query_Update (Query1, 'GEPKOCSI',
		[
		'GK_LIVAN', likod,
		'GK_LICEG', ''''+lista_liceg[CBLiceg.ItemIndex]+'''',
		'GK_LIDAT', ''''+MaskEdit11.Text+'''',
		'GK_LIHON', IntToStr(StrToIntDef(MaskEdit12.Text,0)),
		'GK_LISZE', ''''+MaskEdit13.Text+'''',
		'GK_LIOSZ', SqlSzamString(StringSzam(MaskEdit14.Text),12,2),
		'GK_LIPER', periodlist[CBLiPeriod.ItemIndex],
		'GK_LINEM', ''''+CBLiVnem.Text+'''',
		'GK_VETAR', SqlSzamString(StringSzam(MaskEdit18.Text),12,2),
   	'GK_M_ERT', SqlSzamString(StringSzam(MaskEdit23.Text),12,2),
		'GK_VETVN', ''''+CBVetnem.Text+'''',
		'GK_LIMEG', ''''+MaskEdit15.Text+''''
		],
		' WHERE GK_KOD = '''+ret_kod+''' ' );
	end else begin
		Query_Update (Query1, 'GEPKOCSI',
		[
		'GK_LIVAN', '0',
		'GK_LICEG', ''''+''+'''',
		'GK_LIDAT', ''''+''+'''',
		'GK_LIHON', '0',
		'GK_LISZE', ''''+''+'''',
		'GK_LIOSZ', '0',
		'GK_LINEM', ''''+''+'''',
		'GK_VETAR', '0',
		'GK_VETVN', ''''+''+'''',
		'GK_LIMEG', ''''+''+''''
		],
		' WHERE GK_KOD = '''+ret_kod+''' ' );
	end;
	Query1.Close;
	// A garancia t�bla t�lt�se
  	Query_Run ( Query1, 'DELETE FROM GARANCIA WHERE GA_RENSZ = ''' +M2.Text+ ''' ',true);
	for i := 1 to SG1.RowCount -1 do begin
  		if SG1.Cells[3,i] = 'Igen' then begin
     		ujkod	:= GetNextCode('GARANCIA', 'GA_GAKOD');
     		Query_Run(Query1, 'INSERT INTO GARANCIA ( GA_GAKOD, GA_RENSZ, GA_ALKOD, GA_DATUM ) VALUES '+
           	'( '+IntToStr(ujkod)+','''+M2.Text+''','''+SG1.Cells[1,i]+''','''+
              SG1.Cells[4,i]+''' )');
     end;
	end;
   SofornevKepzes(M2.Text);
  	Close;
end;

procedure TGepkocsibeDlg.MaskEditClick(Sender: TObject);
begin
	with Sender as TMaskEdit do begin
  		SelectAll;
  	end;
end;

procedure TGepkocsibeDlg.Modosit(Sender: TObject);
begin
	modosult := true;
	// A t�li norma sz�m�t�sa
	M5.Text	 	:= Format('%.2f',[StringSzam(M51.Text)*1.03]);
	M52.Text 	:= Format('%.2f',[StringSzam(M53.Text)*1.03]);
end;

procedure TGepkocsibeDlg.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
	EgyebDlg.FormReturn(Key);
end;

procedure TGepkocsibeDlg.MaskKeyPress(Sender: TObject; var Key: Char);
begin
  	with Sender as TMaskEdit do begin
  		Key := EgyebDlg.Jochar(Text,Key,MaxLength,Tag);
  	end;
end;

procedure TGepkocsibeDlg.MaskExit(Sender: TObject);
begin
	SzamExit(Sender);
end;

procedure TGepkocsibeDlg.MClick(Sender: TObject);
begin
	with Sender as TMaskEdit do begin
		SelectAll;
	end;
end;

procedure TGepkocsibeDlg.BitBtn2Click(Sender: TObject);
begin
	EgyebDlg.Calendarbe(M10);
end;

procedure TGepkocsibeDlg.BitBtn1Click(Sender: TObject);
begin
	EgyebDlg.Calendarbe(M11);
end;

procedure TGepkocsibeDlg.MDatumExit(Sender: TObject);
begin
	DatumExit(Sender, false);
end;

procedure TGepkocsibeDlg.BitBtn26Click(Sender: TObject);
var
	GepformDlg2: TGepkocsiUjFmDlg;
begin
	Application.CreateForm(TGepkocsiUjFmDlg,GepFormDlg2);
	GepFormDlg2.valaszt	:= true;
	GepFormDlg2.ShowModal;
	if GepFormDlg2.ret_vkod <> '' then begin
		{Ellen�rizz�k, hogy p�tkocsi lett-e kiv�lasztva}
		Query_Run(EgyebDlg.QueryKOZOS,'SELECT * FROM GEPKOCSI WHERE GK_KOD = '''+GepFormDlg2.ret_vkod+''' ',true);
		if EgyebDlg.QueryKOZOS.RecordCount > 0 then begin
			if not EgyebDlg.QueryKOZOS.FieldByname('GK_POTOS').AsInteger > 0 then begin
				NoticeKi('Csak p�tkocsit lehet kiv�lasztani!');
				MaskEdit5.Text := '';
				GepFormDlg2.Destroy;
				EgyebDlg.QueryKOZOS.Close;
				Exit;
			end;
		end;
		EgyebDlg.QueryKOZOS.Close;
		MaskEdit5.Text := GepFormDlg2.ret_vnev;
		MaskEdit6.Text := GepFormDlg2.ret_vkod;
	end;
	GepFormDlg2.Destroy;
end;

procedure TGepkocsibeDlg.BitBtn37Click(Sender: TObject);
begin
	MaskEdit5.Text	:= '';
	MaskEdit6.Text	:= '';
end;

procedure TGepkocsibeDlg.ButtonModClick(Sender: TObject);
begin
	// A garancia adatok m�dos�t�sa
  if M11A.Text <> '' then begin
     if DatumExit( M11A, true ) then begin
        SG1.Cells[3,SG1.Row] := 'Igen';
        SG1.Cells[4,SG1.Row] := M11A.Text;
     end;
     modosult  := true;
  end;
end;

procedure TGepkocsibeDlg.ButtonTorClick(Sender: TObject);
begin
	// A garancia adatok t�rl�se
  SG1.Cells[3,SG1.Row] := 'Nem';
  SG1.Cells[4,SG1.Row] := '';
end;

procedure TGepkocsibeDlg.BitBtn3Click(Sender: TObject);
begin
	EgyebDlg.Calendarbe(M11A);
end;

procedure TGepkocsibeDlg.SG1Click(Sender: TObject);
begin
	MezoFrissito;
end;

procedure TGepkocsibeDlg.MezoFrissito;
begin
  M11A.Text	:= SG1.Cells[4,SG1.Row];
end;

procedure TGepkocsibeDlg.SG1MouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
	MezoFrissito;
end;

procedure TGepkocsibeDlg.M11AExit(Sender: TObject);
begin
	DatumExit(Sender, false);
end;

procedure TGepkocsibeDlg.Sofortolto(kod : string);
var
	sorsz	: integer;
begin
	BitBtn4.Enabled		:= false;
	SG2.RowCount 		:= 2;
	SG2.Rows[1].Clear;
	if M2.Text <> '' then begin
		Query_Run(Query1, 'SELECT * FROM DOLGOZO WHERE DO_RENDSZ = '''+M2.Text+''' AND DO_ARHIV = 0 ORDER BY DO_KOD');
		if Query1.RecordCount > 0 then begin
			sorsz			:= 1;
			while not Query1.Eof do begin
				SG2.RowCount := sorsz + 1;
				SG2.Cells[0, sorsz]	:= IntToStr(sorsz);
				SG2.Cells[1, sorsz]	:= Query1.FieldByName('DO_KOD').AsString;
				SG2.Cells[2, sorsz]	:= Query1.FieldByName('DO_BELEP').AsString;
				SG2.Cells[3, sorsz]	:= Query1.FieldByName('DO_NAME').AsString;
				Inc(sorsz);
				Query1.Next;
			end;
			BitBtn4.Enabled	:= true;
			SG2.Row			:= 1;
			if kod <> '' then begin
				sorsz		:= SG2.Cols[1].IndexOf(kod);
				if sorsz >= 1 then begin
					SG2.Row	   	:= sorsz;
				end;
(*
				while ( ( SG2.Cells[1, sorsz] <> kod ) and (sorsz < SG2.RowCount) ) do begin
					Inc(sorsz);
				end;
				SG2.Row	   	:= sorsz;
*)
			end;
		end;
	end;
	if ret_kod = '' then begin
		ButtonUj.Enabled	:= false;
		BitBtn4.Enabled		:= false;
   end;
end;


procedure TGepkocsibeDlg.ButtonUjClick(Sender: TObject);
var
	regirsz	: string;
begin
	// �j sof�r hozz�rendel�se a g�pkocsihoz
	Application.CreateForm(TDolgozoUjFmDlg,DolgozoUjFmDlg);
	DolgozoUjFmDlg.valaszt	:= true;
	DolgozoUjFmDlg.ShowModal;
	if DolgozoUjFmDlg.ret_vkod <> '' then begin
		{Ellen�rizz�k, hogy a kiv�lasztott sof�rh�z tartozik-e m�r aut�}
		Query_Run(Query1,'SELECT DO_RENDSZ FROM DOLGOZO WHERE DO_KOD = '''+DolgozoUjFmDlg.ret_vkod+''' ',true);
		if Query1.RecordCount > 0 then begin
			regirsz	:= '';
			if ( ( Query1.FieldByname('DO_RENDSZ').AsString <> '' ) and
				 ( Query1.FieldByname('DO_RENDSZ').AsString <> M2.Text ) ) then begin
				if NoticeKi('A kiv�lasztott sof�rh�z m�r tartozik egy g�pkocsi! Lecser�lj�k a megl�v� elemet?', NOT_QUESTION) <> 0 then begin
					DolgozoUjFmDlg.Destroy;
					Exit;
				end;
				regirsz	:= Query1.FieldByname('DO_RENDSZ').AsString;
			end;
			// A sof�rk�d bejegyz�se
			Query_Update (Query1, 'DOLGOZO',
				[
				'DO_RENDSZ', 	''''+M2.Text+'''',
				'DO_GKKAT',     ''''+Query_Select('GEPKOCSI', 'GK_RESZ', M2.Text, 'GK_GKKAT')+''''
				], ' WHERE DO_KOD = '''+DolgozoUjFmDlg.ret_vkod+''' ' );
			Sofortolto(DolgozoUjFmDlg.ret_vkod);
			SofornevKepzes(M2.Text);
			if regirsz	<> '' then begin
				SofornevKepzes(regirsz);
			end;
		end else begin
			// Nincs meg a visszaadott dolgoz�??!!
		end;
	end;
	DolgozoUjFmDlg.Destroy;
end;

procedure TGepkocsibeDlg.BitBtn4Click(Sender: TObject);
begin
	if NoticeKi('Val�ban t�r�lni k�v�nja a kijel�lt sort?', NOT_QUESTION) <> 0 then begin
       Exit;
   end;
	Query_Run(Query1,'UPDATE DOLGOZO SET DO_RENDSZ = '''', DO_GKKAT = '''' WHERE DO_KOD = '''+SG2.Cells[1,SG2.Row]+''' ',true);
   Sofortolto('');
   SofornevKepzes(M2.Text);
end;

procedure TGepkocsibeDlg.CBLiPeriodChange(Sender: TObject);
begin
  SetPeriodLabel;
end;

procedure TGepkocsibeDlg.CheckBox2Click(Sender: TObject);
begin
	if CheckBox2.Checked then begin
		// Van lizing
		CBLiceg.Enabled		:= true;
		MaskEdit11.Enabled	:= true;
		MaskEdit12.Enabled	:= true;
		MaskEdit13.Enabled	:= true;
		MaskEdit14.Enabled	:= true;
		MaskEdit15.Enabled	:= true;
		CBLiVnem.Enabled	:= true;
    CBLiPeriod.Enabled	:= true;
		MaskEdit18.Enabled	:= true;
		MaskEdit23.Enabled	:= true;
		CBVetnem.Enabled	:= true;
		BitBtn5.Enabled		:= true;
		GroupBox3.Enabled	:= true;
		CheckBox7.Enabled	:= true;
	end else begin
		// Nincs lizing
		CBLiceg.Enabled	:= false;
		MaskEdit11.Enabled	:= false;
		MaskEdit12.Enabled	:= false;
		MaskEdit13.Enabled	:= false;
		MaskEdit14.Enabled	:= false;
		MaskEdit15.Enabled	:= false;
		CBLiVnem.Enabled	:= false;
    CBLiPeriod.Enabled	:= false;
		MaskEdit18.Enabled	:= false;
		MaskEdit23.Enabled	:= false;
		CBVetnem.Enabled	:= false;
		BitBtn5.Enabled		:= false;
		GroupBox3.Enabled	:= false;
		CheckBox7.Enabled	:= false;
	end;
end;

procedure TGepkocsibeDlg.BitBtn5Click(Sender: TObject);
begin
	EgyebDlg.Calendarbe(MaskEdit11);
end;

procedure TGepkocsibeDlg.MaskEdit11Exit(Sender: TObject);
begin
	DatumExit(Sender, false);
end;

procedure TGepkocsibeDlg.BitBtn30Click(Sender: TObject);
begin
	if M2.Text = '' then begin
		NoticeKi('A rendsz�m m�g nincs kit�ltve!');
		Exit;
	end;
	Application.CreateForm(TLizingbeDlg,LizingbeDlg);
	LizingbeDlg.Tolto('�j l�zing befizet�s r�gz�t�se', '', M2.Text, MaskEdit14.Text, CBLiVnem.Text);
	LizingbeDlg.ShowModal;
	if LizingbeDlg.ret_kod <> '' then begin
		lizingTolto;
	end;
	LizingbeDlg.Destroy;
end;

procedure TGepkocsibeDlg.lizingTolto;
begin
	// A befizetett l�zingek megjelen�t�se
	BitBtn31.Enabled	:= false;
	BitBtn32.Enabled	:= false;
	if M2.Text <> '' then begin
		Query_Run(QueryLizing, 'SELECT * FROM LIZING WHERE LZ_RENSZ = '''+M2.Text+''' ORDER BY LZ_DATUM ');
		if QueryLizing.RecordCount > 0 then begin
			BitBtn31.Enabled	:= true;
			BitBtn32.Enabled	:= true;
		end;
	end;
end;

procedure TGepkocsibeDlg.KmValtTolto;
begin
	// A km v�ltoztat�sok megjelen�t�se
	BitBtn41.Enabled	:= false;
	BitBtn42.Enabled	:= false;
	if M2.Text <> '' then begin
		Query_Run(QueryKmValt, 'SELECT * FROM KMCHANGE WHERE KC_RENSZ = '''+M2.Text+''' ORDER BY KC_DATUM ');
		if QueryKmValt.RecordCount > 0 then begin
			BitBtn41.Enabled	:= true;
			BitBtn42.Enabled	:= true;
		end;
	end;
end;


procedure TGepkocsibeDlg.BitBtn31Click(Sender: TObject);
begin
	Application.CreateForm(TLizingbeDlg,LizingbeDlg);
	LizingbeDlg.Tolto('L�zing befizet�s m�dos�t�sa', QueryLizing.FieldByName('LZ_LZKOD').AsString, M2.Text, MaskEdit14.Text, CBLiVnem.Text);
	LizingbeDlg.ShowModal;
	if LizingbeDlg.ret_kod <> '' then begin
		lizingTolto;
	end;
	LizingbeDlg.Destroy;
end;

procedure TGepkocsibeDlg.BitBtn32Click(Sender: TObject);
begin
	if NoticeKi('val�ban t�r�lni k�v�nja a kijel�lt l�zing befizet�st?', NOT_QUESTION) = 0 then begin
		Query_Run(Query1, 'DELETE FROM LIZING WHERE LZ_LZKOD = '+Querylizing.FieldByName('LZ_LZKOD').AsString);
		LizingTolto;
	end;
end;

procedure TGepkocsibeDlg.BitBtn21Click(Sender: TObject);
begin
	// A l�zingbefizet�sek list�ja
	Application.CreateForm(TLizinglistDlg, LizinglistDlg);
	LizinglistDlg.Tolt(M2.Text);
	LizinglistDlg.Rep.Preview;
	LizinglistDlg.Destroy;
end;

procedure TGepkocsibeDlg.BitBtn6Click(Sender: TObject);
var
	dir0	: string;
	dir		: string;
begin
	// A g�pkocsihoz tartoz� dokumentumok megjelen�t�se
	dir0	:= EgyebDlg.Read_SZGrid('999', '721');
	if dir0 = '' then begin
		// Be kell olvasni a kezd�k�nyvt�rat
		dir	:= '';
		if SelectDirectory('A g�pkocsik k�nyvt�ra ...', '', dir ) then begin
			dir0	:= dir;
			// A k�nyvt�r be�r�sa a sz�t�rba
			if Length(dir0) > 120 then begin
				NoticeKi('T�l hossz� k�nyvt�rn�v : '+dir0);
				Exit;
			end;
			Query_Run(EgyebDlg.QueryKOZOS, 'UPDATE SZOTAR SET SZ_MENEV = '''+dir0+''' '+
				' WHERE SZ_FOKOD = ''999'' AND SZ_ALKOD = ''721'' ', TRUE);
			EgyebDlg.Fill_SZGrid;
		end else begin
			NoticeKi('Hiba a g�pkocsi k�nyvt�r megad�s�n�l!');
			Exit;
		end;
	end else begin
		if not Sysutils.DirectoryExists(dir0) then begin
			NoticeKi('Nem l�tez� alapk�nyvt�r : '+dir0);
			Exit;
		end;
	end;
	// A DIR0 egy l�tez� k�nyvt�r
	if ( ( copy(dir0, Length(dir0), 0) <> '\' ) and ( copy(dir0, Length(dir0), 0) <> '/' ) ) then begin
		dir0	:= dir0 + '\';
	end;
	// A n�v hozz�rak�sa a dir0 -hoz
	dir0	:= dir0 + M2.text;
	if not Sysutils.DirectoryExists(dir0) then begin
    if not CreateDir(dir0) then
    begin
		  NoticeKi('Nem l�tez�  g�pkocsi k�nyvt�r : "'+dir0+'"');
		  Exit;
    end;
	end;
//	if (not DirectoryExists(dir0+'\Archiv'))and(not DirectoryExists(dir0+'\Arch�v')) then begin
	if (not Sysutils.DirectoryExists(dir0+'\Arch�v')) then begin
    if not CreateDir(dir0+'\Arch�v') then
    begin
		  NoticeKi('Nem l�tez�  Arch�v k�nyvt�r : "'+dir0+'\Arch�v'+'"');
		 // Exit;
    end;
	end;
	if not Sysutils.DirectoryExists(dir0+'\Foto') then begin
    if not CreateDir(dir0+'\Foto') then
    begin
		  NoticeKi('Nem l�tez�  Foto k�nyvt�r : "'+dir0+'\Foto'+'"');
		 // Exit;
    end;
	end;
	if not Sysutils.DirectoryExists(dir0+'\Gumi') then begin
    if not CreateDir(dir0+'\Gumi') then
    begin
		  NoticeKi('Nem l�tez�  Gumi k�nyvt�r : "'+dir0+'\Gumi'+'"');
		 // Exit;
    end;
	end;
	// Nyissuk meg a k�nyvt�rat
	ShellExecute(Application.Handle,'open',PChar(dir0), '', '', SW_SHOWNORMAL );
end;

procedure TGepkocsibeDlg.ButtonUj2Click(Sender: TObject);
begin
	if M2.Text = '' then begin
		NoticeKi('A rendsz�m m�g nincs kit�ltve!');
		Exit;
	end;
	Application.CreateForm(TKmValtbeDlg,KmValtbeDlg);
	KmValtbeDlg.rendszam	:= M2.Text;
	KmValtbeDlg.Tolto('�j km v�ltoz�s r�gz�t�se', '');
	KmValtbeDlg.ShowModal;
	if KmValtbeDlg.ret_kod <> '' then begin
		KmValtTolto;
	end;
	KmValtbeDlg.Destroy;
end;

procedure TGepkocsibeDlg.BitBtn41Click(Sender: TObject);
begin
	if M2.Text = '' then begin
		NoticeKi('A rendsz�m m�g nincs kit�ltve!');
		Exit;
	end;
	Application.CreateForm(TKmValtbeDlg,KmValtbeDlg);
	KmValtbeDlg.rendszam	:= M2.Text;
	KmValtbeDlg.Tolto('Km v�ltoz�s m�dos�t�sa', QueryKmValt.FieldByName('KC_KCKOD').AsString);
	KmValtbeDlg.ShowModal;
	if KmValtbeDlg.ret_kod <> '' then begin
		KmValtTolto;
	end;
	KmValtbeDlg.Destroy;
end;

procedure TGepkocsibeDlg.BitBtn42Click(Sender: TObject);
begin
	if NoticeKi('val�ban t�r�lni k�v�nja a kijel�lt km v�ltoz�st?', NOT_QUESTION) = 0 then begin
		Query_Run(Query1, 'DELETE FROM KMCHANGE WHERE KC_KCKOD = '+QueryKmValt.FieldByName('KC_KCKOD').AsString);
		KmValtTolto;
	end;
end;

procedure TGepkocsibeDlg.BitBtn7Click(Sender: TObject);
begin
 if MaskEdit16.Text<>'' then
  EgyebDlg.GoogleMap(M2.Text,'','','',False)     ;
end;

procedure TGepkocsibeDlg.BitBtn8Click(Sender: TObject);
begin
	EgyebDlg.Calendarbe(M11S);
end;

procedure TGepkocsibeDlg.BitBtn9Click(Sender: TObject);
begin
	EgyebDlg.Calendarbe(M11K);
end;

procedure TGepkocsibeDlg.TBN1Change(Sender: TObject; NewTab: Integer;
  var AllowChange: Boolean);
var
  i: integer;
  S: string;
begin
  if NewTab= 3 then begin
    ADOQuery1.Close;
    ADOQuery1.Parameters[0].Value:=  M2.Text;
    ADOQuery1.Open;
    ADOQuery1.First;
    end;

  if NewTab= 4 then begin        // Serviz
    S:='select si_kod, si_jelleg from service ';
    if CBKUL.Checked then begin       // NEM c�ges
      S:=S+' where SI_KULSO = 1'; // csak amelyek enged�lyezve vannak nem c�ges g�pkocsiknak is
      end;
    S:=S+'order by si_jelleg';
    Query_Run(SJelleg, S);
    SetLength(SERVTIP,SJelleg.RecordCount);
    TabControl1.Tabs.Clear;
    i:=0;
    while not SJelleg.Eof do begin
      TabControl1.Tabs.Add(SJellegsi_jelleg.Value) ;
      SERVTIP[i]:=SJellegsi_kod.Value;
      inc(i);
      SJelleg.Next;
      end;
    SJelleg.Close;
    TabControl1.TabIndex:=0;
    TabControl1.OnChange(self);
    end;  // NewTab = 4
end;

procedure TGepkocsibeDlg.BitBtn10Click(Sender: TObject);
begin
	EgyebDlg.Calendarbe(MaskEdit22);
end;

procedure TGepkocsibeDlg.CheckBox6Click(Sender: TObject);
begin
      MaskEdit22.Enabled:=CheckBox6.Checked;
      BitBtn10.Enabled:=CheckBox6.Checked;
      if not CheckBox6.Checked then
        MaskEdit22.Text:='';
end;

procedure TGepkocsibeDlg.RadioGroup1Click(Sender: TObject);
var
  i: integer;
  gkkat: string;
begin
	if RadioGroup1.ItemIndex<2 then begin
  	MaskEdit5.Text	:= '';
     MaskEdit6.Text := '';
     BitBtn26.Enabled	:= false;
  end else begin
	  BitBtn26.Enabled	:= true;
  end;
  {
  if RadioGroup1.ItemIndex=1 then
  begin
    GroupBox7.Visible:=RadioGroup1.ItemIndex=1;
  end
  else
  begin
    gkkat:= '['+lista_kateg[CBKateg.ItemIndex]+']';
    GroupBox7.Visible:=(pos(gkkat,ATEGOTIP)>0);
  end;

  if RadioGroup1.ItemIndex=1 then
  begin
    GroupBox7.Caption:='P�tkocsi szerviz';
    ComboBox1.Items.Clear;
    ComboBox1.Items.Add('500');
    for i:=1 to 10 do
    begin
      ComboBox1.Items.Add(IntToStr(i*3000)) ;
    end;
  end;
  if RadioGroup1.ItemIndex<>1 then
  begin
    GroupBox7.Caption:='Atego szerviz' ;
    ComboBox1.Items.Clear;
    for i:=1 to 40 do
    begin
      ComboBox1.Items.Add(IntToStr(i*50000)) ;
    end;
  end;
      }
end;

procedure TGepkocsibeDlg.TabControl1Change(Sender: TObject);
begin
  Service.Close;
  Service.Parameters[0].Value:=SERVTIP[TabControl1.TabIndex];
  Service.Parameters[1].Value:=M2.Text;
  Service.Open;
  Service.First;
  SZERVINT:=ServiceSH_SERVINT.AsInteger;
  JELZES:=ServiceSH_JELZES.AsInteger;
  //STNEV:= SERVTIPNEV[TabControl1.TabIndex];
end;

procedure TGepkocsibeDlg.ServiceAfterInsert(DataSet: TDataSet);
begin
  ServiceSH_RENSZ.Value:=M2.Text;
  ServiceSH_KOD.Value:=SERVTIP[TabControl1.TabIndex];
  ServiceSH_DATUM.Value:=DateToStr(date);
  ServiceSH_SERVINT.Value:=SZERVINT;
  ServiceSH_JELZES.Value:=JELZES;
  ServiceSH_JELLEG.Value:=TabControl1.Tabs[TabControl1.tabindex];
  //ServiceSH_GKTIP.Value:=STNEV;
  DBGrid4.SelectedIndex:=1;
  DBGrid4.SetFocus;
end;

procedure TGepkocsibeDlg.ServiceCalcFields(DataSet: TDataSet);
begin
//  ServiceSH_GKTIP.Value:=STNEV;
end;

procedure TGepkocsibeDlg.BitBtn12Click(Sender: TObject);
begin
  Service.Append;
end;

procedure TGepkocsibeDlg.BitBtn14Click(Sender: TObject);
begin
  if Service.State=dsInsert then
  begin
    DBGrid4.SetFocus;
    exit;
  end;
	if NoticeKi('Val�ban t�r�lni k�v�nja a t�telt?', NOT_QUESTION) = 0 then
  begin
    Service.Delete;
	end;
  DBGrid4.SetFocus;

end;

procedure TGepkocsibeDlg.DBGrid4KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (ssCtrl in Shift) and (Key=VK_DELETE) then begin
    Key:=0;
  end;
  if Key=VK_DOWN then
  begin
    Service.Next;
    if not Service.Eof then
    begin
      Service.Prior;
    end
    else
    begin
      Service.last;
      Key:=0;
    end;
  end;

  if Key=VK_ESCAPE then
    if Service.State in [dsInsert,dsEdit] then
      Service.Cancel ;
end;

procedure TGepkocsibeDlg.DBGrid4KeyPress(Sender: TObject; var Key: Char);
begin
  if (Key=chr(VK_RETURN)) and (DBGrid4.SelectedIndex>=DBGrid4.Columns.Count-1)  then
  begin
    if Service.State in [dsInsert,dsEdit] then
      Service.Post;
  end  ;
  if (Key=chr(VK_RETURN)) then
  begin
    //if (IBVerseny.State in [dsInsert,dsEdit])and IBVerseny.Modified then
     // IBVerseny.Post;
    if (DBGrid4.SelectedIndex<DBGrid4.Columns.Count-1) then
      DBGrid4.SelectedIndex:=DBGrid4.SelectedIndex+1
    else
      DBGrid4.SelectedIndex:=0;
  end;

end;

procedure TGepkocsibeDlg.ServiceSH_SERVINTChange(Sender: TField);
begin
 if (ServiceSH_SERVKM.AsInteger<>0)and(ServiceSH_SERVINT.AsInteger<>0) then
  ServiceSH_SERVKM2.Value:=ServiceSH_SERVKM.Value+ServiceSH_SERVINT.Value
 else
  ServiceSH_SERVKM2.Value:=0;
end;

procedure TGepkocsibeDlg.ServiceSH_SERVKMChange(Sender: TField);
begin
 if ServiceSH_SERVKM2.Value=0 then
  ServiceSH_SERVKM2.Value:=ServiceSH_SERVKM.Value+ServiceSH_SERVINT.Value;
end;

procedure TGepkocsibeDlg.ServiceAfterPost(DataSet: TDataSet);
begin
  SZERVINT:=ServiceSH_SERVINT.AsInteger;
  JELZES:=ServiceSH_JELZES.AsInteger;
end;

procedure TGepkocsibeDlg.ServiceBeforePost(DataSet: TDataSet);
begin
  if ServiceSH_SERVKM2.AsInteger=0 then
    ServiceSH_JELZES.Value:=0;
end;

procedure TGepkocsibeDlg.DBGrid4ColEnter(Sender: TObject);
begin
  if (DBGrid4.Columns[DBGrid4.SelectedIndex].PickList.Count > 0)and(Service.State in [dsInsert,dsEdit]) then
  begin
    keybd_event(VK_F2,0,0,0);
    keybd_event(VK_F2,0,KEYEVENTF_KEYUP,0);
    keybd_event(VK_MENU,0,0,0);
    keybd_event(VK_DOWN,0,0,0);
    keybd_event(VK_DOWN,0,KEYEVENTF_KEYUP,0);
    keybd_event(VK_MENU,0,KEYEVENTF_KEYUP,0);
    //Application.ProcessMessages;
  end;

end;

procedure TGepkocsibeDlg.DBGrid4DblClick(Sender: TObject);
begin
  if DBGrid4.SelectedField.Name='ServiceSH_DATUM' then
  begin
    if ServiceSH_DATUM.AsString<>'' then
      MEdit.Text:=ServiceSH_DATUM.AsString
    else
      MEdit.Text:=DateToStr(date);
        
  	if EgyebDlg.Calendarbe(MEdit) then
    begin
      if not( Service.State in [dsInsert,dsEdit]) then
        Service.Edit;
      ServiceSH_DATUM.Value:=MEdit.Text;
    end;
  end;
end;

end.
