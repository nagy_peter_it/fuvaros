unit OSzamlaFm;

interface

uses
	SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
	StdCtrls, Forms, ExtCtrls, Dialogs, J_FOFORMUJ, DB, ADODB, Buttons, Menus;

type
	TOSzamlaFormDlg = class(TJ_FOFORMUJDLG)
	 ButtonNyomtat: TBitBtn;
	 ButtonVegleges: TBitBtn;
	 SorNyomtat: TBitBtn;
	 Query3: TADOQuery;
	 ButtonStorno: TBitBtn;
	 Query4: TADOQuery;
	 Query5: TADOQuery;
	 ButtonKlon: TBitBtn;
    QueryKoz: TADOQuery;
    PopupMenu1: TPopupMenu;
    Ellenrzs1: TMenuItem;
    QueryAtcsat: TADOQuery;
    BitNavteszt: TBitBtn;
    BitNavEles: TBitBtn;
	  procedure FormCreate(Sender: TObject);override;
    procedure Adatlap(cim, kod : string);override;
	  procedure ButtonNyomtatClick(Sender: TObject);
	  function 	KeresChange(mezo, szoveg : string) : string; override;
	  procedure ButtonVeglegesClick(Sender: TObject);
	  procedure DataSource1DataChange(Sender: TObject; Field: TField);override;
	  procedure SorNyomtatClick(Sender: TObject);
	  procedure FormKeyDown(Sender: TObject; var Key: Word;
	   Shift: TShiftState); override;
	  procedure ButtonStornoClick(Sender: TObject);
	  procedure StornoReszszamla(regikod, regiszamla, ujszamlaszam : string);
	  procedure ViszonylatStorno(regikod, szamla : string; ujkod2 : integer);
    procedure ButtonKlonClick(Sender: TObject);
    procedure Atcsatolas(esakod,usakod: string);
    function  NAVAdatkuldes(tipus : integer) : boolean;
    procedure BitNavtesztClick(Sender: TObject);
    procedure BitNavElesClick(Sender: TObject);
	 private
		elotag : string;
	end;

var
	OSzamlaFormDlg : TOSzamlaFormDlg;

implementation

uses
	Egyeb, Oszamlabe, J_SQL, Kozos, Szamlanezo, Kozos_Local, XML_Export,
  StrUtils, J_NavOnline, J_Datamodule;

{$R *.DFM}

procedure TOSzamlaFormDlg.FormCreate(Sender: TObject);
begin
	Inherited FormCreate(Sender);
	AddSzuromezoRovid('SA_VEVONEV', 'SZFEJ2');
	AddSzuromezoRovid('SA_ATAD', 'SZFEJ2');
	ButtonNyomtat.Parent		:= PanelBottom;
	ButtonVegleges.Parent  		:= PanelBottom;
	ButtonStorno.Parent  		:= PanelBottom;
	ButtonKlon.Parent  			:= PanelBottom;
	Sornyomtat.Parent  			:= PanelBottom;
	BitNavTeszt.Parent 		    := PanelBottom;
	BitNavEles.Parent 		    := PanelBottom;
	width					   	:= DLG_WIDTH;
	height						:= DLG_HEIGHT;
	ButtonTor.Hide;
	ButtonNez.Visible			:= true;
	FelTolto('�j �sszes�tett sz�mla felvitele ', '�sszes�tett sz�mla adatok m�dos�t�sa ',
			'�sszes�tett sz�mla adatok list�ja', 'SA_UJKOD', 'Select * from SZFEJ2 ','SZFEJ2', QUERY_ADAT_TAG );
	elotag						:= EgyebDlg.Read_SZGrid('CEG', '310');
	EgyebDlg.SetADOQueryDatabase(Query3);
	EgyebDlg.SetADOQueryDatabase(Query4);
	EgyebDlg.SetADOQueryDatabase(Query5);
	EgyebDLg.SetADOQueryDatabase(QueryKOZ);
	EgyebDlg.SeTADOQueryDatabase(QueryAtcsat);
end;

procedure TOSzamlaFormDlg.Adatlap(cim, kod : string);
begin
	Application.CreateForm(TOszamlabeDlg, AlForm);
	Inherited Adatlap(cim, kod );
end;

procedure TOSzamlaFormDlg.ButtonNyomtatClick(Sender: TObject);
begin
	if not voltalt then begin
		voltalt 	:= true;
		Exit;
	end;
	Edit1.Text	:= '';
	// A sz�mla megjelen�t�se nyomtathat�an
	Application.CreateForm(TSzamlaNezoDlg, SzamlaNezoDlg);
	SzamlaNezoDlg.OsszesitettNyomtatas(StrToIntDef(Query1.FieldByName('SA_UJKOD').AsString,0), 1);
	SzamlaNezoDlg.Destroy;
	ModLocate (Query1, FOMEZO, Query1.FieldByName(FOMEZO).AsString );
end;

function TOSzamlaFormDlg.KeresChange(mezo, szoveg : string) : string;
var
  uelotag:string;
begin
	Result	:= szoveg;
   if UpperCase(mezo) = 'SA_KOD' then begin
    uelotag:=elotag;
    if OSzamlaFormDlg.ComboBox2.ItemIndex>0 then
    begin
      uelotag:=copy(OSzamlaFormDlg.ComboBox2.Text,3,2)+copy(elotag,3,1);
  		Result := uelotag + Format('%5.5d',[StrToIntDef(Edit1.Text,0)]);
    end
    else
    begin
      uelotag:='';
		  Result := Edit1.Text;
    end;
		//Result := elotag + Format('%5.5d',[StrToIntDef(szoveg,0)]);
   end;
end;

procedure TOSzamlaFormDlg.ButtonVeglegesClick(Sender: TObject);
var
	szakod,kidat,hiba	: string;
  skod, ManCode, sa_man, S: string;
   volthiba    : boolean;
   fl          : string;
   jsz         : TJSzamla;
   jakod       : string;
begin
	if not voltalt then begin
 		voltalt 	:= true;
 		Exit;
 	end;

   // KG 20111214   XML ellen�rz�s futtat�sa
   Screen.Cursor    := crHourGlass;
   Application.ProcessMessages;
	Query_Run( QueryKoz, 'SELECT VE_XMLSZ FROM VEVO WHERE V_KOD= '+Query1.FieldByName('SA_VEVOKOD').AsString, true );
	if QueryKoz.FieldByName('VE_XMLSZ').Value>0 then begin
 	    Application.CreateForm(TXML_ExportDlg, XML_ExportDlg);
       XML_ExportDlg.M3.text:= Query1.FieldByName('SA_UJKOD').AsString ;
       XML_ExportDlg.M50.text:= Query1.FieldByName('SA_VEVOKOD').AsString ;
       //XML_ExportDlg.CheckBox1.Checked:=True;
       if not XML_ExportDlg.XML_Generalas3  then begin
  	        if NoticeKi('XML gener�l�s ellen�rz�si HIBA! / Folytatja?', NOT_QUESTION) <> 0 then begin
    	        XML_ExportDlg.Destroy;
               Screen.Cursor    := crDefault;
               exit;
           end;
       end else begin
  	        NoticeKi('XML gener�l�s ellen�rz�se: OK!');
       end;
   end;
   ////////////////////////
   if Query1.FieldByName('SA_OSSZEG').AsFloat=0 then begin
  		NoticeKi('A sz�mla v�g�sszege 0 (nulla). Nem lehet v�gleges�teni!');
      exit;
   end;

 	Query_Run(Query2, 'SELECT * FROM SZSOR2 WHERE S2_S2KOD='+Query1.FieldByName('SA_UJKOD').AsString ,true);
   while not Query2.Eof do begin
       Query_Run(Query3, 'SELECT * FROM SZFEJ WHERE SA_UJKOD='+Query2.FieldByName('S2_UJKOD').AsString ,true);
       if Query3.FieldByName('SA_VEVOKOD').AsString <> Query1.FieldByName('SA_VEVOKOD').AsString then begin  // SZFEJ2 - SZFEJ    Vev�k�d �sszehasonl�t�sa
           ShowMessage('HIBA! A R�ssz�mla vev�je nem egyezik! '+Query2.FieldByName('S2_UJKOD').AsString);
           Screen.Cursor    := crDefault;
           exit;
       end;
       {  Ez itt nem j� - a sz�mla st�tusza m�g nincs �t�ll�tva!
       Query_Run(Query3, 'SELECT MB_MBKOD FROM VISZONY, MEGBIZAS WHERE MB_VIKOD = VI_VIKOD AND VI_UJKOD = '+Query2.Fieldbyname('S2_UJKOD').AsString);
       if Query3.FieldByName('MB_MBKOD').AsString='' then
            Query_Log_Str('Gy�jt�sz�mla v�gleges�t�s, �res MBKOD! UJKOD='+Query2.Fieldbyname('S2_UJKOD').AsString, 0, true);  // napl�zzuk
       MegbizasKieg(Query3.FieldByName('MB_MBKOD').AsString);
       }
       Query2.Next;
   end;
   Query2.First;

   ////////////////////////////////////////////////////
   hiba:= EgyebDlg.Szamlaszam_ell(copy(Query1.FieldByName('SA_KIDAT').AsString,1,4));
   if hiba<>'' then begin
  		if NoticeKi('Hi�nyz� sz�mlasz�m!'+#13+#10+hiba+#13+#10+'Folytatja?',NOT_QUESTION)<>0 then
       Screen.Cursor    := crDefault;
       exit;
   end;
   ////////////////////////////////////////////

	Edit1.Text	:= '';
	if NoticeKi('Val�ban v�gleges�teni k�v�nja az �sszes�tett sz�ml�t?', NOT_QUESTION ) = 0 then begin
		{Az �sszes�tett sz�mla v�gleges�t�se}
     szakod:= Query1.Fieldbyname('SA_KOD').AsString;   // ha m�r van k�d, azt haszn�ljuk
	 	 { 2017.12.11.
      szakod 	:= GetUjszamla2(copy(Query1.FieldByName('SA_KIDAT').AsString,3,2));
       if szakod='' then begin
           Screen.Cursor    := crDefault;
           exit;
       end;
       }

    /////// Ellen�rz�sek
    kidat:=Query1.FieldByName('SA_KIDAT').AsString ;
    if not EgyebDlg.Szamla_szam_datum_ell(szakod,kidat,True) then begin
       NoticeKi('L�tezik kisebb sz�mlasz�m nagyobb d�tummal!');
       Screen.Cursor    := crDefault;
       exit;
    end;

    Try
       sa_man:='0';  // alap�rtelmez�sben nem egyedi d�jas
       // Ha egyedi a megb�z�s, akkor a TEMAN sz�t kell megjelen�teni
       skod    := Query1.FieldByName('SA_FEJL1').AsString ;
       if Pos('(', skod) > 0 then begin
           skod := copy(skod, 1, Pos('(', skod) - 1);
       end;
       Query_Run(Query2, 'SELECT SUM(MB_EDIJ) ODIJ, COUNT(*) DB FROM MEGBIZAS, VISZONY WHERE MB_VIKOD = VI_VIKOD AND VI_UJKOD IN (SELECT S2_UJKOD FROM SZSOR2 WHERE S2_S2KOD = '+ Query1.FieldByName('SA_UJKOD').AsString+')');
       if StrToIntdef(Query2.FieldByName('ODIJ').AsString, 0) = 0 then begin
           // Nem egyedi a sz�mla!!! Nem kell csin�lni semmit egyel�re.
       end else begin
           if StrToIntdef(Query2.FieldByName('ODIJ').AsString, 0) <> StrToIntdef(Query2.FieldByName('DB').AsString, 0) then begin
               NoticeKi('A sz�ml�hoz tartoz� megb�z�sok k�z�l nem mindegyik egyedi!');
           end;
           // skod:= skod + ' (TEMAN)';
           // --- 2016-01-07
           if LehetEgyediDij(Query1.FieldByName('SA_VEVOKOD').AsString, ManCode) then begin
              if ManCode <> '' then
                skod:= skod + ' ('+ManCode+')';
              sa_man:='1';
              end
           else begin  // nem lehet egyedi d�j: meg kell szak�tani a v�gleges�t�st
               NoticeKi('Ehhez a partnerhez nem k�sz�thet�nk egyedi d�jas sz�ml�t!');
               exit;
              end;
       end;

 		 {if Query1.Fieldbyname('SA_KOD').AsString = '' then begin
			if not Query_Run (Query2, 'UPDATE SZFEJ2 SET SA_KOD = '''+szakod+''' WHERE SA_UJKOD = '+
     		Query1.Fieldbyname('SA_UJKOD').AsString, true) then Raise Exception.Create('');
		 end else begin
			szakod 	:= Query1.Fieldbyname('SA_KOD').AsString;
		 end;}
     if szakod = '' then begin
       szakod:= GetUjszamla2(copy(Query1.FieldByName('SA_KIDAT').AsString,3,2));
       if (szakod='0') or (szakod='') then begin
             S:= 'Sikertelen k�d gener�l�s, k�rem pr�b�lja �jra! (OsszesitettSzamlaVeglegesites)';
             HibaKiiro(S);
             NoticeKi(S);
             Exit;
             end;
       S:= 'UPDATE SZFEJ2 SET SA_KOD = '''+szakod+''' WHERE SA_UJKOD = '+Query1.Fieldbyname('SA_UJKOD').AsString;
	 		 if not Query_Run (Query2, S, true) then begin
          NemHasznaltSzamlaKod(szakod);  // Ez ideiglenes megold�s, hogy ne maradjon lyuk a sz�mlasz�mban. Igaz�b�l az eg�szet �t kellene �rni tranzakci�s megold�sra.
          Raise Exception.Create('Az �j sz�mlak�d be�ll�t�sa nem siker�lt! ('+szakod+') K�rem �rtes�tse a fejleszt�t!');
          end;
  	  end;

     ModLocate (Query1, 'SA_UJKOD', Query1.Fieldbyname('SA_UJKOD').AsString );

       // Ellen�rizz�k, hogy a sz�ml�t a NAV fel� k�ldeni kell-e?
       volthiba    := false;
       if ( ( Query1.Fieldbyname('SA_ORSZ2').AsString = 'HU' ) and (Query1.Fieldbyname('SA_KIDAT').AsString >= '2018.07.01.' ) ) then begin
           // �sszeg ellen�rz�se
           jsz     := TJSzamla.Create(Self);
           jsz.FillOsszSzamla( Query1.Fieldbyname('SA_UJKOD').AsString );
           if Abs(jsz.ossz_afa) >= 100000 then begin
               // Ellen�rizz�k hogy a kiad�s kisebb-e mint a mai d�tum
               if Query1.Fieldbyname('SA_KIDAT').AsString < EgyebDlg.MaiDatum then begin
                   if NoticeKi('Figyelem! A sz�mla kiad�si d�tuma kisebb mint a mai d�tum. Folytatja a NAV bek�ld�st?', NOT_QUESTION) <> 0 then begin
                       Exit;
                   end;
               end;
               if not NAVAdatkuldes(1) then begin
                     volthiba    := true;
               end else begin
                   Query_Run(Query2, 'SELECT SA_ESTAT FROM SZFEJ2 WHERE SA_KOD = '''+Query1.Fieldbyname('SA_KOD').AsString+''' ');
                   if Pos('DONE', Query2.Fieldbyname('SA_ESTAT').AsString) < 1 then begin
                       // Ha nem DONE -val z�rult, akkor sem lehet v�gleges�teni
                       volthiba    := true;
                   end;
               end;
           end;
       end;
       if volthiba then begin
           NoticeKi('Figyelem!  A v�gleges�t�s nem hajthat� v�gre! Pr�b�lja meg k�s�bb !');
           Exit;
       end;

       fl  := '0';
       if TRim(Query1.FieldByName('SA_STORN').AsString) <> '' then begin
           fl  := '3';
       end;



		 if not Query_Run (Query2, 'UPDATE SZFEJ2 SET SA_FLAG = '''+fl+''', SA_FEJL1 = '''+skod+''' , SA_MAN = '+sa_man+ ' WHERE SA_UJKOD = '+
			Query1.Fieldbyname('SA_UJKOD').AsString, true) then Raise Exception.Create('');
		 {V�gigmegy�nk a r�szsz�ml�kon + viszonyokon , �s azokn�l is be�ll�tjuk a sz�mlasz�mot}
		 Query_Run (Query2, 'SELECT * FROM SZSOR2 WHERE S2_S2KOD = '+Query1.Fieldbyname('SA_UJKOD').AsString, true);
		 while not Query2.EOF do begin
			if not Query_Run(Query3,'UPDATE SZFEJ SET SA_KOD='''+szakod+''', SA_FLAG = '''+fl+''', SA_ALLST = '''+GetSzamlaAllapot(StrToIntDef(fl, 0))+''' '+
				' WHERE SA_UJKOD='+ Query2.Fieldbyname('S2_UJKOD').AsString, true) then Raise Exception.Create('');
			if not Query_Run(Query3,'UPDATE SZSOR SET SS_KOD='''+szakod+''' WHERE SS_UJKOD='+
        	Query2.Fieldbyname('S2_UJKOD').AsString, true) then Raise Exception.Create('');

       if not EgyebDlg.Szamlaertek2jar_megb(Query2.Fieldbyname('S2_UJKOD').AsString,szakod) then Raise Exception.Create('') ;     // �sszeg, sz�mlasz�m vissza�r�sa a Viszonyba �s a megb�z�sba.

       Query_Run(Query3, 'SELECT MB_MBKOD FROM VISZONY, MEGBIZAS WHERE MB_VIKOD = VI_VIKOD AND VI_UJKOD = '+Query2.Fieldbyname('S2_UJKOD').AsString);
       if Trim(Query3.FieldByName('MB_MBKOD').AsString) = '' then
            Query_Log_Str('Gy�jt�sz�mla v�gleges�t�s, �res MBKOD! UJKOD='+Query2.Fieldbyname('S2_UJKOD').AsString, 0, true);  // napl�zzuk
       MegbizasKieg(Query3.FieldByName('MB_MBKOD').AsString);

			Query2.Next;
		end;
    Except
  		NoticeKi('A sz�mla v�gleges�t�se nem siker�lt!');
    END;

    ///////////////////////////////////////////////////////////////////
    ModLocate (Query1, FOMEZO, Query1.FieldByName(FOMEZO).AsString );

    /////  XML l�trehoz�sa ha kell ?????
    if QueryKoz.FieldByName('VE_XMLSZ').Value > 0 then begin
       XML_ExportDlg.M3.text:= RightStr(szakod,4);
       XML_ExportDlg.kelluzenet:= False;
       if not XML_ExportDlg.XML_Generalas  then begin

       end;
       XML_ExportDlg.Destroy;
    end;
    //////////////////////////////
  end;
  Screen.Cursor    := crDefault;

end;

procedure TOSzamlaFormDlg.DataSource1DataChange(Sender: TObject; Field: TField);
begin
	// A nyom�gombok szab�lyoz�sa
	case StrToIntDef(Query1.FieldByName('SA_FLAG').AsString,0)  of
  	0:                  // v�gleges�tett
		begin
		{A sz�mla nem m�dos�that�}
		ButtonMod.Enabled := false;
		ButtonMod.Hide;
		{A sz�mla stornozhat�}
		ButtonStorno.Enabled := true;
		ButtonStorno.Show;
		ButtonKlon.Enabled := false;
		ButtonKlon.Hide;
		{A sz�mla nyomtathat�}
		ButtonNyomtat.Enabled := true;
		ButtonNyomtat.Show;
		SorNyomtat.Enabled := true;
		SorNyomtat.Show;
		{A sz�mla nem v�gleges�that�}
		ButtonVegleges.Enabled := false;
		ButtonVegleges.Hide;
		end;
	  1:            // v�gleges�t�sre v�r
		begin
		{A sz�mla m�dos�that�}
		ButtonMod.Enabled := true;
		ButtonMod.Show;
		ButtonKlon.Enabled := false;
		ButtonKlon.Hide;
		{A sz�mla nem stornozhat�}
		ButtonStorno.Enabled := false;
		ButtonStorno.Hide;
		{A sz�mla nem nyomtathat�}
		ButtonNyomtat.Enabled := false;
		ButtonNyomtat.Hide;
		SorNyomtat.Enabled := false;
		SorNyomtat.Hide;
		{A sz�mla v�gleges�that�}
		ButtonVegleges.Enabled := true;
		ButtonVegleges.Show;
		end;
	  2:            // storn�zott
		begin
		{A sz�mla nem m�dos�that�}
		ButtonMod.Enabled := false;
		ButtonMod.Hide;
		ButtonKlon.Enabled := true;
		ButtonKlon.Show;
		{A sz�mla nem stornozhat�}
		ButtonStorno.Enabled := false;
		ButtonStorno.Hide;
		{A sz�mla nyomtathat�}
		ButtonNyomtat.Enabled := true;
		ButtonNyomtat.Show;
		SorNyomtat.Enabled := true;
		SorNyomtat.Show;
		{A sz�mla nem v�gleges�that�}
		ButtonVegleges.Enabled := false;
		ButtonVegleges.Hide;
		end;
	  3:          // storn�
		begin
		{A sz�mla nem m�dos�that�}
		ButtonMod.Enabled := false;
		ButtonMod.Hide;
		ButtonKlon.Enabled := false;
		ButtonKlon.Hide;
		{A sz�mla nem stornozhat�}
		ButtonStorno.Enabled := false;
		ButtonStorno.Hide;
		{A sz�mla nyomtathat�}
		ButtonNyomtat.Enabled := true;
		ButtonNyomtat.Show;
		SorNyomtat.Enabled := true;
		SorNyomtat.Show;
		{A sz�mla nem v�gleges�that�}
		ButtonVegleges.Enabled := false;
		ButtonVegleges.Hide;
		end;
  end;
  if ( SorNyomtat.Enabled and ( Query1.FieldByName('SA_PELDANY').AsInteger >= EgyebDlg.Peldanyszam) ) then begin
    	SorNyomtat.Enabled := false;
  end;
  inherited DataSource1DataChange(Sender, Field);
end;

procedure TOSzamlaFormDlg.SorNyomtatClick(Sender: TObject);
begin
	if not voltalt then begin
 		voltalt 	:= true;
 		Exit;
 	end;
	Edit1.Text	:= '';
	// Az �sszes sz�mlap�ld�ny kinyomtat�sa
	Application.CreateForm(TSzamlaNezoDlg, SzamlaNezoDlg);
	SzamlaNezoDlg.OsszesitettSorozat(StrToIntDef(Query1.FieldByName('SA_UJKOD').AsString,0));
	SzamlaNezoDlg.Destroy;
	ModLocate (Query1, FOMEZO, Query1.FieldByName(FOMEZO).AsString );
end;

procedure TOSzamlaFormDlg.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
	Inherited FormKeyDown(Sender, Key, Shift);
	if ( EgyebDlg.CtrlGomb and EgyebDlg.user_super )then begin
		voltalt	:= true;
		// Sz�mla m�dos�t�sa + p�ld�nysz�mot 0-ra �ll�tjuk
		// Az �tadotts�g ellen�rz�se
		if StrToIntDef(Query1.FieldByName('SA_ATAD').AsString, 0) > 0 then begin
			NoticeKi('A sz�mla m�r �tad�sra ker�lt!');
			Exit;
		end;
		if NoticeKi('Val�ban m�dos�tan� a sz�ml�t?', NOT_QUESTION) = 0 then begin
			Query_Run (Query2, 'UPDATE SZFEJ2 SET SA_FLAG = ''1'', SA_PELDANY = 0, SA_ATAD = 0 WHERE SA_UJKOD = '+ Query1.Fieldbyname('SA_UJKOD').AsString, true);
			// A r�szsz�ml�kban is m�dos�tjuk a v�gleges�t�st
			Query_Run(Query2,'UPDATE SZFEJ SET SA_FLAG = ''1'', SA_ALLST = '''+GetSzamlaAllapot(1)+''', SA_EDISENT = null '+
				' WHERE SA_UJKOD IN ( SELECT S2_UJKOD FROM SZSOR2 WHERE S2_S2KOD = '+Query1.Fieldbyname('SA_UJKOD').AsString+') ');
			ModLocate (Query1, FOMEZO, Query1.FieldByName(FOMEZO).AsString );
		end;
	end;
	if EgyebDlg.CtrlGomb2 then begin
		// P�ld�nysz�m vissza�ll�t�sa
		voltalt	:= true;
		if NoticeKi('Val�ban m�dos�tan� a p�ld�nysz�mot?', NOT_QUESTION) = 0 then begin
			Query_Run (Query2, 'UPDATE SZFEJ2 SET SA_PELDANY = '+
			IntToStr( StrToIntDef(InputBox('P�ld�nysz�m', 'Az �j p�ld�nysz�m :', '0'),0))+
			' WHERE SA_UJKOD = '+ Query1.Fieldbyname('SA_UJKOD').AsString, true);
			ModLocate (Query1, FOMEZO, Query1.FieldByName(FOMEZO).AsString );
		end;
	end;
(*
	if ( ( ssCtrl in Shift ) and (key = VK_F5) ) then begin
		if NoticeKi('Val�ban t�rli az �tad�s jelz�t?', NOT_QUESTION) = 0 then begin
			Query_Run (Query2, 'UPDATE SZFEJ SET SA_ATAD = 0 WHERE SA_KOD  = '''+ Query1.Fieldbyname('SA_KOD').AsString+''' ', true);
			Query_Run (Query2, 'UPDATE SZFEJ2 SET SA_ATAD = 0 WHERE SA_KOD = '''+ Query1.Fieldbyname('SA_KOD').AsString+''' ', true);
			ModLocate (Query1, FOMEZO, Query1.FieldByName(FOMEZO).AsString );
		end;
	end;
*)
end;

procedure TOSzamlaFormDlg.ButtonStornoClick(Sender: TObject);
var
	st, S 			: string;
	ujkodszam		: integer;
	szasor			: integer;
begin
	if not voltalt then begin
		voltalt 	:= true;
		Exit;
	end;
	Edit1.Text	:= '';
	if NoticeKi('Val�ban storn�zni akarja az aktu�lis sz�ml�t?', NOT_QUESTION) = 0 then begin
		Screen.Cursor	:= crHourglass;
		{Sz�mla stornoz�sa}
		Query_Run (Query2, 'UPDATE SZFEJ2 SET SA_FLAG = ''2'' WHERE SA_UJKOD = '+
			Query1.Fieldbyname('SA_UJKOD').AsString, true);

		// Query_Run(Query2, 'SELECT MAX(SA_UJKOD) MAXKOD FROM SZFEJ2 ', true);
		// ujkodszam 	:= Query2.FieldByName('MAXKOD').AsInteger + 1;
    ujkodszam:= GetNewIDLoop(modeGetNextCode, 'SZFEJ2', 'SA_UJKOD', '');
    if (ujkodszam<=0) then begin
     S:= 'Sikertelen k�d gener�l�s, k�rem pr�b�lja �jra! (SzamlaSztorno UJKOD)';
     HibaKiiro(S);
     NoticeKi(S);
     Exit;
     end;

		// L�trehozzuk az �j storn� sz�ml�t
		st	:= GetUjszamla2(copy(Query1.FieldByName('SA_KIDAT').AsString,3,2));
    // if st='' then exit;
    if (st='') or (st='0') then begin
       S:= 'Sikertelen k�d gener�l�s, k�rem pr�b�lja �jra! (OsszesitettStornoVeglegesites SAKOD)';
       HibaKiiro(S);
       NoticeKi(S);
       Exit;
       end;

		Query_Run(Query2, 'INSERT INTO SZFEJ2 (SA_UJKOD, SA_KOD, SA_FLAG ) '+
				'VALUES ('+IntToStr(ujkodszam)+','''+st+''', ''1'' ) ', true);
		{A fejl�c rekord felt�lt�se}
		Query_Update ( Query2, 'SZFEJ2',
		[
		'SA_STORN',		''''+Query1.FieldByName('SA_KOD').AsString +'''',
		'SA_ORSZ2',		''''+Query1.FieldByName('SA_ORSZ2').AsString +'''',
		'SA_DATUM',		''''+EgyebDlg.MaiDatum+'''',
		'SA_TEDAT',		''''+Query1.FieldByName('SA_TEDAT').AsString +'''',
		'SA_KIDAT',		''''+EgyebDlg.MaiDatum+'''',
		'SA_ESDAT',		''''+Query1.FieldByName('SA_ESDAT').AsString +'''',
		'SA_VEVOKOD',	''''+Query1.FieldByName('SA_VEVOKOD').AsString +'''',
		'SA_VEVONEV',	''''+Query1.FieldByName('SA_VEVONEV').AsString +'''',
		'SA_VEIRSZ',	''''+Query1.FieldByName('SA_VEIRSZ').AsString  +'''',
		'SA_VEVAROS',	''''+Query1.FieldByName('SA_VEVAROS').AsString +'''',
		'SA_VECIM',		''''+Query1.FieldByName('SA_VECIM').AsString	+'''',
		'SA_VEADO',		''''+Query1.FieldByName('SA_VEADO').AsString 	+'''',
		'SA_VEBANK',	''''+Query1.FieldByName('SA_VEBANK').AsString 	+'''',
		'SA_VEKEDV',	SqlSzamString(StringSzam(Query1.FieldByName('SA_VEKEDV').AsString),6,2),
		'SA_PELDANY',	'0',
		'SA_FIMOD',		''''+Query1.FieldByName('SA_FIMOD').AsString +'''',
		'SA_OSSZEG',	'-1*'+ SqlSzamString(StringSzam(Query1.FieldByName('SA_OSSZEG').AsString),12,2),
		'SA_AFA',		'-1*'+SqlSzamString(StringSzam(Query1.FieldByName('SA_AFA').AsString),12,2),
		'SA_MEGJ',		''''+Query1.FieldByName('SA_MEGJ').AsString 	+'''',
		'SA_JARAT',		''''+Query1.FieldByName('SA_JARAT').AsString 	+'''',
		'SA_POZICIO',	''''+Query1.FieldByName('SA_POZICIO').AsString 	+'''',
		'SA_RSZ',		''''+Query1.FieldByName('SA_RSZ').AsString 		+'''',
		'SA_MELL1',		''''+Query1.FieldByName('SA_MELL1').AsString	+'''',
		'SA_MELL2',		''''+Query1.FieldByName('SA_MELL2').AsString 	+'''',
		'SA_MELL3',		''''+Query1.FieldByName('SA_MELL3').AsString 	+'''',
		'SA_MELL4',		''''+Query1.FieldByName('SA_KOD').AsString +' sz�mla storn�ja!'+'''',
		'SA_MELL5',		''''+Query1.FieldByName('SA_MELL5').AsString 	+'''',
		'SA_FEJL1',		''''+Query1.FieldByName('SA_FEJL1').AsString	+'''',
		'SA_FEJL2',		''''+Query1.FieldByName('SA_FEJL2').AsString	+'''',
		'SA_FEJL3',		''''+Query1.FieldByName('SA_FEJL3').AsString	+'''',
		'SA_FEJL4',		''''+Query1.FieldByName('SA_FEJL4').AsString	+'''',
		'SA_FEJL5',		''''+Query1.FieldByName('SA_FEJL5').AsString	+'''',
		'SA_FEJL6',		''''+Query1.FieldByName('SA_FEJL6').AsString	+'''',
		'SA_FEJL7',		''''+Query1.FieldByName('SA_FEJL7').AsString	+'''',
		'SA_FOKON',		''''+Query1.FieldByName('SA_FOKON').AsString	+'''',
	    'SA_SAEUR',  	IntToStr(StrToIntDef(Query1.FieldByName('SA_SAEUR').AsString, 0)),
		'SA_ATAD',		'0',
       'SA_VALNEM',  ''''+Query1.FieldByName('SA_VALNEM').AsString	+'''',
		'SA_VALOSS',		'-1*'+SqlSzamString(StringSzam(Query1.FieldByName('SA_VALOSS').AsString),12,2),
		'SA_VALAFA',		'-1*'+SqlSzamString(StringSzam(Query1.FieldByName('SA_VALAFA').AsString),12,2),
		'SA_NYELV',		''''+Query1.FieldByName('SA_NYELV').AsString	+'''',
       'SA_VEFEJ',		''''+Query1.FieldByName('SA_VEFEJ').AsString	+'''',
		'SA_TIPUS',		SqlSzamString(StringSzam(Query1.FieldByName('SA_TIPUS').AsString),12,2)
		], ' WHERE SA_UJKOD = '+IntToStr(ujkodszam));

		{V�gigmegy�nk a r�szsz�ml�kon + viszonyokon , �s azokn�l is l�trehozzuk a storn� sz�ml�t}
		Query_Run (Query2, 'SELECT * FROM SZSOR2 WHERE S2_S2KOD = '+Query1.Fieldbyname('SA_UJKOD').AsString, true);
		while not Query2.EOF do begin
			StornoReszszamla(Query2.FieldByName('S2_UJKOD').AsString, Query1.FieldByName('SA_KOD').AsString , st);
			Query2.Next;
		end;
		// Begy�jtj�k a r�szsz�ml�kat az �sszes�tett sz�mla al�
		szasor	:= 1;
		Query_Run (Query2, 'SELECT * FROM SZFEJ WHERE SA_KOD = '''+st+''' ', true);
		while not Query2.EOF do begin
			Query_Run(Query3, 'INSERT INTO SZSOR2 (S2_S2KOD, S2_S2SOR, S2_UJKOD ) '+
				'VALUES ('+IntToStr(ujkodszam)+','+IntToStr(szasor)+', '+Query2.FieldByName('SA_UJKOD').AsString+' ) ', true);
			Inc(szasor);
			Query2.Next;
		end;

		Screen.Cursor	:= crDefault;
		{Az �j t�tel megjelen�t�se}
		ModLocate (Query1, FOMEZO, IntToStr(ujkodszam) );
	end;
end;

procedure TOSzamlaFormDlg.StornoReszszamla(regikod, regiszamla, ujszamlaszam : string);
var
	ujkodszam	: integer;
  S: string;
begin
	// Storno sz�mla defini�l�sa a r�gi sz�ml�hoz
	Query_Run(Query4,'UPDATE SZFEJ SET SA_FLAG =''2'', SA_ALLST = '''+GetSzamlaAllapot(2)+''' '+
		' WHERE SA_UJKOD='+regikod, true);
	{A storn� sz�mla elk�sz�t�se}
	// Query_Run(Query4, 'SELECT MAX(SA_UJKOD) MAXKOD FROM SZFEJ ', true);
	// ujkodszam 	:= Query4.FieldByName('MAXKOD').AsInteger + 1;
  ujkodszam := GetNewIDLoop(modeGetNextCode, 'SZFEJ', 'SA_UJKOD', '');
  if (ujkodszam<=0) then begin
     S:= 'Sikertelen k�d gener�l�s, k�rem pr�b�lja �jra! (StornoReszSzamla)';
     HibaKiiro(S);
     NoticeKi(S);
     Exit;
     end;
	Query_Run(Query4, 'INSERT INTO SZFEJ (SA_UJKOD, SA_KOD, SA_FLAG, SA_ALLST ) '+
				'VALUES ('+IntToStr(ujkodszam)+','''+ujszamlaszam+''', ''3'', '''+GetSzamlaAllapot(3)+''' ) ', true);
	{A fejl�c rekord felt�lt�se}
	Query_Run(Query5, 'SELECT * FROM SZFEJ WHERE SA_UJKOD = '+regikod, true);
	Query_Update ( Query4, 'SZFEJ',
		[
	  'SA_DATUM',	''''+EgyebDlg.MaiDatum+'''',
	  'SA_TEDAT',	''''+Query5.FieldByName('SA_TEDAT').AsString +'''',
	  'SA_KIDAT',	''''+EgyebDlg.MaiDatum+'''',
	  'SA_ESDAT',	''''+EgyebDlg.MaiDatum+'''',
	  'SA_VEVONEV',	''''+Query5.FieldByName('SA_VEVONEV').AsString +'''',
	  'SA_VEIRSZ',	''''+Query5.FieldByName('SA_VEIRSZ').AsString  +'''',
	  'SA_VEVAROS',	''''+Query5.FieldByName('SA_VEVAROS').AsString +'''',
	  'SA_VECIM',	''''+Query5.FieldByName('SA_VECIM').AsString	+'''',
	  'SA_VEADO',	''''+Query5.FieldByName('SA_VEADO').AsString 	+'''',
	  'SA_VEBANK',	''''+Query5.FieldByName('SA_VEBANK').AsString 	+'''',
	  'SA_VEKEDV',	SqlSzamString(StringSzam(Query5.FieldByName('SA_VEKEDV').AsString),6,2),
	  'SA_PELDANY',	'0',
	  'SA_FIMOD',	''''+Query5.FieldByName('SA_FIMOD').AsString +'''',
	  'SA_OSSZEG',	'-1*'+ SqlSzamString(StringSzam(Query5.FieldByName('SA_OSSZEG').AsString),12,2),
	  'SA_AFA',		'-1*'+SqlSzamString(StringSzam(Query5.FieldByName('SA_AFA').AsString),12,2),
	  'SA_MEGJ',	''''+Query5.FieldByName('SA_MEGJ').AsString +'''',
	  'SA_JARAT',	''''+Query5.FieldByName('SA_JARAT').AsString +'''',
	  'SA_POZICIO',	''''+Query5.FieldByName('SA_POZICIO').AsString +'''',
	  'SA_RSZ',		''''+Query5.FieldByName('SA_RSZ').AsString 	+'''',
	  'SA_MELL1',	''''+Query5.FieldByName('SA_MELL1').AsString	+'''',
	  'SA_MELL2',	''''+Query5.FieldByName('SA_MELL2').AsString +'''',
	  'SA_MELL3',	''''+Query5.FieldByName('SA_MELL3').AsString +'''',
	  'SA_MELL4',	''''+regiszamla +' sz�mla storn�ja!'+'''',
	  'SA_MELL5',	''''+Query5.FieldByName('SA_MELL5').AsString +'''',
	  'SA_FEJL1',	''''+Query5.FieldByName('SA_FEJL1').AsString	+'''',
	  'SA_FEJL2',	''''+Query5.FieldByName('SA_FEJL2').AsString	+'''',
	  'SA_FEJL3',	''''+Query5.FieldByName('SA_FEJL3').AsString	+'''',
	  'SA_FEJL4',	''''+Query5.FieldByName('SA_FEJL4').AsString	+'''',
	  'SA_FEJL5',	''''+Query5.FieldByName('SA_FEJL5').AsString	+'''',
	  'SA_FEJL6',	''''+Query5.FieldByName('SA_FEJL6').AsString	+'''',
	  'SA_FEJL7',	''''+Query5.FieldByName('SA_FEJL7').AsString	+'''',
	  'SA_FOKON',	''''+Query5.FieldByName('SA_FOKON').AsString	+'''',
	  'SA_VEVOKOD',	''''+Query5.FieldByName('SA_VEVOKOD').AsString	+'''',
	  'SA_EGYA1',	''''+Query5.FieldByName('SA_EGYA1').AsString	+'''',
	  'SA_EGYA2',	''''+Query5.FieldByName('SA_EGYA2').AsString	+'''',
	  'SA_EGYA3',	''''+Query5.FieldByName('SA_EGYA3').AsString	+'''',
	  'SA_MEGRE',	''''+Query5.FieldByName('SA_MEGRE').AsString	+'''',
	  'SA_ORSZA',	''''+Query5.FieldByName('SA_ORSZA').AsString	+'''',
	  'SA_ORSZ2',	''''+Query5.FieldByName('SA_ORSZ2').AsString	+'''',
	  'SA_RESZE',	'1',
	  'SA_HELYE',  	IntToStr(StrToIntDef(Query5.FieldByName('SA_HELYE').AsString, 0)),
	  'SA_SAEUR',  	IntToStr(StrToIntDef(Query5.FieldByName('SA_SAEUR').AsString, 0)),
	  'SA_ATAD',	'0',
     'SA_VALNEM',  ''''+Query5.FieldByName('SA_VALNEM').AsString	+'''',
	  'SA_VALOSS',		'-1*'+SqlSzamString(StringSzam(Query5.FieldByName('SA_VALOSS').AsString),12,2),
	  'SA_VALAFA',		'-1*'+SqlSzamString(StringSzam(Query5.FieldByName('SA_VALAFA').AsString),12,2),
     'SA_VALARF',		SqlSzamString(StringSzam(Query5.FieldByName('SA_VALARF').AsString),12,2),
	  'SA_ARFDAT',		''''+Query5.FieldByName('SA_ARFDAT').AsString	+'''',
     'SA_UZPOT',		SqlSzamString(StringSzam(Query5.FieldByName('SA_UZPOT').AsString),12,2),
	  'SA_NYELV',		''''+Query5.FieldByName('SA_NYELV').AsString	+'''',
     'SA_VEFEJ',		''''+Query5.FieldByName('SA_VEFEJ').AsString	+'''',
	  'SA_TIPUS',	SqlSzamString(StringSzam(Query5.FieldByName('SA_TIPUS').AsString),12,2)
	  ], ' WHERE SA_UJKOD = '+IntToStr(ujkodszam));

//   A storn� viszonylat l�trehoz�sa
	  ViszonylatStorno(regikod, ujszamlaszam, ujkodszam);

	  Query_Run (Query4, 'SELECT * FROM SZSOR WHERE SS_UJKOD = '+regikod+' ',true);
	  while not Query4.EOF do begin
		Query_Insert( Query5, 'SZSOR',
		[
		 'SS_KOD', 			''''+ujszamlaszam+'''',
		 'SS_SOR',			Query4.FieldByName('SS_SOR').AsString,
		 'SS_UJKOD',		IntToStr(ujkodszam),
		 'SS_TERKOD', 		''''+Query4.FieldByName('SS_TERKOD').AsString +'''',
		 'SS_TERNEV', 		''''+Query4.FieldByName('SS_TERNEV').AsString +'''',
		 'SS_CIKKSZ', 		''''+Query4.FieldByName('SS_CIKKSZ').AsString +'''',
		 'SS_ITJSZJ', 		''''+Query4.FieldByName('SS_ITJSZJ').AsString +'''',
		 'SS_MEGY', 		''''+Query4.FieldByName('SS_MEGY').AsString +'''',
		 'SS_AFA', 			SqlSzamString(StringSzam(Query4.FieldByName('SS_AFA').AsString),		12,2),
		 'SS_EGYAR', 		'-1*'+SqlSzamString(StringSzam(Query4.FieldByName('SS_EGYAR').AsString),	12,2),
		 'SS_KEDV', 			SqlSzamString(StringSzam(Query4.FieldByName('SS_KEDV').AsString),		12,2),
		 'SS_VALERT', 		SqlSzamString(StringSzam(Query4.FieldByName('SS_VALERT').AsString),	12,2),
		 'SS_VALARF', 		SqlSzamString(StringSzam(Query4.FieldByName('SS_VALARF').AsString),	12,2),
		 'SS_DARAB', 		SqlSzamString(StringSzam(Query4.FieldByName('SS_DARAB').AsString),	12,2),
		 'SS_ARFOL', 		SqlSzamString(StringSzam(Query4.FieldByName('SS_ARFOL').AsString),	12,4),
		 'SS_VALNEM', 		''''+ Query4.FieldByName('SS_VALNEM').AsString +'''',
		 'SS_AFAKOD', 		''''+ Query4.FieldByName('SS_AFAKOD').AsString +'''',
		 'SS_LEIRAS', 		''''+ Query4.FieldByName('SS_LEIRAS').AsString +'''',
		 'SS_KULNEV', 		''''+ Query4.FieldByName('SS_KULNEV').AsString +'''',
		 'SS_KULLEI', 		''''+ Query4.FieldByName('SS_KULLEI').AsString +'''',
		 'SS_KULME', 		''''+ Query4.FieldByName('SS_KULME').AsString  +'''',
		 'SS_ARDAT', 		''''+ Query4.FieldByName('SS_ARDAT').AsString  +''''
		 ]);
		 Query4.Next;
	  end;
end;

procedure TOSzamlaFormDlg.ViszonylatStorno(regikod, szamla : string; ujkod2 : integer);
var
	ujazon	: integer;
  S: string;
begin
	Query_Run(Query4, 'SELECT * FROM VISZONY WHERE VI_UJKOD = '+regikod);
	if Query4.RecordCount < 1 then begin
		NoticeKi('Hi�nyz� megb�z�s rekord! Nem lehet storn� megb�z�st gener�lni!');
		Exit;
	end;

	// ujazon := GetNextStrCode('VISZONY', 'VI_VIKOD', 0, 5);
  ujazon := GetNewIDLoop(modeGetNextStrCode,'VISZONY', 'VI_VIKOD', '', 0, 6);
  if (ujazon<=0) then begin
     S:= 'Sikertelen k�d gener�l�s, k�rem pr�b�lja �jra! (OsszesitettSzamlaViszonylatStorno)';
     HibaKiiro(S);
     NoticeKi(S);
     Exit;
     end;

	{Az �j rekord felvitele}
	Query_Insert (Query4, 'VISZONY', [
		'VI_VIKOD', 	''''+IntToStr(ujazon)+'''',
		'VI_UJKOD',		IntToStr(ujkod2),
		'VI_HONNAN',	''''+Query4.FieldByName('VI_HONNAN').AsString+'''',
		'VI_HOVA',		''''+Query4.FieldByName('VI_HOVA').AsString+'''',
		'VI_JAKOD',		''''+Query4.FieldByName('VI_JAKOD').AsString+'''',
		'VI_VEKOD',		''''+Query4.FieldByName('VI_VEKOD').AsString+'''',
		'VI_VENEV',		''''+Query4.FieldByName('VI_VENEV').AsString+'''',
		'VI_VALNEM',	''''+Query4.FieldByName('VI_VALNEM').AsString+'''',
		'VI_KULF', 		Query4.FieldByName('VI_KULF').AsString,
		'VI_ARFOLY',	SqlSzamString(StringSzam(Query4.FieldByName('VI_ARFOLY').AsString),12,4),
		'VI_FDEXP',		SqlSzamString(-1*StringSzam(Query4.FieldByName('VI_FDEXP').AsString),12,4),
		'VI_FDIMP',		SqlSzamString(-1*StringSzam(Query4.FieldByName('VI_FDIMP').AsString),12,4),
		'VI_FDKOV',		SqlSzamString(-1*StringSzam(Query4.FieldByName('VI_FDKOV').AsString),12,4),
		'VI_TIPUS',		Query4.FieldByName('VI_TIPUS').AsString,
		'VI_BEDAT',		''''+Query4.FieldByName('VI_BEDAT').AsString+'''',
		'VI_KIDAT',		''''+Query4.FieldByName('VI_KIDAT').AsString+'''',
		'VI_SAKOD',		''''+szamla+'''',
		'VI_MEGJ',		''''+'Storn� viszonylat!'+''''
	] );
end;

procedure TOSzamlaFormDlg.BitNavElesClick(Sender: TObject);
begin
   // A sz�mla elk�ld�se a NAV �les ter�letre
   if StrToINtDef(Query1.Fieldbyname('SA_FLAG').AsString, 0) = 1  then begin
       NoticeKi('Figyelem! A rekordot el�sz�r v�gleges�teni kell !');
       Exit;
   end;
      if Pos('DONE', Query1.Fieldbyname('SA_ESTAT').AsString) < 1 then begin
       // Ha nem DONE -val z�rult, lehet v�gleges�teni
       if NoticeKi('Figyelem! Nem NAV rekord! M�gis elk�ldi a sz�ml�t a NAV-nak?', NOT_QUESTION) <> 0 then begin
           Exit;
       end;
   end;
  // A sz�mla elk�ld�se a NAV �lesre
  NAVAdatkuldes(1);

   // �sszes sz�mla elk�ld�se a NAV �lesre
   {While not Query1.Eof do begin
      NAVAdatkuldes(1);
      Query1.Next;
      End;
      }
end;

procedure TOSzamlaFormDlg.BitNavtesztClick(Sender: TObject);
begin
   // A sz�mla elk�ld�se a NAV tesztre
   NAVAdatkuldes(0);

   // �sszes sz�mla elk�ld�se a NAV tesztre
   {While not Query1.Eof do begin
      NAVAdatkuldes(0);
      Query1.Next;
      End;
      }
end;

procedure TOSzamlaFormDlg.ButtonKlonClick(Sender: TObject);
var
	st, regikod, S	: string;
	ujkodszam		: integer;
	szasor			: integer;
  kidat,esdat,ukidat,uesdat: string;
  lejarat: integer;
  ujkodok: WideString;
begin
	if not voltalt then begin
		voltalt 	:= true;
		Exit;
	end;
	Edit1.Text	:= '';
	if NoticeKi('Val�ban kl�nozni akarja a kijel�lt �sszes�tett sz�ml�t?', NOT_QUESTION) = 0 then begin
		Screen.Cursor	:= crHourglass;
		{Sz�mla kl�noz�sa}
		// L�trehozzuk az �j sz�ml�t
		//st	:= GetUjszamla;
		//st	:= GetUjszamla2(copy(Query1.FieldByName('SA_KIDAT').AsString,3,2));
		//st	:= GetUjszamla2('');
    //if st='' then exit;
    //ujkod:=st;
    //regikod:=Query1.FieldByName('SA_KOD').AsString ;
    regikod:=Query1.FieldByName('SA_UJKOD').AsString ;
    ///////////////////////////////////////
    kidat:=Query1.FieldByName('SA_KIDAT').AsString;
    esdat:=Query1.FieldByName('SA_ESDAT').AsString;
    lejarat:=Trunc(StrToDate(esdat)-StrToDate(kidat));
    ukidat:=DateToStr(date);
	  uesdat:= DatumHoznap(ukidat, lejarat, true);
    ///////////////////////////////////////
		// Query_Run(Query2, 'SELECT MAX(SA_UJKOD) MAXKOD FROM SZFEJ2 ', true);
		// ujkodszam 	:= Query2.FieldByName('MAXKOD').AsInteger + 1;
    ujkodszam := GetNewIDLoop(modeGetNextCode, 'SZFEJ2', 'SA_UJKOD', '');
    if (ujkodszam<=0) then begin
     S:= 'Sikertelen k�d gener�l�s, k�rem pr�b�lja �jra! (OsszesitettSzamlaKlon)';
     HibaKiiro(S);
     NoticeKi(S);
     Exit;
     end;
    st:='';
		Query_Run(Query2, 'INSERT INTO SZFEJ2 (SA_UJKOD, SA_KOD, SA_FLAG ) '+
				'VALUES ('+IntToStr(ujkodszam)+','''+st+''', ''1'' ) ', true);

		{A fejl�c rekord felt�lt�se}
		Query_Update ( Query2, 'SZFEJ2',
		[
		'SA_DATUM',		''''+EgyebDlg.MaiDatum+'''',
		'SA_TEDAT',		''''+Query1.FieldByName('SA_TEDAT').AsString +'''',
//		'SA_KIDAT',		''''+Query1.FieldByName('SA_KIDAT').AsString +'''',
//		'SA_ESDAT',		''''+Query1.FieldByName('SA_ESDAT').AsString +'''',
		'SA_KIDAT',		''''+ukidat +'''',
		'SA_ESDAT',		''''+uesdat +'''',
		'SA_VEVOKOD',	''''+Query1.FieldByName('SA_VEVOKOD').AsString +'''',
		'SA_VEVONEV',	''''+Query1.FieldByName('SA_VEVONEV').AsString +'''',
		'SA_VEIRSZ',	''''+Query1.FieldByName('SA_VEIRSZ').AsString  +'''',
		'SA_VEVAROS',	''''+Query1.FieldByName('SA_VEVAROS').AsString +'''',
		'SA_VECIM',		''''+Query1.FieldByName('SA_VECIM').AsString	+'''',
		'SA_VEADO',		''''+Query1.FieldByName('SA_VEADO').AsString 	+'''',
		'SA_VEBANK',	''''+Query1.FieldByName('SA_VEBANK').AsString 	+'''',
		'SA_VEKEDV',	SqlSzamString(StringSzam(Query1.FieldByName('SA_VEKEDV').AsString),6,2),
		'SA_PELDANY',	'0',
		'SA_FIMOD',		''''+Query1.FieldByName('SA_FIMOD').AsString +'''',
		'SA_OSSZEG',	SqlSzamString(StringSzam(Query1.FieldByName('SA_OSSZEG').AsString),12,2),
		'SA_AFA',		SqlSzamString(StringSzam(Query1.FieldByName('SA_AFA').AsString),12,2),
		'SA_MEGJ',		''''+Query1.FieldByName('SA_MEGJ').AsString +'''',
		'SA_JARAT',		''''+Query1.FieldByName('SA_JARAT').AsString +'''',
		'SA_POZICIO',	''''+Query1.FieldByName('SA_POZICIO').AsString +'''',
		'SA_RSZ',		''''+Query1.FieldByName('SA_RSZ').AsString 	+'''',
		'SA_MELL1',		''''+Query1.FieldByName('SA_MELL1').AsString	+'''',
		'SA_MELL2',		''''+Query1.FieldByName('SA_MELL2').AsString +'''',
		'SA_MELL3',		''''+Query1.FieldByName('SA_MELL3').AsString +'''',
		'SA_MELL4',		''''+Query1.FieldByName('SA_MELL4').AsString +'''',
		'SA_MELL5',		''''+Query1.FieldByName('SA_MELL5').AsString +'''',
		'SA_FEJL1',		''''+Query1.FieldByName('SA_FEJL1').AsString	+'''',
		'SA_FEJL2',		''''+Query1.FieldByName('SA_FEJL2').AsString	+'''',
		'SA_FEJL3',		''''+Query1.FieldByName('SA_FEJL3').AsString	+'''',
		'SA_FEJL4',		''''+Query1.FieldByName('SA_FEJL4').AsString	+'''',
		'SA_FEJL5',		''''+Query1.FieldByName('SA_FEJL5').AsString	+'''',
		'SA_FEJL6',		''''+Query1.FieldByName('SA_FEJL6').AsString	+'''',
		'SA_FEJL7',		''''+Query1.FieldByName('SA_FEJL7').AsString	+'''',
		'SA_FOKON',		''''+Query1.FieldByName('SA_FOKON').AsString	+'''',
	    'SA_SAEUR',  	IntToStr(StrToIntDef(Query1.FieldByName('SA_SAEUR').AsString, 0)),
		'SA_ATAD',		'0',
       'SA_VALNEM',  ''''+Query1.FieldByName('SA_VALNEM').AsString	+'''',
		'SA_VALOSS',		SqlSzamString(StringSzam(Query1.FieldByName('SA_VALOSS').AsString),12,2),
		'SA_VALAFA',		SqlSzamString(StringSzam(Query1.FieldByName('SA_VALAFA').AsString),12,2),
		'SA_NYELV',		''''+Query1.FieldByName('SA_NYELV').AsString	+'''',
       'SA_VEFEJ',		''''+Query1.FieldByName('SA_VEFEJ').AsString	+'''',
		'SA_TIPUS',		SqlSzamString(StringSzam(Query1.FieldByName('SA_TIPUS').AsString),12,2)
		], ' WHERE SA_UJKOD = '+IntToStr(ujkodszam));

		{V�gigmegy�nk a r�szsz�ml�kon �s azokn�l is l�trehozzuk a kl�n sz�ml�kat}
		Query_Run (Query2, 'SELECT * FROM SZSOR2 WHERE S2_S2KOD = '+Query1.Fieldbyname('SA_UJKOD').AsString, true);
    ujkodok:='''9999999999''';
		while not Query2.EOF do begin
			ujkodok:=ujkodok+','''+IntToStr( KlonSzamla(Query2.FieldByName('S2_UJKOD').AsString, st ))+'''';
			Query2.Next;
		end;
		// Begy�jtj�k a r�szsz�ml�kat az �sszes�tett sz�mla al�
		szasor	:= 1;
		Query_Run (Query2, 'SELECT * FROM SZFEJ WHERE SA_KOD = '''+st+''' ', true);
		Query_Run (Query2, 'SELECT * FROM SZFEJ WHERE SA_UJKOD IN ('+ujkodok+') ', true);
		while not Query2.EOF do begin
			Query_Run(Query3, 'INSERT INTO SZSOR2 (S2_S2KOD, S2_S2SOR, S2_UJKOD ) '+
				'VALUES ('+IntToStr(ujkodszam)+','+IntToStr(szasor)+', '+Query2.FieldByName('SA_UJKOD').AsString+' ) ', true);
			Inc(szasor);
			Query2.Next;
		end;
    // �tcsatol�s
    //Atcsatolas(regikod,ujkod);
    Atcsatolas(regikod,IntToStr(ujkodszam));

		Screen.Cursor	:= crDefault;
		{Az �j t�tel megjelen�t�se}
		ModLocate (Query1, FOMEZO, IntToStr(ujkodszam) );
	end;
end;

procedure TOSzamlaFormDlg.Atcsatolas(esakod, usakod: string);
var
  vikod, evikod, ujkod: string;
begin
  if (esakod='')or(usakod='') then exit;
  //Query_Run( QueryAtcsat,  'SELECT * from SZFEJ where SA_KOD='''+esakod+'''');

  Query_Run( QueryAtcsat, 'SELECT * from SZSOR2 where S2_S2KOD='''+usakod+'''');
  QueryAtcsat.First;
  while not QueryAtcsat.Eof do
  begin
    ujkod:=QueryAtcsat.fieldbyname('S2_UJKOD').AsString;
    vikod:= Query_Select('VISZONY','VI_UJKOD',ujkod,'VI_VIKOD') ;
    evikod:=Query_Select('VISZONY','VI_UJKOD',ujkod,'VI_EVIKOD') ;
    Query_Run(EgyebDlg.QueryAlap, 'UPDATE MEGBIZAS SET MB_VIKOD = '''+vikod+''' WHERE MB_VIKOD='''+evikod+'''');
    QueryAtcsat.Next;
  end;
  QueryAtcsat.Close;
end;

function TOSzamlaFormDlg.NAVAdatkuldes(tipus : integer) : boolean;
// tipus = 0 : teszt;  1 - �les
var
   ujkod       : integer;
   jsz         : TJSzamla;
   kuldheto    : boolean;
   szamla      : string;
   sorstr      : string;
   szszam      : string;
   sorsz       : integer;
   vtszstr     : string;
   vtszcat     : string;
   eredmeny    : boolean;
   origsz      : string;
   volthiba    : boolean;
   proba       : integer;
   token       : string;
   fimod       : string;
   afasz       : double;
   vnem        : string;
   ii          : integer;
   mezstat     : string;
   meztrid     : string;
   NO          : TJNavOnline;
   egyar       : double;
   vagyar      : double;
   szsor       : TJSzamlasor;
   jakod       : string;
   termeknev   : string;
   rendszam    : string;
   jaratstr    : string;
   S           : string;
begin
   // A sz�mla elk�ld�se a NAV tesztre
   Result      := false;
   volthiba    := false;
   szamla      := Query1.FieldByName('SA_KOD').AsString;
   if szamla = '' then begin
       // �j sz�ml�n�l most hozzuk l�tre a sz�mlasz�mot
       if NoticeKi('A rendszer legener�lja a sz�mlasz�mot! Folytatja?', NOT_QUESTION) <> 0 then begin
           Exit;
       end;
   szamla	:= GetUjszamla2(copy(Query1.FieldByName('SA_KIDAT').AsString,3,2));
   if (szamla='0') or (szamla='') then begin
         S:= 'Sikertelen sz�mlak�d gener�l�s, k�rem pr�b�lja �jra! (NAVAdatkuldes)';
         HibaKiiro(S);
         NoticeKi(S);
         Exit;
         end;
		Query_Run(Query2,'UPDATE SZFEJ2 SET SA_KOD = '''+szamla+''' WHERE SA_UJKOD='+Query1.Fieldbyname('SA_UJKOD').AsString, true);
		{V�gigmegy�nk a r�szsz�ml�kon + viszonyokon , �s azokn�l is be�ll�tjuk a sz�mlasz�mot}
		Query_Run (Query2, 'SELECT * FROM SZSOR2 WHERE S2_S2KOD = '+Query1.Fieldbyname('SA_UJKOD').AsString, true);
		while not Query2.EOF do begin
			Query_Run(Query3,'UPDATE SZFEJ SET SA_KOD='''+szamla+''' WHERE SA_UJKOD='+ Query2.Fieldbyname('S2_UJKOD').AsString, true);
			Query_Run(Query3,'UPDATE SZSOR SET SS_KOD='''+szamla+''' WHERE SS_UJKOD=' + Query2.Fieldbyname('S2_UJKOD').AsString, true);
			Query_Run(Query3,'UPDATE VISZONY SET VI_SAKOD='''+szamla+''' WHERE VI_UJKOD='+ Query2.Fieldbyname('S2_UJKOD').AsString, true);
			Query2.Next;
		end;
       ModLocate (Query1, 'SA_KOD', szamla );
   end;

   Query_Run(Query4,'SELECT * FROM SZFEJ2 WHERE SA_KOD = '''+szamla+''' ');
   if Query4.RecordCount = 0 then begin
       NoticeKi('Hi�nyz� sz�mla : '+szamla);
       Exit;
   end;

   EllenlistTorol;

   NO  := TJNavOnline.Create(Self);
   if tipus = 0 then begin
       // Teszt rendszer
       NO.Tolt(EgyebDlg.TempPath+'INI\T_NAVKULDES.INI', EgyebDlg.TempPath+'NAVFILES', 'T_');
       mezstat := 'SA_TSTAT';
       meztrid := 'SA_TTRID';
   end else begin
       // �les rendszer
       NO.Tolt(EgyebDlg.TempPath+'INI\NAVKULDES.INI', EgyebDlg.TempPath+'NAVFILES', '');
       mezstat := 'SA_ESTAT';
       meztrid := 'SA_ETRID';
   end;

   if Trim( Query1.FieldByName('SA_VEADO').AsString) = '' then begin
       NoticeKi('Az ad�sz�m �res !');
       volthiba    := true;
   end;

   EllenlistTorol;
   if NO.GetErrorCode <> 0 then begin
       EllenListAppend(NO.GetErrorStr, false);
       volthiba    := true;
   end;
   // A sz�mla felt�lt�se
   if not NO.ReadElado then begin
       EllenListAppend(NO.GetErrorStr, false);
       volthiba    := true;
   end;
//   SetVevo(tax, name, country, postal, city, address, bank : string) : boolean;
   if not NO.SetVevo(Query1.FieldByName('SA_VEADO').AsString, Query1.FieldByName('SA_VEVONEV').AsString, Query1.FieldByName('SA_ORSZ2').AsString, Query1.FieldByName('SA_VEIRSZ').AsString,
       Query1.FieldByName('SA_VEVAROS').AsString, Query1.FieldByName('SA_VECIM').AsString, Query1.FieldByName('SA_VEBANK').AsString) then begin
       EllenListAppend(NO.GetErrorStr, false);
       volthiba    := true;
   end;
//   SetFejlec(szamlaszam, categ, kiadas, teljesites, vanem, method, hatarido, fajta : string) : boolean;
   // Most csak �tutal�s van, de lehet pl. CASH, CARD, TRANSFER, ...
   szszam := Query1.FieldByName('SA_KOD').AsString;
   fimod   := 'TRANSFER';
   if Query1.FieldByName('SA_FIMOD').AsString = 'k�szp�nz' then begin
       fimod   := 'CASH';
   end;
   jsz     := TJSzamla.Create(Self);
   jsz.FillOsszSzamla( Query1.FieldByName('SA_UJKOD').AsString );
   if jsz.szamlaeuro < 1 then begin
       vnem    := 'HUF';
   end else begin
       vnem    := 'EUR';
   end;

   if not NO.FejTolto(szszam, 'NORMAL', Query1.FieldByName('SA_KIDAT').AsString, Query1.FieldByName('SA_TEDAT').AsString, vnem, fimod, Query1.FieldByName('SA_ESDAT').AsString, 'PAPER' ) then begin
       EllenListAppend(NO.GetErrorStr, false);
       volthiba    := true;
   end;

   // Sorok felt�lt�se
//   SorTolto(sorsz, vtszcat, vtszj, descr, megys : string; menny, egyar, netto, afasz, afa : double) : boolean;
   for ii := 0 to jsz.Szamlasorok.Count-1 do begin
       szsor   := TJSzamlasor(jsz.Szamlasorok[ii]);
       afasz   := szsor.afaszaz/100;
       egyar   := szsor.egysegar;
       vagyar  := szsor.valutaegyar;
       termeknev   := szsor.termeknev;
       if szsor.regi_sorszam = 1 then begin
           // J�het a b�v�tett term�kn�v - el�vessz�k az eredeti SZFEJ rekordot

           Query_Run(Query5, 'SELECT * FROM SZFEJ WHERE SA_UJKOD = '+ IntToStr(szsor.regi_ujkod), true);
           if Query5.RecordCount > 0 then begin
               rendszam		:= Query5.FieldByName('SA_RSZ').AsString;
               {A j�ratsz�m be�r�sa -> A j�ratsz�m alapj�n megkeress�k a bet�jelet is }
               jaratstr	:= ' (' +Query5.FieldByName('SA_JARAT').AsString + ')';
               if Query_Run(Query5, 'SELECT * FROM JARAT WHERE JA_ALKOD = '+ IntToStr(Round(StringSzam(Query5.FieldByName('SA_JARAT').AsString))),true) then begin
                   jaratstr	:= ' (' +Query5.FieldByName('JA_ORSZ').AsString + ' - ' + Query5.FieldByName('JA_ALKOD').AsString + ')';
               end;
           end;
           termeknev := szsor.termeknev +':' +  szsor.sormegjegyzes + ', ' + szsor.sorteljesites + jaratstr+ '  ' + rendszam;
       end;

       if jsz.szamlaeuro < 1 then begin
           if not NO.SorTolto(IntToStr(szsor.sorszam), szsor.itjszj, termeknev, szsor.megyseg,
               szsor.mennyiseg, egyar, szsor.sornetto, afasz, szsor.sorafa, szsor.sorafa, 1 ) then begin
               EllenListAppend(NO.GetErrorStr, false);
               volthiba    := true;
           end;
       end else begin
           // J�nnek az EUR �rt�kek
           if not NO.SorTolto(IntToStr(szsor.sorszam), szsor.itjszj, termeknev, szsor.megyseg,
               szsor.mennyiseg, vagyar, szsor.sorneteur, afasz, szsor.sorafaeur, szsor.sorafa, szsor.valutaarfoly ) then begin
               EllenListAppend(NO.GetErrorStr, false);
               volthiba    := true;
           end;
       end;
   end;

   NO.transactid   := Query1.Fieldbyname(meztrid).AsString;
   NO.invstatus    := Query1.Fieldbyname(mezstat).AsString;
   NO.inv_storno   := Query1.Fieldbyname('SA_STORN').AsString;
   if volthiba then begin
       EllenListMutat('NAV felad�si hib�k', false);
   end else begin
       // Ellen�rizz�k a NAV sz�mokat
       if  Format('%.2', [jsz.ossz_brutto]) <> Format('%.2', [NO.Szamla.iGrossAmount]) then begin
           NoticeKi('Figyelem! Az �tadott v�g�sszeg nem egyezik a sz�mla v�g�sszeggel : '+Format('%.f <> %.f', [jsz.ossz_brutto, NO.Szamla.iGrossAmount]) );
       end;
       if Format('%.2', [jsz.ossz_afa]) <> Format('%.2', [NO.Szamla.iVatAmountHUF]) then begin
           NoticeKi('Figyelem! Az �tadott �FA �sszeg nem egyezik a sz�mla �FA �sszeg�vel : '+Format('%.f <> %.f', [jsz.ossz_afa, NO.Szamla.iVatAmountHUF]) );
       end;
       if ( ( Trim(Query1.FieldByName(mezstat).AsString) = '' ) or ( Trim(Query1.FieldByName(mezstat).AsString) = 'ABORTED' ) )  then begin
           // Eddig nem volt sikeres az elk�ld�s -> Elk�ldj�k a NAV -nak
           NO.runcommand   := 'KULD';
           NO.ShowModal;
           if NO.ret_value <> '' then begin
               // Be�rjuk az elemeket az adatb�zisba
               Query_Run(Query2,'UPDATE SZFEJ2 SET '+mezstat+' = '''+NO.ret_value+''', '+meztrid+' = '''+NO.transactid+''' WHERE SA_KOD = '''+Query1.Fieldbyname('SA_KOD').AsString+''' ');
               ModLocate (Query1, 'SA_KOD', Query1.FieldByName('SA_KOD').AsString );
               Result  := true;
           end;
       end else begin
           if ( ( Trim(Query1.FieldByName(mezstat).AsString) = 'RECEIVED' ) or ( Trim(Query1.FieldByName(mezstat).AsString) = 'PROCESSING' ) )  then begin
               // Csak a st�tuszt k�rj�k le
               NO.runcommand   := 'STATUS';
               NO.ShowModal;
               if NO.ret_value <> '' then begin
                   // Be�rjuk az elemeket az adatb�zisba
                   Query_Run(Query2,'UPDATE SZFEJ2 SET '+mezstat+' = '''+NO.ret_value+''', '+meztrid+' = '''+NO.transactid+''' WHERE SA_KOD = '''+Query1.Fieldbyname('SA_KOD').AsString+''' ');
                   ModLocate(Query1, 'SA_KOD', Query1.FieldByName('SA_KOD').AsString );
                   Result  := true;
               end;
           end else begin
               // Csak megnyitjuk a dial�gust -> megn�zhetj�k az eddigi m�veleteket
               NO.runcommand   := '';
               NO.ShowModal;
               Result  := true;
           end;
       end;
   end;
end;

end.

