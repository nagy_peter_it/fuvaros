unit TablaOlvaso;

interface

uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, DBTables, DB, StdCtrls, Buttons, Mask, DBCtrls,
  ExtCtrls, ADODB, FileCtrl, StrUtils, Grids;

type
  TTablaOlvasoDlg = class(TForm)
	 BitBtn6: TBitBtn;
	 Label4: TLabel;
	 M1: TMaskEdit;
	 BitBtn5: TBitBtn;
	 BtnExport: TBitBtn;
	 Query1: TADOQuery;
	 Query2: TADOQuery;
	 OpenDialog1: TOpenDialog;
	 SG1: TStringGrid;
	 Memo1: TMemo;
	 CheckBox1: TCheckBox;
    Query3: TADOQuery;
    SG2: TStringGrid;
    M5: TMaskEdit;
    BitBtn2: TBitBtn;
    Label24: TLabel;
	 procedure FormCreate(Sender: TObject);
	 procedure BitBtn6Click(Sender: TObject);
	 procedure BitBtn5Click(Sender: TObject);
	 procedure BtnExportClick(Sender: TObject);
	 procedure RekordIro(datumtol, lanid, desc, plz, kezdo, vegzo, kateg, trday, fteur, info, gkkat: string );
	 procedure Feldolgozas;
	 procedure DuplikacioSzures;
    procedure BitBtn2Click(Sender: TObject);
  private
		kilepes		: boolean;
		alnev		: string;
  public
  end;

var
  TablaOlvasoDlg: TTablaOlvasoDlg;

implementation

uses
	Egyeb, Kozos, J_SQL, J_Excel;
{$R *.DFM}

procedure TTablaOlvasoDlg.FormCreate(Sender: TObject);
begin
	M1.Text	:= '';
	kilepes	:= true;
	EgyebDlg.SetADOQueryDatabase(Query1);
	EgyebDlg.SetADOQueryDatabase(Query2);
	EgyebDlg.SetADOQueryDatabase(Query3);
end;

procedure TTablaOlvasoDlg.BitBtn6Click(Sender: TObject);
begin
	if not kilepes then begin
		kilepes	:= true;
		BtnExport.Show;
	end else begin
		Close;
	end;
end;

procedure TTablaOlvasoDlg.BitBtn2Click(Sender: TObject);
begin
   EgyebDlg.Calendarbe(M5);
end;

procedure TTablaOlvasoDlg.BitBtn5Click(Sender: TObject);
begin
	// Az �llom�ny kiv�laszt�sa
	OpenDialog1.Filter		:= 'XLS �llom�nyok (*.xls)|*.XLS';
	OpenDialog1.DefaultExt  := 'XLS';
	OpenDialog1.InitialDir	:= EgyebDlg.DocumentPath;
	if OpenDialog1.Execute then begin
		M1.Text := OpenDialog1.FileName;
	end;
end;

procedure TTablaOlvasoDlg.BtnExportClick(Sender: TObject);
begin
  if (M1.Text='') then begin
     NoticeKi('�llom�ny megad�sa k�telez�!') ;
     exit;
     end;
  if (M5.Text='') then begin
     NoticeKi('Hat�lyba l�p�si d�tum megad�sa k�telez�!') ;
     exit;
     end;
	Screen.Cursor	:= crHourGlass;
	BtnExport.Hide;
	Feldolgozas;
	DuplikacioSzures;
	Screen.Cursor	:= crDefault;
	NoticeKi('A beolvas�s befejez�d�tt!');
//	Close;
end;

procedure TTablaOlvasoDlg.Feldolgozas;
const
  ElsoAdatOszlop = 13;
  UtolsoAdatOszlop = 38;
  Elso_3_5_oszlop = 25;
  Elso_24_oszlop = 30;
var
	EX		: TJExcel;
	i, j	: integer;
	lanid	: integer;
	ujlanid	: integer;
	desc	: string;
	plz		: string;
	dij, kateg, gkkat, info: string;
	trday	: string;
  kezdo, vegzo, datumtol: string;
  siker: boolean;
begin
	if not FileExists(M1.Text) then begin
		NoticeKi('Hi�nyz� bemeneti �llom�ny!');
		Exit;
	  end;
	EX 	:= TJexcel.Create(Self);
	if not EX.OpenXlsFile(M1.Text) then begin
		NoticeKi('Nem siker�lt megnyitnom az Excel �llom�nyt!');
    Exit;
	  end
  else begin
    EX.SetActiveSheet(1);
		if not EX.ReadFullXls(SG1) then begin
			NoticeKi('Nem siker�lt beolvasnom az Excel �llom�ny 1. lapj�t!');
      Exit;
      end;
    EX.SetActiveSheet(2);
		if not EX.ReadFullXls(SG2) then begin
			NoticeKi('Nem siker�lt beolvasnom az Excel �llom�ny 2. lapj�t!');
      Exit;
      end;
		end; // else
	EX.Free;  // nem kell m�r
	Memo1.Clear;
 	alnev	:= ExtractFileName(M1.Text);
  datumtol:= M5.Text;
	// ---- "General" t�bl�zat feldolgoz�sa ------
 	for i := 3 to SG1.RowCount - 1 do begin
		lanid	:= StrToIntDef(SG1.Cells[1, i], 0);
    if lanid>0 then begin
        kezdo := trim(SG1.Cells[4, i])+'('+trim(SG1.Cells[3, i])+')';
        vegzo := trim(SG1.Cells[6, i])+'('+trim(SG1.Cells[5, i])+')';
        desc	:= kezdo+' - '+vegzo;
        plz		:= SG1.Cells[4, i]+' - '+SG1.Cells[3, i];
        for j := ElsoAdatOszlop to UtolsoAdatOszlop do begin
            dij:= SG1.Cells[j, i];
            kateg:= SG1.Cells[j, 1];  // a fejl�cb�l, pl. '30-50';
            trday:= SG1.Cells[11, i];
            // kateg�ria besorol�s az oszlop alapj�n
            if (j<Elso_3_5_oszlop) then gkkat:='1,5';
            if (j>=Elso_3_5_oszlop) and (j<Elso_24_oszlop) then gkkat:='3,5';
            if (j>=Elso_24_oszlop) then gkkat:='24';
            RekordIro(datumtol, IntToStr(lanid), desc, plz, kezdo, vegzo, kateg, trday, dij, '', gkkat);
            end;  // for j, oszlopok
        end;  // if lanid
  	end; // for i, sorok
	// ---- "Special" t�bl�zat feldolgoz�sa ------
	for i := 6 to SG2.RowCount - 1 do begin
    with SG2 do begin
  		lanid	:= StrToIntDef(Cells[1, i], 0);
      kezdo := trim(Cells[4, i])+'('+trim(Cells[5, i]);
      if trim(Cells[6, i])<>'' then  // ha van 2nd pickup location
          kezdo := kezdo + '+'+trim(Cells[6, i]);
      kezdo := kezdo +')';
      vegzo := trim(Cells[14, i])+'('+trim(Cells[15, i]);
      if trim(Cells[16, i])<>'' then  // ha van 2nd pickup location
          vegzo := vegzo + '+'+trim(Cells[16, i]);
      if trim(Cells[17, i])<>'' then  // ha van 3rd pickup location
          vegzo := vegzo + '+'+trim(Cells[17, i]);
      if trim(Cells[18, i])<>'' then  // ha van 4th pickup location
          vegzo := vegzo + '+'+trim(Cells[18, i]);
      vegzo := vegzo +')';
      desc	:= kezdo+' - '+vegzo;
  		plz		:= Cells[4, i]+' - '+Cells[5, i];
   		dij:= Cells[27, i];
      kateg:= Cells[24, i];
      // ---------- direktben bek�dolt �rt�kek! ----- //
      // legyen sz�t�rban??
      if kateg='sprinter' then gkkat:='1,5';
      if kateg='avia' then gkkat:='3,5';
      if kateg='3tons truck' then gkkat:='3,5';
      if kateg='24t' then gkkat:='24';
      // ------------------------------------------- //
 		  trday:= Cells[25, i];
      info:= Cells[31, i];
		  RekordIro(datumtol, IntToStr(lanid), desc, plz, kezdo, vegzo, kateg, trday, dij, info, gkkat);
      end;  // with
  	end; // for i, sorok

   // A nem beolvasott elemek t�rl�se - amik r�gebbi �llom�nyb�l j�ttek
	Query_Run(Query1, 'SELECT * FROM FUVARTABLA WHERE FT_TOL='''+datumtol+''' AND FT_MEGJE <> '''+copy('Olvas�s:'+alnev,1,60)+'''');
	if Query1.RecordCount > 0 then begin
   	while not Query1.Eof do begin
           Query_Run(Query3, 'SELECT MB_MBKOD FROM MEGBIZAS WHERE MB_FUTID = '+Query1.FieldByName('FT_FTKOD').AsString);
			if Query3.RecordCount > 0 then begin
           	// Nem t�r�lheto, mert haszn�latban van !!!
				Memo1.Lines.Add('   R�gebbi rekord t�rl�se nem lehets�ges, mert haszn�latban van! ' +
              '['+Query1.FieldByName('FT_TOL').AsString+'] '+Query1.FieldByName('FT_LANID').AsString+' - '+
           		Query1.FieldByName('FT_DESCR').AsString+' _ '+Query1.FieldByName('FT_FTPLZ').AsString+' ('+
               	Query1.FieldByName('FT_KATEG').AsString+')  -> '+Query1.FieldByName('FT_FTEUR').AsString);
           end else begin
           	// T�r�lheto
           	Query_Run(Query3, 'DELETE FROM FUVARTABLA WHERE FT_FTKOD = '+Query1.FieldByName('FT_FTKOD').AsString);
				Memo1.Lines.Add('   R�gebbi rekord t�rl�se : ' +
              '['+Query1.FieldByName('FT_TOL').AsString+'] '+Query1.FieldByName('FT_LANID').AsString+' - '+
           		Query1.FieldByName('FT_DESCR').AsString+' _ '+Query1.FieldByName('FT_FTPLZ').AsString+' ('+
               	Query1.FieldByName('FT_KATEG').AsString+')  -> '+Query1.FieldByName('FT_FTEUR').AsString);
           end;

       	Query1.Next;
       end;
   end;
end;

procedure TTablaOlvasoDlg.RekordIro(datumtol, lanid, desc, plz, kezdo, vegzo, kateg, trday, fteur, info, gkkat: string);
var
	ujrec	: integer;
begin
	// Az �j rekord beolvas�sa
	Query_Run(Query1, 'SELECT * FROM FUVARTABLA WHERE FT_TOL='''+datumtol+''' AND FT_LANID = '+lanid+' AND FT_KEZDO = '''+kezdo+''' AND FT_VEGZO = '''+vegzo+''' '+
		'AND FT_FTPLZ = '''+plz+''' AND FT_KATEG = '''+kateg+''' ');
	if Query1.RecordCount > 0 then begin
		// L�tez� rekord, friss�teni kell
		Query_Update (Query2, 'FUVARTABLA',
			[
			'FT_TRDAY', IntToStr(StrToIntDef(trday,0)),
			'FT_MEGJE', ''''+copy('Olvas�s:'+alnev,1,60)+'''',
			'FT_FTEUR', SqlSzamString(StringSzam(fteur),12,2),
			'FT_VALID', ''''+EgyebDlg.MaiDatum+'''',
      'FT_DESCR', ''''+desc+'''',
      'FT_INFO', ''''+info+'''',
      'FT_GKKAT', ''''+gkkat+''''
			], ' WHERE FT_TOL='''+datumtol+''' AND FT_LANID = '+lanid+' AND FT_KEZDO = '''+kezdo+''' AND FT_VEGZO = '''+vegzo+''' AND FT_FTPLZ = '''+plz+''' AND FT_KATEG = '''+kateg+''' ');
		// FuvarHovaUpdate(Query1.FieldByName('FT_FTKOD').AsString);
		Memo1.Lines.Add('R�gebbi rekord friss�t�se : ' + lanid+' - '+desc+' _ '+plz+' ('+kateg+')  -> '+fteur);
	end else begin
		// �j rekord l�trehoz�sa
		ujrec	:= GetNextCode('FUVARTABLA', 'FT_FTKOD', 1);
		Query_Insert (		Query1, 'FUVARTABLA',
			[
			'FT_FTKOD', IntToStr(ujrec),
      'FT_TOL', ''''+datumtol+'''',
			'FT_LANID', lanid,
			'FT_DESCR', ''''+copy(desc, 1, 50)+'''',
			'FT_FTPLZ', ''''+copy(plz, 1, 40)+'''',
      'FT_KEZDO', ''''+kezdo+'''',
      'FT_VEGZO', ''''+vegzo+'''',
			'FT_KATEG', ''''+kateg+'''',
			'FT_TRDAY', IntToStr(StrToIntDef(trday,0)),
			'FT_MEGJE', ''''+copy('Olvas�s:'+alnev,1,60)+'''',
      'FT_INFO', ''''+info+'''',
      'FT_GKKAT', ''''+gkkat+'''',
			'FT_FTEUR', SqlSzamString(StringSzam(fteur),12,2),
			'FT_VALID', ''''+EgyebDlg.MaiDatum+''''
			]);
		// FuvarHovaUpdate(IntToStr(ujrec));
		Memo1.Lines.Add('�j rekord besz�r�sa : ' + lanid+' - '+desc+' _ '+plz+' ('+kateg+')  -> '+fteur);
	end;
  {   ez kell az �j strukt�r�ban?
	Query_Run(Query1, 'SELECT * FROM FUVARTABLA WHERE FT_LANID = '+lanid+' AND FT_DESCR = '''+desc+''' '+
		'AND FT_FTPLZ = '''+plz+''' AND FT_KATEG = '''+kateg+'T'+''' ');
	if Query1.RecordCount > 0 then begin
		// L�tez� rekord, friss�teni kell
		Query_Update (Query2, 'FUVARTABLA',
			[
			'FT_TRDAY', IntToStr(StrToIntDef(trday,0)),
			'FT_MEGJE', ''''+copy('Olvas�s:'+alnev,1,60)+'''',
			'FT_FTEUR', SqlSzamString(StringSzam(fteur),12,2),
			'FT_VALID', ''''+EgyebDlg.MaiDatum+''''
			], ' WHERE FT_LANID = '+lanid+' AND FT_DESCR = '''+desc+''' AND FT_FTPLZ = '''+plz+''' AND FT_KATEG = '''+kateg+'T'+''' ');
		// FuvarHovaUpdate(Query1.FieldByName('FT_FTKOD').AsString);
		Memo1.Lines.Add('R�gebbi rekord friss�t�se : ' + lanid+' - '+desc+' _ '+plz+' ('+kateg+')  -> '+fteur);
	end;
  }
end;

procedure TTablaOlvasoDlg.DuplikacioSzures;
var
	kod1	: string;
	kod2	: string;
  S: string;
begin
	// Megsz�ntetj�k a duplik�lt rekordokat
  S:='SELECT FT_TOL, FT_LANID, FT_KEZDO, FT_VEGZO, FT_FTPLZ, FT_KATEG, COUNT(*) DB FROM FUVARTABLA ';
  S:=S+' GROUP BY FT_TOL, FT_LANID, FT_KEZDO, FT_VEGZO, FT_FTPLZ, FT_KATEG ORDER BY DB DESC';
	Query_Run(Query1, S);
	while StrToIntDef(Query1.FieldByName('DB').AsString, 0) > 1 do begin
    S:= 'SELECT FT_FTKOD FROM FUVARTABLA WHERE FT_TOL= '+Query1.FieldByName('FT_TOL').AsString+' AND FT_LANID = '+Query1.FieldByName('FT_LANID').AsString;
    S:=S+' AND FT_KEZDO = '''+Query1.FieldByName('FT_KEZDO').AsString+''' AND FT_VEGZO = '''+Query1.FieldByName('FT_VEGZO').AsString;
    S:=S+' AND FT_FTPLZ = '''+Query1.FieldByName('FT_FTPLZ').AsString+''' AND FT_KATEG = '''+Query1.FieldByName('FT_KATEG').AsString+''' ORDER BY FT_FTKOD';
		Query_Run(Query2, S);
		kod1	:= Query2.FieldByName('FT_FTKOD').AsString;
		Query2.Next;
		while not Query2.Eof do begin
			kod2	:= Query2.FieldByName('FT_FTKOD').AsString;
			// A csere megcsin�l�sa �s t�rl�s
			Query_Run(Query3, 'UPDATE MEGBIZAS SET MB_FUTID = '+kod1+' WHERE MB_FUTID = '+kod2);
			Query_Run(Query3, 'DELETE FROM FUVARTABLA WHERE FT_FTKOD = '+kod2);
			Query2.Next;
		end;
		Query1.Next;
	end;
end;

end.

