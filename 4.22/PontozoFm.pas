unit PontozoFm;

interface

uses
	SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
	StdCtrls, Forms, DBCtrls, DB, DBGrids, DBTables, Grids, ExtCtrls, Buttons,
	ShellApi, Dialogs, ADODB, J_FOFORMUJ;

type
	TPontozoFmDlg = class(TJ_FOFORMUJDLG)
    Query3: TADOQuery;
	  procedure FormCreate(Sender: TObject);override;
	 procedure Adatlap(cim, kod : string);override;
	end;

var
	PontozoFmDlg : TPontozoFmDlg;

implementation

uses
	Egyeb, J_SQL, Kozos, PontozoBe;

{$R *.DFM}

procedure TPontozoFmDlg.FormCreate(Sender: TObject);
begin
	EgyebDLg.SetADOQueryDatabase(Query3);
   if not Query_Run(Query3, 'SELECT * FROM PONTOZO') then begin
       Query_Run(Query3, 'CREATE TABLE PONTOZO (PO_POKOD INTEGER, PO_DOKOD VARCHAR(5), PO_ALKOD VARCHAR(5))');
   end;
   if not Query_Run(Query3, 'SELECT PO_DOKOD FROM PONTOZO') then begin
       Query_Run(Query3, 'ALTER TABLE PONTOZO ADD PO_DOKOD VARCHAR(5)', false);
   end;
   if not Query_Run(Query3, 'SELECT PO_FEKOD FROM PONTOZO') then begin
       Query_Run(Query3, 'ALTER TABLE PONTOZO ADD PO_FEKOD VARCHAR(5)');
       Query_Run(Query3, 'ALTER TABLE PONTOZO DROP COLUMN PO_DOKOD');
   end;
	Inherited FormCreate(Sender);
	AddSzuromezoRovid('PO_ALKOD',  'PONTOZO');
	AddSzuromezoRovid('PO_FEKOD',  'PONTOZO');
	AddSzuromezoRovid('PO_DOKOD',  'PONTOZO');
	AddSzuromezoRovid('DO_NAME',   'DOLGOZO');
	AddSzuromezoRovid('JE_FENEV',  'JELSZO');
	AddSzuromezo('SZ_MENEV', 'SELECT DISTINCT SZ_MENEV FROM SZOTAR WHERE SZ_FOKOD = ''400'' ');
   FelTolto('Új pontozó hozzárendelés felvitele ', 'Pontozó hozzárendelés módosítása ',
			'Pontozók listája', 'PO_POKOD',
           'SELECT * FROM PONTOZO, JELSZO, SZOTAR, DOLGOZO WHERE PO_FEKOD = JE_FEKOD AND PO_ALKOD = SZ_ALKOD AND SZ_FOKOD = ''400'' AND PO_DOKOD= DO_KOD',
           'PONTOZO',
            QUERY_KOZOS_TAG);
	width	:= DLG_WIDTH;
	height	:= DLG_HEIGHT;
end;

procedure TPontozoFmDlg.Adatlap(cim, kod : string);
begin
	Application.CreateForm(TPontozobeDlg, AlForm);
	Inherited Adatlap(cim, kod );
end;

end.

