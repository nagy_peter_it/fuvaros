unit Info_;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, StdCtrls, Grids, DB, ADODB, DBGrids, ExtCtrls, Mask,
  DBCtrls,  Shellapi,jpeg;

type
  TFInfo_ = class(TForm)
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    TabSheet3: TTabSheet;
    TabSheet4: TTabSheet;
    TabSheet5: TTabSheet;
    Memo1: TMemo;
    Memo2: TMemo;
    Memo3: TMemo;
    Memo4: TMemo;
    TabSheet6: TTabSheet;
    Memo5: TMemo;
    TabSheet7: TTabSheet;
    Memo6: TMemo;
    TabSheet8: TTabSheet;
    TDolgozo: TADODataSet;
    DSDolgozo: TDataSource;
    TTavollet: TADODataSet;
    TTavolletTA_DOKOD: TStringField;
    TTavolletTA_DONEV: TStringField;
    TTavolletTA_HONAP: TStringField;
    TTavolletTA_NAP01: TStringField;
    TTavolletTA_NAP02: TStringField;
    TTavolletTA_NAP03: TStringField;
    TTavolletTA_NAP04: TStringField;
    TTavolletTA_NAP05: TStringField;
    TTavolletTA_NAP06: TStringField;
    TTavolletTA_NAP07: TStringField;
    TTavolletTA_NAP08: TStringField;
    TTavolletTA_NAP09: TStringField;
    TTavolletTA_NAP10: TStringField;
    TTavolletTA_NAP11: TStringField;
    TTavolletTA_NAP12: TStringField;
    TTavolletTA_NAP13: TStringField;
    TTavolletTA_NAP14: TStringField;
    TTavolletTA_NAP15: TStringField;
    TTavolletTA_NAP16: TStringField;
    TTavolletTA_NAP17: TStringField;
    TTavolletTA_NAP18: TStringField;
    TTavolletTA_NAP19: TStringField;
    TTavolletTA_NAP20: TStringField;
    TTavolletTA_NAP21: TStringField;
    TTavolletTA_NAP22: TStringField;
    TTavolletTA_NAP23: TStringField;
    TTavolletTA_NAP24: TStringField;
    TTavolletTA_NAP25: TStringField;
    TTavolletTA_NAP26: TStringField;
    TTavolletTA_NAP27: TStringField;
    TTavolletTA_NAP28: TStringField;
    TTavolletTA_NAP29: TStringField;
    TTavolletTA_NAP30: TStringField;
    TTavolletTA_NAP31: TStringField;
    TTavolletTA_AKTIV: TIntegerField;
    TTavolletTA_O_X: TIntegerField;
    TTavolletTA_O_SZ: TIntegerField;
    TTavolletTA_O_B: TIntegerField;
    TTavolletTA_O_EI: TIntegerField;
    TTavolletTA_O_EL: TIntegerField;
    TTavolletTA_BERKOD: TStringField;
    TTavolletTA_CSKOD: TStringField;
    TTavolletTA_CSNEV: TStringField;
    DBGrid1: TDBGrid;
    Panel1: TPanel;
    TTavolletDO_NAME: TStringField;
    TTavolletDO_BELEP: TStringField;
    TTavolletDO_RENDSZ: TStringField;
    TTavolletDO_CSNEV: TStringField;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    Label1: TLabel;
    Label2: TLabel;
    DBEdit4: TDBEdit;
    TTavolletDO_SZABI: TIntegerField;
    TTavolletDO_KSZAB: TIntegerField;
    DBEdit5: TDBEdit;
    Label3: TLabel;
    Label4: TLabel;
    Image2: TImage;
    TDFOTO: TADOQuery;
    TDFOTODO_KOD: TStringField;
    TDFOTODO_FOTO: TBlobField;
    Button1: TButton;
    TNapok: TADOQuery;
    TNapokNA_ORSZA: TStringField;
    TNapokNA_DATUM: TDateTimeField;
    TNapokNA_UNNEP: TIntegerField;
    TNapokNA_MEGJE: TStringField;
    TNapokNA_EV: TIntegerField;
    TNapokNA_HO: TIntegerField;
    TabSheet9: TTabSheet;
    Memo8: TMemo;
    ComboBox1: TComboBox;
    ComboBox2: TComboBox;
    Csoportok: TADODataSet;
    CsoportokTA_CSNEV: TStringField;
    CheckBox1: TCheckBox;
    DSFelhasznalo: TDataSource;
    DBGrid2: TDBGrid;
    QFelhasznalo: TADOQuery;
    QFelhasznaloJE_FEKOD: TStringField;
    QFelhasznaloJE_FENEV: TStringField;
    QFelhasznaloJE_BEDAT: TStringField;
    QFelhasznaloJE_BEIDO: TStringField;
    QFelhasznaloJE_SWVER: TStringField;
    Panel2: TPanel;
    cbValtozaslist: TComboBox;
    Label5: TLabel;
    Button2: TButton;
    meEv: TMaskEdit;
    procedure FormCreate(Sender: TObject);
    procedure Memo1DblClick(Sender: TObject);
    procedure SG1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormActivate(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
    procedure DBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure DBEdit1Enter(Sender: TObject);
    procedure FormDeactivate(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure TTavolletAfterScroll(DataSet: TDataSet);
    procedure Image2Click(Sender: TObject);
    procedure ComboBox1Change(Sender: TObject);
    procedure CheckBox1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    elo_nap    : integer;
    elo_km     : integer;
  end;

var
  FInfo_: TFInfo_;
  kep2:string;
  valtozas_filelist: TStringList;

implementation

uses Fomenu, Egyeb, DateUtils,Kozos, J_Sql;

{$R *.dfm}

procedure TFInfo_.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  try   // Access Violation ellen
   if valtozas_filelist <> nil then
      valtozas_filelist.Free;
  except
     null;
      end;  // try-except
end;

procedure TFInfo_.FormCreate(Sender: TObject);
var
   fn, valtozasdir, displayname, S: string;
   kereses: TSearchRec;
   talalat: integer;
   elemek: TStringList;
begin
   EgyebDlg.SeTADOQueryDatabase(QFelhasznalo);
   Query_Run(QFelhasznalo, 'SELECT JE_FEKOD, JE_FENEV, JE_BEDAT, JE_BEIDO, JE_SWVER FROM JELSZO WHERE JE_BEDAT <> '''' ORDER BY JE_BEDAT DESC, JE_BEIDO DESC');
   FInfo_.PageControl1.Pages[2].TabVisible := ( ( GetRightTag(549) <> RG_NORIGHT	) or EgyebDlg.user_super );
   FInfo_.PageControl1.Pages[3].TabVisible := ( ( GetRightTag(550) <> RG_NORIGHT	) or EgyebDlg.user_super );
   FInfo_.PageControl1.Pages[4].TabVisible := ( ( GetRightTag(553) <> RG_NORIGHT	) or EgyebDlg.user_super );
   FInfo_.PageControl1.Pages[5].TabVisible := ( ( GetRightTag(554) <> RG_NORIGHT	) or EgyebDlg.user_super );
   FInfo_.PageControl1.Pages[6].TabVisible := ( ( GetRightTag(555) <> RG_NORIGHT	) or EgyebDlg.user_super );
   FInfo_.PageControl1.Pages[8].TabVisible := ( ( GetRightTag(556) <> RG_NORIGHT	) or EgyebDlg.user_super );
   // Az �llom�ny beolvas�sa az els� oldalra
   fn := EgyebDlg.ExePath + 'verzio.txt';
   FInfo_.PageControl1.Pages[1].TabVisible := false;
   if FileExists(fn) then begin
       FInfo_.PageControl1.Pages[1].TabVisible := true;
       FInfo_.Memo1.Lines.LoadFromFile(fn);
   end;
   meEv.Text:= EgyebDlg.EvSzam;
   // ----- V�ltoz�slist�k a combo list�j�ba ------------
   valtozas_filelist:= TStringList.Create;
   elemek:= TStringList.Create;
   try
     valtozasdir:= EgyebDlg.Read_SZGrid('999','757');  // V�ltoz�slista PDF-ek k�nyvt�ra
     talalat		:= FindFirst(valtozasdir+'\*.pdf', faAnyFile, kereses);
     while talalat = 0 do begin
        fn:= kereses.Name;   // pl. JS_SPEED-FUVAROS-VALT-V415.pdf
        StringParse(fn, '.', elemek);
        S:=elemek[0];   // JS_SPEED-FUVAROS-VALT-V415
        StringParse(S, '-', elemek);
        displayname:= elemek[elemek.Count-1];  // az utols�
        displayname:=copy(displayname,1,2)+'.'+copy(displayname,3,100); // --> "V4.15"
        displayname:= 'V�ltoz�slista '+displayname;
        cbValtozaslist.Items.Add(displayname);
        valtozas_filelist.Add(fn);
        talalat:= FindNext(kereses);
        end;  // while
     FindClose(kereses);
     if cbValtozaslist.Items.Count>0 then
         cbValtozaslist.ItemIndex:=cbValtozaslist.Items.Count-1; // az utols�
   finally
     if elemek<>nil then elemek.Free;
     end;  // try-finally

   // ---------------------------------------------------
   elo_nap  := 0;
   elo_km   := 0;
end;

procedure TFInfo_.Memo1DblClick(Sender: TObject);
begin
  if FInfo_.WindowState=wsNormal then
    FInfo_.WindowState:=wsMaximized
  else
    FInfo_.WindowState:=wsNormal;
end;

procedure TFInfo_.SG1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key=VK_ESCAPE then
     FomenuDlg.Close;
end;

procedure TFInfo_.FormActivate(Sender: TObject);
begin
  Left:=0;
  Top:=FomenuDlg.Top+ FomenuDlg.Height-Height-FomenuDlg.StatusBar1.Height;
  FomenuDlg.Timer2.Enabled:=True;
  //FomenuDlg.Timer2Timer(Sender);
  FomenuDlg.Timer2.OnTimer(Self);
end;

procedure TFInfo_.PageControl1Change(Sender: TObject);
begin
   Screen.Cursor   := crHourGlass;
   EllenListTorol;
 if PageControl1.ActivePageIndex=7 then  // T�voll�t
 begin
  Csoportok.Close;
  ComboBox2.Items.Clear;
  ComboBox2.Items.Add('*Minden csoport*');
  Csoportok.Open;
  Csoportok.First;
  while not Csoportok.Eof do
  begin
    ComboBox2.Items.Add(CsoportokTA_CSNEV.Value);
    Csoportok.Next;
  end;
  Csoportok.Close;
  ComboBox2.ItemIndex:=0;
  ///////////////////////////////////
  ComboBox1.ItemIndex:=MonthOf(date)-1;
  ComboBox1.OnChange(self);
 end;

   if PageControl1.ActivePageIndex=2 then begin // G�pkocsi ell.
 		EgyebDlg.GepkocsiErvEllenor(elo_nap, elo_km);
       Memo2.Text:=EgyebDlg.ellenlista.Text;
   end;

   if PageControl1.ActivePageIndex=3 then begin // Dolgoz� ell.
 		EgyebDlg.DolgozoErvEllenor(elo_nap, elo_km);
       Memo3.Text:=EgyebDlg.ellenlista.Text;
   end;

   if PageControl1.ActivePageIndex=4 then begin // Vevo ell.
 		EgyebDlg.VevoErvEllenor(elo_nap, elo_km);
       Memo4.Text:=EgyebDlg.ellenlista.Text;
   end;

   if PageControl1.ActivePageIndex=5 then begin // Z�r� km ell.
 		FInfo_.PageControl1.Pages[5].TabVisible:= (EgyebDlg.HaviZaroKmEllenor(EgyebDlg.EvSzam)>0);
       FInfo_.Memo5.Text   :=EgyebDlg.ellenlista.Text;
   end;

   if PageControl1.ActivePageIndex=6 then begin // Pozicioszam ell.
 		EgyebDlg.PozicioszamEllenor(EgyebDlg.EvSzam);
       Memo6.Text:=EgyebDlg.ellenlista.Text;
   end;

   if PageControl1.ActivePageIndex=8 then begin // Szerviz ell.
 		EgyebDlg.SzervizEllenor;
       Memo8.Text:=EgyebDlg.ellenlista.Text;
   end;

   EllenListTorol;
   Screen.Cursor   := crDefault;
end;

procedure TFInfo_.DBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
  DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
 // if (Odd(TDolgozo.RecNo)) then
 //  dbGrid1.Canvas.Brush.Color :=$0080FFFF;

  Try
    if Column.Title.Font.Color=clRed then
      DBGrid1.Canvas.Brush.Color:=$00FFF4BB;             //clAqua;
  { if (datacol>0) and
    (DayOfWeek(EncodeDate(StrToInt(EgyebDlg.EvSzam),MonthOf(date)  ,
      strtoint(DBGrid1.Columns[datacol].Title.Caption))) in [1,7]) then
      DBGrid1.Canvas.Brush.Color:=$00FFF4BB;             //clAqua;
      }
  Except
  End;
    // if (datacol>0) and (EncodeDate(StrToInt(EgyebDlg.EvSzam),MonthOf(date),strtoint(DBGrid1.Columns[datacol].Title.Caption))=date) then
    // if (datacol>0) and (EncodeDate(YearOf(date),MonthOf(date),strtoint(DBGrid1.Columns[datacol].Title.Caption))=date) then
     if (datacol>0) and (EncodeDate(YearOf(date),ComboBox1.ItemIndex+1,strtoint(DBGrid1.Columns[datacol].Title.Caption))=date) then
      DBGrid1.Canvas.Brush.Color:=clLime;

{
  if gdFocused in state	then
  begin
    DBGrid1.Canvas.Brush.Color:=clHighlight;
    DBGrid1.Canvas.Font.Color := clWhite;
    //DBGrid1.Canvas.Brush.Color := clNavy;
    //RX:=Rect.Right;
    //RY:=Rect.Bottom;
  end;
 }
  DBGrid1.DefaultDrawColumnCell(Rect,DataCol,Column , State);

end;

procedure TFInfo_.DBEdit1Enter(Sender: TObject);
begin
  DBGrid1.SetFocus;
end;

procedure TFInfo_.FormDeactivate(Sender: TObject);
begin
  if FomenuDlg.NELSOINFO then
  begin
    //FInfo_.SetFocus;
    FInfo_.Visible:=False;
    //FomenuDlg.Timer2.Enabled:=False;
  end;
  FomenuDlg.NELSOINFO:=True;
  Try
    FInfo_.SetFocus;
  Except
  End;
end;

procedure TFInfo_.Button1Click(Sender: TObject);
var
  stream: TMemoryStream;
  jpeg: TJPEGImage;
begin
  TDFOTO.Close;
  TDFOTO.Parameters[0].Value:=TTavolletTA_DOKOD.Value;
  TDFOTO.Open;
  TDFOTO.First;

  Image2.Picture.Graphic := nil;
 if not TDFOTO.Eof then
 begin
  try
    jpeg:=TJPEGImage.Create;
    stream:=TMemoryStream.Create;

    TBlobField(TDFOTODO_FOTO).SaveToStream(stream);
    stream.Position:=0;
    jpeg.LoadFromStream(stream);
    jpeg.Scale:=jsFullSize;
    jpeg.Performance:=jpBestQuality;
    jpeg.ProgressiveDisplay:=true;
    Image2.Picture.Graphic:= nil;
    Image2.Picture.Graphic:=jpeg;

    stream.Free;
    jpeg.Free;
  except
    on EInvalidGraphic do
      Image2.Picture.Graphic := nil;
  end;
 end;

end;

procedure TFInfo_.TTavolletAfterScroll(DataSet: TDataSet);
begin
  Button1.OnClick(self)
end;

procedure TFInfo_.Image2Click(Sender: TObject);
begin
  if TDFOTODO_FOTO.BlobSize>0 then
  begin
    //kep2:=LMDSysInfo1.TempPath+'tmp_2.jpg';
    kep2:=EgyebDlg.TempPath+'tmp_2.jpg';
    TDFOTODO_FOTO.SaveToFile(kep2);
    Application.ProcessMessages;
    ShellExecute(Handle, 'open', PChar(kep2), nil, PChar(ExtractFilePath(kep2)), SW_SHOWDEFAULT)
  end;
end;

procedure TFInfo_.ComboBox1Change(Sender: TObject);
var
  sho,ma,sev:string;
  i,n:integer;
  nap: Tdatetime;
begin
  TNapok.Close;
  TNapok.Open;
  DBGrid1.SetFocus;
  for i:=1 to 31 do
  begin
    DBGrid1.Columns[i].Title.Caption:=inttostr(i);
    try
        // nap:=EncodeDate(StrToInt(EgyebDlg.EvSzam),ComboBox1.ItemIndex+1 ,i);
        nap:=EncodeDate(StrToInt(meEv.Text),ComboBox1.ItemIndex+1 ,i);
        if DayOfWeek(nap) in [1,7] then                     // h�tv�ge
          DBGrid1.Columns[i].Title.Font.Color:=clRed
        else
          DBGrid1.Columns[i].Title.Font.Color:=clWindowText;

        if TNapok.Locate('NA_DATUM',nap,[loCaseInsensitive]) then
        begin
          if TNapokNA_UNNEP.Value=1 then  // �nnep
            DBGrid1.Columns[i].Title.Font.Color:=clRed
          else
            DBGrid1.Columns[i].Title.Font.Color:=clWindowText;
        end;

        DBGrid1.Columns[i].Visible:=True;
    except
      on E: EConvertError do
        DBGrid1.Columns[i].Visible:=False;
    end;
  end;
  ///////////////////////////////////

  if CheckBox1.Checked then
      sev:=IntToStr(YearOf(date))
  else sev:= meEv.Text;
  // if MonthOf(date)<10 then
  if ComboBox1.ItemIndex+1 < 10 then
    sho:='0'+IntToStr(ComboBox1.ItemIndex+1)
  else
    sho:=IntToStr(ComboBox1.ItemIndex+1);
  if DayOf(date)<10 then
    ma:='0'+IntToStr(DayOf(date))
  else
    ma:=IntToStr(DayOf(date));

  TTavollet.Close;
  if ComboBox2.ItemIndex=0 then     // Minden csoport
  begin
   if CheckBox1.Checked then
    TTavollet.CommandText:='select * from TAVOLLET where (TA_EV='''+sev+''') and (TA_HONAP='''+sho+''') and (not TA_NAP'+ma+' is null)and (TA_NAP'+ma+'<>'''+''')and (TA_NAP'+ma+'<>'''+'X'''+')'
   else
    TTavollet.CommandText:='select * from TAVOLLET where (TA_EV='''+sev+''') and (TA_HONAP='''+sho+''') and (ta_o_x>0 or ta_o_sz>0 or ta_o_b>0 or ta_o_ei>0 or ta_o_el>0) order by ta_donev';
  end;

  if ComboBox2.ItemIndex>0 then    // Csak egy csoport
  begin
   if CheckBox1.Checked then
    TTavollet.CommandText:='select * from TAVOLLET where (TA_CSNEV= :CSN)and(TA_EV='''+sev+''') and (TA_HONAP='''+sho+''') and (not TA_NAP'+ma+' is null)and (TA_NAP'+ma+'<>'''+''')and (TA_NAP'+ma+'<>'''+'X'''+')'
   else
    TTavollet.CommandText:='select * from TAVOLLET where (TA_CSNEV= :CSN)and(TA_EV='''+sev+''') and (TA_HONAP='''+sho+''') and (ta_o_x>0 or ta_o_sz>0 or ta_o_b>0 or ta_o_ei>0 or ta_o_el>0) order by ta_donev';

   TTavollet.Parameters[0].Value:=ComboBox2.Text;
  end;

//  TTavollet.CommandText:='select * from TAVOLLET where (TA_EV='''+sev+''') and (TA_HONAP='''+sho+''') and (not TA_NAP'+ma+' is null)and (TA_NAP'+ma+'<>'''+''')and (TA_NAP'+ma+'<>'''+'X'''+')';
//  TTavollet.CommandText:='select * from TAVOLLET where (TA_EV='''+sev+''') and (TA_HONAP='''+sho+''') and (not TA_NAP'+ma+' is null)and (TA_NAP'+ma+'<>'''+''')and (TA_NAP'+ma+'<>'''+'X'''+')';
//  TTavollet.CommandText:='select * from TAVOLLET where (TA_HONAP='''+sho+''')';
  //TTavollet.Parameters[0].Value:= sho;
  TTavollet.Open;
  TDolgozo.Close;
  TDolgozo.Open;
  TNapok.Close;

end;

procedure TFInfo_.Button2Click(Sender: TObject);
var
  path, fn: string;
begin
  // fn:= EgyebDlg.Read_SZGrid('999','757')+'\'+cbValtozaslist.Items[cbValtozaslist.ItemIndex];
  fn:= EgyebDlg.Read_SZGrid('999','757')+'\'+valtozas_filelist[cbValtozaslist.ItemIndex];
  ShellExecute(Handle, 'open', PChar(fn), nil, PChar(fn), SW_SHOWMAXIMIZED);  // SW_SHOWMAXIMIZED  // SW_SHOWDEFAULT
end;

procedure TFInfo_.CheckBox1Click(Sender: TObject);
begin
  if CheckBox1.Checked then begin
    meEV.Text:= IntToStr(YearOf(date));
    ComboBox1.ItemIndex:=MonthOf(date)-1;
    end;
  ComboBox1.OnChange(self);
  ComboBox1.Enabled:=not CheckBox1.Checked;
end;

end.
