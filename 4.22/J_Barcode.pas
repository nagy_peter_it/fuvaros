unit J_Barcode;

interface

uses
	StdCtrls, Classes, SysUtils;

const
	NO_BARCODE  = 0;
	EAN_8			= 1;
  UCC_12      = 2;
  EAN_13		= 3;
  EAN_14		= 4;
  SSCC			= 5;

	BC_LENGTH	: array[0..5]  of integer = (0, 8, 12, 13, 14, 18 );
	multi			: array[1..17] of integer = (3,1,3,1,3,1,3,1,3,1,3,1,3,1,3,1,3);


  procedure	FillBarcodeList(Combo : TComboBox; kodlist : TStringList);
	function 	GetBcLastDigit(bc_str	: string; bc_type : integer) : integer;
	function 	CheckBarCode(bc_str	: string; bc_type : integer) : boolean;
	function 	KodKapocs(kod1 : string) : string;


implementation

procedure FillBarcodeList(Combo : TComboBox; kodlist : TStringList);
begin
	Combo.Clear;
 	kodlist.Clear;
	Combo.Items.Add('EAN_8');
	Combo.Items.Add('UCC_12');
	Combo.Items.Add('EAN_13');
	Combo.Items.Add('EAN_14');
	Combo.Items.Add('SSCC');
	kodlist.Add('1');
	kodlist.Add('2');
	kodlist.Add('3');
	kodlist.Add('4');
	kodlist.Add('5');
  Combo.ItemIndex	:= 0;
end;

function CheckBarCode(bc_str	: string; bc_type : integer) : boolean;
begin
	Result	:= false;
  if bc_type < 1 then begin
  	Exit;
  end;
  if IntToStr(GetBcLastDigit(copy(bc_str,1,BC_LENGTH[bc_type]-1),bc_type)) = copy(bc_str,BC_LENGTH[bc_type],1) then begin
		Result	:= true;
  end;
end;

function GetBcLastDigit(bc_str	: string; bc_type : integer) : integer;
var
	summa		: integer;
  i			: integer;
  tmpstr   : string;
begin
	Result	:= -1;
  if ((  bc_type < 1 ) or (length(bc_str) <> BC_LENGTH[bc_type] - 1) ) then begin
  	Exit;
  end;
  tmpstr   := copy('00000000000000000000',1,18-BC_LENGTH[bc_type]) + bc_str;
  summa		:= 0;
  for i := 1 to 17 do begin
  	summa	:= summa + multi[i]*StrToIntDef(copy(tmpstr,i,1),0);
  end;
  Result := ( 10 - ( summa mod 10 ) ) mod 10;
end;

function KodKapocs(kod1 : string) : string;
var
	elso	: char;
begin
	if Length(kod1) = 7 then begin
 		elso := kod1[1];
     	kod1 := elso+elso+elso+elso+elso+elso+kod1;
  	end;
	while Pos('�', kod1) > 0 do begin
   	kod1	:= copy(kod1, 1, Pos('�', kod1) -1 ) + '0' + copy(kod1, Pos('�', kod1) + 1, 999 );
//    	kod1[Pos('�', kod1)] := '0';
  	end;
 	Result := kod1;
end;

end.
