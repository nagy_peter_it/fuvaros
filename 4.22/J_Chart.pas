unit J_Chart;

interface

uses
	Classes, Chart, TeEngine, Series, SysUtils;

const
	MAXSERIES		= 10;

type
	TJCHART = class(TComponent)
  	private
   	MainTitle	: TStringList;
      BottomTitle	: TStringList;
      Chart			: TChart;
	public
      D3Chart		: boolean;
      Legend		: boolean;
      SeriesNr		: integer;
      Series		: array [1..MAXSERIES] of TChartSeries;
    	constructor Create(AOwner: TComponent); override;
    	destructor 	Destroy; override;
      procedure	AssignChart( TargetChart : TChart);
      procedure	ShowChart;
      procedure	AddTitles(t1, t2 : TStringList);
		procedure	AddTitleStr(t1, t2 : string);
(*
      function		OpenXlsFile(fn : string) : boolean;
		function		SaveXlsFile : boolean;
      function		SaveFileAsXls(fn : string) : boolean;
      function		SaveFileAsHtml(fn : string) : boolean;
   	function		FillRow(rnum : integer) : boolean;
      function		GetRowcell(cnum : integer)	: string;
      function		ReadCell(rnum, cnum : integer)	: string;
      function		WriteCell(rnum, cnum : integer ; valu : string)	: boolean;
		function		InitRow : boolean;
		function		SaveRow(rnum : integer) : boolean;
		function		SetRowcell(cnum : integer; val : string ) : boolean;
  	private
		function 	GetColumnName( colnum : integer) : string;
		procedure 	SetColNumber( colnum : integer) ;
  	published
   	property 	ColNumber : integer read FColNumber write SetColnumber;
      property		FileName : string read ExcelName write ExcelName;
*)
  end;

var
  JChart: TJChart;

implementation

constructor TJChart.Create(AOwner: TComponent);
begin
	// Az oszt�ly elemek l�trehoz�sa
  	inherited Create(AOwner);
  	MainTitle	:= TStringList.Create;
   BottomTitle	:= TStringList.Create;
   D3Chart		:= false;
   Legend		:= false;
   Chart			:= nil;
   SeriesNr		:= 0;
end;

destructor TJChart.Destroy;
begin
	// Itt j�nnek a megsz�ntet�sek
   BottomTitle.Free;
   MainTitle.Free;
  	inherited Destroy;
end;

procedure	TJChart.AssignChart( TargetChart : TChart);
begin
	Chart	:= TargetChart;
end;

procedure	TJChart.ShowChart;
var
	i : integer;
begin
	if Chart <> nil then begin
   	// A c�mek megad�sa
   	Chart.Title.Text.Assign(MainTitle);
		Chart.Foot.Text.Assign(BottomTitle);
		Chart.View3D 			:= D3Chart;
      Chart.Legend.Visible	:= Legend;
      if SeriesNr = 0 then begin
      	// �res eset�n egy demo legener�l�sa
			Series[1]				:= TAreaSeries.Create(self);
  			Series[1].FillSampleValues(10);
   		Series[1].Title      := 'Title1';
         SeriesNr					:= 1;
      end;
      // A sorozatok hozz�rendel�se
      for i := 1 to SeriesNr do begin
			Series[i].ParentChart 	:= Chart;
      end;
		Chart.Repaint;
   end;
end;

procedure	TJChart.AddTitles(t1, t2 : TStringList);
begin
	if t1 <> nil then begin
   	MainTitle.Assign(t1);
   end;
	if t2 <> nil then begin
   	BottomTitle.Assign(t2);
   end;
end;

procedure	TJChart.AddTitleStr(t1, t2 : string);
begin
	if t1 <> '' then begin
   	MainTitle.Clear;
      MainTitle.Add(t1);
   end;
	if t2 <> '' then begin
   	BottomTitle.Clear;
      BottomTitle.Add(t2);
   end;
end;

(*
function TJExcel.GetColumnName( colnum : integer) : string;
//******************************************************//
// Creates the column name from the number (eg. 27 = 'AA')
//******************************************************//
begin
	Result	:= '';
	if ( ( colnum < 1 ) or ( colnum > 256 ) ) then begin
   	Exit;
   end;
   if colnum < 27 then begin
   	Result	:= Chr(64+colnum);
   end else begin
  		Result	:= Chr(64+((colnum - 1) DIV 26))+Chr(64+((colnum - 1) MOD 26)+1);
   end;
end;

procedure TJExcel.SetColNumber( colnum : integer);
begin
	if ( ( colnum < 1 ) or ( colnum > 256 ) ) then begin
   	Exit;
   end;
   FColNumber	:= colnum;
end;

function	TJExcel.OpenXlsFile(fn : string) : boolean;
begin
	Result		:= false;
	ExcelName 	:= fn;
	if not FileExists(fn) then begin
   	// �j workbook hozz�ad�sa
   	EXAPP.WorkBooks.Add(xlWBatWorkSheet);
   end;
   try
   	EXAPP.WorkBooks.Add(fn);
      Result	:= true;
   except
   end;
end;

function	TJExcel.SaveXlsFile : boolean;
begin
	Result		:= false;
   try
   	EXAPP.ActiveWorkBook.SaveAs(ExcelName,xlWorkbookNormal);
      Result	:= true;
   except
   end;
end;

function	TJExcel.SaveFileAsXls(fn : string) : boolean;
begin
	Result	:= false;
   try
   	EXAPP.ActiveWorkBook.SaveAs(fn,xlWorkbookNormal);
      Result	:= true;
   except
   end;
end;

function	TJExcel.SaveFileAsHtml(fn : string) : boolean;
begin
	Result	:= false;
   try
   	EXAPP.ActiveWorkBook.SaveAs(fn,xlHtml);
      Result	:= true;
   except
   end;
end;

function	TJExcel.FillRow(rnum : integer) : boolean;
begin
	Result	:= false;
   try
		RowArr 	:= EXAPP.Range['A'+IntToStr(rnum), GetColumnName(ColNumber)+IntToStr(rnum)].Value;
      Result	:= true;
   except
   end;
end;

function	TJExcel.InitRow : boolean;
begin
	Result	:= false;
   try
     	RowArr	:= VarArrayCreate([1,ColNumber],varVariant);
      Result	:= true;
   except
   end;
end;

function	TJExcel.SaveRow(rnum : integer) : boolean;
begin
	Result	:= false;
   try
		EXAPP.Range['A'+IntToStr(rnum), GetColumnName(ColNumber)+IntToStr(rnum)].Value := RowArr;
      Result	:= true;
   except
   end;
end;

function	TJExcel.GetRowcell(cnum : integer)	: string;
begin
	Result	:= '';
	if ( ( cnum < 1 ) or ( cnum > ColNumber) ) then begin
   	Exit;
   end;
   Result	:= RowArr[1,cnum];
end;

function	TJExcel.SetRowcell(cnum : integer; val : string ) : boolean;
begin
	Result	:= false;
	if ( ( cnum < 1 ) or ( cnum > ColNumber) ) then begin
   	Exit;
   end;
   RowArr[cnum]	:= val;
   Result  := true;
end;

function	TJExcel.ReadCell(rnum, cnum : integer)	: string;
begin
	Result	:= '';
	if ( ( cnum < 1 ) or ( cnum > 256) ) then begin
   	Exit;
   end;
   Result	:= EXAPP.ActiveWorkBook.Worksheets[1].Cells[rnum, cnum];
end;

function	TJExcel.WriteCell(rnum, cnum : integer ; valu : string)	: boolean;
begin
	Result	:= true;
   EXAPP.Range[GetColumnName(cnum)+IntToStr(rnum)].select;
   EXAPP.ActiveCell	:= valu;
end;
*)

end.
