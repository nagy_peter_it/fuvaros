unit ValEllenor;

interface

uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, DBTables, DB, StdCtrls, Buttons, Mask, DBCtrls,
  Printers, ExtCtrls, CheckLst, ADODB, Grids;

type
  TValellenorDlg = class(TForm)
	 BitElkuld: TBitBtn;
	 BitKilep: TBitBtn;
    CLB1: TCheckListBox;
	 SpeedButton3: TSpeedButton;
	 Query1: TADOQuery;
	 SpeedButton1: TSpeedButton;
    SpeedButton2: TSpeedButton;
    SpeedButton4: TSpeedButton;
	 procedure BitBtn6Click(Sender: TObject);
	 procedure BitElkuldClick(Sender: TObject);
	 procedure BitKilepClick(Sender: TObject);
	 procedure Tolt(sg : TStringGrid);
	 procedure FormShow(Sender: TObject);
	 procedure FormDestroy(Sender: TObject);
	 procedure FormCreate(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
  private
		kodlist		: TStringList;
  public
		rszlist		: TStringList;
		kilepes		: boolean;
		kodkilist   : TStringList;		// A rendsz�mok list�ja
  end;

var
  ValellenorDlg: TValellenorDlg;

implementation

uses
	Egyeb, Kozos, J_SQL;
{$R *.DFM}

procedure TValellenorDlg.BitBtn6Click(Sender: TObject);
begin
	Close;
end;

procedure TValellenorDlg.BitElkuldClick(Sender: TObject);
var
	i : integer;
begin
	// Itt �ll�tjuk �ssze a visszaadand� stringeket
  kilepes	:= false;
  kodkilist.Clear;
  for i := 0 to CLB1.Items.Count - 1 do begin
	  if CLB1.Checked[i] = true then begin
		 kodkilist.Add(kodlist[i]);
	  end;
  end;
  Close;
end;

procedure TValellenorDlg.BitKilepClick(Sender: TObject);
begin
	kodkilist.Clear;
	kilepes	:= true;
	Close;
end;

procedure TValellenorDlg.Tolt(sg : TStringGrid);
var
	i 		: integer;
	sorsz   : integer;
begin
	kilepes	:= false;
	CLB1.Clear;
	// A j�ratok beolvas�sa
	Screen.Cursor	:= crHourGlass;
	for i := 0 to sg.RowCOunt -1 do begin
		kodlist.Add(sg.Cells[0,i]);
		sorsz := CLB1.Items.Add( sg.Cells[1,i] );
		if VanNyitottSzamla(sg.Cells[0,i]) then begin
			CLB1.ItemEnabled[sorsz]	:= false;
			CLB1.Items[sorsz]		:= CLB1.Items[sorsz] + ' Nem v�gleges�tett sz�mla!';
		end;
	end;
	Screen.Cursor	:= crDefault;
	CLB1.ItemIndex	:= 0;
end;

procedure TValellenorDlg.FormShow(Sender: TObject);
begin
	CLB1.SetFocus;
end;

procedure TValellenorDlg.FormDestroy(Sender: TObject);
begin
	kodlist.Free;
	kodkilist.Free;
end;

procedure TValellenorDlg.FormCreate(Sender: TObject);
begin
	kodlist		:= TStringList.Create;
	kodkilist	:= TStringList.Create;
end;

procedure TValellenorDlg.SpeedButton3Click(Sender: TObject);
var
	i 	: integer;
begin
	// Mindent kijel�l
	for i := 1 to CLB1.Items.Count - 1 do begin
		if CLB1.ItemEnabled[i] then begin
			CLB1.Checked[i] := true;
		end;
	end;
end;

procedure TValellenorDlg.SpeedButton1Click(Sender: TObject);
var
	i 	: integer;
begin
	// Mindent kijel�l�s megsz�ntet�se
	for i := 1 to CLB1.Items.Count - 1 do begin
		if CLB1.ItemEnabled[i] then begin
			CLB1.Checked[i] := false;
		end;
	end;
end;

procedure TValellenorDlg.SpeedButton2Click(Sender: TObject);
var
	rsz	: string;
	i 	: integer;
begin
	// A megegyez� rendsz�mok bejel�l�se
	rsz	:= copy(CLB1.Items[CLB1.ItemIndex],Pos('  Rsz.: ', CLB1.Items[CLB1.ItemIndex])+9,999);
	for i := 1 to CLB1.Items.Count - 1 do begin
		if copy(CLB1.Items[i],Pos('  Rsz.: ', CLB1.Items[i])+9,999) = rsz then begin
			if CLB1.ItemEnabled[i] then begin
				CLB1.Checked[i] := true;
			end;
		end;
	end;
end;

procedure TValellenorDlg.SpeedButton4Click(Sender: TObject);
var
	rsz	: string;
	i 	: integer;
begin
	// A megegyez� rendsz�mok bejel�l�s�nek megsz�ntet�se
	rsz	:= copy(CLB1.Items[CLB1.ItemIndex],Pos('  Rsz.: ', CLB1.Items[CLB1.ItemIndex])+9,999);
	for i := 1 to CLB1.Items.Count - 1 do begin
		if copy(CLB1.Items[i],Pos('  Rsz.: ', CLB1.Items[i])+9,999) = rsz then begin
			if CLB1.ItemEnabled[i] then begin
				CLB1.Checked[i] := false;
			end;
		end;
	end;
end;

end.


