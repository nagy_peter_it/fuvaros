unit FizExport;

interface

uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, DBTables, DB, StdCtrls, Buttons, Mask, DBCtrls,
  ExtCtrls, Shellapi, ADODB;

type
  TFizExportDlg = class(TForm)
    BitBtn6: TBitBtn;
    Label1: TLabel;
    M2: TMaskEdit;
    M3: TMaskEdit;
    Label4: TLabel;
    M0: TMaskEdit;
    M4: TMaskEdit;
    BtnExport: TBitBtn;
    Label2: TLabel;
    M1: TMaskEdit;
    M5: TMaskEdit;
    Label3: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    M6: TMaskEdit;
    Query1: TADOQuery;
    Query2: TADOQuery;
    Query3: TADOQuery;
    Button1: TButton;
    QueryKozos: TADOQuery;
    Query4: TADOQuery;
    M20: TMaskEdit;
    Label12: TLabel;
    Label13: TLabel;
    procedure FormCreate(Sender: TObject);
	 procedure BitBtn6Click(Sender: TObject);
	 procedure BtnExportClick(Sender: TObject);
	 procedure Tolt(sofor, datol, datig, sofnev: string );
	 procedure M5KeyPress(Sender: TObject; var Key: Char);
	 procedure M5Exit(Sender: TObject);
	 procedure FormDestroy(Sender: TObject);
	 procedure FormKeyDown(Sender: TObject; var Key: Word;
	   Shift: TShiftState);
    procedure Button1Click(Sender: TObject);
  private
		dokod		: string;
		dat1		: string;
		dat2		: string;
		sor01		: integer;
  public
		kelluzenet	    : boolean;
       csakgyujtes     : Boolean;
       fncsv           : string;
       kilepes         : boolean;
       vanftkm         : boolean;  // Mozg�b�r sz�m�t�sa Ft/km alapj�n
       jarbmeg         : boolean;  // J�rat megbont�sa megtakar�t�s szerint -> �tl�g�akra t�red�k b�r
  end;

var
  FizExportDlg: TFizExportDlg;

implementation

uses
	J_VALASZTO, FizLis2, Egyeb, Kozos, J_EXCEL, J_SQL, J_Fogyaszt, Math;
{$R *.DFM}

procedure TFizExportDlg.FormCreate(Sender: TObject);
begin
	EgyebDlg.SetADOQueryDatabase(Query1);
	EgyebDlg.SetADOQueryDatabase(Query2);
	EgyebDlg.SetADOQueryDatabase(Query3);
	EgyebDlg.SetADOQueryDatabase(Query4);
	EgyebDlg.SetADOQueryDatabase(QueryKozos);
	dokod	:= '';
   kilepes := false;
   vanftkm := false;
   jarbmeg := false;
	Application.CreateForm(TFogyasztasDlg, FogyasztasDlg);
	kelluzenet	:= true;
   // Mozg�b�r Ft/km adatok t�rol�sa a sz�t�rban
   Query_Run(QueryKozos, 'SELECT * FROM SZOTAR WHERE SZ_FOKOD = ''122'' ');
   if QueryKozos.RecordCount < 1 then begin
       // L�tre kell hozni az �j kateg�ri�t
       Query_Run(QueryKozos, 'INSERT INTO SZOTAR ( SZ_FOKOD, SZ_ALKOD, SZ_MENEV, SZ_LEIRO, SZ_ERVNY, SZ_KODHO, SZ_NEVHO ) VALUES ( ''000'', ''122'', ''Mozg�b�r Ft/km'', ''Mozg�b�r kisz�m�t�s�hoz a Ft/km �rt�k'',  1, 5, 120 ) ', FALSE);
       Query_Run(Query3, 'SELECT * FROM SZOTAR WHERE SZ_FOKOD = ''120'' ');
       while not Query3.Eof do begin
           Query_Insert( QueryKozos, 'SZOTAR',
               [
               'SZ_FOKOD', ''''+'122'+'''',
               'SZ_ALKOD', ''''+Query3.FieldByName('SZ_ALKOD').AsString+'''',
               'SZ_MENEV', ''''+Query3.FieldByName('SZ_ALKOD').AsString+'''',
               'SZ_LEIRO', ''''+'Mozg�b�r Ft/km a '+Query3.FieldByName('SZ_ALKOD').AsString+' kateg�ri�hoz'+'''',
               'SZ_ERVNY', '1'
               ]);
           Query3.Next;
       end;
   end;
end;

procedure TFizExportDlg.BitBtn6Click(Sender: TObject);
begin
   kilepes := true;
	Close;
end;

procedure TFizExportDlg.BtnExportClick(Sender: TObject);
var
  EX	  	: TJExcel;
  i        : integer;
  fn       : string;
  bevet1	: integer;
  bevet2	: integer;
  bevet3	: integer;
  sorsz	: integer;
  sqlstr	: string;
  str		: string;
  kezdokm	: integer;
  vegzokm	: integer;
  k1		: integer;
  k2		: integer;
  apehuzem	: double;
  osszmegt	: double;
  datum	: string;
  arfoly   : double;
  voltkod	: string;
  sor_gyujto: string;
  elszkm, okm: integer;
  osszeg   : double;
  lejartar : double;
  datADR   : string;
  oszADR   : double;
  S, GyujtottUzenetek: string;
begin
  if csakgyujtes then begin
    Button1.OnClick(self);
    exit;
  end;
  ///////////////////////
	BtnExport.Hide;
   fn	:= EgyebDlg.TempList + M0.Text + '_' + M1.Text;
   fn  := StringReplace(fn, '.', '',[rfReplaceAll, rfIgnoreCase]);
//   fn	:= EgyebDlg.TempList + 'TESZTXLS';
//  fncsv:=fn+ '.csv';
	{A lista �ssze�ll�t�sa a sz�r�si elemek alapj�n}
	Screen.Cursor := crHourglass;
	Application.CreateForm(TFizlisDlg, FizlisDlg);
	FizlisDlg.kelluzenet	:= false;
	FizlisDlg.Tolt(dokod, dat1, dat2, M0.Text, vanftkm, jarbmeg, EgyebDlg.fizlist_csehtran);
	Screen.Cursor := crDefault;
	sor01	:= 0;
  osszeg  := 0;
  GyujtottUzenetek:= '';
	if FizlisDlg.vanadat	then begin
		// A felt�lt�tt t�bl�zat kit�tele excel �llom�nyba
       EX := TJexcel.Create(Self);
	    fn := fn + '.'+EX.GetExtension;
		if FileExists(fn) then begin
			DeleteFile(PChar(fn));
		end;
       if not EX.OpenXlsFile(fn) then begin
           NoticeKi('Nem siker�lt megnyitnom az Excel �llom�nyt!');
       end else begin
           EX.ColNumber	:= 11;
           EX.InitRow;
           // Az oszlopsz�less�gek be�ll�t�sa - egyes�vel
           // for i := 1 to EX.ColNumber do begin
           //     EX.SetColumnWidth(i, 10);
           // end;

           EX.SetOrientation(xlLandscape);
           EX.SetMarginsCentimeters(0.8, 0.8, -1, -1); // fent �s lent nem �ll�tjuk

           EX.SetColumnWidth(1, 8);
           EX.SetColumnWidth(2, 10);
           EX.SetColumnWidth(3, 9);
           EX.SetColumnWidth(4, 8);
           EX.SetColumnWidth(5, 35);
           EX.SetColumnWidth(6, 8);
           EX.SetColumnWidth(7, 8);
           EX.SetColumnWidth(8, 7);
           EX.SetColumnWidth(9, 7);
           EX.SetColumnWidth(10, 13);
           EX.SetColumnWidth(11, 10);


           EX.SetRowcell(1, 'Fizet�si lista');
           EX.SaveRow(1);
           EX.SetRowcell(1, 'Sof�r :');
           EX.SetRowcell(2, M0.Text);         // csakgyujt�shez     N�v
           sor_gyujto:=M0.Text;
           Query3.Close;
           Query3.SQL.Clear;
           Query3.SQL.Add('select dc_csnev from dolgozocsop where dc_donev='''+M0.Text+''' order by dc_dattol desc ') ;
           Query3.Open;
           Query3.First;
           sor_gyujto:=sor_gyujto+';'+Query3.FieldByName('DC_CSNEV').AsString;  //   Query_Select('DOLGOZO','DO_NAME',M0.Text,'DO_GKKAT');
           Query3.Close;
           EX.SaveRow(3);
           EX.SetRowcell(1, 'Id�szak :');
           EX.SetRowcell(2, M1.Text);         // csakgyujt�shez      Id�szak
           EX.SetRowcell(3, '');
           EX.SetRowcell(5, '�a. d�j ( Ft/liter) :');
           apehuzem	:= GetApehUzemanyag(DatumHozNap(dat1, 14, false), 1);
           EX.SetRowcell(6, Format('%.2f', [apehuzem]) );
           EX.SaveRow(4);
           EX.Hor_AlignmentCell(4,5,Integer(xlHAlignRight)) ;
           EX.SetRangeFormat(4,6, 4, 6, '0,00');
           EX.SetRowcell(1, 'Sorsz�m');
           EX.SetRowcell(2, 'Kelt');
           EX.SetRowcell(3, 'J�ratsz�m');
           EX.SetRowcell(4, 'Rendsz�m');
           EX.SetRowcell(5, 'Viszonylat');
           EX.SetRowcell(6, 'Elsz.km');
           EX.SetRowcell(7, 'Ft/km');
           EX.SetRowcell(8, 'Felrak�');
           EX.SetRowcell(9, 'Extra d�j');
           EX.SetRowcell(10, 'Cseh tr.');
           EX.SetRowcell(11, '�sszeg');
           EX.SaveRow(6);
           elszkm  := 0;
           oszADR  := 0;
           Query_Run(Query4, 'SELECT DE_DATUM FROM DOLGOZOERV WHERE DE_ALKOD = ''104'' AND DE_DOKOD = '''+dokod+''' ');
           datADR  := Query4.FieldByName('DE_DATUM').AsString;
           for i := 0 to FizlisDlg.SG1.RowCount - 1 do begin
               EX.SetRowcell(1, FizlisDlg.SG1.Cells[0,i]);     // Sorsz�m
               EX.SetRowcell(2, FizlisDlg.SG1.Cells[1,i]);     // D�tum
               if Length(FizlisDlg.SG1.Cells[1,i]) < 10 then begin
                   EX.SetRowcell(2, ''''+ FizlisDlg.SG1.Cells[1,i]);
               end;
               EX.SetRowcell(3, FizlisDlg.SG1.Cells[2,i]);     // J�ratsz�m
               EX.SetRowcell(4, FizlisDlg.SG1.Cells[3,i]);     // Rendsz�m
               EX.SetRowcell(5, FizlisDlg.SG1.Cells[4,i]);     // Viszonylat
               EX.SetRowcell(6, FizlisDlg.SG1.Cells[5,i]);     // Elsz�molhat� km
               elszkm  :=  elszkm + StrToInt( FizlisDlg.SG1.Cells[5,i]);
               // ADR sz�mol�sa
               if datADR >=  FizlisDlg.SG1.Cells[1,i] then begin
                   // Az ADR igazolv�ny m�g �rv�nyes volt indul�skor
                   oszADR  := oszADR + StrToIntDef( FizlisDlg.SG1.Cells[5,i], 0);  // Minden km ut�n 1 FT j�r
               end;

               EX.SetRowcell(7, FizlisDlg.SG1.Cells[6,i]);     // Ft/km
               // EX.SetRowcell(8, FizlisDlg.SG1.Cells[9,i]);
               EX.SetRowcell(8, Format('%.0f', [StringSzam(FizlisDlg.SG1.Cells[9,i])])); // nem kell tizedes

               EX.SetRowcell(9, FizlisDlg.SG1.Cells[8,i]);
               EX.SetRowcell(10, Format('%.0f', [StringSzam(FizlisDlg.SG1.Cells[10,i])]));
               EX.SetRowcell(11, Format('%.0f', [StringSzam(FizlisDlg.SG1.Cells[7,i])]));
               osszeg  :=  osszeg+ StringSzam(FizlisDlg.SG1.Cells[7,i]) ;
               EX.SaveRow(7+i);
               EX.SetRangeFormat(7+i, 11, 7+i, 11, '0')  // nem kell fill�r
           end;
           i := FizlisDlg.SG1.RowCount;
           EX.SetRowcell(1, '�sszesen :');
           EX.SetRowcell(2, '');
           EX.SetRowcell(3, '');
           EX.SetRowcell(4, '');
           EX.SetRowcell(5, '');
           EX.SetRowcell(6, '=SUM(F7:F'+IntToStr(6+i)+')');    // csakgyujt�shez E futott km
  //  sor_gyujto:=sor_gyujto+';'+IntToStr(elszkm) ;
           EX.SetRowcell(7, '=J'+IntToStr(6+i+1)+'/F'+IntToStr(6+i+1));
           EX.SetRowcell(8, '');
           EX.SetRowcell(9, '');
           EX.SetRowcell(10, '=SUM(J7:J'+IntToStr(6+i)+')');
           EX.SetRowcell(11, '=SUM(K7:K'+IntToStr(6+i)+')');
           EX.SaveRow(7+i);
//	   		EX.SetRangeFormat(7,7, 6+i, 9, '0,00');
           EX.SetRangeFormat(7,7, 6+i, 10, '0');
           EX.SetRangeFormat(6+i+1,7, 6+i+1, 7, '0,00');
           sor01	:= 7+i;
           bevet1	:= i + 7;

           sorsz	:= i + 9;
           EX.EmptyRow;
           EX.SetRowcell(3, 'J�rat');
           EX.SetRowcell(4, 'Rendsz�m');
           EX.SetRowcell(5, 'Sof�r km');
           EX.SetRowcell(6, 'T�ny liter');
           EX.SetRowcell(7, 'Terv liter');
           EX.SetRowcell(8, 'Megtak. liter');
           EX.SetRowcell(9, 'S�ly�tlag');
           EX.SaveRow(sorsz);
           EX.Hor_AlignmentCell(sorsz,5,Integer(xlHAlignCenter));
           bevet3	:= i + 10;

           // A megtakar�t�sok sz�mol�sa
           sqlstr	:= 'SELECT JA_KOD, JA_RENDSZ, JA_NYITKM, JA_ZAROKM,  JA_ORSZ, JA_ALKOD, JS_SOFOR FROM JARSOFOR, JARAT , DOLGOZO '+
               ' WHERE JS_JAKOD = JA_KOD AND JS_SOFOR = DO_KOD '+
               ' AND ( ( JA_JKEZD >= '''+dat1+''' AND JA_JKEZD <= '''+dat2+''' ) OR '+
                     ' ( JA_JVEGE >= '''+dat1+''' AND JA_JVEGE <= '''+dat2+''' ) OR '+
                     ' ( JA_JKEZD <  '''+dat1+''' AND JA_JVEGE >  '''+dat2+''' ) )  ';
           sqlstr := sqlstr + ' AND JS_SOFOR = '+dokod;
           sqlstr := sqlstr + ' ORDER BY JA_JKEZD ';
           Query_Run(Query1, sqlstr );
           osszmegt	:= 0;
           Query1.DisableControls;
           voltkod		:= ',';
           okm         :=0;
           while not Query1.Eof do begin
               if Pos(','+Query1.FieldByName('JA_KOD').AsString+',', voltkod) > 0 then begin
                   Query1.Next;
                   Continue;
               end else begin
                   voltkod	:= voltkod + Query1.FieldByName('JA_KOD').AsString+',';
               end;

               // Ellen�rizz�k a g�pkocsihoz a z�r�si adatokat
               str	:= DatumHozNap(dat1, -1, true);
               Query_Run(Query2, 'SELECT ZK_KMORA FROM ZAROKM WHERE ZK_RENSZ = '''+Query1.FieldByName('JA_RENDSZ').AsString+''' AND ZK_DATUM = '''+str+''' ', true);
               if Query2.RecordCount < 1 then begin
                   if kelluzenet then begin
                       NoticeKi('Hi�nyzik a kezd� d�tumhoz tartoz� z�r� km! ( '+Query1.FieldByName('JA_RENDSZ').AsString+'->'+str+' )');
                   end else begin
                       HibaKiiro(M0.Text+': Hi�nyzik a kezd� d�tumhoz tartoz� z�r� km! ( '+Query1.FieldByName('JA_RENDSZ').AsString+'->'+str+' )');
                   end;
                   EX.SaveFileAsXls(fn);
                   EX.Free;
                   BtnExport.Show;
                   Exit;
               end;
               kezdokm	:= StrToIntDef(Query2.FieldByName('ZK_KMORA').AsString,0);
               Query_Run(Query2, 'SELECT ZK_KMORA FROM ZAROKM WHERE ZK_RENSZ = '''+Query1.FieldByName('JA_RENDSZ').AsString+'''  AND ZK_DATUM = '''+dat2+''' ', true);
               if Query2.RecordCount < 1 then begin
                   if kelluzenet then begin
                       NoticeKi('Hi�nyzik a befejez� d�tumhoz tartoz� z�r� km! ( '+Query1.FieldByName('JA_RENDSZ').AsString+'->'+dat2+' )');
                   end else begin
                       HibaKiiro(M0.Text+': Hi�nyzik a befejez� d�tumhoz tartoz� z�r� km! ( '+Query1.FieldByName('JA_RENDSZ').AsString+'->'+dat2+' )');
                   end;
                   EX.SaveFileAsXls(fn);
                   EX.Free;
                   BtnExport.Show;
                   Exit;
               end;
               vegzokm	:= StrToIntDef(Query2.FieldByName('ZK_KMORA').AsString,0);
               // Lehet futtatni a j�ratonk�nti lek�rdez�st
               FogyasztasDlg.ClearAllList;
               k1	:=  StrToIntDef(Query1.FieldByName('JA_NYITKM').AsString, 0);
               if k1 < kezdokm then begin
                   k1 := kezdokm;
               end;
               k2	:=  StrToIntDef(Query1.FieldByName('JA_ZAROKM').AsString, 0);
               if k2 > vegzokm then begin
                   k2 := vegzokm;
               end;
               FogyasztasDlg.GeneralBetoltes(Query1.FieldByName('JA_RENDSZ').AsString, k1, k2);
               S:= FogyasztasDlg.CreateTankLista;
               GyujtottUzenetek:= Felsorolashoz(GyujtottUzenetek, S, const_CRLF, False);
               S:= FogyasztasDlg.CreateAdBlueLista;
               GyujtottUzenetek:= Felsorolashoz(GyujtottUzenetek, S, const_CRLF, False);
               FogyasztasDlg.Szakaszolo;

               sorsz	:= sorsz + 1;
               EX.EmptyRow;
               EX.SetRowcell(3, Query1.FieldByname('JA_ORSZ').AsString + '-' + Format('%5.5d',[StrToIntDef(Query1.FieldByname('JA_ALKOD').AsString,0)]));
               EX.SetRowcell(4, Query1.FieldByName('JA_RENDSZ').AsString);

               // ha form�zva teszem string-k�nt a cell�ba, nem megy r� a pl. a SZUM f�ggv�ny
               {EX.SetRowcell(5, Format('%.0f', [FogyasztasDlg.GetSoforKm(Query1.FieldByName('JS_SOFOR').AsString)]));
               okm := okm+ Trunc( FogyasztasDlg.GetSoforKm(Query1.FieldByName('JS_SOFOR').Value));
               EX.SetRowcell(6, Format('%.2f', [FogyasztasDlg.GetSoforTeny(Query1.FieldByName('JS_SOFOR').AsString)]));
               EX.SetRowcell(7, Format('%.2f', [FogyasztasDlg.GetSoforTerv(Query1.FieldByName('JS_SOFOR').AsString)]));
               EX.SetRowcell(8, Format('%.2f', [FogyasztasDlg.GetSoforMegtakaritas(Query1.FieldByName('JS_SOFOR').AsString)]));
               EX.SetRowcell(9, Format('%.2f', [FogyasztasDlg.GetSoforSulyatlag(Query1.FieldByName('JS_SOFOR').AsString)]));
               }

               EX.SetRowcell(5, FogyasztasDlg.GetSoforKm(Query1.FieldByName('JS_SOFOR').AsString));
               okm := okm+ Trunc( FogyasztasDlg.GetSoforKm(Query1.FieldByName('JS_SOFOR').Value));
               EX.SetRowcell(6, FogyasztasDlg.GetSoforTeny(Query1.FieldByName('JS_SOFOR').AsString));
               EX.SetRowcell(7, FogyasztasDlg.GetSoforTerv(Query1.FieldByName('JS_SOFOR').AsString));
               EX.SetRowcell(8, FogyasztasDlg.GetSoforMegtakaritas(Query1.FieldByName('JS_SOFOR').AsString));
               EX.SetRowcell(9, FogyasztasDlg.GetSoforSulyatlag(Query1.FieldByName('JS_SOFOR').AsString));

               osszmegt	:= osszmegt + FogyasztasDlg.GetSoforMegtakaritas(Query1.FieldByName('JS_SOFOR').AsString);
               EX.SaveRow(sorsz);
               EX.Hor_AlignmentCell(sorsz,5,Integer(xlHAlignCenter));
               Query1.Next;
           end;
           Query1.EnableControls;
           sorsz		:= sorsz + 1;
           EX.EmptyRow;
           EX.SetRowcell(1, '�sszes megtakar�t�s');
           if bevet3 <= sorsz - 1 then begin  // ha van adatsor
               EX.SetRowcell(5, '=SUM(E'+IntToStr(bevet3)+':E'+IntToStr(sorsz-1)+')');
               EX.SetRowcell(6, '=SUM(F'+IntToStr(bevet3)+':F'+IntToStr(sorsz-1)+')');
               EX.SetRowcell(7, '=SUM(G'+IntToStr(bevet3)+':G'+IntToStr(sorsz-1)+')');
               // EX.SetRowcell(8, '=SUM(H'+IntToStr(bevet3)+':H'+IntToStr(sorsz-1)+')');
               // EX.SetRowcell(8,  Format('%.2f Ft', [osszmegt]));
               // 	EX.SetRowcell(10, Format('%.0f Ft', [osszmegt * apehuzem ]));
               EX.SetRowcell(8,  Format('%.2f', [osszmegt]));                      // csakgyujt�shez  C megtakar�t�s
               sor_gyujto:=sor_gyujto+';'+ Format('%.2f', [osszmegt]);//   FloatToStr( osszmegt) ;
  				EX.SetRowcell(11, Format('%.0f', [osszmegt * apehuzem ]));
               osszeg:=osszeg+ RoundTo(osszmegt * apehuzem , 0)  ;
           end else begin
               EX.SetRowcell(5, '0');
               EX.SetRowcell(6, '0');
               EX.SetRowcell(7, '0');
               EX.SetRowcell(8, '0');
               EX.SetRowcell(10, '0');
           end;
           EX.SetRowcell(9, '');
           EX.SaveRow(sorsz);
           EX.SetRangeFormat(bevet3,6, sorsz, 9, '0,00');
//				EX.SetRangeFormat(bevet3,6, sorsz, 9, '0');
           EX.Hor_AlignmentCell(sorsz,5,Integer(xlHAlignCenter));

           // A megtakar�t�si �tlag ki�r�sa
           Ex.EmptyRow;
           EX.SetRowcell(7, '100 km-re es� megtakar�t�s : ');
           EX.SetRowcell(8, '=H'+IntToStr(sorsz)+'*100/E'+IntToStr(sorsz));   // csakgyujt�shez G Megtakar�t�s/km
   //      sor_gyujto:=sor_gyujto+';'+ Format('%.2f', [osszmegt*100/okm]) ;  //FloatToStr(osszmegt*100/okm) ;  csakgyujt�shez G Megtakar�t�s/km
           EX.SaveRow(sorsz+1);
           EX.Hor_AlignmentCell(sorsz+1,7,Integer(xlHAlignRight));
           EX.Hor_AlignmentCell(sorsz+1,8,Integer(xlHAlignCenter));
           EX.SetRangeFormat(sorsz+1,8, sorsz+1, 8, '0,00');

           bevet2	:= sorsz;

           // Szabads�g d�ja
           sorsz	:= sorsz + 2;
           EX.EmptyRow;
           EX.SetRowcell(10, 'Szabads�g :');
           EX.SetRowcell(11, Format('%.0f', [StringSzam(M2.Text)]));
           osszeg:=osszeg+ (StringSzam(M2.Text))  ;
           EX.SaveRow(sorsz);
           EX.Hor_AlignmentCell(sorsz,10,Integer(xlHAlignRight));

           // Betegszabi d�ja
           sorsz	:= sorsz + 2;
           EX.EmptyRow;
           EX.SetRowcell(10, 'Betegszabads�g :');
           EX.SetRowcell(11, Format('%.0f', [StringSzam(M20.Text)]));
           osszeg:=osszeg+ (StringSzam(M20.Text))  ;
           EX.SaveRow(sorsz);
           EX.Hor_AlignmentCell(sorsz,10,Integer(xlHAlignRight));

           if EgyebDlg.v_formatum <> 3 then begin
               // J&S -es sz�mol�s - ADR d�ja
               sorsz	:= sorsz + 2;
               EX.EmptyRow;
               EX.SetRowcell(10, 'ADR :');
               EX.SetRowcell(11, Format('%.0f', [oszADR]));
               osszeg:=osszeg+ oszADR  ;
               EX.SaveRow(sorsz);
           end;

           bevet3	:= sorsz;
           EX.Hor_AlignmentCell(sorsz,10,Integer(xlHAlignRight));

           // Az els� �sszesen
           sorsz	:= sorsz + 2;
           EX.EmptyRow;
           EX.SetRowcell(5, 'Ft / km :');
           EX.SetRowcell(6, '=K'+IntToStr(sorsz)+'/F'+IntToStr(sor01));
           EX.SetRowcell(10, '�sszesen :');
           if EgyebDlg.v_formatum <> 3 then begin
               // J&S -es sz�mol�s - ADR d�ja
               EX.SetRowcell(11, '=K'+IntToStr(bevet1)+'+K'+IntToStr(bevet2)+'+K'+IntToStr(bevet3)+'+K'+IntToStr(bevet3-2)+'+K'+IntToStr(bevet3-4));    // csakgyujt�shez D �sszes fiz
           end else begin
               EX.SetRowcell(11, '=K'+IntToStr(bevet1)+'+K'+IntToStr(bevet2)+'+K'+IntToStr(bevet3)+'+K'+IntToStr(bevet3-2));    // csakgyujt�shez D �sszes fiz - ADR sor nincs benne
           end;
           sor_gyujto:=sor_gyujto+';'+ Format('%.2f', [osszeg]);  // FloatToStr(osszeg) ;
           sor_gyujto:=sor_gyujto+';'+IntToStr(elszkm) ;            // E
           sor_gyujto:=sor_gyujto+';'+ Format('%.2f', [RoundTo( osszmegt*100/okm,-2)]) ;  //FloatToStr(osszmegt*100/okm) ;  csakgyujt�shez G Megtakar�t�s/km
           EgyebDlg.WriteLogFile(fncsv,sor_gyujto);
           EX.SaveRow(sorsz);
           EX.Hor_AlignmentCell(sorsz,10,Integer(xlHAlignRight));
           EX.Hor_AlignmentCell(sorsz,5,Integer(xlHAlignRight));
           bevet3  := sorsz;

           // Az EUR �sszesen
           datum	:= copy(dat1,1,8)+'15.';
           arfoly	:= EgyebDlg.ArfolyamErtek( 'EUR', datum, true);
           sorsz	:= sorsz + 2;
           EX.EmptyRow;
           EX.SetRowcell(5, 'EUR �rfolyam ('+datum+') :');
           EX.SetRowcell(6, Format('%.2f', [arfoly]));
           EX.SetRowcell(10, '�sszesen (EUR) :');
           if arfoly > 0 then begin
               EX.SetRowcell(11, Format('%.2f', [osszeg / arfoly]));
           end else begin
               EX.SetRowcell(11, Format('%.2f', [arfoly]));
           end;
           EX.SaveRow(sorsz);
           EX.Hor_AlignmentCell(sorsz,5,Integer(xlHAlignRight));
           EX.Hor_AlignmentCell(sorsz,6,Integer(xlHAlignRight));
           EX.Hor_AlignmentCell(sorsz,10,Integer(xlHAlignRight));
           EX.Hor_AlignmentCell(sorsz,11,Integer(xlHAlignRight));

           // Utal�sok
           sorsz	:= sorsz + 2;
           EX.EmptyRow;
           EX.SetRowcell(10, 'Utal�s :');
           EX.SetRowcell(11, Format('%.0f', [-1 * StringSzam(M3.Text)]));
           EX.SaveRow(sorsz);
           EX.SetRangeFormat(sorsz, 11, sorsz, 11, '0 Ft');  // nem kell fill�r
           EX.Hor_AlignmentCell(sorsz,10,Integer(xlHAlignRight));

           // A m�sodik �sszesen
           sorsz	:= sorsz + 2;
           EX.EmptyRow;
           EX.SetRowcell(10, '�sszesen :');
           EX.SetRowcell(11, '=K'+IntToStr(sorsz-6)+'+K'+IntToStr(sorsz-2));
           EX.SaveRow(sorsz);
           EX.SetRangeFormat(sorsz, 11, sorsz, 11, '0 Ft');  // nem kell fill�r
           EX.Hor_AlignmentCell(sorsz,10,Integer(xlHAlignRight));


           // Telefon

           sorsz	:= sorsz + 2;
           EX.EmptyRow;
           EX.SetRowcell(10, 'Telefon :');
           EX.SetRowcell(11, Format('%.0f', [-1 * StringSzam(M4.Text)]));
           EX.SaveRow(sorsz);
           EX.SetRangeFormat(sorsz, 11, sorsz, 11, '0 Ft');  // nem kell fill�r
           EX.Hor_AlignmentCell(sorsz,10,Integer(xlHAlignRight));

           // Egy�b
           sorsz	:= sorsz + 2;
           EX.EmptyRow;
           EX.SetRowcell(10, M6.Text);
           // 	EX.SetRowcell(10, Format('%.0f Ft', [-1 * StringSzam(M5.Text)]));
           EX.SetRowcell(11, Format('%.0f', [-1 * StringSzam(M5.Text)]));
           EX.SaveRow(sorsz);
           EX.SetRangeFormat(sorsz, 11, sorsz, 11, '0 Ft');  // nem kell fill�r
           EX.Hor_AlignmentCell(sorsz,10,Integer(xlHAlignRight));

           // Lej�rt okm�nyok
           if EgyebDlg.fizlist_lejart_okmany then begin
               lejartar    := 0;
               Query_Run(Query4, 'SELECT * FROM DOLGOZOERV WHERE DE_DATUM < '''+EgyebDlg.MaiDatum+''' ' + ' AND DE_DOKOD = '''+dokod+''' ');
               if Query4.RecordCount > 0 then begin
                   lejartar := -2000;
               end;
               sorsz	    := sorsz + 2;
               EX.EmptyRow;
               EX.SetRowcell(10, 'Lej�rt okm�nyok :');
               EX.SetRowcell(11, Format('%.0f', [lejartar]));
               EX.SaveRow(sorsz);
               EX.SetRangeFormat(sorsz, 11, sorsz, 11, '0 Ft');  // nem kell fill�r
               EX.Hor_AlignmentCell(sorsz,10,Integer(xlHAlignRight));
           end;

           // A harmadik �sszesen
           sorsz	:= sorsz + 2;
           EX.EmptyRow;
           EX.SetRowcell(10, 'V�g�sszesen :');
           if EgyebDlg.fizlist_lejart_okmany then begin
               EX.SetRowcell(11, '=K'+IntToStr(sorsz-6)+'+K'+IntToStr(sorsz-4)+'+K'+IntToStr(sorsz-2)+'+K'+IntToStr(sorsz-8));
           end else begin  // mivel egy sorral kevesebb van, a szumma is m�s lesz
                EX.SetRowcell(11, '=K'+IntToStr(sorsz-6)+'+K'+IntToStr(sorsz-4)+'+K'+IntToStr(sorsz-2));
           end;
			EX.SaveRow(sorsz);
           EX.SetRangeFormat(sorsz, 11, sorsz, 11, '0 Ft');  // nem kell fill�r
			EX.Hor_AlignmentCell(sorsz,10,Integer(xlHAlignRight));

			// A p�nz form�tum felrak�sa
			// EX.SetRangeStyle(1, 11, sorsz - 12, 11, 'currency');
           EX.SetRangeFormat(1, 11, sorsz - 12, 11, '0 Ft');  // nem kell fill�r
			// EX.SetRangeStyle(sorsz - 8, 10, sorsz, 10, 'currency');

           // A keretez�s megcsin�l�sa
           EX.SetBorder(1,1, sorsz, 11);
           // A sz�nez�sek
           EX.SetCellColor(bevet1, 11, bevet1, 11, clYellow);
           EX.SetCellColor(bevet2, 11, bevet2, 11, clYellow);
           EX.SetCellColor(bevet3, 11, bevet3, 11, clYellow);
           EX.SetCellColor(bevet3+6, 11, bevet3+6, 11, clYellow);
           EX.SetCellColor(sorsz, 11, sorsz, 11, clAqua);
           // BOLD bet�k
           EX.SetRangeBold(3, 1, 3, 2 , true );
           EX.SetRangeBold(4, 1, 4, 2 , true );
           EX.SetRangeBold(bevet1, 1,  bevet1, 1 , true );
           EX.SetRangeBold(bevet1, 11, bevet1, 11 , true );
           EX.SetRangeBold(bevet2, 1,  bevet2, 1 , true );
           EX.SetRangeBold(bevet2, 11, bevet2, 11 , true );
           EX.SetRangeBold(bevet3, 10, bevet3, 10 , true );
           EX.SetRangeBold(bevet3, 11, bevet3, 11 , true );
           EX.SetRangeBold(bevet3+6, 10, bevet3+6, 10 , true );
           EX.SetRangeBold(bevet3+6, 11, bevet3+6, 11 , true );
           EX.SetRangeBold(sorsz, 10, sorsz, 10 , true );
           EX.SetRangeBold(sorsz, 11, sorsz, 11 , true );
           // Megjelen�t�s
           if not csakgyujtes then begin
               EX.SaveFileAsXls(fn);
  				EX.Free;
           end;
		end;
    if GyujtottUzenetek <> '' then begin
           NoticeKi(GyujtottUzenetek);
          end;
		// A kapott XLS megjelen�t�se
		if kelluzenet then begin
			ShellExecute(Application.Handle,'open',PChar(fn), '', '', SW_SHOWNORMAL );
		end;
	end else begin
		if kelluzenet then begin
			NoticeKi('Nincs a megadott felt�teleknek megfelel� t�tel!');
		end else begin
			HibaKiiro(M0.Text + ': Nincs a megadott felt�teleknek megfelel� t�tel!');
		end;
	end;
	FizlisDlg.Destroy;
	BtnExport.Show;
end;

procedure TFizExportDlg.Tolt(sofor, datol, datig, sofnev: string );
begin
	M0.Text	:= sofnev;
	M1.Text	:= datol + ' - ' + datig;
	dokod	:= sofor;
   dat1	:= datol;
   dat2	:= datig;
end;


procedure TFizExportDlg.M5KeyPress(Sender: TObject; var Key: Char);
begin
	with Sender as TMaskEdit do begin
  		Key := EgyebDlg.Jochar(Text,Key,MaxLength,Tag);
  	end;
end;

procedure TFizExportDlg.M5Exit(Sender: TObject);
begin
	with Sender as TMaskEdit do begin
  		Text := StrSzamStr(Text,MaxLength,Tag);
	end;
end;

procedure TFizExportDlg.FormDestroy(Sender: TObject);
begin
  FogyasztasDlg.Destroy;
end;

procedure TFizExportDlg.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
	EgyebDlg.FormReturn(Key);
end;

procedure TFizExportDlg.Button1Click(Sender: TObject);
var
  i        : integer;
  bevet3	: integer;
  sorsz	: integer;
  sqlstr	: string;
  str		: string;
  kezdokm	: integer;
  vegzokm	: integer;
  k1		: integer;
  k2		: integer;
  apehuzem	: double;
  osszmegt	: double;
  datum	: string;
  voltkod	: string;
  sor_gyujto: string;
  elszkm, okm: integer;
  osszeg: double;
  datADR   : string;
  oszADR   : double;
begin
	BtnExport.Hide;
	{A lista �ssze�ll�t�sa a sz�r�si elemek alapj�n}
	Screen.Cursor := crHourglass;
	Application.CreateForm(TFizlisDlg, FizlisDlg);
	FizlisDlg.kelluzenet	:= false;
	FizlisDlg.Tolt(dokod, dat1, dat2, M0.Text, vanftkm, jarbmeg, EgyebDlg.fizlist_csehtran);
	Screen.Cursor := crDefault;
   osszeg:=0;
	if FizlisDlg.vanadat	then begin
		// A felt�lt�tt t�bl�zat kit�tele excel �llom�nyba
       sor_gyujto:=M0.Text;
       Query3.Close;
       Query3.SQL.Clear;
       Query3.SQL.Add('select dc_csnev from dolgozocsop where dc_donev='''+M0.Text+''' order by dc_dattol desc ') ;
       Query3.Open;
       Query3.First;
       sor_gyujto:=sor_gyujto+';'+Query3.FieldByName('DC_CSNEV').AsString;  //   Query_Select('DOLGOZO','DO_NAME',M0.Text,'DO_GKKAT');
       Query3.Close;
		apehuzem	:= GetApehUzemanyag(DatumHozNap(dat1, 14, false), 1);
       elszkm:=0;
       oszADR  := 0;
       Query_Run(Query4, 'SELECT DE_DATUM FROM DOLGOZOERV WHERE DE_ALKOD = ''104'' AND DE_DOKOD = '''+dokod+''' ');
       datADR  := Query4.FieldByName('DE_DATUM').AsString;
		for i := 0 to FizlisDlg.SG1.RowCount - 1 do begin
           elszkm:=elszkm+ StrToIntDef( FizlisDlg.SG1.Cells[5,i], 0);
           osszeg:=osszeg+ StringSzam(FizlisDlg.SG1.Cells[7,i]) ;
           // ADR sz�mol�sa
           if datADR >=  FizlisDlg.SG1.Cells[1,i] then begin
               // Az ADR igazolv�ny m�g �rv�nyes volt indul�skor
               oszADR  := oszADR + StrToIntDef( FizlisDlg.SG1.Cells[5,i], 0);  // Minden km ut�n 1 FT j�r
           end;
       end;

		sorsz	:= FizlisDlg.SG1.RowCount + 9;
		bevet3	:= FizlisDlg.SG1.RowCount + 10;

		// A megtakar�t�sok sz�mol�sa
		sqlstr	:=  'SELECT JA_KOD, JA_RENDSZ, JA_NYITKM, JA_ZAROKM,  JA_ORSZ, JA_ALKOD, JS_SOFOR FROM JARSOFOR, JARAT, DOLGOZO '+
					' WHERE JS_JAKOD = JA_KOD AND JS_SOFOR = DO_KOD '+
					' AND ( ( JA_JKEZD >= '''+dat1+''' AND JA_JKEZD <= '''+dat2+''' ) OR '+
						  ' ( JA_JVEGE >= '''+dat1+''' AND JA_JVEGE <= '''+dat2+''' ) OR '+
						  ' ( JA_JKEZD <  '''+dat1+''' AND JA_JVEGE >  '''+dat2+''' ) ) ';

       sqlstr := sqlstr + ' AND JS_SOFOR = '+dokod;
		sqlstr := sqlstr + ' ORDER BY DO_NAME ';
		Query_Run(Query1, sqlstr );
		osszmegt	:= 0;
		Query1.DisableControls;
		voltkod		:= ',';
       okm         := 0;
		while not Query1.Eof do begin
           if Pos(','+Query1.FieldByName('JA_KOD').AsString+',', voltkod) > 0 then begin
				Query1.Next;
				Continue;
			end else begin
				voltkod	:= voltkod + Query1.FieldByName('JA_KOD').AsString+',';
			end;
			// Ellen�rizz�k a g�pkocsihoz a z�r�si adatokat
           str	:= DatumHozNap(dat1, -1, true);
           Query_Run(Query2, 'SELECT ZK_KMORA FROM ZAROKM WHERE ZK_RENSZ = '''+Query1.FieldByName('JA_RENDSZ').AsString+''' AND ZK_DATUM = '''+str+''' ', true);
           if Query2.RecordCount < 1 then begin
               if kelluzenet then begin
                   NoticeKi('Hi�nyzik a kezd� d�tumhoz tartoz� z�r� km! ( '+Query1.FieldByName('JA_RENDSZ').AsString+'->'+str+' )');
               end else begin
                   HibaKiiro(M0.Text+': Hi�nyzik a kezd� d�tumhoz tartoz� z�r� km! ( '+Query1.FieldByName('JA_RENDSZ').AsString+'->'+str+' )');
               end;
               BtnExport.Show;
               Exit;
           end;
           kezdokm	:= StrToIntDef(Query2.FieldByName('ZK_KMORA').AsString,0);
           Query_Run(Query2, 'SELECT ZK_KMORA FROM ZAROKM WHERE ZK_RENSZ = '''+Query1.FieldByName('JA_RENDSZ').AsString+'''  AND ZK_DATUM = '''+dat2+''' ', true);
           if Query2.RecordCount < 1 then begin
               if kelluzenet then begin
                   NoticeKi('Hi�nyzik a befejez� d�tumhoz tartoz� z�r� km! ( '+Query1.FieldByName('JA_RENDSZ').AsString+'->'+dat2+' )');
               end else begin
                   HibaKiiro(M0.Text+': Hi�nyzik a befejez� d�tumhoz tartoz� z�r� km! ( '+Query1.FieldByName('JA_RENDSZ').AsString+'->'+dat2+' )');
               end;
               BtnExport.Show;
               Exit;
           end;
           vegzokm	:= StrToIntDef(Query2.FieldByName('ZK_KMORA').AsString,0);
           // Lehet futtatni a j�ratonk�nti lek�rdez�st
           FogyasztasDlg.ClearAllList;
           k1	:=  StrToIntDef(Query1.FieldByName('JA_NYITKM').AsString, 0);
           if k1 < kezdokm then begin
               k1 := kezdokm;
           end;
           k2	:=  StrToIntDef(Query1.FieldByName('JA_ZAROKM').AsString, 0);
           if k2 > vegzokm then begin
               k2 := vegzokm;
           end;
           FogyasztasDlg.GeneralBetoltes(Query1.FieldByName('JA_RENDSZ').AsString, k1, k2);
           FogyasztasDlg.CreateTankLista;
           FogyasztasDlg.CreateAdBlueLista;
           FogyasztasDlg.Szakaszolo;

           sorsz	    := sorsz + 1;
           okm         := okm + Trunc( FogyasztasDlg.GetSoforKm(Query1.FieldByName('JS_SOFOR').Value));
           osszmegt	:= osszmegt + FogyasztasDlg.GetSoforMegtakaritas(Query1.FieldByName('JS_SOFOR').AsString);

           Query1.Next;
       end;
       Query1.EnableControls;

       // Ide j�nnek a megtakar�t�sok
       sorsz		:= sorsz + 1;
       if bevet3 < sorsz - 1 then begin
           sor_gyujto:=sor_gyujto+';'+ Format('%.2f', [osszmegt]);//   FloatToStr( osszmegt) ;
           osszeg:=osszeg+ RoundTo(osszmegt * apehuzem , 0)  ;
       end;

       // Szabads�g d�ja
       osszeg  := osszeg+ (StringSzam(M2.Text))  ;

       // Bertegszabads�g d�ja
       osszeg  := osszeg+ (StringSzam(M20.Text))  ;

       // ADR d�ja
       osszeg  := osszeg+ oszADR  ;

       // Az els� �sszesen
       sor_gyujto:=sor_gyujto+';'+ Format('%.2f', [osszeg]);  // FloatToStr(osszeg) ;
       sor_gyujto:=sor_gyujto+';'+IntToStr(elszkm) ;            // E
       if okm<>0 then begin
           sor_gyujto:=sor_gyujto+';'+ Format('%.2f', [RoundTo( osszmegt*100/okm,-2)])
           //FloatToStr(osszmegt*100/okm) ;  csakgyujt�shez G Megtakar�t�s/km
       end else begin
           sor_gyujto:=sor_gyujto+';'+'0.00';
       end;
       EgyebDlg.WriteLogFile(fncsv,sor_gyujto);
		// Az EUR �sszesen
		datum	:= copy(dat1,1,8)+'15.';
	end else begin
		if kelluzenet then begin
			NoticeKi('Nincs a megadott felt�teleknek megfelel� t�tel!');
		end else begin
			HibaKiiro(M0.Text + ': Nincs a megadott felt�teleknek megfelel� t�tel!');
		end;
	end;
	FizlisDlg.Destroy;
	BtnExport.Show;
end;

end.
