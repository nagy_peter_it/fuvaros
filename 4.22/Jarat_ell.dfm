object FJarat_ell: TFJarat_ell
  Left = 677
  Top = 246
  BorderIcons = [biSystemMenu]
  Caption = 'J'#225'ratok ellen'#337'rz'#233'se'
  ClientHeight = 566
  ClientWidth = 384
  Color = clBtnFace
  Constraints.MaxWidth = 400
  Constraints.MinWidth = 400
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 384
    Height = 185
    Align = alTop
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    object Label1: TLabel
      Left = 9
      Top = 96
      Width = 84
      Height = 16
      Caption = 'J'#225'rat kezdete:'
    end
    object Label2: TLabel
      Left = 230
      Top = 96
      Width = 10
      Height = 16
      Caption = ' - '
    end
    object Label3: TLabel
      Left = 16
      Top = 128
      Width = 21
      Height = 16
      Caption = '000'
    end
    object Button1: TButton
      Left = 296
      Top = 152
      Width = 75
      Height = 25
      Caption = 'Kil'#233'p'#233's'
      TabOrder = 0
      OnClick = Button1Click
    end
    object Button2: TButton
      Left = 16
      Top = 152
      Width = 97
      Height = 25
      Caption = 'Ellen'#337'rz'#233's'
      TabOrder = 1
      OnClick = Button2Click
    end
    object MD1: TMaskEdit
      Left = 97
      Top = 92
      Width = 105
      Height = 24
      TabOrder = 2
      Text = ''
    end
    object MD2: TMaskEdit
      Left = 248
      Top = 92
      Width = 105
      Height = 24
      TabOrder = 3
      Text = ''
    end
    object BitBtn2: TBitBtn
      Left = 201
      Top = 92
      Width = 24
      Height = 24
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000120B0000120B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        33333FFFFFFFFFFFFFFF000000000000000077777777777777770FF7FF7FF7FF
        7FF07FF7FF7FF7F37F3709F79F79F7FF7FF077F77F77F7FF7FF7077777777777
        777077777777777777770FF7FF7FF7FF7FF07FF7FF7FF7FF7FF709F79F79F79F
        79F077F77F77F77F77F7077777777777777077777777777777770FF7FF7FF7FF
        7FF07FF7FF7FF7FF7FF709F79F79F79F79F077F77F77F77F77F7077777777777
        777077777777777777770FFFFF7FF7FF7FF07F33337FF7FF7FF70FFFFF79F79F
        79F07FFFFF77F77F77F700000000000000007777777777777777CCCCCC8888CC
        CCCC777777FFFF777777CCCCCCCCCCCCCCCC7777777777777777}
      NumGlyphs = 2
      TabOrder = 4
      TabStop = False
      OnClick = BitBtn2Click
    end
    object BitBtn3: TBitBtn
      Left = 353
      Top = 92
      Width = 24
      Height = 24
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000120B0000120B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        33333FFFFFFFFFFFFFFF000000000000000077777777777777770FF7FF7FF7FF
        7FF07FF7FF7FF7F37F3709F79F79F7FF7FF077F77F77F7FF7FF7077777777777
        777077777777777777770FF7FF7FF7FF7FF07FF7FF7FF7FF7FF709F79F79F79F
        79F077F77F77F77F77F7077777777777777077777777777777770FF7FF7FF7FF
        7FF07FF7FF7FF7FF7FF709F79F79F79F79F077F77F77F77F77F7077777777777
        777077777777777777770FFFFF7FF7FF7FF07F33337FF7FF7FF70FFFFF79F79F
        79F07FFFFF77F77F77F700000000000000007777777777777777CCCCCC8888CC
        CCCC777777FFFF777777CCCCCCCCCCCCCCCC7777777777777777}
      NumGlyphs = 2
      TabOrder = 5
      TabStop = False
      OnClick = BitBtn3Click
    end
    object Button3: TButton
      Left = 142
      Top = 152
      Width = 121
      Height = 25
      Caption = 'J'#225'rat adatlap'
      TabOrder = 6
      OnClick = Button3Click
    end
    object CheckBox1: TCheckBox
      Left = 16
      Top = 32
      Width = 97
      Height = 17
      Caption = 'Norm'#225'l j'#225'rat'
      Checked = True
      State = cbChecked
      TabOrder = 7
      OnClick = CheckBox2Click
    end
    object CheckBox2: TCheckBox
      Left = 16
      Top = 56
      Width = 137
      Height = 17
      Caption = 'NEM norm'#225'l j'#225'rat'
      Checked = True
      State = cbChecked
      TabOrder = 8
      OnClick = CheckBox2Click
    end
    object CheckBox3: TCheckBox
      Left = 152
      Top = 32
      Width = 153
      Height = 17
      Caption = 'Csak sz'#225'mlasz'#225'm ell.'
      Enabled = False
      TabOrder = 9
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 185
    Width = 384
    Height = 381
    Align = alClient
    TabOrder = 1
    object ListBox1: TListBox
      Left = 1
      Top = 1
      Width = 382
      Height = 379
      Align = alClient
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      OnDblClick = ListBox1DblClick
    end
  end
  object jarat: TADOQuery
    Connection = EgyebDlg.ADOConnectionAlap
    Parameters = <
      item
        Name = 'KEZD'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 11
        Value = Null
      end
      item
        Name = 'VEGE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 11
        Value = Null
      end>
    SQL.Strings = (
      'select ja_kod, ja_jkezd , ja_fajko'
      'from jarat'
      'where  ja_jkezd >= :KEZD and ja_jkezd <= :VEGE'
      'order by ja_jkezd ')
    Left = 328
    Top = 8
    object jaratja_kod: TStringField
      DisplayWidth = 6
      FieldName = 'ja_kod'
      Size = 6
    end
    object jaratja_fajko: TIntegerField
      FieldName = 'ja_fajko'
    end
    object jaratja_jkezd: TStringField
      FieldName = 'ja_jkezd'
      Size = 11
    end
  end
  object viszony: TADOQuery
    Connection = EgyebDlg.ADOConnectionAlap
    Parameters = <
      item
        Name = 'KOD'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 5
        Value = Null
      end>
    SQL.Strings = (
      'select vi_jakod,vi_ujkod,vi_sakod from viszony'
      'where vi_jakod= :KOD'
      '')
    Left = 328
    Top = 40
    object viszonyvi_jakod: TStringField
      DisplayWidth = 6
      FieldName = 'vi_jakod'
      Size = 6
    end
    object viszonyvi_ujkod: TIntegerField
      FieldName = 'vi_ujkod'
    end
    object viszonyvi_sakod: TStringField
      FieldName = 'vi_sakod'
      Size = 11
    end
  end
  object ADOQuery2: TADOQuery
    Connection = EgyebDlg.ADOConnectionAlap
    Parameters = <>
    Left = 328
    Top = 64
  end
end
