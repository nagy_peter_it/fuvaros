unit SzamlaLevel;

interface

uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, DBTables, DB, StdCtrls, Buttons, Mask, DBCtrls,
  ExtCtrls, ADODB, FileCtrl, StrUtils, Grids, J_DataModule, Vcl.ImgList;

type
  TComboImages = (Semmi, IgenNemUgyved, Figyelem);
  TSzamlaLevelDlg = class(TForm)
    Panel2: TPanel;
    SG1: TStringGrid;
    Query1: TADOQuery;
    GridPanel1: TGridPanel;
    BtnExport: TBitBtn;
    BitBtn6: TBitBtn;
    IgenNemUgyvedList: TImageList;
    FigyelemList: TImageList;
    chkHideSuccess: TCheckBox;
    InfoPanel: TPanel;
    Button1: TButton;
    Button2: TButton;
	 procedure FormCreate(Sender: TObject);
   procedure AddGridCombo(iColNumber: integer; iRowNumber: Integer; ListItems: array of string; ComboImages: TComboImages);
   procedure AlignGridCombos(row: integer);
   procedure AlignGridCombo(iColNumber, iRowNumber:integer);
   procedure SetGridCombo(iColNumber, iRowNumber:integer; iIndex: integer);
   function GetGridCombo(iColNumber, iRowNumber:integer): integer;
   function GetMailPart(const SzotarFokod: string; const Part: string; const Valtozok: array of string): string;
   function Behelyettesit(const S: string; const Valtozok: array of string): string;

   // procedure AddGridCheckBox(iColNumber: integer; iRowNumber: Integer);
   // procedure SetGridCheckBoxAlignment(iColNumber, iRowNumber:integer; bChecked :Boolean);
	 procedure BitBtn6Click(Sender: TObject);
	 procedure BtnExportClick(Sender: TObject);
	 procedure Feldolgozas;
   procedure FillGrid;
   //  procedure SG1DrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure SG1TopLeftChanged(Sender: TObject);
    procedure IgenNemDraw(Control: TWinControl; Index: Integer;
      Rect: TRect; State: TOwnerDrawState);
    procedure FigyelemDraw(Control: TWinControl; Index: Integer;
      Rect: TRect; State: TOwnerDrawState);
    procedure chkHideSuccessClick(Sender: TObject);
    procedure SG1FixedCellClick(Sender: TObject; ACol, ARow: Integer);
    procedure SG1MouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
    procedure SG1SelectCell(Sender: TObject; ACol, ARow: Integer;
      var CanSelect: Boolean);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);


  private
    FInMouseClick: boolean;
		kilepes		: boolean;
		JSzamla    	: TJSzamla;
    ElsoLevelNap, HanyNaponta, LevelDarabszam: integer;
    HatarNap: string;
    HintLatestRow, HintLatestCol : integer;
  public
  end;

const
  // Ujsor = chr(13)+chr(10);
  Ujsor = '<BR>';
  NyelvArray: array [0..2] of string = ('HUN','ENG','GER');
  FajtaArray: array [0..1] of string = ('Eml�keztet�','Felsz�l�t�');
  micsiSemmi = 0;
  micsiKuld = 1;
  micsiUgyvednek = 2;
  statusNyitott = 0;
  statusLezart = 1;

var
  SzamlaLevelDlg: TSzamlaLevelDlg;

implementation

uses
	Egyeb, Kozos, KozosTipusok, J_SQL, J_Excel, HianyVevobe, Windows, SzamlaNezo;
{$R *.DFM}

procedure TSzamlaLevelDlg.FormCreate(Sender: TObject);
var
  S: string;
  Cur: TCursor;
begin
	ElsoLevelNap:= IntToStr_default(EgyebDlg.Read_SZGrid( '129', '001'), 0);
  HanyNaponta:= IntToStr_default(EgyebDlg.Read_SZGrid( '129', '002'), 30);
  LevelDarabszam:= IntToStr_default(EgyebDlg.Read_SZGrid( '129', '003'), 3);
  HatarNap:=DatumHozNap(EgyebDlg.MaiDatum, -1*HanyNaponta, True);
  EgyebDlg.SeTADOQueryDatabase(Query1);
  chkHideSuccess.Checked:= False;
  Cur:= Screen.Cursor;
  Screen.Cursor:=crHourGlass;
  FillGrid;
  Screen.Cursor:=Cur;
end;

procedure TSzamlaLevelDlg.FillGrid;
var
  S, Info, LegutobbiDatum: string;
  i, KuldottLevelDb: integer;
  Kuldheto: boolean;
begin
  S:='select sa_vevokod, sa_vevonev, VE_EMAIL, VE_UNYELV, sa_kod, sa_valoss+sa_valafa brutto, sa_valnem, ';
  S:=S+' sa_kidat kibocsatas, sa_esdat esedekesseg, sa_tedat, ';
  S:=S+' (select COUNT(SL_SAKOD) from SZAMLALEVEL where SL_SAKOD=sa_kod) KuldottLevelDb, ';
  S:=S+' (select MAX(SL_KULDVE) from SZAMLALEVEL where SL_SAKOD=sa_kod) LegutobbiDatum ';
  S:=S+' from lejartszamla, VEVO';
  S:=S+' where V_KOD=sa_vevokod and sa_lnap>='+IntToStr(ElsoLevelNap);
  S:=S+' and not exists(select SL_SAKOD from SZAMLALEVEL where SL_SAKOD=sa_kod and ((SL_KULDVE>= '''+HatarNap+''') or (SL_STATUS='+IntToStr(statusLezart)+')))';
  // ------ tesztel�shez!!!!!!! --------
  // S:=S+' and sa_kidat>=''2017.04.15.'' ';
  // S:=S+' and sa_kod in (select SA_KOD from SZFEJ2) ';
  // ---------------------------
  S:=S+' order by 2';
  Query_Run(Query1, S);
  if not Query1.Eof then begin
    SG1.DefaultRowHeight:=26;  // A combo-k miatt a 24 nem el�g
    SG1.RowCount:=Query1.RecordCount+1;
    InfoPanel.Caption:=IntToStr(Query1.RecordCount)+' sz�mla (be�ll�t�sok: els� lev�l lej�rat ut�n '+IntToStr(ElsoLevelNap)+' nappal, '+
          'k�ld�s '+IntToStr(HanyNaponta)+' naponta, maximum '+IntToStr(LevelDarabszam)+' lev�l)';
    // SG1.RowCount:=10;  // debug
    // SG1.RowCount:=7;
    SG1.FixedRows:=1;
    SG1.ColWidths[0]:=40;
    SG1.ColWidths[1]:=200;
    SG1.ColWidths[2]:=250;
    SG1.ColWidths[3]:=80;
    SG1.ColWidths[4]:=70;
    SG1.ColWidths[5]:=50;
    SG1.ColWidths[6]:=75;
    SG1.ColWidths[7]:=120;
    SG1.ColWidths[8]:=60;
    SG1.ColWidths[9]:=110;
    SG1.ColWidths[10]:=40;
    SG1.ColWidths[11]:=270;
    SG1.ColWidths[12]:=0; // rejtett oszlop adatt�rol�shoz
    SG1.ColWidths[13]:=0; // rejtett oszlop adatt�rol�shoz

    SG1.Cells[0,0]:='V.k�d';
    SG1.Cells[1,0]:='Vev�';
    SG1.Cells[2,0]:='Email';
    SG1.Cells[3,0]:='Sz�mla k�d';
    SG1.Cells[4,0]:='Br.�sszeg';
    SG1.Cells[5,0]:='Deviza';
    SG1.Cells[6,0]:='Fiz.hat�rid�';
    SG1.Cells[7,0]:='Lev�l fajta';
    SG1.Cells[8,0]:='Nyelv';
    SG1.Cells[9,0]:='K�ldhet�';
    SG1.Cells[10,0]:='Siker';
    SG1.Cells[11,0]:='Info';
    // SG1.Cells[12,0]:=''; // �gysem l�tszik
    SG1.Cells[13,0]:='TEDAT'; // �gysem l�tszik
    i:=1;
    try
    while not Query1.Eof {and (i<10)} do
      with Query1 do begin
        SG1.Cells[0,i]:= FieldByName('sa_vevokod').AsString;
        SG1.Cells[1,i]:= FieldByName('sa_vevonev').AsString;
        SG1.Cells[2,i]:= FieldByName('VE_EMAIL').AsString;
        SG1.Cells[3,i]:= FieldByName('sa_kod').AsString;
        SG1.Cells[4,i]:= FormatFloat('# ##0.00', FieldByName('brutto').AsFloat);
        SG1.Cells[5,i]:= FieldByName('sa_valnem').AsString;
        SG1.Cells[6,i]:= FieldByName('esedekesseg').AsString;
        AddGridCombo(7, i, FajtaArray, Figyelem); // ilyen sorrendben vannak az ImageList-ben is
        SetGridCombo(7, i, 0);  // eml�keztet�
        AddGridCombo(8, i, NyelvArray, Semmi);
        SetGridCombo(8, i, GetElementIndex(NyelvArray, FieldByName('VE_UNYELV').AsString));
        // AddGridCheckBox(7,i); // A checkbox-szal nem tudtam z�ld �gra verg�dni
        AddGridCombo(9, i, ['Nem', 'Igen', '�gyv�dnek'], IgenNemUgyved); // ilyen sorrendben vannak az ImageList-ben is
        KuldottLevelDb:= FieldByName('KuldottLevelDb').AsInteger;
        LegutobbiDatum:= FieldByName('LegutobbiDatum').AsString;
        Kuldheto:=True;
        if Pos('@', FieldByName('VE_EMAIL').AsString)=0 then Kuldheto:=False; // van-e �rv�nyes email c�mnek l�tsz� sz�veg
        if  KuldottLevelDb>= LevelDarabszam then begin
              Kuldheto:=False;
              end;
        if Kuldheto then SetGridCombo(9, i, 1)  // igen
        else SetGridCombo(9, i, 0);  // nem
        if KuldottLevelDb>0 then
          Info:='Elk�ldve '+IntToStr(KuldottLevelDb)+' lev�l, legut�bbi: '+LegutobbiDatum
        else Info:='';
        SG1.Cells[11,i]:= Info;
        // SG1.Cells[12,i]:= FieldByName('sa_ujkod').AsString;
        SG1.Cells[12,i]:= ''; // nem haszn�ljuk
        SG1.Cells[13,i]:= FieldByName('sa_tedat').AsString;
        Inc(i);
        Next;
        end;  // with + while
    except
      on E: Exception do begin
          NoticeKi(E.Message+' in line '+IntToStr(i));
          end;
      end;  // try-except
   end;  // if
end;

procedure TSzamlaLevelDlg.AddGridCombo(iColNumber: integer; iRowNumber: Integer; ListItems: array of string; ComboImages: TComboImages);
var
  NewCombo :TComboBox;
  i: integer;
begin
  NewCombo := TComboBox.Create(SG1);
  NewCombo.Parent := SG1; // Place string grid on one panel
  NewCombo.Style:= csDropDownList;  // text not editable
  NewCombo.Visible := False;
  NewCombo.Color :=  clWhite;
  i	:= 0;
 	while i <= High(ListItems) do begin
    NewCombo.Items.Add(ListItems[i]);
    Inc(i);
   	end;  // while
  NewCombo.Tag:= 100 * iRowNumber + iColNumber;  // assuming less than 100 columns
  if ComboImages = IgenNemUgyved then begin
     NewCombo.Style:= csOwnerDrawFixed;
     NewCombo.OnDrawItem := IgenNemDraw;
     end;
  if ComboImages = Figyelem then begin
     NewCombo.Style:= csOwnerDrawFixed;
     NewCombo.OnDrawItem := FigyelemDraw;
     end;
  SG1.Objects[iColNumber, iRowNumber] := NewCombo;
  AlignGridCombo(iColNumber, iRowNumber); // Calling align function
end;

procedure TSzamlaLevelDlg.AlignGridCombos(row: integer);
begin
   AlignGridCombo(7, row);
   AlignGridCombo(8, row);
   AlignGridCombo(9, row)
end;

procedure TSzamlaLevelDlg.AlignGridCombo(iColNumber, iRowNumber: integer);
var
  ActCombo :TComboBox;
  Rect:TRect;
  LeftGap: integer;
begin
  ActCombo := (SG1.Objects[iColNumber, iRowNumber] as TComboBox);
  if ActCombo <> nil then begin
    Rect := SG1.CellRect(iColNumber,iRowNumber);
    if Rect.Height>0 then begin   // within the canvas at all
      LeftGap := 2;  // gap from cell's left border
      ActCombo.Left  := SG1.Left + Rect.Left + LeftGap;
      ActCombo.Top   := SG1.Top + Rect.Top+2;
      ActCombo.Width := Rect.Right-Rect.Left - LeftGap;  // to fill cell
      ActCombo.Height := Rect.Bottom-Rect.Top;
      ActCombo.Visible := True;
      end
    else begin
      ActCombo.Visible := False;
      end;
    // ActCombo.ItemIndex:=iIndex;
    end;  // if
end;

procedure TSzamlaLevelDlg.SetGridCombo(iColNumber, iRowNumber:integer; iIndex: integer);
var
  NewCombo :TComboBox;
begin
  NewCombo := (SG1.Objects[iColNumber, iRowNumber] as TComboBox);
  if NewCombo <> nil then begin
    NewCombo.ItemIndex:=iIndex;
    end;  // if
end;

function TSzamlaLevelDlg.GetGridCombo(iColNumber, iRowNumber:integer): integer;
var
  NewCombo :TComboBox;
begin
  NewCombo := (SG1.Objects[iColNumber, iRowNumber] as TComboBox);
  if NewCombo <> nil then begin
    Result:= NewCombo.ItemIndex;
    end;  // if
end;

{
procedure TSzamlaLevelDlg.SG1DrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
begin
    AlignGridCombo(ACol, ARow);
end;
}

procedure TSzamlaLevelDlg.SG1FixedCellClick(Sender: TObject; ACol,
  ARow: Integer);
begin
  SG1TopLeftChanged(Sender); // Col resize?
end;

procedure TSzamlaLevelDlg.SG1MouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
var
   P : TPoint;
   gc: TGridCoord;
begin
   gc := TStringGrid(Sender).MouseCoord(x, y);
   P.X := X;
   P.Y := Y;
   if  (gc.x <> HintLatestCol) or (gc.y <> HintLatestRow) then begin
      HintLatestCol := gc.x;
      HintLatestRow := gc.y;
      // ha van hozz� �rv�nyes adat cella
      if (HintLatestCol>=1) and (HintLatestCol<=SG1.ColCount-1) and (HintLatestRow>=0) and (HintLatestRow<=SG1.RowCount-1) then begin
          SG1.Hint := SG1.Cells[HintLatestCol, HintLatestRow];  // cell content
          // SG1.ShowHint:=True;
          Application.HintColor := clInfobk;
          Application.ActivateHint(P);
          end;  // if
      end;  // if
end;

procedure TSzamlaLevelDlg.SG1SelectCell(Sender: TObject; ACol, ARow: Integer;
  var CanSelect: Boolean);
begin
  {if ACol in[2] then begin  // a szerkeszthet� oszlopok
    if goEditing in SG1.Options then
      SG1.Options := SG1.Options+[goEditing];
    end
  else begin
    if not (goEditing in SG1.Options) then
      SG1.Options := SG1.Options-[goEditing];
    end;
    }
  if ACol in[2,4] then   // a szerkeszthet� oszlopok
      CanSelect:= True
  else CanSelect:= False;
end;

procedure TSzamlaLevelDlg.SG1TopLeftChanged(Sender: TObject);
var
  row: integer;
begin
  for row := 0 to SG1.RowCount - 1 do begin    // itt kezel�nk mindenf�le g�rget�st
     AlignGridCombos(row);
     end;  // for
end;


procedure TSzamlaLevelDlg.chkHideSuccessClick(Sender: TObject);
const
   SuccessCol = 10;
var
  i: integer;
begin
  for i:=0 to SG1.RowCount-1 do begin
    if chkHideSuccess.Checked then begin
      if SG1.Cells[SuccessCol,i] = 'OK' then
          SG1.RowHeights[i]:=0;   // "elrejtj�k" a sort
          // ez nem igaz�n j� �gy ... SG1.RowHeights[i]:=-2;   // "elrejtj�k" a sort, a sz�rke cs�kot sem akarom!
      end
    else begin
      SG1.RowHeights[i]:=SG1.DefaultRowHeight;
      end
    end;  // for
end;

procedure TSzamlaLevelDlg.BitBtn6Click(Sender: TObject);
begin
	if not kilepes then begin
		kilepes	:= true;
	  end
  else begin
		Close;
	  end;
end;

procedure TSzamlaLevelDlg.BtnExportClick(Sender: TObject);
var
  fnev,elotag: string;
  Cur: TCursor;
begin
	BtnExport.Visible	:= false;
  if NoticeKi('A k�ldhet� leveleket most elk�ldj�k. Folytatja?', NOT_QUESTION) <> 0 then begin
       Exit;  // ha nem "igen" a v�lasz
       end;
  Cur:= Screen.Cursor;
  Screen.Cursor:=crHourGlass;
	Feldolgozas;
  Screen.Cursor:=Cur;
  NoticeKi('A leveleket elk�ldt�k, megtekinthet�k az Outlook "Elk�ld�tt �zenetek" mapp�j�ban.');
	BtnExport.Visible	:= true;
end;

procedure TSzamlaLevelDlg.Button1Click(Sender: TObject);
const
   MicsiCol = 9;
var
  i: integer;
begin
  if NoticeKi('Biztosan minden sorban �t szeretn� �ll�tani a "K�ldhet�" mez� �rt�k�t?', NOT_QUESTION) <> 0 then
     exit ;
  for i:=0 to SG1.RowCount-1 do begin
    SetGridCombo(MicsiCol, i, micsiKuld); // a felhaszn�l� �ltal v�lasztott Nem, Igen vagy �gyv�dnek
    end;  // for
end;

procedure TSzamlaLevelDlg.Button2Click(Sender: TObject);
const
   MicsiCol = 9;
var
  i: integer;
begin
  if NoticeKi('Biztosan minden sorban �t szeretn� �ll�tani a "K�ldhet�" mez� �rt�k�t?', NOT_QUESTION) <> 0 then
     exit ;
  for i:=0 to SG1.RowCount-1 do begin
    SetGridCombo(MicsiCol, i, micsiSemmi); // a felhaszn�l� �ltal v�lasztott Nem, Igen vagy �gyv�dnek
    end;  // for
end;

procedure TSzamlaLevelDlg.IgenNemDraw(Control: TWinControl;
  Index: Integer; Rect: TRect; State: TOwnerDrawState);
var
   ActCombo: TComboBox;
   OldBrushColor: TColor;
begin
  ActCombo:= (Control as TComboBox);
  // OldBrushColor:= ActCombo.canvas.Brush.Color;
  // ActCombo.canvas.Brush.Color:=clBtnFace;
  ActCombo.canvas.fillrect(rect);
  IgenNemUgyvedList.Draw(ActCombo.Canvas, Rect.Left, Rect.top, Index);
  ActCombo.canvas.textout(rect.left+IgenNemUgyvedList.width+2,rect.top, ActCombo.items[index]);
  // ActCombo.canvas.Brush.Color:= OldBrushColor;
end;

procedure TSzamlaLevelDlg.FigyelemDraw(Control: TWinControl;
  Index: Integer; Rect: TRect; State: TOwnerDrawState);
var
   ActCombo: TComboBox;
   OldBrushColor: TColor;
begin
  ActCombo:= (Control as TComboBox);
  // OldBrushColor:= ActCombo.canvas.Brush.Color;
  // ActCombo.canvas.Brush.Color:=clBtnFace;
  ActCombo.canvas.fillrect(rect);
  FigyelemList.Draw(ActCombo.Canvas, Rect.Left, Rect.top, Index);
  ActCombo.canvas.textout(rect.left+FigyelemList.width+2,rect.top, ActCombo.items[index]);
  // ActCombo.canvas.Brush.Color:= OldBrushColor;
end;

{procedure TSzamlaLevelDlg.CheckBox1Click(Sender: TObject);
begin
   if (Sender as TCheckBox).Tag<=5 then
      NoticeKi('CB:'+IntToStr((Sender as TCheckBox).Tag));
end;
}

procedure TSzamlaLevelDlg.Feldolgozas;
var
  row, i, NyelvIndex, FajtaIndex, level_eleje, level_vege, UjStatus, Micsi: integer;
  Email, MailSubject, MailBody, TablaF1, TablaF2, TablaF3, TablaF4, Tabla: string;
  Vevokod, SzotarFokod, Nyelv, SAKOD, FN: string;
  SAKODLista, Cimzettek, MellekletLista, UresSL: TStringList;
  Siker: boolean;
  Valtozok: array of string;

  function GetFokod(NyelvIndex, FajtaIndex: integer): string;
    begin
      case NyelvIndex of        //  ['HUN','ENG','GER']
         0: begin
              if FajtaIndex=0 then Result:='460'            // ['Eml�keztet�','Felsz�l�t�']
              else Result:='461';
              end;
         1: begin
              if FajtaIndex=0 then Result:='462'            // ['Eml�keztet�','Felsz�l�t�']
              else Result:='462';  // jelenleg csak egyf�le angol lev�l van
              end;
         2: begin
              if FajtaIndex=0 then Result:='464'            // ['Eml�keztet�','Felsz�l�t�']
              else Result:='464';  // jelenleg csak egyf�le n�met lev�l van
              end;
         end;  // case
    end;

begin
  SAKODLista:=TStringList.Create;
  MellekletLista:=TStringList.Create;
  Cimzettek:=TStringList.Create;
  UresSL:=TStringList.Create;
  Application.CreateForm(TSzamlaNezoDlg, SzamlaNezoDlg);
  try
    row:= 1;  // fejl�c nem kell
    while row <= SG1.RowCount - 1 do begin
      Micsi:= GetGridCombo(9,row); // a felhaszn�l� �ltal v�lasztott Nem, Igen vagy �gyv�dnek
      if Micsi <> micsiSemmi then begin  // K�ldhet� vagy �gyv�dnek
         Vevokod:= SG1.Cells[0,row];
         Email:= SG1.Cells[2,row];
         NyelvIndex:= GetGridCombo(8,row);
         Nyelv:= NyelvArray[NyelvIndex];
         FajtaIndex:= GetGridCombo(7,row);
         SzotarFokod:= GetFokod(NyelvIndex, FajtaIndex);

         SetLength(Valtozok, 0);  // t�rl�s
         SetLength(Valtozok, Length(Valtozok)+2);
         Valtozok[Length(Valtozok)-2]:='DATUM';
         Valtozok[Length(Valtozok)-1]:=EgyebDlg.MaiDatum;
         SetLength(Valtozok, Length(Valtozok)+2);
         Valtozok[Length(Valtozok)-2]:='PARTNER';
         Valtozok[Length(Valtozok)-1]:=SG1.Cells[1,row];

         MailSubject:= GetMailPart(SzotarFokod, 'C1', Valtozok);  // c�m
         MailBody:= GetMailPart(SzotarFokod, 'F', Valtozok);   // fejsorok
         TablaF1:= GetMailPart(SzotarFokod, 'T1', Valtozok);   // t�bl�zat fejl�c
         TablaF2:= GetMailPart(SzotarFokod, 'T2', Valtozok);
         TablaF3:= GetMailPart(SzotarFokod, 'T3', Valtozok);
         TablaF4:= GetMailPart(SzotarFokod, 'T4', Valtozok);
         Tabla:= '';
         level_eleje:= row;  // az els� sor, amit ebben a lev�lben k�ld�nk
         SAKODLista.Clear;
         MellekletLista.Clear;
         while (SG1.Cells[0,row] = Vevokod) and (row <= SG1.RowCount - 1) do begin   // azonos �gyf�l sz�ml�i egy lev�lben mennek
           if GetGridCombo(9,row)= micsiKuld then begin
             Tabla:=Tabla + Ujsor; // v�lasszuk el a blokkokat
             Tabla:= Tabla+ TablaF1+': '+ SG1.Cells[13,row]+Ujsor; // teljes�t�s
             Tabla:= Tabla+ TablaF2+': '+ SG1.Cells[3,row]+Ujsor; // sz�mlasz�m
             Tabla:= Tabla+ TablaF3+': '+ SG1.Cells[4,row]+' '+SG1.Cells[5,row]+Ujsor; // Fizetend� �sszeg
             Tabla:= Tabla+ TablaF4+': '+ SG1.Cells[6,row]+Ujsor; // Fizet�si hat�rid�
             SAKOD:= SG1.Cells[3,row];
             SAKODLista.Add(SAKOD);
             FN:= EgyebDlg.TempList+'Szamla_'+SAKOD+'.pdf';
             SzamlaNezoDlg.SzamlaExport(SAKOD, FN);
             MellekletLista.Add(FN);
             end; // if
           if (GetGridCombo(9,row)= micsiUgyvednek) then begin
             Query_Insert (EgyebDlg.Query3, 'SZAMLALEVEL',
                        [
                        'SL_SAKOD', SG1.Cells[3,row],
                        'SL_KULDVE', ''''+EgyebDlg.MaiDatum+'''',
                        'SL_STATUS', IntToStr(statusLezart),
                        'SL_VEVOKOD', ''''+Vevokod+'''',
                        'SL_NYELV', ''''+''+'''',
                        'SL_SZIGOR', ''''+''+'''',
                        'SL_CIMZETT', ''''+'�gyv�dnek �tadva'+''''
                        ]);
             SG1.Cells[10,i]:='OK';
             SG1.Cells[11,row]:='�gyv�dnek �tadva';
             end; // if
           Inc(row);
           end; // while
         if SAKODLista.Count > 0 then begin  // ha legal�bb egy k�ldhet�t tal�lt
           MailBody:= MailBody+ Tabla + Ujsor;
           MailBody:= MailBody+ GetMailPart(SzotarFokod, 'L', Valtozok);   // l�bsorok
           level_vege:= row-1;  // az utols� sor, amit ebben a lev�lben k�ld�nk
           //----------------------------------------
           // Email:='nagyp69@gmail.com';  // NE HAGYJAM �GY!!!
           //----------------------------------------
           Cimzettek.Clear;
           Cimzettek.Add(Email);
           Siker:= SendOutlookHTMLMmail(Cimzettek, UresSL, MellekletLista, MailSubject, MailBody, True, uzleti);
           // Siker:= True;
           // Query_Log_Str('Sz�mla mail to: '+Email+ ' Subject: '+MailSubject+ ' Body: '+MailBody, 0, true);
           if Siker then begin
               // Napl�zzuk minden �rintett sz�ml�hoz
               for i:=0 to SAKODLista.Count-1 do begin
                 Query_Insert (EgyebDlg.Query3, 'SZAMLALEVEL',
                        [
                        'SL_SAKOD', SAKODLista[i],
                        'SL_KULDVE', ''''+EgyebDlg.MaiDatum+'''',
                        'SL_STATUS', IntToStr(statusNyitott),
                        'SL_VEVOKOD', ''''+Vevokod+'''',
                        'SL_NYELV', ''''+NyelvArray[NyelvIndex]+'''',
                        'SL_SZIGOR', ''''+FajtaArray[FajtaIndex]+'''',
                        'SL_CIMZETT', ''''+Email+''''
                        ]);
                  end;  // for
               // Let�r�lj�k az "attachment" �llom�nyokat
               try
                  for i:=0 to MellekletLista.Count-1 do begin
                    FN:= MellekletLista[i];
                    Sysutils.DeleteFile(FN);
                    end;
               finally
                  end; // try-finally
               for i:=level_eleje to level_vege do begin
                  if GetGridCombo(9,i)= micsiKuld then
                     SG1.Cells[10,i]:='OK';
                  end; // for
               end  // if Siker
           else begin
               for i:=level_eleje to level_vege do
                  SG1.Cells[11,i]:='Sikertelen k�ld�s!';
              end; // else
           end; // if SAKODLista.Count
         end //  if Micsi <> micsiSemmi
       else begin
         Inc(row);
         end;  // else
       // SG1.Refresh;  minden �jrarajzol, villognak a combo-k...
       Application.Processmessages;
       end;  // while
  finally
     if SAKODLista <> nil then SAKODLista.Free;
     if MellekletLista <> nil then MellekletLista.Free;
     if Cimzettek <> nil then Cimzettek.Free;
     if UresSL <> nil then UresSL.Free;
     if SzamlaNezoDlg <> nil then SzamlaNezoDlg.Release;
     end;  // try-finally
end;

function TSzamlaLevelDlg.GetMailPart(const SzotarFokod: string; const Part: string; const Valtozok: array of string): string;
var
  Res, Sor: string;
  i: integer;

  function MaxSorok(Part: string): integer;  // ennyi sorb�l �llhat egy blokk (fejsorok, l�bsorok)
    begin
       if Part='F' then Result:=12;
       if Part='L' then Result:=22;
    end;
begin
  if length(Part)>1 then begin  // egy sort szeretn�nk csak
      Res:= EgyebDlg.Read_SZGrid(SzotarFokod, Part);
      end // if
  else begin  // t�bb sorb�l �ll
    Res:='';
    for i:=MaxSorok(Part) downto 1 do begin  // k�nnyebb �gy a blokk v�gi �res sorokat kisz�rni
      Sor:=Trim(EgyebDlg.Read_SZGrid(SzotarFokod, Part + FormatFloat('00',i))); // k�t jegyre 0-val
      if (Sor<>'') or (Res<>'') then
        Res:=Sor+UjSor+Res;
      end;  // for
    end;  // else
  Res:=Behelyettesit(Res, Valtozok);
  Result:= Res;
end;

function TSzamlaLevelDlg.Behelyettesit(const S: string; const Valtozok: array of string): string;
var
  i: integer;
  R, v_ertek, v_nev: string;
begin
  i	:= 0;
  R:=S;  // init
 	while i <= High(Valtozok) do begin
    v_nev:= Valtozok[i];
    v_ertek:= Valtozok[i+1];
    R:=StringReplace(R, '['+v_nev+']', v_ertek, [rfReplaceAll]);  // '[PARTNER]' --> 'UPC Kft'
    i := i+2;
  	end;  // while
  Result:= R;
end;



{
procedure TSzamlaLevelDlg.AddGridCheckBox(iColNumber: integer; iRowNumber: Integer);
var
  NewCheckBox :TCheckBox;
begin
  NewCheckBox := TCheckBox.Create(SG1);
  // NP - ?
  // NewCheckBox.Parent := Panel2; // Place string grid on one panel
  NewCheckBox.Parent := SG1; // Place string grid on one panel
  NewCheckBox.Visible := False;
  NewCheckBox.Color :=  clWhite;
  NewCheckBox.OnClick := CheckBox1.OnClick; //Place one check box on you form..
  NewCheckBox.Tag:= iRowNumber;
  SG1.Objects[iColNumber, iRowNumber] := NewCheckBox;
  SetGridCheckBoxAlignment(iColNumber, iRowNumber, True); // Calling align function
end;

procedure TSzamlaLevelDlg.SetGridCheckBoxAlignment(iColNumber, iRowNumber:integer; bChecked :Boolean);
var
  NewCheckBox :TCheckBox;
  Rect:TRect;
  TempInt: Integer;
begin
  NewCheckBox := (SG1.Objects[iColNumber, iRowNumber] as TCheckBox);
  if NewCheckBox <> nil then begin
    Rect := SG1.CellRect(iColNumber,iRowNumber);
    TempInt := Trunc((Rect.Right-Rect.Left)/2);
    NewCheckBox.Left  := SG1.Left + Rect.Left + TempInt;
    NewCheckBox.Top   := SG1.Top + Rect.Top+2;
    NewCheckBox.Width := TempInt;
    NewCheckBox.Height := Rect.Bottom-Rect.Top;
    NewCheckBox.Visible := True;
    if bChecked then begin
      NewCheckBox.Checked := True;
      end
    else begin
      NewCheckBox.Checked := False;
      NewCheckBox.Color := clBlue;
      end;  // else
    end;  // if
end;

procedure TSzamlaLevelDlg.SG1Click(Sender: TObject);
var
  where: TPoint;
  ACol, ARow: integer;
  btnRect: TRect;
begin
  //Again, check to avoid recursion:
  if not FInMouseClick then begin
    FInMouseClick := true;
    try
      //Get clicked coordinates and cell:
      where := Mouse.CursorPos;
      where := SG1.ScreenToClient(where);
      SG1.MouseToCell(where.x, where.y, ACol, ARow);
      if ARow > 0 then begin
        SetGridCheckBoxAlignment(ACol, ARow, False);
        end;  // if
    finally
      FInMouseClick := false;
      end;  // try-finally
  end;
end;
    }

end.



