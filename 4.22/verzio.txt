Verzi� : 4.19 (2016.09.26)
--------------------------
- Felrak�/lerak�hely nevek egyedis�g�nek biztos�t�sa
- Felrak�/lerak�helyek ment�se
- �tvonal megjelen�t�se k�ls� b�ng�sz�ben
- Karbantart� t�bl�zatok minimaliz�sa
- Dolgoz�i tabl� megjelen�t�se
- Rendk�v�li esem�nyek r�gz�t�se
- �rtes�t�s rendk�v�li esem�nyekr�l
- K�lts�g import sz�r�s id�szakra
- K�lts�g import a J�rat adatok ablakban
- Flotta helyzet megjelen�t�se
- Rezsi j�ratt�pus
- Termel�sben r�szt nem vev� g�pkocsik
- DKV k�rty�k archiv�lhat�k
- Megrendel� k�dok nyilv�ntart�sa
- �j k�d k�r�se
- Megrendel� k�dok list�ja
- Kiegyenl�t�s megjegyz�s lej�rt sz�ml�hoz
- �v v�laszt�s a sz�ml�k Kulcs K�nyvel�sbe k�ld�se sor�n
- Inform�ci�s ablak kikapcsol�s�nak lehet�s�ge
- Dolgoz� nevek egys�ges kisbet�-nagybet� form�ban
- Sztorn�zott sz�ml�hoz tartoz� megb�z�sok m�dos�that�ak
- Leny�l� v�laszt�mez�kben g�pel�s haszn�lata
- Megb�z�s adminisztr�tor jogk�r a fuvard�j m�dos�t�s�ra
- Sz�mla kiegyenl�t�s import�l�s a Kulcs rendszerb�l
- Keres�s az adatsz�t�r f�csoportjaiban
- Telepi �zemanyag k�t �llapota
- Dolgoz� adatok import�l�sa a Kulcs rendszerb�l
- Telepi �zemanyag k�t � alacsony szint figyelmeztet�s
- Telepi �zemanyag k�t � napi helyzetjelent�s
- K�lts�g �sszes�t� � r�szletes riport Excel export�l�sa

Verzi� : 4.18 (2016.06.29)
--------------------------
- Telefonk�sz�l�kek �s el�fizet�sek �jfajta kezel�se
- A H�t�tankol�sok list�ja kimutat�s hib�j�nak jav�t�sa
- Rendszergazda jogk�rrel m�dos�that� a megb�z� a megb�z�sban
- G�pkocsi ellen�rz�sek nem mutatj�k a forgalomb�l ideiglenesen kivon j�rm�veket
- Telefonsz�ml�k nyilv�ntart�sa
- SMS k�ld�s az SMSEagle k�sz�l�kkel
- Partner EU ad�sz�mhoz k�l�n orsz�gk�d adhat� meg
- Teljes sz�veges inkrement�lis keres�s a karbantart� t�bl�zatokban
- Az �zenetszerkeszt�ben megadhatjuk az �zenet t�rgy�t
- Megb�z�s adatainak (�sof�r sms�) k�ld�se alv�llakoz�nak
- Dolgoz� fot� forgat�si lehet�s�g
- Dolgoz� adatok beolvas�sa sor�n a kapcsol�d� felhaszn�l� �rv�nytelen�t�se
- G�pkocsi �j tulajdons�ga: "Elad�sra"

Verzi� : 4.17 (2016.03.07)
--------------------------
- T�ny d�tum �s id�pont figyelembe v�tele a kapcsolt megb�z�sok sz�m�t�s�n�l
- Cseh tranzithoz tartoz� �sszeg az adatsz�t�rban m�dos�that�
- T�voll�ti d�j elsz�mol�s
- EUR sz�ml�k �FA sz�m�t�s�nak m�dos�t�sa (Kulcs �tad�s miatt)
- k�szp�nzes sz�ml�k �tutal�sosk�nt mennek �t a Kulcs F�k�nyvbe (napl�k miatt)
- NAV XML export
- Jav�t�s el�h�z�sokhoz tartoz� automatikus j�ratokn�l
- Egyedi d�jas megb�z�sok megjel�l�se a megjegyz�sben
- D�tum t�l-ig kiv�laszt�sn�l a m�sodik d�tum is v�laszthat� a napt�rb�l
- Szerviz munkalapok adatainak megjelen�t�se
- SMS k�ld�s

Verzi� : 4.16 (2015.12.17)
--------------------------
- Adatkapcsolat a KULCS rendszerrel: sz�ml�k �tad�sa, dolgoz�i adatok friss�t�se, sz�ml�k teljes�t�s�nek �tv�tele
- Telefonok karbantart�sa k�perny�n gyors sz�r�s az arch�v dolgoz�kra
- Cseh tranzitok figyel�se kimutat�s
- Kimutat�sok men�szerkezet�nek �tcsoportos�t�sa
- �zemanyag kimutat�s leny�l� list�j�ban hiba jav�t�sa
- Sz�mla EUR sz�m�t�sa bizonyos sz�ml�kn�l brutt� �sszeget sz�mol - jav�tva.
- Megb�z�sok EUR d�j�nak sz�m�t�sa az �rfolyam napj�hoz a t�ny felrak�s-lerak�s d�tumokat haszn�lja.
- XML gener�l�skor a k�b�s s�ly ellen�rz�s elt�r�s eset�n a j�rat k�dot is megjelen�ti
- �rfolyam d�tum szigor� ellen�rz�se sz�mla k�sz�t�se sor�n
- TYCO nyomtatott sz�ml�kon megjelen�tett "TEMAN" jelz�s �talak�t�sa
- Alv�llalkoz�i j�rathoz megb�z�s csatol�sakor nincs automatikus sz�ml�z�s
- J�ratsz�m v�letlen fel�l�r�s�nak megakad�lyoz�sa
- Felsz�l�t� levelek k�ld�s�n�l "K�ldhet�" oszlop csoportos be�ll�t�sa
- BridgeStone POD template v�ltoz�s (XLS f�lek sorrendje)
- ADR_030 export �llom�ny neve v�ltozott: �j form�ja TE_TED_JSSPEED_IN_FFDATA.<YYYYMMDDHHMMSS>.txt

Verzi� : 4.15 (2015.11.15)
--------------------------
- Megb�z�sok list�ja: be�p�tett sz�r�s az "elt�r� fuvard�j �s sz�ml�zott �sszeg" sorokra
- K�lts�g �sszes�t�: k�l�n l�that� a j�ratok, alj�ratok �s a saj�t alj�ratok km �rt�ke. K�lts�gek �s fuvard�jak is EUR-ban l�tszanak, ezekhez k�l�n EUR norm�k tartoznak.
- �j kateg�ria intervallumok: K�lts�g EUR/km, Nyeres�g EUR/km
- A forgalomb�l ideiglenesen kivont j�rm�veket nem vizsg�lja a g�pkocsi ellen�rz�s
- Sz�ml�zott megb�z�sn�l nem lehet fuvard�jat, egy�b d�jat vagy ezek devizanem�t m�dos�tani
- EUR fuvard�j sz�m�t�s�n�l a partner �rfolyam nap be�ll�t�s�nak figyelembe v�tele
- Cseh tranzitok sz�m�nak r�gz�t�se j�rathoz
- �j megb�z�sn�l g�pkocsi t�pus megad�sa k�telez� a ment�sn�l
- Megb�z�sok: a megb�z� nem m�dos�that�, ha m�r j�rathoz kapcsoltuk
- A "kereskedelmi �zemanyag" list�ban csak a nem arch�v g�pkocsik jelennek meg
- Dolgoz�i csoportok kezel�s�nek jav�t�sa
- Lej�rt sz�ml�k - felsz�l�t� lev�l k�ld�se
- El�re elk�sz�tett sz�r�s a "Megb�z�sok list�ja" ablakon
- Sz�mla EUR kerek�t�si hiba jav�t�sa (4.15.11. 2015.11.17.)

Verzi� : 4.14 (2015.08.12)
--------------------------
- Fuvaroz�si megb�z�s mell�klete t�bbnyelv�, sz�t�rban megadhat� EKAER sz�veg
- Partner adatokn�l megadhat� az �gyint�z�s nyelve
- Megb�z�s �tvonal hossz, teljes fuvard�j, EUR/km �rt�k sz�m�t�sa r�szmegb�z�sokra is
- A fuvaroz�si megb�z�s t�m�rebb, kivett�k az �res r�szeket
- Technikai m�dos�t�s: egy adatb�zis haszn�lata (DAT �s KOZ egyben)
- Alv�llalkoz�i d�j EUR-ban, alv�llalkoz�i EUR/km a megb�z�sok list�j�n
- G�pkocsik: "Egy�b jav�t�sok" a szerviz f�l�n nem c�ges esetben is
- Fizet�si mor�l statisztika
- Pontoz�sok be�ll�t�sa dolgoz�nk�nt
- K�lts�g �sszes�t�skor t�bb j�ratot is ki lehet v�lasztani �gy , hogy a 'CTRL' gombot folyamatosan
   nyomva kell tartani �s �gy kell klikkelni. A kijel�lt elemek sz�rk�k lesznek.
- Shell k�rty�hoz felker�lt k�l�n a rendsz�m mez�. Elk�ld�skor ellen�rz�sre ker�l, hogy a g�pkocsi
   �s a dolgoz� kapcsol�dnak-e. A g�pkocsi karbantart�sban a shell k�rtya sz�ma automatikusan be�r�-
   dik a 175-�s sorba �s a hat�rid� is kit�lt�sre ker�l.
- Bridgestone "POD" jelent�s elk�sz�t�se
- EKAER sz�m nyilv�ntart�sa a megb�z�sokn�l, tov�bb�t�s a g�pkocsivezet� fel�
- dolgoz�i telefonsz�mok sorrendezve jelennek meg (c�ges sz�m, telefon el�l)
- A megb�z�shoz tartoz� "�t�ll�s km" �rt�k szerkeszthet�
- K�nyvel�si �tad�skor a sztorn� sz�ml�n elfogadja a partnert�rzst�l elt�r� nevet (n�vv�ltoz�s)
- "igazolt gyorshajt�s" megad�sa
- Szabads�g nyilv�ntart�s: kiv�lasztott h�nap sz�nnel kiemelve
- Megb�z�sok list�ja: pirossal sz�nezz�k a mez�ket, ha az "EUR d�j" (teljes fuvard�j) �s a "Sz�mla EUR" nem egyeznek (kerek�t�si k�l�nbs�g lehet)
- "Fuvaroz�si megb�z�s" �s "Fuvaroz�si megb�z�s visszaigazol�sa" idegen nyelveken
- MSR riport jav�t�s: alv�llalkoz�k eset�n a g�pkocsi t�pust a fuvart�bl�b�l vessz�k
- �tvonalsz�m�t�s jav�t�sa: a felrak�kkal mindenk�pp a k�perny�n szerepl� sorrendben sz�mol
- Megb�z�s st�tusz lehet�s�gek: "�rv�nyes" vagy "T�r�lve", ez ut�bbi nem jelenik meg a "Megb�z�sok list�ja" ablakon
- SHELL k�rtya hozz�rendelhet� p�tkocsihoz is
- �tvonal megjelen�t�s ("Felrak�hoz", "Lerak�hoz") jav�t�sa

Verzi� : 4.13 (2015.04.28)
--------------------------
- TYCO d�jt�bl�zat id�soros kezel�se
- Felhaszn�l�i jogosults�g �r�k�t�s a t�bl�zat be�ll�t�sokat is �tveszi
- Megadhat� a partnerhez egy p�ld�nyos sz�mla nyomtat�s
- Megb�z�s �rtes�t� mail mez�inek sorrendje v�ltozott

Verzi� : 4.12 (2015.04.23)
--------------------------
- �j megb�z�s felel�se az aktu�lis felhaszn�l�
- Gyorshajt�s statisztika
- Egyszer� sz�r�s tartalmaz�sra ("tartalmaz", "nem tartalmaz")
- Havi �zemanyag kimutat�s m�dos�t�sai
- Lej�rt sz�ml�k megjelen�t�se olyan partnerekn�l, akiknek m�g nincs kifizetett sz�ml�ja
- �j felhaszn�l� r�gz�t�s jav�t�sa

Verzi� : 4.11 (2015.02.09)
--------------------------
- XML ellen�rz�skor a hiba�zenetben a megb�z�s k�dja is szerepel
- Idegen nyelv� sz�mla form�tumok (HA A KIAD�S D�TUMA >= '2015.02.01.')
- Sof�r adatokn�l �s a j�rat dolgoz�i t�bl�zat�ban 'Felrak�' kicser�l�se 'Rakod�s' -ra.
- T�bl�zatok "�v" sz�r� list�j�nak automatikus t�lt�se 
- K�ls� �llom�nyok megnyit�sa egy men�pontb�l (T�rzs�llom�nyok / K�ls� �llom�nyok megnyit�sa)
- Pontoz�s statisztika k�perny� (Kimutat�sok / Pontoz�s statisztika)
- K�lf�ldi sz�ml�k eset�n egy '�sszesen Ft' sor jelenik meg az EUR �sszesen alatt.
- �fak�dok ki�r�s�hoz l�trej�tt egy 230 -as f�csoport a sz�t�rban.
- �FA sz�veg ki�r�sok m�dosultak.
- �sszegek ill. valutanemek hib�s ki�r�sa jav�t�sra ker�lt.
- A sz�ml�kon van egy "Eur�s sz�mla" jel�l�n�gyzet. Ha ez be van kapcsolva, akkor EUR
   a sz�mla, k�l�nben Ft f�ggetlen�l a nyelvt�l.
- Az �sszes�tett sz�ml�kon ha hossz� a j�ratos sz�veg akkor 2 sorba �rjuk.
- Ha nem Ft a sz�mla valutaneme, akkor soronk�nt megjelen�tj�k az �FA forintos �sszeg�t.
- Pontoz�s: pontok m�dos�t�sa jav�tva, saj�t pontoz�s nincs, dolgoz�k bel�p�s�nek idej�t 
   is figyelj�k.
- �sszes�tett sz�ml�n megjelenik a k�sz�t�
- mindk�t sz�ml�n a 2. �sszesen sor karaktere nem bold
- nyelv v�ltoztat�sn�l v�ltozik a sorok sz�vege is ha gener�lt
- K�lts�g �sszes�t�s: alj�ratok figyelembe v�tel�nek jav�t�sa
- A partnert�rzsben a nyelv mellett tov�bbra is megadhat� a sz�mla alap�rtelmezett devizaneme 
  (EUR vagy HUF) 
- A dolgoz� neve csak a Servantes-ben m�dos�that�
- Sz�ml�kon storn� ill. kl�noz�s viszi a nyelvi be�ll�t�sokat is
- Kimutat�s a h�t�tankol�sr�l jav�t�sa (Niki)
- J�ratba d�tum szerinti sz�r�s be�p�t�se (Laci)
- Megb�z�s m�dos�t�s er�forr�s felszabad�t�s jav�t�sa
- �zemanyag kimutat�s "els� �tlag t/km" hiba jav�t�sa

Verzi� : 4.10 (2014.12.09)
--------------------------

Verzi� : 4.09 (2014.12.08)
--------------------------
- R�szrak� mez� t�rl�se a g�pkocsib�l 
- R�szrak� berak�sa a megb�z�s sorokba 
- ADR30 -ban prepaid/collect kezel�s m�dos�t�sa
- J�rat k�lts�gekn�l hiba�zenet kezel�se - nincs k�lts�gimport ellen�rz�s 
- Sz�ml�n�l ha a megb�z�s egyedi, a fejl�cben ki�rja a '(TENAM)' sz�veget 
- �sszes�tett sz�ml�kn�l is ki�rja a '(TENAM)' sz�veget a fejl�cbe  v�gleges�t�skor  ellen�rz�s
  ut�n. Ha nem minden t�tel egyedi akkor hiba�zenetet k�ld 
- Servantes kommunik�ci� jav�t�sa 
- XML export m�dos�t�sa : 
  - standard/express sz�veg megjelen�t�se
  - s�ly �s g�pkocsi kateg�ria �sszevet�se
  - �zemanyag p�tl�g �rt�k�nek ellen�rz�se -> csak eg�sz sz�m lehet

Verzi� : 4.08 (2014.11.24)
--------------------------
- R�szsz�mla is kl�nozhat� 
- Partner adatok megv�ltoz�sa eset�n levelek k�ld�nk a c�mzetteknek a 376,110 sz�t�relemb�l 
- A pontoz�knak k�telez� megadni a havi pontokat mert addig nem tudj�k haszn�lni a rendszert
  am�g a pontozand�k list�ja nem �res. 
- A megb�z�s r�szletekn�l felgyorsult a felrak�k bet�lt�se. 
- Az ADR030 riportban m�dosult az FTL/LTL kezel�s �s beker�lt a standard/express tulajdons�g 
  ill. a fizet�si tulajdons�g 
- Fuvart�bla beolvas�s�nak jav�t�sa 
- A k�bs�ly haszn�lat�nak m�dos�t�sa 
  - A k�bs�ly megjelen�s�t a megb�z�s kartonon elhelyezett mez� szab�lyozza
  - MSR Exportba a k�b�s s�ly is beker�lt a norm�l s�ly mell�, ha 0 akkor megy a norm�l s�ly
  - EDI Exportba a k�b�s s�ly ker�l be�r�sa ha l�tezik, k�l�nben a norm�l

Verzi� : 4.07 (2014.11.09)
--------------------------
- Verzi�sz�m megjelen�t�se minden f�ablak c�m�ben
- F�l�sleges sz�mlaform�tumok t�rl�se -> FR, LE
- Fuvarnapl� kezel�s t�rl�se
- UZAKOLTS t�bla + kezel�s t�rl�se
- K�lts�gimport m�dos�t�sa
  - HU-GO -ba 0 �sszegek nem ker�lnek bele
  - ASFINAG feldolgoz�s t�rl�se
  ( Sajnos a feldolgoz�s m�g mindig nincs k�sz )
- K�bs�ly kezel�se a megb�z�sokn�l
  - 999/570 sz�t�r elemben k�bs�lyos megb�z� k�dok felsorol�sa
  - �j mez�k a megb�z�s sorokn�l a megfelel� megb�z�kn�l 

Verzi� : 4.06 (2014.11.03)
--------------------------
- Csak a bejelentkezett felhaszn�l�k l�tsz�dnak
- J�rat ellen�rz�tts�g vizsg�lat�n�l a d�tumot is n�zi a progi
- Szabads�gok be�ll�t�sa: ha nincs megfelel� dolgoz�i csoport, nem sz�ll el
- Feketelist�s vev�k: - felel�s is megjelenik a lev�lben
                      - m�dos�t�sn�l is megy a lev�l
- Adatb�zis ellen�rz�s �j men�ponttal (Technikai feladatokban)
- G�pkocsikn�l sof�r adatok mindig l�that�k

Verzi� : 4.05 (2014.09.24)
--------------------------
- Excel �llom�nyok gener�l�s�n�l numerikus �rt�kek form�z�sa 
- T�bl�zat k�lalak m�dos�t�s - v�lt�s klasszikus n�zetre 
- Hibaki�r� n�dos�t�sa
- J�rat f� k�perny�n ALT-F11 -> K�lts�g �sszes�t�s -> Ellen�rz�si lehet�s�g 
- G�pkocsi akkor arh�v ha ki van t�ltve a forgalomb�l kiker�l�s d�tuma 
- K�zponti email k�ld�si lehet�s�g  ( SendJSEmail )
- �j megb�z�sn�l feketelist�s megb�z�n�l levelek k�ld�nk a c�mzetteknek a 376,100 sz�t�relemb�l
- K�lts�g import m�dos�t�sa HUGO ill. ETOLL folyamatokra

Verzi� : 4.04 (2014.09.15)
--------------------------
- Dolgoz�i karbantart�s m�dos�t�sa (ad�sz�m csak akkor kell ha enged�lyezett a mez�)
- �llom�nyok ment�se PDF, XLS ill. RTF form�tumokban 
- Gener�tor f�l felvitele a g�pkocsi szerv�z oldalra 
- Fuvarnapl� ment�s jav�t�sa 
- Sablon ment�s jav�t�sa 
- Log -ok �r�sa felhaszn�l�nk�nt

Verzi� : 4.03 (2014.09.10)
--------------------------
- Kereskedelmi �zemanyag b�v�t�se �sszes�t�ssel + kihagyand�kkal

Verzi� : 4.02 (2014.09.08)
--------------------------
- Forgalmi kimutat�s k�sz�t�se felhaszn�l�nk�nt
- Jogosults�gokn�l a k�d megjelen�t�se
- �j lista : Kereskedelmi �zemanyag kimutat�s
- Kombin�lt lista jav�t�sa
- INI �llom�nyokban user n�v �s jelsz� t�rol�sa k�dolva
- Servantes kezel�s �t�r�sa
- A r�szletes megb�z�s panelre felker�lt a k�s�s oka mez�
- RB Export men�pont �s funkci� megjelen�se a Speci�lis riportokban
- Adatimport�l�sban KOLTSEG_GYUJTO t�bla kezel�s�nek m�dos�t�sa


Verzi� : 4.01 (2014.08.21)
--------------------------
- Logok ki�r�sa p�rhuzamos sz�lon fuvar_ado_log.txt �llom�nyba 
- Felhaszn�l� nev�nek ki�r�sa a jobb fels� sarokban 
- Excel �llom�nyok automatikus ment�s�nek jav�t�sa


Verzi� : 4.000 (2014.08.18)
------------------------------------
- Az �j program a r�gi minden funcki�j�t �tvette kiv�ve az �vv�lt�st.
- Tov�bbi m�dos�t�sok:
   - �rv�nyess�g ellen�rz�sek csak akkor futnak ha a felhaszn�l� k�ri (r�klikkel a megfelel� oldalra) 
   - Jogosults�gokn�l a felhaszn�l�k n�v szerinti sorrendben szerepelnek
   - �tnyilv�ntart�s m�dos�t�sra ker�lt -> rendsz�mra is le lehet k�rni
   - Programoz�i csoportban parancsok m�dos�t�sa:
      - K�telez� k�lts�gek men�pont letiltva
      - Z�r� km-ekhez kateg�ria felt�lt�se t�r�lve
      - �rfolyamt�lt� t�r�lve

