unit ComboForm;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls;

type
  TComboFormDlg = class(TForm)
    cbCombo: TComboBox;
    procedure cbComboChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ComboFormDlg: TComboFormDlg;

implementation

{$R *.dfm}

procedure TComboFormDlg.cbComboChange(Sender: TObject);
begin
   Close;
end;

end.
