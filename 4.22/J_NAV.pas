unit J_NAV;

interface

uses
  Classes, Grids, SysUtils
  ;

const
  MAXOSZLOP = 50;

type
  TJNAV  = class (TComponent)
    public
      constructor Create(AOwner: TComponent); override;
    	destructor 	Destroy; override;
      procedure FillGrid( elemek : array of string);
      function SaveGrid( fn : string) : string;
      function Kicserelt(be : string) : string;
      function Konvertal(fromfn, tofn : string) : string;
    private
      DataGrid    : TStringGrid;
    public
    end;

implementation

uses
  Shellapi, Windows, Forms;
{******************************************************************************
Elj�r�s neve  :  Create
Le�r�s        :  Objektum l�trehoz�sa
Param�terek   :  AOwner    - sz�l� objektum
*******************************************************************************}
constructor TJNAV.Create(AOwner: TComponent);
begin
   Inherited Create(AOwner);
   DataGrid               := TStringGrid.Create(nil);
   DataGrid.ColCount      := MAXOSZLOP;
   DataGrid.RowCount      := 1;
   DataGrid.Cells[ 0, 0]  := 'szlasorszam';
   DataGrid.Cells[ 1, 0]  := 'szlatipus';
   DataGrid.Cells[ 2, 0]  := 'szladatum';
   DataGrid.Cells[ 3, 0]  := 'teljdatum';
   DataGrid.Cells[ 4, 0]  := 'szamlakibocsato_adoszam';
   DataGrid.Cells[ 5, 0]  := 'szamlakibocsato_kozadoszam';
   DataGrid.Cells[ 6, 0]  := 'szamlakibocsato_kisadozo';
   DataGrid.Cells[ 7, 0]  := 'szamlakibocsato_nev';
   DataGrid.Cells[ 8, 0]  := 'szamlakibocsato_cim_iranyitoszam';
   DataGrid.Cells[ 9, 0]  := 'szamlakibocsato_cim_telepules';
   DataGrid.Cells[10, 0]  := 'szamlakibocsato_cim_kerulet';
   DataGrid.Cells[11, 0]  := 'szamlakibocsato_cim_kozterulet_neve';
   DataGrid.Cells[12, 0]  := 'szamlakibocsato_cim_kozterulet_jellege';
   DataGrid.Cells[13, 0]  := 'szamlakibocsato_cim_hazszam';
   DataGrid.Cells[14, 0]  := 'szamlakibocsato_cim_epulet';
   DataGrid.Cells[15, 0]  := 'szamlakibocsato_cim_lepcsohaz';
   DataGrid.Cells[16, 0]  := 'szamlakibocsato_cim_szint';
   DataGrid.Cells[17, 0]  := 'szamlakibocsato_cim_ajto';
   DataGrid.Cells[18, 0]  := 'vevo_adoszam';
   DataGrid.Cells[19, 0]  := 'vevo_kozadoszam';
   DataGrid.Cells[20, 0]  := 'vevo_nev';
   DataGrid.Cells[21, 0]  := 'vevo_cim_iranyitoszam';
   DataGrid.Cells[22, 0]  := 'vevo_cim_telepules';
   DataGrid.Cells[23, 0]  := 'vevo_cim_kerulet';
   DataGrid.Cells[24, 0]  := 'vevo_cim_kozterulet_neve';
   DataGrid.Cells[25, 0]  := 'vevo_cim_kozterulet_jellege';
   DataGrid.Cells[26, 0]  := 'vevo_cim_hazszam';
   DataGrid.Cells[27, 0]  := 'vevo_cim_epulet';
   DataGrid.Cells[28, 0]  := 'vevo_cim_lepcsohaz';
   DataGrid.Cells[29, 0]  := 'vevo_cim_szint';
   DataGrid.Cells[30, 0]  := 'vevo_cim_ajto';
   DataGrid.Cells[31, 0]  := 'termek_termeknev';
   DataGrid.Cells[32, 0]  := 'termek_besorszam';
   DataGrid.Cells[33, 0]  := 'termek_menny';
   DataGrid.Cells[34, 0]  := 'termek_mertekegys';
   DataGrid.Cells[35, 0]  := 'termek_nettoar';
   DataGrid.Cells[36, 0]  := 'termek_nettoegysar';
   DataGrid.Cells[37, 0]  := 'termek_adokulcs';
   DataGrid.Cells[38, 0]  := 'termek_adoertek';
   DataGrid.Cells[39, 0]  := 'termek_szazalekertek';
   DataGrid.Cells[40, 0]  := 'termek_bruttoar';
   DataGrid.Cells[41, 0]  := 'nem_kotelezo_tipus_fiz_hatarido';
   DataGrid.Cells[42, 0]  := 'nem_kotelezo_tipus_fiz_mod';
   DataGrid.Cells[43, 0]  := 'nem_kotelezo_tipus_penznem';
end;

{******************************************************************************
Elj�r�s neve  :  Destroy
Le�r�s        :  Objektum megsz�ntet�se
Param�terek   :  -
*******************************************************************************}
destructor  TJNAV.Destroy;
begin
   DataGrid.Free;
end;

procedure TJNAV.FillGrid( elemek : array of string);
var
  i       : integer;
  oszlop  : integer;
begin
  oszlop            := 0;
  Datagrid.RowCount := Datagrid.RowCount + 1;
  for i := Low(elemek) to High(elemek) do begin
    if oszlop < MAXOSZLOP then begin
      Datagrid.Cells[ oszlop, Datagrid.RowCount - 1] := Kicserelt(elemek[i]);
    end;
    Inc(oszlop);
  end;
end;

function TJNAV.SaveGrid( fn : string) : string;
var
  fn1 : string;
  i   : integer;
  j   : integer;
  F   : TextFile;
begin
  Result  := '';
  // A t�bl�zat ment�se
  fn1 := fn;
  i   := 2;
  while ( ( FileExists(fn1+'.txt') or FileExists(fn1+'.xml') ) ) do begin
    fn1 := fn + Format('_%2.2d', [i]);
    Inc(i);
  end;
  {$I-}
  AssignFile(F, fn1+'.txt');
  Rewrite(F);
  {$I+}
  if IOResult <> 0then begin
      // Nem siker�lt ki�rni a sz�veget
      Exit;
  end;
  for i := 0 to Datagrid.RowCount - 1 do begin
    for j := 0 to Datagrid.ColCount - 1 do begin
      Write(F, Datagrid.Cells[j, i]+';');
    end;
    Writeln(F, '');
  end;
  Flush(f);
  CloseFile(f);
  // A txt konvert�l�sa XML-be
  Result  := Konvertal(fn1+'.txt', fn1+'.xml');
  DeleteFile(PWideChar(fn1+'.txt'));
end;

function TJNAV.Kicserelt(be : string) : string;
begin
  Result  := StringReplace(be, ';', ',', [rfReplaceAll, rfIgnoreCase]);
end;

function TJNAV.Konvertal(fromfn, tofn : string) : string;
var
  fnexe     : string;
  SEInfo    : TShellExecuteInfo;
  ExitCode  : DWORD;
  ExecuteFile, ParamString, StartInString: string;
begin
  Result  := '';
  fnexe   := ExtractFilePath(Application.Exename)+'NAVXML.EXE';
  if not FileExists(fnexe) then begin
    Exit;
  end;
  ExecuteFile:=fnexe;
  FillChar(SEInfo, SizeOf(SEInfo), 0) ;
  SEInfo.cbSize := SizeOf(TShellExecuteInfo) ;
  with SEInfo do begin
       fMask           := SEE_MASK_NOCLOSEPROCESS;
       Wnd             := Application.Handle;
       lpFile          := PChar(ExecuteFile) ;
       nShow           := SW_SHOWNORMAL;
       lpParameters    := PChar(fromfn + ' ' + tofn) ;
       lpDirectory     := PChar(ExtractFilePath(Application.Exename)) ;
   end;
   if ShellExecuteEx(@SEInfo) then begin
      repeat
           Application.ProcessMessages;
           GetExitCodeProcess(SEInfo.hProcess, ExitCode) ;
      until (ExitCode <> STILL_ACTIVE) or Application.Terminated;
      Result  := tofn;
   end;
end;

end.
