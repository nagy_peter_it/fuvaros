unit KiDolgErv;

interface

uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls, Egyeb,
  Forms, Dialogs, DBTables, DB, StdCtrls, Buttons, Mask, DBCtrls,
  ExtCtrls, ADODB, QuickRpt;

type
  TKiDolgErvDlg = class(TForm)
	 BitBtn4: TBitBtn;
	 BitBtn6: TBitBtn;
    Label1: TLabel;
    Label2: TLabel;
    CB1: TComboBox;
    CB2: TComboBox;
    CB10: TComboBox;
    Query1: TADOQuery;
	 procedure FormCreate(Sender: TObject);
	 procedure BitBtn4Click(Sender: TObject);
	 procedure BitBtn6Click(Sender: TObject);
  private
  public
  end;

var
  KiDolgErvDlg: TKiDolgErvDlg;

implementation

uses
	DolgErvList, Kozos, J_SQL;
{$R *.DFM}

procedure TKiDolgErvDlg.FormCreate(Sender: TObject);
begin
	// Bet�ltj�k az adatokat
	EgyebDlg.SetADOQueryDatabase(Query1);
	Query_Run(Query1, 'SELECT SZ_ALKOD, SZ_MENEV FROM SZOTAR WHERE SZ_FOKOD = ''127'' ORDER BY SZ_MENEV ');
	CB1.Clear;
	CB10.Clear;
	while not Query1.Eof do begin
		CB1.Items.Add(Query1.FieldByName('SZ_MENEV').AsString);
		CB10.Items.Add(Query1.FieldByName('SZ_ALKOD').AsString);
		Query1.Next;
	end;
	if Query1.RecordCount > 0 then begin
		CB1.ItemIndex	:= 0;
	end;
end;

procedure TKiDolgErvDlg.BitBtn4Click(Sender: TObject);
begin
	{A lista �ssze�ll�t�sa a sz�r�si elemek alapj�n}
	Screen.Cursor	:= crHourGlass;
	Application.CreateForm(TDolgErvListDlg, DolgErvListDlg);
	DolgErvListDlg.Tolto(CB1.Text, CB10.Items[CB1.ItemIndex], CB2.ItemIndex);
	Screen.Cursor	:= crDefault;
	DolgErvListDlg.Rep.Preview;
	DolgErvListDlg.Destroy;
end;

procedure TKiDolgErvDlg.BitBtn6Click(Sender: TObject);
begin
	Close;
end;

end.


