unit RESTAPI_kapcs;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls, Vcl.CheckLst,
  IPPeerClient, REST.Client, REST.Types, Data.Bind.Components, Data.Bind.ObjectScope,
  Vcl.ComCtrls, Data.DBXJSON, Vcl.Menus;

type
  TRESTAPIKapcsolatDlg = class(TForm)
    GridPanel2: TGridPanel;
    Button4: TButton;
    ListView1: TListView;
    lbSelectedID: TListBox;
    PopupMenu1: TPopupMenu;
    UzenetTop1: TMenuItem;
    procedure Button4Click(Sender: TObject);
    procedure UzenetTop1Click(Sender: TObject);
  private
    FThisID: string;
    FRootResource: string;
    FResourceDisplayName: string;
    FKapcsolatElem: string;
    // FListaURL: string;
    FIDElem: string;
    FIDKapcsolatElem: string;
    FURLKapcsolatElem: string;
    FListaURL: string;

  public
    procedure SetRequestResource(const Res, ThisID: string);
    procedure SetResourceDisplayName(const DisplayName: string);
    procedure SetKapcsolatElem(const ElemNeve: string);
    procedure SetListaURL(const ListaURL: string);
    procedure SetIDElem(const IDElem: string);
    procedure SetIDKapcsolatElem(const IDKapcsolatElem: string);
    procedure SetURLKapcsolatElem(const URLKapcsolatElem: string);
    procedure FillKapcsolatData(const AArchivIs: boolean);
    procedure SendRequest;
  end;

var
  RESTAPIKapcsolatDlg: TRESTAPIKapcsolatDlg;

implementation

{$R *.dfm}

uses
   UzenetTop, RESTAPI_Kozos;

procedure TRESTAPIKapcsolatDlg.SetRequestResource(const Res, ThisID: string);
begin
    // RESTReqKapcs.Resource:= Res + '/'+ ThisID;
    FThisID:= ThisID;
    FRootResource:= Res;
end;

procedure TRESTAPIKapcsolatDlg.SetResourceDisplayName(const DisplayName: string);
begin
   FResourceDisplayName:= DisplayName;
   Caption:= FResourceDisplayName;
end;

procedure TRESTAPIKapcsolatDlg.SetListaURL(const ListaURL: string);
begin
   FListaURL:= ListaURL;
end;

procedure TRESTAPIKapcsolatDlg.SetIDElem(const IDElem: string);
begin
   FIDElem:= IDElem;
end;

procedure TRESTAPIKapcsolatDlg.SetIDKapcsolatElem(const IDKapcsolatElem: string);
begin
   FIDKapcsolatElem:= IDKapcsolatElem;
end;

procedure TRESTAPIKapcsolatDlg.SetURLKapcsolatElem(const URLKapcsolatElem: string);
begin
   FURLKapcsolatElem:= URLKapcsolatElem;
end;

procedure TRESTAPIKapcsolatDlg.UzenetTop1Click(Sender: TObject);
begin
   UzenetTopDlg.Show;
end;

procedure TRESTAPIKapcsolatDlg.SetKapcsolatElem(const ElemNeve: string);
begin
   FKapcsolatElem:= ElemNeve;
end;

procedure TRESTAPIKapcsolatDlg.FillKapcsolatData(const AArchivIs: boolean);
var
   i, oszlop: integer;
   ResponseJSON, S, ElemNeve, ElemID: string;
   jsObject: TJSONObject;
   jsKapcsArray, jsListaArray: TJSONArray;
   jsKapcs, jsListaElem, jsRootValue: TJSONValue;
   LVItem, LVSubItem: TListItem;
   JSONResult: TJSONResult;
   ElemArchive: boolean;
begin
   // ---- megjel�ltek lek�r�se ----- //
   JSONResult:= RESTAPIKozosDlg.ExecRequest(Format('%s/%s', [FRootResource, FThisID]), 'get', RESTAPIKozosDlg.EmptyParamsList);
   if JSONResult.ErrorString <> '' then begin
        MessageDlg(JSONResult.ErrorString, mtError,[mbOK],0);
        // NoticeKi(S);
        Exit;  // function
        end; // if
   jsRootValue := JSONResult.JSONContent;
   jsKapcsArray:= (jsRootValue as TJSONObject).Get(FKapcsolatElem).JsonValue as TJSONArray;  // a kapcsolt elemeket tartalmaz� t�mb
   lbSelectedID.Items.Clear;
   for jsKapcs in jsKapcsArray do begin
      ElemID:= ((jsKapcs as TJSONObject).Get(FIDKapcsolatElem).JsonValue as TJSONString).Value;
      lbSelectedID.Items.Add(ElemID);
      end;
   // ----- lista felt�lt�s -------- //
   JSONResult:= RESTAPIKozosDlg.ExecRequest(Format('%s', [FListaURL]), 'get', RESTAPIKozosDlg.EmptyParamsList);
   if JSONResult.ErrorString <> '' then begin
        MessageDlg(JSONResult.ErrorString, mtError,[mbOK],0);
        // NoticeKi(S);
        Exit;  // function
        end; // if
   jsListaArray:= JSONResult.JSONContent as TJSONArray;
   ListView1.Items.Clear;
   for jsListaElem in jsListaArray do begin
      ElemNeve:= ((jsListaElem as TJSONObject).Get('name').JsonValue as TJSONString).Value;
      ElemID:= ((jsListaElem as TJSONObject).Get(FIDKapcsolatElem).JsonValue as TJSONString).Value;
      if (jsListaElem as TJSONObject).Get('archived').JsonValue = nil then
          ElemArchive:= False
      else ElemArchive:= ((jsListaElem as TJSONObject).Get('archived').JsonValue is TJSONTrue);
      if (not AArchivIs) and ElemArchive then begin
            continue;  // next "for" iteration
            end;
      LVItem:= ListView1.Items.Add;
      if ElemArchive then
          LVItem.Caption := ElemNeve+ ' (arch�v)'
      else LVItem.Caption := ElemNeve;
      LVItem.SubItems.Add(ElemID);
      if lbSelectedID.Items.IndexOf(ElemID) >=0 then begin // benne van
        LVItem.Checked:= True;
        LVItem.SubItems.Add('1');  // nem l�tszik a mez�
        end
      else begin
        LVItem.Checked:= False;
        LVItem.SubItems.Add('0');  // nem l�tszik a mez�
        end;
      // JSON kezel�s: ld GoogleGeoCode.pas
      end; // for
end;

procedure TRESTAPIKapcsolatDlg.Button4Click(Sender: TObject);
begin
   SendRequest;
end;

procedure TRESTAPIKapcsolatDlg.SendRequest;
var
  i: integer;
  BodyKey, BodyValue, S, URLResource, KapcsolatItemID, MyRootResource, HibaString, Res: string;
  LMethod: string;
  ItemCheckedNow, StoredCheck: boolean;
  JSONResult: TJSONResult;
begin
  Screen.Cursor:= crHourGlass;
  try
      HibaString:= '';
      for i:=0 to ListView1.Items.Count-1 do begin
         ItemCheckedNow:= ListView1.Items[i].Checked;
         StoredCheck:= (ListView1.Items[i].SubItems[1] = '1');
         KapcsolatItemID:= ListView1.Items[i].SubItems[0];
         if ItemCheckedNow <> StoredCheck then begin  // v�ltozott
            URLResource:= Format('%s/%s/%s/%s', [FRootResource, FThisID, FURLKapcsolatElem, KapcsolatItemID]);
            if ItemCheckedNow then LMethod:= 'post'
            else LMethod:= 'delete';
            JSONResult:= RESTAPIKozosDlg.ExecRequest(URLResource, LMethod, RESTAPIKozosDlg.EmptyParamsList);
            if JSONResult.ErrorString <> '' then begin
                HibaString:= HibaString + ' | '+ JSONResult.ErrorString;
                end;  // if
            end;  // if
         end;  // for
       if HibaString = '' then begin
          ModalResult:= mrOK;  // close form
          end
       else begin
          MessageDlg(HibaString, mtError,[mbOK],0);
         // NoticeKi(S);
          end;
  finally
    Screen.Cursor:= crDefault;;
    end;  // try-finally
end;



end.
