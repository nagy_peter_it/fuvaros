unit TelSzamlaFm;

interface

uses
	SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
	StdCtrls, Forms, DBCtrls, DB, DBGrids, DBTables, Grids, ExtCtrls, Buttons,
	ShellApi, Dialogs, ADODB, J_FOFORMUJ, Vcl.Menus;

type
	TTelSzamlaFmDlg = class(TJ_FOFORMUJDLG)
    Query3: TADOQuery;
    PopupMenu1: TPopupMenu;
    Image1: TImage;
    BitBtn1: TBitBtn;
    Megjegyzsmegtekintse1: TMenuItem;
    miIgazol: TMenuItem;
    miIgazolVissza: TMenuItem;
    BitBtn2: TBitBtn;
	  procedure FormCreate(Sender: TObject);override;
    procedure Adatlap(cim, kod : string);override;
    procedure DBGrid2DrawColumnCell(Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);override;
    procedure BitBtn1Click(Sender: TObject);
    procedure ShowReszletek;
    procedure Megjegyzsmegtekintse1Click(Sender: TObject);
    procedure miIgazolClick(Sender: TObject);
    procedure miIgazolVisszaClick(Sender: TObject);
    procedure Query1AfterScroll(DataSet: TDataSet);override;
    procedure BitBtn2Click(Sender: TObject);
	end;

var
	TelSzamlaFmDlg : TTelSzamlaFmDlg;
  // bmp: TBitmap;

implementation

uses
	Egyeb, J_SQL, Kozos, UniSzovegForm, TelSzamlaTetelekFm, UniGraphInfoForm;

{$R *.DFM}

procedure TTelSzamlaFmDlg.FormCreate(Sender: TObject);
var
  EgyediLimitIroda, EgyediLimitSofor, SajatAtlagSzorzo, CsoportAtlagSzorzo: string;
begin
	EgyebDLg.SetADOQueryDatabase(Query3);
	Inherited FormCreate(Sender);
  EgyediLimitIroda:= EgyebDlg.Read_SZGrid('383', '1');
  EgyediLimitSofor:= EgyebDlg.Read_SZGrid('383', '2');
  SajatAtlagSzorzo:= EgyebDlg.Read_SZGrid('383', '11');
  CsoportAtlagSzorzo:= EgyebDlg.Read_SZGrid('383', '12');

	AddSzuromezoRovid('Telefonsz�m', '');
	AddSzuromezoRovid('Id�szak', '');
	AddSzuromezoRovid('Sz�mlacsoport', '');
	AddSzuromezoRovid('Kin�l', '');
  AddSzuromezoRovid('Igazolva', '', '1');
  AddSzuromezoRovid('T�telOszt�ly', '', '1');
  AddSzuromezoRovid('TS_ID', '');
  AddSzummamezo('T�vk�zl�si_szolg�ltat�sok');
  AddSzummamezo('Teljes_�sszeg_brutt�');
  Alapstr := 'select * from '+
        ' (select convert(varchar, ROW_NUMBER() OVER(ORDER BY TS_ID, TS_IDOSZAK_TOL, TS_IDOSZAK_IG, TT_TELSZAM  ASC)) AS Sorsz�m, '+
        ' TS_IDOSZAK_TOL+'' - ''+TS_IDOSZAK_IG Id�szak, '+
       //  '    ltrim(rtrim(isnull(DO_NAME,'''') + '' ''+ isnull(TT_HINFO,''''))) Kin�l, '+
        '    case when (isnull(TZ_DONEVLIST,'''') <> '''') then ltrim(rtrim(isnull(TZ_DONEVLIST,''''))) else ltrim(rtrim(isnull(TZ_HINFO,''''))) end Kin�l, '+
        '    SZAMLACSOPORT Sz�mlacsoport, '+
        '    TT_TELSZAM Telefonsz�m, '+
        '	SUM(case when TT_TAVKOZLESI=1 then TT_NETTOAR else 0 end) T�vk�zl�si_szolg�ltat�sok, '+
        '	SUM(case when TT_TAVKOZLESI=1 then TT_BRUTTOAR else 0 end) T�vk�zl�si_szolg_brutto, '+
        // '	SUM(TT_BRUTTOAR) Teljes_�sszeg_brutt�, '+
        '	SUM(TT_NETTOAR * (100+TT_AFAKULCS) / 100.0) Teljes_�sszeg_brutt�, '+
        '	szami.GetTetelMegjegyzesek(TS_ID, TT_TELSZAM) Megjegyz�sek, '+
        // ' szami.TelefonszamlaEllenor3 (TS_IDOSZAK_TOL+'' - ''+TS_IDOSZAK_IG, TT_TELSZAM, case when TE_SZAMLACSOPKOD = 1 then '+EgyediLimitIroda+' else '+EgyediLimitSofor+' end, '+
        // '   '+SajatAtlagSzorzo+', '+CsoportAtlagSzorzo+', TZ_IGAZOL) T�telOszt�ly, '+
        ' case when TZ_IGAZOL=''1'' then '''' else TZ_OSZTALY end T�telOszt�ly, '+
        ' case when isnull(TZ_IGAZOL,'''')=''1'' then ''Igen'' else '''' end Igazolva, '+
        ' TS_ID '+
        ' from TELSZAMLATETELEK '+
        ' join TELSZAMLAK on TT_TSID = TS_ID '+
        // ' left outer join DOLGOZO on TT_DOKOD = DO_KOD '+
        ' left outer join (select TE_TELSZAM, substring(SZ_MENEV,1,20) SZAMLACSOPORT, TE_SZAMLACSOPKOD from TELELOFIZETES, SZOTAR '+
        '         where SZ_FOKOD=''145'' and TE_SZAMLACSOPKOD = SZ_ALKOD) x1 '+
        '         on TT_TELSZAM = TE_TELSZAM '+
        ' left outer join TELSZAMLASZAMOK on TZ_TSID = TS_ID and TZ_TELSZAM = TT_TELSZAM '+
        ' group by TS_ID, TS_IDOSZAK_TOL, TS_IDOSZAK_IG, TT_TELSZAM, TZ_DONEVLIST, TZ_HINFO, SZAMLACSOPORT, TE_SZAMLACSOPKOD, TZ_IGAZOL, TZ_OSZTALY '+
        ' ) a where 1=1 ';  // a k�s�bbi sz�r�shez kell
   kellujures:= false;
   FelTolto('', 'Sz�mla adatok m�dos�t�sa',
			'Telefonsz�ml�k', 'Sorsz�m',
           alapstr,
           'TELSZAMLATETELEK',
            QUERY_KOZOS_TAG);
	width	:= DLG_WIDTH;
	height	:= DLG_HEIGHT;
  ButtonUj.Visible:= False;
  ButtonMod.Visible:= False;
  ButtonNez.Visible:= True;
  DBGrid2.PopupMenu	:= PopupMenu1;
	BitBtn1.Parent		:= PanelBottom;
	BitBtn2.Parent		:= PanelBottom;
  // bmp:= TBitmap.Create;
  // bmp.Assign(Image1.Picture);
end;

procedure TTelSzamlaFmDlg.miIgazolClick(Sender: TObject);
var
  Szoveg, TSID, KOD, Telefonszam: string;
begin
    TSID:= Query1.FieldByName('TS_ID').AsString;
    Telefonszam:= Query1.FieldByName('Telefonsz�m').AsString;
    KOD:= Query1.FieldByName(FOMEZO).AsString;
    Query_Update (Query3, 'TELSZAMLASZAMOK', [
      'TZ_IGAZOL',  '1'
        ], ' WHERE TZ_TSID = '+TSID+' and TZ_TELSZAM = '''+Telefonszam+'''');
    SQL_ToltoFo(GetOrderBy(true));   // t�bl�zat friss�t�s
    ModLocate (Query1, FOMEZO, KOD);
end;

procedure TTelSzamlaFmDlg.miIgazolVisszaClick(Sender: TObject);
var
  Szoveg, TSID, KOD, Telefonszam: string;
begin
    TSID:= Query1.FieldByName('TS_ID').AsString;
    Telefonszam:= Query1.FieldByName('Telefonsz�m').AsString;
    KOD:= Query1.FieldByName(FOMEZO).AsString;
    Query_Update (Query3, 'TELSZAMLASZAMOK', [
      'TZ_IGAZOL',  '0'
        ], ' WHERE TZ_TSID = '+TSID+' and TZ_TELSZAM = '''+Telefonszam+'''');
    SQL_ToltoFo(GetOrderBy(true));   // t�bl�zat friss�t�s
    ModLocate (Query1, FOMEZO, KOD);
end;

procedure TTelSzamlaFmDlg.Query1AfterScroll(DataSet: TDataSet);
begin
  if (Query1.FieldByName('Igazolva').AsString <> '') then begin
       miIgazol.Visible:= False;
       miIgazolVissza.Visible:= True;
       end
  else begin
       miIgazol.Visible:= True;
       miIgazolVissza.Visible:= False;
       end;  // else

  inherited Query1AfterScroll(DataSet);
end;

procedure TTelSzamlaFmDlg.Megjegyzsmegtekintse1Click(Sender: TObject);
begin
   ShowInfoString(Query1.FieldByName('Megjegyz�sek').AsString);
end;

procedure TTelSzamlaFmDlg.BitBtn1Click(Sender: TObject);
begin
   ShowReszletek;
end;

procedure TTelSzamlaFmDlg.BitBtn2Click(Sender: TObject);
var
  APoint: TPoint;
begin
  Application.CreateForm(TUniGraphInfoFormDlg, UniGraphInfoFormDlg);
  UniGraphInfoFormDlg.SetPicture('Telefonsz�mla sz�nmagyar�zat', 2);
  APoint:= BitBtn2.ClientToScreen(Point(0, 0));
  UniGraphInfoFormDlg.Left:= TelSzamlaFmDlg.ScreenToClient(APoint).X + BitBtn2.Width - UniGraphInfoFormDlg.Width;
  UniGraphInfoFormDlg.Top:= TelSzamlaFmDlg.ScreenToClient(APoint).Y - UniGraphInfoFormDlg.Height;
  UniGraphInfoFormDlg.ShowModal;
  UniGraphInfoFormDlg.Free;
end;

procedure TTelSzamlaFmDlg.ShowReszletek;
var
   Telefonszam, Idoszak: string;
begin
    Telefonszam:= Query1.FieldByName('Telefonsz�m').AsString;
    Idoszak:= Query1.FieldByName('Id�szak').AsString;
    Application.CreateForm(TTelSzamlaTetelekFmDlg, TelSzamlaTetelekFmDlg);
    with TelSzamlaTetelekFmDlg do begin
      SzuresUrites;
      Alapstr := Alapstr + ' and El�fizet�s='''+Telefonszam+''' and Id�szak='''+Idoszak+'''';
      SQL_ToltoFo(GetOrderBy(false));
      ShowModal;
      Free;
      end;  // with
  	ModLocate (Query1, FOMEZO, Query1.FieldByName(FOMEZO).AsString );
end;

procedure TTelSzamlaFmDlg.Adatlap(cim, kod : string);
begin
   ShowReszletek;
end;

procedure TTelSzamlaFmDlg.DBGrid2DrawColumnCell(Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
var
   c: integer;
   tetelosztaly: string;
   DoDefaultDraw: boolean;
begin

   // init
   DoDefaultDraw:= True;
   DBGrid2.Canvas.Brush.Color:= clWindow;  // alap�rtelmezett h�tt�rsz�n
   if Column.FieldName = 'T�telOszt�ly' then begin  // csak ezt az oszlopot sz�nezz�k
     // DBGrid2.Canvas.Brush.Bitmap:= nil;
     // DBGrid2.Canvas.Brush.Style:= bsSolid;
     tetelosztaly:=  Query1.FieldByName('T�telOszt�ly').AsString;
     if tetelosztaly <> '' then begin
       DoDefaultDraw:= False;  // nem �runk bele sz�veget, csak sz�n lesz
       if Pos('barna', tetelosztaly)>0 then begin // ha van benne barna, akkor mindenk�pp az lesz
            DBGrid2.Canvas.Brush.Color:= EgyebDlg.GetFuvarosSzin('8');
            DBGrid2.Canvas.FillRect(Rect);
            end
       else if tetelosztaly = 's�rga,' then begin
            DBGrid2.Canvas.Brush.Color:= EgyebDlg.GetFuvarosSzin('1');
            DBGrid2.Canvas.FillRect(Rect);
            end
       else if tetelosztaly = 'piros,' then begin
            DBGrid2.Canvas.Brush.Color:= EgyebDlg.GetFuvarosSzin('2');
            DBGrid2.Canvas.FillRect(Rect);
            end
       else if tetelosztaly = 'narancs,' then begin
            DBGrid2.Canvas.Brush.Color:= EgyebDlg.GetFuvarosSzin('3');
            DBGrid2.Canvas.FillRect(Rect);
            end
       else if tetelosztaly = 'lila,' then begin
            DBGrid2.Canvas.Brush.Color:= EgyebDlg.GetFuvarosSzin('7');
            DBGrid2.Canvas.FillRect(Rect);
            end
       else begin  // piros cs�kos
            // DBGrid2.Canvas.Brush.Style:= bsDiagCross;
            // DBGrid2.Canvas.Brush.Bitmap:=Image1.Picture.Bitmap;
            // DBGrid2.Canvas.Brush.Color:= clMoneyGreen;
            DBGrid2.Canvas.FillRect(Rect);
            DBGrid2.Canvas.StretchDraw(Rect, Image1.Picture.Bitmap) ;
            // DBGrid2.Canvas.Draw(Rect.Left, Rect.Top, bmp) ;
            // DBGrid2.Canvas.TextOut(Rect.Left, Rect.Top, tetelosztaly);
            end;
       end;
     end;  // if ez a mez�

  if DoDefaultDraw then begin
    DBGrid2.DefaultDrawDataCell(Rect, Column.Field, State);
    end;

	inherited DBGrid2DrawColumnCell(Sender, Rect, DataCol, Column, State);
end;

end.

