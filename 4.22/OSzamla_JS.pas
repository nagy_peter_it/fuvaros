unit OSzamla_JS;

interface

uses
  WinTypes, WinProcs, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, QuickRpt, QRExpr, Qrctrls, ExtCtrls, printers, DB, DBTables, Egyeb, J_SQL, Grids, Kozos,
  ADODB, J_DataModule, jpeg, System.Contnrs;

type
  TOSZamla_JSDlg = class(TForm)
    Rep: TQuickRep;
    QRLabel69: TQRLabel;
    QRLabel71: TQRLabel;
    QRBand4: TQRBand;
    QRLabel82: TQRLabel;
	 Query1: TADOQuery;
	 Query2: TADOQuery;
	 QFej: TADOQuery;
	 QSor: TADOQuery;
	 QReszek: TADOQuery;
	 QueryAl: TADOQuery;
    SG: TStringGrid;
    QRBand3: TQRBand;
    QRShape4: TQRShape;
    QRShape19: TQRShape;
    QRShape15: TQRShape;
    QRLSzamla: TQRLabel;
    QRShape1: TQRShape;
    QRShape2: TQRShape;
    QRLabel7: TQRLabel;
    QRLNEV: TQRLabel;
    QRLCIMB: TQRLabel;
    QRLIRSZ: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel9: TQRLabel;
    QRLabel10: TQRLabel;
    QRLabel11: TQRLabel;
    QRLabel12: TQRLabel;
    QRLabel13: TQRLabel;
    QRLabel14: TQRLabel;
    QRShape3: TQRShape;
    QRLabel15: TQRLabel;
    QRLabel16: TQRLabel;
    QRShape5: TQRShape;
    QRLabel17: TQRLabel;
    QRShape6: TQRShape;
    QRLabel18: TQRLabel;
    QRShape7: TQRShape;
    QRLabel19: TQRLabel;
    QRShape9: TQRShape;
    QRShape10: TQRShape;
    QRShape11: TQRShape;
    QRShape12: TQRShape;
    QRLabel21: TQRLabel;
    QRLabel22: TQRLabel;
    QRLabel23: TQRLabel;
    QRLabel24: TQRLabel;
    QRShape13: TQRShape;
    QRLabel25: TQRLabel;
    QRLabel26: TQRLabel;
    QRShape14: TQRShape;
    QRLabel27: TQRLabel;
    QRLabel28: TQRLabel;
    QRShape16: TQRShape;
    QRLabel29: TQRLabel;
    QRShape17: TQRShape;
    QRShape18: TQRShape;
    QRShape26: TQRShape;
    QRLabel55: TQRLabel;
    QRLabel56: TQRLabel;
    QRShape27: TQRShape;
    QRLabel58: TQRLabel;
    QRLabel57: TQRLabel;
    QRLabel64: TQRLabel;
    QRLabel66: TQRLabel;
    QRLabel67: TQRLabel;
    QRLabel70: TQRLabel;
    QRLabel30: TQRLabel;
    QRLabel31: TQRLabel;
    QRLabel74: TQRLabel;
    QRLabel75: TQRLabel;
    QRLabel76: TQRLabel;
    QRLabel77: TQRLabel;
    QRLabel81: TQRLabel;
    QRLabel80: TQRLabel;
    QRLabel85: TQRLabel;
    QRLabel86: TQRLabel;
    QRLabel88: TQRLabel;
    QRLabel89: TQRLabel;
    QRLabel90: TQRLabel;
    QRShape29: TQRShape;
    QRShape30: TQRShape;
    QRShape31: TQRShape;
    QRLabel33: TQRLabel;
    QRLabel87: TQRLabel;
    QRShape32: TQRShape;
    QRLabel91: TQRLabel;
    QRShape35: TQRShape;
    QRShape36: TQRShape;
    QRLabel92: TQRLabel;
    QRLabel93: TQRLabel;
    QRLabel94: TQRLabel;
    QRLabel95: TQRLabel;
    QRLabel96: TQRLabel;
    QRLabel97: TQRLabel;
    QRLabel98: TQRLabel;
    QRLabel99: TQRLabel;
    QRLabel101: TQRLabel;
    QRLabel8A: TQRLabel;
    SZ_BOLD1: TQRLabel;
    SZ_CIM1: TQRLabel;
    SZ_NORM1: TQRLabel;
    QRImage2: TQRImage;
    QRLabel6: TQRLabel;
    QRShape8: TQRShape;
    QRLabel20: TQRLabel;
    QRBand1: TQRBand;
    QRShape24: TQRShape;
    QRShape20: TQRShape;
    QRShape21: TQRShape;
    QRShape22: TQRShape;
    QRShape23: TQRShape;
    QRShape25: TQRShape;
    QRLabel1: TQRLabel;
    QRLabel34: TQRLabel;
    QRLabel35: TQRLabel;
    QRLabel36: TQRLabel;
    QRLabel37: TQRLabel;
    QRLabel61: TQRLabel;
    QRLabel68: TQRLabel;
    QRLabel32: TQRLabel;
    QRLabel72: TQRLabel;
    QRLabel73: TQRLabel;
    QRLabel100: TQRLabel;
    QRBand2: TQRBand;
    QRImage1: TQRImage;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel38: TQRLabel;
    QRLabel39: TQRLabel;
    QRLabel40: TQRLabel;
    QRLabel41: TQRLabel;
    QRLabel42: TQRLabel;
    QRLabel43: TQRLabel;
    QRLabel45: TQRLabel;
    QRLabel46: TQRLabel;
    QRLabel48: TQRLabel;
    LabelO1: TQRLabel;
    QRLabel47: TQRLabel;
    QRLabel49: TQRLabel;
    QRLabel50: TQRLabel;
    QRLabel51: TQRLabel;
    QRLabel52: TQRLabel;
    QRLabel53: TQRLabel;
    QRLabel54: TQRLabel;
    QRLabel59: TQRLabel;
    QRLabel60: TQRLabel;
    QRLabel63: TQRLabel;
    QRLabel62: TQRLabel;
    QRShape33: TQRShape;
    QRShape34: TQRShape;
    QRLabel83: TQRLabel;
    QRLabel84: TQRLabel;
    QRLabel78: TQRLabel;
    QRLabel79: TQRLabel;
    QE1: TQRLabel;
    QE2: TQRLabel;
    QE3: TQRLabel;
    QE4: TQRLabel;
    QE5: TQRLabel;
    QE6: TQRLabel;
    QE7: TQRLabel;
    QRLabel65: TQRLabel;
    QRLabel102: TQRLabel;
    QRLabel44: TQRLabel;
    QRLabel105: TQRLabel;
    QRLabel106: TQRLabel;
    QRLabel107: TQRLabel;
    QRLabel103: TQRLabel;
    QRLabel108: TQRLabel;
    QRLabel141: TQRLabel;
	 procedure FormCreate(Sender: TObject);
	 procedure QRBand3BeforePrint(Sender: TQRCustomBand;
	   var PrintBand: Boolean);
	 function 	Tolt(szkod	: string) : boolean;
	 procedure QRBand1BeforePrint(Sender: TQRCustomBand;
	   var PrintBand: Boolean);
	 procedure QRBand2BeforePrint(Sender: TQRCustomBand;
	   var PrintBand: Boolean);
	 procedure RepBeforePrint(Sender: TCustomQuickRep;
	   var PrintReport: Boolean);
	 procedure QRBand4BeforePrint(Sender: TQRCustomBand;
	   var PrintBand: Boolean);
	 procedure RepNeedData(Sender: TObject; var MoreData: Boolean);
  private
		afaszam			: integer;
		val_oarf		: double;
		oldalszam		: integer;
		sorszam			: integer;
		reszafa			: double;
		resznetto		: double;
		reszeuro		: double;
		kellvnem		: boolean;	// Jelz�, hogy kell-e haszn�lni valutanemet a sz�ml�n
		tenyvnem		: string;	// A sz�mla t�nyleges valutaneme
		vanafa27		: boolean;
		vanafa20		: boolean;
       voltmar         : boolean;
  public
		ujkod			: string;
		JSzamla    		: TJSzamla;
  end;

var
  OSZamla_JSDlg: TOSZamla_JSDlg;

implementation

uses Oszamlabe, Lgauge;

{$R *.DFM}

procedure TOSZamla_JSDlg.FormCreate(Sender: TObject);
begin
	EgyebDlg.SetADOQueryDatabase(Query1);
	EgyebDlg.SetADOQueryDatabase(Query2);
	EgyebDlg.SetADOQueryDatabase(QueryAl);
	EgyebDlg.SetADOQueryDatabase(QReszek);
	EgyebDlg.SetADOQueryDatabase(QFej);
	EgyebDlg.SetADOQueryDatabase(QSor);
	afaszam    	:= 0;
	oldalszam	:= 1;
	vanafa27	:= false;
	vanafa20	:= false;
end;

procedure TOSZamla_JSDlg.QRBand3BeforePrint(Sender: TQRCustomBand;
	   var PrintBand: Boolean);
var
	str     : string;
	str2    : string;
	kodstr	: string;
	orsz	: string;
   skod    : string;
   csopaz  : string;
begin
   if Pos('TEMAN', QFej.FieldByName('SA_FEJL1').AsString ) = 0 then begin
       skod:='';
       if Query_Select('VEVO','V_KOD',QFej.FieldByName('SA_VEVOKOD').AsString,'VE_SKOD')<>'' then begin
           skod:='  ('+Query_Select('VEVO','V_KOD',QFej.FieldByName('SA_VEVOKOD').AsString,'VE_SKOD')+')';
       end;
   end;

	if QFej.FieldByName('SA_PELDANY').AsInteger < EgyebDlg.PeldanySzam then begin
		QrLabel66.Caption := EgyebDlg.Read_SZGrid( '210','102')+
		Format('%5.0d',[QFej.FieldByName('SA_PELDANY').AsInteger + 1])+' / '+
			IntToStr(EgyebDlg.Peldanyszam)+'  ';
	end else begin
		QrLabel66.Caption := EgyebDlg.Read_SZGrid( '210','137');
	end;
	QrLabel81.Caption := EgyebDlg.Read_SZGrid( '210','103')+Format('%5d',[oldalszam]) + '  ';
	Inc(oldalszam);
	{A fejl�c adatainak ki�r�sa}
	QRLSzamla.Caption	:= EgyebDlg.Read_SZGrid( '210','101');
	QrLabel6.Caption 	:= EgyebDlg.Read_SZGrid( '210','104');
	QrLabel7.Caption 	:= EgyebDlg.Read_SZGrid( '210','105');
	QrLabel86.Caption 	:= EgyebDlg.Read_SZGrid( '210','106');
	QrLabel88.Caption 	:= EgyebDlg.Read_SZGrid( '210','107');
	QrLabel89.Caption 	:= EgyebDlg.Read_SZGrid( '210','108');
	QrLabel90.Caption 	:= EgyebDlg.Read_SZGrid( '210','109');
	QrLabel85.Caption 	:= EgyebDlg.Read_SZGrid( '210','110');
	QrLabel26.Caption 	:= EgyebDlg.Read_SZGrid( '210','111');
	QrLabel55.Caption 	:= EgyebDlg.Read_SZGrid( '210','112');
	QrLabel58.Caption 	:= EgyebDlg.Read_SZGrid( '210','113');
	QrLabel33.Caption 	:= EgyebDlg.Read_SZGrid( '210','114');
	QrLabel87.Caption 	:= EgyebDlg.Read_SZGrid( '210','115');
	QrLabel92.Caption 	:= EgyebDlg.Read_SZGrid( '210','116');
	QrLabel93.Caption 	:= EgyebDlg.Read_SZGrid( '210','117');
	QrLabel91.Caption 	:= EgyebDlg.Read_SZGrid( '210','118');
	QrLabel94.Caption 	:= EgyebDlg.Read_SZGrid( '210','119');
	QrLabel95.Caption 	:= EgyebDlg.Read_SZGrid( '210','120');
	QrLabel96.Caption 	:= EgyebDlg.Read_SZGrid( '210','121');
	QrLabel97.Caption 	:= EgyebDlg.Read_SZGrid( '210','122');
	QrLabel98.Caption 	:= EgyebDlg.Read_SZGrid( '210','123');
	QrLabel99.Caption 	:= EgyebDlg.Read_SZGrid( '210','124');
  QRLabel105.Caption:=QRLNEV.Caption+' - '+QRLIRSZ.Caption;

	QrLNEV.Caption 		:= EgyebDlg.Read_SZGrid('CEG', '100')+skod;
	QrLIRSZ.Caption 	:= EgyebDlg.Read_SZGrid('CEG', '102');
	QrLabel8.Caption 	:= EgyebDlg.Read_SZGrid('CEG', '104');
	QrLabel9.Caption 	:= EgyebDlg.Read_SZGrid('CEG', '106');
	QrLabel10.Caption 	:= EgyebDlg.Read_SZGrid('CEG', '107');
	QrLabel8A.Caption 	:= EgyebDlg.Read_SZGrid('CEG', '108');
	QrLabel80.Caption 	:= EgyebDlg.Read_SZGrid('CEG', '109');

	if QFej.FieldByName('SA_FEJL1').AsString <> '' then begin
		{L�tezik fejl�c adat, azt kell betenni a sz�ml�ba}
		QrLNEV.Caption 		:= QFej.FieldByName('SA_FEJL1').AsString+skod;
		QrLIRSZ.Caption 	:= QFej.FieldByName('SA_FEJL2').AsString;
		QrLabel8.Caption 	:= QFej.FieldByName('SA_FEJL3').AsString;
		QrLabel9.Caption 	:= QFej.FieldByName('SA_FEJL4').AsString;
		QrLabel10.Caption 	:= QFej.FieldByName('SA_FEJL5').AsString;
		QrLabel8A.Caption 	:= QFej.FieldByName('SA_FEJL6').AsString;
		QrLabel80.Caption 	:= QFej.FieldByName('SA_FEJL7').AsString;
    QRLabel105.Caption:=QRLNEV.Caption+' - '+QRLIRSZ.Caption;
	end;
	QrLabel11.Caption 	:= QFej.FieldByName('SA_VEVONEV').AsString;
	orsz				:= Query_Select('VEVO', 'V_NEV', QFej.FieldByName('SA_VEVONEV').AsString, 'V_ORSZ');
	QrLabel12.Caption 	:= orsz + '-' + QFej.FieldByName('SA_VEIRSZ').AsString;
	QrLabel13.Caption 	:= QFej.FieldByName('SA_VEVAROS').AsString;
	QrLabel14.Caption 	:= QFej.FieldByName('SA_VECIM').AsString;
	if Pos('.', QrLabel14.Caption) < 1 then begin
		QrLabel14.Caption := QrLabel14.Caption + '.';
	end;
	QrLabel101.Caption 		:= JSzamla.szamlaadoszam;

	csopaz:= Query_Select('VEVO', 'V_KOD', QFej.FieldByName('SA_VEVOKOD').AsString, 'VE_CSOPAZ');
  if csopaz<>'' then
    QRLabel141.Caption:='Csoport az.: '+csopaz
  else
    QRLabel141.Caption:='';

//	QrLabel101.Caption := QFej.FieldByName('SA_VEADO').AsString;
	{A k�lf�ldi fizet�si m�d megkeres�se}
	str	:= QFej.FieldByName('SA_FIMOD').AsString;
	if Query_Run(Query1, 'SELECT * FROM SZOTAR WHERE SZ_FOKOD = ''100'' AND SZ_MENEV = '''+str+''' ',true) then begin
		if Query1.RecordCount > 0 then begin
			kodstr 	:= Query1.FieldByName('SZ_ALKOD').AsString;
			str2	:= EgyebDlg.Read_SZGrid( '200',kodstr);
			if str2 <> '' then begin
				str := str + '/'+str2;
			end;
		end;
	end;
	QrLabel20.Caption 	:= str;
	QrLabel21.Caption 	:= QFej.FieldByName('SA_TEDAT').AsString;
	QrLabel22.Caption 	:= QFej.FieldByName('SA_KIDAT').AsString;
	QrLabel23.Caption 	:= QFej.FieldByName('SA_ESDAT').AsString;
	QrLabel24.Caption 	:= QFej.FieldByName('SA_KOD').AsString;
	QrLabel25.Caption 	:= QFej.FieldByName('SA_MEGJ').AsString;
	QrLabel56.Caption 	:= QFej.FieldByName('SA_JARAT').AsString;
	QrLabel57.Caption 	:= QFej.FieldByName('SA_RSZ').AsString;
	QrLabel63.Caption 	:= QFej.FieldByName('SA_POZICIO').AsString;
end;

function	TOSZamla_JSDlg.Tolt(szkod	: string) : boolean;
var
	jaratstr	: string;
	ii			: integer;
	recno		: integer;
	rendszam	: string;
	pozic		: string;
	sakod		: string;
	sor_szama	: integer;
	JSzamlaSor	: TJSzamlaSor;
	k			: integer;
begin
	Result	:= false;
	ujkod		:= szkod;
	{A t�bl�zat felt�lt�se}
	Query_Run(QFej, 'SELECT * FROM SZFEJ2 WHERE SA_UJKOD = '+ujkod, true);
	if QFej.RecordCount < 1 then begin
		NoticeKi('Hi�nyz� sz�mlarekord : ('+ujkod+')');
		ujkod	:= '';
		Exit;
	end;

	JSzamla		:= TJSzamla.Create(Self);
	JSzamla.FillOsszSzamla(ujkod);
	vanafa27	:= ( JSzamla.Afasorok[JSzamla.GetAfaSorszam('106')] as TJAfasor ).afaertek <> 0;
//	lista  := TStringList.Create;
//	JSzamla.WriteToList(lista);
//	lista.SaveToFile(EgyebDlg.TempLIst + 'szamlalista.txt');
//	lista.Free;


	Query_Run(QReszek, 'SELECT DISTINCT S2_UJKOD,S2_S2SOR FROM SZSOR2 WHERE S2_S2KOD = '+ujkod+' ORDER BY S2_S2SOR', true);
	SG.RowCount := 1;
	Application.CreateForm(TFormGaugeDlg, FormGaugeDlg);
	FormGaugeDlg.GaugeNyit('Sz�mla adatainak �sszegy�jt�se... ' , 'K�d : '+
		QFej.FieldByName('SA_UJKOD').AsString+' ('+QFej.FieldByName('SA_KOD').AsString+')   P�ld�ny : '+
		IntToStr(QFej.FieldByName('SA_PELDANY').AsInteger+1), false);
	recno		:= 1;
	while not QReszek.eof do begin
		sakod	:= Query_Select('SZFEJ', 'SA_UJKOD', QReszek.FieldByName('S2_UJKOD').AsString, 'SA_KOD');
		if sakod <> QFej.FieldByName('SA_KOD').AsString then begin
			NoticeKi('Elt�r� sz�mlasz�m  : ' + QReszek.FieldByName('S2_UJKOD').AsString +
			'('+sakod +' <> '+ QFej.FieldByName('SA_KOD').AsString + ')');
		end;
		rendszam  	:= '';
		pozic		:= '';
		FormGaugeDlg.Pozicio ( ( recno * 100 ) div qreszek.RecordCount ) ;
		Inc(recno);
		Query_Run(Query2, 'SELECT * FROM SZFEJ WHERE SA_UJKOD = '+ QReszek.FieldByName('S2_UJKOD').AsString, true);
		if Query2.RecordCount > 0 then begin
			pozic		:= Query2.FieldByName('SA_POZICIO').AsString;
			{A j�ratsz�m be�r�sa -> A j�ratsz�m alapj�n megkeress�k a bet�jelet is }
			if Query_Run(QueryAl, 'SELECT * FROM JARAT WHERE JA_ALKOD = '+
				IntToStr(Round(StringSzam(Query2.FieldByName('SA_JARAT').AsString)))+
               ' AND JA_JKEZD LIKE '''+ copy( Query2.FieldByName('SA_DATUM').AsString ,1,4) +'%'' ',true) then begin
				jaratstr	:= ' (' +QueryAl.FieldByName('JA_ORSZ').AsString + ' - ' + Query2.FieldByName('SA_JARAT').AsString + ')';
			end else begin
				jaratstr	:= ' (' +Query2.FieldByName('SA_JARAT').AsString + ')';
			end;
			rendszam		:= Query2.FieldByName('SA_RSZ').AsString;
		end;
		Query_Run(QSor, 'SELECT * FROM SZSOR WHERE SS_UJKOD = '+ QReszek.FieldByName('S2_UJKOD').AsString+' ORDER BY SS_SOR', true);
		while not QSor.EOF do begin
			sor_szama	:= 0;
			for k 		:= 0 to JSzamla.Szamlasorok.Count - 1 do begin
				if ( (JSzamla.SzamlaSorok[k] as TJszamlaSor).regi_ujkod     = StrToIntDef(Qsor.FieldByName('SS_UJKOD').AsString, 0) ) and
					 ((JSzamla.SzamlaSorok[k] as TJszamlaSor).regi_sorszam  = StrToIntDef(QSor.FieldByName('SS_SOR').AsString, 0)) then begin
					sor_szama	:= k;
				end;
			end;
			if sor_szama >= 0 then begin
				JSzamlasor	:= (JSzamla.SzamlaSorok[sor_szama] as TJszamlaSor);
			end else begin
				NoticeKi('�rv�nytelen sorsz�m : '+QSor.FieldByName('SS_SOR').AsString);
				Exit;
			end;

			if SG.Cells[0,0] <> '' then begin
				SG.RowCount := SG.RowCount + 1;
			end;
			SG.Cells[ 0,SG.RowCount - 1] := IntToStr(JSzamlasor.regi_sorszam);
			SG.Cells[ 1,SG.RowCount - 1] := JSzamlasor.termeknev;
			SG.Cells[ 2,SG.RowCount - 1] := JSzamlasor.itjszj;
			SG.Cells[ 3,SG.RowCount - 1] := JSzamlasor.sormegjegyzes;	// Az alsz�mla megjegyz�s sora
			SG.Cells[ 4,SG.RowCount - 1] := JSzamlasor.sorteljesites;
			SG.Cells[ 5,SG.RowCount - 1] := JSzamlasor.leiras;
			SG.Cells[ 6,SG.RowCount - 1] := Format('%f', [JSzamlasor.mennyiseg]);
			SG.Cells[ 7,SG.RowCount - 1] := JSzamlasor.megyseg;
			SG.Cells[ 8,SG.RowCount - 1] := Format('%f', [JSzamlasor.egysegar]);
			SG.Cells[ 9,SG.RowCount - 1] := JSzamlasor.afakod;
			SG.Cells[10,SG.RowCount - 1] := JSzamlasor.afastring;
			SG.Cells[11,SG.RowCount - 1] := JSzamlasor.valutanem;
			SG.Cells[12,SG.RowCount - 1] := Format('%f', [JSzamlasor.valutaarfoly]);
			SG.Cells[13,SG.RowCount - 1] := Format('%f', [JSzamlasor.valutaegyar]);
			SG.Cells[14,SG.RowCount - 1] := Format('%f', [JSzamlasor.eurarfertek]);
			SG.Cells[15,SG.RowCount - 1] := JSzamlasor.eurarfdatum;
{        	SG.Cells[16,SG.RowCount - 1] := QSor.FieldByName('SS_ARERT').AsString;}
			if pozic <> '' then begin
				if Pos(pozic, JSzamlasor.sormegjegyzes + JSzamlasor.sorteljesites) > 0 then begin
					// M�r l�tezik a poz�ci� a le�r�sban
					pozic	:= '';
				end;
			end;
			SG.Cells[16,SG.RowCount - 1] := pozic;
			SG.Cells[17,SG.RowCount - 1] := jaratstr;
			SG.Cells[18,SG.RowCount - 1] := JSzamlasor.kulfoldimegyseg;
			SG.Cells[19,SG.RowCount - 1] := rendszam;
			SG.Cells[20,SG.RowCount - 1] := Format('%.4f', [JSzamlasor.sornetto]);
			SG.Cells[21,SG.RowCount - 1] := Format('%.4f', [JSzamlasor.sorafa]);
			SG.Cells[22,SG.RowCount - 1] := Format('%.4f', [JSzamlasor.sorbrutto]);
			SG.Cells[23,SG.RowCount - 1] := Format('%.4f', [JSzamlasor.sorneteur]);
			SG.Cells[24,SG.RowCount - 1] := Format('%.4f', [JSzamlasor.sorbruteur]);
			QSor.Next;
		end;
		{Hozz� kell f�zni az �sszesen sort }
		SG.RowCount 					:= SG.RowCount + 1;
		SG.Cells[ 0,SG.RowCount - 1] 	:= '999';
		SG.Cells[ 1,SG.RowCount - 1] 	:= 'R�sz�sszeg : ';
		for ii := 2 to 29 do begin
			SG.Cells[ ii,SG.RowCount - 1] := '';
		end;
		QReszek.Next;
	end;
	FormGaugeDlg.Destroy;
	Result		:= true;
	tenyvnem	:= GetSzamlaValuta(szkod);
	if ( ( tenyvnem	= 'HUF' ) or ( tenyvnem	= '' ) ) then begin
		kellvnem	:= false;
	end else begin
		kellvnem	:= true;
	end;
end;

procedure TOSZamla_JSDlg.QRBand1BeforePrint(Sender: TQRCustomBand;
	   var PrintBand: Boolean);
var
	sorafa		: double;
	ertek		: double;
	eurertek	: double;
  l1:integer;
begin
	QrLabel1.Width				:= 313;
	QrLabel1.Font.Style			:= [];
	QrLabel61.Font.Style		:= [];
	QrLabel32.Font.Style		:= [];
	QrLabel35.Font.Style		:= [];
	QrLabel72.Font.Style		:= [];
	QrLabel35.Top 				:= 17;
	QrLabel32.Alignment	:= taCenter;
	if SG.Cells[0,sorszam] = '999' then begin
		QrLabel100.Caption 		:= '';
		QrLabel32.Caption 		:= '';
		QrLabel37.Caption 		:= '';
		QrLabel68.Caption 		:= '';
		QrLabel61.Caption 		:= '';
		QrLabel34.Caption 		:= '';
		QrLabel36.Caption 		:= '';
		QrLabel35.Caption 		:= '';
		QrLabel73.Caption 		:= '';
		QrLabel1.Font.Style		:= [fsBold];
		QrLabel61.Font.Style	:= [fsBold];
		QrLabel32.Font.Style	:= [fsBold];
		QrLabel35.Font.Style	:= [fsBold];
		QrLabel72.Font.Style	:= [fsBold];
		QrLabel35.Top 			:= QrLabel1.Top;
		QrLabel1.Caption 		:= SG.Cells[1,sorszam];
		QrLabel61.Caption 		:= Format('%.0f',[resznetto]);
//		QrLabel32.Alignment		:= taRightJustify;
		QrLabel32.Caption 		:= Format('%.0f',[reszafa]);
		QrLabel35.Caption 		:= Format('%.0f',[resznetto + reszafa]);
		QrLabel72.Caption 		:= Format('%f EUR',[ reszeuro ]);
		reszafa					:= 0;
		resznetto				:= 0;
		reszeuro				:= 0;
		Inc(sorszam);
		Exit;
	end;
  QRLabel1.Font.Size:=9;
  QRLabel1.Width:=270;
	if SG.Cells[2,sorszam] = '' then begin
		QrLabel1.Caption := SG.Cells[1,sorszam];
	end else begin
		QrLabel1.Caption := SG.Cells[2,sorszam]+', '+SG.Cells[1,sorszam];
	end;
	if SG.Cells[0,sorszam] = '1' then begin
		{Az els� sorokhoz fel kell hozni a viszonylatot + d�tumot}
    QRLabel1.Font.Size:=9;
		QrLabel1.Font.Style		:= [fsBold];
    QRLabel1.AutoSize:=True;
		QrLabel1.Caption 		:= QrLabel1.Caption +':' +  SG.Cells[3,sorszam] +' '+SG.Cells[16,sorszam]+ ', ' + SG.Cells[4,sorszam] + SG.Cells[17,sorszam]+ '  ' + SG.Cells[19,sorszam];
    l1:=QRLabel1.Width;
    while l1>QrShape13.Width do
    begin
      QRLabel1.Font.Size:=QRLabel1.Font.Size-1;
      QRLabel1.Refresh;
      l1:=QRLabel1.Width;
    end;
    QRLabel1.AutoSize:=False;
		QrLabel1.Width			:= QrShape13.Width;
	 //	QrLabel1.Caption 		:= QrLabel1.Caption +':' +  SG.Cells[3,sorszam] +' '+SG.Cells[16,sorszam]+ ', ' + SG.Cells[4,sorszam] + SG.Cells[17,sorszam]+ '  ' + SG.Cells[19,sorszam];
	 //	QrLabel1.Font.Style		:= [fsBold];
	end;
	if SG.Cells[0,sorszam] = '99' then begin
		{Az els� sorokhoz fel kell hozni a viszonylatot + d�tumot}
		QrLabel1.Width			:= 680;
	end;
	{A k�lf�ldi le�r�s megjelen�t�se}
	QrLabel100.Caption := SG.Cells[5,sorszam];
	if StringSzam(SG.Cells[8,sorszam]) = 0 then begin
		QrLabel32.Caption := '';
		QrLabel37.Caption := '';
		QrLabel68.Caption := '';
		QrLabel61.Caption := '';
		QrLabel34.Caption := '';
		QrLabel36.Caption := '';
		QrLabel35.Caption := '';
		QrLabel72.Caption := '';
		QrLabel73.Caption := '';
  end else begin
		QrLabel32.Caption := EgyebDlg.Read_SZGrid( '101',SG.Cells[9,sorszam]);
		QrLabel37.Caption := SG.Cells[6,sorszam];
		QrLabel68.Caption := SG.Cells[7,sorszam];
		if SG.Cells[18,sorszam] <> '' then begin
			QrLabel68.Caption := QrLabel68.Caption + '/'+SG.Cells[18,sorszam];
		end;
		QrLabel61.Caption := Format('%.0f',[StringSzam(SG.Cells[8,sorszam])]);
		ertek				:= StringSzam(SG.Cells[20,sorszam]);
		sorafa				:= StringSzam(SG.Cells[21,sorszam]);
		QrLabel34.Caption 	:= Format('%.0f',[ertek]);
		QrLabel36.Caption 	:= Format('%.0f',[sorafa]);
		QrLabel32.Caption 	:= EgyebDlg.Read_SZGrid( '101', SG.Cells[9,sorszam] );
		QrLabel35.Caption 	:= Format('%.0f',[StringSzam(SG.Cells[22,sorszam])]);
		reszafa				:= reszafa + sorafa;
		resznetto			:= resznetto + ertek;
		eurertek			:= StringSzam(SG.Cells[23,sorszam]);
		QrLabel72.Caption 	:= Format('%f %3s',[ eurertek, SG.Cells[11,sorszam]]);
		QrLabel73.Caption 	:= Format('%9.4f ',[StringSzam(SG.Cells[12,sorszam])]);
		reszeuro			:= reszeuro + eurertek;
	end;
	Inc(sorszam);
end;

procedure TOSZamla_JSDlg.QRBand2BeforePrint(Sender: TQRCustomBand;
	   var PrintBand: Boolean);
begin
	{A k�lf�ldi cimk�k beolvas�sa}
	QrLabel62.Caption := EgyebDlg.Read_SZGrid( '210','125');
	QrLabel38.Caption := EgyebDlg.Read_SZGrid( '210','131');
	QrLabel39.Caption := EgyebDlg.Read_SZGrid( '210','126');
	QrLabel44.Caption := EgyebDlg.Read_SZGrid( '210','127');
	QrLabel83.Caption := EgyebDlg.Read_SZGrid( '210','128');
	QrLabel43.Caption := EgyebDlg.Read_SZGrid( '210','129');
	QrLabel42.Caption := EgyebDlg.Read_SZGrid( '210','130');
	QrLabel40.Caption := EgyebDlg.Read_SZGrid( '210','132');
	QrLabel41.Caption := EgyebDlg.Read_SZGrid( '210','133');
	QrLabel45.Caption := EgyebDlg.Read_SZGrid( '210','134');
	QrLabel3.Caption 	:= EgyebDlg.Read_SZGrid( '210','135');
	if vanafa27 then begin
		QrLabel42.Caption := '27'+copy(QrLabel42.Caption,3,99);
		QrLabel41.Caption := '27'+copy(QrLabel41.Caption,3,99);
	end;
//	if QFej.FieldByName('SA_KIDAT').AsString >= '2009.07.01.' then begin
//		QrLabel42.Caption := '25'+copy(QrLabel42.Caption,3,99);
//		QrLabel41.Caption := '25'+copy(QrLabel41.Caption,3,99);
//	end;
	if QFej.FieldByName('SA_KIDAT').AsString >= '2012.01.01.' then begin
		QrLabel42.Caption := '27'+copy(QrLabel42.Caption,3,99);
		QrLabel41.Caption := '27'+copy(QrLabel41.Caption,3,99);
	end;
	if vanafa20 then begin
		QrLabel42.Caption := '20'+copy(QrLabel42.Caption,3,99);
		QrLabel41.Caption := '20'+copy(QrLabel41.Caption,3,99);
	end;

	LabelO1.Caption 	:= Format('%.0f Ft',[JSzamla.ossz_netto]);
	QrLabel47.Caption 	:= Format('%.0f Ft',[( JSzamla.Afasorok[JSzamla.GetAfaSorszam('102')] as TJAfasor ).afaertek]);
	QrLabel49.Caption 	:= Format('%.0f Ft',[( JSzamla.Afasorok[JSzamla.GetAfaSorszam('110')] as TJAfasor ).afaertek]);
	QrLabel50.Caption 	:= Format('%.0f Ft',[JSzamla.ossz_brutto]);
	QrLabel51.Caption 	:= Format('%.0f Ft',[( JSzamla.Afasorok[JSzamla.GetAfaSorszam('102')] as TJAfasor ).afaalap]);
	QrLabel52.Caption 	:= Format('%.0f Ft',[( JSzamla.Afasorok[JSzamla.GetAfaSorszam('110')] as TJAfasor ).afaalap]);
	QrLabel53.Caption 	:= Format('%.0f Ft',[( JSzamla.Afasorok[JSzamla.GetAfaSorszam('103')] as TJAfasor ).afaalap]);
	QrLabel54.Caption 	:= Format('%.0f Ft',[( JSzamla.Afasorok[JSzamla.GetAfaSorszam('104')] as TJAfasor ).afaalap]);
	QrLabel84.Caption 	:= Format('%.0f Ft',[( JSzamla.Afasorok[JSzamla.GetAfaSorszam('105')] as TJAfasor ).afaalap]);
	QrLabel48.Caption 	:= SzamText(Format('%.0f',[JSzamla.ossz_brutto]))+' forint / 00';
	if vanafa27 then begin
		QrLabel49.Caption := Format('%.0f Ft',[( JSzamla.Afasorok[JSzamla.GetAfaSorszam('106')] as TJAfasor ).afaertek]);
		QrLabel50.Caption := Format('%.0f Ft',[JSzamla.ossz_brutto]);
		QrLabel52.Caption := Format('%.0f Ft',[( JSzamla.Afasorok[JSzamla.GetAfaSorszam('106')] as TJAfasor ).afaalap]);
		QrLabel48.Caption := SzamText(Format('%.0f',[JSzamla.ossz_brutto]))+' forint / 00';
	end;
	QrLabel4.Caption 	:= QFej.FieldByName('SA_MELL1').AsString;
	QrLabel5.Caption 	:= QFej.FieldByName('SA_MELL2').AsString;
	QrLabel59.Caption 	:= QFej.FieldByName('SA_MELL3').AsString;
	QrLabel60.Caption 	:= QFej.FieldByName('SA_MELL4').AsString;
	QrLabel65.Caption 	:= QFej.FieldByName('SA_MELL5').AsString;

	{A valut�s �rt�k ki�r�sa}
	if JSzamla.ossz_bruttoeur <> 0 then begin
		QrLabel78.Caption 	:= EgyebDlg.Read_SZGrid( '210','136')+' : ';
		QrLabel79.Caption 	:= Format('%.2f EUR',[JSzamla.ossz_bruttoeur]);
		QrLabel102.Caption 	:= SzamText(Format('%.0f',[JSzamla.ossz_bruttoeur]))+' eur� / 00';
	end else begin
		QrLabel78.Caption 	:= '';
		QrLabel79.Caption 	:= '';
		QrLabel102.Caption 	:= '';
	end;
	QE1.Caption := '('+Format('%.2f',[( JSzamla.Afasorok[JSzamla.GetAfaSorszam('103')] as TJAfasor ).afaaleur])+')';
	QE2.Caption := '('+Format('%.2f',[( JSzamla.Afasorok[JSzamla.GetAfaSorszam('104')] as TJAfasor ).afaaleur])+')';
	QE3.Caption := '('+Format('%.2f',[( JSzamla.Afasorok[JSzamla.GetAfaSorszam('105')] as TJAfasor ).afaaleur])+')';
	QE4.Caption := '('+Format('%.2f',[( JSzamla.Afasorok[JSzamla.GetAfaSorszam('102')] as TJAfasor ).afaaleur])+')';
	QE5.Caption := '('+Format('%.2f',[( JSzamla.Afasorok[JSzamla.GetAfaSorszam('110')] as TJAfasor ).afaaleur])+')';
	QE6.Caption := '('+Format('%.2f',[( JSzamla.Afasorok[JSzamla.GetAfaSorszam('102')] as TJAfasor ).afaerteur])+')';
	QE7.Caption := '('+Format('%.2f',[( JSzamla.Afasorok[JSzamla.GetAfaSorszam('110')] as TJAfasor ).afaerteur])+')';
	if vanafa27 then begin
		QE5.Caption := '('+Format('%.2f',[( JSzamla.Afasorok[JSzamla.GetAfaSorszam('106')] as TJAfasor ).afaaleur])+')';
		QE7.Caption := '('+Format('%.2f',[( JSzamla.Afasorok[JSzamla.GetAfaSorszam('106')] as TJAfasor ).afaerteur])+')';
	end;

end;

procedure TOSZamla_JSDlg.RepBeforePrint(Sender: TCustomQuickRep;
	   var PrintReport: Boolean);
begin
	val_oarf		:= 0;
	sorszam			:= 0;
	reszafa			:= 0;
	resznetto		:= 0;
	reszeuro		:= 0;
	oldalszam		:= 1;
   voltmar         := false;

  if EgyebDlg.nemetfejlec then // n�met fejl�c
  begin
    QRLabel50.Font.Style:=[];
    QRLabel79.Font.Style:=[fsBold];
    QRLabel45.Font.Style:=[];
    QRLabel78.Font.Style:=[fsBold];
  end
  else
  begin
    QRLabel50.Font.Style:=[fsBold];
    QRLabel79.Font.Style:=[];
    QRLabel45.Font.Style:=[fsBold];
    QRLabel78.Font.Style:=[];
  end;
end;

procedure TOSZamla_JSDlg.QRBand4BeforePrint(Sender: TQRCustomBand;
	   var PrintBand: Boolean);
begin
  QrLabel82.Caption := EgyebDlg.Read_SZGrid( '210','140')+' DMK COMP Kft., Tatab�nya';
end;

procedure TOSZamla_JSDlg.RepNeedData(Sender: TObject; var MoreData: Boolean);
begin
	MoreData	:= sorszam < SG.RowCount;
end;

end.
