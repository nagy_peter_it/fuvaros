unit TelKiegFm;

interface

uses
	SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
	StdCtrls, Forms, ExtCtrls, Dialogs, J_FOFORMUJ, Vcl.Menus, Vcl.Buttons;

type
	TTelKiegFormDlg = class(TJ_FOFORMUJDLG)
    BitBtn2: TBitBtn;
	  procedure FormCreate(Sender: TObject);override;
	 procedure Adatlap(cim, kod : string);override;
	 procedure ButtonModClick(Sender: TObject);override;
    procedure BitBtn2Click(Sender: TObject);
	end;

var
	TelKiegFormDlg : TTelKiegFormDlg;

implementation

uses
	Egyeb, Telefonbe2, J_SQL, TelefonCsere, Clipbrd, Kozos, TelKiegbe, TelKiegTomegesBe;

{$R *.DFM}

procedure TTelKiegFormDlg.FormCreate(Sender: TObject);
var
  S: string;
begin
	Inherited FormCreate(Sender);
  BitBtn2.Parent		:= PanelBottom;

  S:= 'select TI_ID, sz1.SZ_MENEV KIEGESZITO, sz2.SZ_MENEV STATUSZ, isnull(DO_NAME, '''') DOLGOZO, TI_MEGJE MEGJEGYZES, '+
    ' TI_STATUSKOD SK, TI_DOKOD DK from '+
    ' TELKIEG left outer join SZOTAR sz1 on sz1.SZ_ALKOD=TI_TIPUSKOD and sz1.SZ_FOKOD = ''153'' '+
    ' left outer join DOLGOZO on TI_DOKOD = DO_KOD '+
    ' left outer join SZOTAR sz2 on sz2.SZ_ALKOD=TI_STATUSKOD and sz2.SZ_FOKOD = ''144'' ';
  S:= 'select * from ('+S+ ') a where 1=1';  // a sz�r�sekhez be kell "csomagolni"

	AddSzuromezoRovid('DOLGOZO',  '', '1');
  AddSzuromezoRovid('KIEGESZITO',  '');
  AddSzuromezoRovid('STATUSZ',  '');
  AddSzuromezoRovid('MEGJEGYZES',  '', '1');
	FelTolto('�j telefon tartoz�k felvitele ', 'Telefon tartoz�k adatok m�dos�t�sa ',
			'Telefon tartoz�kok list�ja', 'TI_ID', S, 'TELKIEG', QUERY_KOZOS_TAG );
	nevmezo	:= 'TI_ID';
	width	:= DLG_WIDTH;
	height	:= DLG_HEIGHT;
  kellujures:= False;

  // uresszures:= True;
  ButtonTor.Enabled :=True;
end;

procedure TTelKiegFormDlg.Adatlap(cim, kod : string);
begin
	Application.CreateForm(TTelKiegbeDlg, AlForm);
	Inherited Adatlap(cim, kod );
end;

procedure TTelKiegFormDlg.BitBtn2Click(Sender: TObject);
begin
  Application.CreateForm(TTelKiegTomegesBeDlg, TelKiegTomegesBeDlg);
  TelKiegTomegesBeDlg.ShowModal;
  TelKiegTomegesBeDlg.Release;
  SQL_ToltoFo(GetOrderBy(true));
end;

procedure TTelKiegFormDlg.ButtonModClick(Sender: TObject);
var
	tulaj	: string;
begin
	if not voltalt then begin
 		voltalt 	:= true;
 		Exit;
 	end;
	if ButtonMod.Enabled then begin
  		Adatlap(MODOSIT,Query1.FieldByName(FOMEZO).AsString);
      end;
end;

end.

