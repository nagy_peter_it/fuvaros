�
 TCALENDARDLG 0�	  TPF0TCalendarDlgCalendarDlgLeftTTop� BorderIconsbiSystemMenu BorderStylebsSingleCaption   Dátum választásClientHeightClientWidthColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameSystem
Font.Style 
KeyPreview	OldCreateOrder	PositionpoScreenCenterShowHint	OnCreate
FormCreate	OnDestroyFormDestroy	OnKeyDownFormKeyDownPixelsPerInch`
TextHeight TPanelPanel2Left TopPWidthHeight� AlignalClientTabOrder TStringGridHoGridLeftTopWidthHeight� BorderStylebsNoneColor	clBtnFaceColCountDefaultColWidthDefaultRowHeight	FixedCols RowCountOptionsgoFixedVertLinegoFixedHorzLine
goVertLine
goHorzLine 
ScrollBarsssNoneTabOrder OnClickHoGridClick
OnDblClickHoGridDblClick
OnDrawCellHoGridDrawCellOnMouseMoveHoGridMouseMove   TPanelPanel1Left Top WidthHeightPAlignalTopTabOrder  TSpeedButtonSpeedButton1LeftTopWidthHeightHint2   Előző hónap | Az előző hónap megjelenítéseCaption<OnClickSpeedButton1Click  TSpeedButtonSpeedButton2Left� TopWidthHeightHint9   Következő hónap | A következő hónap megjelenítéseCaption>OnClickSpeedButton2Click  TSpeedButtonSpeedButton3LeftTopWidthHeightHint,   Előző év | Az előző év megjelenítéseCaption<<OnClickSpeedButton3Click  TSpeedButtonSpeedButton4Left� TopWidthHeightHint3   Következő év | A következő év megjelenítéseCaption>>OnClickSpeedButton4Click  TLabelLabel1LeftTopWidthHeightAlignalTop	AlignmenttaCenterAutoSizeCaptionLabel1ColorclInactiveBorderParentColor  TSpeedButtonSpeedButton5Left� TopWidth'HeightHint#   Mai nap | A mai nap megjelenítéseCaptionMaOnClickSpeedButton5Click  	TSpinEdit	SpinEdit1Left7TopWidth7HeightHint   Évszám | Évszám választásFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameSystem
Font.StylefsBold 	MaxLengthMaxValue MinValue 
ParentFontTabOrder Value�OnChangeSpinEdit1Change  	TComboBoxHoBoxLeftrTopWidth@HeightHint   Hónap | Hónap választásStylecsDropDownListDropDownCountTabOrderOnChangeHoBoxChangeItems.Stringsjanfebr   márc   ápr   májjun   júlaugszeptoktnovdec    
TStatusBar
StatusBar1Left Top� WidthHeightPanelsTextHintekWidthX 	AlignmenttaCenterTextOraWidthd  
SimpleText.   