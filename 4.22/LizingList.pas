unit LizingList;

interface

uses
  WinTypes, WinProcs, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, QuickRpt, QRExpr, Qrctrls, ExtCtrls, printers, DB, DBTables, Egyeb, Forgalom, J_SQL,
  Grids, Kozos, Kozos_Local, ADODB ;

type
  TLizinglistDlg = class(TForm)
    Rep: TQuickRep;
    QRBand3: TQRBand;
    QRLSzamla: TQRLabel;
    QRBand1: TQRBand;
    QRLabel15: TQRLabel;
    QRLabel16: TQRLabel;
    QRLabel17: TQRLabel;
    QRLabel6: TQRLabel;
    QRSysData1: TQRSysData;
    QRLabel7: TQRLabel;
    QRSysData4: TQRSysData;
    QRLabel1: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel3: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel12: TQRLabel;
    QRLabel13: TQRLabel;
	 QRLabel14: TQRLabel;
    QRDBText4: TQRDBText;
    QRDBText6: TQRDBText;
	 QRLabel2: TQRLabel;
    QRLabel5: TQRLabel;
    QRSysData2: TQRSysData;
    QRBand2: TQRBand;
    QRLabel21: TQRLabel;
    QL3: TQRLabel;
    QRDBText2: TQRDBText;
    QRDBText3: TQRDBText;
    QRLabel10: TQRLabel;
    QRLabel23: TQRLabel;
    Query1: TADOQuery;
    Query2: TADOQuery;
    QRLabel9: TQRLabel;
    QL4: TQRLabel;
    QRLabel19: TQRLabel;
    QRLabel20: TQRLabel;
    QRLabel22: TQRLabel;
    QRLabel24: TQRLabel;
    QRLabel25: TQRLabel;
    QRLabel26: TQRLabel;
    QRLabel27: TQRLabel;
    QRLabel28: TQRLabel;
    QRShape1: TQRShape;
    QRShape2: TQRShape;
    QRLabel11: TQRLabel;
	 QRLabel18: TQRLabel;
    QRLabel29: TQRLabel;
    QRDBText1: TQRDBText;
    QRLabel30: TQRLabel;
    QRLabel31: TQRLabel;
    procedure FormCreate(Sender: TObject);
	 procedure	Tolt(rsz : string );
	 procedure QRBand1BeforePrint(Sender: TQRCustomBand;
	   var PrintBand: Boolean);
	 procedure RepBeforePrint(Sender: TCustomQuickRep;
	   var PrintReport: Boolean);
	 procedure QRBand2BeforePrint(Sender: TQRCustomBand;
	   var PrintBand: Boolean);
  private
	  ossz		: double;
  public
	  vanadat	: boolean;
  end;

var
  LizinglistDlg: TLizinglistDlg;

implementation

{$R *.DFM}

procedure TLizinglistDlg.FormCreate(Sender: TObject);
begin
	EgyebDlg.SetADOQueryDatabase(Query1);
	EgyebDlg.SetADOQueryDatabase(Query2);
end;

procedure	TLizinglistDlg.Tolt(rsz : string );
begin
	{A fejl�c kit�lt�se}
	Query_Run(Query2, 'SELECT * FROM GEPKOCSI left outer join SZOTAR on SZ_FOKOD=''240'' AND GK_LIPER = SZ_ALKOD WHERE GK_RESZ = '''+rsz+''' ');
	if Query2.RecordCount > 0 then begin
		QrLabel16.Caption	:= rsz + ' ' +Query2.FieldByName('GK_TIPUS').AsString;
		QrLabel17.Caption	:= Query2.FieldByName('GK_LIDAT').AsString;
		QrLabel5.Caption	:= Query2.FieldByName('GK_LIHON').AsString + ' ' + 'h�nap';
		QrLabel24.Caption	:= EgyebDlg.Read_SZGrid( '380', Query2.FieldByName('GK_LICEG').AsString);
		QrLabel20.Caption	:= Query2.FieldByName('GK_LIOSZ').AsString + ' ' + Query2.FieldByName('GK_LINEM').AsString+
      '/'+Query2.FieldByName('SZ_LEIRO').AsString; // a peri�dus megnevez�se, r�vid�tve
		QrLabel28.Caption	:= Query2.FieldByName('GK_LISZE').AsString;
		QrLabel26.Caption	:= Query2.FieldByName('GK_LIMEG').AsString;
		QrLabel11.Caption	:= Query2.FieldByName('GK_LINEM').AsString;
		QrLabel18.Caption	:= Query2.FieldByName('GK_LINEM').AsString;
		QrLabel31.Caption	:= Query2.FieldByName('GK_VETAR').AsString + ' ' + Query2.FieldByName('GK_VETVN').AsString;
	end;
	Query_Run(Query1, 'SELECT * FROM LIZING WHERE LZ_RENSZ = '''+rsz+''' ORDER BY LZ_DATUM ');
	vanadat	:= Query1.RecordCount > 0;
end;

procedure TLizinglistDlg.QRBand1BeforePrint(Sender: TQRCustomBand;
	   var PrintBand: Boolean);
begin
  ossz	:= ossz + GetValueInOtherCur(StringSzam(Query1.FieldByName('LZ_ERTEK').AsString),
		Query1.FieldByName('LZ_BEDAT').AsString, Query1.FieldByName('LZ_VANEM').AsString,
		Query2.FieldByName('GK_LINEM').AsString );
end;

procedure TLizinglistDlg.RepBeforePrint(Sender: TCustomQuickRep;
	   var PrintReport: Boolean);
begin
	ossz		:= 0;
end;

procedure TLizinglistDlg.QRBand2BeforePrint(Sender: TQRCustomBand;
	   var PrintBand: Boolean);
var
	str,str2,rensz,dat	: string;
  hpervissza: integer;
begin
	QL3.Caption := Format('%.2f', [ossz]);
	str:=	GetLastLizingDate( Query2.FieldByName('GK_LIDAT').AsString, Query2.FieldByName('GK_LIHON').AsString, Query2.FieldByName('GK_LIPER').AsInteger);

  // hho:=GetRestLizingMonth(copy(str,1,4)+'.'+copy(str,5,2)+'.',Query2.FieldByName('GK_LIDAT').AsString);
  hpervissza:=GetRestLizingPeriod(copy(str,1,4)+'.'+copy(str,5,2)+'.', Query2.FieldByName('GK_LIDAT').AsString, Query2.FieldByName('GK_LIPER').AsInteger);

  rensz:=Query2.FieldByName('GK_RESZ').AsString;
  str2:=copy(EgyebDlg.MaiDatum,1,8);
  dat:=Query_Select2('LIZING','LZ_RENSZ','SUBSTRING(LZ_DATUM,1,8)',rensz,str2,'LZ_DATUM');
  if (hpervissza>0) and (dat<>EmptyStr) then
      hpervissza:=hpervissza-1;

	QL4.Caption := Format('%.2f',[hpervissza*StringSzam(Query2.FieldByName('GK_LIOSZ').AsString)]);
end;

end.
