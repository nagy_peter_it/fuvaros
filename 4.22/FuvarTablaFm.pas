unit FuvartablaFm;

interface

uses
	SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
	StdCtrls, Forms, DBCtrls, DB, DBGrids, DBTables, Grids, ExtCtrls, Buttons,
	ShellApi, Dialogs, J_FOFORMUJ, ADODB, Mask, Menus, Variants;

type
	TFuvartablaFmDlg = class(TJ_FOFORMUJDLG)
    BitBtn7: TBitBtn;
    OpenDialog1: TOpenDialog;
    Query3: TADOQuery;
    Query4: TADOQuery;
    SG1: TStringGrid;
    Query5: TADOQuery;
    BitBtn8: TBitBtn;
	 procedure FormCreate(Sender: TObject);override;
	 procedure Adatlap(cim, kod : string);override;
    procedure BitBtn7Click(Sender: TObject);
	 procedure BitBtn8Click(Sender: TObject);
	 private
	 public
	end;

var
	FuvartablaFmDlg : TFuvartablaFmDlg;

implementation

uses
	Egyeb, J_SQL, Fuvartablabe, Kozos, J_Excel, TablaOlvaso;

{$R *.DFM}

procedure TFuvartablaFmDlg.FormCreate(Sender: TObject);
begin
	Inherited FormCreate(Sender);
  AddSzuromezoRovid( 'FT_TOL', 'FUVARTABLA' );
	AddSzuromezoRovid( 'FT_KEZDO', 'FUVARTABLA' );
	AddSzuromezoRovid( 'FT_VEGZO', 'FUVARTABLA' );
	alapstr		:= 'Select * from FUVARTABLA ';
	FelTolto('�j fuvart�bla d�j felvitele ', 'Fuvart�bla adatok m�dos�t�sa ',
			'Fuvart�bla d�jak list�ja', 'FT_FTKOD', alapstr,'FUVARTABLA', QUERY_ADAT_TAG );
	kulcsmezo			:= 'FT_FTKOD';
	nevmezo				:= 'FT_DESCR';
	width				:= DLG_WIDTH;
	height				:= DLG_HEIGHT;
	BitBtn7.Parent		:= PanelBottom;
	BitBtn8.Parent		:= PanelBottom;
end;

procedure TFuvartablaFmDlg.Adatlap(cim, kod : string);
begin
	Application.CreateForm(TFuvartablabeDlg, AlForm);
	Inherited Adatlap(cim, kod );
end;

procedure TFuvartablaFmDlg.BitBtn7Click(Sender: TObject);
var
	fn			: string;
	EX	  		: TJExcel;
	exsorszam	: integer;
	snum		: integer;
	sname		: string;
	i			: integer;
	str			: string;
begin
	// Az adatok ki�r�sa XLS �llom�nyba
	EgyebDlg.SeTADOQueryDatabase(Query3);
	EgyebDlg.SeTADOQueryDatabase(Query4);
	EgyebDlg.SeTADOQueryDatabase(Query5);
	OpenDialog1.InitialDir	:= EgyebDlg.DocumentPath;
	if not Opendialog1.Execute then begin
		Exit;
	end;
	fn	:= OpenDialog1.FileName;
	if FileExists(fn) then begin
		if NoticeKi('L�tez� �llom�ny. Fel�l�rja?', NOT_QUESTION) <> 0 then begin
			Exit;
		end;
		DeleteFile(PChar(fn));
	end;
	// L�trehozzuk az XLS-t
	EX := TJexcel.Create(Self);
	if not EX.OpenXlsFile(fn) then begin
		NoticeKi('Nem siker�lt megnyitnom az XLS �llom�nyt!');
		Exit;
	end;


	EX.ColNumber	:= 10;
	EX.InitRow;
	exsorszam		:= 1;
	// Az �j worksheet l�trehoz�sa
	snum	:= EX.GetSheetsNumber;
	sname	:= 'TELAG Export';
	while snum > 0 do begin
		if sname = EX.GetSheetName(snum) then begin
			Ex.SetActiveSheet(snum);
			snum	:= -1;
		end;
		Dec(snum);
	end;
	if snum = 0 then begin
		EX.AddSheet(sname);
	end;

	// AZ oszlopsz�less�gek be�ll�t�sa
	EX.SetColumnWidth(1, 10);
	EX.SetColumnWidth(2, 20);
	EX.SetColumnWidth(3, 40);
	EX.SetColumnWidth(4, 10);
	EX.SetColumnWidth(5, 10);
	EX.SetColumnWidth(6, 10);
	EX.SetColumnWidth(7, 20);

	EX.EmptyRow;
	EX.MergeCell(1,1,1,7);
	EX.SetRowcell(1, 'Frachttabelle f�r Tyco Electronics Logistic AG (TELAG) f�r das Jahr '+EgyebDlg.EvSzam);
	EX.SaveRow(exsorszam);
	EX.SetRangeBold(exsorszam, 1, exsorszam, 7, true);
	Inc(exsorszam);
	Inc(exsorszam);
	EX.EmptyRow;
	EX.SetRowcell(1, 'Lane ID');
	EX.SetRowcell(2, 'Export');
	EX.SetRowcell(3, 'PLZ');
	// A kateg�ri�k kigy�jt�se
	Query_Run(Query4, 'SELECT * FROM SZOTAR WHERE SZ_FOKOD = ''390'' ');
	SG1.RowCount 	:= Query4.RecordCount;
//	kategszam		:= Query4.RecordCount;
	i				:= 0;
	while not Query4.Eof do begin
		SG1.Cells[0,i]	:= Query4.FieldByName('SZ_ALKOD').AsString;
		SG1.Cells[1,i]	:= Query4.FieldByName('SZ_MENEV').AsString;
		str				:= Query4.FieldByName('SZ_ALKOD').AsString;
		if Pos('T', UpperCase(str)) > 0 then begin
			str	:= copy(str, 1, Pos('T', UpperCase(str))-1);
		end;
		if Pos(' ', str) > 0 then begin
			str	:= copy(str, 1, Pos(' ', str)-1);
		end;
		SG1.Cells[2,i]	:= Format('%.2f', [StringSzam(str)]);
		Query4.Next;
		Inc(i);
	end;
	// A t�bl�zat rendez�se a kateg�ria nagys�ga szerint
	TablaRendezo( SG1, [2], [1], 0, true );
	for i := 0 to SG1.RowCount - 1 do begin
		EX.SetRowcell(4+i, SG1.Cells[1, i]);
	end;
	EX.SetRowcell(4+i, 'Valid');
	EX.SaveRow(exsorszam);
	Inc(exsorszam);
	Query_Run(Query5, 'SELECT * FROM FUVARTABLA ');
	Query_Run(Query3, 'SELECT DISTINCT FT_LANID FROM FUVARTABLA ORDER BY FT_LANID ');
	while not Query3.Eof do begin
		if Query5.Locate('FT_LANID', Query3.FieldByName('FT_LANID').AsString, []) then begin
			EX.EmptyRow;
			EX.SetRowcell(1, Query3.FieldByName('FT_LANID').AsString);
			EX.SetRowcell(2, Query5.FieldByName('FT_DESCR').AsString);
			EX.SetRowcell(3, Query5.FieldByName('FT_FTPLZ').AsString);
			str	:= Query5.FieldByName('FT_VALID').AsString;
			for i := 0 to SG1.RowCount - 1 do begin
				if Query5.Locate('FT_LANID; FT_KATEG',
								 VarArrayOf([Query3.FieldByName('FT_LANID').AsString, SG1.Cells[0, i]]), []) then begin
					EX.SetRowcell(4+i, Query5.FieldByName('FT_FTEUR').AsString);
				end;
			end;
			EX.SetRowcell(4+i, str);
			EX.SaveRow(exsorszam);
			Inc(exsorszam);
		end;
		Query3.Next;
	end;
	Screen.Cursor	:= crDefault;
	EX.SaveFileAsXls(fn);
	EX.Free;
end;

procedure TFuvartablaFmDlg.BitBtn8Click(Sender: TObject);
begin
	// Beolvassuk az XLS t�bl�t
	Application.CreateForm(TTablaOlvasoDlg, TablaOlvasoDlg);
	TablaOlvasoDlg.ShowModal;
	TablaOlvasoDlg.Destroy;
	ModLocate (Query1, FOMEZO, Query1.FieldByName(FOMEZO).AsString );
end;

end.


