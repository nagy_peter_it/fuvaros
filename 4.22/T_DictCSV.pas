unit T_DictCSV;

interface

uses SysUtils, Classes, Generics.Collections, Contnrs;

 type
  TDataOrHeader = (dohData, dohHeader);
  TDictCSVLine = class(TDictionary<string,string>)
    private
       function GetCSVLine(DataOrHeader: TDataOrHeader): string;
    published
       function GetDataCSVString: string;
       function GetHeaderCSVString: string;
    end;  // class

  TNP_CSV = class(TObjectList)
    private
    published
       function SaveToCSV(FN: string; IncludeHeader: boolean): boolean;
    end;  // class


implementation


function TNP_CSV.SaveToCSV(FN: string; IncludeHeader: boolean): boolean;
var
  F	: TextFile;
  i: integer;
begin
	if FileExists(FN) then begin
		DeleteFile(PChar(FN));
	  end;
	{$I-}
	AssignFile(F, FN);
	Rewrite(F);
	{$I+}
	if IOResult <> 0 then begin
    Result:= False;
		Exit;
  	end;
  if (Self.Count>0) and IncludeHeader then begin
    Writeln(F, TDictCSVLine(Self[0]).GetHeaderCSVString);
    end;  // if
  for i:=0 to Self.Count -1 do begin
  	Writeln(F, TDictCSVLine(Self[i]).GetDataCSVString);
    end;  // for
	CloseFile(F);
end;


function TDictCSVLine.GetDataCSVString: string;
begin
   Result:= GetCSVLine(dohData);
end;

function TDictCSVLine.GetHeaderCSVString: string;
begin
   Result:= GetCSVLine(dohHeader);
end;

function TDictCSVLine.GetCSVLine(DataOrHeader: TDataOrHeader): string;
const
   Sep = ';';
var
   KeyList: TStringList;
   KeyName, S: string;
   i: integer;
begin
  S:='';
  KeyList := TStringList.Create;
  KeyList.Sorted:= True;
  try
     // sorting keys
     for KeyName in Self.Keys do
          KeyList.Add(KeyName);
     // building list
     if KeyList.Count > 0 then begin
        for i:=0 to KeyList.Count - 1 do begin
          KeyName:= KeyList[i];
          if DataOrHeader = dohData then
            S:=S+ Self[KeyName]+Sep;
          if DataOrHeader = dohHeader then
            S:=S+ KeyName+Sep;
          end;  // for
        end; // if
  finally
    KeyList.Free;
  end;  // try-finally
  Result:= S;
end;

end.
