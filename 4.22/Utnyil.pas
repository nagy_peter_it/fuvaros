unit Utnyil;

interface

uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, DBTables, DB, StdCtrls, Buttons, Mask, DBCtrls, ExtCtrls,
  QuickRpt,DateUtils;

type
  TUtnyilDlg = class(TForm)
    BitBtn4: TBitBtn;
    BitBtn6: TBitBtn;
    Label3: TLabel;
    M4: TMaskEdit;
    BitBtn1: TBitBtn;
    Label1: TLabel;
    MaskEdit1: TMaskEdit;
    Label2: TLabel;
    MaskEdit2: TMaskEdit;
    BitBtn2: TBitBtn;
    BitBtn3: TBitBtn;
    Label4: TLabel;
    ComboBox1: TComboBox;
    QRCompositeReport1: TQRCompositeReport;
    M40: TMaskEdit;
    BitBtn32: TBitBtn;
    CB1: TCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure BitBtn4Click(Sender: TObject);
    procedure BitBtn6Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure MaskEdit1Exit(Sender: TObject);
    procedure MaskEdit2Exit(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure QRCompositeReport1AddReports(Sender: TObject);
    procedure BitBtn32Click(Sender: TObject);
  private
  		rendszamok	: string;
  public
  end;

var
  UtnyilDlg: TUtnyilDlg;

implementation

uses
	Utnyilis, Egyeb, Kozos, UtnyilisOsszes, J_Valaszto;
{$R *.DFM}

procedure TUtnyilDlg.FormCreate(Sender: TObject);
begin
 	M4.Text 		:= '';
   rendszamok      := '';
end;

procedure TUtnyilDlg.BitBtn4Click(Sender: TObject);
var
  kedat		: string;
  vedat		: string;
  rszlista     : TStringList;
  i            : integer;
begin
  	kedat	:= MaskEdit1.Text;
  	vedat	:= MaskEdit2.Text;
	if MaskEdit1.Text = '' then begin
  		kedat := '1990.01.01.';
  	end;
	if MaskEdit2.Text = '' then begin
  		vedat := EgyebDlg.MaiDatum;;
  	end;
  	if not Jodatum(kedat) then begin
		NoticeKi('Rossz d�tum megad�sa!');
     	MaskEdit1.SetFocus;
  		Exit;
  	end;
  	if not Jodatum(vedat) then begin
		NoticeKi('Rossz d�tum megad�sa!');
     	MaskEdit2.SetFocus;
     	Exit;
  	end;
   rendszamok  := '';
	if M4.Text <> '' then begin
       // A rendsz�mok id�z�jeles felsorol�sa
       rszlista    := TStringLIst.Create;
       StringParse(M4.Text, ',', rszlista);
       for i := 0 to rszlista.Count - 1 do begin
           if rszlista[i] <> '' then begin
               rendszamok := rendszamok + '''' + rszlista[i] + ''',';
           end;
       end;
       rszlista.Free;
       rendszamok  := copy(rendszamok, 1, Length(rendszamok) - 1);
  	end;

	Screen.Cursor	:= crHourGlass;
  	Application.CreateForm(TUtnyilisDlg, UtnyilisDlg);
  	UtnyilisDlg.Rendezes	:= ComboBox1.ItemIndex;
  	UtnyilisDlg.Tolt(rendszamok, kedat, vedat, MaskEdit1.Text, MaskEdit2.Text);
  	if UtnyilisDlg.vanadat	then begin
   	if CB1.Checked then begin
           UtnyilisDlg.Rep.Prepare;
           Application.CreateForm(TUtnyilisOsszesDlg, UtnyilisOsszesDlg);
           UtnyilisOsszesDlg.Tolt(UtnyilisDlg.SG, MaskEdit1.Text +' - '+ MaskEdit2.Text);
			Screen.Cursor	:= crDefault;
           UtnyilisOsszesDlg.Report.Preview;
		end else begin
           Application.CreateForm(TUtnyilisOsszesDlg, UtnyilisOsszesDlg);
           UtnyilisOsszesDlg.Tolt(UtnyilisDlg.SG, MaskEdit1.Text +' - '+ MaskEdit2.Text);
			Screen.Cursor	:= crDefault;
			QRCompositeReport1.Reports.Clear;
    		QRCompositeReport1.Preview;
		end;
       UtnyilisOsszesDlg.Destroy;
  	end else begin
		Screen.Cursor	:= crDefault;
		NoticeKi('Nincs a megadott felt�teleknek megfelel� t�tel!');
  	end;
  	UtnyilisDlg.Destroy;
end;

procedure TUtnyilDlg.BitBtn6Click(Sender: TObject);
begin
	Close;
end;

procedure TUtnyilDlg.BitBtn1Click(Sender: TObject);
begin
	// T�bb g�pkocsi kiv�laszt�sa
	TobbGepkocsiValaszto( M4, M40, rendszamok) ;
end;

procedure TUtnyilDlg.MaskEdit1Exit(Sender: TObject);
begin
	if ( ( MaskEdit1.Text <> '' ) and ( not Jodatum2(MaskEdit1 ) ) ) then begin
		NoticeKi('Hib�s d�tum megad�sa!');
		MaskEdit1.Text := '';
		MaskEdit1.Setfocus;
	end;
  	EgyebDlg.ElsoNapEllenor(MaskEdit1, MaskEdit2);
end;

procedure TUtnyilDlg.MaskEdit2Exit(Sender: TObject);
begin
	if ( ( MaskEdit2.Text <> '' ) and ( not Jodatum2(MaskEdit2 ) ) ) then begin
		NoticeKi('Hib�s d�tum megad�sa!');
		MaskEdit2.Text := '';
		MaskEdit2.Setfocus;
	end;
end;

procedure TUtnyilDlg.BitBtn2Click(Sender: TObject);
begin
	// EgyebDlg.Calendarbe(MaskEdit1);
 	// EgyebDlg.ElsoNapEllenor(MaskEdit1, MaskEdit2);
  EgyebDlg.CalendarBe_idoszak(MaskEdit1, MaskEdit2);
end;

procedure TUtnyilDlg.BitBtn3Click(Sender: TObject);
begin
	EgyebDlg.Calendarbe(MaskEdit2);
  EgyebDlg.UtolsoNap(MaskEdit2);
  // MaskEdit2.Text:=DateToStr(EndOfTheMonth(StrToDate(MaskEdit2.Text)));
end;

procedure TUtnyilDlg.QRCompositeReport1AddReports(Sender: TObject);
begin
	with QRCompositeReport1.Reports do begin
  		Add(UtnyilisDlg.Rep);
       Add(UtnyilisOsszesDlg.Report);
   end;
end;

procedure TUtnyilDlg.BitBtn32Click(Sender: TObject);
begin
	// Ki�r�tj�k a rendsz�mokat
	M4.Text		:= '';
  	M40.Text	:= '';
   rendszamok	:= '';
end;

end.


