unit J_COLDEFAULTS;

interface

uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, DBTables, DB, StdCtrls, Buttons, Mask, DBCtrls,
  Printers, ExtCtrls, CheckLst, ADODB, Grids;

type
  TColDefaultsDlg = class(TForm)
	 SG1: TStringGrid;
	 Panel1: TPanel;
	 BitElkuld: TBitBtn;
	 BitKilep: TBitBtn;
	 Panel2: TPanel;
	 SpeedButton6: TSpeedButton;
	 SpeedButton5: TSpeedButton;
	 SpeedButton4: TSpeedButton;
	 SpeedButton3: TSpeedButton;
	 Panel3: TPanel;
	 Label1: TLabel;
	 ComboBox1: TComboBox;
	 Button1: TButton;
	 Query1: TADOQuery;
	 procedure BitBtn6Click(Sender: TObject);
	 procedure BitElkuldClick(Sender: TObject);
	 procedure BitKilepClick(Sender: TObject);
	 procedure Tolt;
	 procedure SpeedButton3Click(Sender: TObject);
	 procedure SpeedButton4Click(Sender: TObject);
	 procedure SpeedButton5Click(Sender: TObject);
	 procedure SpeedButton6Click(Sender: TObject);
	 procedure FormCanResize(Sender: TObject; var NewWidth,
	   NewHeight: Integer; var Resize: Boolean);
	 procedure Mentes;
	 procedure SG1GetEditText(Sender: TObject; ACol, ARow: Integer;
	   var Value: String);
	 procedure BitBtn1Click(Sender: TObject);
	 procedure FormCreate(Sender: TObject);
  private
		tagszam		: integer;
  public
  end;

var
  ColDefaultsDlg: TColDefaultsDlg;

implementation

uses
	Egyeb, Kozos, J_SQL;
{$R *.DFM}

procedure TColDefaultsDlg.BitBtn6Click(Sender: TObject);
begin
	Close;
end;

procedure TColDefaultsDlg.BitElkuldClick(Sender: TObject);
begin
	Mentes;
	Close;
end;

procedure TColDefaultsDlg.BitKilepClick(Sender: TObject);
begin
	Close;
end;

procedure TColDefaultsDlg.SpeedButton3Click(Sender: TObject);
var
	i		: integer;
begin
	// Helycsere az el�z� elemmel
	if SG1.Row > 1 then begin
		for i := SG1.Row downto 2 do begin
			SpeedButton4Click(Sender);
		end;
		SG1.Row := 1;
	end;
end;

procedure TColDefaultsDlg.SpeedButton4Click(Sender: TObject);
var
	i		: integer;
	str		: string;
begin
	// Helycsere az el�z� elemmel
	if SG1.Row > 1 then begin
		for i := 1 to SG1.ColCount - 1 do begin
			str						:= SG1.Cells[i,SG1.Row];
			SG1.Cells[i,SG1.Row]	:= SG1.Cells[i,SG1.Row-1];
			SG1.Cells[i,SG1.Row-1] 	:= str;
		end;
		SG1.Row := SG1.Row - 1;
	end;
end;

procedure TColDefaultsDlg.SpeedButton5Click(Sender: TObject);
var
	i		: integer;
	str		: string;
begin
	// Helycsere a k�vetkez� elemmel
	if SG1.Row < SG1.RowCount -1 then begin
		for i := 1 to SG1.ColCount - 1 do begin
			str						:= SG1.Cells[i,SG1.Row];
			SG1.Cells[i,SG1.Row]	:= SG1.Cells[i,SG1.Row+1];
			SG1.Cells[i,SG1.Row+1] 	:= str;
		end;
		SG1.Row := SG1.Row + 1;
	end;
end;

procedure TColDefaultsDlg.SpeedButton6Click(Sender: TObject);
var
	i		: integer;
begin
	// Helycsere az utols� elemmel
	if SG1.Row < SG1.RowCount -1 then begin
		for i := SG1.Row to SG1.RowCount - 2 do begin
			SpeedButton5Click(Sender);
		end;
		SG1.Row := SG1.RowCount - 1;
	end;
end;

procedure TColDefaultsDlg.FormCanResize(Sender: TObject; var NewWidth,
  NewHeight: Integer; var Resize: Boolean);
begin
	if ((NewWidth < 700) or (NewHeight < 500) ) then begin
		Resize	:= false;
	end;
end;

procedure TColDefaultsDlg.Mentes;
var
	i 			: integer;
begin
	// Lementj�k a t�bl�zatot
	Query_Run(Query1, 'DELETE FROM MEZOK WHERE MZ_DIKOD = '+IntToStr(tagszam)+' AND MZ_FEKOD = 0 ');
	for i := 1 to SG1.RowCount -1 do begin
		Query_Insert( Query1, 'MEZOK', [
			'MZ_DIKOD', IntToStr(tagszam),
			'MZ_FEKOD', '0',
			'MZ_SORSZ', IntToStr(i),
			'MZ_TIPUS', SG1.Cells[4,i],
			'MZ_MZNEV', ''''+SG1.Cells[1,i]+'''',
			'MZ_MKIND', SG1.Cells[9,i],
			'MZ_LABEL', ''''+SG1.Cells[2,i]+'''',
			'MZ_WIDTH', IntToStr(StrToIntDef(SG1.Cells[5,i],0)),
			'MZ_MSIZE', IntToStr(StrToIntDef(SG1.Cells[7,i],0)),
			'MZ_VISIB', IntToStr(StrToIntDef(SG1.Cells[3,i],0)),
			'MZ_HIDEN', IntToStr(StrToIntDef(SG1.Cells[11,i],0)),
			'MZ_MZTAG', IntToStr(StrToIntDef(SG1.Cells[6,i],0)),
			'MZ_ALIGN', SG1.Cells[8,i],
			'MZ_MEGJE', ''''+SG1.Cells[10,i]+''''
			]);
	end;
end;

procedure TColDefaultsDlg.Tolt;
var
	Query1	: TADOQuery;
begin
	// A grid felt�lt�se
	KulsoGrid			:= grid;
	tagszam				:= tag;
	Sg1.RowCount		:= 2;
	SG1.Rows[1].Clear;
	SG1.Cells[0,0] 		:= 'Ssz.';
	SG1.Cells[1,0] 		:= 'Mez�n�v';
	SG1.Cells[2,0] 		:= 'Cimke';
	SG1.Cells[3,0] 		:= 'L�that�';
	SG1.Cells[4,0] 		:= 'Tipus';
	SG1.Cells[5,0] 		:= 'Hossz';
	SG1.Cells[6,0] 		:= 'Tag';
	SG1.Cells[7,0] 		:= 'M�ret';
	SG1.Cells[8,0] 		:= 'Ir�ny';
	SG1.Cells[9,0] 		:= 'Fajta';
	SG1.Cells[10,0] 	:= 'Megjegyz�s';
	SG1.Cells[11,0] 	:= 'Hidden';
	SG1.ColWidths[10]	:= 400;
	Caption			:= 'Az oszlopok meghat�roz�sa ( Tag = '+IntToStr(tagszam)+'; User = '+EgyebDlg.user_code+' )';
	Query1		:= TADOQuery.Create(nil);
	Query1.Tag  	:= 0;
	EgyebDlg.SeTADOQueryDatabase(Query1);
	Query_Run(Query1, 'SELECT * FROM MEZOK WHERE MZ_DIKOD = '+IntToStr(tagszam)+
			' AND MZ_FEKOD = '''+EgyebDlg.user_code+''' AND MZ_SORSZ = -1 ');
	if Query1.RecordCount > 0 then begin
		RadioButton1.Checked := true;
	end else begin
		RadioButton2.Checked := true;
	end;
	Query1.Free;
//	RadioButton1Click(Self);
end;

procedure TColDefaultsDlg.SG1GetEditText(Sender: TObject; ACol,
  ARow: Integer; var Value: String);
begin
	modosult	:= true;
end;

procedure TColDefaultsDlg.BitBtn1Click(Sender: TObject);
var
	Query1	: TADOQuery;
	mezoszam	: integer;
begin
	if NoticeKi('Val�ban be kiv�nja t�lteni az alap�rtelmezett be�llit�sokat ? ', NOT_QUESTION) = 0 then begin
		// Az alap�rtelmezett elrendez�s beolva�sa
		Query_Run(Query1, 'SELECT * FROM MEZOK WHERE MZ_DIKOD = '+IntToStr(tagszam)+' AND MZ_FEKOD = 0 ORDER BY MZ_SORSZ');
		if Query1.RecordCount > 0 then begin
			SG1.RowCount   	:=  Query1.RecordCount + 1 ;
			for mezoszam	:= 1 to SG1.RowCount -1 do begin
				SG1.Cells[0,mezoszam] 	:= IntToStr(mezoszam);
				SG1.Cells[1,mezoszam] 	:= Query1.FieldByName('MZ_MZNEV').AsString;
				SG1.Cells[2,mezoszam] 	:= Query1.FieldByName('MZ_LABEL').AsString;
				SG1.Cells[3,mezoszam]	:= Query1.FieldByName('MZ_VISIB').AsString;
				SG1.Cells[4,mezoszam] 	:= Query1.FieldByName('MZ_TIPUS').AsString;
				SG1.Cells[5,mezoszam]	:= Query1.FieldByName('MZ_WIDTH').AsString;
				SG1.Cells[6,mezoszam]	:= Query1.FieldByName('MZ_MZTAG').AsString;
				SG1.Cells[7,mezoszam]	:= Query1.FieldByName('MZ_MSIZE').AsString;
				SG1.Cells[8,mezoszam]	:= Query1.FieldByName('MZ_ALIGN').AsString;
				SG1.Cells[9,mezoszam]	:= Query1.FieldByName('MZ_MKIND').AsString;
				SG1.Cells[10,mezoszam]	:= Query1.FieldByName('MZ_MEGJE').AsString;
				SG1.Cells[11,mezoszam]	:= Query1.FieldByName('MZ_HIDEN').AsString;
				Query1.Next;
			end;
			MezoEllenor;
		end;
	end;
end;

procedure TColDefaultsDlg.FormCreate(Sender: TObject);
begin
	EgyebDlg.SeTADOQueryDatabase(Query1);
end;

end.


