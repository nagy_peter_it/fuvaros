unit CegadatFm;

interface

uses
	SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
	StdCtrls, Forms, ExtCtrls, Dialogs, J_FOFORMUJ;

type
	TCegadatFmDlg = class(TJ_FOFORMUJDLG)
	  procedure FormCreate(Sender: TObject);override;
    procedure Adatlap(cim, kod : string);override;
	end;

var
	CegadatFmDlg : TCegadatFmDlg;

implementation

uses
	Egyeb, J_SQL, Cegadatbe;

{$R *.DFM}

procedure TCegadatFmDlg.FormCreate(Sender: TObject);
begin
	Inherited FormCreate(Sender);
	FelTolto('�j c�gadat felvitele ', 'C�gadatok m�dos�t�sa ',
			'C�gadatok list�ja', 'S_ALKOD', 'Select * from CEGADAT ', 'CEGADAT', QUERY_ADAT_TAG );
	nevmezo				:= 'S_MENEV';
	width	:= DLG_WIDTH;
	height	:= DLG_HEIGHT;
end;

procedure TCegadatFmDlg.Adatlap(cim, kod : string);
begin
	Application.CreateForm(TCegadatbeDlg, AlForm);
	Inherited Adatlap(cim, kod );
end;

end.

