unit VevoExport;

interface

uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, DBTables, DB, StdCtrls, Buttons, Mask, DBCtrls,
  ExtCtrls, ADODB, FileCtrl, StrUtils;

type
  TVevoExportDlg = class(TForm)
    BitBtn6: TBitBtn;
    Label4: TLabel;
    M1: TMaskEdit;
    BitBtn5: TBitBtn;
    BtnExport: TBitBtn;
    Query1: TADOQuery;
    Query2: TADOQuery;
    Label1: TLabel;
    OpenDialog1: TOpenDialog;
    Label2: TLabel;
    M2: TMaskEdit;
    procedure FormCreate(Sender: TObject);
    procedure BitBtn6Click(Sender: TObject);
    procedure BitBtn5Click(Sender: TObject);
    procedure BtnExportClick(Sender: TObject);
    procedure Feldolgozas;
  private
     kilepes		: boolean;
  public
  end;

var
  VevoExportDlg: TVevoExportDlg;

implementation

uses
	Egyeb, Kozos, J_SQL, J_Excel;
{$R *.DFM}

procedure TVevoExportDlg.FormCreate(Sender: TObject);
begin
	M1.Text 		:= '';
  	EgyebDlg.SetADOQueryDatabase(Query1);
	EgyebDlg.SetADOQueryDatabase(Query2);
   Label1.Caption	:= '';
   Label1.Update;
end;

procedure TVevoExportDlg.BitBtn6Click(Sender: TObject);
begin
   if not kilepes then begin
   	kilepes	:= true;
   end else begin
		Close;
   end;
end;

procedure TVevoExportDlg.BitBtn5Click(Sender: TObject);
begin
	// Az �llom�ny kiv�laszt�sa
  	OpenDialog1.Filter		:= 'XLS �llom�nyok (*.xls)|*.XLS';
  	OpenDialog1.DefaultExt  := 'XLS';
   OpenDialog1.InitialDir	:= EgyebDlg.DocumentPath;
	if OpenDialog1.Execute then begin
 		M1.Text := OpenDialog1.FileName;
 	end;
end;

procedure TVevoExportDlg.BtnExportClick(Sender: TObject);
begin
	Feldolgozas;
end;

procedure TVevoExportDlg.Feldolgozas;
var
	fn		   	: string;
	EX	  		: TJExcel;
	sorszam		: integer;
	i			: integer;
	kodstr		: string;
	pozic		: integer;
	szamlakod	: integer;
	szamlaszam	: string;
	venev		: string;
	vegsokod	: string;
begin

	if M1.Text	= '' then begin
		NoticeKi('A kimeneti �lom�ny nem lehet �res!');
		M1.SetFocus;
		Exit;
	end;
	if FileExists(M1.Text) then begin
		if NoticeKi('L�tez� �llom�ny! Fel�l�rjuk?', NOT_QUESTION) <> 0 then begin
			Exit;
		end;
		DeleteFile(PChar(M1.Text));
	end;
	fn	:= m1.Text;

	EX := TJexcel.Create(Self);
	if not EX.OpenXlsFile(fn) then begin
		NoticeKi('Nem siker�lt megnyitnom az Excel �llom�nyt!');
	end else begin
		EX.ColNumber	:= 11;
		EX.InitRow;

       EX.SetRowcell( 1, 'regikod');
       EX.SetRowcell( 2, 'part_nev');
       EX.SetRowcell( 3, 'part_cim');
       EX.SetRowcell( 4, 'part_adosz');
       EX.SetRowcell( 5, 'part_kapcs');
       EX.SetRowcell( 6, 'part_tel');
		EX.SetRowcell( 7, 'part_fax');
		EX.SetRowcell( 8, 'part_email');
		EX.SetRowcell( 9, 'part_fhat');
		EX.SetRowcell(10, 'part_euado');
		EX.SetRowcell(11, 'part_okod');
		EX.SaveRow(1);
		if M2.Text = '' then begin
			Query_Run(Query1, 'SELECT * FROM VEVO WHERE VE_ARHIV = 0');
		end else begin
			// V�gigmegy�nk a megadott sz�mlasz�mokon �s kigy�jtj�k a megfelel� sz�ml�kat
			vegsokod	:= '';
			kodstr	:= M2.Text+',';
			pozic 	:= Pos(',', kodstr);
			while pozic > 0 do begin
				szamlakod	:= StrToIntdef(copy(kodstr,1,pozic-1),0);
				szamlaszam	:= Format('%5.5d', [szamlakod]);
				Query_Run(Query2, 'SELECT SA_VEVONEV, SA_VEVOKOD FROM SZFEJ WHERE SA_KOD LIKE ''%'+szamlaszam+'%'' ');
				if Query2.RecordCount > 0 then begin
					if Query2.FieldByName('SA_VEVOKOD').AsString <> '' then begin
						vegsokod	:= vegsokod + ''''+Query2.FieldByName('SA_VEVOKOD').AsString+''',';
					end else begin
						venev		:= Query2.FieldByName('SA_VEVONEV').AsString;
						Query_Run(Query1, 'SELECT * FROM VEVO WHERE V_NEV = '''+venev+''' ');
						if 1 > Query1.RecordCount then begin
							NoticeKi('Hi�nyz� vev� : '+venev);
						end else begin
							vegsokod	:= vegsokod + ''''+Query1.FieldByName('V_KOD').AsString+''',';
						end;
					end;
				end;
				kodstr	:= copy(kodstr, pozic+1, 99999);
				pozic 	:= Pos(',', kodstr);
			end;
			vegsokod	:= vegsokod +'''aaa''';
			Query_Run(Query1, 'SELECT * FROM VEVO WHERE V_KOD IN ('+vegsokod+')');
		end;
		sorszam	:= 2;
		kilepes	:= false;
    //EX.SetRangeFormat(1,1,5000,20,'@');
    //EX.SetRangeFormat(1,10,5000,10,'@');
		while ( ( not Query1.Eof ) and (not kilepes) ) do begin
			Application.ProcessMessages;
			for i := 1 to 11 do begin
				EX.SetRowcell(i, '');
			end;
			Label1.Caption	:= Query1.FieldByName('V_KOD').AsString + ' - ' + Query1.FieldByName('V_NEV').AsString;
			Label1.Update;

			EX.SetRowcell( 1, Query1.FieldByName('V_KOD').AsString);
			EX.SetRowcell( 2, Query1.FieldByName('V_NEV').AsString);
			EX.SetRowcell( 3, Query1.FieldByName('V_IRSZ').AsString+' '+Query1.FieldByName('V_VAROS').AsString+' '+
				Query1.FieldByName('V_CIM').AsString);
			EX.SetRowcell( 4, Query1.FieldByName('V_ADO').AsString);
			EX.SetRowcell( 5, Query1.FieldByName('V_UGYINT').AsString);
			EX.SetRowcell( 6, Query1.FieldByName('V_TEL').AsString);
			EX.SetRowcell( 7, Query1.FieldByName('V_FAX').AsString);
			EX.SetRowcell( 8, '');
			EX.SetRowcell( 9, Query1.FieldByName('V_LEJAR').AsString);
			EX.SetRowcell(10, Query1.FieldByName('VE_EUADO').AsString);
			EX.SetRowcell(11, Query1.FieldByName('V_ORSZ').AsString);
			EX.SaveRow(sorszam);
			Inc(sorszam);
       	Query1.Next;
       end;
		EX.SaveFileAsXls(fn);
       EX.Free;
   	Label1.Caption	:= '';
   	Label1.Update;
		NoticeKi('Az export�l�s befejez�d�tt!');
       kilepes	:= true;
   end;

end;

end.
