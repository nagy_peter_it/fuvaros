unit HianyKm;

interface

uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls, Egyeb,
  Forms, Dialogs, DBTables, DB, StdCtrls, Buttons, Mask, DBCtrls,
  ExtCtrls,DateUtils;

type
  THianyKmDlg = class(TForm)
    BitBtn4: TBitBtn;
    BitBtn6: TBitBtn;
    Label3: TLabel;
    M0: TMaskEdit;
    BitBtn1: TBitBtn;
    M1: TMaskEdit;
    Label1: TLabel;
    BitBtn2: TBitBtn;
    Label2: TLabel;
    M2: TMaskEdit;
    BitBtn3: TBitBtn;
    Label4: TLabel;
    M4: TMaskEdit;
    Label5: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure BitBtn4Click(Sender: TObject);
    procedure BitBtn6Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure M1Exit(Sender: TObject);
    procedure M2Exit(Sender: TObject);
    procedure M4Exit(Sender: TObject);
    procedure M4KeyPress(Sender: TObject; var Key: Char);
  private
  public
  end;

var
  HianyKmDlg: THianyKmDlg;

implementation

uses
	J_VALASZTO, hianyKmLis, Kozos, Kozos_Local;
{$R *.DFM}

procedure THianyKmDlg.FormCreate(Sender: TObject);
begin
	M0.Text := '';
end;

procedure THianyKmDlg.BitBtn4Click(Sender: TObject);
begin
	{A lista �ssze�ll�t�sa a sz�r�si elemek alapj�n}
	Screen.Cursor	:= crHourGlass;
	Application.CreateForm(THianykmlisDlg, HianykmlisDlg);
	HianykmlisDlg.hatarertek := StrToIntDef(M4.Text, 0);
	HianykmlisDlg.Tolt(M0.Text, M1.Text, M2.Text);
	Screen.Cursor	:= crDefault;
	if HianykmlisDlg.vanadat	then begin
		HianykmlisDlg.Rep.Preview;
	end else begin
		NoticeKi('Nincs a megadott felt�teleknek megfelel� t�tel!');
	end;
	HianykmlisDlg.Destroy;
end;

procedure THianyKmDlg.BitBtn6Click(Sender: TObject);
begin
	Close;
end;

procedure THianyKmDlg.BitBtn1Click(Sender: TObject);
begin
	GepkocsiValaszto(M0, GK_TRAKTOR);
end;

procedure THianyKmDlg.BitBtn2Click(Sender: TObject);
begin
	// EgyebDlg.Calendarbe(M1);
	// EgyebDlg.ElsoNapEllenor(M1, M2);
  EgyebDlg.CalendarBe_idoszak(M1, M2);
end;

procedure THianyKmDlg.BitBtn3Click(Sender: TObject);
begin
	EgyebDlg.Calendarbe(M2);
  EgyebDlg.UtolsoNap(M2);
  // M2.Text:=DateToStr(EndOfTheMonth(StrToDate(M2.Text)));
end;

procedure THianyKmDlg.M1Exit(Sender: TObject);
begin
	if ( ( M1.Text <> '' ) and ( not Jodatum2(M1 ) ) ) then begin
		NoticeKi('Hib�s d�tum megad�sa!');
		M1.Text := '';
		M1.Setfocus;
	end;
  EgyebDlg.ElsoNapEllenor(M1, M2);
end;

procedure THianyKmDlg.M2Exit(Sender: TObject);
begin
	if ( ( M2.Text <> '' ) and ( not Jodatum2(M2 ) ) ) then begin
		NoticeKi('Hib�s d�tum megad�sa!');
		M2.Text := '';
		M2.Setfocus;
	end;
end;

procedure THianyKmDlg.M4Exit(Sender: TObject);
begin
	SzamExit(Sender);
end;

procedure THianyKmDlg.M4KeyPress(Sender: TObject; var Key: Char);
begin
	SzamKeyPress(Sender, Key);
end;

end.


