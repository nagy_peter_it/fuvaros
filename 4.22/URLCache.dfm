object URLCacheDlg: TURLCacheDlg
  Left = 0
  Top = 0
  Caption = 'URLCache'
  ClientHeight = 218
  ClientWidth = 424
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object qryGetPermanentCache: TADOQuery
    Connection = EgyebDlg.ADOConnectionAlap
    Parameters = <>
    Left = 240
    Top = 40
    object qryGetPermanentCacheUC_OBJECT: TBlobField
      FieldName = 'UC_OBJECT'
    end
    object qryGetPermanentCacheUC_CACHEID: TStringField
      FieldName = 'UC_CACHEID'
    end
  end
  object qryPutPermanentCache: TADOQuery
    Connection = EgyebDlg.ADOConnectionAlap
    Parameters = <
      item
        Name = 'CacheID1'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = 'CacheID2'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = 'CacheID3'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = 'CacheObject'
        DataType = ftBlob
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      
        'if exists(select UC_CACHEID from URLCACHE where UC_CACHEID= :Cac' +
        'heID1) begin'
      'delete from URLCACHE where UC_CACHEID= :CacheID2'
      'end'
      
        'insert into URLCACHE (UC_CACHEID, UC_OBJECT, UC_TIMESTAMP) value' +
        's (:CacheID3, :CacheObject, SYSDATETIME())')
    Left = 240
    Top = 120
    object BlobField1: TBlobField
      FieldName = 'UC_OBJECT'
    end
  end
end
