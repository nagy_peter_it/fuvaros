unit Megjbe;

interface

uses
	SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
	Forms, Dialogs, StdCtrls, Buttons, Mask, DB, DBTables;

type
	TMegjbeDlg = class(TForm)
	BitElkuld: TBitBtn;
	BitKilep: TBitBtn;
	Label3: TLabel;
    M4: TMaskEdit;
    M2: TMaskEdit;
    M1: TMaskEdit;
    M3: TMaskEdit;
    M5: TMaskEdit;
    M6: TMaskEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
	procedure Tolto(cim : string; sorok : array of string; kellhat : boolean);
	procedure BitElkuldClick(Sender: TObject);
	procedure BitKilepClick(Sender: TObject);
	private
     kell		: boolean;
	public
   	sortemp  : array[0..5] of string;
		kilepes  : boolean;
	end;

var
	MegjbeDlg: TMegjbeDlg;

implementation

uses
	Egyeb, Kozos;
{$R *.DFM}

procedure TMegjbeDlg.Tolto(cim : string; sorok : array of string; kellhat : boolean);
begin
	kell			:= kellhat;
  kilepes		:= false;
	Caption 		:= cim;

	sortemp[0]	:= sorok[0];
	sortemp[1]	:= sorok[1];
	sortemp[2]	:= sorok[2];
	sortemp[3]	:= sorok[3];
	sortemp[4]	:= sorok[4];
	sortemp[5]	:= sorok[5];

	M1.Text		:= sortemp[0];
	M2.Text		:= sortemp[1];
	M3.Text		:= sortemp[2];
	M4.Text		:= sortemp[3];
	M5.Text		:= sortemp[4];
	M6.Text		:= sortemp[5];

  if not kellhat then begin
  	Label6.Visible	:= false;
     M6.Visible		:= false;
  end;
end;

procedure TMegjbeDlg.BitElkuldClick(Sender: TObject);
begin
	sortemp[0]	:= M1.Text;
	sortemp[1]	:= M2.Text;
	sortemp[2]	:= M3.Text;
	sortemp[3]	:= M4.Text;
	sortemp[4]	:= M5.Text;
	sortemp[5]	:= M6.Text;
	kilepes	:= false;
	Close;
end;

procedure TMegjbeDlg.BitKilepClick(Sender: TObject);
begin
	kilepes	:= true;
	Close;
end;

end.
