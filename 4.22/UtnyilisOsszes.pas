unit UtnyilisOsszes;

interface

uses
  WinTypes, WinProcs, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, QuickRpt, QRExpr, Qrctrls, ExtCtrls, printers, DB, DBTables, Egyeb, Forgalom, J_SQL,
  Grids, Kozos, ADODB ;

type
  TUtnyilisOsszesDlg = class(TForm)
    Report: TQuickRep;
    QRBand1: TQRBand;
    QRLabel1: TQRLabel;
    QRLabel13: TQRLabel;
    QRLabel14: TQRLabel;
    Q6: TQRLabel;
    Q7: TQRLabel;
    Q3: TQRLabel;
    Q5: TQRLabel;
    Q2: TQRLabel;
    Q4: TQRLabel;
    Query1: TADOQuery;
    Query2: TADOQuery;
    Query3: TADOQuery;
    Query4: TADOQuery;
    Q1: TQRLabel;
    QRGroup1: TQRGroup;
    QRBand3: TQRBand;
    QRLabel3: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel6: TQRLabel;
    QRLabel33: TQRLabel;
    QRShape21: TQRShape;
    QRShape22: TQRShape;
    QRShape24: TQRShape;
    QRShape25: TQRShape;
    QRShape26: TQRShape;
    QQ1: TQRLabel;
    QQ5: TQRLabel;
    QQ6: TQRLabel;
    QRShape27: TQRShape;
    QQ7: TQRLabel;
    QRShape23: TQRShape;
    QQ4: TQRLabel;
    QRLabel31: TQRLabel;
    QRSysData3: TQRSysData;
    QRSysData5: TQRSysData;
    QRLabel29: TQRLabel;
    Q8: TQRLabel;
    QRShape1: TQRShape;
    QRLabel2: TQRLabel;
    QRLabel7: TQRLabel;
    Q11: TQRLabel;
    Q12: TQRLabel;
    Q13: TQRLabel;
    Q14: TQRLabel;
    QRShape2: TQRShape;
    procedure FormCreate(Sender: TObject);
    procedure QRBand1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ReportBeforePrint(Sender: TCustomQuickRep;
      var PrintReport: Boolean);
    procedure ReportNeedData(Sender: TObject; var MoreData: Boolean);
    procedure	Tolt(sg : TStringGrid; idoszak : string);
    procedure QRBand3BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
  	private
   	grid			: TStringGrid;
       ossz1			: double;
       ossz2			: double;
       ossz3			: double;
       ossz4			: double;
  public
     	vanadat			: boolean;
       rekordszam		: integer;
  end;

var
  UtnyilisOsszesDlg: TUtnyilisOsszesDlg;

implementation

{$R *.DFM}

procedure TUtnyilisOsszesDlg.FormCreate(Sender: TObject);
begin
	EgyebDlg.SetADOQueryDatabase(Query1);
	EgyebDlg.SetADOQueryDatabase(Query2);
	EgyebDlg.SetADOQueryDatabase(Query3);
	EgyebDlg.SetADOQueryDatabase(Query4);
   ossz1		:= 0;
   ossz2		:= 0;
   ossz3		:= 0;
   ossz4		:= 0;
end;

procedure TUtnyilisOsszesDlg.QRBand1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
begin
	Q1.Caption		:= IntToStr(rekordSzam+1);
   Q2.Caption		:= Grid.Cells[0, rekordszam];
   Q3.Caption		:= Grid.Cells[1, rekordszam];
   Q4.Caption		:= Grid.Cells[2, rekordszam];
   Q5.Caption		:= Grid.Cells[3, rekordszam];
   Q6.Caption		:= Grid.Cells[4, rekordszam];
   Q7.Caption		:= Grid.Cells[5, rekordszam];
   Q8.Caption		:= Grid.Cells[6, rekordszam];
   ossz1			:= ossz1 + StringSzam(Q5.Caption);
   ossz2			:= ossz2 + StringSzam(Q6.Caption);
   ossz3			:= ossz3 + StringSzam(Q7.Caption);
   ossz4			:= ossz4 + StringSzam(Q8.Caption);
   inc(Rekordszam);
end;

procedure TUtnyilisOsszesDlg.ReportBeforePrint(Sender: TCustomQuickRep;
      var PrintReport: Boolean);
begin
	RekordSzam	:= 0;
   ossz1		:= 0;
   ossz2		:= 0;
   ossz3		:= 0;
   ossz4		:= 0;
end;

procedure TUtnyilisOsszesDlg.ReportNeedData(Sender: TObject;
  var MoreData: Boolean);
begin
	Moredata := Rekordszam < grid.RowCount;
end;

procedure	TUtnyilisOsszesDlg.Tolt(sg : TStringGrid; idoszak : string);
begin
	grid	:= sg;
   QrLabel33.Caption	:= idoszak;
end;

procedure TUtnyilisOsszesDlg.QRBand3BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
	Q11.Caption	:= Format('%.0f', [ossz1]);
	Q12.Caption	:= Format('%.0f', [ossz2]);
	Q13.Caption	:= Format('%.2f', [ossz3]);
	Q14.Caption	:= Format('%.2f', [ossz4]);
end;

end.
