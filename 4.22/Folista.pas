unit Folista;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, qrprntr,quickrpt, Buttons, ComCtrls, StdCtrls, Spin, QRExport,
  Egyeb, QRWebFilt, Printers, QRXMLSFilt, QRPDFFilt;

type
  TQRMDIPreviewInterface = class(TQRPreviewInterface)
  public
    function Show(AQRPrinter : TQRPrinter) : TWinControl; override;
    function ShowModal(AQRPrinter : TQRPrinter): TWinControl; override;
  end;

  TQRPatch = class(TQRCompositeWinControl)
  end;

  TFolistaDlg = class(TForm)
    Panel1: TPanel;
    QRPreview10: TQRPreview;
    TB7: TBitBtn;
    TB8: TBitBtn;
    TB3: TBitBtn;
    TB2: TBitBtn;
    TB4: TBitBtn;
    TB5: TBitBtn;
    TB1: TBitBtn;
	 TB6: TBitBtn;
    StatusBar: TStatusBar;
	 ProgressBar: TProgressBar;
    SE1: TSpinEdit;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    QRXMLSFilter1: TQRXMLSFilter;
    QRTextFilter1: TQRTextFilter;
    QRCSVFilter1: TQRCSVFilter;
    QRWMFFilter1: TQRWMFFilter;
    QRHTMLFilter1: TQRHTMLFilter;
    Timer1: TTimer;
    BitBtn3: TBitBtn;
    QRPreview1: TQRPreview;
    QRPDFFilter1: TQRPDFFilter;
    QRExcelFilter1: TQRExcelFilter;
    QRRTFFilter1: TQRRTFFilter;
	 procedure TB8Click(Sender: TObject);
    procedure TB2Click(Sender: TObject);
    procedure TB3Click(Sender: TObject);
    procedure TB4Click(Sender: TObject);
    procedure TB5Click(Sender: TObject);
    procedure TB1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QRPreview10PageAvailable(Sender: TObject; PageNum: Integer);
    procedure FormCreate(Sender: TObject);
    procedure TB6Click(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure TB7Click(Sender: TObject);
    procedure StatusBarDrawPanel(StatusBar: TStatusBar;
      Panel: TStatusPanel; const Rect: TRect);
    procedure QRPreview10ProgressUpdate(Sender: TObject; Progress: Integer);
	 procedure InitProc(single, printable, saveable  : boolean );
    procedure SE1Change(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure UpdateButtons;
    procedure Timer1Timer(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure QRPreview1PageAvailable(Sender: TObject; PageNum: Integer);
    procedure QRPreview1ProgressUpdate(Sender: TObject; Progress: Integer);
  private
    FQRPrinter 	: TQRPrinter;
    SinglePrint	: boolean;
    vege			: boolean;
    ido            : TDateTime;
  public
//    pQuickreport : TQuickRep;
    sStatus	: string;
    constructor CreatePreview(AOwner : TComponent; aQRPrinter : TQRPrinter);
    property QRPrinter : TQRPrinter read FQRPrinter write FQRPrinter;
  end;

var
  FolistaDlg: TFolistaDlg;

implementation

{$R *.DFM}

function TQRMDIPreviewInterface.Show(AQRPrinter : TQRPrinter) : TWinControl;
begin
  Result := TFolistaDlg.CreatePreview(Application, AQRPrinter);
  TFolistaDlg(Result).Show;
end;

function TQRMDIPreviewInterface.ShowModal(AQRPrinter : TQRPrinter) : TWinControl;
begin
  Result := TFolistaDlg.CreatePreview(Application, AQRPrinter);
  TFolistaDlg(Result).ShowModal;
end;

constructor TFolistaDlg.CreatePreview(AOwner : TComponent; aQRPrinter : TQRPrinter);
begin
  inherited Create(AOwner);
  QRPrinter 				:= aQRPrinter;
  QRPreview1.QRPrinter 	:= aQRPrinter;
  QRPreview1.Zoom 		    := EgyebDlg.folista_zoom;
  if (QRPrinter <> nil) and (QRPrinter.Title <> '') then begin
    	Caption := QRPrinter.Title;
  end;
end;

procedure TFolistaDlg.UpdateButtons;
begin
//  Application.ProcessMessages;
(*
  TB2.Enabled := QRPreview1.PageNumber > 1;
  TB3.Enabled := QRPreview1.PageNumber < QRPreview1.QRPrinter.PageCount;
*)
	if  SE1.MaxValue < QRPreview1.QRPrinter.PageCount then begin
   	SE1.MaxValue := QRPreview1.QRPrinter.PageCount;
   end;
  	StatusBar.Panels[0].Text := sStatus;
  	StatusBar.Panels[1].Text := ' ' + IntToStr(QRPreview1.PageNumber) + ' / ' +
 	IntToStr(QRPreview1.QRPrinter.PageCount) + ' oldal' ;
	if SE1.Value <> QRPreview1.PageNumber then begin
 		SE1.Value := QRPreview1.PageNumber;
  end;
end;

procedure TFolistaDlg.TB8Click(Sender: TObject);
var
	prep 		: TCustomquickrep;
begin
	TB7.Enabled				:= false;
	TB8.Enabled				:= false;
	Screen.Cursor	:= crHourGlass;
	try
		printer.Refresh;
		if TCustomquickrep( qrprinter.ParentReport).ParentComposite <> nil then begin
			prep := TCustomquickrep( qrprinter.ParentReport);
			TCustomQuickrep(QRPrinter.ParentReport).PrinterSetup;
			if TCustomQuickrep(QRPrinter.ParentReport).Tag <> 0 then begin
				Exit;
			end;
			qrprinter.PrinterIndex := prep.PrinterSettings.printerindex;
			TQRCompositeReport( prep.ParentComposite).PrinterSettings.PrinterIndex 	:= prep.UserPrinterSettings.PrinterIndex;
			TQRCompositeReport( prep.ParentComposite).PrinterSettings.FirstPage 	:= prep.UserPrinterSettings.FirstPage;
			TQRCompositeReport( prep.ParentComposite).PrinterSettings.LastPage 		:= prep.UserPrinterSettings.LastPage;
			TQRCompositeReport( prep.ParentComposite).PrinterSettings.OutputBin 	:= TQRBin(prep.UserPrinterSettings.CustomBinCode);
			TQRCompositeReport( prep.ParentComposite).PrinterSettings.Collate := prep.UserPrinterSettings.Collate;
			TQRCompositeReport( prep.ParentComposite).PrinterSettings.ColorOption := prep.UserPrinterSettings.ColorOption;
			TQRCompositeReport( prep.ParentComposite).PrinterSettings.PrintQuality := prep.UserPrinterSettings.PrintQuality;
			TQRCompositeReport( prep.ParentComposite).PrinterSettings.Copies := prep.UserPrinterSettings.Copies;
			TQRCompositeReport( prep.ParentComposite).printerSettings.Duplex := prep.UserPrinterSettings.ExtendedDuplex = 1;
			TQRCompositeReport( prep.ParentComposite).PrinterSettings.CustomBinCode := prep.UserPrinterSettings.CustomBinCode;
			TQRCompositeReport( prep.ParentComposite).PrinterSettings.UseCustomBinCode := prep.PrinterSettings.UseCustomBinCode;
			TQRCompositeReport( prep.ParentComposite).PrinterSettings.Duplex := prep.UserPrinterSettings.Duplex;
			QRPrinter.aPrinterSettings.OutputBin := TQRBin(prep.UserPrinterSettings.CustomBinCode);
			QRPrinter.aPrinterSettings.Collate := prep.UserPrinterSettings.Collate;
			QRPrinter.aPrinterSettings.ColorOption := prep.UserPrinterSettings.ColorOption;
			QRPrinter.aPrinterSettings.PrintQuality := prep.UserPrinterSettings.PrintQuality;
			QRPrinter.aPrinterSettings.Copies := prep.UserPrinterSettings.Copies;
			QRPrinter.aPrinterSettings.Duplex := prep.UserPrinterSettings.ExtendedDuplex = 1;
			QRPrinter.aPrinterSettings.CustomBinCode := prep.UserPrinterSettings.CustomBinCode;
			QRPrinter.aPrinterSettings.UseCustomBinCode := prep.PrinterSettings.UseCustomBinCode;
			QRPrinter.LastPage := prep.UserPrinterSettings.LastPage;
			QRPrinter.FirstPage := prep.UserPrinterSettings.FirstPage;
			QRPrinter.aPrinterSettings.Duplex := prep.UserPrinterSettings.Duplex;
			QRPrinter.aPrinterSettings.ExtendedDuplex := prep.UserPrinterSettings.ExtendedDuplex;
			QRPrinter.aPrinterSettings.UseExtendedDuplex := prep.UserPrinterSettings.UseExtendedDuplex;
	   end else begin
		   if qrprinter.ParentReport is TCustomquickrep then begin
				TCustomQuickrep(QRPrinter.ParentReport).PrinterSetup;
				prep := TCustomquickrep( qrprinter.ParentReport);
				if TCustomQuickrep(QRPrinter.ParentReport).Tag <> 0 then begin
					Exit;
				end;
				qrprinter.PrinterIndex := prep.PrinterSettings.printerindex;
				QRPrinter.aPrinterSettings.OutputBin := TQRBin(prep.UserPrinterSettings.CustomBinCode);
				QRPrinter.aPrinterSettings.Collate := prep.UserPrinterSettings.Collate;
				QRPrinter.aPrinterSettings.ColorOption := prep.UserPrinterSettings.ColorOption;
				QRPrinter.aPrinterSettings.PrintQuality := prep.UserPrinterSettings.PrintQuality;
				QRPrinter.aPrinterSettings.Copies := prep.UserPrinterSettings.Copies;
				QRPrinter.aPrinterSettings.Duplex := prep.UserPrinterSettings.ExtendedDuplex = 1;
				QRPrinter.aPrinterSettings.CustomBinCode := prep.UserPrinterSettings.CustomBinCode;
				QRPrinter.aPrinterSettings.UseCustomBinCode := prep.PrinterSettings.UseCustomBinCode;
				QRPrinter.LastPage := prep.UserPrinterSettings.LastPage;
				QRPrinter.FirstPage := prep.UserPrinterSettings.FirstPage;
				QRPrinter.aPrinterSettings.Duplex := prep.UserPrinterSettings.Duplex;
				QRPrinter.aPrinterSettings.ExtendedDuplex := prep.UserPrinterSettings.ExtendedDuplex;
				QRPrinter.aPrinterSettings.UseExtendedDuplex := prep.UserPrinterSettings.UseExtendedDuplex;
				if not PrintMetafileFromPreview then begin
					// set the quickrep settings
					prep.PrinterSettings.PrinterIndex := prep.UserPrinterSettings.PrinterIndex;
					prep.PrinterSettings.OutputBin := TQRBin(prep.UserPrinterSettings.CustomBinCode);
					prep.PrinterSettings.Copies := prep.UserPrinterSettings.Copies;
					prep.PrinterSettings.Duplex := prep.UserPrinterSettings.Duplex;
					prep.PrinterSettings.FirstPage := prep.UserPrinterSettings.FirstPage;
					prep.PrinterSettings.LastPage := prep.UserPrinterSettings.LastPage;
					prep.PrinterSettings.UseStandardprinter := prep.UserPrinterSettings.UseStandardprinter;
					prep.PrinterSettings.UseCustomBinCode := prep.UserPrinterSettings.UseCustomBinCode;
					prep.PrinterSettings.CustomBinCode := prep.UserPrinterSettings.CustomBinCode;
					prep.PrinterSettings.ExtendedDuplex := prep.UserPrinterSettings.ExtendedDuplex;
					prep.PrinterSettings.UseExtendedDuplex := prep.UserPrinterSettings.UseExtendedDuplex;
					prep.PrinterSettings.UseCustomPaperCode := prep.UserPrinterSettings.UseCustomPaperCode;
					prep.PrinterSettings.CustomPaperCode := prep.UserPrinterSettings.CustomPaperCode;
					prep.PrinterSettings.MemoryLimit := prep.UserPrinterSettings.MemoryLimit;
					prep.PrinterSettings.PrintQuality := prep.UserPrinterSettings.PrintQuality;
					prep.PrinterSettings.Collate := prep.UserPrinterSettings.Collate;
					prep.PrinterSettings.ColorOption := prep.UserPrinterSettings.ColorOption;
					prep.PrinterSettings.Orientation := prep.UserPrinterSettings.Orientation;
					prep.PrinterSettings.PaperSize := prep.UserPrinterSettings.PaperSize;
				end;
			end;
		end;
		TQuickRep( qrprinter.ParentReport).print;
		EgyebDlg.VoltNyomtatas	:= true;
		if SinglePrint then begin
			TB8.Hide;
			TB1Click(Sender);
		end;
	finally
		Screen.Cursor	:= crDefault;
		TB7.Enabled		:= true;
		TB8.Enabled		:= true;
	end;
end;

procedure TFolistaDlg.TB2Click(Sender: TObject);
begin
(*
  if QRPreview1.PageNumber > 1 then begin
      QRPreview1.PageNumber := QRPreview1.PageNumber - 1;
  end;
*)
  QRPreview1.PageNumber := 1;
  UpdateButtons;
end;

procedure TFolistaDlg.TB3Click(Sender: TObject);
begin
	QRPreview1.PageNumber := QRPreview1.QRPrinter.PageCount ;
	UpdateButtons;
end;

procedure TFolistaDlg.TB4Click(Sender: TObject);
begin
	QRPreview1.Zoom 	   	:= QRPreview1.Zoom + 10;
	EgyebDlg.folista_zoom 	:= QRPreview1.Zoom;
end;

procedure TFolistaDlg.TB5Click(Sender: TObject);
begin
	if QRPreview1.Zoom > 10 then begin
		QRPreview1.Zoom := QRPreview1.Zoom - 10;
	end;
	EgyebDlg.folista_zoom := QRPreview1.Zoom;
end;

procedure TFolistaDlg.TB1Click(Sender: TObject);
begin
	Screen.Cursor	:= crDefault;
	Close;
end;

procedure TFolistaDlg.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
	QRPrinter.ClosePreview(Self);
	Action := caFree;
end;

procedure TFolistaDlg.QRPreview10PageAvailable(Sender: TObject;
  PageNum: Integer);
begin
	QRPreview1.Zoom 	:= EgyebDlg.folista_zoom;
	QRPreview1.Visible	:= true;
	UpdateButtons;
end;

procedure TFolistaDlg.FormCreate(Sender: TObject);
var
  wp: TWindowPlacement;
  Rec: TRect;
begin
   ido                     := now;
	Screen.Cursor			:= crHourGlass;
	ProgressBar.Parent 		:= StatusBar;
	SystemParametersInfo(SPI_GETWORKAREA, 0, @Rec, 0);
	wp.length 				:= sizeof(wp);
	GetWindowPlacement(handle, @wp);
	wp.rcNormalposition 	:= rec;
	setwindowplacement(handle, @wp);
	SinglePrint				:= false;
	BitBtn1.Hide;
	BitBtn2.Hide;
	BitBtn3.Hide;
	if EgyebDlg.Kelllistagomb then begin
		BitBtn1.Show;
		BitBtn1.Caption		:= EgyebDlg.ListaGomb;
	end;
	if EgyebDlg.Kelllistagomb2 then begin
		BitBtn2.Show;
		BitBtn2.Caption		:= EgyebDlg.ListaGomb2;
	end;
	if EgyebDlg.Kelllistagomb3 then begin
		BitBtn3.Show;
		BitBtn3.Caption		:= EgyebDlg.ListaGomb3;
	end;
	EgyebDlg.ListaValasz 	:= 0;
	vege		  			:= false;
	TB7.Enabled				:= false;
	TB8.Enabled				:= false;
end;

procedure TFolistaDlg.TB6Click(Sender: TObject);
begin
  with TOpenDialog.Create(Application) do
  try
	 Title := 'Lista betöltése';
	 Filter := 'QuickReport állomány (*.' +cQRPDefaultExt + ')|*.' + cqrpDefaultExt;
	 if Execute then
	   if FileExists(FileName) then begin
		 qrprinter.master := nil;
        QRPrinter.Load(Filename);
        QRPreview1.PageNumber := 1;
        UpdateButtons;
      end
      else
        ShowMessage('Error!  File does not exist');
  finally
    	free;
  end;
end;

procedure TFolistaDlg.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  // Let the user navigate through the preview by the keyboard
  if ssAlt in Shift then begin
	 case Key of
      vk_Down  : With QRPreview1.VertScrollBar do
                   Position := Position + trunc(Range / 10);
	   vk_Up    : With QRPreview1.VertScrollBar do
                   Position := Position - trunc(Range / 10);
      vk_Left  : With QRPreview1.HorzScrollBar do
                   Position := Position - trunc(Range / 10);
      vk_Right : With QRPreview1.HorzScrollBar do
                   Position := Position + trunc(Range / 10);
    end;
  end;
  case Key of
    	VK_Next 	 : 	QRPreview1.PageNumber := QRPreview1.PageNumber  + 1;
    	VK_Prior  : 	QRPreview1.PageNumber := QRPreview1.PageNumber  - 1;
		VK_Home 	 : 	QRPreview1.PageNumber := 1;
    	VK_End 	 :    QRPreview1.PageNumber := QRPreview1.QRPrinter.PageCount;
  end;
  UpdateButtons;
  if key = VK_Escape then begin
	vege	:= true;
		TB1Click(self);
		Exit;
  end;
end;

procedure TFolistaDlg.TB7Click(Sender: TObject);
var
	aExportFilter 	: TQRExportFilter;
//	aPDFFilt    	: TQRPDFDocumentFilter;
	dialogus		: TSaveDialog;
begin

	TB7.Enabled				:= false;
	TB8.Enabled				:= false;
	Screen.Cursor	:= crHourGlass;
	try
	   aExportFilter 	:= nil;
	   dialogus			:= TSaveDialog.Create(Application) ;
	   try
			dialogus.Title := 'Save report';
			dialogus.Filter := QRExportFilterLibrary.SaveDialogFilterString;
			dialogus.DefaultExt := cQRPDefaultExt;
			if dialogus.Execute then begin
				if dialogus.FilterIndex = 1 then begin
					QRPrinter.Save(dialogus.Filename)
				end else begin
					try
						aExportFilter := TQRExportFilterLibraryEntry(
							QRExportFilterLibrary.Filters[dialogus.FilterIndex - 2]).ExportFilterClass.Create(dialogus.Filename);
						// Letiltjuk a gombokat
						TB1.Enabled		:= false;
						TB7.Enabled		:= false;
						TB8.Enabled		:= false;
						Screen.Cursor	:= crHourGlass;
						QRPrinter.ExportToFilter(aExportFilter);
						Screen.Cursor	:= crDefault;
						TB1.Enabled		:= true;
						TB7.Enabled		:= true;
						TB8.Enabled		:= true;
					finally
						aExportFilter.Free
					end
				end
			end;
	   finally
			dialogus.Free;
	   end;
	finally
		Screen.Cursor	:= crDefault;
		TB7.Enabled		:= true;
		TB8.Enabled		:= true;
	end;
end;

procedure TFolistaDlg.StatusBarDrawPanel(StatusBar: TStatusBar;
  Panel: TStatusPanel; const Rect: TRect);
var
  aRect: TRect;
begin
  if Panel = StatusBar.Panels[2] then begin
    aRect := Rect;
    InflateRect(aRect, 1, 1);
    ProgressBar.BoundsRect := aRect;
  end;
end;

procedure TFolistaDlg.QRPreview10ProgressUpdate(Sender: TObject;
  Progress: Integer);
begin
  	ProgressBar.Position := Progress;
  	if (Progress = 0) or (Progress = 100) then begin
    	ProgressBar.Position := 0;
	end;
  	if Progress = 100 then begin
     	SE1.MinValue	:= 1;
		SE1.MaxValue	:= QRPreview1.QRPrinter.PageCount;
//		Screen.Cursor	:= crDefault;
	end;
end;

procedure TFolistaDlg.QRPreview1PageAvailable(Sender: TObject;
  PageNum: Integer);
begin
	QRPreview1.Zoom 	:= EgyebDlg.folista_zoom;
	QRPreview1.Visible	:= true;
	UpdateButtons;

end;

procedure TFolistaDlg.QRPreview1ProgressUpdate(Sender: TObject;
  Progress: Integer);
begin
  	ProgressBar.Position := Progress;
  	if (Progress = 0) or (Progress = 100) then begin
    	ProgressBar.Position := 0;
	end;
  	if Progress = 100 then begin
     	SE1.MinValue	:= 1;
		SE1.MaxValue	:= QRPreview1.QRPrinter.PageCount;
//		Screen.Cursor	:= crDefault;
	end;

end;

procedure TFolistaDlg.InitProc(single, printable, saveable  : boolean );
begin
	SinglePrint	:= single;
	if not printable then begin
		TB8.Hide;
	end;
	if not saveable then begin
//		TB7.Hide;
		TB6.Hide;
	end;
end;

procedure TFolistaDlg.SE1Change(Sender: TObject);
begin
	QRPreview1.PageNumber := SE1.Value;
	UpdateButtons;
end;

procedure TFolistaDlg.FormShow(Sender: TObject);
begin
	SinglePrint	:= EgyebDlg.SzamlaNyom;
  	if not EgyebDlg.Nyomtathato then begin
		TB8.Hide;
  	end;
  	if not EgyebDlg.Saveable then begin
//		TB7.Hide;
		TB6.Hide;
	end;
	if ( ( BitBtn1.Visible ) and (BitBtn1.Enabled) ) then begin
		BitBtn1.SetFocus;
	end;
end;

procedure TFolistaDlg.BitBtn1Click(Sender: TObject);
begin
	EgyebDlg.Listavalasz	:= 1;
	Close;
end;

procedure TFolistaDlg.BitBtn2Click(Sender: TObject);
begin
	EgyebDlg.Listavalasz	:= 2;
  	Close;
end;

procedure TFolistaDlg.Timer1Timer(Sender: TObject);
begin
	if Qrprinter.Status = mpFinished then begin
		TB7.Enabled		:= true;
		TB8.Enabled		:= true;
		Timer1.Enabled	:= false;
		Screen.Cursor	:= crDefault;
       Caption         := Caption + '  ' +FormatDateTime('nn:ss.zzz', now-ido);
	end;
end;

procedure TFolistaDlg.BitBtn3Click(Sender: TObject);
begin
	EgyebDlg.Listavalasz	:= 3;
	Close;
end;

end.

