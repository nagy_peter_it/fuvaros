object Form1: TForm1
  Left = 972
  Top = 515
  BorderIcons = [biSystemMenu]
  Caption = 'FuvarTimer v3.0'
  ClientHeight = 595
  ClientWidth = 793
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Visible = True
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object Button2: TButton
    Left = 8
    Top = 8
    Width = 160
    Height = 25
    Caption = 'K'#246'lts'#233'g feldolgoz'#225's'
    TabOrder = 0
    OnClick = Button2Click
  end
  object Edit1: TEdit
    Left = 186
    Top = 8
    Width = 121
    Height = 21
    TabOrder = 1
    Text = 'Edit1'
    Visible = False
  end
  object Edit2: TEdit
    Left = 186
    Top = 35
    Width = 121
    Height = 21
    TabOrder = 2
    Text = 'Edit2'
    Visible = False
  end
  object Button3: TButton
    Left = 8
    Top = 43
    Width = 160
    Height = 25
    Caption = 'Megb'#237'z'#225's figyel'#233's'
    TabOrder = 3
    OnClick = Button3Click
  end
  object Button1: TButton
    Left = 186
    Top = 99
    Width = 113
    Height = 25
    Caption = '?'#218'j megb'#237'z'#225's figyel'#233's?'
    TabOrder = 4
    Visible = False
    OnClick = Button1Click
  end
  object btnArfolyamBe: TButton
    Left = 8
    Top = 77
    Width = 160
    Height = 25
    Caption = #193'rfolyam bet'#246'lt'#233's'
    TabOrder = 5
    OnClick = btnArfolyamBeClick
  end
  object Memo1: TMemo
    Left = 672
    Top = 484
    Width = 97
    Height = 49
    Lines.Strings = (
      '')
    TabOrder = 6
    Visible = False
  end
  object Button4: TButton
    Left = 263
    Top = 398
    Width = 97
    Height = 25
    Caption = 'Teszt gomb'
    TabOrder = 7
    OnClick = Button4Click
  end
  object Button5: TButton
    Left = 8
    Top = 110
    Width = 160
    Height = 25
    Caption = 'Sof'#337'rnek gyorshajt'#225's'
    TabOrder = 8
    OnClick = Button5Click
  end
  object Button6: TButton
    Left = 8
    Top = 141
    Width = 160
    Height = 25
    Caption = 'AutoService feldolgoz'#225's'
    TabOrder = 9
    OnClick = Button6Click
  end
  object Button7: TButton
    Left = 8
    Top = 172
    Width = 160
    Height = 25
    Caption = 'Rendk'#237'v'#252'li esem. '#233'rtes'#237't'#233's'
    TabOrder = 10
    OnClick = Button7Click
  end
  object Button8: TButton
    Left = 8
    Top = 203
    Width = 160
    Height = 25
    Caption = 'GK szakasz friss'#237't'#233's + oda'#233'r'#233's'
    TabOrder = 11
    OnClick = Button8Click
  end
  object Button9: TButton
    Left = 8
    Top = 234
    Width = 160
    Height = 25
    Caption = 'GK k'#233's'#233's el'#337'rejelz'#233's'
    TabOrder = 12
    OnClick = Button9Click
  end
  object Button10: TButton
    Left = 8
    Top = 265
    Width = 160
    Height = 25
    Caption = 'KULCS adat'#225'thoz'#225's'
    TabOrder = 13
    OnClick = Button10Click
  end
  object Button11: TButton
    Left = 8
    Top = 296
    Width = 160
    Height = 25
    Caption = 'Mozdulatlan aut'#243'k'
    TabOrder = 14
    OnClick = Button11Click
  end
  object Button12: TButton
    Left = 8
    Top = 327
    Width = 160
    Height = 25
    Caption = 'KUTAS / HECPOLL kapcsolat'
    TabOrder = 15
    OnClick = Button12Click
  end
  object Button13: TButton
    Left = 8
    Top = 358
    Width = 160
    Height = 25
    Caption = 'Osztr'#225'k adminisztr'#225'ci'#243
    TabOrder = 16
    OnClick = Button13Click
  end
  object Button14: TButton
    Left = 8
    Top = 389
    Width = 160
    Height = 25
    Caption = 'T'#250'l hossz'#250' '#225't'#225'll'#225's'
    TabOrder = 17
    OnClick = Button14Click
  end
  object Button15: TButton
    Left = 8
    Top = 420
    Width = 160
    Height = 25
    Caption = 'SMSEagle outbox m'#233'ret'
    TabOrder = 18
    OnClick = Button15Click
  end
  object Button16: TButton
    Left = 8
    Top = 451
    Width = 160
    Height = 25
    Caption = 'VCARD export'
    TabOrder = 19
    OnClick = Button16Click
  end
  object Button17: TButton
    Left = 8
    Top = 482
    Width = 160
    Height = 25
    Caption = 'EDI ALL export'
    TabOrder = 20
    OnClick = Button17Click
  end
  object Button18: TButton
    Left = 8
    Top = 513
    Width = 160
    Height = 25
    Caption = 'Cockpit szinkroniz'#225'l'#225's'
    TabOrder = 21
    OnClick = Button18Click
  end
  object Button19: TButton
    Left = 8
    Top = 544
    Width = 160
    Height = 25
    Caption = 'Kil'#233'p'#233's-bel'#233'p'#233's '#233'rtes'#237't'#337
    TabOrder = 22
    OnClick = Button19Click
  end
  object Timer1: TTimer
    Enabled = False
    Interval = 2000
    OnTimer = Timer1Timer
    Left = 328
    Top = 8
  end
  object ADOConnectionKozos: TADOConnection
    LoginPrompt = False
    Provider = 'SQLOLEDB.1'
    Left = 600
    Top = 384
  end
  object ADOConnectionAlap: TADOConnection
    LoginPrompt = False
    Provider = 'SQLOLEDB.1'
    Left = 600
    Top = 328
  end
  object ADOCommand1: TADOCommand
    CommandTimeout = 180
    Connection = ADOConnectionAlap
    Parameters = <>
    Left = 536
    Top = 232
  end
  object ADOCommand2: TADOCommand
    CommandText = 'delete from KOLTSEG_GYUJTO'
    CommandTimeout = 180
    Connection = ADOConnectionAlap
    Parameters = <>
    Left = 619
    Top = 228
  end
  object ADOCommand3: TADOCommand
    CommandText = 
      'delete from KOLTSEG_GYUJTO '#13#10'where (KS_JAKOD is Null) and (not K' +
      'S_JARAT is Null)  '
    CommandTimeout = 180
    Connection = ADOConnectionAlap
    Parameters = <>
    Left = 699
    Top = 228
  end
  object ADOQuery1: TADOQuery
    Connection = ADOConnectionAlap
    Parameters = <
      item
        Name = 'KOD'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 5
        Value = Null
      end>
    SQL.Strings = (
      'SELECT JA_FAZIS FROM JARAT WHERE JA_KOD = :KOD')
    Left = 400
    Top = 336
    object ADOQuery1JA_FAZIS: TBCDField
      FieldName = 'JA_FAZIS'
      Precision = 14
    end
  end
  object ADOQuery2: TADOQuery
    Connection = ADOConnectionKozos
    Parameters = <
      item
        Name = 'VN'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 3
        Value = Null
      end
      item
        Name = 'DAT'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 11
        Value = Null
      end>
    SQL.Strings = (
      
        'SELECT * FROM ARFOLYAM WHERE AR_VALNEM = :VN  AND AR_DATUM = :DA' +
        'T')
    Left = 280
    Top = 272
    object ADOQuery2AR_KOD: TIntegerField
      FieldName = 'AR_KOD'
    end
    object ADOQuery2AR_EGYSEG: TBCDField
      FieldName = 'AR_EGYSEG'
      Precision = 14
    end
    object ADOQuery2AR_VALNEM: TStringField
      FieldName = 'AR_VALNEM'
      Size = 3
    end
    object ADOQuery2AR_ERTEK: TBCDField
      FieldName = 'AR_ERTEK'
      Precision = 14
    end
    object ADOQuery2AR_DATUM: TStringField
      FieldName = 'AR_DATUM'
      Size = 11
    end
    object ADOQuery2AR_MEGJE: TStringField
      FieldName = 'AR_MEGJE'
      Size = 30
    end
  end
  object SQLKozos: TADOCommand
    Connection = ADOConnectionKozos
    Parameters = <>
    Left = 432
    Top = 64
  end
  object ADOQuery3: TADOQuery
    Connection = ADOConnectionAlap
    Parameters = <>
    SQL.Strings = (
      '')
    Left = 400
    Top = 216
    object ADOQuery3max: TIntegerField
      FieldName = 'max'
      ReadOnly = True
    end
  end
  object ADOQuery6: TADOQuery
    Connection = ADOConnectionKozos
    Parameters = <
      item
        Name = 'DAT'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 11
        Value = Null
      end
      item
        Name = 'TIP'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      
        'SELECT * FROM TETANAR WHERE TT_DATUM <= :DAT AND TT_TIPUS = :TIP' +
        '  ORDER BY TT_DATUM, TT_TTKOD')
    Left = 336
    Top = 168
    object ADOQuery6TT_TTKOD: TIntegerField
      FieldName = 'TT_TTKOD'
    end
    object ADOQuery6TT_EGYAR: TBCDField
      FieldName = 'TT_EGYAR'
      Precision = 14
    end
    object ADOQuery6TT_DATUM: TStringField
      FieldName = 'TT_DATUM'
      Size = 11
    end
    object ADOQuery6TT_MEGJE: TStringField
      FieldName = 'TT_MEGJE'
      Size = 80
    end
    object ADOQuery6TT_TIPUS: TIntegerField
      FieldName = 'TT_TIPUS'
    end
  end
  object ADOQuery8: TADOQuery
    Connection = ADOConnectionKozos
    Parameters = <
      item
        Name = 'RSZ'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end>
    SQL.Strings = (
      'SELECT gk_hozza FROM gepkocsi WHERE gk_resz= :RSZ ')
    Left = 400
    Top = 168
    object ADOQuery8gk_hozza: TIntegerField
      FieldName = 'gk_hozza'
    end
  end
  object Szotar: TADOQuery
    Connection = ADOConnectionKozos
    Parameters = <
      item
        Name = 'FK'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 3
        Value = Null
      end
      item
        Name = 'AK'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 5
        Value = Null
      end>
    SQL.Strings = (
      
        'SELECT sz_menev, sz_leiro FROM szotar WHERE sz_fokod= :FK and sz' +
        '_alkod= :AK')
    Left = 176
    Top = 336
    object Szotarsz_menev: TStringField
      FieldName = 'sz_menev'
      Size = 120
    end
    object Szotarsz_leiro: TStringField
      FieldName = 'sz_leiro'
      Size = 80
    end
  end
  object Gepkocsi: TADOQuery
    Connection = ADOConnectionKozos
    Parameters = <
      item
        Name = 'RESZ'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end>
    SQL.Strings = (
      'SELECT gk_gkkat FROM gepkocsi WHERE gk_resz= :RESZ ')
    Left = 224
    Top = 272
    object Gepkocsigk_gkkat: TStringField
      FieldName = 'gk_gkkat'
      Size = 5
    end
  end
  object T_GYUJTOMIND: TADOQuery
    Connection = ADOConnectionAlap
    CommandTimeout = 180
    Parameters = <>
    SQL.Strings = (
      'select * from KOLTSEG_GYUJTO'
      '')
    Left = 336
    Top = 218
    object T_GYUJTOMINDKS_KTKOD: TIntegerField
      FieldName = 'KS_KTKOD'
    end
    object T_GYUJTOMINDKS_JAKOD: TStringField
      FieldName = 'KS_JAKOD'
      Size = 5
    end
    object T_GYUJTOMINDKS_DATUM: TStringField
      FieldName = 'KS_DATUM'
      Size = 11
    end
    object T_GYUJTOMINDKS_KMORA: TBCDField
      FieldName = 'KS_KMORA'
      Precision = 14
    end
    object T_GYUJTOMINDKS_VALNEM: TStringField
      FieldName = 'KS_VALNEM'
      Size = 3
    end
    object T_GYUJTOMINDKS_ARFOLY: TBCDField
      FieldName = 'KS_ARFOLY'
      Precision = 14
    end
    object T_GYUJTOMINDKS_MEGJ: TStringField
      FieldName = 'KS_MEGJ'
      Size = 80
    end
    object T_GYUJTOMINDKS_RENDSZ: TStringField
      FieldName = 'KS_RENDSZ'
      Size = 10
    end
    object T_GYUJTOMINDKS_LEIRAS: TStringField
      FieldName = 'KS_LEIRAS'
      Size = 80
    end
    object T_GYUJTOMINDKS_UZMENY: TBCDField
      FieldName = 'KS_UZMENY'
      Precision = 14
    end
    object T_GYUJTOMINDKS_OSSZEG: TBCDField
      FieldName = 'KS_OSSZEG'
      Precision = 14
    end
    object T_GYUJTOMINDKS_TELE: TIntegerField
      FieldName = 'KS_TELE'
    end
    object T_GYUJTOMINDKS_SZKOD: TStringField
      FieldName = 'KS_SZKOD'
      Size = 11
    end
    object T_GYUJTOMINDKS_AFASZ: TBCDField
      FieldName = 'KS_AFASZ'
      Precision = 14
    end
    object T_GYUJTOMINDKS_AFAKOD: TStringField
      FieldName = 'KS_AFAKOD'
      Size = 3
    end
    object T_GYUJTOMINDKS_AFAERT: TBCDField
      FieldName = 'KS_AFAERT'
      Precision = 14
    end
    object T_GYUJTOMINDKS_ERTEK: TBCDField
      FieldName = 'KS_ERTEK'
      Precision = 14
    end
    object T_GYUJTOMINDKS_VIKOD: TStringField
      FieldName = 'KS_VIKOD'
      Size = 5
    end
    object T_GYUJTOMINDKS_ATLAG: TBCDField
      FieldName = 'KS_ATLAG'
      Precision = 14
    end
    object T_GYUJTOMINDKS_FIMOD: TStringField
      FieldName = 'KS_FIMOD'
      Size = 5
    end
    object T_GYUJTOMINDKS_TETAN: TIntegerField
      FieldName = 'KS_TETAN'
    end
    object T_GYUJTOMINDKS_TIPUS: TIntegerField
      FieldName = 'KS_TIPUS'
    end
    object T_GYUJTOMINDKS_THELY: TStringField
      FieldName = 'KS_THELY'
      Size = 80
    end
    object T_GYUJTOMINDKS_TELES: TStringField
      FieldName = 'KS_TELES'
      Size = 3
    end
    object T_GYUJTOMINDKS_JARAT: TStringField
      FieldName = 'KS_JARAT'
      Size = 10
    end
    object T_GYUJTOMINDKS_SOKOD: TStringField
      FieldName = 'KS_SOKOD'
      Size = 5
    end
    object T_GYUJTOMINDKS_SONEV: TStringField
      FieldName = 'KS_SONEV'
      Size = 45
    end
    object T_GYUJTOMINDKS_ORSZA: TStringField
      FieldName = 'KS_ORSZA'
      Size = 3
    end
    object T_GYUJTOMINDKS_IDO: TStringField
      FieldName = 'KS_IDO'
      Size = 5
    end
    object T_GYUJTOMINDKS_DATI: TDateTimeField
      FieldName = 'KS_DATI'
    end
    object T_GYUJTOMINDKS_KTIP: TIntegerField
      FieldName = 'KS_KTIP'
    end
    object T_GYUJTOMINDKS_HMEGJ: TStringField
      FieldName = 'KS_HMEGJ'
      Size = 100
    end
    object T_GYUJTOMINDKS_FELDAT: TDateTimeField
      FieldName = 'KS_FELDAT'
    end
    object T_GYUJTOMINDKS_SOR: TStringField
      FieldName = 'KS_SOR'
      Size = 500
    end
    object T_GYUJTOMINDKS_GKKAT: TStringField
      FieldName = 'KS_GKKAT'
      Size = 5
    end
    object T_GYUJTOMINDKS_NC_KM: TBCDField
      FieldName = 'KS_NC_KM'
      Precision = 12
      Size = 2
    end
    object T_GYUJTOMINDKS_NC_USZ: TBCDField
      FieldName = 'KS_NC_USZ'
      Precision = 12
      Size = 2
    end
    object T_GYUJTOMINDKS_NC_UME: TBCDField
      FieldName = 'KS_NC_UME'
      Precision = 12
      Size = 2
    end
    object T_GYUJTOMINDKS_NC_DAT: TStringField
      FieldName = 'KS_NC_DAT'
      Size = 11
    end
    object T_GYUJTOMINDKS_NC_IDO: TStringField
      FieldName = 'KS_NC_IDO'
      Size = 11
    end
  end
  object T_JARAT: TADOQuery
    Connection = ADOConnectionAlap
    Parameters = <
      item
        Name = 'RSZ'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 30
        Value = Null
      end
      item
        Name = 'DAT1'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 16
        Value = Null
      end
      item
        Name = 'DAT2'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 16
        Value = Null
      end>
    SQL.Strings = (
      'select * from jarat '
      
        'where ja_rendsz= :RSZ and (ja_jkezd + ja_keido )<= :DAT1 and (ja' +
        '_jvege + ja_veido)>= :DAT2'
      'order by ja_jkezd, ja_keido')
    Left = 232
    Top = 210
    object T_JARATJA_KOD: TStringField
      DisplayWidth = 6
      FieldName = 'JA_KOD'
      Size = 6
    end
    object T_JARATJA_JARAT: TStringField
      FieldName = 'JA_JARAT'
      Size = 80
    end
    object T_JARATJA_JKEZD: TStringField
      FieldName = 'JA_JKEZD'
      Size = 11
    end
    object T_JARATJA_JVEGE: TStringField
      FieldName = 'JA_JVEGE'
      Size = 11
    end
    object T_JARATJA_RENDSZ: TStringField
      FieldName = 'JA_RENDSZ'
      Size = 30
    end
    object T_JARATJA_SAJAT: TIntegerField
      FieldName = 'JA_SAJAT'
    end
    object T_JARATJA_MEGJ: TStringField
      FieldName = 'JA_MEGJ'
      Size = 200
    end
    object T_JARATJA_MENSZ: TStringField
      FieldName = 'JA_MENSZ'
      Size = 5
    end
    object T_JARATJA_NYITKM: TBCDField
      FieldName = 'JA_NYITKM'
      Precision = 14
    end
    object T_JARATJA_ZAROKM: TBCDField
      FieldName = 'JA_ZAROKM'
      Precision = 14
    end
    object T_JARATJA_BELKM: TBCDField
      FieldName = 'JA_BELKM'
      Precision = 14
    end
    object T_JARATJA_ALKOD: TBCDField
      FieldName = 'JA_ALKOD'
      Precision = 14
    end
    object T_JARATJA_FAZIS: TBCDField
      FieldName = 'JA_FAZIS'
      Precision = 14
    end
    object T_JARATJA_OSSZKM: TBCDField
      FieldName = 'JA_OSSZKM'
      Precision = 14
    end
    object T_JARATJA_ALVAL: TIntegerField
      FieldName = 'JA_ALVAL'
    end
    object T_JARATJA_ORSZ: TStringField
      FieldName = 'JA_ORSZ'
      Size = 3
    end
    object T_JARATJA_SOFK1: TStringField
      FieldName = 'JA_SOFK1'
      Size = 5
    end
    object T_JARATJA_SOFK2: TStringField
      FieldName = 'JA_SOFK2'
      Size = 5
    end
    object T_JARATJA_SNEV1: TStringField
      FieldName = 'JA_SNEV1'
      Size = 45
    end
    object T_JARATJA_SNEV2: TStringField
      FieldName = 'JA_SNEV2'
      Size = 45
    end
    object T_JARATJA_JPOTK: TStringField
      FieldName = 'JA_JPOTK'
    end
    object T_JARATJA_FAZST: TStringField
      FieldName = 'JA_FAZST'
      Size = 13
    end
    object T_JARATJA_KEIDO: TStringField
      FieldName = 'JA_KEIDO'
      Size = 5
    end
    object T_JARATJA_VEIDO: TStringField
      FieldName = 'JA_VEIDO'
      Size = 5
    end
    object T_JARATJA_LFDAT: TStringField
      FieldName = 'JA_LFDAT'
      Size = 11
    end
    object T_JARATJA_SZAK1: TBCDField
      FieldName = 'JA_SZAK1'
      Precision = 12
      Size = 2
    end
    object T_JARATJA_SZAK2: TBCDField
      FieldName = 'JA_SZAK2'
      Precision = 12
      Size = 2
    end
    object T_JARATJA_URESK: TStringField
      FieldName = 'JA_URESK'
      Size = 5
    end
    object T_JARATJA_EUROS: TStringField
      FieldName = 'JA_EUROS'
      Size = 1
    end
    object T_JARATJA_UBKM: TStringField
      FieldName = 'JA_UBKM'
      Size = 5
    end
    object T_JARATJA_UKKM: TStringField
      FieldName = 'JA_UKKM'
      Size = 5
    end
    object T_JARATJA_JAORA: TIntegerField
      FieldName = 'JA_JAORA'
    end
  end
  object T_GYUJTO: TADODataSet
    Connection = ADOConnectionAlap
    CommandText = 
      'select * from KOLTSEG_GYUJTO'#13#10'where (KS_JAKOD is null or KS_JAKO' +
      'D='#39#39') and ks_rendsz= :RSZ and (ks_datum+ks_ido)>= :KEZD and (ks_' +
      'datum+ks_ido)<= :VEGE'#13#10'order by ks_datum,ks_ido'
    Parameters = <
      item
        Name = 'RSZ'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'KEZD'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 16
        Value = Null
      end
      item
        Name = 'VEGE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 16
        Value = Null
      end>
    Left = 272
    Top = 168
    object T_GYUJTOKS_KTKOD: TIntegerField
      FieldName = 'KS_KTKOD'
    end
    object T_GYUJTOKS_JAKOD: TStringField
      FieldName = 'KS_JAKOD'
      Size = 5
    end
    object T_GYUJTOKS_DATUM: TStringField
      FieldName = 'KS_DATUM'
      Size = 11
    end
    object T_GYUJTOKS_KMORA: TBCDField
      FieldName = 'KS_KMORA'
      Precision = 14
    end
    object T_GYUJTOKS_VALNEM: TStringField
      FieldName = 'KS_VALNEM'
      Size = 3
    end
    object T_GYUJTOKS_ARFOLY: TBCDField
      FieldName = 'KS_ARFOLY'
      Precision = 14
    end
    object T_GYUJTOKS_MEGJ: TStringField
      FieldName = 'KS_MEGJ'
      Size = 80
    end
    object T_GYUJTOKS_RENDSZ: TStringField
      FieldName = 'KS_RENDSZ'
      Size = 10
    end
    object T_GYUJTOKS_LEIRAS: TStringField
      FieldName = 'KS_LEIRAS'
      Size = 80
    end
    object T_GYUJTOKS_UZMENY: TBCDField
      FieldName = 'KS_UZMENY'
      Precision = 14
    end
    object T_GYUJTOKS_OSSZEG: TBCDField
      FieldName = 'KS_OSSZEG'
      Precision = 14
    end
    object T_GYUJTOKS_TELE: TIntegerField
      FieldName = 'KS_TELE'
    end
    object T_GYUJTOKS_SZKOD: TStringField
      FieldName = 'KS_SZKOD'
      Size = 11
    end
    object T_GYUJTOKS_AFASZ: TBCDField
      FieldName = 'KS_AFASZ'
      Precision = 14
    end
    object T_GYUJTOKS_AFAKOD: TStringField
      FieldName = 'KS_AFAKOD'
      Size = 3
    end
    object T_GYUJTOKS_AFAERT: TBCDField
      FieldName = 'KS_AFAERT'
      Precision = 14
    end
    object T_GYUJTOKS_ERTEK: TBCDField
      FieldName = 'KS_ERTEK'
      Precision = 14
    end
    object T_GYUJTOKS_VIKOD: TStringField
      FieldName = 'KS_VIKOD'
      Size = 5
    end
    object T_GYUJTOKS_ATLAG: TBCDField
      FieldName = 'KS_ATLAG'
      Precision = 14
    end
    object T_GYUJTOKS_FIMOD: TStringField
      FieldName = 'KS_FIMOD'
      Size = 5
    end
    object T_GYUJTOKS_TETAN: TIntegerField
      FieldName = 'KS_TETAN'
    end
    object T_GYUJTOKS_TIPUS: TIntegerField
      FieldName = 'KS_TIPUS'
    end
    object T_GYUJTOKS_THELY: TStringField
      FieldName = 'KS_THELY'
      Size = 80
    end
    object T_GYUJTOKS_TELES: TStringField
      FieldName = 'KS_TELES'
      Size = 3
    end
    object T_GYUJTOKS_JARAT: TStringField
      FieldName = 'KS_JARAT'
      Size = 10
    end
    object T_GYUJTOKS_SOKOD: TStringField
      FieldName = 'KS_SOKOD'
      Size = 5
    end
    object T_GYUJTOKS_SONEV: TStringField
      FieldName = 'KS_SONEV'
      Size = 45
    end
    object T_GYUJTOKS_ORSZA: TStringField
      FieldName = 'KS_ORSZA'
      Size = 3
    end
    object T_GYUJTOKS_IDO: TStringField
      FieldName = 'KS_IDO'
      Size = 5
    end
    object T_GYUJTOKS_DATI: TDateTimeField
      FieldName = 'KS_DATI'
    end
    object T_GYUJTOKS_KTIP: TIntegerField
      FieldName = 'KS_KTIP'
    end
    object T_GYUJTOKS_HMEGJ: TStringField
      FieldName = 'KS_HMEGJ'
      Size = 100
    end
    object T_GYUJTOKS_FELDAT: TDateTimeField
      FieldName = 'KS_FELDAT'
    end
  end
  object T_JARPOTOS: TADOQuery
    Connection = ADOConnectionAlap
    Parameters = <
      item
        Name = 'KOD'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 5
        Value = Null
      end
      item
        Name = 'ORA'
        Attributes = [paSigned, paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 12
        Size = 19
        Value = Null
      end
      item
        Name = 'ORA'
        Attributes = [paSigned, paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 12
        Size = 19
        Value = Null
      end>
    SQL.Strings = (
      'select * from jarpotos'
      'where jp_jakod= :KOD and jp_uzem1 <= :ORA and  jp_uzem2 >= :ORA')
    Left = 368
    Top = 114
    object T_JARPOTOSJP_JAKOD: TStringField
      FieldName = 'JP_JAKOD'
      Size = 5
    end
    object T_JARPOTOSJP_POREN: TStringField
      FieldName = 'JP_POREN'
      Size = 10
    end
    object T_JARPOTOSJP_PORKM: TBCDField
      FieldName = 'JP_PORKM'
      Precision = 12
      Size = 2
    end
    object T_JARPOTOSJP_PORK2: TBCDField
      FieldName = 'JP_PORK2'
      Precision = 12
      Size = 2
    end
    object T_JARPOTOSJP_JPKOD: TIntegerField
      FieldName = 'JP_JPKOD'
    end
    object T_JARPOTOSJP_UZEM1: TBCDField
      FieldName = 'JP_UZEM1'
      Precision = 12
      Size = 2
    end
    object T_JARPOTOSJP_UZEM2: TBCDField
      FieldName = 'JP_UZEM2'
      Precision = 12
      Size = 2
    end
  end
  object T_KOLTSEG: TADOQuery
    Connection = ADOConnectionAlap
    Parameters = <>
    SQL.Strings = (
      'select * from koltseg'
      '')
    Left = 432
    Top = 114
  end
  object NMSMTP1: TIdSMTP
    SASLMechanisms = <>
    Left = 224
    Top = 336
  end
  object SQLDATE: TADOCommand
    CommandText = 'select "date"=CAST(FLOOR(CAST(GETDATE() AS FLOAT)) AS DATETIME)'
    Connection = ADOConnectionKozos
    Parameters = <>
    Left = 432
    Top = 16
  end
  object T_MEGSEGED: TADODataSet
    Connection = ADOConnectionAlap
    CursorType = ctStatic
    CommandText = 
      'select s.*,m.mb_venev from MEGSEGED s, MEGBIZAS m'#13#10'where (MB_MBK' +
      'OD=MS_MBKOD)and (MS_DATUM= :DAT) and '#13#10'((MS_FETIDO='#39#39' and MS_FER' +
      'KIDO='#39#39' and MS_TIPUS=0) or'#13#10'(MS_LETIDO='#39#39' and MS_LERKIDO='#39#39' and ' +
      'MS_TIPUS=1) )'#13#10'and m.MB_STATU=100'#13#10'order by MS_MBKOD, MS_SORSZ'#13#10
    Parameters = <
      item
        Name = 'DAT'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 11
        Value = Null
      end>
    Left = 288
    Top = 120
    object T_MEGSEGEDMS_MBKOD: TIntegerField
      FieldName = 'MS_MBKOD'
    end
    object T_MEGSEGEDMS_SORSZ: TIntegerField
      FieldName = 'MS_SORSZ'
    end
    object T_MEGSEGEDMS_FELNEV: TStringField
      FieldName = 'MS_FELNEV'
      Size = 30
    end
    object T_MEGSEGEDMS_FELTEL: TStringField
      FieldName = 'MS_FELTEL'
      Size = 30
    end
    object T_MEGSEGEDMS_FELCIM: TStringField
      FieldName = 'MS_FELCIM'
      Size = 30
    end
    object T_MEGSEGEDMS_FELDAT: TStringField
      FieldName = 'MS_FELDAT'
      Size = 11
    end
    object T_MEGSEGEDMS_FELIDO: TStringField
      FieldName = 'MS_FELIDO'
      Size = 5
    end
    object T_MEGSEGEDMS_FETDAT: TStringField
      FieldName = 'MS_FETDAT'
      Size = 11
    end
    object T_MEGSEGEDMS_FETIDO: TStringField
      FieldName = 'MS_FETIDO'
      Size = 5
    end
    object T_MEGSEGEDMS_LERNEV: TStringField
      FieldName = 'MS_LERNEV'
      Size = 30
    end
    object T_MEGSEGEDMS_LERTEL: TStringField
      FieldName = 'MS_LERTEL'
      Size = 30
    end
    object T_MEGSEGEDMS_LERCIM: TStringField
      FieldName = 'MS_LERCIM'
      Size = 30
    end
    object T_MEGSEGEDMS_LERDAT: TStringField
      FieldName = 'MS_LERDAT'
      Size = 11
    end
    object T_MEGSEGEDMS_LERIDO: TStringField
      FieldName = 'MS_LERIDO'
      Size = 5
    end
    object T_MEGSEGEDMS_LETDAT: TStringField
      FieldName = 'MS_LETDAT'
      Size = 11
    end
    object T_MEGSEGEDMS_LETIDO: TStringField
      FieldName = 'MS_LETIDO'
      Size = 5
    end
    object T_MEGSEGEDMS_FELARU: TStringField
      FieldName = 'MS_FELARU'
      Size = 60
    end
    object T_MEGSEGEDMS_FSULY: TBCDField
      FieldName = 'MS_FSULY'
      Precision = 12
      Size = 2
    end
    object T_MEGSEGEDMS_FEPAL: TBCDField
      FieldName = 'MS_FEPAL'
      Precision = 12
      Size = 2
    end
    object T_MEGSEGEDMS_LSULY: TBCDField
      FieldName = 'MS_LSULY'
      Precision = 12
      Size = 2
    end
    object T_MEGSEGEDMS_LEPAL: TBCDField
      FieldName = 'MS_LEPAL'
      Precision = 12
      Size = 2
    end
    object T_MEGSEGEDMS_FELSE1: TStringField
      FieldName = 'MS_FELSE1'
      Size = 30
    end
    object T_MEGSEGEDMS_FELSE2: TStringField
      FieldName = 'MS_FELSE2'
      Size = 30
    end
    object T_MEGSEGEDMS_FELSE3: TStringField
      FieldName = 'MS_FELSE3'
      Size = 30
    end
    object T_MEGSEGEDMS_FELSE4: TStringField
      FieldName = 'MS_FELSE4'
      Size = 30
    end
    object T_MEGSEGEDMS_FELSE5: TStringField
      FieldName = 'MS_FELSE5'
      Size = 30
    end
    object T_MEGSEGEDMS_LERSE1: TStringField
      FieldName = 'MS_LERSE1'
      Size = 30
    end
    object T_MEGSEGEDMS_LERSE2: TStringField
      FieldName = 'MS_LERSE2'
      Size = 30
    end
    object T_MEGSEGEDMS_LERSE3: TStringField
      FieldName = 'MS_LERSE3'
      Size = 30
    end
    object T_MEGSEGEDMS_LERSE4: TStringField
      FieldName = 'MS_LERSE4'
      Size = 30
    end
    object T_MEGSEGEDMS_LERSE5: TStringField
      FieldName = 'MS_LERSE5'
      Size = 30
    end
    object T_MEGSEGEDMS_ORSZA: TStringField
      FieldName = 'MS_ORSZA'
      Size = 5
    end
    object T_MEGSEGEDMS_FORSZ: TStringField
      FieldName = 'MS_FORSZ'
      Size = 5
    end
    object T_MEGSEGEDMS_TIPUS: TIntegerField
      FieldName = 'MS_TIPUS'
    end
    object T_MEGSEGEDMS_FELIR: TStringField
      FieldName = 'MS_FELIR'
      Size = 10
    end
    object T_MEGSEGEDMS_LELIR: TStringField
      FieldName = 'MS_LELIR'
      Size = 10
    end
    object T_MEGSEGEDMS_DATUM: TStringField
      FieldName = 'MS_DATUM'
      Size = 11
    end
    object T_MEGSEGEDMS_IDOPT: TStringField
      FieldName = 'MS_IDOPT'
      Size = 5
    end
    object T_MEGSEGEDMS_MSKOD: TIntegerField
      FieldName = 'MS_MSKOD'
    end
    object T_MEGSEGEDMS_TINEV: TStringField
      FieldName = 'MS_TINEV'
      Size = 8
    end
    object T_MEGSEGEDMS_STATK: TStringField
      FieldName = 'MS_STATK'
      Size = 3
    end
    object T_MEGSEGEDMS_STATN: TStringField
      FieldName = 'MS_STATN'
    end
    object T_MEGSEGEDMS_TEIDO: TStringField
      FieldName = 'MS_TEIDO'
      Size = 5
    end
    object T_MEGSEGEDMS_TENNEV: TStringField
      FieldName = 'MS_TENNEV'
      Size = 30
    end
    object T_MEGSEGEDMS_TENTEL: TStringField
      FieldName = 'MS_TENTEL'
      Size = 30
    end
    object T_MEGSEGEDMS_TENCIM: TStringField
      FieldName = 'MS_TENCIM'
      Size = 30
    end
    object T_MEGSEGEDMS_TENYL1: TStringField
      FieldName = 'MS_TENYL1'
      Size = 30
    end
    object T_MEGSEGEDMS_TENYL2: TStringField
      FieldName = 'MS_TENYL2'
      Size = 30
    end
    object T_MEGSEGEDMS_TENYL3: TStringField
      FieldName = 'MS_TENYL3'
      Size = 30
    end
    object T_MEGSEGEDMS_TENYL4: TStringField
      FieldName = 'MS_TENYL4'
      Size = 30
    end
    object T_MEGSEGEDMS_TENYL5: TStringField
      FieldName = 'MS_TENYL5'
      Size = 30
    end
    object T_MEGSEGEDMS_TENOR: TStringField
      FieldName = 'MS_TENOR'
      Size = 5
    end
    object T_MEGSEGEDMS_TENIR: TStringField
      FieldName = 'MS_TENIR'
      Size = 10
    end
    object T_MEGSEGEDMS_AKTNEV: TStringField
      FieldName = 'MS_AKTNEV'
      Size = 30
    end
    object T_MEGSEGEDMS_AKTTEL: TStringField
      FieldName = 'MS_AKTTEL'
      Size = 30
    end
    object T_MEGSEGEDMS_AKTCIM: TStringField
      FieldName = 'MS_AKTCIM'
      Size = 30
    end
    object T_MEGSEGEDMS_AKTOR: TStringField
      FieldName = 'MS_AKTOR'
      Size = 5
    end
    object T_MEGSEGEDMS_AKTIR: TStringField
      FieldName = 'MS_AKTIR'
      Size = 10
    end
    object T_MEGSEGEDMS_EXSTR: TStringField
      FieldName = 'MS_EXSTR'
      Size = 10
    end
    object T_MEGSEGEDMS_EXPOR: TIntegerField
      FieldName = 'MS_EXPOR'
    end
    object T_MEGSEGEDMS_TEFNEV: TStringField
      FieldName = 'MS_TEFNEV'
      Size = 30
    end
    object T_MEGSEGEDMS_TEFTEL: TStringField
      FieldName = 'MS_TEFTEL'
      Size = 30
    end
    object T_MEGSEGEDMS_TEFCIM: TStringField
      FieldName = 'MS_TEFCIM'
      Size = 30
    end
    object T_MEGSEGEDMS_TEFL1: TStringField
      FieldName = 'MS_TEFL1'
      Size = 30
    end
    object T_MEGSEGEDMS_TEFL2: TStringField
      FieldName = 'MS_TEFL2'
      Size = 30
    end
    object T_MEGSEGEDMS_TEFL3: TStringField
      FieldName = 'MS_TEFL3'
      Size = 30
    end
    object T_MEGSEGEDMS_TEFL4: TStringField
      FieldName = 'MS_TEFL4'
      Size = 30
    end
    object T_MEGSEGEDMS_TEFL5: TStringField
      FieldName = 'MS_TEFL5'
      Size = 30
    end
    object T_MEGSEGEDMS_TEFOR: TStringField
      FieldName = 'MS_TEFOR'
      Size = 5
    end
    object T_MEGSEGEDMS_TEFIR: TStringField
      FieldName = 'MS_TEFIR'
      Size = 10
    end
    object T_MEGSEGEDMS_REGIK: TIntegerField
      FieldName = 'MS_REGIK'
    end
    object T_MEGSEGEDMS_FAJKO: TIntegerField
      FieldName = 'MS_FAJKO'
    end
    object T_MEGSEGEDMS_FAJTA: TStringField
      FieldName = 'MS_FAJTA'
      Size = 10
    end
    object T_MEGSEGEDMS_RENSZ: TStringField
      FieldName = 'MS_RENSZ'
      Size = 10
    end
    object T_MEGSEGEDMS_POTSZ: TStringField
      FieldName = 'MS_POTSZ'
      Size = 10
    end
    object T_MEGSEGEDMS_DOKOD: TStringField
      FieldName = 'MS_DOKOD'
      Size = 5
    end
    object T_MEGSEGEDMS_DOKO2: TStringField
      FieldName = 'MS_DOKO2'
      Size = 5
    end
    object T_MEGSEGEDMS_GKKAT: TStringField
      FieldName = 'MS_GKKAT'
      Size = 5
    end
    object T_MEGSEGEDMS_SNEV1: TStringField
      FieldName = 'MS_SNEV1'
      Size = 40
    end
    object T_MEGSEGEDMS_SNEV2: TStringField
      FieldName = 'MS_SNEV2'
      Size = 40
    end
    object T_MEGSEGEDMS_KIFON: TIntegerField
      FieldName = 'MS_KIFON'
    end
    object T_MEGSEGEDMS_FETED: TStringField
      FieldName = 'MS_FETED'
      Size = 11
    end
    object T_MEGSEGEDMS_FETEI: TStringField
      FieldName = 'MS_FETEI'
      Size = 5
    end
    object T_MEGSEGEDMS_LETED: TStringField
      FieldName = 'MS_LETED'
      Size = 11
    end
    object T_MEGSEGEDMS_LETEI: TStringField
      FieldName = 'MS_LETEI'
      Size = 5
    end
    object T_MEGSEGEDMS_LDARU: TIntegerField
      FieldName = 'MS_LDARU'
    end
    object T_MEGSEGEDMS_FDARU: TIntegerField
      FieldName = 'MS_FDARU'
    end
    object T_MEGSEGEDMS_ID: TAutoIncField
      FieldName = 'MS_ID'
      ReadOnly = True
    end
    object T_MEGSEGEDMS_NORID: TIntegerField
      FieldName = 'MS_NORID'
    end
    object T_MEGSEGEDMS_FERKDAT: TStringField
      FieldName = 'MS_FERKDAT'
      Size = 11
    end
    object T_MEGSEGEDMS_LERKDAT: TStringField
      FieldName = 'MS_LERKDAT'
      Size = 11
    end
    object T_MEGSEGEDMS_FERKIDO: TStringField
      FieldName = 'MS_FERKIDO'
      Size = 5
    end
    object T_MEGSEGEDMS_LERKIDO: TStringField
      FieldName = 'MS_LERKIDO'
      Size = 5
    end
    object T_MEGSEGEDMS_FETEI2: TStringField
      FieldName = 'MS_FETEI2'
      Size = 5
    end
    object T_MEGSEGEDMS_LETEI2: TStringField
      FieldName = 'MS_LETEI2'
      Size = 5
    end
    object T_MEGSEGEDMS_FELIDO2: TStringField
      FieldName = 'MS_FELIDO2'
      Size = 5
    end
    object T_MEGSEGEDMS_LERIDO2: TStringField
      FieldName = 'MS_LERIDO2'
      Size = 5
    end
    object T_MEGSEGEDMS_LOADID: TStringField
      FieldName = 'MS_LOADID'
      Size = 8
    end
    object T_MEGSEGEDMS_M_MAIL: TDateTimeField
      FieldName = 'MS_M_MAIL'
    end
    object T_MEGSEGEDMS_S_MAIL: TDateTimeField
      FieldName = 'MS_S_MAIL'
    end
    object T_MEGSEGEDALVAL: TStringField
      FieldKind = fkLookup
      FieldName = 'ALVAL'
      LookupDataSet = T_MEGBIZAS
      LookupKeyFields = 'mb_mbkod'
      LookupResultField = 'mb_alval'
      KeyFields = 'MS_MBKOD'
      Lookup = True
    end
    object T_MEGSEGEDALNEV: TStringField
      FieldKind = fkLookup
      FieldName = 'ALNEV'
      LookupDataSet = T_MEGBIZAS
      LookupKeyFields = 'mb_mbkod'
      LookupResultField = 'mb_alnev'
      KeyFields = 'MS_MBKOD'
      Lookup = True
    end
    object T_MEGSEGEDALVKOD: TStringField
      FieldKind = fkLookup
      FieldName = 'ALVKOD'
      LookupDataSet = T_MEGBIZAS
      LookupKeyFields = 'mb_mbkod'
      LookupResultField = 'mb_alval'
      KeyFields = 'MS_MBKOD'
      Lookup = True
    end
    object T_MEGSEGEDHUTOS: TIntegerField
      FieldKind = fkLookup
      FieldName = 'HUTOS'
      LookupDataSet = T_MEGBIZAS
      LookupKeyFields = 'mb_mbkod'
      LookupResultField = 'mb_hutos'
      KeyFields = 'MS_MBKOD'
      Lookup = True
    end
    object T_MEGSEGEDFELFUG: TIntegerField
      FieldKind = fkLookup
      FieldName = 'FELFUG'
      LookupDataSet = T_MEGBIZAS
      LookupKeyFields = 'mb_mbkod'
      LookupResultField = 'mb_felfug'
      KeyFields = 'MS_MBKOD'
      Lookup = True
    end
    object T_MEGSEGEDMS_FETED2: TStringField
      FieldName = 'MS_FETED2'
      Size = 11
    end
    object T_MEGSEGEDMS_LETED2: TStringField
      FieldName = 'MS_LETED2'
      Size = 11
    end
    object T_MEGSEGEDMS_FELDAT2: TStringField
      FieldName = 'MS_FELDAT2'
      Size = 11
    end
    object T_MEGSEGEDMS_LERDAT2: TStringField
      FieldName = 'MS_LERDAT2'
      Size = 11
    end
    object T_MEGSEGEDmb_venev: TStringField
      FieldName = 'mb_venev'
      Size = 80
    end
  end
  object Dolgozo: TADOQuery
    Connection = ADOConnectionKozos
    Parameters = <
      item
        Name = 'NEV'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 45
        Value = Null
      end>
    SQL.Strings = (
      
        'SELECT do_kod, do_name, do_email, do_telef from dolgozo where do' +
        '_name= :NEV')
    Left = 344
    Top = 272
    object Dolgozodo_kod: TStringField
      FieldName = 'do_kod'
      Size = 5
    end
    object Dolgozodo_name: TStringField
      FieldName = 'do_name'
      Size = 45
    end
    object Dolgozodo_email: TStringField
      FieldName = 'do_email'
      Size = 60
    end
    object Dolgozodo_telef: TStringField
      FieldName = 'do_telef'
      Size = 40
    end
  end
  object T_MEGBIZAS: TADOQuery
    Connection = ADOConnectionAlap
    Parameters = <>
    SQL.Strings = (
      
        'select mb_mbkod, mb_alval, mb_alnev, mb_hutos, mb_felfug from me' +
        'gbizas')
    Left = 200
    Top = 120
  end
  object Partner: TADOQuery
    Connection = ADOConnectionKozos
    Parameters = <
      item
        Name = 'KOD'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'select * from vevo where v_kod= :KOD')
    Left = 400
    Top = 272
    object PartnerV_KOD: TStringField
      FieldName = 'V_KOD'
      Size = 4
    end
    object PartnerV_NEV: TStringField
      FieldName = 'V_NEV'
      Size = 80
    end
    object PartnerV_IRSZ: TStringField
      FieldName = 'V_IRSZ'
      Size = 10
    end
    object PartnerV_VAROS: TStringField
      FieldName = 'V_VAROS'
      Size = 40
    end
    object PartnerV_CIM: TStringField
      FieldName = 'V_CIM'
      Size = 40
    end
    object PartnerV_TEL: TStringField
      FieldName = 'V_TEL'
      Size = 30
    end
    object PartnerV_FAX: TStringField
      FieldName = 'V_FAX'
      Size = 30
    end
    object PartnerV_BANK: TStringField
      FieldName = 'V_BANK'
      Size = 30
    end
    object PartnerV_ADO: TStringField
      FieldName = 'V_ADO'
      Size = 30
    end
    object PartnerV_KEDV: TBCDField
      FieldName = 'V_KEDV'
      Precision = 14
    end
    object PartnerV_UGYINT: TStringField
      FieldName = 'V_UGYINT'
      Size = 40
    end
    object PartnerV_MEGJ: TStringField
      FieldName = 'V_MEGJ'
      Size = 80
    end
    object PartnerV_LEJAR: TIntegerField
      FieldName = 'V_LEJAR'
    end
    object PartnerV_AFAKO: TStringField
      FieldName = 'V_AFAKO'
      Size = 3
    end
    object PartnerV_NEMET: TIntegerField
      FieldName = 'V_NEMET'
    end
    object PartnerV_TENAP: TIntegerField
      FieldName = 'V_TENAP'
    end
    object PartnerV_ARNAP: TIntegerField
      FieldName = 'V_ARNAP'
    end
    object PartnerV_ORSZ: TStringField
      FieldName = 'V_ORSZ'
      Size = 5
    end
    object PartnerVE_VBANK: TStringField
      FieldName = 'VE_VBANK'
      Size = 5
    end
    object PartnerVE_EUADO: TStringField
      FieldName = 'VE_EUADO'
      Size = 30
    end
    object PartnerV_UPTIP: TIntegerField
      FieldName = 'V_UPTIP'
    end
    object PartnerV_UPERT: TBCDField
      FieldName = 'V_UPERT'
      Precision = 6
      Size = 2
    end
    object PartnerVE_ARHIV: TIntegerField
      FieldName = 'VE_ARHIV'
    end
    object PartnerVE_FELIS: TIntegerField
      FieldName = 'VE_FELIS'
    end
    object PartnerVE_EMAIL: TStringField
      FieldName = 'VE_EMAIL'
      Size = 60
    end
    object PartnerVE_POCIM: TStringField
      FieldName = 'VE_POCIM'
      Size = 40
    end
    object PartnerVE_POVAR: TStringField
      FieldName = 'VE_POVAR'
      Size = 40
    end
    object PartnerVE_PORSZ: TStringField
      FieldName = 'VE_PORSZ'
      Size = 5
    end
    object PartnerVE_PIRSZ: TStringField
      FieldName = 'VE_PIRSZ'
      Size = 10
    end
    object PartnerVE_PFLAG: TIntegerField
      FieldName = 'VE_PFLAG'
    end
    object PartnerVE_PNEV1: TStringField
      FieldName = 'VE_PNEV1'
      Size = 80
    end
    object PartnerVE_PNEV2: TStringField
      FieldName = 'VE_PNEV2'
      Size = 80
    end
    object PartnerVE_PNEV3: TStringField
      FieldName = 'VE_PNEV3'
      Size = 80
    end
    object PartnerVE_VEFEJ: TStringField
      FieldName = 'VE_VEFEJ'
      Size = 60
    end
    object PartnerVE_XMLSZ: TIntegerField
      FieldName = 'VE_XMLSZ'
    end
    object PartnerVE_PKELL: TIntegerField
      FieldName = 'VE_PKELL'
    end
    object PartnerVE_LANID: TIntegerField
      FieldName = 'VE_LANID'
    end
    object PartnerVE_FTELJ: TIntegerField
      FieldName = 'VE_FTELJ'
    end
    object PartnerVE_TIMO: TStringField
      FieldName = 'VE_TIMO'
      Size = 6
    end
    object PartnerV_EMAIL: TStringField
      FieldName = 'V_EMAIL'
      Size = 60
    end
  end
  object Poziciok: TADOQuery
    Connection = ADOConnectionKozos
    Parameters = <
      item
        Name = 'RENSZ'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *  FROM poziciok WHERE po_rensz= :RENSZ')
    Left = 176
    Top = 272
    object PoziciokPO_RENSZ: TStringField
      FieldName = 'PO_RENSZ'
      Size = 10
    end
    object PoziciokPO_GEKOD: TStringField
      FieldName = 'PO_GEKOD'
      Size = 200
    end
    object PoziciokPO_DATUM: TStringField
      FieldName = 'PO_DATUM'
    end
    object PoziciokPO_SEBES: TIntegerField
      FieldName = 'PO_SEBES'
    end
    object PoziciokPO_DIGIT: TStringField
      FieldName = 'PO_DIGIT'
      Size = 8
    end
    object PoziciokPO_SZELE: TBCDField
      FieldName = 'PO_SZELE'
      Precision = 15
      Size = 13
    end
    object PoziciokPO_HOSZA: TBCDField
      FieldName = 'PO_HOSZA'
      Precision = 15
      Size = 13
    end
    object PoziciokPO_DAT: TDateTimeField
      FieldName = 'PO_DAT'
    end
    object PoziciokPO_IRANYSZOG: TSmallintField
      FieldName = 'PO_IRANYSZOG'
    end
    object PoziciokPO_UDAT: TDateTimeField
      FieldName = 'PO_UDAT'
    end
    object PoziciokPO_DIGIT2: TSmallintField
      FieldName = 'PO_DIGIT2'
    end
    object PoziciokPO_ETIPUS: TSmallintField
      FieldName = 'PO_ETIPUS'
    end
    object PoziciokPO_VALID: TSmallintField
      FieldName = 'PO_VALID'
    end
  end
  object MasikRio: THTTPRIO
    WSDLLocation = 'C:\FUVAR_ADO\AKI\mnb.wsdl'
    HTTPWebNode.Agent = 'Borland SOAP 1.2'
    HTTPWebNode.UseUTF8InHeader = True
    HTTPWebNode.InvokeOptions = [soIgnoreInvalidCerts, soAutoCheckAccessPointViaUDDI]
    HTTPWebNode.WebNodeOptions = []
    Converter.Options = [soSendMultiRefObj, soTryAllSchema, soRootRefNodesToBody, soCacheMimeResponse, soUTF8EncodeXML]
    Left = 192
    Top = 176
  end
  object qUNI: TADOQuery
    Connection = ADOConnectionAlap
    Parameters = <
      item
        Name = 'KOD'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 5
        Value = Null
      end>
    SQL.Strings = (
      'SELECT JA_FAZIS FROM JARAT WHERE JA_KOD = :KOD')
    Left = 552
    Top = 80
  end
  object ADOConnectionArfolyam: TADOConnection
    LoginPrompt = False
    Provider = 'SQLOLEDB.1'
    Left = 600
    Top = 432
  end
  object Query1: TADOQuery
    Connection = ADOConnectionAlap
    Parameters = <
      item
        Name = 'KOD'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 5
        Value = Null
      end>
    SQL.Strings = (
      'SELECT JA_FAZIS FROM JARAT WHERE JA_KOD = :KOD')
    Left = 552
    Top = 16
  end
  object qUNIArfolyam: TADOQuery
    Connection = ADOConnectionArfolyam
    Parameters = <
      item
        Name = 'KOD'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 5
        Value = Null
      end>
    SQL.Strings = (
      'SELECT JA_FAZIS FROM JARAT WHERE JA_KOD = :KOD')
    Left = 552
    Top = 144
  end
  object Query2: TADOQuery
    Connection = ADOConnectionAlap
    Parameters = <
      item
        Name = 'KOD'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 5
        Value = Null
      end>
    SQL.Strings = (
      'SELECT JA_FAZIS FROM JARAT WHERE JA_KOD = :KOD')
    Left = 600
    Top = 16
  end
  object QueryMain: TADOQuery
    Connection = ADOConnectionAlap
    Parameters = <
      item
        Name = 'KOD'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 5
        Value = Null
      end>
    SQL.Strings = (
      'SELECT JA_FAZIS FROM JARAT WHERE JA_KOD = :KOD')
    Left = 656
    Top = 16
  end
  object ADOConnectionMulti: TADOConnection
    LoginPrompt = False
    Provider = 'SQLOLEDB.1'
    Left = 488
    Top = 328
  end
  object QueryMulti: TADOQuery
    Tag = 1
    Connection = ADOConnectionMulti
    CursorType = ctStatic
    Parameters = <>
    Left = 489
    Top = 376
  end
  object QueryMulti2: TADOQuery
    Tag = 1
    Connection = ADOConnectionMulti
    CursorType = ctStatic
    Parameters = <>
    Left = 489
    Top = 432
  end
  object QueryMain_long: TADOQuery
    Connection = ADOConnectionAlap
    CommandTimeout = 120
    Parameters = <
      item
        Name = 'KOD'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 5
        Value = Null
      end>
    SQL.Strings = (
      'SELECT JA_FAZIS FROM JARAT WHERE JA_KOD = :KOD')
    Left = 656
    Top = 80
  end
  object ClientSocket1: TClientSocket
    Active = False
    ClientType = ctNonBlocking
    Port = 0
    Left = 288
    Top = 536
  end
end
