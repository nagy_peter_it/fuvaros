unit UkmFriss;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, DB, ADODB;

type
  TFUkmFriss = class(TForm)
    LabelU1: TLabel;
    LabelU2: TLabel;
    LabelU3: TLabel;
    ComboBoxU1: TComboBox;
    ComboBoxU2: TComboBox;
    ComboBoxU3: TComboBox;
    BitUElkuld: TBitBtn;
    BitUKilep: TBitBtn;
    Label1: TLabel;
    Query1: TADOQuery;
    Query2: TADOQuery;
    procedure FormCreate(Sender: TObject);
    procedure BitUKilepClick(Sender: TObject);
    procedure BitUElkuldClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FUkmFriss: TFUkmFriss;

implementation

uses Egyeb,J_SQL,Kozos;

{$R *.dfm}

procedure TFUkmFriss.FormCreate(Sender: TObject);
var
  i: integer;
begin
  ComboBoxU1.Items.Clear;
  for i:=2013 to StrToInt(EgyebDlg.EvSzam )  do
  begin
    ComboBoxU1.Items.Add(IntToStr(i));
  end;
end;

procedure TFUkmFriss.BitUKilepClick(Sender: TObject);
begin
  close;
end;

procedure TFUkmFriss.BitUElkuldClick(Sender: TObject);
var
  ureskm: string;
  kedat		: string;
  vedat		: string;
  rendsz,ret_kod: string;
  db: integer;
begin
  Query1.SQL.Add('select * from zarokm where zk_ev1 ='+ComboBoxU1.Text+' and zk_ho1>='+ComboBoxU3.Text+' and zk_ho1<='+ComboBoxU3.Text+' order by zk_ev1,zk_ho1,zk_rensz' )   ;
  Query1.Open;
  Query1.First;
  db:=0;
  while not Query1.Eof do
  begin
    inc(db);
    kedat:=copy(Query1.FieldByName('ZK_DATUM').asstring,1,8)+'01.';
    vedat:=Query1.FieldByName('ZK_DATUM').asstring;
    rendsz:=Query1.FieldByName('ZK_RENSZ').asstring;
    ret_kod:=Query1.FieldByName('ZK_ZKKOD').asstring;
    Label1.Caption:=vedat+' '+rendsz+' '+IntToStr(db);
    Label1.Update;

    Query_Run(Query2, 'SELECT sum(cast( ja_uresk as integer)) OSSZ_U_KM  FROM jarat WHERE ja_rendsz ='''+rendsz+''' and JA_JKEZD >= '''+kedat+''' AND JA_JKEZD <= '''+vedat+'''  AND JA_FAZIS <> 2' );

    ureskm:=Query2.FieldByName('OSSZ_U_KM').AsString   ;

    Query_Update (Query2, 'ZAROKM',
    	[
     	'ZK_URESKM', SqlSzamString(StringSzam(ureskm), 12, 0)
     	], 	' WHERE ZK_ZKKOD = '+ret_kod);


    Query1.Next;
  end;
  Query1.Close;
  ShowMessage('K�sz!');

end;

end.
