unit OsztrakRako;

interface

uses
	SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
	Forms, Dialogs, StdCtrls, Buttons, Mask, DB, DBTables, J_ALFORM, ADODB,
  DateUtils;

type
	TOsztrakRakoDlg = class(TJ_AlformDlg)
	BitKilep: TBitBtn;
    Label8: TLabel;
    Label1: TLabel;
    meErkezes: TMaskEdit;
    meLink: TMemo;
    Query1: TADOQuery;
    BitBtn1: TBitBtn;
    Label2: TLabel;
    M1: TMaskEdit;
    Label3: TLabel;
    meErkezesIdo: TMaskEdit;
    Label5: TLabel;
    meTavozas: TMaskEdit;
    meTavozasIdo: TMaskEdit;
    meIdotartam: TMemo;
    meRakodas: TMaskEdit;
    Label7: TLabel;
    BitBtn2: TBitBtn;
    meRakodasIdo: TMaskEdit;
    meEllenor: TMaskEdit;
    Label4: TLabel;
    meEllenorKod: TMaskEdit;
	 procedure BitKilepClick(Sender: TObject);
	 procedure BitElkuldClick(Sender: TObject);
	 procedure MaskEditClick(Sender: TObject);
	 procedure Tolto(cim, kod : string); override;
    procedure FormCreate(Sender: TObject);
    procedure meErkezesExit(Sender: TObject);
    procedure meTavozasExit(Sender: TObject);
    procedure meErkezesIdoExit(Sender: TObject);
    procedure meTavozasIdoExit(Sender: TObject);
    procedure IdotartamFrissit;
    procedure BitBtn2Click(Sender: TObject);
	private
    ElozoErkezes, ElozoErkezesIdo, ElozoTavozas, ElozoTavozasIdo: string;
	public
	end;

var
	OsztrakRakoDlg: TOsztrakRakoDlg;

implementation

uses
 Egyeb, J_SQL, Kozos, Clipbrd;

{$R *.DFM}


procedure TOsztrakRakoDlg.BitKilepClick(Sender: TObject);
begin
	ret_kod := '';
	Close;
end;

procedure TOsztrakRakoDlg.FormCreate(Sender: TObject);
begin
	EgyebDlg.SeTADOQueryDatabase(Query1);
end;

procedure TOsztrakRakoDlg.Tolto(cim, kod : string);
var
  EllenorKod: string;
begin
	// SetMaskEdits([M1]);
	ret_kod  := kod;
	M1.Text		:= ret_kod;
	if ret_kod <> '' then begin
		if Query_Run (Query1, 'SELECT * FROM ATJELENT WHERE AT_MSID = '+ret_kod ,true) then begin
      meRakodas.Text:= Query1.FieldByname('AT_RAKDAT').AsString;
      meRakodasIdo.Text:= Query1.FieldByname('AT_RAKIDO').AsString;
			meErkezes.Text:= Query1.FieldByname('AT_ERKDAT').AsString;
			meErkezesIdo.Text	:= Query1.FieldByname('AT_ERKIDO').AsString;
      meTavozas.Text	:= Query1.FieldByname('AT_TAVDAT').AsString;
      meTavozasIdo.Text	:= Query1.FieldByname('AT_TAVIDO').AsString;
      meIdotartam.Text	:= Query1.FieldByname('AT_IDOTART').AsString;
      ElozoErkezes:= meErkezes.Text;
      ElozoErkezesIdo:= meErkezesIdo.Text;
      ElozoTavozas:= meTavozas.Text;
      ElozoTavozasIdo:= meTavozasIdo.Text;

      EllenorKod:= Query1.FieldByname('AT_ELLENOR').AsString;
			meLink.Text	:= Query1.FieldByname('AT_LINK').AsString;
		  end; // if
 		if Query_Run (Query1, 'SELECT JE_FENEV FROM JELSZO WHERE JE_FEKOD = '''+EllenorKod+'''',true) then begin
      meEllenor.Text:= Query1.FieldByname('JE_FENEV').AsString;
		  end; // if
   end;  // if
end;

procedure TOsztrakRakoDlg.BitBtn2Click(Sender: TObject);
var
  RakoIdo: TDateTime;
begin
  RakoIdo:= DateTimeToNum(meRakodas.Text, meRakodasIdo.Text);
  meErkezes.Text:= FormatDateTime('yyyy.mm.dd.', IncMinute(RakoIdo, -60));
  meErkezesIdo.Text:= FormatDateTime('HH:mm', IncMinute(RakoIdo, -60));
  meTavozas.Text:= FormatDateTime('yyyy.mm.dd.', IncMinute(RakoIdo, 60));
  meTavozasIdo.Text:= FormatDateTime('HH:mm', IncMinute(RakoIdo, 60));
  IdotartamFrissit;
  meEllenor.Text:= EgyebDlg.user_name;
end;

procedure TOsztrakRakoDlg.BitElkuldClick(Sender: TObject);
var
  UjRKID: integer;
  TeruletKod, UjKod, S: string;
begin
 {	if meMegjegyzes.Text = '' then begin
		NoticeKi('Magyarázat megadása kötelező!');
		meMegjegyzes.Setfocus;
		Exit;
  	end;}
  Query_Update (Query1, 'ATJELENT',
      [
      'AT_ERKDAT',  ''''+meErkezes.Text+'''',
      'AT_ERKIDO',  ''''+meErkezesIdo.Text+'''',
      'AT_TAVDAT',  ''''+meTavozas.Text+'''',
      'AT_TAVIDO',  ''''+meTavozasIdo.Text+'''',
      'AT_IDOTART',  ''''+meIdotartam.Text+'''',
      'AT_LINK',  ''''+meLink.Text+'''',
      'AT_ELLENOR',  ''''+meEllenorKod.Text+''''
      ], ' WHERE AT_MSID='+ret_kod);
  Close;
end;

procedure TOsztrakRakoDlg.MaskEditClick(Sender: TObject);
begin
	with Sender as TMaskEdit do begin
  	SelectAll;
  end;
end;


procedure TOsztrakRakoDlg.meErkezesExit(Sender: TObject);
begin
   DatumExit(Sender, false);
   if meErkezes.Text <> ElozoErkezes then begin
     IdotartamFrissit;
     ElozoErkezes:= meErkezes.Text;
     end;
end;

procedure TOsztrakRakoDlg.meErkezesIdoExit(Sender: TObject);
begin
   	IdoExit(Sender, false);
    if meErkezesIdo.Text <> ElozoErkezesIdo then begin
      IdotartamFrissit;
      ElozoErkezesIdo:= meErkezesIdo.Text;
      end;
end;

procedure TOsztrakRakoDlg.meTavozasExit(Sender: TObject);
begin
   DatumExit(Sender, false);
    if meTavozas.Text <> ElozoTavozas then begin
      IdotartamFrissit;
      ElozoTavozas:= meTavozas.Text;
      end;
end;

procedure TOsztrakRakoDlg.meTavozasIdoExit(Sender: TObject);
begin
   	IdoExit(Sender, false);
    if meTavozasIdo.Text <> ElozoTavozasIdo then begin
      IdotartamFrissit;
      ElozoTavozasIdo:= meTavozasIdo.Text;
      end;
end;

procedure TOsztrakRakoDlg.IdotartamFrissit;
var
  DiffOra: double;
begin
  DiffOra:= DateTimeDifferenceSec(meTavozas.Text, meTavozasIdo.Text, meErkezes.Text, meErkezesIdo.Text)/3600;
  meIdotartam.Text:= formatfloat('0', DiffOra); // egészre kerekítve
  meEllenor.Text:= EgyebDlg.user_name;
  meEllenorKod.Text:= EgyebDlg.user_code;
end;

end.




