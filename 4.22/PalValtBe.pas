unit PalValtBe;

interface

uses
	SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
	Forms, Dialogs, StdCtrls, Buttons, Mask, DB, DBTables, J_ALFORM, ADODB,
  Vcl.ExtCtrls, Vcl.ComCtrls;

type
	TPalValtbeDlg = class(TJ_AlformDlg)
	BitElkuld: TBitBtn;
	BitKilep: TBitBtn;
	Label8: TLabel;
	Label12: TLabel;
    M1: TMaskEdit;
    BitBtn7: TBitBtn;
    Query1: TADOQuery;
    Label3: TLabel;
    M2: TMaskEdit;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    TabSheet3: TTabSheet;
    M12: TMaskEdit;
    Label1: TLabel;
    CB11: TComboBox;
    Label7: TLabel;
    Label4: TLabel;
    CB12: TComboBox;
    Label2: TLabel;
    M11: TMaskEdit;
    Label5: TLabel;
    CB21: TComboBox;
    Label6: TLabel;
    M21: TMaskEdit;
    Label10: TLabel;
    M23: TMaskEdit;
    Label11: TLabel;
    M22: TMaskEdit;
    CB31: TComboBox;
    Label13: TLabel;
    M32: TMaskEdit;
    Label14: TLabel;
    M34: TMaskEdit;
    Label15: TLabel;
    M33: TMaskEdit;
    Label16: TLabel;
    Label9: TLabel;
    M31: TMaskEdit;
    RadioGroup1: TRadioGroup;
    CheckBox1: TCheckBox;
	procedure BitKilepClick(Sender: TObject);
	procedure FormCreate(Sender: TObject);
	procedure Tolto(cim : string; teko : string);override;
	procedure BitElkuldClick(Sender: TObject);
	procedure FormDestroy(Sender: TObject);
  procedure MaskEditClick(Sender: TObject);
  procedure MaskEditExit(Sender: TObject);
	procedure MaskEditKeyPress(Sender: TObject; var Key: Char);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure M1Exit(Sender: TObject);
    procedure M11Exit(Sender: TObject);
    procedure M11KeyPress(Sender: TObject; var Key: Char);
    procedure BitBtn7Click(Sender: TObject);
    procedure M2Exit(Sender: TObject);
    procedure Raszamolo;
    procedure CB21Change(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
	private
		helylist		: TStringList;
	public
	end;

var
	PalValtbeDlg: TPalValtbeDlg;

implementation

uses
	Egyeb, J_SQL, Kozos;
{$R *.DFM}

procedure TPalValtbeDlg.BitKilepClick(Sender: TObject);
begin
   ret_kod := '';
   Close;
end;

procedure TPalValtbeDlg.CB21Change(Sender: TObject);
begin
   Raszamolo;
end;

procedure TPalValtbeDlg.FormCreate(Sender: TObject);
begin
 	EgyebDlg.SetADOQueryDatabase(Query1);
	ret_kod 	:= '';
  	helylist 	:= TStringList.Create;
   CB11.Clear;
   helylist.Add('');
   CB11.Items.Add('');
   Query_Run (Query1, 'SELECT * FROM SZOTAR WHERE SZ_FOKOD = ''140'' ',true);
   while not Query1.Eof do begin
       helylist.Add('0'+Query1.FieldByName('SZ_ALKOD').AsString);
       CB11.Items.Add(Query1.FieldByName('SZ_MENEV').AsString);
       Query1.Next;
   end;
//   Query_Run (Query1, 'SELECT GK_RESZ FROM GEPKOCSI WHERE GK_POTOS = 1 AND GK_ARHIV = 0 ORDER BY GK_RESZ',true);
   Query_Run (Query1, 'SELECT GK_RESZ FROM GEPKOCSI WHERE GK_POTOS = 1 ORDER BY GK_RESZ',true);
   while not Query1.Eof do begin
       helylist.Add('1'+Query1.FieldByName('GK_RESZ').AsString);
       CB11.Items.Add(Query1.FieldByName('GK_RESZ').AsString);
       Query1.Next;
   end;
   CB12.Items.Assign(CB11.Items);
   CB21.Items.Assign(CB11.Items);
   CB31.Items.Assign(CB11.Items);
  	CB11.ItemIndex := 0;
  	CB12.ItemIndex := 0;
  	CB21.ItemIndex := 0;
  	CB31.ItemIndex := 0;
   PageControl1.ActivePageIndex    := 0;
   SetMaskEdits([M22]);
end;

procedure TPalValtbeDlg.Tolto(cim : string; teko:string);
var
   kod : string;
begin
	ret_kod     := teko;
	Caption     := cim;
	M1.Text     := EgyebDlg.MaiDatum;
   M2.Text     := FormatDateTime('hh:nn', now);
	if ret_kod <> '' then begin		{M�dos�t�s}
		Query_Run (Query1, 'SELECT * FROM PALVALT WHERE PV_PVKOD = '+ret_kod,true);
       if Query1.RecordCount > 0 then begin
           PageControl1.ActivePageIndex    := StrToIntdef(Query1.FieldByName('PV_TIPUS').AsString, 0);
			M1.Text 	:= Query1.FieldByName('PV_DATUM').AsString;
			M2.Text 	:= Query1.FieldByName('PV_IDOPT').AsString;
           case PageControl1.ActivePageIndex of
               0 :         // v�ltoz�s
               begin
                   PageControl1.Pages[1].Tabvisible := false;
                   PageControl1.Pages[2].Tabvisible := false;
                   //CB11
                   kod := Query1.FieldByName('PV_HETI1').AsString+ Query1.FieldByName('PV_HEKO1').AsString;
			        ComboSet (CB11, kod, helylist);
                   //CB12
                   kod := Query1.FieldByName('PV_HETI2').AsString+ Query1.FieldByName('PV_HEKO2').AsString;
			        ComboSet (CB12, kod, helylist);
                   CheckBox1.Checked   := ( StrToIntDef(Query1.FieldByName('PV_CSERE').AsString, 0) > 0 );
                   M11.Text    := Query1.FieldByName('PV_MENNY').AsString;
                   M12.Text    := Query1.FieldByName('PV_MEGJE').AsString;
               end;
               1 :         // lelt�r
               begin
                   PageControl1.Pages[0].Tabvisible := false;
                   PageControl1.Pages[2].Tabvisible := false;
                   //CB21
                   kod := Query1.FieldByName('PV_HETI1').AsString+ Query1.FieldByName('PV_HEKO1').AsString;
			        ComboSet (CB21, kod, helylist);
                   M21.Text    := Query1.FieldByName('PV_MENNY').AsString;
                   M23.Text    := Query1.FieldByName('PV_MEGJE').AsString;
                   // A rakt�ri mennyis�g kisz�m�t�sa d�tum/id�pont alapj�n
               end;
               2 :         // megb�z�s
               begin
                   PageControl1.Pages[0].Tabvisible := false;
                   PageControl1.Pages[1].Tabvisible := false;
                   //CB31
                   kod := Query1.FieldByName('PV_HETI1').AsString+ Query1.FieldByName('PV_HEKO1').AsString;
			        ComboSet (CB31, kod, helylist);
                   M31.Text    := Query1.FieldByName('PV_MSKOD').AsString;
                   M32.Text    := Query1.FieldByName('PV_MSFEL').AsString;
                   M33.Text    := Query1.FieldByName('PV_MSLER').AsString;
                   M34.Text    := Query1.FieldByName('PV_MEGJE').AsString;
                   CB31.Enabled        := false;
                   M31.Enabled         := false;
                   Radiogroup1.Enabled := false;
               end;
           end;
		end;
   end else begin
       // �j felviteln�l nincs megb�z�s
       PageControl1.Pages[1].Tabvisible := true;
       PageControl1.Pages[2].Tabvisible := true;
       PageControl1.Pages[2].Tabvisible := false;
	end;
end;

procedure TPalValtbeDlg.BitElkuldClick(Sender: TObject);
var
  ujkodszam 	: integer;
  tipus        : integer;
  heti1        : integer;
  heti2        : integer;
  heko1        : string;
  heko2        : string;
  menny        : integer;
  megje        : string;
begin
	if not Jodatum2(M1 ) then begin
		NoticeKi('Hib�s d�tum megad�sa!');
		M1.Text := '';
		M1.Setfocus;
		Exit;
	end;
	if ret_kod = '' then begin   // �j rekord felvitele
		ujkodszam	:= GetNextCode('PALVALT', 'PV_PVKOD', 1, 5);
		ret_kod	    := IntToStr(ujkodszam);
		Query_Run ( Query1, 'INSERT INTO PALVALT (PV_PVKOD) VALUES ('+ret_kod+')',true);
	  end;
   tipus   := PageControl1.ActivePageIndex;
   heti1   := 0;
   heti2   := 0;
   heko1   := '';
   heko2   := '';
   case PageControl1.ActivePageIndex of
       0 :         // v�ltoz�s
       begin
           heti1   := StrToIntDef(copy( helylist[CB11.ItemIndex], 1, 1), 0);
           heko1   := copy( helylist[CB11.ItemIndex], 2, 999);
           heti2   := StrToIntDef(copy( helylist[CB12.ItemIndex], 1, 1), 0);
           heko2   := copy( helylist[CB12.ItemIndex], 2, 999);
           menny   := STrToIntdef(M11.Text, 0);
           megje   := M12.Text;
       end;
       1 :         // lelt�r
       begin
           heti1   := StrToIntDef(copy( helylist[CB21.ItemIndex], 1, 1), 0);
           heko1   := copy( helylist[CB21.ItemIndex], 2, 999);
           menny   := STrToIntdef(M21.Text, 0);
           megje   := M23.Text;
       end;
       2 :         // megb�z�s
       begin
           heti1   := StrToIntDef(copy( helylist[CB31.ItemIndex], 1, 1), 0);
           heko1   := copy( helylist[CB31.ItemIndex], 2, 999);
           menny   := STrToIntdef(M31.Text, 0) - STrToIntdef(M32.Text, 0);
           megje   := M34.Text;
       end;
  end;

   Query_Update(Query1, 'PALVALT',
         [
         'PV_DATUM', ''''+M1.Text+'''',
         'PV_IDOPT', ''''+M2.Text+'''',
         'PV_TIPUS', IntToStr(tipus),
         'PV_HETI1', IntToStr(heti1),
         'PV_HEKO1', ''''+heko1+'''',
         'PV_HETI2', IntToStr(heti2),
         'PV_HEKO2', ''''+heko2+'''',
         'PV_MENNY', IntToStr(menny),
         'PV_CSERE', BoolToStr01(CheckBox1.Checked),
//         'PV_MSKOD', mskod,
         'PV_MSTIP', IntToStr(RadioGroup1.ItemIndex),
         'PV_MSFEL', IntToStr(StrToIntDef(M32.Text, 0)),
         'PV_MSLER', IntToStr(
         StrToIntDef(M32.Text, 0)),
         'PV_MEGJE', ''''+megje+''''
         ], ' WHERE PV_PVKOD = '+ret_kod );
	Query1.Close;
	Close;
end;

procedure TPalValtbeDlg.FormDestroy(Sender: TObject);
begin
  helylist.Free;
end;

procedure TPalValtbeDlg.MaskEditClick(Sender: TObject);
begin
	with Sender as TMaskEdit do begin
	SelectAll;
  end;
end;

procedure TPalValtbeDlg.MaskEditExit(Sender: TObject);
begin
	with Sender as TMaskEdit do begin
		Text := StrSzamStr(Text,12,4);
	end;
end;

procedure TPalValtbeDlg.MaskEditKeyPress(Sender: TObject; var Key: Char);
begin
  with Sender as TMaskEdit do begin
		Key := EgyebDlg.Jochar(Text,Key,12,4);
  end;
end;

procedure TPalValtbeDlg.PageControl1Change(Sender: TObject);
begin
   Raszamolo;
end;

procedure TPalValtbeDlg.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
	EgyebDlg.FormReturn(Key);
end;

procedure TPalValtbeDlg.M1Exit(Sender: TObject);
begin
	if ( ( M1.Text <> '' ) and ( not Jodatum2(M1 ) ) ) then begin
		NoticeKi('Hib�s d�tum megad�sa!');
		M1.Text := '';
		M1.Setfocus;
	end;
   Raszamolo;
end;

procedure TPalValtbeDlg.M2Exit(Sender: TObject);
begin
   IdoExit(Sender, true);
   Raszamolo;
end;

procedure TPalValtbeDlg.M11Exit(Sender: TObject);
begin
	with Sender as TMaskEdit do begin
  		Text := StrSzamStr(Text,5,0);
  	end;
end;

procedure TPalValtbeDlg.M11KeyPress(Sender: TObject; var Key: Char);
begin
  	with Sender as TMaskEdit do begin
  		Key := EgyebDlg.Jochar(Text,Key,5,0);
  	end;
end;

procedure TPalValtbeDlg.BitBtn7Click(Sender: TObject);
begin
	Calendarbe(M1);
end;

procedure TPalValtbeDlg.Raszamolo;
begin
   // A rakt�ri mennyis�g kisz�mol�sa -> lelt�r eset�n
   if PageControl1.ActivePageIndex  <> 1 then begin
      Exit;
   end;
   M22.Text    := IntToStr(GetPaletta(helylist[CB21.ItemIndex], M1.Text, M2.Text));
end;

end.
