unit KoltImpUjFm;

interface

uses
	SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
	StdCtrls, Forms, ExtCtrls, Dialogs, J_FOFORMUJ, Buttons, Jaratbe, Data.DB,
  Data.Win.ADODB, Vcl.Menus, Vcl.Mask, Vcl.Grids, Math;

const
   HIBASTR     = '';

type
	TKoltImpUjFmDlg = class(TJ_FOFORMUJDLG)
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    PopupMenu1: TPopupMenu;
    Id1: TMenuItem;
    Dtum1: TMenuItem;
    M0: TMaskEdit;
    BitBtn3: TBitBtn;
    QKoltseg: TADOQuery;
    Sorfeldolgozsa1: TMenuItem;
    SG1: TStringGrid;
    Query3: TADOQuery;
    QJarat: TADOQuery;
    QKozos01: TADOQuery;
    QAlap01: TADOQuery;
    M2: TMaskEdit;
    BitBtn6: TBitBtn;
    M3: TMaskEdit;
    BitBtn7: TBitBtn;
    BitBtn9: TBitBtn;
    BitBtn8: TBitBtn;
    Query4: TADOQuery;
    BitEllen: TBitBtn;
    BitBtn4: TBitBtn;
    BitBtn5: TBitBtn;
    dummy_jaratkod: TMaskEdit;
    dummy_jaratszam: TMaskEdit;
	  procedure FormCreate(Sender: TObject);override;
    procedure Adatlap(cim, kod : string);override;
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure Id1Click(Sender: TObject);
    procedure Dtum1Click(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure Sorfeldolgozsa1Click(Sender: TObject);
    procedure Feldolgozas(kod : string);
    procedure FormResize(Sender: TObject);
    procedure BitBtn6Click(Sender: TObject);
    procedure BitBtn7Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
	 procedure ButtonTorClick(Sender: TObject); override;
    procedure BitEllenClick(Sender: TObject);
    procedure BitBtn4Click(Sender: TObject);
    procedure BitBtn5Click(Sender: TObject);
    procedure Jarat;
  private
    kell_datum_szures: boolean;
   beodat  : string;
end;

var
	KoltImpUjFmDlg : TKoltImpUjFmDlg;

implementation

uses
	Egyeb, J_SQL, KoltsegImport2, Kozos, LGauge, Koltimpujbe, Ellenorbe, J_Valaszto;

{$R *.DFM}

procedure TKoltImpUjFmDlg.BitBtn1Click(Sender: TObject);
begin
   // Megh�vjuk az �j k�lts�gimportot
   Application.CreateForm(TKoltsegImport2Dlg, KoltsegImport2Dlg);
	KoltsegImport2Dlg.ShowModal;
	KoltsegImport2Dlg.Destroy;
   ModLocate (Query1, FOMEZO, Query1.FieldByName(FOMEZO).AsString );
end;

procedure TKoltImpUjFmDlg.BitBtn2Click(Sender: TObject);
var
   fn      : string;
   ossz1   : double;
   ossz2   : double;
   recno   : integer;
   vistr   : string;
begin
   // Visszavonjuk a kijel�lt �llom�ny m�veleteit
   if Query1.FieldByName('KS_IMPFN').AsString = '' then begin
       NoticeKi('Nincs visszavonand� t�tel!');
       Exit;
   end;
   fn  := Query1.FieldByName('KS_IMPFN').AsString;
   if NoticeKi('Val�ban vissza k�v�nja vonni a(z) '+fn+' k�lts�gimport beolvas�st?', NOT_QUESTION) <> 0 then begin
       Exit;
   end;
   if NoticeKi('AMIT EZZEL AZ IMPORT �LLOM�NNYAL CSIN�LT�L, AZT KEZDHETED MAJD EL�LR�L. BIZTOS HOGY EZT AKAROD?', NOT_QUESTION) <> 0 then begin
       Exit;
   end;
   Screen.Cursor   := crHourGlass;
   Query_Run(Query2, 'SELECT DISTINCT KS_BEODAT, KS_FIMOD FROM KOLTSEG_GYUJTO WHERE KS_IMPFN = '''+fn+''' ');
   beodat  := Query2.FieldByName('KS_BEODAT').AsString;
   if beodat <> '' then begin
       // Ide j�het m�g egy csom� �sszeg �s darabsz�m ellen�rz�s
       Query_Run(Query2, 'DELETE FROM KOLTSEG WHERE KS_BEODAT = '''+beodat+''' AND KS_FIMOD = '''+Query2.FieldByName('KS_FIMOD').AsString+''' ');
       Query_Run(Query2, 'DELETE FROM KOLTSEG_GYUJTO WHERE KS_IMPFN = '''+fn+''' ');
       Noticeki('A visszavon�s megt�rt�nt!');
   end else begin
       Noticeki('A beolvas�si d�tum �res! Nincs visszavon�s!');
   end;

   ModLocate (Query1, FOMEZO, Query1.FieldByName(FOMEZO).AsString );
   Screen.Cursor   := crDefault;

end;

procedure TKoltImpUjFmDlg.BitBtn3Click(Sender: TObject);
begin
   Feldolgozas('');
end;

procedure TKoltImpUjFmDlg.BitBtn4Click(Sender: TObject);
var
   fn      : string;
   ossz1   : double;
   ossz2   : double;
   recno   : integer;
   vistr   : string;
begin
   // Csak a k�lts�geket vonjuk vissza
   if Query1.FieldByName('KS_IMPFN').AsString = '' then begin
       NoticeKi('Nincs visszavonand� t�tel!');
       Exit;
   end;
   fn  := Query1.FieldByName('KS_IMPFN').AsString;
   Query_Run(Query2, 'SELECT * FROM KOLTSEG WHERE KS_KTKOD IN (SELECT KS_KTKOD2 FROM KOLTSEG_GYUJTO WHERE KS_IMPFN = '''+fn+''' )');
   if Query2.RecordCount = 0 then begin
       NoticeKi('M�g nem ker�ltek be az adatok a k�lts�g t�bl�ba!');
       Exit;
   end;
   if NoticeKi('Val�ban vissza k�v�nja vonni a(z) '+fn+' �llom�nyhoz tartoz� k�lts�geket?', NOT_QUESTION) <> 0 then begin
       Exit;
   end;
   Screen.Cursor   := crHourGlass;
   Query_Run(Query2, 'SELECT DISTINCT KS_BEODAT, KS_FIMOD FROM KOLTSEG_GYUJTO WHERE KS_IMPFN = '''+fn+''' ');
   beodat  := Query2.FieldByName('KS_BEODAT').AsString;
   if beodat <> '' then begin
       // Ide j�het m�g egy csom� �sszeg �s darabsz�m ellen�rz�s
       Query_Run(Query2, 'DELETE FROM KOLTSEG WHERE KS_BEODAT = '''+beodat+''' AND KS_FIMOD = '''+Query2.FieldByName('KS_FIMOD').AsString+''' ');
       // Ki�r�tj�k a k�lts�g gy�jt� mez�ket is
       Query_Run(Query2, 'UPDATE KOLTSEG_GYUJTO SET KS_JAKOD = '''', KS_JARAT = '''', KS_KTKOD2 = 0 WHERE KS_IMPFN = '''+fn+''' ');
       Noticeki('A visszavon�s megt�rt�nt!');
   end else begin
       Noticeki('A beolvas�si d�tum �res! Nincs visszavon�s!');
   end;

   ModLocate (Query1, FOMEZO, Query1.FieldByName(FOMEZO).AsString );
   Screen.Cursor   := crDefault;

end;

procedure TKoltImpUjFmDlg.BitBtn5Click(Sender: TObject);
begin
  Jarat;
end;

procedure TKoltImpUjFmDlg.Jarat;
var
	szurosor, i, ujkod	: integer;
  arfolyam,osszeg,ertek: double;
  udatum, uido, valnem, rendsz, skod,snev, jarkod, jakod,megj,leiras, fimod,orsz,tipus, sarfolyam, sertek,sosszeg: string;
begin
	if (Query1.FieldByName('KS_JAKOD').AsString <> '') or
        ((Query1.FieldByName('KS_JARAT').AsString <> '') and (copy(Query1.FieldByName('KS_JARAT').AsString,1,1) <> '!')) then begin    // j�rat megmutat�sa
    jakod:= Query1.FieldByName('KS_JAKOD').AsString;
    if jakod = '' then begin   // Ellen�rz�tt a j�rat, ez�rt nincs feldolgozva
      jakod:= Query_Select('KOLTSEG','KS_JARAT', Query1.FieldByName('KS_JARAT').AsString, 'KS_JAKOD');
      end;
		Application.CreateForm(TJaratbeDlg, JaratbeDlg);
    JaratbeDlg.megtekint:=True;
		JaratbeDlg.Tolto('J�rat megekint�s',jakod);
		JaratbeDlg.ShowModal;
		JaratbeDlg.Destroy;
    JaratbeDlg:=nil;
  	end
  else begin  // j�rat kiv�laszt�sa
  	// A j�rat sz�r�s be�ll�t�sa
	  EgyebDlg.SzuroGrid.RowCount 			:= EgyebDlg.SzuroGrid.RowCount + 1;
  	szurosor								:= EgyebDlg.SzuroGrid.RowCount - 1 ;
	  EgyebDlg.SzuroGrid.Cells[0,szurosor] 	:= '190';
  	EgyebDlg.SzuroGrid.Cells[1,szurosor] 	:= 'JA_RENDSZ = '''+Query1.FieldByName('KS_RENDSZ').AsString+''' ';
  	JaratValaszto(dummy_jaratkod, dummy_jaratszam, false);
	  GridTorles(EgyebDlg.SzuroGrid, szurosor);   // A sz�r�s t�rl�se
  end; // else

end;


procedure TKoltImpUjFmDlg.BitBtn6Click(Sender: TObject);
begin
	// EgyebDlg.Calendarbe(M2);
	// M3.Text	:= M2.Text;
  // EgyebDlg.ElsoNapEllenor(M2, M3);
  EgyebDlg.CalendarBe_idoszak(M2, M3);
  kell_datum_szures:=True;

	alapstr	:= 'SELECT * FROM KOLTSEG_GYUJTO WHERE KS_DATUM >= '''+M2.Text+''' AND KS_DATUM <= '''+M3.Text+''' ';
	SQL_ToltoFo(GetOrderBy(true));
end;

procedure TKoltImpUjFmDlg.BitBtn7Click(Sender: TObject);
begin
	EgyebDlg.Calendarbe(M3, True);
   kell_datum_szures:=True;
	alapstr	:= 'SELECT * FROM KOLTSEG_GYUJTO WHERE KS_DATUM >= '''+M2.Text+''' AND KS_DATUM <= '''+M3.Text+''' ';
	SQL_ToltoFo(GetOrderBy(true));
end;

procedure TKoltImpUjFmDlg.BitEllenClick(Sender: TObject);
begin
   // Megh�vjuk az ellen�rz�st
   Application.CreateForm(TEllenorbeDlg, EllenorbeDlg);
	EllenorbeDlg.ShowModal;
	EllenorbeDlg.Destroy;
end;

procedure TKoltImpUjFmDlg.Dtum1Click(Sender: TObject);
begin
   // A d�tum beolvas�sa
   M0.Text := '';
	EgyebDlg.Calendarbe(M0);
   if M0.Text <> '' then begin
       Query_Update(Query2, 'KOLTSEG_GYUJTO',
           [
           'KS_DATUM', ''''+M0.Text+''''
           ], ' WHERE KS_KTKOD = '+ Query1.FieldByName('KS_KTKOD').AsString);
       ModLocate (Query1, FOMEZO, Query1.FieldByName(FOMEZO).AsString );
   end;
end;

procedure TKoltImpUjFmDlg.FormCreate(Sender: TObject);
var
   sorsz   : integer;
begin
	Inherited FormCreate(Sender);
	EgyebDlg.SeTADOQueryDatabase(Query1);
	EgyebDlg.SeTADOQueryDatabase(Query3);
	EgyebDlg.SeTADOQueryDatabase(Query4);
	EgyebDlg.SeTADOQueryDatabase(QKoltseg);
	EgyebDlg.SeTADOQueryDatabase(QJarat);
	EgyebDlg.SeTADOQueryDatabase(QKozos01);
	EgyebDlg.SeTADOQueryDatabase(QAlap01);

   Query_Run(Query3, 'SELECT KS_KTKOD FROM KOLTSEG_GYUJTO WHERE ( KS_JARAT IS NULL ) OR ( KS_JAKOD IS NULL )');
   if Query3.RecordCount > 0 then begin
       FormGaugeDlg    := TFormGaugeDlg.Create(Self);
       FormGaugeDlg.GaugeNyit('Hi�nyz� j�ratk�dok ellen�rz�se ... ' , '', false);
       sorsz   := 0;
       while not Query3.Eof do begin
           FormGaugeDlg.Pozicio ( ( sorsz * 100 ) div Query3.RecordCount ) ;
           Inc(sorsz);
           Query_Run(QKoltseg, 'UPDATE KOLTSEG_GYUJTO SET KS_JARAT = '''', KS_JAKOD = ''''  WHERE KS_KTKOD = '+ Query3.FieldByName('KS_KTKOD').AsString );
           Query3.Next;
       end;
       FormGaugeDlg.Free;
   end;
	AddSzuromezoRovid('KS_DATUM', 'KOLTSEG_GYUJTO');
	AddSzuromezoRovid('KS_FIMOD', 'KOLTSEG_GYUJTO');
	AddSzuromezoRovid('KS_KTIP', 'KOLTSEG_GYUJTO');
	AddSzuromezoRovid('KS_TIPUS', 'KOLTSEG_GYUJTO');
	AddSzuromezoRovid('KS_JARAT', 'KOLTSEG_GYUJTO');
	AddSzuromezoRovid('KS_RENDSZ', 'KOLTSEG_GYUJTO');
   AddSzuromezoRovid('KS_KMORA', '');
   AddSzuromezoRovid('KS_MEGJ', '');
   AddSzuromezoRovid('KS_OSSZEG', '');
   AddSzuromezoRovid('KS_ERTEK', '');
   AddSzuromezoRovid('KS_VALNEM', '');
   AddSzuromezoRovid('KS_UZMENY', '');
   AddSzuromezoRovid('KS_GKKAT', '');
   AddSzuromezoRovid('KS_ORSZA', '');
   AddSzuromezoRovid('KS_LEIRAS', '');
   AddSzuromezoRovid('KS_IMPFN', '');

  kell_datum_szures:=False;
  M2.Text             := DatumhozNap(EgyebDlg.MaiDatum, -7, true );
  M3.Text             := EgyebDlg.MaiDatum;

	FelTolto('�j k�lts�g import felvitele ', 'Koltseg import adatok m�dos�t�sa ',
			'K�lts�g importok list�ja', 'KS_KTKOD', 'SELECT * FROM KOLTSEG_GYUJTO', 'KOLTSEG_GYUJTO', QUERY_ADAT_TAG );
	width				:= DLG_WIDTH;
	height				:= DLG_HEIGHT;
	BitBtn1.Parent		:= PanelBottom;
	BitBtn2.Parent		:= PanelBottom;
	BitBtn4.Parent		:= PanelBottom;
	BitBtn3.Parent		:= PanelBottom;
   Dbgrid2.Popupmenu   := PopupMenu1;
	M2.Parent			:= PanelBottom;
	M3.Parent			:= PanelBottom;
	BitBtn5.Parent		:= PanelBottom;
	BitBtn6.Parent		:= PanelBottom;
	BitBtn7.Parent		:= PanelBottom;
	BitBtn8.Parent		:= PanelBottom;
	BitBtn9.Parent		:= PanelBottom;
	BitEllen.Parent		:= PanelBottom;
   ButtonUj.Hide;
end;

procedure TKoltImpUjFmDlg.FormResize(Sender: TObject);
begin
  Inherited FormResize(Sender);
	M2.Top			:= BitBtn8.Top+2;
	M2.Left			:= BitBtn8.Left;
	BitBtn6.Top		:= BitBtn8.Top+2;
	BitBtn6.Left	:= BitBtn8.Left + M2.Width;
  BitBtn6.Width	:= BitBtn6.Height;

	M3.Top			:= BitBtn9.Top+2;
	M3.Left			:= BitBtn6.Left + BitBtn6.Width + 10;
	BitBtn7.Top		:= M3.Top;
	BitBtn7.Left	:= M3.Left + M3.Width;
	BitBtn7.Width	:= BitBtn7.Height;
end;

procedure TKoltImpUjFmDlg.FormShow(Sender: TObject);
begin
   Inherited FormShow(Sender);
   if EgyebDlg.user_code = '113' then begin
       // Sz�cs Adriennnek enged�lyek
       ButtonMod.Show;
       ButtonTor.Show;
       ButtonPrint.Show;
   end;
end;

procedure TKoltImpUjFmDlg.Id1Click(Sender: TObject);
var
   idostr  : string;
begin
   // Az id�pont beolvas�sa
   idostr := InputBox('Id�pont megad�sa', 'Az id�pont : ', '');
   if idostr <> '' then begin
       // Az id�pont ellen�rz�se
       if not joido(idostr) then begin
           NoticeKi('Hib�s id�pont!');
           Exit;
       end;
       Query_Update(Query2, 'KOLTSEG_GYUJTO',
           [
           'KS_IDO', ''''+idostr+''''
           ], ' WHERE KS_KTKOD = '+ Query1.FieldByName('KS_KTKOD').AsString);
       ModLocate (Query1, FOMEZO, Query1.FieldByName(FOMEZO).AsString );
   end;
end;

procedure TKoltImpUjFmDlg.Sorfeldolgozsa1Click(Sender: TObject);
begin
   // Az aktu�lis sor feldolgoz�sa
   Feldolgozas( Query1.FieldByName('KS_KTKOD').AsString);
end;

procedure TKoltImpUjFmDlg.Adatlap(cim, kod : string);
begin
	Application.CreateForm(TKoltImpUjBeDlg, AlForm);
	Inherited Adatlap(cim, kod );
end;

procedure TKoltImpUjFmDlg.Feldolgozas(kod : string);
var
   sqlstr      : string;
   rec_szam    : integer;
   t           : TDateTime;
   rsz         : string;
   regilog     : integer;
   strkm       : string;
   datum       : string;
   ido         : string;
   ktip        : integer;
   tipus       : integer;
   arfolyam    : double;
   ertek       : double;
   km          : double;       // km �ra -> megn�velt a GK_HOZZA -val
   ekm         : double;       // Eredeti km �ra
   pluszkm     : double;
   hutotank    : boolean;
   ttank       : boolean;
   htank       : boolean;
   vege        : boolean;
   regi_arf    : double;
   regi_val    : string;
   regi_dat    : string;
   maradt      : integer;
   ujkod       : integer;
   jaratszam   : string;
   gkkat       : string;
   regi_rsz    : string;
   regi_kat    : string;
begin
   t               := now;
    //   SG1.Hide;
   // SG1 OSZLOPOK: SORSZ�M,KS_RENDSZ,KS_DATUM,KS_IDO,KS_ORSZA,KS_OSSZEG,KS_KMORA,KS_VALNEM,KS_TIPUS,KS_LEIRAS,KS_UZMENY, J�RAT, HIBA�ZENET
   //                  0       1           2       3       4       5         6         7        8          9       10       11      12

   if kod = '' then begin  // csak csoportos feldolgoz�s eset�n ellen�rizz�k
       if NoticeKi('Val�ban feldolgozza a(z) '+Query1.FieldByName('KS_IMPFN').AsString+' �llom�ny hi�nyz� elemeit?', NOT_QUESTION) <> 0 then begin
           Exit;
       end;
       Query_Run(Query3, 'SELECT DISTINCT KS_BEODAT FROM KOLTSEG_GYUJTO WHERE KS_IMPFN = '''+Query1.FieldByName('KS_IMPFN').AsString+''' ');
       beodat  := Query3.FieldByName('KS_BEODAT').AsString;
       if beodat = '' then begin
           Noticeki('A beolvas�si d�tum �res! Nincs feldolgoz�s!');
           Exit;
       end;
     end // if kod = ''
   else begin  // sor feldolgoz�s
     Query_Run(Query3, 'SELECT DISTINCT KS_BEODAT FROM KOLTSEG_GYUJTO WHERE KS_KTKOD = '+IntToSTr(StrToIntDef(kod, 0)));
       beodat  := Query3.FieldByName('KS_BEODAT').AsString;
       if beodat = '' then
         beodat:= EgyebDlg.MaiDatum;
     end;  //else
   SG1.RowCount    := 1;
   SG1.ColCount    := 35;
   SG1.Rows[0].Clear;
   // Megkeress�k a hi�nyz� elemeket
   sqlstr  := 'SELECT * FROM KOLTSEG_GYUJTO WHERE KS_JAKOD = '''' ';
   if kod = '' then  // csoportos feldolgoz�s
     sqlstr  := sqlstr + ' AND KS_IMPFN = '''+Query1.FieldByName('KS_IMPFN').AsString+''' '
   else       // egy sor feldolgoz�sa
     sqlstr  := sqlstr + ' AND KS_KTKOD = '+IntToSTr(StrToIntDef(kod, 0));

   // Feldolgozuk a sorokat
   Query_Run(Query3, sqlstr );
   if Query3.RecordCount < 1 then begin
       NoticeKi('Nincs megfelel� elem!');
       Exit;
   end;
   regilog             := EgyebDlg.log_level;
   EgyebDlg.log_level  := 0;
   Query3.DisableControls;
   Screen.Cursor   := crHourGlass;
   // A t�bl�zat felt�lt�se
   SG1.RowCount    := Query3.RecordCount;
   rec_szam        := 0;
   while not   Query3.Eof do begin
       SG1.Cells[0,rec_szam]      := IntToSTr(rec_szam);
       SG1.Cells[1,rec_szam]      := Query3.FieldByName('KS_RENDSZ').AsString;
       SG1.Cells[2,rec_szam]      := Query3.FieldByName('KS_DATUM').AsString;
       SG1.Cells[3,rec_szam]      := Query3.FieldByName('KS_IDO').AsString;
       SG1.Cells[4,rec_szam]      := Query3.FieldByName('KS_ORSZA').AsString;
       SG1.Cells[5,rec_szam]      := Query3.FieldByName('KS_OSSZEG').AsString;
       SG1.Cells[6,rec_szam]      := Query3.FieldByName('KS_KMORA').AsString;
       SG1.Cells[7,rec_szam]      := Query3.FieldByName('KS_VALNEM').AsString;
       SG1.Cells[8,rec_szam]      := Query3.FieldByName('KS_TIPUS').AsString;
       SG1.Cells[9,rec_szam]      := Query3.FieldByName('KS_LEIRAS').AsString;
       SG1.Cells[10,rec_szam]     := Query3.FieldByName('KS_UZMENY').AsString;
       SG1.Cells[11,rec_szam]     := '';       // Ez lesz a j�ratk�d vagy a 'HIBA!' sz�veg
       SG1.Cells[12,rec_szam]     := '';       // A hiba le�r�sa
       SG1.Cells[13,rec_szam]     := '0';      // H�t�tankol�s jelz�
       SG1.Cells[22,rec_szam]     := Query3.FieldByName('KS_KTIP').AsString;
       SG1.Cells[23,rec_szam]     := '0';      // 0 - m�g nincs k�sz a rekord      1 - k�sz van    2 - kell az �sszes�t�sben
       SG1.Cells[24,rec_szam]     := Query3.FieldByName('KS_KTKOD').AsString;
       SG1.Cells[25,rec_szam]     := '1';      // �rfolyam
       SG1.Cells[26,rec_szam]     := Query3.FieldByName('KS_FIMOD').AsString;
       SG1.Cells[27,rec_szam]     := Query3.FieldByName('KS_SOR').AsString;
       SG1.Cells[28,rec_szam]     := Query3.FieldByName('KS_MEGJ').AsString;
       SG1.Cells[29,rec_szam]     := '';       // KS_KTKOD2
       SG1.Cells[30,rec_szam]     := Query3.FieldByName('KS_OSSZEG').AsString;       // Ide j�n majd az �sszes�t�s
       SG1.Cells[31,rec_szam]     := Query3.FieldByName('KS_AFASZ').AsString;
       SG1.Cells[32,rec_szam]     := Query3.FieldByName('KS_AFAERT').AsString;
       SG1.Cells[33,rec_szam]     := Query3.FieldByName('KS_AFAKOD').AsString;
       Inc(rec_szam);
       Query3.Next;
   end;
//   SG1.Show;
   Query3.EnableControls;
   // A rendsz�m, d�tum ill. id�pont ellen�rz�se
   for rec_szam := 0 to SG1.RowCount-1 do begin
       if SG1.Cells[1,rec_szam] = '' then begin
           // �res a rendsz�m
           SG1.Cells[12,rec_szam] := 'Hi�nyz� rendsz�m!';
           SG1.Cells[23,rec_szam] := '1';
       end;
       datum := SG1.Cells[2,rec_szam];
       if not Jodatum(datum) then begin
           // Rossz a d�tum
          SG1.Cells[12,rec_szam] := 'Hib�s d�tum!';
           SG1.Cells[23,rec_szam] := '1';
       end else begin
           SG1.Cells[2,rec_szam] := datum;
       end;
       ido := SG1.Cells[3,rec_szam];
       if not Joido(ido) then begin
           // Rossz az id�pont
           SG1.Cells[12,rec_szam] := 'Hib�s id�pont!';
           SG1.Cells[23,rec_szam] := '1';
       end else begin
           SG1.Cells[3,rec_szam]   := ido;
       end;
   end;
   // H�t� tankol�sok kezel�se -> a p�tkocsi rendsz�ma van meg
   for rec_szam := 0 to SG1.RowCount-1 do begin
       if SG1.Cells[11,rec_szam] = '' then begin
           datum := SG1.Cells[2,rec_szam];
           ido := SG1.Cells[3,rec_szam];
           if (( STrToIntDef(SG1.Cells[8,rec_szam], -1) = 0 ) and (EgyebDlg.Hutosgk(SG1.Cells[1,rec_szam])  ) ) then begin
               strkm   := SG1.Cells[6,rec_szam];
               Query_Run(QAlap01, 'select * from JARPOTOS , JARAT where (jp_poren= '''+SG1.Cells[1,rec_szam]+''')and(jp_uzem1 <= '+strkm+')and(jp_uzem2 >= '+strkm+')and(ja_kod=jp_jakod)and((ja_jkezd + ja_keido )<= '''+datum+ido+''')and((ja_jvege + ja_veido)>= '''+datum+ido+''')');
               if ( (QAlap01.RecordCount = 0) and (Stringszam(strkm)> 0 ) )  then begin
                   SG1.Cells[12,rec_szam] := 'Nincs j�rat ilyen h�t�s p�tkocsi �zem�ra�ll�ssal!';
                   SG1.Cells[23,rec_szam] := '1';
               end;
               if ( (QAlap01.RecordCount > 1) and (Stringszam(strkm)> 0 ) )  then begin
                   SG1.Cells[12,rec_szam] := 'T�bb j�rat l�tezik ilyen h�t�s p�tkocsi �zem�ra�ll�ssal!';
                   SG1.Cells[23,rec_szam] := '1';
               end;
               if ( (QAlap01.RecordCount = 1) and (Stringszam(strkm)> 0 ) )  then begin
                   // Megtal�ltuk a megfelel� p�tkocsit
                   SG1.Cells[11,rec_szam] := QAlap01.FieldByName('JP_JAKOD').AsString ;
                   SG1.Cells[13,rec_szam] := '1';
                   SG1.Cells[23,rec_szam] := '2';
                   // A t�nyleges rendsz�m meghat�roz�sa
                   rsz := Query_Select('JARAT','JA_KOD',SG1.Cells[11,rec_szam],'JA_RENDSZ');
                   if rsz = '' then begin
                       SG1.Cells[12,rec_szam] := '�rv�nytelen vontat�rendsz�m a p�tkocsib�l!';
                       SG1.Cells[23,rec_szam] := '1';
                   end else begin
                       // A rendsz�m be�ll�t�sa
                       SG1.Cells[1,rec_szam] := rsz;
                   end;
               end;
           end;
       end;
   end;
   // Sorbarakjuk rendsz�m ill. d�tum szerint
   TablaRendezo( SG1, [1,2,3], [0,0,0], 0, true);
   // A j�ratok megkeres�se
   rsz := '';
   Application.CreateForm(TFormGaugeDlg, FormGaugeDlg);
   FormGaugeDlg.GaugeNyit('Adatok feldolgoz�sa ... ', '1 / 3 f�zis: j�ratok megkeres�se', true);
   regi_arf    := 0;
   regi_val    := '';
   regi_dat    := '';
   rec_szam    := 0;
   while ( ( rec_szam < SG1.RowCount ) and ( not FormGaugeDlg.kilepes ) ) do begin
       Application.ProcessMessages;
       if SG1.RowCount <> 1 then begin
           FormGaugeDlg.Pozicio ( ( rec_szam * 100 ) div (SG1.RowCount - 1) );
       end;
       ktip    := StrToIntDef(SG1.Cells[22,rec_szam], -1);
       tipus   := StrToIntDef(SG1.Cells[ 8,rec_szam], -1);
       if rsz <> SG1.Cells[1,rec_szam] then begin
           //Bet�ltj�k a rendsz�mhoz tartoz� j�ratokat -> a befejez�s legyen nagyobb mint a legkisebb d�tum
           rsz := SG1.Cells[1,rec_szam];
           Query_Run(QJarat, 'select JA_KOD, JA_JKEZD, JA_JVEGE, JA_KEIDO, JA_VEIDO, JA_FAZIS, JA_NYITKM, JA_ZAROKM, JA_SOFK1, JA_SNEV1 from jarat where JA_FAZIS <> 0 and ja_rendsz= '''+rsz+''' and '''+SG1.Cells[2,rec_szam]+''' <= ja_jvege ORDER BY JA_JKEZD ');
           QJarat.DisableControls;
       end;
       if QJarat.RecordCount > 0 then begin
           // Megkeress�k a megfelel� j�ratot
           // itt nincs km egyeztet�s, csak id�szak!!
           QJarat.First;
           while ( ( not QJarat.Eof ) and  ( QJarat.FieldByName('JA_JKEZD').AsString <= SG1.Cells[2,rec_szam] ) ) do begin
               if SG1.Cells[3,rec_szam] <> '' then begin   // KS_IDO
                   // A percek is sz�m�tanak
                   if ( ( SG1.Cells[2,rec_szam] + ' ' + SG1.Cells[3,rec_szam] >= QJarat.FieldByName('JA_JKEZD').AsString +' '+ QJarat.FieldByName('JA_KEIDO').AsString ) and
                        ( SG1.Cells[2,rec_szam] + ' ' + SG1.Cells[3,rec_szam] <= QJarat.FieldByName('JA_JVEGE').AsString +' '+ QJarat.FieldByName('JA_VEIDO').AsString ) ) then begin
                       // Megtal�ltuk a megfelel� elemet
                       if QJarat.FieldByName('JA_FAZIS').AsString > '0' then begin   // Ellen�rz�tt j�rat
                           SG1.Cells[12,rec_szam] := 'Ellen�rz�tt j�rat : '+GetJaratszamFromDb(QJarat);
                           SG1.Cells[23,rec_szam] := '1';
                       end else begin
                           if SG1.Cells[11,rec_szam] <> '' then begin
                               SG1.Cells[12,rec_szam] := 'T�bb j�rat';
                               SG1.Cells[23,rec_szam] := '1';
                               QJarat.Last;
                           end else begin
                               SG1.Cells[11,rec_szam] := QJarat.FieldByName('JA_KOD').AsString;
                               SG1.Cells[14,rec_szam] := QJarat.FieldByName('JA_NYITKM').AsString;
                               SG1.Cells[15,rec_szam] := QJarat.FieldByName('JA_ZAROKM').AsString;
                               SG1.Cells[16,rec_szam] := QJarat.FieldByName('JA_JKEZD').AsString;
                               SG1.Cells[17,rec_szam] := QJarat.FieldByName('JA_JVEGE').AsString;
                               SG1.Cells[18,rec_szam] := QJarat.FieldByName('JA_KEIDO').AsString;
                               SG1.Cells[19,rec_szam] := QJarat.FieldByName('JA_VEIDO').AsString;
                               SG1.Cells[20,rec_szam] := QJarat.FieldByName('JA_SOFK1').AsString;
                               SG1.Cells[21,rec_szam] := QJarat.FieldByName('JA_SNEV1').AsString;
                               SG1.Cells[23,rec_szam] := '2';
                           end;
                       end;
                       QJarat.Last;
                   end;
               end else begin
                   if ( ( SG1.Cells[2,rec_szam]  >= QJarat.FieldByName('JA_JKEZD').AsString ) and
                        ( SG1.Cells[2,rec_szam]  <= QJarat.FieldByName('JA_JVEGE').AsString  ) ) then begin
                       // Megtal�ltuk a megfelel� elemet
                       if QJarat.FieldByName('JA_FAZIS').AsString > '0' then begin   // Ellen�rz�tt j�rat
                           SG1.Cells[12,rec_szam] := 'Ellen�rz�tt j�rat : '+GetJaratszamFromDb(QJarat)
                       end else begin
                           if SG1.Cells[11,rec_szam] <> '' then begin
                               SG1.Cells[12,rec_szam] := 'T�bb j�rat';
                               SG1.Cells[23,rec_szam] := '1';
                               QJarat.Last;
                           end else begin
                               SG1.Cells[11,rec_szam] := QJarat.FieldByName('JA_KOD').AsString;
                               SG1.Cells[14,rec_szam] := QJarat.FieldByName('JA_NYITKM').AsString;
                               SG1.Cells[15,rec_szam] := QJarat.FieldByName('JA_ZAROKM').AsString;
                               SG1.Cells[16,rec_szam] := QJarat.FieldByName('JA_JKEZD').AsString;
                               SG1.Cells[17,rec_szam] := QJarat.FieldByName('JA_JVEGE').AsString;
                               SG1.Cells[18,rec_szam] := QJarat.FieldByName('JA_KEIDO').AsString;
                               SG1.Cells[19,rec_szam] := QJarat.FieldByName('JA_VEIDO').AsString;
                               SG1.Cells[20,rec_szam] := QJarat.FieldByName('JA_SOFK1').AsString;
                               SG1.Cells[21,rec_szam] := QJarat.FieldByName('JA_SNEV1').AsString;
                               SG1.Cells[23,rec_szam] := '2';
                           end;
                       end;
                   end;
               end;
               QJarat.Next;
           end;
       end;
       // Ha hiba van vagy nincs j�rat, j�het a k�vetkez�
       if SG1.Cells[11,rec_szam] = ''  then begin
           Inc(rec_szam);
           Continue;
       end;
       // Megvan a j�ratk�d  -> 0 mennyis�g ellen�rz�se
       if ( ( ktip <> 0 ) and ( ktip <> 2 ) and ( ktip <> 5 ) and ( ktip <> 6 ) and ( ktip <> 7 ) and ( StringSzam(SG1.Cells[10,rec_szam]) = 0 ) ) then begin
           SG1.Cells[12,rec_szam] := '0 mennyis�g';
           SG1.Cells[23,rec_szam] := '1';
           Inc(rec_szam);
           Continue;
       end;
       arfolyam    := 1;
       if SG1.Cells[7,rec_szam] <> 'HUF' then begin
           if ( (regi_val = SG1.Cells[7,rec_szam]) and (regi_dat = SG1.Cells[2,rec_szam])) then begin
               arfolyam := regi_arf;
           end else begin
               arfolyam    := EgyebDlg.ArfolyamErtek(SG1.Cells[7,rec_szam], SG1.Cells[2,rec_szam], true);
               regi_arf    := arfolyam;
               regi_val    := SG1.Cells[7,rec_szam];
               regi_dat    := SG1.Cells[2,rec_szam];
           end;
       end;
       SG1.Cells[25,rec_szam]  := Format('%.4f', [arfolyam]);
       ertek       := StringSzam(SG1.Cells[5,rec_szam])*arfolyam;
       if ( ( ktip <> 1 ) and ( ertek = 0 ) ) then begin
           SG1.Cells[12,rec_szam] := '0 �rt�k. Lehet hogy nincs �rfolyam';
           SG1.Cells[23,rec_szam] := '1';
           Inc(rec_szam);
           Continue;
       end;
       km  := StrToIntDef(SG1.Cells[6,rec_szam], 0);
       ekm := StrToIntDef(SG1.Cells[6,rec_szam], 0);
       // Csak azokn�l n�zz�k a km -t ahol meg van adva
       if km <> 0 then begin
           if tipus = 2 then begin    // AD-BLUE
               pluszkm := StrToIntDef(Query_Select('GEPKOCSI', 'GK_RESZ', SG1.Cells[1,rec_szam], 'GK_HOZZA'), 0);
               if pluszkm > km then begin
                   km  := km + pluszkm;
                   SG1.Cells[6,rec_szam]   := Format('%.0f',[km]);
               end;
               if ( (Stringszam(SG1.Cells[14,rec_szam]) > km ) or (Stringszam(SG1.Cells[15,rec_szam]) < km) )  then begin
                   SG1.Cells[11,rec_szam] := HIBASTR;
                   SG1.Cells[12,rec_szam] := 'Rossz km!';
                   SG1.Cells[23,rec_szam] := '1';
                   Inc(rec_szam);
                   Continue;
               end;
           end;

           if tipus = 0 then begin     // �zemanyag k�lts�g
               if ( ( ( ktip = 3 ) or ( ktip = 1 ) or ( ktip = 8 ) ) and (km = 0) ) then begin             // km = 0, Telepi, SHELL, BP
                   SG1.Cells[11,rec_szam] := HIBASTR;
                   SG1.Cells[12,rec_szam] := '0 km �ra �ll�s!';
                   Inc(rec_szam);
                   Continue;
               end;
               // �ra�tfordul�s vizsg�lata
               pluszkm     := StrToIntDef(Query_Select('GEPKOCSI', 'GK_RESZ', SG1.Cells[1,rec_szam], 'GK_HOZZA'), 0);
               hutotank    := StrToINtDef(SG1.Cells[13,rec_szam], 0) > 0;
               if (not hutotank) and ( pluszkm > km ) then begin
                   km  := km + pluszkm;
                   SG1.Cells[6,rec_szam]   := Format('%.0f',[km]);
               end;
               // Vizsg�lat : h�tha j� a km a vontat�hoz �s a p�thoz is
               ttank   := not ( (StrToIntDef(SG1.Cells[14,rec_szam], 0) > km) or (StrToIntDef(SG1.Cells[15,rec_szam], 0) < km) );  // A vontat� beleesik a j�ratba
               Query_Run(QAlap01, 'select * from JARPOTOS where JP_JAKOD = '+SG1.Cells[11,rec_szam]+' AND JP_UZEM1 <= '+Format('%.0f', [ekm])+' AND JP_UZEM2 >= '+Format('%.0f', [ekm]));
               htank   := ( ( QAlap01.RecordCount > 0 ) and (ekm > 0) );
               if ttank and htank then begin
                   // tal�ltunk h�t�s p�tot a j�ratban
                   SG1.Cells[12,rec_szam] := 'Az �ra�ll�s j� a vontat�hoz �s a p�thoz is !';
                   Inc(rec_szam);
                   Continue;
               end;
               if not ttank  then begin
                   // Nem esik bele a j�rat km intervallumba
                   // Megvizsg�ljuk hogy nem h�t�tankol�s-e
                   Query_Run(QAlap01, 'select * from JARPOTOS where JP_JAKOD = '+SG1.Cells[11,rec_szam]+' AND JP_UZEM1 <= '+Format('%.0f', [ekm])+' AND JP_UZEM2 >= '+Format('%.0f', [ekm]));
                   if ( (QAlap01.RecordCount = 1 ) and (ekm > 0) )  then begin
                       // Tal�ltunk h�t�s p�tot a j�ratban
                       // NoticeKi('H�t�s p�tos tal�lat !!!');
                   end;
               end;
           end;
           if tipus = 1 then begin     // Egy�b k�lts�g
               km  := 0;
               SG1.Cells[6,rec_szam]   := Format('%.0f',[km]);
           end;
       end;

       // L�p�nk a k�vetkez� sorra
       Inc(rec_szam);
   end;
   vege    := FormGaugeDlg.kilepes;
   FormGaugeDlg.Free;
   if vege then begin
       NoticeKi('Megszak�t�s : '+FormatDateTime('nn:ss.zzz', now-t));
       ModLocate (Query1, FOMEZO, Query1.FieldByName(FOMEZO).AsString );
       Exit;
   end;

   // J�het a k�vetkez� f�zis - �sszes�t�s

   // Sorbarakjuk rendsz�m, j�rat, valutanem, orsz�g, tipus szerint
   TablaRendezo( SG1, [1,11,7,4,8], [0,0,0,0,0], 0, true);

   Application.CreateForm(TFormGaugeDlg, FormGaugeDlg);
   FormGaugeDlg.GaugeNyit('Adatok feldolgoz�sa ... ', '2 / 3 f�zis: �sszevon�s', true);
   rec_szam    := SG1.RowCount -1 ;
   while ( ( rec_szam > 0 ) and ( not FormGaugeDlg.kilepes ) ) do begin
       Application.ProcessMessages;
       if SG1.RowCount <> 1 then begin
           FormGaugeDlg.Pozicio ( ( (SG1.RowCount - 1-rec_szam) * 100 ) div (SG1.RowCount - 1) ) ;
       end;
       SG1.Cells[23,rec_szam]   := '2'; // Alapb�l minden kell az �sszes�t�sben
       if ( ( SG1.Cells[1,rec_szam]  = SG1.Cells[1,rec_szam-1] ) and
            ( SG1.Cells[11,rec_szam] = SG1.Cells[11,rec_szam-1] ) and
            ( SG1.Cells[7,rec_szam]  = SG1.Cells[7,rec_szam-1] ) and
            ( SG1.Cells[8,rec_szam]  = SG1.Cells[8,rec_szam-1] ) and           // TIPUS
            ( SG1.Cells[4,rec_szam]  = SG1.Cells[4,rec_szam-1] ) ) then begin
            // J�het az �sszevon�s
            if ( ( StrToIntdef(SG1.Cells[22, rec_szam], -1) <> 3 ) or ( StrToIntdef(SG1.Cells[8,rec_szam], -1) <> 0 ) ) then begin
               // Ha KTIP = 3 �s TIPUS = 1 (SHELL-�zemanyag), akkor nincs �sszevon�s
               SG1.Cells[30,rec_szam-1]  := Format('%.2f', [StringSZam(SG1.Cells[30,rec_szam-1]) + StringSzam(SG1.Cells[30,rec_szam])]);
               SG1.Cells[23,rec_szam]     := '0';     // Nem kell az �sszes�t�shez
            end;
       end;
       Dec(rec_szam);
   end;
   vege    := FormGaugeDlg.kilepes;
   FormGaugeDlg.Free;
   if vege then begin
       NoticeKi('Megszak�t�s (2) : '+FormatDateTime('nn:ss.zzz', now-t));
       ModLocate (Query1, FOMEZO, Query1.FieldByName(FOMEZO).AsString );
       Exit;
   end;
   maradt  := 0;
   for rec_szam := 0 to SG1.RowCount-1 do begin
       if SG1.Cells[23,rec_szam]   = '2' then begin
           maradt  := maradt + 1;
       end;
   end;

   // J�het a k�vetkez� f�zis - t�rol�s
   // El�sz�r t�r�lj�k az eddigi rekordokat
   if kod = '' then  // csoportos feldolgoz�s
     Query_Run(QAlap01, 'DELETE FROM KOLTSEG_GYUJTO WHERE KS_JAKOD = '''' AND KS_IMPFN = '''+Query1.FieldByName('KS_IMPFN').AsString+''' ')
   else // sor feldolgoz�s
     Query_Run(QAlap01, 'DELETE FROM KOLTSEG_GYUJTO WHERE KS_JAKOD = '''' AND KS_KTKOD = '+IntToSTr(StrToIntDef(kod, 0)) );
   Query_Run(QJarat, 'SELECT JA_ORSZ, JA_ALKOD, JA_KOD FROM JARAT ORDER BY JA_KOD');
   Application.CreateForm(TFormGaugeDlg, FormGaugeDlg);
   FormGaugeDlg.GaugeNyit('Adatok feldolgoz�sa ... ', '3 / 3 f�zis: t�rol�s', true);
   rec_szam    := 0;
   ujkod       := 0;
   regi_rsz    := '';
   while ( ( rec_szam < SG1.RowCount ) and ( not FormGaugeDlg.kilepes ) ) do begin
       Application.ProcessMessages;
       jaratszam   := '';
       if SG1.RowCount <> 1 then begin
           FormGaugeDlg.Pozicio ( ( rec_szam * 100 ) div (SG1.RowCount - 1) ) ;
       end;
       if SG1.Cells[11,rec_szam] <> '' then begin
           if QJarat.Locate('JA_KOD', SG1.Cells[11,rec_szam], []) then begin
               jaratszam   := GetJaratszamFromDb(QJarat);
           end;
           if jaratszam   = '' then begin
               jaratszam   := GetJaratszam(SG1.Cells[11,rec_szam]);
           end;
           if SG1.Cells[23,rec_szam]   = '2' then begin            // �sszes�t�st be kell �rni
               Query_Run(QAlap01, 'SELECT * FROM KOLTSEG WHERE KS_RENDSZ = '''+SG1.Cells[1,rec_szam]+''' AND KS_DATUM  = '''+SG1.Cells[2,rec_szam]+''' '+
                   ' AND KS_ORSZA  = '''+SG1.Cells[4,rec_szam]+''' AND KS_JAKOD  = '''+SG1.Cells[11,rec_szam]+''' AND KS_KTIP   = '+SG1.Cells[22,rec_szam]+
                   ' AND KS_TIPUS  = '+ SG1.Cells[8,rec_szam]+ ' AND KS_FIMOD  = '''+SG1.Cells[26,rec_szam]+''' AND KS_BEODAT = '''+beodat+''' ');
               if QAlap01.RecordCount > 0 then begin     // Van m�r ilyen k�lts�g
                   ujkod   := StrToIntDef(QAlap01.FieldByName('KS_KTKOD').AsString, 0);
                   Query_Update( QAlap01, 'KOLTSEG',
                       [
                       'KS_UZMENY', 'KS_UZMENY +'+SqlSzamString( StringSzam(SG1.Cells[10,rec_szam]), 12, 2),
                       'KS_OSSZEG', 'KS_OSSZEG +'+ SqlSzamString(StringSZam(SG1.Cells[30,rec_szam]),14,4),
                       'KS_ERTEK',  'KS_OSSZEG * KS_ARFOLY '
                       ], ' WHERE KS_KTKOD = '+ IntToStr(ujkod) );
               end else begin
                  ujkod	:= GetNextCode('KOLTSEG', 'KS_KTKOD', 1, 0);
                  Query_Insert( QAlap01, 'KOLTSEG',
                    [
                    'KS_KTKOD', IntToSTr(ujkod),
                    'KS_RENDSZ', ''''+ SG1.Cells[1,rec_szam] +'''',
                    'KS_JAKOD', ''''+ SG1.Cells[11,rec_szam] +'''',
                    'KS_JARAT', ''''+ jaratszam +'''',
                    'KS_DATUM', ''''+ SG1.Cells[2,rec_szam] +'''',
                    'KS_IDO',   ''''+ copy(SG1.Cells[3,rec_szam], 1, 5) +'''',
                    'KS_LEIRAS', ''''+ SG1.Cells[9,rec_szam] +'''',
                    'KS_MEGJ', ''''+ SG1.Cells[28,rec_szam] +'''',
                    'KS_SOKOD', ''''+ SG1.Cells[20,rec_szam] +'''',
                    'KS_SONEV', ''''+ SG1.Cells[21,rec_szam] +'''',
                    'KS_KMORA', SqlSzamString( StringSzam(SG1.Cells[6,rec_szam]), 12, 2),
                    'KS_VALNEM', ''''+ SG1.Cells[7,rec_szam] +'''',
                    'KS_ARFOLY', SqlSzamString( StringSzam(SG1.Cells[25,rec_szam]), 14, 4),
                    'KS_OSSZEG', SqlSzamString( StringSzam(SG1.Cells[30,rec_szam]), 12, 2),
                    'KS_ERTEK', SqlSzamString( StringSzam(SG1.Cells[25,rec_szam])*StringSzam(SG1.Cells[30,rec_szam]), 12, 0),
                    'KS_AFASZ',   SqlSzamString(StringSzam(SG1.Cells[31,rec_szam]),12,2),
                    'KS_AFAERT',  SqlSzamString(StringSzam(SG1.Cells[30,rec_szam])*StringSzam(SG1.Cells[31,rec_szam])/100,12,2),
                    'KS_AFAKOD',   ''''+SG1.Cells[33,rec_szam]+'''',
                    'KS_UZMENY', SqlSzamString( StringSzam(SG1.Cells[10,rec_szam]), 12, 2),
                    'KS_FIMOD', ''''+ SG1.Cells[26,rec_szam] +'''',
                    'KS_ORSZA', ''''+ SG1.Cells[4,rec_szam] +'''',
                    'KS_TIPUS', SG1.Cells[8,rec_szam],
                    'KS_ATLAG', SqlSzamString( 0, 12, 2),
                    'KS_TELE', '0',
                    'KS_TETAN', '0',
                    'KS_TELES', ''''+ 'N' +'''',
                    'KS_KTIP', SG1.Cells[22,rec_szam],
                    'KS_BEODAT', ''''+ beodat +''''
                    ]);
               // ---- t�bb tankol�s ugyanazzal a kilom�terrel - NP 2017.11.16.
                 if (trim(SG1.Cells[22,rec_szam])= '1')    // Tankol�s import
                    and (trim(SG1.Cells[8,rec_szam]) = '0') then begin // �zemanyag k�lts�g

                    EgyebDlg.DuplaTankolasKezeles(SG1.Cells[1,rec_szam], Floor(StringSzam(SG1.Cells[6,rec_szam])));  // rendsz�m, km�ra �ll�s
                    end;
               end;
           end;
       end else begin
           // �res a j�ratk�d
           ujkod   := 0;
       end;
       SG1.Cells[29,rec_szam]  := IntToSTr(ujkod);   // Bejegyezz�k a KS_KTKOD2 -t
       if regi_rsz = SG1.Cells[1,rec_szam] then begin
           gkkat   := regi_kat;
       end else begin
           gkkat    := Query_Select('GEPKOCSI', 'GK_RESZ', SG1.Cells[1,rec_szam], 'GK_GKKAT');
           regi_rsz := SG1.Cells[1,rec_szam];
           regi_kat := gkkat;
       end;

       Query_Insert( QAlap01, 'KOLTSEG_GYUJTO',
        [
        'KS_KTKOD', IntToSTr(StrToIntDef( SG1.Cells[24,rec_szam],0) ),
        'KS_KTKOD2', IntToSTr(StrToIntDef( SG1.Cells[29,rec_szam],0) ),
        'KS_ARFOLY', SqlSzamString( StringSzam(SG1.Cells[25,rec_szam]), 14, 4),
        'KS_ERTEK',  SqlSzamString( StringSzam(SG1.Cells[5,rec_szam])*StringSzam(SG1.Cells[25,rec_szam]), 12, 0),
        'KS_BEODAT', ''''+ beodat +'''',
        'KS_HMEGJ',  ''''+ SG1.Cells[12,rec_szam]+'''',
        'KS_JARAT',  ''''+ jaratszam +'''',
        'KS_JAKOD',  ''''+ SG1.Cells[11,rec_szam]+'''',
        'KS_LEIRAS', ''''+ SG1.Cells[9,rec_szam]+'''',
        'KS_DATUM',  ''''+ SG1.Cells[2,rec_szam] +'''',
        'KS_IDO',    ''''+ copy(SG1.Cells[3,rec_szam], 1, 5) +'''',
        'KS_RENDSZ', ''''+ SG1.Cells[1,rec_szam] +'''',
        'KS_ORSZA',  ''''+ SG1.Cells[4,rec_szam] +'''',
        'KS_OSSZEG', SqlSzamString( StringSzam(SG1.Cells[5,rec_szam]) , 12, 2),
        'KS_KMORA',  SqlSzamString( StringSzam(SG1.Cells[6,rec_szam]), 12, 2),
        'KS_VALNEM', ''''+ SG1.Cells[7,rec_szam] +'''',
        'KS_TIPUS', IntToSTr(StrToIntDef( SG1.Cells[8,rec_szam], 0)),
        'KS_UZMENY', SqlSzamString( StringSzam(SG1.Cells[10,rec_szam]), 12, 2),
        'KS_KTIP', IntToSTr(StrToIntDef( SG1.Cells[22,rec_szam], 0)),
        'KS_FIMOD', ''''+ SG1.Cells[26,rec_szam] +'''',
        'KS_IMPFN', ''''+ Query1.FieldByName('KS_IMPFN').AsString +'''',
        'KS_SOKOD', ''''+ SG1.Cells[20,rec_szam] +'''',
        'KS_SONEV', ''''+ SG1.Cells[21,rec_szam] +'''',
        'KS_TELES', ''''+ 'N' +'''',
        'KS_GKKAT', ''''+ gkkat +'''',
        'KS_AFASZ',   SqlSzamString(StringSzam(SG1.Cells[31,rec_szam]),12,2),
        'KS_AFAERT',  SqlSzamString(StringSzam(SG1.Cells[5,rec_szam])*StringSzam(SG1.Cells[31,rec_szam])/100,12,2),
        'KS_AFAKOD',   ''''+SG1.Cells[33,rec_szam]+'''',
        'KS_SOR', ''''+ SG1.Cells[27,rec_szam] +'''',
        'KS_MEGJ', ''''+ SG1.Cells[28,rec_szam] +''''
        ]);
       Inc(rec_szam);
   end;
   vege    := FormGaugeDlg.kilepes;
   FormGaugeDlg.Free;
   if vege then begin
       NoticeKi('Megszak�t�s : '+FormatDateTime('nn:ss.zzz', now-t));
       ModLocate (Query1, FOMEZO, Query1.FieldByName(FOMEZO).AsString );
       Exit;
   end;
//   SaveGridToFile(SG1, 'Tempgrid.csv');
   EgyebDlg.log_level  := regilog;
   Screen.Cursor       := crDefault;
   if kod = '' then  // csoportos feldolgoz�s
     NoticeKi('K�sz : '+FormatDateTime('nn:ss.zzz', now-t))
   else // sor feldolgoz�s
     NoticeKi('A feldolgoz�s k�sz!');  // itt ne �rjunk ki id�t

   ModLocate (Query1, FOMEZO, Query1.FieldByName(FOMEZO).AsString );
end;

procedure TKoltImpUjFmDlg.ButtonTorClick(Sender: TObject);
var
   regikod : string;
begin
	if not voltalt then begin
		voltalt 	:= true;
		Exit;
	end;
   if Query1.FieldByName('KS_JAKOD').AsString <> '' then begin
       NoticeKi('Ez a k�lts�g import m�r j�rathoz van rendelve, ez�rt nem t�r�lhet�!');
       Exit;
   end;
	if NoticeKi( TORLOTEXT, NOT_QUESTION ) = 0 then begin
       regikod := Query1.FieldByName('KS_KTKOD').AsString;
       Query1.Next;
		Query_Run(Query2, 'DELETE from KOLTSEG_GYUJTO where KS_KTKOD  = '''+regikod+''' ');
		ModLocate(Query1, FOMEZO, Query1.FieldByName(FOMEZO).AsString );
	end;
	Label3.Caption	:= '('+IntToStr(Query1.RecordCount)+')';
	Label3.Update;
end;

end.

