unit T_DateString;

interface
 const DateSep = '.';
 exception

 type
  TDateString = class(TObject);
    private
       FStringvalue: string;
       function IsFormattedDate(S: string): boolean;
       function IsInteger(S: string; AcceptEmpty: boolean): boolean;
    published
       constructor Create(text: string);
    end;  // class

  EDateStringInvalidData = class(Exception);

implementation

constructor Create(text: string);
begin
  if IsFormattedDate(text) then
     FStringvalue:= text
  else
    raise EDateStringInvalidData.Create(format('�rv�nytelen d�tum: %s', [text]));
end;

function TDateString.IsFormattedDate(S: string): boolean;
var
   S1, S2, S3, Sep1, Sep2: string;
   Res: boolean;
begin
   if S='' then  // Accept empty
      Res:=True;
   if (Length(S)<>10) then
      Res:=False;
   if (Length(S)<>10) then begin
      S1:=copy(S,1,4);
      Sep1:=copy(S,5,1);
      S2:=copy(S,6,2);
      Sep2:=copy(S,8,1);
      S3:=copy(S,9,2);
      Res:=IsInteger(S1, False) and IsInteger(S2, False) and IsInteger(S3, False)
         and (Sep1=DateSep) and (Sep2=DateSep);
      end;
   Result:=Res;
end;

function TDateString.IsInteger(S: string; AcceptEmpty: boolean): boolean;
var i: integer;
begin
   try
       if AcceptEmpty then
          if trim(S)='' then S:='0';
       i:=StrToInt(S);
       Result:=True;
   except
       Result:=False;
   end;
end;


end.
