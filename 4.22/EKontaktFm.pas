unit EKontaktFm;

interface

uses
	SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
	StdCtrls, Forms, ExtCtrls, Dialogs, J_FOFORMUJ;

type
	TEKontaklFmDlg = class(TJ_FOFORMUJDLG)
	  procedure FormCreate(Sender: TObject);override;
	 procedure Adatlap(cim, kod : string);override;
	end;

var
	EKontaklFmDlg : TEKontaklFmDlg;

implementation

uses
	Egyeb, J_SQL, EKontaktbe;

{$R *.DFM}

procedure TEKontaklFmDlg.FormCreate(Sender: TObject);
begin
	Inherited FormCreate(Sender);
	FelTolto('�j kontakt felvitele ', 'Kontakt adatok m�dos�t�sa ',
			'Kontakt adatok list�ja', 'EK_ID',
      'select * from (SELECT EK_ID, EK_NEV N�v, isnull(EK_MOBILTEL,'''') Mobil, isnull(EK_EMAIL,'''') Email, '+
      'isnull(EK_IRODAITEL,'''') Irodai, isnull(EK_BEOSZTAS,'''') Beoszt�s, EK_MEGJEGYZES Megjegyz�s, EK_VALID �rv�nyes FROM EKONTAKT) a where 1=1 ',
      'EKONTAKT', QUERY_KOZOS_TAG );
	nevmezo	:= 'N�v';
	width	:= DLG_WIDTH;
	height	:= DLG_HEIGHT;
end;

procedure TEKontaklFmDlg.Adatlap(cim, kod : string);
begin
	Application.CreateForm(TEKontaktbeDlg, AlForm);
	Inherited Adatlap(cim, kod );
end;

end.

