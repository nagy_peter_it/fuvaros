unit Napidij;

interface

uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, DBTables, DB, StdCtrls, Buttons, Mask, DBCtrls, ExtCtrls,DateUtils;

type
  TNapidijDlg = class(TForm)
    Label1: TLabel;
    BitBtn4: TBitBtn;
    BitBtn6: TBitBtn;
    MaskEdit1: TMaskEdit;
    MaskEdit2: TMaskEdit;
    Label2: TLabel;
    CheckBox1: TCheckBox;
    BitBtn10: TBitBtn;
    BitBtn1: TBitBtn;
    procedure FormCreate(Sender: TObject);
    procedure BitBtn4Click(Sender: TObject);
    procedure BitBtn6Click(Sender: TObject);
    procedure MaskEdit1Click(Sender: TObject);
    procedure BitBtn10Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure MaskEdit1Exit(Sender: TObject);
    procedure MaskEdit2Exit(Sender: TObject);
  private
  public
  end;

var
  NapidijDlg: TNapidijDlg;

implementation

uses
	NapiList, Egyeb, Kozos;
{$R *.DFM}

procedure TNapidijDlg.FormCreate(Sender: TObject);
begin
 	MaskEdit1.Text := copy(EgyebDlg.MaiDatum,1,8)+'01.';
 	MaskEdit2.Text := EgyebDlg.MaiDatum;
end;

procedure TNapidijDlg.BitBtn4Click(Sender: TObject);
begin

	{A lista �ssze�ll�t�sa a sz�r�si elemek alapj�n}
	if MaskEdit1.Text = '' then begin
  		MaskEdit1.Text := copy(EgyebDlg.MaiDatum,1,8)+'01.';
  	end;
	if MaskEdit2.Text = '' then begin
  		MaskEdit2.Text := EgyebDlg.MaiDatum;
  	end;
  	if not Jodatum2(MaskEdit1) then begin
		NoticeKi('Rossz d�tum megad�sa!');
     	MaskEdit1.SetFocus;
     	Exit;
  	end;
  	if not Jodatum2(MaskEdit2) then begin
		NoticeKi('Rossz d�tum megad�sa!');
     	MaskEdit2.SetFocus;
     	Exit;
  	end;

  	{Csak eg�sz h�napot adhatunk meg}
   Screen.Cursor	:= crHourGlass;
  	Application.CreateForm(TNapiListDlg, NapiListDlg);
  	NapiListDlg.Tolt(MaskEdit1.Text, MaskEdit2.Text, CheckBox1.Checked);
   Screen.Cursor	:= crDefault;
  	if NapiListDlg.vanadat	then begin
     	NapiListDlg.Rep.Preview;
  	end else begin
    	NoticeKi('Nincs a megadott felt�teleknek megfelel� t�tel!');
  	end;
  	NapiListDlg.Destroy;
end;

procedure TNapidijDlg.BitBtn6Click(Sender: TObject);
begin
	Close;
end;

procedure TNapidijDlg.MaskEdit1Click(Sender: TObject);
begin
	with Sender as TMaskEdit do begin
  	SelectAll;
  end;
end;

procedure TNapidijDlg.BitBtn10Click(Sender: TObject);
begin
	// EgyebDlg.Calendarbe(MaskEdit1);
  // EgyebDlg.ElsoNapEllenor(MaskEdit1, MaskEdit2);
  EgyebDlg.CalendarBe_idoszak(MaskEdit1, MaskEdit2);
end;

procedure TNapidijDlg.BitBtn1Click(Sender: TObject);
begin
	EgyebDlg.Calendarbe(MaskEdit2);
  EgyebDlg.UtolsoNap(MaskEdit2);
  // MaskEdit2.Text:=DateToStr(EndOfTheMonth(StrToDate(MaskEdit2.Text)));
end;

procedure TNapidijDlg.MaskEdit1Exit(Sender: TObject);
begin
	DatumExit(MaskEdit1, false);
  	EgyebDlg.ElsoNapEllenor(MaskEdit1, MaskEdit2);
end;

procedure TNapidijDlg.MaskEdit2Exit(Sender: TObject);
begin
	DatumExit(MaskEdit2, false);
end;

end.


