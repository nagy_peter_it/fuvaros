unit SqlList;

interface

uses
	WinTypes, WinProcs, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
	StdCtrls, ExtCtrls, quickrpt, Qrctrls, printers, ShellAPI, qrprntr,
  Db, DBTables, ADODB;
type
	TSQLListDlg = class(TForm)
    SorBand: TQRBand;
    Rep: TQuickRep;
    OldalFejlecBand: TQRBand;
    QRLabel5: TQRLabel;
    QRSysData4: TQRSysData;
    QRLabel6: TQRLabel;
    QLCim: TQRLabel;
    QLCegNev: TQRLabel;
    Sz1: TQRLabel;
    QRSysData1: TQRSysData;
    QRS: TQRShape;
    QRLine: TQRShape;
    QRExpr1: TQRExpr;
    SumBand: TQRBand;
    QRExp: TQRExpr;
	 LIstaSQL: TADOQuery;
	 procedure Tolto( origsql : TADOQuery; cegnev, cim , szures: string);
	 procedure FormDestroy(Sender: TObject);
	private
	   fejek	: array [0..150] of TQRLabel;
	   mezok	: array [0..150] of TQRDBText;
	   osszes	: array [0..150] of TQRExpr;
	   mezoszam	: integer;
	public
     pFontSize: integer;
     pDisplaySzorzo: double;
     pFieldMargin: integer;
	end;

var
	SQLListDlg: TSQLListDlg;

implementation

uses
	Egyeb, J_SQL;

{$R *.DFM}

procedure TSQLListDlg.Tolto( origsql : TADOQuery; cegnev, cim , szures: string);
var
	i 				: integer;
  balszel  	: integer;
  szorzo		: double;
  magassag		: integer;
  FieldMargin: integer;
  function fontsize_to_height(fontsize: integer): integer;
    begin  // a Delphi enn�l kisebbet nem enged �gysem
      case fontsize of
        6: Result:=	11;
        7: Result:=	13;
        8: Result:=	15;
        9: Result:=	16;
        10: Result:=	17;
        11: Result:=	18;
        12: Result:=	19;
       end;
    end;

begin
  if pFontSize = 0 then pFontSize:= 8; // alap�rtelmezett
  Sz1.Font.Size:= pFontSize;
  if pDisplaySzorzo = 0 then pDisplaySzorzo:= 7;
  szorzo:= pDisplaySzorzo;
  if pFieldMargin = 0 then pFieldMargin:= 20;
  FieldMargin:= pFieldMargin;

	// A konstans mez�k felt�lt�se
	QLCegNev.Caption	:= cegnev;
	QLCim.Caption		:= cim;
	Rep.ReportTitle		:= cim;
	Sz1.Caption			:= '';
	if szures <> '' then begin
		Sz1.Caption			:= 'Sz�r�s : '+szures;
	end;
	ListaSql 	:= origsql;
	Rep.Dataset	:= origsql;
	// �j mez�k l�trehoz�sa
	balszel		:= 5;
	magassag	:= 0;
	mezoszam	:= 150;
	if ( origSql.FieldCount - 1 ) < 150 then begin
		mezoszam	:= origSql.FieldCount - 1;
	end;
	// A v�rhat� sz�less�g kisz�m�t�sa
	for i := 0 to mezoszam do begin
		balszel	:= balszel + round(origSql.Fields[i].DisplayWidth * szorzo) + FieldMargin;
	end;
	if balszel - FieldMargin < 700 then begin
		// Portrait oldal is el�g a nyomtat�shoz
		Rep.Page.Orientation	:= poPortrait;
//     QLCim.Width				:= QLCim.Width - 315;
		QLCim.Width				:= OldalFejlecBand.width;
		Qrs.Width				:= OldalFejlecBand.width;
		Sz1.Width				:= OldalFejlecBand.width;
		QRLine.Width			:= OldalFejlecBand.width;
		QRLabel5.Left			:= OldalFejlecBand.width - 180;
		QRLabel6.Left			:= OldalFejlecBand.width - 180;
		QRSysData1.Left			:= OldalFejlecBand.width - 120;
		QRSysData4.Left			:= OldalFejlecBand.width - 120;
	end;
	balszel	:= 5;
	for i := 0 to mezoszam do begin
		if origSql.Fields[i].Visible then begin
			if balszel + round(origSql.Fields[i].DisplayWidth * szorzo) > OldalFejlecBand.width then begin
				magassag	:= magassag + 20;
				balszel	:= 25;
			end;
			fejek[i] := TQRLabel.Create(OldalFejlecband);
			fejek[i].Name			:= 'LABEL'+IntToStr(i);
			fejek[i].Parent		:= OldalFejlecband;
      // ---- 2017-08-18 NP ------ //
      fejek[i].AutoSize:= False;
      // ------------------------- //
			fejek[i].Caption	   := origSql.Fields[i].DisplayLabel;
			fejek[i].width			:= round(origSql.Fields[i].DisplayWidth * szorzo);
			fejek[i].Left			:= balszel;
			fejek[i].Color			:= clSilver;
			fejek[i].Top			:= Qrs.Top + 1 + magassag;
			fejek[i].Font			:= Sz1.Font;
			fejek[i].Font.Style	:= [fsBold];

			mezok[i] := TQRDBText.Create(Sorband);
  	   		mezok[i].Name		:= 'TEXT'+IntToStr(i);
     		  mezok[i].Parent		:= Sorband;
          // ---- 2017-08-18 NP ------ //
          mezok[i].AutoSize:= False;
          // ------------------------- //
	      	mezok[i].DataSet	:= ListaSql;
        	mezok[i].DataField  := OrigSql.Fields[i].FieldName;
	      	mezok[i].width		:= round(OrigSql.Fields[i].DisplayWidth * szorzo);
	      	mezok[i].Left		:= balszel;
	      	mezok[i].Top		:= 1 + magassag;
	      	mezok[i].Font		:= Sz1.Font;
          mezok[i].Height:= fontsize_to_height(pFontSize);
          mezok[i].Alignment	:= OrigSql.Fields[i].Alignment;
        // �sszes�t� mez�k felt�tele
   		if origSql.Fields[i].Tag = 1 then begin
				osszes[i] 				:= TQRExpr.Create(SumBand);
  	   			osszes[i].Name			:= 'SUM'+IntToStr(i);
     			osszes[i].Parent		:= Sumband;
           	osszes[i].Expression 	:= 'SUM('+OrigSql.Fields[i].FieldName+')';
	      		osszes[i].width			:= round(OrigSql.Fields[i].DisplayWidth * szorzo);
	      		osszes[i].Left			:= balszel;
	      		osszes[i].Top			:= 1 + magassag;
	      		osszes[i].Font			:= Sz1.Font;
           	osszes[i].Alignment		:= OrigSql.Fields[i].Alignment;
//        	QRExp.Expression := 'SUM('+OrigSql.Fields[i].FieldName+')';
        	end;
           if OrigSql.Fields[i] is TFloatField then begin
           	mezok[i].Mask	:= TFloatField(OrigSql.Fields[i]).DisplayFormat;
   			if origSql.Fields[i].Tag = 1 then begin
					osszes[i].Mask	:= TFloatField(OrigSql.Fields[i]).DisplayFormat;
               end;
           end;
           if OrigSql.Fields[i] is TIntegerField then begin
           	mezok[i].Mask	:= TIntegerField(OrigSql.Fields[i]).DisplayFormat;
   			if origSql.Fields[i].Tag = 1 then begin
           		osszes[i].Mask	:= TIntegerField(OrigSql.Fields[i]).DisplayFormat;
               end;
           end;
           if OrigSql.Fields[i] is TBCDField then begin
           	mezok[i].Mask	:= TBCDField(OrigSql.Fields[i]).DisplayFormat;
               if origSql.Fields[i].Tag = 1 then begin
           		osszes[i].Mask	:= TBCDField(OrigSql.Fields[i]).DisplayFormat;
               end;
           end;
	      	balszel	:= balszel + round(origSql.Fields[i].DisplayWidth * szorzo) + FieldMargin;
	   	end;
  	end;

 		OldalFejlecBand.Height		:= OldalFejlecBand.Height + magassag;
    SorBand.Height:= mezok[0].Height + 4;
   	SorBand.Height:= SorBand.Height + magassag;
   	QRS.Height:= QRS.Height + magassag;
   	QRLine.Top:= SorBand.Height - 2; // az utols� el�tti sorba

  	ListaSql.Open;
end;

procedure TSQLListDlg.FormDestroy(Sender: TObject);
var
	i : integer;
begin
	for i := 0 to mezoszam do begin
		if fejek[i] <> nil then begin
			fejek[i].Destroy;
			mezok[i].Destroy;
		end;
		if osszes[i] <> nil then begin
			osszes[i].Destroy;
	   end;
	end;
end;

end.
