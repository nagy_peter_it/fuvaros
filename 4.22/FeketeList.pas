unit FeketeList;

interface

uses
  WinTypes, WinProcs, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, QuickRpt, QRExpr, Qrctrls, ExtCtrls, printers, DB, DBTables, Egyeb, Forgalom, J_SQL,
  Grids, Kozos, ADODB ;

type
  TFeketelistDlg = class(TForm)
    Rep: TQuickRep;
    QRBand3: TQRBand;
    QRLSzamla: TQRLabel;
    QRBand1: TQRBand;
    QRLabel15: TQRLabel;
    QRLabel6: TQRLabel;
    QRSysData1: TQRSysData;
    QRLabel7: TQRLabel;
    QRSysData4: TQRSysData;
    QRLabel1: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel12: TQRLabel;
    QRLabel13: TQRLabel;
    QRLabel14: TQRLabel;
    QRDBText4: TQRDBText;
    QRSysData2: TQRSysData;
    QRDBText2: TQRDBText;
    QRLabel10: TQRLabel;
    Query1: TADOQuery;
    Query2: TADOQuery;
	 QRShape1: TQRShape;
    Query1V_KOD: TStringField;
    Query1V_NEV: TStringField;
    Query1V_IRSZ: TStringField;
    Query1V_VAROS: TStringField;
    Query1V_CIM: TStringField;
    Query1V_TEL: TStringField;
    Query1V_FAX: TStringField;
    Query1V_BANK: TStringField;
    Query1V_ADO: TStringField;
    Query1V_KEDV: TBCDField;
	 Query1V_UGYINT: TStringField;
    Query1V_MEGJ: TStringField;
    Query1V_LEJAR: TIntegerField;
    Query1V_AFAKO: TStringField;
    Query1V_NEMET: TIntegerField;
    Query1V_TENAP: TIntegerField;
    Query1V_ARNAP: TIntegerField;
    Query1V_ORSZ: TStringField;
    Query1VE_VBANK: TStringField;
    Query1VE_EUADO: TStringField;
    Query1V_UPTIP: TIntegerField;
    Query1V_UPERT: TBCDField;
    Query1VE_ARHIV: TIntegerField;
    QRLabel2: TQRLabel;
    procedure FormCreate(Sender: TObject);
    procedure QRBand1BeforePrint(Sender: TQRCustomBand;
	   var PrintBand: Boolean);
  private
  public
  end;

var
  FeketelistDlg: TFeketelistDlg;

implementation

{$R *.DFM}

procedure TFeketelistDlg.FormCreate(Sender: TObject);
begin
	EgyebDlg.SetADOQueryDatabase(Query1);
	Query_Run(Query1, 'SELECT * FROM VEVO WHERE VE_FELIS = 1 ORDER BY V_NEV ');
end;

procedure TFeketelistDlg.QRBand1BeforePrint(Sender: TQRCustomBand;
	   var PrintBand: Boolean);
begin
	QrLabel2.Caption := Query1.FieldByName('V_IRSZ').AsString+' '+Query1.FieldByName('V_VAROS').AsString+', '+
		Query1.FieldByName('V_CIM').AsString;
end;

end.
