unit FelForm;

interface

uses
	SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
	StdCtrls, Forms, ExtCtrls, Dialogs, J_FOFORMUJ;

type
	TFelFormDlg = class(TJ_FOFORMUJDLG)
	  procedure FormCreate(Sender: TObject);override;
	 procedure Adatlap(cim, kod : string);override;
	 procedure ButtonKuldClick(Sender: TObject);override;
  public
	end;

var
	FelFormDlg : TFelFormDlg;

implementation

uses
	Egyeb, Felhabe, J_SQL;

{$R *.DFM}

procedure TFelFormDlg.FormCreate(Sender: TObject);
begin
	Inherited FormCreate(Sender);
//	AddSzuromezoRovid( 'TT_TIPUS', 'TETANAR' );
	alapstr		:= 'SELECT * FROM JELSZO';
	FelTolto('Új felhasználó felvitele ', 'Felhasználó adatok módosítása ',
			'Felhasználók listája', 'JE_FEKOD', alapstr,'JELSZO', QUERY_KOZOS_TAG );
	NEVMEZO		:= 'JE_FENEV';
	width		:= DLG_WIDTH;
	height		:= DLG_HEIGHT;
end;

procedure TFelFormDlg.ButtonKuldClick(Sender: TObject);
begin
	Inherited ButtonKuldClick(Sender);
   ret_vnev	:= Query1.FieldByName('JE_FENEV').AsString;
end;

procedure TFelFormDlg.Adatlap(cim, kod : string);
begin
	Application.CreateForm(TFelhabeDlg, AlForm);
	Inherited Adatlap(cim, kod );
end;

end.

