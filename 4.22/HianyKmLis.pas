unit HianyKmLis;

interface

uses
  WinTypes, WinProcs, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, QuickRpt, QRExpr, Qrctrls, ExtCtrls, printers, DB, DBTables, Egyeb, Forgalom, J_SQL,
  Grids, Kozos, ADODB ;

type
  THianykmlisDlg = class(TForm)
    Rep: TQuickRep;
    QRBand3: TQRBand;
    QRLSzamla: TQRLabel;
    QRBand1: TQRBand;
    QRLabel6: TQRLabel;
    QRSysData1: TQRSysData;
    QRLabel7: TQRLabel;
    QRSysData4: TQRSysData;
    QRLabel1: TQRLabel;
    QRLabel13: TQRLabel;
    QRLabel14: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel18: TQRLabel;
    QRLabel21: TQRLabel;
    QRLabel26: TQRLabel;
	 QRLabel27: TQRLabel;
    Query1: TADOQuery;
	 QRShape1: TQRShape;
	 procedure FormCreate(Sender: TObject);
	 procedure	Tolt(rsz, dat1, dat2 : string);
	 procedure QRBand1BeforePrint(Sender: TQRCustomBand;
	   var PrintBand: Boolean);
    procedure RepBeforePrint(Sender: TCustomQuickRep;
      var PrintReport: Boolean);
  private
		jarat		: string;
		kezdokm		: integer;
		rendszam    : string;
  public
	  vanadat			: boolean;
	  hatarertek		: integer;
  end;

var
  HianykmlisDlg: THianykmlisDlg;

implementation

{$R *.DFM}

procedure THianykmlisDlg.FormCreate(Sender: TObject);
begin
	EgyebDlg.SetADOQueryDatabase(Query1);
	hatarertek	:= 0;
end;

procedure	THianykmlisDlg.Tolt(rsz, dat1, dat2 : string);
var
  sqlstr	: string;
  kezdat	: string;
  vegdat	: string;
begin
	if rsz = '' then begin
		QRLabel26.Caption := '';
		sqlstr	:= 'SELECT * FROM JARAT WHERE JA_FAZIS <> 2 AND JA_RENDSZ LIKE ''%'' ';
	end else begin
		QRLabel26.Caption := rsz;
		sqlstr	:= 'SELECT * FROM JARAT WHERE JA_FAZIS <> 2 AND JA_RENDSZ = '''+ rsz +''' ';
	end;
	QRLabel27.Caption := dat1+' - '+dat2;
	kezdat := dat1;
	if kezdat = '' then begin
		kezdat := '2000.01.01';
	end;
	vegdat := dat2;
	if vegdat = '' then begin
		vegdat := EgyebDlg.MaiDatum;
	end;
	sqlstr := sqlstr + ' AND JA_JKEZD >= '''+kezdat+''' AND JA_JKEZD <= '''+vegdat+''' ';
	sqlstr := sqlstr + ' ORDER BY JA_RENDSZ,JA_NYITKM';
	{A kapott sql alapj�n v�grehajtja a lek�rdez�st}
	Query_Run(Query1, sqlstr );
	vanadat 	:= Query1.RecordCount > 0;
end;

procedure THianykmlisDlg.QRBand1BeforePrint(Sender: TQRCustomBand;
	   var PrintBand: Boolean);
var
	hianykm	: integer;
begin
	PrintBand	:= false;
	if rendszam = Query1.FieldByName('JA_RENDSZ').AsString then begin
		hianykm		:= StrToIntDef(Query1.FieldByName('JA_NYITKM').AsString, 0) - kezdokm;
		if hianykm > hatarertek then begin
			// Hi�nyz� km-ek
			PrintBand	:= true;
			QRLabel5.Caption := 'Rendsz�m : '+rendszam+'   Hi�nyz� km : '+Format('%6d km ',[hianykm]) + '  J�ratok : '+
				jarat + ' ('+IntToStr(kezdokm) + ')'+
				' - ' + GetJaratszamFromDb(Query1)+ ' ('+Query1.FieldByName('JA_NYITKM').AsString + ')';
		end else begin
			if hianykm < (-1 * hatarertek) then begin
				// Hi�nyz� km-ek
				PrintBand	:= true;
				QRLabel5.Caption := 'Rendsz�m : '+rendszam+'   Km �tk�z�s : '+Format('%6d km ',[-1 * hianykm]) + '  J�ratok : '+
					jarat + ' ('+IntToStr(kezdokm) + ')'+
					' - ' + GetJaratszamFromDb(Query1)+ ' ('+Query1.FieldByName('JA_NYITKM').AsString + ')';
			end;
		end;
	end;
	jarat		:= GetJaratszamFromDb(Query1);
	kezdokm		:= StrToIntDef(Query1.FieldByName('JA_ZAROKM').AsString, 0);
	rendszam	:= Query1.FieldByName('JA_RENDSZ').AsString;
end;

procedure THianykmlisDlg.RepBeforePrint(Sender: TCustomQuickRep;
  var PrintReport: Boolean);
begin
	jarat		:= GetJaratszamFromDb(Query1);
	kezdokm		:= StrToIntDef(Query1.FieldByName('JA_NYITKM').AsString, 0);
	rendszam	:= Query1.FieldByName('JA_RENDSZ').AsString;
end;

end.
