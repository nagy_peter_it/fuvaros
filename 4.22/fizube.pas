unit fizube;

interface

uses
	SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
	Forms, Dialogs, StdCtrls, Buttons, Mask, DB, DBTables;

type
	TFizubeDlg = class(TForm)
	BitElkuld: TBitBtn;
	BitKilep: TBitBtn;
	Label3: TLabel;
	 M4: TMaskEdit;
	 M2: TMaskEdit;
	 M1: TMaskEdit;
	 M3: TMaskEdit;
	 M5: TMaskEdit;
	 M6: TMaskEdit;
	 Label1: TLabel;
	 Label2: TLabel;
	 Label4: TLabel;
	 Label5: TLabel;
	 Label6: TLabel;
    BitBtn1: TBitBtn;
	procedure Tolto(s1, s2, s3, s4, s5, s6 : string);
	procedure BitElkuldClick(Sender: TObject);
	procedure BitKilepClick(Sender: TObject);
    procedure M4Exit(Sender: TObject);
    procedure M4KeyPress(Sender: TObject; var Key: Char);
    procedure BitBtn1Click(Sender: TObject);
	private
	public
		kilepes  : boolean;
	end;

var
	FizubeDlg: TFizubeDlg;

implementation

uses
	Egyeb, Kozos;
{$R *.DFM}

procedure TFizubeDlg.Tolto(s1, s2, s3, s4, s5, s6 : string);
begin
	kilepes		:= false;
	M1.Text		:= s1;
	M2.Text		:= s2;
	M3.Text		:= s3;
	M4.Text		:= s4;
	M5.Text		:= s5;
	M6.Text		:= s6;
end;

procedure TFizubeDlg.BitElkuldClick(Sender: TObject);
begin
	kilepes	:= false;
	Close;
end;

procedure TFizubeDlg.BitKilepClick(Sender: TObject);
begin
	kilepes	:= true;
	Close;
end;

procedure TFizubeDlg.M4Exit(Sender: TObject);
begin
   SzamExit(Sender);
end;

procedure TFizubeDlg.M4KeyPress(Sender: TObject; var Key: Char);
begin
	with Sender as TMaskEdit do begin
		Key := EgyebDlg.Jochar(Text,Key,6,2);
	end;
end;

procedure TFizubeDlg.BitBtn1Click(Sender: TObject);
begin
	// A sz�t�ri adatok beolvas�sa
	if M1.Text + M2.Text + M3.Text <> '' then begin
		if NoticeKi('Figyelem! A jelenlegi �rt�kek elvesznek! Folytatja? ', NOT_QUESTION) <> 0 then begin
			Exit;
		end;
	end;
	M1.Text := EgyebDlg.Read_SZGrid('999', '730');
	M2.Text	:= EgyebDlg.Read_SZGrid('999', '732');
	M3.Text	:= EgyebDlg.Read_SZGrid('999', '734');
	M4.Text	:= EgyebDlg.Read_SZGrid('999', '731');
	M5.Text	:= EgyebDlg.Read_SZGrid('999', '733');
	M6.Text	:= EgyebDlg.Read_SZGrid('999', '735');
end;

end.
