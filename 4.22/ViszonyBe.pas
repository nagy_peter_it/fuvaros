unit Viszonybe;

interface

uses
	SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
	Forms, Dialogs, StdCtrls, Buttons, Mask, DB, DBTables, Egyeb, J_SQL,
   VevoUjfm, ExtCtrls, Kozos, ADODB;

type
	TViszonybeDlg = class(TForm)
	BitElkuld: TBitBtn;
	BitKilep: TBitBtn;
	Label2: TLabel;
    M1: TMaskEdit;
	Label8: TLabel;
	Label12: TLabel;
    M2: TMaskEdit;
    Label1: TLabel;
    Label3: TLabel;
    M7: TMaskEdit;
    CValuta: TComboBox;
    Label4: TLabel;
    Label5: TLabel;
    M3: TMaskEdit;
    M4: TMaskEdit;
	 BitBtn1: TBitBtn;
    Label10: TLabel;
    GroupBox1: TGroupBox;
    Label9: TLabel;
    M6: TMaskEdit;
    M5: TMaskEdit;
    Label7: TLabel;
    RG1: TRadioGroup;
    RG2: TRadioGroup;
    CheckBox1: TCheckBox;
    Label6: TLabel;
    M8: TMaskEdit;
    Label11: TLabel;
	 Label13: TLabel;
    Label14: TLabel;
    M9: TMaskEdit;
    M10: TMaskEdit;
    BitBtn10: TBitBtn;
    BitBtn2: TBitBtn;
    Query1: TADOQuery;
    Query2: TADOQuery;
    BitBtn3: TBitBtn;
    BitBtn4: TBitBtn;
    MPozic: TMaskEdit;
    Label15: TLabel;
    BitBtn5: TBitBtn;
    BitBtn6: TBitBtn;
    MHonnan: TMaskEdit;
    MHova: TMaskEdit;
    Label16: TLabel;
    Label17: TLabel;
    CSzlaValuta: TComboBox;
	procedure BitKilepClick(Sender: TObject);
	procedure FormCreate(Sender: TObject);
	procedure Tolt(cim : string; teko:string; jarkod:string);
	procedure BitElkuldClick(Sender: TObject);
  procedure MaskEditClick(Sender: TObject);
	 procedure Modosit(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure BitBtn1Click(Sender: TObject);
    procedure M4Exit(Sender: TObject);
    procedure MaskKeyPress(Sender: TObject; var Key: Char);
    procedure CValutaChange(Sender: TObject);
    procedure CheckBox1Click(Sender: TObject);
    procedure RG2Click(Sender: TObject);
    procedure M8KeyPress(Sender: TObject; var Key: Char);
    procedure M8Exit(Sender: TObject);
    procedure OsszegBonto;
    procedure M5Exit(Sender: TObject);
    procedure M9Exit(Sender: TObject);
    procedure M10Exit(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn10Click(Sender: TObject);
    procedure Arfolyamszamito;
	 procedure ToltMegbizas(mekod, jarkod : string);
    procedure BitBtn3Click(Sender: TObject);
    procedure BitBtn4Click(Sender: TObject);
    procedure BitBtn5Click(Sender: TObject);
    procedure BitBtn6Click(Sender: TObject);
    procedure SetVevoSzlaValuta(VEKOD: string);
	private
		arany			: double;
	  modosult 	: boolean;
	listavnem	: TStringList;
	  jaratkod		: string;
	  varnap		: integer;
	public
		ret_kod 		: string;
	end;

var
	ViszonybeDlg: TViszonybeDlg;

implementation

uses
	ArfolyView, Vevobe, Kozos_Local, FelrakoFm;
{$R *.DFM}

procedure TViszonybeDlg.BitKilepClick(Sender: TObject);
begin
	if modosult then begin
  	if NoticeKi('M�dosultak az adatok. Ki akar l�pni ment�s n�lk�l?', NOT_QUESTION) = 0 then begin
			ret_kod := '';
			Close;
     end;
  end else begin
		ret_kod := '';
		Close;
  end;
end;

procedure TViszonybeDlg.FormCreate(Sender: TObject);
var
	str	: string;
	sorsz : integer;
begin
	EgyebDlg.SetADOQueryDatabase(Query1);
	EgyebDlg.SetADOQueryDatabase(Query2);
	ret_kod 	:= '';
	varnap		:= 0;
	modosult 	:= false;
	listavnem	:= TStringList.Create;
	SzotarTolt(CValuta, '110' , listavnem, false );
	// SzotarTolt(CSzlaValuta, '110' , listavnem, false );  -- csak EUR, HUF
	{A v�rosok felt�lt�se}
	CheckBox1Click(Sender);
	{Az ar�ny % beolvas�sa }
	arany 			:= StringSzam(EgyebDlg.Read_SZGrid('999','601'));
	M8.Text 		:= SzamString(arany, 6, 2);
	RG2Click(Sender);
	MHonnan.Text	:= '';
	MHova.Text		:= '';
	{Alap�rtelmezett valutanem be�ll�t�sa}
	str	:= EgyebDlg.Read_SZGrid('999','661');
	sorsz := listavnem.IndexOf(str);
	if sorsz < 0 then begin
		sorsz := 0;
	end;
	CValuta.ItemIndex  := sorsz;
  // CSzlaValuta.ItemIndex  := sorsz;  // alapb�l ugyanaz, mint a VISZONY valutaneme
	Query1.Close;
end;

procedure TViszonybeDlg.Tolt(cim : string; teko:string; jarkod:string);
var
  sorsz		: integer;
begin
	jaratkod		:= jarkod;
	ret_kod 		:= teko;
	Caption 		:= cim;
	M1.Text 		:= '';
	M2.Text 		:= '';
	M3.Text 		:= '';
	M4.Text 		:= '';
	M5.Text 		:= '';
	M6.Text 		:= '';
	M7.Text 		:= '';
	if ret_kod <> '' then begin		{M�dos�t�s}
		if Query_Run (Query2, 'SELECT * FROM VISZONY WHERE VI_VIKOD = '''+teko+''' ',true) then begin
			MHonnan.Text	:= Query2.FieldByName('VI_HONNAN').AsString;
			MHova.Text		:= Query2.FieldByName('VI_HOVA').AsString;
			M1.Text 		:= Query2.FieldByName('VI_VEKOD').AsString;
			M2.Text 		:= Query2.FieldByName('VI_VENEV').AsString;
			MPozic.Text 	:= Query2.FieldByName('VI_POZIC').AsString;
			sorsz 			:= listavnem.IndexOf(Query2.FieldByName('VI_VALNEM').AsString);
			if sorsz < 0 then begin
				sorsz := 0;
			end;
			CSzlaValuta.ItemIndex 	:= CSzlaValuta.Items.IndexOf(Query2.FieldByName('VI_SZLAVAL').AsString);
			M3.Text 			:= SzamString(Query2.FieldByName('VI_ARFOLY').AsFloat,12,2);
			M4.Text 			:= SzamString(Query2.FieldByName('VI_FDEXP').AsFloat,12,2);
			M5.Text 			:= SzamString(Query2.FieldByName('VI_FDIMP').AsFloat,12,2);
			M6.Text 			:= SzamString(Query2.FieldByName('VI_FDKOV').AsFloat,12,2);
			M7.Text 			:= Query2.FieldByName('VI_MEGJ').AsString;
			M9.Text 			:= Query2.FieldByName('VI_BEDAT').AsString;
			M10.Text 			:= Query2.FieldByName('VI_KIDAT').AsString;
			Label16.Caption:= Query2.FieldByName('VI_ARFDAT').AsString;
			RG2.ItemIndex 		:= Query2.FieldByName('VI_KULF').AsInteger;
			RG1.ItemIndex		:= StrToIntDef(Query2.FieldByName('VI_TIPUS').AsString,0);
			varnap				:= StrToIntDef(Query_Select('VEVO','V_KOD', Query2.FieldByName('VI_VEKOD').AsString, 'V_ARNAP' ),0);
		end;
	end;
	modosult 	:= false;
end;

procedure TViszonybeDlg.BitElkuldClick(Sender: TObject);
var
  S: string;
begin
	if ret_kod = '' then begin
		{�j rekord felvitele}
		// ret_kod := IntToStr(GetNextStrCode('VISZONY', 'VI_VIKOD', 0, 5,True));
    ret_kod := IntToStr(GetNewIDLoop(modeGetNextStrCode, 'VISZONY', 'VI_VIKOD', '', 0, 6,True));
    if (ret_kod='0') or (ret_kod='') then begin
       S:= 'Sikertelen k�d gener�l�s, k�rem pr�b�lja �jra! (ViszonylatElkuld)';
       HibaKiiro(S);
       NoticeKi(S);
       Exit;
       end;
		Query_Run ( Query2, 'INSERT INTO VISZONY (VI_VIKOD) VALUES (''' +ret_kod+ ''' )',true);
	end;

	{A r�gi rekord m�dos�t�sa}
	Query_Update (Query2, 'VISZONY', [
		'VI_HONNAN',	''''+copy(MHonnan.Text,1,80)+'''',
		'VI_HOVA',		''''+copy(MHova.Text,1,80)+'''',
		'VI_JAKOD',		''''+jaratkod+'''',
		'VI_VEKOD',		''''+M1.Text+'''',
		'VI_VENEV',		''''+M2.Text+'''',
		'VI_POZIC',		''''+copy(MPozic.Text, 1, 80)+'''',
		'VI_VALNEM',	''''+listavnem[CValuta.ItemIndex]+'''',
		'VI_SZLAVAL',	''''+CSzlaValuta.Text+'''',
		'VI_KULF', 		BoolToStr01(RG2.ItemIndex = 1),
		'VI_ARFOLY',	SqlSzamString(StringSzam(M3.Text),12,4),
		'VI_ARFDAT',	''''+Label16.Caption+'''',
		'VI_FDEXP',		SqlSzamString(StringSzam(M4.Text),12,4),
		'VI_FDIMP',		SqlSzamString(StringSzam(M5.Text),12,4),
		'VI_FDKOV',		SqlSzamString(StringSzam(M6.Text),12,4),
		'VI_TIPUS',		IntToStr(RG1.ItemIndex),
		'VI_BEDAT',		''''+M9.Text+'''',
		'VI_KIDAT',		''''+M10.Text+'''',
		'VI_MEGJ',		''''+M7.Text+''''
	], ' WHERE VI_VIKOD = '''+ret_kod+''' ' );

  Query2.Close;
//	Close;
 {if EgyebDlg.AUTOSZAMLA then begin
     JaratbeDlg.BitBtn11.OnClick(self);
  end;  }
  close;
end;

procedure TViszonybeDlg.MaskEditClick(Sender: TObject);
begin
	with Sender as TMaskEdit do begin
	SelectAll;
  end;
end;

procedure TViszonybeDlg.Modosit(Sender: TObject);
begin
	modosult := true;
end;

procedure TViszonybeDlg.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
	EgyebDlg.FormReturn(Key);
end;

procedure TViszonybeDlg.BitBtn1Click(Sender: TObject);
begin
	{A megb�z� beolvas�sa}
	Application.CreateForm(TVevoUjFmDlg, VevoUjFmDlg);
	VevoUjFmDlg.valaszt := true;
	VevoUjFmDlg.ShowModal;
	if VevoUjFmDlg.ret_vkod <> '' then begin
		if VevoUjFmDlg.felista > 0 then begin
			if NoticeKi('Feketelist�s vev�! Folytatja?', NOT_QUESTION) <> 0 then begin
				VevoUjFmDlg.Destroy;
				Exit;
			end;
		end;
		if Query_Run (Query1, 'SELECT * FROM VEVO WHERE V_KOD = '''+VevoUjFmDlg.ret_vkod+''' ',true) then begin
			M1.Text 	:= Query1.FieldByName('V_KOD').AsString;
			M2.Text 	:= Query1.FieldByName('V_NEV').AsString;
			varnap	:= StrToIntDef(Query1.FieldByName('V_ARNAP').AsString,0);
			Arfolyamszamito;
      SetVevoSzlaValuta(M1.Text);
			CValuta.SetFocus;
		end;
	end;
	VevoUjFmDlg.Destroy;
end;

procedure TViszonybeDlg.M4Exit(Sender: TObject);
begin
	with Sender as TMaskEdit do begin
		Text := StrSzamStr( Text , 12, Tag);
  end;
  OsszegBonto;
end;

procedure TViszonybeDlg.MaskKeyPress(Sender: TObject; var Key: Char);
begin
  with Sender as TMaskEdit do begin
  	Key := EgyebDlg.Jochar(Text,Key,12,Tag);
  end;
end;

procedure TViszonybeDlg.CValutaChange(Sender: TObject);
begin
  Arfolyamszamito;
end;

procedure TViszonybeDlg.CheckBox1Click(Sender: TObject);
begin
	if CheckBox1.Checked then begin
		SetMaskEdits([M5, M6], 1);
    M8.SetFocus;
  end else begin
		SetMaskEdits([M5, M6]);
  end;
end;

procedure TViszonybeDlg.RG2Click(Sender: TObject);
begin
	if RG2.ItemIndex = 0 then begin
		{Belf�ldin�l nem kell %}
		SetMaskEdits([M8]);
	end else begin
		SetMaskEdits([M8], 1);
	end;
	OsszegBonto;
end;

procedure TViszonybeDlg.M8KeyPress(Sender: TObject; var Key: Char);
begin
	with Sender as TMaskEdit do begin
		Key := EgyebDlg.Jochar(Text,Key,6,2);
	end;
end;

procedure TViszonybeDlg.M8Exit(Sender: TObject);
begin
	EgyebDlg.Joszazalek(M8);
	arany	:= StringSzam(M8.Text);
	OsszegBonto;
end;

procedure TViszonybeDlg.OsszegBonto;
var
	alap : double;
begin
	{Az �sszeg megbont�sa ha k�lf�ldi}
	if RG2.ItemIndex = 1 then begin
		{K�lf�ldir�l van sz�}
		alap		:= StringSzam(M4.Text);
		M5.Text 	:= SzamString(alap*arany/100,12,2);
		M6.Text 	:= SzamString(alap*(100-arany)/100,12,2);
	end else begin
		{Belf�ldir�l van sz�}
		M5.Text := M4.Text;
		M6.Text := '';
	end;
end;

procedure TViszonybeDlg.M5Exit(Sender: TObject);
begin
	with Sender as TMaskEdit do begin
		Text := StrSzamStr( Text , 12, Tag);
	end;
end;

procedure TViszonybeDlg.M9Exit(Sender: TObject);
begin
	DatumExit(Sender, false);
  Arfolyamszamito;
end;

procedure TViszonybeDlg.M10Exit(Sender: TObject);
begin
	DatumExit(Sender, false);
  Arfolyamszamito;
end;

procedure TViszonybeDlg.BitBtn2Click(Sender: TObject);
begin
	EgyebDlg.Calendarbe(M9);
  	Arfolyamszamito;
end;

procedure TViszonybeDlg.BitBtn10Click(Sender: TObject);
begin
	EgyebDlg.Calendarbe(M10);
  	Arfolyamszamito;
end;

procedure TViszonybeDlg.Arfolyamszamito;
begin
  // A vev� �rfolyamnapj�nak �rtelmez�se 1:lerak�s 0:felrak�s
  	if varnap = 1 then begin
  		M3.Text	:= SzamString(EgyebDlg.ArfolyamErtek(listavnem[CValuta.ItemIndex], M10.Text, true, true),12,2);
      Label16.Caption:=M10.Text;
  	end else begin
  		M3.Text	:= SzamString(EgyebDlg.ArfolyamErtek(listavnem[CValuta.ItemIndex], M9.Text, true, true),12,2);
      Label16.Caption:=M9.Text;
  	end;
end;

procedure TViszonybeDlg.ToltMegbizas(mekod, jarkod : string);
var
  sorsz			: integer;
  FUDIJ, SEDIJ, OSSZDIJ, SEDIJ_konvertalva, FUARF, SEARF: double;
  FUNEM, SENEM, arfdatum, Vevo_Nemet, VevoSzlaValuta: string;
begin
	jaratkod		:= jarkod;
	ret_kod 		:= '';
	Caption 		:= 'Megb�z�shoz tartoz� viszonylat felvitele';
	M1.Text 		:= '';
	M2.Text 		:= '';
	M3.Text 		:= '';
	M4.Text 		:= '';
	M5.Text 		:= '';
	M6.Text 		:= '';
	M7.Text 		:= '';
	if Query_Run (Query2, 'SELECT * FROM MEGBIZAS WHERE MB_MBKOD = '+mekod,true) then begin
		M1.Text 	:= Query2.FieldByName('MB_VEKOD').AsString;
		M7.Text 	:= Query2.FieldByName('MB_MEGJE').AsString;
		MPozic.Text := Query2.FieldByName('MB_POZIC').AsString;
		M2.Text 	:= Query_Select('VEVO', 'V_KOD', M1.Text, 'V_NEV');
		sorsz 		:= listavnem.IndexOf(Query2.FieldByName('MB_FUNEM').AsString);
		if sorsz < 0 then begin
			sorsz := 0;
		end;
    CValuta.ItemIndex := sorsz;
    SetVevoSzlaValuta(M1.Text);
		M4.Text    	:= SzamString(StringSzam(Query2.FieldByName('MB_FUDIJ').AsString),12,2);

    // --- hozz�adjuk az egy�b d�jat is, �s �gy tessz�k be ------
    //  M�GSE....
    {
    FUDIJ:= Query2.FieldByName('MB_FUDIJ').AsFloat;
    SEDIJ:= Query2.FieldByName('MB_SEDIJ').AsFloat;
    FUNEM:= Query2.FieldByName('MB_FUNEM').AsString;
    SENEM:= Query2.FieldByName('MB_SENEM').AsString;
    if FUNEM = SENEM then begin
      OSSZDIJ:= FUDIJ + SEDIJ;
      end
    else begin
      arfdatum:= GetArfolyamDat(Query2.FieldByName('MB_MBKOD').AsString);
      FUARF:= EgyebDlg.ArfolyamErtek(FUNEM, arfdatum, true, true);
      SEARF:= EgyebDlg.ArfolyamErtek(SENEM, arfdatum, true, true);
      if FUARF <> 0 then
        SEDIJ_konvertalva:= SEDIJ * SEARF / FUARF // kereszt�rfolyammal
      else begin
        SEDIJ_konvertalva:= 0;
        NoticeKi('Nincs '+FUNEM+' �rfolyam '+ arfdatum + ' napra! Az egy�b d�jat nem tudtuk hozz�adni a fuvard�jhoz.');
        end;
      OSSZDIJ:= FUDIJ + SEDIJ_konvertalva;
      end;
    M4.Text:= SzamString(OSSZDIJ,12,2);
    }
    // ------------------------------------------------------------

		RG2.ItemIndex 	:= 0;
		RG1.ItemIndex	:= 0;
		varnap			:= StrToIntDef(Query_Select('VEVO','V_KOD', Query2.FieldByName('MB_VEKOD').AsString, 'V_ARNAP' ),0);
		MHonnan.Text	:= '';
		MHova.Text		:= '';
		M9.Text			:= '9999.99.99';
		M10.Text		:= '';
		if Query_Run (Query2, 'SELECT * FROM MEGSEGED WHERE MS_MBKOD = '+mekod+' ORDER BY MS_SORSZ',true) then begin
			while not Query2.Eof do begin
				if Pos(Query2.FieldByName('MS_FELTEL').AsString, MHonnan.Text) < 1 then begin
					// MHonnan.Text	:= MHonnan.Text	+ Query2.FieldByName('MS_FELIR').AsString + ' ' + Query2.FieldByName('MS_FELTEL').AsString + '(' +Query2.FieldByName('MS_FORSZ').AsString+');';
          MHonnan.Text:= MHonnan.Text	+ Query2.FieldByName('MS_FORSZ').AsString+' '+Query2.FieldByName('MS_FELIR').AsString + ' ' + Query2.FieldByName('MS_FELTEL').AsString +';';
				end;
				if Pos(Query2.FieldByName('MS_LERTEL').AsString, MHova.Text) < 1 then begin
					// MHova.Text		:= MHova.Text	+ Query2.FieldByName('MS_LELIR').AsString + ' ' + Query2.FieldByName('MS_LERTEL').AsString + '(' +Query2.FieldByName('MS_ORSZA').AsString+');';
          MHova.Text:= MHova.Text	+ Query2.FieldByName('MS_ORSZA').AsString+' '+Query2.FieldByName('MS_LELIR').AsString + ' ' + Query2.FieldByName('MS_LERTEL').AsString +';';
				end;
				if M9.Text > Query2.FieldByName('MS_FELDAT').AsString then begin
					M9.Text    		:= Query2.FieldByName('MS_FETDAT').AsString;
					//M9.Text    		:= Query2.FieldByName('MS_FELDAT').AsString;
				end;
				if M10.Text  < Query2.FieldByName('MS_LERDAT').AsString then begin
					M10.Text  		:= Query2.FieldByName('MS_LETDAT').AsString;
					//M10.Text  		:= Query2.FieldByName('MS_LERDAT').AsString;
				end;
				Query2.Next;
			end;
		end;
		if MHonnan.Text > '' then begin
			MHonnan.Text	:= copy(copy(MHonnan.Text, 1, Length(MHonnan.Text)-1),1,80);
		end;
		if MHova.Text > '' then begin
			MHova.Text	:= copy(copy(MHova.Text, 1, Length(MHova.Text)-1),1,80);
		end;
		ArfolyamSzamito;
	end;
  	modosult 	:= false;
end;

procedure TViszonybeDlg.BitBtn3Click(Sender: TObject);
begin
	// A k�t d�tumhoz tartoz� �rfolyam megjelen�t�se
	Application.CreateForm(TArfolyViewDlg, ArfolyViewDlg);
   ArfolyViewDlg.Tolto(M9.Text, M10.Text);
	ArfolyViewDlg.ShowModal;
	ArfolyViewDlg.Destroy;
end;

procedure TViszonybeDlg.BitBtn4Click(Sender: TObject);
begin
	// Beolvassuk a vev�t
	if M1.Text = '' then begin
		NoticeKi('M�g nincs megb�z� kiv�lasztva!');
       Exit;
   end;
   Application.CreateForm(TVevobeDlg, VevobeDlg);
   VevobeDlg.Tolto('Vev�i adatok m�dos�t�sa', M1.Text);
   VevobeDlg.ShowModal;
   if VevobeDlg.ret_kod <> '' then begin
       if Query_Run (Query1, 'SELECT * FROM VEVO WHERE V_KOD = '''+VevobeDlg.ret_kod+''' ',true) then begin
			    M1.Text 	:= Query1.FieldByName('V_KOD').AsString;
			    M2.Text 	:= Query1.FieldByName('V_NEV').AsString;
			    varnap		:= StrToIntDef(Query1.FieldByName('V_ARNAP').AsString,0);
          Arfolyamszamito;
          SetVevoSzlaValuta(M1.Text);
          CValuta.SetFocus;
       end;
   end;
   VevobeDlg.Destroy;
end;

procedure TViszonybeDlg.SetVevoSzlaValuta(VEKOD: string);
var
    Vevo_Nemet, VevoSzlaValuta: string;
begin
    Vevo_Nemet:= Query_Select('VEVO', 'V_KOD', VEKOD, 'V_NEMET');
    if Vevo_Nemet='1' then
        VevoSzlaValuta:= 'EUR'
    else VevoSzlaValuta:= 'HUF';
    CSzlaValuta.ItemIndex := CSzlaValuta.Items.IndexOf(VevoSzlaValuta);
end;

procedure TViszonybeDlg.BitBtn5Click(Sender: TObject);
begin
	// Kiv�lasztjuk a k�v�nt felrak�t / lerak�t
	Application.CreateForm(TFelrakoFmDlg, FelrakoFmDlg);
	FelrakoFmDlg.valaszt	:= true;
	FelrakoFmDlg.ShowModal;
	if FelrakoFmDlg.ret_vkod <> '' then begin
		Query_Run(Query1, 'SELECT * FROM FELRAKO WHERE FF_FEKOD = '+FelrakoFmDlg.ret_vkod);
		MHonnan.Text	:= Query1.FieldByName('FF_FELIR').AsString + ' ' + Query1.FieldByName('FF_FELTEL').AsString + '(' +Query1.FieldByName('FF_ORSZA').AsString+')';
	end;
	FelrakoFmDlg.Destroy;
end;

procedure TViszonybeDlg.BitBtn6Click(Sender: TObject);
begin
	// Kiv�lasztjuk a k�v�nt felrak�t / lerak�t
	Application.CreateForm(TFelrakoFmDlg, FelrakoFmDlg);
	FelrakoFmDlg.valaszt	:= true;
	FelrakoFmDlg.ShowModal;
	if FelrakoFmDlg.ret_vkod <> '' then begin
		Query_Run(Query1, 'SELECT * FROM FELRAKO WHERE FF_FEKOD = '+FelrakoFmDlg.ret_vkod);
		MHova.Text	:= Query1.FieldByName('FF_FELIR').AsString + ' ' + Query1.FieldByName('FF_FELTEL').AsString + '(' +Query1.FieldByName('FF_ORSZA').AsString+')';
	end;
	FelrakoFmDlg.Destroy;
end;

end.
