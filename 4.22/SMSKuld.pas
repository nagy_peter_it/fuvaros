unit SMSKuld;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Kozos, MSHTML, SHDocVw, ShellAPI, Egyeb,
  Vcl.OleCtrls, HTTPRutinok, json, J_SQL, Data.DBXJSON, Vcl.ExtCtrls, ADODB;

type
  TSMSKuldDlg = class(TForm)
    ebSzoveg: TEdit;
    ebSzam: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    Button1: TButton;
    ebResult: TEdit;
    ebResult2: TEdit;
    Edit2: TEdit;
    Label4: TLabel;
    Panel1: TPanel;
    Label3: TLabel;
    ebCustomRequest: TEdit;
    Button2: TButton;
    ebAuthToken: TEdit;
    Label5: TLabel;
    Button3: TButton;
    ebResult3: TEdit;
    ebRef: TEdit;
    Label6: TLabel;
    Edit1: TEdit;
    Edit3: TEdit;
    Button4: TButton;
    procedure Button1Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
  private
    // function ExecuteScript(doc: IHTMLDocument2; script: string; language: string): Boolean;
    function MySMS_Post(RefPoint, RequestJSON: string): string;
    function GetAuthToken(JSONString: string): string;
    function GetMySMSErrorCode(JSONString: string): integer;
    function RequestAuthToken: string;
    function TESZT_SendSms: boolean;
    procedure SzalakFeldolgozas(JSONString, KeresettTelefonszam: string);
    function UpdateEgySzal(Telefonszam: string; UzenetDarabszam, TaroltUzenetDarabszam: integer): string;
    function GetMaxUzenetID(Telefonszam: string): integer;
    function GetUzenetDarabszam(Telefonszam: string): integer;
    function UtolsoUzenetEllenorzes(JSONString: string): string;
    procedure SzalUpdate(Telefonszam, LegutobbiUzenetReszlet: string; UzenetekSzama, OlvasatlanDB,
        ElkuldetlenDB, UtolsoUzenetID: integer; LegutobbiUzenetDatum: string; UtolsoTaroltUzenetID, TaroltUzenetDarabszam: integer);
    function KuldesHibaUzenet(Hibakod: integer): string;
  public
    APIKey, APIPassword, SendingPhone: string;
    procedure InitMySMSAuthToken;
    function SendMySms(Szamok, Szoveg: string): string;
    function RequestConversations: string;
    function RequestThread(PhoneNumber: string; Offset, Limit: integer): string;
    procedure RefreshAllConversations;
    procedure RefreshConversation (KeresettTelefonszam: string);
    procedure Proba;
  end;
const
  MySMSRoot = 'https://api.mysms.com';
  // APIKey = 'zDVf5m0vqwu1cs0H_4ju6Q';
  // APIPassword='jssms';
  // SendingPhone = '36209589455';

var
  SMSKuldDlg: TSMSKuldDlg;
  AuthToken: string;
  TransactionQuery: TADOQuery;
implementation

{$R *.dfm}

procedure TSMSKuldDlg.InitMySMSAuthToken;
const
  trycount = 5;
var
  i: integer;
  S: string;
begin
  i:=1;
  AuthToken:= '';
  while (AuthToken= '') and (i<= trycount) do begin
    AuthToken:= RequestAuthToken;
    Sleep(1000);
    Inc(i);
    end;  // while
  if AuthToken= '' then begin
    S:= 'Sikertelen MySMS autoriz�ci�! K�rem l�pjen ki a programb�l �s l�pjen be �jra.';
    NoticeKi(S);
    Hibakiiro(S);
    end;
end;

procedure TSMSKuldDlg.Button1Click(Sender: TObject);
begin
  SendMySms(ebSzam.Text, ebSzoveg.Text);
end;

procedure TSMSKuldDlg.Button2Click(Sender: TObject);
begin
  ebResult3.Text:= UTF8Decode(MySMS_Post(ebRef.Text, ebCustomRequest.Text));
end;

procedure TSMSKuldDlg.Button3Click(Sender: TObject);
begin
  ebAuthToken.Text:= RequestAuthToken;
end;

procedure TSMSKuldDlg.Button4Click(Sender: TObject);
begin
   Edit3.Text:= UTF8Decode(Edit1.Text);
end;

function TSMSKuldDlg.RequestAuthToken: string;
var
   PostResultJSON, JSONString, Res: string;
begin
   JSONString := '{"apiKey": "'+APIKEY+'", "msisdn": "'+SendingPhone+'", "password": "'+APIPassword+'"}';
   PostResultJSON:= MySMS_Post('/json/user/login', JSONString);
   Res:= GetAuthToken(PostResultJSON);
   if Res <> '' then
      Result:= Res
   else begin
      Hibakiiro('SMS k�ld�s - API authoriz�l�si k�r�s: '+JSONString);
      Result:= '';
      end;  // else


end;

function TSMSKuldDlg.RequestConversations: string;
var
   JSONString: string;
begin
   JSONString := '{"authToken": "'+AuthToken+'", "apiKey": "'+APIKEY+'"}';
   Result:= MySMS_Post('/json/user/message/conversations/get', JSONString);
end;

function TSMSKuldDlg.RequestThread(PhoneNumber: string; Offset, Limit: integer): string;
// offset: id�rendben visszafel� h�nyadik �zenett�l k�rdezz�k le, 0-t�l indul!
// Limit: h�ny �zenetet k�r�nk
var
   JSONString, PostResultJSON, HibaString: string;
begin
   // A PhoneNumber-ben m�r benne van az id�z�jel
   JSONString := '{"address": '+PhoneNumber+', "offset": '+IntToStr(Offset)+', "limit": '+IntToStr(Limit)+', ' +
      '"authToken": "'+AuthToken+'", "apiKey": "'+APIKEY+'"}';
   PostResultJSON:= MySMS_Post('/json/user/message/get/by/conversation', JSONString);
   if pos('Hiba a HTTP Post', PostResultJSON)>0 then begin
      HibaString:= 'Sz�l lek�rdez�s - HTTP hiba: "'+PostResultJSON+'". Request: '+JSONString;
      HibaKiiro(HibaString);
      Result:= '';
      end
   else begin  // nem volt HTTP hiba
      Result:= PostResultJSON;
      end;
end;

procedure TSMSKuldDlg.RefreshAllConversations;
begin
  RefreshConversation(''); // ha nem adunk meg sz�mot, mindet friss�ti
end;

procedure TSMSKuldDlg.RefreshConversation (KeresettTelefonszam: string);
var
   JSON: string;
begin
  if EgyebDlg.SMS_KULDES_FORMA='MYSMS' then begin
    JSON:= RequestConversations;
    SzalakFeldolgozas(JSON, KeresettTelefonszam);
    end;
end;

procedure TSMSKuldDlg.SzalakFeldolgozas(JSONString, KeresettTelefonszam: string);
var
  JSON: TJSONObject;
  vJSONScenario, vJSONValue, Szalak: TJSONValue;
  retvalue, HibaUzenetek, HibaString, Telefonszam, JSDate, LegutobbiUzenetDatum, Tulajdonos: string;
  LegutobbiUzenetReszlet: string;
  KeyName: string;
  Hibakod, UtolsoUzenetID, UtolsoTaroltUzenetID, OlvasatlanDB, ElkuldetlenDB, UzenetekSzama, TaroltUzenetDarabszam: integer;
  VoltHiba: boolean;
begin
   vJSONScenario := TJSONObject.ParseJSONValue(JSONString);    // {"errorCode":0,"authToken":"4566sdfv", ....}
   try
     VoltHiba:= False;
     HibaUzenetek:= '';
     if vJSONScenario <> nil then begin
        Hibakod:= StrToInt(((vJSONScenario as TJSONObject).Get('errorCode').JsonValue as TJSONString).Value);
        if Hibakod <> 0 then begin
            HibaString:= 'Sz�lak lek�rdez�se - MySMS hiba: "'+JSONString;
            HibaKiiro(HibaString);
            VoltHiba:= True;
            HibaUzenetek:= HibaUzenetek+' | '+HibaString;
          end
        else begin
          Szalak:= (vJSONScenario as TJSONObject).Get('conversations').JsonValue;
          for vJSONValue in Szalak as TJSONArray do begin
             Telefonszam:= ((vJSONValue as TJSONObject).Get('address').JsonValue as TJSONString).Value;
             if (KeresettTelefonszam = '') or (KeresettTelefonszam = Telefonszam) then begin
               UtolsoUzenetID:= StrToInt(((vJSONValue as TJSONObject).Get('maxMessageId').JsonValue as TJSONString).Value);
               UtolsoTaroltUzenetID:= GetMaxUzenetID(Telefonszam);
               TaroltUzenetDarabszam:= GetUzenetDarabszam(Telefonszam);
               // debug
               // if (Telefonszam='+36209588663') and (UtolsoUzenetID > UtolsoTaroltUzenetID) then begin  // csak ekkor dolgozzuk fel
               if (UtolsoUzenetID > UtolsoTaroltUzenetID) then begin  // csak ekkor dolgozzuk fel
                   UzenetekSzama:= StrToInt(((vJSONValue as TJSONObject).Get('messages').JsonValue as TJSONString).Value);
                   JSDate:= ((vJSONValue as TJSONObject).Get('dateLastMessage').JsonValue as TJSONString).Value;
                   LegutobbiUzenetDatum:= JavaScriptTimestampToFuvaros(StrToInt64(JSDate));
                   LegutobbiUzenetReszlet:= ((vJSONValue as TJSONObject).Get('snippet').JsonValue as TJSONString).Value;
                   OlvasatlanDB:= StrToInt(((vJSONValue as TJSONObject).Get('messagesUnread').JsonValue as TJSONString).Value);
                   ElkuldetlenDB:= StrToInt(((vJSONValue as TJSONObject).Get('messagesUnsent').JsonValue as TJSONString).Value);
                   SzalUpdate(Telefonszam, LegutobbiUzenetReszlet, UzenetekSzama, OlvasatlanDB, ElkuldetlenDB, UtolsoUzenetID,
                      LegutobbiUzenetDatum, UtolsoTaroltUzenetID, TaroltUzenetDarabszam);
                   end;  // if
               end;  // if KeresettTelefonszam
             end;  // for
          end;  // else
        end; // if vJSONScenario
   finally
      if vJSONScenario<>nil then vJSONScenario.Free;
      end;  // try-finally
end;


procedure TSMSKuldDlg.SzalUpdate(Telefonszam, LegutobbiUzenetReszlet: string; UzenetekSzama, OlvasatlanDB,
        ElkuldetlenDB, UtolsoUzenetID: integer; LegutobbiUzenetDatum: string; UtolsoTaroltUzenetID, TaroltUzenetDarabszam: integer);
var
  Res, S: string;
begin
  TransactionQuery:= TADOQuery.Create(nil);
	TransactionQuery.Tag:= QUERY_TRANSACTION_TAG;
	EgyebDlg.SetADOQueryDatabase(TransactionQuery);
  Query_Run(TransactionQuery, 'BEGIN TRANSACTION');
  try  // finally
  try  // except
    Res:= Query_SelectString('SMSSZALAK', 'select count(*) from SMSSZALAK where SZ_TELEF='''+Telefonszam+'''');
    if (Res='0') then begin
      Query_Insert(TransactionQuery, 'SMSSZALAK',
						[
						'SZ_TELEF', ''''+Telefonszam+'''',
						'SZ_RESZL', ''''+LegutobbiUzenetReszlet+'''',
						'SZ_DARAB', IntToStr(UzenetekSzama),
						'SZ_OLVATL', IntToStr(OlvasatlanDB),
						'SZ_KULDETL', IntToStr(ElkuldetlenDB),
						'SZ_UTOID', IntToStr(UtolsoUzenetID),
						'SZ_UTODA', ''''+LegutobbiUzenetDatum+''''
						]);
       end // if
    else begin
      Query_Update(TransactionQuery, 'SMSSZALAK',
						[
						'SZ_RESZL', ''''+LegutobbiUzenetReszlet+'''',
						'SZ_DARAB', IntToStr(UzenetekSzama),
						'SZ_OLVATL', IntToStr(OlvasatlanDB),
						'SZ_KULDETL', IntToStr(ElkuldetlenDB),
						'SZ_UTOID', IntToStr(UtolsoUzenetID),
						'SZ_UTODA', ''''+LegutobbiUzenetDatum+''''
        		], ' WHERE SZ_TELEF = '''+Telefonszam+'''' );
       end;  // else
    S:= UpdateEgySzal(Telefonszam, UzenetekSzama, TaroltUzenetDarabszam);
    // -- akkor is legyen commit, ha m�r l�tez� �zenetet pr�b�l t�rolni ---
    S:='';
    if S='' then begin
      Query_Run(TransactionQuery, 'COMMIT TRANSACTION')
      end
    else begin
      Query_Run(TransactionQuery, 'ROLLBACK TRANSACTION');
      end;
  except
      Query_Run(TransactionQuery, 'ROLLBACK TRANSACTION');
      end;  // try-except
  finally
      if TransactionQuery<>nil then TransactionQuery.Free;
      end;  // try-except
end;


function TSMSKuldDlg.UpdateEgySzal(Telefonszam: string; UzenetDarabszam, TaroltUzenetDarabszam: integer): string;
var
  vJSONScenario, vJSONValue, Uzenetek: TJSONValue;
  retvalue, HibaUzenetek, HibaString, UzenetSzoveg, JSDate, ElkuldveDatum, StatusDatum, S, JSON, Res: string;
  // UzenetSzoveg: AnsiString;
  incoming, VoltHiba: boolean;
  Hibakod, UzenetID, Bejovo, Olvasva, Zarolva, Forras, Allapot: integer;
begin
   HibaUzenetek:= '';
   // JSON:= RequestThread('"'+Telefonszam+'"', UtolsoTaroltUzenetID+1, 999999); // a m�g t�rolatlanok
   JSON:= RequestThread('"'+Telefonszam+'"', 0, UzenetDarabszam-TaroltUzenetDarabszam); // a m�g t�rolatlanok
   vJSONScenario := TJSONObject.ParseJSONValue(JSON);    // {"errorCode":0,"authToken":"4566sdfv", ....}
   try  // finally
    try  // except
     if vJSONScenario <> nil then begin
        Hibakod:= StrToInt(((vJSONScenario as TJSONObject).Get('errorCode').JsonValue as TJSONString).Value);
        if Hibakod <> 0 then begin
            HibaString:= 'Sz�l lek�rdez�se - MySMS hiba: "'+JSON;
            HibaKiiro(HibaString);
            VoltHiba:= True;
            HibaUzenetek:= HibaUzenetek+' | '+HibaString;
          end
        else begin
          Uzenetek:= (vJSONScenario as TJSONObject).Get('messages').JsonValue;
          for vJSONValue in Uzenetek as TJSONArray do begin
             UzenetID:= StrToInt(((vJSONValue as TJSONObject).Get('messageId').JsonValue as TJSONString).Value);
             UzenetSzoveg:= ((vJSONValue as TJSONObject).Get('message').JsonValue as TJSONString).Value;
             JSDate:= ((vJSONValue as TJSONObject).Get('dateSent').JsonValue as TJSONString).Value;
             ElkuldveDatum:= JavaScriptTimestampToFuvaros(StrToInt64(JSDate));
             if (vJSONValue as TJSONObject).Get('dateStatus').JsonValue is TJSONNull then
                StatusDatum:=''
             else begin
               JSDate:= ((vJSONValue as TJSONObject).Get('dateStatus').JsonValue as TJSONString).Value;
               StatusDatum:= JavaScriptTimestampToFuvaros(StrToInt64(JSDate));
               end;  // else
             if (vJSONValue as TJSONObject).Get('incoming').JsonValue is TJSONTrue then
                Bejovo:=1
             else Bejovo:=0;
             if (vJSONValue as TJSONObject).Get('read').JsonValue is TJSONTrue then
                Olvasva:=1
             else Olvasva:=0;
             if (vJSONValue as TJSONObject).Get('locked').JsonValue is TJSONTrue then
                Zarolva:=1
             else Zarolva:=0;
             Forras:= StrToInt(((vJSONValue as TJSONObject).Get('origin').JsonValue as TJSONString).Value);
             Allapot:= StrToInt(((vJSONValue as TJSONObject).Get('status').JsonValue as TJSONString).Value);
              Res:= Query_SelectString('SMSUZENETEK', 'select count(*) from SMSUZENETEK where SU_ID= '+IntToStr(UzenetID));
              if (Res='0') then begin
                Query_Insert(TransactionQuery, 'SMSUZENETEK',
                  [
                  'SU_ID', IntToStr(UzenetID),
                  'SU_TELEF', ''''+Telefonszam+'''',
                  'SU_UZENE', ''''+UzenetSzoveg+'''',
                  'SU_BEJOVO', IntToStr(Bejovo),
                  'SU_OLVVA', IntToStr(Olvasva),
                  'SU_ZARVA', IntToStr(Zarolva),
                  'SU_HONNAN', IntToStr(Forras),
                  'SU_STATUS', IntToStr(Allapot),
                  'SU_KULDI', ''''+ElkuldveDatum+'''',
                  'SU_STATI', ''''+StatusDatum+''''
                  ]);
                end
              else begin  // m�r l�tez� ID, itt valami gubanc van!
                HibaString:= '�zenet insert - l�tez� UzenetID: '+IntToStr(UzenetID);
                HibaKiiro(HibaString);
                VoltHiba:= True;
                HibaUzenetek:= HibaUzenetek+' | '+HibaString;
                end;
             end;  // for
          end;  // else
        end; // if vJSONScenario
     except
       on E: exception do
         // NoticeKi('Hiba "UpdateEgySzal"-ban: '+E.Message);
         Hibakiiro('Hiba "UpdateEgySzal"-ban: '+E.Message);
       end;  // try-except
   finally
      if vJSONScenario<>nil then vJSONScenario.Free;
      end;  // try-finally
   Result:= HibaUzenetek;
end;


function TSMSKuldDlg.GetMaxUzenetID(Telefonszam: string): integer;
var
   S: string;
begin
   S:= Trim(Query_SelectString('SMSUZENETEK', 'select max(SU_ID) from SMSUZENETEK where SU_TELEF = '''+Telefonszam+''''));
   if S='' then
     Result:=-1  // nincs m�g eleme: a 0. elemt�l kell majd lek�rdezni
   else Result:= StrToInt(S);
end;

function TSMSKuldDlg.GetUzenetDarabszam(Telefonszam: string): integer;
var
   S: string;
begin
   S:= Trim(Query_SelectString('SMSUZENETEK', 'select count(SU_ID) from SMSUZENETEK where SU_TELEF = '''+Telefonszam+''''));
   if S='' then
     Result:=-1  // nincs m�g eleme: a 0. elemt�l kell majd lek�rdezni
   else Result:= StrToInt(S);
end;


function TSMSKuldDlg.SendMySms(Szamok, Szoveg: string): string;
const
   CRLF = chr(13)+chr(10);
   TAB = chr(9);
   SendingLimitChars = 750;
var
   PostResultJSON, JSONString, HibaString, KuldendoSzoveg, DarabSzoveg: string;
   HibaUzenetek: string;
   MySMSErrorCode, i: integer;
   VoltHiba: boolean;
begin
   // A sz�vegben a sort�r�st nem szereti
   Szoveg:= StringReplace(Szoveg, CRLF, ' ', [rfReplaceAll]);
   // A TAB-ot sem
   Szoveg:= StringReplace(Szoveg, TAB, ' ', [rfReplaceAll]);
   // Az id�z�jel a JSON miatt escape-elend�
   Szoveg:= StringReplace(Szoveg, '"', '\"', [rfReplaceAll]);


   if AuthToken <> '' then begin
      KuldendoSzoveg:= Szoveg;
      VoltHiba:= False;
      HibaUzenetek:= '';
      while KuldendoSzoveg <> '' do begin
        if Length(KuldendoSzoveg) > SendingLimitChars then begin
          // sz�k�zn�l daraboljuk!
          i:= SendingLimitChars+1;
          repeat
            i:= i-1;
            until copy(KuldendoSzoveg, i, 1) = ' ';
          DarabSzoveg:= copy(KuldendoSzoveg, 1, i);
          KuldendoSzoveg:= copy(KuldendoSzoveg, i+1, 99999999);
          end
        else begin
          DarabSzoveg:= KuldendoSzoveg;
          KuldendoSzoveg:= '';
          end;
        JSONString := '{"recipients": '+Szamok+', "message": "'+DarabSzoveg+'", "encoding": "'+'0'+'", '+
             '"smsConnectorId": "'+'0'+'", "store": '+'true'+', "authToken": "'+AuthToken+'", '+
             '"apiKey": "'+APIKEY+'"}';
        Edit2.text:= JSONString;
        PostResultJSON:= MySMS_Post('/json/remote/sms/send', JSONString);
        ebResult2.Text:= PostResultJSON;
        if pos('Hiba a HTTP Post', PostResultJSON)>0 then begin
            HibaString:= 'SMS k�ld�s - HTTP hiba: "'+PostResultJSON+'". Request: '+JSONString;
            HibaKiiro(HibaString);
            VoltHiba:= True;
            // HibaUzenetek:= HibaUzenetek+' | '+HibaString;
            end
        else begin  // nem volt HTTP hiba
            MySMSErrorCode:= GetMySMSErrorCode(PostResultJSON);   // ellen�rizz�k hogy nincs-e MySMS hibak�d a v�laszban  -- {"errorCode":0,"requestId":296925482,....}
            if MySMSErrorCode <> 0 then begin
              HibaString:= 'SMS k�ld�s - MySMS hiba: "'+KuldesHibaUzenet(MySMSErrorCode)+ ' V�lasz JSON:' +PostResultJSON+'". Request: '+JSONString;
              HibaKiiro(HibaString);
              VoltHiba:= True;
              // HibaUzenetek:= HibaUzenetek+' | '+HibaString;
              end
            else begin  // l�tsz�lag rendben van, az�rt majd visszak�rdez�nk, hogy kiment-e az �zenet
              RefreshConversation('');  // friss�tj�k az adatb�zisban
              HibaString:= UtolsoUzenetEllenorzes(PostResultJSON);
              if HibaString <> '' then begin
                 VoltHiba:= True;
                 // HibaUzenetek:= HibaUzenetek+' | '+HibaString;
                 end;  // if
              end; // else
          end; // else
        end;  // while
      if VoltHiba then
         Result:= HibaString
      else
         Result:= '';
      end  // if AuthToken OK
   else begin
      Result:= '�rv�nytelen MySMS autoriz�ci�s token! A k�ld�s nem siker�lt.';
      end;
end;

function TSMSKuldDlg.KuldesHibaUzenet(Hibakod: integer): string;
begin
  case Hibakod of
     98: Result:= 'T�l sok SMS k�ld�s r�vid id� alatt, k�rlek egy percnyi v�rakoz�s ut�n pr�b�ld �jra a k�ld�st!';
     99: Result:= 'A SMS k�ld� szolg�ltat�s pillanatnyilag nem el�rhet�.';
     700: Result:= 'A c�mzettek k�z�tt hib�s form�tum� telefonsz�m is szerepel!';
     701:	Result:= 'Egyszerre maximum 50 c�mzettnek k�ldhetsz �zenetet';
     702: Result:= 'A c�mzettek k�z�tt blokkolt telefonsz�m is szerepel!';
     else Result:= IntToStr(Hibakod) +'. sz�m� hiba';
     end;  // case
end;

function TSMSKuldDlg.MySMS_Post(RefPoint, RequestJSON: string): string;
var
   UTF8Url: string;
   lRequest: TStringStream;
   TotalPoints: integer;
begin
   UTF8Url:=StringEncodeAsUTF8(MySMSRoot + RefPoint);
   try
     lRequest := TStringStream.Create(RequestJSON, TEncoding.UTF8);
     Result:= UTF8Decode(HttpPost(UTF8Url, 'application/json', lRequest));  // {"errorCode":0,"authToken":"4566sdfv", ....}
   finally
     if lRequest <>nil then lRequest.Free;
     end;  // try-finally
end;

function TSMSKuldDlg.TESZT_SendSms: boolean;
var
   // str, json_output: string;
   UTF8Url, str, PostResultJSON: string;
   // PostParams: TStringList;
   lRequest: TStringStream;
   // lJSO : ISuperObject;
   JSONString, AuthToken: string;
   TotalPoints: integer;
begin
   // PostParams:=TStringList.Create;
      str:=MySMSRoot + '/json/remote/sms/send';
      JSONString := Edit2.text;
      try
        lRequest := TStringStream.Create(JSONString, TEncoding.UTF8);
        UTF8Url:=StringEncodeAsUTF8(str);
        PostResultJSON:= HttpPost(UTF8Url, 'application/json', lRequest);
      finally
        if lRequest <>nil then lRequest.Free;
        end;  // try-finally
      ebResult2.Text:= PostResultJSON;
end;


// TJSONObject-tel
function TSMSKuldDlg.GetAuthToken(JSONString: string): string;
var
  JSON: TJSONObject;
  vJSONScenario, vJSONValue: TJSONValue;
  retvalue: string;
  Hibakod: integer;
begin
  try
   vJSONScenario := TJSONObject.ParseJSONValue(JSONString);    // {"errorCode":0,"authToken":"4566sdfv", ....}
   if vJSONScenario <> nil then
      Hibakod:= StrToInt(((vJSONScenario as TJSONObject).Get('errorCode').JsonValue as TJSONString).Value);
      if Hibakod <> 0 then begin
        Hibakiiro('SMS k�ld�s - API authoriz�l�s hiba: '+IntToStr(Hibakod));
        retvalue:='';
        end
      else begin
        retvalue:= ((vJSONScenario as TJSONObject).Get('authToken').JsonValue as TJSONString).Value;
    end;
    Result:= retvalue;
   finally
    vJSONScenario.Free;
      end;  // try-finally
end;


function TSMSKuldDlg.GetMySMSErrorCode(JSONString: string): integer;
var
  JSON: TJSONObject;
  vJSONScenario, vJSONValue: TJSONValue;
  retvalue: string;
  Hibakod: integer;
begin
  try
   vJSONScenario := TJSONObject.ParseJSONValue(JSONString);    // {"errorCode":0,"authToken":"4566sdfv", ....}
   if vJSONScenario <> nil then begin
      Result:= StrToInt(((vJSONScenario as TJSONObject).Get('errorCode').JsonValue as TJSONString).Value);
      end
   else begin
      Result:= -1;  // egy saj�t hibak�d
      end;
   finally
    vJSONScenario.Free;
      end;  // try-finally
end;

// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// F�lk�sz!!!!
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
{function TSMSKuldDlg.UtolsoUzenetEllenorzes_f�lk�sz(JSONString: string): string;
var
  JSON: TJSONObject;
  vJSONScenario, vJSONValue, Acks: TJSONValue;
  retvalue, S, Szam, KuldottMessageID, TaroltStatusz: string;
  Hibakod: integer;
begin
  try
   vJSONScenario := TJSONObject.ParseJSONValue(JSONString);
   if vJSONScenario <> nil then begin
     Acks:= (vJSONScenario as TJSONObject).Get('remoteSmsSendAcks').JsonValue;
     retvalue:= '';
     for vJSONValue in Acks as TJSONArray do begin
        Szam:= ((vJSONValue as TJSONObject).Get('recipient').JsonValue as TJSONString).Value;
        KuldottMessageID:= ((vJSONValue as TJSONObject).Get('messageId').JsonValue as TJSONString).Value;
        TaroltStatusz := Query_Select('SMSUZENETEK', 'SU_ID', KuldottMessageID, 'SU_STATUS');  // 0: none, 1: pending, 2: completed, 3: failed, 4: sending, 5: unsent
        *****
        // Mit csin�ljunk, ha nem v�gleges st�tuszban van? V�rjunk �s lek�rdezz�k �jra?
        // + a lek�rdez�skor az�rt a st�tuszukat lehetne friss�teni!
        // "vegyes" �zenetek? pl. cc. gar�zsmester
        if
        *****
        if TaroltMessageID < KuldottMessageID then begin  // nem ment ki az �zenet
          S:= Szam+' [�zenet ID:'+KuldottMessageID+', t�rolt utols�:'+TaroltMessageID +']';
          retvalue:= Felsorolashoz(retvalue, S, ',', False);
          end;  // if
        end; // for
      if retvalue <> '' then
        Result:= 'El nem k�ld�tt SMS-ek: '+retvalue
      else Result:= '';
      end // if
   else begin
      Result:= 'JSON hiba! (UtolsoUzenetEllenorzes)';
      end;
   finally
    vJSONScenario.Free;
      end;  // try-finally
end;
}

// ---  V1, SZ_UTOID alapj�n ellen�riz  ----
function TSMSKuldDlg.UtolsoUzenetEllenorzes(JSONString: string): string;
var
  JSON: TJSONObject;
  vJSONScenario, vJSONValue, Acks: TJSONValue;
  retvalue, S, Szam, KuldottMessageID, TaroltMessageID: string;
  Hibakod: integer;
begin
  try
   vJSONScenario := TJSONObject.ParseJSONValue(JSONString);
   if vJSONScenario <> nil then begin
     Acks:= (vJSONScenario as TJSONObject).Get('remoteSmsSendAcks').JsonValue;
     retvalue:= '';
     for vJSONValue in Acks as TJSONArray do begin
        Szam:= ((vJSONValue as TJSONObject).Get('recipient').JsonValue as TJSONString).Value;
        KuldottMessageID:= ((vJSONValue as TJSONObject).Get('messageId').JsonValue as TJSONString).Value;
        TaroltMessageID := Query_Select('SMSSZALAK', 'SZ_TELEF', Szam, 'SZ_UTOID');
        if TaroltMessageID < KuldottMessageID then begin  // nem ment ki az �zenet
          S:= Szam+' [�zenet ID:'+KuldottMessageID+', t�rolt utols�:'+TaroltMessageID +']';
          retvalue:= Felsorolashoz(retvalue, S, ',', False);
          end;  // if
        end; // for
      if retvalue <> '' then
        Result:= 'El nem k�ld�tt SMS-ek: '+retvalue
      else Result:= '';
      end // if
   else begin
      Result:= 'JSON hiba! (UtolsoUzenetEllenorzes)';
      end;
   finally
    vJSONScenario.Free;
      end;  // try-finally
end;


// Thomas Erlang-f�le TJSON-nal
{function TSMSKuldDlg.GetAuthToken(JSONString: string): string;
var
  JSON: TJSON;
  retvalue: string;
begin
  try
   JSON := TJSON.Parse(JSONString);
  except
    on E: exception do begin
        Hibakiiro('SMS k�ld�s - GetAuthToken Parse error ('+E.Message+'), text='+JSONString);
        retvalue:='';
        end; // on E
    end;  // try-esxept
  if JSON['errorCode'].AsInteger <> 0 then begin
    Hibakiiro('SMS k�ld�s - API authoriz�l�s hiba: '+JSON['errorCode'].AsString);
    retvalue:='';
    end
  else begin
    retvalue:=JSON['authToken'].AsString;
    end;
  Result:= retvalue;
end;
}

procedure TSMSKuldDlg.Proba;
var
  vJSONScenario, vJSONValue, Uzenetek: TJSONValue;
  retvalue, HibaUzenetek, HibaString, UzenetSzoveg, JSDate, ElkuldveDatum, StatusDatum, S, JSON, Res: string;
  // UzenetSzoveg: AnsiString;
  incoming, VoltHiba: boolean;
  Hibakod, UzenetID, Bejovo, Olvasva, Zarolva, Forras, Allapot: integer;
begin
   JSON:= '{"errorCode":0,"messages":[{"messageId":150,"address":"+36302748540","message":"Sziasztok!","incoming":false,"read":true,"locked":false,"origin":0,"status":0,"dateSent":1463737728306,"dateStatus":null}]}';
   vJSONScenario := TJSONObject.ParseJSONValue(JSON);    // {"errorCode":0,"authToken":"4566sdfv", ....}
      Uzenetek:= (vJSONScenario as TJSONObject).Get('messages').JsonValue;
      for vJSONValue in Uzenetek as TJSONArray do begin
         if (vJSONValue as TJSONObject).Get('dateStatus').JsonValue is TJSONNull then
            StatusDatum:=''
         else begin
           JSDate:= ((vJSONValue as TJSONObject).Get('dateStatus').JsonValue as TJSONString).Value;
           StatusDatum:= JavaScriptTimestampToFuvaros(StrToInt64(JSDate));
           end;  // else
         end;  // for
   vJSONScenario.Free;
end;

end.
