unit KiAdBlueList;

interface

uses
  WinTypes, WinProcs, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, QuickRpt, QRExpr, Qrctrls, ExtCtrls, printers, DB, DBTables, Grids, IniFiles,
  ADODB ;

type
  TKiAdBlueListDlg = class(TForm)
    Rep: TQuickRep;
    QRBand3: TQRBand;
    QRLSzamla: TQRLabel;
    QRBand1: TQRBand;
    QRBand2: TQRBand;
    QRLabel38: TQRLabel;
    QRLabel6: TQRLabel;
    QRSysData1: TQRSysData;
    QRLabel7: TQRLabel;
    QRSysData4: TQRSysData;
    QRLabel1: TQRLabel;
    QV5: TQRLabel;
    Q3: TQRLabel;
    Q2: TQRLabel;
    Q5: TQRLabel;
    Q4: TQRLabel;
    QV4: TQRLabel;
    QRLabel21: TQRLabel;
    Q1: TQRLabel;
    ArfolyamGrid: TStringGrid;
    Osszesito: TStringGrid;
    QV3: TQRLabel;
    QRLabel13: TQRLabel;
    QRLabel17: TQRLabel;
    Query1: TADOQuery;
    QRGroup1: TQRGroup;
    QRBand5: TQRBand;
    O1: TQRLabel;
    O3: TQRLabel;
    O5: TQRLabel;
    O4: TQRLabel;
	 QRLabel4: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel15: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRLabel18: TQRLabel;
    QRShape1: TQRShape;
    QRLabel9: TQRLabel;
    Q6: TQRLabel;
    O6: TQRLabel;
    QV6: TQRLabel;
    QRLabel10: TQRLabel;
    Q7: TQRLabel;
    QRShape2: TQRShape;
    ChildBand1: TQRChildBand;
    QRLabel11: TQRLabel;
    QRLabel12: TQRLabel;
    QRShape3: TQRShape;
	 QRLabel22: TQRLabel;
    Q50: TQRLabel;
    QRLabel14: TQRLabel;
    QRLabel16: TQRLabel;
    QRLabel19: TQRLabel;
    QRLabel20: TQRLabel;
    QRLabel23: TQRLabel;
    QRLabel24: TQRLabel;
    Q51: TQRLabel;
    O51: TQRLabel;
    QV51: TQRLabel;
    procedure FormCreate(Sender: TObject);
    procedure	Tolt(dat1, dat2, rendsz	: string; reszletes : boolean );
    procedure QRBand1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBand2BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure RepBeforePrint(Sender: TCustomQuickRep;
	   var PrintReport: Boolean);
	 procedure QRGroup1BeforePrint(Sender: TQRCustomBand;
	   var PrintBand: Boolean);
	 procedure QRBand5BeforePrint(Sender: TQRCustomBand;
	   var PrintBand: Boolean);
	 procedure ChildBand1BeforePrint(Sender: TQRCustomBand;
	   var PrintBand: Boolean);
  private
		sorszam			: integer;
		reszlet			: boolean;
		atlag			: double;
		elozokm			: double;
		osszmenny		: double;
		vegkm			: double;
		vegmenny		: double;
		osszertek		: double;
		vegertek		: double;
		kezdokm			: double;
		gepkezdo		: double;
		gepveg			: double;
		gepmenny		: double;
		gepertek		: double;
  public
	  vanadat			: boolean;
  end;

var
  KiAdBlueListDlg: TKiAdBlueListDlg;

implementation

uses
	J_SQL, Egyeb, KOzos;

{$R *.DFM}

procedure TKiAdBlueListDlg.FormCreate(Sender: TObject);
begin
	EgyebDlg.SetADOQueryDatabase(Query1);
  	sorszam		:= 1;
end;

procedure	TKiAdBlueListDlg.Tolt(dat1, dat2, rendsz	: string; reszletes : boolean );
var
  	sqlstr		: string;
   dattol	  	: string;
   datig 		: string;
begin
	vanadat 	:= false;
   dattol		:= dat1;
	datig		:= dat2;
  	reszlet		:= reszletes;
   QrLabel21.Caption	:= dat1 + ' - ' + dat2;
   if dat1 = '' then begin
   	dattol	:= '1990.01.01.';
   end;
   if dat2 = '' then begin
		datig	:= EgyebDlg.MaiDatum;
	end;

	// A j�rat ellen�rz�tts�g ellen�rz�se
	sqlstr	:= 'SELECT * FROM KOLTSEG WHERE KS_TIPUS = 2 AND KS_DATUM >= '''+dattol+''' '+
		' AND KS_DATUM <= '''+datig+''' ';
	if rendsz <> '' then begin
		sqlstr	:= sqlstr + ' AND KS_RENDSZ = '''+rendsz+''' ';
		// Egyetlen aut�n�l nem kell a v�g�sszesen
		QRBand2.Enabled 	:= false;
	end;
	sqlstr	:= sqlstr + ' ORDER BY KS_RENDSZ, KS_DATUM, KS_KTKOD ';
	{A kapott sql alapj�n v�grehajtja a lek�rdez�st}
	Query_Run(Query1, sqlstr);
	vanadat := Query1.RecordCount > 0;
	if not reszlet then begin
		// Az �sszesen sorokat kisebbre venni
		QRBand5.Height := O1.Top + O1.Height + 5;
		QRShape2.Enabled 	:= false;
	end;
end;

procedure TKiAdBlueListDlg.QRBand1BeforePrint(Sender: TQRCustomBand;
	   var PrintBand: Boolean);
var
	kmora	: double;
	menny,ear	: double;
begin
	kmora		:= StringSzam(Query1.FieldByName('KS_KMORA').AsString);
	menny		:= StringSzam(Query1.FieldByName('KS_UZMENY').AsString);
	Q1.Caption 	:= IntToStr(sorszam);
	Inc(sorszam);
	atlag		:= 0;
	if StrToIntDef(Query1.FieldByName('KS_TELE').AsString, 0) > 0 then begin
		// Tele tankr�l van sz�
		Q50.Caption 	:= 'igen';
		osszertek	:= osszertek + StringSzam(Query1.FieldByName('KS_ERTEK').AsString);
		if elozokm	<> -1 then begin
			// M�r volt kezd� km, hozz�adjuk a mennyis�get, az �tlagot is sz�moljuk
			osszmenny	:= osszmenny + menny;
			if (kmora - elozokm) <> 0 then begin
				atlag	:= osszmenny * 100 / (kmora - elozokm);
			end;
			gepmenny	:= gepmenny + osszmenny;
			gepertek	:= gepertek + osszertek;
			gepveg		:= kmora;
			elozokm		:= kmora;
			osszertek	:= 0;
		end else begin
			// Ez az els� tele tank
			elozokm		:= kmora;
			gepkezdo	:= kmora;
			gepveg		:= kmora;
		end;
		osszmenny	:= 0;
	end else begin
		// Nincs tele a tank
		Q50.Caption 	:= 'nem';
		if elozokm	<> -1 then begin
			// M�r volt kezd� km, hozz�adjuk a mennyis�get, az �tlag marad 0
			osszmenny	:= osszmenny + menny;
			osszertek	:= osszertek + StringSzam(Query1.FieldByName('KS_ERTEK').AsString);
		end;
	end;
	Q2.Caption 	:= Query1.FieldByName('KS_DATUM').AsString;
	Q3.Caption 	:= Format('%.0f', [kmora]);
	Q4.Caption	:= Format('%.2f', [menny]);
	Q5.Caption 	:= Format('%.2f', [atlag]);
	Q6.Caption	:= Format('%.0f', [StringSzam(Query1.FieldByName('KS_ERTEK').AsString)]);
	Q7.Caption 	:= Query1.FieldByName('KS_THELY').AsString ;
  ear:=0;
  if menny<>0 then
	  ear:=Query1.FieldByName('KS_ERTEK').Value/menny;
	Q51.Caption 	:= Format('%.2f', [ear]);
	PrintBand	:= reszlet;
end;

procedure TKiAdBlueListDlg.QRBand2BeforePrint(Sender: TQRCustomBand;
	   var PrintBand: Boolean);
var ear:double;
begin
	// A v�g�sszesenek ki�r�sa
	atlag		:= 0;
	if vegkm <> 0 then begin
		atlag 	:= vegmenny * 100 / vegkm;
	end;
	QV3.Caption 	:= Format('%.0f', [vegkm]);
	QV4.Caption		:= Format('%.2f', [vegmenny]);
	QV5.Caption 	:= Format('%.2f', [atlag]);
	QV6.Caption 	:= Format('%.0f', [vegertek]);
  ear:=0;
  if vegmenny<>0 then
	  ear:=vegertek/vegmenny;
	QV51.Caption 	:= Format('%.2f', [ear]);
end;

procedure TKiAdBlueListDlg.RepBeforePrint(Sender: TCustomQuickRep;
	   var PrintReport: Boolean);
begin
	elozokm		:= -1;
	vegkm		:= 0;
	vegmenny	:= 0;
	vegertek	:= 0;
	sorszam		:= 1;
	osszmenny	:= 0;
	osszertek	:= 0;
	gepmenny	:= 0;
	gepertek	:= 0;
	kezdokm		:= -1;
end;

procedure TKiAdBlueListDlg.QRGroup1BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
	// A rendsz�m be�r�sa
	QrLabel5.Caption	:= Query1.FieldByName('KS_RENDSZ').AsString + ' ' +
		Query_Select('GEPKOCSI', 'GK_RESZ', Query1.FieldByName('KS_RENDSZ').AsString, 'GK_TIPUS');
	PrintBand			:= reszlet;
end;

procedure TKiAdBlueListDlg.QRBand5BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
	osszkm,ear	: double;
begin
	// A g�pkocsi �sszesenek be�r�sa
	O1.Caption	:= Query1.FieldByName('KS_RENDSZ').AsString + ' �sszes :';
	atlag		:= 0;
	osszkm		:= gepveg-gepkezdo;
	if osszkm <> 0 then begin
		atlag 	:= gepmenny * 100 / osszkm;
	end;
	O3.Caption 	:= Format('%.0f', [osszkm]);
	O4.Caption	:= Format('%.2f', [gepmenny]);
	O5.Caption 	:= Format('%.2f', [atlag]);
	O6.Caption 	:= Format('%.0f', [gepertek]);
  ear:=0;
  if gepmenny<>0 then
	  ear:=gepertek/gepmenny;
	O51.Caption 	:= Format('%.2f', [ear]);
	vegkm		:= vegkm + osszkm;
	vegmenny	:= vegmenny + gepmenny;
	vegertek	:= vegertek + gepertek;
	osszmenny	:= 0;
	osszertek	:= 0;
	elozokm		:= -1;
	sorszam		:= 1;
	kezdokm		:= -1;
	gepmenny	:= 0;
	gepertek	:= 0;
end;

procedure TKiAdBlueListDlg.ChildBand1BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
	PrintBand	:= not reszlet;
end;

end.
