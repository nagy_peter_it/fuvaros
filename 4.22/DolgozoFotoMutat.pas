unit DolgozoFotoMutat;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.Imaging.jpeg, Vcl.ExtCtrls, Data.DB,
  Data.Win.ADODB, Math, PictureUtils;

type
  TDolgozoFotoMutatDlg = class(TForm)
    Image2: TImage;
    DOLGFOTO: TADODataSet;
    DOLGFOTODO_KOD: TStringField;
    DOLGFOTODO_FOTO: TBlobField;
    procedure Image2Click(Sender: TObject);
  private
    { Private declarations }
  public
    function OpenPic(DOKOD: string): boolean;
  end;

var
  DolgozoFotoMutatDlg: TDolgozoFotoMutatDlg;

implementation

{$R *.dfm}


procedure TDolgozoFotoMutatDlg.Image2Click(Sender: TObject);
begin
  Close;  // csukja be a formot
end;

function TDolgozoFotoMutatDlg.OpenPic(DOKOD: string): boolean;
var
  ms: TMemoryStream;
  JPG: TJPEGImage;
  bmp1: TBitMap;
  eredetiwidth, eredetiheight, ujwidth, ujheight, ujkezdox, ujkezdoy: integer;
begin
  DOLGFOTO.Close;
  DOLGFOTO.Parameters[0].Value:=DOKOD;
  DOLGFOTO.Open;
  if DOLGFOTODO_FOTO.BlobSize=0 then begin
      Result:= False; // nincs mit megmutatni
      Exit;  // function
      end;
  JPG:=TJPEGImage.Create;
  ms:=TMemoryStream.Create;
  try
    DOLGFOTODO_FOTO.SaveToStream(ms);
    ms.Position := 0;
    JPG.LoadFromStream(ms);
    // Image2.Picture.Assign(JPG);
    // ha nem n�gyzetes a k�p, akkor "kieg�sz�tj�k" - nem v�gjuk, mert van ahol belev�gn�nk az arcba
    eredetiwidth:=JPG.width;
    eredetiheight:=JPG.height;
    if eredetiwidth <> eredetiheight then begin
      if eredetiwidth > eredetiheight then begin
          ujwidth:=eredetiwidth;
          ujheight:=eredetiwidth;
          ujkezdox:= 0;
          ujkezdoy:=floor((ujheight-eredetiheight)/2);
          end;
      if eredetiwidth < eredetiheight then begin
          ujwidth:=eredetiheight;
          ujheight:=eredetiheight;
          ujkezdox:=floor((ujwidth-eredetiwidth)/2);
          ujkezdoy:=0;
          end;
      bmp1:=TBitmap.Create;
      try
        bmp1.Assign(jpg);
        CropBitmap(bmp1, 0, 0, ujkezdox, ujkezdoy, ujheight, ujwidth, clBtnFace);
        // bmp1.SaveToFile('D:\Nagyp\X\x.bmp');
        jpg.Assign(bmp1);
      finally
        bmp1.Free;
        end;  // try-finally
      end;  // if nem n�gyzetes
    Image2.Picture.Assign(JPG);
    Result:= True;
  finally
     JPG.Free;
     ms.Free;
  end;

end;


end.
