unit EKontaktBe;

interface

uses
	SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
	Forms, Dialogs, StdCtrls, Buttons, Mask, DB, DBTables, J_ALFORM, ADODB;

type
	TEKontaktbeDlg = class(TJ_AlformDlg)
	BitElkuld: TBitBtn;
	BitKilep: TBitBtn;
	Label8: TLabel;
    ebEmail: TMaskEdit;
    Label3: TLabel;
    ebMegjegyzes: TMaskEdit;
    Label5: TLabel;
    ebBeosztas: TMaskEdit;
    M1: TMaskEdit;
    Label1: TLabel;
    ebNev: TMaskEdit;
    Label2: TLabel;
    Query1: TADOQuery;
    Label4: TLabel;
    ebMobiltel: TMaskEdit;
    Label6: TLabel;
    ebIrodaitel: TMaskEdit;
    chkValid: TCheckBox;
	procedure BitKilepClick(Sender: TObject);
	procedure FormCreate(Sender: TObject);
	procedure BitElkuldClick(Sender: TObject);
  procedure Tolto(cim, kod : string); override;
	private
	public
	end;

var
	EKontaktbeDlg: TEKontaktbeDlg;

implementation

uses
  Egyeb, J_SQL, J_VALASZTO, Kozos;

{$R *.DFM}

procedure TEKontaktbeDlg.BitKilepClick(Sender: TObject);
begin
	ret_kod := '';
	Close;
end;

procedure TEKontaktbeDlg.FormCreate(Sender: TObject);
begin
	EgyebDlg.SetADOQueryDatabase(Query1);
	ret_kod 	:= '';
end;

procedure TEKontaktbeDlg.Tolto(cim, kod : string);
begin
	SetMaskEdits([M1]);
	Caption 	:= cim;
	ret_kod  	:= kod;
  M1.Text		:= ret_kod;
  if ret_kod <> '' then begin
  		if Query_Run (Query1, 'SELECT * FROM EKONTAKT WHERE EK_ID = '''+ret_kod+''' ' ,true) then begin
     		ebNev.Text	:= Query1.FieldByname('EK_NEV').AsString;
     		ebMobiltel.Text	:= Query1.FieldByname('EK_MOBILTEL').AsString;
     		ebEmail.Text		:= Query1.FieldByname('EK_EMAIL').AsString;
     		ebIrodaiTel.Text		:= Query1.FieldByname('EK_IRODAITEL').AsString;
     		ebBeosztas.Text		:= Query1.FieldByname('EK_BEOSZTAS').AsString;
     		ebMegjegyzes.Text:= Query1.FieldByname('EK_MEGJEGYZES').AsString;
        chkValid.Checked:= Query1.FieldByName('EK_VALID').AsInteger > 0;
       	end; // if
    	end // if
  else begin
    chkValid.Checked:= True;  // �j kontaktn�l legyen ez alap�rtelmezett
    end;
end;

procedure TEKontaktbeDlg.BitElkuldClick(Sender: TObject);
var
  UjEKID: integer;
begin
	if ebNev.Text = '' then begin
		NoticeKi('N�v megad�sa k�telez�!');
		ebNev.Setfocus;
    Exit;
  end;

	if ret_kod = '' then begin  // �j adatsor
  	  Query_Run (Query1, 'SELECT EK_NEV FROM EKONTAKT WHERE EK_NEV = '''+ebNev.Text+''' ' ,true);
     	if Query1.RecordCount > 0 then begin
				NoticeKi('A megadott n�v m�r szerepel!');
        ebNev.Setfocus;
        Exit;
        end; // if m�r szerepel
     UjEKID:= Query_Insert_Identity (Query1, 'EKONTAKT',
        [
        'EK_NEV',  ''''+ebNev.Text+'''',
        'EK_MOBILTEL', ''''+ebMobiltel.Text+'''',
        'EK_EMAIL', ''''+ebEmail.Text+'''',
        'EK_IRODAITEL',  ''''+ebIrodaiTel.Text+'''',
        'EK_BEOSZTAS', ''''+ebBeosztas.Text+'''',
        'EK_MEGJEGYZES', ''''+ebMegjegyzes.Text+'''',
        'EK_VALID', BoolToIntStr(chkValid.Checked)
        ], 'EK_ID' );
      ret_kod:= IntToStr(UjEKID);
      M1.Text:= IntToStr(UjEKID);
      end  // if �j
  else begin  // update lesz
      Query_Update (Query1, 'EKONTAKT',
        [
        'EK_NEV',  ''''+ebNev.Text+'''',
        'EK_MOBILTEL', ''''+ebMobiltel.Text+'''',
        'EK_EMAIL', ''''+ebEmail.Text+'''',
        'EK_IRODAITEL',  ''''+ebIrodaiTel.Text+'''',
        'EK_BEOSZTAS', ''''+ebBeosztas.Text+'''',
        'EK_MEGJEGYZES', ''''+ebMegjegyzes.Text+'''',
        'EK_VALID', BoolToIntStr(chkValid.Checked)
        ], ' where EK_ID = '+ ret_kod );
      end;
  Close;
end;

end.
