unit TermekFm;

interface

uses
	SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
	StdCtrls, Forms, ExtCtrls, Dialogs, J_FOFORMUJ;

type
	TTermekFmDlg = class(TJ_FOFORMUJDLG)
	  procedure FormCreate(Sender: TObject);override;
	 procedure Adatlap(cim, kod : string);override;
	 procedure ButtonKuldClick(Sender: TObject);override;
	end;

var
	TermekFmDlg : TTermekFmDlg;

implementation

uses
	Egyeb, J_SQL, Termekbe;

{$R *.DFM}

procedure TTermekFmDlg.FormCreate(Sender: TObject);
begin
	Inherited FormCreate(Sender);
	FelTolto('Új termék/szolgáltatás felvitele ', 'Termék/szolgáltatás adatok módosítása ',
			'Termékek/szolgáltatások listája', 'T_TERKOD', 'Select * from TERMEK ','TERMEK', QUERY_KOZOS_TAG );
	NEVMEZO		:= 'T_TERNEV';
	width		:= DLG_WIDTH;
	height		:= DLG_HEIGHT;
end;

procedure TTermekFmDlg.ButtonKuldClick(Sender: TObject);
begin
	Inherited ButtonKuldClick(Sender);
   ret_vnev	:= Query1.FieldByName('T_TERNEV').AsString;
end;

procedure TTermekFmDlg.Adatlap(cim, kod : string);
begin
	Application.CreateForm(TTermekbeDlg, AlForm);
	Inherited Adatlap(cim, kod );
end;

end.

