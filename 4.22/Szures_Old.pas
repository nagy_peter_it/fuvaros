unit Szures_Old;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, Dbtables;

type
  TSzuresDlg = class(TForm)
    ButtonKilep: TBitBtn;
    ButtonTor: TBitBtn;
    ButtonKuld: TBitBtn;
    ButtonUj: TBitBtn;
    ListBox1: TListBox;
    ListBox3: TListBox;
    procedure ButtonKilepClick(Sender: TObject);
    procedure ButtonUjClick(Sender: TObject);
    procedure ButtonTorClick(Sender: TObject);
    procedure ButtonKuldClick(Sender: TObject);
    procedure Tolt(vQuery : TQuery; formtag : string);
    procedure FormCreate(Sender: TObject);
  private
    SzuresQuery	: TQuery;
  public
    SzuresStr		: string;
    kilepes		 	: boolean;
  end;

var
  SzuresDlg: TSzuresDlg;

implementation

{$R *.DFM}

uses
	Egyeb, Szurbe, Kozos;

procedure TSzuresDlg.ButtonKilepClick(Sender: TObject);
begin
	kilepes := true;
	Close;
end;

procedure TSzuresDlg.ButtonUjClick(Sender: TObject);
begin
	Application.CreateForm(TSzurbeDlg, SzurbeDlg);
  	SzurbeDlg.querymas 	:= SzuresQuery;
  	SzurbeDlg.Tolt;
  	SzurbeDlg.ShowModal;
  	if SzurbeDlg.ret_str <> '' then begin
  		// Van szur�si felt�tel
     	ListBox1.Items.Add(SzurbeDlg.ret_str);
     	ListBox3.Items.Add(SzurbeDlg.ret_str2);
  		ListBox1.ItemIndex := ListBox1.Items.Count - 1;
  		ListBox3.ItemIndex := ListBox3.Items.Count - 1;
     	ButtonTor.Show;
  	end;
  	SzurbeDlg.Destroy;
end;

procedure TSzuresDlg.ButtonTorClick(Sender: TObject);
var
	index		: integer;
begin
   if	NoticeKi( 'Val�ban t�rli a kijel�lt elemet? ', NOT_QUESTION ) = 0 then begin
  		index := ListBox3.ItemIndex;
  		ListBox1.Items.Delete(index);
  		ListBox3.Items.Delete(index);
     	if index > 0 then begin
     		Dec(index);
     	end;
  		if ListBox1.Items.Count < 1 then begin
  			ButtonTor.Hide;
     	end else begin
     		ListBox1.ItemIndex := index;
     		ListBox3.ItemIndex := index;
	  	end;
  	end;
end;

procedure TSzuresDlg.ButtonKuldClick(Sender: TObject);
var
	i : integer;
begin
	SzuresStr := '';
 	if listBox1.Items.Count > 0 then begin
 		SzuresStr := listBox1.Items[0];
    	for i := 1 to listBox1.Items.Count - 1 do begin
 			SzuresStr := SzuresStr + ' and ' +listBox1.Items[i];
    	end;
 	end;
 	kilepes := false;
 	Close;
end;

procedure TSzuresDlg.Tolt(vQuery : TQuery; formtag : string);
var
	str 		: string;
  mezostr  : string;
  mezoszam	: integer;
begin
	SzuresQuery := vQuery;		// Ennek a query-nek az elemeit kell sz�rni
	SzuresStr 	:= EgyebDlg.GetSzuroStr(formtag);
  	ListBox1.Items.Clear;;
  	while Pos('and',SzuresStr) > 0 do begin
  		str 			:= copy(SzuresStr,1,Pos('and',SzuresStr) - 1);
  		SzuresStr 	:= copy(SzuresStr,Pos('and',SzuresStr) +4, 9999);
     	ListBox1.Items.Add(str);
     	mezostr := str;
     	if Pos(' ', str) > 0 then begin
     		mezostr 		:= copy(mezostr, 1, Pos(' ', str)-1);
        	for mezoszam 	:= 0 to SzuresQuery.FieldCount -  1 do begin
        		if SzuresQuery.Fields[mezoszam].FieldName = mezostr then begin
					mezostr := SzuresQuery.Fields[mezoszam].DisplayName + copy(str, Pos(' ', str), 200);
           end;
        end;
     end;
     ListBox3.Items.Add(mezostr);
  	end;
  	if Length( SzuresStr ) > 0 then begin
  		str 			:= SzuresStr;
  		ListBox1.Items.Add(SzuresStr);
     	mezostr := SzuresStr;
     	if Pos(' ', SzuresStr) > 0 then begin
     		mezostr 		:= copy(mezostr, 1, Pos(' ', SzuresStr)-1);
        	for mezoszam 	:= 0 to SzuresQuery.FieldCount -  1 do begin
        		if SzuresQuery.Fields[mezoszam].FieldName = mezostr then begin
					mezostr := SzuresQuery.Fields[mezoszam].DisplayName + copy(SzuresStr, Pos(' ', str), 200);
           	end;
        	end;
     	end;
  		ListBox3.Items.Add(mezostr);
  	end;
  	if ListBox1.Items.Count > 0 then begin
  		ListBox1.ItemIndex := 0;
  		ListBox3.ItemIndex := 0;
  	end else begin
  		ButtonTor.Hide;
  	end;
end;

procedure TSzuresDlg.FormCreate(Sender: TObject);
begin
	kilepes := true;
end;


end.
