unit Koltsegbe;

interface

uses
	SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
	Forms, Dialogs, StdCtrls, Buttons, Mask, DB, DBTables, J_ALFORM, ADODB,
  ComCtrls, ExtCtrls;

type
	TKoltsegbeDlg = class(TJ_AlformDlg)
	BitElkuld: TBitBtn;
	BitKilep: TBitBtn;
    Label3: TLabel;
    M7: TMaskEdit;
    CValuta: TComboBox;
    Label4: TLabel;
    Label5: TLabel;
    M3: TMaskEdit;
    Label6: TLabel;
    M4: TMaskEdit;
    Label7: TLabel;
    M5: TMaskEdit;
    Label13: TLabel;
	 AFACombo: TComboBox;
    Label14: TLabel;
	 M6: TMaskEdit;
    Label15: TLabel;
    Label17: TLabel;
    FimodCombo: TComboBox;
    PageControl1: TPageControl;
	 TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    TabSheet3: TTabSheet;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label16: TLabel;
    MaskEdit1: TMaskEdit;
    BitBtn2: TBitBtn;
    MaskEdit2: TMaskEdit;
    MaskEdit3: TMaskEdit;
    CheckBox1: TCheckBox;
    MaskEdit4: TMaskEdit;
	 CheckBox2: TCheckBox;
    Label12: TLabel;
    CHonnan: TComboBox;
    Label18: TLabel;
    Label19: TLabel;
    Label20: TLabel;
	 MaskEdit5: TMaskEdit;
    BitBtn3: TBitBtn;
    MaskEdit6: TMaskEdit;
    MaskEdit7: TMaskEdit;
	 TabSheet4: TTabSheet;
    Label22: TLabel;
    Label23: TLabel;
    Label24: TLabel;
    MaskEdit8: TMaskEdit;
    BitBtn4: TBitBtn;
    MaskEdit9: TMaskEdit;
    MaskEdit10: TMaskEdit;
    CB1: TComboBox;
    CheckBox3: TCheckBox;
    CheckBox4: TCheckBox;
    Label25: TLabel;
    MaskEdit11: TMaskEdit;
    Label27: TLabel;
    MaskEdit12: TMaskEdit;
    Panel1: TPanel;
    Label2: TLabel;
    Label8: TLabel;
    Label21: TLabel;
    Label1: TLabel;
    Label26: TLabel;
    Label28: TLabel;
    M1: TMaskEdit;
    M2: TMaskEdit;
    BitBtn1: TBitBtn;
    BitBtn10: TBitBtn;
    M0: TMaskEdit;
    M1B: TMaskEdit;
    OrszagCombo: TComboBox;
    M21: TMaskEdit;
    MaskEdit13: TMaskEdit;
    BitBtn5: TBitBtn;
    QueryKoz: TADOQuery;
    QueryAl2: TADOQuery;
    QueryAl: TADOQuery;
    QueryAl3: TADOQuery;
	procedure BitKilepClick(Sender: TObject);
	procedure FormCreate(Sender: TObject);
	procedure Tolto(cim : string; teko:string); override;
	procedure BitElkuldClick(Sender: TObject);
  procedure MaskEditClick(Sender: TObject);
    procedure Modosit(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure M4Exit(Sender: TObject);
    procedure MaskKeyPress(Sender: TObject; var Key: Char);
    procedure CValutaChange(Sender: TObject);
	 procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure M2Change(Sender: TObject);
    procedure M2Exit(Sender: TObject);
	 procedure BitBtn10Click(Sender: TObject);
    procedure Tolt2(cim : string; teko, jakod, rendsz, jarat :string );
    procedure FormDestroy(Sender: TObject);
    procedure AFAComboChange(Sender: TObject);
    procedure Afaszam;
	 procedure CheckBox1Click(Sender: TObject);
    procedure CValutaExit(Sender: TObject);
    procedure AFAComboExit(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure BitBtn4Click(Sender: TObject);
    procedure CB1Change(Sender: TObject);
	 procedure KmHozzaadas(Mresz, Mkmora : TMaskEdit );
    procedure FimodComboChange(Sender: TObject);
    procedure MaskEdit2Change(Sender: TObject);
	 procedure MaskEdit3Change(Sender: TObject);
    procedure MaskEdit6Change(Sender: TObject);
    procedure MaskEdit7Change(Sender: TObject);
    procedure M21Change(Sender: TObject);
    procedure M21Exit(Sender: TObject);
    procedure OrszagComboChange(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
    procedure MaskEdit10Change(Sender: TObject);
    procedure MaskEdit9Change(Sender: TObject);
    procedure BitBtn5Click(Sender: TObject);
    procedure PageControl1Enter(Sender: TObject);
    procedure MaskEdit13Change(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure MaskEdit8Change(Sender: TObject);
    procedure MaskEdit13KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit8Enter(Sender: TObject);
    procedure MaskEdit1Enter(Sender: TObject);
    procedure MaskEdit5Enter(Sender: TObject);
    procedure MaskEdit13Enter(Sender: TObject);
    procedure PageControl1Changing(Sender: TObject;
      var AllowChange: Boolean);
    function JaratDatumEllenorzes(datum,ido,jaratszam: string): boolean;
    procedure MaskEdit9Exit(Sender: TObject);
    procedure M3Enter(Sender: TObject);
    procedure CheckBox2Click(Sender: TObject);
	private
	  modosult 		: boolean;
    telemod     : boolean;
	  listavnem		: TStringList;
	  jaratkod		: string;
	  rendszam		: string;
	  afalist 		: TStringList;
	  listafimod	: TStringList;
	  orszaglist	: TStringList;
    HUTOTIP, K_TIPUS: string;
    KTIPUS:integer;
    HUTOS, ADBLUES: boolean;
    MAXPOTTANKL, MAXTANKL : integer;
	public
    JARATBOL: Boolean;
	end;

var
	KoltsegbeDlg: TKoltsegbeDlg;

implementation

uses
	  Egyeb, J_SQL, J_VALASZTO, Kozos, Windows,Jaratbe, Math;

{$R *.DFM}

procedure TKoltsegbeDlg.BitKilepClick(Sender: TObject);
begin
	if modosult and BitElkuld.Enabled then begin
  	if NoticeKi('M�dosultak az adatok . Ki akar l�pni ment�s n�lk�l?', NOT_QUESTION) = 0 then begin
		ret_kod := '';
	   	Close;
	end;
  	end else begin
		ret_kod := '';
		Close;
  end;
end;

procedure TKoltsegbeDlg.FormCreate(Sender: TObject);
var
	str	: string;
  	sorsz : integer;
begin
	HUTOTIP:= EgyebDlg.Read_SZGrid( '999', '922' );
	MAXPOTTANKL:=StrToIntDef(EgyebDlg.Read_SZGrid( '999', '749' ),0);;

	EgyebDlg.SetADOQueryDatabase(QueryAl);
	EgyebDlg.SetADOQueryDatabase(QueryAl2);
	EgyebDlg.SetADOQueryDatabase(QueryAl3);
	EgyebDlg.SetADOQueryDatabase(QueryKoz);
	SetMaskEdits([M1, M1B, MaskEdit4, MaskEdit11, MaskEdit12]);
	ret_kod 						:= '';
  	jaratkod						:= '';
  	modosult 						:= false;
    telemod:=False;
  	listavnem						:= TStringList.Create;
	PageControl1.ActivePageIndex	:= 0;
	SzotarTolt(CValuta, '110' , listavnem, false );
  	CValuta.ItemIndex				:= 0;
	afalist 						:= TStringList.Create;
	SzotarTolt(AFACombo, '101' , afalist, false );
  	Afacombo.ItemIndex	:= 0;
	listafimod						:= TStringList.Create;
	SzotarTolt(FimodCombo, '310' , listafimod, false );
	FimodCombo.ItemIndex		:= listafimod.IndexOf('Telep');
	rendszam					:= '';
	{A le�r�sok felt�lt�se}
	if Query_Run (QueryKoz, 'SELECT * FROM SZOTAR WHERE SZ_FOKOD = ''113'' AND SZ_ERVNY = 1 ORDER BY SZ_MENEV ',true) then begin
		CHonnan.Clear;
		QueryKoz.First;
		while not QueryKoz.EOF do begin
			CHonnan.Items.Add(QueryKoz.FieldByname('SZ_MENEV').AsString);
			QueryKoz.Next;
		end;
	end;
	QueryKoz.Close;
	{Alap�rtelmezett valutanem be�ll�t�sa}
	str	:= EgyebDlg.Read_SZGrid('999','661');
	sorsz := listavnem.IndexOf(str);
	if sorsz < 0 then begin
		sorsz := 0;
	end;
	CValuta.ItemIndex  := sorsz;
	{Alap�rtelmezett �FA be�ll�t�sa}
	str	:= EgyebDlg.Read_SZGrid('999', '660');
	sorsz := afalist.IndexOf(str);
	if sorsz < 0 then begin
		sorsz := 0;
	end;
	AFACombo.ItemIndex  := sorsz;
	// Az orsz�gok bet�lt�se �s HU be�ll�t�sa
	orszaglist := TStringList.Create;
	SzotarTolt(OrszagCombo,  '300' , orszaglist, false );
	for sorsz	:= 0 to OrszagCombo.Items.Count - 1 do begin
		OrszagCombo.Items[sorsz] := orszaglist[sorsz];
	end;
	OrszagCombo.ItemIndex  	:= orszaglist.IndexOf('HU');
end;

procedure TKoltsegbeDlg.Tolto(cim : string; teko:string);
var
	tipus	: integer;
	sorsz	: integer;
begin
	ret_kod 		:= teko;
	Caption 		:= cim;
	M1.Text 		:= '';
	M2.Text 		:= '';
	M21.Text 		:= '';
	M3.Text 		:= '';
	M4.Text 		:= '';
	M5.Text 		:= '';
	M7.Text 		:= '';
	MaskEdit1.Text 	:= '';
	MaskEdit2.Text 	:= '';
	MaskEdit3.Text 	:= '';
	CHonnan.Text 	:=	'';
  K_TIPUS:='';
	if ret_kod <> '' then begin		{M�dos�t�s}
		if Query_Run (QueryAl, 'SELECT * FROM KOLTSEG WHERE KS_KTKOD = '+teko,true) then begin
      //if  (QueryAl.FieldByName('KS_BEODAT').AsString<>'') then      // aut. beolvasott k�lts�g
      //    BitElkuld.Enabled:=False;

			jaratkod	  	:= QueryAl.FieldByName('KS_JAKOD').AsString;
			M1.Text 	  	:= QueryAl.FieldByName('KS_JARAT').AsString;
			M1B.Text 	  	:= QueryAl.FieldByName('KS_SONEV').AsString;
			M2.Text 	  	:= QueryAl.FieldByName('KS_DATUM').AsString;
			M21.Text 	  	:= QueryAl.FieldByName('KS_IDO').AsString;
   		K_TIPUS 			:= QueryAl.FieldByName('KS_KTIP').AsString;
			tipus			:= StrToIntdef(QueryAl.FieldByName('KS_TIPUS').AsString,0);
      KTIPUS:=tipus;
			case tipus of
				0: // �zemanyag k�lts�gr�l van sz�!
				begin
					MaskEdit1.Text		:= QueryAl.FieldByName('KS_RENDSZ').AsString;
					MaskEdit2.Text		:= QueryAl.FieldByName('KS_KMORA').AsString;
					MaskEdit3.Text		:= QueryAl.FieldByName('KS_UZMENY').AsString;
					CheckBox1.Checked 	:= QueryAl.FieldByName('KS_TELE').AsInteger > 0;
					CheckBox2.Checked 	:= QueryAl.FieldByName('KS_TETAN').AsInteger > 0;
					MaskEdit4.Text 		:= QueryAl.FieldByName('KS_ATLAG').AsString;
				end;
				1: // Egy�b k�lts�g
				begin
					//CHonnan.Text	:= QueryAl.FieldByName('KS_LEIRAS').AsString;
					MaskEdit1.Text		:= QueryAl.FieldByName('KS_RENDSZ').AsString;
					CHonnan.ItemIndex:= CHonnan.Items.IndexOf(QueryAl.FieldByName('KS_LEIRAS').AsString);
				end;
				2: // AD Blue
				begin
          ADBLUES:= Query_Select('GEPKOCSI','GK_RESZ',QueryAl.FieldByName('KS_RENDSZ').AsString,'GK_EUROB')>'3';
					MaskEdit5.Text		:= QueryAl.FieldByName('KS_RENDSZ').AsString;
					MaskEdit6.Text		:= QueryAl.FieldByName('KS_KMORA').AsString;
					MaskEdit7.Text		:= QueryAl.FieldByName('KS_UZMENY').AsString;
					CheckBox4.Checked 	:= QueryAl.FieldByName('KS_TELE').AsInteger > 0;
					MaskEdit11.Text		:= QueryAl.FieldByName('KS_ATLAG').AsString;
				end;
				3: // H�t� tankol�s
				begin
					MaskEdit8.Text		:= QueryAl.FieldByName('KS_RENDSZ').AsString;
					MaskEdit9.Text		:= QueryAl.FieldByName('KS_KMORA').AsString;
					MaskEdit10.Text		:= QueryAl.FieldByName('KS_UZMENY').AsString;
					MaskEdit12.Text		:= QueryAl.FieldByName('KS_ATLAG').AsString;
					CheckBox3.Checked 	:= QueryAl.FieldByName('KS_TELE').AsInteger > 0;
					if jaratkod <> '' then begin
						// A h�t� tankol�shoz berakjuk a j�raton l�v� p�tkocsikat egy list�ba , azok k�z�l v�laszthatunk
						CB1.Visible			:= true;
						MaskEdit8.Visible	:= false;
						BitBtn4.Visible		:= false;
						CB1.Clear;
						Query_Run (QueryAl2, 'SELECT DISTINCT JP_POREN FROM JARPOTOS WHERE JP_JAKOD = '+jaratkod,true);
						if QueryAl2.RecordCount < 1 then begin
							// A h�t�s a f� g�pkocsihoz lesz hozz�rakva
							CB1.Items.Add(QueryAl.FieldByName('KS_RENDSZ').AsString);
						end else begin
							while not QueryAl2.Eof do begin
								CB1.Items.Add(QueryAl2.FieldByName('JP_POREN').AsString);
								QueryAl2.Next;
							end;
						end;
						sorsz			:= CB1.Items.IndexOf(QueryAl.FieldByName('KS_RENDSZ').AsString);
						if sorsz < 0 then begin
							sorsz	:= 0;
						end;
						CB1.ItemIndex		:= sorsz;
						MaskEdit8.Text		:= CB1.Items[sorsz];
					end;
				end;
			end;
     // for i:=0 to  PageControl1.PageCount-1 do
     //   PageControl1.Pages[i].Visible:=(i=tipus);
			PageControl1.ActivePageIndex	:= tipus;
			M3.Text 		:= SzamString(QueryAl.FieldByName('KS_ARFOLY').AsFloat,12,4);
			M4.Text 		:= SzamString(QueryAl.FieldByName('KS_OSSZEG').AsFloat,12,2);
			M5.Text 		:= SzamString(QueryAl.FieldByName('KS_ERTEK').AsFloat,12,2);
			M7.Text 		:= QueryAl.FieldByName('KS_MEGJ').AsString;
			M6.Text 		:= SzamString(QueryAl.FieldByName('KS_AFAERT').AsFloat,12,2);
			ComboSet (CValuta, QueryAl.FieldByName('KS_VALNEM').AsString, listavnem);
			ComboSet (AfaCombo,QueryAl.FieldByName('KS_AFAKOD').AsString, afalist);
			ComboSet (FimodCombo,QueryAl.FieldByName('KS_FIMOD').AsString, listafimod);
			if QueryAl.FieldByName('KS_ORSZA').AsString <> '' then begin
				ComboSet (OrszagCombo,QueryAl.FieldByName('KS_ORSZA').AsString, orszaglist);
			end else begin
				OrszagCombo.ItemIndex  	:= orszaglist.IndexOf('HU');
			end;
      MaskEdit13.Text:=QueryAl.FieldByName('KS_RENDSZ').AsString;
		end;
  end;
	modosult 	:= false;
  telemod:=False;
	// A telepi tankol�sok kezel�s�nek ellen�rz�se
	if ( ( GetRightTag(566) = RG_NORIGHT	) and not EgyebDlg.user_super ) then begin
		CheckBox2.Enabled	:= false;
	end;
  /////////////////////
  if (MaskEdit13.Text='') and (M1.Text<>'') then
  begin
	  MaskEdit13.Text	:= Query_Select('JARAT', 'JA_KOD', jaratkod, 'JA_RENDSZ');
    modosult:=True;
  end;
end;

procedure TKoltsegbeDlg.BitElkuldClick(Sender: TObject);
var
  	ujkodszam 		: integer;
	atlag			: double;
	telestr	, S: string;
	s1, s2, s3, s4	: string;
//   tankhely		: string;
	telepitank		: string;
	tipus			: integer;
	ftliter			: double;
	alsoft			: double;
	felsoft			: double;
	telestr2		: string;
//  utkm: double;
  cseredatum: string;
  VanJoga: boolean;
begin
  if not modosult and not telemod then
  begin
    BitKilep.OnClick(self);
    exit;
  end;
  VanJoga:= EgyebDlg.user_super or (GetRightTag(593) <> RG_NORIGHT);
  if (not VanJoga) and (K_TIPUS<>'') and (modosult) then
  begin
		NoticeKi('Automatikusan beolvasott k�lts�g nem m�dos�that�!');
    BitKilep.SetFocus;
		Exit;
	end;

	if ( jaratkod='') then begin
		NoticeKi('Nincs j�ratsz�m!');
    M1.SetFocus;
		Exit;
	end;

	if ( ( M2.Text = '' ) or ( not Jodatum2( M2 ) ) )then begin
		NoticeKi('Hib�s d�tum megad�sa!');
		M2.Setfocus;
		Exit;
	end;

  	{J�rat ellen�rz�tts�g�nek ellen�rz�se}
	if EllenorzottJarat(jaratkod) > 0 then begin
  		Exit;
  	end;

  if not JaratDatumEllenorzes(M2.Text,M21.Text,jaratkod)    then
  begin
      NoticeKi('A k�lts�g id�pontja nem esik a j�ratid� intervallum�ba! '+M2.Text+'  '+M21.Text);
      M2.SetFocus;
      exit;
  end;

   // Az 1000000 -�s hat�r ellen�rz�se
	if StringSzam(M5.Text) > 1000000 then begin
		if NoticeKi('T�l magas k�lts�g!!! ( > 1000000) / Folytatja?', NOT_QUESTION) <> 0 then begin
			M5.SetFocus;
       	Exit;
		end;
   end;

{   // Az �rfolyam ellen�rz�se
	if StringSzam(M3.Text) < 1 then begin
   	if NoticeKi('T�l alacsony �rfolyam! / Folytatja?', NOT_QUESTION) <> 0 then begin
			M3.SetFocus;
       	Exit;
		end;
   end;    }
   // Az �rfolyam ellen�rz�se
	if StringSzam(M3.Text) < 1 then begin
   	//if NoticeKi('T�l alacsony �rfolyam! / Folytatja?', NOT_QUESTION) <> 0 then begin
      NoticeKi('Az �rfolyamot meg kell adni!');
			M3.SetFocus;
      Exit;
		//end;
   end;

   // A 0 -�s k�lts�g ellen�rz�se
	if StringSzam(M5.Text) =0 then begin
  		NoticeKi('Az �rt�k nem lehet nulla!!!');
			M4.SetFocus;
      Exit;
   end;
   cseredatum:='';

	tipus			:= PageControl1.ActivePageIndex;
	// Nem lehet azonos km-re felvinni k�t fogyaszt�st
	if tipus <> 1 then begin
		case tipus of
			0: // �zemanyag k�lts�gr�l van sz�!
			begin
				s1			:=	MaskEdit1.Text;
				s2			:= SqlSzamString(StringSzam(MaskEdit2.Text),12,2);
			end;
			2: // AD Blue
			begin
				s1			:= MaskEdit5.Text;
				s2			:= SqlSzamString(StringSzam(MaskEdit6.Text),12,2);
			end;
			3: // H�t� tankol�s
			begin
				s1			:= MaskEdit8.Text;
				s2			:= SqlSzamString(StringSzam(MaskEdit9.Text),12,2);
			end;
		end;
		// Megkeress�k az azonos tankol�sokat
		if ret_kod = '' then begin
			Query_Run (QueryAl, 'SELECT KS_KTKOD FROM KOLTSEG WHERE KS_RENDSZ = '''+s1+''' AND KS_TIPUS = '+IntToStr(tipus)+' AND KS_KMORA = '+s2);
		end else begin
			Query_Run (QueryAl, 'SELECT KS_KTKOD FROM KOLTSEG WHERE KS_RENDSZ = '''+s1+''' AND KS_TIPUS = '+IntToStr(tipus)+' AND KS_KMORA = '+s2+' AND KS_KTKOD <> '+ret_kod);
		end;
		if QueryAl.RecordCount > 0 then begin
			if NoticeKi('A megadott rendsz�mmal �s kilom�ter�ra(�zem�ra) �ll�ssal m�r l�tezik k�lts�g rekord! Folytatja?',NOT_QUESTION)<>0 then
    	//if NoticeKi('M�dosultak az adatok . Ki akar l�pni ment�s n�lk�l?', NOT_QUESTION) = 0 then begin
			Exit;
		end;
{		// Az el�tte ill. m�g�tte l�v� elemek lek�rdez�se
		Query_Run (QueryAl, 'SELECT * FROM KOLTSEG WHERE KS_RENDSZ = '''+s1+''' AND KS_TIPUS = '+IntToStr(tipus)+' AND ( (  KS_KMORA < '+s2+' AND KS_DATUM > '''+M2.Text+''' ) OR '+
		   ' ( KS_KMORA > '+s2+' AND KS_DATUM < '''+M2.Text+''' ) ) ');
		if QueryAl.RecordCount > 0 then begin
			NoticeKi('A km �ra �ll�s �s a d�tum �tk�zik egy m�sik rekorddal (nagyobb km kisebb d�tummal vagy kisebb km nagyobb d�tummal)! J�ratk�d: '+QueryAl.FieldByName('KS_JAKOD').AsString);
			Exit;
		end;
    }
	end;

	case tipus of
		0: // �zemanyag k�lts�gr�l van sz�!
		begin
			{A km �ra �ll�s ellen�rz�se}
			if M1.Text <> ''  then begin
				KmHozzaadas(MaskEdit1, MaskEdit2);
				if Query_Run (QueryAl, 'SELECT * FROM JARAT WHERE JA_KOD = '''+jaratkod+''' ',true) then begin
					if QueryAl.RecordCount > 0 then begin
						if ( 	( StringSzam( MaskEdit2.Text) < StringSzam(QueryAl.FieldByName('JA_NYITKM').AsString) ) or
							( StringSzam( MaskEdit2.Text) > StringSzam(QueryAL.FieldByName('JA_ZAROKM').AsString) ) )
						then begin
							NoticeKi('A megadott km nem esik bele a j�rat intervallumba!'+Chr(13)+
                               '( '+QueryAL.FieldByName('JA_NYITKM').AsString+' - '+QueryAl.FieldByName('JA_ZAROKM').AsString+' )');
              M2.Setfocus;
              Exit;
            end;
          end;
				end;
    		// Az el�tte ill. m�g�tte l�v� elemek lek�rdez�se
     Query_Run(QueryKoz,'SELECT * FROM KMCHANGE WHERE KC_RENSZ='''+s1+''' AND KC_DATUM<='''+M2.Text+''' order by KC_datum desc');    // volt e �racsere
     cseredatum:= QueryKoz.FieldByname('KC_DATUM').AsString ;
		    Query_Run (QueryAl, 'SELECT * FROM KOLTSEG WHERE KS_RENDSZ = '''+s1+''' AND KS_TIPUS = '+IntToStr(tipus)+' AND ( (  KS_KMORA < '+s2+' AND KS_DATUM > '''+M2.Text+''' ) OR '+
		      ' ( KS_KMORA > '+s2+' AND KS_DATUM < '''+M2.Text+''' ) )AND KS_DATUM>'''+cseredatum+'''');
    		if QueryAl.RecordCount > 0 then begin
		    	// NoticeKi('A km �ra �ll�s �s a d�tum �tk�zik egy m�sik rekorddal (nagyobb km kisebb d�tummal vagy kisebb km nagyobb d�tummal)! J�ratk�d: '+QueryAl.FieldByName('KS_JAKOD').AsString);
          S:='Egy m�sik k�lts�gnek ellentmond� adatok! (nagyobb km kisebb d�tummal vagy kisebb km nagyobb d�tummal)! '+
            'J�rat: '+QueryAl.FieldByName('KS_JARAT').AsString+', d�tum: '+QueryAl.FieldByName('KS_DATUM').AsString +
            ', km: '+QueryAl.FieldByName('KS_KMORA').AsString+
            ' (' + QueryAl.FieldByName('KS_LEIRAS').AsString+' '+QueryAl.FieldByName('KS_MEGJ').AsString;
          NoticeKi(S);
			    Exit;
		    end;
      end;
      // �zemanyag mennyis�g ellen�rz�se
      MAXTANKL:= StrToIntDef( Query_Select('GEPKOCSI','GK_RESZ',MaskEdit13.Text,'GK_TANKL'),0) ;
      if MAXTANKL>0 then
      begin
            if StringSzam( MaskEdit3.Text)> MAXTANKL then
            begin
              NoticeKi('Maximum '+IntToStr(MAXTANKL)+' liter tankolhat�!');
              exit;
            end;
      end;

      // Az �zemanyag �r�nak ellen�rz�se
      if StringSzam(MaskEdit3.Text) <> 0 then begin
				       ftliter	:= StringSzam(M5.Text) / StringSzam(MaskEdit3.Text);
               alsoft	:= StringSzam(EgyebDlg.Read_SZGrid('999','714'));
				       felsoft	:= StringSzam(EgyebDlg.Read_SZGrid('999','715'));
               if alsoft > 0 then begin
               	if alsoft > ftliter then begin
                   	NoticeKi('Az �zemanyag egys�g�ra kisebb mint a megadott intervallum als� hat�ra : /'+
                    Format('%.0f Ft/l < %.0f Ft/l',[ftliter, alsoft]));
                    M4.SetFocus;
						        Exit;
					      end;
               end;
               if felsoft > 0 then begin
               	if ftliter > felsoft then begin
						      NoticeKi('Az �zemanyag egys�g�ra nagyobb mint a megadott intervallum fels� hat�ra : /'+
                  Format('%.0f Ft/l > %.0f Ft/l',[ftliter, felsoft]));
						      M4.SetFocus;
                  Exit;
					      end;
               end;
			end;
		end;
		1: // Egy�b k�lts�g
		begin
		end;
		2: // AD Blue
		begin
      if not ADBLUES then
      begin
        NoticeKi('FIGYELEM! A g�pkocsi nem EUR4-es vagy feletti kateg�ri�j�!');
        exit;
      end;
			if M1.Text <> ''  then begin
				KmHozzaadas(MaskEdit5, MaskEdit6);
				if Query_Run (QueryAl, 'SELECT * FROM JARAT WHERE JA_KOD = '''+jaratkod+''' ',true) then begin
					if QueryAl.RecordCount > 0 then begin
						if ( 	( StringSzam( MaskEdit6.Text) < StringSzam(QueryAl.FieldByName('JA_NYITKM').AsString) ) or
							( StringSzam( MaskEdit6.Text) > StringSzam(QueryAL.FieldByName('JA_ZAROKM').AsString) ) )
						then begin
							NoticeKi('A megadott km nem esik bele a j�rat intervallumba!'+Chr(13)+
								'( '+QueryAL.FieldByName('JA_NYITKM').AsString+' - '+QueryAl.FieldByName('JA_ZAROKM').AsString+' )');
							MaskEdit6.Setfocus;
							Exit;
						end;
					end;
				end;
			end;
           //  AD blue �r�nak ellen�rz�se
           if StringSzam(MaskEdit7.Text) <> 0 then begin
				       ftliter	:= StringSzam(M5.Text) / StringSzam(MaskEdit7.Text);
               alsoft	:= StringSzam(EgyebDlg.Read_SZGrid('999','716'));
				       felsoft	:= StringSzam(EgyebDlg.Read_SZGrid('999','717'));
               if alsoft > 0 then begin
               	if alsoft > ftliter then begin
                   	NoticeKi('Az AD Blue egys�g�ra kisebb mint a megadott intervallum als� hat�ra : /'+
                    Format('%.0f Ft/l < %.0f Ft/l',[ftliter, alsoft]));
                    M4.SetFocus;
						        Exit;
					      end;
               end;
               if felsoft > 0 then begin
               	if ftliter > felsoft then begin
						      NoticeKi('Az AD Blue egys�g�ra nagyobb mint a megadott intervallum fels� hat�ra : /'+
                  Format('%.0f Ft/l > %.0f Ft/l',[ftliter, felsoft]));
						      M4.SetFocus;
                  Exit;
					      end;
               end;
			     end;
		end;
		3: // H�t� tankol�s
		begin
			if M1.Text <> ''  then begin
(*
				KmHozzaadas(MaskEdit8, MaskEdit9);
				if Query_Run (QueryAl, 'SELECT * FROM JARAT WHERE JA_KOD = '''+jaratkod+''' ',true) then begin
					if QueryAl.RecordCount > 0 then begin
						if ( 	( StringSzam( MaskEdit9.Text) < StringSzam(QueryAl.FieldByName('JA_NYITKM').AsString) ) or
							( StringSzam( MaskEdit9.Text) > StringSzam(QueryAL.FieldByName('JA_ZAROKM').AsString) ) )
						then begin
							NoticeKi('A megadott km nem esik bele a j�rat intervallumba!'+Chr(13)+
								'( '+QueryAL.FieldByName('JA_NYITKM').AsString+' - '+QueryAl.FieldByName('JA_ZAROKM').AsString+' )');
							MaskEdit9.Setfocus;
							Exit;
						end;
					end;
				end;
*)
           // �zemanyag mennyis�g ellen�rz�se
           if MAXPOTTANKL>0 then
           begin
            if StringSzam( MaskEdit10.Text)> MAXPOTTANKL then
            begin
              NoticeKi('Maximum '+IntToStr(MAXPOTTANKL)+' liter tankolhat�!');
              exit;
            end;
           end;
           // �zem�ra ellen�rz�se
           if not EgyebDlg.UzemoraEllenorzes(jaratkod,MaskEdit8.Text,SqlSzamString(StringSzam( MaskEdit9.Text),12,2)) then
           begin
                   	NoticeKi('Az �zem�ra nem esik a j�rat intervallum�ba!');
                    MaskEdit9.SetFocus;
						        Exit;
           end;
   {
           utkm:=StrToFloatDef( Query_Select('GEPKOCSI','GK_RESZ',CB1.Text,'GK_UT_KM'),0);
           if StrToFloatDef(MaskEdit9.Text,0)+200>utkm then
           begin
                   	NoticeKi('Az �zem�ra sokkal nagyobb mint az utolj�ra be�rt! ('+FloatToStr( utkm)+')');
                    MaskEdit9.SetFocus;

           end;
    }
           // Az �zemanyag �r�nak ellen�rz�se
           if StringSzam(MaskEdit10.Text) <> 0 then begin
				       ftliter	:= StringSzam(M5.Text) / StringSzam(MaskEdit10.Text);
               alsoft	:= StringSzam(EgyebDlg.Read_SZGrid('999','714'));
				       felsoft	:= StringSzam(EgyebDlg.Read_SZGrid('999','715'));
               if alsoft > 0 then begin
               	if alsoft > ftliter then begin
                   	NoticeKi('Az �zemanyag egys�g�ra kisebb mint a megadott intervallum als� hat�ra : /'+
                    Format('%.0f Ft/l < %.0f Ft/l',[ftliter, alsoft]));
                    M4.SetFocus;
						        Exit;
					      end;
               end;
               if felsoft > 0 then begin
               	if ftliter > felsoft then begin
						      NoticeKi('Az �zemanyag egys�g�ra nagyobb mint a megadott intervallum fels� hat�ra : /'+
                  Format('%.0f Ft/l > %.0f Ft/l',[ftliter, felsoft]));
						      M4.SetFocus;
                  Exit;
					      end;
               end;
			     end;

			end;
		end;
	end;

	{A d�tum ellen�rz�se}
	if Query_Run (QueryAl, 'SELECT * FROM JARAT WHERE JA_KOD = '''+jaratkod+''' ',true) then begin
		if QueryAl.RecordCount > 0 then begin
			if (  ( M2.Text < QueryAl.FieldByName('JA_JKEZD').AsString  ) or
					( ( M2.Text > QueryAl.FieldByName('JA_JVEGE').AsString ) and
					( QueryAl.FieldByName('JA_JVEGE').AsString <> '' ) ) ) then begin
			   NoticeKi('A megadott d�tum nem esik a j�rat d�tumai k�z�!'+Chr(13)+
				'( '+QueryAl.FieldByName('JA_JKEZD').AsString+' - '+QueryAl.FieldByName('JA_JVEGE').AsString+' )');
			   M2.Setfocus;
			   Exit;
			end;
		 end;
	end;
{
	if ret_kod = '' then begin
		//�j rekord felvitele
		ujkodszam	:= GetNextCode('KOLTSEG', 'KS_KTKOD', 1, 0);
		ret_kod 	:= IntToStr(ujkodszam);
		Query_Run ( QueryAl, 'INSERT INTO KOLTSEG (KS_KTKOD) VALUES ('+ret_kod+ ' )',true);
	end;
 }
  {Tele tank eset�n az �tlag kisz�m�t�sa}
	telestr		:= '0';
	case tipus of
		0: // �zemanyag k�lts�gr�l van sz�!
		begin
			s1			:=	MaskEdit1.Text;
			s2			:= SqlSzamString(StringSzam(MaskEdit2.Text),12,2);
			s3			:= SqlSzamString(StringSzam(MaskEdit3.Text),12,2);
			//s4			:= '';
			s4			:= '�zemanyag k�lts�g';
			telestr		:= BoolToStr01(CheckBox1.Checked);
		end;
		1: // Egy�b k�lts�g
		begin
			s1			:= MaskEdit13.Text;    // '';
			s2			:= '0';
			s3			:= '0';
			s4			:= CHonnan.Text;
		end;
		2: // AD Blue
		begin
			s1			:= MaskEdit5.Text;
			s2			:= SqlSzamString(StringSzam(MaskEdit6.Text),12,2);
			s3			:= SqlSzamString(StringSzam(MaskEdit7.Text),12,2);
		 //	s4			:= '';
			s4			:= 'AD Blue';
			telestr		:= BoolToStr01(CheckBox4.Checked);
		end;
		3: // H�t� tankol�s
		begin
			s1			:= MaskEdit8.Text;
			s2			:= SqlSzamString(StringSzam(MaskEdit9.Text),12,2);
			s3			:= SqlSzamString(StringSzam(MaskEdit10.Text),12,2);
		 //	s4			:= '';
			s4			:= 'H�t� tankol�s';
			telestr		:= BoolToStr01(CheckBox3.Checked);
		end;
	end;
	telestr2	:= ' N ';
	if telestr = '1' then begin
		telestr2	:= ' I ';
	end;
	telepitank	:= '0';
	if CheckBox2.Checked then begin
		telepitank	:= '1';
	end;

  if s1='' then  // nincs rendsz�m
  begin
		NoticeKi('NINCS rendsz�m!');
    exit;
  end;

	if ret_kod = '' then begin
		{�j rekord felvitele}
		ujkodszam	:= GetNextCode('KOLTSEG', 'KS_KTKOD', 1, 0);
		ret_kod 	:= IntToStr(ujkodszam);
		Query_Run ( QueryAl, 'INSERT INTO KOLTSEG (KS_KTKOD,KS_RENDSZ) VALUES ('+ret_kod+ ','''+s1+''' )',true);
	end;
	ujkodszam	:= 0;
	if not Query_Update (QueryAl, 'KOLTSEG',
		[
		'KS_JAKOD', 	''''+jaratkod+'''',
		'KS_DATUM', 	''''+M2.Text +'''',
		'KS_IDO', 	''''+M21.Text +'''',
		'KS_RENDSZ', 	''''+s1 +'''',
		'KS_KMORA', 	s2,
		'KS_UZMENY', 	s3,
		'KS_LEIRAS', 	''''+s4 +'''',
		'KS_VALNEM', 	''''+listavnem[CValuta.ItemIndex]+'''',
		'KS_ARFOLY',	SqlSzamString(StringSzam(M3.Text),12,4),
		'KS_OSSZEG',	SqlSzamString(StringSzam(M4.Text),12,2),
		'KS_ERTEK',		SqlSzamString(StringSzam(M5.Text),12,2),
		'KS_AFASZ',		SqlSzamString(StringSzam(AfaCombo.Text),6,2),
		'KS_AFAERT',	SqlSzamString(StringSzam(M6.Text),12,2),
		'KS_AFAKOD',	''''+afalist[AfaCombo.ItemIndex]+'''',
		'KS_FIMOD',		''''+listafimod[FimodCombo.ItemIndex]+'''',
		'KS_ORSZA', 	''''+orszaglist[OrszagCombo.ItemIndex]+'''',
		'KS_ATLAG',		'0',
		'KS_THELY',     ''''+''+'''',
		'KS_TELE',		telestr,
		'KS_TELES',		''''+telestr2+'''',
		'KS_TETAN',		telepitank,
		'KS_TIPUS', 	IntToStr(tipus),
		'KS_MEGJ',		''''+M7.Text+''''
	], ' WHERE KS_KTKOD = '+ret_kod ) then   exit;

	KoltsegJaratTolto(ret_kod);
	if tipus = 0 then begin
		// Az �tlag lek�r�se �s be�r�sa
		atlag	:= GetKoltsegAtlag(ret_kod);
		Query_Update (QueryAl, 'KOLTSEG',
			[
			'KS_ATLAG',		SqlSzamString(atlag,10,2)
			], ' WHERE KS_KTKOD = '+ret_kod );
	end;
	if tipus = 2 then begin
		// Az �tlag lek�r�se �s be�r�sa
		atlag	:= GetAdBlueKoltsegAtlag(ret_kod);
		Query_Update (QueryAl, 'KOLTSEG',
			[
			'KS_ATLAG',		SqlSzamString(atlag,10,2)
			], ' WHERE KS_KTKOD = '+ret_kod );
	end;
	if tipus = 3 then begin
		// Az �tlag lek�r�se �s be�r�sa
		atlag	:= GetHutoKoltsegAtlag(ret_kod);
		Query_Update (QueryAl, 'KOLTSEG',
			[
			'KS_ATLAG',		SqlSzamString(atlag,10,2)
			], ' WHERE KS_KTKOD = '+ret_kod );
	end;
	{Le�r�s lekezel�se}
	if tipus = 1 then begin
		if Query_Run (QueryKoz, 'SELECT * FROM SZOTAR WHERE SZ_FOKOD = ''113'' AND SZ_MENEV = ''' +CHonnan.Text+ ''' ',true) then begin
			if QueryKoz.RecordCount < 1 then begin
				{Felvinni az �j elemet}
				if Query_Run (QueryKoz, 'SELECT MAX(SZ_ALKOD) MAXERT FROM SZOTAR WHERE SZ_FOKOD = ''113'' ',true) then begin
					ujkodszam := StrToIntDef(QueryKoz.FieldByName('MAXERT').AsString,0) + 1;
				end;
				if ujkodszam < 100 then begin
					ujkodszam := 100;
				end;
				Query_Run ( QueryKoz, 'INSERT INTO SZOTAR (SZ_FOKOD, SZ_ALKOD, SZ_MENEV, SZ_ERVNY) VALUES '+
					'(''113'', ''' +Format('%5.5d', [ujkodszam])+ ''', '''+CHonnan.Text+''', 1 )',true);
			end;
		end;
	end;
	{A k�s�bbi rekordokra az �tlagok �tsz�m�t�sa ha �zemanyag volt }
	if tipus = 0 then begin
		Query_Run (QueryAl, 'SELECT * FROM KOLTSEG WHERE KS_RENDSZ = '''+MaskEdit1.Text+
			''' AND KS_TELE > 0 AND KS_KMORA > '+SqlSzamString(StringSzam(MaskEdit2.Text), 10,2)+
			' AND KS_TIPUS = 0 ' +
			' AND KS_TETAN = ''0'' '+
			' ORDER BY KS_KMORA ' ,true);
		if QueryAl.RecordCount > 0 then begin
			atlag		:= GetKoltsegAtlag(QueryAl.FieldByName('KS_KTKOD').AsString);
			Query_Update (QueryAl, 'KOLTSEG',
				[
				'KS_ATLAG',		SqlSzamString(atlag,10,2)
				], ' WHERE KS_KTKOD = '+QueryAl.FieldByName('KS_KTKOD').AsString );
		end;
	end;
	if tipus = 2 then begin
		Query_Run (QueryAl, 'SELECT * FROM KOLTSEG WHERE KS_RENDSZ = '''+MaskEdit1.Text+
			''' AND KS_TELE > 0 AND KS_KMORA > '+SqlSzamString(StringSzam(MaskEdit2.Text), 10,2)+
			' AND KS_TIPUS = 2 ' +
			' ORDER BY KS_KMORA ' ,true);
		if QueryAl.RecordCount > 0 then begin
			atlag		:= GetAdBlueKoltsegAtlag(QueryAl.FieldByName('KS_KTKOD').AsString);
			Query_Update (QueryAl, 'KOLTSEG',
				[
				'KS_ATLAG',		SqlSzamString(atlag,10,2)
				], ' WHERE KS_KTKOD = '+QueryAl.FieldByName('KS_KTKOD').AsString );
		end;
	end;

//  if MaskEdit13.Text='' then
  if JARATBOL then
  	Close
  else
  begin
		//Query_Run(JaratbeDlg.Query4, 'SELECT * FROM koltseg WHERE KS_JAKOD = '''+jaratkod+''' ORDER BY KS_DATUM',true);
		//Query4.Locate('KS_KTKOD', KoltsegbeDlg.ret_kod, [] );
    jaratkod:='';
    ret_kod:='';
    M1.Text:='';
    M1B.Text:='';
    M2.Text:='';
    M3.Text:='';
    M4.Text:='';
    M5.Text:='';
    M6.Text:='';
    M7.Text:='';
    MaskEdit2.Text:='';
    MaskEdit3.Text:='';
    MaskEdit4.Text:='';
    MaskEdit6.Text:='';
    MaskEdit7.Text:='';
    MaskEdit9.Text:='';
    MaskEdit10.Text:='';
    MaskEdit11.Text:='';
    MaskEdit12.Text:='';
    //MaskEdit13.Text:='';
    MaskEdit1.Text:='';
    MaskEdit5.Text:='';
    MaskEdit8.Text:='';
    CB1.Items.Clear;
    CB1.Visible:=False;
    BitBtn4.Visible:=True;
    M2.SetFocus;
    modosult:=False;
  end;
end;

procedure TKoltsegbeDlg.MaskEditClick(Sender: TObject);
begin
	with Sender as TMaskEdit do begin
		SelectAll;
  	end;
end;

procedure TKoltsegbeDlg.Modosit(Sender: TObject);
begin
	modosult := true;

end;

procedure TKoltsegbeDlg.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
	EgyebDlg.FormReturn(Key);
end;

procedure TKoltsegbeDlg.M4Exit(Sender: TObject);
begin
	with Sender as TMaskEdit do begin
		Text 	:= StrSzamStr( Text , 12, Tag);
	end;
	//M5.Text 	:= SzamString(StringSzam(M3.Text)* StringSzam(M4.Text),12,2);
  M5.Text 	:=  SzamString(RoundTo( StringSzam(M3.Text) * StringSzam(M4.Text),0),12,2);
//	Afaszam;
end;

procedure TKoltsegbeDlg.MaskKeyPress(Sender: TObject; var Key: Char);
begin
	with Sender as TMaskEdit do begin
		Key := EgyebDlg.Jochar(Text,Key,12,Tag);
  	end;
end;

procedure TKoltsegbeDlg.CValutaChange(Sender: TObject);
begin
	modosult := true;
{
	M3.Text	:= SzamString(EgyebDlg.ArfolyamErtek(listavnem[CValuta.ItemIndex], M2.Text, true),12,4);
	M5.Text 	:= SzamString(StringSzam(M3.Text) * StringSzam(M4.Text),12,2);
	Afaszam;
}
{  BitElkuld.SetFocus;}
end;

procedure TKoltsegbeDlg.BitBtn2Click(Sender: TObject);
begin
  if (ret_kod<>'')or(MaskEdit13.Text<>'') then
  begin
    MaskEdit2.SetFocus;
    exit;
  end;
	GepkocsiValaszto(MaskEdit1);
  MaskEdit2.Setfocus;
end;

procedure TKoltsegbeDlg.BitBtn1Click(Sender: TObject);
begin
	if ret_kod <> '' then exit;		{M�dos�t�s}
	JaratValaszto(M0, M1);
	jaratkod	:= M0.Text;
	M1B.Text	:= Query_Select('JARAT', 'JA_KOD', jaratkod, 'JA_SNEV1');
	M2.Setfocus;
end;

procedure TKoltsegbeDlg.M2Change(Sender: TObject);
begin
  if ret_kod<>'' then exit;
	Modosit(Sender);
 if Length(m2.Text)=11 then
 begin
  if jaratkod<>'' then
  begin
    if not JaratDatumEllenorzes(M2.Text,M21.Text,jaratkod)    then
    begin
      NoticeKi('A k�lts�g id�pontja nem esik a j�ratid� intervallum�ba! '+M2.Text+'  '+M21.Text);
//      NoticeKi('A k�lts�g id�pontja nem esik a j�ratid� intervallum�ba!');
      Try
      M2.SetFocus;
      Except
      End;
    end;
  end;
	M3.Text	:= SzamString(EgyebDlg.ArfolyamErtek(listavnem[CValuta.ItemIndex], M2.Text, false),12,4);
	modosult := true;
  MaskEdit13.OnChange(self);
 end;
end;

procedure TKoltsegbeDlg.M2Exit(Sender: TObject);
begin
	DatumExit(Sender, false);
end;

procedure TKoltsegbeDlg.BitBtn10Click(Sender: TObject);
begin
	EgyebDlg.Calendarbe(M2);
  	M3.Text	:= SzamString(EgyebDlg.ArfolyamErtek(listavnem[CValuta.ItemIndex], M2.Text, false),12,4);
end;

{A j�ratokb�l h�vtuk meg a TOLT2 met�dust}
procedure TKoltsegbeDlg.Tolt2(cim : string; teko, jakod, rendsz, jarat:string );
begin
	Tolto(cim, teko);
  	{Kit�ltj�k a j�ratot + rendsz�mot �s nem lehet megv�ltoztatni}
	M1.Text 			:= jarat;
  	BitBtn1.Enabled		:= false;
  	jaratkod          	:= jakod;
  	MaskEdit1.Text		:= rendsz;
  	MaskEdit13.Text		:= rendsz;
	BitBtn2.Enabled		:= false;
	rendszam			:= rendsz;
	MaskEdit5.Text		:= rendsz;
  ADBLUES:= Query_Select('GEPKOCSI','GK_RESZ',rendsz,'GK_EUROB')>'3';
	SetMaskEdits([MaskEdit1, MaskEdit5]);
	BitBtn3.Enabled		:= false;
   // A h�t� tankol�shoz berakjuk a j�raton l�v� p�tkocsikat egy list�ba , azok k�z�l v�laszthatunk
	if ret_kod = '' then begin		{NEM M�dos�t�s}
   CB1.Visible			:= true;
   MaskEdit8.Visible	:= false;
   BitBtn4.Visible		:= false;
	 CB1.Clear;
   Query_Run (QueryAl, 'SELECT DISTINCT JP_POREN FROM JARPOTOS WHERE JP_JAKOD = '+jakod,true);
	 if QueryAl.RecordCount < 1 then begin
   	// A h�t�s a f� g�pkocsihoz lesz hozz�rakva
       CB1.Items.Add(rendsz);
   end else begin
		while not QueryAl.Eof do begin
       	CB1.Items.Add(QueryAl.FieldByName('JP_POREN').AsString);
       	QueryAl.Next;
    end;
	 end;
   CB1.ItemIndex		:= 0;
   MaskEdit8.Text		:= CB1.Items[0];
	 modosult   			:= false;
	 BitBtn4.Enabled		:= false;
  end;
end;

procedure TKoltsegbeDlg.FormDestroy(Sender: TObject);
begin
	listavnem.Free;
	afalist.Free;
end;

procedure TKoltsegbeDlg.AFAComboChange(Sender: TObject);
begin
	modosult := true;
//	Afaszam;
end;

procedure TKoltsegbeDlg.Afaszam;
var
	afaert	: double;
  netto		: double;
begin
	afaert		:= StringSzam(AfaCombo.Text);
	netto		:= StringSzam(M5.Text) / (1+afaert/100);
  	M6.Text 	:= SzamString(StringSzam(M5.Text) - netto,12,2);
	Modosit(self);
end;

procedure TKoltsegbeDlg.CheckBox1Click(Sender: TObject);
var
  BEODAT: string;
begin
  // ha most felvitt, m�g mentetlen adat, akkor a QueryAl m�g nem tartalmazza az adatsort
  try
     BEODAT:= QueryAl.FieldByName('KS_BEODAT').AsString;
  except
     BEODAT:= '';
     end;  // try-except
  if  (not (EgyebDlg.user_super or (GetRightTag(593) <> RG_NORIGHT))) and (BEODAT<>'') then begin// aut. beolvasott k�lts�g
		CheckBox1.Checked 	:= QueryAl.FieldByName('KS_TELE').AsInteger > 0;
		NoticeKi('Automatikusan beolvasott k�lts�get csak rendszergazda vagy jogosult felhaszn�l� m�dos�that!');
    exit;
    end;
	MaskEdit4.Text	:= '';
	//modosult := true;
  telemod:=True;
//  BitElkuld.Enabled:=True;
end;

procedure TKoltsegbeDlg.CValutaExit(Sender: TObject);
begin
  	M3.Text		:= SzamString(EgyebDlg.ArfolyamErtek(listavnem[CValuta.ItemIndex], M2.Text, true),12,4);
  	M5.Text 	:=  SzamString(RoundTo( StringSzam(M3.Text) * StringSzam(M4.Text),0),12,2);
//	Afaszam;
//	AfaCombo.SetFocus;
	FiModCombo.SetFocus;
end;

procedure TKoltsegbeDlg.AFAComboExit(Sender: TObject);
begin
	Afaszam;
//  	BitElkuld.SetFocus;
end;

procedure TKoltsegbeDlg.BitBtn3Click(Sender: TObject);
begin
  if (ret_kod<>'')or(MaskEdit13.Text<>'') then
//   if ret_kod<>EmptyStr then
   begin
    MaskEdit6.SetFocus;
    exit;
   end;
	GepkocsiValaszto(MaskEdit5);
  MaskEdit6.Setfocus;
end;

procedure TKoltsegbeDlg.BitBtn4Click(Sender: TObject);
var
	szurosor	: integer;
begin
  if M2.Text='' then
  begin
    NoticeKi('A d�tumot meg kell adni!');
    M2.SetFocus;
    exit;
  end;
  if ret_kod<>'' then
  begin
    MaskEdit9.SetFocus;
    exit;
  end;
	GepkocsiValaszto(MaskEdit8);
  if not EgyebDlg.HutosGk(MaskEdit8.Text) then
  begin
    // nem h�t�s
		NoticeKi('NEM h�t�s g�pkocsi!');
    MaskEdit8.Text:='';
    exit;
  end;
//  HUTOS:=pos( Query_Select('GEPKOCSI','GK_RESZ',MaskEdit13.Text,'GK_GKKAT'),HUTOTIP)>0 ;

  if MaskEdit8.Text='' then
  begin
    exit;
  end;
{  if MaskEdit13.Text<>'' then
  begin
    exit;
  end;
	modosult := true;
             }
  /// j�rat keres�s
  Query_Run (QueryAl, 'SELECT * FROM JARAT,JARPOTOS WHERE JA_KOD=JP_JAKOD and JP_POREN='''+MaskEdit8.Text+''' and JA_JKEZD<='''+M2.Text+''' and JA_JVEGE>='''+M2.Text+'''', True);
  if QueryAl.RecordCount=0 then
  begin
    ShowMessage('Nincs j�rat!!!');
    exit;
  end;

  if QueryAl.RecordCount>1 then
  begin
    ShowMessage('T�bb j�rat! V�lasszon!');
  	EgyebDlg.SzuroGrid.RowCount := EgyebDlg.SzuroGrid.RowCount + 1;
	  szurosor := EgyebDlg.SzuroGrid.RowCount - 1 ;
  	EgyebDlg.SzuroGrid.Cells[0,szurosor] 	:= '190';
//    EgyebDlg.SzuroGrid.Cells[1,szurosor] 	:='' ;
    QueryAl.First;
    EgyebDlg.SzuroGrid.Cells[1,szurosor] 	:= 'JA_KOD='''''+QueryAl.FieldByName('JA_KOD').AsString+''''  ;
    QueryAl.Next;
    while not QueryAl.Eof do
    begin
      EgyebDlg.SzuroGrid.Cells[1,szurosor]:=EgyebDlg.SzuroGrid.Cells[1,szurosor]+ ' or JA_KOD='''''+QueryAl.FieldByName('JA_KOD').AsString+''''  ;
      QueryAl.Next;
    end;
//    EgyebDlg.SzuroGrid.Cells[1,szurosor] 	:= 'JA_KOD='''''+QueryAl.FieldByName('JA_KOD').AsString+''''  ;
//    EgyebDlg.SzuroGrid.Cells[1,szurosor] 	:= 'JA_RENDSZ = '''+MaskEdit13.Text+''' and JA_JKEZD<='''+M2.Text+''' and JA_JVEGE>='''+M2.Text+''''  ;

	  EgyebDlg.SzuroGrid.RowCount := EgyebDlg.SzuroGrid.RowCount + 1;
  	EgyebDlg.SzuroGrid.Cells[0,szurosor+1] 	:= '190';
	  EgyebDlg.SzuroGrid.Cells[1,szurosor+1] 	:= 'JA_KOD = '''' ';

    EgyebDlg.stralap:=True;

  	JaratValaszto(M0, M1);
	  jaratkod	:= M0.Text;
  	M1B.Text	:= Query_Select('JARAT', 'JA_KOD', jaratkod, 'JA_SNEV1');

	  // A sz�r�s t�rl�se
  	GridTorles(EgyebDlg.SzuroGrid, szurosor+1);
	  GridTorles(EgyebDlg.SzuroGrid, szurosor);

  end
  else
  begin
    jaratkod:=QueryAl.FieldByName('JA_KOD').AsString ;
    M1.Text:= GetJaratszam(jaratkod) ;
    M1B.Text:=QueryAl.FieldByName('JA_SNEV1').AsString ;
  end;

 	MaskEdit9.Setfocus;
end;

procedure TKoltsegbeDlg.CB1Change(Sender: TObject);
begin
	modosult := true;
	MaskEdit8.Text		:= CB1.Items[CB1.ItemIndex];
end;

procedure TKoltsegbeDlg.KmHozzaadas(Mresz, Mkmora : TMaskEdit );
var
	hozza	: double;
begin
   // A hozz�adott km. ellen�rz�se
   if Mresz.Text <> '' then begin
   	hozza	:= StringSzam(Query_Select('GEPKOCSI', 'GK_RESZ', Mresz.Text, 'GK_HOZZA'));
       if (hozza > 0 ) then begin
       	if StringSzam(Mkmora.Text) <> 0 then begin
               if StringSzam(Mkmora.Text) < hozza then begin
               	Mkmora.Text	:= Format('%.2f', [hozza + StringSZam(Mkmora.Text)]);
               end;
           end;
       end;
   end;
end;

procedure TKoltsegbeDlg.FimodComboChange(Sender: TObject);
var
	egyar	: double;
	tipus	: integer;
begin
	modosult := true;
	// Ha telepi lesz kiv�lasztva, akkor bet�ltj�k az �rat
	if UpperCase(FimodCombo.Text) = 'TELEP' then begin
		tipus	:= PageControl1.ActivePageIndex;
		case tipus of
		0:
			begin
			egyar	:= GetTelepiAr(M2.Text);
			M4.Text	:= Format('%.2f', [StringSzam(MaskEdit3.Text) * egyar]);
			ComboSet (CValuta, 'HUF', listavnem);
			M3.Text	:= '1';
			M4Exit(M4);
			end;
		2:
			begin
			egyar	:= GetTelepiAr(M2.Text, 1);
			M4.Text	:= Format('%.2f', [StringSzam(MaskEdit7.Text) * egyar]);
			ComboSet (CValuta, 'HUF', listavnem);
			M3.Text	:= '1';
			M4Exit(M4);
			end;
		3:
			begin
			egyar	:= GetTelepiAr(M2.Text);
			M4.Text	:= Format('%.2f', [StringSzam(MaskEdit10.Text) * egyar]);
			ComboSet (CValuta, 'HUF', listavnem);
			M3.Text	:= '1';
			M4Exit(M4);
			end;
		end;
	end;
end;

procedure TKoltsegbeDlg.MaskEdit2Change(Sender: TObject);
begin
	MaskEdit4.Text	:= '';
	modosult := true;
end;

procedure TKoltsegbeDlg.MaskEdit3Change(Sender: TObject);
begin
	MaskEdit4.Text	:= '';
	modosult := true;
end;

procedure TKoltsegbeDlg.MaskEdit6Change(Sender: TObject);
begin
       if not ADBLUES then
       begin
        NoticeKi('FIGYELEM! A g�pkocsi nem EUR4-es vagy feletti kateg�ri�j�!');
       end;
	modosult := true;
	MaskEdit11.Text	:= '';
end;

procedure TKoltsegbeDlg.MaskEdit7Change(Sender: TObject);
begin
       if not ADBLUES then
       begin
        NoticeKi('FIGYELEM! A g�pkocsi nem EUR4-es vagy feletti kateg�ri�j�!');
       end;
	modosult := true;
	MaskEdit11.Text	:= '';
end;

procedure TKoltsegbeDlg.M21Change(Sender: TObject);
begin
	Modosit(Sender);
	modosult := true;
 if Length(M21.Text)=5 then
 begin
  if jaratkod<>'' then
  begin
    if not JaratDatumEllenorzes(M2.Text,M21.Text,jaratkod)    then
    begin
      NoticeKi('A k�lts�g id�pontja nem esik a j�ratid� intervallum�ba! '+M2.Text+'  '+M21.Text);
//      NoticeKi('A k�lts�g id�pontja nem esik a j�ratid� intervallum�ba!');
      Try
      M21.SetFocus;
      Except
      End;
    end;
  end;
 end;
end;

procedure TKoltsegbeDlg.M21Exit(Sender: TObject);
begin
	IdoExit(Sender, false);
end;

procedure TKoltsegbeDlg.OrszagComboChange(Sender: TObject);
begin
	modosult := true;

end;

procedure TKoltsegbeDlg.PageControl1Change(Sender: TObject);
var
  tipus:integer;
begin
	if ret_kod <> '' then begin		{M�dos�t�s}
			tipus			:= KTIPUS;
      PageControl1.ActivePageIndex:=tipus;
  end
  else
  begin
   case PageControl1.ActivePageIndex of
    3:       // H�t�
    begin
    //  if PageControl1.ActivePageIndex<>3 then exit;
     if jaratkod='' then exit;
     // A h�t� tankol�shoz berakjuk a j�raton l�v� p�tkocsikat egy list�ba , azok k�z�l v�laszthatunk
     CB1.Visible			:= true;
     MaskEdit8.Visible	:= false;
     BitBtn4.Visible		:= false;
  	 CB1.Clear;
     Query_Run (QueryAl, 'SELECT DISTINCT JP_POREN FROM JARPOTOS WHERE JP_JAKOD = '+jaratkod,true);
  	 if QueryAl.RecordCount < 1 then begin
      if HUTOS then
     	  // A h�t�s a f� g�pkocsihoz lesz hozz�rakva
         CB1.Items.Add(MaskEdit13.Text);
     end else begin
	  	while not QueryAl.Eof do begin
        if EgyebDlg.HutosGk(QueryAl.FieldByName('JP_POREN').AsString) then
         	CB1.Items.Add(QueryAl.FieldByName('JP_POREN').AsString);
         	QueryAl.Next;
      end;
	   end;
     if CB1.Items.IndexOf(MaskEdit13.Text) > -1 then
       CB1.ItemIndex:= CB1.Items.IndexOf(MaskEdit13.Text)
     else
       CB1.ItemIndex		:= 0;
     MaskEdit8.Text		:= CB1.Items[0];
  	 //modosult   			:= false;
	   BitBtn4.Enabled		:= false;
     if CB1.Items.Count=0  then  // nincs h�t�s rendsz�m
     begin
        NoticeKi('NINCS h�t�s j�rm�!');
        PageControl1.ActivePageIndex:=1;
     end;
    end;
    2:        // AD Blue
    begin
//       if (not ADBLUES)and(MaskEdit5.Text<>'') then
       if (not ADBLUES) then
       begin
        NoticeKi('FIGYELEM! A g�pkocsi nem EUR4-es vagy feletti kateg�ri�j�!');
        PageControl1.ActivePageIndex:=1;
       end;
    end;
    0:        // �zemanyag
    begin
      if EgyebDlg.HutosGk(MaskEdit13.Text) then  // H�t�s
      begin
        NoticeKi('H�t�s j�rm�! �zemanyagot nem lehet megadni!');
        PageControl1.ActivePageIndex:=3;
      end;
    end;
   end;
  end;
end;

procedure TKoltsegbeDlg.MaskEdit10Change(Sender: TObject);
begin
  modosult:=True;
	MaskEdit12.Text	:= '';

end;

procedure TKoltsegbeDlg.MaskEdit9Change(Sender: TObject);
begin
  modosult:=True;
	MaskEdit12.Text	:= '';

end;

procedure TKoltsegbeDlg.BitBtn5Click(Sender: TObject);
begin
//	if (ret_kod <> '')or (jaratkod<>'') then
  if JARATBOL then
    exit;
//  if (jaratkod<>'') and (MaskEdit13.Text<>'') then
//    exit;
	if (ret_kod <> '') then
  begin
    OrszagCombo.SetFocus;
     exit;
  end;
  M1.Text:='';
  M1B.Text:='';
  jaratkod:='';
	GepkocsiValaszto(MaskEdit13);

//  jaratkod:=QueryAl.FieldByName('JA_KOD').AsString ;
//  M1.Text:= GetJaratszam(jaratkod) ;
//  M1B.Text:=QueryAl.FieldByName('JA_SNEV1').AsString ;
//  ADBLUES:= Query_Select('GEPKOCSI','GK_RESZ',MaskEdit13.Text,'GK_EUROB')>'3';

end;

procedure TKoltsegbeDlg.PageControl1Enter(Sender: TObject);
begin
  { if ret_kod<>EmptyStr then
   begin
    M4.SetFocus;
   end; }
end;

procedure TKoltsegbeDlg.MaskEdit13Change(Sender: TObject);
var
	szurosor	: integer;
begin
  if ret_kod<>'' then exit;
  if jaratkod<>'' then
  begin
  	M1B.Text	:= Query_Select('JARAT', 'JA_KOD', jaratkod, 'JA_SNEV1');
    //ADBLUES:= Query_Select('GEPKOCSI','GK_RESZ',MaskEdit13.Text,'GK_EUROB')>'3';
    exit;
  end;
  ADBLUES:= Query_Select('GEPKOCSI','GK_RESZ',MaskEdit13.Text,'GK_EUROB')>'3';
  modosult:=True;
  if M2.Text='' then       // d�tum
  begin
   // M2.SetFocus;
    exit;
  end;
  if MaskEdit13.Text='' then
  begin
  //  MaskEdit13.SetFocus;
    exit;
  end;
  HUTOS:=EgyebDlg.HutosGk(MaskEdit13.Text);
  if HUTOS then
  begin
    MaskEdit8.Text:=MaskEdit13.Text;
    //MaskEdit13.Text:='';
    PageControl1.ActivePageIndex:=3;
    MaskEdit9.SetFocus;
    Query_Run (QueryAl, 'SELECT * FROM JARAT,JARPOTOS WHERE JA_KOD=JP_JAKOD and JP_POREN='''+MaskEdit8.Text+''' and JA_JKEZD<='''+M2.Text+''' and JA_JVEGE>='''+M2.Text+'''', True);
  end
  else
  begin
    MaskEdit1.Text:=MaskEdit13.Text;
    if ADBLUES then
    MaskEdit5.Text:=MaskEdit13.Text;
    Query_Run (QueryAl, 'SELECT * FROM JARAT WHERE JA_RENDSZ='''+MaskEdit13.Text+''' and JA_JKEZD<='''+M2.Text+''' and JA_JVEGE>='''+M2.Text+'''', True);
  end;
  if QueryAl.RecordCount=0 then
  begin
    ShowMessage('Nincs j�rat!!!');
    exit;
  end;

  if QueryAl.RecordCount>1 then
  begin
    ShowMessage('T�bb j�rat! V�lasszon!');

  	EgyebDlg.SzuroGrid.RowCount := EgyebDlg.SzuroGrid.RowCount + 1;
	  szurosor := EgyebDlg.SzuroGrid.RowCount - 1 ;
  	EgyebDlg.SzuroGrid.Cells[0,szurosor] 	:= '190';
    if HUTOS then
    begin
      QueryAl.First;
      EgyebDlg.SzuroGrid.Cells[1,szurosor] 	:= 'JA_KOD='''''+QueryAl.FieldByName('JA_KOD').AsString+''''  ;
      QueryAl.Next;
      while not QueryAl.Eof do
      begin
        EgyebDlg.SzuroGrid.Cells[1,szurosor]:=EgyebDlg.SzuroGrid.Cells[1,szurosor]+ ' or JA_KOD='''''+QueryAl.FieldByName('JA_KOD').AsString+''''  ;
        QueryAl.Next;
      end;
      //EgyebDlg.SzuroGrid.Cells[1,szurosor] 	:= 'JA_RENDSZ = '''+MaskEdit13.Text+''' and JA_JKEZD<='''+M2.Text+''' and JA_JVEGE>='''+M2.Text+''''
    end
    else
      EgyebDlg.SzuroGrid.Cells[1,szurosor] 	:= 'JA_RENDSZ = '''+MaskEdit13.Text+''' and JA_JKEZD<='''+M2.Text+''' and JA_JVEGE>='''+M2.Text+''''  ;

	  EgyebDlg.SzuroGrid.RowCount := EgyebDlg.SzuroGrid.RowCount + 1;
  	EgyebDlg.SzuroGrid.Cells[0,szurosor+1] 	:= '190';
	  EgyebDlg.SzuroGrid.Cells[1,szurosor+1] 	:= 'JA_KOD = '''' ';

    EgyebDlg.stralap:=True;

  	JaratValaszto(M0, M1);
	  jaratkod	:= M0.Text;
  	M1B.Text	:= Query_Select('JARAT', 'JA_KOD', jaratkod, 'JA_SNEV1');

	  // A sz�r�s t�rl�se
  	GridTorles(EgyebDlg.SzuroGrid, szurosor+1);
	  GridTorles(EgyebDlg.SzuroGrid, szurosor);

  {
    jaratkod:=QueryAl.FieldByName('JA_KOD').AsString ;
    M1.Text:= GetJaratszam(jaratkod) ;
    if CB1.Text='' then
    begin
      CB1.ItemIndex:= CB1.Items.IndexOf(QueryAl.FieldByName('JA_ORSZ').AsString) ;
    end;
    M1B.Text:=QueryAl.FieldByName('JA_SNEV1').AsString ;
    }
  end
  else
  begin
    jaratkod:=QueryAl.FieldByName('JA_KOD').AsString ;
    M1.Text:= GetJaratszam(jaratkod) ;
    if CB1.Text='' then
    begin
      CB1.ItemIndex:= CB1.Items.IndexOf(QueryAl.FieldByName('JA_ORSZ').AsString) ;
    end;
    M1B.Text:=QueryAl.FieldByName('JA_SNEV1').AsString ;
  end;
  PageControl1.OnChange(self);
end;

procedure TKoltsegbeDlg.FormShow(Sender: TObject);
begin
// modosult:= modosult;
// modosult:=False;
end;

procedure TKoltsegbeDlg.MaskEdit8Change(Sender: TObject);
begin
  if M2.Text='' then
  begin
    exit;
  end;
  if MaskEdit8.Text='' then
  begin
    exit;
  end;
  if MaskEdit13.Text<>'' then
  begin
    exit;
  end;
	modosult := true;

  /// j�rat keres�s
{  Query_Run (QueryAl, 'SELECT * FROM JARAT,JARPOTOS WHERE JA_KOD=JP_JAKOD and JP_POREN='''+MaskEdit8.Text+''' and JA_JKEZD<='''+M2.Text+''' and JA_JVEGE>='''+M2.Text+'''', True);
  if QueryAl.RecordCount>1 then
    ShowMessage('T�bb j�rat!!!');
  if QueryAl.RecordCount=0 then
  begin
    ShowMessage('Nincs j�rat!!!');
    exit;
  end;
  jaratkod:=QueryAl.FieldByName('JA_KOD').AsString ;
  M1.Text:= GetJaratszam(jaratkod) ;
  M1B.Text:=QueryAl.FieldByName('JA_SNEV1').AsString ;
//  PageControl1.OnChange(self);
 }
end;

procedure TKoltsegbeDlg.MaskEdit13KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (key=VK_RETURN) and (MaskEdit13.Text='') then
    BitBtn5.OnClick(self);
end;

procedure TKoltsegbeDlg.MaskEdit8Enter(Sender: TObject);
begin
  if ret_kod<>'' then
  begin
    MaskEdit9.SetFocus;
    exit;
  end;

end;

procedure TKoltsegbeDlg.MaskEdit1Enter(Sender: TObject);
begin
  if (ret_kod<>'')or(MaskEdit13.Text<>'') then
  begin
    MaskEdit2.SetFocus;
    exit;
  end;

end;

procedure TKoltsegbeDlg.MaskEdit5Enter(Sender: TObject);
begin
  if (ret_kod<>'')or(MaskEdit13.Text<>'') then
//   if ret_kod<>EmptyStr then
   begin
    MaskEdit6.SetFocus;
   end;

end;

procedure TKoltsegbeDlg.MaskEdit13Enter(Sender: TObject);
begin
	if (ret_kod <> '')or (jaratkod<>'') then
    OrszagCombo.SetFocus;

end;

procedure TKoltsegbeDlg.PageControl1Changing(Sender: TObject;
  var AllowChange: Boolean);
begin
  if jaratkod='' then
  begin
    NoticeKi('NINCS j�ratsz�m!');
    AllowChange:=False;
    M2.SetFocus;
  end;
end;


function TKoltsegbeDlg.JaratDatumEllenorzes(datum, ido,
  jaratszam: string): boolean;
var
  kdat,vdat,kido,vido: string;
  ad,kd,vd: double;
begin
  ad:=DateTimeToNum(datum,ido);
  kdat:= Query_Select('JARAT','JA_KOD',jaratszam,'JA_JKEZD');
  vdat:= Query_Select('JARAT','JA_KOD',jaratszam,'JA_JVEGE');
  kido:= Query_Select('JARAT','JA_KOD',jaratszam,'JA_KEIDO');
  vido:= Query_Select('JARAT','JA_KOD',jaratszam,'JA_VEIDO');
  kd:=DateTimeToNum(kdat,kido);
  vd:=DateTimeToNum(vdat,vido);

  Result:= (kd<=ad)and(vd>=ad);

  if ido=EmptyStr then
    Result:= (kdat<=datum)and(vdat>=datum);
end;

procedure TKoltsegbeDlg.MaskEdit9Exit(Sender: TObject);
var
  utkm: double;
begin
	with Sender as TMaskEdit do begin
		Text 	:= StrSzamStr( Text , 12, Tag);
	end;
  utkm:=StrToFloatDef( Query_Select('GEPKOCSI','GK_RESZ',CB1.Text,'GK_UT_KM'),0);
  if StrToFloatDef(MaskEdit9.Text,0)>utkm+200 then
  begin
     	NoticeKi('Az �zem�ra sokkal nagyobb mint az utolj�ra be�rt! ('+FloatToStr( utkm)+') K�rem ellen�rizze az adatokat!');
     // MaskEdit9.SetFocus;
  end;
end;

procedure TKoltsegbeDlg.M3Enter(Sender: TObject);
begin
  if StringSzam(M3.Text)>0 then
    M4.SetFocus;
end;

procedure TKoltsegbeDlg.CheckBox2Click(Sender: TObject);
begin
	MaskEdit4.Text	:= '';
	modosult := true;
end;

end.


