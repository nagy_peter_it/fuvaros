unit UniEllenor;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls;

type
  TUniEllenorDlg = class(TForm)
    Memo1: TMemo;
    Button1: TButton;
    Image1: TImage;
    procedure Button1Click(Sender: TObject);
  private
    procedure TelenormailEllenorzes;
    procedure DolgozoTelefonLista;
  public
    { Public declarations }
  end;

var
  UniEllenorDlg: TUniEllenorDlg;

implementation

uses
	Egyeb, J_SQL, Kozos;
{$R *.dfm}

procedure TUniEllenorDlg.Button1Click(Sender: TObject);
begin
  // TelenormailEllenorzes;
  DolgozoTelefonLista;
  {Image1.Canvas.Brush.Color:= clRed;
  Image1.Canvas.Brush.Style:= bsDiagCross;
  Image1.Canvas.Rectangle
  Image1.Refresh;}

end;

procedure TUniEllenorDlg.TelenormailEllenorzes;
var
  S, KOD, Telefonszam, Email, EmailSzam, Tulaj: string;

begin
  S:= 'select do_kod, do_name, do_email from dolgozo where DO_ARHIV=0';
  Query_Run(EgyebDlg.Query1, S, True);
  Memo1.Lines.Clear;
  with EgyebDlg.Query1 do begin
    while not Eof do begin
        KOD:= Fields[0].AsString;
        Tulaj:= Fields[1].AsString;
        Email:= Fields[2].AsString;
        Telefonszam:= EgyebDlg.GetDolgozoSMSKontakt(KOD);
        if (pos('@telenormail', Email) > 0) or (pos('@pannonmail', Email) > 0) then begin
           EmailSzam:= EgyebDlg.SepFrontToken('@', Email);  // pl. '3255014'
           EmailSzam:= '+3620'+EmailSzam;
           if EmailSzam <> Telefonszam then begin  // gubanc
              S:= KOD+' '+Tulaj+ ': '+Telefonszam+' <> '+ EmailSzam+ ' ('+Email+')';
              Memo1.Lines.Add(S);
              end;
           end;  // if
        Next;
        end;  // while
     end;  // with
  Memo1.Lines.Add('-------------------   lista v�ge   ------------------');
end;

procedure TUniEllenorDlg.DolgozoTelefonLista;
var
  S, KOD, Telefonszam, Email, EmailSzam, Tulaj: string;

begin
  S:= 'select do_kod, do_name, do_email from dolgozo where DO_ARHIV=0';
  Query_Run(EgyebDlg.Query1, S, True);
  Memo1.Lines.Clear;
  with EgyebDlg.Query1 do begin
    while not Eof do begin
        KOD:= Fields[0].AsString;
        Tulaj:= Fields[1].AsString;
        Email:= Fields[2].AsString;
        Telefonszam:= EgyebDlg.GetDolgozoSMSKontakt(KOD);
        S:=Tulaj+ ';'+Telefonszam;
        Memo1.Lines.Add(S);
        Next;
        end;  // while
     end;  // with
  Memo1.Lines.Add('-------------------   lista v�ge   ------------------');
end;

end.
