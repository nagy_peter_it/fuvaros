unit DolgCsopbe;

interface

uses
	SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
	Forms, Dialogs, StdCtrls, Buttons, Mask, DB, DBTables, J_ALFORM, ADODB;

type
	TDolgCsopbeDlg = class(TJ_AlformDlg)
	BitElkuld: TBitBtn;
	BitKilep: TBitBtn;
    Label1: TLabel;
	 Query1: TADOQuery;
	 Label9: TLabel;
    M1: TMaskEdit;
    BitBtn8: TBitBtn;
    cbCsoport: TComboBox;
	 procedure BitKilepClick(Sender: TObject);
	 procedure FormCreate(Sender: TObject);
	 procedure BitElkuldClick(Sender: TObject);
	 procedure MaskEditClick(Sender: TObject);
	 procedure FormKeyDown(Sender: TObject; var Key: Word;
	   Shift: TShiftState);
	 procedure Tolto(cim, kod : string); override;
    procedure BitBtn8Click(Sender: TObject);
	private
    lista_cskod   : TStringList;
	public
    DOKOD: string;
	end;

var
	DolgCsopbeDlg: TDolgCsopbeDlg;

implementation

uses
 Egyeb, J_SQL, Kozos, J_VALASZTO;

{$R *.DFM}

procedure TDolgCsopbeDlg.BitKilepClick(Sender: TObject);
begin
	ret_kod := '';
	Close;
end;

procedure TDolgCsopbeDlg.FormCreate(Sender: TObject);
begin
	EgyebDlg.SeTADOQueryDatabase(Query1);
	ret_kod:= '';
  lista_cskod:= TStringList.Create;
  SzotarTolt(cbCsoport, '400' , lista_cskod, false, true );
end;

procedure TDolgCsopbeDlg.Tolto(cim, kod : string);  // nincs m�dos�t�s
begin
	Caption:= cim;
	ret_kod:= kod;
end;

procedure TDolgCsopbeDlg.BitElkuldClick(Sender: TObject);
var
  DONEV, CSNEV: string;
begin

	if M1.Text = '' then begin
		NoticeKi('D�tum megad�sa k�telez�!');
		M1.Setfocus;
		Exit;
	end;
  // csak "�j" m�velet van
	if ret_kod = '' then begin
    DONEV:=Query_select('DOLGOZO', 'DO_KOD', DOKOD, 'DO_NAME');
    CSNEV:=Query_SelectString('SZOTAR', 'select SZ_MENEV from SZOTAR where SZ_FOKOD=''400'' and SZ_ALKOD='''+lista_cskod[cbCsoport.ItemIndex]+'''');
		Query_Run(Query1, 'INSERT INTO DOLGOZOCSOP (DC_DOKOD, DC_DONEV, DC_DATTOL, DC_CSKOD, DC_CSNEV) ' +
      ' VALUES ( '''+DOKOD+''', '''+DONEV+''', '''+M1.Text+''', '''+lista_cskod[cbCsoport.ItemIndex]+''', '''+CSNEV+''')');
    ret_kod		:= DOKOD+'|'+M1.Text;
  	end;
	Close;
end;

procedure TDolgCsopbeDlg.MaskEditClick(Sender: TObject);
begin
	with Sender as TMaskEdit do begin
  	SelectAll;
  end;
end;

procedure TDolgCsopbeDlg.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
	EgyebDlg.FormReturn(Key);
end;

procedure TDolgCsopbeDlg.BitBtn8Click(Sender: TObject);
begin
	EgyebDlg.Calendarbe(M1);
end;

end.







