unit Telelofizetbe;

interface

uses
	SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
	Forms, Dialogs, StdCtrls, Buttons, Mask, DB, DBTables, J_ALFORM, ADODB,
  Vcl.ExtCtrls, Vcl.CheckLst;

type
	TTelElofizetBeDlg = class(TJ_AlformDlg)
	BitElkuld: TBitBtn;
	BitKilep: TBitBtn;
    Label3: TLabel;
    M7_: TMaskEdit;
    Label5: TLabel;
    ebPIN: TMaskEdit;
    Label6: TLabel;
    M5: TMaskEdit;
    M6: TMaskEdit;
    BitBtn1: TBitBtn;
    ebTelefonszam: TMaskEdit;
    Label2: TLabel;
    M1: TMaskEdit;
    Label4: TLabel;
	 Query1: TADOQuery;
	 Query2: TADOQuery;
	 BitBtn37: TBitBtn;
	 Label9: TLabel;
    M12: TMaskEdit;
    BitBtn8: TBitBtn;
    ebMegjegyzes: TMemo;
    Label7: TLabel;
    chkTech: TCheckBox;
    M15: TMaskEdit;
    M16: TMaskEdit;
    BitBtn2: TBitBtn;
    BitBtn3: TBitBtn;
    chkElsodleges: TCheckBox;
    Label1: TLabel;
    Label8: TLabel;
    cbSzamlazasiCsoport: TComboBox;
    Label11: TLabel;
    chkHivasSzures: TCheckBox;
    Label12: TLabel;
    cbStatusz: TComboBox;
    meTemp1: TMaskEdit;
    meTemp2: TMaskEdit;
    M61: TMaskEdit;
    meTelDokod: TMaskEdit;
    Label13: TLabel;
    Label14: TLabel;
    meAppleID: TMaskEdit;
    meAppleJelszo: TMaskEdit;
    clbSzolgaltatasok: TCheckListBox;
    SzummaPanel: TPanel;
    SzolgaltatasLabel: TLabel;
	 procedure BitKilepClick(Sender: TObject);
	 procedure FormCreate(Sender: TObject);
	 procedure BitElkuldClick(Sender: TObject);
	 procedure MaskEditClick(Sender: TObject);
	 procedure FormKeyDown(Sender: TObject; var Key: Word;
	   Shift: TShiftState);
	 procedure Tolto(cim, kod : string); override;
   procedure CheckListBoxSet (CLB: TCheckListBox; kodlist: TStringList; SelectSQL: string);
   procedure SzummaPanelFrissit;
   function TelszolgElment: string;
	 procedure BitBtn1Click(Sender: TObject);
	 procedure BitBtn37Click(Sender: TObject);
    procedure BitBtn8Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure M16Click(Sender: TObject);
    procedure M61Click(Sender: TObject);
    procedure clbSzolgaltatasokClick(Sender: TObject);
	private
    lista_statusz, lista_szolgaltatasok, lista_szamlazasicsoport: TStringList;
	public
	end;

var
	TelElofizetBeDlg: TTelElofizetBeDlg;

implementation

uses
 Egyeb, J_SQL, Kozos,  J_VALASZTO;

{$R *.DFM}

procedure TTelElofizetBeDlg.BitKilepClick(Sender: TObject);
begin
	ret_kod := '';
	Close;
end;

procedure TTelElofizetBeDlg.FormCreate(Sender: TObject);
begin
	EgyebDlg.SeTADOQueryDatabase(Query1);
	EgyebDlg.SeTADOQueryDatabase(Query2);
  lista_statusz:= TStringList.Create;
  lista_szolgaltatasok:= TStringList.Create;
  lista_szamlazasicsoport:= TStringList.Create;
  SzotarTolt(CBStatusz, '144' , lista_statusz, false, false );
  // SzotarTolt(cbMobilnet, '143' , lista_mobilnet, false, false );
  SzotarTolt_multi(clbSzolgaltatasok, '143' , lista_szolgaltatasok, false);
  SzotarTolt(cbSzamlazasiCsoport, '145' , lista_szamlazasicsoport, false, false );

  // pr�b�lkoz�s... FlowPanelTolt(fpMobilnet, '143' , lista_mobilnet, false, false );
	ret_kod:= '';
end;

procedure TTelElofizetBeDlg.FormDestroy(Sender: TObject);
begin
  if lista_statusz <> nil then lista_statusz.Free;
  if lista_szolgaltatasok <> nil then lista_szolgaltatasok.Free;
  if lista_szamlazasicsoport <> nil then lista_szamlazasicsoport.Free;
end;

procedure TTelElofizetBeDlg.Tolto(cim, kod : string);
begin
	SetMaskEdits([M1, M5, M6, M15, M16]);
	Caption 		:= cim;
	ret_kod  := kod;
	M1.Text		:= ret_kod;
	if ret_kod <> '' then begin
		if Query_Run (Query1, 'SELECT * FROM TELELOFIZETES WHERE TE_ID = '+ret_kod ,true) then begin
      ComboSet (cbStatusz, Trim(Query1.FieldByName('TE_STATUSKOD').AsString), lista_statusz);
      ComboSet (cbSzamlazasiCsoport, Trim(Query1.FieldByName('TE_SZAMLACSOPKOD').AsString), lista_szamlazasicsoport);

      // ComboSet (cbMobilnet, Trim(Query1.FieldByName('TE_MOBILNETKOD').AsString), lista_mobilnet);
      CheckListBoxSet (clbSzolgaltatasok, lista_szolgaltatasok, 'select TZ_SZOLGID from TELSZOLG where TZ_TEID='+ret_kod);
      SzummaPanelFrissit;

			ebPIN.Text		:= Query1.FieldByname('TE_PIN').AsString;
			ebTelefonszam.Text	:= Query1.FieldByname('TE_TELSZAM').AsString;
			ebMegjegyzes.Text		:= Query1.FieldByname('TE_MEGJE').AsString;
 			M12.Text	:= Query1.FieldByname('TE_KIADDAT').AsString;


			M15.Text:= Query1.FieldByname('TE_DOKOD').AsString;
      M16.Text:= Query_Select('DOLGOZO', 'DO_KOD', M15.Text, 'DO_NAME');
			M5.Text:= Query1.FieldByname('TE_TKID').AsString;
      M6.Text:= GetTelMegnevezes(M5.Text);
      M61.Text:= GetTelDoNev(M5.Text);
      meTelDokod.Text:= GetTelDokod(M5.Text);
 			meAppleID.Text		:= Query1.FieldByname('TE_APPLEID').AsString;
			meAppleJelszo.Text		:= Query1.FieldByname('TE_APPLEJELSZO').AsString;

      chkElsodleges.Checked	:= Query1.FieldByName('TE_DOELS').AsInteger > 0;
      // chkRoaming.Checked	:= Query1.FieldByName('TE_ROAMING').AsInteger > 0;
      chkHivasSzures.Checked	:= Query1.FieldByName('TE_HIVSZUR').AsInteger > 0;
      chkTech.Checked	:= Query1.FieldByName('TE_TECH').AsInteger > 0;
			ebMegjegyzes.Text		:= Query1.FieldByname('TE_MEGJE').AsString;
		end;
	end;
end;

procedure TTelElofizetBeDlg.CheckListBoxSet (CLB: TCheckListBox; kodlist: TStringList; SelectSQL: string);
var
	sorszam, i: integer;
begin
  for i := 0 to CLB.Items.Count - 1 do CLB.Checked[i]:= False;  // t�rl�s
  if Query_Run (Query2, SelectSQL, true) then begin
    while not Query2.Eof do begin
      sorszam	:= kodlist.IndexOf(Query2.Fields[0].AsString);
      CLB.Checked[sorszam]:= True;  // be�ll�tjuk
      Query2.Next;
      end;  // while
    end;  // if
end;

procedure TTelElofizetBeDlg.clbSzolgaltatasokClick(Sender: TObject);
begin
   SzummaPanelFrissit;
end;

procedure TTelElofizetBeDlg.SzummaPanelFrissit;
var
  i: integer;
  S: string;
begin
  S:= '';
  for i := 0 to clbSzolgaltatasok.Items.Count - 1 do begin
     if clbSzolgaltatasok.Checked[i] then begin
        S:= Felsorolashoz(S, ' '+clbSzolgaltatasok.Items[i], ',', True);
        end;  // if
     end;  // for
  // SzummaPanel.Caption:= S;
  SzolgaltatasLabel.Caption:= S;
end;

procedure TTelElofizetBeDlg.BitElkuldClick(Sender: TObject);
var
  UjTeID, ElsodlegesSzamCount: integer;
  DOKOD, ElsodlegesSzam, Res: string;
begin

	if ebTelefonszam.Text = '' then begin
		NoticeKi('Telefonsz�m megad�sa k�telez�!');
		ebTelefonszam.Setfocus;
		Exit;
	  end;
  if M15.Text <> '' then DOKOD:= M15.Text;
  if DOKOD='' then DOKOD:= GetTelDokod(M5.Text);
  ElsodlegesSzamCount:= GetElsodlegesSzamCount(DOKOD);  // -1, ha egy�ltal�n nincs telefonja
  ElsodlegesSzam:= GetElsodlegesSzam(DOKOD);
  if chkElsodleges.Checked then begin
    if ElsodlegesSzam <> M1.Text then begin
        if NoticeKi('A dolgoz�hoz m�s els�dleges sz�m van rendelve. Szeretn� hogy ez a telefonsz�m legyen az els�dleges?', NOT_QUESTION ) = 0 then begin
          SetElsodlegesSzam (DOKOD, M1.Text);
          end  // if noticeki
        else begin
          chkElsodleges.Checked:= False;
          end;
        end;
    end
  else begin
    if ElsodlegesSzamCount=0 then begin
        if NoticeKi('A dolgoz�hoz nincs els�dleges sz�m rendelve. Szeretn� hogy ez a telefonsz�m legyen az els�dleges?', NOT_QUESTION ) = 0 then begin
           chkElsodleges.Checked:= True;
           end;  // if noticeki
        end;
    end;  // else

	if ret_kod = '' then begin  // �j el�fizet�s
    UjTeID:= Query_Insert_Identity (Query1, 'TELELOFIZETES',
      [
      'TE_STATUSKOD',  ''''+lista_statusz[cbStatusz.ItemIndex]+'''',
      'TE_SZAMLACSOPKOD', ''''+lista_szamlazasicsoport[cbSzamlazasiCsoport.ItemIndex]+'''',
      // 'TE_MOBILNETKOD', ''''+lista_mobilnet[cbMobilnet.ItemIndex]+'''',
      'TE_TELSZAM', ''''+ebTelefonszam.Text+'''',
      'TE_PIN', ''''+ebPIN.Text+'''',
      // ezeket majd k�l�n �ll�tjuk
      // 'TE_DOKOD', ''''+M15.Text+'''',
      // 'TE_TKID', ''''+M5.Text+'''',
      'TE_TECH', BoolToIntStr(chkTech.Checked),
      'TE_DOELS', BoolToIntStr(chkElsodleges.Checked),
      // 'TE_ROAMING', BoolToIntStr(chkRoaming.Checked),
      'TE_HIVSZUR', BoolToIntStr(chkHivasSzures.Checked),
      'TE_APPLEID', ''''+meAppleID.Text+'''',
      'TE_APPLEJELSZO', ''''+meAppleJelszo.Text+'''',
      'TE_KIADDAT', ''''+M12.Text+'''',
      'TE_MEGJE', ''''+ebMegjegyzes.Text+''''
      ], 'TE_ID' );
     if UjTeID>=0 then begin  // ha nem volt sikeres, �gyis kap SQL hiba ablakot
      ret_kod:= IntToStr(UjTeID);
      M1.Text:= ret_kod;
      end;
     end
  else begin
     Query_Update (Query1, 'TELELOFIZETES',
      [
      'TE_STATUSKOD',  ''''+lista_statusz[cbStatusz.ItemIndex]+'''',
      'TE_SZAMLACSOPKOD', ''''+lista_szamlazasicsoport[cbSzamlazasiCsoport.ItemIndex]+'''',
      // 'TE_MOBILNETKOD', ''''+lista_mobilnet[cbMobilnet.ItemIndex]+'''',
      'TE_TELSZAM', ''''+ebTelefonszam.Text+'''',
      'TE_PIN', ''''+ebPIN.Text+'''',
      // ezeket majd k�l�n �ll�tjuk
      // 'TE_DOKOD', ''''+M15.Text+'''',
      // 'TE_TKID', ''''+M5.Text+'''',
      'TE_TECH', BoolToIntStr(chkTech.Checked),
      'TE_DOELS', BoolToIntStr(chkElsodleges.Checked),
      // 'TE_ROAMING', BoolToIntStr(chkRoaming.Checked),
      'TE_HIVSZUR', BoolToIntStr(chkHivasSzures.Checked),
      'TE_APPLEID', ''''+meAppleID.Text+'''',
      'TE_APPLEJELSZO', ''''+meAppleJelszo.Text+'''',
      'TE_KIADDAT', ''''+M12.Text+'''',
      'TE_MEGJE', ''''+ebMegjegyzes.Text+''''
      ], ' WHERE TE_ID='+ret_kod);
      end;  // else

  Res:= TelszolgElment;
  if Res='' then Res:= SetElofizetesDolgozo(ret_kod, M15.Text);
  if Res='' then Res:= SetElofizetesTelefon(ret_kod, M5.Text);

  if Res='' then
  	Close
  else NoticeKi(Res);  // ha hiba volt, nem csukjuk be
end;

function TTelElofizetBeDlg.TelszolgElment: string;
var
  i: integer;
  SQLArray: TStringList;
  S: string;
begin
   Result:= '';
   SQLArray:= TStringList.Create;
   try  // finally
     try  // except
      S:= 'delete from TELSZOLG where TZ_TEID='+ret_kod;
      SQLArray.Add(S);
      for i := 0 to clbSzolgaltatasok.Items.Count - 1 do begin
        if clbSzolgaltatasok.Checked[i] then begin
           S:='insert into TELSZOLG (TZ_TEID, TZ_SZOLGID)';
           S:= S+ ' values ('+ret_kod+', '+lista_szolgaltatasok[i]+')';
           SQLArray.Add(S);
           end;  // if
        end;  // for
       Result:= ExecuteSQLTransaction(SQLArray);
      except
        on E: exception do begin
          Result:= E.Message;
          end;
        end;  // try-except
    finally
      if SQLArray<>nil then SQLArray.Free;
      end;  // try-finally
end;

procedure TTelElofizetBeDlg.M16Click(Sender: TObject);
begin
   EgyebDlg.DolgozoFotoMutato(M15.Text, M16);
end;

procedure TTelElofizetBeDlg.M61Click(Sender: TObject);
begin
   EgyebDlg.DolgozoFotoMutato(meTelDokod.Text, M61);
end;

procedure TTelElofizetBeDlg.MaskEditClick(Sender: TObject);
begin
	with Sender as TMaskEdit do begin
  	SelectAll;
  end;
end;

procedure TTelElofizetBeDlg.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 // Nagyp 2016-08-03
 // EgyebDlg.FormReturn(Key);
end;

procedure TTelElofizetBeDlg.BitBtn1Click(Sender: TObject);
var
  UjDOKOD, TelefonElofizetese: string;
begin
  if M15.text <> '' then begin
    if NoticeKi('Az el�fizet�s m�r dolgoz�hoz van rendelve. Szeretn� hogy t�r�lj�k a dolgoz�hoz rendel�st �s egy '+
         'telefonhoz rendelj�k az el�fizet�st?', NOT_QUESTION ) <> 0 then begin
            Exit;
         end;
    end;
  M15.Text	:= '';
	M16.Text	:= '';
	TelefonValaszto2(M5, True);
  TelefonElofizetese := Query_Select('TELELOFIZETES', 'TE_TKID', M5.Text, 'TE_ID');
  if (TelefonElofizetese <> '') and (TelefonElofizetese <> M1.Text) then begin
    if NoticeKi('A kiv�lasztott telefonk�sz�l�khez egy m�sik el�fizet�s is tartozik. Folytatjuk az �sszerendel�st?', NOT_QUESTION) <> 0 then begin
     	Exit;
      end;
    end;
  M6.Text:= GetTelMegnevezes(M5.Text);
  M61.Text:= GetTelDoNev(M5.Text);
end;



procedure TTelElofizetBeDlg.BitBtn2Click(Sender: TObject);
begin
  if M5.text <> '' then begin
    if NoticeKi('Az el�fizet�s m�r telefonhoz van rendelve. Szeretn� hogy t�r�lj�k a telefonhoz rendel�st �s k�zvetlen�l '+
         'a dolgoz�hoz rendelj�k az el�fizet�st?', NOT_QUESTION ) <> 0 then begin
            Exit;
         end;
    end;
  M5.Text:= '';
  M6.Text:= '';
  M61.Text:= '';
	DolgozoValaszto(M15, M16);
end;

procedure TTelElofizetBeDlg.BitBtn37Click(Sender: TObject);
begin
	M5.Text	:= '';
	M6.Text	:= '';
  M61.Text:= '';
end;

procedure TTelElofizetBeDlg.BitBtn3Click(Sender: TObject);
begin
	M15.Text	:= '';
	M16.Text	:= '';
end;

procedure TTelElofizetBeDlg.BitBtn8Click(Sender: TObject);
begin
	EgyebDlg.Calendarbe(M12);
end;

end.




