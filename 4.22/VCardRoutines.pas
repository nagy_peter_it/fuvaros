unit VCardRoutines;

interface

uses KozosTipusok, Math, Sysutils;

function GetVCARD(const VCUtonev, VCVezeteknev,  VCMobile, VCWorkPhone, VCEmail, VCBeosztas, VCSzervezet, UID: string): string;
function String2UTF8HEX(S: string): string;

implementation

function GetVCARD(const VCUtonev, VCVezeteknev, VCMobile, VCWorkPhone, VCEmail, VCBeosztas, VCSzervezet, UID: string): string;
const
  VCEleje = 'BEGIN:VCARD';
  VCVege = 'END:VCARD';
  UTFEleje = 'CHARSET=UTF-8;ENCODING=QUOTED-PRINTABLE:';
  CRLF = AnsiString(#13#10);
var
  S: string;
  MyUID: TGuid;
  HRes: HResult;
begin
  S:= VCEleje + CRLF;
  S:= S+ 'VERSION:3.0' + CRLF;
  // S:= S+ 'N:'+VCUtonev+';'+VCVezeteknev+';;'+ CRLF;
  S:= S+ 'N;' + UTFEleje + String2UTF8HEX(VCUtonev) + ';' + String2UTF8HEX(VCVezeteknev)+ ';;'+ CRLF;
  // S:= S+ 'FN:'+VCVezeteknev+' '+VCUtonev+ CRLF;
  S:= S+ 'FN;'+UTFEleje + String2UTF8HEX(VCVezeteknev+' '+VCUtonev)+ CRLF;
  if trim(VCSzervezet) <>'' then
    S:= S+ 'ORG;'+UTFEleje + String2UTF8HEX(VCSzervezet)+ CRLF;
  if trim(VCMobile) <>'' then
    S:= S+ 'TEL;TYPE=CELL;TYPE=PREF:'+VCMobile+ CRLF;
  if trim(VCWorkPhone) <>'' then
    S:= S+ 'TEL;TYPE=WORK:'+VCWorkPhone+ CRLF;
  if trim(VCEmail) <>'' then
    S:= S+ 'EMAIL:'+VCEmail+ CRLF;
  if trim(VCBeosztas) <>'' then
    // S:= S+ 'TITLE:'+VCBeosztas+ CRLF;
    S:= S+ 'TITLE;'+UTFEleje + String2UTF8HEX(VCBeosztas)+ CRLF;
  if UID <> '' then begin
    // HRes:= CreateGuid(MyUID);
    // if HRes = S_OK then
    //   S:= S+ 'UID:'+GuidToString(MyUID)+ CRLF
    // else S:= S+ 'UID:'+VCMobile+'_'+VCEmail+ CRLF; // ha ne adj' isten nem megy a CreateGuid, akkor sz�ks�gmegold�snak j� lesz
    S:= S+ 'UID:'+UID+ CRLF; // valami stabil UID kell, hogy ellen�rizni tudjuk a v�ltoz�st a file-ban
    end;  // UID
  S:= S+ VCVege;
  Result:= S;
end;

{  V2.1
function GetVCARD(const VCUtonev, VCVezeteknev, VCMobile, VCWorkPhone, VCEmail, VCBeosztas, VCSzervezet, UID: string): string;
const
  VCEleje = 'BEGIN:VCARD';
  VCVege = 'END:VCARD';
  UTFEleje = 'CHARSET=UTF-8;ENCODING=QUOTED-PRINTABLE:';
  CRLF = AnsiString(#13#10);
var
  S: string;
  MyUID: TGuid;
  HRes: HResult;
begin
  S:= VCEleje + CRLF;
  S:= S+ 'VERSION:2.1' + CRLF;
  // S:= S+ 'N:'+VCUtonev+';'+VCVezeteknev+';;'+ CRLF;
  S:= S+ 'N;' + UTFEleje + String2UTF8HEX(VCUtonev) + ';' + String2UTF8HEX(VCVezeteknev)+ ';;'+ CRLF;
  // S:= S+ 'FN:'+VCVezeteknev+' '+VCUtonev+ CRLF;
  S:= S+ 'FN;'+UTFEleje + String2UTF8HEX(VCVezeteknev+' '+VCUtonev)+ CRLF;
  if trim(VCSzervezet) <>'' then
    S:= S+ 'ORG;'+UTFEleje + String2UTF8HEX(VCSzervezet)+ CRLF;
  if trim(VCMobile) <>'' then
    S:= S+ 'TEL;CELL:'+VCMobile+ CRLF;
  if trim(VCWorkPhone) <>'' then
    S:= S+ 'TEL;WORK:'+VCWorkPhone+ CRLF;
  if trim(VCEmail) <>'' then
    S:= S+ 'EMAIL:'+VCEmail+ CRLF;
  if trim(VCBeosztas) <>'' then
    // S:= S+ 'TITLE:'+VCBeosztas+ CRLF;
    S:= S+ 'TITLE;'+UTFEleje + String2UTF8HEX(VCBeosztas)+ CRLF;
  if UID <> '' then begin
    // HRes:= CreateGuid(MyUID);
    // if HRes = S_OK then
    //   S:= S+ 'UID:'+GuidToString(MyUID)+ CRLF
    // else S:= S+ 'UID:'+VCMobile+'_'+VCEmail+ CRLF; // ha ne adj' isten nem megy a CreateGuid, akkor sz�ks�gmegold�snak j� lesz
    S:= S+ 'UID:'+UID+ CRLF; // valami stabil UID kell, hogy ellen�rizni tudjuk a v�ltoz�st a file-ban
    end;  // UID
  S:= S+ VCVege;
  Result:= S;
end;
}

{function GetVCARD(const VCUtonev, VCVezeteknev, VCMobile, VCWorkPhone, VCEmail, VCBeosztas, VCSzervezet: string; KellUID: boolean): string;
const
  VCEleje = 'BEGIN:VCARD';
  VCVege = 'END:VCARD';
  UTFEleje = 'CHARSET=UTF-8;ENCODING=QUOTED-PRINTABLE:';
  CRLF = AnsiString(#13#10);
var
  S: string;
  MyUID: TGuid;
  HRes: HResult;
begin
  S:= VCEleje + CRLF;
  S:= S+ 'VERSION:2.1' + CRLF;
  // S:= S+ 'N:'+VCUtonev+';'+VCVezeteknev+';;'+ CRLF;
  S:= S+ 'N;' + UTFEleje + String2UTF8HEX(VCUtonev) + ';' + String2UTF8HEX(VCVezeteknev)+ ';;'+ CRLF;
  // S:= S+ 'FN:'+VCVezeteknev+' '+VCUtonev+ CRLF;
  S:= S+ 'FN;'+UTFEleje + String2UTF8HEX(VCVezeteknev+' '+VCUtonev)+ CRLF;
  if trim(VCSzervezet) <>'' then
    S:= S+ 'ORG;'+UTFEleje + String2UTF8HEX(VCSzervezet)+ CRLF;
  if trim(VCMobile) <>'' then
    S:= S+ 'TEL;CELL:'+VCMobile+ CRLF;
  if trim(VCWorkPhone) <>'' then
    S:= S+ 'TEL;WORK:'+VCWorkPhone+ CRLF;
  if trim(VCEmail) <>'' then
    S:= S+ 'EMAIL:'+VCEmail+ CRLF;
  if trim(VCBeosztas) <>'' then
    // S:= S+ 'TITLE:'+VCBeosztas+ CRLF;
    S:= S+ 'TITLE;'+UTFEleje + String2UTF8HEX(VCBeosztas)+ CRLF;
  if KellUID then begin
    // HRes:= CreateGuid(MyUID);
    // if HRes = S_OK then
    //   S:= S+ 'UID:'+GuidToString(MyUID)+ CRLF
    // else S:= S+ 'UID:'+VCMobile+'_'+VCEmail+ CRLF; // ha ne adj' isten nem megy a CreateGuid, akkor sz�ks�gmegold�snak j� lesz
    S:= S+ 'UID:'+VCMobile+'_'+VCEmail+ CRLF; // valami stabil UID kell, hogy ellen�rizni tudjuk a v�ltoz�st a file-ban
    end;  // UID
  S:= S+ VCVege;
  Result:= S;
end;
}

function String2UTF8HEX(S: string): string;
var
   BlockString: string;
   utf8Text: UTF8String;
   ConvertedLength, L, i: integer;
   Dest: PAnsiChar;
begin
   L := Length(S);
   SetLength(utf8Text, L * SizeOf(Char) + 1);
   ConvertedLength:= UnicodeToUtf8(PAnsiChar(utf8Text),  Length(utf8Text), PWideChar(S), Length(S));
   BlockString:= '';
   // for i:=1 to length(utf8Text) do begin
   for i:=1 to ConvertedLength-1 do begin  // nem akarjuk a z�r� 00-kat...
      BlockString:=BlockString + '='+inttohex(Integer(utf8Text[i]),2);
      end;
   result:= BlockString;
end;


end.
