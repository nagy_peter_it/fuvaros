unit Jaratbe;

interface

uses
	SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
	Forms, Dialogs, StdCtrls, Buttons, Mask, DB, DBTables, TabNotBk, J_ALFORM,
	Grids, ComCtrls, DBGrids, ExtCtrls, ADODB, QuickRpt, FileCtrl, ShellApi,
	QrPdfFilt, Clipbrd ;

type
 TJaratbeDlg = class(TJ_AlformDlg)
	Label12: TLabel;
	 TabbedNotebook1: TTabbedNotebook;
	 DBGrid1: TDBGrid;
	 DataSource1: TDataSource;
	 DBGrid2: TDBGrid;
	 DataSource2: TDataSource;
	 DBGrid4: TDBGrid;
	 DataSource4: TDataSource;
	 TabbedNotebook2: TTabbedNotebook;
	 GroupBox2: TGroupBox;
	 Label27: TLabel;
	 Label28: TLabel;
	 Label29: TLabel;
	 MaskEdit4: TMaskEdit;
	 MaskEdit6: TMaskEdit;
	 MaskEdit7: TMaskEdit;
	 MaskEdit8: TMaskEdit;
	 Label30: TLabel;
	 DSFelrako: TDataSource;
	 ButValVevo: TBitBtn;
	 Query1: TADOQuery;
	 Query2: TADOQuery;
	 Query3: TADOQuery;
	 QueryKoz: TADOQuery;
	 Query3VI_JAKOD: TStringField;
	 Query3VI_HONNAN: TStringField;
	 Query3VI_HOVA: TStringField;
	 Query3VI_VEKOD: TStringField;
	 Query3VI_FDEXP: TBCDField;
	 Query3VI_FDIMP: TBCDField;
	 Query3VI_FDKOV: TBCDField;
	 Query3VI_VALNEM: TStringField;
	 Query3VI_ARFOLY: TBCDField;
	 Query3VI_MEGJ: TStringField;
	 Query3VI_VIKOD: TStringField;
	 Query3VI_VENEV: TStringField;
	 Query3VI_KULF: TIntegerField;
	 Query3VI_TIPUS: TBCDField;
	 Query3VI_BEDAT: TStringField;
	 Query3VI_KIDAT: TStringField;
	 Query3VI_SAKOD: TStringField;
	 Query3VI_UJKOD: TIntegerField;
	 Query4: TADOQuery;
	 Query4KS_JAKOD: TStringField;
	 Query4KS_DATUM: TStringField;
	 Query4KS_KMORA: TFloatField;
	 Query4KS_VALNEM: TStringField;
	 Query4KS_OSSZEG: TFloatField;
	 Query4KS_ARFOLY: TFloatField;
	 Query4KS_RENDSZ: TStringField;
	 Query4KS_UZMENY: TFloatField;
	 Query4VISZONY: TStringField;
	 Query4KS_MEGJ: TStringField;
	 Query4KS_LEIRAS: TStringField;
	 Query4KS_VIKOD: TStringField;
	 Query6: TADOQuery;
	 Query6SU_JAKOD: TStringField;
	 Query6SU_KMORA: TFloatField;
	 Query6SU_KMOR2: TFloatField;
	 Query6SU_MEGJE: TStringField;
	 Query6SU_ERTEK: TFloatField;
	 Query6SU_SUKOD: TStringField;
	 QueryFelrako: TADOQuery;
	 DataSource7: TDataSource;
	 Query7: TADOQuery;
    Query7JP_JAKOD: TStringField;
    Query7JP_POREN: TStringField;
    Query7JP_PORKM: TBCDField;
    Query8: TADOQuery;
	 DataSource8: TDataSource;
    Query8JS_JAKOD: TStringField;
	 Query8JS_SORSZ: TIntegerField;
	 Query8JS_SOFOR: TStringField;
    Query8JS_KIDAT: TStringField;
	 Query8JS_KIIDO: TStringField;
    Query8JS_BEDAT: TStringField;
	 Query8JS_BEIDO: TStringField;
	 Query8JS_MEGJE: TStringField;
	 Query8JS_SZAKM: TBCDField;
    Query8JS_JADIJ: TBCDField;
	 Query8JS_EXDIJ: TBCDField;
    Query8JS_FELRA: TBCDField;
	 Query8SOFORNEV: TStringField;
	 Panel1: TPanel;
	 BitElkuld: TBitBtn;
	 BitBtn21: TBitBtn;
	 BitKilep: TBitBtn;
    Panel2: TPanel;
    BitBtn3: TBitBtn;
    BitBtn4: TBitBtn;
	 BitBtn34: TBitBtn;
    BitBtn5: TBitBtn;
    BitBtn22: TBitBtn;
    BitBtn11: TBitBtn;
    BitBtn12: TBitBtn;
    Panel3: TPanel;
    BitBtn13: TBitBtn;
    BitBtn14: TBitBtn;
	 BitBtn15: TBitBtn;
    BitBtn19: TBitBtn;
	 Panel4: TPanel;
    BitBtn23: TBitBtn;
	 BitBtn24: TBitBtn;
	 BitBtn25: TBitBtn;
	 Panel5: TPanel;
    Label2: TLabel;
	 M01: TMaskEdit;
    Label22: TLabel;
	 M02: TMaskEdit;
	 Label16: TLabel;
	 CheckBox4: TCheckBox;
    Panel6: TPanel;
	 Label9: TLabel;
	 M12: TMaskEdit;
    Panel7: TPanel;
	 GroupBox5: TGroupBox;
    DBGrid3: TDBGrid;
	 Panel9: TPanel;
    Label31: TLabel;
    Label32: TLabel;
	 Label33: TLabel;
	 Label34: TLabel;
    Label35: TLabel;
    MaskEdit5: TMaskEdit;
	 MaskEdit9: TMaskEdit;
    MaskEdit10: TMaskEdit;
	 CVnem: TComboBox;
	 MaskEdit11: TMaskEdit;
    MaskEdit12: TMaskEdit;
    Panel10: TPanel;
    GroupBox3: TGroupBox;
	 DBGrid5: TDBGrid;
	 Me1: TMemo;
	 Me2: TMemo;
    Me3: TMemo;
    Panel11: TPanel;
    Label26: TLabel;
	 MM1: TMaskEdit;
    MM2: TMaskEdit;
	 MM3: TMaskEdit;
    MM4: TMaskEdit;
	 MM5: TMaskEdit;
	 Panel12: TPanel;
	 BitBtn31: TBitBtn;
    Panel13: TPanel;
	 BitBtn27: TBitBtn;
    BitBtn28: TBitBtn;
	 BitBtn29: TBitBtn;
    BitBtn33: TBitBtn;
    Query7JP_PORK2: TBCDField;
	 Query7JP_JPKOD: TIntegerField;
    Panel15: TPanel;
    BitBtn20: TBitBtn;
	 BitBtn35: TBitBtn;
	 BitBtn38: TBitBtn;
	 Query8JS_KMOR1: TBCDField;
	 Query8JS_KMOR2: TBCDField;
	 Query4KS_TIPUS: TIntegerField;
	 Query4KS_KTKOD: TIntegerField;
    Label4: TLabel;
    CBFelelos: TComboBox;
	 BitBtn2: TBitBtn;
    MaskEdit50: TMaskEdit;
	 Label5: TLabel;
	 Query7JP_UZEM1: TBCDField;
    Query7JP_UZEM2: TBCDField;
    BitBtn6: TBitBtn;
    BitBtn9: TBitBtn;
    Panel14: TPanel;
    Label3: TLabel;
	 Label13: TLabel;
	 Label14: TLabel;
	 Label15: TLabel;
    Label21: TLabel;
    Label25: TLabel;
    M4: TMaskEdit;
    CheckBox1: TCheckBox;
    BitBtn1: TBitBtn;
	 M13: TMaskEdit;
    M14: TMaskEdit;
    M15: TMaskEdit;
    M20: TMaskEdit;
    M27: TMaskEdit;
	 MaskEdit1: TMaskEdit;
	 GroupBox6: TGroupBox;
	 DBGrid6: TDBGrid;
    Panel8: TPanel;
    BitBtn18: TBitBtn;
    BitBtn26: TBitBtn;
	 BitBtn39: TBitBtn;
	 Panel16: TPanel;
	 Panel17: TPanel;
	 Panel18: TPanel;
	 RadioButton1: TRadioButton;
	 RadioButton2: TRadioButton;
    Button1: TButton;
    CBOrsz: TComboBox;
    Panel19: TPanel;
	 BitBtn10: TBitBtn;
	 BitBtn16: TBitBtn;
    BitBtn17: TBitBtn;
    BitBtn36: TBitBtn;
    DBGrid7: TDBGrid;
    DataSourceCMR: TDataSource;
	 QueryCMR: TADOQuery;
	 QueryCMRCM_CMKOD: TIntegerField;
    QueryCMRCM_CSZAM: TStringField;
    QueryCMRCM_CRDAT: TStringField;
	 QueryCMRCM_DOKOD: TStringField;
    QueryCMRCM_KIDAT: TStringField;
	 QueryCMRCM_VIDAT: TStringField;
    QueryCMRCM_JAKOD: TStringField;
	 QueryCMRCM_JARAT: TStringField;
    QueryCMRCM_MEGJE: TStringField;
    QueryCMRCM_MASIK: TIntegerField;
    QueryCMRCM_VIKOD: TStringField;
    QueryCMRCM_DONEV: TStringField;
	 QueryCMRVISZONY: TStringField;
    Label8: TLabel;
    M2: TMaskEdit;
    BitBtn7: TBitBtn;
    Label1: TLabel;
	 M3: TMaskEdit;
    BitBtn8: TBitBtn;
    BitBtn37: TBitBtn;
	 SpeedButton5: TSpeedButton;
	 SpeedButton4: TSpeedButton;
    BitBtn40: TBitBtn;
    BitBtn41: TBitBtn;
    BitBtn42: TBitBtn;
    Label7: TLabel;
    M2A: TMaskEdit;
    M3A: TMaskEdit;
	 Label10: TLabel;
    BitBtn43: TBitBtn;
	 M2B: TMaskEdit;
	 BitBtn44: TBitBtn;
    BitBtn45: TBitBtn;
    BitBtn46: TBitBtn;
    BitBtn47: TBitBtn;
    SpeedButton3: TSpeedButton;
	 SpeedButton6: TSpeedButton;
    QueryFelrako2: TADOQuery;
    BitBtn48: TBitBtn;
    Query4KS_TELE: TIntegerField;
    Query4KS_TETAN: TIntegerField;
    QueryFelrakoJM_SORSZ: TIntegerField;
    QueryFelrakoMS_FAJTA: TStringField;
    QueryFelrakoMS_FELNEV: TStringField;
    QueryFelrakoMS_FORSZ: TStringField;
    QueryFelrakoMS_FELIR: TStringField;
    QueryFelrakoMS_FELTEL: TStringField;
    QueryFelrakoMS_FELCIM: TStringField;
    QueryFelrakoMS_FELDAT: TStringField;
    QueryFelrakoMS_FELIDO: TStringField;
    QueryFelrakoMS_LERNEV: TStringField;
    QueryFelrakoMS_ORSZA: TStringField;
	 QueryFelrakoMS_LELIR: TStringField;
    QueryFelrakoMS_LERTEL: TStringField;
    QueryFelrakoMS_LERCIM: TStringField;
    QueryFelrakoMS_LERDAT: TStringField;
    QueryFelrakoMS_LERIDO: TStringField;
    QueryFelrakoMS_FELARU: TStringField;
    QueryFelrakoJM_MBKOD: TIntegerField;
    QueryUj3: TADOQuery;
    QueryFelrakoJM_ORIGS: TIntegerField;
    Label17: TLabel;
    M28: TMaskEdit;
    RG1: TRadioGroup;
    Label18: TLabel;
    M29: TMaskEdit;
    Label19: TLabel;
    M30: TMaskEdit;
    BitBtn30: TBitBtn;
    CheckBox2: TCheckBox;
    Query4KS_ORSZA: TStringField;
    BitBtn32: TBitBtn;
    Label6: TLabel;
    BitBtn49: TBitBtn;
    Query11: TADOQuery;
    Query13: TADOQuery;
    Label20: TLabel;
    M3B: TMaskEdit;
    Label23: TLabel;
    Panel20: TPanel;
    BitBtn50: TBitBtn;
    T_GYUJTOMIND: TADOQuery;
    T_GYUJTOMINDKS_KTKOD: TIntegerField;
    T_GYUJTOMINDKS_JAKOD: TStringField;
    T_GYUJTOMINDKS_DATUM: TStringField;
    T_GYUJTOMINDKS_KMORA: TBCDField;
    T_GYUJTOMINDKS_VALNEM: TStringField;
    T_GYUJTOMINDKS_ARFOLY: TBCDField;
    T_GYUJTOMINDKS_MEGJ: TStringField;
    T_GYUJTOMINDKS_RENDSZ: TStringField;
    T_GYUJTOMINDKS_LEIRAS: TStringField;
    T_GYUJTOMINDKS_UZMENY: TBCDField;
    T_GYUJTOMINDKS_OSSZEG: TBCDField;
    T_GYUJTOMINDKS_TELE: TIntegerField;
    T_GYUJTOMINDKS_SZKOD: TStringField;
    T_GYUJTOMINDKS_AFASZ: TBCDField;
    T_GYUJTOMINDKS_AFAKOD: TStringField;
    T_GYUJTOMINDKS_AFAERT: TBCDField;
    T_GYUJTOMINDKS_ERTEK: TBCDField;
    T_GYUJTOMINDKS_VIKOD: TStringField;
    T_GYUJTOMINDKS_ATLAG: TBCDField;
    T_GYUJTOMINDKS_FIMOD: TStringField;
    T_GYUJTOMINDKS_TETAN: TIntegerField;
    T_GYUJTOMINDKS_TIPUS: TIntegerField;
    T_GYUJTOMINDKS_THELY: TStringField;
    T_GYUJTOMINDKS_TELES: TStringField;
    T_GYUJTOMINDKS_JARAT: TStringField;
    T_GYUJTOMINDKS_SOKOD: TStringField;
    T_GYUJTOMINDKS_SONEV: TStringField;
    T_GYUJTOMINDKS_ORSZA: TStringField;
    T_GYUJTOMINDKS_IDO: TStringField;
    T_GYUJTOMINDKS_DATI: TDateTimeField;
    T_GYUJTOMINDKS_KTIP: TIntegerField;
    T_GYUJTOMINDKS_HMEGJ: TStringField;
    T_GYUJTOMINDKS_FELDAT: TDateTimeField;
    T_GYUJTOMINDKS_SOR: TStringField;
    T_GYUJTOMINDKS_GKKAT: TStringField;
    Query3VI_ARFDAT: TStringField;
    Panel21: TPanel;
    Panel22: TPanel;
    BitBtn51: TBitBtn;
    BitBtn52: TBitBtn;
    Memo1: TMemo;
    DBGrid8: TDBGrid;
    QSeged: TADOQuery;
    Panel23: TPanel;
    DataSourceSeged: TDataSource;
    BitBtn55: TBitBtn;
    BitBtn56: TBitBtn;
    BitBtn53: TBitBtn;
    Query5: TADOQuery;
    BitBtn54: TBitBtn;
    BitBtn57: TBitBtn;
    CheckBox3: TCheckBox;
    CheckBox5: TCheckBox;
    BitBtn58: TBitBtn;
    BitBtn59: TBitBtn;
    BitBtn60: TBitBtn;
    CheckBox6: TCheckBox;
    BitBtn541: TBitBtn;
    DBGrid9: TDBGrid;
    DBGrid10: TDBGrid;
    Query12: TADOQuery;
    IntegerField1: TIntegerField;
    IntegerField2: TIntegerField;
    StringField1: TStringField;
    StringField2: TStringField;
    StringField3: TStringField;
    StringField4: TStringField;
    StringField5: TStringField;
    FloatField1: TFloatField;
    StringField6: TStringField;
    FloatField2: TFloatField;
    FloatField3: TFloatField;
    StringField7: TStringField;
    FloatField4: TFloatField;
    StringField8: TStringField;
    Query12VISZONY: TStringField;
    IntegerField3: TIntegerField;
    IntegerField4: TIntegerField;
    DataSource3: TDataSource;
    DataSource5: TDataSource;
    Query14: TADOQuery;
    Panel24: TPanel;
    BitBtn61: TBitBtn;
    BitBtn62: TBitBtn;
    Query14JK_JAKOD: TStringField;
    Query14JK_CSJAR: TStringField;
    Query14JA_KOD: TStringField;
    Query14JA_JARAT: TStringField;
    Query14JA_JKEZD: TStringField;
    Query14JA_JVEGE: TStringField;
    Query14JA_RENDSZ: TStringField;
    Query14JA_SAJAT: TIntegerField;
    Query14JA_MEGJ: TStringField;
    Query14JA_MENSZ: TStringField;
    Query14JA_NYITKM: TBCDField;
    Query14JA_ZAROKM: TBCDField;
    Query14JA_BELKM: TBCDField;
    Query14JA_ALKOD: TBCDField;
    Query14JA_FAZIS: TBCDField;
    Query14JA_OSSZKM: TBCDField;
    Query14JA_ALVAL: TIntegerField;
    Query14JA_ORSZ: TStringField;
    Query14JA_SOFK1: TStringField;
    Query14JA_SOFK2: TStringField;
    Query14JA_SNEV1: TStringField;
    Query14JA_SNEV2: TStringField;
    Query14JA_JPOTK: TStringField;
    Query14JA_FAZST: TStringField;
    Query14JA_KEIDO: TStringField;
    Query14JA_VEIDO: TStringField;
    Query14JA_LFDAT: TStringField;
    Query14JA_SZAK1: TBCDField;
    Query14JA_SZAK2: TBCDField;
    Query14JA_URESK: TStringField;
    Query14JA_EUROS: TStringField;
    Query14JA_UBKM: TStringField;
    Query14JA_UKKM: TStringField;
    Query14JA_JAORA: TIntegerField;
    Query14JA_FAJKO: TIntegerField;
    Query14JA_FOJAR: TStringField;
    Query14JA_MSFAJ: TStringField;
    Query14JA_MSDB: TIntegerField;
    jaratkod: TMaskEdit;
    jaratszam: TMaskEdit;
    BitBtn63: TBitBtn;
    Query8JS_CSEHTRAN: TIntegerField;
    BitBtn64: TBitBtn;
    cbFAJKO: TComboBox;
    cbM1: TComboBox;
    Query3VI_SZLAVAL: TStringField;
	procedure BitKilepClick(Sender: TObject);
	procedure FormCreate(Sender: TObject);
	procedure Tolto(cim : string; teko : string); override;
	procedure BitElkuldClick(Sender: TObject);
  procedure MaskEditClick(Sender: TObject);
	 procedure Modosit(Sender: TObject);
	 procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Idokilep(Sender: TObject);
	 procedure M7Change(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure M14Exit(Sender: TObject);
    procedure M14KeyPress(Sender: TObject; var Key: Char);
	 procedure BitBtn3Click(Sender: TObject);
	 procedure BitBtn4Click(Sender: TObject);
	 procedure BitBtn5Click(Sender: TObject);
	 procedure BitBtn7Click(Sender: TObject);
    procedure BitBtn8Click(Sender: TObject);
	 procedure BitBtn11Click(Sender: TObject);
	 procedure BitBtn12Click(Sender: TObject);
    procedure BitBtn13Click(Sender: TObject);
	 procedure BitBtn14Click(Sender: TObject);
	 procedure BitBtn15Click(Sender: TObject);
	 procedure M24Exit(Sender: TObject);
	 procedure M26KeyPress(Sender: TObject; var Key: Char);
	 procedure SzamClick(Sender: TObject);
	 procedure M02KeyPress(Sender: TObject; var Key: Char);
    procedure M02Exit(Sender: TObject);
	 procedure FormShow(Sender: TObject);
	 procedure BitBtn19Click(Sender: TObject);
    procedure GombEllenor;
	 procedure M13Exit(Sender: TObject);
	 procedure BitBtn21Click(Sender: TObject);
    procedure BitBtn22Click(Sender: TObject);
	 procedure BitBtn23Click(Sender: TObject);
	 procedure BitBtn24Click(Sender: TObject);
	 procedure BitBtn25Click(Sender: TObject);
    procedure BitBtn29Click(Sender: TObject);
	 procedure FormDestroy(Sender: TObject);
    procedure BitBtn27Click(Sender: TObject);
    procedure BitBtn28Click(Sender: TObject);
    procedure BitBtn33Click(Sender: TObject);
	 procedure ButValVevoClick(Sender: TObject);
	 procedure MaskEdit10Exit(Sender: TObject);
    procedure MaskEdit10KeyPress(Sender: TObject; var Key: Char);
    procedure BitBtn34Click(Sender: TObject);
	 procedure M13AExit(Sender: TObject);
	 procedure M13AKeyPress(Sender: TObject; var Key: Char);
	 procedure BitKeresClick(Sender: TObject);
	 procedure MaskEdit4KeyDown(Sender: TObject; var Key: Word;
	   Shift: TShiftState);
	 procedure BitBtn38Click(Sender: TObject);
	 procedure BitBtn20Click(Sender: TObject);
	 procedure BitBtn35Click(Sender: TObject);
	 procedure BitBtn18Click(Sender: TObject);
	 procedure BitBtn39Click(Sender: TObject);
	 procedure BitBtn26Click(Sender: TObject);
	 procedure Query8CalcFields(DataSet: TDataSet);
	 procedure FormCanResize(Sender: TObject; var NewWidth,
	   NewHeight: Integer; var Resize: Boolean);
	 procedure FormResize(Sender: TObject);
	 procedure FormClose(Sender: TObject; var Action: TCloseAction);
	 function  Kmellenor : boolean;
	 procedure CBFelelosChange(Sender: TObject);
	 procedure ToltMegbizas;
	 procedure KmHozzaadas(Mresz, Mkmora : TMaskEdit );
	 procedure BitBtn2Click(Sender: TObject);
	 procedure ViszonyEllenor(Mind:Boolean);
	 function KoltsegEllenor : boolean;
	 procedure TabbedNotebook1Change(Sender: TObject; NewTab: Integer;
	   var AllowChange: Boolean);
	 procedure BitBtn6Click(Sender: TObject);
	 procedure BitBtn9Click(Sender: TObject);
	 procedure RadioButton1Click(Sender: TObject);
	 procedure Button1Click(Sender: TObject);
	 procedure Szerkesztheto(irhato : boolean);
	 procedure MaskEdit5Change(Sender: TObject);
	 procedure CBOrszChange(Sender: TObject);
	 procedure Query4CalcFields(DataSet: TDataSet);
	 procedure QueryCMRCalcFields(DataSet: TDataSet);
	 procedure BitBtn10Click(Sender: TObject);
	 procedure GepkocsiOlvaso;
	 procedure BitBtn16Click(Sender: TObject);
	 procedure BitBtn17Click(Sender: TObject);
	 procedure BitBtn36Click(Sender: TObject);
	 procedure BitBtn37Click(Sender: TObject);
	 procedure M4KeyDown(Sender: TObject; var Key: Word;
	   Shift: TShiftState);
	 procedure M27KeyDown(Sender: TObject; var Key: Word;
	   Shift: TShiftState);
	 procedure SpeedButton4Click(Sender: TObject);
	 procedure SpeedButton5Click(Sender: TObject);
	 procedure BitBtn40Click(Sender: TObject);
	 procedure BitBtn41Click(Sender: TObject);
	 procedure BitBtn42Click(Sender: TObject);
	 procedure IdoEx(Sender: TObject);
	 procedure MaskEdit4Exit(Sender: TObject);
	 procedure BitBtn43Click(Sender: TObject);
	 procedure BitBtn44Click(Sender: TObject);
	 procedure BitBtn45Click(Sender: TObject);
	 procedure BitBtn46Click(Sender: TObject);
	 procedure M2BKeyDown(Sender: TObject; var Key: Word;
	   Shift: TShiftState);
	 procedure BitBtn47Click(Sender: TObject);
	 procedure FelrakoTolto(sorkod : string);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton6Click(Sender: TObject);
    procedure BitBtn31Click(Sender: TObject);
    procedure BitBtn48Click(Sender: TObject);
    procedure M28Exit(Sender: TObject);
    procedure M28KeyPress(Sender: TObject; var Key: Char);
    procedure M3AKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure BitBtn30Click(Sender: TObject);
    procedure Query3AfterScroll(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure BitBtn32Click(Sender: TObject);
    procedure BitBtn49Click(Sender: TObject);
    procedure M3AExit(Sender: TObject);
    procedure M2AExit(Sender: TObject);
    procedure M2Change(Sender: TObject);
    // procedure RG_FAJKOClick(Sender: TObject);
    // procedure BitBtn50Click(Sender: TObject);
    // function Koltsegimport(kelluzenet: boolean = True): integer;
    procedure DBGrid6DblClick(Sender: TObject);
    procedure BitBtn51Click(Sender: TObject);
    procedure BitBtn52Click(Sender: TObject);
    procedure BitBtn55Click(Sender: TObject);
    procedure BitBtn56Click(Sender: TObject);
    procedure M3Change(Sender: TObject);
    procedure M3AChange(Sender: TObject);
    procedure M2AChange(Sender: TObject);
    procedure M2Exit(Sender: TObject);
    function  SoforEllenor : boolean;
    procedure BitBtn53Click(Sender: TObject);
    procedure BitBtn57Click(Sender: TObject);
    procedure BitBtn54Click(Sender: TObject);
    procedure CheckBox5Click(Sender: TObject);
    procedure CheckBox3Click(Sender: TObject);
    procedure BitBtn58Click(Sender: TObject);
    procedure BitBtn59Click(Sender: TObject);
    procedure BitBtn60Click(Sender: TObject);
    procedure GombAllapot(ga : boolean);
    procedure Query14AfterScroll(DataSet: TDataSet);
    procedure Query12CalcFields(DataSet: TDataSet);
    procedure BitBtn61Click(Sender: TObject);
    procedure BitBtn62Click(Sender: TObject);
    procedure BitBtn63Click(Sender: TObject);
    procedure NyelvFrissit;
    function FAJKO_ellenor: boolean;
    procedure BitBtn64Click(Sender: TObject);
    procedure cbFAJKOChange(Sender: TObject);
    procedure FAJKO_valtozas;
	private
		modosult 			: boolean;
		ujrekord			: boolean;
		vnemlist			: TStringList;
		feleloslist			: TStringList;
		felelos_fekodlist: TStringList;
		f1, f2, f3, f4, f5	: string;
		l1, l2, l3, l4, l5	: string;
		fizu1, fizu2, fizu3	: string;
		fisz1, fisz2, fisz3	: string;
		elkuldes			: boolean;
		alval_jarat			: boolean;
		listaorsz			: TStringList;
		emailcim			: string;
		alvalkod			: string;
       jaratfajta          : string;
    FAX: string;
    STORNO, STORNOKULDVE: boolean;
    fajko: integer;
    msmskod, msmssorsz: integer;
    kilepes             : boolean;
    evstr , fojarat              : string;
    UNYELV: string;  // az alv�llalkoz� "�gyint�z�s nyelve"
    Elindult: boolean;  //  False by default
  public
    elszamoltjarat: boolean;
    GKKATEG: string;
	 function 	Mentes : boolean;
   function NemElszamoltJarat(rendsz: string): integer;
   function Viszony_Szamla_Vevoell(uzenet: boolean): boolean;
   procedure Idotartam;
   function Megbizas_db : integer;
  end;

var
	JaratbeDlg: TJaratbeDlg;

implementation

uses
 SzamlaBe, Koltsegbe, Sulybe, J_SQL, Kozos, Egyeb, J_VALASZTO,
 AlvallalUjFm, Mellek, Rendval, ViszonyBe, JaratFm ,
 Fumegli, Megjmemo, Jarlis, Valgepk, Potkocsibe, Soforbe, Megbizasujfm,
 Osszlis, Ossznyil, CMRBe, CMRFm, AlGepFm, Fizube, SzamlaNezo, DoksiLIsta,
 Megbizasbe, Kozos_Local, J_Fogyaszt, Csatolmanyfm, Segedbe2 , XML_Export,
  Windows,Uzema, DateUtils,VevoUjfm, {KoltsegImport, } Megbizasfm, StrUtils;

{$R *.DFM}

procedure TJaratbeDlg.BitKilepClick(Sender: TObject);
begin
	ret_kod := '';
	Close;
end;

procedure TJaratbeDlg.FormCreate(Sender: TObject);
var
	sorsz, i	: integer;
  fn		: string;
begin
	SetMaskEdits([M28]);
	SetMaskEdits([M3B]);

 // CheckBox6.Checked:=EgyebDlg.DATUMIDOELL;

	EgyebDLg.SetADOQueryDatabase(Query1);
	EgyebDLg.SetADOQueryDatabase(Query2);
	EgyebDLg.SetADOQueryDatabase(QueryUj3);
	EgyebDLg.SetADOQueryDatabase(Query3);
	EgyebDLg.SetADOQueryDatabase(Query4);
	EgyebDLg.SetADOQueryDatabase(Query6);
	EgyebDLg.SetADOQueryDatabase(Query7);
	EgyebDLg.SetADOQueryDatabase(Query8);
	EgyebDLg.SetADOQueryDatabase(QueryKOZ);
	EgyebDLg.SetADOQueryDatabase(QueryFelrako);
	EgyebDLg.SetADOQueryDatabase(QueryFelrako2);
	EgyebDLg.SetADOQueryDatabase(QueryCMR);
	EgyebDlg.SetADOQueryDatabase(Query11);
	EgyebDlg.SetADOQueryDatabase(Query13);
	EgyebDlg.SetADOQueryDatabase(QSeged);
	EgyebDLg.SetADOQueryDatabase(Query12);
	EgyebDLg.SetADOQueryDatabase(Query14);
	SetMaskEdits([MaskEdit1]);
	ret_kod 				  	:= '';
   evstr                       := '';
	modosult 				  	:= false;
	elkuldes					:= false;
	alval_jarat					:= false;
   jaratfajta                  := ' - norm�l';
	TabbedNotebook1.PageIndex	:= 0;
  UNYELV:='HUN';   // alap�rtelmezett: magyar
	{A valutanem t�bla felt�lt�se a k�dokkal}
	vnemlist := TStringList.Create;
	SzotarTolt(CVnem, '110' , vnemlist, false, false );
	CVnem.Items.Clear;
	for sorsz := 0 to vnemlist.Count - 1 do begin
		CVnem.Items.Add(vnemlist[sorsz]);
	end;
	CVnem.ItemIndex	:= CVNem.Items.IndexOf('EUR');
  	Me1.Height	:= 0;
  	Me2.Height	:= 0;
  	Me3.Height	:= 0;
  	TabbedNotebook2.PageIndex	:= 0;
  	{A memo mez�k beolvas�sa}

	fn	:= EgyebDlg.DocumentPath+'MEMO1.TXT';
  	if FileExists(fn) then begin
  		Me1.Lines.LoadFromFile(fn);
  	end;
	fn	:= EgyebDlg.DocumentPath+'MEMO2.TXT';
  	if FileExists(fn) then begin
  		Me2.Lines.LoadFromFile(fn);
	end;
	fn	:= EgyebDlg.DocumentPath+'MEMO3.TXT';
	if FileExists(fn) then begin
		Me3.Lines.LoadFromFile(fn);
	end;
	{A fejsorok + l�bsorok beolvas�sa}
	f1	:= EgyebDlg.Read_SZGrid('999', '690' );
	f2	:= EgyebDlg.Read_SZGrid('999', '691' );
	f3	:= EgyebDlg.Read_SZGrid('999', '692' );
	f4	:= EgyebDlg.Read_SZGrid('999', '693' );
	f5	:= EgyebDlg.Read_SZGrid('999', '694' );
	// l1	:= EgyebDlg.Read_SZGrid('999', '695' );  // NyelvFrissit -ben
	l2	:= EgyebDlg.Read_SZGrid('999', '696' );
	l3	:= EgyebDlg.Read_SZGrid('999', '697' );
	l4	:= EgyebDlg.Read_SZGrid('999', '698' );
	l5	:= EgyebDlg.Read_SZGrid('999', '699' );
	l2	:= l2 + EgyebDlg.MaiDatum;

	// A fizet�si felt�telek beolvas�sa
	  //fizu1 := EgyebDlg.Read_SZGrid('999', '730');  // NyelvFrissit -ben
	  //fizu2	:= EgyebDlg.Read_SZGrid('999', '732');  // NyelvFrissit -ben
  NyelvFrissit;
	fizu3	:= EgyebDlg.Read_SZGrid('999', '734');
	fisz1	:= EgyebDlg.Read_SZGrid('999', '731');
	fisz2	:= EgyebDlg.Read_SZGrid('999', '733');
	fisz3	:= EgyebDlg.Read_SZGrid('999', '735');

	MaskEdit11.Text	:= EgyebDlg.Read_SZGrid('999', '700' );
	MaskEdit12.Text	:= EgyebDlg.Read_SZGrid('999', '701' );
	// A felel�s�k bet�lt�se
	feleloslist := TStringList.Create;
  felelos_fekodlist:= TStringList.Create;
	CBFelelos.Clear;
	Query_Run( QueryKoz, 'SELECT * FROM SZOTAR WHERE SZ_FOKOD = ''330'' AND SZ_ALKOD LIKE ''%0'' AND SZ_ERVNY > 0 ', true );
	while not QueryKoz.Eof do begin
		feleloslist.Add(QueryKoz.FieldByName('SZ_ALKOD').AsString);
    felelos_fekodlist.Add(QueryKoz.FieldByName('SZ_EGYEB1').AsString); // felhaszn�l� k�d
		CBFelelos.Items.Add(QueryKoz.FieldByName('SZ_MENEV').AsString);
		QueryKoz.Next;
	end;
	if QueryKoz.RecordCount > 0 then begin
    for i := 0 to CBfelelos.Items.Count - 1 do begin
			if felelos_fekodlist[i] = EgyebDlg.user_code then begin  // k�d alapj�n szeretj�k
				CBfelelos.ItemIndex	:= i;
				CBFelelosChange(Sender);
			  end;  // if
		  end;  // for
    if CBFelelos.ItemIndex= -1 then
        CBFelelos.ItemIndex:= 0;  // az els�
		CBFelelosChange(Sender);
	end;
	listaorsz	:= TStringList.Create;
	SzotarTolt(CBOrsz, '300' , listaorsz, true, true );
	SzotarTolt_CsakLista(cbM1, '151');
  Elindult:= True;  // az ellen�rz�sek ne fussanak FormCreate-en bel�l
end;

procedure TJaratbeDlg.NyelvFrissit;
var
  szokod: string;
begin
  if UNYELV='' then  UNYELV:='HUN';  // haladjunk
  fizu1 := EgyebDlg.Read_SZGrid('999', UNYELV+'730');  // p�ld�nyv�ltoz� alapj�n
  fizu2	:= EgyebDlg.Read_SZGrid('999', UNYELV+'732');
	l1	:= EgyebDlg.Read_SZGrid('999', UNYELV+'695');
  MM1.Text := EgyebDlg.Read_SZGrid('999', UNYELV+'420');

  if CBFelelos.ItemIndex >= 0 then begin  // van valaki kiv�lasztva
    szokod	:= feleloslist[CBFelelos.ItemIndex];
    l1		:= EgyebDlg.Read_SZGrid('330', UNYELV + szokod + 'L1');
    l2		:= EgyebDlg.Read_SZGrid('330', UNYELV + szokod + 'L2');
    // l3		:= EgyebDlg.Read_SZGrid('330', UNYELV + szokod + 'L3');
    l3		:= EgyebDlg.Read_SZGrid('330', UNYELV + szokod + 'L3')+' '+EgyebDlg.MaiDatum;
    l4		:= EgyebDlg.Read_SZGrid('330', UNYELV + szokod + 'L4');
    l5		:= EgyebDlg.Read_SZGrid('330', UNYELV + szokod + 'L5');
    end;
end;


procedure TJaratbeDlg.Tolto(cim : string; teko:string);
var
	st 			: string;
	vanem		: integer;
	vanbox		: boolean;
begin
	EgyebDlg.torles_jakod	:= '';
	vanbox			:= true;
	ret_kod 		:= teko;
	Caption 		:= cim;
	M01.Text 		:= '';
	M02.Text 		:= '';
	cbM1.Text 		:= '';
	M3.Text 		:= '';
	M4.Text 		:= '';
	M12.Text		:= '';
	M13.Text		:= '';
	M14.Text		:= '';
	M15.Text		:= '';
	ujrekord		:= false;
   M2.Text         := EgyebDlg.MaiDatum;
   evstr           := copy(M2.Text, 1, 4);
   fojarat:='';

	if ret_kod <> '' then begin		{M�dos�t�s}
		if Query_Run (Query1, 'SELECT * FROM JARAT WHERE JA_KOD = '''+teko+''' ',true) then begin
			{J�ratsz�m elemek felt�lt�se �s nem edit�lhat�v� t�tele}
			fojarat:= Query1.FieldByName('JA_FOJAR').AsString;
			M01.Text 	:= Query1.FieldByName('JA_ORSZ').AsString;
			M02.Text 	:= Format('%5.5d',[Query1.FieldByName('JA_ALKOD').AsInteger]);
			ComboSet (CBOrsz, Query1.FieldByName('JA_ORSZ').AsString, listaorsz);
			SetMaskEdits([M02]);
			cbM1.Text  	:= Query1.FieldByName('JA_JARAT').AsString;
			M20.Text 	:= Query1.FieldByName('JA_KOD').AsString;
{			if Query1.FieldByName('JA_ALVAL').AsBoolean then begin }
			{Az alv�llalkoz�i adatokat kell megjelen�teni}
			if Query_Run (Query2, 'SELECT * FROM ALJARAT WHERE AJ_JAKOD = '''+teko+''' ',true) then begin
				MaskEdit4.Text	:= Query2.FieldByName('AJ_ALNEV').AsString;
				// Az emailc�m beolvas�sa
        if EgyebDlg.v_evszam<'2013' then begin
  				emailcim		:= Query_Select('ALVALLAL', 'V_NEV', MaskEdit4.Text,  'V_EMAIL');
	  			alvalkod		:= Query_Select('ALVALLAL', 'V_NEV', MaskEdit4.Text,  'V_KOD');
          end
        else begin
  				alvalkod  	:= Query2.FieldByName('AJ_V_KOD').AsString;
  				emailcim		:= Query_Select('VEVO', 'V_KOD', alvalkod,  'VE_EMAIL');
          // --- a nyelvf�gg� adattartalomhoz -------------------------------
          UNYELV := Query_Select('VEVO', 'V_KOD', alvalkod,  'VE_UNYELV');
          if UNYELV='' then  UNYELV:= Query_Select('VEVO', 'V_KOD', alvalkod,  'VE_NYELV');  // sz�ml�z�si nyelv
          // ----------------------------------------------------------------
          if alvalkod='' then begin
    				emailcim		:= Query_Select('VEVO', 'V_NEV', MaskEdit4.Text,  'VE_EMAIL');
  	  			alvalkod		:= Query_Select('VEVO', 'V_NEV', MaskEdit4.Text,  'V_KOD');
            end;
          end;
        // itt nem kell: m�r van ALJARAT adatsor, abban benne van a sz�veg
        // NyelvFrissit; // nyelvf�gg� adattartalom
				MaskEdit6.Text	:= Query2.FieldByName('AJ_ALCIM').AsString;
				MaskEdit7.Text	:= Query2.FieldByName('AJ_ALTEL').AsString;
				MaskEdit8.Text	:= Query2.FieldByName('AJ_ALFAX').AsString;
				MaskEdit5.Text	:= Query2.FieldByName('AJ_ALREN').AsString;
				MaskEdit50.Text	:= Query2.FieldByName('AJ_POTOS').AsString;
				MaskEdit9.Text	:= Query2.FieldByName('AJ_ALTIP').AsString;
				MaskEdit10.Text:= SzamString(StringSzam(Query2.FieldByName('AJ_FUDIJ').AsString),10,2);
				vanem	:= vnemlist.IndexOf(Query2.FieldByName('AJ_FUVAL').AsString);
				if vanem < 0 then begin
					vanem	:= 0;
				end;
				CVnem.ItemIndex		:= vanem;
				MaskEdit11.Text		:= Query2.FieldByName('AJ_POMEG').AsString;
				MaskEdit12.Text		:= Query2.FieldByName('AJ_FUMEG').AsString;
//				MaskEdit3.Text		:= Query2.FieldByName('AJ_MEGJE').AsString;
				MM1.Text			:= Query2.FieldByName('AJ_MEMO01').AsString;
				MM2.Text			:= Query2.FieldByName('AJ_MEMO02').AsString;
				MM3.Text			:= Query2.FieldByName('AJ_MEMO03').AsString;
				MM4.Text			:= Query2.FieldByName('AJ_MEMO04').AsString;
				MM5.Text			:= Query2.FieldByName('AJ_MEMO05').AsString;
				{A fejl�cek, l�bl�cek, memo -k bet�lt�se}
				f1					:= Query2.FieldByName('AJ_FEJS1').AsString;
				f2					:= Query2.FieldByName('AJ_FEJS2').AsString;
				f3					:= Query2.FieldByName('AJ_FEJS3').AsString;
				f4					:= Query2.FieldByName('AJ_FEJS4').AsString;
				f5					:= Query2.FieldByName('AJ_FEJS5').AsString;
				l1					:= Query2.FieldByName('AJ_LABS1').AsString;
				l2					:= Query2.FieldByName('AJ_LABS2').AsString;
				l3					:= Query2.FieldByName('AJ_LABS3').AsString;
				l4					:= Query2.FieldByName('AJ_LABS4').AsString;
				l5					:= Query2.FieldByName('AJ_LABS5').AsString;
				// A fizet�si felt�telek beolvas�sa
				fizu1				:= Query2.FieldByName('AJ_FIZU1').AsString;
				fizu2				:= Query2.FieldByName('AJ_FIZU2').AsString;
				fizu3				:= Query2.FieldByName('AJ_FIZU3').AsString;
				fisz1				:= Query2.FieldByName('AJ_FISZ1').AsString;
				fisz2				:= Query2.FieldByName('AJ_FISZ2').AsString;
				fisz3				:= Query2.FieldByName('AJ_FISZ3').AsString;
				{A memo �llom�nyok beolvas�sa}
				Me1.Clear;
				Me1.Lines.Add(Query2.FieldByName('AJ_MEMO11').AsString);
				Me1.Lines.Add(Query2.FieldByName('AJ_MEMO12').AsString);
				Me1.Lines.Add(Query2.FieldByName('AJ_MEMO13').AsString);
				Me1.Lines.Add(Query2.FieldByName('AJ_MEMO14').AsString);
				Me1.Lines.Add(Query2.FieldByName('AJ_MEMO15').AsString);
				Me1.Lines.Add(Query2.FieldByName('AJ_MEMO16').AsString);
				Me2.Clear;
				Me2.Lines.Add(Query2.FieldByName('AJ_MEMO21').AsString);
				Me2.Lines.Add(Query2.FieldByName('AJ_MEMO22').AsString);
				Me2.Lines.Add(Query2.FieldByName('AJ_MEMO23').AsString);
				Me3.Clear;
				Me3.Lines.Add(Query2.FieldByName('AJ_MEMO31').AsString);
				Me3.Lines.Add(Query2.FieldByName('AJ_MEMO32').AsString);
				Me3.Lines.Add(Query2.FieldByName('AJ_MEMO33').AsString);
				// A felel�s be�ll�t�sa
				ComboSet (CBFelelos, Query2.FieldByName('AJ_FEKOD').AsString, feleloslist);
			end;
{    	end else begin }
           evstr           := copy(Query1.FieldByName('JA_JKEZD').AsString, 1, 4);
			M2.Text 		:= Query1.FieldByName('JA_JKEZD').AsString;
			M2A.Text 		:= Query1.FieldByName('JA_KEIDO').AsString;
			M3.Text 		:= Query1.FieldByName('JA_JVEGE').AsString;
			M3A.Text 		:= Query1.FieldByName('JA_VEIDO').AsString;
           jaratfajta      := ' - '+Query1.FieldByName('JA_MSFAJ').AsString;
			if (Query1.FieldByName('JA_JAORA').AsInteger > 0)and(M3A.Text<>'') then
  			M3B.Text 		:= Query1.FieldByName('JA_JAORA').AsString
      else
        M3B.Text:='';
			M2B.Text 		:= Query1.FieldByName('JA_LFDAT').AsString;
			M4.Text 		:= Query1.FieldByName('JA_RENDSZ').AsString;
			{Elt�lt�tt id� kisz�m�t�sa}
			M12.Text 		:= Query1.FieldByName('JA_MEGJ').AsString;
			M13.Text 		:= Query1.FieldByName('JA_MENSZ').AsString;
			M14.Text 		:= Query1.FieldByName('JA_NYITKM').AsString;
			M15.Text 		:= Query1.FieldByName('JA_ZAROKM').AsString;
			M27.Text 		:= Query1.FieldByName('JA_BELKM').AsString;
			M28.Text 		:= Query1.FieldByName('JA_URESK').AsString;
			M29.Text 		:= Query1.FieldByName('JA_UBKM').AsString;
			M30.Text 		:= Query1.FieldByName('JA_UKKM').AsString;
			RG1.ItemIndex 	:= StrToIntDef(Query1.FieldByName('JA_EUROS').AsString, 0);
      fajko:=StrToIntDef(Query1.FieldByName('JA_FAJKO').AsString, 0) ;
      // if fajko<>0 then fajko:=1;  2016-12-08
      cbFAJKO.ItemIndex:=fajko ;
      FAJKO_valtozas;  // friss�ti a c�mk�t jobbra fent

			CheckBox4.Checked	:= ( Query1.FieldByName('JA_FAZIS').AsInteger = -1);
      elszamoltjarat:=CheckBox4.Checked;
      CBOrsz.Enabled:= Query1.FieldByName('JA_FAZIS').AsInteger<1;
			if ( ( StrToIntDef(Query1.FieldByName('JA_FAZIS').AsString, 0) = -2 ) or ( StrToIntDef(Query1.FieldByName('JA_FAZIS').AsString, 0) = 2 ) ) then begin
				CheckBox4.Caption   := GetJaratFazis( StrToIntDef(Query1.FieldByName('JA_FAZIS').AsString, 0) );
				CheckBox4.Checked	:= true;
        elszamoltjarat:=CheckBox4.Checked;
				CheckBox4.Enabled	:= false;
				vanbox				:= false;
			end;
			// Le is tiltjuk a m�sik f�let
			MaskEdit1.Text := Query1.FieldByName('JA_OSSZKM').AsString;
			TabbedNotebook2.PageIndex	:= 0;
			alval_jarat := false;
			if MaskEdit4.Text <> '' then begin
				TabbedNotebook2.PageIndex	:= 1;
				alval_jarat := true;
			end;
      BitBtn32.Visible:=alval_jarat;
			Panel17.Hide;
			Panel18.Show;
			if alval_jarat then begin
				Panel18.Caption	:= 'Alv�llalkoz�i megb�z�s';
			end else begin
				Panel18.Caption	:= 'Saj�t j�rat';
			end;
			Szerkesztheto(true);
      STORNO:=	Query1.FieldByName('JA_FAZIS').AsInteger=2;
      Label6.Visible:=STORNO;


		if megtekint then begin
        	{Ellen�rz�tt j�rat eset�n csak megtekint�s van}
			// CMR adatok r�gz�t�se
			BitBtn10.Enabled		:= false;
			BitBtn16.Enabled		:= false;
			BitBtn17.Enabled		:= false;
			BitBtn36.Enabled		:= false;
			// S�ly adatok r�gz�t�se
           BitBtn23.Enabled		:= false;
           BitBtn24.Enabled		:= false;
           BitBtn25.Enabled		:= false;
           // K�lts�g adatok r�gz�t�se
           BitBtn13.Enabled		:= false;
			BitBtn14.Enabled		:= false;
			BitBtn15.Enabled		:= false;
			BitBtn19.Enabled		:= false;
           // Megb�z�sok r�gz�t�se
           BitBtn3.Enabled			:= false;
			BitBtn4.Enabled			:= false;
			BitBtn5.Enabled			:= false;
			BitBtn34.Enabled		:= false;
			BitBtn22.Enabled		:= false;
		 //	BitBtn2.Enabled			:= false;
			BitBtn43.Enabled		:= false;
			BitBtn37.Enabled		:= false;
			// A j�rat adatok r�gz�t�se
			M01.Enabled				:= false;
			M02.Enabled				:= false;
			CBOrsz.Enabled			:= false;
			cbM1.Enabled				:= false;
			CheckBox4.Enabled		:= false;
			M2.Enabled				:= false;
			M2A.Enabled				:= false;
			M3.Enabled				:= false;
			M3A.Enabled				:= false;
			M2B.Enabled				:= false;
			M4.Enabled				:= false;
			M13.Enabled				:= false;
      BitBtn53.Enabled  := false;
			M14.Enabled				:= false;
			M15.Enabled				:= false;
			M27.Enabled				:= false;
			M28.Enabled				:= false;
			M29.Enabled				:= false;
			M30.Enabled				:= false;
			Maskedit1.Enabled		:= false;
			CheckBox1.Enabled		:= false;
			RG1.Enabled				:= false;
			BitBtn7.Enabled			:= false;
			BitBtn1.Enabled			:= false;
			BitBtn8.Enabled			:= false;
			BitBtn18.Enabled		:= false;
			BitBtn26.Enabled		:= false;
			BitBtn39.Enabled		:= false;
			BitBtn20.Enabled 		:= false;
			BitBtn35.Enabled 		:= false;
			BitBtn38.Enabled 		:= false;
			BitBtn44.Enabled		:= false;
		//	BitBtn2.Enabled			:= false;
			BitBtn32.Enabled 		:= false;

			M12.Enabled				:= false;

			// Alv�llalkoz�i adatok r�gz�t�se
			GroupBox2.Enabled		:= false;
			GroupBox3.Enabled		:= false;
			MaskEdit5.Enabled		:= false;
			MaskEdit50.Enabled		:= false;
			MaskEdit9.Enabled		:= false;
			MaskEdit10.Enabled		:= false;
			MaskEdit11.Enabled		:= false;
			MaskEdit12.Enabled		:= false;
           CVNem.Enabled			:= false;
//         MaskEdit3.Enabled		:= false;
			MM1.Enabled				:= false;
			MM2.Enabled				:= false;
			MM3.Enabled				:= false;
           MM4.Enabled				:= false;
           MM5.Enabled				:= false;
			BitBtn27.Enabled		:= false;
           BitBtn28.Enabled		:= false;
			BitBtn31.Enabled		:= false;
           BitBtn48.Enabled		:= false;
			BitBtn33.Enabled		:= false;
			ButValVevo.Enabled		:= false;

			// Nyom�gombok r�gz�t�se
			BitElkuld.Enabled		:= false;
           BitKilep.Show;
        end;
		end;
	end else begin
		{�j k�d gener�l�sa}
(*
		ujkodszam	:= 0;
		if Query_Run (Query1, 'SELECT MAX(JA_KOD) MAXERT FROM JARAT ',true) then begin
			ujkodszam := StrToIntDef(Query1.FieldByName('MAXERT').AsString,0) + 1;
		end;
		Query_Run (Query1, 'SELECT JA_KOD FROM JARAT WHERE JA_KOD = '''+Format('%5.5d',[ujkodszam])+''' ',true);
		while Query1.RecordCount > 0 do begin
			Inc(ujkodszam);
			Query_Run (Query1, 'SELECT JA_KOD FROM JARAT WHERE JA_KOD = '''+Format('%5.5d',[ujkodszam])+''' ',true);
		end;
*)
	  ret_kod					:= Format('%6.6d',[GetNextStrCode('JARAT', 'JA_KOD', 0, 6)]);
		M20.Text 				:= ret_kod;
		EgyebDlg.torles_jakod	:= ret_kod;
		{Az �j j�rat l�trehoz�sa}
		Query_Run ( Query11, 'INSERT INTO JARAT (JA_KOD) VALUES ('''+ret_kod+''' )',true);
		ujrekord		:= true;
		{�j j�ratsz�m k�d gener�l�sa}
(*
		if Query_Run (Query1, 'SELECT MAX(JA_ALKOD) MAXERT FROM JARAT ',true) then begin
			ujkodszam 	:= StrToIntDef(Query1.FieldByName('MAXERT').AsString,0) + 1;
		end;
*)
//       ujkodszam 	    := GetJaratAlkod(evstr);
//		M02.Text		:= Format('%5.5d',[ujkodszam]);
		Panel17.Show;
		Panel18.Hide;
		RadioButton1Click(Self);
		Szerkesztheto(false);
		if EgyebDlg.megbizaskod <> '' then begin
			ToltMegbizas;
			TabbedNotebook2.PageIndex	:= 0;
			alval_jarat := false;
			Panel18.Caption	:= 'Saj�t j�rat';
			if MaskEdit4.Text <> '' then begin
				TabbedNotebook2.PageIndex	:= 1;
				alval_jarat := true;
				Panel18.Caption	:= 'Alv�llalkoz�i megb�z�s';
			end;
			Panel17.Hide;
			Panel18.Show;
			Szerkesztheto(true);
		end;
	end;
	modosult 	:= false;

	{Felt�lti a Grid1 t�mb�t a megb�z�sokkal}
{ 	Query_Run(Query3, 'DELETE FROM VISZONY WHERE VI_JAKOD IS NULL ');}
	Query_Run(Query3, 'SELECT * FROM VISZONY WHERE VI_JAKOD = '''+M20.Text+''' ORDER BY VI_VIKOD',true);

	{Felt�lti a Grid2 t�mb�t a k�lts�gekkel}
	Query_Run(Query4, 'SELECT * FROM koltseg WHERE KS_JAKOD = '''+M20.Text+''' ORDER BY KS_DATUM, KS_KMORA, KS_TELE',true);

	{Felt�lti a Grid4 t�mb�t a k�lts�gekkel}
	Query_Run(Query6, 'SELECT * FROM sulyok WHERE SU_JAKOD = '''+M20.Text+''' ORDER BY SU_KMORA',true);

	{Felt�lti a felrak�kat/lerak�kat}
	FelrakoTolto('');

	{Felt�lti a Grid7 t�mb�t a CMR adatokkal}
	Query_Run(QueryCMR, 'SELECT * FROM CMRDAT WHERE CM_JAKOD = '''+M20.Text+''' ORDER BY CM_KIDAT, CM_VIDAT',true);

	// A p�tkocsik beolvas�sa
	Query_Run(Query7, 'SELECT * FROM JARPOTOS WHERE JP_JAKOD = '''+ret_kod+''' ORDER BY JP_POREN, JP_PORKM');

	// A dolgoz�k beolvas�sa
	Query_Run(Query8, 'SELECT * FROM JARSOFOR WHERE JS_JAKOD = '''+ret_kod+''' ORDER BY JS_SORSZ');

	// Az Elsz�molt checkbox megjelen�t�s�nek szab�lyoz�sa
	if vanbox then begin
		CheckBox4.Enabled := false;
		st	:= ','+EgyebDlg.Read_SZGrid('999','640')+','; {A user code olvas�sa}
		if ( ( Pos(','+EgyebDlg.user_code+',', st) > 0 ) and (not megtekint) ) then begin
			//CheckBox4.Enabled := true;
		end;
	end;

	// A k�lts�g �sszes�t�s gomb szab�lyoz�sa
	BitBtn6.Hide;
	if GetRightTag(340) <> RG_NORIGHT then begin
		BitBtn6.Show;
	end;

   Query_Run(QSeged,'SELECT * FROM JARATMEGSEGED, MEGSEGED WHERE JG_JAKOD = '''+ret_kod+''' AND JG_MSKOD = MS_MSKOD ');

   Query_Run(Query14,'SELECT * FROM JARATKOLTSEG, JARAT WHERE JK_JAKOD = '''+ret_kod+''' AND JA_KOD =JK_CSJAR');

   // RG_FAJKO.ItemIndex:=fajko ;  alap�rtelmezett: nincs kit�ltve

//	GombEllenor;
end;

procedure TJaratbeDlg.BitElkuldClick(Sender: TObject);
begin
   if M02.text = '-1' then begin
       NoticeKi('Nincs j�ratsz�m, kattints bele a "J�rat kezdete" mez�be!');
       Exit;
       end;
   if not SoforEllenor then begin
       Exit;
       end;

  if STORNO and not STORNOKULDVE then begin
    if MessageBox(0, 'A storno megb�z�s (lev�l) m�g nem lett elk�ldve!'+#13+#10+'Ennek ellen�re folytatja?', '', MB_ICONWARNING or MB_YESNO or MB_DEFBUTTON2)=IDNO then
		//NoticeKi('A storno megb�z�s (lev�l) m�g nem lett elk�ldve!');
      exit;
  end;
  if STORNO then
    BitBtn5.OnClick(self);

	if Mentes then begin
		elkuldes	:= true;
		Close;
	end;
end;

procedure TJaratbeDlg.MaskEditClick(Sender: TObject);
begin
	with Sender as TMaskEdit do begin
	   SelectAll;
  end;
end;

procedure TJaratbeDlg.Modosit(Sender: TObject);
begin
	modosult 	:= true;
end;

procedure TJaratbeDlg.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
	EgyebDlg.FormReturn(Key);
	if TabbedNotebook1.PageIndex = 2 then begin
		// A k�lts�gekn�l lehet telepi tankol�st gener�ltatni
		if ssCtrl in Shift then begin
   	  if Chr(Key) in ['T','t'] then begin
			//if Key = Key_T then begin
				if ( ( GetRightTag(566) = RG_NORIGHT	) and not EgyebDlg.user_super ) then begin
					NoticeKi('�n nem jogosult telepi tankol�sok l�trehoz�s�ra!');
					Exit;
				end;
				if alval_jarat then begin
					NoticeKi('Alv�llalkoz�i j�rathoz nem lehet telepi tankol�st rendelni!');
					Exit;
				end;
				if M4.Text = '' then begin
					NoticeKi('K�lts�gek megad�sa el�tt ki kell t�lteni a rendsz�mot!');
					TabbedNotebook1.PageIndex	:= 0;
					Exit;
				end;
				// Ellen�rizz�k, hogy ehhez aj�rathoz m�r van-e telepi tankol�s
				Query_Run(Query11, 'SELECT * FROM KOLTSEG WHERE KS_JAKOD = '''+M20.Text+''' AND KS_TETAN = 1 ');
				if Query11.RecordCount > 0 then begin
					if NoticeKi('Figyelem! Ehhez a j�rathoz m�r tartozik telepi tankol�s! Folytatja?', NOT_QUESTION) <> 0 then begin
						Exit;
					end;
				end;
				Application.CreateForm(TKoltsegbeDlg, KoltsegbeDlg);
				KoltsegbeDlg.Tolt2('Telepi tankol�s felvitele', '', M20.Text, M4.Text, M01.Text+'-'+M02.Text);
				KoltsegbeDlg.PageControl1.ActivePageIndex		:= 0;
				KoltsegbeDlg.PageControl1.Pages[1].TabVisible	:= false;
				KoltsegbeDlg.PageControl1.Pages[2].TabVisible	:= false;
				KoltsegbeDlg.PageControl1.Pages[3].TabVisible	:= false;
				KoltsegbeDlg.CheckBox2.Checked					:= true;
				KoltsegbeDlg.MaskEdit2.Text						:= Format('%9.0d',[Round(StringSzam(M15.Text)+StringSzam(M14.Text)) div 2]);
				KoltsegbeDlg.M2.Text							:= FormatDateTime('YYYY.MM.DD.', ( DateTimeToNum(M2.Text,'')+DateTimeToNum(M3.Text,'') ) / 2);
				KoltsegbeDlg.ShowModal;
				if KoltsegbeDlg.ret_kod <> '' then begin
					// Friss�t�s
					Query_Run(Query4, 'SELECT * FROM koltseg WHERE KS_JAKOD = '''+M20.Text+''' ORDER BY KS_DATUM',true);
					Query4.Locate('KS_KTKOD', KoltsegbeDlg.ret_kod, [] );
					GombEllenor;
				end;
				KoltsegbeDlg.Destroy;
			end;
		end;

	end;
end;

procedure TJaratbeDlg.Idokilep(Sender: TObject);
begin
	DatumExit(Sender, false);
	if M2B.Text = '' then begin
		M2B.Text	:= M2.Text;
	end;
  Idotartam;
end;

procedure TJaratbeDlg.M7Change(Sender: TObject);
begin
  Modosult := true;
end;

procedure TJaratbeDlg.BitBtn1Click(Sender: TObject);
begin
	GepkocsiValaszto(M4, GK_TRAKTOR);
	GepkocsiOlvaso;
end;

procedure TJaratbeDlg.M14Exit(Sender: TObject);
var
  km:real;
  jdatum: string;
begin
	with Sender as TMaskEdit do begin
		Text := StrSzamStr(Text,9,0);
	end;

    Query_Run(Query5, 'SELECT * FROM KMCHANGE WHERE KC_RENSZ='''+M4.Text+''' order by KC_DATUM');
    Query5.Last;
    jdatum:= Query5.FieldByname('KC_DATUM').AsString ;
    if jdatum<>'' then
      jdatum:=' and ja_jkezd>='''+ jdatum+'''';        // csak az utols� �racsere ut�ni j�ratokat veszi figyelembe

  if (Sender as TMaskEdit).Name = 'M14' then begin       // Nyit�
		KmHozzaadas(M4, M14 );


    // KG 20120123
    // A KM-hez hozz� kell adni az esetleges �racser�b�l ad�d� ill. a 1000000 -t
    if (M14.Text<>'') and (M15.Text<>'') then begin
     //if (StringSzam(M14.Text)>0) and (StringSzam(M15.Text)>0) then begin
		//  Query_Run(Query1, 'SELECT * FROM JARAT WHERE ja_fazis<>0 and JA_RENDSZ = '''+M4.Text+''' AND JA_ALKOD<>'''+M02.Text+''' AND '+M14.Text+'>= JA_NYITKM AND '+M14.Text+'< JA_ZAROKM'+jdatum);  // jav. NP 2015-02-12
     Query_Run(Query11, 'SELECT * FROM JARAT WHERE ja_fazis<>0 and JA_RENDSZ = '''+M4.Text+''' AND JA_ALKOD<>'+IntToStr(StrToIntDef(M02.Text, 0))+' AND '+M14.Text+'>= JA_NYITKM AND '+M14.Text+'< JA_ZAROKM'+jdatum);
		 if Query11.RecordCount > 0 then begin
			if NoticeKi('Figyelem! A nyit� km m�r szerepel egy m�sik j�rat intervallum�ban! J�rat:'+Query11.FieldByname('JA_ALKOD').AsString+' KM: '+
      Query11.FieldByname('JA_NYITKM').AsString+' - '+Query11.FieldByname('JA_ZAROKM').AsString+' Folytatja?', NOT_QUESTION) <> 0 then begin
        M14.SetFocus;
			end;
		 end;
    end;
    if (M14.Text<>'') then begin
		 Query_Run(Query11, 'SELECT * FROM JARAT WHERE ja_fazis<>0 and JA_RENDSZ = '''+M4.Text+''' AND JA_ALKOD<>'+IntToStr(StrToIntDef(M02.Text, 0))+' AND '''+M2.Text+'''>JA_JKEZD AND '+M14.Text+'< JA_NYITKM '+jdatum);
		 if Query11.RecordCount > 0 then begin
			if NoticeKi('Figyelem! A nyit� km kisebb mint egy el�z� j�ratban! J�rat:'+Query11.FieldByname('JA_ALKOD').AsString+' KM: '+
      Query11.FieldByname('JA_NYITKM').AsString+' D�tum: '+Query11.FieldByname('JA_JKEZD').AsString+' Folytatja?', NOT_QUESTION) <> 0 then begin
        M14.SetFocus;
			end;
		 end;
    end;
	end;
  if (Sender as TMaskEdit).Name = 'M15' then begin   // z�r�
		KmHozzaadas(M4, M15 );
    if (M14.Text<>'') and (M15.Text<>'') then begin
	 // 	 Query_Run(Query1, 'SELECT * FROM JARAT WHERE JA_RENDSZ = '''+M4.Text+''' AND JA_ALKOD<>'''+M02.Text+''' AND '+M15.Text+'> JA_NYITKM AND '+M15.Text+'<= JA_ZAROKM'+jdatum);
   	 Query_Run(Query11, 'SELECT * FROM JARAT WHERE JA_RENDSZ = '''+M4.Text+''' AND JA_ALKOD<>'+IntToStr(StrToIntDef(M02.Text, 0))+' AND '+M15.Text+'> JA_NYITKM AND '+M15.Text+'<= JA_ZAROKM'+jdatum);
		 if Query11.RecordCount > 0 then begin
			  NoticeKi('Figyelem! A z�r� km m�r szerepel egy m�sik j�rat intervallum�ban! J�rat:'+Query11.FieldByname('JA_ALKOD').AsString+' KM: '+
        Query11.FieldByname('JA_NYITKM').AsString+' - '+Query11.FieldByname('JA_ZAROKM').AsString);
				M15.SetFocus;
		 end;
    end;
	end;
  if ( ( (Sender as TMaskEdit).Name = 'M14' ) or  ( (Sender as TMaskEdit).Name = 'M15' ) ) then begin
      if StringSzam(M15.Text)<>0 then begin
  			km:= Round(StringSzam(M15.Text)-StringSzam(M14.Text));
  			MaskEdit1.Text	:= SzamString(km,10,0);
        if M3B.Text<>'' then begin
          if (EgyebDlg.ATLAG_KM_ORA>0)and(km > StringSzam(M3B.Text)*EgyebDlg.ATLAG_KM_ORA) then
            NoticeKi('T�l sok Megtett km! K�rem ellen�rizze az adatokat.');
        end;
      end;
 	end;
  ///////////
  if StrToIntDef( Query_Select('JARSOFOR','JS_JAKOD',ret_kod,'JS_SZAKM'),0) > 0 then  // m�r van km; jav�t�s
   	Query_Run(Query8, 'UPDATE JARSOFOR set JS_KMOR1='+IntToStr(StrToIntDef(M14.Text,0))+',JS_KMOR2='+IntToStr(StrToIntDef(M15.Text,0))+',JS_SZAKM='+IntToStr(StrToIntDef(M15.Text,0)-StrToIntDef(M14.Text,0)) +' WHERE JS_JAKOD = '''+ret_kod+'''')
  else
 	  Query_Run(Query8, 'UPDATE JARSOFOR set JS_KMOR1='+IntToStr(StrToIntDef(M14.Text,0))+',JS_KMOR2='+IntToStr(StrToIntDef(M15.Text,0))+' WHERE JS_JAKOD = '''+ret_kod+'''');

	Query_Run(Query8, 'SELECT * FROM JARSOFOR WHERE JS_JAKOD = '''+ret_kod+''' ORDER BY JS_SORSZ');

end;

procedure TJaratbeDlg.M14KeyPress(Sender: TObject; var Key: Char);
begin
  with Sender as TMaskEdit do begin
		Key := EgyebDlg.Jochar(Text,Key,9,0);
  end;
end;

procedure TJaratbeDlg.BitBtn3Click(Sender: TObject);
begin
  if fajko<>0 then exit;
  EgyebDlg.AUTOSZAMLA:=True;
	Application.CreateForm(TViszonybeDlg, ViszonybeDlg);
	ViszonybeDlg.Tolt('�j megb�z�s felvitele','',M20.Text);
	ViszonybeDlg.ShowModal;
	if ViszonybeDlg.ret_kod <> '' then begin
		{Felt�lti a Grid1 t�mb�t a megb�z�sokkal}
  	Query_Run(Query3, 'SELECT * FROM VISZONY WHERE VI_JAKOD = '''+M20.Text+''' ORDER BY VI_VIKOD',true);
    {R�l�p az utols� elemre}
		Query3.First;
		while ( ( not Query3.EOF ) and (Query3.FieldByname('VI_VIKOD').AsString <> ViszonybeDlg.ret_kod) ) do begin
     		Query3.Next;
    end;
		GombEllenor;
    ViszonyEllenor(False);
  end;
	ViszonybeDlg.Destroy;

  /// Automatikus sz�mla k�sz�t�s
  EgyebDlg.AUTOSZAMLA:=True;
  // [2017-05-09 NagyP] Teljesen kikapcsolom itt az automatikus sz�mla k�sz�t�st, mert a megb�z�si d�j m�dos�t�sa eset�n nem
  // tudunk "ut�nany�lni", el�g ha a viszonylatban �t kell jav�tani a d�jat
  // [2017-05-18 NagyP] Vissza az eg�sz. Kell az automatikus sz�mla, de fuvard�j m�dos�t�skor t�r�lni fogja a sz�ml�t, a
  // viszonylatban pedig m�dos�tja
  // EgyebDlg.AUTOSZAMLA:=False;

  if EgyebDlg.AUTOSZAMLA then
  begin
     BitBtn11.OnClick(self);
  end;
end;

procedure TJaratbeDlg.BitBtn4Click(Sender: TObject);
var
  rk: string;
begin
	Application.CreateForm(TViszonybeDlg, ViszonybeDlg);
  ViszonybeDlg.Tolt('Megb�z�s m�dos�t�sa',Query3.FieldByName('VI_VIKOD').AsString,M20.Text);
	ViszonybeDlg.ShowModal;
  rk:=ViszonybeDlg.ret_kod;
	if ViszonybeDlg.ret_kod <> '' then begin
		  {Felt�lti a Grid1 t�mb�t a megb�z�sokkal}
  		Query_Run(Query3, 'SELECT * FROM VISZONY WHERE VI_JAKOD = '''+M20.Text+''' ORDER BY VI_VIKOD',true);
     	{R�l�p az utols� elemre}
     	Query3.First;
     	while ( ( not Query3.EOF ) and (Query3.FieldByname('VI_VIKOD').AsString <> ViszonybeDlg.ret_kod) ) do begin
     		Query3.Next;
     	end;
		  GombEllenor;
      ViszonyEllenor(False);
	end;
	ViszonybeDlg.Destroy;
  if rk<>'' then
  begin
    /// Automatikus sz�mla k�sz�t�s
    EgyebDlg.AUTOSZAMLA:=True;
    // [2017-05-09 NagyP] Teljesen kikapcsolom itt az automatikus sz�mla k�sz�t�st, mert a megb�z�si d�j m�dos�t�sa eset�n nem
    // tudunk "ut�nany�lni", el�g ha a viszonylatban �t kell jav�tani a d�jat
     // [2017-05-18 NagyP] Vissza az eg�sz. Kell az automatikus sz�mla, de fuvard�j m�dos�t�skor t�r�lni fogja a sz�ml�t, a
    // viszonylatban pedig m�dos�tja
    // EgyebDlg.AUTOSZAMLA:=False;

    if EgyebDlg.AUTOSZAMLA then
    begin
       BitBtn11.OnClick(self);
    end;
  end;
end;

procedure TJaratbeDlg.BitBtn5Click(Sender: TObject);
var
  ujkod, mbk,jakod, Hibahely, HibaInfo: string;
  sorsz:integer;
begin
	if Query3.FieldByname('VI_SAKOD').AsString <> '' then begin
		NoticeKi('A megb�z�shoz tartozik sz�mla, ez�rt nem t�r�lhet�!');
     	Exit;
  	end;

	ujkod:= Query_select( 'SZSOR2', 'S2_UJKOD', Query3VI_UJKOD.AsString, 'S2_S2KOD');
	if ujkod <>'' then
  begin
		NoticeKi('A sz�mla szerepel egy �sszes�t� sz�ml�n, ez�rt nem t�r�lhet�! K�d = '+ujkod);
   	Exit;
  end;

	if STORNO or (NoticeKi('Val�ban t�r�lni akarja az aktu�lis rekordot?', NOT_QUESTION) = 0) then begin
   Try
    EgyebDlg.BeginTrans_ALAPKOZOS;
  		// VI_UJKOD csak akkor l�tezik, ha van m�r hozz� sz�mla
		if Query3.FieldByname('VI_UJKOD').AsString <> '' then begin
      Hibahely:='SZFEJ t�rl�s';
			if not Query_Run(Query2, 'DELETE FROM SZFEJ WHERE  SA_UJKOD= '+Query3.FieldByname('VI_UJKOD').AsString,true) then Raise Exception.Create('');
      Hibahely:='SZSOR t�rl�s';
			if not Query_Run(Query2, 'DELETE FROM SZSOR WHERE  SS_UJKOD= '+Query3.FieldByname('VI_UJKOD').AsString,true) then Raise Exception.Create('');
		  end;  // if
    Hibahely:='MEGBIZAS 1';
    mbk:=Query_Select('MEGBIZAS','MB_VIKOD',Query3.FieldByName('VI_VIKOD').AsString ,'MB_MBKOD');
    EgyebDlg.MegbizasDebug (mbk, 'T�rl�s_Update el�tt 1.');
    Hibahely:='MEGBIZAS 2';
    jakod:=Query_Select('MEGBIZAS','MB_VIKOD',Query3.FieldByName('VI_VIKOD').AsString ,'MB_JAKOD');
    Hibahely:='VISZONY t�rl�s';
		if not Query_Run(Query2, 'DELETE FROM VISZONY WHERE VI_VIKOD = '''+Query3.FieldByName('VI_VIKOD').AsString+''' ',true) then Raise Exception.Create('');
    Hibahely:='MEGBIZAS m�dos�t�s';
  	// if not Query_Run(Query2, 'UPDATE MEGBIZAS SET MB_VIKOD = '''', MB_SZKOD = '''', MB_JAKOD = '''', MB_JASZA = ''''  WHERE MB_VIKOD = '''+Query3.FieldByName('VI_VIKOD').AsString+''' ',true) then Raise Exception.Create('');
    if not Query_Run(Query2, 'UPDATE MEGBIZAS SET MB_VIKOD = '''', MB_SAKO2 = '''', MB_JAKOD = '''', MB_JASZA = ''''  WHERE MB_VIKOD = '''+Query3.FieldByName('VI_VIKOD').AsString+''' ',true) then Raise Exception.Create('');
    Hibahely:='MEGSEGED m�dos�t�s';
  	if not Query_Run(Query2, 'UPDATE MEGSEGED SET MS_JAKOD = '''', MS_JASZA = ''''  WHERE MS_JAKOD = '''+Query3.FieldByName('VI_JAKOD').AsString+''' ',true) then Raise Exception.Create('');
    Hibahely:='VISZONY lek�rdez�s';
		Query_Run(Query3, 'SELECT * FROM VISZONY WHERE VI_JAKOD = '''+M20.Text+''' ORDER BY VI_VIKOD',true);

   EgyebDlg.MegbizasDebug (mbk, 'T�rl�s_Update ut�n 1.');
  	// A j�rat megb�z�sok t�rl�se
    Hibahely:='JARATMEGBIZAS lek�rdez�s';
	  if not Query_Run(Query2, 'DELETE FROM JARATMEGBIZAS WHERE JM_MBKOD = '''+mbk+''' ') then Raise Exception.Create('');
  	// �tsorsz�mozzuk a megl�v�ket
	  sorsz	:= 1;
  	Query_Run( Query2,  'SELECT * FROM JARATMEGBIZAS WHERE JM_JAKOD = '''+jakod+''' ORDER BY JM_SORSZ ', TRUE);
	  while not Query2.Eof do begin
      Hibahely:='JARATMEGBIZAS m�dos�t�s';
		  if not Query_Update (QueryUj3, 'JARATMEGBIZAS',
			[
			'JM_SORSZ', IntToStr(sorsz)
			], ' WHERE JM_JAKOD = '''+jakod+''' '+
			   ' AND JM_MBKOD = '+Query2.FieldByName('JM_MBKOD').AsString +
			   ' AND JM_ORIGS = '+Query2.FieldByName('JM_ORIGS').AsString ) then Raise Exception.Create('');
		  Inc(sorsz);
		  Query2.Next;
	    end;  // while
    EgyebDlg.CommitTrans_ALAPKOZOS;
   Except
     on E: Exception do begin
      EgyebDlg.RollbackTrans_ALAPKOZOS;
      if E.Message<>'' then
        HibaInfo:=': '+E.Message
      else HibaInfo:='';  // az elkapott kiv�telekr�l nem j�n info, m�r feldobta az �zenetet a "Query_Run"
  		NoticeKi('A megb�z�s t�rl�se nem siker�lt! ('+Hibahely+HibaInfo+')');
      end;  // on E
   END;

		GombEllenor;
	end;
end;

procedure TJaratbeDlg.BitBtn7Click(Sender: TObject);
begin
	EgyebDlg.Calendarbe(M2);
	if M2B.Text = '' then begin
		M2B.Text	:= M2.Text;
	end;
   // Az �vsz�m ellen�rz�se
   if evstr <> copy (M2.Text, 1, 4) then begin
       evstr       := copy (M2.Text, 1, 4);
       M02.Text    := IntToStr(GetJaratAlkod(evstr));
   end;
   modosult        := true;
end;

procedure TJaratbeDlg.BitBtn8Click(Sender: TObject);
begin
	EgyebDlg.Calendarbe(M3);
end;

procedure TJaratbeDlg.BitBtn11Click(Sender: TObject);
var
	kod		: string;
	regikod	: string;
	potstr,arfdat	: string;
  arfoly: string;
begin
   GombAllapot(false);
   if fajko<>0 then begin
       NoticeKi ('Alj�rathoz nem k�sz�thet� sz�mla!');
       GombAllapot(true);
       exit;
   end;
	regikod	:= Query3.FieldByName('VI_VIKOD').AsString;
	arfdat	:= Query3.FieldByName('VI_ARFDAT').AsString;
	arfoly	:= Query3.FieldByName('VI_ARFOLY').AsString;
	if not megtekint then begin
		if not Mentes then begin
      NoticeKi ('A ment�s nem siker�lt, pr�b�ld �jra!');
      GombAllapot(true);
			Exit;
		end;
	end;
	Query3.Locate('VI_VIKOD', regikod, []);
	regikod	:= Query3.FieldByName('VI_VIKOD').AsString;
	if StrToIntDef(Query3.FieldByName('VI_UJKOD').AsString, 0) <> 0 then begin
		{Meg kell vizsg�lni, hogy a sz�mla v�gleges�tett-e}
		Query_Run(Query2, 'SELECT * FROM SZFEJ WHERE SA_UJKOD = '+Query3.FieldByName('VI_UJKOD').AsString,true);
		if Query2.FieldByname('SA_FLAG').AsString = '1' then begin
			{A sz�mla m�dos�that�}
			Application.CreateForm(TSzamlaBeDlg, SzamlaBeDlg);
			SzamlaBeDlg.Tolto('Sz�mla m�dos�t�sa', Query3.FieldByName('VI_UJKOD').AsString);
		  //	SzamlaBeDlg.ShowModal;
      if EgyebDlg.AUTOSZAMLA then
      begin
       // SzamlaBeDlg.SorBeolvaso;
        EgyebDlg.AUTOSZAMLA:=False;
        SzamlaBeDlg.Elkuldes ;
      end
      else
    	  SzamlaBeDlg.ShowModal;

      EgyebDlg.AUTOSZAMLA:=False;
			SzamlaBeDlg.Destroy;
  		{A t�bl�zat friss�t�se}
	  	Query3.Close;
		  Query3.Open;
  		// Vissza�llunk az eredeti mez�re
	  	while ( ( Query3.FieldByName('VI_VIKOD').AsString <> regikod ) and ( not Query3.EOF) ) do begin
		  	Query3.Next;
  		end;
           GombAllapot(true);
      NoticeKi ('Automatikus sz�mla k�sz!');
			Exit;
		end;
		{A sz�mla m�r l�tezik �s v�gleges�tett, megjelen�tj�k}
		Application.CreateForm(TSzamlaNezoDlg, SzamlaNezoDlg);
		SzamlaNezoDlg.SzamlaNyomtatas(StrToIntDef(Query3.FieldByName('VI_UJKOD').AsString,0), 1);
		SzamlaNezoDlg.Destroy;
		Query_Run(Query3, 'SELECT * FROM VISZONY WHERE VI_JAKOD = '''+M20.Text+''' ORDER BY VI_VIKOD',true);
		// Vissza�llunk az eredeti mez�re
		while ( ( Query3.FieldByName('VI_VIKOD').AsString <> regikod ) and ( not Query3.EOF) ) do begin
			Query3.Next;
		end;
		GombEllenor;
       GombAllapot(true);
		Exit;
	end;

	{A sz�mla m�g nem l�tezik, l�tre kell hozni}
	Application.CreateForm(TSzamlaBeDlg, SzamlaBeDlg);
	// A p�tkocsi rendsz�mok �sszegy�jt�se
	Query_Run(Query2, 'SELECT * FROM MEGBIZAS WHERE MB_DATUM > '''+EgyebDlg.MaiDatum+''' AND MB_VIKOD = '+IntToSTr(StrToIntDef(Query3.FieldByName('VI_VIKOD').AsString, 0)));
	if Query2.RecordCount > 0 then begin
		if Query2.FieldByName('MB_POTSZ').AsString <> '' then begin
			potstr	:= '('+Query2.FieldByName('MB_POTSZ').AsString+')';
		end;
	end;
	SzamlaBeDlg.potkocsistr	:= potstr;
	SzamlaBeDlg.Tolt2('�j sz�mla felvitele', M20.Text, Query3.FieldByName('VI_VIKOD').AsString);
  SzamlaBeDlg.MaskEdit19.Text:=arfdat;
  SzamlaBeDlg.Label23.Caption:=arfoly;

  if GKKATEG<>'' then
      SzamlaBeDlg.GKKAT:=GKKATEG+'T';

  if EgyebDlg.AUTOSZAMLA then
  begin
    SzamlaBeDlg.Elkuldes ;
  end
  else
  begin
  	SzamlaBeDlg.ShowModal;
  end;
  EgyebDlg.AUTOSZAMLA:=False;

	if SzamlaBeDlg.ret_kod <> '' then begin
		{A sz�mlasz�m bejegyz�se a sorba}
		kod	:= Query3.FieldByName('VI_VIKOD').AsString;
		Query_Run(Query2, 'UPDATE VISZONY SET VI_UJKOD = '+SzamlaBeDlg.ret_kod+' WHERE VI_VIKOD = '''+kod+''' ',true);
		{Felt�lti a Grid1 t�mb�t a megb�z�sokkal}
		Query_Run(Query3, 'SELECT * FROM VISZONY WHERE VI_JAKOD = '''+M20.Text+''' ORDER BY VI_VIKOD',true);
		while ( ( Query3.FieldByName('VI_VIKOD').AsString <> regikod ) and ( not Query3.EOF) ) do begin
			Query3.Next;
     	end;
		GombEllenor;
   end;
	SzamlaBeDlg.Destroy;
   GombAllapot(true);
end;

procedure TJaratbeDlg.BitBtn12Click(Sender: TObject);
begin
   GombAllapot(false);
	if Query3.FieldByName('VI_SAKOD').AsString <> '' then begin
		{Meg kell vizsg�lni, hogy a sz�mla v�gleges�tett-e}
		Query_Run(Query2, 'SELECT SA_FLAG FROM SZFEJ WHERE SA_KOD = '''+Query3.FieldByName('VI_SAKOD').AsString+''' ',true);
		if Query2.FieldByname('SA_FLAG').AsString = '1' then begin
			NoticeKi('A sz�mla m�g nem v�gleges�tett !');
           Exit;
		end;
		// Az �sszes sz�mlap�ld�ny kinyomtat�sa
		Application.CreateForm(TSzamlaNezoDlg, SzamlaNezoDlg);
		SzamlaNezoDlg.SzamlaSorozat(StrToIntDef(Query3.FieldByName('VI_UJKOD').AsString,0));
		SzamlaNezoDlg.Destroy;
	end;
   GombAllapot(true);
end;

function TJaratbeDlg.Mentes : boolean;
var
	st 				: string;
	folytat			: boolean;
	fazis,i			: integer;
	vanextra		: boolean;
	s1, s2, s3, s4	: string;
	feldat			: string;
	ledat			: string;
	fazis2			: integer;
	s5, s6			: string;
	rsz				: string;
	potstr, idotart,jdatum,msfajta,msdb 			: string;
begin
	Result	:= false;
  if trim(ret_kod)= '' then begin
      NoticeKi('�res j�ratk�d, a ment�s nem siker�lt!');
			Exit;
		end;

  // Fajta
  if FAJKO_ellenor then Exit;

	if TabbedNotebook2.PageIndex = 0 then begin         // SAJ�T J�RAT
		{Akkor kellenek ezek az ellen�rz�sek, ha nem alv�llalkoz�i j�ratot kezelek}

    // J�rat kezdete
		if M2.Text = ''  then begin
			NoticeKi('D�tum megad�sa k�telez�!');
			TabbedNotebook1.PageIndex	:= 0;
			Exit;
		end;

    if M02.Text='' then
    begin
      evstr       := copy (M2.Text, 1, 4);
      if evstr<>'' then
      begin
        M02.Text    := IntToStr(GetJaratAlkod(evstr));
        M02.Text	:= Format('%5.5d',[StrToIntDef(M02.Text,0)]);
      end;
    end;

		if not DatumExit(M2, false ) then begin
			Exit;
		end;
		if not DatumExit(M3, false ) then begin
			Exit;
		end;
    CheckBox4.Checked:= (M3.Text<>'')and(M3A.Text<>'') ;
    elszamoltjarat:=CheckBox4.Checked;

		if CheckBox4.Checked and not Kmellenor then begin
			Exit;
		end;

		if (M4.Text = '')and(M3.Text<>'') then begin           // ha a v�ge id�pont ki van t�ltve
			NoticeKi('A rendsz�m mez� �res!');
			//if NoticeKi('A rendsz�m mez� �res! Folytatja?', NOT_QUESTION) <> 0 then begin
           	TabbedNotebook1.PageIndex	:= 0;
           	M4.Setfocus;
				    Exit;
     	//end;
    end;

    if (cbFajko.ItemIndex=2) and (cbM1.Text='') then begin           // rezsi j�rathoz kell legyen le�r�s
  			NoticeKi('Rezsi j�rat eset�n a le�r�s mez� nem lehet �res!');
       	cbM1.Setfocus;
	      Exit;
    end;

    //   A j�rat intervallumban van e m�sik j�ratkezdet?
    if (EgyebDlg.DATUMIDOELL)and(M2.Text<>'')and(M2A.Text<>'')and(M3.Text<>'')and(M3A.Text<>'') then
    begin
  		 Query_Run(Query1, 'SELECT * FROM JARAT WHERE JA_FAZIS<>'+'0'+' and JA_KOD<>'''+ret_kod+''' and JA_RENDSZ = '''+M4.Text+''' AND  '''+M2.Text+M2A.Text+''' <= ja_jkezd+ja_keido and '''+M3.Text+M3A.Text+''' > ja_jkezd+ja_keido');
       if Query1.RecordCount>0 then
       begin
		  	NoticeKi('Figyelem! Ebben a j�rat intervallumban m�r van egy m�sik j�rat! J�rat:'+Query1.FieldByname('JA_ALKOD').AsString+' Kezdet: '+
        Query1.FieldByname('JA_JKEZD').AsString+' '+Query1.FieldByname('JA_KEIDO').AsString+'  V�ge: '+ Query1.FieldByname('JA_JVEGE').AsString+' '+Query1.FieldByname('JA_VEIDO').AsString);
			 	TabbedNotebook1.PageIndex	:= 0;
        M2.Setfocus;
        Exit;
       end;
       Query1.Close;
    end;
    //   A j�rat intervallumban van e m�sik j�ratv�g?
    if (EgyebDlg.DATUMIDOELL)and(M2.Text<>'')and(M2A.Text<>'')and(M3.Text<>'')and(M3A.Text<>'') then
    begin
  		 Query_Run(Query1, 'SELECT * FROM JARAT WHERE JA_FAZIS<>'+'0'+' and JA_KOD<>'''+ret_kod+''' and JA_RENDSZ = '''+M4.Text+''' AND  '''+M2.Text+M2A.Text+''' < ja_jvege+ja_veido and '''+M3.Text+M3A.Text+''' >= ja_jvege+ja_veido');
       if Query1.RecordCount>0 then
       begin
		  	NoticeKi('Figyelem! Ebben a j�rat intervallumban m�r van egy m�sik j�rat! J�rat:'+Query1.FieldByname('JA_ALKOD').AsString+' Kezdet: '+
        Query1.FieldByname('JA_JKEZD').AsString+' '+Query1.FieldByname('JA_KEIDO').AsString+'  V�ge: '+ Query1.FieldByname('JA_JVEGE').AsString+' '+Query1.FieldByname('JA_VEIDO').AsString);
			 	TabbedNotebook1.PageIndex	:= 0;
        M2.Setfocus;
        Exit;
       end;
       Query1.Close;
    end;

    // Kezd� id�pont ell. Nem szerepel e egy m�sik j�ratban
    // 2017-01-09 NP // if (EgyebDlg.DATUMIDOELL) and (M2.Text<>'') and (M2A.Text<>'') then
    if (EgyebDlg.DATUMIDOELL) and (M3.Text<>'') and (M2.Text<>'') and (M2A.Text<>'') then  // csak akkor ellen�riz, ha ki van t�ltve a "j�rat v�ge" d�tum is
    begin
  		 Query_Run(Query1, 'SELECT * FROM JARAT WHERE JA_FAZIS<>'+'0'+' and JA_KOD<>'''+ret_kod+''' and JA_RENDSZ = '''+M4.Text+''' AND  '''+M2.Text+M2A.Text+''' >= ja_jkezd+ja_keido and '''+M2.Text+M2A.Text+''' < ja_jvege+ja_veido');
       if Query1.RecordCount>0 then
       begin
		  	NoticeKi('Figyelem! A kezd� id�pont m�r szerepel egy m�sik j�rat intervallum�ban! J�rat:'+Query1.FieldByname('JA_ALKOD').AsString+' Kezdet: '+
        Query1.FieldByname('JA_JKEZD').AsString+' '+Query1.FieldByname('JA_KEIDO').AsString+'  V�ge: '+ Query1.FieldByname('JA_JVEGE').AsString+' '+Query1.FieldByname('JA_VEIDO').AsString);
			 	TabbedNotebook1.PageIndex	:= 0;
        M2.Setfocus;
        Exit;
       end;
       Query1.Close;
    end;

    // V�ge id�pont ell. Nem szerepel e egy m�sik j�ratban
    if (EgyebDlg.DATUMIDOELL) and (M3.Text<>'')and(M3A.Text<>'') then
    begin
  		 Query_Run(Query1, 'SELECT * FROM JARAT WHERE JA_FAZIS<>'+'0'+' and JA_KOD<>'''+ret_kod+''' and JA_RENDSZ = '''+M4.Text+''' AND  '''+M3.Text+M3A.Text+''' > ja_jkezd+ja_keido and '''+M3.Text+M3A.Text+''' <= ja_jvege+ja_veido');
       if Query1.RecordCount>0 then
       begin
		  	NoticeKi('Figyelem! A v�ge id�pont m�r szerepel egy m�sik j�rat intervallum�ban! J�rat:'+Query1.FieldByname('JA_ALKOD').AsString+' Kezdet: '+
        Query1.FieldByname('JA_JKEZD').AsString+' '+Query1.FieldByname('JA_KEIDO').AsString+'  V�ge: '+ Query1.FieldByname('JA_JVEGE').AsString+' '+Query1.FieldByname('JA_VEIDO').AsString);
			 	TabbedNotebook1.PageIndex	:= 0;
        M2.Setfocus;
        Exit;
       end;
       Query1.Close;
    end;

		if (M13.Text = '')or(StringSzam(M13.Text)=0) then begin
			//NoticeKi('A menetlev�l mez� �res!');
			if NoticeKi('A menetlev�l mez� �res! Folytatja?', NOT_QUESTION) <> 0 then begin
				TabbedNotebook1.PageIndex	:= 0;
        M13.Setfocus;
        Exit;
			end;
    end;

    if ((M14.Text = '')or((M14.Text = '0')))and(M3.Text<>'') then begin       // csak ha a v�ge d�tum ki van t�ltve
			  NoticeKi('Az indul� km mez� �res!');
			  //if NoticeKi('Az indul� km mez� �res! Folytatja?', NOT_QUESTION) <> 0 then begin
				  TabbedNotebook1.PageIndex	:= 0;
          M14.Setfocus;
          Exit;
        //end;
		end;
    if CheckBox4.Checked and ((M15.Text = '')or(M15.Text = '0')) then begin
      NoticeKi('A z�r� km mez� �res!');
			//if NoticeKi('A z�r� km mez� �res! Folytatja?', NOT_QUESTION) <> 0 then begin
				TabbedNotebook1.PageIndex	:= 0;
				M15.Setfocus;
				Exit;
      //end;
    end;

    Query_Run(Query5, 'SELECT * FROM KMCHANGE WHERE KC_RENSZ='''+M4.Text+''' order by KC_DATUM');
    Query5.Last;
    jdatum:= Query5.FieldByname('KC_DATUM').AsString ;
    if jdatum<>'' then
      jdatum:=' and ja_jkezd>='''+ jdatum+'''';        // csak az utols� �racsere ut�ni j�ratokat veszi figyelembe

    if CheckBox4.Checked then begin        // km �tk�z�s ell.
  		 Query_Run(Query1, 'SELECT * FROM JARAT WHERE ja_fazis<>0 and JA_RENDSZ = '''+M4.Text+''' AND JA_ALKOD<>'''+M02.Text+''' AND '+M14.Text+'>= JA_NYITKM AND '+M14.Text+'< JA_ZAROKM'+jdatum);
	  	 if Query1.RecordCount > 0 then begin
		  	NoticeKi('Figyelem! A nyit� km m�r szerepel egy m�sik j�rat intervallum�ban! J�rat:'+Query1.FieldByname('JA_ALKOD').AsString+' KM: '+
        Query1.FieldByname('JA_NYITKM').AsString+' - '+Query1.FieldByname('JA_ZAROKM').AsString);
  			TabbedNotebook1.PageIndex	:= 0;
        M14.SetFocus;
        exit;
	  	 end;

  		 Query_Run(Query1, 'SELECT * FROM JARAT WHERE ja_fazis<>0 and JA_RENDSZ = '''+M4.Text+''' AND JA_ALKOD<>'+IntToStr(StrToIntDef(M02.Text, 0))+' AND '''+M2.Text+'''>JA_JKEZD AND '+M14.Text+'< JA_NYITKM '+jdatum);
	  	 if Query1.RecordCount > 0 then begin
		  	NoticeKi('Figyelem! A nyit� km kisebb mint egy el�z� j�ratban! J�rat:'+Query1.FieldByname('JA_ALKOD').AsString+' KM: '+
        Query1.FieldByname('JA_NYITKM').AsString+' D�tum: '+Query1.FieldByname('JA_JKEZD').AsString);
  			TabbedNotebook1.PageIndex	:= 0;
        M14.SetFocus;
        exit;
		   end;

  		 Query_Run(Query1, 'SELECT * FROM JARAT WHERE ja_fazis<>0 and JA_RENDSZ = '''+M4.Text+''' AND JA_ALKOD<>'''+M02.Text+''' AND '+M15.Text+'> JA_NYITKM AND '+M15.Text+'<= JA_ZAROKM'+jdatum);
	  	 if Query1.RecordCount > 0 then begin
		  	NoticeKi('Figyelem! A z�r� km m�r szerepel egy m�sik j�rat intervallum�ban! J�rat:'+Query1.FieldByname('JA_ALKOD').AsString+' KM: '+
        Query1.FieldByname('JA_NYITKM').AsString+' - '+Query1.FieldByname('JA_ZAROKM').AsString);
  			TabbedNotebook1.PageIndex	:= 0;
				M15.SetFocus;
        exit;
	  	 end;
    end;

		if CheckBox4.Checked and (StringSzam(MasKedit1.Text) < 1) then begin
			if NoticeKi('�sszes�tett km = 0! Folytatja?', NOT_QUESTION) <> 0 then begin
           	TabbedNotebook1.PageIndex	:= 0;
           	MaskEdit1.Setfocus;
           	Exit;
			end;
    end;

		if StringSzam(MasKedit1.Text) < StringSzam(M28.Text) then begin
			// if NoticeKi('Az �resenen futott km nagyobb, mint a Megtett km! Folytatja?', NOT_QUESTION) <> 0 then begin
      NoticeKi('Az �resenen futott km nagyobb, mint a Megtett km!');  // nem lehet elfogadni �rtelmetlen adatokkal
      TabbedNotebook1.PageIndex	:= 0;
      M29.Setfocus;
      Exit;
			// end;
    end;
		try
			if CheckBox4.Checked and (Query4.RecordCount < 1) then begin
				if NoticeKi('M�g nem l�tezik k�lts�g mez�! Folytatja?', NOT_QUESTION) <> 0 then begin
					TabbedNotebook1.PageIndex	:= 2;
					Exit;
				end;
			end;
		except
		end;
    if ((Query6.RecordCount>0)and(Query7.RecordCount=0))or((Query7.RecordCount>0)and(Query6.RecordCount=0)) then
      NoticeKi('P�tkocsi - S�lyadatok probl�ma. Ellen�rizze az adatokat!');

		{A menetlev�lsz�m ellen�rz�se}
   	M13.Text	:= Format('%4.4d',[Round(StringSzam(M13.Text))]);
    if Length(M13.Text)=4 then
      M13.Text:='0'+M13.Text;
		if StringSzam(M13.Text) > 0 then begin
			Query_Run(Query2,'SELECT JA_MENSZ, JA_KOD FROM JARAT WHERE JA_MENSZ = '''+M13.Text+''' AND '+
               ' JA_JKEZD LIKE '''+copy(M2.Text, 1, 4)+'%'' ',true);
			folytat	:= true;
			while not Query2.EOF do begin
				if Query2.FieldByName('JA_KOD').AsString <> M20.Text then begin
					folytat	:= false;
				end;
				Query2.Next;
			end;
			if not folytat then begin
				{Van m�r ilyen rekord, �jat kell v�lasztani}
				NoticeKi('L�tez� menetlev�l megad�sa!');
				TabbedNotebook1.PageIndex	:= 0;
				M13.Setfocus;
				Exit;
			end;
		end;

    if EgyebDlg.AUTOSZAMLA then
  		ViszonyEllenor(False)
    else
		  ViszonyEllenor(True);



		try
		{Ha van km. vagy extra, akkor k�telez� legal�bb egy megb�z�s}
		vanextra	:= false;
		if ( vanextra and (Query3.RecordCount < 1 ) ) then begin
			NoticeKi('Megb�z�s megad�sa k�telez�!');
			TabbedNotebook1.PageIndex	:= 1;
			Exit;
		end;
		except
			// Nincs megadva sof�r!!!
		end;
	end else begin        // ALV�LLALKOZ�I J�RAT
		{Ellen�rz�s alv�llalkoz�i rekord eset�n}
		if MaskEdit4.Text = '' then begin
			NoticeKi('Az alv�llalkoz� nev�nek megad�sa k�telez�!');
		 //	TabbedNotebook1.PageIndex	:= 1;
			MaskEdit4.SetFocus;
			Exit;
		end;

    if M02.Text='' then
    begin
      evstr       := copy (M2.Text, 1, 4);
      if evstr<>'' then
      begin
        M02.Text    := IntToStr(GetJaratAlkod(evstr));
        M02.Text	:= Format('%5.5d',[StrToIntDef(M02.Text,0)]);
      end;
    end;

	end;  //  END ALV�LLALKOZ�I J�RAT

	// A bel�p�s - kil�p�s d�tum�nak ellen�rz�se
	if ( ( M2.Text > M3.Text ) and (M3.Text <> '') ) then begin
		NoticeKi('A kezd� d�tum nem lehet nagyobb a befejez�sn�l!');
		TabbedNotebook1.PageIndex	:= 0;
		M2.SetFocus;
		Exit;
	end;

	if (M2A.Text = '')and(M3.Text<>'') then begin        // csak akkor ellen�rizz�k a kezd� id�pontot, ha a v�ge d�tum ki van t�ltve
		NoticeKi('A kezd�si id�pont nem lehet �res!');
		TabbedNotebook1.PageIndex	:= 0;
		M2A.SetFocus;
		Exit;
	end;

		if (M3A.Text = '')and(M3.Text<>'') then begin
			NoticeKi('A befejez� id�pont nem lehet �res!');
			TabbedNotebook1.PageIndex	:= 0;
			M3A.SetFocus;
			Exit;
		end;

	if ( ( M2.Text = M3.Text ) and (M2A.Text > M3A.Text) ) then begin
		NoticeKi('A kezd�si id�pont nem lehet nagyobb a befejez� id�pontn�l!');
		TabbedNotebook1.PageIndex	:= 0;
		M2A.SetFocus;
		Exit;
	end;

  CheckBox4.Checked:= (M3.Text<>'')and(M3A.Text<>'') ;
  elszamoltjarat:=CheckBox4.Checked;

	if alval_jarat then begin
		if cbM1.Text = '' then begin
			cbM1.Text	:= MaskEdit4.Text;
		end;

		// A kezd� - v�gz� d�tumok �sszehasonl�t�sa az els� fel ill. utols� lerak�ssal
		feldat	:= '9999.99.99.';
		ledat	:= '';
		try
			QueryFelrako.First;
			while not QueryFelrako.Eof do begin
				st := copy(QueryFelrako.FieldByName('AS_FELIDO').AsString, 1, 11);
				if Jodatum(st) then begin
					if feldat > st then begin
						feldat	:= st;
					end;
				end;
				st := copy(QueryFelrako.FieldByName('AS_LERIDO').AsString, 1, 11);
				if Jodatum(st) then begin
					if ledat < st then begin
						ledat	:= st;
					end;
				end;
				QueryFelrako.Next;
			end;
		except
		end;
		if ( ( feldat <> '' ) and (feldat <> '9999.99.99.') ) then begin
			if feldat <> M2.Text then begin
				if NoticeKi('Az els� felrak�s d�tuma �s a j�rat kezdete nem egyeznek! Korrig�ljak?', NOT_QUESTION) = 0 then begin
					M2.Text := feldat;
				end;
			end;
		end;
		if ledat <> ''  then begin
			if ledat <> M3.Text then begin
				if NoticeKi('Az utols� lerak�s d�tuma �s a j�rat befejez�se nem egyeznek! Korrig�ljak?', NOT_QUESTION) = 0 then begin
					M3.Text := ledat;
				end;
			end;
		end;
	end;

 	{A j�rat ellen�rz�se}
	if M01.Text = '' then begin
		NoticeKi('Orsz�g megad�sa k�telez�!');
		TabbedNotebook1.PageIndex	:= 0;
//	M01.Setfocus;
		CBOrsz.SetFocus;
    Exit;
	end;

	Query_Run(Query2,'SELECT JA_ALKOD, JA_JKEZD FROM JARAT WHERE JA_ALKOD = '+M02.Text+
		' AND JA_KOD <> '''+M20.Text+''' '+
       ' AND JA_JKEZD LIKE '''+copy(M2.Text, 1, 4)+'%'' ' ,true);
	if Query2.RecordCount > 0 then begin
		{Van m�r ilyen rekord, �jat kell v�lasztani}
		if NoticeKi('L�tez� j�ratsz�m megad�sa! Gener�ljak egy �jat?', NOT_QUESTION) <> 0 then begin
			TabbedNotebook1.PageIndex	:= 0;
			M02.Setfocus;
			Exit;
		end;
		// Gener�ljunk �j j�ratsz�mot
       if evstr = '' then begin
           NoticeKi('A j�rat kezd� d�tum kit�lt�se k�telez� !');
			TabbedNotebook1.PageIndex	:= 0;
			M2.Setfocus;
			Exit;
       end;
       M02.Text    := IntToStr(GetJaratAlkod(evstr));
	end;

  // Viszony - Sz�mla vev�n�v ell.
  Viszony_Szamla_Vevoell(True) ;
//  if not Viszony_Szamla_Vevoell(True)  then
//    exit;

	// Ellen�rizz�k a k�telez� k�lts�g megl�t�t
	KoltsegEllenor;
  //Koltsegimport(False);

	Query_Run ( Query1, 'SELECT AJ_JAKOD FROM ALJARAT WHERE AJ_JAKOD = '''+M20.Text+''' ',true);
	if alval_jarat and ( Query1.RecordCount < 1) then begin
		Query_Run ( Query1, 'INSERT INTO ALJARAT (AJ_JAKOD) VALUES ( '''+M20.Text+''' ) ',true);
	end;

  // K�lts�gek d�tum�nak ellen�rz�se
  if CheckBox4.Checked then
  begin
   Query_Run(Query1, 'select * from koltseg where ks_jakod='''+M20.Text+'''');
   while not Query1.Eof do
   begin
    if  (Query1.Fieldbyname('KS_DATUM').AsString < M2.Text) or (Query1.Fieldbyname('KS_DATUM').AsString > M3.Text)  then
    begin
      NoticeKi('K�lts�g d�tuma nem esik bele a j�rat d�tum intervallumba!');
      exit;
    end;
    Query1.Next;
   end;
  end;
	fazis		:= 0;
	if CheckBox4.Checked then begin
		fazis	:= -1;
	end;
	// Ellen�rizz�k az ellen�rz�tts�get
	fazis2	:= EllenorzottJarat(M20.Text);
	if ( ( fazis2 > 0 ) or (fazis2 = -2) ) then begin
		fazis	:= fazis2;
	end;
	// Az els� k�t sof�r adatainak �sszegy�jt�se
	s1 	:= '';
	s2 	:= '';
	s3 	:= '';
	s4 	:= '';
	s5	:= '0';
	s6	:= '0';
	try
		Query8.First;
		if not Query8.Eof then begin
			s1	:= Query8.FieldByName('JS_SOFOR').AsString;
			s2	:= Query8.FieldByName('SOFORNEV').AsString;
			s5	:= SqlSzamString(StrinGSzam(Query8.FieldByName('JS_SZAKM').AsString), 12,2);
		end;
		Query8.Next;
		if not Query8.Eof then begin
			s3	:= Query8.FieldByName('JS_SOFOR').AsString;
			s4	:= Query8.FieldByName('SOFORNEV').AsString;
			s6	:= SqlSzamString(StrinGSzam(Query8.FieldByName('JS_SZAKM').AsString), 12,2);
		end;
	except
	end;
	if M2.Text = '' then begin
		M2.Text	:= EgyebDlg.MaiDatum;
	end;
	rsz			:= M4.Text;
	if MaskEdit5.Text <> '' then begin	// Alv�llalkoz�n�l ki van t�ltve a rendsz�m
		rsz 	:= MaskEdit5.Text;
	end;

	// A p�tkocsik felt�lt�se
	potstr		:= '';
	Query_Run(Query1, 'SELECT * FROM JARPOTOS WHERE JP_JAKOD = '''+ret_kod+''' ', FALSE);
	if Query1.RecordCount > 0 then begin
		potstr	:= Query1.FieldByName('JP_POREN').AsString;
		Query1.Next;
		if not Query1.Eof then begin
			potstr	:= potstr + ';' + Query1.FieldByName('JP_POREN').AsString;
		end;
	end;
  idotart:=		IntToStr(StrToIntDef(M3B.Text,0));
  msfajta:=EgyebDlg.Jarat_MBfajtak(ret_kod) ;
  i:=PosEx ('_',msfajta);
  msdb:='0';
  if i > 0 then
  begin
    msdb:=copy(msfajta,1,i-1);
    msfajta:=copy(msfajta,i+1,50);
  end;

  Try
 	 if not Query_Update ( Query1, 'JARAT' , [
		'JA_ORSZ',		''''+M01.Text+'''',
		'JA_ALKOD',		''''+Format('%4d',[Round(StringSzam(M02.Text))])+'''',
		'JA_MEGJ',		''''+M12.Text+'''',
		'JA_JARAT',		''''+cbM1.Text+'''',
		'JA_JKEZD',		''''+M2.Text+'''',
		'JA_KEIDO',		''''+M2A.Text+'''',
		'JA_JVEGE',		''''+M3.Text+'''',
		'JA_VEIDO',		''''+M3A.Text+'''',
		'JA_JAORA',		idotart,
		'JA_LFDAT',		''''+M2B.Text+'''',
		'JA_RENDSZ',	''''+rsz+'''',
// 	'JA_ALVAL', 	'0',
//	'JA_MENSZ',		''''+Format('%4.4d',[Round(StringSzam(M13.Text))])+'''',
		'JA_MENSZ',		''''+M13.Text+'''',
		'JA_FAZIS',		IntToStr(fazis),
		'JA_FAZST',     ''''+GetJaratFazis(fazis)+'''',
		'JA_NYITKM',	IntToStr(StrToIntDef(M14.Text,0)),
		'JA_ZAROKM',	IntToStr(StrToIntDef(M15.Text,0)),
		'JA_OSSZKM',	IntToStr(StrToIntDef(MaskEdit1.Text,0)),
		'JA_BELKM',	  	IntToStr(StrToIntDef(M27.Text,0)),
		'JA_URESK',	  	IntToStr(StrToIntDef(M28.Text,0)),
		'JA_UBKM',	  	IntToStr(StrToIntDef(M29.Text,0)),
		'JA_UKKM',	  	IntToStr(StrToIntDef(M30.Text,0)),
		'JA_EUROS', 	IntToStr(RG1.ItemIndex),
		'JA_JPOTK',		''''+potstr+'''',
		'JA_SOFK1',		''''+s1+'''',
		'JA_SOFK2',		''''+s3+'''',
		'JA_SZAK1',		s5,
		'JA_SNEV1',		''''+s2+'''',
		'JA_SNEV2',		''''+s4+'''',
		'JA_SZAK2',		s6,
		'JA_MSFAJ',		''''+msfajta+'''',
		'JA_MSDB',		msdb,
		'JA_FAJKO', 	IntToStr(cbFAJKO.ItemIndex)
		], ' WHERE JA_KOD = '''+ret_kod+''' ' ) then Raise Exception.Create('') ;

	 EgyebDlg.torles_jakod	:= '';

   if MaskEdit4.Text<>'' then
	 if not Query_Update ( Query1, 'ALJARAT', [
		'AJ_ALNEV',		''''+MaskEdit4.Text+'''',
		'AJ_V_KOD',		''''+alvalkod+'''',
		'AJ_ALCIM',		''''+MaskEdit6.Text+'''',
		'AJ_ALTEL',		''''+MaskEdit7.Text+'''',
		'AJ_ALFAX',		''''+MaskEdit8.Text+'''',
		'AJ_ALREN',		''''+MaskEdit5.Text+'''',
		'AJ_POTOS',		''''+MaskEdit50.Text+'''',
		'AJ_ALTIP',		''''+MaskEdit9.Text+'''',
		'AJ_FUDIJ',		SqlSzamString(StringSzam(MaskEdit10.Text), 10, 2),
		'AJ_FUVAL',		''''+vnemlist[CVnem.ItemIndex]+'''',
		'AJ_POMEG',		''''+MaskEdit11.Text+'''',
		'AJ_FUMEG',		''''+MaskEdit12.Text+'''',
//     	'AJ_MEGJE',		''''+MaskEdit3.Text+'''',
		'AJ_MEMO01',	''''+MM1.Text+'''',
		'AJ_MEMO02',	''''+MM2.Text+'''',
		'AJ_MEMO03',	''''+MM3.Text+'''',
		'AJ_MEMO04',	''''+MM4.Text+'''',
		'AJ_MEMO05',	''''+MM5.Text+'''',
		// A fizet�si felt�telek
		'AJ_FIZU1',		''''+fizu1+'''',
		'AJ_FIZU2',		''''+fizu2+'''',
		'AJ_FIZU3',		''''+fizu3+'''',
		'AJ_FISZ1',		SqlSzamString(StringSzam(fisz1), 6, 2),
		'AJ_FISZ2',		SqlSzamString(StringSzam(fisz2), 6, 2),
		'AJ_FISZ3',		SqlSzamString(StringSzam(fisz3), 6, 2),
		// A felel�s k�dja
		'AJ_FEKOD', 	''''+feleloslist[CBFelelos.ItemIndex]+'''',
	  { A fejl�cek, l�bl�cek, memo -k bet�lt�se }
		'AJ_FEJS1',		''''+copy(f1,1,80)+'''',
		'AJ_FEJS2',		''''+copy(f2,1,80)+'''',
		'AJ_FEJS3',		''''+copy(f3,1,80)+'''',
		'AJ_FEJS4',		''''+copy(f4,1,80)+'''',
		'AJ_FEJS5',		''''+copy(f5,1,80)+'''',
		'AJ_LABS1',		''''+copy(l1,1,80)+'''',
		'AJ_LABS2',		''''+copy(l2,1,80)+'''',
		'AJ_LABS3',		''''+copy(l3,1,80)+'''',
		'AJ_LABS4',		''''+copy(l4,1,80)+'''',
		'AJ_LABS5',		''''+copy(l5,1,80)+'''',
		'AJ_MEMO11',	''''+Me1.Lines[0]+'''',
		'AJ_MEMO12',	''''+Me1.Lines[1]+'''',
		'AJ_MEMO13',	''''+Me1.Lines[2]+'''',
		'AJ_MEMO14',	''''+Me1.Lines[3]+'''',
		'AJ_MEMO15',	''''+Me1.Lines[4]+'''',
		'AJ_MEMO16',	''''+Me1.Lines[5]+'''',
		'AJ_MEMO21',	''''+Me2.Lines[0]+'''',
		'AJ_MEMO22',	''''+Me2.Lines[1]+'''',
		'AJ_MEMO23',	''''+Me2.Lines[2]+'''',
		'AJ_MEMO31',	''''+Me3.Lines[0]+'''',
		'AJ_MEMO32',	''''+Me3.Lines[1]+'''',
		'AJ_MEMO33',	''''+Me3.Lines[2]+''''
		], ' WHERE AJ_JAKOD = '''+ret_kod+''' ' ) then Raise Exception.Create('');

   // Megb�z�s    Ha esetleg v�ltozik az orsz�gk�d, akkor a megb�z�son is �tvezetj�k
   if not ujrekord then
   begin
	 if not Query_Update ( Query1, 'MEGBIZAS' , [
		'MB_JASZA',		''''+M01.Text+'-'+Format('%4d',[Round(StringSzam(M02.Text))])+''''
		], ' WHERE MB_JAKOD = '''+ret_kod+''' ' ) then Raise Exception.Create('') ;
	 if not Query_Update ( Query1, 'MEGSEGED' , [
		'MS_JASZA',		''''+M01.Text+'-'+Format('%4d',[Round(StringSzam(M02.Text))])+''''
		], ' WHERE MS_JAKOD = '''+ret_kod+''' ' ) then Raise Exception.Create('') ;

   end;

	 // A k�lts�gek friss�t�se
	 try
		Query4.First;
		while not Query4.Eof do begin
			KoltsegJaratTolto(Query4.FieldByName('KS_KTKOD').AsString);
			Query4.Next;
		end;
	 except
	 end;
	 Result	:= true;
  Except
  		NoticeKi('A j�rat t�rol�sa nem siker�lt!');
  End;
end;

procedure TJaratbeDlg.BitBtn13Click(Sender: TObject);
begin
	if M4.Text = '' then begin
		NoticeKi('K�lts�gek megad�sa el�tt ki kell t�lteni a rendsz�mot!');
		TabbedNotebook1.PageIndex	:= 0;
		Exit;
	end;
	Application.CreateForm(TKoltsegbeDlg, KoltsegbeDlg);
  KoltsegbeDlg.JARATBOL:=True;
	KoltsegbeDlg.Tolt2('�j k�lts�g felvitele', '', M20.Text, M4.Text, M01.Text+'-'+M02.Text);
	// Alv�llalkoz�i j�rat eset�n csak az egy�b k�lts�get lehet haszn�lni
	if alval_jarat then begin
		KoltsegbeDlg.PageControl1.ActivePageIndex		:= 1;
		KoltsegbeDlg.PageControl1.Pages[0].TabVisible	:= false;
		KoltsegbeDlg.PageControl1.Pages[2].TabVisible	:= false;
		KoltsegbeDlg.PageControl1.Pages[3].TabVisible	:= false;
	end;
	KoltsegbeDlg.ShowModal;
	if KoltsegbeDlg.ret_kod <> '' then begin
		Query_Run(Query4, 'SELECT * FROM koltseg WHERE KS_JAKOD = '''+M20.Text+''' ORDER BY KS_DATUM',true);
		Query4.Locate('KS_KTKOD', KoltsegbeDlg.ret_kod, [] );
		GombEllenor;
	end;
	KoltsegbeDlg.Destroy;
end;

procedure TJaratbeDlg.BitBtn14Click(Sender: TObject);
begin
	Application.CreateForm(TKoltsegbeDlg, KoltsegbeDlg);
  KoltsegbeDlg.JARATBOL:=True;
  	KoltsegbeDlg.Tolt2('K�lts�gek m�dos�t�sa',Query4.FieldByName('KS_KTKOD').AsString, M20.Text, M4.Text, M01.Text+'-'+M02.Text);
	if alval_jarat then begin
		KoltsegbeDlg.PageControl1.ActivePageIndex		:= 1;
		KoltsegbeDlg.PageControl1.Pages[0].TabVisible	:= false;
		KoltsegbeDlg.PageControl1.Pages[2].TabVisible	:= false;
		KoltsegbeDlg.PageControl1.Pages[3].TabVisible	:= false;
	end;
	KoltsegbeDlg.ShowModal;
	if KoltsegbeDlg.ret_kod <> '' then begin
		Query_Run(Query4, 'SELECT * FROM koltseg WHERE KS_JAKOD = '''+M20.Text+''' ORDER BY KS_DATUM',true);
		Query4.Locate('KS_KTKOD', KoltsegbeDlg.ret_kod, [] );
		GombEllenor;
	end;
	KoltsegbeDlg.Destroy;
end;

procedure TJaratbeDlg.BitBtn15Click(Sender: TObject);
var
  Mess: string;
begin
	if NoticeKi('Val�ban t�r�lni akarja az aktu�lis k�lts�get? A k�lts�g a t�rl�s ut�n �jra el�rhet� lesz a feldolgoz�sra v�r�k k�z�tt.', NOT_QUESTION) = 0 then begin
		// Query_Run(Query2, 'DELETE FROM KOLTSEG WHERE KS_KTKOD = '+Query4.FieldByName('KS_KTKOD').AsString,true);
    Mess:= KoltsegTorles(Query4.FieldByName('KS_KTKOD').AsInteger, True);
    if Mess='' then begin
  		Query_Run(Query4, 'SELECT * FROM koltseg WHERE KS_JAKOD = '''+M20.Text+''' ORDER BY KS_DATUM',true);
      end
    else begin
      NoticeKi('A t�rl�s nem siker�lt! ('+Mess+')', NOT_ERROR);
      end;
		GombEllenor;
	end; // if
end;

procedure TJaratbeDlg.M24Exit(Sender: TObject);
begin
	with Sender as TMaskEdit do begin
  	Text := StrSzamStr(Text,9,2);
  end;
end;

procedure TJaratbeDlg.M26KeyPress(Sender: TObject; var Key: Char);
begin
  with Sender as TMaskEdit do begin
  	Key := EgyebDlg.Jochar(Text,Key,9,2);
  end;
end;

procedure TJaratbeDlg.SzamClick(Sender: TObject);
begin
	with Sender as TMaskEdit do begin
  		SelectAll;
  	end;
end;

procedure TJaratbeDlg.M02KeyPress(Sender: TObject; var Key: Char);
begin
  with Sender as TMaskEdit do begin
  	Key := EgyebDlg.Jochar(Text,Key,5,0);
  end;
end;

procedure TJaratbeDlg.M02Exit(Sender: TObject);
begin
  if M02.Text<>'' then
	M02.Text	:= Format('%5.5d',[StrToIntDef(M02.Text,0)]);
end;

procedure TJaratbeDlg.FormShow(Sender: TObject);
begin

	BitBtn3.Visible	:= 	GetRightTag(564) <> RG_NORIGHT;

 Try
	if not megtekint then
  begin
    if not alval_jarat then
      NemElszamoltJarat(M4.Text);

		if CheckBox4.Checked then begin
			cbM1.SetFocus;
		end else begin
			CBOrsz.SetFocus;
		end;
	end else
  begin
		BitKIlep.SetFocus;
	end;
 Except
 End;
	GombEllenor;
end;

procedure TJaratbeDlg.BitBtn19Click(Sender: TObject);
begin
	if Query4KS_VIKOD.AsString <> '' then begin
		{M�r van a k�lts�ghez megb�z�s -> annak t�rl�se}
		if NoticeKi('Val�ban t�rli a megb�z�s kapcsolatot?', NOT_QUESTION) = 0 then begin
			Query_Run (Query1, 'UPDATE KOLTSEG SET KS_VIKOD = '''' WHERE KS_KTKOD = '+Query4KS_KTKOD.AsString,true);
			ModLocate(Query4, 'KS_KTKOD', Query4KS_KTKOD.AsString );
		end;
	end else begin
		{Kiv�lasztjuk a j�rathoz kapcsol�d� megrendel�t, ha t�bb van}
		Application.CreateForm(TRendvalDlg,RendvalDlg);
		RendvalDlg.Tolt('A megb�z�s kiv�laszt�sa',M20.Text);
		if RendvalDlg.vanrek then begin
			RendvalDlg.ShowModal;
			if RendvalDlg.ret_vikod <> '' then begin
				{A megb�z�s hozz�rendel�se a k�lts�ghez}
				Query_Run (Query1, 'UPDATE KOLTSEG SET KS_VIKOD = '''+RendvalDlg.ret_vikod
					+''' WHERE KS_KTKOD = '+Query4KS_KTKOD.AsString,true);
				ModLocate(Query4, 'KS_KTKOD', Query4KS_KTKOD.AsString );
			end;
		end;
		RendvalDlg.Destroy;
	end;
end;

procedure TJaratbeDlg.GombEllenor;
begin
	if megtekint then begin
		Exit;
	end;
	{Ellen�rzi a m�dos�t�s, t�rl�s nyom�gombokat}
	BitBtn4.Enabled		:= false;
	BitBtn5.Enabled		:= false;
	BitBtn22.Enabled	:= false;
	BitBtn34.Enabled	:= false;
	try
		if Query3.RecordCount > 0 then begin
			BitBtn4.Enabled		:= true;
			BitBtn34.Enabled	:= true;
			BitBtn5.Enabled		:= true;
			BitBtn22.Enabled	:= true;
		end;
	except
	end;

	BitBtn14.Enabled	:= false;
	BitBtn15.Enabled	:= false;
	try
		if Query4.RecordCount > 0 then begin
			BitBtn14.Enabled	:= true;
			BitBtn15.Enabled	:= true;
		end;
	except
	end;

	BitBtn24.Enabled	:= false;
	BitBtn25.Enabled	:= false;
	try
		if Query6.RecordCount > 0 then begin
			BitBtn24.Enabled	:= true;
			BitBtn25.Enabled	:= true;
		end;
	except
	end;

	BitBtn16.Enabled	:= false;
	BitBtn17.Enabled	:= false;
	try
		if QueryCMR.RecordCount > 0 then begin
			BitBtn16.Enabled	:= true;
			BitBtn17.Enabled	:= true;
		end;
	except
	end;


	{Ha van k�lts�g vagy viszony �s van Elk�ld�s, akkor nincs Kil�p gomb}
	try
		if ( ( ( Query3.RecordCount > 0 ) or (Query4.RecordCount > 0) ) and (BitElkuld.Visible) )then begin
			BitKilep.Hide;
		end;
	except
	end;

	BitBtn31.Enabled	:= false;
	BitBtn48.Enabled	:= false;
	try
		if QueryFelrako.RecordCount > 0 then begin
			BitBtn31.Enabled	:= true;
			BitBtn48.Enabled	:= true;
		end;
	except
	end;

	BitBtn35.Enabled := false;
	BitBtn38.Enabled := false;
	try
		if Query7.RecordCount > 0 then begin
			BitBtn35.Enabled := true;
			BitBtn38.Enabled := true;
		end;
	except
	end;

	BitBtn26.Enabled := false;
	BitBtn39.Enabled := false;
	try
		if Query8.RecordCount > 0 then begin
			BitBtn26.Enabled := true;
			BitBtn39.Enabled := true;
		end;
	except
	end;

	// A list�z�s gomb csak akkor l�tszik, ha van j�ratk�d
	BitBtn21.Visible	:= false;
	if M20.Text <> '' then begin
		BitBtn21.Visible	:= true;
	end;
end;

procedure TJaratbeDlg.M13Exit(Sender: TObject);
begin
	M13.Text	:= Format('%4.4d',[Round(StringSzam(M13.Text))]);
  if Length(M13.Text)=4 then
    M13.Text:='0'+M13.Text;
end;

procedure TJaratbeDlg.BitBtn21Click(Sender: TObject);
begin
	// Ellen�rizz�k az ellen�rz�tts�get
	if M20.Text <> '' then begin
		EgyebDlg.kelllistagomb	:= false;
		EgyebDlg.ListaGomb	   	:= '';
		if EllenorzottJarat(M20.Text) < 1 then begin
			if GetRightTag(552) <> RG_NORIGHT then begin
				EgyebDlg.kelllistagomb	:= true;
				EgyebDlg.ListaGomb	   	:= '&Ellen�rz�s';
			end;
		end;
		Application.CreateForm(TJarlisDlg, JarlisDlg);
		JarlisDlg.Tolt(M20.Text);
		JarlisDlg.Rep.Preview;
		JarlisDlg.Destroy;
		if EgyebDlg.ListaValasz = 1 then begin
			if NoticeKi('Val�ban ellen�rz�tt� k�v�nja tenni a j�ratot?', NOT_QUESTION) = 0 then begin
				// Ellen�rz�tt� tessz�k a j�ratot �s kil�p�nk
				if VanNyitottSzamla(M20.Text) then begin
					NoticeKi('M�g van a j�rathoz kapcsolt nem v�gleges�tett sz�mla!');
					Exit;
				end;
				// Ellen�rizz�k az utols� tankol�st -> saj�t j�rat eset�n
				if not alval_jarat then begin
					 Query_Run(Query2, 'SELECT MIN(KS_KMORA) KMORA FROM KOLTSEG WHERE KS_RENDSZ = '''+M4.Text+''' '+
						' AND KS_KMORA >= '+IntToStr(StrToIntDef(M15.Text,0))+
						' AND KS_TELE  = 1 ' +
						' AND KS_TIPUS = 0 ' );
					 if ( Query2.RecordCount < 1 ) then begin
						NoticeKi('Hi�nyz� z�r� km ut�ni tankol�s : ' + IntToStr(StrToIntDef(M15.Text,0)));
						Exit;
					 end;
				end;
				{Ellen�rz�tt� kell tenni a rekordot}
				if Mentes then begin
					elkuldes	:= true;
					Query_Run(Query1, 'UPDATE JARAT SET JA_FAZIS = 1, JA_FAZST = ''Ellen�rz�tt'' WHERE JA_KOD = '''+M20.Text+''' ',true);
					Close;
				end;
			end;
		end;
	end;
end;

procedure TJaratbeDlg.BitBtn22Click(Sender: TObject);
begin
	{A sz�mlasz�m t�rl�se a viszonyb�l (csak akkor, ha duplik�lt) }
	if Query3VI_SAKOD.AsString = '' then begin
		Exit;
	end;
	if Query_Run (Query1, 'SELECT * FROM VISZONY WHERE VI_SAKOD = '''+Query3VI_SAKOD.AsString+''' ORDER BY VI_VIKOD',true) then begin
		if Query1.RecordCount > 1 then begin
			if NoticeKi('Val�ban t�rli a sz�mlasz�mot a megb�z�sb�l?', NOT_QUESTION) <> 0 then begin
				Exit;
			end;
			{A sz�mlasz�m lev�tele}
			Query_Run (Query1, 'UPDATE VISZONY SET VI_SAKOD = '''' WHERE VI_VIKOD = '''+Query3VI_VIKOD.AsString+''' ',true);
			{Felt�lti a Grid1 t�mb�t a megb�z�sokkal}
			Query_Run(Query3, 'SELECT * FROM VISZONY WHERE VI_JAKOD = '''+M20.Text+''' ORDER BY VI_VIKOD',true);
			{R�l�p az utols� elemre}
			Query3.First;
			GombEllenor;
		end else begin
			NoticeKi('A sz�mlasz�m nem duplik�lt!');
			Exit;
		end;
	end;
end;

procedure TJaratbeDlg.BitBtn23Click(Sender: TObject);
begin
	{�jabb s�lyadat felvitele}
	if not Kmellenor then begin
		Exit;
	end;
	Application.CreateForm(TSulybeDlg, SulybeDlg);
  	SulybeDlg.Tolt2('�j s�lyadat felvitele', '', M20.Text, M4.Text, M01.Text+'-'+M02.Text);
	SulybeDlg.ShowModal;
	if SulybeDlg.ret_kod <> '' then begin
		{Felt�lti a Grid4 t�mb�t a s�lyadatokkal}
 		Query_Run(Query6, 'SELECT * FROM sulyok WHERE SU_JAKOD = '''+M20.Text+''' ORDER BY SU_KMORA',true);
		{R�l�p az utols� elemre}
     	Query6.First;
     	while ( ( not Query6.EOF ) and (Query6.FieldByname('SU_SUKOD').AsString <> SulybeDlg.ret_kod) ) do begin
			Query6.Next;
     	end;
     	GombEllenor;
	end;
	SulybeDlg.Destroy;
end;

procedure TJaratbeDlg.BitBtn24Click(Sender: TObject);
begin
	if not Kmellenor then begin
		Exit;
	end;
	Application.CreateForm(TSulybeDlg, SulybeDlg);
	SulybeDlg.Tolt2('S�lyadatok m�dos�t�sa',Query6.FieldByName('SU_SUKOD').AsString, M20.Text, M4.Text, M01.Text+'-'+M02.Text);
	SulybeDlg.ShowModal;
	if SulybeDlg.ret_kod <> '' then begin
		{Felt�lti a Grid4 t�mb�t a s�lyadatokkal}
		Query_Run(Query6, 'SELECT * FROM sulyok WHERE SU_JAKOD = '''+M20.Text+''' ORDER BY SU_KMORA',true);
	  {R�l�p az utols� elemre}
	  Query6.First;
	  while ( ( not Query6.EOF ) and (Query6.FieldByname('SU_SUKOD').AsString <> SulybeDlg.ret_kod) ) do begin
		Query6.Next;
	  end;
	  GombEllenor;
	end;
	SulybeDlg.Destroy;
end;

procedure TJaratbeDlg.BitBtn25Click(Sender: TObject);
begin
	if NoticeKi('Val�ban t�r�lni akarja az aktu�lis rekordot?', NOT_QUESTION) = 0 then begin
	Query_Run(Query2, 'DELETE FROM sulyok WHERE SU_SUKOD = '''+Query6.FieldByName('SU_SUKOD').AsString+''' ',true);
 		Query_Run(Query6, 'SELECT * FROM sulyok WHERE SU_JAKOD = '''+M20.Text+''' ORDER BY SU_KMORA',true);
     GombEllenor;
	end;
end;

procedure TJaratbeDlg.BitBtn29Click(Sender: TObject);
begin
	if not megtekint then begin
		if not Mentes then begin
			Exit;
		end;
	end;
//  FAX:= Query_Select('VEVO','V_KOD',Query3.FieldByName('VI_VEKOD').AsString ,'V_FAX');
  FAX:=MaskEdit8.Text;
  if FAX<>'' then
  begin
    Clipboard.Clear;
    Clipboard.AsText:=FAX;
    NoticeKi('FAX sz�m a v�g�lapra t�ve. CTRL-V (Beilleszt�s) billenty� le�t�ssel beilleszthet�.');
  end;
	{A Fuvaroz�si megb�z�s nyomtat�sa}
	EgyebDlg.kelllistagomb	:= true;
	EgyebDlg.ListaGomb	   	:= 'Email k�ld�s';
	Application.CreateForm(TFumegliDlg, FumegliDlg);
  FumegliDlg.STORNO:=STORNO;
	FumegliDlg.Tolt(M20.Text);
	FumegliDlg.Rep.Preview;
	FumegliDlg.Destroy;
	if EgyebDlg.ListaValasz = 1 then begin
		// Itt van az elk�ld�s
		BitBtn45Click(Sender);
    //STORNOKULDVE:=True;
	end;
end;

procedure TJaratbeDlg.FormDestroy(Sender: TObject);
begin
	CVnem.Free;
	listaorsz.Free;
end;

procedure TJaratbeDlg.BitBtn27Click(Sender: TObject);
begin
	{A fejl�c beolvas�sa}
	Application.CreateForm(TMellekForm, MellekForm);
  	MellekForm.Tolt('A fejl�c adatai', f1, f2, f3, f4, f5, '', '', 5);
  	MellekForm.ShowModal;
  	if not MellekForm.Kilepes then begin
		f1 := MellekForm.mell1;
		f2 := MellekForm.mell2;
		f3 := MellekForm.mell3;
		f4 := MellekForm.mell4;
		f5 := MellekForm.mell5;
       modosult	:= true;
	end;
	MellekForm.Destroy;
end;

procedure TJaratbeDlg.BitBtn28Click(Sender: TObject);
begin
	{A l�bl�c beolvas�sa}
	Application.CreateForm(TMellekForm, MellekForm);
  	MellekForm.Tolt('A l�bl�c elemek kit�lt�se', l1, l2, l3, l4, l5, '', '', 5);
	MellekForm.ShowModal;
  	if not MellekForm.Kilepes then begin
		l1 := MellekForm.mell1;
		l2 := MellekForm.mell2;
		l3 := MellekForm.mell3;
		l4 := MellekForm.mell4;
		l5 := MellekForm.mell5;
		modosult	:= true;
	end;
	MellekForm.Destroy;
end;

procedure TJaratbeDlg.BitBtn33Click(Sender: TObject);
var
	s1, s2, s3	: string;
begin
	{A megjegyz�sek beolvas�sa}
	Application.CreateForm(TMegjmemoFm, MegjmemoFm);
	MegjmemoFm.Tolt(me1, me2, me3);
	MegjmemoFm.ShowModal;
	if not MegjmemoFm.kilepes then begin
		me1.Clear;
		me1.Lines.AddStrings(MegjmemoFm.Memo1.Lines);
		me2.Clear;
		me2.Lines.AddStrings(MegjmemoFm.Memo2.Lines);
		me3.Clear;
		me3.Lines.AddStrings(MegjmemoFm.Memo3.Lines);
		modosult	:= true;
	end;
	MegjmemoFm.Destroy;
	s1	:= me1.Lines[0];
	s2	:= me1.Lines[1];
	s3	:= me1.Lines[2];
end;

procedure TJaratbeDlg.ButValVevoClick(Sender: TObject);
begin
	{Alv�llalkoz� beolvas�sa}
 if EgyebDlg.v_evszam<'2013' then
 begin
	Application.CreateForm(TAlvallalUjFmDlg,AlvallalUjFmDlg);
	AlvallalUjFmDlg.valaszt	:= true;
	AlvallalUjFmDlg.ShowModal;
	emailcim		:= '';
	if AlvallalUjFmDlg.ret_vkod <> '' then begin
		alvalkod		:= AlvallalUjFmDlg.ret_vkod;
		MaskEdit4.Text	:= AlvallalUjFmDlg.ret_vnev;
		Query_Run(QueryKOZ,'SELECT * FROM ALVALLAL WHERE V_KOD = '''+AlvallalUjFmDlg.ret_vkod+''' ',true);
		if QueryKOZ.RecordCount > 0 then begin
			MaskEdit6.Text	:= QueryKOZ.FieldByName('v_irsz').AsString + ' ' +
								QueryKOZ.FieldByName('v_varos').AsString + ', '+
								QueryKOZ.FieldByName('v_cim').AsString ;
			MaskEdit7.Text	:= QueryKOZ.FieldByName('v_tel').AsString;
			MaskEdit8.Text	:= QueryKOZ.FieldByName('v_fax').AsString;
			emailcim := QueryKOZ.FieldByName('v_email').AsString;
      UNYELV := 'HUN';  // itt m�g nincs ilyen adat
		end;
		// Ha �res a le�r�s mez�, akkor t�lts�k be az alv�llalkoz� nev�t, ellenkez� esetben k�rd�s
		if cbM1.Text = '' then begin
			cbM1.Text	:= AlvallalUjFmDlg.ret_vnev;
		end else begin
			if cbM1.Text <> AlvallalUjFmDlg.ret_vnev then begin
				if NoticeKi('A jelenlegi le�r�s elt�r az alv�llalkoz� nev�t�l. Kicser�ljem?', NOT_QUESTION) = 0 then begin
					cbM1.Text	:= AlvallalUjFmDlg.ret_vnev;
				end;
			end;
		end;
	end;
	AlvallalUjFmDlg.Destroy;
 end
 else
 begin
  Application.CreateForm(TVevoUjFmDlg, VevoUjFmDlg);
	//Application.CreateForm(TAlvallalUjFmDlg,AlvallalUjFmDlg);
	VevoUjFmDlg.valaszt	:= true;
	VevoUjFmDlg.ShowModal;
	emailcim		:= '';
	if VevoUjFmDlg.ret_vkod <> '' then begin
		alvalkod		:= VevoUjFmDlg.ret_vkod;
		MaskEdit4.Text	:= VevoUjFmDlg.ret_vnev;
		Query_Run(QueryKOZ,'SELECT * FROM VEVO WHERE V_KOD = '''+VevoUjFmDlg.ret_vkod+''' ',true);
		if QueryKOZ.RecordCount > 0 then begin
			MaskEdit6.Text	:= QueryKOZ.FieldByName('v_irsz').AsString + ' ' +
								QueryKOZ.FieldByName('v_varos').AsString + ', '+
								QueryKOZ.FieldByName('v_cim').AsString ;
			MaskEdit7.Text	:= QueryKOZ.FieldByName('v_tel').AsString;
			MaskEdit8.Text	:= QueryKOZ.FieldByName('v_fax').AsString;
			emailcim		:= QueryKOZ.FieldByName('v_email').AsString;
      UNYELV := QueryKOZ.FieldByName('VE_UNYELV').AsString;
      if UNYELV='' then  UNYELV:= QueryKOZ.FieldByName('VE_NYELV').AsString;  // sz�ml�z�si nyelv
		end;
		// Ha �res a le�r�s mez�, akkor t�lts�k be az alv�llalkoz� nev�t, ellenkez� esetben k�rd�s
		if cbM1.Text = '' then begin
			cbM1.Text	:= VevoUjFmDlg.ret_vnev;
      NyelvFrissit;
		end else begin
			if cbM1.Text <> VevoUjFmDlg.ret_vnev then begin
				if NoticeKi('A jelenlegi le�r�s elt�r az alv�llalkoz� nev�t�l. Kicser�ljem?', NOT_QUESTION) = 0 then begin
					cbM1.Text	:= VevoUjFmDlg.ret_vnev;
          NyelvFrissit;
				end;
			end;
		end;
	end;
	VevoUjFmDlg.Destroy;
 end;
end;

procedure TJaratbeDlg.cbFAJKOChange(Sender: TObject);
begin
   FAJKO_valtozas;
end;

procedure TJaratbeDlg.FAJKO_valtozas;
var
  ejasza, msmbkod: string;
begin
  if (cbFAJKO.ItemIndex<>fajko)and((Query3.RecordCount<>0)or(QSeged.RecordCount<>0)) then
  begin
    NoticeKi('A j�rathoz megb�z�s tartozik, ez�rt a fajt�ja m�r nem m�dos�that�!')  ;
    cbFAJKO.ItemIndex:=fajko;
    exit;
  end;

  fajko:=cbFAJKO.ItemIndex;
  //Panel20.Caption:=RG_FAJKO.Items[RG_FAJKO.itemindex];
  case cbFAJKO.itemindex of
    0: Panel20.Caption:='Norm�l';
    1: Panel20.Caption:='Alj�rat';
    2: Panel20.Caption:='Rezsi j�rat';
    end;  // case

  if (cbFAJKO.ItemIndex=1)and(M20.Text<>'') then begin
    msmbkod:= Query_Select('MEGSEGED','MS_JAKOD',M20.Text,'MS_MBKOD');
    if msmbkod<>'' then begin
      ejasza:= Query_Select('MEGBIZAS','MB_MBKOD', msmbkod,'MB_JASZA');
      if ejasza<>'' then
        Panel20.Caption:='Alj�rat(norm�l:'+ejasza+')';
    end;
  end;
end;

procedure TJaratbeDlg.MaskEdit10Exit(Sender: TObject);
begin
	SzamExit(Sender);
end;

procedure TJaratbeDlg.MaskEdit10KeyPress(Sender: TObject; var Key: Char);
begin
	with Sender as TMaskEdit do begin
		Key := EgyebDlg.Jochar(Text,Key,10,2);
	end;
end;

procedure TJaratbeDlg.BitBtn34Click(Sender: TObject);
var
	kod 		: string;
	regikod	: string;
  lejarat:integer;
  kdat,ldat,max_kidat,hiba:string;
  skod : string;
  s: string;
  mbkod, ManCode, sa_man, vekod, LejaratS: string;
begin
  if fajko<>0 then exit;
	// Ellen�rizni kell, hogy a sor v�gleges�tett-e m�r?
	if Query3.FieldByName('VI_SAKOD').AsString <> '' then begin
		NoticeKi('Ez a sor m�r nem v�gleges�thet�!');
		Exit;
	end;
	{Csak akkor lehet v�gleges�teni, ha m�r volt a sz�ml�z�sban}
	if StrToIntDef(Query3.FieldByName('VI_UJKOD').AsString,0) = 0 then begin
		NoticeKi('El�sz�r be kell l�pni a sz�ml�z�sba, csak ut�na lehet v�gleges�teni!');
		Exit;
	end;
	{Sz�mla kelt�nek ellen�rz�se}
//	if Query3.FieldByName('VI_KIDAT').AsString = '' then begin
   if Query_Select('SZFEJ','SA_UJKOD', Query3.FieldByName('VI_UJKOD').AsString,'SA_KIDAT') = '' then begin
		//NoticeKi('A sz�mla kelt�t meg kell adni, csak ut�na lehet v�gleges�teni!');
		//Exit;
       Query_Log_Str('nincs_kidat_1 (VI_UJKOD:'+Query3.FieldByName('VI_UJKOD').AsString+'), vekod='+Query3.FieldByName('VI_VEKOD').AsString, 0, false, true);
       LejaratS:= Query_Select('VEVO','V_KOD',Query3.FieldByName('VI_VEKOD').AsString ,'V_LEJAR');
       Query_Log_Str('nincs_kidat_2 (VI_UJKOD:'+Query3.FieldByName('VI_UJKOD').AsString+'), LejaratS='+LejaratS, 0, false, true);
       lejarat:= StrToIntDef(LejaratS, 0);
       Query_Log_Str('nincs_kidat_3 (VI_UJKOD:'+Query3.FieldByName('VI_UJKOD').AsString+'), lejarat='+IntToStr(lejarat), 0, false, true);
       kdat:=DateToStr(date);
		   ldat:=DatumHoznap(kdat, lejarat, true);
       S:= 'Sz�mla kelte: '+kdat+'  Esed�kess�g: '+ldat+' ('+inttostr( lejarat)+')';
       Query_Log_Str('nincs_kidat_4, (VI_UJKOD:'+Query3.FieldByName('VI_UJKOD').AsString+') '+S, 0, false, true);
       if NoticeKi(S ,NOT_QUESTION)<>0 then begin
         Query_Log_Str('nincs_kidat_5.0 (VI_UJKOD:'+Query3.FieldByName('VI_UJKOD').AsString+'), USER ABORT', 0, false, true);
          exit;
          end;
       Query_Update ( Query1, 'SZFEJ', [
		      'SA_KIDAT',		''''+kdat+'''',
  		    'SA_ESDAT',		''''+ldat+''''
        	], ' WHERE SA_UJKOD = '+Query3.FieldByName('VI_UJKOD').AsString);
        Query_Log_Str('nincs_kidat_5 (VI_UJKOD:'+Query3.FieldByName('VI_UJKOD').AsString+'), UPDATE OK', 0, false, true);
	end;
//  if Query3.FieldByName('VI_FDEXP').AsFloat=0 then
	if Query_Select('SZFEJ','SA_UJKOD', Query3.FieldByName('VI_UJKOD').AsString,'SA_OSSZEG') = '0' then begin
       if (MessageBox(0, 'A sz�mla v�g�sszege 0 (nulla)!'+#13+#10+'Folytatja?', '', MB_ICONQUESTION or MB_YESNO or MB_DEFBUTTON2) in [ idNo]) then begin
  	        //	NoticeKi('A sz�mla v�g�sszege 0 (nulla). Nem lehet v�gleges�teni!');
           exit;
       end;
   end;

   // KG 20111214   XML ellen�rz�s futtat�sa
	Query_Run( QueryKoz, 'SELECT VE_XMLSZ FROM VEVO WHERE V_KOD= '+Query3.FieldByName('VI_VEKOD').AsString, true );
	if QueryKoz.FieldByName('VE_XMLSZ').Value>0 then begin
 	    Application.CreateForm(TXML_ExportDlg, XML_ExportDlg);
       XML_ExportDlg.M3.text:= Query3.FieldByName('VI_UJKOD').AsString ;
       //XML_ExportDlg.CheckBox1.Checked:=True;
       if not XML_ExportDlg.XML_Generalas2  then begin
           if MessageBox(0,'XML gener�l�s ellen�rz�si HIBA!'+#13+ 'Folytatja?', 'XML ellen�rz�s', MB_ICONWARNING or MB_YESNO or MB_DEFBUTTON2)=IDNO then begin
  		        //NoticeKi('XML gener�l�s ellen�rz�si HIBA!');
    	        XML_ExportDlg.Destroy;
               exit;
           end;
       end else begin
  	        NoticeKi('XML gener�l�s ellen�rz�se: OK!');
       end;
       XML_ExportDlg.Destroy;
   end;
   /////////////////////////////////////////

   Query_Run(Query2, 'select max(sa_kidat) maxdat from SZFEJ where sa_kod<>'''+'''');
   max_kidat:= Query2.FieldByName('maxdat').AsString;
   if Query_Select('SZFEJ','SA_UJKOD', Query3.FieldByName('VI_UJKOD').AsString,'SA_KIDAT') <max_kidat then begin
  		NoticeKi('Az �j sz�mla d�tuma kor�bbi mint egy el�z��! '+max_kidat);
       exit;
   end;

   hiba:= EgyebDlg.Szamlaszam_ell();
   if hiba<>'' then begin
  		if NoticeKi('Hi�nyz� sz�mlasz�m!'+#13+#10+hiba+#13+#10+'Folytatja?',NOT_QUESTION)<>0 then begin
           exit;
       end;
   end;

	{A sz�mla v�gleges�t�se}
	if NoticeKi('Val�ban v�gleges�teni k�v�nja a sz�ml�t?', NOT_QUESTION) = 0 then begin
		regikod	:= Query3.FieldByName('VI_VIKOD').AsString;
       Try
		 {Sz�mlasz�m gener�l�sa, ha m�g nincs sz�mlasz�ma}
		 if Query3.FieldByName('VI_SAKOD').AsString = '' then begin
  	 	    kod 	:= GetUjszamla2(copy(Query_Select('SZFEJ','SA_UJKOD', Query3.FieldByName('VI_UJKOD').AsString,'SA_KIDAT'),3,2));
          if (kod='0') or (kod='') then begin
             S:= 'Sikertelen k�d gener�l�s, k�rem pr�b�lja �jra! (JaratSzamlaVeglegesites)';
             HibaKiiro(S);
             NoticeKi(S);
             Exit;
             end;
          sa_man:='0';  // alap�rtelmez�sben nem egyedi d�jas
           // Ha egyedi a megb�z�s, akkor a TEMAN sz�t kell megjelen�teni
           Query_Run(Query2, 'SELECT SA_FEJL1, SA_VEVOKOD FROM SZFEJ WHERE SA_UJKOD = '+Query3.FieldByName('VI_UJKOD').AsString, true);
           skod    := Query2.FieldByName('SA_FEJL1').AsString ;
           vekod:= Query2.FieldByName('SA_VEVOKOD').AsString;
           if Pos('(', skod) > 0 then begin
               skod := copy(skod, 1, Pos('(', skod) - 1);  // ha m�r van a v�g�n z�r�jelben valami, akkor levessz�k
           end;
           Query_Run(Query2, 'SELECT MB_EDIJ FROM MEGBIZAS, VISZONY WHERE MB_VIKOD = VI_VIKOD AND VI_UJKOD = '+ Query3.FieldByName('VI_UJKOD').AsString);
           if StrToIntdef(Query2.FieldByName('MB_EDIJ').AsString, 0) > 0 then begin
               // Egyedi d�jr�l van sz�
               // skod    := skod + ' (TEMAN)';
               // --- 2016-01-07
               if LehetEgyediDij(vekod, ManCode) then begin
                  if ManCode <> '' then
                      skod:= skod + ' ('+ManCode+')';
                  sa_man:='1';
                  end
               else begin  // nem lehet egyedi d�j: meg kell szak�tani a v�gleges�t�st
                   NoticeKi('Ehhez a partnerhez nem k�sz�thet�nk egyedi d�jas sz�ml�t!');
                   exit;
                  end;
           end;
           Query_Run(Query2, 'UPDATE SZFEJ SET SA_FEJL1 = '''+ skod +''', SA_MAN = '+sa_man+ ' WHERE SA_UJKOD = '+Query3.FieldByName('VI_UJKOD').AsString,true);

           if kod='' then begin
               exit;
           end;
			if not Query_Run(Query2, 'UPDATE SZFEJ SET SA_FLAG=''0'', SA_ALLST = '''+GetSzamlaAllapot(0)+''', SA_KOD='''+kod+''' '+
				' WHERE SA_UJKOD = '+Query3.FieldByName('VI_UJKOD').AsString,true) then Raise Exception.Create('SZFEJ m�dos�t�s');
			if not Query_Run(Query2, 'UPDATE SZSOR SET SS_KOD='''+kod+''' WHERE SS_UJKOD = '+
				Query3.FieldByName('VI_UJKOD').AsString,true) then Raise Exception.Create('SZSOR m�dos�t�s');
			if not Query_Run(Query2, 'UPDATE VISZONY SET VI_SAKOD = '''+kod+''' WHERE VI_VIKOD = '''+Query3VI_VIKOD.AsString+''' ',true) then Raise Exception.Create('VISZONY m�dos�t�s');
		 end else begin
			kod := Query3.FieldByName('VI_SAKOD').AsString;
			if not Query_Run(Query2, 'UPDATE SZFEJ SET SA_FLAG=''0'', SA_ALLST = '''+GetSzamlaAllapot(0)+''' '+
			  ' WHERE SA_UJKOD = '+Query3.FieldByName('VI_UJKOD').AsString,true) then Raise Exception.Create('SZFEJ m�dos�t�s');
			if not Query_Run(Query2, 'UPDATE SZSOR SET SS_KOD='''+kod+''' WHERE SS_UJKOD = '+
				Query3.FieldByName('VI_UJKOD').AsString,true) then Raise Exception.Create('SZSOR m�dos�t�s');
		 end;
     if not EgyebDlg.Szamlaertek2jar_megb(Query3.FieldByName('VI_UJKOD').AsString,kod) then Raise Exception.Create('Szamlaertek2jar_megb') ;     // �sszeg, sz�mlasz�m vissza�r�sa a Viszonyba �s a megb�z�sba.
     // ------ ez �gy marhas�g, Query2-ben teljesen m�s van itt. ------------
     // if not MegbizasKieg(Query2.FieldByName('MB_MBKOD').AsString) then Raise Exception.Create('MegbizasKieg');
     mbkod:=Query_Select('MEGBIZAS','MB_VIKOD',Query3.FieldByName('VI_VIKOD').AsString ,'MB_MBKOD');
     if Trim(mbkod)='' then
       Query_Log_Str('Sz�mla v�gleges�t�s (j�ratb�l), �res MBKOD! VIKOD='+Query3.Fieldbyname('VI_VIKOD').AsString, 0, true);  // napl�zzuk

     if not MegbizasKieg(mbkod) then Raise Exception.Create('MegbizasKieg');
     // ----------------------------------------------------------------------
    Except
      on E: exception do begin
        S:= 'A sz�mla v�gleges�t�se nem siker�lt! ('+E.Message+')';
        HibaKiiro(S + 'SA_UJKOD='+ Query3.FieldByName('VI_UJKOD').AsString);   // b�v�tett form�ban napl�zzuk
    		NoticeKi(S);
        end;  // on E
    END;

		{A t�bl�zat friss�t�se}
		Query3.Close;
		Query3.Open;
		// Vissza�llunk az eredeti mez�re
		while ( ( Query3.FieldByName('VI_VIKOD').AsString <> regikod ) and ( not Query3.EOF) ) do begin
			Query3.Next;
		end;
	end;
end;

procedure TJaratbeDlg.M13AExit(Sender: TObject);
begin
	with Sender as TMaskEdit do begin
		Text := StrSzamStr(Text,5,1);
	end;
end;

procedure TJaratbeDlg.M13AKeyPress(Sender: TObject; var Key: Char);
begin
	with Sender as TMaskEdit do begin
		Key := EgyebDlg.Jochar(Text,Key,5,1);
	end;
end;

procedure TJaratbeDlg.BitKeresClick(Sender: TObject);
var
	keres_str	: string;
begin
	// MaskEdit4 alapj�n kital�lja, hog melyik alv�llalkoz�r�l van sz�
	if MaskEdit4.Text	= '' then begin
		Exit;
	end;
	keres_str	:= MaskEdit4.Text;
	while Pos(' ', keres_str) > 0 do begin
  		keres_str := copy(keres_str,1,Pos(' ', keres_str)-1)+	'%#' + copy(keres_str,Pos(' ', keres_str)+1, 255);
	end;
	while Pos('#', keres_str) > 0 do begin
		keres_str := copy(keres_str,1,Pos('#', keres_str)-1)+	' ' + copy(keres_str,Pos('#', keres_str)+1, 255);
	end;
	keres_str := keres_str + '%';
  if EgyebDlg.v_evszam<'2013' then
  	Query_Run(QueryKOZ,'SELECT * FROM ALVALLAL WHERE UPPER(V_NEV) LIKE '''+UpperCase(keres_str)+''' ',true)
  else
  	Query_Run(QueryKOZ,'SELECT * FROM VEVO WHERE UPPER(V_NEV) LIKE '''+UpperCase(keres_str)+''' ',true);
	if QueryKOZ.RecordCount > 0 then begin
		MaskEdit4.Text	:= 	QueryKOZ.FieldByName('v_nev').AsString;
		MaskEdit6.Text	:= 	QueryKOZ.FieldByName('v_irsz').AsString + ' ' +
							QueryKOZ.FieldByName('v_varos').AsString + ', '+
							QueryKOZ.FieldByName('v_cim').AsString ;
		MaskEdit7.Text	:= 	QueryKOZ.FieldByName('v_tel').AsString;
		MaskEdit8.Text	:= 	QueryKOZ.FieldByName('v_fax').AsString;
	end;
end;

procedure TJaratbeDlg.MaskEdit4KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
	if Key = vk_F4 then begin
		// A beg�pelt sz�vegnek megfelel� alv�llalkoz� bet�lt�se
		if Maskedit4.Text <> '' then begin
			BitKeresClick(Sender);
			MaskEdit5.SetFocus;
		end;
	end;
end;

procedure TJaratbeDlg.BitBtn38Click(Sender: TObject);
begin
	if NoticeKi('Val�ban t�r�lni k�v�nja a kijel�lt p�tkocsit? ', NOT_QUESTION) = 0 then begin
       Query_Run(Query2, 'DELETE FROM JARPOTOS WHERE JP_JPKOD = '+Query7.FieldByname('JP_JPKOD').AsString);
   	Query7.Close;
   	Query7.Open;
       modosult	:= true;
	end;
   GombEllenor;
end;

procedure TJaratbeDlg.BitBtn20Click(Sender: TObject);
begin
	if not Kmellenor then begin
   	Exit;
   end;
	Application.CreateForm(TPotkocsibeDlg, PotkocsibeDlg);
   PotkocsibeDlg.rendszam	:= M4.Text;
   PotkocsibeDlg.Tolt('P�tkocsi adatok beolvas�sa', ret_kod, '', M14.Text, M15.Text);
	PotkocsibeDlg.M3.Text		:= M14.Text;
	PotkocsibeDlg.M4.Text		:= M15.Text;
	PotkocsibeDlg.ShowModal;
   if PotkocsibeDlg.ret_kod <> '' then begin
   	Query7.Close;
   	Query7.Open;
  	  	Query7.Locate('JP_JPKOD',PotkocsibeDlg.ret_kod,[loCaseInsensitive, loPartialKey]);
       modosult	:= true;
   end;
   PotkocsibeDlg.Destroy;
	GombEllenor;
end;

procedure TJaratbeDlg.BitBtn35Click(Sender: TObject);
begin
	if not Kmellenor then begin
		Exit;
	end;
	Application.CreateForm(TPotkocsibeDlg, PotkocsibeDlg);
	PotkocsibeDlg.rendszam	:= M4.Text;
	PotkocsibeDlg.Tolt('P�tkocsi adatok m�dos�t�sa', ret_kod, Query7.FieldByname('JP_JPKOD').AsString, M14.Text, M15.Text);
	if ( ( StrToIntDef(PotkocsibeDlg.M3.Text, 0) = 0 ) and (StrToIntDef(PotkocsibeDlg.M4.Text, 0) = 0) ) then begin
		PotkocsibeDlg.M3.Text		:= M14.Text;
		PotkocsibeDlg.M4.Text		:= M15.Text;
	end;
	PotkocsibeDlg.ShowModal;
   if PotkocsibeDlg.ret_kod <> '' then begin
   	Query7.Close;
   	Query7.Open;
  	  	Query7.Locate('JP_JPKOD',PotkocsibeDlg.ret_kod,[loCaseInsensitive, loPartialKey]);
       modosult	:= true;
   end;
	PotkocsibeDlg.Destroy;
end;

procedure TJaratbeDlg.BitBtn18Click(Sender: TObject);
begin
	if CheckBox4.Checked and not Kmellenor then begin
   	Exit;
	end;
  jkdat:=M2.Text+' '+M2A.Text+':00';
  jvdat:=M3.Text+' '+M3A.Text+':00';
	Application.CreateForm(TSoforbeDlg, SoforbeDlg);
  SoforbeDlg.kellellenorzes:=(M3.Text<>'');
  SoforbeDlg.elszamoltjarat:=elszamoltjarat;
  SoforbeDlg.kmdij	:= '0';
  if M4.Text <> '' then begin
		SoforbeDlg.kmdij	:= Query_select('GEPKOCSI', 'GK_RESZ', M4.Text, 'GK_DIJ1');
  end;
	SoforbeDlg.rendszam		:= M4.Text;
	SoforbeDlg.Tolt('Sof�r adatok beolvas�sa', ret_kod, '');
	SoforbeDlg.M3.Text		:= M14.Text;
	SoforbeDlg.M4.Text		:= M15.Text;
	SoforbeDlg.eujarat		:= (RG1.ItemIndex = 1);
  SoforbeDlg.cbCSEHTRAN.ItemIndex:=0;  // alap�rtelmezett 0 �rt�k
  SoforbeDlg.ShowModal;
  if SoforbeDlg.ret_kod <> '' then begin
		Query8.Close;
   	Query8.Open;
  	Query8.Locate('JS_SORSZ',SoforbeDlg.ret_kod,[loCaseInsensitive, loPartialKey]);
    modosult	:= true;
  end;
  SoforbeDlg.Destroy;
  GombEllenor;
end;

procedure TJaratbeDlg.BitBtn39Click(Sender: TObject);
begin
	if NoticeKi('Val�ban t�r�lni k�v�nja a kijel�lt sof�rt? ', NOT_QUESTION) = 0 then begin
       Query_Run(Query2, 'DELETE FROM JARSOFOR WHERE JS_JAKOD = '''+ret_kod+''' AND JS_SORSZ = '''+Query8.FieldByname('JS_SORSZ').AsString+''' ');
		Query8.Close;
   	Query8.Open;
       modosult	:= true;
	end;
   GombEllenor;
end;

procedure TJaratbeDlg.BitBtn26Click(Sender: TObject);
begin
	if not Kmellenor then begin
		Exit;
	end;
  jkdat:=M2.Text+' '+M2A.Text+':00';
  jvdat:=M3.Text+' '+M3A.Text+':00';
	Application.CreateForm(TSoforbeDlg, SoforbeDlg);
  SoforbeDlg.elszamoltjarat:=elszamoltjarat;
	SoforbeDlg.rendszam		:= M4.Text;
	SoforbeDlg.Tolt('Sof�r adatok m�dos�t�sa', ret_kod, Query8.FieldByname('JS_SORSZ').AsString);
	if ( ( StrToIntDef(SoforbeDlg.M3.Text, 0) = 0 ) and (StrToIntDef(SoforbeDlg.M4.Text, 0) = 0) ) then begin
		SoforbeDlg.M3.Text		:= M14.Text;
		SoforbeDlg.M4.Text		:= M15.Text;
	end;
	SoforbeDlg.ShowModal;
   if SoforbeDlg.ret_kod <> '' then begin
   	Query8.Close;
   	Query8.Open;
  	  	Query8.Locate('JS_SORSZ',SoforbeDlg.ret_kod,[loCaseInsensitive, loPartialKey]);
       modosult	:= true;
   end;
   SoforbeDlg.Destroy;
	GombEllenor;
	BitElkuld.SetFocus;
end;

procedure TJaratbeDlg.Query8CalcFields(DataSet: TDataSet);
begin
    Query8SOFORNEV.AsString	:= Query_select('DOLGOZO', 'DO_KOD', Query8.FieldByName('JS_SOFOR').AsString, 'DO_NAME');
end;

procedure TJaratbeDlg.FormCanResize(Sender: TObject; var NewWidth,
  NewHeight: Integer; var Resize: Boolean);
begin
	if ( ( NewWidth < 1024 ) or ( NewHeight < 768 ) ) then begin
		Resize := false;
	end;
end;

procedure TJaratbeDlg.FormResize(Sender: TObject);
begin
	// Nyom�gombok elrendez�se
	// Az als� nyom�gombok
	ObjektumSorolo( Panel1.Width, [BitElkuld, BitBtn21, BitBtn46, BitBtn6, BitBtn49, BitBtn32, BitKilep]);
	// A megb�z�sok nyom�gombjai
	ObjektumSorolo( Panel2.Width, [BitBtn3, BitBtn4, BitBtn5, BitBtn22, BitBtn2, BitBtn43, BitBtn37, BitBtn11, BitBtn34, BitBtn12 ]);
	// A k�lts�gek nyom�gombjai
	ObjektumSorolo( Panel3.Width, [BitBtn13, BitBtn14, BitBtn15, BitBtn19 ]);
	// A s�lyadatok nyom�gombjai
	ObjektumSorolo( Panel4.Width, [BitBtn23, BitBtn24, BitBtn25]);
	// A dolgoz�i t�bl�zat nyom�gombjai
	ObjektumSorolo( Panel8.Width, [BitBtn18, BitBtn26, BitBtn39 ]);
	// A cmr adatok nyom�gombjai
	ObjektumSorolo( Panel19.Width, [BitBtn10, BitBtn16, BitBtn17, BitBtn36 ]);
	// A takar� panel elhelyez�se
	Panel16.Left	:= Panel5.Left;
	Panel16.Top		:= Panel5.Top + panel5.Height;
	Panel16.Width	:= Panel5.Width;
end;

procedure TJaratbeDlg.FormClose(Sender: TObject; var Action: TCloseAction);
begin
//	if ( Sender as TBitBtn ) .Name <> 'BitElkuld' then begin
	if not elkuldes then begin
		elkuldes	:= false;
    if modosult then begin
			if NoticeKi('M�dosultak az adatok . Ki akar l�pni ment�s n�lk�l?', NOT_QUESTION) <> 0 then begin
               Action := caNone;
               Exit;
      end;
    end;
    if ujrekord then begin
      {Ki kell t�r�lni az el�z�leg l�trehozott rekordokat}
			Query_Run ( Query1, 'DELETE FROM JARAT    WHERE JA_KOD   = '''+M20.Text+''' ',true);
			Query_Run ( Query1, 'DELETE FROM VISZONY  WHERE VI_JAKOD = '''+M20.Text+''' ',true);
			Query_Run ( Query1, 'DELETE FROM koltseg  WHERE KS_JAKOD = '''+M20.Text+''' ',true);
			Query_Run ( Query1, 'DELETE FROM sulyok   WHERE SU_JAKOD = '''+M20.Text+''' ',true);
			Query_Run ( Query1, 'DELETE FROM JARPOTOS WHERE JP_JAKOD = '''+M20.Text+''' ',true);
			Query_Run ( Query1, 'DELETE FROM JARSOFOR WHERE JS_JAKOD = '''+M20.Text+''' ',true);
			Query_Run ( Query1, 'DELETE FROM ALSEGED  WHERE AS_JAKOD = '''+M20.Text+''' ',true);
			Query_Run ( Query1, 'DELETE FROM JARATMEGBIZAS WHERE JM_JAKOD = '''+M20.Text+''' ',true);
			Query_Update( Query1, 'MEGBIZAS', [
				'MB_JAKOD', ''''+''+'''',
				'MB_JASZA', ''''+''+'''',
        // 'MB_SZKOD', ''''+''+'''',
        'MB_SAKO2', ''''+''+'''',
				'MB_VIKOD', ''''+''+''''
			], ' WHERE MB_JAKOD = '''+M20.Text+''' AND MB_DATUM > '''+EgyebDlg.MaiDatum+''' ');
			Query_Update( Query1, 'MEGSEGED', [
				'MS_JAKOD', ''''+''+'''',
				'MS_JASZA', ''''+''+''''
			], ' WHERE MS_JAKOD = '''+M20.Text+''' ');
		end;
	  ret_kod := '';
  end;
end;

function TJaratbeDlg.Kmellenor : boolean;
begin
	Result	:= false;
	if StrToIntDef(M14.Text,-1) < 0 then begin
     	NoticeKi('�rv�nytelen j�rat indul� km!');
  		TabbedNotebook1.PageIndex	:= 0;
  		TabbedNotebook2.PageIndex	:= 0;
      M14.SetFocus;
      Exit;
  end;
	if StrToIntDef(M15.Text,0) <> 0 then begin
		if StringSzam(M14.Text) > StringSzam(M15.Text) then begin
			     NoticeKi('Rossz km-ek megad�sa! A z�r�nak nagyobbnak kell lennie a nyit�n�l!');
           TabbedNotebook1.PageIndex	:= 0;
           TabbedNotebook2.PageIndex	:= 0;
           M15.Text 					:= '';
           M15.Setfocus;
           Exit;
    end;
  end;
	if ( ( StrToIntDef(M14.Text,0) = 0 ) and (StrToIntDef(M15.Text,0) = 0 ) ) then begin
       NoticeKi('Rossz km-ek megad�sa! Nem lehet mindk�t km 0!');
  		 TabbedNotebook1.PageIndex	:= 0;
  		 TabbedNotebook2.PageIndex	:= 0;
       M15.Text 					:= '';
       M15.Setfocus;
       Exit;
   end;
	Result	:= true;
end;

procedure TJaratbeDlg.CBFelelosChange(Sender: TObject);
var
	szokod	: string;
begin
	// Bet�ltj�k a fejl�c �s l�bl�c elemeket
	szokod	:= feleloslist[CBFelelos.ItemIndex];
	f1		:= EgyebDlg.Read_SZGrid('330', szokod + 'F1' );
  f2		:= EgyebDlg.Read_SZGrid('330', szokod + 'F2' );
	f3		:= EgyebDlg.Read_SZGrid('330', szokod + 'F3' );
  f4		:= EgyebDlg.Read_SZGrid('330', szokod + 'F4' );
  f5		:= EgyebDlg.Read_SZGrid('330', szokod + 'F5' );

  // l1		:= EgyebDlg.Read_SZGrid('330', szokod + 'L1' );  // NyelvFrissit -ben
  // l2		:= EgyebDlg.Read_SZGrid('330', szokod + 'L2' );  // NyelvFrissit -ben
  // l3		:= EgyebDlg.Read_SZGrid('330', szokod + 'L3' );  // NyelvFrissit -ben
  // l4		:= EgyebDlg.Read_SZGrid('330', szokod + 'L4' );  // NyelvFrissit -ben
  // l5		:= EgyebDlg.Read_SZGrid('330', szokod + 'L5' );  // NyelvFrissit -ben
  NyelvFrissit;
end;

procedure TJaratbeDlg.ToltMegbizas;
var
	vanem		: integer;
begin
	// A j�rat mez�k felt�lt�se az EgyebDlg.megbizaskod alapj�n
	Query_Run(Query1, 'SELECT MIN(MS_FELDAT) MINDAT, MAX(MS_LERDAT) MAXDAT FROM MEGSEGED WHERE MS_MBKOD = '+IntToStr(StrToIntDef(EgyebDlg.megbizaskod,0)));
	if Query1.RecordCount < 1 then begin
		Exit;
	end;
	M2.Text			:= Query1.FieldByName('MINDAT').AsString;
	if M2.Text = '' then begin
		M2.Text		:= EgyebDlg.MaiDatum;
	end;
	M3.Text			:= Query1.FieldByName('MAXDAT').AsString;
	if M3.Text = '' then begin
		M3.Text		:= EgyebDlg.MaiDatum;
	end;
	// Az id�pontok meghat�roz�sa
	Query_Run(Query1, 'SELECT MIN(MS_FELIDO) MINIDO FROM MEGSEGED WHERE MS_MBKOD = '+IntToStr(StrToIntDef(EgyebDlg.megbizaskod,0))+	' AND MS_FELDAT = '''+M2.Text+''' ' );
	M2A.Text  		:= Query1.FieldByName('MINIDO').AsString;
	if M2A.Text = '' then begin
		M2A.Text  	:= '0:00';
	end;
	Query_Run(Query1, 'SELECT MAX(MS_LERIDO) MAXIDO FROM MEGSEGED WHERE MS_MBKOD = '+IntToStr(StrToIntDef(EgyebDlg.megbizaskod,0))+
		' AND MS_LERDAT = '''+M3.Text+''' ' );
	M3A.Text  		:= Query1.FieldByName('MAXIDO').AsString;
	if M3A.Text = '' then begin
		M3A.Text  	:= '24:00';
	end;
	Query_Run(Query1, 'SELECT * FROM MEGBIZAS WHERE MB_MBKOD = '+IntToStr(StrToIntDef(EgyebDlg.megbizaskod,0)));
	if Query1.RecordCount < 1 then begin
		Exit;
	end;
	if Query1.FieldByName('MB_ALVAL').AsString = '' then begin
		M4.Text			:= Query1.FieldByName('MB_RENSZ').AsString;
		Exit;
	end;
	// Most j�n az alv�llalkoz�
	TabbedNotebook2.PageIndex	:= 1;
  if EgyebDlg.v_evszam<'2013' then
  	Query_Run(QueryKOZ,'SELECT * FROM ALVALLAL WHERE V_KOD = '''+Query1.FieldByName('MB_ALVAL').AsString+''' ',true)
  else
  	Query_Run(QueryKOZ,'SELECT * FROM VEVO WHERE V_KOD = '''+Query1.FieldByName('MB_ALVAL').AsString+''' ',true);
	if QueryKOZ.RecordCount > 0 then begin
		MaskEdit4.Text	:= QueryKOZ.FieldByName('v_nev').AsString;
		MaskEdit6.Text	:= QueryKOZ.FieldByName('v_irsz').AsString + ' ' +
							QueryKOZ.FieldByName('v_varos').AsString + ', '+
							QueryKOZ.FieldByName('v_cim').AsString ;
		MaskEdit7.Text	:= QueryKOZ.FieldByName('v_tel').AsString;
		MaskEdit8.Text	:= QueryKOZ.FieldByName('v_fax').AsString;
    if EgyebDlg.v_evszam>='2013' then begin
        UNYELV := QueryKOZ.FieldByName('VE_UNYELV').AsString;
        if UNYELV='' then  UNYELV:= QueryKOZ.FieldByName('VE_NYELV').AsString;  // sz�ml�z�si nyelv
        end;
    NyelvFrissit; // nyelvf�gg� adattartalom
	end;
	MaskEdit5.Text		:= Query1.FieldByName('MB_RENSZ').AsString;
	MaskEdit9.Text		:= Query1.FieldByName('MB_GKKAT').AsString;
	MaskEdit10.Text		:= Query1.FieldByName('MB_ALDIJ').AsString;
	vanem	:= vnemlist.IndexOf(Query1.FieldByName('MB_ALNEM').AsString);
	if vanem < 0 then begin
		vanem	:= 0;
	end;
	CVnem.ItemIndex	:= vanem;
end;

procedure TJaratbeDlg.KmHozzaadas(Mresz, Mkmora : TMaskEdit );
var
	hozza	: double;
begin
   // A hozz�adott km. ellen�rz�se
   if Mresz.Text <> '' then begin
   	hozza	:= StringSzam(Query_Select('GEPKOCSI', 'GK_RESZ', Mresz.Text, 'GK_HOZZA'));
       if hozza > 0 then begin
       	if StringSzam(Mkmora.Text) <> 0 then begin
				if StringSzam(Mkmora.Text) < hozza then begin
               	Mkmora.Text	:= Format('%.0f', [hozza + StringSZam(Mkmora.Text)]);
               end;
           end;
       end;
   end;
end;

procedure TJaratbeDlg.BitBtn2Click(Sender: TObject);
var
	mskod		: string;
	jaratszam	: string;
	szurosor, indexsor	: integer;
	sorsz		: integer;
	vanem		: integer;
  LANID: integer;
  KELLLANID:integer;
  mssorsz, szfajko,mbrensz,mbkod,fojar: string;
   mbk, S         :string;
   meguj       : TMegbizasbeDlg;
   SajatMegbizas: boolean;
begin

  if FAJKO_ellenor then Exit;

  if (cbFAJKO.ItemIndex>0)and(TabbedNotebook1.PageIndex=1) then
  begin
		NoticeKi('NEM norm�l j�rat!!');
    exit;
  end;
  if (cbFAJKO.ItemIndex=0)and(TabbedNotebook1.PageIndex=2) then
  begin
		NoticeKi('Norm�l j�rat!!');
    exit;
  end;
  if MegbizasUjFmDlg<>nil then
  begin
		NoticeKi('A megb�z�s ablak m�r meg van nyitva!');
    Exit;
  end;

  if megtekint then
  begin
       mbk:=Query_Select('MEGBIZAS','MB_VIKOD',Query3.FieldByName('VI_VIKOD').AsString ,'MB_MBKOD');
       Application.CreateForm(TMegbizasbeDlg, meguj);
       meguj.Tolto('Megbizas megjelen�t�se', mbk);
       meguj.ShowModal;
       meguj.Destroy;
    exit;
  end;

	// Egy �j megb�z�s beolvas�sa a j�rathoz
	if M4.Text = '' then begin
		NoticeKi('A rendsz�m nem lehet �res!');
		TabbedNotebook1.PageIndex	:= 0;
		M4.Setfocus;
		Exit;
	end;
	// Be�ll�tjuk a rendsz�mot �s hogy ne legyen j�ratsz�ma
	// A j�rat sz�r�s be�ll�t�sa

  fajko:=cbFAJKO.ItemIndex;
  if fajko=0 then
    szfajko:=' and MS_FAJKO=0'
  else
    szfajko:=' and MS_FAJKO>0'  ;
	EgyebDlg.SzuroGrid.RowCount := EgyebDlg.SzuroGrid.RowCount + 1;
	szurosor := EgyebDlg.SzuroGrid.RowCount - 1 ;
	EgyebDlg.SzuroGrid.Cells[0,szurosor] 	:= '113';
  if  CheckBox2.Checked then
	  EgyebDlg.SzuroGrid.Cells[1,szurosor] 	:= 'MS_RENSZ = '''+M4.Text+''' and ((MS_LETDAT>='''+M2.Text+''' and MS_LETDAT<='''+M3.Text+''')or(MS_FETDAT>='''+M2.Text+''' and MS_FETDAT<='''+M3.Text  +'''))'+szfajko
  else
	  EgyebDlg.SzuroGrid.Cells[1,szurosor] 	:= 'MS_RENSZ = '''+M4.Text+''''+szfajko ;

	EgyebDlg.IndexGrid.RowCount := EgyebDlg.IndexGrid.RowCount + 1;
	indexsor := EgyebDlg.IndexGrid.RowCount - 1 ;
	EgyebDlg.IndexGrid.Cells[0,indexsor] 	:= '113';
  EgyebDlg.IndexGrid.Cells[1,indexsor] 	:= ' MS_DATUM, MS_IDOPT ';


  EgyebDlg.stralap:=True;
	Application.CreateForm(TMegbizasUjFmDlg, MegbizasUjFmDlg);
  MegbizasUjFmDlg.Query1.RecordCount;
 // MegbizasUjFmDlg.alap:=True;
	MegbizasUjFmDlg.valaszt := true;
	MegbizasUjFmDlg.ShowModal;

	mskod := MegbizasUjFmDlg.ret_mskod;
  mssorsz:=MegbizasUjFmDlg.ret_mssorsz;
  mbrensz:=MegbizasUjFmDlg.ret_rendsz;
  mbkod:=MegbizasUjFmDlg.ret_mbkod;
	EgyebDlg.SzuroGrid.RowCount  := 1;
	EgyebDlg.SzuroGrid.Rows[0].Clear;

	MegbizasUjFmDlg.Destroy;
  MegbizasUjFmDlg:=nil;
	// A sz�r�s t�rl�se

	GridTorles(EgyebDlg.SzuroGrid, szurosor+1);
	GridTorles(EgyebDlg.SzuroGrid, szurosor);
	GridTorles(EgyebDlg.IndexGrid, indexsor+1);
	GridTorles(EgyebDlg.IndexGrid, indexsor);

	if mskod = '' then begin
		Exit;
	end;
	// Ellen�rizz�k a megb�z�st
 	//Query_Run(Query1, 'SELECT * FROM MEGBIZAS, MEGSEGED WHERE MS_MBKOD = MB_MBKOD AND MS_MSKOD = '+mskod);
 	Query_Run(Query1, 'SELECT * FROM MEGBIZAS, MEGSEGED WHERE MS_MBKOD = MB_MBKOD AND MS_MBKOD = '+mbkod);
  if (fajko=0)and( Query1.FieldByName('MB_RENSZ').AsString <> M4.Text) then begin
		if NoticeKi('A kiv�lasztott megb�z�s rendsz�ma nem egyezik a j�rat rendsz�mmal. Folytatja?', NOT_QUESTION) <> 0 then begin
			Exit;
		end;
  end;
	if (fajko=0)and(Query1.FieldByName('MB_JAKOD').AsString <> '') then begin
   if Query1.FieldByName('MB_JAKOD').AsString<>M20.Text then  // M�s a j�ratsz�m
   begin
		NoticeKi('A kiv�lasztott megb�z�s j�ratsz�ma nem �res!');
		Exit;
   end
   else begin // egyezik a j�ratsz�m
      // nem engedj�k tov�bb itt, mert az a megb�z�s �s a viszonylat+j�rat kapcsolat elveszt�s�hez vezet
	  	{if NoticeKi('A kiv�lasztott megb�z�s j�ratsz�ma nem �res! Folytatja?', NOT_QUESTION) = -1 then begin
        exit;
        end
      else begin
        S:='Megb�z�s - nem �res j�ratsz�m - elfogadva. MBKOD='+mbkod+', J�ratsz�m:'+ Query1.FieldByName('MB_JAKOD').AsString;
        S:=S+', r�gi VIKOD='+Query1.FieldByName('MB_VIKOD').AsString;
        NaploKiiro(S);
        end;
        }
     	NoticeKi('A kiv�lasztott megb�z�s j�ratsz�ma nem �res!');
      Exit;
      end;
	end;
	if (fajko>0)and(Query1.FieldByName('MS_JAKOD').AsString <> '') then begin
		NoticeKi('A kiv�lasztott megb�z�s j�ratsz�ma nem �res!');
		Exit;
	end;
	// Ellen�rizni kell, hogy a megb�z�shoz l�tezik-e LAN_ID
//	Query_Run( QueryKoz, 'SELECT VE_LANID FROM VEVO WHERE V_KOD= '+Query3.FieldByName('VI_VEKOD').AsString, true );
  if Query1.FieldByName('MB_VEKOD').AsString <> '' then begin
  	Query_Run( QueryKoz, 'SELECT VE_LANID FROM VEVO WHERE V_KOD= '+Query1.FieldByName('MB_VEKOD').AsString, true );
  	KELLLANID:= QueryKoz.FieldByName('VE_LANID').AsInteger;
    end
  else begin
    KELLLANID:= 0;
    end;

  GKKATEG:='';
  GKKATEG:= MaskEdit9.Text;
  if (GKKATEG='')and( Query1.FieldByName('MB_ALV').AsInteger=1) then // Alv�llalkoz�
  begin
    GKKATEG:= Query_Select('ALGEPKOCSI','AG_RENSZ',Query1.FieldByName('MB_RENSZ').AsString,'AG_GKKAT' );
  end;
	if (EgyebDlg.VANLANID > 0)and(KELLLANID=1) then begin
		LANID:=StrToIntDef(Query1.FieldByName('MB_FUTID').AsString,0);
		if LANID = 0 then begin
			if NoticeKi('A megb�z�shoz m�g nincs LAN ID kiv�lasztva! Meg k�v�nja adni?', NOT_QUESTION) = 0 then begin
				Application.CreateForm(TMegbizasbeDlg, MegbizasbeDlg);
				MegbizasbeDlg.KELL_LAN_ID	:= true;
				MegbizasbeDlg.Tolto('Megb�z�s karbantart�sa', Query1.FieldByName('MB_MBKOD').AsString);
				MegbizasbeDlg.ShowModal;
				Query_Run(Query1, 'SELECT * FROM MEGBIZAS, MEGSEGED WHERE MS_MBKOD = MB_MBKOD AND MS_MSKOD = '+mskod);
				MegbizasbeDlg.Destroy;
				if StrToIntDef(Query1.FieldByName('MB_FUTID').AsString,0) = 0 then begin
					NoticeKi('Nem lett LAN ID megadva!');
					Exit;
				end;
			end;
		end
    else  // van lanid
    begin
    	Query_Run(Query2, 'SELECT FT_GKKAT FROM FUVARTABLA WHERE FT_FTKOD = '+IntToStr(LANID));
      GKKATEG:= Query2.FieldByName('FT_GKKAT').AsString;
    end;
	end;
	// L�trehozzuk a viszony-t �s hozz�rendelj�k a j�rathoz
	// jaratszam	:= M01.Text + '-'+ M02.Text;
   jaratszam   := GetJaratszam(M20.Text);

  if fajko=0 then begin
  	Application.CreateForm(TViszonybeDlg, ViszonybeDlg);
	  ViszonybeDlg.ToltMegbizas(Query1.FieldByName('MB_MBKOD').AsString ,M20.Text);
    if fajko=0 then
  	  ViszonybeDlg.ShowModal
    else
      ViszonybeDlg.BitElkuld.OnClick(self);
	  if ViszonybeDlg.ret_kod <> '' then begin
		// L�trej�tt az �j viszonylat, bejegyezz�k a megb�z�sba is

    if fajko=0 then
		Query_Update( Query2, 'MEGBIZAS', [
			'MB_JAKOD', ''''+M20.Text+'''',
			'MB_JASZA', ''''+jaratszam+'''',
			'MB_VIKOD', ''''+ViszonybeDlg.ret_kod+''''
		], ' WHERE MB_MBKOD = '+Query1.FieldByName('MB_MBKOD').AsString);     // csak ha norm�l
    EgyebDlg.MegbizasDebug (mbkod, 'BitBtn2_Update ut�n 1.');

    if fajko=0 then
  		Query_Update( Query2, 'MEGSEGED', [
			'MS_JAKOD', ''''+M20.Text+'''',
			'MS_JASZA', ''''+jaratszam+''''
	  	], ' WHERE MS_MBKOD = '+Query1.FieldByName('MB_MBKOD').AsString+' and MS_FAJKO=0');    //

    if fajko>0 then
  		Query_Update( Query2, 'MEGSEGED', [
			'MS_JAKOD', ''''+M20.Text+'''',
			'MS_JASZA', ''''+jaratszam+''''
	  	], ' WHERE MS_MBKOD = '+Query1.FieldByName('MB_MBKOD').AsString+' and MS_FAJKO>0'+' and MS_SORSZ='+mssorsz);    //

    EgyebDlg.MegbizasDebug (mbkod, 'BitBtn2_Update ut�n 2.');
    if fajko=0 then
  		MegbizasSzamlaTolto(Query1.FieldByName('MB_MBKOD').AsString);
    EgyebDlg.MegbizasDebug (mbkod, 'BitBtn2_Update ut�n 3.');
		// A megbizasok hozz�kapcsol�sa a j�rathoz
		Query_Run( Query2,  'SELECT COUNT(*) DB FROM JARATMEGBIZAS WHERE JM_JAKOD = '''+M20.Text+''' ');
		sorsz	:= StrToIntDef(Query2.FieldByName('DB').AsString, 1) + 1;
		Query_Run( Query2,  'SELECT DISTINCT MB_JAKOD, MS_MBKOD, MS_SORSZ FROM MEGSEGED, MEGBIZAS WHERE MS_MBKOD = MB_MBKOD AND MB_JAKOD <> '''' '+
			' AND MB_MBKOD = '+Query1.FieldByName('MB_MBKOD').AsString+' ORDER BY MB_JAKOD, MS_MBKOD, MS_SORSZ ', TRUE);
		while not Query2.Eof do begin
			Query_Insert (QueryUj3, 'JARATMEGBIZAS',
				[
				'JM_JAKOD', ''''+Query2.FieldByName('MB_JAKOD').AsString+'''',
				'JM_MBKOD', Query2.FieldByName('MS_MBKOD').AsString,
				'JM_SORSZ', IntToStr(sorsz),
				'JM_ORIGS', Query2.FieldByName('MS_SORSZ').AsString
				]);
			Inc(sorsz);
			Query2.Next;
		end;
    EgyebDlg.MegbizasDebug (mbkod, 'BitBtn2_Update ut�n 4.');
		FelrakoTolto('1');
		if not FuvardijTolto(M20.Text) then begin
			NoticeKi('A fuvard�j be�ll�t�sa nem siker�lt!');
		end else begin
			// A  fuvard�j beolvas�sa
			Query_Run (Query2, 'SELECT * FROM ALJARAT WHERE AJ_JAKOD = '''+M20.Text+''' ',true);
			MaskEdit10.Text:= SzamString(StringSzam(Query2.FieldByName('AJ_FUDIJ').AsString),10,2);
			vanem	:= vnemlist.IndexOf(Query2.FieldByName('AJ_FUVAL').AsString);
			if vanem < 0 then begin
				vanem	:= 0;
			end;
			CVnem.ItemIndex		:= vanem;
		end;
    end;
  	ViszonybeDlg.Destroy;
	  Query3.Close;
  	Query3.Open;
    Query3.Last;
	  //GombEllenor;
	end
  else  // fajko>0
  begin

  Try
      //jaratszam	:= JARAT3JA_ORSZ.Value + '-'+ JARAT3JA_ALKOD.AsString;
      //mskod:= Query_Select2('MEGSEGED','MS_MBKOD','MS_SORSZ',mbkod,mssorsz,'MS_MSKOD');
      Query_Run(Query2, 'select mb_jakod from megbizas where mb_mbkod='+mbkod );
      fojar:=Query2.fieldbyname('MB_JAKOD').AsString;
      //jakod:= JARAT3JA_KOD.AsString;

  		if not Query_Update( Query2, 'MEGSEGED', [
			'MS_JAKOD', ''''+M20.Text+'''',
			'MS_JASZA', ''''+jaratszam+''''
	  	], ' WHERE MS_MBKOD = '+mbkod+' and MS_FAJKO>0'+' and MS_SORSZ='+mssorsz) then Raise Exception.Create('');    //

      // A megbizasok hozz�kapcsol�sa a j�rathoz
      if not Query_Insert (Query2, 'JARATMEGSEGED',[
          				'JG_FOJAR', ''''+fojar+'''',
          				'JG_JAKOD', ''''+ret_kod+'''',
          				'JG_MBKOD', mbkod,
          				'JG_MSKOD', mskod
          				]) then Raise Exception.Create('');
      Query_Run(QSeged,'SELECT * FROM JARATMEGSEGED, MEGSEGED WHERE JG_JAKOD = '''+ret_kod+''' AND JG_MSKOD = MS_MSKOD ');
  Except
  		NoticeKi('A megb�z�s csatol�sa nem siker�lt!');
  END;


  end;
	GombEllenor;
  /// Automatikus sz�mla k�sz�t�s

  EgyebDlg.AUTOSZAMLA:=True;
  // [2017-05-09 NagyP] Teljesen kikapcsolom itt az automatikus sz�mla k�sz�t�st, mert a megb�z�si d�j m�dos�t�sa eset�n nem
  // tudunk "ut�nany�lni", el�g ha a viszonylatban �t kell jav�tani a d�jat
  // [2017-05-18 NagyP] Vissza az eg�sz. Kell az automatikus sz�mla, de fuvard�j m�dos�t�skor t�r�lni fogja a sz�ml�t, a
  // viszonylatban pedig m�dos�tja
  // EgyebDlg.AUTOSZAMLA:=False;


    // NP 2016-01-18
    // az alv�llakoz�i megb�z�sokat r�gz�t�s�kkor j�rathoz csatolj�k, ez m�g a teljes�t�st megel�z�en t�rt�nik.
    // Ilyenkor ne k�sz�lj�n automatikusan sz�mla.
    // if (fajko=0) and EgyebDlg.AUTOSZAMLA then begin
    SajatMegbizas:= (Query_Select('MEGBIZAS', 'MB_MBKOD', Query1.FieldByName('MB_MBKOD').AsString, 'MB_ALV') = '0');
    if (fajko=0) and EgyebDlg.AUTOSZAMLA and SajatMegbizas then begin
       BitBtn11.OnClick(self);       // Sz�mla gomb
       EgyebDlg.AUTOSZAMLA:=False;
    end;
   EgyebDlg.MegbizasDebug (mbkod, 'BitBtn2 v�g�n');
end;

procedure TJaratbeDlg.ViszonyEllenor(Mind:Boolean);
begin
	// Ellen�rizz�k, hogy a viszonyok d�tuma beleessen a j�rat d�tumokba
  if Mind then
  	Query3.First;
	if ( ( M2.Text <> '' ) and ( M3.Text <> '' ) ) then begin
       while not Query3.Eof do begin
           if (Query3.FieldByName('VI_BEDAT').AsString < M2.Text)and(Query3.FieldByName('VI_KIDAT').AsString > M3.Text) then begin
           	NoticeKi('J�rat �s a viszonylat id�intervalluma elt�r�! (A Megb�z�s �s a J�rat id�intervalluma val�sz�n�leg elt�r�.)');
           end;
           if Mind then
             Query3.Next
           else
             exit;
       end;
	end;
end;

function TJaratbeDlg.KoltsegEllenor : boolean;
var
	vankoltseg	: boolean;
	ujktgkod	: string;
   kellossz	: integer;
begin
	// Ellen�rizz�k ill. l�trehozzuk a k�telez� k�lts�get
	Result	:= false;
	if TabbedNotebook2.PageIndex = 1 then begin
		// Alv�llalkoz�i j�ratn�l nem kell k�lts�g ellen�rz�s?
		Exit;
	end;
  if M3.Text='' then
    exit;
	if M20.Text = '' then begin
		NoticeKi('Hi�nyz� j�ratk�d!');
       Exit;
   end;
	if M2.Text = '' then begin
   	NoticeKi('Hi�nyz� kezd� d�tum!');
       Exit;
	end;
   if M4.Text = '' then begin
   	NoticeKi('Hi�nyz� rendsz�m!');
		Exit;
	end;
	if 0 > ( StrToIntDef(M15.Text,0)-StrToIntDef(M14.Text,0) ) then begin
   	Exit;
  end;
  try
    Query4.First;
    vankoltseg	:= false;
    while ( ( not Query4.Eof ) and (not vankoltseg) ) do begin
       	if Query4.FieldByName('KS_LEIRAS').AsString = 'K�telez� k�lts�g' then begin
           	vankoltseg	:= true;
        end
        else begin
       		Query4.Next;
			  end;
		end;
    if not vankoltseg then begin
      // M�g nincs k�telez� k�lts�g, l�tre kell hozni
		  ujktgkod	:= IntToStr(GetNextCode('KOLTSEG', 'KS_KTKOD', 1, 0));
      if ((StrToIntDef(M15.Text,0)-StrToIntDef(M14.Text,0)) * 10) >0 then
			if not Query_Insert(Query1, 'KOLTSEG',
				[
				'KS_KTKOD', 	''''+ujktgkod+'''',
				'KS_JAKOD',     ''''+M20.Text+'''',
				'KS_DATUM',		''''+M2.Text+'''',
				'KS_IDO',		''''+M2A.Text+'''',
				'KS_KMORA',		'0',
				'KS_VALNEM',    ''''+'HUF'+'''',
				'KS_ARFOLY',	'1',
				'KS_MEGJ',		''''+'Automatikusan gener�lt k�telez� k�lts�g'+'''',
				'KS_RENDSZ',	''''+copy(M4.Text,1,10)+'''',
				'KS_LEIRAS',	''''+'K�telez� k�lts�g'+'''',
				'KS_UZMENY',	'0',
				'KS_ORSZA',	'''HU''',
				'KS_OSSZEG',	IntToStr(( StrToIntDef(M15.Text,0)-StrToIntDef(M14.Text,0)) * 10),
				'KS_TELE',      '0',
				'KS_TELES',     ''''+' N '+'''',
				'KS_AFASZ',		'0',
				'KS_AFAKOD',    '''103''',
				'KS_AFAERT',	'0',
				'KS_ERTEK',     IntToStr(( StrToIntDef(M15.Text,0)-StrToIntDef(M14.Text,0)) * 10),
				'KS_ATLAG', 	'0',
				'KS_FIMOD',		'''EGY�B''',   //KP volt
				'KS_TETAN',		'0',
				'KS_TIPUS',		'1'
				]) then begin
      //  Clipboard.Clear;
      //  Clipboard.AsText:=Query1.SQL.Text;
				NoticeKi('A k�telez� k�lts�g besz�r�sa nem siker�lt! Ezt k�zzel kell p�tolni!');
			end;
			Query4.Close;
			Query4.Open;
		end else begin      // van
		  // Ellen�rizz�k a rendsz�mot?
		  if Query4.FieldByName('KS_RENDSZ').AsString <> copy(M4.Text,1,10) then begin
				// A rendsz�m cser�je !
				NoticeKi('Megv�ltozott a rendsz�m! A k�telez� k�lts�gben is m�dos�t�sra ker�l a rendsz�m!');
				Query_Run (EgyebDlg.QueryAl2, 'UPDATE KOLTSEG SET KS_RENDSZ = '''+copy(M4.Text,1,10)+''' '+
					' WHERE KS_KTKOD = '+Query4.FieldByName('KS_KTKOD').AsString);
		  end;
      // Ellen�rizz�k a d�tumot
      if (Query4.FieldByName('KS_DATUM').AsString < M2.Text) or ((M3.Text<>'')and(M3.Text< Query4.FieldByName('KS_DATUM').AsString)) then
      begin
				NoticeKi('Megv�ltozott a d�tum! A k�telez� k�lts�gben is m�dos�t�sra ker�l!');
				Query_Run (EgyebDlg.QueryAl2, 'UPDATE KOLTSEG SET KS_DATUM = '''+M2.Text+''' '+
					' WHERE KS_KTKOD = '+Query4.FieldByName('KS_KTKOD').AsString);
      end;
		  // Ellen�rizz�k az �sszeget?
		  kellossz	:= ( StrToIntDef(M15.Text,0)-StrToIntDef(M14.Text,0)) * 10;
		  if StrToIntDef(Query4.FieldByName('KS_OSSZEG').AsString,0) <> kellossz then begin
				if NoticeKi('Figyelem! �rv�nytelen k�telez� k�lts�g �sszeg! Jav�tsam?', NOT_QUESTION) = 0 then begin
					Query_Run (EgyebDlg.QueryAl2, 'UPDATE KOLTSEG SET KS_OSSZEG = '+IntToStr(kellossz)+','+
						' KS_ERTEK = '+IntToStr(kellossz)+' WHERE KS_KTKOD = '+Query4.FieldByName('KS_KTKOD').AsString);
					ujktgkod	:= Query4.FieldByName('KS_KTKOD').AsString;
					Query4.Close;
					Query4.Open;
					Query4.Locate('KS_KTKOD', ujktgkod, []);
				end;
		  end;
		end;
	except
  end;
  Result	:= true;
end;

procedure TJaratbeDlg.TabbedNotebook1Change(Sender: TObject;
  NewTab: Integer; var AllowChange: Boolean);
begin

  if FAJKO_ellenor then begin
      AllowChange:=False;
      Exit;
      end;
  if ujrekord and not elkuldes and (TabbedNotebook1.PageIndex=0) then
  begin
    if not Mentes then begin
      AllowChange:=False;
      exit;
    end;
    ujrekord:=False;
  end;
  if NewTab=1 then  //Megb�z�sok
  begin
    if cbFAJKO.ItemIndex=1 then
    begin
      AllowChange:=False;
      TabbedNotebook1.PageIndex:=2;
      exit;
    end;
    //Query3.First;
    Query3.Last;
    if Query3.Eof then exit;
    //DBGrid1.SetFocus;

    // [2017-05-09 NagyP] Teljesen kikapcsolom itt az automatikus sz�mla k�sz�t�st, mert a megb�z�si d�j m�dos�t�sa eset�n nem
    // tudunk "ut�nany�lni", el�g ha a viszonylatban �t kell jav�tani a d�jat
    // [2017-05-18 NagyP] Vissza az eg�sz. Kell az automatikus sz�mla, de fuvard�j m�dos�t�skor t�r�lni fogja a sz�ml�t, a
    // viszonylatban pedig m�dos�tja

    if EgyebDlg.AUTOSZAMLA and  (Query3.FieldByName('VI_UJKOD').AsString='') then begin
       BitBtn11.OnClick(self);
       end;
  end;

  if NewTab=2 then  //Megb�z�sok sorok
  begin
    if cbFAJKO.ItemIndex=0 then
    begin
      AllowChange:=False;
      TabbedNotebook1.PageIndex:=1;
      exit;
    end;
  end;

  if NewTab=3 then  //K�lts�gek
  begin
    KoltsegEllenor;
    // Koltsegimport(False);  innen kiveszem, 2016-11-03 NagyP
  end;

  if NewTab=5 then  //S�ly
  begin
    if (not elkuldes)and(not Mentes) then
    begin
      AllowChange:=False;
      exit;
    end;
  end;
end;

procedure TJaratbeDlg.BitBtn6Click(Sender: TObject);
begin
	// A k�lts�g �sszes�t�s megh�v�sa a j�ratk�ddal + ellen�rizhet�s�g
	if GetRightTag(340) = RG_NORIGHT then begin
		Exit;
	end;
	Screen.Cursor				:= crHourGlass;
	Application.CreateForm(TOssznyilDlg, OssznyilDlg);
	Screen.Cursor				:= crDefault;
	OssznyilDlg.M0.Text			:= ret_kod;
	OssznyilDlg.M1.Text     	:= M01.Text + '-'+M02.Text;
	OssznyilDlg.M1.Enabled		:= false;
	OssznyilDlg.BitBtn2.Enabled	:= false;
	OssznyilDlg.BitBtn3.Enabled	:= false;
	OssznyilDlg.BitBtn5.Enabled	:= false;
	OssznyilDlg.BitBtn8.Enabled	:= false;
	OssznyilDlg.BitBtn7.Enabled	:= false;
	OssznyilDlg.BitBtn32.Enabled	:= false;
	OssznyilDlg.MD1.Enabled		:= false;
	OssznyilDlg.MD2.Enabled		:= false;
	OssznyilDlg.ShowModal;
	OssznyilDlg.Destroy;
end;

procedure TJaratbeDlg.BitBtn9Click(Sender: TObject);
begin
	// Ha van megadva alv�llalkoz�, akkor az alv��llalkoz�i g�pkocsikb�l v�lasztunk
  if EgyebDlg.v_evszam<'2013' then
  	Query_Run(QueryKOZ,'SELECT * FROM ALVALLAL WHERE UPPER(V_NEV) LIKE '''+UpperCase(MaskEdit4.Text)+''' ',true)
  else
  begin
	  Query_Run(QueryKOZ,'SELECT * FROM VEVO WHERE V_KOD= '''+alvalkod+''' ',true);
  	if QueryKoz.RecordCount = 0 then
	    Query_Run(QueryKOZ,'SELECT * FROM VEVO WHERE UPPER(V_NEV) LIKE '''+UpperCase(MaskEdit4.Text)+''' ',true);
  end;
	if QueryKoz.RecordCount > 0 then begin
		AlGepkocsiValaszto(MaskEdit5,QueryKoz.FieldByName('V_KOD').AsString, GK_TRAKTOR);
	end else begin
		// Itt alv�llalkoz�i g�pkocsi rendsz�mot v�lasztunk ki
		Application.CreateForm(TAlGepFmDlg,AlGepFmDlg);
		AlGepFmDlg.valaszt	:= true;
		AlGepFmDlg.ShowModal;
		if AlGepFmDlg.ret_vkod <> '' then begin
			MaskEdit5.Text	:= AlGepFmDlg.ret_vnev;
		end;
		AlGepFmDlg.Destroy;
	end;
end;

procedure TJaratbeDlg.RadioButton1Click(Sender: TObject);
begin
	if RadioButton1.Checked then begin
		TabbedNotebook2.PageIndex	:= 0;
	end else begin
		TabbedNotebook2.PageIndex	:= 1;
	end;
end;

procedure TJaratbeDlg.Button1Click(Sender: TObject);
var
	i : integer;
begin
	// Be�ll�tjuk a j�rat t�pus�t �s ut�na nem engedj�k m�r v�ltoztatni
	Szerkesztheto(true);
	if RadioButton1.Checked then begin
		TabbedNotebook2.PageIndex	:= 0;
		alval_jarat					:= false;
		Panel18.Caption				:= 'Saj�t j�rat'+jaratfajta;
		M4.SetFocus;
	end else begin
		TabbedNotebook2.PageIndex	:= 1;
		alval_jarat					:= true;
		Panel18.Caption				:= 'Alv�llalkoz�i megb�z�s';
		MaskEdit4.SetFocus;
		// Felel�s be�ll�t�sa
		{for i := 0 to CBfelelos.Items.Count - 1 do begin
			if CBfelelos.Items[i] = EgyebDlg.user_name then begin
				CBfelelos.ItemIndex	:= i;
				CBFelelosChange(Sender);
			end;}
    for i := 0 to CBfelelos.Items.Count - 1 do begin
			if felelos_fekodlist[i] = EgyebDlg.user_code then begin  // k�d alapj�n szeretj�k
				CBfelelos.ItemIndex	:= i;
				CBFelelosChange(Sender);
			  end;  // if
		  end;  // for
	end;
	Panel17.Hide;
	Panel18.Show;
end;

procedure TJaratbeDlg.Szerkesztheto(irhato : boolean);
begin
	if irhato then begin
		Panel7.Enabled 		:= true;
		Panel6.Enabled 		:= true;
		Panel9.Enabled 		:= true;
		Panel11.Enabled 	:= true;
		GroupBox2.Enabled 	:= true;
		GroupBox3.Enabled 	:= true;
		GroupBox6.Enabled 	:= true;
		BitElkuld.Enabled 	:= true;
		BitBtn21.Enabled 	:= true;
		BitBtn6.Enabled 	:= true;
		BitBtn32.Enabled 	:= true;
	end else begin
		Panel7.Enabled 		:= false;
		Panel6.Enabled 		:= false;
		Panel9.Enabled 		:= false;
		Panel11.Enabled 	:= false;
		GroupBox2.Enabled 	:= false;
		GroupBox3.Enabled 	:= false;
		GroupBox6.Enabled 	:= false;
		BitElkuld.Enabled 	:= false;
		BitBtn21.Enabled 	:= false;
		BitBtn6.Enabled 	:= false;
		BitBtn32.Enabled 	:= false;
	end;
end;

procedure TJaratbeDlg.MaskEdit5Change(Sender: TObject);
begin
	M4.Text	:= MaskEdit5.Text;
	Modosit(Sender);
end;

procedure TJaratbeDlg.CBOrszChange(Sender: TObject);
begin
	M01.Text	:= CBOrsz.Text;
end;

procedure TJaratbeDlg.Query4CalcFields(DataSet: TDataSet);
begin
	Query4VISZONY.AsString	:= '';
	if Query4.FieldByName('KS_VIKOD').AsString <> '' then begin
		Query_Run(Query2, 'SELECT VI_HONNAN, VI_HOVA FROM VISZONY WHERE VI_VIKOD = '''+Query4.FieldByName('KS_VIKOD').AsString+''' ');
		if Query2.RecordCount > 0 then begin
			Query4VISZONY.AsString	:= Query2.FieldByName('VI_HONNAN').AsString+ '-'+Query2.FieldByName('VI_HOVA').AsString;
		end;
	end;
end;

procedure TJaratbeDlg.QueryCMRCalcFields(DataSet: TDataSet);
begin
	QueryCMRVISZONY.AsString	:= '';
	if QueryCMR.FieldByName('CM_VIKOD').AsString <> '' then begin
		Query_Run(Query2, 'SELECT VI_HONNAN, VI_HOVA FROM VISZONY WHERE VI_VIKOD = '''+QueryCMR.FieldByName('CM_VIKOD').AsString+''' ');
		if Query2.RecordCount > 0 then begin
			QueryCMRVISZONY.AsString	:= Query2.FieldByName('VI_HONNAN').AsString+ '-'+Query2.FieldByName('VI_HOVA').AsString;
		end;
	end;
end;

procedure TJaratbeDlg.BitBtn10Click(Sender: TObject);
var
	i 			: integer;
	cmkodok     : string;
	jaratszam	: string;
	fosofor		: string;
	fosonev		: string;
	soforkodok	: string;
begin
	// �j CMR rekord hozz�f�z�se a j�rathoz
	Query_Run(Query2, 'SELECT * FROM JARSOFOR WHERE JS_JAKOD = '''+ret_kod+''' ORDER BY JS_SORSZ');
	if Query2.RecordCount < 1 then begin
		NoticeKi('Ehhez a j�rathoz m�g nincs sof�r hozz�rendelve');
		Exit;
	end;
	Application.CreateForm(TCmrFmDlg, CmrFmDlg);
	// A sof�rk�dok �sszegy�jt�se �s a sz�r�si felt�telek megad�sa
	CmrFmDlg.SzuresUrites;
	fosofor	:= Query2.FieldByName('JS_SOFOR').AsString;
	fosonev	:= Query_Select('DOLGOZO', 'DO_KOD', fosofor, 'DO_NAME');
	soforkodok	:= '';
	while not Query2.Eof do begin
		CmrFmDlg.SzuresHozzaadas('CM_DOKOD', Query2.FieldByName('JS_SOFOR').AsString);
		soforkodok	:= soforkodok + ''''+Query2.FieldByName('JS_SOFOR').AsString+''',';
		Query2.Next;
	end;
	soforkodok	:= soforkodok + '''9999999''';
	CmrFmDlg.SzuresHozzaadas('CM_VIDAT', '');
	CmrFmDlg.SQL_ToltoFo(CmrFmDlg.GetOrderBy(true));
	// A sof�r�knek megfelel� CMR-ek megjelen�t�se
	CmrFmDlg.valaszt		:= true;
	CmrFmDlg.multiselect	:= true;
	CmrFmDlg.ShowModal;
	if CmrFmDlg.selectlist.Count > 0 then begin
		if CmrFmDlg.selectlist[0] <> '' then begin
			// A kiv�lasztott CMR-ek hozz�rendel�se a j�rathoz
			cmkodok := CmrFmDlg.selectlist[0]+',';
			for i := 1 to CmrFmDlg.selectlist.Count - 1 do begin
				cmkodok := cmkodok + CmrFmDlg.selectlist[i] + ',';
			end;
			cmkodok := cmkodok + '999999';
			Query_Run (Query2, 'SELECT * FROM CMRDAT WHERE CM_CMKOD IN ('+cmkodok+') AND '+
				' CM_JAKOD <> '''+ret_kod+''' AND CM_JAKOD IS NOT NULL AND CM_JAKOD <> '''' '+
				' AND CM_VIKOD <> '''' ' );
			if Query2.RecordCount > 0 then begin
//				NoticeKi('Figyelem! A kiv�lasztott elemek k�z�tt vannak m�s j�rathoz tartoz�k melyek nem ker�lhetnek �thelyez�sre!');
				NoticeKi('Figyelem! A kiv�lasztott elemek k�z�tt vannak m�r megb�z�sokhoz rendeltek, melyek nem ker�lhetnek �thelyez�sre!');
			end else begin
				Query_Run (Query2, 'SELECT * FROM CMRDAT WHERE CM_CMKOD IN ('+cmkodok+') AND '+
					' CM_DOKOD NOT IN ('+soforkodok+') AND CM_VIKOD <> '''' ' );
				if Query2.RecordCount > 0 then begin
//					NoticeKi('Figyelem! A kiv�lasztott elemek k�z�tt vannak m�s sof�r�kh�z tartoz�k vagy �resek, melyek nem ker�lhetnek �thelyez�sre!');
					NoticeKi('Figyelem! A kiv�lasztott elemek k�z�tt vannak m�r megb�z�sokhoz rendeltek, melyek nem ker�lhetnek �thelyez�sre!');
				end else begin
					// Az �res kiad�si d�tummal rendelkez� elemeknek be�ll�tjuk a kiad�si d�tum�t is
					Query_Run (Query2, 'UPDATE CMRDAT SET CM_KIDAT = '''+EgyebDlg.MaiDatum+''' WHERE CM_CMKOD IN ('+cmkodok+') AND CM_KIDAT = '''' OR CM_KIDAT IS NULL ');
					jaratszam	:= GetJaratszam(ret_kod);
					Query_Run (Query2, 'UPDATE CMRDAT SET CM_JAKOD = '''+ret_kod+''', CM_JARAT = '''+jaratszam+''', '+
						'CM_VIDAT = '''+EgyebDlg.MaiDatum+''' WHERE CM_CMKOD IN ('+cmkodok+') ');
					Query_Run (Query2, 'UPDATE CMRDAT SET CM_DOKOD = '''+fosofor+''', CM_DONEV = '''+fosonev+''' WHERE CM_CMKOD IN ('+cmkodok+') ' );
					// Friss�tj�k a n�zetet
					ModLocate(QueryCMR, 'CM_CMKOD', CmrFmDlg.selectlist[0] );
					GombEllenor;
				end;
			end;
		end;
	end;
	CmrFmDlg.SzuresUrites;
	CmrFmDlg.Destroy;
end;

procedure TJaratbeDlg.BitBtn16Click(Sender: TObject);
begin
	// CMR adatok m�dos�t�sa
	Application.CreateForm(TCmrbeDlg, CmrbeDlg);
	CmrbeDlg.Tolto('CMR adatok m�dos�t�sa', QueryCMR.FieldByName('CM_CMKOD').AsString);
	CmrbeDlg.BitBtn1.Enabled	:= false;
	CmrbeDlg.BitBtn2.Enabled	:= false;
	CmrbeDlg.ShowModal;
	if CmrbeDlg.ret_kod <> '' then begin
		ModLocate(QueryCMR, 'CM_CMKOD', QueryCMR.FieldByName('CM_CMKOD').AsString );
	end;
	CmrbeDlg.Destroy;
end;

procedure TJaratbeDlg.BitBtn17Click(Sender: TObject);
begin
	// CMR adatok t�rl�se
	if NoticeKi('Val�ban el akarja t�vol�tani a CMR rekordot a j�ratb�l?', NOT_QUESTION) <> 0 then begin
		Exit;
	end;
	Query_Run(Query2, 'UPDATE CMRDAT SET CM_JARAT = '''', CM_JAKOD = '''', CM_VIKOD = '''', CM_VIDAT = '''' WHERE CM_CMKOD = '''+QueryCMR.FieldByName('CM_CMKOD').AsString+''' ');
	ModLocate(QueryCMR, 'CM_CMKOD', QueryCMR.FieldByName('CM_CMKOD').AsString );
  GombEllenor;

  /// KG 20120410 CMR adatok sz�ml�ra �r�sa
  BitBtn37.OnClick(self);

end;

procedure TJaratbeDlg.BitBtn36Click(Sender: TObject);
begin
	if QueryCMRCM_VIKOD.AsString <> '' then begin
		{M�r van a CMR-hez megb�z�s -> annak t�rl�se}
		if NoticeKi('Val�ban t�rli a megb�z�s kapcsolatot?', NOT_QUESTION) = 0 then begin
			Query_Run (Query2, 'UPDATE CMRDAT SET CM_VIKOD = '''' WHERE CM_CMKOD = '+QueryCMR.FieldByName('CM_CMKOD').AsString,true);
			ModLocate(QueryCMR, 'CM_CMKOD', QueryCMR.FieldByName('CM_CMKOD').AsString );
      /// KG 20120410 CMR adatok sz�ml�ra �r�sa
      BitBtn37.OnClick(self);
		end;
	end else begin
		{Kiv�lasztjuk a j�rathoz kapcsol�d� megrendel�t, ha t�bb van}
		Application.CreateForm(TRendvalDlg,RendvalDlg);
		RendvalDlg.Tolt('A megb�z�s kiv�laszt�sa',M20.Text);
		if RendvalDlg.vanrek then begin
			RendvalDlg.ShowModal;
			if RendvalDlg.ret_vikod <> '' then begin
				{A megb�z�s hozz�rendel�se a k�lts�ghez}
				Query_Run (Query2, 'UPDATE CMRDAT SET CM_VIKOD = '''+RendvalDlg.ret_vikod
					+''' WHERE CM_CMKOD = '+QueryCMR.FieldByName('CM_CMKOD').AsString,true);
				ModLocate(QueryCMR, 'CM_CMKOD', QueryCMR.FieldByName('CM_CMKOD').AsString );

        /// KG 20120410 CMR adatok sz�ml�ra �r�sa
        BitBtn37.OnClick(self);
			end;
		end;
		RendvalDlg.Destroy;
	end;
end;

procedure TJaratbeDlg.BitBtn37Click(Sender: TObject);
var
	cmrszamok	: string;
	cmrdb		: integer;
begin
	// A CMR sz�mok be�r�sa a sz�ml�ba
	if StrToIntDef(Query3.FieldByName('VI_UJKOD').AsString, 0) <> 0 then begin
		{Meg kell vizsg�lni, hogy a sz�mla v�gleges�tett-e}
		Query_Run(Query2, 'SELECT * FROM SZFEJ WHERE SA_UJKOD = '+Query3.FieldByName('VI_UJKOD').AsString,true);
		if Query2.FieldByname('SA_FLAG').AsString <> '1' then begin
			NoticeKi('A sz�mla m�r nem m�dos�that�!!!');
			Exit;
		end;
		if NoticeKi('Val�ban fel�rja a CMR adatokat a sz�ml�ra?', NOT_QUESTION)  <> 0 then begin
			Exit;
		end;
		// Bet�ltj�k a viszonyhoz kapcsol�d� CMR sz�mokat
		cmrszamok	:= '';
		cmrdb		:= 0;
		if Query_Run(Query2, 'SELECT CM_CSZAM FROM CMRDAT WHERE CM_VIKOD = '''+Query3.FieldByName('VI_VIKOD').AsString+''' ORDER BY CM_CSZAM ',true) then begin
			Query2.First;
			while not Query2.EOF do begin
				cmrszamok	:= cmrszamok + Query2.FieldByName('CM_CSZAM').AsString+ ',';
				Inc(cmrdb);
				Query2.Next;
			end;
			if Length(cmrszamok) > 1 then begin
				cmrszamok := copy(cmrszamok, 1, Length(cmrszamok) - 1);
			end;
		end;
		if cmrdb > 0 then
			cmrszamok	:= IntToStr(cmrdb) + ' db CMR : ' +cmrszamok
    else
			cmrszamok	:='';

		Query_Run(Query2, 'UPDATE SZFEJ SET SA_MELL5 = '''+cmrszamok+
				''' WHERE SA_UJKOD = '+Query3.FieldByName('VI_UJKOD').AsString,true);
	end;
end;

procedure TJaratbeDlg.GepkocsiOlvaso;
var
	sorsz	: integer;
	ujsor	: integer;
	kmdij	: string;
	regiev	: string;
begin
	if M4.Text <> '' then begin
		{A dolgoz�k �s a d�jak beolvas�sa}
		Query_Run(QueryKOZ,'SELECT * FROM DOLGOZO WHERE DO_RENDSZ = '''+M4.Text+''' AND DO_ARHIV < 1 ',true);
		if QueryKOZ.RecordCount > 0 then begin
			try
        //  furcsa hib�k voltak, a lenti 'INSERT INTO JARSOFOR' PK_Violation-t dobott.
				// if Query8.RecordCount > 0 then begin
        if StrToIntDef( Query_Select('JARSOFOR','JS_JAKOD',ret_kod,'count(JS_JAKOD)'),0) > 0 then  begin// van sof�r hozz�rendel�s
					if NoticeKi('Figyelem a sof�r adatok t�rl�sre ker�lnek!!! Folytatja?', NOT_QUESTION) = 0 then begin
						Query8.Close;
						Query_Run(Query8, 'DELETE FROM JARSOFOR WHERE JS_JAKOD = '''+ret_kod+''' ');
					end else begin
						Exit;
					end;
				end;
			except
        on E: exception do begin
           HibaKiiro('JaratbeDlg.GepkocsiOlvaso: '+E.Message+' at block 1');
           end;  // on E
			end;
			// A sof�r adatok felvitele
			sorsz	:= 1;
			kmdij	:= Query_select('GEPKOCSI', 'GK_RESZ', M4.Text, 'GK_DIJ1');
			while not QueryKOZ.EOF do begin
				Query_Run ( Query8, 'INSERT INTO JARSOFOR (JS_JAKOD, JS_SOFOR, JS_SORSZ, JS_JADIJ ) VALUES ('''+ret_kod+''','''+
					QueryKOZ.FieldByName('DO_KOD').AsString+''', '+IntToStr(sorsz)+', '+SqlSzamString(StringSzam(kmdij),12,2)+' )',true);
				Inc(sorsz);
				QueryKoz.Next;
			end;
			QueryKOZ.Close;
		end;
		Query_Run(Query8, 'SELECT * FROM JARSOFOR WHERE JS_JAKOD = '''+ret_kod+''' ORDER BY JS_SORSZ');
		GombEllenor;

		// A kapcsol�d� p�tkocsik beolvas�sa
		Query_Run(QueryKOZ,'SELECT * FROM GEPKOCSI WHERE GK_RESZ = '''+M4.Text+''' ',true);
		if QueryKOZ.RecordCount > 0 then begin
			try
				if Query7.RecordCount > 0 then begin
					if NoticeKi('Figyelem a p�tkocsi adatok t�rl�sre ker�lnek!!!, Folytatja?', NOT_QUESTION) = 0 then begin
						Query7.Close;
						Query_Run(Query7, 'DELETE FROM JARPOTOS WHERE JP_JAKOD = '''+ret_kod+''' ');
						Query_Run(Query7, 'SELECT * FROM JARPOTOS WHERE JP_JAKOD = '''+ret_kod+''' ORDER BY JP_POREN');
					end else begin
						Exit;
					end;
				end;
			except
			end;
			{A p�tkocsi be�ll�t�sa, ha van}
			if QueryKOZ.FieldByName('GK_POKOD').AsString <> '' then begin
				{Van hozz� p�tkocsi -> az adatok bet�lt�se}
				if Query_Run (EgyebDlg.QueryKOZOS, 'SELECT * FROM GEPKOCSI WHERE GK_KOD = '''+
					QueryKOZ.FieldByName('GK_POKOD').AsString+''' AND GK_ARHIV < 1 ',true) then begin
					if EgyebDlg.QueryKOZOS.RecordCount > 0 then begin
						// A p�tkocsi adatait felvinni a t�bl�zatba
						Query7.Close;
						ujsor	:= GetNextCode('JARPOTOS', 'JP_JPKOD', 1, 0);
						Query_Insert ( Query7, 'JARPOTOS' , [
							'JP_JPKOD', IntToStr(ujsor),
							'JP_JAKOD', ''''+ret_kod+'''',
							'JP_POREN', ''''+EgyebDlg.QueryKOZOS.FieldByName('GK_RESZ').AsString+''''
						]);
						Query_Run(Query7, 'SELECT * FROM JARPOTOS WHERE JP_JAKOD = '''+ret_kod+''' ORDER BY JP_POREN');
					end;
				end;
				QueryKOZ.Close;
			end;
			CheckBox1.Checked 	:= true;
			M13.Setfocus;
		end;
		GombEllenor;

		{A g�pkocsi el�z� j�rat�nak befejez� km-�t behozni}
		M14.Text	:= '';
//	if Query_Run (Query1, 'SELECT JA_ZAROKM FROM JARAT WHERE JA_RENDSZ = '''+M4.Text+''' ORDER BY JA_ZAROKM ',true) then begin
		if Query_Run (Query1, 'SELECT JA_ZAROKM FROM JARAT WHERE JA_RENDSZ = '''+M4.Text+''' and JA_ZAROKM<>0 ORDER BY JA_KOD ',true) then begin
			if Query1.RecordCount > 0 then begin
				Query1.Last;
				M14.Text	:= Query1.FieldByname('JA_ZAROKM').AsString;
			end else begin
				// Pr�b�ljuk meg az el�z� �vb�l behozni a z�r� km-eket
				regiev              := EgyebDlg.v_evszam;
				EgyebDlg.v_evszam 	:= IntToStr(StrToIntDef(EgyebDlg.EvSzam, 1) - 1);
				try
					EgyebDlg.AlapToltes;	// Bet�ltj�k az el�z� �vi
					if Query_Run (Query1, 'SELECT JA_ZAROKM FROM '+GetTableFullName(EgyebDlg.v_alap_db, 'JARAT')+' WHERE JA_RENDSZ = '''+M4.Text+''' ORDER BY JA_ZAROKM ',true) then begin
						Query1.Last;
						M14.Text	:= Query1.FieldByname('JA_ZAROKM').AsString;
					end;
				except
				end;
				EgyebDlg.v_evszam 	:= regiev;
				EgyebDlg.AlapToltes;	// Visszat�ltj�k az eredeti �vi
			end;
		end;
		M15.Text	:= '';
	end;
end;

procedure TJaratbeDlg.M4KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
	if Key = VK_RETURN then begin
		GepkocsiOlvaso;
		M13.SetFocus;
	end;
end;

procedure TJaratbeDlg.M27KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
	if Key = VK_RETURN then begin
		BitBtn18.SetFocus;
	end;
end;

procedure TJaratbeDlg.SpeedButton4Click(Sender: TObject);
var
	most	: integer;
begin
	// Eggyel feljebb rakjuk a megbizast
	most 	:= StrToIntDef(QueryFelrako.FieldByName('JM_SORSZ').AsString,0);
	if most > 1 then begin
		Query_Run(QueryFelrako2, 'UPDATE JARATMEGBIZAS SET JM_SORSZ = 999999 WHERE JM_JAKOD = '''+M20.Text+''' '+
			' AND JM_MBKOD = '+QueryFelrako.FieldByName('JM_MBKOD').AsString+' AND JM_SORSZ = '+IntToStr(most) ,true);
		Query_Run(QueryFelrako2, 'UPDATE JARATMEGBIZAS SET JM_SORSZ = '+IntToStr(most)+' WHERE JM_JAKOD = '''+M20.Text+''' '+
			' AND JM_SORSZ = '+IntToStr(most-1) ,true);
		Query_Run(QueryFelrako2, 'UPDATE JARATMEGBIZAS SET JM_SORSZ = '+IntToStr(most-1)+' WHERE JM_JAKOD = '''+M20.Text+''' '+
			' AND JM_SORSZ = '+'999999' ,true);
		FelrakoTolto(IntToStr(most-1));
	end;
end;

procedure TJaratbeDlg.SpeedButton5Click(Sender: TObject);
var
	most	: integer;
begin
	// Eggyel feljebb rakjuk a megbizast
	most 	:= StrToIntDef(QueryFelrako.FieldByName('JM_SORSZ').AsString,0);
	if most < QueryFelrako.RecordCount then begin
		QueryFelrako.Next;
		SpeedButton4Click(Sender);
		FelrakoTolto(IntToStr(most+1));
	end;
end;

procedure TJaratbeDlg.BitBtn40Click(Sender: TObject);
var
	dir0		: string;
	dir			: string;
	kereses		: TSearchRec;
	talalat		: integer;
	fileok		: TStringList;
	jarszam		: string;
  i: integer;
  ev: string;
begin
	// A j�rathoz tartoz� dokumentumok megjelen�t�se
	dir0	:= EgyebDlg.Read_SZGrid('999', '722');
  i:=Pos('20',dir0);
  ev:=copy(dir0,i,4);
  dir0:=StringReplace(dir0,ev,copy(m2.Text,1,4),[rfReplaceAll]	) ;
	if dir0 = '' then begin
		// Be kell olvasni a kezd�k�nyvt�rat
		dir	:= '';
		if SelectDirectory('A j�ratok k�nyvt�ra ...', '', dir ) then begin
			dir0	:= dir;
			// A k�nyvt�r be�r�sa a sz�t�rba
			if Length(dir0) > 120 then begin
				NoticeKi('T�l hossz� k�nyvt�rn�v : '+dir0);
				Exit;
			end;
			Query_Run(EgyebDlg.QueryKOZOS, 'UPDATE SZOTAR SET SZ_MENEV = '''+dir0+''' '+
				' WHERE SZ_FOKOD = ''999'' AND SZ_ALKOD = ''722'' ', TRUE);
			EgyebDlg.Fill_SZGrid;
		end else begin
			NoticeKi('Hiba a j�rat k�nyvt�r megad�s�n�l!');
			Exit;
		end;
	end else begin
		if not SysUtils.DirectoryExists(dir0) then begin
			NoticeKi('Nem l�tez� alapk�nyvt�r : '+dir0);
			Exit;
		end;
	end;
	// A DIR0 egy l�tez� k�nyvt�r
	if ( ( copy(dir0, Length(dir0), 0) <> '\' ) and ( copy(dir0, Length(dir0), 0) <> '/' ) ) then begin
		dir0	:= dir0 + '\';
	end;
	// Az �llom�ny nevek beolvas�sa
	jarszam		:= IntToStr(StrToIntDef(M02.Text,0));
	if jarszam = '0' then begin
		NoticeKi('�rv�nytelen j�ratsz�m!');
		Exit;
	end;
	fileok		:= TStringList.Create;
   talalat		:= FindFirst(dir0+jarszam+'.*', faAnyFile, kereses );
   //talalat		:= FindFirst(dir0+'*'+jarszam+'.*', faAnyFile, kereses );
	while talalat = 0 do begin
		fileok.Add(kereses.Name);
		talalat	:= FindNext(kereses);
	end;
	SysUtils.FindClose(kereses);
	if fileok.Count < 1 then begin
		NoticeKi('Nincs megfelel� �llom�ny a j�rathoz!');
		Exit;
	end;
	if fileok.Count = 1 then begin
		// �llom�ny megnyit�sa
		ShellExecute(Application.Handle,'open',PChar(dir0+fileok[0]), '', '', SW_SHOWNORMAL );
	end else begin
		// Kell egy lista az �llom�nyokkal, amiket meg lehet nyitni
		fileok.Sort;
		Application.CreateForm(TDoksiListaDlg, DoksiListaDlg);
		DoksiListaDlg.Tolto(dir0, fileok);
		DoksiListaDlg.ShowModal;
		DoksiListaDlg.Destroy;
	end;
end;

procedure TJaratbeDlg.BitBtn41Click(Sender: TObject);
begin
	// A fizet�si felt�telek beolvas�sa
	Application.CreateForm(TFizubeDlg, FizubeDlg);
	FizubeDlg.Tolto(fizu1, fizu2, fizu3, fisz1, fisz2, fisz3);
	FizubeDlg.ShowModal;
	if not FizubeDlg.kilepes then begin
		fizu1		:= FizubeDlg.M1.Text;
		fizu2		:= FizubeDlg.M2.Text;
		fizu3		:= FizubeDlg.M3.Text;
		fisz1		:= FizubeDlg.M4.Text;
		fisz2		:= FizubeDlg.M5.Text;
		fisz3		:= FizubeDlg.M6.Text;
		modosult	:= true;
	end;
	FizubeDlg.Destroy;
end;

procedure TJaratbeDlg.BitBtn42Click(Sender: TObject);
begin
	// Ha van megadva alv�llalkoz�, akkor az alv��llalkoz�i g�pkocsikb�l v�lasztunk
  if EgyebDlg.v_evszam<'2013' then
  	Query_Run(QueryKOZ,'SELECT * FROM ALVALLAL WHERE UPPER(V_NEV) LIKE '''+UpperCase(MaskEdit4.Text)+''' ',true)
  else
  begin
	  Query_Run(QueryKOZ,'SELECT * FROM VEVO WHERE V_KOD= '''+alvalkod+''' ',true);
  	if QueryKoz.RecordCount = 0 then
	    Query_Run(QueryKOZ,'SELECT * FROM VEVO WHERE UPPER(V_NEV) LIKE '''+UpperCase(MaskEdit4.Text)+''' ',true);
  end;
	if QueryKoz.RecordCount > 0 then begin
		AlGepkocsiValaszto(MaskEdit50,QueryKoz.FieldByName('V_KOD').AsString, GK_POTKOCSI);
	end else begin
		// Itt alv�llalkoz�i g�pkocsi rendsz�mot v�lasztunk ki
		Application.CreateForm(TAlGepFmDlg,AlGepFmDlg);
		AlGepFmDlg.valaszt	:= true;
		AlGepFmDlg.ShowModal;
		if AlGepFmDlg.ret_vkod <> '' then begin
			MaskEdit5.Text	:= AlGepFmDlg.ret_vnev;
		end;
		AlGepFmDlg.Destroy;
	end;
end;

procedure TJaratbeDlg.IdoEx(Sender: TObject);
begin
	IdoExit(Sender, false);
end;

procedure TJaratbeDlg.MaskEdit4Exit(Sender: TObject);
begin
	// Ha �res a le�r�s mez�, akkor t�lts�k be az alv�llalkoz� nev�t, ellenkez� esetben k�rd�s
	if MaskEdit4.Text <> '' then begin
		if cbM1.Text = '' then begin
			cbM1.Text	:= MaskEdit4.Text;
		end else begin
			if cbM1.Text <> MaskEdit4.Text then begin
				if NoticeKi('A jelenlegi le�r�s elt�r az alv�llalkoz� nev�t�l. Kicser�ljem?', NOT_QUESTION) = 0 then begin
					cbM1.Text	:= MaskEdit4.Text;
				end;
			end;
		end;
	end;
end;

procedure TJaratbeDlg.BitBtn43Click(Sender: TObject);
var
	mskod, mbkod: string;
	jaratszam	: string;
	szurosor	: integer;
	sorsz		: integer;
	vanem		: integer;		
begin
  if fajko<>0 then exit;
	// Egy �j megb�z�s beolvas�sa a j�rathoz
	if M4.Text = '' then begin
		NoticeKi('A rendsz�m nem lehet �res!');
		TabbedNotebook1.PageIndex	:= 0;
		M4.Setfocus;
		Exit;
	end;
	// Ellen�rizz�k, hogy a viszonylat hozz�tartozik-e m�r egy l�tez� megb�z�shoz.
	Query_Run(Query1, 'SELECT * FROM MEGBIZAS WHERE MB_VIKOD = '+Query3.FieldByName('VI_VIKOD').AsString+' AND MB_DATUM > '''+EgyebDlg.MaiDatum+''' ');
	if Query1.RecordCount > 0 then begin
		NoticeKi('Ez a viszonylat m�r hozz� van rendelve egy megb�z�shoz!');
		Exit;
	end;

	Application.CreateForm(TMegbizasUjFmDlg, MegbizasUjFmDlg);
	MegbizasUjFmDlg.valaszt := true;
	// Be�ll�tjuk a rendsz�mot �s hogy ne legyen j�ratsz�ma
	// A j�rat sz�r�s be�ll�t�sa
	EgyebDlg.SzuroGrid.RowCount 			:= EgyebDlg.SzuroGrid.RowCount + 1;
	szurosor								:= EgyebDlg.SzuroGrid.RowCount - 1 ;
	EgyebDlg.SzuroGrid.Cells[0,szurosor] 	:= '113';
	EgyebDlg.SzuroGrid.Cells[1,szurosor] 	:= 'MB_RENSZ = '''+M4.Text+''' ';
	EgyebDlg.SzuroGrid.RowCount 			:= EgyebDlg.SzuroGrid.RowCount + 1;
	EgyebDlg.SzuroGrid.Cells[0,szurosor+1] 	:= '113';
	EgyebDlg.SzuroGrid.Cells[1,szurosor+1] 	:= 'MB_JAKOD = '''' ';
	MegbizasUjFmDlg.ShowModal;
	mskod := MegbizasUjFmDlg.ret_mskod;
	// A sz�r�s t�rl�se
	GridTorles(EgyebDlg.SzuroGrid, szurosor+1);
	GridTorles(EgyebDlg.SzuroGrid, szurosor);
	MegbizasUjFmDlg.Destroy;
  MegbizasUjFmDlg:=nil;
	if mskod = '' then begin
		Exit;
	end;
	// Ellen�rizz�k a megb�z�st
	Query_Run(Query1, 'SELECT * FROM MEGBIZAS, MEGSEGED WHERE MS_MBKOD = MB_MBKOD AND MS_MSKOD = '+mskod);
	if Query1.FieldByName('MB_RENSZ').AsString <> M4.Text then begin
		if NoticeKi('A kiv�lasztott megb�z�s rendsz�ma nem egyezik a j�rat rendsz�mmal. Folytatja?', NOT_QUESTION) <> 0 then begin
			Exit;
		end;
	end;
	if Query1.FieldByName('MB_JAKOD').AsString <> '' then begin
		NoticeKi('A kiv�lasztott megb�z�s j�ratsz�ma nem �res!');
		Exit;
	end;
	if Query1.FieldByName('MB_VIKOD').AsString <> '' then begin
		NoticeKi('A kiv�lasztott megb�z�shoz m�r tartozik egy m�sik viszonylat!');
		Exit;
	end;
	// Ellen�rizni kell, hogy a megb�z�shoz l�tezik-e LAN_ID
	if EgyebDlg.VANLANID > 0 then begin
		if StrToIntDef(Query1.FieldByName('MB_FUTID').AsString,0) = 0 then begin
			if NoticeKi('A megb�z�shoz m�g nincs LAN ID kiv�lasztva! Meg k�v�nja adni?', NOT_QUESTION) = 0 then begin
				Application.CreateForm(TMegbizasbeDlg, MegbizasbeDlg);
				MegbizasbeDlg.KELL_LAN_ID	:= true;
				MegbizasbeDlg.Tolto('Megb�z�s karbantart�sa', Query1.FieldByName('MB_MBKOD').AsString);
				MegbizasbeDlg.ShowModal;
				Query_Run(Query1, 'SELECT * FROM MEGBIZAS, MEGSEGED WHERE MS_MBKOD = MB_MBKOD AND MS_MSKOD = '+mskod);
				MegbizasbeDlg.Destroy;
				if StrToIntDef(Query1.FieldByName('MB_FUTID').AsString,0) = 0 then begin
					NoticeKi('Nem lett LAN ID megadva!');
					Exit;
				end;
			end;
		end;
	end;
	// L�trehozzuk a viszony-t �s hozz�rendelj�k a j�rathoz
	jaratszam	:= M01.Text + '-'+ M02.Text;
  mbkod:= Query1.FieldByName('MB_MBKOD').AsString;
	// L�trej�tt az �j viszonylat, bejegyezz�k a megb�z�sba is
	Query_Update( Query2, 'MEGBIZAS', [
		'MB_JAKOD', ''''+M20.Text+'''',
		'MB_JASZA', ''''+jaratszam+'''',
		'MB_VIKOD', ''''+ Query3.FieldByName('VI_VIKOD').AsString +''''
	], ' WHERE MB_MBKOD = '+Query1.FieldByName('MB_MBKOD').AsString);

  EgyebDlg.MegbizasDebug (mbkod, 'Egyb�l update ut�n');

		Query_Update( Query2, 'MEGSEGED', [
			'MS_JAKOD', ''''+M20.Text+'''',
			'MS_JASZA', ''''+jaratszam+''''
		], ' WHERE MS_MBKOD = '+Query1.FieldByName('MB_MBKOD').AsString+' and MS_FAJKO=0');


	MegbizasSzamlaTolto(Query1.FieldByName('MB_MBKOD').AsString);
  EgyebDlg.MegbizasDebug (mbkod, 'Update ut�n 2.');
	// A megbizasok hozz�kapcsol�sa a j�rathoz
	Query_Run( Query2,  'SELECT COUNT(*) DB FROM JARATMEGBIZAS WHERE JM_JAKOD = '''+M20.Text+''' ');
	sorsz	:= StrToIntDef(Query2.FieldByName('DB').AsString, 1) + 1;
	Query_Run( Query2,  'SELECT DISTINCT MB_JAKOD, MS_MBKOD, MS_SORSZ FROM MEGSEGED, MEGBIZAS WHERE MS_MBKOD = MB_MBKOD AND MB_JAKOD <> '''' '+
		' AND MB_MBKOD = '+Query1.FieldByName('MB_MBKOD').AsString+' ORDER BY MB_JAKOD, MS_MBKOD, MS_SORSZ ', TRUE);
	while not Query2.Eof do begin
		Query_Insert (QueryUj3, 'JARATMEGBIZAS',
			[
			'JM_JAKOD', ''''+Query2.FieldByName('MB_JAKOD').AsString+'''',
			'JM_MBKOD', Query2.FieldByName('MS_MBKOD').AsString,
			'JM_SORSZ', IntToStr(sorsz),
			'JM_ORIGS', Query2.FieldByName('MS_SORSZ').AsString
			]);
		Inc(sorsz);
		Query2.Next;
	end;
	FelrakoTolto('1');
	if not FuvardijTolto(M20.Text) then begin
		NoticeKi('A fuvard�j be�ll�t�sa nem siker�lt!');
	end else begin
		// A  fuvard�j beolvas�sa
		Query_Run (Query2, 'SELECT * FROM ALJARAT WHERE AJ_JAKOD = '''+M20.Text+''' ',true);
		MaskEdit10.Text:= SzamString(StringSzam(Query2.FieldByName('AJ_FUDIJ').AsString),10,2);
		vanem	:= vnemlist.IndexOf(Query2.FieldByName('AJ_FUVAL').AsString);
		if vanem < 0 then begin
			vanem	:= 0;
		end;
		CVnem.ItemIndex		:= vanem;
	end;
	Query3.Close;
	Query3.Open;
	GombEllenor;
  EgyebDlg.MegbizasDebug (mbkod, 'Update ut�n 3.');
end;

procedure TJaratbeDlg.BitBtn44Click(Sender: TObject);
begin
	EgyebDlg.Calendarbe(M2B);
end;

procedure TJaratbeDlg.BitBtn45Click(Sender: TObject);
const
  LevelKellekekSzotarban = '373';
var
	aPDFFilt    : TQRPDFDocumentFilter;
	fn          : string;
	dir0		: string;
	dir			: string;
	body		: string;
	sorsz		: integer;
  UNYELV  : string;
begin
	// A j�rathoz tartoz� dokumentum elk�ld�se
	dir0	:= EgyebDlg.Read_SZGrid('999', '723');
	if dir0 = '' then begin
		// Be kell olvasni a kezd�k�nyvt�rat
		dir	:= '';
		if SelectDirectory('A PDF dokumentumok k�nyvt�ra ...', '', dir ) then begin
			dir0	:= dir;
			// A k�nyvt�r be�r�sa a sz�t�rba
			if Length(dir0) > 120 then begin
				NoticeKi('T�l hossz� k�nyvt�rn�v : '+dir0);
				Exit;
			end;
			Query_Run(EgyebDlg.QueryKozos, 'DELETE FROM SZOTAR WHERE SZ_FOKOD = ''999'' AND SZ_ALKOD = ''723'' ', FALSE);
			Query_Run(EgyebDlg.QueryKozos, 'INSERT INTO SZOTAR ( SZ_FOKOD, SZ_ALKOD, SZ_MENEV, SZ_LEIRO, SZ_ERVNY ) VALUES '+
				'( ''999'', ''723'', '''+dir0+''', ''PDF dokumentumok k�nyvt�ra'', 1 ) ', FALSE);
			EgyebDlg.Fill_SZGrid;
		end else begin
			NoticeKi('Hiba a PDF k�nyvt�r megad�s�n�l!');
			Exit;
		end;
	end else begin
		if not SysUtils.DirectoryExists(dir0) then begin
			NoticeKi('Nem l�tez� PDF k�nyvt�r : '+dir0);
			Exit;
		end;
	end;
	// A DIR0 egy l�tez� k�nyvt�r
	if ( ( copy(dir0, Length(dir0), 0) <> '\' ) and ( copy(dir0, Length(dir0), 0) <> '/' ) ) then begin
		dir0	:= dir0 + '\';
	end;
	if StrToIntDef(M02.Text,0) = 0 then begin
		NoticeKi('�rv�nytelen j�ratsz�m!');
		Exit;
	end;
	Screen.Cursor	:= crHourGlass;
	sorsz			:= 2;
	// fn				:= CBORSZ.Text + '-' +Format('%5.5d',[StrToIntDef(M02.Text,0)]) + '.PDF';
  fn:= CBORSZ.Text + '-' +Format('%5.5d',[StrToIntDef(M02.Text,0)])+ '_'+copy(M2.Text, 1, 4) + '.PDF';
	// Az �llom�ny nev�nek ellen�rz�se
	while FileExists(dir0+fn) do begin
		// fn	:= CBORSZ.Text + '-' +Format('%5.5d_%2.2d',[StrToIntDef(M02.Text,0), sorsz]) + '.PDF';
    fn:= CBORSZ.Text + '-' +Format('%5.5d',[StrToIntDef(M02.Text,0)])+ '_'+copy(M2.Text, 1, 4)+'_' + Format('%2.2d',[sorsz]) + '.PDF';
		Inc(sorsz);
	end;
	aPDFFilt 				:= TQRPDFDocumentFilter.Create(dir0+fn);
	aPDFFilt.CompressionOn 	:= true;
	Application.CreateForm(TFumegliDlg, FumegliDlg);
  FumegliDlg.STORNO:=STORNO;
  FumegliDlg.VALTOZAT:=sorsz-2;
	try
		FumegliDlg.Tolt(M20.Text);
		FumegliDlg.Rep.ExportToFilter(aPDFFilt);
		FumegliDlg.Destroy;
	finally
		aPDFFilt.free;
	end;
	// Ellen�rizz�k az �llom�ny l�trej�tt�t
	Screen.Cursor				:= crDefault;
	if not FileExists(dir0+fn) then begin
		NoticeKi('Figyelem! A PDF �llom�ny l�trehoz�sa nem siker�lt!');
		Exit;
	end;
	if emailcim = '' then begin
		// Az alv�llalkoz� emailc�m�nek megad�sa
		emailcim := InputBox('Email c�m megad�sa', 'Az alv�llalkoz� email c�me : ', '');
		if emailcim <> '' then begin
			// A c�m vissza�r�sa
      if EgyebDlg.v_evszam<'2013' then
  			Query_Run(EgyebDlg.QueryKozos, 'UPDATE ALVALLAL SET V_EMAIL = '''+copy(emailcim, 1, 60)+''' WHERE V_NEV = '''+MaskEdit4.Text+''' ', FALSE)
      else
	  		Query_Run(EgyebDlg.QueryKozos, 'UPDATE VEVO SET VE_EMAIL = '''+copy(emailcim, 1, 60)+''' WHERE V_KOD = '''+alvalkod+''' ', FALSE);
	  		//Query_Run(EgyebDlg.QueryKozos, 'UPDATE VEVO SET VE_EMAIL = '''+copy(emailcim, 1, 60)+''' WHERE V_NEV = '''+MaskEdit4.Text+''' ', FALSE);
		end;
	end;
  UNYELV:=Query_Select('VEVO', 'V_KOD', alvalkod, 'VE_UNYELV');
  if UNYELV='' then UNYELV:='HUN';  // ha valami�rt nem tal�lja
	// body	:= 'Tisztelt C�m,'+Chr(13)+Chr(10)+Chr(13)+Chr(10)+'Mell�kelten k�ldj�k a fuvaroz�si megb�z�st.';
  body:= EgyebDlg.Read_SZGrid(LevelKellekekSzotarban, UNYELV+'M2'); // 'Tisztelt C�m,'
  body:= body + Chr(13)+Chr(10)+Chr(13)+Chr(10);
  body:= Body + EgyebDlg.Read_SZGrid(LevelKellekekSzotarban, UNYELV+'M3'); // 'Mell�kelten k�ldj�k a fuvaroz�si megb�z�st.';
	// Email k�ld�se
	// CreateOutlookEmailAndOpen(emailcim, 'Fuvaroz�si megb�z�s', body, dir0+fn);
  CreateOutlookEmailAndOpen(emailcim, EgyebDlg.Read_SZGrid(LevelKellekekSzotarban, UNYELV+'M1'), body, dir0+fn);
	// A dokumentum adatainak t�rol�sa a CSATOLMANY t�bl�ban
  STORNOKULDVE:=True;
	sorsz	:= GetNextCode('CSATOLMANY', 'CS_CSKOD', 1, 0);
	Query_Insert ( EgyebDlg.QueryAlap, 'CSATOLMANY' , [
		'CS_CSKOD', IntToStr(sorsz),
		'CS_FEKOD', ''''+EgyebDlg.user_code+'''',
		'CS_FNAME', ''''+copy(EgyebDlg.user_name, 1, 40)+'''',
		'CS_CSDAT', ''''+EgyebDlg.MaiDatum+'''',
		'CS_CSIDO', ''''+FormatDateTime('HH:NN', now)+'''',
		'CS_JAKOD', ''''+M20.Text+'''',
		'CS_JASZA', ''''+M01.Text + '-'+ M02.Text+'''',
		'CS_CSPDF', ''''+fn+'''',
		'CS_ALKOD', ''''+alvalkod+'''',
		'CS_ALNEV', ''''+MaskEdit4.Text+''''
	]);


    Query_Log_Str('Fuvaroz�si megb�z�s gener�lva ', 0, true);

end;

procedure TJaratbeDlg.BitBtn46Click(Sender: TObject);
begin
	// A fogyaszt�si adatok megjelen�t�se
	Screen.Cursor			:= crHourGlass;
	Application.CreateForm(TFogyasztasDlg, FogyasztasDlg);
	FogyasztasDlg.M1.Text	:= M02.Text;
	FogyasztasDlg.M0.Text	:= M20.Text;
	FogyasztasDlg.BitBtn2Click(Sender);
	FogyasztasDlg.Panel1.Hide;
	Screen.Cursor			:= crDefault;
	FogyasztasDlg.ShowModal;
	FogyasztasDlg.Destroy;
end;

procedure TJaratbeDlg.M2BKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
	if Key = VK_RETURN then begin
		if Panel17.Visible then begin
			Panel17.SetFocus;
		end else begin
			if alval_jarat then begin
				GroupBox2.SetFocus;
			end else begin
				BitBtn1.SetFocus;
			end;
		end;
	end;
end;

procedure TJaratbeDlg.BitBtn47Click(Sender: TObject);
begin
	// Az eddig elk�ld�tt pdf-ek megjelen�t�se
	Query_Run (Query1, 'SELECT * FROM CSATOLMANY WHERE CS_JAKOD = '''+M20.Text+''' ',true);
	if Query1.RecordCount > 0 then begin
		// Megnyitjuk a csatolm�nyokat
		EgyebDlg.parameter_str	:= M20.Text;
		Application.CreateForm(TCsatolmanyFmDlg, CsatolmanyFmDlg);
		CsatolmanyFmDlg.ShowModal;;
		CsatolmanyFmDlg.Destroy;
		EgyebDlg.parameter_str	:= '';
	end else begin
		NoticeKi('M�g nem k�ldt�k el az alv�llalkoz�i megb�z�st! ');
	end;
end;

procedure TJaratbeDlg.FelrakoTolto(sorkod : string);
begin
	QueryFelrako.Close;
	Query_Run(QueryFelrako, 'SELECT DISTINCT JM_SORSZ, MS_FAJTA, MS_FELNEV,  MS_FORSZ, MS_FELIR, MS_FELTEL, MS_FELCIM, MS_FELDAT, MS_FELIDO, MS_LERNEV, '+
		' MS_ORSZA, MS_LELIR, MS_LERTEL, MS_LERCIM, MS_LERDAT, MS_LERIDO, MS_FELARU, JM_MBKOD, JM_ORIGS '+
		' FROM JARATMEGBIZAS, MEGSEGED WHERE MS_MBKOD = JM_MBKOD '+
		' AND MS_FAJKO = 0 ' + 	// csak a norm�l szakaszok jelenjenek itt meg 
		' AND MS_SORSZ = JM_ORIGS AND JM_JAKOD = '''+M20.Text+''' ORDER BY JM_SORSZ' );
	QueryFelrako.First;
	if sorkod <> '' then begin
		QueryFelrako.Locate('JM_SORSZ', sorkod, []);
	end;
end;

procedure TJaratbeDlg.SpeedButton3Click(Sender: TObject);
var
	most	: integer;
begin
	// Elej�re rakjuk a megbizast
	most 	:= StrToIntDef(QueryFelrako.FieldByName('JM_SORSZ').AsString,0);
	if most > 1 then begin
		Query_Run(QueryFelrako2, 'UPDATE JARATMEGBIZAS SET JM_SORSZ = 999999 WHERE JM_JAKOD = '''+M20.Text+''' '+
			' AND JM_MBKOD = '+QueryFelrako.FieldByName('JM_MBKOD').AsString+' AND JM_SORSZ = '+IntToStr(most) ,true);
		Query_Run(QueryFelrako2, 'UPDATE JARATMEGBIZAS SET JM_SORSZ = JM_SORSZ + 1 WHERE JM_JAKOD = '''+M20.Text+''' '+
			' AND JM_SORSZ < '+IntToStr(most) ,true);
		Query_Run(QueryFelrako2, 'UPDATE JARATMEGBIZAS SET JM_SORSZ = 1 WHERE JM_JAKOD = '''+M20.Text+''' '+
			' AND JM_SORSZ = '+'999999' ,true);
		FelrakoTolto('1');
	end;
end;

procedure TJaratbeDlg.SpeedButton6Click(Sender: TObject);
var
	most	: integer;
	db		: integer;
begin
	// V�g�re rakjuk a megbizast
	most 	:= StrToIntDef(QueryFelrako.FieldByName('JM_SORSZ').AsString,0);
	if most < QueryFelrako.RecordCount then begin
		db		:= QueryFelrako.RecordCount;
		Query_Run(QueryFelrako2, 'UPDATE JARATMEGBIZAS SET JM_SORSZ = 999999 WHERE JM_JAKOD = '''+M20.Text+''' '+
			' AND JM_MBKOD = '+QueryFelrako.FieldByName('JM_MBKOD').AsString+' AND JM_SORSZ = '+IntToStr(most) ,true);
		Query_Run(QueryFelrako2, 'UPDATE JARATMEGBIZAS SET JM_SORSZ = JM_SORSZ - 1 WHERE JM_JAKOD = '''+M20.Text+''' '+
			' AND JM_SORSZ > '+IntToStr(most) ,true);
		Query_Run(QueryFelrako2, 'UPDATE JARATMEGBIZAS SET JM_SORSZ = '+IntToStr(db)+' WHERE JM_JAKOD = '''+M20.Text+''' '+
			' AND JM_SORSZ = '+'999998' ,true);
		FelrakoTolto(IntToStr(QueryFelrako.RecordCount));
	end;
end;

procedure TJaratbeDlg.BitBtn31Click(Sender: TObject);
var
	afkod	: string;
begin
	// Ellen�rizz�k, hogy ki dolgozik m�g ezen a megb�z�son
	afkod	:= Query_Select('MEGBIZAS', 'MB_MBKOD', QueryFelrako.FieldByName('JM_MBKOD').AsString, 'MB_AFKOD');
	if afkod <> '' then begin
		if afkod <> EgyebDlg.user_code then begin
			NoticeKi('A megb�z�st �ppen m�s m�dos�tja!');
			Exit;
		end;
	end;
	// A kijel�lt felrak� megjelenit�se
	Application.CreateForm(TSegedbe2Dlg,Segedbe2Dlg);
	Segedbe2Dlg.MegForm		:= nil;
	Segedbe2Dlg.kellelore	:= false;
	Query_Run(QueryFelrako2, 'SELECT MS_MSKOD FROM MEGSEGED WHERE MS_MBKOD = '+QueryFelrako.FieldByName('JM_MBKOD').AsString+
		' AND MS_SORSZ = '+QueryFelrako.FieldByname('JM_ORIGS').AsString);
	Segedbe2Dlg.tolt('Felrak�s/lerak�s m�dos�t�sa', QueryFelrako.FieldByName('JM_MBKOD').AsString,
		QueryFelrako.FieldByname('JM_ORIGS').AsString, QueryFelrako2.FieldByName('MS_MSKOD').AsString);
	Segedbe2Dlg.ShowModal;
	if Segedbe2Dlg.voltenter then begin
		FelrakoTolto(QueryFelrako.FieldByName('JM_SORSZ').AsString);
	end;
	Segedbe2Dlg.Destroy;
end;

procedure TJaratbeDlg.BitBtn48Click(Sender: TObject);
var
	vanem		: integer;
begin
	// Ellen�rizz�k, hogy �res soron futunk-4
	Application.CreateForm(TMegbizasbeDlg,MegbizasbeDlg);
	MegbizasbeDlg.Tolto('Megb�z�s m�dos�t�sa', QueryFelrako.FieldByName('JM_MBKOD').AsString);
	MegbizasbeDlg.ShowModal;
	if MegbizasbeDlg.ret_kod  <> '' then begin
		FelrakoTolto(QueryFelrako.FieldByName('JM_SORSZ').AsString);
    // KG 20111201
		if not FuvardijTolto(M20.Text) then begin
			NoticeKi('A fuvard�j be�ll�t�sa nem siker�lt!');
		end else begin
			// A  fuvard�j beolvas�sa
			Query_Run (Query2, 'SELECT * FROM ALJARAT WHERE AJ_JAKOD = '''+M20.Text+''' ',true);
			MaskEdit10.Text:= SzamString(StringSzam(Query2.FieldByName('AJ_FUDIJ').AsString),10,2);
			vanem	:= vnemlist.IndexOf(Query2.FieldByName('AJ_FUVAL').AsString);
			if vanem < 0 then begin
				vanem	:= 0;
			end;
			CVnem.ItemIndex		:= vanem;
		end;

	end;
	MegbizasbeDlg.Destroy;
end;

procedure TJaratbeDlg.M28Exit(Sender: TObject);
begin
	with Sender as TMaskEdit do begin
		Text := StrSzamStr(Text,9,0);
	end;
  if ( ( (Sender as TMaskEdit).Name = 'M29' ) or  ( (Sender as TMaskEdit).Name = 'M30' ) ) then begin
    //  if M15.Text <> '' then begin
  			M28.Text	:= Format('%9.0d',[Round(StringSzam(M29.Text)+StringSzam(M30.Text))]);
    //  end;
 	end;
end;

procedure TJaratbeDlg.M28KeyPress(Sender: TObject; var Key: Char);
begin
  with Sender as TMaskEdit do begin
		Key := EgyebDlg.Jochar(Text,Key,9,0);
  end;
end;

procedure TJaratbeDlg.M3AKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
	if Key = VK_RETURN then begin
		if Panel17.Visible then begin
			Panel17.SetFocus;
		end else begin
			if alval_jarat then begin
				GroupBox2.SetFocus;
			end else begin
				BitBtn1.SetFocus;
			end;
		end;
	end;
end;

procedure TJaratbeDlg.BitBtn30Click(Sender: TObject);
begin
  JaratFormDlg.BitBtn3.OnClick(self)  ;
  Close;
end;

procedure TJaratbeDlg.Query3AfterScroll(DataSet: TDataSet);
begin
//  BitBtn4.Enabled:=(Query3.FieldByname('VI_SAKOD').AsString = EmptyStr) or (Query3.FieldByname('VI_SAKOD').IsNull)
  if (Query3.FieldByname('VI_SAKOD').AsString = EmptyStr) or (Query3.FieldByname('VI_SAKOD').IsNull) then
    BitBtn4.Enabled:=True
  else
  begin
		Query_Run(Query2, 'SELECT * FROM SZFEJ WHERE SA_UJKOD = '+Query3.FieldByName('VI_UJKOD').AsString,true);
		if Query2.FieldByname('SA_FLAG').AsString <> '1' then
      BitBtn4.Enabled:=False
    else
      BitBtn4.Enabled:=True;
  end;
end;

procedure TJaratbeDlg.FormActivate(Sender: TObject);
begin
	if not megtekint then
  begin
    EgyebDlg.AUTOSZAMLA:=True;
    // [2017-05-09 NagyP] Teljesen kikapcsolom itt az automatikus sz�mla k�sz�t�st, mert a megb�z�si d�j m�dos�t�sa eset�n nem
    // tudunk "ut�nany�lni", el�g ha a viszonylatban �t kell jav�tani a d�jat
    // [2017-05-18 NagyP] Vissza az eg�sz. Kell az automatikus sz�mla, de fuvard�j m�dos�t�skor t�r�lni fogja a sz�ml�t, a
    // viszonylatban pedig m�dos�tja
    // EgyebDlg.AUTOSZAMLA:=False;

    if (TabbedNotebook1.PageIndex=1)and EgyebDlg.AUTOSZAMLA then  // Megb�z�sok
    begin
      Query3.First;
      while not Query3.Eof do
      begin
        if Query3VI_UJKOD.AsString='' then
          BitBtn11.OnClick(self);
        Query3.Next;
      end;
    end;
  end;
end;

procedure TJaratbeDlg.BitBtn32Click(Sender: TObject);
var
  JaratKod: string;
begin
    try
       JaratKod:= Query1.FieldByName('JA_KOD').AsString;
    except
        NoticeKi('Nincs j�rat kiv�lasztva! K�rem csukja be a "J�ratok" ablakot, majd nyissa meg �jra.');
        exit;
        end;

    if Query_Select('KOLTSEG','KS_JAKOD',JaratKod,'KS_JAKOD') <> '' then
    begin
      NoticeKi('FIGYELEM ! A j�rathoz k�lts�g tartozik, ez�rt nem stornozhat�!');
      exit;
    end;
{    if Query_Select('MEGBIZAS','MB_JAKOD',Query1.FieldByName('JA_KOD').AsString,'MB_JAKOD') = '' then
    begin
      NoticeKi('FIGYELEM ! A j�rathoz nem tartozik megb�z�s, ez�rt nem stornozhat�!');
      exit;
    end;
   }

	// J�rat storn�z�sa
	if NoticeKi('Val�ban sztorn�zni k�v�nja a j�ratot?', NOT_QUESTION) <> 0 then begin
		Exit;
	end;
	if NoticeKi('A storn�zott j�ratot a k�s�bbiek sor�n m�r nem m�dos�thatja. A storno megb�z�st most kell elk�ldeni! Folytatja?', NOT_QUESTION) <> 0 then begin
		Exit;
	end;

	Query_Run (Query2, 'UPDATE JARAT SET JA_FAZIS = 2, JA_FAZST = '''+GetJaratFazis( 2)+''' '+
		' WHERE JA_KOD = '''+Query1.Fieldbyname('JA_KOD').AsString  + ''' ', true);

  Label6.Caption:='STORNO';
  Label6.Show;
  BitBtn32.Hide;
  STORNO:=True;
 // NoticeKi('');

end;

procedure TJaratbeDlg.BitBtn49Click(Sender: TObject);
begin

	// A k�lts�g �sszes�t�s megh�v�sa a j�ratk�ddal + ellen�rizhet�s�g
	if GetRightTag(340) = RG_NORIGHT then begin
		Exit;
	end;

	Screen.Cursor				:= crHourGlass;
	Application.CreateForm(TOssznyilDlg, OssznyilDlg);
	Screen.Cursor				:= crDefault;
	OssznyilDlg.M0.Text			:= ret_kod;
	OssznyilDlg.M1.Text     	:= M01.Text + '-'+M02.Text;
	OssznyilDlg.M1.Enabled		:= false;
	OssznyilDlg.BitBtn2.Enabled	:= false;
	OssznyilDlg.BitBtn3.Enabled	:= false;
	OssznyilDlg.BitBtn5.Enabled	:= false;
	OssznyilDlg.BitBtn8.Enabled	:= false;
	OssznyilDlg.BitBtn7.Enabled	:= false;
	OssznyilDlg.BitBtn32.Enabled	:= false;
	OssznyilDlg.MD1.Enabled		:= false;
	OssznyilDlg.MD2.Enabled		:= false;
  OssznyilDlg.chkCsakSajat.Checked:= False;
  OssznyilDlg.BitBtn9.OnClick(self);
  Mentes;
	OssznyilDlg.Destroy;
end;

function TJaratbeDlg.NemElszamoltJarat(rendsz: string): integer;
begin
  Result:=0;
  if rendsz='' then exit;
	Query_Run (Query1, 'SELECT * FROM JARAT WHERE JA_RENDSZ= '''+rendsz+''' and (JA_FAZIS= 0)',true) ;
  Result:= Query1.RecordCount;
  if Result>1 then
      NoticeKi(IntToStr(Result)+' db el nem sz�molt j�rat ehhez a rendsz�mhoz!');
end;

procedure TJaratbeDlg.M3AExit(Sender: TObject);
begin
	IdoExit(Sender, false);
  Idotartam;
end;

procedure TJaratbeDlg.M2AExit(Sender: TObject);
begin
	IdoExit(Sender, false);
  Idotartam;
end;

procedure TJaratbeDlg.Idotartam;
var
  ora,min: integer;
  kido, vido: string;
begin
  M3B.Text:='';
  if (M3.Text<>'')and(M3A.Text<>'') then
  begin
    kido:=M2A.Text; if kido='24:00' then kido:='23:59';
    vido:=M3A.Text; if vido='24:00' then vido:='23:59';
    ora:= MinutesBetween(StrToDateTime(M2.Text+' '+kido),StrToDateTime(M3.Text+' '+vido)) div 60         ;
    min:= MinutesBetween(StrToDateTime(M2.Text+' '+kido),StrToDateTime(M3.Text+' '+vido)) mod 60         ;
    if min>=30 then ora:=ora+1;
    M3B.Text:=IntToStr(ora);
  end;
end;

procedure TJaratbeDlg.M2Change(Sender: TObject);
begin
   // Az �vsz�m ellen�rz�se
{
   if evstr <> copy (M2.Text, 1, 4) then begin
       evstr       := copy (M2.Text, 1, 4);
       M02.Text    := IntToStr(GetJaratAlkod(evstr));
   end;
   }
	modosult 	:= true;
//   Idotartam;
end;

{procedure TJaratbeDlg.RG_FAJKOClick(Sender: TObject);
var
  ejasza, msmbkod: string;
begin
  if (RG_FAJKO.ItemIndex<>fajko)and((Query3.RecordCount<>0)or(QSeged.RecordCount<>0)) then
  begin
    NoticeKi('A j�rathoz megb�z�s tartozik, ez�rt a fajt�ja m�r nem m�dos�that�!')  ;
    RG_FAJKO.ItemIndex:=fajko;
    exit;
  end;

  fajko:=RG_FAJKO.ItemIndex;
  //Panel20.Caption:=RG_FAJKO.Items[RG_FAJKO.itemindex];
  if RG_FAJKO.itemindex=0 then
    Panel20.Caption:='Norm�l'
  else
    Panel20.Caption:='NEM norm�l';

  if (RG_FAJKO.ItemIndex>0)and(M20.Text<>'') then
  begin
    msmbkod:= Query_Select('MEGSEGED','MS_JAKOD',M20.Text,'MS_MBKOD');
    if msmbkod<>'' then
    begin
      ejasza:= Query_Select('MEGBIZAS','MB_MBKOD',Query_Select('MEGSEGED','MS_JAKOD',M20.Text,'MS_MBKOD'),'MB_JASZA');
      if ejasza<>'' then
        Panel20.Caption:='NEM norm�l(norm�l:'+ejasza+')';
    end;
  end;
end;
}
{
procedure TJaratbeDlg.BitBtn50Click(Sender: TObject);
var
  db: integer;
begin
  KoltsegEllenor;
  db:=Koltsegimport();
  if db > 0 then
    ShowMessage('K�sz! ('+IntToStr(db)+')')
  else
    ShowMessage('Nincs import�lhat� k�lts�g!');
end;
 }
  // M�r nem ezt a modult haszn�ljuk! Kikommentezem 2017-08-17
{
function TJaratbeDlg.Koltsegimport(kelluzenet: boolean): integer;
var
  ktip,db      : integer;
  rendsz,ks_sor: string;
  kell         : boolean;
begin


  // Result  :=0;
  // Exit;
  // -----------------------------------------------
  db:=0;
  rendsz:=M4.Text;
  if rendsz='' then exit;
  if kelluzenet and (MessageBox(0, 'Mehet a k�lts�gek import�l�sa?', '', MB_ICONQUESTION or MB_YESNO or MB_DEFBUTTON2)= IDNO) then
    exit;

  Application.CreateForm(TFKoltsegImport, FKoltsegImport);

  T_GYUJTOMIND.Close;
  T_GYUJTOMIND.SQL.Clear;
  T_GYUJTOMIND.SQL.Add('select * from KOLTSEG_GYUJTO where ((KS_JAKOD is null)or(KS_JAKOD='''+''')) and '+' KS_RENDSZ= :RSZ and KS_DATUM>= :KDAT and KS_DATUM<= :VDAT order by ks_ktip,ks_datum,ks_ido');
  T_GYUJTOMIND.Parameters[0].Value:=rendsz ;
  T_GYUJTOMIND.Parameters[1].Value:= M2.Text ;
  T_GYUJTOMIND.Parameters[2].Value:= M3.Text ;
  T_GYUJTOMIND.Open;
  T_GYUJTOMIND.First;
  if not T_GYUJTOMIND.Eof then
  begin
    Panel21.Caption:='K�lts�gek import�l�sa...';
    Panel21.Update;
  end;
  while not T_GYUJTOMIND.Eof do
  begin
    ktip:=T_GYUJTOMINDKS_KTIP.Value;
    ks_sor:=T_GYUJTOMINDKS_SOR.Value;
    db:=db+FKoltsegImport.Feldolgozas(ktip,ks_sor,False);
    T_GYUJTOMIND.Next;
  end;
  T_GYUJTOMIND.Close;
  FKoltsegImport.Free;
	Query_Run(Query4, 'SELECT * FROM koltseg WHERE KS_JAKOD = '''+M20.Text+''' ORDER BY KS_DATUM, KS_KMORA, KS_TELE',true);
  //TabbedNotebook1.OnChange(TabbedNotebook1,TabbedNotebook1.PageIndex,kell);

  Panel21.Caption:='';
  Panel21.Update;
  Result:=db;
end;
}

procedure TJaratbeDlg.DBGrid6DblClick(Sender: TObject);
begin
  if BitBtn26.Enabled then
    BitBtn26.OnClick(self);
end;

function TJaratbeDlg.Viszony_Szamla_Vevoell(uzenet: boolean): boolean;
var
  ujkod, svnev,vvnev: string;
begin
//
  Result:=True;
  Query3.First;
  while not Query3.Eof do
  begin
    ujkod:=Query3VI_UJKOD.AsString;
    if ujkod='' then exit;
    vvnev:=Query3VI_VENEV.Value;
    svnev:=Query_Select('SZFEJ','SA_UJKOD',ujkod,'SA_VEVONEV') ;
    if vvnev<>svnev then
    begin
      if uzenet then
      begin
        NoticeKi('Elt�r� vev�n�v! '+Query3VI_SAKOD.Value+' (Viszony-Sz�mla) '+vvnev+' - '+svnev);
      end;
      Result:=False;
      Exit;
    end;
    Query3.Next;
  end;
end;

function TJaratbeDlg.Megbizas_db: integer;
var
	db	    : integer;
   embkod  : integer;
   szuro   : string;
begin
  db       :=0;
  embkod   :=0;
  msmssorsz:=0;
  if cbFAJKO.ItemIndex=0 then // norm�l
    szuro:=' and ((MB_JASZA='''+''')or(MB_JASZA is null))'
  else
    szuro:=' and ((MS_JASZA='''+''')or(MS_JASZA is null))';

  if cbFAJKO.ItemIndex=0 then // norm�l
  Query_Run(EgyebDlg.QueryAlap,'select mb_mbkod,ms_mskod,ms_sorsz from megseged, megbizas '+
  'where ms_mbkod=mb_mbkod and MS_RENSZ ='''+M4.Text+''' and ((MS_LETDAT>='''+M2.Text+''' and MS_LETDAT<='''+M3.Text+''')or(MS_FETDAT>='''+M2.Text+''''+
  ' and MS_FETDAT<='''+M3.Text +'''' +')) and MS_FAJKO=0 '+szuro)
  else
  //Query_Run(EgyebDlg.QueryAlap,'select mb_mbkod,ms_mskod,ms_sorsz from megseged, megbizas '+
  Query_Run(EgyebDlg.QueryAlap,'select DISTINCT mb_mbkod,ms_sorsz from megseged, megbizas '+
  'where ms_mbkod=mb_mbkod and MS_RENSZ ='''+M4.Text+''' and ((MS_LETDAT>='''+M2.Text+''' and MS_LETDAT<='''+M3.Text+''')or(MS_FETDAT>='''+M2.Text+''''+
  ' and MS_FETDAT<='''+M3.Text +'''' +')) and MS_FAJKO>0 '+' and ((MS_JASZA='''+''')or(MS_JASZA is null))'+' order by mb_mbkod,MS_SORSZ') ;

  while not EgyebDlg.QueryAlap.Eof do
  begin
    if (embkod<>EgyebDlg.QueryAlap.FieldByName('MB_MBKOD').AsInteger)or(msmssorsz<>EgyebDlg.QueryAlap.FieldByName('MS_SORSZ').AsInteger)   then
    begin
      embkod:=EgyebDlg.QueryAlap.FieldByName('MB_MBKOD').AsInteger;
      msmskod:=EgyebDlg.QueryAlap.FieldByName('MS_MSKOD').AsInteger;
      msmssorsz:=EgyebDlg.QueryAlap.FieldByName('MS_SORSZ').AsInteger;
      inc(db);
    end;
    EgyebDlg.QueryAlap.Next;
  end;
  EgyebDlg.QueryAlap.RecordCount;
  Result:=db;
end;

procedure TJaratbeDlg.BitBtn51Click(Sender: TObject);
begin
   if Noticeki('Val�ban ki akarja �r�teni a JARATMEGSEGED t�bl�t?', NOT_QUESTION) = 0 then begin
       Query_Run(EgyebDlg.QueryAlap,'DELETE FROM JARATMEGSEGED');
       Query_Run(EgyebDlg.QueryAlap,'DELETE FROM ALJARAT WHERE AJ_JAKOD IN (SELECT JA_KOD FROM JARAT WHERE JA_ALVAL = -1)');
       Query_Run(EgyebDlg.QueryAlap,'DELETE FROM JARAT WHERE JA_ALVAL = -1');
   end;
end;

procedure TJaratbeDlg.BitBtn52Click(Sender: TObject);
var
   ssz     : integer;
   jodb    : integer;
   sorsz   : integer;
   t       : TDateTime;
begin
   Memo1.Clear;
   t       := now;
   Screen.Cursor   := crHourGlass;
   Memo1.Lines.Add('Adatok �sszegy�jt�se');
   Memo1.Lines.Add('--------------------');
   Query_Run(EgyebDlg.QueryAlap,'SELECT MS_JAKOD, MS_MSKOD, MS_MBKOD FROM MEGSEGED WHERE MS_JAKOD = '''' AND MS_FAJKO <> 0 ORDER BY MS_MBKOD ');
   kilepes := false;
   sorsz   := Memo1.Lines.Add('   ');
   ssz     := 1;
   jodb    := 0;
   while ( ( not EgyebDlg.QueryAlap.Eof ) and ( not kilepes ) ) do begin
       Application.ProcessMessages;
       // Az �j j�ratok l�trehoz�sa
       if MegSegedJarat(EgyebDlg.QueryAlap.FieldByName('MS_MBKOD').AsString, EgyebDlg.QueryAlap.FieldByName('MS_MSKOD').AsString) then begin
           Inc(jodb);
       end;
       Memo1.Lines[sorsz] := '   '+Format('%5d',[ssz])+'/'+Format('%5d',[EgyebDlg.QueryAlap.RecordCount])+'. �j elemek : '+EgyebDlg.QueryAlap.FieldByName('MS_MBKOD').AsString+' -> '+IntToStr(jodb);
       Inc(ssz);
       EgyebDlg.QueryAlap.Next;
   end;
   Memo1.Lines.Add('   ');
   Memo1.Lines.Add('Az �j r�gi elemek beilleszt�se befejez�d�tt ('+FormatDateTime('nn:ss.zzz', now-t)+')');
   Memo1.Lines.Add('   ');
   Query_Run(EgyebDlg.QueryAlap,'SELECT MS_JAKOD, MS_MSKOD, MS_MBKOD FROM MEGSEGED WHERE MS_JAKOD <> '''' AND MS_FAJKO <> 0 ORDER BY MS_MBKOD ');
   kilepes := false;
   sorsz   := Memo1.Lines.Add('   ');
   while ( ( not EgyebDlg.QueryAlap.Eof ) and ( not kilepes ) ) do begin
       Application.ProcessMessages;
       // A megl�v� elemek berak�sa a t�bl�ba
       Query_Run ( QSeged, 'INSERT INTO JARATMEGSEGED ( JG_JAKOD, JG_MBKOD, JG_MSKOD, JG_FOJAR ) VALUES ( '+
               ''''+ EgyebDlg.QueryAlap.FieldByName('MS_JAKOD').AsString + ''', '+EgyebDlg.QueryAlap.FieldByName('MS_MBKOD').AsString +','+EgyebDlg.QueryAlap.FieldByName('MS_MSKOD').AsString +','+
               ''''+ Query_Select('MEGBIZAS', 'MB_MBKOD', EgyebDlg.QueryAlap.FieldByName('MS_MBKOD').AsString, 'MB_JAKOD')+''' )');
       Memo1.Lines[sorsz] := '   '+Format('%5d',[ssz])+'/'+Format('%5d',[EgyebDlg.QueryAlap.RecordCount])+'. megseged : '+EgyebDlg.QueryAlap.FieldByName('MS_MBKOD').AsString;
       Inc(ssz);
       EgyebDlg.QueryAlap.Next;
   end;
   Memo1.Lines.Add('   ');
   Memo1.Lines.Add('A v�grehajt�s befejez�d�tt ('+FormatDateTime('nn:ss.zzz', now-t)+')');
   Screen.Cursor   := crDefault;
end;

procedure TJaratbeDlg.BitBtn55Click(Sender: TObject);
var
   mskod   : string;
   mbkod   : string;
   JarVal  : TJaratFormDlg;
   jasza   : string;
   szurosor: integer;
begin
   // �trakjuk a kijel�lt elemet egy m�sik j�ratba
   if QSeged.RecordCount < 1 then begin
       Exit;
   end;
   mbkod   := QSeged.FieldByName('JG_MBKOD').AsString;
   mskod   := QSeged.FieldByName('JG_MSKOD').AsString;
	// A j�rat sz�r�s be�ll�t�sa
	EgyebDlg.SzuroGrid.RowCount 			:= EgyebDlg.SzuroGrid.RowCount + 1;
	szurosor								:= EgyebDlg.SzuroGrid.RowCount - 1 ;
	EgyebDlg.SzuroGrid.Cells[0,szurosor] 	:= '190';
  EgyebDlg.SzuroGrid.Cells[1,szurosor] 	:= 'JA_FAJKO=1';
  //EgyebDlg.SzuroGrid.Cells[1,szurosor] 	:= 'JA_RENDSZ = '''+Query1.FieldByName('MS_RENSZ').AsString+''' and JA_FAJKO=1';

   {Kiv�lasztjuk a j�rathoz kapcsol�d� megrendel�t, ha t�bb van}
   Application.CreateForm(TJaratFormDlg,JarVal);
   JarVal.valaszt              := true;
   JarVal.ButtonUj.Visible    := false;
   JarVal.ButtonMod.Visible    := false;
   JarVal.ButtonTor.Visible    := false;
   JarVal.BitBtn1.Visible      := false;
   JarVal.BitBtn2.Visible      := false;
   JarVal.BitBtn3.Visible      := false;
   JarVal.BitBtn4.Visible      := false;
   JarVal.BitBtn5.Visible      := false;
   JarVal.ShowModal;
   if JarVal.ret_vkod <> '' then begin
       // A j�rat cser�je
       Query_Run(QSeged, 'UPDATE JARATMEGSEGED SET JG_JAKOD = '''+JarVal.ret_vkod+''' WHERE JG_MBKOD = '+mbkod+
           ' AND JG_MSKOD = '+mskod+' ');
       Query_Run(QSeged, 'SELECT count(*) MSDB FROM JARATMEGSEGED WHERE JG_JAKOD = '''+JarVal.ret_vkod+'''');
       Query_Run(QSeged, 'UPDATE JARAT SET JA_MSDB = '+QSeged.fieldbyname('MSDB').AsString +' WHERE JA_KOD = '''+JarVal.ret_vkod+'''');

       jasza:=JarVal.ret_orsz+'-'+JarVal.ret_alkod;
       Query_Run(QSeged, 'UPDATE MEGSEGED SET MS_JAKOD = '''+JarVal.ret_vkod+''',MS_JASZA = '''+jasza+''' WHERE MS_MBKOD = '+mbkod+' AND MS_MSKOD = '+mskod+' ');

       Query_Run(QSeged,'SELECT * FROM JARATMEGSEGED, MEGSEGED WHERE JG_JAKOD = '''+ret_kod+''' AND JG_MSKOD = MS_MSKOD ');
   end;
   JarVal.Destroy;
	// A sz�r�s t�rl�se
	GridTorles(EgyebDlg.SzuroGrid, szurosor);end;

procedure TJaratbeDlg.BitBtn56Click(Sender: TObject);
var
   meguj   : TMegbizasbeDlg;
begin
   if QSeged.FieldByName('JG_MBKOD').AsString <> '' then begin
       Application.CreateForm(TMegbizasbeDlg, meguj);
       meguj.Tolto('Megbizas megjelen�t�se', QSeged.FieldByName('JG_MBKOD').AsString);
       meguj.ShowModal;
       meguj.Destroy;
   end;
end;

procedure TJaratbeDlg.M3Change(Sender: TObject);
begin
	modosult 	:= true;
 //  Idotartam;
end;

procedure TJaratbeDlg.M3AChange(Sender: TObject);
begin
	modosult 	:= true;
 //  Idotartam;
end;

procedure TJaratbeDlg.M2AChange(Sender: TObject);
begin
	modosult 	:= true;
 //  Idotartam;
end;

procedure TJaratbeDlg.M2Exit(Sender: TObject);
begin
	DatumExit(Sender, false);
	if M2B.Text = '' then begin
		M2B.Text	:= M2.Text;
	end;
  Idotartam;

  if trim(M02.Text)='' then
  begin
      evstr       := copy (M2.Text, 1, 4);
      if evstr<>'' then
      begin
        M02.Text    := IntToStr(GetJaratAlkod(evstr));
        M02.Text	:= Format('%5.5d',[StrToIntDef(M02.Text,0)]);
      end;
  end;
end;

function TJaratbeDlg.SoforEllenor: boolean;
var
   kidat   : string;
begin
   // Ellen�rizz�k, hogy a sof�r�k arhiv�l�sa, kil�p�se nem r�gebbi-e mint a j�rat d�tuma
	Result 	:= true;
   Query8.First;
   while not Query8.Eof do begin
       kidat := Query_Select('DOLGOZO', 'DO_KOD', Query8.FieldByname('JS_SOFOR').AsString, 'DO_KILEP');
       if ( ( kidat <> '' ) and ( kidat < M2.Text ) ) then begin
           NoticeKi('A dolgoz� m�r arh�v! '+ Query8.FieldByName('SOFORNEV').AsString + ' ( '+kidat+' )');
           Result  := false;
           Query8.Last;
       end;
       Query8.Next;
   end;
end;

procedure TJaratbeDlg.BitBtn53Click(Sender: TObject);
begin
	if (M13.Text <> '')and(StringSzam(M13.Text)<>0) then
  begin
    NoticeKi('A mez� nem �res!');
    M13.SetFocus;
    exit;
  end;            

	Query_Run(Query2,'SELECT MAX(JA_MENSZ) max FROM JARAT WHERE JA_JKEZD LIKE '''+copy(M2.Text, 1, 4)+'%'' ',true);
  M13.Text:= IntToStr(StrToIntDef( Query2.FieldByName('max').AsString,0)+1)  ;
  M13.OnExit(self);
end;

procedure TJaratbeDlg.BitBtn57Click(Sender: TObject);
var
  sorsz, mbkod,mskod: string;
begin
   if QSeged.RecordCount = 0 then begin
       Exit;
   end;

	if (NoticeKi('Val�ban t�r�lni akarja az aktu�lis rekordot?', NOT_QUESTION) <> 0) then
  begin
    exit;
  end;

  Try
    sorsz:= Query_Select('MEGSEGED','MS_MSKOD', QSeged.FieldByName('JG_MSKOD').AsString,'MS_SORSZ');
    mbkod:= QSeged.FieldByName('JG_MBKOD').AsString ;
    mskod:= QSeged.FieldByName('JG_MSKOD').AsString ;

  	if not Query_Run(Query2, 'UPDATE MEGSEGED SET MS_JAKOD = '''', MS_JASZA = ''''  WHERE MS_MBKOD = '+mbkod+' and MS_SORSZ='+sorsz,true) then Raise Exception.Create('');
  	if not Query_Run(Query2, 'DELETE FROM JARATMEGSEGED WHERE JG_MBKOD = '+mbkod+' and JG_MSKOD='+mskod,true) then Raise Exception.Create('');

  Except
  		NoticeKi('A megb�z�s t�rl�se nem siker�lt!');
  END;

  Query_Run(QSeged,'SELECT * FROM JARATMEGSEGED, MEGSEGED WHERE JG_JAKOD = '''+ret_kod+''' AND JG_MSKOD = MS_MSKOD ');

end;

procedure TJaratbeDlg.BitBtn54Click(Sender: TObject);
var
	mskod		: string;
	jaratszam	: string;
	szurosor, indexsor	: integer;
  KELLLANID:integer;
  mssorsz, szfajko,mbrensz,mbkod,fojar: string;
begin
  if FAJKO_ellenor then Exit;
  if (cbFAJKO.ItemIndex=0)and(TabbedNotebook1.PageIndex=2) then
  begin
		NoticeKi('Norm�l j�rat!!');
    exit;
  end;
  if MegbizasUjFmDlg<>nil then
  begin
		NoticeKi('A megb�z�s ablak m�r meg van nyitva!');
    Exit;
  end;
	// Egy �j megb�z�s beolvas�sa a j�rathoz
	if M4.Text = '' then begin
		NoticeKi('A rendsz�m nem lehet �res!');
		TabbedNotebook1.PageIndex	:= 0;
		M4.Setfocus;
		Exit;
	end;
	// Be�ll�tjuk a rendsz�mot �s hogy ne legyen j�ratsz�ma
	// A j�rat sz�r�s be�ll�t�sa

  fajko:=cbFAJKO.ItemIndex;
  szfajko:=' and MS_FAJKO>0'  ;

	EgyebDlg.SzuroGrid.RowCount := EgyebDlg.SzuroGrid.RowCount + 1;
	szurosor := EgyebDlg.SzuroGrid.RowCount - 1 ;
	EgyebDlg.SzuroGrid.Cells[0,szurosor] 	:= '113';
  if  CheckBox3.Checked then     //D�tum
  begin
    if CheckBox5.Checked then    //D�tum+Id�
	    EgyebDlg.SzuroGrid.Cells[1,szurosor] 	:= 'MS_RENSZ = '''+M4.Text+''' and ((MS_LETDAT+MS_LETIDO>='''+M2.Text+M2A.Text+''' and MS_LETDAT+MS_LETIDO<='''+M3.Text+M3A.Text+''')or(MS_FETDAT+MS_FETIDO>='''+M2.Text+M2A.Text+''' and MS_FETDAT+MS_FETIDO<='''+M3.Text+M3A.Text  +'''))'+szfajko
    else                         //D�tum
	    EgyebDlg.SzuroGrid.Cells[1,szurosor] 	:= 'MS_RENSZ = '''+M4.Text+''' and ((MS_LETDAT>='''+M2.Text+''' and MS_LETDAT<='''+M3.Text+''')or(MS_FETDAT>='''+M2.Text+''' and MS_FETDAT<='''+M3.Text  +'''))'+szfajko;
  end
  else
	  EgyebDlg.SzuroGrid.Cells[1,szurosor]  	:= 'MS_RENSZ = '''+M4.Text+''''+szfajko ;

//  Clipboard.Clear;
//  Clipboard.AsText:=EgyebDlg.SzuroGrid.Cells[1,szurosor];
	EgyebDlg.IndexGrid.RowCount := EgyebDlg.IndexGrid.RowCount + 1;
	indexsor := EgyebDlg.IndexGrid.RowCount - 1 ;
	EgyebDlg.IndexGrid.Cells[0,indexsor] 	:= '113';
  EgyebDlg.IndexGrid.Cells[1,indexsor] 	:= ' MS_DATUM, MS_IDOPT ';


  EgyebDlg.stralap:=True;
	Application.CreateForm(TMegbizasUjFmDlg, MegbizasUjFmDlg);
  MegbizasUjFmDlg.ComboBox2.ItemIndex:=0;
  MegbizasUjFmDlg.ComboBox2.OnChange(self);
  MegbizasUjFmDlg.Query1.RecordCount;
	MegbizasUjFmDlg.valaszt := true;
	MegbizasUjFmDlg.ShowModal;

	mskod := MegbizasUjFmDlg.ret_mskod;
  mssorsz:=MegbizasUjFmDlg.ret_mssorsz;
  mbrensz:=MegbizasUjFmDlg.ret_rendsz;
  mbkod:=MegbizasUjFmDlg.ret_mbkod;
	EgyebDlg.SzuroGrid.RowCount  := 1;
	EgyebDlg.SzuroGrid.Rows[0].Clear;

	MegbizasUjFmDlg.Destroy;
  MegbizasUjFmDlg:=nil;
	// A sz�r�s t�rl�se

	GridTorles(EgyebDlg.SzuroGrid, szurosor+1);
	GridTorles(EgyebDlg.SzuroGrid, szurosor);
	GridTorles(EgyebDlg.IndexGrid, indexsor+1);
	GridTorles(EgyebDlg.IndexGrid, indexsor);

	if mskod = '' then begin
		Exit;
	end;
	// Ellen�rizz�k a megb�z�st
 	Query_Run(Query1, 'SELECT * FROM MEGBIZAS, MEGSEGED WHERE MS_MBKOD = MB_MBKOD AND MS_MSKOD = '+mskod);
  if (fajko=0)and( Query1.FieldByName('MB_RENSZ').AsString <> M4.Text) then begin
		if NoticeKi('A kiv�lasztott megb�z�s rendsz�ma nem egyezik a j�rat rendsz�mmal. Folytatja?', NOT_QUESTION) <> 0 then begin
			Exit;
		end;
  end;
	if (fajko=0)and(Query1.FieldByName('MB_JAKOD').AsString <> '') then begin
		NoticeKi('A kiv�lasztott megb�z�s j�ratsz�ma nem �res!');
		Exit;
	end;
	if (fajko>0)and(Query1.FieldByName('MS_JAKOD').AsString <> '') then begin
		NoticeKi('A kiv�lasztott megb�z�s j�ratsz�ma nem �res!');
		Exit;
	end;
	// Ellen�rizni kell, hogy a megb�z�shoz l�tezik-e LAN_ID
	Query_Run( QueryKoz, 'SELECT VE_LANID FROM VEVO WHERE V_KOD= '+Query1.FieldByName('MB_VEKOD').AsString, true );

  GKKATEG:='';
  GKKATEG:= MaskEdit9.Text;
  if (GKKATEG='')and( Query1.FieldByName('MB_ALV').AsInteger=1) then // Alv�llalkoz�
  begin
    GKKATEG:= Query_Select('ALGEPKOCSI','AG_RENSZ',Query1.FieldByName('MB_RENSZ').AsString,'AG_GKKAT' );
  end;
	jaratszam	:= M01.Text + '-'+ M02.Text;

  Try
      Query_Run(Query2, 'select mb_jakod from megbizas where mb_mbkod='+mbkod );
      fojar:=Query2.fieldbyname('MB_JAKOD').AsString;

  		if not Query_Update( Query2, 'MEGSEGED', [
			'MS_JAKOD', ''''+M20.Text+'''',
			'MS_JASZA', ''''+jaratszam+''''
	  	], ' WHERE MS_MBKOD = '+mbkod+' and MS_FAJKO>0'+' and MS_SORSZ='+mssorsz) then Raise Exception.Create('');    //

      // A megbizasok hozz�kapcsol�sa a j�rathoz
      if not Query_Insert (Query2, 'JARATMEGSEGED',[
          				'JG_FOJAR', ''''+fojar+'''',
          				'JG_JAKOD', ''''+ret_kod+'''',
          				'JG_MBKOD', mbkod,
          				'JG_MSKOD', mskod
          				]) then Raise Exception.Create('');

      Query_Run(QSeged,'SELECT * FROM JARATMEGSEGED, MEGSEGED WHERE JG_JAKOD = '''+ret_kod+''' AND JG_MSKOD = MS_MSKOD ');
  Except
  		NoticeKi('A megb�z�s csatol�sa nem siker�lt!');
  END;

	GombEllenor;
end;

procedure TJaratbeDlg.CheckBox5Click(Sender: TObject);
begin
  if CheckBox5.Checked then
    CheckBox3.Checked:=True;;
end;

procedure TJaratbeDlg.CheckBox3Click(Sender: TObject);
begin
  if not CheckBox3.Checked then
    CheckBox5.Checked:=False;

end;

procedure TJaratbeDlg.BitBtn58Click(Sender: TObject);
begin
  EgyebDlg.MBujtetel:=False;
	Application.CreateForm(TSegedbe2Dlg,Segedbe2Dlg);
 //	Segedbe2Dlg.MegForm		:= Self;
	Segedbe2Dlg.kellelore	:= false;
	Segedbe2Dlg.tolt('Felrak�s/lerak�s ', QSeged.FieldByName('JG_MBKOD').AsString,
		QSeged.FieldByname('MS_SORSZ').AsString, QSeged.FieldByName('JG_MSKOD').AsString);
  Segedbe2Dlg.BitElkuld.Visible:=False;

	Segedbe2Dlg.ShowModal;

	Segedbe2Dlg.Destroy;
end;

procedure TJaratbeDlg.BitBtn59Click(Sender: TObject);
var
   JarVal  : TJaratFormDlg;
begin
   {Kiv�lasztjuk a j�rathoz kapcsol�d� megrendel�t, ha t�bb van}
   Application.CreateForm(TJaratFormDlg,JarVal);
   JarVal.SzuresUrites;
   JarVal.SQL_ToltoFo('');
   JarVal.valaszt              := true;
   JarVal.ButtonUj.Visible     := false;
   JarVal.ButtonMod.Visible    := false;
   JarVal.ButtonTor.Visible    := false;
   JarVal.BitBtn1.Visible      := false;
   JarVal.BitBtn2.Visible      := false;
   JarVal.BitBtn3.Visible      := false;
   JarVal.BitBtn4.Visible      := false;
   JarVal.BitBtn5.Visible      := false;
   JarVal.ShowModal;
   if JarVal.ret_vkod <> '' then begin
       fojarat:=JarVal.ret_vkod;
       Query_Update( Query2, 'JARAT', [
          			'JA_FOJAR', ''''+fojarat+''''
        	    	], ' WHERE JA_KOD = '+ret_kod);    //
   end;
   JarVal.Destroy;

end;

procedure TJaratbeDlg.BitBtn60Click(Sender: TObject);
begin
   if fojarat='' then begin
       Exit;
   end;

	if (NoticeKi('Val�ban t�r�lni akarja a F�j�rat sz�mot? '+fojarat, NOT_QUESTION) <> 0) then
  begin
    exit;
  end;

  Query_Update( Query2, 'JARAT', [
          			'JA_FOJAR', ''''+EmptyStr+''''
        	    	], ' WHERE JA_KOD = '+ret_kod);    //
  fojarat:='';
end;

procedure TJaratbeDlg.GombAllapot(ga : boolean);
begin
   // A gombok be�ll�t�sa
   BitBtn11.Visible    := ga;
   BitBtn12.Visible    := ga;
end;

procedure TJaratbeDlg.Query14AfterScroll(DataSet: TDataSet);
begin
	Query_Run(Query12, 'SELECT * FROM koltseg WHERE KS_JAKOD = '''+Query14.FieldByName('JK_CSJAR').AsString+''' ORDER BY KS_DATUM, KS_KMORA, KS_TELE',true);

end;

procedure TJaratbeDlg.Query12CalcFields(DataSet: TDataSet);
begin
	Query12VISZONY.AsString	:= '';
	if Query12.FieldByName('KS_VIKOD').AsString <> '' then begin
		Query_Run(Query2, 'SELECT VI_HONNAN, VI_HOVA FROM VISZONY WHERE VI_VIKOD = '''+Query12.FieldByName('KS_VIKOD').AsString+''' ');
		if Query2.RecordCount > 0 then begin
			Query12VISZONY.AsString	:= Query2.FieldByName('VI_HONNAN').AsString+ '-'+Query2.FieldByName('VI_HOVA').AsString;
		end;
	end;

end;

procedure TJaratbeDlg.BitBtn61Click(Sender: TObject);
var
  jaratuj: TJaratFormDlg;
begin

	Application.CreateForm(TJaratFormDlg,jaratuj);
  jaratuj.Query1.SQL.Clear;
  jaratuj.valaszt :=True;
	jaratuj.FelTolto('�j J�rat felvitele ', 'J�rat adatok m�dos�t�sa ',
			'J�ratok list�ja', 'JA_KOD', 'select * from jarat,aljarat where ja_kod=aj_jakod and aj_alnev<>'''' ','JARAT', QUERY_ADAT_TAG );
	jaratuj.ShowModal;
	if jaratuj.ret_vkod <> '' then
  begin
		jaratkod.Text		:= jaratuj.ret_vkod;
		jaratszam.Text 	:= GetJaratszam(jaratuj.ret_vkod);
	end
  else
  begin
		jaratkod.Text		:= '';
		jaratszam.Text 	:= '';
	end  ;
	EgyebDlg.SzuroGrid.RowCount  := 1;
	EgyebDlg.SzuroGrid.Rows[0].Clear;
	jaratuj.Destroy;
	if jaratkod.Text = '' then begin
		Exit;
	end;

	Query_Insert (Query1, 'JARATKOLTSEG',
				[
				'JK_JAKOD', ''''+ret_kod+'''',
				'JK_CSJAR', ''''+jaratkod.Text+''''
				]) ;

    Query_Run(Query14,'SELECT * FROM JARATKOLTSEG, JARAT WHERE JK_JAKOD = '''+ret_kod+''' AND JA_KOD =JK_CSJAR');
//  TabbedNotebook1.OnChange(TabbedNotebook1,TabbedNotebook1.PageIndex,kell);
end;

procedure TJaratbeDlg.BitBtn62Click(Sender: TObject);
begin
  if Query14.FieldByName('JK_CSJAR').AsString='' then exit;
	if NoticeKi('Val�ban t�r�lni akarja az aktu�lis rekordot?', NOT_QUESTION) = 0 then begin
		Query_Run(Query14,'DELETE FROM JARATKOLTSEG WHERE JK_JAKOD = '''+ret_kod+''' AND JK_CSJAR='''+Query14.FieldByName('JK_CSJAR').AsString+'''',true);
    Query_Run(Query14,'SELECT * FROM JARATKOLTSEG, JARAT WHERE JK_JAKOD = '''+ret_kod+''' AND JA_KOD =JK_CSJAR');
	end;

end;

procedure TJaratbeDlg.BitBtn63Click(Sender: TObject);
var
  jaruj: TJaratbeDlg;
begin
    if Query14.FieldByName('JK_CSJAR').AsString='' then exit;
		Application.CreateForm(TJaratbeDlg, jaruj);
    jaruj.megtekint:= True;
		jaruj.Tolto('J�rat megtekint�se',Query14.FieldByName('JK_CSJAR').AsString);
		jaruj.ShowModal;
		jaruj.Destroy;
end;

procedure TJaratbeDlg.BitBtn64Click(Sender: TObject);
var
   JaratSzam: string;
begin
   JaratSzam:= InputBox('J�ratsz�m megad�sa', 'J�ratsz�m: ', '');
   M02.text:= JaratSzam;
end;

function TJaratbeDlg.FAJKO_ellenor: boolean;
begin
	if Elindult and (cbFAJKO.ItemIndex = -1)  then begin
			NoticeKi('J�rat fajta megad�sa k�telez�!');
			cbFAJKO.Setfocus;
      Result:= True;
	  	end
  else Result:= False;
end;

end.

