unit Idobe;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, Mask;

type
  TIdobeDlg = class(TForm)
    Label6: TLabel;
    M5: TMaskEdit;
    BitBtn10: TBitBtn;
    M6: TMaskEdit;
    Label5: TLabel;
    BitElkuld: TBitBtn;
    BitKilep: TBitBtn;
    Label15: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    M7: TMaskEdit;
    M8: TMaskEdit;
    Label1: TLabel;
    Label2: TLabel;
    M51: TMaskEdit;
    BitBtn1: TBitBtn;
    M61: TMaskEdit;
    Label3: TLabel;
    procedure BitBtn10Click(Sender: TObject);
    procedure M6Exit(Sender: TObject);
    procedure M5Exit(Sender: TObject);
    procedure BitElkuldClick(Sender: TObject);
    procedure BitKilepClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure M51Exit(Sender: TObject);
    procedure M61Exit(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure M51KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
  public
  		ret_datum	: string;
      ret_idopt	: string;
  		ret_datum1	: string;
      ret_idopt1	: string;
      ret_paletta: string;
      ret_suly : string;
  end;

var
  IdobeDlg: TIdobeDlg;

implementation

uses
	Egyeb, Kozos;
{$R *.dfm}

procedure TIdobeDlg.BitBtn10Click(Sender: TObject);
begin
	EgyebDlg.Calendarbe(M51);
end;

procedure TIdobeDlg.M6Exit(Sender: TObject);
begin
	IdoExit(M6, false);
end;

procedure TIdobeDlg.M5Exit(Sender: TObject);
begin
	DatumExit(M5, false);
end;

procedure TIdobeDlg.BitElkuldClick(Sender: TObject);
begin
  if (M6.Text<>'') then
  begin
    if (M61.Text='') then
    begin
  		NoticeKi('Az �rkez�si id�t meg kell adni!');
	  	M61.SetFocus;
		  Exit;
    end;
{    if (Label3.Caption.Text='Lerak�') then
    begin
      if (StringSzam(M160.Text)=0)and(StringSzam(M16.Text)<>0) then
      begin
    		NoticeKi('A palett�t meg kell adni!');
	    	M160.SetFocus;
		    Exit;
      end;
      if (StringSzam(M90.Text)=0)and(StringSzam(M9.Text)<>0) then
      begin
  		  NoticeKi('A s�lyt meg kell adni!');
  	  	M90.SetFocus;
	  	  Exit;
      end;
    end;   }
  end;

	if (not DatumExit(M5, true))and(not DatumExit(M51, true)) then begin
   	Exit;
   end;
   if (not Idoexit( M6, false ))and(not Idoexit( M61, false )) then begin
       Exit;
   end;
   ret_datum	:= M5.Text;
   ret_idopt	:= M6.Text;
   ret_datum1	:= M51.Text;
   ret_idopt1	:= M61.Text;
   ret_paletta	:= M7.Text;
   ret_suly	:= M8.Text;
   Close;
end;

procedure TIdobeDlg.BitKilepClick(Sender: TObject);
begin
   ret_datum	:= '';
   ret_idopt	:= '';
   ret_datum1	:= '';
   ret_idopt1	:= '';
   ret_paletta	:= '';
   ret_suly	:= '';
   Close;
end;

procedure TIdobeDlg.FormCreate(Sender: TObject);
begin
   ret_datum	:= '';
   ret_idopt	:= '';
   ret_datum1	:= '';
   ret_idopt1	:= '';
   ret_paletta	:= '';
   ret_suly	:= '';
end;

procedure TIdobeDlg.M51Exit(Sender: TObject);
begin
	DatumExit(M51, false);

end;

procedure TIdobeDlg.M61Exit(Sender: TObject);
begin
	IdoExit(M61, false);

end;

procedure TIdobeDlg.BitBtn1Click(Sender: TObject);
begin
	EgyebDlg.Calendarbe(M5);

end;

procedure TIdobeDlg.FormShow(Sender: TObject);
begin
  if M61.Text='' then
   ActiveControl:=M61;
end;

procedure TIdobeDlg.M51KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
	EgyebDlg.FormReturn(Key);
end;

end.
