unit J_Fogyaszt;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, Contnrs, DB, ADODB, Mask, ExtCtrls;

type

	TSoforSzakasz	= class
		constructor	Create(kod : string; km1, km2 : integer);
	private
		soforkod	: string;
		soforkm1	: integer;
		soforkm2	: integer;
	end;

	TNormaSzakasz	= class
		constructor	Create(dat : string; km : integer; sima,apeh, klima : double);
	private
		normakm				: integer;
		normadatum			: string;
		simanorma			: double;
		apehnorma			: double;
		klimaszaz			: double;
	end;

	TPotSzakasz	= class
		constructor	Create(rsz : string; km1, km2 : integer; ons,slt,alt : double);
	private
		potrsz	: string;
		potkm1	: integer;
		potkm2	: integer;
		onsuly	: double;
		sulylt	: double;
		apehlt	: double;
	end;

	TSulySzakasz	= class
		constructor	Create(rsz : string; km1, km2 : integer; suly : double);
	private
		sulyrsz	: string;
		sulykm1	: integer;
		sulykm2	: integer;
		sulyt	: double;
	end;

	TUtSzakasz	= class
		constructor	Create(km1, km2 : integer);
	public
		utsofor		: string;
		utsofordb	: integer;
		utpotkocsi	: string;
		utkezdokm	: integer;
		utvegzokm	: integer;
		utonsuly	: double;   	// Az adott szakaszon l�v� p�tkocsik �ns�lya (tonna)
		utraksuly	: double;		// Az adott szakaszon l�v� s�ly ( tonna )
    ut_ervenyes_tank: boolean; // Az adott szakaszhoz tal�ltunk-e �rv�nyes tankol�st
    ut_ervenyes_adblue: boolean; // Az adott szakaszhoz tal�ltunk-e �rv�nyes adblue tankol�st
		uttenyatlag	: double;		// Az adott szakaszhoz tartoz� tankol�s �tlaga
		uttenyliter	: double;       // A t�ny liter (km2-km1/100*t�ny�tlag)
		uttelepatlag: double;		// Az adott szakaszon a telepi tankol�sok �tlaga
		uttelepliter: double;		// Az adott szakaszon a telepi tankol�sok litere
		simasulylt	: double;       // A norm�l suly/liter �rt�ke az adott km-en
		apehsulylt	: double;       // Az apeh suly/liter �rt�ke az adott km-en
		simaatlag	: double;		// Az adott szakaszra jut� tervezett �tlagfogyaszt�s
		apehatlag	: double;		// Az APEH szerinti tervezett fogyaszt�s
		utkoltseg	: double;		// A szakaszra es� k�lts�g
		blueatlag	: double;		// A szakaszra es� ad blue �tlag fogyaszt�s
		blueliter	: double;		// A szakaszon elfogyott ad blue liter
		bluekoltg	: double;		// A szakaszra es� ad blue k�lts�g (Ft)
		belsoszaz	: double;		// A belf�ldi szakasz norma % -a
	end;

	TTankolas	= class
		constructor	Create(km1, km2 : integer; uzlt, atl, ktg, tlt, tatl : double);
	public
		tankkm1		: integer;
		tankkm2  	: integer;
		uzemalt		: double;
		atlag  		: double;
		uzktg		: double;
		telepilt	: double;
		telepiatlag	: double;
	end;

	TAdBlueTankolas	= class
		constructor	Create(km1, km2 : integer; uzlt, atl, ktg : double);
	public
		adbluekm1		: integer;
		adbluekm2  		: integer;
		adbluelt		: double;
		adblueatl  		: double;
		adbluektg		: double;
	end;

	TFogyasztasDlg = class(TForm)
	 Memo1: TMemo;
	 QueryDat1: TADOQuery;
	 QueryDat2: TADOQuery;
	 QueryKoz2: TADOQuery;
	 QueryKoz1: TADOQuery;
	 Panel1: TPanel;
	 M0: TMaskEdit;
	 M1: TMaskEdit;
	 BitBtn5: TBitBtn;
	 Label1: TLabel;
	 ButtonPrint: TBitBtn;
	 BitKilep: TBitBtn;
	 BitElkuld: TBitBtn;
	 QueryGepkocsi: TADOQuery;
	 M2: TMaskEdit;
	 BitBtn1: TBitBtn;
	 RadioButton1: TRadioButton;
	 RadioButton2: TRadioButton;
	 M3: TMaskEdit;
	 M4: TMaskEdit;
	 Label2: TLabel;
	 BitBtn2: TBitBtn;
    QueryJarpotos: TADOQuery;
    QueryJarsofor: TADOQuery;
    QuerySulyok: TADOQuery;
    QueryJarat: TADOQuery;
    QueryDolgozo: TADOQuery;

	 // BEL�P�SI PONTOK !!!
	 procedure JaratBetoltes(jakod : string);
	 procedure GeneralBetoltes(rensz : string; km1, km2 : integer);

	 procedure FormCreate(Sender: TObject);
	 procedure JaratBetoltesAlap(jakod : string);
	 procedure AddSoforJarat(jakod : string);
	 procedure AddPotJarat(jakod : string);
	 procedure AddNorma(dat : string; km : integer);
	 procedure AddSulyJarat(jakod : string);
	 procedure AddTankolas(km1, km2 : integer; uzlt, atl, ktg, telt, teatlag : double);
	 procedure AddAdBlue(km1, km2 : integer; uzlt, atl, ktg : double);
   function CreateTankLista: string;
   function CreateAdBlueLista: string;
	 procedure AddUtSzakasz(km1, km2, sorsz : integer);
	 procedure SoforSzLista;
	 procedure PotSzLista;
	 procedure SulySzLista;
	 procedure UtSzLista;
	 procedure AdBlueListazas;
	 procedure TankListazo;
	 procedure NormaListazo;
	 procedure TeljesLista;
	 procedure FormDestroy(Sender: TObject);
	 procedure Szakaszolo;
	 procedure SzakaszkmHozzaado(km : integer);
	 procedure BitBtn5Click(Sender: TObject);
	 procedure ListaUrites;
	 procedure SzakaszSzamito;
	 function  GetMegtakaritas : double;
	 function  GetApehMegtakaritas : double;
	 function  GetTenyFogyasztas : double;
	 function  GetApehTenyFogyasztas : double;
	 function  GetBlueFogyasztas : double;
	 function  GetTervFogyasztas : double;
	 function  GetApehTervFogyasztas : double;
	 function  GetKoltseg : double;
	 function  GetBlueKoltseg : double;
	 function  GetSulyAtlag : double;
   function  GetAtlag : double;
   function  GetSoforLista : TStringList;
	 function  GetSoforSulyAtlag(skod : string) : double;
	 function  GetSoforMegtakaritas(skod : string) : double;
	 function  GetSoforApehMegtakaritas(skod : string) : double;
	 function  GetSoforTeny(skod : string) : double;
	 function  GetSoforApehTeny(skod : string) : double;
	 function  GetSoforBlueTeny(skod : string) : double;
	 function  GetSoforKm(skod : string) : double;
	 function  GetSoforKoltseg(skod : string) : double;
	 function  GetSoforBlueKoltseg(skod : string) : double;
	 function  GetSoforTerv(skod : string) : double;
	 function  GetSoforApehTerv(skod : string) : double;
	 function  GetBlueAtlag(km : integer) : double;
	 procedure GetSzakaszNorma(km : integer; var sinor, apnor, klsz : double);
	 function  GetBelfoldiSzaz(jakod : string) : double;
	 procedure ClearAllList;
	 procedure ButtonPrintClick(Sender: TObject);
	 procedure BitKilepClick(Sender: TObject);
	 procedure BitElkuldClick(Sender: TObject);
	 function  GetOsszKm : double;
	 procedure M1KeyDown(Sender: TObject; var Key: Word;
	   Shift: TShiftState);
	 procedure RadioButton1Click(Sender: TObject);
	 procedure BitBtn2Click(Sender: TObject);
	 procedure BitBtn1Click(Sender: TObject);
  private
		jaratszam			: string;
       ki_jaratszam		: string;
		rendszam			: string;
		kezdokm				: integer;
		vegzokm				: integer;
		nyitokm				: integer;
		zarokm				: integer;
		kezdodatum			: string;
		vegzodatum			: string;
		SoforSzakaszLista	: TObjectList;
		PotSzakaszLista		: TObjectList;
		SulySzakaszLista	: TObjectList;
		UtSzakaszLista		: TObjectList;
		TankolasLista		: TObjectList;
		AdBlueLista			: TObjectList;
		NormaLista			: TObjectList;
		// A hiba�zenetek t�rol�sa a k�s�bbi megjelen�t�shez
		SoforHiba			: string;
		TankolasHiba		: string;
		AdBlueHiba			: string;
       elozoschema         : string;
       kovetschema         : string;
  public
    ErvenyesTenyFogyasztas: boolean;  // igaz, ha minden �tszakaszhoz tal�ltunk teli tankol�ssal v�gz�d� tankol�s szakaszt
    ErvenyesAdBlueFogyasztas: boolean;  // ugyanaz AdBlue-val
  end;


var
  FogyasztasDlg: TFogyasztasDlg;

implementation

uses
	Egyeb, J_SQL, Kozos, J_Valaszto, Kozos_Local;

{$R *.dfm}

function NormaHasonlito(Item1, Item2: Pointer): Integer;
begin
	Result	:=  TNormaSzakasz(Item1).normakm - TNormaSzakasz(Item2).normakm;
end;

(******************************************************************************
A NORMASZAKASZ defin�ci�ja
******************************************************************************)

constructor TNormaSzakasz.Create(dat : string; km : integer; sima,apeh, klima : double);
begin
	normakm				:= km;
	normadatum			:= dat;
	simanorma			:= sima;
	apehnorma			:= apeh;
	klimaszaz			:= klima;
end;

(******************************************************************************
A SOFORSZAKASZ defin�ci�ja
******************************************************************************)

constructor TSoforSzakasz.Create(kod : string; km1, km2 : integer);
begin
	soforkod	:= kod;
	soforkm1	:= km1;
	soforkm2	:= km2;
end;

(******************************************************************************
A POTSZAKASZ defin�ci�ja
******************************************************************************)

constructor TPotSzakasz.Create(rsz : string; km1, km2 : integer; ons, slt, alt : double);
begin
	potrsz	:= rsz;
	potkm1	:= km1;
	potkm2	:= km2;
	onsuly	:= ons;
	sulylt	:= slt;
	apehlt	:= alt;
end;

(******************************************************************************
A SULYSZAKASZ defin�ci�ja
******************************************************************************)

constructor TSulySzakasz.Create(rsz : string; km1, km2 : integer; suly : double);
begin
	sulyrsz	:= rsz;
   sulykm1	:= km1;
   sulykm2	:= km2;
   sulyt	:= suly;
end;

(******************************************************************************
Az UTSZAKASZ defin�ci�ja
******************************************************************************)

constructor TUtSzakasz.Create(km1, km2 : integer);
begin
	utsofor		:= '';
   utpotkocsi	:= '';
   utkezdokm	:= km1;
   utvegzokm	:= km2;
   utraksuly	:= 0;
   utonsuly	:= 0;
end;

(******************************************************************************
A TANKOLAS defin�ci�ja
*****************************************************************************)
constructor TTankolas.Create(km1, km2 : integer; uzlt, atl, ktg, tlt, tatl : double);
begin
	tankkm1  	:= km1;
	tankkm2  	:= km2;
	uzemalt		:= uzlt;
	atlag  		:= atl;
	uzktg		:= ktg;
	telepilt	:= tlt;
	telepiatlag	:= tatl;
end;


(******************************************************************************
Az ADBLUE defin�ci�ja
******************************************************************************)
constructor TAdBlueTankolas.Create(km1, km2 : integer; uzlt, atl, ktg : double);
begin
	adbluekm1	:= km1;
	adbluekm2  	:= km2;
	adbluelt	:= uzlt;
	adblueatl  	:= atl;
	adbluektg	:= ktg;
end;

(******************************************************************************
A FOGYASZTASDLG defin�ci�ja
******************************************************************************)
procedure TFogyasztasDlg.FormCreate(Sender: TObject);
begin
	rendszam			:= '';
	jaratszam			:= '';
   ki_jaratszam		:= '';
	kezdokm				:= 0;
	vegzokm				:= 0;
	SoforSzakaszLista	:= TObjectList.Create;
	PotSzakaszLista		:= TObjectList.Create;
	SulySzakaszLista	:= TObjectList.Create;
	UtSzakaszLista		:= TObjectList.Create;
	TankolasLista		:= TObjectList.Create;
	AdBlueLista			:= TObjectList.Create;
	NormaLista			:= TObjectList.Create;
	EgyebDlg.SetADOQueryDatabase(QueryDat1);
	EgyebDlg.SetADOQueryDatabase(QueryDat2);
	EgyebDlg.SetADOQueryDatabase(QueryKoz1);
	EgyebDlg.SetADOQueryDatabase(QueryKoz2);
	EgyebDlg.SetADOQueryDatabase(QueryGepkocsi);
	EgyebDlg.SetADOQueryDatabase(QueryJarsofor);
	EgyebDlg.SetADOQueryDatabase(QueryJarpotos);
	EgyebDlg.SetADOQueryDatabase(QuerySulyok);
	EgyebDlg.SetADOQueryDatabase(QueryJarat);
	EgyebDlg.SetADOQueryDatabase(QueryDolgozo);
	// Az alaplek�rdez�sek v�grehajt�sa
  Query_Run(QueryGepkocsi, 'SELECT GK_RESZ, GK_OSULY, GK_SULYL, GK_SULYA, GK_APENY, GK_NORNY, GK_APETE, GK_NORMA, GK_KLIMA FROM GEPKOCSI ORDER BY GK_RESZ ');
	Query_Run(QueryJarsofor, 'SELECT JS_JAKOD, JS_SOFOR, JS_KMOR1, JS_KMOR2 FROM JARSOFOR ORDER BY JS_JAKOD ');
	Query_Run(QueryJarpotos, 'SELECT JP_JAKOD, JP_POREN, JP_PORKM, JP_PORK2 FROM JARPOTOS ORDER BY JP_JAKOD ');
	Query_Run(QuerySulyok,   'SELECT SU_JAKOD, SU_KMORA, SU_KMOR2, SU_ERTEK FROM SULYOK ORDER BY SU_JAKOD ');
	Query_Run(QueryJarat,    'SELECT * FROM JARAT ORDER BY JA_KOD ');
  Query_Run(QueryDolgozo,  'SELECT DO_NAME, DO_KOD FROM DOLGOZO ORDER BY DO_KOD ');

  // teszt: cs�kkentett adatokkal mennyivel lesz gyorsabb?
	{Query_Run(QueryGepkocsi, 'SELECT GK_RESZ, GK_OSULY, GK_SULYL, GK_SULYA, GK_APENY, GK_NORNY, GK_APETE, GK_NORMA, GK_KLIMA FROM GEPKOCSI ORDER BY GK_RESZ ');
	Query_Run(QueryJarsofor, 'SELECT JS_JAKOD, JS_SOFOR, JS_KMOR1, JS_KMOR2 FROM JARSOFOR '+
                           ' WHERE JS_JAKOD IN (SELECT JA_KOD FROM JARAT WHERE JA_JKEZD BETWEEN ''2018.01.01.'' AND ''2018.02.01.'') '+
                           'ORDER BY JS_JAKOD ');
	Query_Run(QueryJarpotos, 'SELECT JP_JAKOD, JP_POREN, JP_PORKM, JP_PORK2 FROM JARPOTOS '+
                           ' WHERE JP_JAKOD IN (SELECT JA_KOD FROM JARAT WHERE JA_JKEZD BETWEEN ''2018.01.01.'' AND ''2018.02.01.'') '+
                            'ORDER BY JP_JAKOD ');
	Query_Run(QuerySulyok,   'SELECT SU_JAKOD, SU_KMORA, SU_KMOR2, SU_ERTEK FROM SULYOK '+
                           ' WHERE SU_JAKOD IN (SELECT JA_KOD FROM JARAT WHERE JA_JKEZD BETWEEN ''2018.01.01.'' AND ''2018.02.01.'') '+
                           'ORDER BY SU_JAKOD ');
	Query_Run(QueryJarat,    'SELECT * FROM JARAT WHERE JA_JKEZD BETWEEN ''2018.01.01.'' AND ''2018.02.01.'' ORDER BY JA_KOD ');
	Query_Run(QueryDolgozo,  'SELECT DO_NAME, DO_KOD FROM DOLGOZO ORDER BY DO_KOD ');
   }

	RadioButton1Click(Sender);
   // A s�m�k meghat�roz�sa
   // 2017-03-30: kikapcsolom ezt a funkci�t, 2013 �ta egy s�m�ban dolgozunk
   // elozoschema     := GetTableSchema(copy(EgyebDlg.v_alap_db, 1, Length(EgyebDlg.v_alap_db) - 4 ) + IntToStr(StrToIntDef(EgyebDlg.Evszam,1000)-1), 'KOLTSEG');
   // kovetschema     := GetTableSchema(copy(EgyebDlg.v_alap_db, 1, Length(EgyebDlg.v_alap_db) - 4 ) + IntToStr(StrToIntDef(EgyebDlg.Evszam,1000)+1), 'KOLTSEG');
   elozoschema:= '';
   kovetschema:= '';

end;

procedure TFogyasztasDlg.AddSoforJarat(jakod : string);
var
	sofszak		: TSoforSzakasz;
begin
	// J�ratk�d alapj�n az �sszes sof�r km-et hozz�adjuk
	SoforHiba	:= '';
//	Query_Run(QueryDat1, 'SELECT JS_SOFOR, JS_KMOR1, JS_KMOR2 FROM JARSOFOR WHERE JS_JAKOD = '''+jakod+''' ');
   if not QueryJarsofor.Locate('JS_JAKOD', jakod, []) then begin
		SoforHiba	:= 'A j�rathoz nem tartoznak sof�r�k!!! ( '+ jaratszam + ')';
       Exit;
   end;
//	if QueryDat1.RecordCount > 0 then begin
   while ( ( not QueryJarsofor.Eof ) and (QueryJarsofor.FieldByName('JS_JAKOD').AsString = jakod) ) do begin
//		while not QueryDat1.Eof do begin
       sofszak		:= TSoforSzakasz.Create(QueryJarsofor.FieldByName('JS_SOFOR').AsString,
                       StrToIntDef(QueryJarsofor.FieldByName('JS_KMOR1').AsString, 0), StrToIntDef(QueryJarsofor.FieldByName('JS_KMOR2').AsString, 0));
       SoforSzakaszLista.Add(sofszak);
       QueryJarsofor.Next;
   end;
(*
	end else begin
		SoforHiba	:= 'A j�rathoz nem tartoznak sof�r�k!!! ( '+ jaratszam + ')';
	end;
*)
end;

procedure TFogyasztasDlg.AddPotJarat(jakod : string);
var
	potszak		: TPotSzakasz;
	onsuly	: double;
	suly1	: double;
	suly2	: double;
begin
	// J�ratk�d alapj�n az �sszes p�tkocsi km-et hozz�adjuk
//	Query_Run(QueryDat1, 'SELECT JP_POREN, JP_PORKM, JP_PORK2 FROM JARPOTOS WHERE JP_JAKOD = '''+jakod+''' ');
   if not QueryJarpotos.Locate('JP_JAKOD', jakod, []) then begin
       Exit;
   end;
//	while not QueryDat1.Eof do begin
   while ( ( not QueryJarpotos.Eof ) and (QueryJarpotos.FieldByName('JP_JAKOD').AsString = jakod) ) do begin
		onsuly	:= 0;
		suly1	:= 0;
		suly2	:= 0;
		if QueryGepkocsi.Locate('GK_RESZ', QueryJarpotos.FieldByName('JP_POREN').AsString, []) then begin
			onsuly	:= StringSzam(QueryGepkocsi.FieldByName('GK_OSULY').AsString) / 1000;
			suly1	:= StringSzam(QueryGepkocsi.FieldByName('GK_SULYL').AsString);
			suly2	:= StringSzam(QueryGepkocsi.FieldByName('GK_SULYA').AsString);
		end;
		potszak		:= TPotSzakasz.Create(QueryJarpotos.FieldByName('JP_POREN').AsString,
						StrToIntDef(QueryJarpotos.FieldByName('JP_PORKM').AsString, 0), StrToIntDef(QueryJarpotos.FieldByName('JP_PORK2').AsString, 0),
						onsuly, suly1, suly2 );
		PotSzakaszLista.Add(potszak);
//		QueryDat1.Next;
       QueryJarpotos.Next;
	end;
end;

procedure TFogyasztasDlg.AddTankolas(km1, km2 : integer; uzlt, atl, ktg, telt, teatlag : double);
var
	tank	: TTankolas;
begin
	tank	:= TTankolas.Create(km1, km2, uzlt, atl, ktg, telt, teatlag);
	TankolasLista.Add(tank);
end;

procedure TFogyasztasDlg.AddAdBlue(km1, km2 : integer; uzlt, atl, ktg : double);
var
	tank	: TAdBlueTankolas;
begin
	tank	:= TAdBlueTankolas.Create(km1, km2, uzlt, atl, ktg);
	AdBlueLista.Add(tank);
end;

function TFogyasztasDlg.CreateTankLista: string;
var
	tankkezdes	: integer;
   tankvegzes	: integer;
   uzmenny		: double;
   km1			: integer;
   atlag		: double;
   koltseg		: double;
//   tetanstr	: string;
	kellelozo	: boolean;
	kellkovet	: boolean;
	teleplt		: double;
	telepatlag	: double;
	datumkezd	: string;
begin
	// A kezd�/befejez� km-ek alapj�n beolvassa a tankol�sokat
	TankolasHiba	:= '';
	TankolasLista.Clear;
//	Query_Run(QueryDat1, 'SELECT MAX(KS_KMORA) KMORA FROM KOLTSEG WHERE KS_RENDSZ = '''+rendszam+''' '+
	Query_Run(QueryDat1, 'SELECT KS_DATUM, KS_KMORA FROM KOLTSEG WHERE KS_RENDSZ = '''+rendszam+''' '+
		' AND KS_KMORA <= '+IntToStr(nyitokm)+
		' AND KS_TELE  = 1 ' +
  //		' AND KS_TETAN IN ( ' + tetanstr+ ' ) ' +
		' AND KS_TETAN = 0 ' +
		' AND KS_TIPUS = 0 ' +
		' ORDER BY KS_KMORA ');
	QueryDat1.Last;
	kellelozo	:= false;
	tankkezdes	:= StrToIntDef(QueryDat1.FieldByName('KS_KMORA').AsString,0); // a nyitokm-t megel�z� utols� teletank �ra�ll�sa
	datumkezd 	:= QueryDat1.FieldByName('KS_DATUM').AsString;  // a nyitokm-t megel�z� utols� teletank d�tuma
	if ( ( QueryDat1.RecordCount < 1 ) or ( tankkezdes = 0 ) ) then begin
		// Keres�nk az el�z� �vb�l tankol�st
		if elozoschema <> '' then begin
			// Volt el�z� �vi adat
			// kellelozo	:= true;  // kivettem 2017-03-30
			Query_Run(QueryDat1, 'SELECT KS_DATUM, KS_KMORA FROM '+elozoschema+'.KOLTSEG WHERE KS_RENDSZ = '''+rendszam+''' '+
				' AND KS_KMORA <= '+IntToStr(nyitokm)+
				' AND KS_TELE  = 1 ' +
//				' AND KS_TETAN IN ( ' + tetanstr+ ' ) ' +
				' AND KS_TETAN = 0 ' +
				' AND KS_TIPUS = 0 ' +
				' ORDER BY KS_KMORA ' );
			QueryDat1.Last;
			tankkezdes	:= StrToIntDef(QueryDat1.FieldByName('KS_KMORA').AsString,0);
			datumkezd 	:= QueryDat1.FieldByName('KS_DATUM').AsString;
		end;
	end;
	if ( ( QueryDat1.RecordCount < 1 ) or ( tankkezdes = 0 ) ) then begin
		TankolasHiba	:= 'Hi�nyz� kezd� km el�tti �zemanyag tankol�s : '+rendszam + ' -> kezd� km : ' + IntToStr(nyitokm);
    Result:= TankolasHiba;
		Exit;
	end;
	Query_Run(QueryDat1, 'SELECT MIN(KS_KMORA) KMORA FROM KOLTSEG WHERE KS_RENDSZ = '''+rendszam+''' '+
		' AND KS_KMORA >= '+IntToStr(zarokm)+
		' AND KS_TELE  = 1 ' +
		' AND KS_TETAN = 0 ' +
		' AND KS_TIPUS = 0 ' );
	tankvegzes	:= StrToIntDef(QueryDat1.FieldByName('KMORA').AsString, 0);
	kellkovet	:= false;
	if ( ( QueryDat1.RecordCount < 1 ) or (tankvegzes = 0 ) ) then begin
		// Keres�nk a k�vetkez� �vb�l a tankol�st
		if kovetschema <> '' then begin
			// Volt el�z� �vi adat
			kellkovet	:= true;
			Query_Run(QueryDat1, 'SELECT MAX(KS_KMORA) KMORA FROM '+kovetschema+'.KOLTSEG WHERE KS_RENDSZ = '''+rendszam+''' '+
				' AND KS_KMORA >= '+IntToStr(zarokm)+
				' AND KS_TELE  = 1 ' +
//				' AND KS_TETAN IN ( ' + tetanstr+ ' ) ' +
				' AND KS_TETAN = 0 ' +
				' AND KS_TIPUS = 0 ' );
			tankvegzes	:= StrToIntDef(QueryDat1.FieldByName('KMORA').AsString,0);
		end;
	end;
	if ( ( QueryDat1.RecordCount < 1 ) or (tankvegzes = 0 ) ) then begin
		TankolasHiba	:= 'Hi�nyz� z�r� km ut�ni �zemanyag tankol�s : '+rendszam + ' -> z�r� km : ' + IntToStr(zarokm);
    Result:= TankolasHiba;
		Exit;
	end;
	// Beolvassuk a k�t tele tank k�z�tti tankol�sokat -> a telepieket is

	if ( ( kellelozo ) and ( kellkovet ) ) then begin
		Query_Run(QueryDat1,
			'SELECT KS_DATUM, KS_TELE, KS_KMORA, KS_UZMENY, KS_ERTEK, KS_TETAN FROM KOLTSEG WHERE KS_RENDSZ = '''+rendszam+''' '+
			' AND KS_KMORA > '+IntToStr(tankkezdes)+ ' AND KS_KMORA <= '+IntToStr(tankvegzes)+
			' AND KS_TIPUS = 0 '+
			' UNION '+
			' SELECT KS_DATUM, KS_TELE, KS_KMORA, KS_UZMENY, KS_ERTEK, KS_TETAN FROM '+elozoschema+'.KOLTSEG WHERE KS_RENDSZ = '''+rendszam+''' '+
			' AND KS_KMORA > '+IntToStr(tankkezdes)+ ' AND KS_KMORA <= '+IntToStr(tankvegzes)+
			' AND KS_TIPUS = 0 '+
			' UNION '+
			' SELECT KS_DATUM, KS_TELE, KS_KMORA, KS_UZMENY, KS_ERTEK, KS_TETAN FROM '+kovetschema+'.KOLTSEG WHERE KS_RENDSZ = '''+rendszam+''' '+
			' AND KS_KMORA > '+IntToStr(tankkezdes)+ ' AND KS_KMORA <= '+IntToStr(tankvegzes)+
			' AND KS_TIPUS = 0 '+
			' ORDER BY KS_KMORA, KS_TETAN DESC, KS_TELE ' );
	end else begin
		if kellelozo then begin
			Query_Run(QueryDat1, 'SELECT KS_DATUM, KS_TELE, KS_KMORA, KS_UZMENY, KS_ERTEK, KS_TETAN FROM KOLTSEG WHERE KS_RENDSZ = '''+rendszam+''' '+
				' AND KS_KMORA > '+IntToStr(tankkezdes)+ ' AND KS_KMORA <= '+IntToStr(tankvegzes)+
				' AND KS_TIPUS = 0 '+
				' UNION '+
				' SELECT KS_DATUM, KS_TELE, KS_KMORA, KS_UZMENY, KS_ERTEK, KS_TETAN FROM '+elozoschema+'.KOLTSEG WHERE KS_RENDSZ = '''+rendszam+''' '+
				' AND KS_KMORA > '+IntToStr(tankkezdes)+ ' AND KS_KMORA <= '+IntToStr(tankvegzes)+
				' AND KS_TIPUS = 0 '+
				' ORDER BY KS_KMORA, KS_TETAN DESC, KS_TELE ' );
		end else begin
			if kellkovet then begin
			Query_Run(QueryDat1,
				'SELECT KS_DATUM, KS_TELE, KS_KMORA, KS_UZMENY, KS_ERTEK, KS_TETAN FROM KOLTSEG WHERE KS_RENDSZ = '''+rendszam+''' '+
				' AND KS_KMORA > '+IntToStr(tankkezdes)+ ' AND KS_KMORA <= '+IntToStr(tankvegzes)+
				' AND KS_TIPUS = 0 '+
				' UNION '+
				' SELECT KS_DATUM, KS_TELE, KS_KMORA, KS_UZMENY, KS_ERTEK, KS_TETAN FROM '+kovetschema+'.KOLTSEG WHERE KS_RENDSZ = '''+rendszam+''' '+
				' AND KS_KMORA > '+IntToStr(tankkezdes)+ ' AND KS_KMORA <= '+IntToStr(tankvegzes)+
				' AND KS_TIPUS = 0 '+
				' ORDER BY KS_KMORA, KS_TETAN DESC, KS_TELE ' );
			end else begin
				Query_Run(QueryDat1, 'SELECT KS_DATUM, KS_TELE, KS_KMORA, KS_UZMENY, KS_ERTEK, KS_TETAN FROM KOLTSEG WHERE KS_RENDSZ = '''+rendszam+''' '+
					' AND KS_KMORA > '+IntToStr(tankkezdes)+ ' AND KS_KMORA <= '+IntToStr(tankvegzes)+
					' AND KS_TIPUS = 0 ORDER BY KS_KMORA, KS_TETAN DESC, KS_TELE ' );
			end;
		end;
	end;

	uzmenny		:= 0;
	koltseg		:= 0;
	teleplt		:= 0;
//	km1			:= StrToIntDef(QueryDat1.FieldByName('KS_KMORA').AsString, 0);
	km1			:= tankkezdes;
	AddNorma(datumkezd, tankkezdes);
//	QueryDat1.Next;
	while not QueryDat1.Eof do begin
		if QueryDat1.FieldByName('KS_TETAN').AsString = '1' then begin
			// A telepi tankol�s hozz�ad�sa
			teleplt	:= teleplt + StringSzam(QueryDat1.FieldByName('KS_UZMENY').AsString);
			AddNorma(QueryDat1.FieldByName('KS_DATUM').AsString, StrToIntDef(QueryDat1.FieldByName('KS_KMORA').AsString, 0));
		end else begin
(*
			if ( ( StrToIntDef(QueryDat1.FieldByName('KS_TELE').AsString, 0) < 1 ) and
				 ( tankkezdes = StrToIntDef(QueryDat1.FieldByName('KS_KMORA').AsString, 0) ) ) then begin
				// Ki kell hagyni azokat, amik kezdeti tele tankn�l vannak m�g megrakva
			end else begin
*)
				uzmenny	:= uzmenny + StringSzam(QueryDat1.FieldByName('KS_UZMENY').AsString);
				koltseg	:= koltseg + StringSzam(QueryDat1.FieldByName('KS_ERTEK').AsString);
//			end;
			if QueryDat1.FieldByName('KS_TELE').AsString = '1' then begin
				// L�trehozzuk a tankol�si rekordot
				atlag			:= 0;
				telepatlag     	:= 0;
				if (StrToIntDef(QueryDat1.FieldByName('KS_KMORA').AsString, 0) - km1) > 0 then begin
					atlag 		:= uzmenny  * 100 / (StrToIntDef(QueryDat1.FieldByName('KS_KMORA').AsString, 0) - km1);
					telepatlag 	:= teleplt * 100 / (StrToIntDef(QueryDat1.FieldByName('KS_KMORA').AsString, 0) - km1);
				end;
				AddTankolas(km1, StrToIntDef(QueryDat1.FieldByName('KS_KMORA').AsString, 0), uzmenny, atlag, koltseg, teleplt, telepatlag);
				AddNorma(QueryDat1.FieldByName('KS_DATUM').AsString, StrToIntDef(QueryDat1.FieldByName('KS_KMORA').AsString, 0));
				uzmenny	:= 0;
				koltseg	:= 0;
				teleplt	:= 0;
				km1		:= StrToIntDef(QueryDat1.FieldByName('KS_KMORA').AsString, 0);
			end;
		end;
		QueryDat1.Next;
	end;
end;

function TFogyasztasDlg.CreateAdBlueLista: string;
var
	tankkezdes	: integer;
	tankvegzes	: integer;
	uzmenny		: double;
	km1			: integer;
	atlag		: double;
	koltseg		: double;
	kellelozo	: boolean;
begin
	// A kezd�/befejez� km-ek alapj�n beolvassa az AdBlue tankol�sokat
	AdBlueHiba		:= '';
	AdBlueLista.Clear;
	Query_Run(QueryDat1, 'SELECT MAX(KS_KMORA) KMORA FROM KOLTSEG WHERE KS_RENDSZ = '''+rendszam+''' '+
		' AND KS_KMORA <= '+IntToStr(nyitokm)+
		' AND KS_TELE  = 1 ' +
		' AND KS_TIPUS = 2 ' );
	kellelozo	:= false;
	tankkezdes	:= StrToIntDef(QueryDat1.FieldByName('KMORA').AsString,0);
	if ( ( QueryDat1.RecordCount < 1 ) or ( tankkezdes = 0 ) ) then begin
		// Keres�nk az el�z� �vb�l tankol�st

		// kellelozo	:= true;  // kivettem 2017-03-30

		if elozoschema <> '' then begin
			// Volt el�z� �vi adat
			Query_Run(QueryDat1, 'SELECT MAX(KS_KMORA) KMORA FROM '+elozoschema+'.KOLTSEG WHERE KS_RENDSZ = '''+rendszam+''' '+
				' AND KS_KMORA <= '+IntToStr(nyitokm)+
				' AND KS_TELE  = 1 ' +
				' AND KS_TIPUS = 2 ' );
			tankkezdes	:= StrToIntDef(QueryDat1.FieldByName('KMORA').AsString,0);
		end;
	end;
	if ( ( QueryDat1.RecordCount < 1 ) or ( tankkezdes = 0 ) ) then begin
		AdBlueHiba	:= 'Hi�nyz� kezd� km el�tti ad-blue tankol�s : '+rendszam + ' -> kezd� km : ' + IntToStr(nyitokm);
    Result:= AdBlueHiba;
		Exit;
	end;
	Query_Run(QueryDat1, 'SELECT MIN(KS_KMORA) KMORA FROM KOLTSEG WHERE KS_RENDSZ = '''+rendszam+''' '+
		' AND KS_KMORA >= '+IntToStr(zarokm)+
		' AND KS_TELE  = 1 ' +
		' AND KS_TIPUS = 2 ' );
	tankvegzes	:= StrToIntDef(QueryDat1.FieldByName('KMORA').AsString, 0);
	if ( ( QueryDat1.RecordCount < 1 ) or (tankvegzes = 0 ) ) then begin
		AdBlueHiba	:= 'Hi�nyz� z�r� km ut�ni ad-blue tankol�s : '+rendszam + ' -> z�r� km : ' + IntToStr(zarokm);
    Result:= AdBlueHiba;
		Exit;
	end;
	// Beolvassuk a k�t tele tank k�z�tti tankol�sokat
	if kellelozo then begin
		if elozoschema <> '' then begin
			// Volt el�z� �vi adat
			Query_Run(QueryDat1,
				'SELECT KS_TELE, KS_KMORA, KS_UZMENY, KS_ERTEK FROM KOLTSEG WHERE KS_RENDSZ = '''+rendszam+''' '+
				' AND KS_KMORA >= '+IntToStr(tankkezdes)+ ' AND KS_KMORA <= '+IntToStr(tankvegzes)+
				' AND KS_TIPUS = 2 '+
				' UNION '+
				' SELECT KS_TELE, KS_KMORA, KS_UZMENY, KS_ERTEK FROM '+elozoschema+'.KOLTSEG WHERE KS_RENDSZ = '''+rendszam+''' '+
				' AND KS_KMORA >= '+IntToStr(tankkezdes)+ ' AND KS_KMORA <= '+IntToStr(tankvegzes)+
				' AND KS_TIPUS = 2 '+
				' ORDER BY KS_KMORA, KS_TELE ' );
		end else begin
			Query_Run(QueryDat1,
				'SELECT KS_TELE, KS_KMORA, KS_UZMENY, KS_ERTEK FROM KOLTSEG WHERE KS_RENDSZ = '''+rendszam+''' '+
				' AND KS_KMORA >= '+IntToStr(tankkezdes)+ ' AND KS_KMORA <= '+IntToStr(tankvegzes)+
				' AND KS_TIPUS = 2 ORDER BY KS_KMORA, KS_TELE ' );
		end;
	end else begin
		Query_Run(QueryDat1, 'SELECT KS_TELE, KS_KMORA, KS_UZMENY, KS_ERTEK FROM KOLTSEG WHERE KS_RENDSZ = '''+rendszam+''' '+
			' AND KS_KMORA >= '+IntToStr(tankkezdes)+
			' AND KS_KMORA <= '+IntToStr(tankvegzes)+
			' AND KS_TIPUS = 2 '+
			' ORDER BY KS_KMORA, KS_TELE ' );
	end;
	while ( ( QueryDat1.FieldByName('KS_TELE').AsString <> '1' ) and (not QueryDat1.Eof) ) do begin
		QueryDat1.Next;
	end;
	uzmenny	:= 0;
	koltseg	:= 0;
	km1		:= StrToIntDef(QueryDat1.FieldByName('KS_KMORA').AsString, 0);
	QueryDat1.Next;
	while not QueryDat1.Eof do begin
		if ( ( StrToIntDef(QueryDat1.FieldByName('KS_TELE').AsString, 0) < 1 ) and
			 ( tankkezdes = StrToIntDef(QueryDat1.FieldByName('KS_KMORA').AsString, 0) ) ) then begin
			// Ki kell hagyni azokat, amik kezdeti tele tankn�l vannak m�g megrakva
		end else begin
			uzmenny	:= uzmenny + StringSzam(QueryDat1.FieldByName('KS_UZMENY').AsString);
			koltseg	:= koltseg + StringSzam(QueryDat1.FieldByName('KS_ERTEK').AsString);
		end;
		if QueryDat1.FieldByName('KS_TELE').AsString = '1' then begin
			// L�trehozzuk a tankol�si rekordot
			atlag	:=   0;
			if (StrToIntDef(QueryDat1.FieldByName('KS_KMORA').AsString, 0) - km1) > 0 then begin
				atlag := uzmenny * 100 / (StrToIntDef(QueryDat1.FieldByName('KS_KMORA').AsString, 0) - km1);
			end;
			AddAdBlue(km1, StrToIntDef(QueryDat1.FieldByName('KS_KMORA').AsString, 0), uzmenny, atlag, koltseg);
			uzmenny	:= 0;
			koltseg	:= 0;
			km1		:= StrToIntDef(QueryDat1.FieldByName('KS_KMORA').AsString, 0);
		end;
		QueryDat1.Next;
	end;
end;

procedure TFogyasztasDlg.AddSulyJarat(jakod : string);
var
	sulyszak		: TSulySzakasz;
begin
	// J�ratk�d alapj�n az �sszes s�ly km-et hozz�adjuk
//	Query_Run(QueryDat1, 'SELECT SU_KMORA, SU_KMOR2, SU_ERTEK FROM SULYOK WHERE SU_JAKOD = '''+jakod+''' ');
//	while not QueryDat1.Eof do begin
   if not QuerySulyok.Locate('SU_JAKOD', jakod, []) then begin
       Exit;
   end;
//	if QueryDat1.RecordCount > 0 then begin
   while ( ( not QuerySulyok.Eof ) and (QuerySulyok.FieldByName('SU_JAKOD').AsString = jakod) ) do begin
		sulyszak  := TSulySzakasz.Create('',StrToIntDef(QuerySulyok.FieldByName('SU_KMORA').AsString, 0),
           StrToIntDef(QuerySulyok.FieldByName('SU_KMOR2').AsString, 0),
			StringSzam(QuerySulyok.FieldByName('SU_ERTEK').AsString)/1000);
		SulySzakaszLista.Add(sulyszak);
		QuerySulyok.Next;
	end;
end;

procedure TFogyasztasDlg.FormDestroy(Sender: TObject);
begin
	ListaUrites;
end;

procedure TFogyasztasDlg.SoforSzLista;
var
	i 			: integer;
	sofszak     : TSoforSzakasz;
   sonev       : string;
begin
	if SoforSzakaszLista.Count > 0 then begin
		Memo1.Lines.Add('');
		Memo1.Lines.Add('Sof�rszakaszok :');
		Memo1.Lines.Add('****************');
		for i := 0 to SoforSzakaszLista.Count - 1 do begin
			sofszak	:= TSoforSzakasz(SoforSzakaszLista[i]);
           sonev   := '';
           if QueryDolgozo.Locate('DO_KOD', sofszak.soforkod, []) then begin
               sonev   := QueryDolgozo.FieldByName('DO_NAME').AsString;
           end;
			Memo1.Lines.Add(Format('%3d.', [i+1])+' K�d : ' + sofszak.Soforkod+'  '+copy(sonev+URESSTRING, 1, 25)+
				Format('  Sz.: %7d - %7d = %6d km ', [sofszak.soforkm1, sofszak.soforkm2, sofszak.soforkm2 - sofszak.soforkm1]) );
		end;
	end;
	if SoforHiba <> '' then begin
		Memo1.Lines.Add('');
		Memo1.Lines.Add('Hib�s sof�r adatok : '+SoforHiba);
		Memo1.Lines.Add('');
	end;
end;

procedure TFogyasztasDlg.PotSzLista;
var
	i 			: integer;
	potszak     : TPotSzakasz;
begin
	if PotSzakaszLista.Count > 0 then begin
		Memo1.Lines.Add('');
		Memo1.Lines.Add('P�tkocsi szakaszok :');
		Memo1.Lines.Add('********************');
		for i := 0 to PotSzakaszLista.Count - 1 do begin
			potszak	:= TPotSzakasz(PotSzakaszLista[i]);
			Memo1.Lines.Add(Format('%3d.', [i+1])+' Rsz:' + potszak.potrsz+ ' Sz.: '+
				Format('%7d - %7d = %6d km ', [potszak.potkm1, potszak.potkm2, potszak.potkm2 - potszak.potkm1]) +
				' �ns�ly: '+Format('%.3f t', [potszak.onsuly])+
				Format(' S�ly szerint/AP: %.2f / %.2f l/100km ', [potszak.sulylt, potszak.apehlt]));
		end;
	end;
end;

procedure TFogyasztasDlg.SulySzLista;
var
	i 			: integer;
	sulyszak    : TSulySzakasz;
begin
	if SulySzakaszLista.Count > 0 then begin
		Memo1.Lines.Add('');
		Memo1.Lines.Add('S�ly szakaszok :');
		Memo1.Lines.Add('****************');
		for i := 0 to SulySzakaszLista.Count - 1 do begin
			sulyszak	:= TSulySzakasz(SulySzakaszLista[i]);
			Memo1.Lines.Add(Format('%3d.', [i+1])+' Rsz : ' + sulyszak.sulyrsz+ '  Szakasz : '+
				Format('%7d - %7d = %6d km ', [sulyszak.sulykm1, sulyszak.sulykm2, sulyszak.sulykm2 - sulyszak.sulykm1]) +
				' s�ly : '+Format('%.3f', [sulyszak.sulyt]));
		end;
	end;
end;

procedure TFogyasztasDlg.UtSzLista;
var
	i 			: integer;
	utszak    	: TUtSzakasz;
	belsostr	: string;
   str			: string;
begin
	if UtSzakaszLista.Count > 0 then begin
//		SzakaszRendezo;
		Memo1.Lines.Add('');
		Memo1.Lines.Add('�tszakaszok :');
		Memo1.Lines.Add('*************');
		for i := 0 to UtSzakaszLista.Count - 1 do begin
			utszak	:= TUtSzakasz(UtSzakaszLista[i]);
			Memo1.Lines.Add(Format('%3d',[i+1])+'. Sof.: ' + copy(utszak.utsofor+'     ',1,5) + ' P�t.: ' + copy(utszak.utpotkocsi+'        ', 1,8) +
				' km: '+ Format('%7d - %7d = %7d km;', [utszak.utkezdokm, utszak.utvegzokm, utszak.utvegzokm - utszak.utkezdokm]) +
				' s�ly: '+Format('%6.3f + %6.3f = %6.3f t;', [utszak.utonsuly, utszak.utraksuly, utszak.utonsuly+utszak.utraksuly]) );
			Memo1.Lines.Add('   T�nyliter : '+Format('%7d km * %10.3f l/100km =      %10.3f liter (%.3f)', [utszak.utvegzokm - utszak.utkezdokm, utszak.uttenyatlag, utszak.uttenyliter, utszak.uttelepliter]) );
			// A belf�ldi % jelz�se
			belsostr	:= '';
			if utszak.belsoszaz > 0 then begin
				belsostr := Format(' (+%.2f%%)', [utszak.belsoszaz]);
			end;
			Memo1.Lines.Add('   '+
				'S�lyl./AP : '+Format('%10.3f', [utszak.simasulylt]) +' / '+Format('%10.3f', [utszak.apehsulylt]) +
				'   Terv l./AP : '+Format('%10.3f', [utszak.simaatlag])+ belsostr +' / '+Format('%10.3f', [utszak.apehatlag]) +
				'   Ktg. : '+Format('%10.2f Ft', [utszak.utkoltseg]) );
           str	:= '';
           if utszak.utvegzokm - utszak.utkezdokm <> 0 then begin
				str := Format('%10.3f / %10.3f l/100km', [(utszak.uttenyliter - utszak.simasulylt - utszak.simaatlag ) * -100 / ( utszak.utvegzokm - utszak.utkezdokm ), (utszak.uttenyliter + utszak.uttelepliter - utszak.apehsulylt - utszak.apehatlag ) * -100 / (utszak.utvegzokm - utszak.utkezdokm)]);
           end;
			Memo1.Lines.Add('   Megtakar. : '+Format('%10.3f / %10.3f liter', [-utszak.uttenyliter + utszak.simasulylt + utszak.simaatlag, -utszak.uttenyliter - utszak.uttelepliter + utszak.apehsulylt + utszak.apehatlag]) + '         (' + str + ')' );
			if ( ( utszak.blueliter <> 0 ) or ( utszak.blueatlag <> 0 ) or ( utszak.bluekoltg <> 0 ) ) then begin
				Memo1.Lines.Add('   Ad Blue liter / �tlag / k�lts�g : '+Format('%10.2f l', [utszak.blueliter]) +' / '+Format('%10.3f l/100km', [utszak.blueatlag]) +' / '+Format('%10.2f Ft', [utszak.bluekoltg]) );
           end;
			Memo1.Lines.Add('');
		end;
	end;
end;

procedure TFogyasztasDlg.TankListazo;
var
	i 			: integer;
	tank    	: TTankolas;
begin
	if TankolasLista.Count > 0 then begin
		Memo1.Lines.Add('');
		Memo1.Lines.Add('Tankol�sok :');
		Memo1.Lines.Add('************');
		for i := 0 to TankolasLista.Count - 1 do begin
			tank	:= TTankolas(TankolasLista[i]);
			if tank.telepilt <> 0 then begin
				Memo1.Lines.Add(Format('%3d.', [i+1])+' sz.:'+ IntToStr(tank.tankkm1) + ' - '+IntToStr(tank.tankkm2) +
					' = ' + IntToStr(tank.tankkm2 - tank.tankkm1) + ' km ' +
					'  '+Format('�a.:%7.2f l (%.2f)  �tlag:%7.3f l/100km (%.3f)  ktg.:%10.2f Ft', [tank.uzemalt, tank.telepilt, tank.atlag, tank.telepiatlag, tank.uzktg]));
			end else begin
				Memo1.Lines.Add(Format('%3d.', [i+1])+' sz.:'+ IntToStr(tank.tankkm1) + ' - '+IntToStr(tank.tankkm2) +
					' = ' + IntToStr(tank.tankkm2 - tank.tankkm1) + ' km ' +
					'  '+Format('�a.:%7.2f l   �tlag:%7.3f l/100km   ktg.:%10.2f Ft', [tank.uzemalt, tank.atlag, tank.uzktg]));
			end;
		end;
	end;
	if TankolasHiba <> '' then begin
		Memo1.Lines.Add('');
		Memo1.Lines.Add('Tankol�si hiba : '+TankolasHiba);
		Memo1.Lines.Add('');
	end;
end;

procedure TFogyasztasDlg.NormaListazo;
var
	i 			: integer;
	norm    	: TNormaSzakasz;
begin
	if NormaLista.Count > 0 then begin
		NormaLista.Sort(NormaHasonlito);
		Memo1.Lines.Add('');
		Memo1.Lines.Add('Norma-pontok :');
		Memo1.Lines.Add('**************');
		for i := 0 to NormaLista.Count - 1 do begin
			norm	:= TNormaSzakasz(NormaLista[i]);
			Memo1.Lines.Add(Format('%3d.', [i+1])+' sz. D�tum : '+norm.normadatum +' Km: '+IntToStr(norm.normakm) +
				'  '+Format(' Sima/apeh n.: %6.2f / %6.2f l/100km   Kl�ma %% :%5.1f Ft', [norm.simanorma, norm.apehnorma, norm.klimaszaz]));
		end;
		Memo1.Lines.Add('');
	end;
end;

procedure TFogyasztasDlg.AdBlueListazas;
var
	i 			: integer;
	tank    	: TAdBlueTankolas;
begin
	if AdBlueLista.Count > 0 then begin
		Memo1.Lines.Add('');
		Memo1.Lines.Add('Ad-Blue tankol�sok :');
		Memo1.Lines.Add('********************');
		for i := 0 to AdBlueLista.Count - 1 do begin
			tank	:= TAdBlueTankolas(AdBlueLista[i]);
			Memo1.Lines.Add(Format('%3d.', [i+1])+' sz.:'+ IntToStr(tank.adbluekm1) + ' - '+IntToStr(tank.adbluekm2) +
				' = ' + IntToStr(tank.adbluekm2 - tank.adbluekm1) + ' km ' +
				'  '+Format('�a.:%7.2f l   �tlag:%7.3f l/100km   ktg.:%10.2f Ft', [tank.adbluelt, tank.adblueatl, tank.adbluektg]));
		end;
	end;
	if AdBlueHiba <> '' then begin
		Memo1.Lines.Add('');
		Memo1.Lines.Add('Ad-Blue tankol�si hiba : '+AdBlueHiba);
		Memo1.Lines.Add('');
	end;
end;

procedure TFogyasztasDlg.TeljesLista;
var
	i 			: integer;
	sofszak		: TSoforSzakasz;
	voltmar		: string;
  sonev, S1, S2: string;
  TenyFogyasztas, BlueFogyasztas, Megtakaritas: double;
begin
	Memo1.Clear;
	Memo1.Lines.Add('D�tum : '+FormatDateTime('YYY.MM.DD hh:nn:ss', now));
	Memo1.Lines.Add('');
	Memo1.Lines.Add('Fogyaszt�si adatok');
	Memo1.Lines.Add('******************');
	Memo1.Lines.Add('');
	Memo1.Lines.Add('A j�ratsz�m      : '+ki_jaratszam);
	Memo1.Lines.Add('A rendsz�m       : '+rendszam);
	Memo1.Lines.Add('Az id�szak       : '+kezdodatum + ' - ' + vegzodatum);
	Memo1.Lines.Add('A km intervallum : '+IntToStr(nyitokm)+ ' - ' + IntToStr(zarokm) + ' = ' + IntToStr(zarokm-nyitokm)+ ' km');
	Memo1.Lines.Add('');
	TankListazo;
	NormaListazo;
	AdBlueListazas;
	SoforSzLista;
	PotSzLista;
	SulySzLista;
	UtSzLista;
	Memo1.Lines.Add('');
	Memo1.Lines.Add('�ltal�nos adatok : ');
  TenyFogyasztas:= GetTenyFogyasztas;
  if ErvenyesTenyFogyasztas then
    S1:= Format('%10.3f (%10.3f)', [GetTenyFogyasztas, GetApehTenyFogyasztas])
  else
    S1:= nem_szamolhato_cimke;
  BlueFogyasztas:= GetBlueFogyasztas;
  if ErvenyesAdBlueFogyasztas then
    S2:= Format('%10.3f', [GetBlueFogyasztas])
  else
    S2:= nem_szamolhato_cimke;
 	Memo1.Lines.Add('A t�nyleges fogyaszt�s  : '+S1+'           Ad-Blue fogyaszt�s : '+S2);
	if zarokm-nyitokm > 0 then begin
    if ErvenyesTenyFogyasztas then
      S1:= Format('%10.3f l/100km (%10.3f)', [TenyFogyasztas * 100/(zarokm-nyitokm), GetApehTenyFogyasztas * 100/(zarokm-nyitokm) ])
    else
      S1:= nem_szamolhato_cimke;
    if ErvenyesAdBlueFogyasztas then
      S2:= Format('%10.3f l/100km', [BlueFogyasztas * 100/(zarokm-nyitokm) ])
    else
      S2:= nem_szamolhato_cimke;
		Memo1.Lines.Add('Az �tlag fogyaszt�s     : '+S1+'   Ad-Blue �tlag      : '+S2);
	end else begin
		Memo1.Lines.Add('Az �tlag fogyaszt�s     : '+'0.000 l/100km' +'                      Az Ad-Blue �tlag        : '+'0.000 l/100km');
	end;
	Memo1.Lines.Add('Az �zemanyag k�lts�g    : '+Format('%10.3f', [GetKoltseg])+'                        Ad-Blue k�lts�g    : '+Format('%10.3f', [GetBlueKoltseg]));
	Memo1.Lines.Add('A terv fogy.   - norm�l : '+Format('%10.3f', [GetTervFogyasztas]) + ' - apeh : '+Format('%10.3f', [GetApehTervFogyasztas]));
  Megtakaritas:= GetMegtakaritas;
  if ErvenyesTenyFogyasztas then
      S1:= Format('%10.3f', [Megtakaritas])
    else
      S1:= nem_szamolhato_cimke;
	Memo1.Lines.Add('A megtakar�t�s - norm�l : '+ S1 + ' - apeh : '+Format('%10.3f', [GetApehMegtakaritas]));
	Memo1.Lines.Add('A s�ly�tlag             : '+Format('%10.3f', [GetSulyAtlag]));
	Memo1.Lines.Add('');
	Memo1.Lines.Add('Sof�r�k szerinti adatok : ');
	voltmar	:= ',';
	for i := 0 to SoforSzakaszLista.Count - 1 do begin
		sofszak	:= TSoforSzakasz(SoforSzakaszLista[i]);
		if Pos(','+sofszak.soforkod+',', voltmar) < 1 then begin
           sonev   := '';
           if QueryDolgozo.Locate('DO_KOD', sofszak.soforkod, []) then begin
               sonev   := QueryDolgozo.FieldByName('DO_NAME').AsString;
           end;
			Memo1.Lines.Add('A sof�r : '+sofszak.soforkod+' '+sonev);
			Memo1.Lines.Add('A megtett km               : '+Format('%10.0f', [GetSoforKm(sofszak.soforkod)]));
			Memo1.Lines.Add('A t�ny fogyaszt�s          : '+Format('%10.3f (%10.3f)', [GetSoforTeny(sofszak.soforkod), GetSoforApehTeny(sofszak.soforkod)]));
			Memo1.Lines.Add('Az Ad-Blue fogyaszt�s      : '+Format('%10.3f', [GetSoforBlueTeny(sofszak.soforkod)]));
			Memo1.Lines.Add('Az �zemanyag k�lts�g       : '+Format('%10.3f', [GetSoforKoltseg(sofszak.soforkod)]));
			Memo1.Lines.Add('Az Ad-Blue k�lts�g         : '+Format('%10.3f', [GetSoforBlueKoltseg(sofszak.soforkod)]));
			Memo1.Lines.Add('A terv fogyaszt�s - norm�l : '+Format('%10.3f', [GetSoforTerv(sofszak.soforkod)]) + ' - apeh : '+Format('%10.3f', [GetSoforApehTerv(sofszak.soforkod)]));
			Memo1.Lines.Add('A megtakar�t�s - norm�l    : '+Format('%10.3f', [GetSoforMegtakaritas(sofszak.soforkod)]) + ' - apeh : '+Format('%10.3f', [GetSoforApehMegtakaritas(sofszak.soforkod)]));
			Memo1.Lines.Add('A s�ly�tlag                : '+Format('%10.3f', [GetSoforSulyAtlag(sofszak.soforkod)]));
			Memo1.Lines.Add('');
			voltmar	:= voltmar + sofszak.soforkod+',';
		end;
	end;
end;

procedure TFogyasztasDlg.AddUtSzakasz(km1, km2, sorsz : integer);
var
	utszak	: TUtSzakasz;
begin
	utszak				:= TUtszakasz.Create(km1, km2);
	if sorsz = 0 then begin
		UtSzakaszLista.Add(utszak);
	end else begin
		UtSzakaszLista.Insert(sorsz, utszak);
	end;
end;

procedure TFogyasztasDlg.Szakaszolo;
var
	i			: integer;
	utszak      : TUtSzakasz;
begin
	// L�trehozza a bejegyz�shez tartoz� szakaszokat
	// Megkeress�k a legkisebb km-et
	if nyitokm = 0 then begin
		nyitokm	:= MaxInt;
		for i := 0 to SoforSzakaszLista.Count - 1 do begin
			if nyitokm >= TSoforSzakasz(SoforSzakaszLista[i]).soforkm1 then begin
				nyitokm := TSoforSzakasz(SoforSzakaszLista[i]).soforkm1;
			end;
		end;
	end;
	if zarokm = 0 then begin
		for i := 0 to SoforSzakaszLista.Count - 1 do begin
			if zarokm <= TSoforSzakasz(SoforSzakaszLista[i]).soforkm2 then begin
				zarokm := TSoforSzakasz(SoforSzakaszLista[i]).soforkm2;
			end;
		end;
	end;
	UtSZakaszLista.Clear;
	if nyitokm  = MaxInt then begin
		nyitokm := zarokm;
	end;
//	utszak				:= TUtSzakasz.Create(kezdokm, vegzokm);
	utszak				:= TUtSzakasz.Create(nyitokm, zarokm);
	UtSzakaszLista.Add(utszak);
	for i := 0 to SoforSzakaszLista.Count - 1 do begin
		SzakaszkmHozzaado(TSoforSzakasz(SoforSzakaszLista[i]).soforkm1);
		SzakaszkmHozzaado(TSoforSzakasz(SoforSzakaszLista[i]).soforkm2);
	end;
	for i := 0 to PotSzakaszLista.Count - 1 do begin
		SzakaszkmHozzaado(TPotSzakasz(PotSzakaszLista[i]).potkm1);
		SzakaszkmHozzaado(TPotSzakasz(PotSzakaszLista[i]).potkm2);
	end;
	for i := 0 to SulySzakaszLista.Count - 1 do begin
		SzakaszkmHozzaado(TSulySzakasz(SulySzakaszLista[i]).sulykm1);
		SzakaszkmHozzaado(TSulySzakasz(SulySzakaszLista[i]).sulykm2);
	end;
	for i := 0 to TankolasLista.Count - 1 do begin
		SzakaszkmHozzaado(TTankolas(TankolasLista[i]).tankkm1);
		SzakaszkmHozzaado(TTankolas(TankolasLista[i]).tankkm2);
	end;
	for i := 0 to AdBlueLista.Count - 1 do begin
		SzakaszkmHozzaado(TAdBlueTankolas(AdBlueLista[i]).adbluekm1);
		SzakaszkmHozzaado(TAdBlueTankolas(AdBlueLista[i]).adbluekm2);
	end;
	SzakaszSzamito;
end;

procedure TFogyasztasDlg.SzakaszkmHozzaado(km : integer);
var
	utsz	: integer;
	regikm	: integer;
begin
	utsz	:= 0;
	while utsz < UtSzakaszLista.Count do begin
		if ( 	( km > TUtSzakasz(UtSzakaszLista[utsz]).utkezdokm ) and
				( km < TUtSzakasz(UtSzakaszLista[utsz]).utvegzokm ) ) then begin
			// Az �j �tszakasz beilleszt�se az �tvonalba
			regikm	:= TUtSzakasz(UtSzakaszLista[utsz]).utvegzokm;
			TUtSzakasz(UtSzakaszLista[utsz]).utvegzokm	:= km;
			AddUtSzakasz(km, regikm, utsz+1);
			utsz	:= UtSzakaszLista.Count;
		end;
		Inc(utsz);
	end;
end;


procedure TFogyasztasDlg.JaratBetoltes(jakod : string);
begin
	JaratBetoltesAlap(jakod);
   nyitokm			:= kezdokm;
   zarokm			:= vegzokm;
   ki_jaratszam	:= jaratszam;
	AddNorma(kezdodatum, kezdokm);
	AddNorma(vegzodatum, vegzokm);
	AddSoforJarat(jakod);
	AddPotJarat(jakod);
	AddSulyJarat(jakod);
end;

procedure TFogyasztasDlg.BitBtn5Click(Sender: TObject);
begin
	ClearAllList;
	JaratValaszto( M0, M1);
	JaratBetoltes(M0.Text);
	CreateTankLista;
	CreateAdBlueLista;
	Szakaszolo;
   TeljesLista;
end;

procedure TFogyasztasDlg.ListaUrites;
var
	i : integer;
begin
	if SoforSzakaszLista <> nil then begin
   	for i := 0 to SoforSzakaszLista.Count - 1 do begin
			TSoforSzakasz(SoforSzakaszLista[i]).Free;
       end;
//       SoforSzakaszLista.Destroy;
	end;
	if PotSzakaszLista <> nil then begin
		for i := 0 to PotSzakaszLista.Count - 1 do begin
			TPotSzakasz(PotSzakaszLista[i]).Free;
		end;
//       PotSzakaszLista.Destroy;
	end;
	if SulySzakaszLista <> nil then begin
		for i := 0 to SulySzakaszLista.Count - 1 do begin
			TSulySzakasz(SulySzakaszLista[i]).Free;
		end;
//       SulySzakaszLista.Destroy;
	end;
	if TankolasLista <> nil then begin
		for i := 0 to TankolasLista.Count - 1 do begin
			TTankolas(TankolasLista[i]).Free;
		end;
//       TankolasLista.Destroy;
	end;
	if NormaLista <> nil then begin
		for i := 0 to NormaLista.Count - 1 do begin
			TTankolas(NormaLista[i]).Free;
		end;
//       NormaLista.Destroy;
	end;
	if UtSzakaszLista <> nil then begin
		for i := 0 to UtSzakaszLista.Count - 1 do begin
			TUtSzakasz(UtSzakaszLista[i]).Free;
		end;
//       UtSzakaszLista.Destroy;
	end;
end;

procedure TFogyasztasDlg.SzakaszSzamito;
var
	i 				: integer;
	utkozep			: double;
	utszak			: TUtSzakasz;
	tenyatlag   	: double;
	telepatlag		: double;
	j				: integer;
	onsuly			: double;
	raksuly			: double;
	soforok			: string;
	potkocsik		: string;
	sofordb			: integer;
	szorzo			: double;
	egysegktg		: double;
	simasulyliter 	: double;
	apehsulyliter 	: double;
	simanor 		: double;
	apehnor 		: double;
	klimaer 		: double;
	szorzo2			: double;
  jojakod         : string;
  van_tank, van_adblue: boolean;
begin
	// V�gigmegy az s�lyokon �s meg�llap�tja a rendsz�mokat
	for i := 0 to SulySzakaszLista.Count - 1 do begin
		utkozep		:= ( TSulySzakasz(SulySzakaszLista[i]).sulykm1 + TSulySzakasz(SulySzakaszLista[i]).sulykm2 ) / 2;
		j 			:= 0;
		while j < PotszakaszLista.Count do begin
			if  ( ( TPotszakasz(PotszakaszLista[j]).potkm1 < utkozep ) and
				  ( TPotszakasz(PotszakaszLista[j]).potkm2 > utkozep ) ) then begin
				TSulySzakasz(SulySzakaszLista[i]).sulyrsz	:= TPotszakasz(PotszakaszLista[j]).potrsz;
			end;
			Inc(j);
		end;
	end;
	// V�gigmegy az �tszakaszokon �s meg�llap�tja a param�tereket (sof�r, �tlag, ...)
	for i := 0 to UtSzakaszLista.Count - 1 do begin
		// A tankol�s alapj�n meg�llap�tja a t�ny �tlagot
		utszak		:= TUtSzakasz(UtSzakaszLista[i]);
		utkozep		:= ( utszak.utkezdokm + utszak.utvegzokm ) / 2;
		tenyatlag	:= 0;
		telepatlag	:= 0;
		j 			:= 0;
		egysegktg	:= 0;
    van_tank:= False;
		while j < TankolasLista.Count do begin
			if  ( ( TTankolas(TankolasLista[j]).tankkm1 < utkozep ) and
				  ( TTankolas(TankolasLista[j]).tankkm2 > utkozep ) ) then begin
				// Ebbe a tankol�sba esik, ez lesz az �tlaga
        van_tank:= True;
				tenyatlag	:= TTankolas(TankolasLista[j]).atlag;
				telepatlag	:= TTankolas(TankolasLista[j]).telepiatlag;
				if ( TTankolas(TankolasLista[j]).tankkm2 - TTankolas(TankolasLista[j]).tankkm1 ) <> 0  then begin
					egysegktg	:= TTankolas(TankolasLista[j]).uzktg / ( TTankolas(TankolasLista[j]).tankkm2 - TTankolas(TankolasLista[j]).tankkm1 );
				  end; // if
				j			:= TankolasLista.Count;
			  end;  // if
			Inc(j);
  		end;  // while
    if van_tank then begin
      utszak.ut_ervenyes_tank := true;
  		utszak.uttenyatlag	:= tenyatlag;
  		utszak.uttelepatlag	:= telepatlag;
  		utszak.uttenyliter 	:= (  utszak.utvegzokm - utszak.utkezdokm ) * tenyatlag  / 100;
  		utszak.uttelepliter	:= (  utszak.utvegzokm - utszak.utkezdokm ) * telepatlag / 100;
  		utszak.utkoltseg := (  utszak.utvegzokm - utszak.utkezdokm ) * egysegktg;
      end // if
    else begin
      utszak.ut_ervenyes_tank := false;
      end;

		// Az ad blue adatok kisz�m�t�sa
		tenyatlag	:= 0;
		j 			:= 0;
		egysegktg	:= 0;
    van_adblue:= false;
		while j < AdBlueLista.Count do begin
			if  ( ( TAdBlueTankolas(AdBlueLista[j]).adbluekm1 < utkozep ) and
				  ( TAdBlueTankolas(AdBlueLista[j]).adbluekm2 > utkozep ) ) then begin
				// Ebbe a tankol�sba esik, ez lesz az �tlaga
        van_adblue:= True;
				tenyatlag	:= TAdBlueTankolas(AdBlueLista[j]).adblueatl;
				if ( TAdBlueTankolas(AdBlueLista[j]).adbluekm2 - TAdBlueTankolas(AdBlueLista[j]).adbluekm1 ) <> 0  then begin
					egysegktg	:= TAdBlueTankolas(AdBlueLista[j]).adbluektg / ( TAdBlueTankolas(AdBlueLista[j]).adbluekm2 - TAdBlueTankolas(AdBlueLista[j]).adbluekm1 );
				end;
				j			:= AdBlueLista.Count;
			end;
			Inc(j);
		end;
    if van_adblue then begin
      utszak.ut_ervenyes_adblue := true;
		  utszak.blueatlag	:= tenyatlag;
  		utszak.blueliter 	:= (  utszak.utvegzokm - utszak.utkezdokm ) * tenyatlag / 100;
  		utszak.bluekoltg 	:= (  utszak.utvegzokm - utszak.utkezdokm ) * egysegktg;
      end
    else begin
      utszak.ut_ervenyes_adblue := false;
      end;
		// Az  �ns�lyok kisz�m�t�sa + a p�tkocsik �sszegy�jt�se
		onsuly			:= 0;
		potkocsik		:= '';
		j				:= 0;
		simasulyliter 	:= 0;
		apehsulyliter 	:= 0;
		while j < PotszakaszLista.Count do begin
			if  ( ( TPotszakasz(PotszakaszLista[j]).potkm1 < utkozep ) and
				  ( TPotszakasz(PotszakaszLista[j]).potkm2 > utkozep ) ) then begin
				onsuly			:= onsuly + TPotszakasz(PotszakaszLista[j]).onsuly;
				potkocsik		:= potkocsik + TPotSzakasz(PotSzakaszLista[j]).potrsz+';';
				// Az urols� p�tkocsinak a s�ly literjeit vessz�k figyelembe!!!
				simasulyliter	:= TPotSzakasz(PotSzakaszLista[j]).sulylt;
				apehsulyliter	:= TPotSzakasz(PotSzakaszLista[j]).apehlt;
			end;
			Inc(j);
		end;
		utszak.utonsuly		:= onsuly;
		utszak.utpotkocsi	:= potkocsik;
		// A raks�lyok �sszegy�jt�se
		raksuly	:= 0;
		j			:= 0;
		while j < SulySzakaszLista.Count do begin
			if  ( ( TSulySzakasz(SulySzakaszLista[j]).sulykm1 < utkozep ) and
				  ( TSulySzakasz(SulySzakaszLista[j]).sulykm2 > utkozep ) ) then begin
				raksuly	:= raksuly + TSulySzakasz(SulySzakaszLista[j]).sulyt;
			end;
			Inc(j);
		end;
		utszak.utraksuly	:= raksuly;
		// A sof�r�k �sszegy�jt�se
		soforok		:= '';
		sofordb		:= 0;
		j			:= 0;
		while j < SoforSzakaszLista.Count do begin
			if  ( ( TSoforSzakasz(SoforSzakaszLista[j]).soforkm1 < utkozep ) and
				  ( TSoforSzakasz(SoforSzakaszLista[j]).soforkm2 > utkozep ) ) then begin
				soforok	:= soforok + TSoforSzakasz(SoforSzakaszLista[j]).soforkod+';';
				Inc(sofordb);
			end;
			Inc(j);
		end;
		utszak.utsofor		:= soforok;
		utszak.utsofordb    := sofordb;
		// A szakasznorm�k meghat�roz�sa
		GetSzakaszNorma(utszak.utkezdokm, simanor, apehnor, klimaer);
		// Az �tlagok kisz�m�t�sa
		szorzo	:= 1;
		if klimaer > 0 then begin
			szorzo	:= 1 + (klimaer / 100);
		end;
		// A belf�ldi j�ratok +%-�nak sz�m�t�sa

		Query_Run(QueryDat1, 'SELECT JA_KOD FROM JARAT WHERE JA_RENDSZ = '''+rendszam+''' '+
			' AND JA_NYITKM <= '+IntToStr(utszak.utkezdokm)+
			' AND JA_ZAROKM >= '+IntToStr(utszak.utvegzokm) );
       utszak.belsoszaz	:= GetBelfoldiSzaz(QueryDat1.FieldByName('JA_KOD').AsString);
		szorzo2	:= 1;
		if utszak.belsoszaz > 0 then begin
			szorzo2	:= 1 + (utszak.belsoszaz / 100);
		end;

		utszak.simasulylt	:= ( utszak.utvegzokm - utszak.utkezdokm ) * simasulyliter * (onsuly + raksuly) * szorzo2 / 100;
		utszak.apehsulylt	:= ( utszak.utvegzokm - utszak.utkezdokm ) * apehsulyliter * (onsuly + raksuly) * szorzo / 100;
		utszak.simaatlag	:= ( utszak.utvegzokm - utszak.utkezdokm ) * szorzo2 * simanor / 100;
		utszak.apehatlag	:= ( utszak.utvegzokm - utszak.utkezdokm ) * apehnor * szorzo / 100;
	end;
end;

function  TFogyasztasDlg.GetMegtakaritas : double;
var
	i 		: integer;
	utszak	: TUtSzakasz;
	ertek, TenyFogyasztas: double;
begin
	ertek	:= 0;
  TenyFogyasztas:= FogyasztasDlg.GetTenyFogyasztas;
  if TenyFogyasztas = 0 then begin  // Ha nincs fogyaszt�s, nincs megtakar�t�s sem
      Result  := 0;
      end
  else begin
      // V�gigmegy az �tszakaszokon �s meg�llap�tja a param�tereket (sof�r, �tlag, ...)
      for i := 0 to UtSzakaszLista.Count - 1 do begin
         utszak		:= TUtSzakasz(UtSzakaszLista[i]);
         ertek		:= ertek + (utszak.uttenyliter - utszak.simasulylt - utszak.simaatlag);
         end;  // for
      Result	:= -1*ertek;
      end; // else
end;

function  TFogyasztasDlg.GetApehMegtakaritas : double;
var
	i 		: integer;
	utszak	: TUtSzakasz;
	ertek	: double;
begin
	ertek	:= 0;
	// V�gigmegy az �tszakaszokon �s �sszeadja a litereket
	for i := 0 to UtSzakaszLista.Count - 1 do begin
		utszak		:= TUtSzakasz(UtSzakaszLista[i]);
		ertek	:= ertek + (utszak.uttenyliter + utszak.uttelepliter - utszak.apehsulylt - utszak.apehatlag);
	end;
	Result	:= -1*ertek;
end;


function  TFogyasztasDlg.GetTenyFogyasztas : double;
var
	i 		: integer;
	utszak	: TUtSzakasz;
	ertek	: double;
  volt_ervenytelen: boolean;
begin
	ertek	:= 0;
  volt_ervenytelen:= false;
	// V�gigmegy az �tszakaszokon �s meg�llap�tja a param�tereket (sof�r, �tlag, ...)
	for i := 0 to UtSzakaszLista.Count - 1 do begin
		utszak	:= TUtSzakasz(UtSzakaszLista[i]);
    ertek	:= ertek + utszak.uttenyliter;
    // if not(utszak.ut_ervenyes_tank) then begin
    if not(utszak.ut_ervenyes_tank) and (utszak.utkezdokm <> utszak.utvegzokm) then begin  // 20171208: a 0 km-nyi szakaszokra nem k�vetel�nk meg fogyaszt�st
      ErvenyesTenyFogyasztas:= False;
      end;  // if
	  end;  // for
 	Result := ertek;
end;

function  TFogyasztasDlg.GetAtlag : double;
var
  TenyFogyasztas: double;
begin
   if ( zarokm-nyitokm ) <> 0 then begin
      Result := GetTenyFogyasztas * 100/(zarokm-nyitokm)
   end else begin
      Result  := 0;
   end;
end;

function TFogyasztasDlg.GetSoforLista : TStringList;
var
	SL: TStringList;
	sofszak: TSoforSzakasz;
  i: integer;
begin
  SL := TStringList.Create;
  for i := 0 to SoforSzakaszLista.Count - 1 do begin
			sofszak	:= TSoforSzakasz(SoforSzakaszLista[i]);
      SL.Add(sofszak.soforkod);
		  end;  // for
  Result:= SL;
end;

function  TFogyasztasDlg.GetApehTenyFogyasztas : double;
var
	i 		: integer;
	utszak	: TUtSzakasz;
	ertek	: double;
begin
	ertek	:= 0;
	// V�gigmegy az �tszakaszokon �s meg�llap�tja a param�tereket (sof�r, �tlag, ...)
	for i := 0 to UtSzakaszLista.Count - 1 do begin
		utszak	:= TUtSzakasz(UtSzakaszLista[i]);
		ertek	:= ertek + utszak.uttenyliter+utszak.uttelepliter;
	end;
	Result	:= ertek;
end;

function  TFogyasztasDlg.GetBlueFogyasztas : double;
var
	i 		: integer;
	utszak	: TUtSzakasz;
	ertek	: double;
  volt_ervenytelen: boolean;
begin
	ertek	:= 0;
	// V�gigmegy az �tszakaszokon �s meg�llap�tja a param�tereket (sof�r, �tlag, ...)
	for i := 0 to UtSzakaszLista.Count - 1 do begin
		utszak	:= TUtSzakasz(UtSzakaszLista[i]);
		ertek	:= ertek + utszak.blueliter;
    if not(utszak.ut_ervenyes_adblue) then begin
      ErvenyesAdBlueFogyasztas:= False;
      end;  // if
	  end;  // for
	Result	:= ertek;
end;

function  TFogyasztasDlg.GetSulyAtlag: double;
var
	i 		: integer;
   utszak	: TUtSzakasz;
   ertek	: double;
begin
	ertek	:= 0;
	// V�gigmegy az �tszakaszokon �s meg�llap�tja a param�tereket (sof�r, �tlag, ...)
   for i := 0 to UtSzakaszLista.Count - 1 do begin
       utszak	:= TUtSzakasz(UtSzakaszLista[i]);
      	ertek	:= ertek + ( utszak.utonsuly + utszak.utraksuly ) * ( utszak.utvegzokm - utszak.utkezdokm );
	end;
	if ( zarokm - nyitokm )  <> 0 then begin
   	ertek	:= ertek / ( zarokm - nyitokm );
   end;
   Result	:= ertek;
end;

function  TFogyasztasDlg.GetSoforSulyAtlag(skod : string): double;
var
	i 		: integer;
   utszak	: TUtSzakasz;
   ertek	: double;
   hossz	: double;
begin
	ertek	:= 0;
   hossz	:= 0;
	// V�gigmegy az �tszakaszokon �s meg�llap�tja a param�tereket (sof�r, �tlag, ...)
   for i := 0 to UtSzakaszLista.Count - 1 do begin
       utszak	:= TUtSzakasz(UtSzakaszLista[i]);
   	if Pos(';'+skod+';', ';'+utszak.utsofor+';') > 0 then begin
			ertek	:= ertek + ( utszak.utonsuly + utszak.utraksuly ) * ( utszak.utvegzokm - utszak.utkezdokm );
           hossz	:= hossz + ( utszak.utvegzokm - utszak.utkezdokm );
       end;
	end;
   if ( hossz )  <> 0 then begin
   	ertek	:= ertek / hossz;
   end;
   Result	:= ertek;
end;

function  TFogyasztasDlg.GetKoltseg : double;
var
	i 		: integer;
	utszak	: TUtSzakasz;
	ertek	: double;
begin
	ertek	:= 0;
	// V�gigmegy az �tszakaszokon �s meg�llap�tja a param�tereket (sof�r, �tlag, ...)
	for i := 0 to UtSzakaszLista.Count - 1 do begin
		utszak	:= TUtSzakasz(UtSzakaszLista[i]);
		ertek	:= ertek + utszak.utkoltseg;
	end;
	Result	:= ertek;
end;

function  TFogyasztasDlg.GetBlueKoltseg : double;
var
	i 		: integer;
	utszak	: TUtSzakasz;
	ertek	: double;
begin
	ertek	:= 0;
	// V�gigmegy az �tszakaszokon �s meg�llap�tja a param�tereket (sof�r, �tlag, ...)
	for i := 0 to UtSzakaszLista.Count - 1 do begin
		utszak	:= TUtSzakasz(UtSzakaszLista[i]);
		ertek	:= ertek + utszak.bluekoltg;
	end;
	Result	:= ertek;
end;

function  TFogyasztasDlg.GetTervFogyasztas : double;
var
	i 		: integer;
	utszak	: TUtSzakasz;
	ertek	: double;
begin
	ertek	:= 0;
	// V�gigmegy az �tszakaszokon �s meg�llap�tja a param�tereket (sof�r, �tlag, ...)
	for i := 0 to UtSzakaszLista.Count - 1 do begin
		utszak		:= TUtSzakasz(UtSzakaszLista[i]);
		ertek	:= ertek + (utszak.simasulylt + utszak.simaatlag);
	end;
	Result	:= ertek;
end;

function  TFogyasztasDlg.GetApehTervFogyasztas : double;
var
	i 		: integer;
	utszak	: TUtSzakasz;
	ertek	: double;
begin
	ertek	:= 0;
	// V�gigmegy az �tszakaszokon �s meg�llap�tja a param�tereket (sof�r, �tlag, ...)
	for i := 0 to UtSzakaszLista.Count - 1 do begin
		utszak		:= TUtSzakasz(UtSzakaszLista[i]);
		ertek		:= ertek + (utszak.apehsulylt + utszak.apehatlag);
	end;
	Result	:= ertek;
end;

function  TFogyasztasDlg.GetSoforMegtakaritas(skod : string) : double;
var
	i 		: integer;
	utszak	: TUtSzakasz;
	ertek	: double;
begin
	ertek	:= 0;
	// V�gigmegy az �tszakaszokon �s meg�llap�tja a param�tereket (sof�r, �tlag, ...)
	for i := 0 to UtSzakaszLista.Count - 1 do begin
		utszak		:= TUtSzakasz(UtSzakaszLista[i]);
		if Pos(';'+skod+';', ';'+utszak.utsofor+';') > 0 then begin
			if utszak.utsofordb = 0 then begin
				utszak.utsofordb := 1;
			end;
				// 2011.03.01 ut�n 60% - 60% megbont�s j�r a k�tkezes sof�rk�d�s�rt
				if utszak.utsofordb > 1 then begin
					if kezdodatum < '2011.03.01.' then begin
						ertek	:= ertek + (utszak.uttenyliter - utszak.simasulylt - utszak.simaatlag) / utszak.utsofordb;
					end else begin
						ertek	:= ertek + (utszak.uttenyliter - utszak.simasulylt - utszak.simaatlag) * 1.2 / utszak.utsofordb;
					end;
				end else begin
					ertek	:= ertek + (utszak.uttenyliter - utszak.simasulylt - utszak.simaatlag);
				end;
		end;
	end;
	Result	:= -1*ertek;
end;

function  TFogyasztasDlg.GetSoforApehMegtakaritas(skod : string) : double;
var
	i 		: integer;
	utszak	: TUtSzakasz;
	ertek	: double;
begin
	ertek	:= 0;
	// V�gigmegy az �tszakaszokon �s �sszeadja a litereket
	for i := 0 to UtSzakaszLista.Count - 1 do begin
		utszak		:= TUtSzakasz(UtSzakaszLista[i]);
		if Pos(';'+skod+';', ';'+utszak.utsofor+';') > 0 then begin
			if utszak.utsofordb = 0 then begin
				utszak.utsofordb := 1;
			end;
			ertek	:= ertek + (utszak.uttenyliter + utszak.uttelepliter - utszak.apehsulylt - utszak.apehatlag) / utszak.utsofordb;
		end;
	end;
	Result	:= -1*ertek;
end;


function  TFogyasztasDlg.GetSoforTeny(skod : string) : double;
var
	i 		: integer;
	utszak	: TUtSzakasz;
	ertek	: double;
begin
	ertek	:= 0;
	// V�gigmegy az �tszakaszokon �s meg�llap�tja a param�tereket (sof�r, �tlag, ...)
	for i := 0 to UtSzakaszLista.Count - 1 do begin
		utszak		:= TUtSzakasz(UtSzakaszLista[i]);
		if Pos(';'+skod+';', ';'+utszak.utsofor+';') > 0 then begin
			if utszak.utsofordb = 0 then begin
				utszak.utsofordb := 1;
			end;
			ertek	:= ertek + utszak.uttenyliter / utszak.utsofordb;
		end;
	end;
	Result	:= ertek;
end;

function  TFogyasztasDlg.GetSoforApehTeny(skod : string) : double;
var
	i 		: integer;
	utszak	: TUtSzakasz;
	ertek	: double;
begin
	ertek	:= 0;
	// V�gigmegy az �tszakaszokon �s meg�llap�tja a param�tereket (sof�r, �tlag, ...)
	for i := 0 to UtSzakaszLista.Count - 1 do begin
		utszak		:= TUtSzakasz(UtSzakaszLista[i]);
		if Pos(';'+skod+';', ';'+utszak.utsofor+';') > 0 then begin
			if utszak.utsofordb = 0 then begin
				utszak.utsofordb := 1;
			end;
			ertek	:= ertek + ( utszak.uttenyliter + utszak.uttelepliter ) / utszak.utsofordb;
		end;
	end;
	Result	:= ertek;
end;

function  TFogyasztasDlg.GetSoforBlueTeny(skod : string) : double;
var
	i 		: integer;
	utszak	: TUtSzakasz;
	ertek	: double;
begin
	ertek	:= 0;
	// V�gigmegy az �tszakaszokon �s meg�llap�tja a param�tereket (sof�r, �tlag, ...)
	for i := 0 to UtSzakaszLista.Count - 1 do begin
		utszak		:= TUtSzakasz(UtSzakaszLista[i]);
		if Pos(';'+skod+';', ';'+utszak.utsofor+';') > 0 then begin
			if utszak.utsofordb = 0 then begin
				utszak.utsofordb := 1;
			end;
			ertek	:= ertek + utszak.blueliter / utszak.utsofordb;
		end;
	end;
	Result	:= ertek;
end;

function  TFogyasztasDlg.GetSoforKm(skod : string) : double;
var
	i 		: integer;
	utszak	: TUtSzakasz;
	ertek	: double;
begin
	ertek	:= 0;
	// V�gigmegy az �tszakaszokon �s meg�llap�tja a param�tereket (sof�r, �tlag, ...)
	for i := 0 to UtSzakaszLista.Count - 1 do begin
		utszak		:= TUtSzakasz(UtSzakaszLista[i]);
		if Pos(';'+skod+';', ';'+utszak.utsofor+';') > 0 then begin
			ertek	:= ertek + ( utszak.utvegzokm - utszak.utkezdokm );
		end;
	end;
	Result	:= ertek;
end;

function  TFogyasztasDlg.GetSoforKoltseg(skod : string) : double;
var
	i 		: integer;
	utszak	: TUtSzakasz;
	ertek	: double;
begin
	ertek	:= 0;
	// V�gigmegy az �tszakaszokon �s meg�llap�tja a param�tereket (sof�r, �tlag, ...)
	for i := 0 to UtSzakaszLista.Count - 1 do begin
		utszak		:= TUtSzakasz(UtSzakaszLista[i]);
		if Pos(';'+skod+';', ';'+utszak.utsofor+';') > 0 then begin
			if utszak.utsofordb = 0 then begin
				utszak.utsofordb := 1;
			end;
			ertek	:= ertek + utszak.utkoltseg / utszak.utsofordb;
		end;
	end;
	Result	:= ertek;
end;

function  TFogyasztasDlg.GetSoforBlueKoltseg(skod : string) : double;
var
	i 		: integer;
	utszak	: TUtSzakasz;
	ertek	: double;
begin
	ertek	:= 0;
	// V�gigmegy az �tszakaszokon �s meg�llap�tja a param�tereket (sof�r, �tlag, ...)
	for i := 0 to UtSzakaszLista.Count - 1 do begin
		utszak		:= TUtSzakasz(UtSzakaszLista[i]);
		if Pos(';'+skod+';', ';'+utszak.utsofor+';') > 0 then begin
			if utszak.utsofordb = 0 then begin
				utszak.utsofordb := 1;
			end;
			ertek	:= ertek + utszak.bluekoltg / utszak.utsofordb;
		end;
	end;
	Result	:= ertek;
end;

function  TFogyasztasDlg.GetSoforTerv(skod : string) : double;
var
	i 		: integer;
	utszak	: TUtSzakasz;
	ertek	: double;
begin
	ertek	:= 0;
	// V�gigmegy az �tszakaszokon �s meg�llap�tja a param�tereket (sof�r, �tlag, ...)
	for i := 0 to UtSzakaszLista.Count - 1 do begin
		utszak		:= TUtSzakasz(UtSzakaszLista[i]);
		if Pos(';'+skod+';', ';'+utszak.utsofor+';') > 0 then begin
			if utszak.utsofordb = 0 then begin
				utszak.utsofordb := 1;
			end;
			ertek	:= ertek + (utszak.simasulylt + utszak.simaatlag) / utszak.utsofordb;
		end;
	end;
	Result	:= ertek;
end;

function  TFogyasztasDlg.GetSoforApehTerv(skod : string) : double;
var
	i 		: integer;
	utszak	: TUtSzakasz;
	ertek	: double;
begin
	ertek	:= 0;
	// V�gigmegy az �tszakaszokon �s meg�llap�tja a param�tereket (sof�r, �tlag, ...)
	for i := 0 to UtSzakaszLista.Count - 1 do begin
		utszak		:= TUtSzakasz(UtSzakaszLista[i]);
		if Pos(';'+skod+';', ';'+utszak.utsofor+';') > 0 then begin
			if utszak.utsofordb = 0 then begin
				utszak.utsofordb := 1;
			end;
			ertek	:= ertek + (utszak.apehsulylt + utszak.apehatlag) / utszak.utsofordb;
		end;
	end;
	Result	:= ertek;
end;

procedure TFogyasztasDlg.ClearAllList;
begin
	SoforSzakaszLista.Clear;
	PotSzakaszLista.Clear;
	TankolasLista.Clear;
	NormaLista.Clear;
	AdBlueLista.Clear;
	SulySzakaszlista.Clear;
	UtszakaszLista.Clear;
  ErvenyesTenyFogyasztas:=True;
end;

procedure TFogyasztasDlg.GeneralBetoltes(rensz : string; km1, km2 : integer);
var
	jakod		: string;
   kdat		: string;
   vdat		: string;
begin
	jaratszam	:= '';
	ki_jaratszam:= '';
	rendszam	:= rensz;
	kezdokm		:= km1;
	vegzokm		:= km2;
  nyitokm		:= km1;
  zarokm		:= km2;
	// Megkeress�k a km-ekhez tartoz� j�ratokat �s azokra megh�vjuk a m�veleteket
	Query_Run(QueryDat1, 'SELECT JA_NYITKM FROM JARAT WHERE JA_RENDSZ = '''+rensz+''' '+' AND JA_NYITKM <= '+IntToStr(kezdokm)+' ORDER BY JA_NYITKM ');
	QueryDat1.Last;
	if QueryDat1.RecordCount < 1 then begin
		// Ha nincs el�z� �v, akkor megint a mostaniban keres, ami m�r az el�bb sem volt!!!!
		// Query_Run(QueryDat1, 'SELECT  JA_NYITKM FROM '+elozoschema+'.JARAT WHERE JA_RENDSZ = '''+rensz+''' '+
    Query_Run(QueryDat1, 'SELECT  JA_NYITKM FROM JARAT WHERE JA_RENDSZ = '''+rensz+''' '+
			' AND JA_NYITKM <= '+IntToStr(kezdokm)+' ORDER BY JA_NYITKM ');
		QueryDat1.Last;
		if QueryDat1.RecordCount < 1 then begin
			// Az el�z� �vben sem tal�lt adatokat
			Exit;
		end;
	end;
  kdat	:= '';
  vdat	:= '';
	Query_Run(QueryDat2, 'SELECT JA_NYITKM, JA_ZAROKM, JA_JKEZD, JA_JVEGE, JA_KOD FROM JARAT WHERE JA_RENDSZ = '''+rensz+''' '+
		' AND JA_NYITKM >= '+QueryDat1.FieldByName('JA_NYITKM').AsString+
   // NP 2015-04-13 - A j�ratokn�l az egyik nyit�ja a m�sik z�r�ja, �gy ide vett�nk olyan j�ratot is, amely az id�szak utols� km-�vel
   // kezd�d�tt (hiszen az el�z� j�rat v�ge �ta nem mozdult az aut�)
   // Most ezt az esetet kihagyjuk a feldolgoz�sb�l.
	 // 	' AND JA_NYITKM <= '+IntToStr(vegzokm) +
   	' AND JA_NYITKM < '+IntToStr(vegzokm) +
		' ORDER BY JA_NYITKM ');
	while not QueryDat2.Eof do begin
		jakod			:= QueryDat2.FieldByName('JA_KOD').AsString;
		JaratBetoltesAlap(jakod);
    if kdat	= '' then begin
       	kdat	:= kezdodatum;
    end;
    vdat	:= vegzodatum;
		AddNorma(QueryDat2.FieldByName('JA_JKEZD').AsString, StrToIntDef(QueryDat2.FieldByName('JA_NYITKM').AsString,0));
		AddNorma(QueryDat2.FieldByName('JA_JVEGE').AsString, StrToIntDef(QueryDat2.FieldByName('JA_ZAROKM').AsString,0));
		AddSoforJarat(jakod);
		AddPotJarat(jakod);
		AddSulyJarat(jakod);
		QueryDat2.Next;
	end;
  kezdodatum	:= kdat;
  vegzodatum	:= vdat;
end;

procedure TFogyasztasDlg.ButtonPrintClick(Sender: TObject);
var
	filename	: string;
begin
	// Kinyomtatjuk a TXT-t
	filename	:= EgyebDlg.GetTempFileName;
	Memo1.Lines.SaveToFile(filename);
	EgyebDlg.FileMutato( filename,	'',	'',  '', '', '', 0);
end;

procedure TFogyasztasDlg.BitKilepClick(Sender: TObject);
begin
	Close;
end;

procedure TFogyasztasDlg.BitElkuldClick(Sender: TObject);
begin
	Memo1.SelectAll;
	Memo1.CopyToClipboard;
end;

function  TFogyasztasDlg.GetOsszKm : double;
begin
	Result	:= zarokm - nyitokm;
end;

procedure TFogyasztasDlg.M1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
	if Key = VK_RETURN then begin
		BitBtn2Click(Sender);
	end;
end;

procedure TFogyasztasDlg.RadioButton1Click(Sender: TObject);
begin
	Memo1.Clear;
	M1.Text	:= '';
	M2.Text	:= '';
	M3.Text	:= '';
	M4.Text	:= '';
	if RadioButton1.Checked then begin
		M1.Enabled			:= true;
		BitBtn5.Enabled		:= true;
		M2.Enabled			:= false;
		M3.Enabled			:= false;
		M4.Enabled			:= false;
		BitBtn1.Enabled		:= false;
		try
//			M1.SetFocus;
		except
		end;
	end else begin
		M1.Enabled			:= false;
		BitBtn5.Enabled		:= false;
		M2.Enabled			:= true;
		M3.Enabled			:= true;
		M4.Enabled			:= true;
		BitBtn1.Enabled		:= true;
		try
			M2.SetFocus;
		except
		end;
	end;
end;

procedure TFogyasztasDlg.BitBtn2Click(Sender: TObject);
begin
	BitBtn2.Hide;
   Screen.Cursor	:= crHourGlass;
	Memo1.Clear;
	// V�grehajtjuk a keres�st
	if RadioButton1.Checked then begin
		{Megkeress�k a megadott sz�m� j�ratot}
		if M1.Text <> '' then begin
           if QueryJarat.Locate('JA_KOD', Format('%6.6d',[StrToIntDef(M0.Text,0)]) , []) then begin
  				M1.Text 	:= '';
               M1.Text 	:= QueryJarat.FieldByname('JA_ORSZ').AsString + '-' + QueryJarat.FieldByname('JA_ALKOD').AsString;
               ClearAllList;
               JaratBetoltes(QueryJarat.FieldByname('JA_KOD').AsString);
               CreateTankLista;
               CreateAdBlueLista;
               Szakaszolo;
               TeljesLista;
			end;
		end;
	end else begin
		// Rendsz�m alapj�n dolgozunk
		if M2.Text <> '' then begin
			Query_Run(QueryKoz1, 'SELECT * FROM GEPKOCSI WHERE GK_RESZ = '''+M2.Text+''' ');
			if QueryKoz1.RecordCount > 0 then begin
				if StrToIntDef(M3.Text, 0) = 0 then begin
					// Megkeress�k az els� tele tankot a g�pkocsihoz
					Query_Run(QueryDat1, 'SELECT MIN(KS_KMORA) MINORA FROM KOLTSEG WHERE KS_RENDSZ = '''+M2.Text+''' '+
						' AND KS_TELE = 1 AND KS_TIPUS = 0 ');
					M3.Text	:= QueryDat1.FieldByName('MINORA').AsString;
				end;
				if StrToIntDef(M4.Text, 0) = 0 then begin
					// Megkeress�k az els� tele tankot a g�pkocsihoz
					Query_Run(QueryDat1, 'SELECT MAX(KS_KMORA) MAXORA FROM KOLTSEG WHERE KS_RENDSZ = '''+M2.Text+''' '+
						' AND KS_TELE = 1 AND KS_TIPUS = 0 ');
					M4.Text	:= QueryDat1.FieldByName('MAXORA').AsString;
				end;
				if ( ( StrToIntDef(M3.Text, 0) <> 0 ) and ( StrToIntDef(M4.Text, 0) <> 0  ) ) then begin
					// Mehet a sz�m�t�s
					ClearAllList;
					GeneralBetoltes(M2.Text, StrToIntDef(M3.Text, 0), StrToIntDef(M4.Text, 0));
					CreateTankLista;
					CreateAdBlueLista;
					Szakaszolo;
					TeljesLista;
				end;
			end;
		end;
	end;
   Screen.Cursor	:= crDefault;
   BitBtn2.Show;
end;

procedure TFogyasztasDlg.BitBtn1Click(Sender: TObject);
begin
	GepkocsiValaszto(M2, GK_TRAKTOR);
end;

function  TFogyasztasDlg.GetBlueAtlag(km : integer) : double;
var
	i 		: integer;
	utszak	: TUtSzakasz;
begin
	Result	:= 0;
	// V�gigmegy�nk az �tszakaszokon �s megkeress�k, hogy az adott km-n�l mennyi volt az �tlag
	i 	:= 0;
	while i <= ( UtSzakaszLista.Count - 1 ) do begin
		utszak		:= TUtSzakasz(UtSzakaszLista[i]);
		if ( (utszak.utkezdokm <= km) and (km <= utszak.utvegzokm) ) then begin
			Result	:= utszak.blueatlag;
			i		:= UtSzakaszLista.Count;
		end;
		Inc(i);
	end;
end;

procedure TFogyasztasDlg.AddNorma(dat : string; km : integer);
var
	NormaSzakasz 	: TNormaSzakasz;
	i               : integer;
	vannorma		: boolean;
	telkezd			: string;
	telvege			: string;
	klimake  		: string;
	klimave			: string;
	apehn			: double;
	siman			: double;
	klimasz			: double;
begin
	// L�trehozunk egy �jabb norma-pontot, ha az sz�ks�ges
	vannorma	:= false;
	for i := 0 to NormaLista.Count - 1 do begin
		if ( ( TNormaSzakasz(NormaLista[i]).normakm = km ) and (TNormaSzakasz(NormaLista[i]).normadatum = dat) ) then begin
			vannorma	:= true;
		end;
	end;
	if vannorma then begin
		Exit;
	end;
	if ( ( dat = '' ) or (km <= 0) ) then begin
		Exit;
	end;
	if QueryGepkocsi.Locate('GK_RESZ', rendszam, []) then begin
		telkezd			:= EgyebDlg.Read_SZGrid( '999', '642' );
		telvege			:= EgyebDlg.Read_SZGrid( '999', '643' );
		if ( ( copy(dat ,6,5) > telvege ) and ( copy(dat ,6,5) < telkezd ) ) then begin
			{Ny�r van}
			apehn		:= StringSzam(QueryGepkocsi.FieldByName('GK_APENY').AsString);
			siman		:= StringSzam(QueryGepkocsi.FieldByName('GK_NORNY').AsString);
		end else begin
			{T�l van}
			apehn		:= StringSzam(QueryGepkocsi.FieldByName('GK_APETE').AsString);
			siman		:= StringSzam(QueryGepkocsi.FieldByName('GK_NORMA').AsString);
		end;
		// A klima meghat�roz�sa
		klimasz			:= 0;
		if StrToIntDef(QueryGepkocsi.FieldByName('GK_KLIMA').AsString,0) > 0 then begin
			klimake	:= EgyebDlg.Read_SZGrid( '999', '646' );
			klimave	:= EgyebDlg.Read_SZGrid( '999', '647' );
			if ( ( copy(dat,6,5) >= klimake ) and ( copy(dat,6,5) <= klimave ) ) then begin
				klimasz	:= StringSzam(EgyebDlg.Read_SZGrid( '999', '644' ));
			end;
		end;
		// A norma l�trehoz�sa �s csatol�sa
		NormaSzakasz 	:= TNormaSzakasz.Create(dat, km, siman, apehn, klimasz);
		NormaLista.Add(NormaSzakasz);
	end;
end;

procedure TFogyasztasDlg.GetSzakaszNorma(km : integer; var sinor, apnor, klsz : double);
var
	i 		: integer;
	maxkm   : integer;
begin
	sinor	:= 0;
	apnor	:= 0;
	klsz	:= 0;
	maxkm	:= 0;
	for i := 0 to NormaLista.Count - 1 do begin
		if ( ( TNormaSzakasz(NormaLista[i]).normakm <= km ) and (maxkm <= km) ) then begin
			maxkm	:= TNormaSzakasz(NormaLista[i]).normakm;
			sinor	:= TNormaSzakasz(NormaLista[i]).simanorma;
			apnor	:= TNormaSzakasz(NormaLista[i]).apehnorma;
			klsz	:= TNormaSzakasz(NormaLista[i]).klimaszaz;
		end;
	end;
end;

function TFogyasztasDlg.GetBelfoldiSzaz(jakod : string) : double;
begin
	// A t�pus alapj�n a belf�ldis�g eld�nt�se
	Result	:= 0;
   if not QueryJarat.Locate('JA_KOD', jakod, []) then begin
       Exit;
   end;
	if StrToIntDef(QueryJarat.FieldByName('JA_EUROS').AsString,0) = 2 then begin
		// Belf�ldi a j�rat
		Result	:= StringSzam(EgyebDlg.Read_SZGrid( '999', '746' ));
	end;
end;

procedure TFogyasztasDlg.JaratBetoltesAlap(jakod : string);
begin
	// A j�rat adatainak bet�lt�se
   if not QueryJarat.Locate('JA_KOD', jakod, []) then begin
		Exit;
	end;
	jaratszam		:= GetJaratszamFromDb(QueryJarat);
	rendszam		:= trim(QueryJarat.FieldByName('JA_RENDSZ').AsString);
	kezdokm			:= StrToIntDef(QueryJarat.FieldByName('JA_NYITKM').AsString,0);
	vegzokm			:= StrToIntDef(QueryJarat.FieldByName('JA_ZAROKM').AsString,0);
	kezdodatum		:= QueryJarat.FieldByName('JA_JKEZD').AsString;
	vegzodatum		:= QueryJarat.FieldByName('JA_JVEGE').AsString;
end;

end.
