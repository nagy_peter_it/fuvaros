unit Felhabe;

interface

uses
	SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
	Forms, Dialogs, StdCtrls, Buttons, Mask, DB, DBTables, ADODB, J_ALFORM;

type
	TFelhabeDlg = class(TJ_AlformDlg)
	BitElkuld: TBitBtn;
	BitKilep: TBitBtn;
	Label2: TLabel;
	Label3: TLabel;
	MaskEdit1: TMaskEdit;
	MaskEdit2: TMaskEdit;
    CheckBox1: TCheckBox;
    Label1: TLabel;
    MaskEdit3: TMaskEdit;
    CheckBox2: TCheckBox;
    Label4: TLabel;
    MaskEdit4: TMaskEdit;
    Query1: TADOQuery;
    BitBtn11: TBitBtn;
    MaskEdit5: TMaskEdit;
    Query2: TADOQuery;
    meDOKOD: TMaskEdit;
    Label5: TLabel;
	procedure FormCreate(Sender: TObject);
	procedure Tolto(cim : string; kod : string);override;
	procedure BitElkuldClick(Sender: TObject);
	procedure BitKilepClick(Sender: TObject);
	procedure FormShow(Sender: TObject);
	procedure Kiiro;
    procedure BitBtn11Click(Sender: TObject);
	private
	public
	end;

var
	FelhabeDlg: TFelhabeDlg;

implementation

uses
	J_SQL, Egyeb, Kozos,J_Valaszto;
{$R *.DFM}

procedure TFelhabeDlg.FormCreate(Sender: TObject);
begin
	EgyebDlg.SeTADOQueryDatabase(Query1);
	EgyebDlg.SeTADOQueryDatabase(Query2);
	MaskEdit1.Text 	:= '';
	MaskEdit2.Text 	:= '';
	MaskEdit3.Text 	:= '';
	MaskEdit4.Text 	:= '';
  meDOKOD.Text 	  := '';
end;

procedure TFelhabeDlg.Tolto(cim : string; kod : string);
begin
	Caption 					:= cim;
	ret_kod					:= kod;
	MaskEdit1.Text 		:= kod;
	if kod <> '' then begin
		if Query_Run(Query1,'SELECT * from JELSZO where JE_FEKOD = '''+kod+'''') then begin
			MaskEdit2.Text 	:= Query1.FieldByName('JE_FENEV').AsString;
			MaskEdit3.Text 	:= Query1.FieldByName('JE_JESZO').AsString;
			MaskEdit4.Text 	:= Query1.FieldByName('JE_JESZO').AsString;
      meDOKOD.Text    := IntToStr(Query1.FieldByName('JE_DOKOD').AsInteger);
      CheckBox1.Checked := Query1.FieldByName('JE_VALID').AsInteger > 0;
    	CheckBox2.Checked := Query1.FieldByName('JE_SUPER').AsInteger > 0;
			Query1.Close;
		end;
		SetMaskEdits([MaskEdit1]);
		Label1.Hide;
		MaskEdit3.Hide;
		Label4.Hide;
		MaskEdit4.Hide;
  	end;
end;

procedure TFelhabeDlg.BitElkuldClick(Sender: TObject);
begin
	if MaskEdit1.Text = '' then begin
		 // Query_Run(Query2,'SELECT max(JE_FEKOD) as KOD from JELSZO' );
     Query_Run(Query2,'SELECT max(convert(int,JE_FEKOD)) as KOD from JELSZO' );
		 MaskEdit1.Text 	:=IntToStr( Query2.FieldByName('KOD').AsInteger + 1);

		//NoticeKi('A k�d nem lehet �res!', NOT_ERROR);
		//MaskEdit1.SetFocus;
    // Exit;
  end;

	if MaskEdit3.Text = '' then begin
		NoticeKi('A jelsz� nem lehet �res!', NOT_ERROR);
		MaskEdit3.SetFocus;
     Exit;
  end;

  if not JoEgesz(meDOKOD.Text) then begin
		NoticeKi('A dolgoz� k�d csak sz�m lehet!', NOT_ERROR);
		meDOKOD.SetFocus;
    Exit;
  end;

	if MaskEdit3.Text <> MaskEdit4.Text then begin
		NoticeKi('A jelsz�nak �s az ism�tl�s�nek meg kell egyeznie!', NOT_ERROR);
     MaskEdit3.Text 	:= '';
     MaskEdit4.Text 	:= '';
		MaskEdit3.SetFocus;
     Exit;
  end;

		if ret_kod = '' then begin
				{�j felhaszn�l� felvitele}
  			if not Query_Run(Query1,'SELECT * from JELSZO where JE_FEKOD = '''+UpperCase(MaskEdit1.Text)+'''') then begin
           	Exit;
           end;
           if Query1.RecordCount > 0 then begin
					NoticeKi('Ez a k�d m�r l�tezik!', NOT_ERROR);
					MaskEdit1.Text := '';
					MaskEdit1.SetFocus;
				end else begin
  				if Query_Run(Query1,'INSERT INTO JELSZO (JE_FEKOD, JE_JESZO, JE_KRSMD) VALUES ('''+
              UpperCase(MaskEdit1.Text)+''', '''+MaskEdit3.Text+''', 1) ') then begin
                ret_kod		:= UpperCase(MaskEdit1.Text);
              	Kiiro;
                Query_Run(Query1, 'insert into JOGOS (JO_DIKOD, JO_FEKOD, JO_JOGOS, JO_SUPER) '+
                    '	select DL_DLKOD, '+ret_kod+', 1, 0	from DIALOGS where DL_DLKOD not in(500, 504)', FALSE);
              end;
				end;
		end else begin
				{Alk�d m�dos�t�sa}
  			if Query_Run(Query1,'SELECT * from JELSZO where JE_FEKOD = '''+UpperCase(MaskEdit1.Text)+'''') then begin
              Kiiro;
				end;
		end;
end;

procedure TFelhabeDlg.BitKilepClick(Sender: TObject);
begin
	ret_kod			:= '';
	Close;
end;

procedure TFelhabeDlg.FormShow(Sender: TObject);
begin
	if ret_kod <> '' then begin
		MaskEdit1.Enabled := false;
		MaskEdit2.SetFocus;
	end;
end;

procedure TFelhabeDlg.Kiiro;
var
	str1, str2	: string;
begin
  	str1 	:= '0';
  	str2	:= '0';
  	if CheckBox1.Checked then begin
  		str1 	:= '1';
  	end;
  	if CheckBox2.Checked then begin
  		str2 	:= '1';
  	end;
	Query_Run(Query1,'UPDATE JELSZO SET '+
  		'JE_FENEV = '''+ MaskEdit2.Text + ''', '+
      'JE_DOKOD = '+ meDOKOD.Text + ', '+
  		'JE_VALID = '+ str1 + ', '+
  		'JE_SUPER = '+ str2 + ' ' +
     	' WHERE JE_FEKOD = '''+UpperCase(MaskEdit1.Text)+''' ');
	Close;
end;



procedure TFelhabeDlg.BitBtn11Click(Sender: TObject);
begin
	DolgozoValaszto(MaskEdit5, MaskEdit2);
end;

end.


