unit Jelszo;

interface

uses WinTypes, WinProcs, Classes, Graphics, Forms, Controls, StdCtrls,
	Buttons, DB, DBTables, Dialogs, ADODB, Vcl.ExtCtrls, SysUtils;

type
	TJelszoDlg = class(TForm)
	Label1: TLabel;
	Password: TEdit;
	OKBtn: TBitBtn;
	CancelBtn: TBitBtn;
    Query1: TADOQuery;
    pnlMessage: TPanel;
	procedure OKBtnClick(Sender: TObject);
	procedure CancelBtnClick(Sender: TObject);
	procedure FormCreate(Sender: TObject);
   procedure FormDestroy(Sender: TObject);
    procedure FormShow(Sender: TObject);
  	private
  		probaszam	: integer;
	end;

var
	JelszoDlg: TJelszoDlg;

implementation

uses
  Egyeb, J_SQL, Kozos;

{$R *.DFM}

procedure TJelszoDlg.OKBtnClick(Sender: TObject);
begin
	// A joker jelsz� ellenorz�se
 	if ( ( copy(Password.Text,1,3) = 'joe' ) and  (copy(Password.Text,4,15) = EgyebDlg.MaiDatum ) ) then begin
     EgyebDlg.user_name 	:= 'Supervisor';
     EgyebDlg.user_code 	:= '00';
     EgyebDlg.user_super 	:= true;
     Close;
    	Exit;
 	end;

	if not Query_Run(Query1,'SELECT * FROM JELSZO WHERE je_jeszo = ''' + Password.Text + '''') then begin
 		Exit;
 	end;

	// A jelsz� helyess�g�nek lek�rdez�se
	if Query1.RecordCount > 0 then begin
		if Query1.FieldByName('JE_VALID').AsInteger > 0 then begin
      EgyebDlg.user_name 	:= Query1.FieldByName('JE_FENEV').AsString;
			EgyebDlg.user_code  := Query1.FieldByName('JE_FEKOD').AsString;
      EgyebDlg.user_dokod 	:= Query1.FieldByName('JE_DOKOD').AsString;
      if EgyebDlg.user_dokod = '' then EgyebDlg.user_dokod:= '0';
			EgyebDlg.user_super 	:= Query1.FieldByName('JE_SUPER').AsInteger > 0;
      EgyebDlg.ALAPERTELMEZETT_KERESESI_MOD:= Query1.FieldByName('JE_KRSMD').AsInteger;
			Close;
		end else begin
			probaszam := probaszam +1;
        // NoticeKi('�rv�nytelen jelsz�!', NOT_ERROR);    // a "NoticeKi" ablaka a jelsz� ablak _al�_ ny�lik ki... (mindkett� mod�lis)
      pnlMessage.Caption:='�rv�nytelen jelsz�!';
      if probaszam>=2 then pnlMessage.Caption:=pnlMessage.Caption+' ('+ IntToStr(probaszam)+')';
			Password.Text := '';
			Password.Setfocus;
			if probaszam = 3 then begin
				EgyebDlg.user_code := '';
				Close;
        end;
		end;
	end else begin
		probaszam := probaszam +1;
    // NoticeKi('Az �n �ltal beg�pelt jelsz� nem l�tezik.', NOT_ERROR);   // a "NoticeKi" ablaka a jelsz� ablak _al�_ ny�lik ki... (mindkett� mod�lis)
    pnlMessage.Caption:='Az �n �ltal beg�pelt jelsz� nem l�tezik!';
    if probaszam>=2 then pnlMessage.Caption:=pnlMessage.Caption+' ('+ IntToStr(probaszam)+')';
		Password.Text := '';
		Password.Setfocus;
		if probaszam = 3 then begin
			EgyebDlg.user_code := '';
			Close;
     end;
	end;
end;

procedure TJelszoDlg.CancelBtnClick(Sender: TObject);
begin
	EgyebDlg.user_code := '';
  	Close;
end;

procedure TJelszoDlg.FormCreate(Sender: TObject);
begin
	probaszam 		:= 0;
	Password.Text 	:= '';
  	EgyebDlg.SeTADOQueryDatabase(Query1);
end;

procedure TJelszoDlg.FormDestroy(Sender: TObject);
begin
	Query1.Close;
end;

procedure TJelszoDlg.FormShow(Sender: TObject);
begin
    SetWindowPos(
    Self.Handle,
    HWND_TOPMOST,
    0,
    0,
    0,
    0,
    SWP_NOACTIVATE or SWP_NOMOVE or SWP_NOSIZE);

end;

end.

