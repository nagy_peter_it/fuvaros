unit MegbizasUjFm;

interface

uses
	SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
	StdCtrls, Forms, DBCtrls, DB, DBGrids, DBTables, Grids, ExtCtrls, Buttons,
   ShellApi, Dialogs, J_FOFORMUJ, ADODB, Mask, Menus, Variants, ImgList;

type
	TMegbizasUjFmDlg = class(TJ_FOFORMUJDLG)
    BitBtn1: TBitBtn;
	 BitBtn2: TBitBtn;
    jaratkod: TMaskEdit;
    jaratszam: TMaskEdit;
    BitBtn3: TBitBtn;
    M2: TMaskEdit;
    BitBtn5: TBitBtn;
    BitBtn6: TBitBtn;
    BitBtn7: TBitBtn;
    BitBtn8: TBitBtn;
    PopupMenu1: TPopupMenu;
	 DatumAtiras: TMenuItem;
    BitBtn9: TBitBtn;
    ColorDialog2: TColorDialog;
    Szinbeallitas: TMenuItem;
    Timer1: TTimer;
    QueryUj3: TADOQuery;
    M3: TMaskEdit;
    BitBtn4: TBitBtn;
	 BitBtn10: TBitBtn;
    ImageList1: TImageList;
    BitBtn11: TBitBtn;
    BitBtn12: TBitBtn;
	  procedure FormCreate(Sender: TObject);override;
	 procedure Adatlap(cim, kod : string);override;
	 procedure ButtonKuldClick(Sender: TObject);override;
	 procedure DataSource1DataChange(Sender: TObject; Field: TField);override;
	 procedure Mezoiro;override;
	 procedure BitBtn1Click(Sender: TObject);
	 procedure BitBtn2Click(Sender: TObject);
	 procedure ButtonTorClick(Sender: TObject); override;
	 procedure FormResize(Sender: TObject);override;
	 procedure BitBtn5Click(Sender: TObject);
	 procedure DBGrid2DrawColumnCell(Sender: TObject; const Rect: TRect;
	   DataCol: Integer; Column: TColumn; State: TGridDrawState);override;
	 procedure BitBtn6Click(Sender: TObject);
	 procedure BitBtn7Click(Sender: TObject);
	 procedure BitBtn8Click(Sender: TObject);
	 procedure DatumAtirasClick(Sender: TObject);
	 procedure BitBtn9Click(Sender: TObject);
	 procedure SzinbeallitasClick(Sender: TObject);
	 procedure Timer1Timer(Sender: TObject);
	 procedure StatuszBeiras(mbkod : string);
	 procedure StatuszTorles;
	 procedure SummaTolto;
	 procedure BitBtn4Click(Sender: TObject);
	 procedure BitBtn10Click(Sender: TObject);
	 procedure BitBtn11Click(Sender: TObject);
	 procedure BitBtn12Click(Sender: TObject);
	 function  KeresChange(mezo, szoveg : string) : string; override;
	 private
		statusszin	: array [1..10, 1..2] of string;
		checksumma	: integer;
		elotag		: string;
	 public
		ret_mskod	: string;
	end;

var
	MegbizasUjFmDlg : TMegbizasUjFmDlg;

implementation

uses
	Egyeb, J_SQL, MegbizasBe, J_VALASZTO, Viszonybe, Kozos, Segedbe2, Idobe,
	GepStatFm, Jaratbe, RendVal, CsatLIstFm;

procedure TMegbizasUjFmDlg.FormCreate(Sender: TObject);
var
	i : integer;
begin
	ret_mskod	:= '';
	Inherited FormCreate(Sender);
	EgyebDlg.SeTADOQueryDatabase(QueryUj3);
	kellujures	:= false;

	AddSzuromezoRovid('MS_TIPUS',  'MEGSEGED');
	AddSzuromezoRovid('MS_TINEV',  'MEGSEGED');
	AddSzuromezoRovid('MS_STATN',  'MEGSEGED');
	AddSzuromezoRovid('MS_EXSTR',  'MEGSEGED');
	AddSzuromezoRovid('MS_FORSZ',  'MEGSEGED');
	AddSzuromezoRovid('MS_ORSZA',  'MEGSEGED');
	AddSzuromezoRovid('MS_FAJTA',  'MEGSEGED');
	AddSzuromezoRovid('MS_RENSZ',  'MEGSEGED');
	AddSzuromezoRovid('MS_POTSZ',  'MEGSEGED');
	AddSzuromezoRovid('MS_GKKAT',  'MEGSEGED');
	AddSzuromezoRovid('MS_FELNEV', 'MEGSEGED');
	AddSzuromezoRovid('MS_FELTEL', 'MEGSEGED');
	AddSzuromezoRovid('MS_LERNEV', 'MEGSEGED');
	AddSzuromezoRovid('MS_LERTEL', 'MEGSEGED');
	AddSzuromezoRovid('MS_KIFON',  'MEGSEGED' );

	AddSzuromezoRovid('MB_VEKOD',  'MEGBIZAS');
	AddSzuromezoRovid('MB_VENEV',  'MEGBIZAS');
	AddSzuromezoRovid('MB_RENSZ',  'MEGBIZAS');
	AddSzuromezoRovid('MB_EXSTR',  'MEGBIZAS');
	AddSzuromezoRovid('MB_EXPOR',  'MEGBIZAS');
	AddSzuromezoRovid('MB_GKKAT',  'MEGBIZAS');
	AddSzuromezoRovid('MB_POTSZ',  'MEGBIZAS');
	AddSzuromezoRovid('MB_ALNEV',  'MEGBIZAS');

	// Bel�p�skor t�r�lj�k ki a keres�s tartalm�t
//	SzuresUrites;

	M2.Text		:= EgyebDlg.MaiDatum;
	M3.Text		:= EgyebDlg.MaiDatum;
	alapstr		:= 'SELECT * FROM MEGSEGED, MEGBIZAS WHERE MS_MBKOD = MB_MBKOD AND MS_DATUM = '''+M2.Text+''' ';
	FelTolto('�j megb�z�s felvitele ', 'Megb�z�s adatok m�dos�t�sa ',
			'Megb�z�sok list�ja', 'MB_MBKOD', alapstr, 'MEGBIZAS', QUERY_ADAT_TAG );
	kulcsmezo			:= 'MS_MSKOD';
	width				:= DLG_WIDTH;
	height				:= DLG_HEIGHT;
	BitBtn1.Parent		:= PanelBottom;
	BitBtn2.Parent		:= PanelBottom;
	BitBtn3.Parent		:= PanelBottom;
	M2.Parent			:= PanelBottom;
	M3.Parent			:= PanelBottom;
	BitBtn5.Parent		:= PanelBottom;
	BitBtn4.Parent		:= PanelBottom;
	BitBtn6.Parent		:= PanelBottom;
	BitBtn7.Parent		:= PanelBottom;
	BitBtn8.Parent		:= PanelBottom;
	BitBtn9.Parent		:= PanelBottom;
	BitBtn10.Parent		:= PanelBottom;
	BitBtn11.Parent		:= PanelBottom;
	BitBtn12.Parent		:= PanelBottom;
	DBGrid2.PopupMenu	:= PopupMenu1;
	// A st�tussz�nek felt�lt�se
	for i := 1 to 10 do begin
		statusszin[i, 1] := '';
		statusszin[i, 2] := IntToStr(clAqua);
	end;
	Query_Run(EgyebDlg.QueryKozos, 'SELECT SZ_ALKOD, SZ_KODHO FROM SZOTAR WHERE SZ_FOKOD = ''960'' ' );
	i := 1;
	while ( ( i <= 10 ) and ( not EgyebDlg.QueryKozos.Eof ) ) do begin
		statusszin[i, 1] := EgyebDlg.QueryKozos.FieldByName('SZ_ALKOD').AsString;
		statusszin[i, 2] := EgyebDlg.QueryKozos.FieldByName('SZ_KODHO').AsString;
		Inc(i);
		EgyebDlg.QueryKozos.Next;
	end;
	SummaTolto;
	uresszures	:= false;
	elotag	   	:= EgyebDlg.Read_SZGrid('CEG', '310');
end;

procedure TMegbizasUjFmDlg.ButtonKuldClick(Sender: TObject);
begin
	Inherited ButtonKuldClick(Sender);
   ret_mskod	:= Query1.FieldByName('MS_MSKOD').AsString;
end;

procedure TMegbizasUjFmDlg.Adatlap(cim, kod : string);
var
	oldmezo1	: string;
begin
	if kod <> '' then begin
		if Query1.FieldByName('MB_AFKOD').AsString <> '' then begin
			// Ellen�rizz�k, hogy ki dolgozik m�g ezen a megb�z�son
			if Query1.FieldByName('MB_AFKOD').AsString <> EgyebDlg.user_code then begin
				NoticeKi('A megb�z�st �ppen m�s m�dos�tja!');
				Exit;
			end;
		end;
	end;
	oldmezo1	:= Query1.FieldByName('MS_MSKOD').AsString;
	StatuszBeiras(kod);
	Application.CreateForm(TMegbizasbeDlg, AlForm);
	Inherited Adatlap(cim, kod );
	StatuszTorles;
	if Alform.ret_kod = '' then begin
   	Exit;
   end;
	SummaTolto;
	if kod <> '' then begin
		ModLocate (Query1, 'MS_MSKOD', oldmezo1 );
	end;
	GepStatTolto(Query1.FieldByName('MS_RENSZ').AsString);
	PotStatTolto(Query1.FieldByName('MS_POTSZ').AsString);
end;

procedure TMegbizasUjFmDlg.Mezoiro;
begin
	Query1.FieldByname('STATUSZ').AsString 	:= EgyebDlg.Read_SZGrid('950', Query1.FieldByName('MB_STATU').AsString);
end;

procedure TMegbizasUjFmDlg.DataSource1DataChange(Sender: TObject; Field: TField);
begin
	// A gombok megjelen�t�s�nek vez�rl�se
	oldrecord	:= Query1.FieldByName('MS_MSKOD').AsString;
	DBGrid2.Refresh;
	if Query1.FieldByName('MB_JAKOD').AsString = '' then begin
		BitBtn1.Show;
		BitBtn2.Hide;
		BitBtn8.Hide;
	end else begin
		BitBtn1.Hide;
		BitBtn2.Show;
		BitBtn8.Show;
	end;
end;

procedure TMegbizasUjFmDlg.BitBtn1Click(Sender: TObject);
var
	szurosor	: integer;
	sorsz		: integer;
begin
	if not voltalt then begin
		voltalt  := true;
		Exit;
	end;
	// Hozz�rendelj�k a rekordot a j�rathoz
	if Query1.FieldByName('MB_JAKOD').AsString <> '' then begin
		NoticeKi('A kiv�lasztott megb�z�s m�r j�rathoz tartozik!');
		Exit;
	end;
	// Megjelen�tj�k a rendsz�mhoz tartoz� j�ratokat, amib�l ki kell v�lasztani a megfelel�t
	if Query1.FieldByName('MB_RENSZ').AsString = '' then begin
		NoticeKi('Nincs megadva rendsz�m a megb�z�shoz!');
		Exit;
	end;
	if Query1.FieldByName('MB_VEKOD').AsString = '' then begin
		NoticeKi('Nincs megadva megb�z� a megb�z�shoz!');
		Exit;
	end;
	// A j�rat sz�r�s be�ll�t�sa
	EgyebDlg.SzuroGrid.RowCount 			:= EgyebDlg.SzuroGrid.RowCount + 1;
	szurosor								:= EgyebDlg.SzuroGrid.RowCount - 1 ;
	EgyebDlg.SzuroGrid.Cells[0,szurosor] 	:= '190';
	EgyebDlg.SzuroGrid.Cells[1,szurosor] 	:= 'JA_RENDSZ = '''+Query1.FieldByName('MB_RENSZ').AsString+''' ';
	EgyebDlg.megbizaskod					:= Query1.FieldByName('MB_MBKOD').AsString;
	JaratValaszto(jaratkod, jaratszam);
	EgyebDlg.megbizaskod					:= '';
	// A sz�r�s t�rl�se
	GridTorles(EgyebDlg.SzuroGrid, szurosor);
	if jaratkod.Text = '' then begin
		Exit;
	end;
	// Ellen�rizz�k a j�rat ellen�rz�tts�g�t
	if EllenorzottJarat(jaratkod.Text) > 0 then begin
		Exit;
	end;
	// L�trehozzuk a viszony-t �s hozz�rendelj�k a j�rathoz
	Application.CreateForm(TViszonybeDlg, ViszonybeDlg);
	ViszonybeDlg.ToltMegbizas(Query1.FieldByName('MB_MBKOD').AsString ,jaratkod.Text);
	ViszonybeDlg.ShowModal;
	if ViszonybeDlg.ret_kod <> '' then begin
		// L�trej�tt az �j viszonylat, bejegyezz�k a megb�z�sba is
		Query_Update( Query2, 'MEGBIZAS', [
			'MB_JAKOD', ''''+jaratkod.Text+'''',
			'MB_JASZA', ''''+jaratszam.Text+'''',
//           'MB_STATU', ''''+'130'+'''',
			'MB_VIKOD', ''''+ViszonybeDlg.ret_kod+''''
		], ' WHERE MB_MBKOD = '+Query1.FieldByName('MB_MBKOD').AsString);
		MegbizasSzamlaTolto(Query1.FieldByName('MB_MBKOD').AsString);

		// A megbizasok hozz�kapcsol�sa a j�rathoz
		Query_Run( Query2,  'SELECT COUNT(*) DB FROM JARATMEGBIZAS WHERE JM_JAKOD = '''+jaratkod.Text+''' ');
		sorsz	:= StrToIntDef(Query2.FieldByName('DB').AsString, 1) + 1;
		Query_Run( Query2,  'SELECT DISTINCT MB_JAKOD, MS_MBKOD, MS_SORSZ FROM MEGSEGED, MEGBIZAS WHERE MS_MBKOD = MB_MBKOD AND MB_JAKOD <> '''' '+
			' AND MB_MBKOD = '+Query1.FieldByName('MB_MBKOD').AsString+' ORDER BY MB_JAKOD, MS_MBKOD, MS_SORSZ ', TRUE);
		while not Query2.Eof do begin
			Query_Insert (QueryUj3, 'JARATMEGBIZAS',
				[
				'JM_JAKOD', ''''+Query2.FieldByName('MB_JAKOD').AsString+'''',
				'JM_MBKOD', Query2.FieldByName('MS_MBKOD').AsString,
				'JM_SORSZ', IntToStr(sorsz),
				'JM_ORIGS', Query2.FieldByName('MS_SORSZ').AsString
				]);
			Inc(sorsz);
			Query2.Next;
		end;
		if not FuvardijTolto(jaratkod.Text) then begin
			NoticeKi('A fuvard�j be�ll�t�sa nem siker�lt!');
		end;
	end;
	ViszonybeDlg.Destroy;
	ModLocate (Query1, 'MS_MSKOD', Query1.FieldByName('MS_MSKOD').AsString );
	SummaTolto;
end;

procedure TMegbizasUjFmDlg.BitBtn2Click(Sender: TObject);
var
	sorsz		: integer;
   ujkod		: string;
   torolheto   : boolean;
begin
	if not voltalt then begin
		voltalt 	:= true;
		Exit;
	end;
	if Query1.FieldByName('MB_JAKOD').AsString = '' then begin
		NoticeKi('A kiv�lasztott megb�z�s m�g nem kapcsol�dik j�rathoz!');
		Exit;
	end;
	// Ellen�rizz�k a j�rat ellen�rz�tts�g�t
	if EllenorzottJarat(Query1.FieldByName('MB_JAKOD').AsString) > 0 then begin
		Exit;
	end;
	// Ellen�rizz�k a viszonylat sz�ml�zotts�g�t
	Query_Run(Query2, 'SELECT VI_UJKOD FROM VISZONY WHERE VI_VIKOD = '''+Query1.FieldByName('MB_VIKOD').AsString+''' ');
   ujkod		:= Query2.FieldByName('VI_UJKOD').AsString;
   torolheto	:= true;
	if StrToIntDef(ujkod,0) > 0 then begin
       Query_Run(Query2, 'SELECT VI_JAKOD, MB_JAKOD FROM MEGBIZAS, VISZONY, JARAT WHERE VI_VIKOD = MB_VIKOD AND JA_KOD = MB_JAKOD AND VI_JAKOD <> MB_JAKOD '+
       	'AND MB_DATUM LIKE '''+EgyebDlg.Evszam+'%'' AND MB_MBKOD = '+Query1.FieldByName('MB_MBKOD').AsString);
       if Query2.FieldByName('VI_JAKOD').AsString = Query2.FieldByName('MB_JAKOD').AsString then begin
           // Ha nem egyezik a k�t k�d, akkor lehet t�r�lni, k�l�nben nem biztos ...
           Query_Run(Query2, 'SELECT * FROM SZFEJ WHERE SA_UJKOD = '+ujkod);
           if Query2.FieldByName('SA_FLAG').AsString = '1' then begin
           	// Csak a v�gleges�tett sz�ml�kat nem lehet t�r�lni, a storn�t lehet
				NoticeKi('A viszonylathoz m�r tartozik sz�mla, ez�rt nem t�r�lhet�!');
				Exit;
           end;
           if Query2.FieldByName('SA_FLAG').AsString > '1' then begin
           	// Stornos sz�ml�hoz tartoz� viszonyt nem lehet t�r�lni
           	torolheto	:= false;
           end;
       end;
	end;
	if NoticeKi('Val�ban t�r�lni k�v�nja a kijel�lt rekord j�rat kapcsolat�t?', NOT_QUESTION) <> 0 then begin
		Exit;
	end;
	// A viszonylat t�rl�se �s a MEGBIZAS rekord friss�t�se
   if torolheto then begin
		Query_Run(Query2, 'DELETE FROM VISZONY WHERE VI_VIKOD = '''+Query1.FieldByName('MB_VIKOD').AsString+''' ');
   end;
	Query_Update( Query2, 'MEGBIZAS', [
		'MB_JAKOD', ''''+''+'''',
		'MB_JASZA', ''''+''+'''',
		'MB_SZKOD', ''''+''+'''',
		'MB_VIKOD', ''''+''+''''
	], ' WHERE MB_MBKOD = '+Query1.FieldByName('MB_MBKOD').AsString);
	// A j�rat megb�z�sok t�rl�se
	Query_Run(Query2, 'DELETE FROM JARATMEGBIZAS WHERE JM_MBKOD = '''+Query1.FieldByName('MB_MBKOD').AsString+''' ');
	// �tsorsz�mozzuk a megl�v�ket
	sorsz	:= 1;
	Query_Run( Query2,  'SELECT * FROM JARATMEGBIZAS WHERE JM_JAKOD = '''+Query1.FieldByName('MB_JAKOD').AsString+''' ORDER BY JM_SORSZ ', TRUE);
	while not Query2.Eof do begin
		Query_Update (QueryUj3, 'JARATMEGBIZAS',
			[
			'JM_SORSZ', IntToStr(sorsz)
			], ' WHERE JM_JAKOD = '''+Query1.FieldByName('MB_JAKOD').AsString+''' '+
			   ' AND JM_MBKOD = '+Query2.FieldByName('JM_MBKOD').AsString +
			   ' AND JM_ORIGS = '+Query2.FieldByName('JM_ORIGS').AsString );
		Inc(sorsz);
		Query2.Next;
	end;
	if not FuvardijTolto(Query1.FieldByName('MB_JAKOD').AsString) then begin
		NoticeKi('A fuvard�j be�ll�t�sa nem siker�lt!');
	end;
	// A t�bla friss�t�se
	ModLocate (Query1, 'MS_MSKOD', Query1.FieldByName('MS_MSKOD').AsString );
	SummaTolto;
end;

procedure TMegbizasUjFmDlg.ButtonTorClick(Sender: TObject);
var
	rszlist	: TStringList;
	potlist	: TStringList;
	i		: integer;
begin
	if not voltalt then begin
		voltalt 	:= true;
		Exit;
	end;
	rszlist	:= TStringList.Create;
	potlist	:= TStringList.Create;
	// Ellen�rizz�k a viszonylat sz�ml�zotts�g�t
	Query_Run(Query2, 'SELECT VI_UJKOD FROM VISZONY WHERE VI_VIKOD = '''+Query1.FieldByName('MB_VIKOD').AsString+''' ');
	if StrToIntDef(Query2.FieldByName('VI_UJKOD').AsString,0) > 0 then begin
		NoticeKi('A viszonylathoz m�r tartozik sz�mla, ez�rt nem t�r�lhet�!');
		Exit;
	end;
	if StrToIntDef(Query1.FieldByName('MB_XMLSZ').AsString, 0) > 0 then begin
		if NoticeKi('FIGYELEM!!! A megb�z�s m�r export�l�sra ker�lt XML-be. Folytatja a t�rl�st?', NOT_QUESTION) <> 0 then begin
			Exit;
		end;
	end;
	if NoticeKi( 'Figyelem! Csak a kijel�lt t�telt k�v�nja t�r�lni? ', NOT_QUESTION ) = 0 then begin
		Query_Run(Query2, 'SELECT MS_RENSZ, MS_POTSZ FROM MEGSEGED WHERE MS_MBKOD = '''+Query1.FieldByName('MS_MBKOD').AsString+''' '+
			' AND MS_SORSZ = '+Query1.FieldByName('MS_SORSZ').AsString);
		while not Query2.Eof do begin
			if rszlist.IndexOf(Query2.FieldByName('MS_RENSZ').AsString) < 0 then begin
				rszlist.Add(Query2.FieldByName('MS_RENSZ').AsString);
			end;
			if potlist.IndexOf(Query2.FieldByName('MS_POTSZ').AsString) < 0 then begin
				potlist.Add(Query2.FieldByName('MS_POTSZ').AsString);
			end;
			Query2.Next;
		end;
		Query_Run(Query2, 'DELETE FROM MEGSEGED WHERE MS_MBKOD = '''+Query1.FieldByName('MS_MBKOD').AsString+''' '+
			' AND MS_SORSZ = '+Query1.FieldByName('MS_SORSZ').AsString);
		ModLocate(Query1, 'MS_MBKOD', Query1.FieldByName('MS_MBKOD').AsString );
		SummaTolto;
		if rszlist.Count > 0 then begin
			for i := 0 to rszlist.Count - 1 do begin
				GepStatTolto(rszlist[i]);
			end;
		end;
		if potlist.Count > 0 then begin
			for i := 0 to potlist.Count - 1 do begin
				PotStatTolto(potlist[i]);
			end;
		end;
		rszlist.Free;
		potlist.Free;
		Exit;
	end;
	if NoticeKi( 'Figyelem! Val�ban a teljes megb�z�st t�r�lni k�v�nja? ', NOT_QUESTION ) = 0 then begin
		Query_Run(Query2, 'SELECT MS_RENSZ, MS_POTSZ FROM MEGSEGED WHERE MS_MBKOD = '''+Query1.FieldByName('MS_MBKOD').AsString+''' ');
		while not Query2.Eof do begin
			if rszlist.IndexOf(Query2.FieldByName('MS_RENSZ').AsString) < 0 then begin
				rszlist.Add(Query2.FieldByName('MS_RENSZ').AsString);
			end;
			if potlist.IndexOf(Query2.FieldByName('MS_POTSZ').AsString) < 0 then begin
				potlist.Add(Query2.FieldByName('MS_POTSZ').AsString);
			end;
			Query2.Next;
		end;
		// A megb�z�s g�pkocsi + p�tkocsi hozz�rak�sa
		if rszlist.IndexOf(Query1.FieldByName('MB_RENSZ').AsString) < 0 then begin
			rszlist.Add(Query1.FieldByName('MB_RENSZ').AsString);
		end;
		if potlist.IndexOf(Query1.FieldByName('MB_POTSZ').AsString) < 0 then begin
			potlist.Add(Query1.FieldByName('MB_POTSZ').AsString);
		end;
		Query_Run(Query2, 'DELETE FROM MEGSEGED WHERE MS_MBKOD = '''+Query1.FieldByName('MS_MBKOD').AsString+''' ');
		Query_Run(Query2, 'DELETE FROM MEGBIZAS WHERE MB_MBKOD = '''+Query1.FieldByName('MS_MBKOD').AsString+''' ');
		Query_Run(Query2, 'DELETE FROM MEGLEVEL WHERE ML_MBKOD = '''+Query1.FieldByName('MS_MBKOD').AsString+''' ');
		ModLocate(Query1, 'MS_MSKOD', Query1.FieldByName('MS_MSKOD').AsString );
		SummaTolto;
		if rszlist.Count > 0 then begin
			for i := 0 to rszlist.Count - 1 do begin
				GepStatTolto(rszlist[i]);
			end;
		end;
		if potlist.Count > 0 then begin
			for i := 0 to potlist.Count - 1 do begin
				PotStatTolto(potlist[i]);
			end;
		end;
	end;
	rszlist.Free;
	potlist.Free;
end;

procedure TMegbizasUjFmDlg.FormResize(Sender: TObject);
begin
	Inherited FormResize(Sender);
	M2.Top			:= BitBtn3.Top+2;
	M2.Left			:= BitBtn3.Left;
	BitBtn5.Top		:= BitBtn3.Top+2;
	BitBtn5.Left	:= BitBtn3.Left + M2.Width;
	BitBtn5.Width	:= BitBtn5.Height;

	M3.Top			:= BitBtn6.Top+2;
	M3.Left			:= BitBtn5.Left + BitBtn5.Width + 10;
	BitBtn4.Top		:= M3.Top;
	BitBtn4.Left	:= M3.Left + M3.Width;
	BitBtn4.Width	:= BitBtn4.Height;

	BitBtn10.Top	:= BitBtn6.Top;
	BitBtn10.Left	:= BitBtn4.Left + BitBtn4.Width + 10;
	BitBtn10.Width	:= 40;

	// Az �tcsatol�sok �s list�z�sok kezel�se
//	BitBtn11.Top	:= BitBtn6.Top;
//	BitBtn11.Left	:= BitBtn10.Left + BitBtn10.Width + 20;
	BitBtn11.Width	:= 90;
	BitBtn12.Top	:= BitBtn6.Top;
	BitBtn12.Left	:= BitBtn11.Left + BitBtn11.Width + 2;
	BitBtn12.Width	:= 40;

end;

procedure TMegbizasUjFmDlg.BitBtn5Click(Sender: TObject);
begin
	EgyebDlg.Calendarbe(M2);
	M3.Text	:= M2.Text;
//	alapstr	:= 'SELECT * FROM MEGSEGED, MEGBIZAS WHERE MS_MBKOD = MB_MBKOD AND MS_DATUM = '''+M2.Text+''' ';
	alapstr	:= 'SELECT * FROM MEGSEGED, MEGBIZAS WHERE MS_MBKOD = MB_MBKOD AND MS_DATUM = '''+M2.Text+''' ';
	SQL_ToltoFo(GetOrderBy(true));
end;

procedure TMegbizasUjFmDlg.DBGrid2DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
var
	szin	: integer;
//	oldszin	: integer;
   stkod	: string;
   i		: integer;
	Icon	: TBitmap;
begin
//	oldszin := DBGrid2.Canvas.Brush.Color;
	// A felrak�s/lerak�s sz�n�nek meghat�roz�sa
	if oldrecord = Query1.FieldByName('MS_MSKOD').AsString then begin
//     	szin		:= EgyebDlg.row_color;
	end else begin
		szin	:= clWindow;
	end;
	stkod	:= '';
	try
		stkod	:= Query1.FieldByName('MS_STATK').AsString;
	except
	end;
	if stkod <> '' then begin
		i := 1;
		while i <= 10 do begin
			if statusszin[i, 1] = stkod then begin
				szin	:= StrToIntDef(statusszin[i, 2], clAqua);
				i		:= 10;
			end;
           Inc(i);
		end;
   end;
	// Ha felrakott exportunk van �s ez lerak�s �s nincs lerak�si d�tum, akkor a sz�n = szabad export = 110
   if ( ( stkod = '130' ) and (Query1.FieldByName('MS_TIPUS').AsString = '1' ) and (Query1.FieldByName('MS_LETIDO').AsString = '' ) ) then begin
		stkod := '110';
		i := 1;
		while i <= 10 do begin
			if statusszin[i, 1] = stkod then begin
				szin	:= StrToIntDef(statusszin[i, 2], clAqua);
				i		:= 10;
			end;
			Inc(i);
		end;
	end;
	if ( ( stkod = '230' ) and (Query1.FieldByName('MS_TIPUS').AsString = '1' ) and (Query1.FieldByName('MS_LETIDO').AsString = '' ) ) then begin
		stkod := '210';
		i := 1;
		while i <= 10 do begin
			if statusszin[i, 1] = stkod then begin
				szin	:= StrToIntDef(statusszin[i, 2], clAqua);
				i		:= 10;
			end;
			Inc(i);
		end;
	end;
	if SelectList.IndexOf(Query1.FieldByName(FOMEZO).AsString) > -1 then begin
		// A sor ki van jel�lve
		szin		:= clSilver;
	end;
	if oldrecord = Query1.FieldByName('MS_MSKOD').AsString then begin
//     	szin		:= EgyebDlg.row_color;
	end;
	DBGrid2.Canvas.Brush.Color		:= szin;
	DBGrid2.DefaultDrawDataCell(Rect, Column.Field, State);
	alapszin						:= szin;	
	inherited DBGrid2DrawColumnCell(Sender, Rect, DataCol, Column, State);
	// Ikon kirajzol�sa a fontoss�ghoz!!!
	if Column.FieldName = 'MS_KIFON' then begin
		Icon:=TBitmap.Create;
		DBGrid2.Canvas.FillRect(Rect);
		if Query1.FieldByName('MS_KIFON').AsString = '1' then begin
			ImageList1.GetBitmap(0,Icon);
			DBGrid2.Canvas.Draw(round((Rect.Left+Rect.Right-Icon.Width) DIV 2), Rect.Top + ((Rect.Bottom-Rect.Top-Icon.Height) DIV 2),Icon);
		end;
	end;
//  	DBGrid2.Canvas.Brush.Color		:= oldszin;
end;

procedure TMegbizasUjFmDlg.BitBtn6Click(Sender: TObject);
begin
(*
	// Let�r�lj�k a felt�teleket
	alapstr	:= 'SELECT * FROM MEGSEGED, MEGBIZAS WHERE MS_MBKOD = MB_MBKOD ';
	SQL_ToltoFo(GetOrderBy(true));
*)	
end;

procedure TMegbizasUjFmDlg.BitBtn7Click(Sender: TObject);
begin
	// Ellen�rizz�k, hogy ki dolgozik m�g ezen a megb�z�son
	 if Query1.FieldByName('MB_AFKOD').AsString <> '' then begin
		if Query1.FieldByName('MB_AFKOD').AsString <> EgyebDlg.user_code then begin
			NoticeKi('A megb�z�st �ppen m�s m�dos�tja!');
			Exit;
		end;
	 end;
	// A kijel�lt felrak� megjelenit�se
	StatuszBeiras(Query1.FieldByName('MB_MBKOD').AsString);
	Application.CreateForm(TSegedbe2Dlg,Segedbe2Dlg);
	Segedbe2Dlg.MegForm		:= Self;
	Segedbe2Dlg.kellelore	:= true;
	Segedbe2Dlg.tolt('Felrak�s/lerak�s m�dos�t�sa', Query1.FieldByName('MB_MBKOD').AsString,
		Query1.FieldByname('MS_SORSZ').AsString, Query1.FieldByName('MS_MSKOD').AsString);
	Segedbe2Dlg.ShowModal;
	StatuszTorles;
	SummaTolto;
	if Segedbe2Dlg.voltenter then begin
		ModLocate (Query1, 'MS_MSKOD', Query1.FieldByName('MS_MSKOD').AsString );
	end;
	Segedbe2Dlg.Destroy;
end;

procedure TMegbizasUjFmDlg.BitBtn8Click(Sender: TObject);
begin
	// J�rat megjelen�t�se
	if Query1.FieldByName('MB_JAKOD').AsString <> '' then begin
		Query_Run (QueryUj3, 'SELECT * FROM JARAT WHERE JA_KOD = '''+Query1.FieldByName('MB_JAKOD').AsString+''' ',true);
		if QueryUj3.RecordCount < 1 then begin
			NoticeKi('El�z� �vi j�rat!');
			Exit;
		end;
		Application.CreateForm(TJaratbeDlg, JaratbeDlg);
		JaratbeDlg.Tolto('J�rat m�dos�t�s',Query1.FieldByName('MB_JAKOD').AsString);
		JaratbeDlg.ShowModal;
		JaratbeDlg.Destroy;
	end;
end;

procedure TMegbizasUjFmDlg.DatumAtirasClick(Sender: TObject);
var
	oldmezo1	: string;
   statusz		: integer;
begin
	oldmezo1	:= Query1.FieldByName('MS_MSKOD').AsString;
	Application.CreateForm(TIdobeDlg,IdobeDlg);
   IdobeDlg.M5.Text	:= Query1.FieldByName('MS_DATUM').AsString;
   IdobeDlg.M6.Text	:= Query1.FieldByName('MS_IDOPT').AsString;
	IdobeDlg.ShowModal;
  	if IdobeDlg.ret_datum <> '' then begin
       if Query1.FieldByName('MS_TIPUS').AsString = '0' then begin
       	// Felrak�sr�l van sz�
           Query_Update (Query2, 'MEGSEGED',
               [
               'MS_FETDAT', 	''''+IdobeDlg.ret_datum+'''',
               'MS_FETIDO', 	''''+IdobeDlg.ret_idopt+''''
				],
				' WHERE MS_MBKOD = '+Query1.FieldByName('MS_MBKOD').AsString+
				' AND MS_SORSZ = '+Query1.FieldByName('MS_SORSZ').AsString );
			Query_Update (Query2, 'MEGSEGED',
				[
				'MS_DATUM',     ''''+IdobeDlg.ret_datum+'''',
				'MS_IDOPT',     ''''+IdobeDlg.ret_idopt+'''',
				'MS_TEIDO',     ''''+IdobeDlg.ret_idopt+''''
				],
				' WHERE MS_MBKOD = '+Query1.FieldByName('MS_MBKOD').AsString+
				' AND MS_SORSZ = '+Query1.FieldByName('MS_SORSZ').AsString+' AND MS_TIPUS = 0' );
		end else begin
       	// Lerak�sr�l van sz�
           Query_Update (Query2, 'MEGSEGED',
               [
               'MS_LETDAT', 	''''+IdobeDlg.ret_datum+'''',
               'MS_LETIDO', 	''''+IdobeDlg.ret_idopt+''''
               ],
               ' WHERE MS_MBKOD = '+Query1.FieldByName('MS_MBKOD').AsString+
               ' AND MS_SORSZ = '+Query1.FieldByName('MS_SORSZ').AsString );
           Query_Update (Query2, 'MEGSEGED',
               [
               'MS_DATUM',     ''''+IdobeDlg.ret_datum+'''',
               'MS_IDOPT',     ''''+IdobeDlg.ret_idopt+'''',
               'MS_TEIDO',     ''''+IdobeDlg.ret_idopt+''''
               ],
               ' WHERE MS_MBKOD = '+Query1.FieldByName('MS_MBKOD').AsString+
               ' AND MS_SORSZ = '+Query1.FieldByName('MS_SORSZ').AsString+' AND MS_TIPUS = 1' );
       end;
		// A st�tusz be�ll�t�sa az �rintett elemekn�l
       Query_Run(Query2, 'SELECT * FROM MEGSEGED WHERE MS_MBKOD = '+Query1.FieldByName('MS_MBKOD').AsString+
			' AND MS_SORSZ = '+Query1.FieldByName('MS_SORSZ').AsString);
		if Query2.RecordCount > 0 then begin
           while not Query2.Eof do begin
               statusz	:= GetFelrakoStatus(Query2.FieldByName('MS_MSKOD').AsString);
               SetFelrakoStatus(Query2.FieldByName('MS_MSKOD').AsString, statusz);
               Query2.Next;
			end;
		end;
		// �jrasz�moljuk a g�pkocsi statisztik�kat
		GepStatTolto(Query1.FieldByName('MS_RENSZ').AsString);
		PotStatTolto(Query1.FieldByName('MS_POTSZ').AsString);
//		alapstr	:= 'SELECT * FROM MEGSEGED, MEGBIZAS WHERE MS_MBKOD = MB_MBKOD AND MS_DATUM = '''+M2.Text+''' ';
		alapstr	:= 'SELECT * FROM MEGSEGED, MEGBIZAS WHERE MS_MBKOD = MB_MBKOD AND MS_DATUM >= '''+M2.Text+''' '+
			'AND MS_DATUM <= '''+M3.Text+''' ';
		SQL_ToltoFo(GetOrderBy(true));
		ModLocate (Query1, 'MS_MSKOD', oldmezo1 );
		SummaTolto;
	end;
	IdobeDlg.Destroy;
end;

procedure TMegbizasUjFmDlg.BitBtn9Click(Sender: TObject);
begin
	// Megjelen�tj�k a g�pkocsi formot
	Screen.Cursor	:= crHourGlass;
	Application.CreateForm(TGepStatFmDlg, GepStatFmDlg);
	Screen.Cursor	:= crDefault;
	GepStatFmDlg.ShowModal;
	GepStatFmDlg.Destroy;
end;

procedure TMegbizasUjFmDlg.SzinbeallitasClick(Sender: TObject);
var
	stkod	: string;
	i		: integer;
begin
	// A kijel�lt sor st�tusz sz�n�nek be�ll�t�sa
	if ColorDialog2.Execute then begin
		stkod	:= '';
		try
			stkod	:= Query1.FieldByName('MS_STATK').AsString;
		except
		end;
		if stkod <> '' then begin
			Query_Run(EgyebDlg.QueryKozos, 'UPDATE SZOTAR SET SZ_KODHO = '+IntToStr(ColorDialog2.Color)+
				' WHERE SZ_FOKOD = ''960'' AND SZ_ALKOD = '''+stkod+''' ' );
			// A t�bl�zat friss�t�se
			Query_Run(EgyebDlg.QueryKozos, 'SELECT SZ_ALKOD, SZ_KODHO FROM SZOTAR WHERE SZ_FOKOD = ''960'' ' );
			i := 1;
			while ( ( i <= 10 ) and ( not EgyebDlg.QueryKozos.Eof ) ) do begin
				statusszin[i, 1] := EgyebDlg.QueryKozos.FieldByName('SZ_ALKOD').AsString;
				statusszin[i, 2] := EgyebDlg.QueryKozos.FieldByName('SZ_KODHO').AsString;
				Inc(i);
				EgyebDlg.QueryKozos.Next;
			end;
			ModLocate (Query1, 'MS_MSKOD', Query1.FieldByName('MS_MSKOD').AsString );
			SummaTolto;
		end;
	end;
end;

procedure TMegbizasUjFmDlg.Timer1Timer(Sender: TObject);
begin
	// Ellen�rizz�k, hogy t�rt�nt-e v�ltoz�s az adatb�zisban, �s ha igen, friss�t�nk
	Timer1.Enabled	:= false;
	Query_Run(QueryUj3, 'SELECT SUM(MB_AFLAG) OSSZEG FROM MEGBIZAS ');
	if checksumma <> StrToIntDef(QueryUj3.FieldByName('OSSZEG').AsString,1) then begin
		checksumma	:= StrToIntDef(QueryUj3.FieldByName('OSSZEG').AsString,1);
		ModLocate (Query1, 'MS_MSKOD', Query1.FieldByName('MS_MSKOD').AsString );
	end;
	Timer1.Enabled	:= true;
end;

procedure TMegbizasUjFmDlg.StatuszBeiras(mbkod : string);
begin
	if mbkod = '' then begin
		Exit;
	end;
	Query_Update(QueryUj3, 'MEGBIZAS',
		[
		'MB_AFKOD', ''''+EgyebDlg.user_code+'''',
		'MB_AFNEV', ''''+EgyebDlg.user_name+'''',
		'MB_AFLAG', IntToStr(StrToINtDef(FormatDateTime('nnss', now),0))
		], ' WHERE MB_MBKOD = '+mbkod );
end;

procedure TMegbizasUjFmDlg.StatuszTorles;
begin
	Query_Update(QueryUj3, 'MEGBIZAS',
		[
		'MB_AFKOD', ''''+''+'''',
		'MB_AFNEV', ''''+''+'''',
		'MB_AFLAG', IntToStr(StrToINtDef(FormatDateTime('nnss', now),0))
		], ' WHERE MB_AFKOD = '''+EgyebDlg.user_code+'''' );
end;

procedure TMegbizasUjFmDlg.SummaTolto;
begin
	// Felt�ltj�k a checksumm�t
	Timer1.Enabled	:= false;
	Query_Run(QueryUj3, 'SELECT SUM(MB_AFLAG) OSSZEG FROM MEGBIZAS ');
	checksumma	:= StrToIntDef(QueryUj3.FieldByName('OSSZEG').AsString,1);
	Timer1.Enabled	:= true;
end;

procedure TMegbizasUjFmDlg.BitBtn4Click(Sender: TObject);
begin
	EgyebDlg.Calendarbe(M3);
	// Az intervallum megad�sa
	alapstr	:= 'SELECT * FROM MEGSEGED, MEGBIZAS WHERE MS_MBKOD = MB_MBKOD AND MS_DATUM >= '''+M2.Text+''' '+
		'AND MS_DATUM <= '''+M3.Text+''' ';
	SQL_ToltoFo(GetOrderBy(true));
end;

procedure TMegbizasUjFmDlg.BitBtn10Click(Sender: TObject);
begin
	// Let�r�lj�k a felt�teleket
	alapstr	:= 'SELECT * FROM MEGSEGED, MEGBIZAS WHERE MS_MBKOD = MB_MBKOD ';
	SQL_ToltoFo(GetOrderBy(true));
end;

procedure TMegbizasUjFmDlg.BitBtn11Click(Sender: TObject);
var
	szurosor	: integer;
	ujkod		: string;
	vikod		: string;
begin
	// Az �tcsatol�s megval�s�t�sa
	if not voltalt then begin
		voltalt 	:= true;
		Exit;
	end;
	// Hozz�rendelj�k a rekordot a j�rathoz
	if Query1.FieldByName('MB_JAKOD').AsString = '' then begin
		NoticeKi('A kiv�lasztott megb�z�shoz m�g nem tartozik j�rat!');
		Exit;
	end;
	// Megjelen�tj�k a rendsz�mhoz tartoz� j�ratokat, amib�l ki kell v�lasztani a megfelel�t
	if Query1.FieldByName('MB_RENSZ').AsString = '' then begin
		NoticeKi('Nincs megadva rendsz�m a megb�z�shoz!');
		Exit;
	end;
	if Query1.FieldByName('MB_VEKOD').AsString = '' then begin
		NoticeKi('Nincs megadva megb�z� a megb�z�shoz!');
		Exit;
	end;
	// A j�rat sz�r�s be�ll�t�sa
	EgyebDlg.SzuroGrid.RowCount 			:= EgyebDlg.SzuroGrid.RowCount + 1;
	szurosor								:= EgyebDlg.SzuroGrid.RowCount - 1 ;
	EgyebDlg.SzuroGrid.Cells[0,szurosor] 	:= '190';
	EgyebDlg.SzuroGrid.Cells[1,szurosor] 	:= 'JA_RENDSZ = '''+Query1.FieldByName('MB_RENSZ').AsString+''' ';
	EgyebDlg.megbizaskod					:= Query1.FieldByName('MB_MBKOD').AsString;
	JaratValaszto(jaratkod, jaratszam);
	EgyebDlg.megbizaskod					:= '';
	// A sz�r�s t�rl�se
	GridTorles(EgyebDlg.SzuroGrid, szurosor);
	if jaratkod.Text = '' then begin
		Exit;
	end;
	// Ellen�rizz�k a j�rat ellen�rz�tts�g�t
	if EllenorzottJarat(jaratkod.Text) > 0 then begin
		Exit;
	end;
	// Ellen�rizz�k a j�rathoz tartoz� viszonyokat
	Query_Run(Query2, 'SELECT * FROM VISZONY WHERE VI_JAKOD = '''+jaratkod.Text+''' ');
	if Query2.RecordCount < 1 then begin
		NoticeKi('A j�rathoz m�g nem kapcsol�dnak viszonylatok!');
		Exit;
	end;
	// A megfelel� viszonylat kiv�laszt�sa
	vikod	:= '';
	if Query2.RecordCount > 1 then begin
		Application.CreateForm(TRendvalDlg,RendvalDlg);
		RendvalDlg.Tolt('A megb�z�s kiv�laszt�sa',jaratkod.Text);
		if RendvalDlg.vanrek then begin
			RendvalDlg.ShowModal;
			if RendvalDlg.ret_vikod <> '' then begin
				{A megb�z�s hozz�rendel�se a k�lts�ghez}
				vikod	:= RendvalDlg.ret_vikod;
			end;
		end;
		RendvalDlg.Destroy;
	end else begin
		vikod	:= Query2.FieldByName('VI_VIKOD').AsString;
	end;
	if vikod = '' then begin
		Exit;
	end;
	// Az �tkapcsol�s v�grehajt�sa
	ujkod	:= IntToStr(GetNextCode('MBCSATOL', 'MC_MCKOD', 1, 0));
	if not Query_Insert(Query2, 'MBCSATOL',
		[
		'MC_MCKOD', 	ujkod,
		'MC_MBKOD',     Query1.FieldByName('MB_MBKOD').AsString,
		'MC_JAKOD',		''''+Query1.FieldByName('MB_JAKOD').AsString+'''',
		'MC_VIKOD',		Query1.FieldByName('MB_VIKOD').AsString,
		'MC_DATUM',     ''''+FormatDateTime('YYY.MM.DD.', now)+'''',
		'MC_IDOPT',     ''''+FormatDateTime('hh:nn:ss', now)+''''
		]) then begin
		NoticeKi('A csatol�si rekord besz�r�sa nem siker�lt!');
		Exit;
	end;
   // A JARATMEGBIZAS m�dos�t�sa
	Query_Update( Query2, 'JARATMEGBIZAS', [
		'JM_JAKOD', ''''+jaratkod.Text+''''
		], ' WHERE JM_MBKOD = '+IntToStr(StrToIntDef(Query1.FieldByName('MB_MBKOD').AsString, 0))+
   	' AND JM_JAKOD = '''+Query1.FieldByName('MB_JAKOD').AsString+'''');

	Query_Update( Query2, 'MEGBIZAS', [
		'MB_JAKOD', ''''+jaratkod.Text+'''',
		'MB_JASZA', ''''+jaratszam.Text+'''',
		'MB_VIKOD', IntToStr(StrToIntDef(vikod, 0))
	], ' WHERE MB_MBKOD = '+IntToStr(StrToIntDef(Query1.FieldByName('MB_MBKOD').AsString, 0)));
	MegbizasSzamlaTolto(IntToStr(StrToIntDef(Query1.FieldByName('MB_MBKOD').AsString, 0)));
	ModLocate (Query1, 'MS_MSKOD', Query1.FieldByName('MS_MSKOD').AsString );
end;

procedure TMegbizasUjFmDlg.BitBtn12Click(Sender: TObject);
begin
	// Az �tcsatolt elemek list�z�sa
	if not voltalt then begin
		voltalt 	:= true;
		Exit;
	end;
	// Hozz�rendelj�k a rekordot a j�rathoz
	if Query1.FieldByName('MB_JAKOD').AsString = '' then begin
		NoticeKi('A kiv�lasztott megb�z�shoz m�g nem tartozik j�rat!');
		Exit;
	end;
	// Ellen�rizz�k a j�rathoz tartoz� �tcsatol�sokat
	Query_Run(Query2, 'SELECT * FROM MBCSATOL WHERE MC_MBKOD = '''+Query1.FieldByName('MB_MBKOD').AsString+''' ');
	if Query2.RecordCount < 1 then begin
		NoticeKi('A megb�z�shoz m�g nem tartoznak �tcsatol�sok!');
		Exit;
	end;
	// Az �tcsatol�sok megjelen�t�se
	Application.CreateForm(TCsatListFmDlg,CsatListFmDlg);
	CsatListFmDlg.Tolt('�tcsatol�sok list�ja',Query1.FieldByName('MB_MBKOD').AsString);
	CsatListFmDlg.ShowModal;
	CsatListFmDlg.Destroy;
end;

function TMegbizasUjFmDlg.KeresChange(mezo, szoveg : string) : string;
begin
	Result	:= szoveg;
	if UpperCase(mezo) = 'MB_SZKOD' then begin
		Result := elotag + Format('%5.5d',[StrToIntDef(szoveg,0)]);
	end;
end;

end.

