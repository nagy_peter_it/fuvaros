unit BazisEllen;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Buttons, Vcl.ExtCtrls,
  Data.DB, Data.Win.ADODB;

type
  TBazisEllenDlg = class(TForm)
    Panel1: TPanel;
    BitKilep: TBitBtn;
    BitElkuld: TBitBtn;
    Label1: TLabel;
    Memo1: TMemo;
    Query1: TADOQuery;
    procedure BitElkuldClick(Sender: TObject);
    procedure BitKilepClick(Sender: TObject);
    procedure Kodellenor(tname, fname, id : string);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  BazisEllenDlg: TBazisEllenDlg;

implementation

{$R *.dfm}

uses
   Egyeb, J_SQL;

procedure TBazisEllenDlg.BitElkuldClick(Sender: TObject);
begin
   Memo1.Clear;
   Memo1.Lines.Add('Adatb�zis ellen�rz�s ind�t�sa '+FormatDateTime('YYYY.MM.DD. hh.nn.ss', now));
   // A f�k�dok ellen�rz�se
   Kodellenor('ALGEPKOCSI', 'AG_AGKOD', '0');
   Kodellenor('ALVALLAL', 'V_KOD', '0');
   Kodellenor('ARFOLYAM', 'AR_KOD', '0');
   Kodellenor('DIALOGS', 'DL_DLKOD', '0');
   Kodellenor('DKVKARTYA', 'DK_KAKOD', '0');
   Kodellenor('DOLGOZO', 'DO_KOD', '0');
   Kodellenor('DOLGOZOFOTO', 'DO_KOD', '0');
   Kodellenor('FELRAKO', 'FF_FEKOD', '0');
   Kodellenor('GARANCIA', 'GA_GAKOD', '0');
   Kodellenor('GEPKOCSI', 'GK_KOD', '0');
   Kodellenor('JELSZO', 'JE_FEKOD', '0');
   Kodellenor('PONTOK', 'PP_PPKOD', '0');
   Kodellenor('SHELL', 'SH_KAKOD', '0');
   Kodellenor('TELEFON', 'TE_FOKOD', '0');
   Kodellenor('TERMEK', 'T_TERKOD', '0');
   Kodellenor('TETANAR', 'TT_TTKOD', '0');
   Kodellenor('UZEMANYAG', 'UA_UAKOD', '0');
   Kodellenor('UZENET', 'UZ_UZKOD', '0');
   Kodellenor('VEVO', 'V_KOD', '0');
   Kodellenor('ALJARAT', 'AJ_JAKOD', '1');
   Kodellenor('ALSEGED', 'AS_JAKOD', '1');
   Kodellenor('CMRDAT', 'CM_CMKOD', '1');
   Kodellenor('CSATOLMANY', 'CS_CSKOD', '1');
   Kodellenor('FUVARTABLA', 'FT_FTKOD', '1');
   Kodellenor('JARAT', 'JA_KOD', '1');
   Kodellenor('JARPOTOS', 'JP_JPKOD', '1');
   Kodellenor('KOLTSEG', 'KS_KTKOD', '1');
   Kodellenor('KOLTSEG_GYUJTO', 'KS_KTKOD', '1');
   Kodellenor('MEGBIZAS', 'MB_MBKOD', '1');
   Kodellenor('MEGBIZAS2', 'MB_MBKOD', '1');
   Kodellenor('MEGLEVEL', 'ML_MLKOD', '1');
//   Kodellenor('MEGSEGED', 'MS_MSKOD', '1');
   Kodellenor('MEGSEGED2', 'MS_MSKOD', '1');
   Kodellenor('PALETTA', 'PA_ID', '1');
//   Kodellenor('SERVICE_HIS', 'SH_KOD', '1');
   Kodellenor('SULYOK', 'SU_SUKOD', '1');
   Kodellenor('SZFEJ', 'SA_UJKOD', '1');
   Kodellenor('SZFEJ2', 'SA_KOD', '1');
   Kodellenor('T_GEPK', 'TG_RENSZ', '1');
   Kodellenor('T_POTK', 'TP_POTSZ', '1');
   Kodellenor('TEHER', 'TH_THKOD', '1');
   Kodellenor('VISZONY', 'VI_VIKOD', '1');
   Kodellenor('ZAROKM', 'ZK_ZKKOD', '1');
   //   Kodellenor('', '', '0');

   Memo1.Lines.Add('Az ellen�rz�s befejez�d�tt '+FormatDateTime('YYYY.MM.DD. hh.nn.ss', now));
   Label1.Caption  := '';
   Label1.Update;
end;

procedure TBazisEllenDlg.BitKilepClick(Sender: TObject);
begin
   Close;
end;

procedure TBazisEllenDlg.Kodellenor(tname, fname, id : string);
begin
   Label1.Caption  := tname+ '-'+fname+' egyedis�g ellen�rz�se';
   Label1.Update;
   Query1.Close;
   Query1.Tag  := StrToIntDef(id, 0);
   EgyebDlg.SeTADOQueryDatabase(Query1);
   Query_Run(Query1, 'SELECT '+fname+', COUNT(*) AS DB FROM '+tname+' GROUP BY '+fname+' ORDER BY DB DESC');
   while ( ( Query1.FieldByName('DB').AsString > '1' ) and (not Query1.Eof) ) do begin
       Memo1.Lines.Add('     K�d duplik�ci� : '+tname+' - ' + fname + '='+Query1.FieldByName(fname).AsString+'->'+Query1.FieldByName('DB').AsString);
       Query1.Next;
   end;
end;


end.
