unit J_DataModule;

interface

uses
  SysUtils, Classes, J_SQL, Egyeb, DB, ADODB, Kozos, Contnrs;

type

  TJDataModuleDlg = class(TDataModule)
	 QKozAdat: TADOQuery;
	 QDatAdat: TADOQuery;
	 procedure DataModuleCreate(Sender: TObject);
	private
	public
		eredmenyflag	:boolean;
	end;

	TJAfaExportSor  = class (TJDataModuleDlg)
		private
		public
		kod				: string;
		korzet			: string;
		bizonylat		: string;
		kiegybiz		: string;
		teljkelt		: string;
		esedkelt		: string;
		szoveg			: string;
		tartfsz	 		: string;
		kovfsz			: string;
		tartafafsz		: string;
		kovafafsz		: string;
		netto			: string;
		afa_ft			: string;
		afakulcs		: string;
		brutto	  		: string;
		valuta    		: string;
		valutaafa		: string;
		valutanem		: string;
		part_nev		: string;
		part_cim		: string;
		bizkelt			: string;
		bizbelso		: string;
		afanul			: string;
		afakelt			: string;
		techkelt		: string;
		bekelt	   		: string;
	end;

	TJAfaSor  = class (TJDataModuleDlg)
		private
		public
		afakod		: string;
		afaszaz	    : double;
		afaalap		: double;
		afaertek    : double;
		afaaleur    : double;
    afaaleur2   : double;  // k�zvetlen�l SS_VALERT -b�l
		afaerteur   : double;
	end;

	TJSzamlaSor  = class (TJDataModuleDlg)
		function FillRowByQuery(qe : TADOQuery; szamlavaluta, datum : string; euros : double) : boolean;
		private
		public
			fejazonosito 	: integer;
			szamlaszam		: string;
			sorszam			: integer;
			regi_sorszam	: integer;
			regi_ujkod		: integer;
			termekkod		: string;
			termeknev   	: string;
			cikkszam     	: string;
			itjszj       	: string;
			megyseg      	: string;
			mennyiseg       : double;
			egysegar        : double;
			afakod     		: string;
			afaszaz         : double;
			kedvszaz        : double;
			valutanem    	: string;
			valutaegyar     : double;
			valutaarfoly    : double;
			leiras      	: string;
			kulfoldinev  	: string;
			kulfoldileiras 	: string;
			kulfoldimegyseg	: string;
			eurarfdatum  	: string;
			eurarfertek   	: double;
			nemeus			: integer;
			// Sz�molt mez�k
			sornetto		: double;
			sorafa			: double;
			sorbrutto		: double;
			sorneteur		: double;
			sorbruteur		: double;
			sorbruteur2		: double; // k�zvetlen�l SS_VALERT-b�l
			sorafaeur		: double;
			afastring		: string;
			sorteljesites	: string;
			sormegjegyzes	: string;
	end;

	TJSzamla  = class (TJDataModuleDlg)
		constructor Create(tobj : TComponent);
		destructor Destroy;
		function FillSzamla(szkod : string; tipus : integer = 0) : boolean;
		function FillOsszSzamla(szkod : string) : boolean;
		function GetAfaSorszam(akod : string) : integer;
		function WriteToList(li : TStringList) : boolean;
		private
			uapotlek_kod1,	uapotlek_kod2, uapotlek_kod3: string;
			spedicio_kod	: string;
		public
			azonosito		: integer;	//UJKOD
			osszesitett		: integer;
			szamlaszam		: string;
			datum		 	: string;
			kiadas        	: string;
			esedekesseg		: string;
			teljesites		: string;
			vevokod			: string;
			vevonev			: string;
			vevovaros		: string;
			vevoirsz		: string;
			vevocim			: string;
			vevoorszag		: string;
      adoorszag		: string;
			vevoadoszam		: string;
			vevoEUadoszam	: string;
			szamlaadoszam	: string;
			vevobank		: string;
			vevokedv        : double;
			szamlaosszeg    : double;
			szamlaeuro      : double;
			szamlaafa       : double;
			fizetesimod		: string;
			megjegyzes		: string;
			allapot			: integer;
			melleklet		: array[1..5] of string;
			fejlec			: array[1..7] of string;
			nemetmegj		: array[1..3] of string;
			jaratszam  		: string;
			rendszam  		: string;
			pozicio    		: string;
			peldany			: integer;
			tipus      		: integer;
			reszszamla    	: integer;
			sorrend     	: integer;
			helyesbito  	: integer;
			fokonyv    		: string;
			atadasflag		: integer;
			atadasdatum		: string;
			atadasfinev		: string;
			megrendeles		: string;
			Szamlasorok		: TObjectList;
			Afasorok		: TObjectList;
			ossz_netto		: double;
			ossz_afa		: double;
			ossz_brutto		: double;
			ossz_nettoeur	: double;
			ossz_nettoeur2 : double;  // k�zvetlen�l SS_VALERT �sszegz�se
			ossz_afaeur		: double;
			ossz_bruttoeur	: double;
			uapotlekeur		: double;
			spedicioeur		: double;
			spedicioft		: double;
			szamla_valutanem: string;
      szamla_valuta_arfolyam: double;
			vannaksorok		: boolean;
      szamla_nyelv    : string;
      afakod27        : string;
	end;

var
	JDataModuleDlg		: TJDataModuleDlg;
	JSzamla          	: TJSzamla;

implementation

{$R *.dfm}

procedure TJDataModuleDlg.DataModuleCreate(Sender: TObject);
begin
	EgyebDlg.SeTADOQueryDatabase(QKozAdat);
	EgyebDlg.SeTADOQueryDatabase(QDatAdat);
	// L�trehozzuk az aloszt�lyokat?
end;

constructor TJSzamla.Create(tobj : TComponent);
begin
	Inherited Create(tobj);
	Szamlasorok			:= TObjectList.Create;
	Afasorok			:= TObjectList.Create;
end;

destructor TJSzamla.Destroy;
begin
	Inherited Destroy;
end;

function TJSzamla.FillSzamla(szkod : string; tipus : integer = 0) : boolean;
var
	JSzamlaSor	: TJSzamlaSor;
	JAfaSor		: TJAfaSor;
	i			: integer;
	afasorsz  	: integer;
	QDatAdat2	: TADOQuery;
	QueryVevo	: TADOQuery;
begin
	Result	:=false;
	vevonev	:= '';
	QDatAdat2	    := TADOQuery.Create(Self);
   QDatAdat2.Tag   := 1;
	EgyebDlg.SeTADOQueryDatabase(QDatAdat2);
	QueryVevo	    := TADOQuery.Create(Self);
   QueryVevo.Tag   := 0;
	EgyebDlg.SeTADOQueryDatabase(QueryVevo);
	Query_Run(QDatAdat2, 	'SELECT * FROM SZFEJ WHERE SA_UJKOD = '+IntToStr(StrToIntDef(szkod, -1)));
	if QDatAdat2.RecordCount < 1 then begin
		eredmenyflag	:= false;
       Exit;
	end;
	// Az �a.p�tl�k �s sped�ci�s k�d beolvas�sa
	uapotlek_kod1		:= EgyebDlg.Read_SZGrid('999','681');  // HUN
 	uapotlek_kod2		:= EgyebDlg.Read_SZGrid('999','781');  // ENG
 	uapotlek_kod3		:= EgyebDlg.Read_SZGrid('999','881');  // GER

	spedicio_kod		:= EgyebDlg.Read_SZGrid('999','682');
   afakod27            := '';
   Query_Run(QKozAdat, 'SELECT * FROM SZOTAR WHERE SZ_FOKOD = ''101'' AND SZ_MENEV LIKE ''%27%'' ');
   if QKozAdat.RecordCount = 1 then begin
       afakod27        := QKozAdat.FieldByName('SZ_ALKOD').AsString;
   end;

	azonosito       	:= StrToIntDef(QDatAdat2.FieldByName('SA_UJKOD').AsString,0);
	osszesitett			:= 0;
	szamlaszam			:= QDatAdat2.FieldByName('SA_KOD').AsString;
	datum				:= QDatAdat2.FieldByName('SA_DATUM').AsString;
	kiadas          	:= QDatAdat2.FieldByName('SA_KIDAT').AsString;
	esedekesseg     	:= QDatAdat2.FieldByName('SA_ESDAT').AsString;
	teljesites      	:= QDatAdat2.FieldByName('SA_TEDAT').AsString;
	vevokod         	:= QDatAdat2.FieldByName('SA_VEVOKOD').AsString;
	vevonev         	:= QDatAdat2.FieldByName('SA_VEVONEV').AsString;
	vevovaros       	:= QDatAdat2.FieldByName('SA_VEVAROS').AsString;
	vevoirsz        	:= QDatAdat2.FieldByName('SA_VEIRSZ').AsString;
	vevocim         	:= QDatAdat2.FieldByName('SA_VECIM').AsString;
//	vevoorszag      	:= QDatAdat.FieldByName('SA_ORSZA').AsString;
	vevoorszag := '';
  adoorszag := '';
  Query_Run(QueryVevo, 	'SELECT V_ORSZ, VE_AORSZ FROM VEVO WHERE V_KOD = '''+QDatAdat2.FieldByName('SA_VEVOKOD').AsString+''' ');
	if QueryVevo.RecordCount > 0 then begin
		vevoorszag	:= QueryVevo.FieldByName('V_ORSZ').AsString;
    adoorszag	:= QueryVevo.FieldByName('VE_AORSZ').AsString;
 	  end;
	if vevoorszag = '' then begin
		vevoorszag	:= Query_Select('VEVO', 'V_NEV', QDatAdat2.FieldByName('SA_VEVONEV').AsString, 'V_ORSZ');  // megpr�b�ljuk n�v alapj�n
	  end;

	if adoorszag = '' then begin  // ha esetleg nincs kit�ltve
		adoorszag	:=  QueryVevo.FieldByName('V_ORSZ').AsString;
	  end;

	vevoadoszam     	:= QDatAdat2.FieldByName('SA_VEADO').AsString;
	vevoEUadoszam   	:= QDatAdat2.FieldByName('SA_EUADO').AsString;
	vevobank			:= QDatAdat2.FieldByName('SA_VEBANK').AsString;
	vevokedv        	:= StringSzam(QDatAdat2.FieldByName('SA_VEKEDV').AsString);
	szamlaosszeg    	:= StringSzam(QDatAdat2.FieldByName('SA_OSSZEG').AsString);
	szamlaeuro      	:= StringSzam(QDatAdat2.FieldByName('SA_SAEUR').AsString);
	szamlaafa       	:= StringSzam(QDatAdat2.FieldByName('SA_AFA').AsString);
	fizetesimod			:= QDatAdat2.FieldByName('SA_FIMOD').AsString;
	megjegyzes			:= QDatAdat2.FieldByName('SA_MEGJ').AsString;
	allapot				:= StrToIntDef(QDatAdat2.FieldByName('SA_FLAG').AsString, 0);
	melleklet[1]		:= QDatAdat2.FieldByName('SA_MELL1').AsString;
	melleklet[2]		:= QDatAdat2.FieldByName('SA_MELL2').AsString;
	melleklet[3]		:= QDatAdat2.FieldByName('SA_MELL3').AsString;
	melleklet[4]		:= QDatAdat2.FieldByName('SA_MELL4').AsString;
	melleklet[5]		:= QDatAdat2.FieldByName('SA_MELL5').AsString;
	fejlec[1]			:= QDatAdat2.FieldByName('SA_FEJL1').AsString;
	fejlec[2]			:= QDatAdat2.FieldByName('SA_FEJL2').AsString;
	fejlec[3]			:= QDatAdat2.FieldByName('SA_FEJL3').AsString;
	fejlec[4]			:= QDatAdat2.FieldByName('SA_FEJL4').AsString;
	fejlec[5]			:= QDatAdat2.FieldByName('SA_FEJL5').AsString;
	fejlec[6]			:= QDatAdat2.FieldByName('SA_FEJL6').AsString;
	fejlec[7]			:= QDatAdat2.FieldByName('SA_FEJL7').AsString;
	nemetmegj[1]		:= QDatAdat2.FieldByName('SA_EGYA1').AsString;
	nemetmegj[2]		:= QDatAdat2.FieldByName('SA_EGYA2').AsString;
	nemetmegj[3]		:= QDatAdat2.FieldByName('SA_EGYA3').AsString;
	jaratszam  			:= QDatAdat2.FieldByName('SA_JARAT').AsString;
	rendszam  			:= QDatAdat2.FieldByName('SA_RSZ').AsString;
	pozicio         	:= QDatAdat2.FieldByName('SA_POZICIO').AsString;
	peldany				:= StrToIntDef(QDatAdat2.FieldByName('SA_PELDANY').AsString, 0);
//	tipus           	:= StrToIntDef(QDatAdat2.FieldByName('SA_TIPUS').AsString, 0);
	reszszamla      	:= StrToIntDef(QDatAdat2.FieldByName('SA_RESZE').AsString, 0);
	sorrend         	:= StrToIntDef(QDatAdat2.FieldByName('SA_SORRE').AsString, 0);	// Csak forgalmi kimutat�sban haszn�ljuk!
	helyesbito      	:= StrToIntDef(QDatAdat2.FieldByName('SA_HELYE').AsString, 0);
	fokonyv         	:= QDatAdat2.FieldByName('SA_FOKON').AsString;
	atadasflag      	:= StrToIntDef(QDatAdat2.FieldByName('SA_ATAD').AsString, 0);
	atadasdatum     	:= QDatAdat2.FieldByName('SA_ATDAT').AsString;
	atadasfinev     	:= QDatAdat2.FieldByName('SA_ATNEV').AsString;
	megrendeles			:= QDatAdat2.FieldByName('SA_MEGRE').AsString;
  szamla_nyelv        := QDatAdat2.FieldByName('SA_NYELV').AsString;
  // nincs mindenhol kit�ltve, ink�bb HUF/FCY h�nyadossal sz�moljuk
  // szamla_valuta_arfolyam:= QDatAdat2.FieldByName('SA_VALARF').AsFloat;

	// A sz�mla ad�sz�m�nak meghat�roz�sa
	if szamlaeuro > 0 then begin
		szamlaadoszam	:= vevoeuadoszam;
		if szamlaadoszam = '' then begin
			szamlaadoszam	:= vevoadoszam;
		end;
	end else begin
		szamlaadoszam	:= vevoadoszam;
		if szamlaadoszam = '' then begin
			szamlaadoszam	:= vevoeuadoszam;
		end;
	end;
	if Pos('-', szamlaadoszam ) = 0 then begin	// Nem magyar az ad�sz�m!!!
		// Orsz�gk�d besz�r�sa az ad�sz�m elej�re
		// if Pos(vevoorszag, szamlaadoszam) = 0 then begin
		//	szamlaadoszam	:= vevoorszag + szamlaadoszam;
    if Pos(adoorszag, szamlaadoszam) = 0 then begin
			szamlaadoszam	:= adoorszag + szamlaadoszam;

		end;
	end;
	// A 0.sorba betesz�nk egy �reset -> ezt adjuk vissza, ha hi�nyzik az �FA
	JAfaSor				:= TJAfaSor.Create(Self);
	JAfasor.afakod		:= '';
	JAfasor.afaszaz		:= 0;
	JAfasor.afaalap		:= 0;
	JAfasor.afaertek	:= 0;
	JAfasor.afaaleur  := 0;
	JAfasor.afaaleur2 := 0;
	JAfasor.afaerteur	:= 0;
	Afasorok.Add(JAfasor);
	ossz_netto			:= 0;
	ossz_afa			:= 0;
	ossz_brutto			:= 0;
	ossz_nettoeur		:= 0;
	ossz_nettoeur2	:= 0;
	ossz_afaeur			:= 0;
	ossz_bruttoeur		:= 0;
	uapotlekeur			:= 0;
	spedicioeur			:= 0;
	spedicioft			:= 0;

	vannaksorok			:= false;
	Query_Run(QDatAdat, 'SELECT * FROM SZSOR WHERE SS_UJKOD = '+IntToStr(StrToIntDef(szkod, -1))+' ORDER BY SS_SOR ', false);
	// A sz�mla valutanem�nek meghat�roz�sa
	szamla_valutanem := '';
	while not QDatAdat.Eof do begin
		if QDatAdat.FieldByName('SS_VALNEM').AsString <> '' then begin
			if ( ( szamla_valutanem <> QDatAdat.FieldByName('SS_VALNEM').AsString ) and (szamla_valutanem <> '') ) then begin
				// Kett� valutanem is van a sz�mlasorokhoz rendelve !!!
				szamla_valutanem	:= '';
				QDatAdat.Last;
			end else begin
				szamla_valutanem	:= QDatAdat.FieldByName('SS_VALNEM').AsString;
			end;
		end;
		QDatAdat.Next;
	end;
	QDatAdat.First;
	while not QDatAdat.Eof do begin
		JSzamlasor	:= TJSzamlaSor.Create(Self);
		JSzamlaSor.FillRowByQuery(QDatAdat, szamla_valutanem, kiadas, szamlaeuro);
		// A sz�molt �sszegek meghat�roz�sa
		ossz_netto				:= ossz_netto + JSzamlaSor.sornetto;
		// Az �zemanyag p�tl�k eld�nt�se

		// if ((uapotlek_kod <> '') and (uapotlek_kod = JSzamlasor.termekkod)) then begin  // mindh�rom nyelv� term�ket vizsg�ljuk
    if ((uapotlek_kod1 <> '') and (uapotlek_kod1 = JSzamlasor.termekkod))
     or ((uapotlek_kod2 <> '') and (uapotlek_kod2 = JSzamlasor.termekkod))
     or ((uapotlek_kod3 <> '') and (uapotlek_kod3 = JSzamlasor.termekkod)) then begin
			uapotlekeur := uapotlekeur + JSzamlasor.sorneteur;
		end;
		// A sped�ci�s d�j kezel�se
		if ((spedicio_kod <> '') and (spedicio_kod = JSzamlasor.termekkod)) then begin
			spedicioeur := spedicioeur + JSzamlasor.sorneteur;
			spedicioft	:= spedicioft + JSzamlasor.sornetto;
		end;
		// Betessz�k az �FA k�dot a t�bl�zatban
		afasorsz	:= -1;
		for i := 0 to Afasorok.Count -1 do begin
			if ( Afasorok[i] as TJAfaSor ).afakod = JSzamlasor.afakod then begin
				afasorsz	:= i;
			end;
		end;
		if afasorsz = -1 then begin
			// �j �FAsor l�trehoz�sa
			JAfaSor				:= TJAfaSor.Create(Self);
			JAfasor.afakod		:= JSzamlasor.afakod;
			JAfasor.afaszaz		:= JSzamlasor.afaszaz;
			JAfasor.afaalap		:= JSzamlaSor.sornetto;
			JAfasor.afaertek	:= JSzamlaSor.sorafa;
			JAfasor.afaaleur    := 0;
			JAfasor.afaaleur2   := 0;
			JAfasor.afaerteur	:= 0;
			afasorsz  			:= Afasorok.Add(JAfasor);
		end else begin
			// Sor l�tezik
			( Afasorok[afasorsz] as TJAfasor ).afaalap 	:= ( Afasorok[afasorsz] as TJAfasor ).afaalap  + JSzamlaSor.sornetto;
			( Afasorok[afasorsz] as TJAfasor ).afaertek := ( Afasorok[afasorsz] as TJAfasor ).afaertek + JSzamlaSor.sorafa;
		end;
		// A valut�s �FA berak�sa a megfelel� sorba
		if ( Pos(( Afasorok[afasorsz] as TJAfasor ).afakod, ',103,104,105,') > 0 ) then begin
			// Ezek a 0 �F�-sok, itt a kerek�tett �rt�k megy be
			( Afasorok[afasorsz] as TJAfasor ).afaaleur		:= ( Afasorok[afasorsz] as TJAfasor ).afaaleur  + JSzamlaSor.sorbruteur;
      ( Afasorok[afasorsz] as TJAfasor ).afaaleur2	:= ( Afasorok[afasorsz] as TJAfasor ).afaaleur2 + JSzamlaSor.sorbruteur2;
		end else begin
			( Afasorok[afasorsz] as TJAfasor ).afaaleur		:= ( Afasorok[afasorsz] as TJAfasor ).afaaleur  + JSzamlaSor.sorneteur;
 			( Afasorok[afasorsz] as TJAfasor ).afaaleur2 	:= ( Afasorok[afasorsz] as TJAfasor ).afaaleur2 + JSzamlaSor.valutaegyar;
			( Afasorok[afasorsz] as TJAfasor ).afaerteur 	:= ( Afasorok[afasorsz] as TJAfasor ).afaerteur + JSzamlaSor.sorafaeur;
		end;
		vannaksorok			:= true;
		SzamlaSorok.Add(JSzamlaSor);
		QDatAdat.Next;
	end;
	// Az �FA t�bl�zat kerek�t�se
	ossz_afa		:= 0;
	ossz_brutto		:= 0;
	ossz_nettoeur	:= 0;
	ossz_nettoeur2	:= 0;
	ossz_afaeur		:= 0;
	ossz_bruttoeur	:= 0;
	for i := 0 to Afasorok.Count - 1 do begin
		Jafasor				:= (Afasorok[i] as TJAfasor);
		Jafasor.afaalap		:= Kerekit(Jafasor.afaalap);
		Jafasor.afaertek	:= Kerekit(Jafasor.afaertek);
		Jafasor.afaaleur	:= StringSzam(Format('%.2f',[Jafasor.afaaleur]));
    Jafasor.afaaleur2 := StringSzam(Format('%.2f',[Jafasor.afaaleur2]));
		Jafasor.afaerteur	:= StringSzam(Format('%.2f',[Jafasor.afaerteur]));
		ossz_afa		:= ossz_afa 		+ Jafasor.afaertek;
		ossz_brutto		:= ossz_brutto 		+ Jafasor.afaalap + Jafasor.afaertek;
		ossz_nettoeur	:= ossz_nettoeur	+ Jafasor.afaaleur;
		ossz_nettoeur2 := ossz_nettoeur2	+ Jafasor.afaaleur2;
		ossz_afaeur		:= ossz_afaeur		+ Jafasor.afaerteur;
		ossz_bruttoeur	:= ossz_bruttoeur	+ Jafasor.afaaleur + Jafasor.afaerteur;
	end;
  // --- 2018-03-13 nem j� megold�s, "szamla_valuta_arfolyam" a sz�mlasorokb�l j�n, att�l m�g lehet a nyomtatott sz�mla devizaneme m�s.
  // ezt a mez�t a NyitoExport-ban haszn�ljuk.
  // if szamla_valutanem <> 'HUF' then
  //    szamla_valuta_arfolyam:=ossz_brutto / ossz_bruttoeur
  // else szamla_valuta_arfolyam:= 1;
  szamla_valuta_arfolyam:=ossz_brutto / ossz_bruttoeur;

	eredmenyflag	:= true;
   QDatAdat2.Destroy;
end;

function TJSzamla.FillOsszSzamla(szkod : string) : boolean;
var
	JAlszamla		: TJSzamla;
	JSzamlaSor		: TJSzamlaSor;
	JRegiSzamlaSor	: TJSzamlaSor;
	JAfaSor			: TJAfaSor;
	i				: integer;
	j				: integer;
	afasorsz  		: integer;
	sor_szama		: integer;
	QDatAdat2	    : TADOQuery;
begin
	Result	:=false;
	vevonev	:= '';
	QDatAdat2	    := TADOQuery.Create(Self);
   QDatAdat2.Tag   := 1;
	EgyebDlg.SeTADOQueryDatabase(QDatAdat2);
	if not Query_Run(QDatAdat2, 'SELECT * FROM SZFEJ2 WHERE SA_UJKOD = '+IntToStr(StrToIntDef(szkod, -1)), false) then begin
		eredmenyflag	:= false;
		Exit;
	end;
	if QDatAdat2.RecordCount < 1 then begin
		eredmenyflag	:= false;
		Exit;
	end;
	azonosito       := StrToIntDef(QDatAdat2.FieldByName('SA_UJKOD').AsString,0);
	osszesitett		:= 1;
	szamlaszam		:= QDatAdat2.FieldByName('SA_KOD').AsString;
	datum			:= QDatAdat2.FieldByName('SA_DATUM').AsString;
	kiadas          := QDatAdat2.FieldByName('SA_KIDAT').AsString;
	esedekesseg     := QDatAdat2.FieldByName('SA_ESDAT').AsString;
	teljesites      := QDatAdat2.FieldByName('SA_TEDAT').AsString;
	vevokod         := QDatAdat2.FieldByName('SA_VEVOKOD').AsString;
	vevonev         := QDatAdat2.FieldByName('SA_VEVONEV').AsString;
	vevovaros       := QDatAdat2.FieldByName('SA_VEVAROS').AsString;
	vevoirsz        := QDatAdat2.FieldByName('SA_VEIRSZ').AsString;
	vevocim         := QDatAdat2.FieldByName('SA_VECIM').AsString;

	vevoorszag		:= Query_Select('VEVO', 'V_KOD', QDatAdat2.FieldByName('SA_VEVOKOD').AsString, 'V_ORSZ');
	if vevoorszag = ''then begin
		vevoorszag		:= Query_Select('VEVO', 'V_NEV', QDatAdat2.FieldByName('SA_VEVONEV').AsString, 'V_ORSZ');
	end;

  adoorszag :=  Query_Select('VEVO', 'V_KOD', QDatAdat2.FieldByName('SA_VEVOKOD').AsString, 'VE_AORSZ');
	if adoorszag = ''then begin
		adoorszag:= Query_Select('VEVO', 'V_NEV', QDatAdat2.FieldByName('SA_VEVONEV').AsString, 'VE_AORSZ');
	  end;
	if adoorszag = ''then begin
		adoorszag:= Query_Select('VEVO', 'V_KOD', QDatAdat2.FieldByName('SA_VEVOKOD').AsString, 'V_ORSZ');
	  end;

	vevoadoszam     := '';
	vevoEUadoszam   := QDatAdat2.FieldByName('SA_VEADO').AsString;
	vevobank		:= QDatAdat2.FieldByName('SA_VEBANK').AsString;
	vevokedv        := StringSzam(QDatAdat2.FieldByName('SA_VEKEDV').AsString);
	szamlaosszeg    := StringSzam(QDatAdat2.FieldByName('SA_OSSZEG').AsString);
	szamlaeuro      := StringSzam(QDatAdat2.FieldByName('SA_SAEUR').AsString);
	szamlaafa       := StringSzam(QDatAdat2.FieldByName('SA_AFA').AsString);
	fizetesimod		:= QDatAdat2.FieldByName('SA_FIMOD').AsString;
	megjegyzes		:= QDatAdat2.FieldByName('SA_MEGJ').AsString;
	allapot			:= StrToIntDef(QDatAdat2.FieldByName('SA_FLAG').AsString, 0);
	melleklet[1]	:= QDatAdat2.FieldByName('SA_MELL1').AsString;
	melleklet[2]	:= QDatAdat2.FieldByName('SA_MELL2').AsString;
	melleklet[3]	:= QDatAdat2.FieldByName('SA_MELL3').AsString;
	melleklet[4]	:= QDatAdat2.FieldByName('SA_MELL4').AsString;
	melleklet[5]	:= QDatAdat2.FieldByName('SA_MELL5').AsString;
	fejlec[1]		:= QDatAdat2.FieldByName('SA_FEJL1').AsString;
	fejlec[2]		:= QDatAdat2.FieldByName('SA_FEJL2').AsString;
	fejlec[3]		:= QDatAdat2.FieldByName('SA_FEJL3').AsString;
	fejlec[4]		:= QDatAdat2.FieldByName('SA_FEJL4').AsString;
	fejlec[5]		:= QDatAdat2.FieldByName('SA_FEJL5').AsString;
	fejlec[6]		:= QDatAdat2.FieldByName('SA_FEJL6').AsString;
	fejlec[7]		:= QDatAdat2.FieldByName('SA_FEJL7').AsString;
	nemetmegj[1]	:= '';
	nemetmegj[2]	:= '';
	nemetmegj[3]	:= '';
	jaratszam  		:= QDatAdat2.FieldByName('SA_JARAT').AsString;
	rendszam  		:= QDatAdat2.FieldByName('SA_RSZ').AsString;
	pozicio         := QDatAdat2.FieldByName('SA_POZICIO').AsString;
	peldany			:= StrToIntDef(QDatAdat2.FieldByName('SA_PELDANY').AsString, 0);
	tipus           := StrToIntDef(QDatAdat2.FieldByName('SA_TIPUS').AsString, 0);
	reszszamla      := 0;
	sorrend         := 0;
	helyesbito      := 0;
	fokonyv         := QDatAdat2.FieldByName('SA_FOKON').AsString;
	atadasflag      := StrToIntDef(QDatAdat2.FieldByName('SA_ATAD').AsString, 0);
	atadasdatum     := '';
	atadasfinev     := '';
	megrendeles		:= '';
  szamla_nyelv    := QDatAdat2.FieldByName('SA_NYELV').AsString;
	// A sz�mla ad�sz�m�nak meghat�roz�sa
	szamlaadoszam	:= vevoeuadoszam;
	if Pos('-', szamlaadoszam ) = 0 then begin	// Nem magyar az ad�sz�m!!!
		// Orsz�gk�d besz�r�sa az ad�sz�m elej�re
		// if Pos(vevoorszag, szamlaadoszam) = 0 then begin
		//	szamlaadoszam	:= vevoorszag + szamlaadoszam;
    if Pos(adoorszag, szamlaadoszam) = 0 then begin
			szamlaadoszam	:= adoorszag + szamlaadoszam;
		end;
	end;
	// A sorok hozz�rak�sa
	Szamlasorok		:= TObjectList.Create;
	Afasorok		:= TObjectList.Create;
	// A 0.sorba betesz�nk egy �reset -> ezt adjuk vissza, ha hi�nyzik az �FA
	JAfaSor				:= TJAfaSor.Create(Self);
	JAfasor.afakod		:= '';
	JAfasor.afaszaz		:= 0;
	JAfasor.afaalap		:= 0;
	JAfasor.afaertek	:= 0;
	JAfasor.afaaleur    := 0;
	JAfasor.afaaleur2   := 0;
	JAfasor.afaerteur	:= 0;
	Afasorok.Add(JAfasor);
	ossz_netto			:= 0;
	ossz_afa			:= 0;
	ossz_brutto			:= 0;
	ossz_nettoeur		:= 0;
	ossz_nettoeur2	:= 0;
	ossz_afaeur			:= 0;
	ossz_bruttoeur		:= 0;
	sor_szama			:= 1;
	szamla_valutanem	:= 'EUR';
	spedicioeur			:= 0;
	spedicioft			:= 0;


	// Az �sszes�tett sz�mla sorainak �sszegy�jt�se
	vannaksorok			:= false;
	Query_Run(QDatAdat, 'SELECT * FROM SZSOR2 WHERE S2_S2KOD = '+IntToStr(StrToIntDef(szkod, -1))+' ORDER BY S2_S2SOR ', false);
	while not QDatAdat.Eof do begin
		// A r�sz-sz�mla l�trehoz�sa
		JAlszamla	:= TJSzamla.Create(Self);
		JAlszamla.FillSzamla(QDatAdat.FieldByName('S2_UJKOD').AsString, 1);
		spedicioeur			:= spedicioeur + JAlszamla.spedicioeur;
		spedicioft			:= spedicioft  + JAlszamla.spedicioft;

		// A sz�mlasorok bem�sol�sa az �sszes�tett sz�ml�ba
		for i := 0 to JAlszamla.Szamlasorok.Count - 1 do begin
			JSzamlasor				   := TJSzamlaSor.Create(Self);
			JRegiSzamlasor			   := (JAlSzamla.SzamlaSorok[i] as TJszamlaSor);
			JSzamlasor.fejazonosito    := StrToIntDef(szkod, -1);
			JSzamlasor.regi_ujkod	   := JRegiSzamlasor.fejazonosito;
			JSzamlasor.szamlaszam      := szamlaszam;
			JSzamlasor.sorszam         := sor_szama;
			JSzamlasor.regi_sorszam	   := JRegiSzamlasor.sorszam;
			JSzamlasor.termekkod	   := JRegiSzamlasor.termekkod;
			JSzamlasor.termeknev	   := JRegiSzamlasor.termeknev;
			JSzamlasor.cikkszam		   := JRegiSzamlasor.cikkszam;
			JSzamlasor.itjszj	  	   := JRegiSzamlasor.itjszj;
			JSzamlasor.megyseg         := JRegiSzamlasor.megyseg;
			JSzamlasor.mennyiseg       := JRegiSzamlasor.mennyiseg;
			JSzamlasor.egysegar        := JRegiSzamlasor.egysegar;
			JSzamlasor.afakod          := JRegiSzamlasor.afakod;
			JSzamlasor.afaszaz         := JRegiSzamlasor.afaszaz;
			JSzamlasor.kedvszaz        := JRegiSzamlasor.kedvszaz;
			JSzamlasor.valutanem       := JRegiSzamlasor.valutanem;
			JSzamlasor.valutaegyar     := JRegiSzamlasor.valutaegyar;
			JSzamlasor.valutaarfoly    := JRegiSzamlasor.valutaarfoly;
			JSzamlasor.leiras          := JRegiSzamlasor.leiras;
			JSzamlasor.kulfoldinev     := JRegiSzamlasor.kulfoldinev;
			JSzamlasor.kulfoldileiras  := JRegiSzamlasor.kulfoldileiras;
			JSzamlasor.kulfoldimegyseg := JRegiSzamlasor.kulfoldimegyseg;
			JSzamlasor.eurarfdatum     := JRegiSzamlasor.eurarfdatum;
			JSzamlasor.eurarfertek     := JRegiSzamlasor.eurarfertek;
			JSzamlasor.nemeus          := JRegiSzamlasor.nemeus;
			JSzamlasor.sorteljesites   := JAlszamla.teljesites;
			JSzamlasor.sormegjegyzes   := JAlszamla.megjegyzes+' '+JAlszamla.pozicio;
			// Sz�molt mez�k
			JSzamlasor.sornetto        := JRegiSzamlasor.sornetto;
			JSzamlasor.sorafa          := JRegiSzamlasor.sorafa;
			JSzamlasor.sorbrutto       := JRegiSzamlasor.sorbrutto;
			JSzamlasor.sorneteur       := JRegiSzamlasor.sorneteur;
			JSzamlasor.sorbruteur      := JRegiSzamlasor.sorbruteur;
      JSzamlasor.sorbruteur2     := JRegiSzamlasor.sorbruteur2;
			JSzamlasor.sorafaeur       := JRegiSzamlasor.sorafaeur;
			JSzamlasor.afastring       := JRegiSzamlasor.afastring;

			ossz_netto				   := ossz_netto + JRegiSzamlasor.sornetto;

			// Betessz�k az �FA k�dot a t�bl�zatban
			afasorsz	:= -1;
			for j := 0 to Afasorok.Count -1 do begin
				if ( Afasorok[j] as TJAfaSor ).afakod = JRegiSzamlasor.afakod then begin
					afasorsz	:= j;
				end;
			end;
			if afasorsz = -1 then begin
				// �j �FAsor l�trehoz�sa
				JAfaSor				:= TJAfaSor.Create(Self);
				JAfasor.afakod		:= JRegiSzamlasor.afakod;
				JAfasor.afaszaz		:= JRegiSzamlasor.afaszaz;
				JAfasor.afaalap		:= JRegiSzamlasor.sornetto;
				JAfasor.afaertek	:= JRegiSzamlasor.sorafa;
				JAfasor.afaaleur    := 0;
				JAfasor.afaaleur2    := 0;
				JAfasor.afaerteur	:= 0;
				afasorsz  			:= Afasorok.Add(JAfasor);
			end else begin
				// Sor l�tezik
				( Afasorok[afasorsz] as TJAfasor ).afaalap 	:= ( Afasorok[afasorsz] as TJAfasor ).afaalap  + JRegiSzamlasor.sornetto;
				( Afasorok[afasorsz] as TJAfasor ).afaertek := ( Afasorok[afasorsz] as TJAfasor ).afaertek + JRegiSzamlasor.sorafa;
			end;
			// A valut�s �FA berak�sa a megfelel� sorba
			if ( Pos(( Afasorok[afasorsz] as TJAfasor ).afakod, ',103,104,105,') > 0 ) then begin
				// Ezek a 0 �F�-sok, itt a kerek�tett �rt�k megy be
				( Afasorok[afasorsz] as TJAfasor ).afaaleur		:= ( Afasorok[afasorsz] as TJAfasor ).afaaleur  + JRegiSzamlasor.sorbruteur;
        ( Afasorok[afasorsz] as TJAfasor ).afaaleur2	:= ( Afasorok[afasorsz] as TJAfasor ).afaaleur2 + JRegiSzamlasor.sorbruteur2;
			end else begin
				( Afasorok[afasorsz] as TJAfasor ).afaaleur		:= ( Afasorok[afasorsz] as TJAfasor ).afaaleur  + JRegiSzamlasor.sorneteur;
				( Afasorok[afasorsz] as TJAfasor ).afaaleur2	:= ( Afasorok[afasorsz] as TJAfasor ).afaaleur2 + JRegiSzamlasor.valutaegyar;
				( Afasorok[afasorsz] as TJAfasor ).afaerteur 	:= ( Afasorok[afasorsz] as TJAfasor ).afaerteur + JRegiSzamlasor.sorafaeur;
			end;
			SzamlaSorok.Add(JSzamlaSor);
			vannaksorok			:= true;
			Inc(sor_szama);
		end;
		JAlszamla.Destroy;
		QDatAdat.Next;
	end;

	// Az �FA t�bl�zat kerek�t�se
	ossz_afa		:= 0;
	ossz_brutto		:= 0;
	ossz_nettoeur	:= 0;
	ossz_nettoeur2	:= 0;
	ossz_afaeur		:= 0;
	ossz_bruttoeur	:= 0;
	for i := 0 to Afasorok.Count - 1 do begin
		Jafasor				:= (Afasorok[i] as TJAfasor);
		Jafasor.afaalap		:= Kerekit(Jafasor.afaalap);
		Jafasor.afaertek	:= Kerekit(Jafasor.afaertek);
		Jafasor.afaaleur	:= StringSzam(Format('%.2f',[Jafasor.afaaleur]));
		Jafasor.afaaleur2	:= StringSzam(Format('%.2f',[Jafasor.afaaleur2]));
		Jafasor.afaerteur	:= StringSzam(Format('%.2f',[Jafasor.afaerteur]));
		ossz_afa		:= ossz_afa 		+ Jafasor.afaertek;
		ossz_brutto		:= ossz_brutto 		+ Jafasor.afaalap + Jafasor.afaertek;
		ossz_nettoeur	:= ossz_nettoeur	+ Jafasor.afaaleur;
		ossz_nettoeur2:= ossz_nettoeur2	+ Jafasor.afaaleur2;
		ossz_afaeur		:= ossz_afaeur		+ Jafasor.afaerteur;
		ossz_bruttoeur	:= ossz_bruttoeur	+ Jafasor.afaaleur + Jafasor.afaerteur;
	end;
  if szamla_valutanem <> 'HUF' then
      szamla_valuta_arfolyam := ossz_brutto / ossz_bruttoeur
  else szamla_valuta_arfolyam:= 1;
	eredmenyflag	:= true;
   QDatAdat2.Destroy;
end;

function TJSzamlaSor.FillRowByQuery(qe : TADOQuery; szamlavaluta, datum : string; euros : double) : boolean;
begin
	Result			:= true;
	fejazonosito 	:= StrToIntDef(qe.FieldByName('SS_UJKOD').AsString,0);
	regi_ujkod	    := StrToIntDef(qe.FieldByName('SS_UJKOD').AsString,0);
	szamlaszam      := qe.FieldByName('SS_KOD').AsString;
	sorszam			:= StrToIntDef(qe.FieldByName('SS_SOR').AsString, 0);
	termekkod		:= qe.FieldByName('SS_TERKOD').AsString;
	termeknev   	:= qe.FieldByName('SS_TERNEV').AsString;
	cikkszam     	:= qe.FieldByName('SS_CIKKSZ').AsString;
	itjszj       	:= qe.FieldByName('SS_ITJSZJ').AsString;
	megyseg      	:= qe.FieldByName('SS_MEGY').AsString;
	mennyiseg       := StringSzam(qe.FieldByName('SS_DARAB').AsString);
	egysegar        := StringSzam(qe.FieldByName('SS_EGYAR').AsString);
	afakod			:= qe.FieldByName('SS_AFAKOD').AsString;
	afaszaz         := StringSzam(qe.FieldByName('SS_AFA').AsString);
	kedvszaz        := StringSzam(qe.FieldByName('SS_KEDV').AsString);
	valutanem    	:= qe.FieldByName('SS_VALNEM').AsString;
	valutaegyar     := StringSzam(qe.FieldByName('SS_VALERT').AsString);
	valutaarfoly    := StringSzam(qe.FieldByName('SS_VALARF').AsString);
	leiras      	:= qe.FieldByName('SS_LEIRAS').AsString;
	kulfoldinev  	:= qe.FieldByName('SS_KULNEV').AsString;
	kulfoldileiras 	:= qe.FieldByName('SS_KULLEI').AsString;
	kulfoldimegyseg	:= qe.FieldByName('SS_KULME').AsString;
	eurarfdatum  	:= qe.FieldByName('SS_ARDAT').AsString;
	nemeus			:= StrToIntDef(qe.FieldByName('SS_NEMEU').AsString, 0);
	sorteljesites   := '';
	sormegjegyzes   := '';
	// A tov�bbi �rt�kek kisz�m�t�sa
	sornetto		:= Kerekit(egysegar * mennyiseg);
  // if sornetto>20000 then
  //  date;
	afastring  		:= EgyebDlg.Read_SZGrid('101', afakod);
	sorafa  		:= Kerekit(sornetto * afaszaz / 100) ;
	sorbrutto		:= sornetto + sorafa;
	eurarfertek		:= valutaarfoly;
	if 	szamlavaluta = 'HUF' then begin
		// HUF eset�n mindig EUR a valutanem, csak az �rfolyamot kell meghat�rozni
		eurarfertek     := StringSzam(qe.FieldByName('SS_ARFOL').AsString);    // Az EUR �rfolyam
		if eurarfertek <= 1 then begin
			if eurarfdatum = '' then begin
				eurarfdatum	:=  EgyebDlg.MaiDatum;
			end;
			eurarfertek := EgyebDlg.ArfolyamErtek( 'EUR', eurarfdatum, true, true); // lehessen r�gebbi is
		end;
		valutanem		:= 'EUR';
		valutaarfoly	:= eurarfertek;
		if valutaarfoly <> 0 then begin
      // NP 2015-12-17
			// valutaegyar		:= StringSzam(Format('%.2f',[sorbrutto / eurarfertek]));
      valutaegyar		:= StringSzam(Format('%.2f',[sornetto / eurarfertek]));
      // -----------------------------------------------------------
		end;
	end;
	if eurarfertek = 0 then begin
		sorafaeur	:= 0;
		sorneteur	:= 0;
		sorbruteur	:= 0;
    sorbruteur2	:= 0;
	end else begin
		sorafaeur	:= StringSzam(Format('%.2f',[sorafa / eurarfertek]));
		sorneteur	:= StringSzam(Format('%.2f',[sornetto / eurarfertek]));
		sorbruteur 	:= StringSzam(Format('%.2f',[sorneteur])) + StringSzam(Format('%.2f',[sorafaeur]));
		sorbruteur2 := StringSzam(Format('%.2f',[valutaegyar])) + StringSzam(Format('%.2f',[sorafaeur]));
	end;
   // A kulcs �tad�shoz m�dos�tani kell az eur�s sz�mla �FA sz�mol�st
   if datum >= '2016.04.05.' then begin
      if euros > 0 then begin
          if eurarfertek = 0 then begin
              sorneteur	:= 0;
          end else begin
              sorneteur	:= StringSzam(Format('%.2f',[sornetto / eurarfertek]));
          end;
         sorafaeur	:= StringSzam(Format('%.2f',[sorneteur * afaszaz / 100]));
         sorbruteur 	:= StringSzam(Format('%.2f',[sorneteur])) + StringSzam(Format('%.2f',[sorafaeur]));
         sorbruteur2  := StringSzam(Format('%.2f',[valutaegyar])) + StringSzam(Format('%.2f',[sorafaeur]));
      end;
   end;
end;

function TJSzamla.GetAfaSorszam(akod : string) : integer;
var
	i : integer;
begin
	Result	:= 0;
	for i := 0 to Afasorok.Count - 1 do begin
		if ( Afasorok[i] as TJAfaSor ).afakod = akod  then begin
			Result	:= i;
		end;
	end;
end;

function TJSzamla.WriteToList(li : TStringList) : boolean;
var
	i 				: integer;
	JSzamlaSor		: TJSzamlaSor;
	JAFASor			: TJAfaSor;
	st				: string;
begin
	li.Clear;
	li.Add('Sz�mlasz�m : '+szamlaszam);
	li.Add('------------');
	li.Add('Azonos�t� + �sszes�tett flag                        : '+IntToStr(azonosito)+'   ('+IntToStr(osszesitett)+')');
	li.Add('D�tumok (k�sz�t�s, kiad�s, teljes�t�s, esed�kess�g) : '+datum+' - '+kiadas+' - '+esedekesseg+' - '+teljesites);
	li.Add('Vev� (k�d, n�v, orsz�g, irsz, v�ros, c�m )          : '+vevokod+';'+vevonev+';'+vevoorszag+';'+vevoirsz+';'+vevovaros+';'+vevocim);
	li.Add('Vev� ad�sz�mok + banksz�mla sz�ma                   : '+vevoadoszam+' / '+vevoEUadoszam + ' - ' +vevobank );
	li.Add('Sz�mla �sszege (Ft , �FA, EUR )                     : '+Format('%f ;  %f  ; %f', [szamlaosszeg, szamlaeuro, szamlaafa]));
	li.Add('Fiz. m�d, j�ratsz�m, rendsz�m, poz�ci�, megrendel�s : '+fizetesimod+'; '+jaratszam+'; '+rendszam+'; '+pozicio+'; '+megrendeles);
	li.Add('T�pus, p�ld., �llapot, r�sz, n�met, helyesb�t�      : '+Format('%d;  %d; %d;  %d;  %d', [tipus,peldany,allapot,reszszamla,helyesbito]));
	li.Add('�sszesen + EUR �sszesen                             : '+Format('%f + %f = %f;    %f + %f = %f', [ossz_netto,ossz_afa,ossz_brutto,ossz_nettoeur,ossz_afaeur,ossz_bruttoeur]));
	li.Add('�sszes sped�ci� + EUR sped�ci� + Valutanem          : '+Format('%f ; %f ; %s', [spedicioft, spedicioeur, szamla_valutanem]));
	li.Add('�tad�si adatok (jelz�s, d�tum, �llom�ny neve )      : '+IntToStr(atadasflag)+'; '+atadasdatum +'; '+ atadasfinev);
	li.Add('F�k�nyvi sz�m + megjegyz�s                          : '+fokonyv+'; '+megjegyzes );
	li.Add('');
	li.Add('A sz�mlasorok ('+IntToStr(Szamlasorok.Count)+') :');
	// A sz�mlasorok list�z�sa
	st		:= '     ';
	for i := 0 to Szamlasorok.Count - 1 do begin
		JSzamlasor	:= SzamlaSorok[i] as TJszamlaSor;
		li.Add(st+'Sorsz�m, sz�mlasz�m, azonos�t�   : '+ IntToStr(JSzamlasor.sorszam)+';     '+JSzamlasor.szamlaszam+'; '+IntToStr(JSzamlasor.fejazonosito));
		li.Add(st+'Term�k adatok (k�d, n�v, le�r�s) : '+ JSzamlasor.termekkod+'; '+JSzamlasor.termeknev+'; '+JSzamlasor.leiras);
		li.Add(st+'T2.(cikksz.,SZJ,k�lf.n�v+le�r�s) : '+ JSzamlasor.cikkszam +'; '+JSzamlasor.itjszj+'; '+JSzamlasor.kulfoldinev+'; '+JSzamlasor.kulfoldileiras);
		li.Add(st+'Mennyis�g + menny. egys�gek      : '+ Format('%f;  %s / %s', [JSzamlasor.mennyiseg,JSzamlasor.megyseg,JSzamlasor.kulfoldimegyseg]));
		li.Add(st+'�FA k�d, sz�zal�k, sz�veg        : '+ Format('%s;  %f;  %s', [JSzamlasor.afakod,JSzamlasor.afaszaz, JSzamlasor.afastring]));
		li.Add(st+'Egys�g�r/val.egys., vnem, �rfoly.: '+ Format('%f / %f;  %s (%f)', [JSzamlasor.egysegar,JSzamlasor.valutaegyar,JSzamlasor.valutanem,JSzamlasor.valutaarfoly]));
		li.Add(st+'EUR �rfolyam d�tuma,�rt�ke(NEMEU): '+ Format('%s - %f    (%d)', [JSzamlasor.eurarfdatum, JSzamlasor.eurarfertek, JSzamlasor.nemeus]));
		li.Add(st+'�sszesen + EUR �sszesen          : '+ Format('%f + %f = %f;    %f + %f = %f', [JSzamlasor.sornetto,JSzamlasor.sorafa,JSzamlasor.sorbrutto,JSzamlasor.sorneteur,JSzamlasor.sorafaeur,JSzamlasor.sorbruteur]));
		li.Add('');
	end;

	li.Add('Az �FA sorok ('+IntToStr(AFASorok.Count)+') :');
	// Az �FA sorok list�z�sa
	st		:= '     ';
	for i := 0 to AFASorok.Count - 1 do begin
		JAFAsor	:= AFASorok[i] as TJAFASor;
		li.Add(st+'Sorsz�m, �FA k�d, �FA %      : '+ Format('%d;  %s,  %f', [i+1, JAFAsor.afakod, JAFAsor.afaszaz]));
		li.Add(st+'�FA alap, �rt�k ( EUR -ban ) : '+ Format('%f -> %f Ft ( %f -> %f EUR )', [JAFAsor.afaalap, JAFAsor.afaertek, JAFAsor.afaaleur, JAFAsor.afaerteur]));
		li.Add('');
	end;
	Result	:= true;
end;

end.
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              