unit Kutallapot;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, VclTee.TeeGDIPlus, Vcl.ExtCtrls,
  VCLTee.TeEngine, VCLTee.Series, VCLTee.TeeProcs, VCLTee.Chart, Vcl.ComCtrls,
  Data.DB, Data.Win.ADODB;

type
  TKutAllapotDlg = class(TForm)
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Chart1: TChart;
    Series1: TBarSeries;
    Chart2: TChart;
    BarSeries1: TBarSeries;
    Splitter1: TSplitter;
    PageControl2: TPageControl;
    TabSheet3: TTabSheet;
    Chart3: TChart;
    BarSeries3: TBarSeries;
    Query1: TADOQuery;
    procedure FormCreate(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
  private
    procedure ShowGraph(Melyik: integer);
  public
    procedure RefreshGraph(Tank: integer; Idoszak: string; MyChart: TChart);
    function GetJelenlegiLiter(Tank: integer): integer;
  end;

var
  KutAllapotDlg: TKutAllapotDlg;

implementation

uses  Egyeb, J_SQL, Kozos;

{$R *.dfm}

procedure TKutAllapotDlg.FormCreate(Sender: TObject);
begin
	EgyebDlg.SeTADOQueryDatabase(Query1);
  ShowGraph(2);
  ShowGraph(3);
end;

procedure TKutAllapotDlg.PageControl1Change(Sender: TObject);
begin
  if PageControl1.ActivePage = TabSheet1 then
      ShowGraph(1);
  if PageControl1.ActivePage = TabSheet2 then
      ShowGraph(2);
end;

procedure TKutAllapotDlg.ShowGraph(Melyik: integer);
begin
  Screen.Cursor:=crHourGlass;
  case Melyik of
    1: begin
        RefreshGraph(2, 'heti', Chart1);
        PageControl1.ActivePage:= TabSheet1;
        end;
    2: begin
        RefreshGraph(2, 'napi', Chart2);
        PageControl1.ActivePage:= TabSheet2;
        end;
    3: begin
        RefreshGraph(3, 'heti', Chart3);
        end;
    end;  // case
  Screen.Cursor:=crDefault;
end;

procedure TKutAllapotDlg.RefreshGraph(Tank: integer; Idoszak: string; MyChart: TChart);
// Idoszak: 'heti' vagy 'napi'
var
  S, ChartLabel: string;
begin
  S:='exec KutSelectGrafikon '+IntToStr(Tank)+', '''+Idoszak+'''';
  Query_Run(Query1, S);
  with MyChart, Query1 do begin
    Series[0].Clear;
    while not Query1.Eof do begin
      S:= FieldByName('ido').AsString;  // pl. '2017.02.13. 00:01:00'
      if Idoszak='heti' then ChartLabel:= copy(S, 6, 6);  // pl. '02.13.'
      if Idoszak='napi' then ChartLabel:= copy(S, 13, 5);  // pl. '00:01'
      Series[0].Add(Round(FieldByName('ertek').AsFloat), ChartLabel);  // egészre kerekítünk
      Next;
      end;  // while
    // if Legend.Visible then
    //  Legend.Visible:= False;
    //   Legend.Visible:= True;  // kinda redraw
    end;  // with
end;

function TKutAllapotDlg.GetJelenlegiLiter(Tank: integer): integer;
var
  S, ChartLabel: string;
begin
  S:='exec KutSelectJelenlegiLiter '+IntToStr(Tank);
  Query_Run(Query1, S);
  Result:= Round(Query1.Fields[0].AsFloat);
end;

end.
