unit TelSzamlaOlvasoPDF;


interface

uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, DBTables, DB, StdCtrls, Buttons, Mask, DBCtrls,
  ExtCtrls, ADODB, FileCtrl, StrUtils, Grids, Generics.Collections, Math,
  XMLIntf, XMLDoc, KozosTipusok;

type
  TSzamlaImportEredmeny  = record
     HibaSzoveg: string;
     SzamlaID: integer;
     end;

type
  TTelSzamlaOlvasoPDFDlg = class(TForm)
	 Memo1: TMemo;
    GridPanel1: TGridPanel;
    BtnReadfile: TBitBtn;
    BitBtn1: TBitBtn;
    BitBtn6: TBitBtn;
    Panel1: TPanel;
    Label4: TLabel;
    M1: TMaskEdit;
    BitBtn5: TBitBtn;
    SG1: TStringGrid;
    Query1: TADOQuery;
    Query2: TADOQuery;
    OpenDialog1: TOpenDialog;
    Query3: TADOQuery;
    Edit1: TEdit;
	 procedure FormCreate(Sender: TObject);
	 procedure BitBtn6Click(Sender: TObject);
	 procedure BitBtn5Click(Sender: TObject);
	 procedure BtnReadfileClick(Sender: TObject);
   function StoreSzamla(const Telenor_sorszam, KiallitasDatum, TeljesitesDatum, FizetesiHatarido, Idoszak_tol, Idoszak_ig, SzamlaSzintuInfo: string): integer;
   procedure StoreSzamlaTetel(const SzamlaID, TetelSorszam: integer; MobilSzam: string; TavkozlesiSzolgaltatas: boolean;
          TermekNev: string; Mennyiseg: string; MennyisegEgyseg: string; TermekNetto, TermekBrutto: double; AFAkulcs: integer; Idoszak_tol, Idoszak_ig: string);
   // procedure HonapEllenorzes;
   procedure SzamlaEllenorzes(const SzamlaID: integer);
	 procedure Feldolgozas(const XMLString: string);
   // function MobilnetEllenor(const MobilSzam, TermekNev: string): string;
   procedure TelSzamlaStatFrissit;
   procedure TelSzamlaOsztalyFrissit(TSID: integer);
   function XMLFeldolgozas(const XMLString: string): TSzamlaImportEredmeny;
   function GetTelefonszamlaTermekkod(const TermekNev: string): TTermekInfo;
    procedure TelefonTulajokFeltolt;
    procedure FormDestroy(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
  private
		kilepes		: boolean;
		alnev		: string;
    TelefonTulajok: TDictionary<string,string>;
  public
  end;

var
  TelSzamlaOlvasoPDFDlg: TTelSzamlaOlvasoPDFDlg;

implementation

uses
	Egyeb, Kozos, J_SQL, J_Excel;
{$R *.DFM}

procedure TTelSzamlaOlvasoPDFDlg.FormCreate(Sender: TObject);
begin
	M1.Text	:= '';
	kilepes	:= true;
	EgyebDlg.SetADOQueryDatabase(Query1);
	EgyebDlg.SetADOQueryDatabase(Query2);
	EgyebDlg.SetADOQueryDatabase(Query3);
  TelefonTulajok:=TDictionary<string,string>.Create;
  TelefonTulajokFeltolt;
end;

procedure TTelSzamlaOlvasoPDFDlg.FormDestroy(Sender: TObject);
begin
  if TelefonTulajok <> nil then TelefonTulajok.Free;
end;

procedure TTelSzamlaOlvasoPDFDlg.BitBtn6Click(Sender: TObject);
begin
	if not kilepes then begin
		kilepes	:= true;
		// BtnExport.Show;
	end else begin
		Close;
	end;
end;

procedure TTelSzamlaOlvasoPDFDlg.BitBtn1Click(Sender: TObject);
begin
	Screen.Cursor	:= crHourGlass;
	// HonapEllenorzes;
  // test and debug
  SzamlaEllenorzes(StrToInt(Edit1.Text));
	Screen.Cursor	:= crDefault;
end;

procedure TTelSzamlaOlvasoPDFDlg.SzamlaEllenorzes(const SzamlaID: integer);
var
  S, TelSzam, Kiadhato: string;
  OsszesElofizetes: TStringList;
  VoltMarUjszamla, VoltMarMegszuntszamla: boolean;
  i: integer;
begin
  VoltMarUjszamla:= False;
  VoltMarMegszuntszamla:= False;
  OsszesElofizetes:=TStringList.Create;
  try
      // --- a legut�bbit megel�z� sz�mla telefonsz�mai ---
      // S:= 'select distinct TZ_TELSZAM from TELSZAMLASZAMOK where TZ_TSID in ( '+
      //   ' select TS_ID from TELSZAMLAK where TS_IDOSZAK_TOL = '+
      //   '(select max (TS_IDOSZAK_TOL) from TELSZAMLAK where TS_IDOSZAK_TOL< '+
      //   '         (select TS_IDOSZAK_TOL from TELSZAMLAK where TS_ID = '+IntToStr(SzamlaID)+')) )'+
      //   ' and TZ_TELSZAM like ''+%'' ';
      // Query_Run(EgyebDlg.Query1, S, True);
      // while not EgyebDlg.Query1.Eof do begin
      //   OsszesElofizetes.Add(EgyebDlg.Query1.Fields[0].AsString);
      //   EgyebDlg.Query1.Next;
      //   end;  // while
      // --- a legut�bbi sz�mla nem haszn�lt telefonsz�mai ---
      S:= 'select TE_TELSZAM '+
              ' from telszamlaszamok left join TELELOFIZETES on TE_TELSZAM=TZ_TELSZAM '+
              ' where TZ_TELSZAM like ''+%'' and TZ_TSID='+IntToStr(SzamlaID)+' and TZ_DOKODLIST='''' and isnull(TE_KIADHATO,0)=1 ';
      Query_Run(EgyebDlg.Query1, S, True);
      Memo1.Lines.Add('-------------------   Ellen�rz�s   ------------------');
      while not EgyebDlg.Query1.Eof do
        with EgyebDlg.Query1 do begin
          TelSzam:= FieldByname('TE_TELSZAM').AsString;
          Query_Update(Query3, 'TELSZAMLASZAMOK',
          [
          // pedig milyen sz�p :-)...
          // 'TZ_OSZTALY', 'case when TZ_OSZTALY='''' then ''barna'' else TZ_OSZTALY+'',barna'' end'
           'TZ_OSZTALY', 'TZ_OSZTALY+''barna,'' '
          ], ' WHERE TZ_TSID='+IntToStr(SzamlaID) + ' and TZ_TELSZAM='''+TelSzam+'''');
          Memo1.Lines.Add('Kiadhat� sz�m a sz�ml�n: '+TelSzam);
        Next;
        end; // with + while
      Memo1.Lines.Add('-------------------   lista v�ge   ------------------');
  finally
    if OsszesElofizetes <> nil then OsszesElofizetes.Free;
    end; // try-finally
end;

{procedure TTelSzamlaOlvasoPDFDlg.HonapEllenorzes;
var
  S, TelSzam, Kiadhato: string;
  OsszesElofizetes: TStringList;
  VoltMarUjszamla, VoltMarMegszuntszamla: boolean;
  i: integer;
begin
  VoltMarUjszamla:= False;
  VoltMarMegszuntszamla:= False;
  OsszesElofizetes:=TStringList.Create;
  try
      // --- a legut�bbit megel�z� sz�mla telefonsz�mai ---
      S:= 'select distinct TZ_TELSZAM from TELSZAMLASZAMOK where TZ_TSID in ( '+
        ' select TS_ID from TELSZAMLAK where TS_IDOSZAK_TOL = '+
        '(select max (TS_IDOSZAK_TOL) from TELSZAMLAK where TS_IDOSZAK_TOL< (select max (TS_IDOSZAK_TOL) from TELSZAMLAK)) )'+
        ' and TZ_TELSZAM like ''+%'' ';
      Query_Run(EgyebDlg.Query1, S, True);
      while not EgyebDlg.Query1.Eof do begin
        OsszesElofizetes.Add(EgyebDlg.Query1.Fields[0].AsString);
        EgyebDlg.Query1.Next;
        end;  // while
      // --- a legut�bbi sz�mla telefonsz�mai ---
      S:= 'select distinct TZ_TELSZAM from TELSZAMLASZAMOK where TZ_TSID in ( '+
        ' select TS_ID from TELSZAMLAK where TS_IDOSZAK_TOL = '+
        ' (select max (TS_IDOSZAK_TOL) from TELSZAMLAK) )'+
        ' and TZ_TELSZAM like ''+%'' ';
      Query_Run(EgyebDlg.Query1, S, True);
      Memo1.Lines.Clear;

      while not EgyebDlg.Query1.Eof do
        with EgyebDlg.Query1 do begin
          TelSzam:= Fields[0].AsString;
          Kiadhato:= Query_Select('TELELOFIZETES', 'TE_TELSZAM', TelSzam, 'TE_KIADHATO');
          if Kiadhato = '1' then begin
              if not VoltMarUjszamla then begin
                Memo1.Lines.Add('-----  "Kiadhat�" el�fizet�sek a telefonsz�ml�n!  -----');
                VoltMarUjszamla:= True;
                end; // if
              Memo1.Lines.Add(TelSzam);
              end;  // if
          Next;
          end; // with + while

       EgyebDlg.Query1.First;  // �j loop
       while not EgyebDlg.Query1.Eof do
        with EgyebDlg.Query1 do begin
          TelSzam:= Fields[0].AsString;
          if OsszesElofizetes.IndexOf(TelSzam) = -1 then begin
             if not VoltMarUjszamla then begin
                Memo1.Lines.Add('-----  �j el�fizet�sek a telefonsz�ml�n (az el�z� h�naphoz k�pest)  -----');
                VoltMarUjszamla:= True;
                end;
             Memo1.Lines.Add(TelSzam);
             end  // if
          else begin // megtal�ltuk
            OsszesElofizetes.Delete(OsszesElofizetes.IndexOf(TelSzam));
            end;
          Next;
         end;  // with + while
      // --- megsz�nt telefonsz�mok ---
      for i:=0 to OsszesElofizetes.Count-1 do begin  // ami itt maradt, az nincs a telefonsz�ml�n
         if not VoltMarMegszuntszamla then begin
            Memo1.Lines.Add('-----  Megsz�nt el�fizet�sek (az el�z� h�naphoz k�pest)  -----');
            VoltMarMegszuntszamla:= True;
            end;
         Memo1.Lines.Add(OsszesElofizetes[i]);
        end;  // for

      Memo1.Lines.Add('-------------------   lista v�ge   ------------------');
  finally
    if OsszesElofizetes <> nil then OsszesElofizetes.Free;
    end; // try-finally
end;
}

{ // -------- el�z� koncepci� ------------- //
procedure TTelSzamlaOlvasoPDFDlg.HonapEllenorzes(Honap: string);
var
  S, TelSzam: string;
  OsszesElofizetes: TStringList;
  VoltMarUjszamla, VoltMarMegszuntszamla: boolean;
  i: integer;
begin
  VoltMarUjszamla:= False;
  VoltMarMegszuntszamla:= False;
  OsszesElofizetes:=TStringList.Create;
  try
      S:= 'select TE_TELSZAM from TELELOFIZETES where TE_STATUSKOD=1';
      Query_Run(EgyebDlg.Query1, S, True);
      while not EgyebDlg.Query1.Eof do begin
        OsszesElofizetes.Add(EgyebDlg.Query1.Fields[0].AsString);
        EgyebDlg.Query1.Next;
        end;  // while
      // --- ismeretlen (�j) telefonsz�mok ---
      S:= 'select TS_TELSZAM from TELSZAMLAK where TS_TELSZAM like ''+%'' and TS_HONAP= '''+Honap+'''';
      Query_Run(EgyebDlg.Query1, S, True);
      Memo1.Lines.Clear;
      while not EgyebDlg.Query1.Eof do
        with EgyebDlg.Query1 do begin
          TelSzam:= Fields[0].AsString;
          if OsszesElofizetes.IndexOf(TelSzam) = -1 then begin
             if not VoltMarUjszamla then begin
                Memo1.Lines.Add('-------------------  �j el�fizet�sek a telefonsz�ml�n   ------------------');
                VoltMarUjszamla:= True;
                end;
             Memo1.Lines.Add(TelSzam);
             end  // if
          else begin // megtal�ltuk
            OsszesElofizetes.Delete(OsszesElofizetes.IndexOf(TelSzam));
            end;
          Next;
         end;  // with + while
      // --- megsz�nt telefonsz�mok ---
      for i:=0 to OsszesElofizetes.Count-1 do begin  // ami itt maradt, az nincs a telefonsz�ml�n
         if not VoltMarMegszuntszamla then begin
            Memo1.Lines.Add('-------------------  Megsz�nt el�fizet�sek (nem szerepelnek a telefonsz�ml�n)   ------------------');
            VoltMarMegszuntszamla:= True;
            end;
         Memo1.Lines.Add(OsszesElofizetes[i]);
        end;  // for

      Memo1.Lines.Add('-------------------   lista v�ge   ------------------');
  finally
    if OsszesElofizetes <> nil then OsszesElofizetes.Free;
    end; // try-finally
end;
 }


procedure TTelSzamlaOlvasoPDFDlg.BitBtn5Click(Sender: TObject);
begin
	// Az �llom�ny kiv�laszt�sa
	// OpenDialog1.Filter		:= 'PDF �llom�nyok (*.pdf)';
	// OpenDialog1.DefaultExt  := 'PDF';
	OpenDialog1.Filter		:= 'XML �llom�nyok (*.xml)';
	OpenDialog1.DefaultExt  := 'XML';
	OpenDialog1.InitialDir	:= EgyebDlg.DocumentPath;
	if OpenDialog1.Execute then begin
		M1.Text := OpenDialog1.FileName;
	end;
end;

procedure TTelSzamlaOlvasoPDFDlg.BtnReadfileClick(Sender: TObject);
var
  S: string;
begin
  if (M1.Text='') then begin
     NoticeKi('�llom�ny megad�sa k�telez�!') ;
     exit;
     end;
	Screen.Cursor	:= crHourGlass;
	// BtnExport.Hide;
  S:= LoadBinaryStringFromFile(M1.Text);
  if S<>'' then begin
  	Feldolgozas(S);
    NoticeKi('A beolvas�s befejez�d�tt!');
    end
  else begin
    NoticeKi('�res bemeneti �llom�ny!');
    end;
  Screen.Cursor	:= crDefault;

//	Close;
end;

procedure TTelSzamlaOlvasoPDFDlg.Feldolgozas(const XMLString: string);
const
  XMLStreamBeginPattern = 'R/Type/EmbeddedFile>>stream'+const_LF;
  XMLStreamEndPattern = const_LF+'endstream'+const_LF;
var
  S: string;
  SzamlaImportEredmeny: TSzamlaImportEredmeny;
begin
  // XMLString:= EgyebDlg.SepRest(XMLStreamBeginPattern, PDFString);
  // XMLString:= EgyebDlg.SepFrontToken(XMLStreamEndPattern, XMLString);
 //  XMLString:= PDFString;  // ideiglenesen
  SzamlaImportEredmeny:= XMLFeldolgozas(XMLString);
  if SzamlaImportEredmeny.HibaSzoveg <> '' then begin
    NoticeKi('Hiba a feldolgoz�sban: '+SzamlaImportEredmeny.HibaSzoveg);
    end
  else begin
    TelSzamlaStatFrissit;
    TelSzamlaOsztalyFrissit (SzamlaImportEredmeny.SzamlaID);  // csak a most beolvasott sz�ml�t; ezt futtatjuk el�bb, mert a "SzamlaEllenorzes" hozz�ad a "TZ_OSZTALY" mez�h�z
    SzamlaEllenorzes (SzamlaImportEredmeny.SzamlaID);  // csak a most beolvasott sz�ml�t
    // NoticeKi('Feldolgoz�s k�sz!');
    end;
end;

procedure TTelSzamlaOlvasoPDFDlg.TelSzamlaStatFrissit;
var
  S: string;
begin
  S := 'exec TelefonszamlaStatFrissit2';
  Command_Run(EgyebDlg.SQLAlap, S, True);
end;

procedure TTelSzamlaOlvasoPDFDlg.TelSzamlaOsztalyFrissit(TSID: integer);
var
  S: string;
begin
  S := 'exec TelefonszamlaOsztalyFrissit '+IntToStr(TSID);
  Command_Run(EgyebDlg.SQLAlap, S, True);
end;

function TTelSzamlaOlvasoPDFDlg.XMLFeldolgozas(const XMLString: string): TSzamlaImportEredmeny;
const
  SzamlaElement = 'szamla';
  FejlecElement = 'fejlec';
  SzamlaInfoElement = 'szamlainfo';
  TelenorSorszamElement = 'sorszam';
  OsszesitesElement  = 'osszesites';
  VegosszegElement  = 'vegosszeg';
  NettoArOsszElement = 'nettoarossz';
  AFAOsszElement = 'afaertekossz';
  BruttoArosszElement = 'bruttoarossz';
  KiallDatumElement = 'kialldatum';
  TeljesitesDatumElement = 'teljdatum';
  FizHataridoElement = 'fizhatarido';
  IdoszakElement = 'idoszak';
  InformaciokElement = 'informaciok';
  SzamlaSzintuInfoElement = 'szamlaszintuinformacio';
  TetelekElement = 'tetelek';
  SzamlaSzintuTetelekElement = 'szamlaszintutetelek';
  KartyaSzintuTetelekElement = 'kartyaszintutetelek';
  MobilszamElement = 'mobilszam';
  TavkozlesiSzolgaltatasokElement = 'tavkozlesi_szolgaltatasok';
  MobilVasarlasElement = 'mobil_vasarlas';
  EsetidijakElement = 'esetidijak';
  HavidijakElement = 'havidijak';
  ForgalmidijakElement = 'forgalmidijak';
  HavidijkedvElement = 'havidijkedv';
  ForgalmidijkedvElement = 'forgalmidijkedv';
  TermeknevElement = 'termeknev';
  BesorszamElement = 'besorszam';
  TermekMennyisegElement = 'menny';
  TermekMennyisegEgysegElement = 'mennyegys';
  TermekNettoArElement = 'nettoar';
  TermekBruttoArElement = 'bruttoar';
  TermekAFAKulcsElement = 'afakulcs';
  MobilSzamAttribute = 'ctn';
  TetelSorszamAttribute = 'id';
var
	i: integer;
  LDocument: IXMLDocument;
  LNodeElement, LSzamlaElement, LSzamlaInfoElement, LKartyaSzintuTetelekElement, LSzamlaSzintuTetelekElement, LNode: IXMLNode;
  LMobilNode, LTetelekElement, LTetelListaNode, LAlkategoriaNode, LTetelNode, LOsszesitesElement: IXMLNode;
  LVegosszegElement, LSzamlaSzintuInfoElement: IXMLNode;
  Telenor_sorszam, Idoszak, MobilSzam, S, MennyisegEgyseg, Mennyiseg: string;
  SzamlaSzintuInfo, Idoszak_tol, Idoszak_ig, KiallitasDatum, TeljesitesDatum, FizetesiHatarido: string;
  TermekNev, Besorszam, TermekNetto, TermekBrutto: string;
  NettoOssz, AFAOssz, BruttoOssz, AfaKulcs: string;
  TetelSorszam, SzamlaID: integer;
  TavkozlesiSzolgaltatas: boolean;

  function MyGetChildText(ParentNode: IXMLNode; NodeName: string): string;
    begin
       if ParentNode.ChildNodes.FindNode(NodeName) <> nil then
           Result:= ParentNode.ChildNodes.FindNode(NodeName).Text
       else Result:='';
    end;

  {  procedure TetelekBeolvasas(LRootNode: IXMLNode; Azonosito: string);
   var
     j, k, l: integer;
   begin
    for j:= 0 to LRootNode.ChildNodes.Count - 1 do begin
       LTetelListaNode:= LRootNode.ChildNodes.Get(j);
       TavkozlesiSzolgaltatas:= (LTetelListaNode.NodeName = TavkozlesiSzolgaltatasokElement);
       for k:= 0 to LTetelListaNode.ChildNodes.Count - 1 do begin  // pl. "eseti d�jak", "havid�jak"
          LAlkategoriaNode:= LTetelListaNode.ChildNodes.Get(k);
          for l:= 0 to LAlkategoriaNode.ChildNodes.Count - 1 do begin  // pl. "eseti d�jak", "havid�jak"
             LTetelNode:= LAlkategoriaNode.ChildNodes.Get(l);
             TetelSorszam:= StrToInt(LTetelNode.Attributes[TetelSorszamAttribute]);
             TermekNev:= Trim(UTF8Decode(LTetelNode.ChildNodes.FindNode(TermeknevElement).Text));
             Mennyiseg:= MyGetChildText(LTetelNode, TermekMennyisegElement);
             MennyisegEgyseg:= UTF8Decode(MyGetChildText(LTetelNode, TermekMennyisegEgysegElement));
             TermekNetto:= LTetelNode.ChildNodes.FindNode(TermekNettoArElement).Text;
             TermekBrutto:= LTetelNode.ChildNodes.FindNode(TermekBruttoArElement).Text;
             StoreSzamlaTetel(SzamlaID, TetelSorszam, Azonosito, TavkozlesiSzolgaltatas, TermekNev,
                        MindenfeleStringToszam(Mennyiseg), MennyisegEgyseg,
                        MindenfeleStringToszam(TermekNetto), MindenfeleStringToszam(TermekBrutto));
             end;  // for l
         end;  // for k
       end;  // for j
    end;}

  procedure TetelekBeolvasas_rekurziv(LRootNode: IXMLNode; Azonosito, Idoszak_tol, Idoszak_ig: string);
   var
     i: integer;
   begin
    if LRootNode.NodeName = 'tetel' then begin
        TetelSorszam:= StrToInt(LRootNode.Attributes[TetelSorszamAttribute]);
        TermekNev:= Trim(UTF8Decode(MyGetChildText(LRootNode, TermeknevElement)));
        Besorszam:= Trim(UTF8Decode(MyGetChildText(LRootNode, BesorszamElement)));
        TermekNev:= TermekNev + ' ('+Besorszam+')';
        Mennyiseg:= Trim(MyGetChildText(LRootNode, TermekMennyisegElement));
        MennyisegEgyseg:= Trim(UTF8Decode(MyGetChildText(LRootNode, TermekMennyisegEgysegElement)));
        TermekNetto:= LRootNode.ChildNodes.FindNode(TermekNettoArElement).Text;
        AfaKulcs:= Trim(MyGetChildText(LRootNode, TermekAFAKulcsElement));
        TermekBrutto:= LRootNode.ChildNodes.FindNode(TermekBruttoArElement).Text;
        if AfaKulcs='' then begin  // nincs minden t�telben benne -> visszasz�moljuk, mi lehetett
            if Abs(MindenfeleStringToszam(TermekNetto)) > 0 then
               AfaKulcs:= IntToStr(round(MindenfeleStringToszam(TermekBrutto) / MindenfeleStringToszam(TermekNetto)*100)-100)
            else AfaKulcs:= '0';
            end;
        StoreSzamlaTetel(SzamlaID, TetelSorszam, Azonosito, TavkozlesiSzolgaltatas, TermekNev,
                 Mennyiseg, MennyisegEgyseg,
                 MindenfeleStringToszam(TermekNetto), MindenfeleStringToszam(TermekBrutto), StrToInt(AfaKulcs), Idoszak_tol, Idoszak_ig);
        end
    else begin
        for i:= 0 to LRootNode.ChildNodes.Count - 1 do begin
          TetelekBeolvasas_rekurziv(LRootNode.ChildNodes.Get(i), Azonosito, Idoszak_tol, Idoszak_ig);
          end;  // for
        end;  // else
    end;

  procedure TetelekBeolvasas_toplevel(LRootNode: IXMLNode; Azonosito, Idoszak_tol, Idoszak_ig: string);
   var
     j, k: integer;
   begin
    for j:= 0 to LRootNode.ChildNodes.Count - 1 do begin
       LTetelListaNode:= LRootNode.ChildNodes.Get(j);
       TavkozlesiSzolgaltatas:= (LTetelListaNode.NodeName = TavkozlesiSzolgaltatasokElement);
       for k:= 0 to LTetelListaNode.ChildNodes.Count - 1 do begin
          TetelekBeolvasas_rekurziv(LTetelListaNode.ChildNodes.Get(k), Azonosito, Idoszak_tol, Idoszak_ig);
         end;  // for k
       end;  // for j
    end;

begin
  Result.HibaSzoveg:= '';
  Memo1.Clear;
  LDocument := TXMLDocument.Create(nil);
  try
      LDocument.LoadFromXml(XMLString);
      LSzamlaElement := LDocument.ChildNodes.FindNode(SzamlaElement);
      LNodeElement := LSzamlaElement.ChildNodes.FindNode(FejlecElement);
      LSzamlaInfoElement := LNodeElement.ChildNodes.FindNode(SzamlaInfoElement);
      Telenor_sorszam:= trim(LSzamlaInfoElement.ChildNodes.FindNode(TelenorSorszamElement).Text);
      KiallitasDatum:= trim(LSzamlaInfoElement.ChildNodes.FindNode(KiallDatumElement).Text);
      TeljesitesDatum:= trim(LSzamlaInfoElement.ChildNodes.FindNode(TeljesitesDatumElement).Text);
      FizetesiHatarido:= trim(LSzamlaInfoElement.ChildNodes.FindNode(FizHataridoElement).Text);
      Idoszak:= LSzamlaInfoElement.ChildNodes.FindNode(IdoszakElement).Text;   // pl. "2010.10.22. - 2010.11.21."
      Idoszak_tol:= trim(EgyebDlg.SepFronttoken('-', Idoszak));
      Idoszak_ig:= trim(EgyebDlg.SepRest('-', Idoszak));
      LNodeElement:= LSzamlaInfoElement.ChildNodes.FindNode(InformaciokElement);
      LSzamlaSzintuInfoElement:= LNodeElement.ChildNodes.FindNode(SzamlaSzintuInfoElement);
      if LSzamlaSzintuInfoElement <> nil then
        SzamlaSzintuInfo:= UTF8Decode(LSzamlaSzintuInfoElement.Text)
      else SzamlaSzintuInfo:= '';
      // LOsszesitesElement := LSzamlaElement.ChildNodes.FindNode(OsszesitesElement);
      // LVegosszegElement := LOsszesitesElement.ChildNodes.FindNode(VegosszegElement);
      // NettoOssz:= LVegosszegElement.ChildNodes.FindNode(NettoArOsszElement).Text;
      // AFAOssz:= LVegosszegElement.ChildNodes.FindNode(AFAOsszElement).Text;
      // BruttoOssz:= LVegosszegElement.ChildNodes.FindNode(BruttoArosszElement).Text;
      SzamlaID:= StoreSzamla(Telenor_sorszam, KiallitasDatum, TeljesitesDatum, FizetesiHatarido, Idoszak_tol, Idoszak_ig, SzamlaSzintuInfo);
      Result.SzamlaID := SzamlaID;
          // MindenfeleStringToszam(NettoOssz), MindenfeleStringToszam(AFAOssz), MindenfeleStringToszam(BruttoOssz));
      // ------------------------  t � t e l e k  ----------------------------//
      LTetelekElement := LSzamlaElement.ChildNodes.FindNode(TetelekElement);
      LSzamlaSzintuTetelekElement:= LTetelekElement.ChildNodes.FindNode(SzamlaSzintuTetelekElement);
      TetelekBeolvasas_toplevel(LSzamlaSzintuTetelekElement, 'Sz�mla szint�', Idoszak_tol, Idoszak_ig);

      LKartyaSzintuTetelekElement:= LTetelekElement.ChildNodes.FindNode(KartyaSzintuTetelekElement);
      for i:= 0 to LKartyaSzintuTetelekElement.ChildNodes.Count - 1 do begin
        LMobilNode:= LKartyaSzintuTetelekElement.ChildNodes.Get(i);
        if LMobilNode.NodeName = MobilszamElement then begin
          MobilSzam:= LMobilNode.Attributes[MobilSzamAttribute];  // 20-954-1639
          MobilSzam:= JoTelefonszam(MobilSzam);  // --> +36209541639
          TetelekBeolvasas_toplevel(LMobilNode, Mobilszam, Idoszak_tol, Idoszak_ig);
          end;  // if
        end;  // for i
      // LDocument.Destroy;
  except
      on E: Exception do begin
        S:= 'XML feldolgoz�si hiba: '+E.Message+' - Sz�l�: '+LNodeElement.NodeName;
        Result.HibaSzoveg:= S;
        end;  // on E
      end;  // try-except
end;

function TTelSzamlaOlvasoPDFDlg.StoreSzamla(const Telenor_sorszam, KiallitasDatum, TeljesitesDatum, FizetesiHatarido, Idoszak_tol, Idoszak_ig, SzamlaSzintuInfo: string): integer;
var
  S: string;
  tsid: integer;
begin
	Query_Run(Query1, 'SELECT TS_ID FROM TELSZAMLAK WHERE TS_SORSZAM='''+Telenor_sorszam+''' ');
	if Query1.RecordCount > 0 then begin  // L�tez� rekord, friss�teni kell
    tsid:= Query1.Fields[0].AsInteger;
		Query_Update (Query2, 'TELSZAMLAK',
			[
			'TS_SORSZAM', ''''+Telenor_sorszam+'''',
			'TS_KIALLDAT', ''''+KiallitasDatum+'''',
			'TS_TELJDAT', ''''+TeljesitesDatum+'''',
			'TS_FIZHAT', ''''+FizetesiHatarido+'''',
      // 'TS_NETTOAR', SqlSzamString(NettoOssz,18,2),
      // 'TS_AFA', SqlSzamString(AFAOssz,18,2),
      // 'TS_BRUTTOAR', SqlSzamString(BruttoOssz,18,2),
      'TS_SZAMLAINFO', ''''+SzamlaSzintuInfo+''''
			], ' WHERE TS_ID='+IntToStr(tsid));
    Query_Run(Query2, 'delete from TELSZAMLATETELEK where TT_TSID = '+ IntToStr(tsid), True);
    Query_Run(Query2, 'delete from TELSZAMLASZAMOK where TZ_TSID = '+ IntToStr(tsid), True);
		Memo1.Lines.Add('L�tez� sz�mla friss�t�se ['+Idoszak_tol+' - '+Idoszak_ig+'], kor�bbi t�telek t�r�lve.');
    end
  else begin  // �j rekord l�trehoz�sa
		tsid:= Query_Insert_identity (Query2, 'TELSZAMLAK',
			[
			'TS_SORSZAM', ''''+Telenor_sorszam+'''',
      'TS_IDOSZAK_TOL', ''''+Idoszak_tol+'''',
      'TS_IDOSZAK_IG', ''''+Idoszak_ig+'''',
			'TS_KIALLDAT', ''''+KiallitasDatum+'''',
			'TS_TELJDAT', ''''+TeljesitesDatum+'''',
			'TS_FIZHAT', ''''+FizetesiHatarido+'''',
      // 'TS_NETTOAR', SqlSzamString(NettoOssz,18,2),
      // 'TS_AFA', SqlSzamString(AFAOssz,18,2),
      // 'TS_BRUTTOAR', SqlSzamString(BruttoOssz,18,2),
      'TS_SZAMLAINFO', ''''+SzamlaSzintuInfo+''''
			], 'TS_ID');
		Memo1.Lines.Add('�j sz�mla ['+Idoszak_tol+' - '+Idoszak_ig+']');
	end;
  Result:= tsid;
  // S:= KiallitasDatum+'|'+TeljesitesDatum+'|'+FizetesiHatarido+'|'+Idoszak_tol+'|'+Idoszak_ig+'|'+SzamlaSzintuInfo;
end;

procedure TTelSzamlaOlvasoPDFDlg.StoreSzamlaTetel(const SzamlaID, TetelSorszam: integer; MobilSzam: string; TavkozlesiSzolgaltatas: boolean;
          TermekNev: string; Mennyiseg: string; MennyisegEgyseg: string; TermekNetto, TermekBrutto: double; AFAkulcs: integer; Idoszak_tol, Idoszak_ig: string);
var
  S, TermekKod, GubancMegjegyzes, RegiMennyiseg, UjMennyiseg: string;
  tsid: integer;
  UjNetto, UjBrutto: double;
  TermekInfo: TTermekInfo;
  ElofizetesInfo: TElofizetesInfoMulti;
begin
  S:= 'select * from TELSZAMLASZAMOK '+
      ' where TZ_TSID='+IntToStr(SzamlaID)+' and TZ_TELSZAM='''+ MobilSzam+'''';
  Query_Run (Query3, S);
  if Query3.Eof then begin // l�trehozzuk, ha nincs
    ElofizetesInfo:= EgyebDlg.GetElofizetesInfo_Idoszak(MobilSzam, Idoszak_tol, Idoszak_ig);
    Query_Insert (Query2, 'TELSZAMLASZAMOK',
			[
      'TZ_TSID', IntToStr(SzamlaID),
      'TZ_TELSZAM', ''''+MobilSzam+'''',
      'TZ_DOKODLIST', ''''+ElofizetesInfo.dokodlista+'''',
      'TZ_DONEVLIST', ''''+ElofizetesInfo.dolgozonevlista+'''',
			'TZ_HINFO', ''''+ElofizetesInfo.megjegyzes+''''
			], True);
    end;
  // ElofizetesInfo:= EgyebDlg.GetElofizetesInfo(MobilSzam);
  TermekInfo:= GetTelefonszamlaTermekkod(TermekNev);
  TermekKod:= TermekInfo.TermekKod;
  // if TermekInfo.MobilnetTermek then
  //   GubancMegjegyzes:= MobilnetEllenor(MobilSzam, TermekNev)
  // else
  GubancMegjegyzes:= '';
  S:= 'select TT_ID, TT_MENNYISEG, TT_MENNYEGYS, TT_NETTOAR, TT_BRUTTOAR, TT_AFAKULCS '+
      ' from TELSZAMLATETELEK '+
      ' where TT_TSID='+IntToStr(SzamlaID)+' and TT_TELSZAM='''+ MobilSzam+''' and TT_TERMEKID='''+ TermekKod+'''';
  Query_Run (Query3, S);  // ellen�rizz�k hogy van-e m�r ugyanehhez a term�khez adatsor
  if not Query3.Eof then begin
    if (Query3.FieldByName('TT_MENNYEGYS').AsString <> MennyisegEgyseg) then begin
       GubancMegjegyzes:= GubancMegjegyzes+ ' T�bbf�le mennyis�gi egys�g! ('+MennyisegEgyseg+', '+Query3.FieldByName('TT_MENNYEGYS').AsString+')';
       end;
    if (Query3.FieldByName('TT_AFAKULCS').AsInteger <> AFAkulcs) then begin
       GubancMegjegyzes:= GubancMegjegyzes+ ' T�bbf�le �FA kulcs! ('+IntToStr(AFAkulcs)+', '+Query3.FieldByName('TT_AFAKULCS').AsString+')';
       end;
    RegiMennyiseg:=Query3.FieldByName('TT_MENNYISEG').AsString;
    if not ((JoSzam(RegiMennyiseg, True) and JoSzam(Mennyiseg, True))
        or (JoTelenorIdo(RegiMennyiseg, True) and JoTelenorIdo(Mennyiseg, True))) then begin
      GubancMegjegyzes:= GubancMegjegyzes+ ' Nem �sszeadhat� mennyis�gek! ('+RegiMennyiseg+', '+Mennyiseg+')';
      UjMennyiseg := RegiMennyiseg+'***';
      end
    else begin
      if (trim(RegiMennyiseg) = '') and (trim(Mennyiseg) = '') then
         UjMennyiseg := ''
      else begin
        if JoSzam(RegiMennyiseg, True) and JoSzam(Mennyiseg, True) then
            UjMennyiseg := SqlSzamString(StringSzam(RegiMennyiseg) + StringSzam(Mennyiseg), 18,2);
        if JoTelenorIdo(RegiMennyiseg, True) and JoTelenorIdo(Mennyiseg, True) then
            UjMennyiseg := TelenorIdoOsszead(RegiMennyiseg, Mennyiseg);
        end;  // else
      end;

    UjNetto:= Query3.FieldByName('TT_NETTOAR').AsFloat + TermekNetto;
    UjBrutto:= Query3.FieldByName('TT_BRUTTOAR').AsFloat + TermekBrutto;

    Query_Update (Query2, 'TELSZAMLATETELEK',
			[
      'TT_MENNYISEG', ''''+trim(UjMennyiseg)+'''',
      'TT_NETTOAR', SqlSzamString(UjNetto,18,2),
      'TT_BRUTTOAR', SqlSzamString(UjBrutto,18,2),
      'TT_MEGJE', ''''+GubancMegjegyzes+''''
			], ' WHERE TT_ID = '+IntToStr(Query3.FieldByName('TT_ID').AsInteger), True);
    S:= 'M�dos�tott sz�mlat�tel ['+IntToStr(TetelSorszam)+'. '+MobilSzam+' / '+TermekNev+', '+
        RegiMennyiseg+' -> '+UjMennyiseg+' '+MennyisegEgyseg+' = '+formatFloat('0.00', UjBrutto)+' Ft]';
		Memo1.Lines.Add(S);
    end // if not Query2.EOF
 else begin
	Query_Insert (Query2, 'TELSZAMLATETELEK',
			[
      'TT_TSID', IntToStr(SzamlaID),
      'TT_SORSZAM', IntToStr(TetelSorszam),
      'TT_TELSZAM', ''''+MobilSzam+'''',
      // 'TT_DOKOD', ''''+ElofizetesInfo.dokod+'''',
			// 'TT_HINFO', ''''+ElofizetesInfo.megjegyzes+'''',
      'TT_TAVKOZLESI', BoolToStr01(TavkozlesiSzolgaltatas),
			'TT_TERMEKID', ''''+TermekKod+'''',
			'TT_MENNYISEG', ''''+trim(Mennyiseg)+'''',  // van hogy "11:21:00" az �rt�k
			'TT_MENNYEGYS', ''''+MennyisegEgyseg+'''',
      'TT_NETTOAR', SqlSzamString(TermekNetto,18,2),
      'TT_BRUTTOAR', SqlSzamString(TermekBrutto,18,2),
      'TT_AFAKULCS', IntToStr(AFAkulcs),
      'TT_MEGJE', ''''+GubancMegjegyzes+''''
			], True);
    S:= '�j sz�mlat�tel ['+IntToStr(TetelSorszam)+'. '+MobilSzam+' / '+TermekNev+', '+
         Mennyiseg+' '+MennyisegEgyseg+' = '+formatFloat('0.00', TermekBrutto)+' Ft]';
		Memo1.Lines.Add(S);
    end;  // else
end;

{function TTelSzamlaOlvasoPDFDlg.MobilnetEllenor(const MobilSzam, TermekNev: string): string;
var
  S, MobilnetRovidites, TelenorTermekkod: string;
  tsid: integer;
begin
  S:= 'select isnull(SZ_MENEV,'''') SZ_MENEV, isnull(SZ_LEIRO,'''') SZ_LEIRO'+
      ' from TELELOFIZETES left outer join SZOTAR on TE_MOBILNETKOD = SZ_ALKOD and SZ_FOKOD=''143'' '+
      ' where TE_TELSZAM='''+MobilSzam+'''';
	Query_Run(Query1, S);
  Result:= '';
	if Query1.RecordCount > 0 then begin  // L�tezik
    MobilnetRovidites:= Query1.FieldByName('SZ_MENEV').AsString;
    TelenorTermekkod:= Query1.FieldByName('SZ_LEIRO').AsString;
    if Pos(TelenorTermekkod, TermekNev)=0 then begin
      Result:= 'Elt�r� mobilnet!! A Fuvarosban "'+MobilnetRovidites+'"';
      end;
    end;  // if
end;}

function TTelSzamlaOlvasoPDFDlg.GetTelefonszamlaTermekkod(const TermekNev: string): TTermekInfo;
var
  S, UjAlkod: string;
  tsid: integer;
begin
	Query_Run(Query1, 'SELECT SZ_ALKOD, SZ_EGYEB1 FROM SZOTAR WHERE SZ_FOKOD=''155'' AND SZ_MENEV='''+TermekNev+'''');
	if Query1.RecordCount > 0 then begin  // L�tezik
    Result.TermekKod:= Query1.Fields[0].AsString;
    Result.MobilnetTermek:= (Query1.Fields[1].AsInteger = 1);
    end  // if
  else begin
    UjAlkod:= IntToStr(GetNextStrCode('(select * from SZOTAR where SZ_FOKOD=''155'') a ', 'SZ_ALKOD'));
    Query_Insert (Query2, 'SZOTAR',
			[
			'SZ_FOKOD', ''''+'155'+'''',
			'SZ_ALKOD', ''''+UjAlkod+'''',
			'SZ_MENEV', ''''+TermekNev+'''',
      'SZ_ERVNY', ''''+'1'+''''
			], True);
    Result.TermekKod:= UjAlkod;
    Result.MobilnetTermek:= False;
    end; // else
end;

{
procedure TTelSzamlaOlvasoPDFDlg.RekordIro(honap, telszam, dokod, megjegyzes: string; vasarlas, osszesen: double);
var
	tsid: string;
begin
	// Az �j rekord beolvas�sa
	Query_Run(Query1, 'SELECT TS_ID FROM TELSZAMLAK WHERE TS_HONAP='''+honap+''' AND TS_TELSZAM = '''+telszam+''' ');
	if Query1.RecordCount > 0 then begin
		// L�tez� rekord, friss�teni kell
    tsid:= Query1.Fields[0].AsString;
		Query_Update (Query2, 'TELSZAMLAK',
			[
			'TS_VASARLAS', SqlSzamString(vasarlas,18,2),
			'TS_OSSZESEN', SqlSzamString(osszesen,18,2),
      'TS_DOKOD', ''''+dokod+'''',
      'TS_MEGJE', ''''+megjegyzes+''''
			], ' WHERE TS_ID='+tsid);
		Memo1.Lines.Add('R�gebbi adatsor friss�t�se : ['+honap+'] '+telszam+' = '
      +SzamString(vasarlas, 10, 2, true)+ ' / '+SzamString(osszesen, 10, 2, true));
	end else begin
		// �j rekord l�trehoz�sa
		Query_Insert (Query2, 'TELSZAMLAK',
			[
			'TS_TELSZAM', ''''+telszam+'''',
			'TS_HONAP', ''''+honap+'''',
			'TS_VASARLAS', SqlSzamString(vasarlas,18,2),
			'TS_OSSZESEN', SqlSzamString(osszesen,18,2),
      'TS_DOKOD', ''''+dokod+'''',
      'TS_MEGJE', ''''+megjegyzes+''''
			]);
		Memo1.Lines.Add('�j rekord besz�r�sa : ['+honap+'] '+telszam+' = '
      +SzamString(vasarlas, 10, 2, true)+ ' / '+SzamString(osszesen, 10, 2, true));
	end;
end;
     }
procedure TTelSzamlaOlvasoPDFDlg.TelefonTulajokFeltolt;
// a form�z�sok miatt nem tudjuk visszaolvasni az SMS sz�mb�l a tulajt, ez�rt k�sz�t�nk egy lookup t�bl�zatot
var
  S, KOD, Telefonszam, Tulaj: string;
begin
  // -------- dolgoz� telefonsz�mok �s email-ek  ------------
  S:= 'select do_kod, do_name from dolgozo where DO_ARHIV=0';
  Query_Run(EgyebDlg.Query1, S, True);
  with EgyebDlg.Query1 do begin
    while not Eof do begin
        KOD:= Fields[0].AsString;
        Tulaj:= Fields[1].AsString;
        Telefonszam:= EgyebDlg.GetDolgozoSMSKontakt(KOD);
        if copy(Telefonszam,1,1)='+' then begin  // telefonsz�m-szer�, de lehet hogy nem magyar
          if not(TelefonTulajok.ContainsKey (Telefonszam)) then
            TelefonTulajok.Add(Telefonszam, KOD);
          end; // if
        Next;
        end;  // while
    end;  // with

end;


end.

