unit GyorshajtStatFm;

interface

uses
	SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
	StdCtrls, Forms, DBCtrls, DB, DBGrids, DBTables, Grids, ExtCtrls, Buttons,
	ShellApi, Dialogs, J_FOFORMUJ, ADODB, Mask, Menus, Variants;

type
	TGyorshajtStatFmDlg = class(TJ_FOFORMUJDLG)
    QueryKozos: TADOQuery;
      procedure FormCreate(Sender: TObject);override;
      procedure Adatlap(cim, kod : string);override;
	 private
	 public
	end;

var
	GyorshajtStatFmDlg : TGyorshajtStatFmDlg;

implementation

uses
	Egyeb, J_SQL, Kozos, GyorshajtReszlet;

{$R *.DFM}

procedure TGyorshajtStatFmDlg.FormCreate(Sender: TObject);
var
  S: string;
begin
	Inherited FormCreate(Sender);
	kellujures	:= false;

  EgyebDlg.SetADOQueryDatabase(QueryKozos);

	AddSzuromezoRovid( 'GY_RENSZ', '(select distinct GY_RENSZ from NVC_GYORS_SZURT) a' );
	AddSzuromezoRovid( 'GY_HONAP', '(select distinct GY_HONAP from NVC_GYORS_SZURT) a' );
  AddSzuromezoRovid( 'GY_AUTIP', '(select distinct GY_AUTIP from NVC_GYORS_SZURT) a' );
  AddSzuromezoRovid( 'GK_GKKAT', '(select distinct GK_GKKAT from GEPKOCSI) a' );

  // a J_FOFORMUJ nem szereti a "group by" alapstr-eket, �gy cselezz�k ki
  S:='select * from ( ';
  // S:=S+ 'select GY_RENSZ, SUBSTRING(GY_DATUM,1,7) GY_HONAP, count(GY_RENSZ) GY_DARAB from NVC_GYORS ';
  S:=S+ 'select GY_RENSZ, GY_AUTIP, GK_GKKAT, GY_HONAP, GY_RENSZ+''^''+GY_HONAP GY_FOMEZO, count(GY_RENSZ) GY_DARAB ';
  S:=S+ 'from NVC_GYORS_SZURT JOIN GEPKOCSI ON GY_RENSZ = GK_RESZ ';
  S:=S+ ' group by GY_RENSZ, GY_AUTIP, GK_GKKAT, GY_HONAP, GY_RENSZ+''^''+GY_HONAP ';
  S:=S+ ' ) a';
	alapstr	:= S;
  // FELVISZ, MODOSIT, LISTCIM, FOMEZO, ALAPSTR, FOTABLA
	FelTolto('', '', 'Gyorshajt�s statisztika', 'GY_FOMEZO', alapstr, '__', QUERY_KOZOS_TAG );
	kulcsmezo			:= 'GY_FOMEZO';
	nevmezo				:= 'GY_FOMEZO';
	width				:= DLG_WIDTH;
	height				:= DLG_HEIGHT;
  ButtonTor.Enabled :=False;
  ButtonUj.Enabled :=False;
  ButtonMod.Enabled :=False;
  ButtonNez.Visible :=True;
  ButtonNez.Enabled :=True;
end;

procedure TGyorshajtStatFmDlg.Adatlap(cim, kod : string);
begin
	Application.CreateForm(TGyorshajtReszletDlg, AlForm);
	Inherited Adatlap(cim, kod );
end;

end.


