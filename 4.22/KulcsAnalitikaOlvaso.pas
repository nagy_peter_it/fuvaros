unit KulcsAnalitikaOlvaso;

interface

uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, DBTables, DB, StdCtrls, Buttons, Mask, DBCtrls,
  ExtCtrls, ADODB, FileCtrl, StrUtils, Grids;

type
  TKulcsAnalitikaOlvasoDlg = class(TForm)
	 BitBtn6: TBitBtn;
	 Label4: TLabel;
	 M1: TMaskEdit;
	 BitBtn5: TBitBtn;
    BtnImport: TBitBtn;
	 Query1: TADOQuery;
	 Query2: TADOQuery;
	 OpenDialog1: TOpenDialog;
	 SG1: TStringGrid;
	 Memo1: TMemo;
    KulcsQuery: TADOQuery;
    RadioButton1: TRadioButton;
    RadioButton2: TRadioButton;
    Panel1: TPanel;
    Label1: TLabel;
    Panel2: TPanel;
    QueryLong: TADOQuery;
    meEv: TMaskEdit;
	 procedure FormCreate(Sender: TObject);
	 procedure BitBtn6Click(Sender: TObject);
	 procedure BitBtn5Click(Sender: TObject);
	 procedure BtnImportClick(Sender: TObject);
   procedure KiegyenlitesTarolas(bizonylat, kiegyenlites: string);
	 procedure Feldolgozas;
    procedure RadioButton2Click(Sender: TObject);
    procedure RadioButton1Click(Sender: TObject);
    procedure RadioChanged;
  private
		kilepes		: boolean;
		alnev		: string;
    UpdateCount: integer;
  public
   function Feldolgozas_DirectDB(LogFN: string): integer;
  end;

var
  KulcsAnalitikaOlvasoDlg: TKulcsAnalitikaOlvasoDlg;

implementation

uses
	Egyeb, Kozos, J_SQL, J_Excel, LGauge;
{$R *.DFM}

procedure TKulcsAnalitikaOlvasoDlg.FormCreate(Sender: TObject);
const
  FirstKULCSYear =  2016;
var
  ThisYear, i   : integer;
begin
	M1.Text	:= '';
	kilepes	:= true;
	EgyebDlg.SetADOQueryDatabase(Query1);
	EgyebDlg.SetADOQueryDatabase(Query2);
	EgyebDlg.SetADOQueryDatabase(QueryLong);
  KulcsQuery.Connection:= EgyebDlg.ADOConnectionKulcs;
  RadioChanged;  // hogy be�ll�tsa, mi l�that�
end;

procedure TKulcsAnalitikaOlvasoDlg.BitBtn6Click(Sender: TObject);
begin
	if not kilepes then begin
		kilepes	:= true;
	end else begin
		Close;
	end;
end;

procedure TKulcsAnalitikaOlvasoDlg.BitBtn5Click(Sender: TObject);
begin
	// Az �llom�ny kiv�laszt�sa
	OpenDialog1.Filter		:= 'Excel �llom�nyok (*.xlsx)|*.XLSX|R�gebbi Excel �llom�nyok (*.xls)|*.XLS';
	OpenDialog1.DefaultExt  := 'XLSX';
	OpenDialog1.InitialDir	:= EgyebDlg.Read_SZGrid('999','756'); // KULCS analitika kapcsolat k�nyvt�ra
	if OpenDialog1.Execute then begin
		M1.Text := OpenDialog1.FileName;
	end;
end;

procedure TKulcsAnalitikaOlvasoDlg.BtnImportClick(Sender: TObject);
begin
 if RadioButton2.Checked then begin
    if (M1.Text='') then begin
       NoticeKi('�llom�ny megad�sa k�telez�!') ;
       exit;
       end;

    end;
  try
    Screen.Cursor	:= crHourGlass;
    if RadioButton2.Checked then Feldolgozas
    else Feldolgozas_DirectDB('');
   	NoticeKi('A beolvas�s befejez�d�tt!');
  except
    on E: Exception do begin
       NoticeKi('Sikertelen feldolgoz�s: '+E.Message);
       HibaKiiro('Sikertelen analitika feldolgoz�s: '+E.Message);
       end; // on E
    end;  // try-except
	Screen.Cursor	:= crDefault;
//	Close;
end;



procedure TKulcsAnalitikaOlvasoDlg.Feldolgozas;
const
  BizonylatOszlop = 6; // Excelben: 7.
  KiegyenlitesOszlop = 12; // Excelben: 13
  KintlevosegOszlop = 17; // Excelben: 18.
  BizonylatSzoveg = 'Bizonylat';
  KiegyenlitesSzoveg = 'Kiegyenl�t�s';
  KintlevosegSzoveg = 'Kintlev�s�g';
  FormatExceptionDesc = 'Az Excel t�bl�zat form�tuma megv�ltozott, k�rem �rtes�tse a Fuvaros fejleszt�ket!';
var
	EX		: TJExcel;
	i, ActSor, FeldolgozottSor, SorCount: integer;
	bizonylat, kiegyenlites, kint_s: string;
  XLBizonylatSzoveg, XLKiegyenlitesSzoveg, XLKintlevosegSzoveg: string;
  kintlevoseg: double;
  siker: boolean;
begin
	if not FileExists(M1.Text) then begin
		NoticeKi('Hi�nyz� bemeneti �llom�ny!');
		Exit;
	  end;
	EX 	:= TJexcel.Create(Self);
	if not EX.OpenXlsFile(M1.Text) then begin
		NoticeKi('Nem siker�lt megnyitnom az Excel �llom�nyt!');
    Exit;
	  end
  else begin
    EX.SetActiveSheet(1);
		if not EX.ReadFullXls(SG1) then begin
			NoticeKi('Nem siker�lt beolvasnom az Excel �llom�ny 1. lapj�t!');
      Exit;
      end;
		end; // else
	EX.Free;  // nem kell m�r
	Memo1.Clear;
	// ---- A t�bl�zat feldolgoz�sa ------
  ActSor:= 5;
  while Trim(SG1.Cells[BizonylatOszlop, ActSor])='' do begin
    Inc(ActSor);
    end; // while
  // XLBizonylatSzoveg:= Trim(SG1.Cells[BizonylatOszlop, ActSor]);
  // XLKiegyenlitesSzoveg:= Trim(SG1.Cells[KiegyenlitesOszlop, ActSor]);
  // XLKintlevosegSzoveg:= Trim(SG1.Cells[KintlevosegOszlop, ActSor]);
  if (Trim(SG1.Cells[BizonylatOszlop, ActSor]) <> BizonylatSzoveg) then
      raise Exception.Create(FormatExceptionDesc+ ' Sor: '+IntToStr(ActSor)+ ', oszlop: '+IntToStr(BizonylatOszlop));
  if (Trim(SG1.Cells[KiegyenlitesOszlop, ActSor]) <> KiegyenlitesSzoveg) then
      raise Exception.Create(FormatExceptionDesc+ ' Sor: '+IntToStr(ActSor)+ ', oszlop: '+IntToStr(KiegyenlitesOszlop));
  if (Trim(SG1.Cells[KintlevosegOszlop, ActSor]) <> KintlevosegSzoveg) then
      raise Exception.Create(FormatExceptionDesc+ ' Sor: '+IntToStr(ActSor)+ ', oszlop: '+IntToStr(KintlevosegOszlop));
  Inc(ActSor);  // most �llunk az els� adatsoron
  Application.CreateForm(TFormGaugeDlg, FormGaugeDlg);
  FormGaugeDlg.GaugeNyit('Feldolgoz�s folyamatban... ' , '', false);
  // a sok �res sor miatt sz�m�tunk egy v�rhat� rekordsz�mot azokra a sorokra, ahol van bizonylatsz�m
  SorCount:=0;
  for i := ActSor to SG1.RowCount - 1 do
      if Trim(SG1.Cells[BizonylatOszlop, i]) <> '' then Inc(SorCount);
  FeldolgozottSor:= 0;
  UpdateCount:= 0;
  try
    for i := ActSor to SG1.RowCount - 1 do begin
      FormGaugeDlg.Pozicio ( ( FeldolgozottSor * 100 ) div SorCount ) ;
      bizonylat:= Trim(SG1.Cells[BizonylatOszlop, i]);
      if bizonylat<>'' then begin  // az �res sorokat kihagyjuk
          Inc(FeldolgozottSor);
          kiegyenlites:= Trim(SG1.Cells[KiegyenlitesOszlop, i]);
          // nem kell... kiegyenlites:= kiegyenlites + '.';
          if not JoFuvarosDatum(kiegyenlites, True) then begin  // �res vagy
            raise Exception.Create(FormatExceptionDesc+ ' Sor: '+IntToStr(i)+ ', oszlop: '+IntToStr(KiegyenlitesOszlop));
            end;
          kint_s:= Trim(SG1.Cells[KintlevosegOszlop, i]);
          if not JoSzam(kint_s, False) then begin
            raise Exception.Create(FormatExceptionDesc+ ' Sor: '+IntToStr(i)+ ', oszlop: '+IntToStr(KintlevosegOszlop));
            end;
          kintlevoseg:= StringSzam(kint_s);
          if (kiegyenlites <> '') and (kintlevoseg = 0) then begin // megt�rt�nt a teljes kiegyenl�t�s
              KiegyenlitesTarolas(bizonylat, kiegyenlites);
              end;  // if
          end;  // if
      end; // for i, sorok
      Memo1.Lines.Add('---------------------------------------------------------------');
      Memo1.Lines.Add('�sszesen feldolgozva: '+IntToStr(FeldolgozottSor)+ ' sz�mla.');
      Memo1.Lines.Add('�sszesen friss�tve: '+IntToStr(UpdateCount)+ ' sz�mla.');
      Inc(UpdateCount);
  finally
    FormGaugeDlg.Destroy;
    end;  // try-finally
 end;

function TKulcsAnalitikaOlvasoDlg.Feldolgozas_DirectDB(LogFN: string): integer;
var
	i, DBsorszam, Ev, ElozoEv, UpdatedCount: integer;
  FizDatMettol: string;

  function Frissit_EgyEv(i_FizDatMettol: string; i_DBsorszam, i_Ev: integer): integer;
    var
      TablePrefix: string;
      S: string;
    begin
      // az adott �vben mind benne lesz
      TablePrefix := EgyebDlg.Read_SZGrid('382', '1') + '.';    // pl. [sql\SQL2005]
      // az 1 az utols� �v, a 2 az el�z� stb. - de lehet hogy 2018-ban nem �gy lesz
     //  TablePrefix := TablePrefix + Format(EgyebDlg.Read_SZGrid('382', 'FOKDB'), [IntToStr(i_DBsorszam), IntToStr(i_Ev)]);  // pl. [sql\SQL2005].ber32_2016_ceg_jsspeedfuvaroz1
      TablePrefix := TablePrefix + EgyebDlg.Read_SZGrid('382', 'FOKDB'+IntToStr(i_Ev));  // pl. [sql\SQL2005].ber32_2016_ceg_jsspeedfuvaroz1
      TablePrefix := TablePrefix + '.dbo.';
      S := 'EXEC KulcsKiegyenlitesFrissit ''' + TablePrefix + ''', ''' + i_FizDatMettol + ''' ';
      if LogFN <> '' then KozosWriteLogFile(LogFN, 'Frissit_EgyEv SQL: '+S);
      // Query_run(Query1, S);
      Query_run(QueryLong, S);
      if LogFN <> '' then KozosWriteLogFile(LogFN, 'Frissit_EgyEv SQL OK');
      Result:= QueryLong.Fields[0].AsInteger;
    end;

begin
  Memo1.Clear;
  // FizDatMettol:= EgyebDlg.Read_SZGrid('382', '10');
  // FizDatMettol:= DatumHozNap(FizDatMettol, -10, True); // a biztons�g kedv��rt visszaolvasunk
  // if FizDatMettol='' then FizDatMettol:='2000.01.01.';
  FizDatMettol:='2000.01.01.';  // az eg�sz �vet, nem tudjuk hogy mikor import�lj�k be a banki adatokat
  if meEv.Text <> '' then
    Ev:= StringEgesz(meEv.Text)   // a felhaszn�l� �ltal megadott �vvel fut
  else Ev:= StringEgesz(copy(EgyebDlg.MaiDatum, 1, 4));
  DBsorszam := 1;
  Memo1.Lines.Add('Adatok �thoz�sa a '+IntToStr(Ev)+'. �vi adatb�zisb�l '+ FizDatMettol+' d�tumt�l...');
  if LogFN <> '' then KozosWriteLogFile(LogFN, 'Adatok �thoz�sa a '+IntToStr(Ev)+'. �vi adatb�zisb�l '+ FizDatMettol+' d�tumt�l...');
  try
    UpdatedCount:= Frissit_EgyEv(FizDatMettol, DBsorszam, Ev);
  except
     on E: exception do begin
       if LogFN <> '' then KozosWriteLogFile(LogFN, 'Hiba ("Frissit_EgyEv"): '+E.Message);
       Memo1.Lines.Add('Hiba ("Frissit_EgyEv"): '+E.Message);
       end;  // on E
    end;  // try-except

  Memo1.Lines.Add('OK');
  if (StringEgesz(copy(EgyebDlg.MaiDatum, 6, 2)) <= 2) and (meEv.Text = '') then begin  // visszaolvasunk az el�z� �vbe is...
      ElozoEv:= Ev-1;
      DBsorszam := 2;
      FizDatMettol:=IntToStr(ElozoEv)+'.11.01.'; // ... mondjuk novembert�l
      Memo1.Lines.Add('Adatok �thoz�sa a '+IntToStr(ElozoEv)+'. �vi adatb�zisb�l '+ FizDatMettol+' d�tumt�l...');
      if LogFN <> '' then KozosWriteLogFile(LogFN, 'Adatok �thoz�sa a '+IntToStr(Ev)+'. �vi adatb�zisb�l '+ FizDatMettol+' d�tumt�l...');
      try
        UpdatedCount:= UpdatedCount + Frissit_EgyEv(FizDatMettol, DBsorszam, ElozoEv);
      except
        on E: exception do begin
          if LogFN <> '' then KozosWriteLogFile(LogFN, 'Hiba ("Frissit_EgyEv"): '+E.Message);
          Memo1.Lines.Add('Hiba ("Frissit_EgyEv"): '+E.Message);
          end;  // on E
        end;  // try-except
      Memo1.Lines.Add('OK');
      end;

  Memo1.Lines.Add('---------------------------------------------------------------');
  Memo1.Lines.Add('Feldolgoz�s k�sz!');
  Memo1.Lines.Add('�sszesen friss�tve: '+IntToStr(UpdatedCount)+ ' sz�mla.');
  Memo1.Lines.Add('---------------------------------------------------------------');
  Query_Run(Query2, 'UPDATE SZOTAR set SZ_MENEV= '''+EgyebDlg.MaiDatum+''' where SZ_FOKOD= ''382'' and SZ_ALKOD = ''10'' ', FALSE);

  Result:= UpdatedCount;
end;




{procedure TKulcsAnalitikaOlvasoDlg.Feldolgozas_DirectDB_kliensoldali;
var
	i, ActSor, FeldolgozottSor, SorCount, DBsorszam: integer;
	bizonylat, kiegyenlites, kint_s: string;
  S, FizDatMettol, TablePrefix: string;
  kintlevoseg: double;
  siker: boolean;
begin
  FizDatMettol:= EgyebDlg.Read_SZGrid('382', '10');
  if FizDatMettol='' then FizDatMettol:='2000.01.01.';  // az adott �vben mind benne lesz
  TablePrefix:= EgyebDlg.Read_SZGrid('382', '1')+'.';  // pl. [sql\SQL2005]
  DBsorszam:= cbEv.Items.Count - cbEv.ItemIndex;  // az 1 az utols� �v, a 2 az el�z� stb. - de lehet hogy 2018-ban nem �gy lesz
  TablePrefix:= TablePrefix + Format(EgyebDlg.Read_SZGrid('382', 'FOKDB'), [IntToStr(DBsorszam), cbEv.Text]); // pl. [sql\SQL2005].ber32_2016_ceg_jsspeedfuvaroz1
  TablePrefix:= TablePrefix + '.dbo.';

  EgyebDlg.ConnectKulcs;
  try
     S:= 'select T.szamlaszam Bizonylat, case when T.fizdat is not null then CONVERT(varchar(11), T.fizdat,102)+''.'' else '''' end  Kiegyenlites,	'+
        ' case when T.tkjel=''T'' then bruttoval-fizreszval else fizreszval-bruttoval end Kintlevoseg '+
        '	from (select sz.ksorszam,sz.idszam,sz.kod,sz.naplojel,sz.naploszam,sz.szamlaszam,u.ugyfelnev, u.euadoszam,  u.adoszam ,'+
        '			isnull((select top 1 bankszamlaszam from ugyfelbankszamlak as ubsz where ubsz.ugyfelkod = u.ugyfelkod and  ubsz.alapert = 1), '''') as bankszlszam ,'+
        '			isnull(sz.bankszamlakod, 0) bankszamlakod,sz.ugyfelkod, n.bizdatum, sz.fizdat, sz.eseddat,sz.fizresz,f.fizmodkod,f.fizmodnev fizmodnev ,sz.brutto,sz.tkjel ,sz.fizreszval,sz.bruttoval,sz.fizreszval as szlafizreszval,sz.valutanem,k.arfolyam as arfolyam1,'+
        '			sz.valutanem as szlavalutanem ,k.reszleg as reszkod,k.munkaszam as munkod ,k.projekt as projekt ,k.ellenmegjegyzes ,n.naplokod ,k.clearingid ,sz.teljdat ,k.torolt ,sz.kezhezdat , sz.elszzarodatum ,'+
        '			case when substring(sz.kod, 1, 1) = ''N'' then'+
        '				case when (sz.kezhezdat is not null) and (sz.kezhezdat<>0) and ((sz.naplojel=''S'') or (sz.naplojel=''V'')) and sz.ksorszam in (select distinct ksorszam from '+TablePrefix+'afanyilv where torolt is null and elszksorszam<>0) then'+
        '					sz.kezhezdat'+
        '					else sz.teljdat end'+
        '				else Bizdatum  end DATUM,'+
        '			sz.szamlaszam IdMezo , sz.szamladat kelt'+
        '			from  '+TablePrefix+'szamlak sz, '+TablePrefix+'ugyfel u, '+TablePrefix+'fizmodok f, '+TablePrefix+'karton k, '+TablePrefix+'naplo n'+
        '			where n.egyedi=k.egyedi and sz.fizmod=f.fizmodkod and sz.ugyfelkod=u.ugyfelkod AND sz.torolt is null AND  n.torolt is null and k.torolt is null'+
        '			AND k.sorszam not in (select ksorszam from '+TablePrefix+'teljesites where naplojel<>''B'' and naplojel<>''P'') AND sz.ksorszam=k.sorszam and ( k.fokszam like ''31%'' ) AND sz.naplojel=''V'' AND ( SZ.FIZMOD = ''1'' OR SZ.FIZMOD = ''2''  )'+
        '		UNION'+
        '			select t.ksorszam,t.szamlaid as idszam,t.kod,t.naplojel,t.naploszam,t.szamlaszam,u.ugyfelnev, u.euadoszam,  u.adoszam ,'+
        '			isnull((select top 1 bankszamlaszam from '+TablePrefix+'ugyfelbankszamlak as ubsz where ubsz.ugyfelkod = u.ugyfelkod and  ubsz.alapert = 1), '''') as bankszlszam ,  0 as bankszamlakod  ,t.ukod as ugyfelkod,t.fizdatum as bizdatum,'+
        '			t.fizdatum as fizdat,t.fizdatum as eseddat,t.fizossz as fizresz , cast( -1 as int ) fizmodkod  ,'+
        '				case k.naplojel when ''A'' then (select f.fizmodnev from '+TablePrefix+'fizmodok f where f.fizmodkod=t.fizmod) else  (select f.fizmodnev from '+TablePrefix+'naplo n left outer join '+TablePrefix+'fizmodok f on n.fizmod=f.fizmodkod where n.egyedi=k.egyedi) end as fizmodnev ,'+
        '				t.fizossz as brutto,k.tkjel,t.fizval as fizreszval,t.fizval as bruttoval,t.fizvalszla as szlafizreszval,t.valutanem,k.arfolyam as arfolyam1 ,'+
        '				(select xsz.valutanem from '+TablePrefix+'szamlak xsz where xsz.idszam=t.szamlaid) as szlavalutanem  ,k.reszleg as reszkod  ,k.munkaszam as munkod  ,k.projekt as projekt ,k.ellenmegjegyzes  ,n3.naplokod ,k.clearingid ,'+
        '				t.fizdatum as teljdat ,k.torolt , null as kezhezdat , null as elszzarodatum , t.fizdatum as datum,  sz.szamlaszam IdMezo , null as kelt'+
        '				from '+TablePrefix+'teljesites t left join '+TablePrefix+'szamlak sz on t.szamlaid= sz.idszam left join '+TablePrefix+'ugyfel u on t.ukod=u.ugyfelkod left join '+TablePrefix+'karton k on t.ksorszam=k.sorszam'+
        '						left join '+TablePrefix+'naplo n3 on n3.egyedi=k.egyedi left join '+TablePrefix+'karton ksz on sz.ksorszam=ksz.sorszam left join '+TablePrefix+'naplo n4 on n4.egyedi=ksz.egyedi'+
        '				where  t.kod not like ''S%'' AND  k.egyedi in (select n2.egyedi from '+TablePrefix+'naplo n2 where  ( N2.FIZMOD = ''1'' OR N2.FIZMOD = ''2''   OR N2.FIZMOD IS NULL)  ) AND  ( ( k.fokszam like ''31%'' ) or (t.kod like ''S%''))'+
        '	) T'+
       // '	where (T.fizdat is null) or (CONVERT(varchar(11), T.fizdat,102)+'.' >= '''+FizDatMettol+''')'+
       '	where CONVERT(varchar(11), T.fizdat,102)+''.'' >= '''+FizDatMettol+''''+
        '	order by fizdat ';
     S:= StringReplace(S, const_TAB, ' ', [rfReplaceAll]);
     // memo2.text:= S;

     Query_run(KulcsQuery, S);
   	 Memo1.Clear;
     ActSor:= 1;
     Application.CreateForm(TFormGaugeDlg, FormGaugeDlg);
     FormGaugeDlg.GaugeNyit('Feldolgoz�s folyamatban... ' , '', false);
     SorCount:=KulcsQuery.RecordCount;
     FeldolgozottSor:= 0;
     UpdateCount:= 0;
     while not KulcsQuery.Eof do begin
        FormGaugeDlg.Pozicio ( ( FeldolgozottSor * 100 ) div SorCount ) ;
        bizonylat:= Trim(KulcsQuery.FieldByName('Bizonylat').AsString);
        Inc(FeldolgozottSor);
        if bizonylat<>'' then begin  // az �res sorokat kihagyjuk
            kiegyenlites:= Trim(KulcsQuery.FieldByName('Kiegyenlites').AsString);
            kint_s:= Trim(KulcsQuery.FieldByName('Kintlevoseg').AsString);
            kintlevoseg:= StringSzam(kint_s);
            if (kiegyenlites <> '') and (kintlevoseg = 0) then begin // megt�rt�nt a teljes kiegyenl�t�s
               KiegyenlitesTarolas(bizonylat, kiegyenlites);
               end;  // if
            end;  // if
        KulcsQuery.Next;
        Inc(UpdateCount);
        end; // while, sorok

      Memo1.Lines.Add('---------------------------------------------------------------');
      Memo1.Lines.Add('�sszesen feldolgozva: '+IntToStr(FeldolgozottSor)+ ' sz�mla.');
      Memo1.Lines.Add('�sszesen friss�tve: '+IntToStr(UpdateCount)+ ' sz�mla.');
      Query_Run(Query2,  'UPDATE SZOTAR set SZ_MENEV= '''+EgyebDlg.MaiDatum+''' where SZ_FOKOD= ''382'' and SZ_ALKOD = ''10'' ', FALSE);

  finally
    FormGaugeDlg.Destroy;
    EgyebDlg.DisconnectKulcs;
    end;  // try-finally


end;
}

procedure TKulcsAnalitikaOlvasoDlg.KiegyenlitesTarolas(bizonylat, kiegyenlites: string);
 var
   S, tabla, kiedat: string;
 begin
  S:= 'SELECT SA_KIEDAT, ''SZFEJ'' tabla FROM SZFEJ WHERE SA_RESZE=0 and SA_KOD='''+bizonylat+'''' +
      'union '+
      'SELECT SA_KIEDAT, ''SZFEJ2'' FROM SZFEJ2 WHERE SA_KOD='''+bizonylat+'''';
	Query_Run(Query1, S);
	if Query1.RecordCount > 0 then begin  // megtal�ltuk
    kiedat:= Query1.FieldByName('SA_KIEDAT').AsString;
    tabla:= Query1.FieldByName('tabla').AsString;
    if kiedat <> kiegyenlites then begin
      Memo1.Lines.Add('Sz�mla kiegyenl�t�s friss�tve: '+bizonylat+ ' ('+kiegyenlites+')');
      Inc(UpdateCount);
      Query_Update (Query1, tabla,
			[
			'SA_KIEDAT', ''''+kiegyenlites+''''
			], ' WHERE SA_KOD='''+bizonylat+'''');
      end;  // if
    end;  // if
 end;


procedure TKulcsAnalitikaOlvasoDlg.RadioButton1Click(Sender: TObject);
begin
  RadioChanged;
end;

procedure TKulcsAnalitikaOlvasoDlg.RadioButton2Click(Sender: TObject);
begin
  RadioChanged;
end;

procedure TKulcsAnalitikaOlvasoDlg.RadioChanged;
begin
if RadioButton1.Checked then begin
  Panel1.Visible:= False;
  Panel2.Visible:= True;
  M1.Enabled:= False;
  BitBtn5.Enabled:= False;
  end  // if
else begin
  Panel1.Visible:= True;
  Panel2.Visible:= False;
  M1.Enabled:= True;
  BitBtn5.Enabled:= True;
  end  // if
end;

end.

