unit Fuviszli;

interface

uses
  WinTypes, WinProcs, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, QuickRpt, QRExpr, Qrctrls, ExtCtrls, printers, DB, DBTables, Egyeb, J_SQL,
  ADODB, jpeg,StrUtils ;

type
  TFuviszliDlg = class(TForm)
    Rep: TQuickRep;
    QRBand3: TQRBand;
    QRLabel99: TQRLabel;
    QRBand1: TQRBand;
    QRBand2: TQRBand;
    QRLabel37: TQRLabel;
    QRLabel69: TQRLabel;
	 QRLabel71: TQRLabel;
    QRLabel80: TQRLabel;
    QRShape33: TQRShape;
	 QRShape8: TQRShape;
    QRLabel1: TQRLabel;
    QRLabel20: TQRLabel;
    QRLabel21: TQRLabel;
    QRLabel22: TQRLabel;
    QRLabel15: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel9: TQRLabel;
    QRLabel10: TQRLabel;
    QRLabel16: TQRLabel;
	 QRLabel17: TQRLabel;
    QRLabel18: TQRLabel;
    QRLabel19: TQRLabel;
	 QRLabel23: TQRLabel;
    QFej1: TQRLabel;
    QFEJ2: TQRLabel;
    QFEJ3: TQRLabel;
    QFEJ4: TQRLabel;
	 QFEJ5: TQRLabel;
    QRLabel14: TQRLabel;
	 QRLabel24: TQRLabel;
    QRLabel25: TQRLabel;
    QRLabel26: TQRLabel;
    QRLabel27: TQRLabel;
	 QRLabel28: TQRLabel;
    QRLabel29: TQRLabel;
    QRLabel30: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel6: TQRLabel;
    QRLabel7: TQRLabel;
    QRLabel11: TQRLabel;
    Query1: TADOQuery;
	 Query3: TADOQuery;
    ChildBand1: TQRChildBand;
    ChildBand2: TQRChildBand;
    QLAB3: TQRLabel;
    QLAB4: TQRLabel;
    QLAB5: TQRLabel;
    QRLabel12: TQRLabel;
    QRLabel13: TQRLabel;
    QRLabel31: TQRLabel;
    QRLabel32: TQRLabel;
    QL01: TQRLabel;
    QL02: TQRLabel;
    QL03: TQRLabel;
    QL05: TQRLabel;
    QL06: TQRLabel;
    QL07: TQRLabel;
    QL08: TQRLabel;
    QL09: TQRLabel;
    QL04: TQRLabel;
    QL11: TQRLabel;
    QL12: TQRLabel;
    QL13: TQRLabel;
    QL14: TQRLabel;
    QL15: TQRLabel;
    QL16: TQRLabel;
    QL17: TQRLabel;
    QL18: TQRLabel;
    QL19: TQRLabel;
    QL21: TQRLabel;
    Query22: TADOQuery;
    QL00: TQRLabel;
    QL09A: TQRLabel;
    QRLabel34: TQRLabel;
    QL19A: TQRLabel;
    QRLabel33: TQRLabel;
    QRLabel35: TQRLabel;
    QRLabel36: TQRLabel;
    Query4: TADOQuery;
    QueryKoz: TADOQuery;
    QRBand4: TQRBand;
    QRLabel2: TQRLabel;
    QRBand5: TQRBand;
    Image11: TQRImage;
    Image1: TQRImage;
	 procedure FormCreate(Sender: TObject);
	 procedure	Tolt(szkod	: string);
	 procedure QRBand1BeforePrint(Sender: TQRCustomBand;
	   var PrintBand: Boolean);
	 procedure ChildBand2BeforePrint(Sender: TQRCustomBand;
	   var PrintBand: Boolean);
    procedure ChildBand3BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure RepBeforePrint(Sender: TCustomQuickRep;
      var PrintReport: Boolean);
    procedure CimkeBeallit(UNYELV: string);
    procedure InitCimkeArray;
  private
		kellkep	: boolean;
		sorszam	: integer;
    LabelArray: array of TQRLabel;
  public
    STORNO: boolean;
    VKOD, VPOZ, GKRENDSZ,GKKAT, FDIJ, USERNAME: string;
  end;

var
  FuviszliDlg: TFuviszliDlg;

implementation

uses Kozos, MegbizasBe;
{$R *.DFM}

procedure TFuviszliDlg.FormCreate(Sender: TObject);
begin
	EgyebDlg.SetADOQueryDatabase(Query1);
	EgyebDlg.SetADOQueryDatabase(Query22);
	EgyebDlg.SetADOQueryDatabase(Query3);
	EgyebDlg.SetADOQueryDatabase(QueryKoz);
  InitCimkeArray;
end;

procedure	TFuviszliDlg.Tolt(szkod	: string);
var
  szokod ,f1,f2,f3,f4,f5 : string;
	feleloslist, felelosnev: TStringList;
  i: integer;
  UNYELV: string;
begin
	{T�lt�s}
	if FileExists(EgyebDlg.ExePath+'FU_FEJ.BMP') then
  begin
		Image1.Picture.LoadFromFile(EgyebDlg.ExePath+'FU_FEJ.BMP');
    QRBand4.Height:= Image1.Height;
    Image1.Top:=0;
    Image1.Left:=Trunc( (QRBand4.Width-Image1.Width)/2)   ;
    QRLabel2.Enabled:=False;
	end
  else
  begin
    QRLabel2.Enabled:=True;
  end;

	if FileExists(EgyebDlg.ExePath+'FU_LAB.BMP') then
  begin
		Image11.Picture.LoadFromFile(EgyebDlg.ExePath+'FU_LAB.BMP');
    QRBand5.Enabled:=True;
	end
  else
  begin
    QRBand5.Enabled:=False;
  end;

{
	if FileExists(EgyebDlg.ExePath+'FULOGO.BMP') then begin
		Image1.Picture.LoadFromFile(EgyebDlg.ExePath+'FULOGO.BMP');
	end;
 }
	felelosnev := TStringList.Create;
	feleloslist := TStringList.Create;
	Query_Run( QueryKoz, 'SELECT * FROM SZOTAR WHERE SZ_FOKOD = ''330'' AND SZ_ALKOD LIKE ''%0'' AND SZ_ERVNY > 0 ', true );
	while not QueryKoz.Eof do begin
		feleloslist.Add(QueryKoz.FieldByName('SZ_ALKOD').AsString);
		felelosnev.Add(QueryKoz.FieldByName('SZ_MENEV').AsString);
		QueryKoz.Next;
	end;

{	Query_Run(Query22, 'SELECT DISTINCT  MS_TEFNEV, MS_TEFTEL, MS_TEFL1, MS_TEFL2, MS_TEFL3, MS_TEFL4, '+
		' MS_TEFOR, MS_TEFIR, MS_TENOR, MS_TENIR, MS_FEPAL, MS_LEPAL, MS_FSULY, MS_LSULY, '+
		' MS_TEFL5, MS_TEFCIM, MS_FELIDO, MS_FELARU, MS_TENNEV, MS_TENTEL, MS_TENYL1, MS_TENYL2, MS_TENYL3, MS_TENYL4, '+
		' MS_TENYL5, MS_FELDAT, MS_LERDAT, MS_LERIDO, MS_TENCIM, MS_LDARU,MS_FDARU '+
		' FROM MEGSEGED WHERE MS_MBKOD = '''+szkod+''' '+
		' AND MS_FAJKO = 0 ' + 	// csak a norm�l szakaszok jelenjenek itt meg
		' ORDER BY MS_SORSZ' );
 }


	Query_Run(Query22, 'SELECT * FROM MEGSEGED WHERE MS_MBKOD = '''+szkod+''' '+
		' AND MS_FAJKO = 0 ' + 	// csak a norm�l szakaszok jelenjenek itt meg
		' ORDER BY MS_SORSZ' );

     // ---------------- duplik�tumok sz�r�s�re ----------------------------
      // Nem ilyen egyszer�. �nmag�val kellene join-olni a MEGSEGED-et, hogy az MS_TIPUS= 0: felrak�s  1: lerak�s sorok
      // egy adatsorba, �s �gy egy Band-re ker�ljenek.
  {
	Query_Run(Query22, 'SELECT distinct MS_SORSZ, MS_MBKOD, MS_TIPUS, MS_TEFNEV, MS_TEFOR, MS_TEFIR, MS_TEFTEL, MS_TEFCIM, MS_TEFL1, '+
	    ' MS_TEFL2, MS_TEFL3, MS_TEFL4, MS_TEFL5, MS_FELIDO2, MS_FELDAT, MS_FELIDO, MS_FEPAL, MS_FSULY, MS_CSPAL, MS_FCPAL, MS_FSULY, '+
	    ' MS_TENNEV, MS_TENOR, MS_TENIR, MS_TENTEL, MS_TENCIM, MS_TENYL1, MS_TENYL2, MS_TENYL3, MS_TENYL4, MS_TENYL5, MS_LERIDO2, MS_LERDAT, '+
      ' MS_LERIDO, MS_LEPAL, MS_LSULY, MS_CSPAL, MS_FELARU FROM MEGSEGED WHERE MS_MBKOD = '''+szkod+''' '+
  		' AND MS_FAJKO = 0 ' + 	// csak a norm�l szakaszok jelenjenek itt meg
	  	' ORDER BY MS_SORSZ' );
   }
  //  ----------------------------------------------------------------------

	Query_Run(Query4,'SELECT * FROM VEVO WHERE V_KOD = '''+VKOD+''' ', true);
	if Query4.RecordCount > 0 then begin
		QrLabel2.Caption	:= EgyebDlg.Read_SZGrid('CEG', '101');
		QrLabel23.Caption	:= Query4.FieldByName('V_NEV').AsString;
		QrLabel18.Caption	:= Query4.FieldByName('V_IRSZ').AsString+' '+Query4.FieldByName('V_VAROS').AsString+' '+Query4.FieldByName('V_CIM').AsString;
		QrLabel16.Caption	:= Query4.FieldByName('V_TEL').AsString;
		QrLabel9.Caption	:= Query4.FieldByName('V_FAX').AsString;
    UNYELV:= Query4.FieldByName('VE_UNYELV').AsString;       // �gyint�z�si nyelv
    if UNYELV='' then Query4.FieldByName('VE_NYELV').AsString;  // a sz�ml�z�si nyelv
    if UNYELV='' then UNYELV:='HUN';  // ha valami�rt egyiket sem tal�lja, akkor HUF
    CimkeBeallit(UNYELV);  // magyar vagy idegen nyelv� feliratok be�ll�t�sa
 //		QRLabel30.Caption	:= VPOZ;

		QrLabel24.Caption	:= GKRENDSZ;
		QrLabel26.Caption	:= GKKAT;
		QrLabel28.Caption	:= FDIJ;
    QRLabel80.Caption := VPOZ;
{	 	QrLabel4.Caption	:= Query1.FieldByName('AJ_FUMEG').AsString;
		QrLabel30.Caption	:= Query1.FieldByName('AJ_MEMO01').AsString;
		QrLabel5.Caption	:= Query1.FieldByName('AJ_MEMO02').AsString;
		QrLabel6.Caption	:= Query1.FieldByName('AJ_MEMO03').AsString;
		QrLabel7.Caption	:= Query1.FieldByName('AJ_MEMO04').AsString;
		QrLabel11.Caption	:= Query1.FieldByName('AJ_MEMO05').AsString;
    }
		QrLabel30.Caption	:= EgyebDlg.Read_SZGrid('375', '1' );
		QrLabel5.Caption	:= EgyebDlg.Read_SZGrid('375', '2' );
		QrLabel6.Caption	:= EgyebDlg.Read_SZGrid('375', '3' );
		QrLabel7.Caption	:= EgyebDlg.Read_SZGrid('375', '4' );
		QrLabel11.Caption	:= EgyebDlg.Read_SZGrid('375', '5' );
		{Az oldal �sszeh�z�sa, ha vannak �res sorok}
		if QrLabel11.Caption = '' then begin
			QrBand2.Height		:= QrLabel11.Top;
			QRLabel11.Enabled	:= false;
			if QrLabel7.Caption = '' then begin
				QrBand2.Height		:= QrLabel7.Top;
				QRLabel7.Enabled	:= false;
				if QrLabel6.Caption = '' then begin
					QrBand2.Height		:= QrLabel6.Top;
					QRLabel6.Enabled	:= false;
				end;
			end;
		end;

	// Bet�ltj�k a fejl�c �s l�bl�c elemeket

	i:=felelosnev.IndexOf(USERNAME);
  if i> -1 then //van
  begin
  	szokod	:= feleloslist[i];
	  f1		:= EgyebDlg.Read_SZGrid('330', szokod + 'F1' );
    f2		:= EgyebDlg.Read_SZGrid('330', szokod + 'F2' );
	  f3		:= EgyebDlg.Read_SZGrid('330', szokod + 'F3' );
    f4		:= EgyebDlg.Read_SZGrid('330', szokod + 'F4' );
    f5		:= EgyebDlg.Read_SZGrid('330', szokod + 'F5' );
		QFej1.Caption	:= EgyebDlg.Read_SZGrid('330', szokod + 'F1' );
		QFej2.Caption	:= EgyebDlg.Read_SZGrid('330', szokod + 'F2' );
		QFej3.Caption	:= EgyebDlg.Read_SZGrid('330', szokod + 'F3' );
		QFej4.Caption	:= EgyebDlg.Read_SZGrid('330', szokod + 'F4' );
		QFej5.Caption	:= EgyebDlg.Read_SZGrid('330', szokod + 'F5' );
  end
  else
  begin
  	f1	:= '';
  	f2	:= EgyebDlg.Read_SZGrid('999', '692' );
  	f3	:= EgyebDlg.Read_SZGrid('999', '693' );
  	f4	:= EgyebDlg.Read_SZGrid('999', '691' );
  	f5	:= '';
		QFej1.Caption	:= f1;
		QFej2.Caption	:= f2;
		QFej3.Caption	:= f3;
		QFej4.Caption	:= f4;
		QFej5.Caption	:= f5;
  end;
//  QLab1.Caption  	:= EgyebDlg.Read_SZGrid('330', szokod + 'L1' );
//  QLab2.Caption  	:= EgyebDlg.Read_SZGrid('330', szokod + 'L2' );
//  QLab1.Caption  	:= '';
//  QLab2.Caption  	:= '';

// QLab3.Caption  	:= EgyebDlg.Read_SZGrid('330', szokod + 'L3' )+copy(EgyebDlg.MaiDatum,5,6);
  QLab3.Caption  	:= EgyebDlg.Read_SZGrid('330', UNYELV + szokod + 'L3' )+' '+EgyebDlg.MaiDatum;  // ki kell venni az �vsz�mot a sz�t�relemb�l
  QLab4.Caption  	:= EgyebDlg.Read_SZGrid('330', UNYELV + szokod + 'L4' );
  QLab5.Caption  	:= EgyebDlg.Read_SZGrid('330', UNYELV + szokod + 'L5' );

	end;
end;

procedure TFuviszliDlg.QRBand1BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
	kulonbseg	: integer;
  	szorzo		: integer;
begin
	// Az �rt�kek felt�lt�se
	QL00.Caption  	:= IntToStr(sorszam);
	Inc(sorszam);
	QL01.Caption  	:= Query22.FieldByName('MS_TEFNEV').AsString;
	QL02.Caption  	:= copy ( Query22.FieldByName('MS_TEFOR').AsString+'-'+Query22.FieldByName('MS_TEFIR').AsString+' '+Query22.FieldByName('MS_TEFTEL').AsString, 1, 30 );
	QL03.Caption  	:= Query22.FieldByName('MS_TEFCIM').AsString;
	QL04.Caption  	:= Query22.FieldByName('MS_TEFL1').AsString;
	QL05.Caption  	:= Query22.FieldByName('MS_TEFL2').AsString;
	QL06.Caption  	:= Query22.FieldByName('MS_TEFL3').AsString;
	QL07.Caption  	:= Query22.FieldByName('MS_TEFL4').AsString;
	QL08.Caption  	:= Query22.FieldByName('MS_TEFL5').AsString;
  if Query22.FieldByName('MS_FELIDO2').AsString<>'' then
  	QL09.Caption  	:= Query22.FieldByName('MS_FELDAT').AsString+' '+Query22.FieldByName('MS_FELIDO').AsString+'-'+Query22.FieldByName('MS_FELIDO2').AsString
  else
  	QL09.Caption  	:= Query22.FieldByName('MS_FELDAT').AsString+' '+Query22.FieldByName('MS_FELIDO').AsString;
//QL09A.Caption  	:= Query22.FieldByName('MS_FEPAL').AsString+' pal.  '+Query22.FieldByName('MS_FSULY').AsString+' kg';
	QL09A.Caption  	:= Query22.FieldByName('MS_FEPAL').AsString+IfThen(Query22.FieldByName('MS_CSPAL').AsInteger=1,' ('+Query22.FieldByName('MS_FCPAL').AsString+'db CSERE) pal.  ',' pal.  ')+Query22.FieldByName('MS_FSULY').AsString+' kg';
	QL11.Caption  	:= Query22.FieldByName('MS_TENNEV').AsString;
	QL12.Caption  	:= copy ( Query22.FieldByName('MS_TENOR').AsString+'-'+Query22.FieldByName('MS_TENIR').AsString+' '+Query22.FieldByName('MS_TENTEL').AsString, 1, 30 );
	QL13.Caption  	:= Query22.FieldByName('MS_TENCIM').AsString;
	QL14.Caption  	:= Query22.FieldByName('MS_TENYL1').AsString;
	QL15.Caption  	:= Query22.FieldByName('MS_TENYL2').AsString;
	QL16.Caption  	:= Query22.FieldByName('MS_TENYL3').AsString;
	QL17.Caption  	:= Query22.FieldByName('MS_TENYL4').AsString;
	QL18.Caption  	:= Query22.FieldByName('MS_TENYL5').AsString;
  if Query22.FieldByName('MS_LERIDO2').AsString<>'' then
  	QL19.Caption  	:= Query22.FieldByName('MS_LERDAT').AsString+' '+Query22.FieldByName('MS_LERIDO').AsString+'-'+Query22.FieldByName('MS_LERIDO2').AsString
  else
  	QL19.Caption  	:= Query22.FieldByName('MS_LERDAT').AsString+' '+Query22.FieldByName('MS_LERIDO').AsString;
//QL19A.Caption  	:= Query22.FieldByName('MS_LEPAL').AsString+' pal.  '+Query22.FieldByName('MS_LSULY').AsString+' kg';
	QL19A.Caption  	:= Query22.FieldByName('MS_LEPAL').AsString+IfThen(Query22.FieldByName('MS_CSPAL').AsInteger=1,' ('+Query22.FieldByName('MS_FCPAL').AsString+'db CSERE) pal.  ',' pal.  ')+Query22.FieldByName('MS_LSULY').AsString+' kg';
	QL21.Caption  	:= Query22.FieldByName('MS_FELARU').AsString;

	Query_Run(Query3,'SELECT MS_DARU FROM MEGSEGED WHERE MS_MBKOD ='+Query22.FieldByName('MS_MBKOD').AsString+' and MS_SORSZ='+Query22.FieldByName('MS_SORSZ').AsString+' and MS_TIPUS=0', true);
  QRLabel36.Enabled:=(Query3.FieldByName('MS_DARU').AsInteger=1);

	Query_Run(Query3,'SELECT MS_DARU FROM MEGSEGED WHERE MS_MBKOD ='+Query22.FieldByName('MS_MBKOD').AsString+' and MS_SORSZ='+Query22.FieldByName('MS_SORSZ').AsString+' and MS_TIPUS=1', true);
  QRLabel35.Enabled:=(Query3.FieldByName('MS_DARU').AsInteger=1);

  //QRLabel35.Enabled:=(Query22.FieldByName('MS_LDARU').Value=1);
  //QRLabel36.Enabled:=(Query22.FieldByName('MS_FDARU').Value=1);

	// Az �res sorok kiiktat�sa
	kulonbseg		:= QL02.Top - QL01.Top;
	szorzo			:= 0;
	if  ( ( Query22.FieldByname('MS_TEFL5').AsString = '' ) and
		( Query22.FieldByname('MS_TENYL5').AsString = '' ) ) then begin
		szorzo	:= 1;
		if  ( ( Query22.FieldByname('MS_TEFL4').AsString = '' ) and
			( Query22.FieldByname('MS_TENYL4').AsString = '' ) ) then begin
			szorzo	:= 2;
			if  ( ( Query22.FieldByname('MS_TEFL3').AsString = '' ) and
				( Query22.FieldByname('MS_TENYL3').AsString = '' ) ) then begin
				szorzo	:= 3;
				if  ( ( Query22.FieldByname('MS_TEFL2').AsString = '' ) and
					( Query22.FieldByname('MS_TENYL2').AsString = '' ) ) then begin
					szorzo	:= 4;
					if ( 	( Query22.FieldByname('MS_TEFL1').AsString = '' ) and
						( Query22.FieldByname('MS_TENYL1').AsString = '' ) ) then begin
						szorzo	:= 5;
					end;
				end;
			end;
		end;
	end;

	QRLabel1.Top	:= QL01.Top + (8-szorzo) * kulonbseg;
	QrLabel22.Top	:= QL01.Top + (8-szorzo) * kulonbseg;
	QL09.Top		:= QRLabel1.Top;
	QL19.Top		:= QRLabel1.Top;

	QrLabel34.Top	:= QL01.Top + (9-szorzo) * kulonbseg;
	QL09A.Top		:= QrLabel34.Top;
  QrLabel33.Top	:= QrLabel34.Top;
  QrLabel35.Top	:= QrLabel22.Top;
  QrLabel36.Top	:= QrLabel22.Top;
	QL19A.Top		:= QrLabel34.Top;

	QrLabel20.Top	:= QL01.Top + (10-szorzo) * kulonbseg;
	QL21.Top		:= QrLabel20.Top;
	QrShape33.Top	:= QL01.Top + (11-szorzo) * kulonbseg;
	QRBand1.Height 	:= QrShape33.Top + 5;
end;

procedure TFuviszliDlg.ChildBand2BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin

 //		PrintBand	:= false;
 //		Exit;
  {
	// Ide j�n a fizet�si felt�telek be�r�sa, ha van!!!
	PrintBand	:= true;
	if ( Query1.FieldByName('AJ_FIZU1').AsString + Query1.FieldByName('AJ_FIZU2').AsString +
		 Query1.FieldByName('AJ_FIZU3').AsString = '' ) then begin
		PrintBand	:= false;
		Exit;
	end;
	// Az �rt�kek ki�r�sa
	QrLabel13.Caption	:= Query1.FieldByName('AJ_FIZU1').AsString +
		Format('%.0f', [StringSzam(Query1.FieldByName('AJ_FUDIJ').AsString)*StringSzam(Query1.FieldByName('AJ_FISZ1').AsString) / 100])	+
		' ' + Query1.FieldByName('AJ_FUVAL').AsString;
	QrLabel31.Caption	:= Query1.FieldByName('AJ_FIZU2').AsString +
		Format('%.0f', [StringSzam(Query1.FieldByName('AJ_FUDIJ').AsString)*StringSzam(Query1.FieldByName('AJ_FISZ2').AsString) / 100])	+
		' ' + Query1.FieldByName('AJ_FUVAL').AsString;
	QrLabel32.Caption	:= Query1.FieldByName('AJ_FIZU3').AsString +
		Format('%.0f', [StringSzam(Query1.FieldByName('AJ_FUDIJ').AsString)*StringSzam(Query1.FieldByName('AJ_FISZ3').AsString) / 100])	+
		' ' + Query1.FieldByName('AJ_FUVAL').AsString;
	// A band magass�g be�ll�t�sa
	if Query1.FieldByName('AJ_FIZU3').AsString = '' then begin
		ChildBand1.Height	:= QrLabel32.Top; //ChildBand1.Height - QrLabel32.Height;
		QrLabel32.Caption	:= '';
		QrLabel32.Enabled	:= false;
		if Query1.FieldByName('AJ_FIZU2').AsString = '' then begin
			ChildBand1.Height	:= QrLabel31.Top; //ChildBand1.Height - QrLabel32.Height;
			QrLabel31.Caption	:= '';
			QrLabel31.Enabled	:= false;
		end;
	end; }
end;

procedure TFuviszliDlg.ChildBand3BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
	PrintBand	:= kellkep;
end;

procedure TFuviszliDlg.RepBeforePrint(Sender: TCustomQuickRep;
  var PrintReport: Boolean);
begin
	sorszam	:= 1;
end;

procedure TFuviszliDlg.InitCimkeArray;
const
   hossz = 100;
var
   i: integer;
begin
   // jobb ha ford�t�si id�ben kider�l, ha valamelyik nem l�tezik.
   SetLength(LabelArray, hossz);
   LabelArray[1]:=QRLabel1;
   LabelArray[8]:=QRLabel8;
   LabelArray[10]:=QRLabel10;
   LabelArray[12]:=QRLabel12;
   LabelArray[14]:=QRLabel14;
   LabelArray[15]:=QRLabel15;
   LabelArray[17]:=QRLabel17;
   LabelArray[19]:=QRLabel19;
   LabelArray[20]:=QRLabel20;
   LabelArray[21]:=QRLabel21;
   LabelArray[22]:=QRLabel22;
   LabelArray[25]:=QRLabel25;
   LabelArray[27]:=QRLabel27;
   LabelArray[29]:=QRLabel29;
   LabelArray[33]:=QRLabel33;
   LabelArray[34]:=QRLabel34;
   LabelArray[35]:=QRLabel35;
   LabelArray[36]:=QRLabel36;
   LabelArray[37]:=QRLabel37;
   LabelArray[99]:=QRLabel99;
end;

procedure TFuviszliDlg.CimkeBeallit(UNYELV: string);
var
  S, ID, Cimke: string;
  IDI: integer;
begin
  S:= 'SELECT SZ_ALKOD, SZ_MENEV FROM SZOTAR WHERE SZ_FOKOD = ''374'' ';
  S:=S+ ' AND substring(SZ_ALKOD,1,3)='''+UNYELV+'''';
  Query_Run(Query3, S, true);
  with Query3 do begin
    while not Eof do begin
      ID:= FieldByName('SZ_ALKOD').AsString;
      if (length(ID)>=4) and (copy(ID, 4,1)<>'M') then begin  // a "mail" param�terekkel se foglalkozzunk
        ID:= copy(ID, 4, length(ID)-3);  // 'HUN15' --> '15'
        Cimke:= FieldByName('SZ_MENEV').AsString;
        if JoEgesz(ID) then begin
          IDI:= StrToInt(ID);
          if LabelArray[IDI]<> nil then
             LabelArray[IDI].Caption:= Cimke;
          end; // if JoEgesz
        end;  // if Length OK
      Next;
      end;  // while
    end;  // with
end;

end.
