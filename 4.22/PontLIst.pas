unit PontLIst;

interface

uses
  WinTypes, WinProcs, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, QuickRpt, QRExpr, Qrctrls, ExtCtrls, printers, DB, DBTables, Grids, IniFiles,
  ADODB ;

type
  TPontListDlg = class(TForm)
    Rep: TQuickRep;
    QRBand3: TQRBand;
	 QRLSzamla: TQRLabel;
	 QRBand1: TQRBand;
    QRLabel6: TQRLabel;
    QRSysData1: TQRSysData;
    QRLabel7: TQRLabel;
    QRSysData4: TQRSysData;
    QRLabel1: TQRLabel;
    Q3: TQRLabel;
    Q2: TQRLabel;
    Q4: TQRLabel;
    QRLabel21: TQRLabel;
    Q1: TQRLabel;
    ArfolyamGrid: TStringGrid;
    Osszesito: TStringGrid;
    QRLabel13: TQRLabel;
    QRLabel17: TQRLabel;
    Query1: TADOQuery;
    QRGroup1: TQRGroup;
    QRLabel4: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel15: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel3: TQRLabel;
    QRLabel18: TQRLabel;
    QRShape1: TQRShape;
    Query2: TADOQuery;
    QRBand2: TQRBand;
    QRLabel2: TQRLabel;
    QRLabel9: TQRLabel;
    Query3: TADOQuery;
    QRBand4: TQRBand;
    QRLabel10: TQRLabel;
    QRLabel11: TQRLabel;
    procedure FormCreate(Sender: TObject);
    procedure	Tolt(dat1: string; reszletes, csoportos : boolean );
    procedure QRBand1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRGroup1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBand2BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure RepBeforePrint(Sender: TCustomQuickRep;
      var PrintReport: Boolean);
    procedure QRBand4BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
  private
    	sorszam			: integer;
     	reszlet			: boolean;
       kellcsop        : boolean;
       datum           : string;
  public
     vanadat			: boolean;
  end;

var
  PontListDlg: TPontListDlg;

implementation

uses
	J_SQL, Egyeb, KOzos, Calend1;

{$R *.DFM}

procedure TPontListDlg.FormCreate(Sender: TObject);
begin
	EgyebDlg.SetADOQueryDatabase(Query1);
	EgyebDlg.SetADOQueryDatabase(Query2);
	EgyebDlg.SetADOQueryDatabase(Query3);
  	sorszam		:= 1;
   reszlet     := false;
   kellcsop    := false;
end;

procedure	TPontListDlg.Tolt(dat1 : string; reszletes, csoportos : boolean );
begin
   QrLabel21.Caption	:= copy(dat1, 1, 5) + ' ' + FormatSettings.LongMonthNames[StrToIntDef(copy(dat1, 6,2), 1)] ;
   datum               := dat1;
	{A kapott sql alapján végrehajtja a lekérdezést}
   Query_Run(Query1, 'SELECT * FROM DOLGOZOCSOP ORDER BY DC_CSNEV, DC_DONEV ');
   kellcsop            := csoportos;
   if kellcsop then begin
       Query_Run(Query1, 'SELECT DC_CSKOD, DC_CSNEV, PP_DOKOD AS DOKOD, AVG(PP_UPONT) AS ATLAG  FROM PONTOK,  DOLGOZOCSOP WHERE DC_DOKOD = PP_DOKOD AND PP_DATUM = '''+dat1+''' GROUP BY DC_CSKOD, DC_CSNEV, PP_DOKOD '+
           ' UNION SELECT DC_CSKOD, DC_CSNEV, DC_DOKOD AS DOKOD,''0'' AS ATLAG  FROM DOLGOZOCSOP WHERE DC_DOKOD NOT IN (SELECT PP_DOKOD FROM PONTOK WHERE PP_DATUM = '''+dat1+''' ) ORDER BY DC_CSNEV, ATLAG ' );
       QrGroup1.Expression := 'Query1.DC_CSKOD';
       QrGroup1.Visible    := true;
       QrBand2.Visible    := true;
   end else begin
       Query_Run(Query1, 'SELECT DC_CSKOD, DC_CSNEV, PP_DOKOD AS DOKOD, AVG(PP_UPONT) AS ATLAG  FROM PONTOK,  DOLGOZOCSOP WHERE DC_DOKOD = PP_DOKOD AND PP_DATUM = '''+dat1+''' GROUP BY DC_CSKOD, DC_CSNEV, PP_DOKOD '+
           ' UNION SELECT DC_CSKOD, DC_CSNEV, DC_DOKOD AS DOKOD,''0'' AS ATLAG  FROM DOLGOZOCSOP WHERE DC_DOKOD NOT IN (SELECT PP_DOKOD FROM PONTOK WHERE PP_DATUM = '''+dat1+''' ) ORDER BY ATLAG ' );
       QrGroup1.Expression := '';
       QrGroup1.Visible    := false;
       QRBand2.Visible    := true;
   end;
   Query_Run(Query2, 'SELECT DO_KOD, DO_NAME FROM DOLGOZO');
   vanadat := Query1.RecordCount > 0;
   reszlet := reszletes;
end;

procedure TPontListDlg.QRBand1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
var
   atlag   : double;
begin
  	Q1.Caption 	:= IntToStr(sorszam);
   Inc(sorszam);
  	Q2.Caption 	:= Query1.FieldByName('DOKOD').AsString;
//  	Q3.Caption 	:= Query_Select('DOLGOZO', 'DO_KOD', Query1.FieldByName('DOKOD').AsString, 'DO_NAME');
  	Q3.Caption 	:= '';
   if Query2.Locate('DO_KOD', Query1.FieldByName('DOKOD').AsString, []) then begin
  	    Q3.Caption 	:= Query2.FieldByName('DO_NAME').AsString;
   end;
   // Az átlag számolása
   atlag       := StringSzam(Query1.FieldByName('ATLAG').AsString);
  	Q4.Caption	:= Format('%.2f', [atlag]);
   PrintBand   := true;
   if ( ( atlag = 0 ) and not reszlet ) then begin
       PrintBand   := false;
       Dec(sorszam);
   end;
end;

procedure TPontListDlg.QRGroup1BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
   QrLabel5.Caption    := Query1.FieldByName('DC_CSNEV').AsString;
   PrintBand   := true;
   if not kellcsop then begin
       PrintBand   := false;
   end;
end;

procedure TPontListDlg.QRBand2BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
   csatlag : double;
begin
   Query_Run(Query3, 'SELECT AVG(PP_UPONT) AS ATLAG  FROM PONTOK WHERE PP_DATUM = '''+datum+
       ''' AND PP_ALKOD = '''+Query1.FieldByName('DC_CSKOD').AsString+''' ');
   csatlag             := StringSzam(Query3.FieldByName('ATLAG').AsString);;
   QrLabel9.Caption    :=  Format('%.2f', [csatlag]);
   PrintBand   := true;
   if not kellcsop then begin
       PrintBand   := false;
   end;
end;

procedure TPontListDlg.RepBeforePrint(Sender: TCustomQuickRep;
  var PrintReport: Boolean);
begin
   sorszam := 1;
end;

procedure TPontListDlg.QRBand4BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
   csatlag : double;
begin
   Query_Run(Query3, 'SELECT AVG(PP_UPONT) AS ATLAG  FROM PONTOK WHERE PP_DATUM = '''+datum+''' ');
   csatlag             := StringSzam(Query3.FieldByName('ATLAG').AsString);;
   QrLabel11.Caption   :=  Format('%.2f', [csatlag]);
end;

end.
