unit CegadatBe;

interface

uses
	SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
	Forms, Dialogs, StdCtrls, Buttons, Mask, DB, DBTables, J_ALFORM, ADODB;

type
	TCegadatbeDlg = class(TJ_AlformDlg)
	BitElkuld: TBitBtn;
	BitKilep: TBitBtn;
	Label2: TLabel;
    M1: TMaskEdit;
	Label8: TLabel;
    M2: TMaskEdit;
    Label6: TLabel;
    M3: TMaskEdit;
    Query1: TADOQuery;
	procedure BitKilepClick(Sender: TObject);
	procedure FormCreate(Sender: TObject);
	procedure Tolto(cim : string; teko : string); override;
	procedure BitElkuldClick(Sender: TObject);
  procedure MaskEditClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
	private
	public
	end;

var
	CegadatbeDlg: TCegadatbeDlg;

implementation

uses
	J_VALASZTO, Egyeb, J_SQL, Kozos;
{$R *.DFM}

procedure TCegadatbeDlg.BitKilepClick(Sender: TObject);
begin
	Close;
end;

procedure TCegadatbeDlg.FormCreate(Sender: TObject);
begin
  EgyebDlg.SetADOQueryDatabase(Query1);
	ret_kod 	:= '';
end;

procedure TCegadatbeDlg.Tolto(cim : string; teko:string);
begin
	ret_kod 		:= teko;
	Caption 		:= cim;
	M1.Text 		:= teko;
	M2.Text 		:= '';
	M3.Text 		:= '';
	if ret_kod <> '' then begin		{M�dos�t�s}
     if Query_Run (Query1, 'SELECT * FROM CEGADAT WHERE S_ALKOD = '''+teko+''' ',true) then begin
			M2.Text 			:= Query1.FieldByName('S_NEV').AsString;
			M3.Text 			:= Query1.FieldByName('S_MEGJ').AsString;
			SetMaskEdits([M1]);
		end;
  end;
end;

procedure TCegadatbeDlg.BitElkuldClick(Sender: TObject);
begin
	if M1.Text = '' then begin
		NoticeKi('K�d megad�sa k�telez�!');
		M1.Setfocus;
     Exit;
  end;
	if ret_kod = '' then begin
  	{�j rekord felvitele}
     if Query_Run (Query1, 'SELECT * FROM CEGADAT WHERE S_ALKOD = '''+M1.Text+''' ',true) then begin
        if Query1.RecordCount > 0 then begin
				NoticeKi('L�tez� k�d!');
				M1.Setfocus;
     		Exit;
        end;
     end;
     Query_Run ( Query1, 'INSERT INTO CEGADAT (S_ALKOD) VALUES (''' +M1.Text+ ''' )',true);
     ret_kod := M1.Text;
  end;
 	{A r�gi rekord m�dos�t�sa}
  Query_Update (Query1, 'CEGADAT',
    	[
     'S_NEV', 	''''+M2.Text+'''',
     'S_MEGJ', 	''''+M3.Text+''''
     ], ' WHERE S_ALKOD = '''+ret_kod+''' ' );
  Close;
end;

procedure TCegadatbeDlg.MaskEditClick(Sender: TObject);
begin
	with Sender as TMaskEdit do begin
  	SelectAll;
  end;
end;

procedure TCegadatbeDlg.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
	EgyebDlg.FormReturn(Key);
end;

end.
