unit KihutoList;

interface

uses
  WinTypes, WinProcs, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, QuickRpt, QRExpr, Qrctrls, ExtCtrls, printers, DB, DBTables, Grids, IniFiles,
  ADODB ;

type
  TKihutoListDlg = class(TForm)
    Rep: TQuickRep;
    QRBand3: TQRBand;
	 QRLSzamla: TQRLabel;
	 QRBand1: TQRBand;
    QRBand2: TQRBand;
    QRLabel38: TQRLabel;
    QRLabel6: TQRLabel;
    QRSysData1: TQRSysData;
    QRLabel7: TQRLabel;
    QRSysData4: TQRSysData;
    QRLabel1: TQRLabel;
    QV5: TQRLabel;
    Q3: TQRLabel;
    Q2: TQRLabel;
    Q5: TQRLabel;
    Q4: TQRLabel;
    QV4: TQRLabel;
    QRLabel21: TQRLabel;
    Q1: TQRLabel;
    ArfolyamGrid: TStringGrid;
    Osszesito: TStringGrid;
    QV3: TQRLabel;
    QRLabel13: TQRLabel;
    QRLabel17: TQRLabel;
    Query1: TADOQuery;
    QRGroup1: TQRGroup;
    QRBand5: TQRBand;
    O1: TQRLabel;
    O3: TQRLabel;
    O5: TQRLabel;
    O4: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel15: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRLabel18: TQRLabel;
    QRShape1: TQRShape;
    QRLabel9: TQRLabel;
    Q6: TQRLabel;
    O6: TQRLabel;
    QV6: TQRLabel;
    QRLabel10: TQRLabel;
    Q7: TQRLabel;
    QRShape2: TQRShape;
    ChildBand1: TQRChildBand;
    QRLabel11: TQRLabel;
    QRLabel12: TQRLabel;
    QRLabel14: TQRLabel;
    QRLabel16: TQRLabel;
    QRLabel19: TQRLabel;
    QRLabel20: TQRLabel;
    QRShape3: TQRShape;
    QRLabel22: TQRLabel;
    Q8: TQRLabel;
    QRLabel23: TQRLabel;
    Q9: TQRLabel;
    Q10: TQRLabel;
    QRLabel24: TQRLabel;
    Q50: TQRLabel;
    procedure FormCreate(Sender: TObject);
    procedure	Tolt(dat1, dat2, rendsz	: string; reszletes : boolean );
    procedure QRBand1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBand2BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure RepBeforePrint(Sender: TCustomQuickRep;
      var PrintReport: Boolean);
    procedure QRGroup1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBand5BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRGroup1AfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
  private
    	sorszam			: integer;
     	reszlet			: boolean;
       atlag			: double;
       elozoora		: double;
       osszora			: double;
       osszmenny       : double;  // a szumma sorhoz
       omenny		    : double;
       kumulalt_menny: double;  // a nem-teli tankokhoz
       kumulalt_ora: double;  // a nem-teli tankokhoz
       vomenny         : double;
       vegora			: double;
       vegmenny		: double;
       osszertek		: double;
       vegertek		: double;
       elso: boolean;
  public
     vanadat			: boolean;
  end;

var
  KihutoListDlg: TKihutoListDlg;

implementation

uses
	J_SQL, Egyeb, KOzos;

{$R *.DFM}

procedure TKihutoListDlg.FormCreate(Sender: TObject);
begin
	EgyebDlg.SetADOQueryDatabase(Query1);
  	sorszam		:= 1;
end;

procedure	TKihutoListDlg.Tolt(dat1, dat2, rendsz	: string; reszletes : boolean );
var
  	sqlstr		: string;
   dattol	  	: string;
   datig 		: string;
begin
	 vanadat 	:= false;
   dattol		:= dat1;
   datig		:= dat2;
   reszlet		:= reszletes;
   QrLabel21.Caption	:= dat1 + ' - ' + dat2;
   if dat1 = '' then begin
   	dattol	:= '1990.01.01.';
   end;
   if dat2 = '' then begin
   	datig	:= EgyebDlg.MaiDatum;
   end;

	 // A j�rat ellen�rz�tts�g ellen�rz�se
   sqlstr	:= 'SELECT * FROM KOLTSEG WHERE KS_TIPUS = 3 AND KS_DATUM >= '''+dattol+''' '+' AND KS_DATUM <= '''+datig+''' ';
   if rendsz <> '' then begin
		  // Egyetlen aut�n�l nem kell a v�g�sszesen
      //QRBand2.Enabled 	:= false;
      //QRBand2.Enabled 	:= pos(rendsz,',')>0;         // ha t�bb rendsz�m van

   	  //sqlstr	:= sqlstr + ' AND KS_RENDSZ = '''+rendsz+''' ';
      rendsz:='('''+StringReplace(rendsz,',',''',''',[rfReplaceAll])+''')';
   	  sqlstr	:= sqlstr + ' AND KS_RENDSZ in '+rendsz;
   end;
	 // sqlstr	:= sqlstr + ' ORDER BY KS_RENDSZ, KS_DATUM, KS_KTKOD ';
	 sqlstr	:= sqlstr + ' ORDER BY KS_RENDSZ, KS_DATUM, KS_IDO ';  // napon bel�li t�bb tankol�sn�l �gy jobb lesz, rem�lem

	 {A kapott sql alapj�n v�grehajtja a lek�rdez�st}
   Query_Run(Query1, sqlstr);
   vanadat := Query1.RecordCount > 0;
   if not reszlet then begin
   	   // Az �sszesen sorokat kisebbre venni
       QRBand5.Height := O1.Top + O1.Height + 5;
       QRShape2.Enabled 	:= false;
   end;
end;

procedure TKihutoListDlg.QRBand1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
var
	 uzemora	: double;
   menny	: double;
begin
	 uzemora		:= StringSzam(Query1.FieldByName('KS_KMORA').AsString);
   menny		:= StringSzam(Query1.FieldByName('KS_UZMENY').AsString);
   kumulalt_ora:= kumulalt_ora + (uzemora - elozoora);
   kumulalt_menny:= kumulalt_menny + menny;
	 Q1.Caption 	:= IntToStr(sorszam);
   Inc(sorszam);
   atlag		:= 0;
   if elozoora	<> -1 then begin
   	  if kumulalt_ora <> 0 then begin
       	atlag	:= kumulalt_menny / kumulalt_ora;
		    end;
    	end
   else begin
		 //menny	:= 0;
     atlag:=StringSzam(Query1.FieldByName('KS_ATLAG').AsString);
     end;
   Q2.Caption 	:= Query1.FieldByName('KS_DATUM').AsString;
   Q3.Caption 	:= Format('%.2f', [uzemora]);
   Q4.Caption	:= Format('%.2f', [menny]);
   if  Query1.FieldByName('KS_TELE').AsString = '1' then begin
    	Q5.Caption 	:= Format('%.2f', [atlag]);
  		Q50.Caption 	:= 'igen';
      kumulalt_menny:= 0;
      kumulalt_ora:= 0;
   end else begin
      Q5.Caption:='';
  		Q50.Caption 	:= '';
   end;
  	Q6.Caption	:= Format('%.2f', [StringSzam(Query1.FieldByName('KS_ERTEK').AsString)]);
  	Q7.Caption 	:= Query1.FieldByName('KS_THELY').AsString ;
  	Q8.Caption 	:= Query1.FieldByName('KS_ORSZA').AsString ;
  	Q9.Caption 	:= Query1.FieldByName('KS_FIMOD').AsString ;
   if elozoora	<> -1 then begin
		osszora		:= osszora	+ ( uzemora - elozoora );
	end;
   elozoora	:= uzemora;
   osszmenny	:= osszmenny + menny;
   if not elso then begin
     omenny	:= omenny + menny;
   end;
   elso:=False;
   osszertek	:= osszertek + StringSzam(Query1.FieldByName('KS_ERTEK').AsString);
   PrintBand	:= reszlet;
end;

// a nem-telitankok kumul�l�sa nem m�k�dik j�l
{procedure TKihutoListDlg.QRBand1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
var
	 uzemora	: double;
   menny	: double;
begin
	 uzemora		:= StringSzam(Query1.FieldByName('KS_KMORA').AsString);
   menny		:= StringSzam(Query1.FieldByName('KS_UZMENY').AsString);
	 Q1.Caption 	:= IntToStr(sorszam);
   Inc(sorszam);
   atlag		:= 0;
   if elozoora	<> -1 then begin
   	if (uzemora - elozoora) <> 0 then begin
       	atlag	:= menny / (uzemora - elozoora);
		end;
	end else begin
		//menny	:= 0;
       atlag:=StringSzam(Query1.FieldByName('KS_ATLAG').AsString);
   end;
  	Q2.Caption 	:= Query1.FieldByName('KS_DATUM').AsString;
  	Q3.Caption 	:= Format('%.2f', [uzemora]);
  	Q4.Caption	:= Format('%.2f', [menny]);
   if  Query1.FieldByName('KS_TELE').AsString = '1' then begin
    	Q5.Caption 	:= Format('%.2f', [atlag]);
  		Q50.Caption 	:= 'igen';
   end else begin
      Q5.Caption:='';
  		Q50.Caption 	:= '';
   end;
  	Q6.Caption	:= Format('%.2f', [StringSzam(Query1.FieldByName('KS_ERTEK').AsString)]);
  	Q7.Caption 	:= Query1.FieldByName('KS_THELY').AsString ;
  	Q8.Caption 	:= Query1.FieldByName('KS_ORSZA').AsString ;
  	Q9.Caption 	:= Query1.FieldByName('KS_FIMOD').AsString ;
   if elozoora	<> -1 then begin
		osszora		:= osszora	+ ( uzemora - elozoora );
	end;
   elozoora	:= uzemora;
   osszmenny	:= osszmenny + menny;
   if not elso then begin
     omenny	:= omenny + menny;
   end;
   elso:=False;
   osszertek	:= osszertek + StringSzam(Query1.FieldByName('KS_ERTEK').AsString);
   PrintBand	:= reszlet;
end;

}

procedure TKihutoListDlg.QRBand2BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
begin
	// A v�g�sszesenek ki�r�sa
   atlag		:= 0;
   if vegora <> 0 then begin
   	atlag 	:= vomenny / vegora;
   end;
  	QV3.Caption 	:= Format('%.2f', [vegora]);
  	QV4.Caption		:= Format('%.2f', [vegmenny]);
  	QV5.Caption 	:= Format('%.2f', [atlag]);
  	QV6.Caption 	:= Format('%.2f', [vegertek]);
end;

procedure TKihutoListDlg.RepBeforePrint(Sender: TCustomQuickRep;
      var PrintReport: Boolean);
begin
	elozoora	:= -1;
   vegora		:= 0;
   vegmenny	:= 0;
   vegertek	:= 0;
   sorszam		:= 1;
   osszora		:= 0;
   osszmenny	:= 0;
   omenny      := 0;
   osszertek	:= 0;
   vomenny     := 0;
   kumulalt_ora:= 0;
   kumulalt_menny := 0;
end;

procedure TKihutoListDlg.QRGroup1BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
	// A rendsz�m be�r�sa
   QrLabel5.Caption	:= Query1.FieldByName('KS_RENDSZ').AsString + ' ' +
   	Query_Select('GEPKOCSI', 'GK_RESZ', Query1.FieldByName('KS_RENDSZ').AsString, 'GK_TIPUS');
   PrintBand			:= reszlet;
end;

procedure TKihutoListDlg.QRBand5BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
	// A g�pkocsi �sszesenek be�r�sa
   O1.Caption	:= Query1.FieldByName('KS_RENDSZ').AsString + ' �sszes :';
   atlag		:= 0;
   if osszora <> 0 then begin
   	atlag 	:= omenny / osszora;
   end;
  	O3.Caption 	:= Format('%.2f', [osszora]);
  	O4.Caption	:= Format('%.2f', [osszmenny]);
  	O5.Caption 	:= Format('%.2f', [atlag]);
  	O6.Caption 	:= Format('%.2f', [osszertek]);
   if osszmenny<>0 then begin
       Q10.Caption:=Format('%.0f Ft/l', [osszertek/osszmenny])
   end else begin
       Q10.Caption:='';
   end;
   vegora		:= vegora + osszora;
   vegmenny	:= vegmenny + osszmenny;
   vegertek	:= vegertek + osszertek;
   vomenny     := vomenny + omenny;
   omenny      := 0;
   osszora		:= 0;
   osszmenny	:= 0;
   osszertek	:= 0;
   elozoora	:= -1;
   sorszam		:= 1;
   kumulalt_ora:= 0;
   kumulalt_menny := 0;
end;

procedure TKihutoListDlg.ChildBand1BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
	PrintBand	:= not reszlet;
end;

procedure TKihutoListDlg.QRGroup1AfterPrint(Sender: TQRCustomBand;
  BandPrinted: Boolean);
begin
  elso:=True;
end;

end.
