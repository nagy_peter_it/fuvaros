unit Alvallalbe;

interface

uses
	SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
	Forms, Dialogs, StdCtrls, Buttons, Mask, DB, DBTables, J_ALFORM, ADODB,
  ExtCtrls, Grids, DBGrids, ComCtrls;
	
type
	TAlvallalbeDlg = class(TJ_AlformDlg)
	BitElkuld: TBitBtn;
	BitKilep: TBitBtn;
	Label1: TLabel;
	Label2: TLabel;
	Label3: TLabel;
	Label4: TLabel;
	Label5: TLabel;
	Label6: TLabel;
	Label7: TLabel;
	MaskEdit1: TMaskEdit;
	MaskEdit2: TMaskEdit;
	MaskEdit3: TMaskEdit;
	MaskEdit4: TMaskEdit;
	MaskEdit5: TMaskEdit;
	MaskEdit6: TMaskEdit;
	MaskEdit8: TMaskEdit;
	Label9: TLabel;
	MaskEdit7: TMaskEdit;
	Label12: TLabel;
	Label10: TLabel;
	MaskEdit9: TMaskEdit;
	Label15: TLabel;
	 MaskEdit11: TMaskEdit;
	Label16: TLabel;
    MaskEdit12: TMaskEdit;
    Label8: TLabel;
    MaskEdit13: TMaskEdit;
    Label14: TLabel;
    Label18: TLabel;
    AFACombo: TComboBox;
    Label20: TLabel;
    OrszagCombo: TComboBox;
    Query1: TADOQuery;
    MaskEdit10: TMaskEdit;
    Label11: TLabel;
    PageControl1: TPageControl;
    TabSheet2: TTabSheet;
    Panel1: TPanel;
    BitBtn30: TBitBtn;
    BitBtn31: TBitBtn;
    BitBtn32: TBitBtn;
    DBGrid1: TDBGrid;
    QueryGk: TADOQuery;
    DataSource1: TDataSource;
    QueryGkAG_RENSZ: TStringField;
    QueryGkAG_TIPUS: TStringField;
    QueryGkAG_AGKOD: TIntegerField;
	procedure BitKilepClick(Sender: TObject);
	procedure FormCreate(Sender: TObject);
	procedure Tolto(cim : string; teko : string);override;
	procedure BitElkuldClick(Sender: TObject);
	procedure FormDestroy(Sender: TObject);
	procedure MaskEdit10Exit(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit13KeyPress(Sender: TObject; var Key: Char);
    procedure BitBtn30Click(Sender: TObject);
	function Elkuldes : boolean;
    procedure BitBtn31Click(Sender: TObject);
    procedure BitBtn32Click(Sender: TObject);
	private
		afalist 			: TStringList;
		orszaglist 		: TStringList;
	public
	end;

var
	AlvallalbeDlg: TAlvallalbeDlg;

implementation

uses
	Egyeb, J_SQL, Kozos, AlGepbe;
{$R *.DFM}

procedure TAlvallalbeDlg.BitKilepClick(Sender: TObject);
begin
	ret_kod := '';
	Close;
end;

procedure TAlvallalbeDlg.FormCreate(Sender: TObject);
begin
	ret_kod := '';
  	EgyebDlg.SetADOQueryDatabase(Query1);
	EgyebDlg.SetADOQueryDatabase(QueryGK);
	SetMaskEdits([MaskEdit1]);
	afalist := TStringList.Create;
	SzotarTolt(AFACombo, '101' , afalist, false );
	AfaCombo.ItemIndex  := 0;
	orszaglist := TStringList.Create;
	SzotarTolt(OrszagCombo, '300' , orszaglist, false );
	OrszagCombo.ItemIndex  := 0;
end;

procedure TAlvallalbeDlg.Tolto(cim : string; teko:string);
begin
	ret_kod 	:= teko;
	Caption := cim;
	MaskEdit1.Text := '';
	MaskEdit2.Text := '';
	MaskEdit3.Text := '';
	MaskEdit4.Text := '';
	MaskEdit5.Text := '';
	MaskEdit6.Text := '';
	MaskEdit7.Text := '';
	MaskEdit8.Text := '';
	MaskEdit9.Text := '';
	MaskEdit11.Text := '';
	MaskEdit12.Text := '';
	BitBtn31.Enabled	:= false;
	BitBtn32.Enabled	:= false;
	if ret_kod <> '' then begin		{M�dos�t�s}
		if Query_Run(Query1, 'SELECT * FROM ALVALLAL WHERE V_KOD = '''+ret_kod+''' ') then begin
			if Query1.RecordCount > 0 then begin
				MaskEdit1.Text 			:= Query1.FieldByName('V_KOD').AsString;
				MaskEdit2.Text 			:= Query1.FieldByName('V_NEV').AsString;
				MaskEdit3.Text 			:= Query1.FieldByName('V_IRSZ').AsString;
				MaskEdit4.Text 			:= Query1.FieldByName('V_VAROS').AsString;
				MaskEdit5.Text 			:= Query1.FieldByName('V_CIM').AsString;
				MaskEdit6.Text 			:= Query1.FieldByName('V_TEL').AsString;
				MaskEdit7.Text 			:= Query1.FieldByName('V_FAX').AsString;
				MaskEdit8.Text 			:= Query1.FieldByName('V_ADO').AsString;
				MaskEdit9.Text 			:= Query1.FieldByName('V_BANK').AsString;
				MaskEdit11.Text 		:= Query1.FieldByName('V_UGYINT').AsString;
				MaskEdit10.Text 		:= Query1.FieldByName('V_EMAIL').AsString;
				MaskEdit12.Text 		:= Query1.FieldByName('V_MEGJ').AsString;
				MaskEdit13.Text 		:= Query1.FieldByName('V_LEJAR').AsString;
          		ComboSet (AfaCombo, Query1.FieldByName('V_AFAKO').AsString, afalist);
				ComboSet (OrszagCombo, Query1.FieldByName('V_ORSZ').AsString, orszaglist);
				Query_Run(QueryGK, 'SELECT * FROM ALGEPKOCSI WHERE AG_AVKOD = '+ret_kod+' ORDER BY AG_RENSZ ');
				if QueryGK.RecordCount > 0 then begin
					BitBtn31.Enabled	:= true;
					BitBtn32.Enabled	:= true;
				end;
			end;
		end;
	end;
end;

procedure TAlvallalbeDlg.BitElkuldClick(Sender: TObject);
begin
	if Elkuldes then begin
		Close;
	end;
end;

procedure TAlvallalbeDlg.FormDestroy(Sender: TObject);
begin
	afalist.Free;
end;

procedure TAlvallalbeDlg.MaskEdit10Exit(Sender: TObject);
begin
	SzamExit(Sender);
end;

procedure TAlvallalbeDlg.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
	EgyebDlg.FormReturn(Key);
end;

procedure TAlvallalbeDlg.MaskEdit13KeyPress(Sender: TObject; var Key: Char);
begin
	Key := EgyebDlg.Jochar(MaskEdit13.Text,Key,3,0);
end;

procedure TAlvallalbeDlg.BitBtn30Click(Sender: TObject);
begin
	if Elkuldes then begin
		Application.CreateForm(TAlgepbeDlg, AlgepbeDlg);
		AlgepbeDlg.Tolto('�j alv�llalkoz�i g�pkocsi felvitele', '');
		// Az alv�llalkoz� felt�lt�se
		AlgepbeDlg.M1A.Text	:= MaskEdit1.Text;
		AlgepbeDlg.M1B.Text	:= MaskEdit2.Text;
		AlgepbeDlg.ButValVevo.Enabled	:= false;
		AlgepbeDlg.ShowModal;
		if AlgepbeDlg.ret_kod <> '' then begin
			// R�ugrunk a megfelel� rekordra
			QueryGK.Close;
			Query_Run(QueryGK, 'SELECT * FROM ALGEPKOCSI WHERE AG_AVKOD = '+ret_kod+' ORDER BY AG_RENSZ ');
			QueryGK.Locate('AG_AGKOD', AlgepbeDlg.ret_kod, [] );
			BitBtn31.Enabled	:= true;
			BitBtn32.Enabled	:= true;
		end;
		AlgepbeDlg.Destroy;
	end;
end;

function TAlvallalbeDlg.Elkuldes : boolean;
var
	st 			: string;
	nemetstr   	: string;
begin
	Result	:= true;
	if MaskEdit2.Text = '' then begin
		NoticeKi('Hi�nyz� alv�llalkoz� n�v!');
		MaskEdit2.SetFocus;
		Exit;
	end;
	{�j alv�llalkoz� felvitele}
	if ret_kod = '' then begin
		{�j k�d gener�l�sa}
		st				:= Format('%4.4d',[GetNextCode('ALVALLAL', 'V_KOD', 0, 4)]);
		Query_Run(Query1, 'INSERT INTO ALVALLAL (V_KOD) VALUES ('''+st+''') ');
		MaskEdit1.Text 	:= st;
		ret_kod			:= st;
	end;
	nemetstr	:= '0';
	Query_Update( Query1, 'ALVALLAL', [
		'V_NEV', 		''''+MaskEdit2.Text+'''',
		'V_IRSZ',		''''+MaskEdit3.Text+'''',
		'V_VAROS',		''''+MaskEdit4.Text+'''',
		'V_CIM',		''''+MaskEdit5.Text+'''',
		'V_TEL',		''''+MaskEdit6.Text+'''',
		'V_FAX',		''''+MaskEdit7.Text+'''',
		'V_ADO',		''''+MaskEdit8.Text+'''',
		'V_BANK',		''''+MaskEdit9.Text+'''',
		'V_UGYINT',		''''+MaskEdit11.Text+'''',
		'V_EMAIL',		''''+MaskEdit10.Text+'''',
		'V_LEJAR',		IntToStr(StrToIntDef(MaskEdit13.Text,0)),
		'V_MEGJ',		''''+MaskEdit12.Text+'''',
		'V_ORSZ',		''''+orszaglist[OrszagCombo.ItemIndex]+'''',
		'V_AFAKO',		''''+afalist[AfaCombo.ItemIndex]+''''
		], ' WHERE V_KOD = '''+ret_kod+''' ' );
	Result	:= true;
end;
procedure TAlvallalbeDlg.BitBtn31Click(Sender: TObject);
begin
	if Elkuldes then begin
		Application.CreateForm(TAlgepbeDlg, AlgepbeDlg);
		AlgepbeDlg.Tolto('Alv�llalkoz�i g�pkocsi m�dos�t�sa', QueryGK.FieldByName('AG_AGKOD').AsString);
		// Az alv�llalkoz� felt�lt�se
//		AlgepbeDlg.M1A.Text	:= MaskEdit1.Text;
//		AlgepbeDlg.M1A.Text	:= MaskEdit2.Text;
		AlgepbeDlg.ButValVevo.Enabled	:= false;
		AlgepbeDlg.ShowModal;
		if AlgepbeDlg.ret_kod <> '' then begin
			// R�ugrunk a megfelel� rekordra
			QueryGK.Close;
			Query_Run(QueryGK, 'SELECT * FROM ALGEPKOCSI WHERE AG_AVKOD = '+ret_kod+' ORDER BY AG_RENSZ ');
			QueryGK.Locate('AG_AGKOD', AlgepbeDlg.ret_kod, [] );
			BitBtn31.Enabled	:= true;
			BitBtn32.Enabled	:= true;
		end;
		AlgepbeDlg.Destroy;
	end;
end;

procedure TAlvallalbeDlg.BitBtn32Click(Sender: TObject);
var
	regikod	: string;
begin
	if NoticeKi('Val�ban t�r�lni akarja a kiv�lasztott elemet?', NOT_QUESTION) <> 0 then begin
		Exit;
	end;
	regikod				:= QueryGK.FieldByName('AG_AGKOD').AsString;
	QueryGK.Close;
	Query_Run(QueryGK, 'DELETE FROM ALGEPKOCSI WHERE AG_AGKOD = '+regikod);
	Query_Run(QueryGK, 'SELECT * FROM ALGEPKOCSI WHERE AG_AVKOD = '+ret_kod+' ORDER BY AG_RENSZ ');
	BitBtn31.Enabled	:= QueryGK.RecordCount > 0;
	BitBtn32.Enabled	:= QueryGK.RecordCount > 0;
end;

end.
