unit ArfolyamFm;

interface

uses
	SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
	StdCtrls, Forms, ExtCtrls, Dialogs, J_FOFORMUJ, Buttons, Jaratbe;

type
	TArfolyamFmDlg = class(TJ_FOFORMUJDLG)
	  procedure FormCreate(Sender: TObject);override;
    procedure Adatlap(cim, kod : string);override;
	end;

var
	ArfolyamFmDlg : TArfolyamFmDlg;

implementation

uses
	Egyeb, J_SQL, Arfolybe;

{$R *.DFM}

procedure TArfolyamFmDlg.FormCreate(Sender: TObject);
begin
	Inherited FormCreate(Sender);
	AddSzuromezoRovid('AR_VALNEM', 'ARFOLYAM');
	FelTolto('�j �rfolyam felvitele ', '�rfolyam adatok m�dos�t�sa ',
			'�rfolyamok list�ja', 'AR_KOD', 'Select * from ARFOLYAM ', 'ARFOLYAM', QUERY_KOZOS_TAG );
	width				:= DLG_WIDTH;
	height				:= DLG_HEIGHT;
end;

procedure TArfolyamFmDlg.Adatlap(cim, kod : string);
begin
	Application.CreateForm(TArfolybeDlg, AlForm);
	Inherited Adatlap(cim, kod );
end;

end.

