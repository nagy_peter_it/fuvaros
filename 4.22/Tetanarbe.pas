unit Tetanarbe;

interface

uses
	SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
	Forms, Dialogs, StdCtrls, Buttons, Mask, DB, DBTables, J_ALFORM, ADODB,
  ExtCtrls;

type
	TTetanarbeDlg = class(TJ_AlformDlg)
	BitElkuld: TBitBtn;
	BitKilep: TBitBtn;
	Label2: TLabel;
    M1: TMaskEdit;
    M2: TMaskEdit;
	Label8: TLabel;
	Label9: TLabel;
    M3: TMaskEdit;
    BitBtn7: TBitBtn;
    Query1: TADOQuery;
    Label1: TLabel;
    M4: TMaskEdit;
    Label3: TLabel;
    RGP1: TRadioGroup;
	procedure BitKilepClick(Sender: TObject);
	procedure FormCreate(Sender: TObject);
	procedure Tolto(cim : string; teko : string);override;
	procedure BitElkuldClick(Sender: TObject);
  procedure MaskEditClick(Sender: TObject);
  procedure MaskEditExit(Sender: TObject);
	procedure MaskEditKeyPress(Sender: TObject; var Key: Char);
	 procedure Modosit(Sender: TObject);
	 procedure FormKeyDown(Sender: TObject; var Key: Word;
	   Shift: TShiftState);
	 procedure M3Exit(Sender: TObject);
	 procedure BitBtn7Click(Sender: TObject);
	private
	  modosult 	: boolean;
	public
	end;

var
	TetanarbeDlg: TTetanarbeDlg;

implementation

uses
	Egyeb, J_SQL, Kozos;
{$R *.DFM}

procedure TTetanarbeDlg.BitKilepClick(Sender: TObject);
begin
	if modosult then begin
		if NoticeKi('M�dosultak az adatok . Ki akar l�pni ment�s n�lk�l?', NOT_QUESTION ) = 0 then begin
			ret_kod := '';
			Close;
		end;
	end else begin
		ret_kod := '';
		Close;
	end;
end;

procedure TTetanarbeDlg.FormCreate(Sender: TObject);
begin
	EgyebDlg.SetADOQueryDatabase(Query1);
	ret_kod 	:= '';
	modosult 	:= false;
end;

procedure TTetanarbeDlg.Tolto(cim : string; teko:string);
begin
	ret_kod 	:= teko;
	Caption 	:= cim;
	M3.Text := EgyebDlg.MaiDatum;
	SetMaskEdits([M1]);
	if ret_kod <> '' then begin		{M�dos�t�s}
		if Query_Run (Query1, 'SELECT * FROM TETANAR WHERE TT_TTKOD = '+teko,true) then begin
			M2.Text 		:= SzamString(Query1.FieldByName('TT_EGYAR').AsFloat,12,4);
			M3.Text 		:= Query1.FieldByName('TT_DATUM').AsString;
			M4.Text 		:= Query1.FieldByName('TT_MEGJE').AsString;
			RGP1.ItemIndex	:= StrToIntdef(Query1.FieldByName('TT_TIPUS').AsString,0);
		end;
	end else begin
		M1.Text	:= IntToStr(GetNextCode('TETANAR', 'TT_TTKOD'));
	end;
	modosult 	:= false;
end;

procedure TTetanarbeDlg.BitElkuldClick(Sender: TObject);
begin
	if not DatumExit(M3, true) then begin
		Exit;
	end;
	if ret_kod = '' then begin
		{�j rekord felvitele}
		Query_Run(Query1, 'SELECT * FROM TETANAR WHERE TT_DATUM = '''+M3.Text+''' ');
		if Query1.RecordCount > 0 then begin
			NoticeKi('A megadott d�tummal m�r l�tezik egys�g�r!');
			Exit;
		end;
		ret_kod	:= IntToStr(GetNextCode('TETANAR', 'TT_TTKOD'));
		Query_Run ( Query1, 'INSERT INTO TETANAR (TT_TTKOD) VALUES (' +ret_kod+ ' )',true);
	end;
	{A r�gi rekord m�dos�t�sa}
	Query_Update (Query1, 'TETANAR',
		[
		'TT_EGYAR', SqlSzamString(StringSzam(M2.Text),12,2),
		'TT_TIPUS', IntToStr(RGP1.ItemIndex),
		'TT_DATUM', ''''+M3.Text+'''',
		'TT_MEGJE', ''''+M4.Text+''''
	  ]	,
		' WHERE TT_TTKOD = '+ret_kod);
	Close;
end;

procedure TTetanarbeDlg.MaskEditClick(Sender: TObject);
begin
	with Sender as TMaskEdit do begin
		SelectAll;
	end;
end;

procedure TTetanarbeDlg.MaskEditExit(Sender: TObject);
begin
	SzamExit(Sender);
end;

procedure TTetanarbeDlg.MaskEditKeyPress(Sender: TObject; var Key: Char);
begin
  with Sender as TMaskEdit do begin
		Key := EgyebDlg.Jochar(Text,Key,12,2);
  end;
end;

procedure TTetanarbeDlg.Modosit(Sender: TObject);
begin
	modosult := true;
end;

procedure TTetanarbeDlg.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
	EgyebDlg.FormReturn(Key);
end;

procedure TTetanarbeDlg.M3Exit(Sender: TObject);
begin
	DatumExit(Sender);
end;

procedure TTetanarbeDlg.BitBtn7Click(Sender: TObject);
begin
	EgyebDlg.Calendarbe(M3);
end;

end.
