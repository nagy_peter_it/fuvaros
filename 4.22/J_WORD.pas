unit J_WORD;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls,  OleServer, COMobj, ActiveX, Word2000;


type
	TJWord = class(TComponent)
  	private
      WordName		: string;
      WordAPP		: Variant;
      Range		: Variant;

	public
    	constructor Create(AOwner: TComponent); override;
    	destructor 	Destroy; override;
     	function	OpenWordFile(fn : string) : boolean;
		function	SaveWordFile : boolean;
		function	SaveDocumentAs(docnr : integer; fn : string) : boolean;
		function	CloseWordFile : boolean;
       function	GetDocumentsCount : integer;
		function	GetDocumentName( docnr : integer) : string;
       function	ReplaceText(findstr, replstr : string) : integer;
		function	CreateDocument : integer;
       function	SetActiveDocument ( docnr : integer ) : boolean;
       function	GetRange ( docnr : integer) : Variant;
		function	GetParagraphCount ( docnr : integer) :  integer;
		function	PasteToEnd ( docnr : integer)  : boolean;
		function	GetEnd ( docnr : integer) : Variant;
  	private
  	published
     	property		FileName : string read WordName write WordName;
  end;

var
  JWord: TJWord;

implementation

constructor TJWord.Create(AOwner: TComponent);
begin
  	inherited Create(AOwner);
  	Coinitialize(nil);
  	WordAPP			:= CreateOLEObject('Word.Application');     // Ole object creation
  	WordAPP.visible	:= false;
	WordName		:= '';
end;

destructor TJWord.Destroy;
begin
	// Itt j�nnek a megsz�ntet�sek
	WordApp.Quit;
	WordApp := Unassigned;
  	inherited Destroy;
end;

function	TJWord.OpenWordFile(fn : string) : boolean;
begin
	Result		:= false;
	WordName 	:= fn;
  	try
     	if not FileExists(fn) then begin
        	// �j workbook hozz�ad�sa
        	Exit;
     	end else begin
        	WordApp.Documents.Open(fn);
     	end;
  		Result	:= true;
  	except
  	end;
end;

function	TJWord.SaveWordFile : boolean;
begin
	Result		:= false;
   try
   	WordApp.ActiveDocument.Save;
     	Result	:= true;
   except
   end;
end;

function	TJWord.CloseWordFile : boolean;
begin
	Result		:= false;
   try
   	WordApp.ActiveDocument.Close;
     	Result	:= true;
   except
   end;
end;

function	TJWord.GetDocumentsCount : integer;
begin
	Result		:= 0;
   try
   	Result	:= WordApp.Documents.Count;
   except
   end;
end;

function	TJWord.GetDocumentName( docnr : integer) : string;
begin
	Result		:= '';
   try
   	Result	:= WordApp.Documents.Item(docnr).Name;
   except
   end;
end;

function	TJWord.SaveDocumentAs(docnr : integer; fn : string) : boolean;
begin
	Result	:= false;
   try
   	WordApp.Documents.Item(docnr).SaveAs(fn);
     	Result	:= true;
   except
   end;
end;


function	TJWord.ReplaceText(findstr, replstr : string)  : integer;
begin
   WordApp.Selection.Find.ClearFormatting;
   WordApp.Selection.Find.Text 				:= findstr;
   WordApp.Selection.Find.Replacement.Text 	:= replstr;
   WordApp.Selection.Find.Forward 				:= True;
   WordApp.Selection.Find.MatchAllWordForms 	:= False;
   WordApp.Selection.Find.MatchCase 			:= False;
   WordApp.Selection.Find.MatchWildcards 		:= False;
   WordApp.Selection.Find.MatchSoundsLike 		:= False;
   WordApp.Selection.Find.MatchWholeWord 		:= False;
   WordApp.Selection.Find.MatchFuzzy 			:= False;
   WordApp.Selection.Find.Wrap 				:= 1;
   WordApp.Selection.Find.Format 				:= False;
	WordApp.Selection.Find.Execute(Replace := 2);
   Result	:= 1;
end;

function	TJWord.CreateDocument : integer;
begin
	Result	:= 0;
   try
   	WordApp.Documents.Add;
   	Result	:= WordApp.Documents.Count;
   except
   end;
end;

function	TJWord.SetActiveDocument ( docnr : integer) : boolean;
begin
	Result	:=false;
   try
  		WordApp.ActiveDocument := WordApp.Documents.Item(docnr);
   except
   end;
end;

function	TJWord.GetRange ( docnr : integer) : Variant;
begin
   Result := WordApp.Documents.Item(docnr).Range;
end;

function	TJWord.GetParagraphCount ( docnr : integer) :  integer;
begin
  	Result := WordApp.Documents.Item(docnr).Paragraphs.Count;
end;

function	TJWord.PasteToEnd ( docnr : integer)  : boolean;
begin
	Result	:= true;
	Range 	:= WordApp.Documents.Item(docnr).Range(
   	WordApp.Documents.Item(docnr).Paragraphs.Item(WordApp.Documents.Item(docnr).Paragraphs.Count).Range.Start,
       WordApp.Documents.Item(docnr).Paragraphs.Item(WordApp.Documents.Item(docnr).Paragraphs.Count).Range.End );
   Range.Paste;
end;

function	TJWord.GetEnd ( docnr : integer) : Variant;
begin
	Result 	:= WordApp.Documents.Item(docnr).Range(
   	WordApp.Documents.Item(docnr).Paragraphs.Item(WordApp.Documents.Item(docnr).Paragraphs.Count).Range.Start       );
end;




end.



(*
or replace a first occur only:

WordApp.Selection.Find.Execute(Replace := wdReplaceOne);

5. if you want to check if text was found then use Found method:

if WordApp.Selection.Find.Found then  <do something>

6. save a modified document

WordApp.ActiveDocument.SaveAs(yourDocFileName);

7. close MS Word instance

WordApp.ActiveDocument.Close;
WordApp.Quit;
WordApp := Unassigned;

All what you need is to add ActiveX, ComObj units in use-clause and declare such consts and variables:

const   wdFindContinue = 1;
 wdReplaceOne = 1;
wdReplaceAll = 2;var

WordApp: Variant;

*)