﻿unit Fap_Export1;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Buttons, Vcl.Mask,
  Vcl.ExtCtrls, Data.DB, Data.Win.ADODB, Vcl.CheckLst, Vcl.Grids, IniFiles, J_Datamodule;

type
  TFap_Export1Dlg = class(TForm)
    Panel1: TPanel;
    BitElkuld: TBitBtn;
    BitKilep: TBitBtn;
    Memo1: TMemo;
    Label2: TLabel;
    M6: TMaskEdit;
    Query1: TADOQuery;
    Query2: TADOQuery;
    SG1: TStringGrid;
    M1: TMaskEdit;
    M2: TMaskEdit;
    CH1: TCheckBox;
    Query3: TADOQuery;
    RB1: TRadioButton;
    RB2: TRadioButton;
    CH2: TCheckBox;
    M3: TMaskEdit;
    BB1: TBitBtn;
    Label1: TLabel;
    M4: TMaskEdit;
    BB2: TBitBtn;
    RB3: TRadioButton;
    CH3: TCheckBox;
    Q1: TADOQuery;
    Query4: TADOQuery;
    RB4: TRadioButton;
    M10: TMaskEdit;
    Q2: TADOQuery;
    procedure FormCreate(Sender: TObject);
    procedure M1Exit(Sender: TObject);
    procedure BitKilepClick(Sender: TObject);
    procedure BitElkuldClick(Sender: TObject);
    procedure FormCanResize(Sender: TObject; var NewWidth, NewHeight: Integer;
      var Resize: Boolean);
    procedure FtpKuldes(fn : string);
    procedure M1Change(Sender: TObject);
    function  FileNyito(var fn1 : string) : boolean;
    procedure FileZaro;
    procedure FejlecIro;
    function  FileIro( kod : string; tipus : integer ): boolean;
    procedure SorIro( sor : string);
    procedure LablecIro;
    function  ConvertText(str: string): string;
    function  ConvertSzam(str: string): string;
    procedure BB1Click(Sender: TObject);
    procedure BB2Click(Sender: TObject);
    procedure DatumSzerint;
  private

       file1       : TextFile;
       automata    : boolean;

       ftphost     : string;
       ftpport     : string;
       ftpuser     : string;
       ftppswd     : string;
       ftpdir      : string;
       use_ftp     : boolean;

       mess_ref    : string;   // Message Reference Number
       mess_num    : integer;  // Number of messages in an interchange.
       int_ref     : string;   // Interchange control reference

       tetelszam   : integer;
       fapdir      : string;
  public
  end;

var
  Fap_Export1Dlg: TFap_Export1Dlg;

implementation

uses J_SQL, Kozos, Egyeb ;

{$R *.dfm}

procedure TFap_Export1Dlg.FormCreate(Sender: TObject);
var
   Inifn	    : TIniFile;
   megbstr     : string;
   fn          : string;
   i           : integer;
begin
   // A kezdő értékek feltöltése
   automata    := false;
   CAPTION := 'FAP Export';
   EgyebDlg.SetAdoQueryDatabase(Query1);
   EgyebDlg.SetAdoQueryDatabase(Query2);
   EgyebDlg.SetAdoQueryDatabase(Query3);
   EgyebDlg.SetAdoQueryDatabase(Query4);
   EgyebDlg.SetAdoQueryDatabase(Q1);
   EgyebDlg.SetAdoQueryDatabase(Q2);
   Application.DefaultFont.Name    := 'Arial';
   Application.DefaultFont.Size    := 10;

   M1.Text := '';
//   M1.Text := '67447;61869;60102;58104;58475;61411;57888;59984;63215;58794;67050;62209';

(*
   v_JIni  := TJIni.Create(self);
   fn      := ExtractFilePath(Application.ExeName) + ChangeFileExt( ExtractFileName(Application.ExeName), '.INI' );
   v_Jini.SetIni( fn);
   if v_Jini.OpenIni then begin
		megbstr     := v_Jini.ReadIni('GENERAL', 'MEGBIZOK', '0547');
		felrakostr  := v_Jini.ReadIni('GENERAL', 'FELRAKOK', '');

       ftphost     := v_Jini.ReadIni('FTP', 'FTPHOST', '');
       ftpport     := v_Jini.ReadIni('FTP', 'FTPPORT', '');
       ftpuser     := v_Jini.ReadIni('FTP', 'FTPUSER', '');
       ftppswd     := v_Jini.ReadIni('FTP', 'FTPPSWD', '');
       ftpdir      := v_Jini.ReadIni('FTP', 'FTPDIR', '');

       use_ftp     := StrToIntDef(v_Jini.ReadIni('GENERAL', 'USEFTP', '0'), 0) > 0;

       v_Jini.CloseIni;
   end else begin
       megbstr := '0547';
   end;
*)
   M1Change(Sender);
   // A FAP mentés helyének meghatározása
   fapdir		:= EgyebDlg.Read_SZGrid('999', 'FAPHELY');
   if fapdir = '' then begin
       NoticeKi('Figyelem! A FAP export helye még nincs megadva (szótár 999, FAPHELY) , ezért az exportálás az exe könyvtárába történik! ');
       // Bejegyezzük a szótárba
  	    Query_Run(Query3, 'DELETE FROM SZOTAR WHERE SZ_FOKOD = ''999'' AND SZ_ALKOD = ''FAPHELY'' ', FALSE);
       Query_Run(Query3, 'INSERT INTO SZOTAR ( SZ_FOKOD, SZ_ALKOD, SZ_MENEV, SZ_LEIRO, SZ_ERVNY ) VALUES ( ''999'', ''FAPHELY'', '''', ''A FAP export helye'',  1 ) ', FALSE);
       fapdir  := EgyebDlg.TempPath;
   end;
   fapdir		:= IncludeTrailingPathDelimiter(fapdir);
end;

procedure TFap_Export1Dlg.BB1Click(Sender: TObject);
begin
	// Calendarbe(M3);
  // EgyebDlg.ElsoNapEllenor(M3, M4);
  EgyebDlg.CalendarBe_idoszak(M3, M4);
end;

procedure TFap_Export1Dlg.BB2Click(Sender: TObject);
begin
	Calendarbe(M4);
end;

procedure TFap_Export1Dlg.BitElkuldClick(Sender: TObject);
var
   fn          : string;
   str         : string;
   elval       : string;
   i           : integer;
   sorsz       : integer;
   tip         : integer;
   mez         : TStringList;
   fsz         : integer;
begin
   if RB3.Checked then begin
       // Fátum szerinti lekérdezés
       DatumSzerint;
       Exit;
   end;

   // A megbízó kijelölés ellenőrzése
   if RB1.Checked then begin
       str    := M1.Text;
       if str = '' then begin
           NoticeKi('Legalább egy megbízó megadása kötelező!');
           Exit;
       end;
   end;
   if RB2.Checked then begin
       str    := M2.Text;
       if str = '' then begin
           NoticeKi('Legalább egy load id megadása kötelező!');
           Exit;
       end;
   end;
   if RB4.Checked then begin
       str    := M10.Text;
       if str = '' then begin
           NoticeKi('Számlaszám megadása kötelező!');
           Exit;
       end;
   end;
   if not CH3.Checked then begin
       fn  := '';
       if not FileNyito(fn) then begin
           Exit;
       end;
   end;
   Memo1.Lines.Clear;
   Memo1.Lines.Add('START : ' + FormatDateTime ('hh:nn:ss', now) );
   Memo1.Lines.Add('');
   if not CH3.Checked then begin
       FejlecIro;
   end;
   elval := ';';
   if Pos(',', str) > 0 then begin
       elval := ',';
   end;
   mez         := TStringList.Create;
   StringParse(str, elval, mez);
   mess_num    := 0;
   tetelszam   := 0;
   if mez.Count > 0 then begin
       for i := 0 to mez.Count - 1 do begin
           str := mez[i];
           tip := 0;
           if RB2.Checked then begin
               tip := 1;
           end;
           if RB4.Checked then begin
               tip := 2;
           end;
           // bmkod ill. load id alapján meghívjuk az állomány írót
           if str <> '' then begin
               Inc(tetelszam);
               if CH3.Checked then begin
                   fn := fapdir + str+'.edi';
                   fsz := 1;
                   while FileExists(fn) do begin
                       Inc(fsz);
                       fn  := fapdir + str+'_'+IntToStr(fsz)+'.edi';
                   end;
                   if not FileNyito(fn) then begin
                       Exit;
                   end;
                   FejlecIro;
               end;
               if FileIro(str, tip) then begin
                   Inc(mess_num);
               end;
               if CH3.Checked then begin
                   LablecIro;
               end;
           end;
       end;
   end;
   if not CH3.Checked then begin
       LablecIro;
   end;
   if mez.Count = 0 then begin
       // Töröljük a felesleges állományt
       DeleteFile(fn);
   end;
   mez.Free;
   Memo1.Lines.Add('');
   Memo1.Lines.Add('STOP : ' + FormatDateTime ('hh:nn:ss', now) );
   Memo1.Lines.Add('');
end;

procedure TFap_Export1Dlg.BitKilepClick(Sender: TObject);
begin
   Close;
end;

procedure TFap_Export1Dlg.FormCanResize(Sender: TObject; var NewWidth,
  NewHeight: Integer; var Resize: Boolean);
begin
   if ( ( NewWidth < 700 ) or (NewHeight < 400) ) then begin
       Resize := false;
   end;
end;

procedure TFap_Export1Dlg.M1Change(Sender: TObject);
begin
   //
   if RB1.Checked then begin
       M1.Enabled  := true;
       M10.Enabled  := false;
       M10.Text     := '';
       M2.Enabled  := false;
       M2.Text     := '';
       M3.Enabled  := false;
       M3.Text     := '';
       M4.Enabled  := false;
       M4.Text     := '';
       BB1.Enabled := false;
       BB2.Enabled := false;
   end;
   if RB2.Checked then begin
       M2.Enabled  := true;
       M1.Enabled  := false;
       M1.Text     := '';
       M10.Enabled  := false;
       M10.Text     := '';
       M3.Enabled  := false;
       M3.Text     := '';
       M4.Enabled  := false;
       M4.Text     := '';
       BB1.Enabled := false;
       BB2.Enabled := false;
   end;
   if RB3.Checked then begin
       M1.Enabled  := false;
       M1.Text     := '';
       M10.Enabled  := false;
       M10.Text     := '';
       M2.Enabled  := false;
       M2.Text     := '';
       M3.Enabled  := true;
       M4.Enabled  := true;
       BB1.Enabled := true;
       BB2.Enabled := true;
       // Az időszak beállítása
       M3.Text     := copy(EgyebDlg.MaiDatum, 1, 8)+'01.';
       M4.Text     := DatumHoznap(copy(DatumHoznap(M3.Text, 31, false), 1, 8)+'01.', -1, false);
   end;
   if RB4.Checked then begin
       M10.Enabled  := true;
       M1.Enabled  := false;
       M1.Text     := '';
       M2.Enabled  := false;
       M2.Text     := '';
       M3.Enabled  := false;
       M3.Text     := '';
       M4.Enabled  := false;
       M4.Text     := '';
       BB1.Enabled := false;
       BB2.Enabled := false;
   end;
end;

procedure TFap_Export1Dlg.M1Exit(Sender: TObject);
begin
	DatumExit(Sender, true);
end;

procedure TFap_Export1Dlg.FtpKuldes(fn : string);
var
//       jftp        : TJFtp;
       str         : string;
begin
(*
    // Az állomány elküdése a megadott FTP paraméterek alapján
    if not FileExists(fn) then begin
       Memo1.Lines.Add('FTP küldés sikertelen! Hiányzó állomány : '+fn);
       Exit;
    end;

    // A szerver beállításai
   jftp        := TJFtp.Create(Self);
   jftp.SetAttributes(ftphost, ftpuser, ftppswd, StrToIntDef(ftpport, 21) );
   if jftp.Connect then begin
       if ftpdir <> '' then begin
           jftp.ChangeDir(ftpdir);
       end;
       if jftp.SendFile(fn) then begin
           Memo1.Lines.Add('FTP küldés sikeres !');
       end else begin
           Memo1.Lines.Add('FTP küldés sikertelen! ');
       end;
       jftp.Abort;
       jftp.Destroy;
   end else begin
       Memo1.Lines.Add('FTP küldés sikertelen! Kapcsolódási hiba : '+ftphost +'-'+ ftpuser+'-'+ftppswd+'-'+ftpport);
       Exit;
   end;
*)
end;

function TFap_Export1Dlg.FileIro( kod : string; tipus : integer ): boolean;
var
  fn           : string;
  memossz      : Integer;
  sor          : string;
  str          : string;
  megb         : string;
  i            : Integer;
  felrakoszam  : Integer;
  ii           : Integer;
  folytat      : Boolean;
  seged1       : Integer;
  sqlstr       : string;
  mbkod        : string;
  sorszam      : Integer;
  loadid       : string;
  suly         : string;
  pozicio      : string;
  szamlaszam   : string;
  szamladatum  : string;
  saujkod      : string;
  cegnev       : string;
  cim          : string;
  varos        : string;
  irsz         : string;
  orszag       : string;
  sajatadosz   : string;
  szadoszam    : string;
  felrakodat   : string;
  lerakodat    : string;
  paletta      : integer;
  leiras       : string;
  netto        : double;
  ossznetto    : double;
  afa          : double;
  osszafa      : double;
  NADII_sor    : string;
  NADIV_sor    : string;
  NADSF_sor    : string;
  NADST_sor    : string;
  JSzamlaSor	: TJSzamlaSor;
  lanid        : string;
  tobbloadid   : boolean;
  loadidstr    : string;
  kellship     : boolean;
  MegbizasStandTrailer: boolean;
  aru_leiras: string;
  // NumberOfTailers: integer;
begin
   // tipus = 0 - bmkod;  1 - load id
   Result  := false;
   megb    := kod;
   if tipus = 1 then begin
       // A LOAD ID alapján megkeressük a megbízó kódot
       Query_Run(Query1, 'SELECT MT_MBKOD FROM MEGTETEL WHERE MT_LOADID = ''' + kod + ''' ');
       megb    := Query1.FieldByName('MT_MBKOD').AsString;
   end;
   if tipus = 2 then begin
       // A számlaszám alapján megkeressük a megbízó kódot
       Query_Run(Query1, 'SELECT MB_MBKOD FROM MEGBIZAS, VISZONY, SZFEJ WHERE MB_VIKOD = VI_VIKOD AND VI_UJKOD = SA_UJKOD AND SA_KOD = ''' + kod + ''' ');
       megb    := Query1.FieldByName('MB_MBKOD').AsString;
   end;
   Query_Run(Query1, 'select MB_MBKOD, MB_VEKOD, MB_POZIC, MB_LECEG, MB_EDIJ, MB_FORSZ, MB_LORSZ, MB_FERAK, SA_KOD, JA_ORSZ, JA_ALKOD, MB_JASZA, MB_FUDIJ, MB_FUNEM, VI_HONNAN, '+
       ' VI_HOVA, SA_KOD, SA_UJKOD, SA_KIDAT, SA_VEVONEV, SA_VECIM, SA_VEVAROS, SA_VEIRSZ, SA_VEVOKOD, SA_EUADO, MB_FUTID from szfej, viszony, megbizas, jarat '+
       ' where vi_ujkod = sa_ujkod and vi_jakod = ja_kod and vi_vikod = mb_vikod and ja_kod = mb_jakod and MB_MBKOD = ''' + megb + ''' ');
   if Query1.RecordCount < 1 then begin
       NoticeKi('Nem létező megbízás + járat + viszony + szfej kapcsolat!');
       Exit;
   end;
   if Query1.RecordCount > 1 then begin
       NoticeKi('Túl sok megbízás + járat + viszony + szfej kapcsolat!');
       Exit;
   end;
   // -------------------------------------------------------------------- //
   mbkod := IntToStr(StrToIntDef(Query1.FieldByName('MB_MBKOD').AsString, 0));
   MegbizasStandTrailer:= EgyebDlg.IsMegbizasStandTailer(StrToInt(mbkod), aru_leiras);
   // -------------------------------------------------------------------- //
   Memo1.Lines.Add('');
   Memo1.Lines.Add(Format('A %3d. tétel : ', [tetelszam]));
   Memo1.Lines.Add('***************');
   Memo1.Lines.Add('A számla száma : ' + Query1.FieldByName('SA_KOD').AsString);
   Memo1.Lines.Add('A járatszám    : ' + Query1.FieldByName('JA_ORSZ').AsString + '-' + Query1.FieldByName('JA_ALKOD').AsString + '(' + Query1.FieldByName('MB_JASZA').AsString + ')');
   Memo1.Lines.Add('A fuvardíj     : ' + Query1.FieldByName('MB_FUDIJ').AsString + ' ' + Query1.FieldByName('MB_FUNEM').AsString);
   Memo1.Lines.Add('Honnan - hova  : ' + Query1.FieldByName('VI_HONNAN').AsString + '-' + Query1.FieldByName('VI_HOVA').AsString);
   loadid      := '';
   kellship    := false;
   Query_Run(Query2, 'SELECT MT_LOADID, MT_SORSZ, MT_FSULY, MT_POZIC, MT_FEPAL FROM MEGTETEL WHERE MT_MBKOD = ' + mbkod+' ORDER BY MT_SORSZ');
   if Query2.RecordCount < 1 then begin
       if Query1.FieldByName('MB_POZIC').AsString = 'Non SAP' then begin
           loadid  := 'NON SAP';
       end else begin
           if (Query1.FieldByName('MB_POZIC').AsString <> '') or MegbizasStandTrailer then begin
               loadid      := Query1.FieldByName('MB_POZIC').AsString;
               kellship    := true;
           end else begin
               NoticeKi('Nem létező load ID!');
               Exit;
           end;
       end;
   end;
   if loadid = '' then begin
       loadid      := Query2.FieldByName('MT_LOADID').AsString;
   end;
   tobbloadid  := false;
   loadidstr   := '';
   if Query2.RecordCount > 1 then begin
       tobbloadid  := true;
       // A többi loadid összegyűjtése
       Query2.Next;
       while not Query2.Eof do begin
           loadidstr   := loadidstr + Query2.FieldByName('MT_LOADID').AsString + ',';
           Query2.Next;
       end;
       Query2.First;
   end;
   // Köbös súly meghatározása
   Query_Run(Query3, 'SELECT MS_FEKOB FROM MEGSEGED WHERE MS_MBKOD = ' + mbkod+ ' AND MS_SORSZ = '+IntToSTr(StrToIntDef( Query2.FieldByName('MT_SORSZ').AsString, 0 )));
   if Query3.RecordCount > 0  then begin
       suly     := Query3.FieldByName('MS_FEKOB').AsString;
   end;
   if StringSzam(suly) = 0 then begin
       suly     := Query2.FieldByName('MT_FSULY').AsString;
   end;
   if tobbloadid  then begin
       Query_Run(Query3, 'SELECT SUM(MS_FEKOB) AS OSSZSULY FROM MEGSEGED WHERE MS_MBKOD = ' + mbkod );
       suly     := Query3.FieldByName('OSSZSULY').AsString;
       if StringSzam(suly) = 0 then begin
           Query_Run(Query3, 'SELECT SUM(MT_FSULY) AS OSSZSULY FROM MEGTETEL WHERE MT_MBKOD = '+mbkod);
           suly     := Query3.FieldByName('OSSZSULY').AsString;
       end;
   end;
   pozicio  := Query2.FieldByName('MT_POZIC').AsString;
   paletta  := StrToIntDef(Query2.FieldByName('MT_FEPAL').AsString,0);
   // Ha a paletta vagy a súly 0 akkor összegezzük a megsegéd -ből a felrakó adatokat.
   if ( (paletta = 0) or (StringSzam(suly) = 0) ) then begin
       Query_Run(Query3, 'SELECT SUM(MS_FEPAL) AS OSSZPAL,  SUM(MS_FSULY) AS OSSZSULY FROM MEGSEGED WHERE MS_MBKOD = ' + mbkod );
       paletta  := StrToIntDef(Query3.FieldByName('OSSZPAL').AsString,0);
       suly     := Query3.FieldByName('OSSZSULY').AsString;
   end;

   Memo1.Lines.Add('Paletta - súly : ' + Format('%d db  -  %.2f kg', [paletta, StringSzam(suly)]));
   Memo1.Lines.Add('A loadid       : ' + loadid);
   Memo1.Lines.Add('A pozíció      : ' + pozicio);
   szamlaszam   := Query1.FieldByName('SA_KOD').AsString;
   saujkod      := Query1.FieldByName('SA_UJKOD').AsString;
   szamladatum  := Query1.FieldByName('SA_KIDAT').AsString;
   Memo1.Lines.Add('A számlaszám   : ' + szamlaszam);
   Query_Run(Query3, 'SELECT S2_S2KOD FROM SZSOR2 WHERE S2_UJKOD = '+ saujkod);
   if Query3.RecordCount > 0 then begin
       // Van hozzá összesített számla
       Query_Run(Query3, 'SELECT SA_KIDAT FROM SZFEJ2 WHERE SA_UJKOD = ' + Query3.FieldByName('S2_S2KOD').AsString);
       szamladatum := Query3.FieldByName('SA_KIDAT').AsString;
       Memo1.Lines.Add('      összesített számla' );
   end;
   Memo1.Lines.Add('A kiáll. dátuma: ' + szamladatum);
   szamladatum := copy(szamladatum, 1, 4) + copy(szamladatum, 6, 2) + copy(szamladatum, 9, 2);
   Query_Run(Query3, 'SELECT SZ_MENEV FROM SZOTAR WHERE SZ_FOKOD = ''888'' AND SZ_ALKOD = ''131'' ');
   if Query3.RecordCount = 0 then begin
       Query_Run(Query4, 'INSERT INTO SZOTAR ( SZ_ALKOD, SZ_FOKOD, SZ_MENEV ) VALUES ( ''131'', ''888'', ''1'' )');
       mess_ref    := '000001';
   end else begin
       mess_ref    := Format('%6.6d', [StrToIntDef(Query3.FieldByName('SZ_MENEV').AsString, 0)+1]);
   end;
   sorszam     := 0;

   // A számla kibocsátó feltöltése
   cegnev       := 'J&S SPEED Kft.';
   cim          := 'Agostyáni út 91.';
   varos        := 'Tata';
   irsz         := '2890';
   orszag       := 'HU';
   NADII_sor    := 'NAD+II+++' + cegnev + '+' + cim + '+' + varos + '++' + irsz + '+' + orszag + '''';
   sajatadosz   := 'HU12939848';
   // A számla címzettje feltöltése
   // cegnev       := Query1.FieldByName('SA_VEVONEV').AsString;
   cegnev       := Nevbol_extrainfo_levesz(Query1.FieldByName('SA_VEVONEV').AsString);  // két azonos nevű TE Connectivity cég miatt
(*
   if copy(cegnev, Length(cegnev), 1)  <> '.' then begin
       cegnev  := cegnev + '.';
   end;
*)
   cim          := Query1.FieldByName('SA_VECIM').AsString;
   varos        := Query1.FieldByName('SA_VEVAROS').AsString;
   irsz         := Query1.FieldByName('SA_VEIRSZ').AsString;
   orszag       := Query_Select('VEVO', 'V_KOD', Query1.FieldByName('SA_VEVOKOD').AsString, 'V_ORSZ');
   if copy(cim, Length(cim), 1) = '.' then begin
       cim := copy(cim, 1, Length(cim)- 1);
   end;

   NADIV_sor    := 'NAD+IV+'+Query1.FieldByName('SA_VEVOKOD').AsString+'++' + cegnev + '+' + cim + '+' + varos + '++' + irsz + '+' + orszag + '''';
   // Az adószám összerakása
   szadoszam    := Query_Select('VEVO', 'V_KOD', Query1.FieldByName('SA_VEVOKOD').AsString, 'VE_AORSZ')+Query1.FieldByName('SA_EUADO').AsString;
   if StrToIntDef(Query1.FieldByName('SA_VEVOKOD').AsString , 0) = 547 then begin
       szadoszam    := 'CHE-116.347.444 MWST';
   end;

   // Az első felrakás meghatározása
   Query_Run(Query4, 'SELECT MS_FETDAT, MS_TEFNEV, MS_TEFCIM, MS_TEFTEL, MS_TEFIR, MS_TEFOR FROM MEGSEGED where ms_mbkod = '+mbkod+' AND MS_TIPUS = 0 ORDER BY MS_FETDAT, MS_FETIDO');
   felrakodat   := Query4.FieldByName('MS_FETDAT').AsString;
   felrakodat   := copy(felrakodat, 1, 4) + copy(felrakodat, 6, 2) + copy(felrakodat, 9, 2);
   if (felrakodat = '') and not MegbizasStandTrailer then begin
       NoticeKi('Hiányzó felrakás dátuma!');
       Exit;
   end;
   // Az utolsó lerakás meghatározása
   Query_Run(Query2, 'SELECT MS_LETDAT, MS_TENNEV, MS_TENCIM, MS_TENTEL, MS_TENIR, MS_TENOR FROM MEGSEGED where ms_mbkod = '+mbkod+' AND MS_TIPUS = 1 ORDER BY MS_LETDAT, MS_LETIDO');
   Query2.Last;
   lerakodat    := Query2.FieldByName('MS_LETDAT').AsString;
   lerakodat    := copy(lerakodat, 1, 4) + copy(lerakodat, 6, 2) + copy(lerakodat, 9, 2);
   if (lerakodat = '') and not MegbizasStandTrailer then begin
       NoticeKi('Hiányzó lerakás dátuma!');
       Exit;
   end;

   // A felrakó adatai
   // cegnev       := Query4.FieldByName('MS_TEFNEV').AsString;
   cegnev       := Nevbol_extrainfo_levesz(Query4.FieldByName('MS_TEFNEV').AsString);  // két azonos nevű TE Connectivity cég miatt
   cim          := Query4.FieldByName('MS_TEFCIM').AsString;
   varos        := Query4.FieldByName('MS_TEFTEL').AsString;
   irsz         := Query4.FieldByName('MS_TEFIR').AsString;
   orszag       := Query4.FieldByName('MS_TEFOR').AsString;
   NADSF_sor    := 'NAD+SF+++' + cegnev + '+' + cim + '+' + varos + '++' + irsz + '+' + orszag + '''';
   // A lerakó adatai
   // cegnev       := Query2.FieldByName('MS_TENNEV').AsString;
   cegnev       := Nevbol_extrainfo_levesz(Query2.FieldByName('MS_TENNEV').AsString);  // két azonos nevű TE Connectivity cég miatt
   cim          := Query2.FieldByName('MS_TENCIM').AsString;
   varos        := Query2.FieldByName('MS_TENTEL').AsString;
   irsz         := Query2.FieldByName('MS_TENIR').AsString;
   orszag       := Query2.FieldByName('MS_TENOR').AsString;
   NADST_sor    := 'NAD+ST+++' + cegnev + '+' + cim + '+' + varos + '++' + irsz + '+' + orszag + '''';
   // Ha minden jól ment, a legvégén beírunk minden elemet a file-ba
   Soriro('UNH+'+mess_ref+'+INVOIC:D:05B:UN''');
   if MegbizasStandTrailer then
     Soriro('BGM+383+' + szamlaszam + '+9''')  // "383 - Debit Note for Goods and Services"
   else
     Soriro('BGM+380+' + szamlaszam + '+9''');  // "380 - Commercial invoice"
//   Soriro('BGM+380+' + ConvertText(Query1.FieldByName('MB_POZIC').AsString) + '+9''');
   Soriro('DTM+3:' + szamladatum + ':102''');
   Soriro('FTX+ACB++' + mbkod + '''');
   Soriro(ConvertText(NADII_sor));
   Soriro(ConvertText(NADIV_sor));
   Soriro('CUX+1:EUR:16''');
   Soriro('PYT+2''');
   Soriro('PAI+::2''');
   Soriro('LIN+1''');
   Soriro('MEA+WT+AAD+KG:'+ConvertSzam(Format('%.2f', [StringSzam(suly)])) +'''');
   // A 'consolidated shippment' eldöntése
   if MegbizasStandTrailer then begin
      // végül nem így lett... TrailerConst:= Read_SZGrid('157', 'STAND_TRAILER_FAP');
      Soriro('FTX+ACB++' + aru_leiras + '''');
      end
   else begin   // normál számla
       if tobbloadid then begin
           Soriro('FTX+ACB++Consolidated with ' + loadidstr + '''');
       end else begin
           if ( ( Query1.FieldByName('MB_LECEG').AsString = 'Tyco GmbH Wört' ) AND
                ( Pos('ESZTERGOM', Uppercase(Query1.FieldByName('MB_FERAK').AsString ) ) > 0 ) ) then begin
               Soriro('FTX+ACB++' + 'consolidated shipment' + '''');
           end;
       end;
       // A több lerakás eldöntése
       Query_Run(Q2, 'select COUNT(*) AS DB from megseged where ms_mbkod = '+mbkod+' and ms_tipus = 1 and ms_fajko = 0');
       if StrToIntDef(Q2.FieldByName('DB').AsString, 0) > 1 then begin
           Soriro('FTX+ACB++' + 'MILKRUN' + '''');
       end;
       // Az egyedi díj eldöntése
       if StrToIntDef(Query1.FieldByName('MB_EDIJ').AsString, 0) > 0 then begin
           Soriro('FTX+ACB++' + 'out of rate card / non lan id rate' + '''');
       end;
       // Felrakó = lerakó = HU
       if ( ( Query1.FieldByName('MB_FORSZ').AsString = 'HU' ) and (Query1.FieldByName('MB_LORSZ').AsString = 'HU') ) then begin
           Soriro('FTX+ACB++' + 'domestic' + '''');
       end;

       if kellship then begin
           Soriro('FTX+ACB++' + 'No load id' + '''');
         end;
       end;  // end normál számla
   if StrToIntDef(Query1.FieldByName('MB_VEKOD').AsString, 0) = 6289 then  begin   // CSAK TESOG ESETÉN !!!
       Soriro('FTX+TXD+++' + 'Reverse Charge' + '''');
   end;

   Soriro('RFF+HWB:'+pozicio+'''');
   if MegbizasStandTrailer then begin
      Soriro('RFF+CR:'+EgyebDlg.Read_SZGrid('157', 'STAND_TRAILER_FAP')+'''');
      end
   else begin  // normál számla
      Soriro('RFF+CR:'+loadid+'''');
      end;
   Soriro('RFF+VA:'+sajatadosz+'''');
   Soriro('RFF+ZZZ:'+szadoszam+'''');
//   Soriro('RFF+FN:'+ConvertText(Query1.FieldByName('MB_POZIC').AsString)+'''');
   Soriro('RFF+FN:'+szamlaszam+'''');
   sorszam := 16;
   lanid   := Query_Select('FUVARTABLA', 'FT_FTKOD', Query1.FieldByName('MB_FUTID').AsString, 'FT_LANID');
   if lanid <> '' then begin
       Soriro('RFF+AWA:'+lanid+'''');
       sorszam := sorszam+1;
   end;
   Soriro('DTM+110:'+felrakodat+':102''');
   Soriro('DTM+35:'+lerakodat+':102''');
   Soriro('PAC+'+IntToStr(paletta)+'++PAL''');
   Soriro(ConvertText(NADSF_sor));
   Soriro(ConvertText(NADST_sor));
   sorszam := sorszam+5;
   // A tételek rögzítése
   ossznetto   := 0;
   osszafa     := 0;
   // A számla sor adatok kiírása
   JSzamla := TJSzamla.Create(Self);
   JSzamla.FillSzamla(saujkod);
   for i := 0 to JSzamla.Szamlasorok.Count-1 do begin
       JSzamlaSor  := (JSzamla.Szamlasorok[i] as TJSzamlaSor);
       if JSzamlaSor.afaszaz = 0 then
           leiras  := JSzamlaSor.termeknev;
       if Pos ('Közúti teherszállítás', leiras) > 0 then begin
           leiras  := 'Road transportation';
       end;
       if Pos ('Üzemanyagpótlék', leiras) > 0 then begin
           leiras  := 'Fuel surcharge';
       end;
       Soriro('ALC+C++++:::'+ConvertSzam(ConvertText(leiras))+'''');
       netto   := JSzamlaSor.sorneteur;
       afa     := JSzamlasor.sorafaeur;
       Soriro('MOA+23:'+ConvertSzam(Format('%.2f', [netto+afa])+''''));
       if afa = 0 then begin
           Soriro('TAX+7+VAT+++:::0+E''');
       end else begin
           Soriro('TAX+7+VAT+++:::'+ConvertSzam(Format('%f', [JSZamlaSor.afaszaz]))+'+S''');
       end;
       sorszam     := sorszam + 3;
       ossznetto   := ossznetto + netto;
       osszafa     := osszafa   + afa;
   end;
   // Az összesített sorok megjelenítése
   Soriro('TDT+21++1''');
   Soriro('TOD+6+001::INCOTERMS 1''');
   Soriro('UNS+S''');
   Soriro('MOA+9:'+ConvertSzam(Format('%.2f', [ossznetto+osszafa]))+'''');
   Soriro('MOA+125:'+ConvertSzam(Format('%.2f', [ossznetto]))+'''');
   Soriro('MOA+176:'+ConvertSzam(Format('%.2f', [osszafa]))+'''');
   sorszam     := sorszam + 7;
   Soriro('UNT+' + IntToStr(sorszam) + '+'+mess_ref+'''');
   Query_Run(Query4, 'UPDATE SZOTAR SET SZ_MENEV = '''+mess_ref+''' WHERE SZ_FOKOD = ''888'' AND SZ_ALKOD = ''131'' ');
   Result := true;
end;

function TFap_Export1Dlg.FileNyito(var fn1 : string) : boolean;
var
   fsz : integer;
begin
   // Állomány nyitása, ha van név, akkor azzal, ha nincs, akkor újjal
   Result := false;
   if fn1 = '' then begin
       // Nincs megadva név, generálunk
       fn1 := fapdir + 'JSSPEED_FAP_'+FormatDateTime('yyyymmdd', now)+'.edi';
       fsz := 1;
       while FileExists(fn1) do begin
           Inc(fsz);
           fn1  := fapdir + 'JSSPEED_FAP_'+FormatDateTime('yyyymmdd', now)+'_'+IntToStr(fsz)+'.edi';
       end;
   end;
   M6.Text := ExtractFileName(fn1);
   {$I-}
   AssignFile(file1, fn1);
   Rewrite(file1);
   {$I+}
   if IOResult <> 0 then begin
       // Állomány nyitási hiba
       Memo1.Lines.Add('File nyitási hiba : '+fn1);
       Exit;
   end;
   Result := true;
end;

procedure TFap_Export1Dlg.FileZaro;
begin
 	CloseFile(file1);
end;

procedure TFap_Export1Dlg.SorIro( sor : string);
begin
   if CH2.Checked then begin
       Writeln(file1, sor);
   end else begin
       Write(file1, sor);
   end;
end;

function TFap_Export1Dlg.ConvertText(str: string): string;
var
  res: string;
  i: Integer;
  mibol: string;
  mibe: string;
begin
  // Konvertálni kell UNOA karakterkészlet szerint (nagybetű, szám, szóköz, .,-()/=
  res := UpperCase(str);
  mibol := 'áúüíé';
  mibe := 'AUUIE';
  for i := 1 to Length(mibol) do
  begin
    while Pos(copy(mibol, i, 1), res) > 0 do
    begin
      res := copy(res, 1, Pos(copy(mibol, i, 1), res) - 1) + copy(mibe, i, 1) +
        copy(res, Pos(copy(mibol, i, 1), res) + 1, 999);
    end;
  end;
  Result := res;
end;

function TFap_Export1Dlg.ConvertSzam(str: string): string;
begin
   Result := StringReplace(str,',','.', [rfReplaceAll]);;
end;

procedure TFap_Export1Dlg.FejlecIro;
begin
   //  A fejléc kiírása
   Query_Run(Query4, 'SELECT SZ_MENEV FROM SZOTAR WHERE SZ_FOKOD = ''888'' AND SZ_ALKOD = ''130'' ');
   if Query4.RecordCount = 0 then begin
       Query_Run(Query4, 'INSERT INTO SZOTAR ( SZ_ALKOD, SZ_FOKOD, SZ_MENEV ) VALUES ( ''130'', ''888'', ''1'' )');
       int_ref    := '00001';
   end else begin
       int_ref    := Format('%5.5d', [StrToIntDef(Query4.FieldByName('SZ_MENEV').AsString, 0)+1]);
   end;
   SorIro( 'UNB+UNOA:1+JSSPEED+TYCOELECEMEA:ZZ+' + FormatDateTime('YYMMDD:hhnn', now) + '+'+int_ref+'''');
end;

procedure TFap_Export1Dlg.LablecIro;
begin
   // A lábléc kiírása
   SorIro('UNZ+'+IntToStr(mess_num)+'+'+int_ref+'''');
   FileZaro;
   Query_Run(Query4, 'UPDATE SZOTAR SET SZ_MENEV = '''+int_ref+''' WHERE SZ_FOKOD = ''888'' AND SZ_ALKOD = ''130'' ');
end;

procedure TFap_Export1Dlg.DatumSzerint;
var
   fn          : string;
   str         : string;
   elval       : string;
   i           : integer;
   sorsz       : integer;
   tip         : integer;
   mez         : TStringList;
   fsz         : integer;
begin

   // Dátum szerinti lekérdezés
   if not CH3.Checked then begin
       fn  := '';
       if not FileNyito(fn) then begin
           Exit;
       end;
   end;
   Memo1.Lines.Clear;
   Memo1.Lines.Add('START : ' + FormatDateTime ('hh:nn:ss', now) );
   Memo1.Lines.Add('');
   if not CH3.Checked then begin
       FejlecIro;
   end;
   elval := ';';
   if Pos(',', str) > 0 then begin
       elval := ',';
   end;
   // Lekérdezzük a megfelelő elemeket
   Query_Run(Q1, 'SELECT DISTINCT MB_MBKOD FROM MEGBIZAS, MEGSEGED WHERE MS_MBKOD = MB_MBKOD AND MB_VEKOD = ''6289'' AND MS_FELTEL = ''Esztergom'' AND MS_LERTEL = ''Pohorelice'' '+
       ' AND MB_DATUM >= '''+M3.Text+''' AND MB_DATUM <= '''+M4.Text+''' ');
   if Q1.RecordCount = 0 then begin
       NoticeKi('Nincs megfelelő elem!');
       if not CH3.Checked then begin
           FileZaro;
           DeleteFile(fn);
       end;
       Exit;
   end else begin
       while not Q1.Eof do begin
           Application.ProcessMessages;
           tip := 0;
           // bmkod alapján meghívjuk az állomány írót
           str := Q1.FieldByName('MB_MBKOD').AsString;
           if str <> '' then begin
               Inc(tetelszam);
               if CH3.Checked then begin
                   fn := fapdir + str+'.edi';
                   fsz := 1;
                   while FileExists(fn) do begin
                       Inc(fsz);
                       fn  := fapdir + str+'_'+IntToStr(fsz)+'.edi';
                   end;
                   if not FileNyito(fn) then begin
                       Exit;
                   end;
                   FejlecIro;
               end;
               if FileIro(str, tip) then begin
                   Inc(mess_num);
               end;
               if CH3.Checked then begin
                   LablecIro;
               end;
           end;
           Q1.Next;
       end;
   end;
   if not CH3.Checked then begin
       LablecIro;
   end;

   Memo1.Lines.Add('');
   Memo1.Lines.Add('STOP : ' + FormatDateTime ('hh:nn:ss', now) );
   Memo1.Lines.Add('');
end;

end.
