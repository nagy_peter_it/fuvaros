unit Gepkocsiujfm;

interface

uses
	SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
	StdCtrls, Forms, DBCtrls, DB, DBGrids, DBTables, Grids, ExtCtrls, Buttons,
	ShellApi, Dialogs, J_FOFORMUJ, ADODB, Mask, Menus, Variants;

type
	TGepkocsiUjFmDlg = class(TJ_FOFORMUJDLG)
	 QueryUj3: TADOQuery;
    BitBtn13: TBitBtn;
    BitBtn1: TBitBtn;
	  procedure FormCreate(Sender: TObject);override;
	 procedure Adatlap(cim, kod : string);override;
	 procedure ButtonKuldClick(Sender: TObject);override;
	 procedure ButtonTorClick(Sender: TObject);override;
    procedure BitBtn13Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
	 procedure DBGrid2DrawColumnCell(Sender: TObject; const Rect: TRect;
	   DataCol: Integer; Column: TColumn; State: TGridDrawState);override;
	 private
	 public
	end;

var
	GepkocsiUjFmDlg : TGepkocsiUjFmDlg;

implementation

uses
	Egyeb, J_SQL, GepkocsiBe, Kozos;

{$R *.DFM}

procedure TGepkocsiUjFmDlg.FormCreate(Sender: TObject);
begin
	Inherited FormCreate(Sender);
	EgyebDlg.SeTADOQueryDatabase(QueryUj3);
	kellujures	:= false;
	AddSzuromezoRovid( 'GK_ARHIV', 'GEPKOCSI' );
	AddSzuromezoRovid( 'GK_GKKAT', 'GEPKOCSI','1' );
	AddSzuromezoRovid( 'GK_TIPUS', 'GEPKOCSI' );
	AddSzuromezoRovid( 'GK_RESZ' , 'GEPKOCSI' );
	AddSzuromezoRovid( 'GK_KLIMA', 'GEPKOCSI' );
	AddSzuromezoRovid( 'GK_KULSO', 'GEPKOCSI' );
	AddSzuromezoRovid( 'GK_POTOS', 'GEPKOCSI' );
	AddSzuromezoRovid( 'GK_NEMZE', 'GEPKOCSI' );
	AddSzuromezoRovid( 'GK_RESZR', 'GEPKOCSI' );
	AddSzuromezoRovid( 'GK_LIVAN', 'GEPKOCSI' );
	AddSzuromezoRovid( 'GK_KIVON', 'GEPKOCSI' );
	AddSzuromezoRovid( 'GK_EUROB', 'GEPKOCSI' );
  AddSzuromezoRovid( 'GK_ELADO', '' ); // m�r nem haszn�ljuk az adatt�bla param�tert
	alapstr		:= 'Select * from GEPKOCSI where GK_RESZ <> ''''';
  {alapstr		:= 'Select GEPKOCSI.*, g1.GA_DATUM ATP_DATUM ' +
      ' from GEPKOCSI '+
      ' left join GARANCIA g1 on GA_RENSZ = GK_RESZ and GA_ALKOD = ''250'' '+
      ' where GK_RESZ <> ''''  ';   }
	FelTolto('�j g�pkocsi felvitele ', 'G�pkocsi adatok m�dos�t�sa ',
			'G�pkocsik list�ja', 'GK_KOD', alapstr, 'GEPKOCSI', QUERY_KOZOS_TAG );
	kulcsmezo			:= 'GK_KOD';
	nevmezo				:= 'GK_RESZ';
	width				:= DLG_WIDTH;
	height				:= DLG_HEIGHT;
	BitBtn13.Parent		:= PanelBottom;
	BitBtn13.Visible 	:= EgyebDlg.NavC_Kapcs_OK;
	BitBtn1.Parent		:= PanelBottom;
end;

procedure TGepkocsiUjFmDlg.ButtonKuldClick(Sender: TObject);
begin
	ret_vnev	:= Query1.FieldByName('GK_RESZ').AsString;
	Inherited ButtonKuldClick(Sender);
end;

procedure TGepkocsiUjFmDlg.ButtonTorClick(Sender: TObject);
begin
  if not EgyebDlg.user_super then
    exit;
	if not voltalt then begin
		voltalt 	:= true;
		Exit;
	end;
	if NoticeKi( TORLOTEXT, NOT_QUESTION ) = 0 then begin
		Query_Run(Query2, 'DELETE from GEPKOCSI where GK_RESZ  ='''+ Query1.FieldByName('GK_RESZ').AsString+''' ');
		Query_Run(Query2, 'DELETE FROM GARANCIA WHERE GA_RENSZ = ''' +Query1.FieldByName('GK_RESZ').AsString+ ''' ',true);
		Query_Run(Query2, 'DELETE FROM LIZING   WHERE LZ_RENSZ = ''' +Query1.FieldByName('GK_RESZ').AsString+ ''' ',true);
		// A rendsz�m t�rl�se a dolgoz�kt�l
		Query_Run(Query2, 'UPDATE DOLGOZO SET DO_RENDSZ = '''', DO_GKKAT = '''' WHERE DO_RENDSZ = ''' +Query1.FieldByName('GK_RESZ').AsString+ ''' ',true);
		ModLocate(Query1, FOMEZO, Query1.FieldByName(FOMEZO).AsString );
	end;
	Label3.Caption	:= '('+IntToStr(Query1.RecordCount)+')';
	Label3.Update;
end;

procedure TGepkocsiUjFmDlg.Adatlap(cim, kod : string);
begin
	Application.CreateForm(TGepkocsibeDlg, AlForm);
	Inherited Adatlap(cim, kod );
	GepStatTolto(Query1.FieldByName('GK_RESZ').AsString);
	PotStatTolto(Query1.FieldByName('GK_RESZ').AsString);
end;

procedure TGepkocsiUjFmDlg.BitBtn13Click(Sender: TObject);
begin
//  inherited;
 if Query1.FieldByName('GK_RESZ').AsString<>'' then
  EgyebDlg.GoogleMap(Query1.FieldByName('GK_RESZ').AsString,'','','', False)     ;

end;

procedure TGepkocsiUjFmDlg.BitBtn1Click(Sender: TObject);
var
	dir0, dir00	: string;
	dir		: string;
begin
	dir00	:= EgyebDlg.Read_SZGrid('999', '721');
	if dir00 = '' then begin
		// Be kell olvasni a kezd�k�nyvt�rat
	end else begin
		if not DirectoryExists(dir00) then begin
			NoticeKi('Nem l�tez� alapk�nyvt�r : '+dir0);
			Exit;
		end;
	end;
 Query1.First;
 while not Query1.Eof do
 begin
 if (Query1.FieldByName('GK_RESZ').AsString)<>'' then
 begin
	// A DIR0 egy l�tez� k�nyvt�r
  dir0:=dir00;
	if ( ( copy(dir0, Length(dir0), 0) <> '\' ) and ( copy(dir0, Length(dir0), 0) <> '/' ) ) then begin
		dir0	:= dir0 + '\';
	end;
	// A n�v hozz�rak�sa a dir0 -hoz
	dir0	:= dir0 + Query1.FieldByName('GK_RESZ').AsString;
	if not DirectoryExists(dir0) then begin
 //   if not CreateDir(dir0) then
 //   begin
 //		  NoticeKi('Nem l�tez�  g�pkocsi k�nyvt�r : "'+dir0+'"');
		  //Exit;
 //   end;
	end;
//	if (not DirectoryExists(dir0+'\Archiv'))and(not DirectoryExists(dir0+'\Arch�v')) then begin
	if (not DirectoryExists(dir0+'\Arch�v')) then begin
    if not CreateDir(dir0+'\Arch�v') then
    begin
		  NoticeKi('Nem l�tez�  Arch�v k�nyvt�r : "'+dir0+'\Arch�v'+'"');
		 // Exit;
    end;
	end;
	if not DirectoryExists(dir0+'\Foto') then begin
    if not CreateDir(dir0+'\Foto') then
    begin
		  NoticeKi('Nem l�tez�  Foto k�nyvt�r : "'+dir0+'\Foto'+'"');
		 // Exit;
    end;
	end;
 end;
  Query1.Next;
 end;
 ShowMessage('K�sz');
end;

procedure TGepkocsiUjFmDlg.DBGrid2DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
  //inherited;
	inherited DBGrid2DrawColumnCell(Sender, Rect, DataCol, Column, State);
  // ... az�rt ilyen durva g�nyt r�gen l�ttam......
  if Column.FieldName = 'GK_EUROB' then begin
		if Query1.FieldByName('GK_EUROB').AsString = '7' then begin
      DBGrid2.Canvas.FillRect(Rect);
      DBGrid2.Canvas.TextRect(Rect, Rect.Left, Rect.Top, '5EEV');
    end;
  end;
end;

end.


