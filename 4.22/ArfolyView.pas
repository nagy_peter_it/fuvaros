unit ArfolyView;

interface

uses
	SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
	Forms, Dialogs, StdCtrls, Buttons, Mask, DB, DBTables, ADODB;

type
	TArfolyViewDlg = class(TForm)
	BitKilep: TBitBtn;
    M2: TMaskEdit;
	Label8: TLabel;
	Label9: TLabel;
	Label12: TLabel;
    M1: TMaskEdit;
    Query1: TADOQuery;
    Label3: TLabel;
    Label1: TLabel;
    M3: TMaskEdit;
    Label2: TLabel;
    M4: TMaskEdit;
    Label4: TLabel;
	procedure FormCreate(Sender: TObject);
	procedure Tolto(dat1, dat2 : string);
    procedure BitKilepClick(Sender: TObject);
	private
	public
	end;

var
	ArfolyViewDlg: TArfolyViewDlg;

implementation

uses
	Egyeb, J_SQL, Kozos;
{$R *.DFM}

procedure TArfolyViewDlg.FormCreate(Sender: TObject);
begin
 	EgyebDlg.SetADOQueryDatabase(Query1);
end;

procedure TArfolyViewDlg.Tolto(dat1, dat2 : string);
begin
	M1.Text := dat1;
	M2.Text := SzamString(EgyebDlg.ArfolyamErtek('EUR', M1.Text, true),12,4);
	M3.Text := dat2;
	M4.Text := SzamString(EgyebDlg.ArfolyamErtek('EUR', M3.Text, true),12,4);
end;

procedure TArfolyViewDlg.BitKilepClick(Sender: TObject);
begin
	Close;
end;

end.
