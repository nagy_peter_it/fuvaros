unit Lejartszamla;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, DB, ADODB, StdCtrls, Buttons, Grids, DBGrids, Mask,
  DBCtrls;

type
  TFLejartszamla = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    Panel3: TPanel;
    BitBtn2: TBitBtn;
    Query1: TADOQuery;
    DataSource1: TDataSource;
    DBGrid1: TDBGrid;
    Query2: TADOQuery;
    Query2sa_vevokod: TStringField;
    Query2sa_vevonev: TStringField;
    Query2db: TIntegerField;
    Query2hnap: TIntegerField;
    Query2lnap: TBCDField;
    Query2moral: TIntegerField;
    Query1sa_kod: TStringField;
    Query1sa_vevokod: TStringField;
    Query1sa_vevonev: TStringField;
    Query1sa_kidat: TStringField;
    Query1sa_esdat: TStringField;
    Query1sa_valoss: TBCDField;
    Query1sa_valafa: TBCDField;
    Query1sa_valnem: TStringField;
    Query1sa_osszeg: TBCDField;
    Query1sa_afa: TBCDField;
    Query1sa_gyujto: TStringField;
    Query1sa_lnap: TIntegerField;
    Edit1: TEdit;
    Edit2: TEdit;
    Label1: TLabel;
    Edit3: TEdit;
    Label2: TLabel;
    Label3: TLabel;
    Query1sa_ujkod: TBCDField;
    BitSzamla: TBitBtn;
    Edit4: TEdit;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Query3: TADOQuery;
    Query3osszeg: TBCDField;
    DBEdit1: TDBEdit;
    DataSource3: TDataSource;
    procedure FormShow(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure BitSzamlaClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FLejartszamla: TFLejartszamla;
  vevokod: string;

implementation

uses Egyeb, SzamlaNezo, J_SQL;

{$R *.dfm}

procedure TFLejartszamla.FormShow(Sender: TObject);
var
  hnap,lnap: integer;
begin
  vevokod:=Query1.Parameters[0].Value;
  Query3.Close;
  Query3.Parameters[0].Value:=vevokod;
  Query3.Open;
  Query3.First;
  Query2.Close;
  Query2.Parameters[0].Value:=vevokod;
  Query2.Open;
  Query2.First;

  Edit1.Text:=vevokod;
  if not(Query2.Eof) then begin   // m�r volt kifizetett sz�ml�ja
    Edit2.Text:=Query2sa_vevonev.Value;
    Edit3.Text:=Query2moral.AsString+' %';
    Label2.Caption:='('+Query2db.AsString+' db sz�mla alapj�n)';
    end
  else begin
    Edit2.Text:=Query_Select('VEVO', 'V_KOD', vevokod, 'V_NEV');
    Edit3.Text:='-';
    Label2.Caption:='(0 db sz�mla alapj�n)';
    end;
  hnap:=0;
  lnap:=0;
  if Query2db.AsInteger<>0 then
  begin
    hnap:= Trunc(Query2hnap.Value/Query2db.Value);
    lnap:= Trunc(Query2lnap.Value/Query2db.Value);
  end;  
  Edit4.Text:= IntToStr(hnap)+' / '+IntToStr(hnap+lnap);

  Query2.Close;

end;

procedure TFLejartszamla.BitBtn2Click(Sender: TObject);
begin
  close;
end;

procedure TFLejartszamla.DBGrid1DblClick(Sender: TObject);
begin
		Application.CreateForm(TSzamlaNezoDlg, SzamlaNezoDlg);
	 //	SzamlaNezoDlg.SzamlaNyomtatas(StrToIntDef(ret_kod,0), 0);
    if Query1sa_gyujto.Value='0' then
    begin
  		SzamlaNezoDlg.SzamlaNyomtatas(Query1sa_ujkod.AsInteger, 0);
    end
    else
    begin
      SzamlaNezoDlg.OsszesitettNyomtatas(Query1sa_ujkod.AsInteger, 0);
    end;
 		SzamlaNezoDlg.Destroy;

end;

procedure TFLejartszamla.BitSzamlaClick(Sender: TObject);
begin
  DBGrid1.SetFocus;
  DBGrid1.OnDblClick(self);
end;

end.
