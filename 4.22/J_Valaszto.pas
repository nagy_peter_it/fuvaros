unit J_VALASZTO;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Mask, Kozos, Kozos_Local;

	procedure DolgozoValaszto( MKod , MNev : TMAskEdit) ;
  procedure DolgozoValaszto2( MKod , MNev : TMAskEdit; ArchivNemKell: boolean) ;
  procedure TobbDolgozoValaszto2( MKod , MNev : TMAskEdit; ArchivNemKell: boolean) ;
  procedure TelElofizetValaszto2( MKod: TMAskEdit; ArchivNemKell: boolean) ;
  procedure TelefonValaszto2( MKod: TMAskEdit; ArchivNemKell: boolean) ;
  procedure PartnerValaszto( MKod , MNev : TMAskEdit);
  procedure TobbPartnerValaszto( MKod , MNev : TMAskEdit) ;
  procedure GepkocsiValaszto( ms : TMAskEdit; gkfajta : integer = GK_MINDENGK; lehetArchive: boolean = True);
	procedure GepkocsiValaszto2( ms : TMAskEdit; gkkategoria : string) ;
  procedure AlGepkocsiValaszto( ms : TMAskEdit; alkod : string; gkfajta : integer = GK_MINDENGK; lehetArchive: boolean = True);
	procedure TobbGepkocsiValaszto( kod, rsz : TMaskEdit;  rsz_str : string; gkkategoria : string = '') ;
  procedure TelKiegValaszto( MKod: TMAskEdit; CsakKiadhato: boolean) ;
	procedure JaratValaszto( MJaratKod , MJaratSzam : TMAskEdit; i_kellujures: boolean = true) ;
	function  FokonyvValaszto( regifok : string) : string;

type
  TForm1 = class(TForm)
  private
  public
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

uses
	Egyeb, DolgozoUjFm, GepkocsiUjFm, JaratFm, FokonyvBe, J_SQL, AlGepFm ,
  StrUtils, VevoUjFm, TelefonFm, TelElofizetFm, TelKiegFm;

procedure DolgozoValaszto( MKod , MNev : TMAskEdit) ;
begin
	DolgozoValaszto2( MKod , MNev, False);
end;

procedure DolgozoValaszto2( MKod , MNev : TMAskEdit; ArchivNemKell: boolean) ;
begin
	{Dolgoz� v�laszt�s}
	Application.CreateForm(TDolgozoUjFmDlg,DolgozoUjFmDlg);
  if ArchivNemKell then begin
    DolgozoUjFmDlg.AlapStr := DolgozoUjFmDlg.AlapStr + ' and DO_ARHIV=0 ';
		DolgozoUjFmDlg.FelTolto('�j dolgoz� felvitele ', 'Dolgoz�i adatok m�dos�t�sa ',
			'Dolgoz�k list�ja', 'DO_KOD', DolgozoUjFmDlg.alapstr, 'DOLGOZO', QUERY_KOZOS_TAG );
    end;  // if
	DolgozoUjFmDlg.valaszt	:= true;
	DolgozoUjFmDlg.ShowModal;
	if DolgozoUjFmDlg.ret_vkod <> '' then begin
		MKod.Text 		:= DolgozoUjFmDlg.ret_vkod;
		MNev.Text 		:= DolgozoUjFmDlg.ret_vnev;
	end;
	DolgozoUjFmDlg.Destroy;
end;

procedure TobbDolgozoValaszto2( MKod , MNev : TMAskEdit; ArchivNemKell: boolean) ;
var
  i: integer;
  SKOD, SNEV: string;
begin
	{Dolgoz� v�laszt�s}
	Application.CreateForm(TDolgozoUjFmDlg,DolgozoUjFmDlg);
  if ArchivNemKell then begin
    DolgozoUjFmDlg.AlapStr := DolgozoUjFmDlg.AlapStr + ' and DO_ARHIV=0 ';
		DolgozoUjFmDlg.FelTolto('�j dolgoz� felvitele ', 'Dolgoz�i adatok m�dos�t�sa ',
			'Dolgoz�k list�ja', 'DO_NAME', DolgozoUjFmDlg.alapstr, 'DOLGOZO', QUERY_KOZOS_TAG );
    end;  // if
  DolgozoUjFmDlg.kulcsmezo:= 'DO_KOD';
  DolgozoUjFmDlg.nevmezo:= 'DO_NAME';
	DolgozoUjFmDlg.valaszt	:= true;
  DolgozoUjFmDlg.multiselect	:= true;
	DolgozoUjFmDlg.ShowModal;
	SKOD:= '';
  SNEV:= '';
  if DolgozoUjFmDlg.selectlist.Count > 0 then begin
		for i := 0 to DolgozoUjFmDlg.selectlist.Count - 1 do begin
      SKOD:= Felsorolashoz(SKOD, DolgozoUjFmDlg.Selectlist[i], ',', False);
      SNEV:= Felsorolashoz(SNEV, DolgozoUjFmDlg.SelectNevlist[i], ',', False);
		  end;  // for
    end;  // if
  MKod.Text:= SKOD;
  MNev.Text:= SNEV;
	DolgozoUjFmDlg.Destroy;
end;

procedure TelElofizetValaszto2( MKod: TMAskEdit; ArchivNemKell: boolean) ;
begin
	{Dolgoz� v�laszt�s}
	Application.CreateForm(TTelElofizetFormDlg,TelElofizetFormDlg);
  if ArchivNemKell then begin
    TelElofizetFormDlg.AlapStr := TelElofizetFormDlg.AlapStr + ' and TE_STATUSKOD = 1 ';
	  TelElofizetFormDlg.FelTolto('�j el�fizet�s felvitele ', 'El�fizet�s adatok m�dos�t�sa ',
			'Telefon el�fizet�sek list�ja', 'TE_ID', TelElofizetFormDlg.alapstr, 'TELELOFIZETES', QUERY_KOZOS_TAG );
    end;  // if
	TelElofizetFormDlg.valaszt	:= true;
	TelElofizetFormDlg.ShowModal;
	if TelElofizetFormDlg.ret_vkod <> '' then begin
		MKod.Text:= TelElofizetFormDlg.ret_vkod;
	end;
	TelElofizetFormDlg.Destroy;
end;

procedure TelefonValaszto2( MKod: TMAskEdit; ArchivNemKell: boolean) ;
begin
	{Dolgoz� v�laszt�s}
	Application.CreateForm(TTelefonFormDlg,TelefonFormDlg);
  if ArchivNemKell then begin
    TelefonFormDlg.AlapStr := TelefonFormDlg.AlapStr + ' and TK_STATUSKOD = 1 ';
    TelefonFormDlg.FelTolto('�j telefonk�sz�l�k felvitele ', 'Telefonk�sz�l�k adatok m�dos�t�sa ',
			'Telefonk�sz�l�kek list�ja', 'TK_ID', TelefonFormDlg.alapstr, 'TELKESZULEK', QUERY_KOZOS_TAG );
    end;  // if
	TelefonFormDlg.valaszt	:= true;
	TelefonFormDlg.ShowModal;
	if TelefonFormDlg.ret_vkod <> '' then begin
		MKod.Text:= TelefonFormDlg.ret_vkod;
	end;
	TelefonFormDlg.Destroy;
end;

procedure TelKiegValaszto( MKod: TMAskEdit; CsakKiadhato: boolean) ;
begin
	Application.CreateForm(TTelKiegFormDlg, TelKiegFormDlg);
  if CsakKiadhato then begin
    TelKiegFormDlg.AlapStr := TelKiegFormDlg.AlapStr + ' and SK = 1 and isnull(DK,'''')='''' ';  // st�tusz k�d �s dolgoz� k�d
    TelKiegFormDlg.FelTolto('�j telefon tartoz�k felvitele ', 'Telefon tartoz�k adatok m�dos�t�sa ',
			'Telefon tartoz�kok list�ja', 'SORSZAM', TelKiegFormDlg.AlapStr, 'TELKIEG', QUERY_KOZOS_TAG );
    end;  // if
	TelKiegFormDlg.valaszt	:= true;
	TelKiegFormDlg.ShowModal;
	if TelKiegFormDlg.ret_vkod <> '' then begin
		MKod.Text:= TelKiegFormDlg.ret_vkod;
	end;
	TelKiegFormDlg.Destroy;
end;

procedure PartnerValaszto( MKod , MNev : TMAskEdit);
begin
  Application.CreateForm(TVevoUjFmDlg, VevoUjFmDlg);
  try
    VevoUjFmDlg.valaszt	:= true;
    VevoUjFmDlg.ShowModal;
    if VevoUjFmDlg.ret_vkod <> '' then begin
      MKod.Text	:= VevoUjFmDlg.ret_vkod;
      MNev.Text	:= VevoUjFmDlg.ret_vnev;
      end; // if
  finally
    VevoUjFmDlg.Destroy;
    end;  // try-finally
end;

procedure TobbPartnerValaszto( MKod , MNev : TMAskEdit) ;
var
  SKOD, SNEV: string;
  i: integer;
begin
  Application.CreateForm(TVevoUjFmDlg, VevoUjFmDlg);
  try
    VevoUjFmDlg.valaszt	:= true;
    VevoUjFmDlg.multiselect	:= true;
    VevoUjFmDlg.ShowModal;
		SKOD:= '';
		SNEV:= '';
    if VevoUjFmDlg.selectlist.Count > 0 then begin
      for i := 0 to VevoUjFmDlg.selectlist.Count - 1 do begin
        SKOD:= Felsorolashoz(SKOD, VevoUjFmDlg.Selectlist[i], ',', False);
        SNEV:= Felsorolashoz(SNEV, VevoUjFmDlg.SelectNevlist[i], ',', False);
        end;  // for
      end;  // if
    MKod.Text:= SKOD;
    MNev.Text:= SNEV;
  finally
  	VevoUjFmDlg.Destroy;
    end;  // try-finally
end;

procedure GepkocsiValaszto( ms : TMAskEdit; gkfajta : integer = GK_MINDENGK; lehetArchive: boolean = True);
begin
	Application.CreateForm(TGepkocsiUjFmDlg,GepkocsiUjFmDlg);
   if gkfajta = GK_POTKOCSI then begin
		GepkocsiUjFmDlg.ALAPSTR := 'Select * from GEPKOCSI WHERE GK_POTOS = 1 ';
    if not lehetArchive then
    		GepkocsiUjFmDlg.ALAPSTR := GepkocsiUjFmDlg.ALAPSTR + ' AND GK_ARHIV = 0 ';
		GepkocsiUjFmDlg.FelTolto('�j g�pkocsi felvitele ', 'G�pkocsi adatok m�dos�t�sa ',
			'G�pkocsik list�ja', 'GK_KOD', GepkocsiUjFmDlg.alapstr, 'GEPKOCSI', QUERY_KOZOS_TAG );
	end;
	if gkfajta = GK_TRAKTOR then begin
		GepkocsiUjFmDlg.ALAPSTR := 'Select * from GEPKOCSI WHERE GK_POTOS <> 1 ';
    if not lehetArchive then
    		GepkocsiUjFmDlg.ALAPSTR := GepkocsiUjFmDlg.ALAPSTR + ' AND GK_ARHIV = 0 ';
		GepkocsiUjFmDlg.FelTolto('�j g�pkocsi felvitele ', 'G�pkocsi adatok m�dos�t�sa ',
			'G�pkocsik list�ja', 'GK_KOD', GepkocsiUjFmDlg.alapstr, 'GEPKOCSI', QUERY_KOZOS_TAG );
	end;

	GepkocsiUjFmDlg.valaszt	:= true;
 //	GepkocsiUjFmDlg.multiselect	:= true;
	if GepkocsiUjFmDlg.mentettfo = '' then begin
		GepkocsiUjFmDlg.Query1.First;
	end else begin
		GepkocsiUjFmDlg.Query1.Locate(GepkocsiUjFmDlg.FOMEZO, GepkocsiUjFmDlg.mentettfo, [] );
	end;
	GepkocsiUjFmDlg.ShowModal;
	if GepkocsiUjFmDlg.ret_vkod <> '' then begin
		ms.Text := GepkocsiUjFmDlg.ret_vnev;
	end;
	GepkocsiUjFmDlg.Destroy;
end;

procedure GepkocsiValaszto2( ms : TMAskEdit; gkkategoria : string) ;
begin
	Application.CreateForm(TGepkocsiUjFmDlg,GepkocsiUjFmDlg);
	GepkocsiUjFmDlg.ALAPSTR := 'Select * from GEPKOCSI WHERE GK_GKKAT in ('+gkkategoria+')';
	GepkocsiUjFmDlg.FelTolto('�j g�pkocsi felvitele ', 'G�pkocsi adatok m�dos�t�sa ',
			'G�pkocsik list�ja', 'GK_KOD', GepkocsiUjFmDlg.alapstr, 'GEPKOCSI', QUERY_KOZOS_TAG );
	GepkocsiUjFmDlg.valaszt	:= true;
	GepkocsiUjFmDlg.multiselect	:= true;
	if GepkocsiUjFmDlg.mentettfo = '' then begin
		GepkocsiUjFmDlg.Query1.First;
	end else begin
		GepkocsiUjFmDlg.Query1.Locate(GepkocsiUjFmDlg.FOMEZO, GepkocsiUjFmDlg.mentettfo, [] );
	end;
	GepkocsiUjFmDlg.ShowModal;
	if GepkocsiUjFmDlg.ret_vkod <> '' then begin
		ms.Text :=ms.Text+IfThen(ms.Text='','',',')+ GepkocsiUjFmDlg.ret_vnev;
	end;
	GepkocsiUjFmDlg.Destroy;
end;

procedure AlGepkocsiValaszto( ms : TMAskEdit; alkod : string; gkfajta : integer = GK_MINDENGK; lehetArchive: boolean = True);
begin
	Application.CreateForm(TAlGepFmDlg, AlGepFmDlg);
  try
      if (gkfajta = GK_POTKOCSI) or (gkfajta = GK_TRAKTOR) or (alkod <> '') or (not lehetArchive) then begin  // van specifikus felt�tel
        AlGepFmDlg.ALAPSTR := 'Select * from ALGEPKOCSI WHERE 1=1 ';
        if gkfajta = GK_POTKOCSI then
            AlGepFmDlg.ALAPSTR := AlGepFmDlg.ALAPSTR + ' and AG_POTOS = 1 ';
        if gkfajta = GK_TRAKTOR then
            AlGepFmDlg.ALAPSTR := AlGepFmDlg.ALAPSTR + ' and AG_POTOS = 0 ';
        if alkod <> '' then
            AlGepFmDlg.ALAPSTR := AlGepFmDlg.ALAPSTR + ' AND AG_AVKOD = '''+alkod+''' ';
        if not lehetArchive then
            AlGepFmDlg.ALAPSTR := AlGepFmDlg.ALAPSTR + ' AND AG_ARHIV = 0 ';
        AlGepFmDlg.FelTolto('�j alv�llalkoz�i g�pkocsi felvitele ', 'Alv�llalkoz�i g�pkocsi adatok m�dos�t�sa ',
          'Alv�llalkoz�i g�pkocsik list�ja', 'AG_AGKOD', AlGepFmDlg.alapstr, 'ALGEPKOCSI', QUERY_KOZOS_TAG );
        end; // if specifikus felt�tel

      {	if gkfajta = GK_POTKOCSI then begin
        if alkod = '' then begin
          AlGepFmDlg.ALAPSTR := 'Select * from ALGEPKOCSI WHERE AG_POTOS = 1 ';
        end else begin
          AlGepFmDlg.ALAPSTR := 'Select * from ALGEPKOCSI WHERE AG_POTOS = 1 AND AG_AVKOD = '''+alkod+''' ';
        end;
        AlGepFmDlg.FelTolto('�j alv�llalkoz�i g�pkocsi felvitele ', 'Alv�llalkoz�i g�pkocsi adatok m�dos�t�sa ',
          'ALv�llalkoz�i g�pkocsik list�ja', 'AG_AGKOD', AlGepFmDlg.alapstr, 'ALGEPKOCSI', QUERY_KOZOS_TAG );
      end;
      if gkfajta = GK_TRAKTOR then begin
        if alkod = '' then begin
          AlGepFmDlg.ALAPSTR := 'Select * from ALGEPKOCSI WHERE AG_POTOS = 0 ';
        end else begin
          AlGepFmDlg.ALAPSTR := 'Select * from ALGEPKOCSI WHERE AG_POTOS = 0 AND AG_AVKOD = '''+alkod+''' ';
        end;
        AlGepFmDlg.FelTolto('�j alv�llalkoz�i g�pkocsi felvitele ', 'Alv�llalkoz�i g�pkocsi adatok m�dos�t�sa ',
          'ALv�llalkoz�i g�pkocsik list�ja', 'AG_AGKOD', AlGepFmDlg.alapstr, 'ALGEPKOCSI', QUERY_KOZOS_TAG );
      end;
      if ( ( gkfajta <> GK_POTKOCSI ) and ( gkfajta <> GK_TRAKTOR ) and (alkod <> '') ) then begin
        AlGepFmDlg.ALAPSTR := 'Select * from ALGEPKOCSI WHERE AG_AVKOD = '''+alkod+''' ';
        AlGepFmDlg.FelTolto('�j alv�llalkoz�i g�pkocsi felvitele ', 'Alv�llalkoz�i g�pkocsi adatok m�dos�t�sa ',
          'ALv�llalkoz�i g�pkocsik list�ja', 'AG_AGKOD', AlGepFmDlg.alapstr, 'ALGEPKOCSI', QUERY_KOZOS_TAG );
      end;
    }

      AlGepFmDlg.valaszt	:= true;
      if AlGepFmDlg.mentettfo = '' then begin
        AlGepFmDlg.Query1.First;
      end else begin
        AlGepFmDlg.Query1.Locate(AlGepFmDlg.FOMEZO, AlGepFmDlg.mentettfo, [] );
      end;
      AlGepFmDlg.ShowModal;
      if AlGepFmDlg.ret_vkod <> '' then begin
        ms.Text := AlGepFmDlg.ret_vnev;
      end;
  finally
    	if AlGepFmDlg<> nil then AlGepFmDlg.Release;
      end;  // try-finally
end;

procedure JaratValaszto( MJaratKod , MJaratSzam : TMAskEdit; i_kellujures: boolean = true) ;
begin
	Application.CreateForm(TJaratFormDlg,JaratFormDlg);
	JaratFormDlg.valaszt	:= true;
  JaratFormDlg.kellujures:= i_kellujures;
	JaratFormDlg.ShowModal;
	if JaratformDlg.ret_vkod <> '' then
  begin
		MJaratKod.Text		:= JaratformDlg.ret_vkod;
		MJaratSzam.Text 	:= GetJaratszam(JaratformDlg.ret_vkod);
	end
  else
  begin
		MJaratKod.Text		:= '';
		MJaratSzam.Text 	:= '';
	end  ;
	EgyebDlg.SzuroGrid.RowCount  := 1;
	EgyebDlg.SzuroGrid.Rows[0].Clear;
	JaratformDlg.Destroy;
end;

function FokonyvValaszto( regifok : string) : string;
begin
	Application.CreateForm(TFokonybeDlg,FokonybeDlg);
	FokonybeDlg.Tolt(regifok);
	FokonybeDlg.ShowModal;
	if FokonybeDlg.ret_fokonyv <> '' then begin
		Result	:= FokonybeDlg.ret_fokonyv;
	end else begin
		Result	:= '';
	end;
	FokonybeDlg.Destroy;
end;

//procedure TobbGepkocsiValaszto( kod, rsz : TMaskEdit; var rsz_str : string; gkkategoria : string = '') ;
procedure TobbGepkocsiValaszto( kod, rsz : TMaskEdit; rsz_str : string;  gkkategoria : string = '') ;
var
	i : integer;
begin
	Application.CreateForm(TGepkocsiUjFmDlg, GepkocsiUjFmDlg);
  if gkkategoria<>'' then
  begin
  	GepkocsiUjFmDlg.ALAPSTR := 'Select * from GEPKOCSI WHERE GK_GKKAT in ('+gkkategoria+')';
	  GepkocsiUjFmDlg.FelTolto('�j g�pkocsi felvitele ', 'G�pkocsi adatok m�dos�t�sa ',
			'G�pkocsik list�ja', 'GK_KOD', GepkocsiUjFmDlg.alapstr, 'GEPKOCSI', QUERY_KOZOS_TAG );
	  GepkocsiUjFmDlg.nevmezo				:= 'GK_RESZ';

  end;
	GepkocsiUjFmDlg.valaszt		:= true;
	GepkocsiUjFmDlg.multiselect	:= true;
	GepkocsiUjFmDlg.ShowModal;
	kod.Text	:= '';
	rsz.Text    := '';
	rsz_str		:= '';
	if GepkocsiUjFmDlg.selectlist.Count > 0 then begin
		kod.Text	:= '';
		rsz.Text	:= '';
		for i := 0 to GepkocsiUjFmDlg.selectlist.Count - 1 do begin
			kod.Text   	:= kod.Text  	+ GepkocsiUjFmDlg.SelectNevlist[i]+',';
			rsz.Text	:= rsz.Text 	+ GepkocsiUjFmDlg.Selectlist[i]+',';
			rsz_str		:= rsz_str 		+ ''''+GepkocsiUjFmDlg.SelectNevlist[i]+''''+',';
		end;
		rsz_str		:= rsz_str + '''zzzz''';
	end;
	GepkocsiUjFmDlg.Destroy;
end;

end.
