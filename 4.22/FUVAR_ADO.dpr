program FUVAR_ADO;

uses
  Forms,
  ShellApi,
  Windows,
  SysUtils,
  Classes,
  Variants,
  Controls,
  Dialogs,
  NyitoKep in 'NyitoKep.pas' {NyitoKepDlg},
  Kozos in 'Kozos.pas' {NyitoKepDlg},
  Jelszo in 'Jelszo.pas' {JelszoDlg},
  Fomenu in 'Fomenu.pas' {FomenuDlg},
  ActiveX,
  Egyeb in 'Egyeb.pas' {EgyebDlg},
  GKErvenyesseg in 'GKErvenyesseg.pas' {FGKErvenyesseg},
  Vcl.Themes,
  Vcl.Styles,
  KulsoFile in 'KulsoFile.pas' {KulsoFileDlg},
  ComboForm in 'ComboForm.pas' {ComboFormDlg},
  Proba in 'Proba.pas' {ProbaDlg},
  GeoAddress in 'GeoAddress.pas',
  T_DictCSV in 'T_DictCSV.pas',
  UzemaExport in 'UzemaExport.pas' {UzemaExportDlg},
  SMSKuld in 'SMSKuld.pas' {SMSKuldDlg},
  HTTPRutinok in 'HTTPRutinok.pas',
  J_SQL in 'J_SQL.pas',
  info_ in 'info_.pas',
  MunkalapReszlet in 'MunkalapReszlet.pas' {MunkalapReszletDlg},
  _proba_Cimsor in '_proba_Cimsor.pas' {FlowPanelDlg},
  FogyasztSzamolo in 'FogyasztSzamolo.pas' {FogyasztSzamoloDlg},
  DolgozoFotoMutat in 'DolgozoFotoMutat.pas' {DolgozoFotoMutatDlg},
  PictureUtils in 'PictureUtils.pas',
  VCardRoutines in 'VCardRoutines.pas',
  _proba_szamitas in '_proba_szamitas.pas' {Proba_szamitas},
  FelrakoValaszt in 'FelrakoValaszt.pas' {FelrakoValasztDlg},
  FlottaHelyzet in 'FlottaHelyzet.pas' {FlottaHelyzetDlg},
  DolgozoTablo in 'DolgozoTablo.pas' {DolgozoTabloDlg},
  Kutallapot in 'Kutallapot.pas' {KutAllapotDlg},
  UzenetSzerkeszto in 'UzenetSzerkeszto.pas' {UzenetSzerkesztoDlg},
  Edifact_Invoic in 'Edifact_Invoic.pas',
  Email in 'Email.pas' {EmailDlg},
  RESTAPI_Kozos in 'RESTAPI_Kozos.pas' {EmailDlg},
  UzenetTop in 'UzenetTop.pas' {NotificationTopDlg},
  URLCache in 'URLCache.pas' {URLCacheDlg};

{$R *.res}

{$SetPEOptFlags IMAGE_DLLCHARACTERISTICS_TERMINAL_SERVER_AWARE}
{$SetPEFlags IMAGE_FILE_RELOCS_STRIPPED
  or IMAGE_FILE_REMOVABLE_RUN_FROM_SWAP
  or IMAGE_FILE_NET_RUN_FROM_SWAP
  or IMAGE_FILE_LARGE_ADDRESS_AWARE}

var
  Hiany: string;
begin

	Screen.Cursor	:= crHourGlass;
	NyitoKepDlg 	:= TNyitoKepDlg.Create(Application);
	if NyitoKepDlg.vankep then begin
		NyitoKepDlg.Show;
		NyitoKepDlg.Update;
	end;

	//  Sleep(2000);
	Coinitialize(nil);
	EgyebDlg 			:= TEgyebDlg.Create(Application);
	EgyebDlg.v_evszam	:= '0';
	EgyebDlg.AlapToltes;
	if ( EgyebDlg.VanAdat ) then begin
    JelszoDlg   	:= TJelszoDlg.Create(Application);
		JelszoDlg.ShowModal;
    JelszoDlg.Destroy;
		if EgyebDlg.user_code <> '' then begin
	 		EgyebDlg.SqlInit;
      EgyebDlg.AlapadatToltes;
	 		Application.Title := 'Fuvaros';
      Application.CreateForm(TFomenuDlg, FomenuDlg);
  Application.CreateForm(TEmailDlg, EmailDlg);
  Application.CreateForm(TURLCacheDlg, URLCacheDlg);
  FomenuDlg.WindowState:= wsMaximized;
      Application.CreateForm(TFGKErvenyesseg, FGKErvenyesseg);
      Application.CreateForm(TUzenetSzerkesztoDlg, UzenetSzerkesztoDlg); // a partner �zenetekhez haszn�ljuk
      Application.CreateForm(TRESTAPIKozosDlg, RESTAPIKozosDlg);
      Hiany:= '';
      if (EgyebDlg.MobilAppTelszam = '') then
         Hiany:= Kozos.Felsorolashoz(Hiany, 'telefonsz�m', ' �s ', False);
      if (EgyebDlg.MobilAppJelszo = '') then
         Hiany:= Kozos.Felsorolashoz(Hiany, 'jelsz�', ' �s ', False);

      // if (Hiany <> '') then begin
      //   NoticeKi('Nincs �rv�nyes '+Hiany+', nem tudsz bel�pni az �zenet modulba!');
      //   end
      //else begin  // csak ha OK
      if (Hiany = '') then
         Application.CreateForm(TUzenetTopDlg, UzenetTopDlg);
      //  end; // else
      // ---- Ide tettem 2016.10.03. NagyP
      // if EgyebDlg.user_kellinfoablak then
      if GetRightTag(599, False) <> RG_NORIGHT then
        Application.CreateForm(TFInfo_, FInfo_);

      if (EgyebDlg.SMSEAGLE_DOMAIN <> '') then FomenuDlg.SMS1.Enabled:= True;

      // NyitoKepDlg.Destroy;
      Screen.Cursor	:= crDefault;
      Application.Run;
      if FRestart then begin
         WinExec(PAnsiChar(Application.ExeName), SW_SHOWDEFAULT);
         end;
      end
    else begin
      NyitoKepDlg.Destroy;
      end;
	  end;
	EgyebDlg.Destroy;

end.


