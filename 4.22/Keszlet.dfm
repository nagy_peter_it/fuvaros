object FKeszlet: TFKeszlet
  Left = 827
  Top = 246
  Caption = 'K'#233'szletkezel'#233's'
  ClientHeight = 730
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -16
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 20
  object Panel1: TPanel
    Left = 0
    Top = 644
    Width = 1008
    Height = 86
    Align = alBottom
    TabOrder = 0
    DesignSize = (
      1008
      86)
    object DBNavigator1: TDBNavigator
      Left = 8
      Top = 48
      Width = 264
      Height = 33
      DataSource = DSKMozgas
      VisibleButtons = [nbFirst, nbPrior, nbNext, nbLast, nbInsert, nbDelete, nbPost, nbCancel]
      TabOrder = 0
    end
    object DBEdit1: TDBEdit
      Left = 8
      Top = 8
      Width = 1001
      Height = 28
      Anchors = [akLeft, akTop, akRight]
      DataField = 'KM_MEGJ'
      DataSource = DSKMozgas
      TabOrder = 1
    end
  end
  object TabControl1: TTabControl
    Left = 0
    Top = 0
    Width = 1008
    Height = 644
    Align = alClient
    TabOrder = 1
    Tabs.Strings = (
      'Minden rakt'#225'r')
    TabIndex = 0
    OnChange = TabControl1Change
    object DBGrid1: TDBGrid
      Left = 4
      Top = 31
      Width = 1000
      Height = 609
      Align = alClient
      DataSource = DSKMozgas
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -16
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'KM_DATUM'
          Title.Alignment = taCenter
          Title.Caption = 'D'#225'tum'
          Width = 80
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'KM_RAKNEV'
          ReadOnly = True
          Title.Alignment = taCenter
          Title.Caption = 'Rakt'#225'r neve'
          Width = 120
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'KM_TNEV'
          Title.Alignment = taCenter
          Title.Caption = 'Term'#233'k megnevez'#233'se'
          Width = 200
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'KM_GKRESZ'
          Title.Alignment = taCenter
          Title.Caption = 'Rendsz'#225'm'
          Width = 80
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'KM_KMORA'
          Title.Alignment = taCenter
          Title.Caption = 'Km '#243'ra'
          Width = 80
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'KM_MNEM'
          Title.Alignment = taCenter
          Title.Caption = 'Mozg'#225'snem'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'KM_MENNY'
          Title.Alignment = taCenter
          Title.Caption = 'Mennyis'#233'g'
          Width = 90
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'KM_ME'
          ReadOnly = True
          Title.Alignment = taCenter
          Title.Caption = 'M.egys'#233'g'
          Width = 80
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'KM_MEGJ'
          Title.Alignment = taCenter
          Title.Caption = 'Megjegyz'#233's'
          Width = 130
          Visible = True
        end>
    end
  end
  object TKMOZGAS: TADODataSet
    Connection = EgyebDlg.ADOConnectionAlap
    CursorType = ctStatic
    AfterInsert = TKMOZGASAfterInsert
    CommandText = 'select * from KMOZGAS'
    Parameters = <>
    Left = 80
    Top = 112
    object TKMOZGASKM_ID: TAutoIncField
      FieldName = 'KM_ID'
      ReadOnly = True
    end
    object TKMOZGASKM_RAKKOD: TStringField
      FieldName = 'KM_RAKKOD'
      Size = 2
    end
    object TKMOZGASKM_RAKNEV: TStringField
      FieldName = 'KM_RAKNEV'
      Size = 40
    end
    object TKMOZGASKM_TKOD: TStringField
      FieldName = 'KM_TKOD'
      Size = 4
    end
    object TKMOZGASKM_TNEV: TStringField
      FieldName = 'KM_TNEV'
      Size = 40
    end
    object TKMOZGASKM_ME: TStringField
      FieldName = 'KM_ME'
      Size = 10
    end
    object TKMOZGASKM_GKRESZ: TStringField
      FieldName = 'KM_GKRESZ'
      Size = 10
    end
    object TKMOZGASKM_GKKAT: TStringField
      FieldName = 'KM_GKKAT'
      Size = 5
    end
    object TKMOZGASKM_GKEUROB: TIntegerField
      FieldName = 'KM_GKEUROB'
    end
    object TKMOZGASKM_KMORA: TIntegerField
      FieldName = 'KM_KMORA'
    end
    object TKMOZGASKM_MENNY: TBCDField
      FieldName = 'KM_MENNY'
      Precision = 14
      Size = 2
    end
    object TKMOZGASKM_EMENNY: TBCDField
      FieldName = 'KM_EMENNY'
      Precision = 14
      Size = 2
    end
    object TKMOZGASKM_KIBE: TStringField
      FieldName = 'KM_KIBE'
      Size = 2
    end
    object TKMOZGASKM_MNEM: TStringField
      FieldName = 'KM_MNEM'
      Size = 10
    end
    object TKMOZGASKM_MEGJ: TStringField
      FieldName = 'KM_MEGJ'
      Size = 200
    end
    object TKMOZGASKM_MODDAT: TDateTimeField
      FieldName = 'KM_MODDAT'
    end
    object TKMOZGASKM_USER: TStringField
      FieldName = 'KM_USER'
      Size = 40
    end
    object TKMOZGASKM_DATUM: TDateTimeField
      FieldName = 'KM_DATUM'
    end
  end
  object TKESZLET: TADOQuery
    Connection = EgyebDlg.ADOConnectionAlap
    Parameters = <>
    Left = 144
    Top = 112
  end
  object DSKMozgas: TDataSource
    DataSet = TKMOZGAS
    Left = 104
    Top = 144
  end
  object TTermek: TADOQuery
    Connection = EgyebDlg.ADOConnectionKozos
    Parameters = <>
    SQL.Strings = (
      'select * from termek where t_kkez=1 order by t_ternev')
    Left = 200
    Top = 112
    object TTermekT_TERKOD: TStringField
      FieldName = 'T_TERKOD'
      Size = 4
    end
    object TTermekT_TERNEV: TStringField
      FieldName = 'T_TERNEV'
      Size = 40
    end
    object TTermekT_LEIRAS: TStringField
      FieldName = 'T_LEIRAS'
      Size = 80
    end
    object TTermekT_CIKKSZAM: TStringField
      FieldName = 'T_CIKKSZAM'
      Size = 30
    end
    object TTermekT_ITJSZJ: TStringField
      FieldName = 'T_ITJSZJ'
    end
    object TTermekT_AFA: TStringField
      FieldName = 'T_AFA'
      Size = 3
    end
    object TTermekT_EGYAR: TBCDField
      FieldName = 'T_EGYAR'
      Precision = 14
    end
    object TTermekT_MEGJ: TStringField
      FieldName = 'T_MEGJ'
      Size = 80
    end
    object TTermekT_ME: TStringField
      FieldName = 'T_ME'
      Size = 10
    end
    object TTermekT_KKEZ: TIntegerField
      FieldName = 'T_KKEZ'
    end
    object TTermekT_KESZLET: TBCDField
      FieldName = 'T_KESZLET'
      Precision = 14
    end
  end
  object TGEPKOCSI: TADOQuery
    Connection = EgyebDlg.ADOConnectionKozos
    Parameters = <>
    SQL.Strings = (
      'select * from gepkocsi where gk_arhiv=1 order by gk_resz')
    Left = 248
    Top = 112
    object TGEPKOCSIGK_KOD: TStringField
      FieldName = 'GK_KOD'
      Size = 5
    end
    object TGEPKOCSIGK_RESZ: TStringField
      FieldName = 'GK_RESZ'
      Size = 10
    end
    object TGEPKOCSIGK_TIPUS: TStringField
      FieldName = 'GK_TIPUS'
      Size = 45
    end
    object TGEPKOCSIGK_MEGJ: TStringField
      FieldName = 'GK_MEGJ'
      Size = 80
    end
    object TGEPKOCSIGK_NORMA: TBCDField
      FieldName = 'GK_NORMA'
      Precision = 14
    end
    object TGEPKOCSIGK_DIJ1: TBCDField
      FieldName = 'GK_DIJ1'
      Precision = 14
    end
    object TGEPKOCSIGK_DIJ2: TBCDField
      FieldName = 'GK_DIJ2'
      Precision = 14
    end
    object TGEPKOCSIGK_SULYF: TIntegerField
      FieldName = 'GK_SULYF'
    end
    object TGEPKOCSIGK_KLIMA: TIntegerField
      FieldName = 'GK_KLIMA'
    end
    object TGEPKOCSIGK_SULYL: TBCDField
      FieldName = 'GK_SULYL'
      Precision = 14
    end
    object TGEPKOCSIGK_APETE: TBCDField
      FieldName = 'GK_APETE'
      Precision = 14
    end
    object TGEPKOCSIGK_APENY: TBCDField
      FieldName = 'GK_APENY'
      Precision = 14
    end
    object TGEPKOCSIGK_NORTE: TBCDField
      FieldName = 'GK_NORTE'
      Precision = 14
    end
    object TGEPKOCSIGK_NORNY: TBCDField
      FieldName = 'GK_NORNY'
      Precision = 14
    end
    object TGEPKOCSIGK_ALVAZ: TStringField
      FieldName = 'GK_ALVAZ'
      Size = 30
    end
    object TGEPKOCSIGK_MOTOR: TStringField
      FieldName = 'GK_MOTOR'
      Size = 30
    end
    object TGEPKOCSIGK_EVJAR: TStringField
      FieldName = 'GK_EVJAR'
      Size = 11
    end
    object TGEPKOCSIGK_FORHE: TStringField
      FieldName = 'GK_FORHE'
      Size = 11
    end
    object TGEPKOCSIGK_FEHER: TStringField
      FieldName = 'GK_FEHER'
      Size = 10
    end
    object TGEPKOCSIGK_RAKSU: TBCDField
      FieldName = 'GK_RAKSU'
      Precision = 14
    end
    object TGEPKOCSIGK_TERAK: TBCDField
      FieldName = 'GK_TERAK'
      Precision = 14
    end
    object TGEPKOCSIGK_RAME1: TBCDField
      FieldName = 'GK_RAME1'
      Precision = 14
    end
    object TGEPKOCSIGK_RAME2: TBCDField
      FieldName = 'GK_RAME2'
      Precision = 14
    end
    object TGEPKOCSIGK_RAME3: TBCDField
      FieldName = 'GK_RAME3'
      Precision = 14
    end
    object TGEPKOCSIGK_EUPAL: TBCDField
      FieldName = 'GK_EUPAL'
      Precision = 14
    end
    object TGEPKOCSIGK_KULSO: TIntegerField
      FieldName = 'GK_KULSO'
    end
    object TGEPKOCSIGK_SULYA: TBCDField
      FieldName = 'GK_SULYA'
      Precision = 14
    end
    object TGEPKOCSIGK_OSULY: TBCDField
      FieldName = 'GK_OSULY'
      Precision = 14
    end
    object TGEPKOCSIGK_POTOS: TIntegerField
      FieldName = 'GK_POTOS'
    end
    object TGEPKOCSIGK_POKOD: TStringField
      FieldName = 'GK_POKOD'
      Size = 5
    end
    object TGEPKOCSIGK_NEMZE: TIntegerField
      FieldName = 'GK_NEMZE'
    end
    object TGEPKOCSIGK_GARKM: TBCDField
      FieldName = 'GK_GARKM'
      Precision = 14
    end
    object TGEPKOCSIGK_GARFI: TIntegerField
      FieldName = 'GK_GARFI'
    end
    object TGEPKOCSIGK_GARNO: TStringField
      FieldName = 'GK_GARNO'
      Size = 60
    end
    object TGEPKOCSIGK_ARHIV: TIntegerField
      FieldName = 'GK_ARHIV'
    end
    object TGEPKOCSIGK_GKKAT: TStringField
      FieldName = 'GK_GKKAT'
      Size = 5
    end
    object TGEPKOCSIGK_FAJTA: TStringField
      FieldName = 'GK_FAJTA'
    end
    object TGEPKOCSIGK_SONEV: TStringField
      FieldName = 'GK_SONEV'
      Size = 80
    end
    object TGEPKOCSIGK_HOZZA: TIntegerField
      FieldName = 'GK_HOZZA'
    end
    object TGEPKOCSIGK_LIVAN: TIntegerField
      FieldName = 'GK_LIVAN'
    end
    object TGEPKOCSIGK_LICEG: TStringField
      FieldName = 'GK_LICEG'
      Size = 3
    end
    object TGEPKOCSIGK_LIDAT: TStringField
      FieldName = 'GK_LIDAT'
      Size = 11
    end
    object TGEPKOCSIGK_LIHON: TIntegerField
      FieldName = 'GK_LIHON'
    end
    object TGEPKOCSIGK_LISZE: TStringField
      FieldName = 'GK_LISZE'
      Size = 30
    end
    object TGEPKOCSIGK_LIOSZ: TBCDField
      FieldName = 'GK_LIOSZ'
      Precision = 12
      Size = 2
    end
    object TGEPKOCSIGK_LINEM: TStringField
      FieldName = 'GK_LINEM'
      Size = 3
    end
    object TGEPKOCSIGK_LIMEG: TStringField
      FieldName = 'GK_LIMEG'
      Size = 80
    end
    object TGEPKOCSIGK_TANKL: TBCDField
      FieldName = 'GK_TANKL'
      Precision = 12
      Size = 2
    end
    object TGEPKOCSIGK_TARTL: TBCDField
      FieldName = 'GK_TARTL'
      Precision = 12
      Size = 2
    end
    object TGEPKOCSIGK_RESZR: TIntegerField
      FieldName = 'GK_RESZR'
    end
    object TGEPKOCSIGK_KIVON: TIntegerField
      FieldName = 'GK_KIVON'
    end
    object TGEPKOCSIGK_VETAR: TBCDField
      FieldName = 'GK_VETAR'
      Precision = 12
      Size = 2
    end
    object TGEPKOCSIGK_VETVN: TStringField
      FieldName = 'GK_VETVN'
      Size = 3
    end
    object TGEPKOCSIGK_EUROB: TIntegerField
      FieldName = 'GK_EUROB'
    end
    object TGEPKOCSIGK_SFORH: TStringField
      FieldName = 'GK_SFORH'
      Size = 11
    end
    object TGEPKOCSIGK_SOBEL: TStringField
      FieldName = 'GK_SOBEL'
      Size = 80
    end
    object TGEPKOCSIGK_UT_DA: TStringField
      FieldName = 'GK_UT_DA'
      Size = 11
    end
    object TGEPKOCSIGK_UT_KM: TBCDField
      FieldName = 'GK_UT_KM'
      Precision = 14
    end
    object TGEPKOCSIGK_FORKI: TStringField
      FieldName = 'GK_FORKI'
      Size = 11
    end
  end
  object TSZOTAR: TADOQuery
    Connection = EgyebDlg.ADOConnectionKozos
    Parameters = <
      item
        Name = 'SZK'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 3
        Value = Null
      end>
    SQL.Strings = (
      'select * from szotar where SZ_FOKOD= :SZK order by sz_alkod')
    Left = 312
    Top = 112
    object TSZOTARSZ_FOKOD: TStringField
      FieldName = 'SZ_FOKOD'
      Size = 3
    end
    object TSZOTARSZ_ALKOD: TStringField
      FieldName = 'SZ_ALKOD'
      Size = 5
    end
    object TSZOTARSZ_MENEV: TStringField
      FieldName = 'SZ_MENEV'
      Size = 120
    end
    object TSZOTARSZ_LEIRO: TStringField
      FieldName = 'SZ_LEIRO'
      Size = 80
    end
    object TSZOTARSZ_KODHO: TBCDField
      FieldName = 'SZ_KODHO'
      Precision = 14
    end
    object TSZOTARSZ_NEVHO: TBCDField
      FieldName = 'SZ_NEVHO'
      Precision = 14
    end
    object TSZOTARSZ_ERVNY: TIntegerField
      FieldName = 'SZ_ERVNY'
    end
  end
end
