unit HianyVevoBe;

interface

uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, DBTables, DB, StdCtrls, Buttons, Mask, DBCtrls,
  ExtCtrls, ADODB;

type
  THianyVevobeDlg = class(TForm)
    BitBtn6: TBitBtn;
    M0: TMaskEdit;
    Label3: TLabel;
    BitElkuld: TBitBtn;
    Query1: TADOQuery;
    M2: TMaskEdit;
    M1: TMaskEdit;
    Label1: TLabel;
    BitBtn5: TBitBtn;
    Label2: TLabel;
    Label4: TLabel;
    M00: TMaskEdit;
    procedure FormCreate(Sender: TObject);
    procedure BitBtn6Click(Sender: TObject);
    procedure Tolt(vevonev, cim : string);
    procedure BitElkuldClick(Sender: TObject);
    procedure M1Exit(Sender: TObject);
    procedure M1KeyPress(Sender: TObject; var Key: Char);
    procedure M1Click(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure BitBtn5Click(Sender: TObject);
  private
  public
  		vevokod	: string;
  end;

var
  HianyVevobeDlg: THianyVevobeDlg;

implementation

uses
	Egyeb, Kozos, J_SQL, VevoUjFm;
{$R *.DFM}

procedure THianyVevobeDlg.FormCreate(Sender: TObject);
begin
  	EgyebDlg.SetADOQueryDatabase(Query1);
  	SetMaskEdits([M0, M00, M1]);
end;

procedure THianyVevobeDlg.BitBtn6Click(Sender: TObject);
begin
	vevokod	:= '';
	Close;
end;

procedure THianyVevobeDlg.Tolt(vevonev, cim : string);
begin
   M0.Text		:= vevonev;
   M00.Text	:= cim;
end;

procedure THianyVevobeDlg.BitElkuldClick(Sender: TObject);
begin
	if M2.Text = '' then begin
		NoticeKi('Érvénytelen vevőkód!');
		M2.SetFocus;
       Exit;
   end;
	vevokod	:= M2.Text;
   Close;
end;

procedure THianyVevobeDlg.M1Exit(Sender: TObject);
begin
	SzamExit(Sender);
end;

procedure THianyVevobeDlg.M1KeyPress(Sender: TObject; var Key: Char);
begin
	SzamKeyPress(Sender, Key);
end;

procedure THianyVevobeDlg.M1Click(Sender: TObject);
begin
	with Sender as TMaskEdit do begin
  		SelectAll;
  	end;
end;

procedure THianyVevobeDlg.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
	EgyebDlg.FormReturn(Key);
end;

procedure THianyVevobeDlg.BitBtn5Click(Sender: TObject);
begin
	Application.CreateForm(TVevoUjFmDlg, VevoUjFmDlg);
	VevoUjFmDlg.valaszt := true;
	VevoUjFmDlg.ShowModal;
	if VevoUjFmDlg.ret_vkod <> '' then begin
		M1.Text	:= VevoUjFmDlg.ret_vnev;
		M2.Text	:= VevoUjFmDlg.ret_vkod;
	end;
	VevoUjFmDlg.Destroy;
end;

end.


