unit KPI_Export;

interface

uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls, Egyeb,
  Forms, Dialogs, DBTables, DB, StdCtrls, Buttons, Mask, DBCtrls, Grids,
  Printers, Shellapi, ExtCtrls, ADODB, J_Excel;

type
  TKPI_ExportDlg = class(TForm)
    Label1: TLabel;
    BitBtn4: TBitBtn;
    BitBtn6: TBitBtn;
    M1: TMaskEdit;
	 M2: TMaskEdit;
	 Label2: TLabel;
	 Label3: TLabel;
	 BitBtn10: TBitBtn;
	 BitBtn1: TBitBtn;
	 Query1: TADOQuery;
    Label4: TLabel;
	 Query2: TADOQuery;
	 Query3: TADOQuery;
    M50: TMaskEdit;
    M5: TMaskEdit;
    BitBtn5: TBitBtn;
	 procedure FormCreate(Sender: TObject);
	 procedure BitBtn4Click(Sender: TObject);
	 procedure BitBtn6Click(Sender: TObject);
	 procedure M1Click(Sender: TObject);
	 procedure BitBtn10Click(Sender: TObject);
	 procedure BitBtn1Click(Sender: TObject);
	 procedure M1Exit(Sender: TObject);
	 function XLS_Generalas : boolean;
	 procedure BitBtn5Click(Sender: TObject);
  private
		kilepes		: boolean;
  public
		gr			: TStringGrid;
		voltkilepes	: boolean;
		kellshell	: boolean;
		kelluzenet	: boolean;
		kombinalt	: boolean;
  end;

var
  KPI_ExportDlg: TKPI_ExportDlg;

implementation

uses
	j_sql, Kozos, ValVevo, J_Datamodule, Kozos_Export,Kombi_Export;
{$R *.DFM}

procedure TKPI_ExportDlg.FormCreate(Sender: TObject);
begin
	gr			:= nil;
	kilepes     := true;
	kellshell	:= true;
	voltkilepes	:= false;
	kelluzenet	:= true;
	kombinalt	:= false;
	EgyebDlg.SetADOQueryDatabase(Query1);
	EgyebDlg.SetADOQueryDatabase(Query2);
	EgyebDlg.SetADOQueryDatabase(Query3);
	Kezdoertekek(M1, M2, M5, M50);
end;

procedure TKPI_ExportDlg.BitBtn4Click(Sender: TObject);
begin
	if not DatumExit(M1, false) then begin
		Exit;
	end;
	if not DatumExit(M2, false) then begin
		Exit;
	end;
	if M50.Text = '' then begin
		NoticeKi('A megb�z� kiv�laszt�sa k�telez�!');
		M5.SetFocus;
		Exit;
	end;
	// Az XLS �llom�ny gener�l�sa
	Screen.Cursor	:= crHourGlass;
	XLS_Generalas;
	Screen.Cursor	:= crDefault;
	if kombinalt then begin
		Close;
	end else begin
		NoticeKi('A riport gener�l�sa befejez�d�tt!');
	end;
end;

procedure TKPI_ExportDlg.BitBtn6Click(Sender: TObject);
begin
	if not kilepes then begin
		kilepes	:= true;
	end else begin
		Close;
	end;
end;

procedure TKPI_ExportDlg.M1Click(Sender: TObject);
begin
	with Sender as TMaskEdit do begin
		SelectAll;
	end;
end;

procedure TKPI_ExportDlg.BitBtn10Click(Sender: TObject);
begin
	// EgyebDlg.Calendarbe(M1);
	// EgyebDlg.ElsoNapEllenor(M1, M2);
  EgyebDlg.CalendarBe_idoszak(M1, M2);
end;

procedure TKPI_ExportDlg.BitBtn1Click(Sender: TObject);
begin
	EgyebDlg.Calendarbe(M2, True);
end;

procedure TKPI_ExportDlg.M1Exit(Sender: TObject);
begin
	DatumExit(M1,false);
	EgyebDlg.ElsoNapEllenor(M1, M2);
end;

function TKPI_ExportDlg.XLS_Generalas : boolean;
var
	fn			: String;
	sorszam		: integer;
	ujkod		: string;
	lanossz		: double;
	potlek		: double;

	EX	  		: TJExcel;
	fuszam		: integer;
	kiszam		: integer;
	dbelo		: integer;
	db0			: integer;
	db1			: integer;
	db2			: integer;
	db3			: integer;
	dbsok		: integer;
	dbszamla   	: integer;
	dbkeso		: integer;
	dblejart   	: integer;
	dbjova		: integer;
	fukg		: double;
	kikg		: double;
	forg		: double;
	uapot		: double;
	teldat		: string;
	telido		: string;
	utodat		: string;
	utoido		: string;
	kulnap		: integer;
	kulora		: integer;
	hatdat		: string;
	folytat		: boolean;
	suly		: double;
	lanid		: string;
  kelluzpotl: boolean;
begin
	result		:= true;
	fn			:= 'KPI_'+M1.Text+'_'+M2.Text;
	while Pos('.', fn) > 0 do begin
		fn		:= copy(fn, 1, Pos('.', fn) - 1 ) + copy(fn, Pos('.', fn) + 1, 999 );
	end;
	fn			:= EgyebDlg.DocumentPath + fn + '.XLS';
	if FileExists(fn) then begin
		DeleteFile(PChar(fn));
	end;
	EllenListTorol;
	Query_Run(Query1, 'SELECT MB_MBKOD, MB_DATUM, MB_JASZA, MB_RENSZ, MB_VIKOD, MB_JAKOD, MB_FUTID, MB_FUDIJ, MB_KISCS,MB_ALVAL,MB_NUZPOT FROM MEGBIZAS '+
			' WHERE MB_MBKOD IN ( SELECT MS_MBKOD FROM MEGSEGED WHERE MS_LETDAT >= '''+M1.Text+''' AND MS_LETDAT <= '''+M2.Text+''' ) '+
			' AND MB_VEKOD IN ('+M50.Text+') '+
			' ORDER BY MB_DATUM, MB_MBKOD ' );
	if Query1.RecordCount < 1 then begin
		if kelluzenet then begin
			NoticeKi('Nincs a felt�teleknek megfelel� elem!');
		end;
		result		:= false;
		Exit;
	end;
	BitBtn4.Hide;
	// Az XLS megnyit�sa
	EX 	:= TJexcel.Create(Self);
	if not EX.OpenXlsFile(fn) then begin
		if kelluzenet then begin
			NoticeKi('Nem siker�lt megnyitnom az Excel �llom�nyt!');
		end;
	end else begin
		EX.ColNumber	:= 15;
		EX.InitRow;
		EX.SetActiveSheet(0);
		EX.SetRowcell(1, 'Id�szak :');
		EX.SetRowcell(2, M1.Text);
		EX.SetRowcell(3, M2.Text);
		EX.SaveRow(1);
		fuszam		:= 0;
		kiszam		:= 0;
		dbelo		:= 0;
		db0			:= 0;
		db1			:= 0;
		db2			:= 0;
		db3			:= 0;
		dbsok		:= 0;
		dbszamla   	:= 0;
		dbkeso		:= 0;
		dblejart   	:= 0;
		dbjova		:= 0;
		fukg		:= 0;
		kikg		:= 0;
		forg		:= 0;
		uapot		:= 0;

		EX.SetActiveSheet(1);
		sorszam			:= 2;
		EX.SetRowcell(1, 'Megb�z�s k�d');
		EX.SetRowcell(2, 'J�ratsz�m');
		EX.SetRowcell(3, 'Rendsz�m');
		EX.SetRowcell(4, 'Kis csomag');
		EX.SetRowcell(5, 'Teljes�t�s d�tuma');
		EX.SetRowcell(6, 'Teljes�t�s ideje');
		EX.SetRowcell(7, 'Teljes�t�s d�tuma');
		EX.SetRowcell(8, 'Teljes�t�s ideje');
		EX.SetRowcell(9, 	'LAN ID');
		EX.SetRowcell(10, 	'Hat�rid�');
		EX.SetRowcell(11, 	'�rak�l�nbs�g');
		EX.SetRowcell(12, 	'S�ly');
		EX.SetRowcell(13, 	'Fuvard�j');
		EX.SetRowcell(14, 	'�zemanyag p�tl�k');
		EX.SaveRow(sorszam);
		Inc(sorszam);

		kilepes				:= false;
		Query1.DisableControls;
		while ( ( not Query1.Eof ) and (not kilepes) ) do begin
		 Query_Run(Query2, 'SELECT MS_MBKOD FROM MEGSEGED WHERE MS_MBKOD = '+Query1.FieldByName('MB_MBKOD').AsString+' AND MS_FETDAT <'''+ M1.Text+''' ');
     if Query2.RecordCount=0 then
     begin
			Application.ProcessMessages;
      kelluzpotl:= StrToIntDef( Query1.FieldByName('MB_NUZPOT').AsString,0) = 0 ;
			Label4.Caption	:= 'D�tum : '+Query1.FieldByName('MB_DATUM').AsString + ' - megb�z�s k�d : ' +Query1.FieldByName('MB_MBKOD').AsString;
			Label4.Update;
			folytat	:= true;
			lanossz				:= StringSzam(Query1.FieldByName('MB_FUDIJ').AsString);
			potlek				:= 0;
			EX.EmptyRow;
			EX.SetRowcell(1, Query1.FieldByName('MB_MBKOD').AsString);
			EX.SetRowcell(2, Query1.FieldByName('MB_JASZA').AsString);
			EX.SetRowcell(3, Query1.FieldByName('MB_RENSZ').AsString);
			Query_Run(Query3, 'SELECT * FROM VISZONY WHERE VI_VIKOD = '+ IntToStr(StrToIntDef(Query1.FieldByName('MB_VIKOD').AsString, 0)) +
				' AND VI_JAKOD = '''+Query1.FieldByName('MB_JAKOD').AsString+''' ');
			if Query3.RecordCount <> 1  then begin
				// Nins sz�mla
				EllenListAppend('A megb�z�shoz nem tartozik sz�mla! Megb�z�s k�d : '+Query1.FieldByName('MB_MBKOD').AsString);
				folytat	:= false;
			end else begin
				ujkod	:= Query3.FieldByName('VI_UJKOD').AsString;
				// A sz�mla k�pz�se
				if StrToIntDef(ujkod, 0) = 0 then begin
					EllenListAppend('A megb�z�shoz nem tartozik sz�mla! Megb�z�s k�d : '+Query1.FieldByName('MB_MBKOD').AsString);
					folytat	:= false;
				end else begin
					JSzamla	:= TJSzamla.Create(Self);
					JSzamla.FillSzamla(ujkod);
					potlek := JSzamla.uapotlekeur;
				 //	if potlek = 0 then begin
					// if kelluzpotl and (potlek = 0)and(pos(JSzamla.szamlaszam ,Kombi_ExportDlg.kellaszamla)=0) then begin
          // 2017-02-13 Mintha ezt a "Kombi_ExportDlg.kellaszamla"-t nem t�lten�nk fel...
          if kelluzpotl and (potlek = 0) then begin
						EllenListAppend('Az �zemanyag p�tl�k �sszege 0! Sz�mlasz�m : '+JSzamla.szamlaszam);
						folytat	:= false;
					end else begin
						EX.SetRowcell(14, Format('%.2f', [potlek]));
						//if ( Abs ( JSzamla.ossz_nettoeur - JSzamla.spedicioeur - potlek - lanossz ) > 0.00001 ) then begin
						if ( Abs ( JSzamla.ossz_nettoeur - JSzamla.spedicioeur - potlek - lanossz ) > 0.00001 )and(pos(Query1.FieldByName('MB_ALVAL').AsString,Kombi_ExportDlg.belf_alval)=0) then begin
							EllenListAppend('A sz�mla nett� eur - sped�ci�s d�j <> megb�z�si fuvard�j! Sz�mla : '+JSzamla.szamlaszam+'  Megb�z�s k�d : '+Query1.FieldByName('MB_MBKOD').AsString);
							folytat	:= false;
						end else begin
              lanossz:= JSzamla.ossz_nettoeur - JSzamla.spedicioeur - potlek;
							EX.SetRowcell(13, Format('%.2f', [lanossz+potlek]));
							//EX.SetRowcell(13, Format('%.2f', [lanossz]));
						end;
					end;
					JSzamla.Destroy;
				end;
			end;

			if folytat then begin
				suly	:= GetSuly(Query1.FieldByName('MB_MBKOD').AsString);
				EX.SetRowcell(12, Format('%f', [suly]));
				// Kell sz�molni a megb�z�ssal
				if StrToIntDef(Query1.FieldByName('MB_KISCS').AsString, 0) > 0 then begin
					Inc(kiszam);
					kikg	:= kikg + suly;
					EX.SetRowcell(4, '1');
				end else begin
					Inc(fuszam);
					fukg	:= fukg + suly;
					EX.SetRowcell(4, '0');
				end;
				// A d�tumok kider�t�se
				Query_Run(Query3, 'SELECT MIN(MS_FETDAT+'' ''+MS_FETIDO) TELJESDAT, MAX(MS_LETDAT+'' ''+MS_LETIDO) UTODAT FROM MEGSEGED WHERE MS_MBKOD = '+Query1.FieldByName('MB_MBKOD').AsString );
				teldat := copy(Query3.FieldByName('TELJESDAT').AsString, 1, Pos(' ', Query3.FieldByName('TELJESDAT').AsString)-1);
				utodat := copy(Query3.FieldByName('UTODAT').AsString, 1, Pos(' ', Query3.FieldByName('UTODAT').AsString)-1);
				EX.SetRowcell(5, teldat);
				EX.SetRowcell(7, utodat);
				if ( ( teldat >= M1.Text ) and ( teldat <= M2.Text ) ) then begin
					telido := Trim(copy(Query3.FieldByName('TELJESDAT').AsString, Pos(' ', Query3.FieldByName('TELJESDAT').AsString)+1, 999));
					utoido := Trim(copy(Query3.FieldByName('UTODAT').AsString, 	 Pos(' ', Query3.FieldByName('UTODAT').AsString)+1, 999));
					EX.SetRowcell(6, telido);
					EX.SetRowcell(8, utoido);
					if telido = '' then begin
						EllenListAppend('Hi�nyz� felrak�s t�ny id�! Felrak�s t�ny d�tuma : '+teldat+'  J�rat: '+Query1.FieldByName('MB_JASZA').AsString + '  Rendsz�m : '+Query1.FieldByName('MB_RENSZ').AsString+ '  Megb�z�s k�d : '+Query1.FieldByName('MB_MBKOD').AsString);
						EX.SetRowcell(15, 'Hi�nyz� felrak�s t�ny id�! ');
						folytat	:= false;
					end;
					if ( ( telido = '' ) and folytat ) then begin
						EllenListAppend('Hi�nyz� utols� lerak�s t�ny id�! Lerak�s t�ny d�tuma : '+utodat+'  J�rat: '+Query1.FieldByName('MB_JASZA').AsString + '  Rendsz�m : '+Query1.FieldByName('MB_RENSZ').AsString + '  Megb�z�s k�d : '+Query1.FieldByName('MB_MBKOD').AsString);
						EX.SetRowcell(15, 'Hi�nyz� utols� lerak�s t�ny id�! ');
						folytat	:= false;
					end;
					if folytat then begin
						// A LAN ID ellen�rz�se
						lanid	:= Query_Select('FUVARTABLA', 'FT_FTKOD', IntToStr(StrToIntDef(Query1.FieldByName('MB_FUTID').AsString,0)), 'FT_LANID');
						if lanid = '' then begin
							EllenListAppend('Hi�nyz� LAN_ID!   J�rat: '+Query1.FieldByName('MB_JASZA').AsString + '  Rendsz�m : '+Query1.FieldByName('MB_RENSZ').AsString + ' Megb�z�s k�d : '+Query1.FieldByName('MB_MBKOD').AsString);
							folytat	:= false;
						end;
						if folytat then begin
							EX.SetRowcell(9, lanid);
							hatdat	:= DatumHozNap(teldat, StrToIntDef(Query_Select('FUVARTABLA', 'FT_FTKOD', IntToStr(StrToIntDef(Query1.FieldByName('MB_FUTID').AsString,0)), 'FT_TRDAY'), 0), true);
							EX.SetRowcell(10, hatdat);
							// Kisz�moljuk a k�l�nbs�get �r�kban
							if hatdat+telido > utodat+utoido then begin
								DateTimeDifference(utodat, utoido, hatdat, telido, kulnap, kulora);
								kulora	:= -1*(kulnap * 24 + kulora);
							end else begin
								DateTimeDifference(hatdat, telido, utodat, utoido, kulnap, kulora);
								kulora	:= kulnap * 24 + kulora;
							end;
							EX.SetRowcell(11, IntToStr(kulora));
							if kulora < -24 then begin
								Inc(dbelo);
							end else begin
								if kulora < 24 then begin
									Inc(db0);
								end else begin
									if kulora < 48 then begin
										Inc(db1);
									end else begin
										if kulora < 72 then begin
											Inc(db2);
										end else begin
											if kulora < 96 then begin
												Inc(db3);
											end else begin
												Inc(dbsok);
											end;
										end;
									end;
								end;
							end;
						end;
					end;
				end else begin
					EX.SetRowcell(15, 'Id�szakon k�v�li teljes�t�s! NEM HIBA !!!');
				end;
				if folytat then begin
					EX.SaveRow(sorszam);
					Inc(sorszam);
					SorLogolas(gr, Query1.FieldByName('MB_MBKOD').AsString, suly, lanossz+potlek, potlek, 2);
				end;
			end;
     end; 
			Query1.Next;
		end;

		// A fuvard�j ill. �zemanyag p�tl�k �sszesen ki�r�sa
		EX.EmptyRow;
		EX.SetRowcell(13, '=SUM(M3'+':M'+IntToStr(sorszam-1)+')');
		EX.SetRowcell(14, '=SUM(N3'+':N'+IntToStr(sorszam-1)+')');
		EX.SaveRow(sorszam);


		EX.SetActiveSheet(0);
		EX.EmptyRow;
//		sorszam			:= 3;
		Inc(sorszam);
		Inc(sorszam);
		EX.SetRowcell(1, 'Total number of shipments delivered by road');
		EX.SetRowcell(2, IntToStr(fuszam));
		EX.SetRowcell(3, '');
		EX.SaveRow(sorszam);
		Inc(sorszam);
		EX.SetRowcell(1, 'Total number of shipments by express/small package');
		EX.SetRowcell(2, IntToStr(kiszam));
		EX.SaveRow(sorszam);
		Inc(sorszam);
		EX.SetRowcell(1, 'Total number of actual kilograms delivered by road');
		EX.SetRowcell(2, Format('%f', [fukg]));
		EX.SaveRow(sorszam);
		Inc(sorszam);
		EX.SetRowcell(1, 'Total number of actual kilograms delivered by express/small package');
		EX.SetRowcell(2, Format('%f', [kikg]));
		EX.SaveRow(sorszam);
		Inc(sorszam);
		EX.SetRowcell(1, 'Total number of shipments delivered early');
		EX.SetRowcell(2, Format('%d', [dbelo]));
		EX.SaveRow(sorszam);
		Inc(sorszam);
		EX.SetRowcell(1, 'Total number of shipments delivered on the agreed transit time');
		EX.SetRowcell(2, Format('%d', [db0]));
		EX.SaveRow(sorszam);
		Inc(sorszam);
		EX.SetRowcell(1, 'Total number of shipments delivered 1 calendar day late');
		EX.SetRowcell(2, Format('%d', [db1]));
		EX.SaveRow(sorszam);
		Inc(sorszam);
		EX.SetRowcell(1, 'Total number of shipments delivered 2 calendar days late');
		EX.SetRowcell(2, Format('%d', [db2]));
		EX.SaveRow(sorszam);
		Inc(sorszam);
		EX.SetRowcell(1, 'Total number of shipments delivered 3 calendar days late');
		EX.SetRowcell(2, Format('%d', [db3]));
		EX.SaveRow(sorszam);
		Inc(sorszam);
		EX.SetRowcell(1, 'Total number of shipments delivered more then 3 calendar days late');
		EX.SetRowcell(2, Format('%d', [dbsok]));
		EX.SaveRow(sorszam);
		Inc(sorszam);

		EX.SetRowcell(1, 'Total shipments invoiced (on invoice date)');
		EX.SetRowcell(2, Format('%f', [forg]));
		EX.SaveRow(sorszam);
		Inc(sorszam);
		EX.SetRowcell(1, 'Total shipments associated with Fuel Surcharges');
		EX.SetRowcell(2, Format('%f', [uapot]));
		EX.SaveRow(sorszam);
		Inc(sorszam);
		EX.SetRowcell(1, 'Total shipment invoices sent electronically (on invoice date)');
		EX.SetRowcell(2, Format('%d', [dbszamla]));
		EX.SaveRow(sorszam);
		Inc(sorszam);
		EX.SetRowcell(1, 'Total shipment invoices with invoice date > 30 calendar days after POD (on invoice date)');
		EX.SetRowcell(2, Format('%d', [dbkeso]));
		EX.SaveRow(sorszam);
		Inc(sorszam);
		EX.SetRowcell(1, 'Total outstanding invoice amount at end of the month');
		EX.SetRowcell(2, Format('%d', [dblejart]));
		EX.SaveRow(sorszam);
		Inc(sorszam);
		EX.SetRowcell(1, 'Total shipment credit notes and/or invoice-adjustments (on credit note/adjustment date)');
		EX.SetRowcell(2, Format('%d', [dbjova]));
		EX.SaveRow(sorszam);
	end;
	EX.SaveFileAsXls(fn);
	EX.Free;
	voltkilepes	:= kilepes;
	if ( ( EgyebDlg.ellenlista.Count > 0 ) and kelluzenet ) then begin
		EllenListMutat('A KPI kimutat�s eredm�nye', false);
	end;
	BitBtn4.Show;
	if kellshell then begin
		ShellExecute(Application.Handle,'open',PChar(fn), '', '', SW_SHOWNORMAL );
	end;
	Label4.Caption	:= '';
	Label4.Update;
	kilepes			:= true;
end;

procedure TKPI_ExportDlg.BitBtn5Click(Sender: TObject);
begin
	// T�bb vev� kiv�laszt�sa
	Application.CreateForm(TValvevoDlg, ValvevoDlg);
	ValvevoDlg.Tolt(M50.Text);
	ValvevoDlg.ShowModal;
	if ValvevoDlg.kodkilist.Count > 0 then begin
		M5.Text		:= ValvevoDlg.nevlista;
		M50.Text	:= ValvevoDlg.kodlista;
	end;
	ValvevoDlg.Destroy;
end;

end.
