unit AlGepBe;

interface

uses
	SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
	Forms, Dialogs, StdCtrls, Buttons, Mask, DB, DBTables, J_ALFORM,
	TabNotBk, ComCtrls, Grids, ADODB, ExtCtrls, DBGrids, Shellapi, FileCtrl,VevoUjfm;

type
	TAlgepbeDlg = class(TJ_AlformDlg)
    Query1: TADOQuery;
    Panel1: TPanel;
    BitElkuld: TBitBtn;
    BitKilep: TBitBtn;
    Label2: TLabel;
    Label8: TLabel;
    Label1: TLabel;
    Label24: TLabel;
    Label25: TLabel;
    Label35: TLabel;
    Label50: TLabel;
    M1: TMaskEdit;
    M2: TMaskEdit;
    M3: TMaskEdit;
    M10: TMaskEdit;
    BitBtn2: TBitBtn;
    M11: TMaskEdit;
    BitBtn1: TBitBtn;
    CheckBox4: TCheckBox;
    CBKateg: TComboBox;
    CBEuro: TComboBox;
    Label3: TLabel;
    Label12: TLabel;
    Label26: TLabel;
    Label27: TLabel;
    Label28: TLabel;
    Label31: TLabel;
    M27: TMaskEdit;
    M21: TMaskEdit;
    M22: TMaskEdit;
	 M23: TMaskEdit;
    M26: TMaskEdit;
    M24: TMaskEdit;
    M25: TMaskEdit;
    M20A: TMaskEdit;
    CPot: TCheckBox;
    M1A: TMaskEdit;
    M1B: TMaskEdit;
    Label4: TLabel;
    ButValVevo: TBitBtn;
	procedure BitKilepClick(Sender: TObject);
	procedure FormCreate(Sender: TObject);
	procedure Tolto(cim : string; teko : string); override;
	procedure BitElkuldClick(Sender: TObject);
  procedure MaskEditClick(Sender: TObject);
    procedure Modosit(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
	   Shift: TShiftState);
    procedure MaskKeyPress(Sender: TObject; var Key: Char);
    procedure MaskExit(Sender: TObject);
    procedure MClick(Sender: TObject);
	 procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure MDatumExit(Sender: TObject);
	 procedure M11AExit(Sender: TObject);
	 procedure MaskEdit11Exit(Sender: TObject);
    procedure ButValVevoClick(Sender: TObject);
	private
	  modosult 		: boolean;
	  lista_kateg	: TStringList;
	public
	end;

var
	AlgepbeDlg: TAlgepbeDlg;


implementation

uses
	Kozos, Egyeb, J_SQL, AlvallalUjFm;

{$R *.DFM}

procedure TAlgepbeDlg.BitKilepClick(Sender: TObject);
begin
	if modosult then begin
	if NoticeKi('M�dosultak az adatok . Ki akar l�pni ment�s n�lk�l?', NOT_QUESTION ) = 0 then begin
			ret_kod := '';
			Close;
	  end;
  end else begin
		ret_kod := '';
		Close;
  end;
end;

procedure TAlgepbeDlg.FormCreate(Sender: TObject);
begin
	ret_kod 	:= '';
	EgyebDlg.SetADOQueryDatabase(Query1);
	modosult 	:= false;
	lista_kateg	:= TStringList.Create;
	SzotarTolt(CBKateg, '340' , lista_kateg, false, true );
	CBKateg.ItemIndex  := 0;
end;

procedure TAlgepbeDlg.Tolto(cim : string; teko:string);
begin
	// A t�li norm�k nem m�dos�that�ak
	SetMaskEdits([M1, M1A, M1B]);
	ret_kod 		:= teko;
	Caption 		:= cim;
	M1.Text 		:= teko;
	if ret_kod <> '' then begin		{M�dos�t�s}
		if Query_Run (Query1, 'SELECT * FROM ALGEPKOCSI WHERE AG_AGKOD = '+teko,true) then begin
			M1A.Text   			:= Query1.FieldByName('AG_AVKOD').AsString;
			M1B.Text			:= Query_select ('ALGEPKOCSI', 'AG_AVKOD', M1A.Text, 'AG_AVNEV');
			M2.Text    			:= Query1.FieldByName('AG_RENSZ').AsString;
			M3.Text    			:= Query1.FieldByName('AG_TIPUS').AsString;
			M27.Text 			:= Query1.FieldByName('AG_MEGJE').AsString;
			M10.Text 			:= Query1.FieldByName('AG_EVJAR').AsString;
			M11.Text 			:= Query1.FieldByName('AG_FORHE').AsString;
			M20A.Text 			:= SzamString(Query1.FieldByName('AG_OSULY').AsFloat,9,2);
			M21.Text 			:= SzamString(Query1.FieldByName('AG_RAKSU').AsFloat,9,2);
			M22.Text 			:= SzamString(Query1.FieldByName('AG_TERAK').AsFloat,9,2);
			M23.Text 			:= SzamString(Query1.FieldByName('AG_RAME1').AsFloat,9,2);
			M24.Text 			:= SzamString(Query1.FieldByName('AG_RAME2').AsFloat,9,2);
			M25.Text 			:= SzamString(Query1.FieldByName('AG_RAME3').AsFloat,9,2);
			M26.Text 			:= SzamString(Query1.FieldByName('AG_EUPAL').AsFloat,4,0);
			CPot.Checked		:= Query1.FieldByName('AG_POTOS').AsInteger > 0;
			CheckBox4.Checked	:= Query1.FieldByName('AG_ARHIV').AsInteger > 0;
			ComboSet (CBKateg, 	 Trim(Query1.FieldByName('AG_GKKAT').AsString), lista_kateg);
			CBEuro.ItemIndex	:= StrToIntDef(Query1.FieldByName('AG_EUROB').AsString, 0);
		end;
	end else begin
		{�j felviteln�l gener�ljon egy �j azonos�t�t}
		M1.Text	:= IntToStr(GetNextCode('ALGEPKOCSI', 'AG_AGKOD'));
	end;
	modosult 	:= false;
end;

procedure TAlgepbeDlg.BitElkuldClick(Sender: TObject);
begin
	if M1A.Text = '' then begin
		NoticeKi('Az alv�llalkoz� megad�sa k�telez�!');
		Exit;
	end;

	if M2.Text = '' then begin
		NoticeKi('A rendsz�m megad�sa k�telez�!');
		M2.Setfocus;
		Exit;
	end;

	if ret_kod = '' then begin
		{�j rekord felvitele}
		M1.Text	:= IntToStr(GetNextCode('ALGEPKOCSI', 'AG_AGKOD'));
		ret_kod	:= M1.Text;
		Query_Run ( Query1, 'INSERT INTO ALGEPKOCSI (AG_AGKOD) VALUES (' +M1.Text+ ' )',true);
	end;

	{A r�gi rekord m�dos�t�sa}
	Query_Update (Query1, 'ALGEPKOCSI',
		[
		'AG_RENSZ', 	''''+M2.Text+'''',
		'AG_TIPUS', ''''+M3.Text+'''',
		'AG_GKKAT', ''''+lista_kateg[CBKateg.ItemIndex]+'''',
		'AG_MEGJE', ''''+M27.Text+'''',
		'AG_OSULY', SqlSzamString(StringSzam(M20A.Text),9,2),
		'AG_RAKSU', SqlSzamString(StringSzam(M21.Text),9,2),
		'AG_TERAK', SqlSzamString(StringSzam(M22.Text),9,2),
		'AG_RAME1', SqlSzamString(StringSzam(M23.Text),9,2),
		'AG_RAME2', SqlSzamString(StringSzam(M24.Text),9,2),
		'AG_RAME3', SqlSzamString(StringSzam(M25.Text),9,2),
		'AG_EUPAL', SqlSzamString(StringSzam(M26.Text),4,0),
		'AG_EVJAR', ''''+M10.Text+'''',
		'AG_FORHE', ''''+M11.Text+'''',
		'AG_POTOS', BoolToIntStr(CPot.Checked),
		'AG_EUROB', IntToStr(CBEuro.ItemIndex),
		'AG_AVKOD', ''''+M1A.Text+'''',
		'AG_AVNEV', ''''+M1B.Text+'''',
		'AG_ARHIV', BoolToIntStr(CheckBox4.Checked)
		], ' WHERE AG_AGKOD = '+ret_kod);
	Query1.Close;
	Close;
end;

procedure TAlgepbeDlg.MaskEditClick(Sender: TObject);
begin
	with Sender as TMaskEdit do begin
		SelectAll;
	end;
end;

procedure TAlgepbeDlg.Modosit(Sender: TObject);
begin
	modosult := true;
end;

procedure TAlgepbeDlg.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
	EgyebDlg.FormReturn(Key);
end;

procedure TAlgepbeDlg.MaskKeyPress(Sender: TObject; var Key: Char);
begin
  	with Sender as TMaskEdit do begin
  		Key := EgyebDlg.Jochar(Text,Key,MaxLength,Tag);
  	end;
end;

procedure TAlgepbeDlg.MaskExit(Sender: TObject);
begin
	SzamExit(Sender);
end;

procedure TAlgepbeDlg.MClick(Sender: TObject);
begin
	with Sender as TMaskEdit do begin
		SelectAll;
	end;
end;

procedure TAlgepbeDlg.BitBtn2Click(Sender: TObject);
begin
	EgyebDlg.Calendarbe(M10);
end;

procedure TAlgepbeDlg.BitBtn1Click(Sender: TObject);
begin
	EgyebDlg.Calendarbe(M11, True);
end;

procedure TAlgepbeDlg.MDatumExit(Sender: TObject);
begin
	DatumExit(Sender, false);
end;

procedure TAlgepbeDlg.M11AExit(Sender: TObject);
begin
	DatumExit(Sender, false);
end;

procedure TAlgepbeDlg.MaskEdit11Exit(Sender: TObject);
begin
	DatumExit(Sender, false);
end;

procedure TAlgepbeDlg.ButValVevoClick(Sender: TObject);
begin
	{Alv�llalkoz� beolvas�sa}
 if EgyebDlg.v_evszam<'2013' then
 begin
	Application.CreateForm(TAlvallalUjFmDlg,AlvallalUjFmDlg);
	AlvallalUjFmDlg.valaszt	:= true;
	AlvallalUjFmDlg.ShowModal;
	if AlvallalUjFmDlg.ret_vkod <> '' then begin
		M1A.Text	:= AlvallalUjFmDlg.ret_vkod;
		M1B.Text	:= AlvallalUjFmDlg.ret_vnev;
  end;
  AlvallalUjFmDlg.Destroy;
 end
 else
 begin
  Application.CreateForm(TVevoUjFmDlg, VevoUjFmDlg);
	VevoUjFmDlg.valaszt	:= true;
	VevoUjFmDlg.ShowModal;
	if VevoUjFmDlg.ret_vkod <> '' then begin
		M1A.Text	:= VevoUjFmDlg.ret_vkod;
		M1B.Text	:= VevoUjFmDlg.ret_vnev;
  end;
  VevoUjFmDlg.Destroy;
 end;
end;

end.
