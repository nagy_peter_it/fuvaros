unit RESTAPI_Kozos;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  System.IOUtils, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, IPPeerClient, REST.Client, REST.Types,
  Data.Bind.Components, Data.Bind.ObjectScope, Generics.Collections, Data.DBXJSON,
  Data.DB, Vcl.Grids, Vcl.DBGrids, REST.Response.Adapter, Vcl.Imaging.jpeg,
  Data.Win.ADODB, Vcl.StdCtrls, JOSE_Encoding_Base64, Vcl.ExtCtrls, HTTPRutinok,
  {System.Win.ScktComp, IdHTTPWebsocketClient, superobject, IdSocketIOHandling,}
  cefvcl, ceflib, Vcl.ImgList, IdBaseComponent, IdComponent, IdTCPConnection,
  IdTCPClient, IdHTTP;

type
  TCEFDelphiClass = class(TObject)
    private
      FValue: string;
    protected
      procedure SetValue(Value: string);
      function GetValue: string;
    public
      property Value: string read GetValue write SetValue;
    end;

type
  THTTPFormParamList = TDictionary<string,string>;
  TUserStatusList = TDictionary<string,string>;
  TJSONResult = record
      JSONContent: TJSONValue;
      ResponseCode: integer;
      ErrorString: string;
      end;
  TStringResult = record
      ResultString: string;
      Success: boolean;
      end;

  TRefreshCallbackProc = procedure() of object;
type
  TRESTAPIKozosDlg = class(TForm)
    RESTClient1: TRESTClient;
    RESTRequest1: TRESTRequest;
    RESTResponse1: TRESTResponse;
    TokenRequest: TRESTRequest;
    TokenResponse: TRESTResponse;
    Memo1: TMemo;
    RESTRequest_proba: TRESTRequest;
    RESTResponse_proba: TRESTResponse;
    Button1: TButton;
    Button2: TButton;
    Chromium1: TChromium;
    ScrollBox1: TScrollBox;
    Button3: TButton;
    Image1: TImage;
    imageCache: TImageList;
    Button4: TButton;
    IdHTTP1: TIdHTTP;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Chromium1ConsoleMessage(Sender: TObject;
      const browser: ICefBrowser; const message, source: ustring; line: Integer;
      out Result: Boolean);
    procedure FormShow(Sender: TObject);
    procedure MyExecuteJavaScript(const JSString: string);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
  private
    function StringEncodeAsUTF8_REST(str: string): ansistring;
    procedure SetRequestMethod(RMethod: string);
    function ParamListToString(const AParamsList: THTTPFormParamList): string;
    function Base64HTMLPatch(const S: string): string;
  public
    EmptyParamsList, RESTAPIParamsList: THTTPFormParamList;
    // KozosResponseValue: string;
    // KozosResponseCode: integer;
    APIToken: string;
    // UserStatusList: TUserStatusList;
    procedure InitTokenRequest(const APhone, APassword: string);
    function GetNewToken(var ResponseCode: integer; var ErrorText: string): boolean;
    function ExecRequest(const AResource, AHTTPMethod: string; AParamsList: THTTPFormParamList): TJSONResult;
    function ExecRequestCore(const AResource, AHTTPMethod, AAuthToken: string; AParamsList: THTTPFormParamList;
         var ResponseCode: integer; var ErrorText: string): string;
    function RESTTimeStampFormat(const S: string): string;
    function GetMagyarKezdobetu(const S: string): string;
    procedure Fuvaros_backend_szinkron;
    function AddUserToBackend(const XUID, DO_NAME, DO_EMAIL, TE_TELSZAM, OVERKILL: string): string;
    function GetListOp(CL: string; N: integer; Sep: string): string;
    function SepListLength(Sep, Miben: string): integer;
    function SepRest(Sep, Miben: string): string;
    procedure ConvertFileToBase64(const AInFileName, AAttachmime: string; var AAttachBase64, AThumbBase64: string);
    function ConvertStreamToBase64(AInStream: TMemoryStream): string;
    function File2String(FileName: string): string;
    procedure String2File(const S, FileName: string);
    function GetURLPicture(AImage: TImage; const AFullUrl, AMimeType, ImageCacheID: string): string;
    function SaveURLFile(const AFullUrl, AMimeType: string): string;
    procedure DataSlice(Data:string;Substr:String;var first,rest:string);
    function ListabanBenne(const Elem, Lista, Sep: string): boolean;
    // procedure StartWebSocket(SocketProc: TMyCallbackProc);
    function GetAttachmentMime(const AFileNev: string): string;
    function GetMimeExtension(const AMime: string): string;
    function GetCsoportTagok(const AGroupID: string): TStringResult;
    procedure RefreshAllUserStatus;
    function GetUserPiheno(const AXUID: string): boolean;
    function GetLegyenSurgos(const AXUID: string): boolean;
    function DeleteMessage(const AMessageID: string): string;
    function DeleteThread(const AMessageID: string): string;
    procedure SendAvatar(const AAvatarResource, Axuid, Ajpegbase64: string; ACsendesMod: boolean = False);
    procedure csoportos_process;
  end;

const
  cUserResource = 'admin/users';
  cGroupResource = 'admin/groups';
  cMessageResource = 'messages/';
  cThreadDeleteResource = 'messages/delete_so_far/';
  cUserGroupResource = 'groups';
  cGroupUsersResource = 'users';
  cUserAvatarResource = 'admin/users/%s/avatar';  // admin/users/:xuid/avatar
  cGroupAvatarResource = 'admin/groups/%s/avatar';  // admin/groups/:xgid/avatar
  cGroupArchiveResource = 'admin/groups/%s/archive';  // admin/groups/:xgid/archive
  cUserDataResource = '/users/contacts/user/%s';  // /users/contacts/user/:xuid
  cGetMeResource = 'users/me';
  cThumbMaxSize = 100;
  C_CLIENT_EVENT = 'CLIENT_TO_SERVER_EVENT_TEST';
  C_SERVER_EVENT = 'SERVER_TO_CLIENT_EVENT_TEST';
  WM_ONSHOWCALLBACK = WM_USER + 565;
  CRLF = chr(13)+chr(10);

  pdfmime = 'application/pdf';
  docxmime = 'application/vnd.openxmlformats-officedocument.wordprocessingml.document';
  docmime = 'application/msword';
  xlsxmime = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
  xlsmime = 'application/vnd.ms-excel';

var
  RESTAPIKozosDlg: TRESTAPIKozosDlg;
  //WebsocketClient: TIdHTTPWebsocketClient;
  done: Boolean;
  MyCEFDelphiClass: TCEFDelphiClass;

implementation

{$R *.dfm}

  uses JpegConv, Egyeb, DateUtils, Kozos, J_SQL, UzenetTop, URLCache, KozosTipusok;


procedure TCEFDelphiClass.SetValue(Value: string);
begin
   if (Value <> FValue) then begin
      FValue := Value;
      RESTAPIKozosDlg.Memo1.Lines.Add('JavaScript tells us: ' + FValue);
      // FValue := FValue + ' This is a modification from Delphi!';  // can return value do JavaScript
      end; // if
    end;

function TCEFDelphiClass.GetValue: string;
begin
   Result := 'This is the result from the getter: ' + FValue;
end;

function TRESTAPIKozosDlg.ExecRequest(const AResource, AHTTPMethod: string; AParamsList: THTTPFormParamList): TJSONResult;
var
  ResponseCode, TResp: integer;
  Res, S, ErrorText, TErr: string;
  jsRootValue: TJSONValue;
begin
  Res:= ExecRequestCore(AResource, AHTTPMethod, APIToken, AParamsList, ResponseCode, ErrorText);
  if (ResponseCode = 401) or (ResponseCode = 403) then begin  // vsz. lej�rt a token
      if GetNewToken(TResp, TErr) then begin
        Res:= ExecRequestCore(AResource, AHTTPMethod, APIToken, AParamsList, ResponseCode, ErrorText);
        end;
      end;
  Result.ResponseCode:= ResponseCode;
  Result.JSONContent:= TJSONObject.ParseJSONValue(Res);
  if not ((ResponseCode >= 200) and (ResponseCode <= 299)) then begin
        S:='Resource: '+RESTRequest1.Resource+'('+AHTTPMethod+'), Params: ['+ParamListToString(AParamsList)+'], HTTP k�d: '+ IntToStr(ResponseCode)+', �zenet: '+ ErrorText;
        Result.ErrorString:= S;  // function
        end
  else begin
        Result.ErrorString:= '';
        end; // else
end;

function TRESTAPIKozosDlg.GetNewToken(var ResponseCode: integer; var ErrorText: string): boolean;
var
  S: string;
  jsRootValue: TJSONValue;
begin
    TokenRequest.Execute;
    ResponseCode:= TokenResponse.StatusCode;
    ErrorText:= TokenResponse.ErrorMessage;
    if ((ResponseCode >= 200) and (ResponseCode <= 299)) then begin
      S:= TokenResponse.Content;
      jsRootValue:= TJSONObject.ParseJSONValue(S);
      APIToken:= ((jsRootValue as TJSONObject).Get('token').JsonValue as TJSONString).Value;
      Result:= True;
      end
    else begin
      Result:= False;
      end;
end;


function TRESTAPIKozosDlg.ExecRequestCore(const AResource, AHTTPMethod, AAuthToken: string; AParamsList: THTTPFormParamList;
    var ResponseCode: integer; var ErrorText: string): string;
var
  i, ResponseStatus: integer;
  ParamName, S: string;
begin
   RestRequest1.Resource:= AResource;
   SetRequestMethod(AHTTPMethod);
   RESTRequest1.Params.Clear;
   // ----- header: charset ------ //
   {RESTRequest1.Params.AddItem;
   with RESTRequest1.Params[RESTRequest1.Params.Count-1] do begin
      Name:= 'Content-Type';
      // magyar sz�veghez ez kellett
      // Value:= 'application/json;charset=UTF-8';
      // -----------------------------
      // Value:= 'application/x-www-form-urlencoded';  // a k�p felt�lt�s?
      Kind := pkHTTPHEADER;
      Options:= [poDoNotEncode];
      end;  // with
      }
   // ----- header: authorization ------ //
   RESTRequest1.Params.AddItem;
   with RESTRequest1.Params[RESTRequest1.Params.Count-1] do begin
      Name:= 'Authorization';
      Value:= 'Bearer '+AAuthToken;
      Kind := pkHTTPHEADER;
      Options:= [poDoNotEncode];
      end;  // with
   for ParamName in AParamsList.Keys do begin
        RESTRequest1.Params.AddItem;
        with RESTRequest1.Params[RESTRequest1.Params.Count-1] do begin
          Name:= ParamName;
          if (ParamName = 'attachment') or (ParamName = 'thumbnail') then begin
            Value:= AParamsList[ParamName];
            end
          else begin
            Value:= StringEncodeAsUTF8_REST(AParamsList[ParamName]);
            end;
          // --- sz�veges inf�hoz ez kell ------
          ContentType := ctAPPLICATION_X_WWW_FORM_URLENCODED;
          // --- k�phez? ------
          // ContentType := ctAPPLICATION_OCTET_STREAM;
          {if (ParamName = 'attachment') or (ParamName = 'thumbnail') then begin
            ContentType := ctAPPLICATION_X_WWW_FORM_URLENCODED;
            end
          else begin
            ContentType := ctAPPLICATION_X_WWW_FORM_URLENCODED;
            end;
            }
          Kind := pkGETorPOST;
          Options:= [poDoNotEncode];
          end;  // with
        end;  // for
   try
     RESTRequest1.Execute;
   except
     on E: exception do begin
        // benne lesz a RESTResponse1.ErrorMessage-ben
        // MessageDlg(E.Message, mtError,[mbOK],0);
        end;
     end;
   ResponseCode:= RESTResponse1.StatusCode;
   ErrorText:= RESTResponse1.ErrorMessage;
   Result:= RESTResponse1.Content;
end;

function TRESTAPIKozosDlg.ParamListToString(const AParamsList: THTTPFormParamList): string;
var
   ParamName, S: string;
begin
  S:= '';
  for ParamName in AParamsList.Keys do begin
    S:= S+ ParamName+'='+AParamsList[ParamName]+'; '
    end;  // for
  Result:= S;
end;

procedure TRESTAPIKozosDlg.InitTokenRequest(const APhone, APassword: string);
begin
   TokenRequest.Resource:= 'auth/login';
   TokenRequest.Params.Clear;
   // ----- header: charset ------ //
   TokenRequest.Params.AddItem;
   with TokenRequest.Params[TokenRequest.Params.Count-1] do begin
      Name:= 'phone';
      Value:= APhone;
      ContentType := ctAPPLICATION_X_WWW_FORM_URLENCODED;
      Kind := pkGETorPOST;
      Options:= [poDoNotEncode];
      end;  // with
   TokenRequest.Params.AddItem;
   with TokenRequest.Params[TokenRequest.Params.Count-1] do begin
      Name:= 'password';
      Value:= APassword;
      ContentType := ctAPPLICATION_X_WWW_FORM_URLENCODED;
      Kind := pkGETorPOST;
      Options:= [poDoNotEncode];
      end;  // with
end;

procedure TRESTAPIKozosDlg.SetRequestMethod(RMethod: string);
begin
    if RMethod='get' then RESTRequest1.Method:= rmGET;
    if RMethod='post' then RESTRequest1.Method:= rmPOST;
    if RMethod='put' then RESTRequest1.Method:= rmPUT;
    if RMethod='delete' then RESTRequest1.Method:= rmDELETE;
end;

function TRESTAPIKozosDlg.StringEncodeAsUTF8_REST(str: string): ansistring;
// const
 //  UnicodeAA = AnsiString(0xC3);     //(0xC3 0xA1);    (#195#161);
  // UnicodeAA = AnsiChar(Integer($C3))+AnsiChar(Integer($A1));     //(0xC3 0xA1);    (#195#161);
begin
  str:=StringReplace(str, '�', AnsiChar(Integer($C3))+AnsiChar(Integer($81)), [rfReplaceAll]);
  str:=StringReplace(str, '�', AnsiChar(Integer($C3))+AnsiChar(Integer($89)), [rfReplaceAll]);
  str:=StringReplace(str, '�', AnsiChar(Integer($C3))+AnsiChar(Integer($8D)), [rfReplaceAll]);
  str:=StringReplace(str, '�', AnsiChar(Integer($C3))+AnsiChar(Integer($93)), [rfReplaceAll]);
  str:=StringReplace(str, '�', AnsiChar(Integer($C3))+AnsiChar(Integer($96)), [rfReplaceAll]);
  str:=StringReplace(str, '�', AnsiChar(Integer($C3))+AnsiChar(Integer($90)), [rfReplaceAll]);
  str:=StringReplace(str, '�', AnsiChar(Integer($C3))+AnsiChar(Integer($9A)), [rfReplaceAll]);
  str:=StringReplace(str, '�', AnsiChar(Integer($C3))+AnsiChar(Integer($9C)), [rfReplaceAll]);
  str:=StringReplace(str, '�', AnsiChar(Integer($C3))+AnsiChar(Integer($B0)), [rfReplaceAll]);

  str:=StringReplace(str, '�', AnsiChar(Integer($C3))+AnsiChar(Integer($A1)), [rfReplaceAll]);
  str:=StringReplace(str, '�', AnsiChar(Integer($C3))+AnsiChar(Integer($A9)), [rfReplaceAll]);
  str:=StringReplace(str, '�', AnsiChar(Integer($C3))+AnsiChar(Integer($AD)), [rfReplaceAll]);
  str:=StringReplace(str, '�', AnsiChar(Integer($C3))+AnsiChar(Integer($B3)), [rfReplaceAll]);
  str:=StringReplace(str, '�', AnsiChar(Integer($C3))+AnsiChar(Integer($B6)), [rfReplaceAll]);
  str:=StringReplace(str, '�', AnsiChar(Integer($C5))+AnsiChar(Integer($91)), [rfReplaceAll]);
  str:=StringReplace(str, '�', AnsiChar(Integer($C3))+AnsiChar(Integer($BA)), [rfReplaceAll]);
  str:=StringReplace(str, '�', AnsiChar(Integer($C3))+AnsiChar(Integer($BC)), [rfReplaceAll]);
  str:=StringReplace(str, '�', AnsiChar(Integer($C5))+AnsiChar(Integer($B1)), [rfReplaceAll]);
  Result:= str;
end;

procedure TRESTAPIKozosDlg.FormCreate(Sender: TObject);
begin
   RESTClient1.BaseURL:= EgyebDlg.TRUCKPIT_BASE_URL;
   EmptyParamsList:= THTTPFormParamList.Create;
   RESTAPIParamsList:= THTTPFormParamList.Create;
   // UserStatusList:= TUserStatusList.Create;
   APIToken:= '';
   imageCache.Clear;

   // MyCEFDelphiClass:= TCEFDelphiClass.Create;   // nem biztos hogy kell

   // TESZT InitTokenRequest('2', '12345678');

   // WebsocketClient := TIdHTTPWebsocketClient.Create(Self);
   // WebsocketClient.Port := 443;
   // WebsocketClient.Host := 'wss://soforbackend.herokuapp.com/msgbus';
   // WebsocketClient.Host := 'wss://soforbackend.herokuapp.com:443/msgbus';
   // WebsocketClient.SocketIOCompatible := True;
   // WebsocketClient.SocketIO.OnEvent(C_SERVER_EVENT,
    {procedure(const ASocket: ISocketIOContext; const aArgument: TSuperArray; const aCallback: ISocketIOCallback)
    begin
      Memo1.Lines.Add('Data PUSHED from server: ' + aArgument[0].AsJSon);
      //server wants a response?
      if aCallback <> nil then
        aCallback.SendResponse('thank for the push!');
    end);
         }
end;

procedure TRESTAPIKozosDlg.FormDestroy(Sender: TObject);
begin
   if EmptyParamsList<> nil then EmptyParamsList.Free;
   if RESTAPIParamsList<> nil then RESTAPIParamsList.Free;
   // if UserStatusList<> nil then UserStatusList.Free;
end;

procedure TRESTAPIKozosDlg.FormShow(Sender: TObject);
begin
  if not done then begin
     // I want to have the application show its window, then start loading the webpage.
     PostMessage(Handle, WM_ONSHOWCALLBACK, 0, 0);
     done := true;
  end;
  while Chromium1.Browser.IsLoading do Application.ProcessMessages;
end;

function TRESTAPIKozosDlg.RESTTimeStampFormat(const S: string): string;
// bemenet: "2018-05-14T13:37:13.945Z" , universal date-time

var
  D: string;
  DT, DLocalDateTime: TDateTime;
  InputOK: boolean;

 function GetNumberPart(const FromPos, Digits: integer): integer;
   begin
      try
        Result:= StrToInt(copy(S, FromPos, Digits));
      except
        // NoticeKi('Hib�s timestamp form�tum! [RESTTimeStampFormat]: '+S);
        InputOK:= False;
        Result:= 0;
        end; // try-except
   end;

begin
  InputOK:= True;
  DT := EncodeDateTime(GetNumberPart(1,4),
                       GetNumberPart(6,2),
                       GetNumberPart(9,2),
                       GetNumberPart(12,2),
                       GetNumberPart(15,2),
                       GetNumberPart(18,2),
                       0);
  if InputOK then begin
    DLocalDateTime:= TTimeZone.Local.ToLocalTime(DT);
    D:= formatdatetime('yyyy.mm.dd. HH:mm:ss', DLocalDateTime);
    if copy(D,1,11) = formatdatetime('yyyy.mm.dd.', now) then begin
        if copy(D,13,5) = formatdatetime('HH:mm', now) then
            Result:= 'most'
        else Result:= copy(D,13,5);
        end
    else Result:= copy(D,1,11);
    end
  else begin
    Result:= '<format error>';
    end;
end;


{
function TRESTAPIKozosDlg.RESTTimeStampFormat(const S: string): string;
// bemenet: "2018-05-14T13:37:13.945Z"
var
  D: string;
begin
    D:=  copy(S,1,4)+'.'+   // YYYY
              copy(S,6,2)+'.'+   // MM
              copy(S,9,2)+'. '+  // DD
              copy(S,12,5);      // HH:MM
    if copy(D,1,11) = formatdatetime('yyyy.mm.dd.', now) then begin
        if copy(D,13,5) = formatdatetime('HH:mm', now) then
          Result:= 'most'
        else Result:= copy(D,13,5);
        end
    else Result:= copy(D,1,11);
end;
 }


function TRESTAPIKozosDlg.GetMagyarKezdobetu(const S: string): string;
var
  S2, S3: string;
begin
  S3:=LowerCase(copy(S,1,3));
  S2:=LowerCase(copy(S,1,2));
  if (S3='dzs') then
    Result:= copy(S,1,3)
  else if (S2='sz') or (S2='zs') or (S2='ty') or (S2='gy') or (S2='ny') or (S2='cs') then
    Result:= copy(S,1,2)
  else Result:= copy(S,1,1);
end;


{ Behalt pr�b�lkoz�sok...
function TRESTAPIKozosDlg.GetMagyarKezdobetu(const S: string): string;
//type
  // TTriplabetuk = set of 'dzs'; // Dzsudzs�k
  // TDuplabetuk = set of 'sz', 'zs', 'ty', 'gy', 'ny', 'cs';
 // TBetuHalmaz = set of 'aaa'..'zzz';
// var
 //  Triplabetuk, Duplabetuk: TBetuHalmaz
const
  triplabetuk : array[1..1] of string = ('dzs'); // Dzsudzs�k
  duplabetuk : array[1..6] of string = ('sz', 'zs', 'ty', 'gy', 'ny', 'cs');
begin
  // Triplabetuk:= ['dzs'];
  // Duplabetuk:= ['sz','zs','ty','gy','ny','cs'];
  if LowerCase(copy(S,1,3)) in Triplabetuk then
    Result:= copy(S,1,3)
  else if LowerCase(copy(S,1,2)) in Duplabetuk then
    Result:= copy(S,1,2)
  else Result:= copy(S,1,1);
end
end;
}

procedure TRESTAPIKozosDlg.csoportos_process;
var
   S, XUID, TELSZAM, NEV, RequestRes: string;
   Archiv: boolean;
   RequestResult: TJSONResult;
   jsUserArray: TJSONArray;
   jsActUser, jsKapcs: TJSONValue;
   XUIDList: TStringList;
   JSONResult: TJSONResult;
begin
    RequestResult:= RESTAPIKozosDlg.ExecRequest(Format('%s', [cUserResource]), 'get', EmptyParamsList);
    if RequestResult.ErrorString <> '' then begin
      Memo1.Lines.Add(RequestResult.ErrorString);
      Exit;
      end;
    XUIDList:= TStringList.Create;
    try
        jsUserArray:= RequestResult.JSONContent as TJSONArray;
        // fill XUIDLIST
        for jsActUser in jsUserArray do begin
          XUID:= ((jsActUser as TJSONObject).Get('xuid').JsonValue as TJSONString).Value;
          XUIDList.Add(XUID);  // a backend-en l�tez� �sszes user
          end;  // for
        // process
        for jsActUser in jsUserArray do begin
          XUID:= ((jsActUser as TJSONObject).Get('xuid').JsonValue as TJSONString).Value;
          TELSZAM:= ((jsActUser as TJSONObject).Get('phone').JsonValue as TJSONString).Value;
          NEV:= ((jsActUser as TJSONObject).Get('name').JsonValue as TJSONString).Value;
          Archiv:= ((jsActUser as TJSONObject).Get('archived').JsonValue is TJSONTrue);
          // duplik�tumok archiv�l�sa
          {if (not Archiv) and (copy(XUID,1,5) = 'SPEED') and (XUIDList.IndexOf(copy(XUID,6,999))>= 0) then begin
              Memo1.Lines.Add('Archiv�land�: '+XUID+ ' = ' +NEV );
              RequestRes:= 'admin/users'+'/'+XUID;  // elem m�dos�t�sa
              RESTAPIKozosDlg.RESTAPIParamsList.Clear;
              RESTAPIKozosDlg.RESTAPIParamsList.Add('archived', 'true');
              JSONResult:= RESTAPIKozosDlg.ExecRequest(RequestRes, 'put', RESTAPIKozosDlg.RESTAPIParamsList);
              end
          else if copy(TELSZAM,1,1)=' ' then begin
              Memo1.Lines.Add('Sz�k�z kivesz: '+XUID+' <'+ TELSZAM+'>');
              RequestRes:= 'admin/users'+'/'+XUID;  // elem m�dos�t�sa
              RESTAPIKozosDlg.RESTAPIParamsList.Clear;
              RESTAPIKozosDlg.RESTAPIParamsList.Add('phone', copy(TELSZAM,2,999));
              JSONResult:= RESTAPIKozosDlg.ExecRequest(RequestRes, 'put', RESTAPIKozosDlg.RESTAPIParamsList);
              end;
              }
          // telefonsz�m v�g�re X
          {if Archiv and (copy(TELSZAM, length(TELSZAM),1) <> 'x') then begin
              Memo1.Lines.Add('Telsz�mba X arch�v usern�l: '+XUID+' <'+ TELSZAM+'>');
              RequestRes:= 'admin/users'+'/'+XUID;  // elem m�dos�t�sa
              RESTAPIKozosDlg.RESTAPIParamsList.Clear;
              RESTAPIKozosDlg.RESTAPIParamsList.Add('phone', TELSZAM+'x');
              JSONResult:= RESTAPIKozosDlg.ExecRequest(RequestRes, 'put', RESTAPIKozosDlg.RESTAPIParamsList);
              end;
              }
          end;  // for
    finally
        if XUIDList<> nil then XUIDList.Free;
        end;  // finally
end;

procedure TRESTAPIKozosDlg.Fuvaros_backend_szinkron;
const
  BelsoSzep = '^';
var
  S, XUID, DO_NAME, DO_EMAIL, TE_TELSZAM, CSOPKOD, URLResource, GroupID: string;
  HibaString, Res, LookupXUID, CegXuidFrefix, OVERKILL: string;
  RequestResult: TJSONResult;
  jsActUser, jsKapcs: TJSONValue;
  jsCsoportArray, jsUserArray: TJSONArray;
  MarTagja, Megvan: boolean;
  XUIDList: TStringList;
  i: integer;
begin
  CegXuidFrefix:= EgyebDlg.Read_SZGrid('999', 'TRCKPT_XUID_PREFIX');  // 'SPEED' vagy 'TRANS', egyel�re

  XUIDList:= TStringList.Create;
  try
    try
      // l�tez� felhaszn�l�k list�ja
      RequestResult:= RESTAPIKozosDlg.ExecRequest(Format('%s', [cUserResource]), 'get', EmptyParamsList);
      if RequestResult.ErrorString <> '' then begin
        Memo1.Lines.Add(RequestResult.ErrorString);
        Exit;
        end;
      jsUserArray:= RequestResult.JSONContent as TJSONArray;
      for jsActUser in jsUserArray do begin
        XUID:= ((jsActUser as TJSONObject).Get('xuid').JsonValue as TJSONString).Value;
        XUIDList.Add(XUID);  // a backend-en l�tez� �sszes user
        end;  // for

      S:= 'select DO_KOD, DO_NAME, DO_EMAIL, TE_TELSZAM, '+
          ' case when DC_CSKOD IN (510,520,530,580,600) then '''+CegXuidFrefix+'TG1'' '+
          ' when DC_CSKOD IN (540,560) then '''+CegXuidFrefix+'TG2''  '+
          ' when DC_CSKOD IN (550) then '''+CegXuidFrefix+'TG3'' end CSOPKOD, '+
          ' 0 OVERKILL '+
          ' from DOLGOZO '+
          ' left outer join TELKESZULEK on TK_DOKOD = DO_KOD '+
          ' left outer join TELELOFIZETES on TE_TKID = TK_ID '+
          ' left outer join DOLGOZOCSOP on DC_DOKOD = DO_KOD '+
          ' where DO_ARHIV=0 '+
          ' and TE_TELSZAM is not null '+
          ' and DC_CSKOD IN (510,520,530,580,600,540,560,550) '+
          ' and DC_DATTOL= (select max(cs2.DC_DATTOL) from DOLGOZOCSOP cs2 where cs2.DC_DOKOD=DO_KOD and DC_DATTOL<=CONVERT(varchar(11), getdate(),102)+''.'') '+
          ' union '+
          ' select DO_KOD, DO_NAME, DO_EMAIL, TE_TELSZAM, x1.CSOPKOD, '+
          ' 1 '+  // overkill
          ' from (select '''+CegXuidFrefix+'TG1'' CSOPKOD union select '''+CegXuidFrefix+'TG2'' union select '''+CegXuidFrefix+'TG3'') x1, DOLGOZO '+
          ' left outer join TELKESZULEK on TK_DOKOD = DO_KOD '+
          ' left outer join TELELOFIZETES on TE_TKID = TK_ID '+
          ' left outer join DOLGOZOCSOP on DC_DOKOD = DO_KOD '+
          ' where DO_ARHIV=0 '+
          ' and TE_TELSZAM is not null '+
          ' and DC_CSKOD IN (100,200,300,400,401,500) '+
          ' and DC_DATTOL= (select max(cs2.DC_DATTOL) from DOLGOZOCSOP cs2 where cs2.DC_DOKOD=DO_KOD and DC_DATTOL<=CONVERT(varchar(11), getdate(),102)+''.'') '+
          ' order by DO_NAME';

        EgyebDlg.Query3.SQL.Clear;
        EgyebDlg.Query3.SQL.Add(S);
        EgyebDlg.Query3.Open;
        HibaString:= '';
        Memo1.Lines.Clear;
        while not EgyebDlg.Query3.eof do begin
            // XUID:= EgyebDlg.Query3.FieldByName('DO_KOD').AsString;
            XUID:= CegXuidFrefix+EgyebDlg.Query3.FieldByName('DO_KOD').AsString;
            CSOPKOD:= EgyebDlg.Query3.FieldByName('CSOPKOD').AsString;
            OVERKILL:= EgyebDlg.Query3.FieldByName('OVERKILL').AsString;
            if XUIDList.IndexOf(XUID)>= 0 then begin  // ha benne van a list�ban
              Megvan:= False;
              for jsActUser in jsUserArray do begin // az elej�n k�rt "jsUserArray"-on megy�nk v�gig
                LookupXUID:= ((jsActUser as TJSONObject).Get('xuid').JsonValue as TJSONString).Value;
                if LookupXUID = XUID then begin
                  Megvan:= True;
                  Break;
                  end; // if benne
                end;  // for
               if Megvan then begin
                  jsCsoportArray:= (jsActUser as TJSONObject).Get('Groups').JsonValue as TJSONArray;
                  MarTagja:= False;
                  for jsKapcs in jsCsoportArray do begin
                    GroupID:= ((jsKapcs as TJSONObject).Get('xgid').JsonValue as TJSONString).Value;
                    if GroupID = CSOPKOD then begin
                      MarTagja:= True;
                      Break;  // exit "for" loop
                      end; // if
                    end;  // for
                 end; // if Megvan
               end  // if XUIDList.IndexOf
            else begin   // nem tal�ltuk a user-t
              DO_NAME:= Trim(EgyebDlg.Query3.FieldByName('DO_NAME').AsString);
              DO_EMAIL:= Trim(EgyebDlg.Query3.FieldByName('DO_EMAIL').AsString);
              TE_TELSZAM:= Trim(EgyebDlg.Query3.FieldByName('TE_TELSZAM').AsString);
              if copy(TE_TELSZAM,1,1) = '+' then begin
                TE_TELSZAM:= copy(TE_TELSZAM,2,9999);  // 3620....
                end;
              // debug
              Res:= AddUserToBackend(XUID, DO_NAME, DO_EMAIL, TE_TELSZAM, OVERKILL);
              // Res:= '';
              // -------------------
              if Res <> '' then begin
                 HibaString:= HibaString + BelsoSzep + Res;
                 end  // if
              else begin // ha nem volt hiba
                Memo1.Lines.Add('New user: '+XUID+' | '+DO_NAME+' | '+DO_EMAIL+' | '+TE_TELSZAM+' | '+OVERKILL);
                XUIDList.Add(XUID);
                end;
              MarTagja:= False;
              end;  // else
            if (XUIDList.IndexOf(XUID)>= 0) and not MarTagja then begin
                URLResource:= Format('%s/%s/%s/%s', [cUserResource, XUID, 'groups', CSOPKOD]);
                // debug
                RequestResult:= RESTAPIKozosDlg.ExecRequest(URLResource, 'post', RESTAPIKozosDlg.EmptyParamsList);
                // RequestResult.ErrorString:= '';
                // -------------------
                Memo1.Lines.Add('Add user to group: '+XUID+' | '+CSOPKOD);
                if RequestResult.ErrorString <> '' then begin
                   HibaString:= HibaString + BelsoSzep+ Res;
                   end;  // if
                end;  // if not MarTagja
            EgyebDlg.Query3.Next;
            end;  // while

      if HibaString <> '' then begin
          for i:=0 to SepListLength(BelsoSzep, HibaString) do begin
            S:= GetListOp(HibaString, i, BelsoSzep);
            if S <> '' then begin
              Memo1.Lines.Add('--------------------------------------------------');
              Memo1.Lines.Add(S);
              end;  // if
            end;  // for
          end;
   except
     on E: Exception do begin
        Memo1.Lines.Add(E.Message);
        end; // on E
     end;  // try-except

  finally
      if XUIDList<> nil then XUIDList.Free;
      end;  // finally
end;

function TRESTAPIKozosDlg.AddUserToBackend(const XUID, DO_NAME, DO_EMAIL, TE_TELSZAM, OVERKILL: string): string;
var
   JSONResult: TJSONResult;
   S, over: string;
begin
   if OVERKILL = '1' then over:= 'true'
   else over:= 'false';
   RESTAPIKozosDlg.RESTAPIParamsList.Clear;
   RESTAPIKozosDlg.RESTAPIParamsList.Add('xuid', XUID);
   RESTAPIKozosDlg.RESTAPIParamsList.Add('name', DO_NAME);
   RESTAPIKozosDlg.RESTAPIParamsList.Add('phone', TE_TELSZAM);
   RESTAPIKozosDlg.RESTAPIParamsList.Add('overkill', over);
   if DO_EMAIL='' then
      S:= 'js'+copy(TE_TELSZAM,4,99)+'@icloud.com'
   else S:=DO_EMAIL;
   RESTAPIKozosDlg.RESTAPIParamsList.Add('email', S);
   // RESTAPIKozosDlg.RESTAPIParamsList.Add('admin', 'false');
   RESTAPIKozosDlg.RESTAPIParamsList.Add('password', '123456');
   RESTAPIKozosDlg.RESTAPIParamsList.Add('password_confirmation', '123456');

   JSONResult:= RESTAPIKozosDlg.ExecRequest(cUserResource, 'post', RESTAPIKozosDlg.RESTAPIParamsList);
   Result:= JSONResult.ErrorString;
end;


function TRESTAPIKozosDlg.GetListOp(CL: string; N: integer; Sep: string): string;
var T:string;
    i: integer;

    function SepFrontToken(Sep, Miben: string): string;
    var  P:integer;
    begin
         P:=Pos(Sep, Miben);
         if P>0 then
            SepFrontToken:=Copy(Miben, 1, P-1)
         else
            SepFrontToken:=Miben;
    end;
begin
     i:=0;
     T:=SepFrontToken(Sep, CL);
     while i<N do begin
           CL:=Copy(CL,Length(T)+Length(Sep)+1,Length(CL)-Length(T)-Length(Sep));
           T:=SepFrontToken(Sep, CL);
           i:=i+1;
           end;
     GetListOp:=T;
end;

function TRESTAPIKozosDlg.SepListLength(Sep, Miben: string): integer;
var H, T, Res: string;
    P: integer;
begin
    T:=Miben;
    P:=0;
    repeat
       T:=SepRest(Sep,T);
       Inc(P);
       until T='';
    Result:=P;
end;

function TRESTAPIKozosDlg.SepRest(Sep, Miben: string): string;
var  P:integer;
begin
     P:=Pos(Sep, Miben);
     if P>0 then
        SepRest:=Copy(Miben, P+Length(Sep), Length(Miben)-Length(Sep)-P+1)
     else
        SepRest:='';
end;

function TRESTAPIKozosDlg.File2String(FileName: string): string;
var
  MyStream: TFileStream;
  MyString: string;
begin
  MyStream := TFileStream.Create(FileName, fmOpenRead or fmShareDenyNone);
  try
    MyStream.Position := 0;
    SetLength(MyString, MyStream.Size);
    MyStream.ReadBuffer(Pointer(MyString)^, MyStream.Size);
  finally
    MyStream.Free;
    end;  // try-finally
  Result := MyString;
end;

procedure TRESTAPIKozosDlg.String2File(const S, FileName: string);
begin
  with TStringList.Create do
   try
    Add(S);
    SaveToFile(FileName);
   finally
    Free;
   end; // try-finally
end;

function TRESTAPIKozosDlg.Base64HTMLPatch(const S: string): string;
var
   temp: string;
begin
  // Result:= StringReplace(S, '+','%2B', [rfReplaceAll]);  // a backend-ben "+" n�lk�l jelent meg a base64 stream. Ez egy workaround.

  // according to https://en.wikipedia.org/wiki/Base64#URL_applications
  temp:= StringReplace(S, '+', '-', [rfReplaceAll]);  // modified Base64 for URL
  temp:= StringReplace(temp, '=', '.', [rfReplaceAll]);  // modified Base64 for URL
  Result:= StringReplace(temp, '/', '_', [rfReplaceAll]);  // modified Base64 for URL
end;

procedure TRESTAPIKozosDlg.Button1Click(Sender: TObject);
begin
   Memo1.Lines.Clear;
   // TCefRTTIExtension.Register('mydelphiclass', TCEFDelphiClass);

   // Chromium1.Browser.MainFrame.LoadString(WebSocketHTMLStr, 'source://html');

   // ClientSocket.Active:= True;

   // WebsocketClient.Connect;
   {
   WebsocketClient.SocketIO.Emit(C_CLIENT_EVENT, SO([ 'request', 'some data']),
    //provide callback
    procedure(const ASocket: ISocketIOContext; const aJSON: ISuperObject; const aCallback: ISocketIOCallback)
    begin
      //show response (threadsafe)
      Memo1.Lines.Add('RESPONSE: ' + aJSON.AsJSon);
    end);
    }
end;


procedure TRESTAPIKozosDlg.MyExecuteJavaScript(const JSString: string);
begin
   Chromium1.Browser.MainFrame.ExecuteJavaScript(JSString,'about:blank', 0);
end;

procedure TRESTAPIKozosDlg.Button2Click(Sender: TObject);
begin
   // Memo1.text:= WebSocketHTMLStr;
end;

procedure TRESTAPIKozosDlg.Button3Click(Sender: TObject);
begin
 //   UploadAllAvatars;
end;


procedure TRESTAPIKozosDlg.Button4Click(Sender: TObject);
begin
  csoportos_process;
end;

procedure TRESTAPIKozosDlg.Chromium1ConsoleMessage(Sender: TObject;
  const browser: ICefBrowser; const message, source: ustring; line: Integer;
  out Result: Boolean);
begin
  // CallbackSocketProc(message);
   // Memo1.Lines.Add(message);
   Result:= True;
end;

procedure TRESTAPIKozosDlg.DataSlice(Data:string;Substr:String;var first,rest:string);
begin
  first:='';
  rest:='';
  if Data[length(Data)]<>substr then Data:=Data+substr;
  first:=Copy(Data,1,Pos(Substr,Data)-1);
  rest:=Copy(Data,Pos(Substr,Data)+1,length(Data)-length(first)-1);
end;


procedure TRESTAPIKozosDlg.ConvertFileToBase64(const AInFileName, AAttachmime: string; var AAttachBase64, AThumbBase64: string);
const
  universal_thumb_jpg = 'Thumb_uni.jpg';
  video_thumb_jpg = 'Thumb_video.jpg';
  pdf_thumb_jpg = 'Thumb_pdf.jpg';
  word_thumb_jpg = 'Thumb_Excel.jpg';
  excel_thumb_jpg = 'Thumb_Word.jpg';
var
  // EredetiS: TJOSEBytes;
  MyStream: TFileStream;
  ThumbMemoryStream: TMemoryStream;
  ThumbFileStream: TFileStream;
  EncodeInput: TBytes;
  ThumbFN: string;
begin
  // EredetiS:= TFile.ReadAllText(AInFileName);
  // EredetiS:= File2String(AInFileName);
  MyStream := TFileStream.Create(AInFileName, fmOpenRead or fmShareDenyNone);
  try
    MyStream.Position := 0;
    SetLength(EncodeInput, MyStream.Size);
    MyStream.Read(EncodeInput[0], MyStream.Size);
    AAttachBase64:= TBase64.Encode(EncodeInput);
    AAttachBase64:= Base64HTMLPatch(AAttachBase64);
    try
     try
      if (AAttachmime='image/jpg') or (AAttachmime='image/png') then begin  // let's make thumbnail
        ThumbMemoryStream:= TMemoryStream.Create;
        MyStream.Position := 0;
        CreateThumbnail(MyStream, ThumbMemoryStream, AAttachmime, 100, 100); // megtartja az ar�nyokat, ez lesz a max m�rete
        SetLength(EncodeInput, 0);
        SetLength(EncodeInput, ThumbMemoryStream.Size);
        ThumbMemoryStream.Position := 0;
        ThumbMemoryStream.Read(EncodeInput[0], ThumbMemoryStream.Size);
        end  // if
     else begin
        ThumbFN:= universal_thumb_jpg;
        if (EgyebDlg.SepRest('/', AAttachmime) = 'video') then ThumbFN:= video_thumb_jpg;
        if (AAttachmime = pdfmime) then ThumbFN:= pdf_thumb_jpg;
        if (AAttachmime = docmime) or (AAttachmime = docxmime) then ThumbFN:= word_thumb_jpg;
        if (AAttachmime = xlsmime) or (AAttachmime = xlsxmime) then ThumbFN:= excel_thumb_jpg;
        if FileExists(ThumbFN) then begin
          ThumbFileStream:= TFileStream.Create(ThumbFN, fmOpenRead or fmShareDenyNone);
          SetLength(EncodeInput, 0);
          SetLength(EncodeInput, ThumbFileStream.Size);
          ThumbFileStream.Position := 0;
          ThumbFileStream.Read(EncodeInput[0], ThumbFileStream.Size);
          end // if FileExists
        else AThumbBase64:= '';
        end;  // else
      AThumbBase64:= TBase64.Encode(EncodeInput);
      AThumbBase64:= Base64HTMLPatch(AThumbBase64);
      except
       AThumbBase64:= '';  // ha hiba van, maradjon �res
       end;
     finally
       if ThumbFileStream <> nil then ThumbFileStream.Free;
       if ThumbMemoryStream <> nil then ThumbMemoryStream.Free;
       end; // try-finally Thumbstream
   finally
    MyStream.Free;
    end;  // try-finally
end;

function TRESTAPIKozosDlg.ConvertStreamToBase64(AInStream: TMemoryStream): string;
var
  EncodeInput: TBytes;
begin
  AInStream.Position := 0;
  SetLength(EncodeInput, AInStream.Size);
  AInStream.Read(EncodeInput[0], AInStream.Size);
  Result:= TBase64.Encode(EncodeInput);
  Result:= Base64HTMLPatch(Result);
end;


function TRESTAPIKozosDlg.SaveURLFile(const AFullUrl, AMimeType: string): string;
var
  FName, GoodExt, Res: string;
  responseStream : TMemoryStream;
begin
  GoodExt:= GetMimeExtension(AMimeType);
  responseStream := TMemoryStream.Create;
  try
    Res:= HttpGetBinary(AFullUrl, responseStream);
    FName:= GetEnvironmentVariable('TEMP')+ '\T'+IntToStr(Random(999999))+'.'+GoodExt;
    responseStream.SaveToFile(FName);
    Result:=FName;
  finally
    responseStream.Free;
    end;  // try-finally
end;


function TRESTAPIKozosDlg.GetURLPicture(AImage: TImage; const AFullUrl, AMimeType, ImageCacheID: string): string;
var
  Res: string;
  responseStream : TMemoryStream;
  JPEGImage: TJPEGImage;
  isCacheOK: boolean;
  UrlcacheItem: TUrlcacheItem;

begin
  UrlcacheItem.cacheid:= ImageCacheID;
  UrlcacheItem.url:= AFullUrl;

  isCacheOK:= URLCacheDlg.MemoryCacheGetObject(UrlcacheItem);  // mindenk�pp p�ld�nyos�tott stream-et ad vissza
  if isCacheOK then begin
     responseStream:= UrlcacheItem.stream;
     responseStream.Position := 0;
      if responseStream.Size>0 then begin  // any content
        if AMimeType = 'image/jpg' then begin
          JPEGImage := TJPEGImage.Create;
          try
             try
              JPEGImage.LoadFromStream(responseStream);
              AImage.Picture.Assign(JPEGImage);
              AImage.AutoSize:= True;
             except
              on E: Exception do begin
                Result:= E.Message;
                end;  // on E
              end;  // try-except
            finally
              JPEGImage.Free;
              end;  // try-finally
            end;  // JPEG
          end  // if any content
      else begin
          Result:= '�res dokumentum!';
          end;
     end;
end;


function TRESTAPIKozosDlg.ListabanBenne(const Elem, Lista, Sep: string): boolean;
begin
  Result:= (Pos(Sep+Elem+Sep, Sep+Lista+Sep) > 0 );  // pl. ",1,2,3,4," tartalmazza ",2," -t
end;

function TRESTAPIKozosDlg.GetMimeExtension(const AMime: string): string;
const
  constVideo = 'video/';
  constImage = 'image/';
var
  attachtipus: string;
begin
   // Result:= EgyebDlg.SepRest('/', AMime);
   if AMime = docxmime then
      Result:= 'docx';
   if AMime = docmime then
      Result:= 'doc';
   if AMime = xlsxmime then
      Result:= 'xlsx';
   if AMime = xlsmime then
      Result:= 'xls';
   if AMime = pdfmime then
      Result:= 'pdf';
   if copy(AMime, 1, length(constVideo)) = constVideo then
      Result:= copy(AMime, length(constVideo)+1, 99999);
   if copy(AMime, 1, length(constImage)) = constImage then
      Result:= copy(AMime, length(constImage)+1, 99999);
end;

function TRESTAPIKozosDlg.GetAttachmentMime(const AFileNev: string): string;
var
  attachtipus: string;
begin
   attachtipus:= lowercase(ExtractFileExt(AFileNev));
   if copy(attachtipus,1,1) = '.' then
       attachtipus:= copy(attachtipus,2,999);
   if (attachtipus = 'jpg') or (attachtipus = 'png') then begin
      Result:= 'image/'+attachtipus;
      end;
   if (attachtipus = 'mpg') or (attachtipus = 'mp4') then begin
      Result:= 'video/'+attachtipus;
      end;
   if (attachtipus = 'pdf') then begin
      Result:= 'application/pdf';
      end;
   if (attachtipus = 'xls') then begin
      Result:= 'application/vnd.ms-excel';
      end;
   if (attachtipus = 'xlsx') then begin
      Result:= 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
      end;
   if (attachtipus = 'doc') then begin
      Result:= 'application/msword';
      end;
   if (attachtipus = 'docx') then begin
      Result:= 'application/vnd.openxmlformats-officedocument.wordprocessingml.document';
      end;
end;

function TRESTAPIKozosDlg.GetCsoportTagok(const AGroupID: string): TStringResult;
var
   ResponseJSON, S, ElemID: string;
   jsObject: TJSONObject;
   jsKapcsArray, jsListaArray: TJSONArray;
   jsKapcs, jsListaElem, jsRootValue: TJSONValue;
   JSONResult: TJSONResult;
begin
   Result.ResultString:='';
   JSONResult:= RESTAPIKozosDlg.ExecRequest(Format('%s/%s', [cUserGroupResource, AGroupID]), 'get', RESTAPIKozosDlg.EmptyParamsList);
   if JSONResult.ErrorString <> '' then begin
        Result.ResultString:='';
        Result.Success:=False;
        Exit;  // function
        end; // if
   jsRootValue := JSONResult.JSONContent;
   jsKapcsArray:= (jsRootValue as TJSONObject).Get(cGroupUsersResource).JsonValue as TJSONArray;  // a kapcsolt elemeket tartalmaz� t�mb
   S:= '';
   for jsKapcs in jsKapcsArray do begin
      ElemID:= ((jsKapcs as TJSONObject).Get('xuid').JsonValue as TJSONString).Value;
      S:= Felsorolashoz(S, ElemID, ',', False);
      end; // for
   Result.ResultString:= S;
   Result.Success:=True;
end;


procedure TRESTAPIKozosDlg.RefreshAllUserStatus;
var
   ResponseJSON, S, ElemID, XUIDValue, StatusValue: string;
   Overkill: boolean;
   // jsObject: TJSONObject;
   // jsKapcsArray, jsListaArray: TJSONArray;
   // jsKapcs, jsListaElem, jsRootValue: TJSONValue;
   jsRecordArray: TJSONArray;
   jsRecord: TJSONValue;
   JSONResult: TJSONResult;
begin
   // -------- overkill ------- //
   JSONResult:= RESTAPIKozosDlg.ExecRequest(Format(cGetMeResource, []), 'get', RESTAPIKozosDlg.EmptyParamsList);
   if JSONResult.ErrorString <> '' then begin
        NoticeKi('Sikertelen saj�t st�tusz lek�rdez�s! '+JSONResult.ErrorString);
        Exit;  // function
        end; // if
   jsRecord := JSONResult.JSONContent as TJSONValue;
   if (jsRecord as TJSONObject).Get('overkill').JsonValue is TJSONTrue then
      Overkill:= True
   else Overkill:= False;
   UzenetTopDlg.ActOverkill:= Overkill;
   // -------- resting ------- //
   // nem t�roljuk, hanem on-the-fly k�rdezz�k le
   {UserStatusList.Clear;
   JSONResult:= RESTAPIKozosDlg.ExecRequest(Format('%s', [cUserResource]), 'get', RESTAPIKozosDlg.EmptyParamsList);
   if JSONResult.ErrorString <> '' then begin
        NoticeKi('Sikertelen felhaszn�l�i st�tusz lek�rdez�s! '+JSONResult.ErrorString);
        Exit;  // function
        end; // if
   jsRecordArray := JSONResult.JSONContent as TJSONArray;
   for jsRecord in jsRecordArray do begin
      XUIDValue:= ((jsRecord as TJSONObject).Get('xuid').JsonValue as TJSONString).Value;
      StatusValue:= ((jsRecord as TJSONObject).Get('status').JsonValue as TJSONString).Value;
      UserStatusList.Add(XUIDValue, StatusValue);
      end; // for
      }
end;

function TRESTAPIKozosDlg.GetUserPiheno(const AXUID: string): boolean;
var
  JSONResult: TJSONResult;
  jsRecord: TJSONValue;
  StatusValue: string;
begin
  JSONResult:= RESTAPIKozosDlg.ExecRequest(Format(cUserDataResource, [AXUID]), 'get', RESTAPIKozosDlg.EmptyParamsList);
  if JSONResult.ErrorString <> '' then begin
        NoticeKi('Sikertelen felhaszn�l�i st�tusz lek�rdez�s! '+JSONResult.ErrorString);
        Exit;  // function
        end; // if
   jsRecord := JSONResult.JSONContent as TJSONValue;
   StatusValue:= ((jsRecord as TJSONObject).Get('status').JsonValue as TJSONString).Value;
   Result:= (StatusValue = 'P');
end;


function TRESTAPIKozosDlg.GetLegyenSurgos(const AXUID: string): boolean;
var
  S, DONEV: string;
  Pihen: boolean;
begin
  {if UserStatusList.ContainsKey(AXUID) then begin
    Pihen:= (UserStatusList[AXUID] = 'P');
    end
  else begin
    NoticeKi('Nincs pihen�id� inform�ci�!');
    Pihen:= False;
    end;
    }
  Pihen:= GetUserPiheno(AXUID);

  if (not Pihen) or (not UzenetTopDlg.ActOverkill) then  // ha nem pihen, vagy a k�ld�nek nincs joga overkill-t k�ldeni
      Result:= False
  else begin
      DONEV:= Query_Select('DOLGOZO', 'DO_KOD', AXUID, 'DO_NAME');
      S:= DONEV+' pihen�idej�t t�lti. Az �zenetet n�m�tva kapja meg. Szeretn� hogy sz�m�ra jelz�hanggal k�ldj�k?';
      if NoticeKi(S, NOT_QUESTION) = 0 then begin
        Query_Log_Str(S+ ' V�lasz: Igen', 0, false, false);
			  Result:= True;
     	  end
      else begin
        Query_Log_Str(S+ ' V�lasz: Nem', 0, false, false);
        Result:= False;
        end; // else
      end; // else
end;

function TRESTAPIKozosDlg.DeleteMessage(const AMessageID: string): string;
var
   JSONResult: TJSONResult;
   S: string;
begin
   JSONResult:= RESTAPIKozosDlg.ExecRequest(Format('%s%s/', [cMessageResource, AMessageID]), 'delete', EmptyParamsList);
   Result:= JSONResult.ErrorString;
end;

function TRESTAPIKozosDlg.DeleteThread(const AMessageID: string): string;
var
   JSONResult: TJSONResult;
   S, Req: string;
begin
   Req:= Format('%s%s', [cThreadDeleteResource, AMessageID]);
   JSONResult:= RESTAPIKozosDlg.ExecRequest(Req, 'post', EmptyParamsList);
   Result:= JSONResult.ErrorString;
end;

procedure TRESTAPIKozosDlg.SendAvatar(const AAvatarResource, Axuid, Ajpegbase64: string; ACsendesMod: boolean = False);
var
  JSONResult: TJSONResult;
  ResourceS, S: string;
begin
  RESTAPIKozosDlg.RESTAPIParamsList.Clear;
  ResourceS:= Format(AAvatarResource, [Axuid]);
  RESTAPIKozosDlg.RESTAPIParamsList.Add('jpeg_image', '"'+Ajpegbase64+'"');
  JSONResult:= RESTAPIKozosDlg.ExecRequest(ResourceS, 'post', RESTAPIKozosDlg.RESTAPIParamsList);
  if JSONResult.ErrorString = '' then begin
    if not ACsendesMod then begin
      NoticeKi('Avatar felt�ltve.');
      end;
    end
  else begin
    S:='Hiba a felt�lt�s sor�n! K�d: '+ JSONResult.ErrorString;
    HibaKiiro(S);
    if not ACsendesMod then begin
      NoticeKi(S);
      end;
    end;  // if
end;


end.
