unit SzamlaFm;

interface

uses
	SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
	StdCtrls, Forms, ExtCtrls, Dialogs, J_FOFORMUJ, DB, ADODB, Buttons;

type
	TSzamlaFormDlg = class(TJ_FOFORMUJDLG)
	 ButtonNyomtat: TBitBtn;
	 ButtonVegleges: TBitBtn;
    ButtonStorno: TBitBtn;
    SorNyomtat: TBitBtn;
    Helyesbit: TBitBtn;
    Query3: TADOQuery;
	 ButtonFokonyv: TBitBtn;
    Query4: TADOQuery;
    ButtonKlon: TBitBtn;
    QueryKoz: TADOQuery;
    QueryAtcsat: TADOQuery;
    BtnJarat: TBitBtn;
    BitNavteszt: TBitBtn;
    BitNavEles: TBitBtn;
	  procedure FormCreate(Sender: TObject);override;
	 procedure Adatlap(cim, kod : string);override;
	 procedure ButtonNyomtatClick(Sender: TObject);
	 procedure ButtonVeglegesClick(Sender: TObject);
	 procedure ButtonStornoClick(Sender: TObject);
	 procedure DataSource1DataChange(Sender: TObject; Field: TField);override;
	 function 	KeresChange(mezo, szoveg : string) : string; override;
	 procedure SorNyomtatClick(Sender: TObject);
	 procedure FormKeyDown(Sender: TObject; var Key: Word;
	   Shift: TShiftState);override;
	 procedure HelyesbitClick(Sender: TObject);
	 procedure ButtonFokonyvClick(Sender: TObject);
	 procedure ViszonylatStorno(regikod, szamla : string; ujkod2 : integer);
    procedure ButtonKlonClick(Sender: TObject);
    procedure Atcsatolas(esakod,usakod: string);
    procedure BtnJaratClick(Sender: TObject);
    procedure Jarat;
    function NAVAdatkuldes(tipus : integer) : boolean;
    procedure BitNavtesztClick(Sender: TObject);
    procedure BitNavElesClick(Sender: TObject);
	 private
		elotag	: string;
	end;

var
	SzamlaFormDlg : TSzamlaFormDlg;

implementation

uses
	Egyeb, Szamlabe, J_SQL, Kozos, J_Valaszto, SzamlaNezo, Kozos_Local,
  XML_Export,StrUtils, Jaratbe, J_NavOnline, J_Datamodule;

{$R *.DFM}

procedure TSzamlaFormDlg.FormCreate(Sender: TObject);
begin
	EgyebDLg.SetADOQueryDatabase(QueryKOZ);
	EgyebDlg.SetADOQueryDatabase(Query4);
	EgyebDlg.SeTADOQueryDatabase(QueryAtcsat);
	Inherited FormCreate(Sender);
	AddSzuromezoRovid('SA_VEVONEV', 'SZFEJ');
	AddSzuromezoRovid('SA_RSZ', 'SZFEJ');
	AddSzuromezoRovid('SA_ATAD', 'SZFEJ');
	AddSzuromezoRovid('SA_ORSZA', 'SZFEJ');
	AddSzuromezoRovid('SA_ALLST', 'SZFEJ');
	ButtonNez.Visible	:= true;
	FelTolto('�j sz�mla felvitele ', 'Sz�mla adatok m�dos�t�sa ',
			'Sz�mla adatok list�ja', 'SA_UJKOD', 'Select * from SZFEJ ','SZFEJ', QUERY_ADAT_TAG );
	ButtonNyomtat.Parent	:= PanelBottom;
	ButtonVegleges.Parent  	:= PanelBottom;
	ButtonStorno.Parent  	:= PanelBottom;
	SorNyomtat.Parent  		:= PanelBottom;
	Helyesbit.Parent  	    := PanelBottom;
	ButtonFokonyv.Parent    := PanelBottom;
	ButtonKlon.Parent  		:= PanelBottom;
   BtnJarat.Parent  		:= PanelBottom;
	BitNavTeszt.Parent 		:= PanelBottom;
	BitNavEles.Parent 		:= PanelBottom;
	width					:= DLG_WIDTH;
	height					:= DLG_HEIGHT;
	ButtonTor.Hide;
	//ButtonUj.Hide;
	elotag					:= EgyebDlg.Read_SZGrid('CEG', '310');
end;

procedure TSzamlaFormDlg.Adatlap(cim, kod : string);
begin
   Application.CreateForm(TSzamlabeDlg, AlForm);
	Inherited Adatlap(cim, kod );
end;

procedure TSzamlaFormDlg.ButtonNyomtatClick(Sender: TObject);
begin
	if not voltalt then begin
		voltalt 	:= true;
		Exit;
	end;
	Edit1.Text	:= '';
	// A sz�mla megjelen�t�se nyomtathat�an
	Application.CreateForm(TSzamlaNezoDlg, SzamlaNezoDlg);
	SzamlaNezoDlg.SzamlaNyomtatas(StrToIntDef(Query1.FieldByName('SA_UJKOD').AsString,0), 1);
	SzamlaNezoDlg.Destroy;
	ModLocate (Query1, FOMEZO, Query1.FieldByName(FOMEZO).AsString );
end;

procedure TSzamlaFormDlg.ButtonVeglegesClick(Sender: TObject);
var
	sakod, S: string;
  satipus,kidat,hiba  : string;
  skod, ManCode, sa_man: string;
   volthiba    : boolean;
   fl          : string;
   jsz         : TJSzamla;
begin
	if not voltalt then begin
 		voltalt 	:= true;
		Exit;
	end;

	{Sz�mla kelt�nek ellen�rz�se}
	if Query1.FieldByName('SA_KIDAT').AsString = '' then begin
		NoticeKi('A sz�mla kelt�t meg kell adni, csak ut�na lehet v�gleges�teni!');
		Exit;
	end;
  // V�g�sszeg ellen�rz�se
  if Query1.FieldByName('SA_OSSZEG').AsFloat=0 then
  begin
  		NoticeKi('A sz�mla v�g�sszege 0 (nulla). Nem lehet v�gleges�teni!');
      exit;
  end;
{
  Query_Run(Query4, 'select max(sa_kidat) maxdat from SZFEJ where sa_kod<>'''+'''');
  max_kidat:= Query4.FieldByName('maxdat').AsString;
  if Query1.FieldByName('SA_KIDAT').AsString<max_kidat then            // az �j sz�mla d�tuma kor�bbi, mint egy el�z��
  begin
  		NoticeKi('Az �j sz�mla d�tuma kor�bbi mint egy el�z��! '+max_kidat);
      exit;
  end;
        }
  // KG 20111214   XML ellen�rz�s futtat�sa
	Query_Run( QueryKoz, 'SELECT VE_XMLSZ FROM VEVO WHERE V_KOD= '+Query1.FieldByName('SA_VEVOKOD').AsString, true );
	if QueryKoz.FieldByName('VE_XMLSZ').Value>0 then begin
 	 Application.CreateForm(TXML_ExportDlg, XML_ExportDlg);
   XML_ExportDlg.M3.text:= Query1.FieldByName('SA_UJKOD').AsString ;
   //XML_ExportDlg.CheckBox1.Checked:=True;
   if not XML_ExportDlg.XML_Generalas2  then
   begin
    if MessageBox(0,'XML gener�l�s ellen�rz�si HIBA!'+#13+ 'Folytatja?', 'XML ellen�rz�s', MB_ICONWARNING or MB_YESNO or MB_DEFBUTTON2)=IDNO then
  	//if NoticeKi('XML gener�l�s ellen�rz�si HIBA!'+#13+ 'Folytatja?', NOT_QUESTION) = 0 then
    begin
  		//NoticeKi('XML gener�l�s ellen�rz�si HIBA!');
    	XML_ExportDlg.Destroy;
      exit;
    end;
   end
   else
  	NoticeKi('XML gener�l�s ellen�rz�se: OK!');
   XML_ExportDlg.Destroy;
  end;

  //hiba:= EgyebDlg.Szamlaszam_ell();
  hiba:= EgyebDlg.Szamlaszam_ell(copy(Query1.FieldByName('SA_KIDAT').AsString,1,4));
  if hiba<>'' then
  begin
  		if NoticeKi('Hi�nyz� sz�mlasz�m!'+#13+#10+hiba+#13+#10+'Folytatja?',NOT_QUESTION)<>0 then
        exit;
  end;
  ////////////////////////////////////////////
	Edit1.Text	:= '';
	{Sz�mla v�gleges�t�se : Be�ll�tja a FLAG -et 0 -ra}
	if NoticeKi('Val�ban v�gleges�teni szeretn� a sz�ml�t?', NOT_QUESTION) = 0 then begin
		satipus := '101';
		if Query1.FieldByName('SA_ESDAT').AsString < EgyebDlg.MaiDatum then begin
			satipus := '102';
		end;
		sakod	:= Query1.FieldByName('SA_KOD').AsString;
		if sakod = '' then begin
			// �j sz�mlasz�mot kell gener�lni
			//sakod	:= GetUjszamla;
			//sakod	:= GetUjszamla2('');
			sakod	:= GetUjszamla2(copy(Query1.FieldByName('SA_KIDAT').AsString,3,2));
      if (sakod='0') or (sakod='') then begin
           S:= 'Sikertelen k�d gener�l�s, k�rem pr�b�lja �jra! (SzamlaVeglegesites)';
           HibaKiiro(S);
           NoticeKi(S);
           Exit;
           end;
      // if sakod='' then exit;
		end;
    /////////////////////////
    kidat:=Query1.FieldByName('SA_KIDAT').AsString ;
    if not EgyebDlg.Szamla_szam_datum_ell(sakod,kidat,False) then
    begin
     NoticeKi('L�tezik kisebb sz�mlasz�m nagyobb d�tummal!');
     exit;
    end;
    ////////////////////////
   Try
    if not EgyebDlg.Szamlaertek2jar_megb(Query1.Fieldbyname('SA_UJKOD').AsString,sakod) then Raise Exception.Create('') ;     // �sszeg, sz�mlasz�m vissza�r�sa a Viszonyba �s a megb�z�sba.

		//Query_Run(Query2,'UPDATE VISZONY SET VI_SAKOD='''+sakod+''' WHERE VI_UJKOD='+Query1.Fieldbyname('SA_UJKOD').AsString, true);
    ////////////////////////////////////////////////////////

       // Ha egyedi a megb�z�s, akkor a TEMAN sz�t kell megjelen�teni
       skod    := Query1.FieldByName('SA_FEJL1').AsString ;
       if Pos('(', skod) > 0 then begin
           skod := copy(skod, 1, Pos('(', skod) - 1);
       end;
       sa_man:='0';  // alap�rtelmez�sben nem egyedi d�jas
       Query_Run(Query2, 'SELECT MB_EDIJ FROM MEGBIZAS, VISZONY WHERE MB_VIKOD = VI_VIKOD AND VI_UJKOD = '+ Query1.FieldByName('SA_UJKOD').AsString);
       if StrToIntdef(Query2.FieldByName('MB_EDIJ').AsString, 0) > 0 then begin
           // Egyedi d�jr�l van sz�
           // skod    := skod + ' (TEMAN)';
           // --- 2016-01-07
           if LehetEgyediDij(Query1.FieldByName('SA_VEVOKOD').AsString, ManCode) then begin
              if ManCode <> '' then
                skod:= skod + ' ('+ManCode+')';
              sa_man:='1';
              end
           else begin  // nem lehet egyedi d�j: meg kell szak�tani a v�gleges�t�st
               NoticeKi('Ehhez a partnerhez nem k�sz�thet�nk egyedi d�jas sz�ml�t!');
               exit;
              end;
          end;


       // Ellen�rizz�k, hogy a sz�ml�t a NAV fel� k�ldeni kell-e?
       volthiba    := false;
       if ( ( Query1.Fieldbyname('SA_ORSZ2').AsString = 'HU' ) and (Query1.Fieldbyname('SA_KIDAT').AsString >= '2018.07.01.' ) ) then begin
           // �sszeg ellen�rz�se
           jsz     := TJSzamla.Create(Self);
           jsz.FillSzamla( Query1.Fieldbyname('SA_UJKOD').AsString );
           if Abs(jsz.ossz_afa) >= 100000 then begin
               // Ellen�rizz�k hogy a kiad�s kisebb-e mint a mai d�tum
               if Query1.Fieldbyname('SA_KIDAT').AsString < EgyebDlg.MaiDatum then begin
                   if NoticeKi('Figyelem! A sz�mla kiad�si d�tuma kisebb mint a mai d�tum. Folytatja a NAV bek�ld�st?', NOT_QUESTION) <> 0 then begin
                       Exit;
                   end;
               end;
               if not NAVAdatkuldes(1) then begin
                     volthiba    := true;
               end else begin
                   Query_Run(Query2, 'SELECT SA_ESTAT FROM SZFEJ WHERE SA_KOD = '''+Query1.Fieldbyname('SA_KOD').AsString+''' ');
                   if Pos('DONE', Query2.Fieldbyname('SA_ESTAT').AsString) < 1 then begin
                       // Ha nem DONE -val z�rult, akkor sem lehet v�gleges�teni
                       volthiba    := true;
                   end;
               end;
           end;
       end;
       if volthiba then begin
           NoticeKi('Figyelem!  A v�gleges�t�s nem hajthat� v�gre! Pr�b�lja meg k�s�bb !');
           Exit;
       end;
       fl  := '0';
       if TRim(Query1.FieldByName('SA_STORN').AsString) <> '' then begin
           fl  := '3';
       end;



		if not Query_Run(Query2,'UPDATE SZFEJ SET SA_FLAG ='''+fl+''', SA_FEJL1 = '''+ skod +''' , SA_ALLST = '''+GetSzamlaAllapot(StrToIntdef(fl, 0))+''', SA_KOD = '''+sakod+''', SA_TIPUS = '''+satipus+''', SA_MAN = '+sa_man+
           ' WHERE SA_UJKOD='+Query1.Fieldbyname('SA_UJKOD').AsString, true) then Raise Exception.Create('');

		if not Query_Run(Query2,'UPDATE SZSOR SET SS_KOD='''+sakod+''' WHERE SS_UJKOD='+Query1.Fieldbyname('SA_UJKOD').AsString, true) then Raise Exception.Create('');

       Except
  		    NoticeKi('A sz�mla v�gleges�t�se nem siker�lt!');
       END;

       ModLocate (Query1, FOMEZO, Query1.FieldByName('SA_UJKOD').AsString );
  end;
   Query_Run(Query2, 'SELECT MB_MBKOD FROM VISZONY, MEGBIZAS WHERE MB_VIKOD = VI_VIKOD AND VI_UJKOD = '+Query1.Fieldbyname('SA_UJKOD').AsString);
   if Trim(Query2.FieldByName('MB_MBKOD').AsString) = '' then
       Query_Log_Str('Sz�mla v�gleges�t�s, �res MBKOD! UJKOD='+Query1.Fieldbyname('SA_UJKOD').AsString, 0, true);  // napl�zzuk
   MegbizasKieg(Query2.FieldByName('MB_MBKOD').AsString);

end;

procedure TSzamlaFormDlg.ButtonStornoClick(Sender: TObject);
var
	st ,hiba, S		 	: string;
	ujkodszam,fazis	: integer;
begin
	if not voltalt then begin
 		voltalt 	:= true;
 		Exit;
	end;
   if  Query1.FieldByname('SA_RESZE').AsInteger > 0 then exit;  // Ha r�ssz�mla, akkor nem lehet stornozni.
   fazis:=0;
   // csak most!!
   if  Query1.Fieldbyname('SA_JARAT').AsString<>EmptyStr then
       fazis:=EllenorzottJarat2(Query1.Fieldbyname('SA_JARAT').AsString,copy(Query1.Fieldbyname('SA_TEDAT').AsString,1,4)) ;

   if fazis>0 then  // ellen�rz�tt   v. stornozott
   begin
       // NoticeKi('Ellen�rz�tt j�rathoz tartozik! M�velet megszak�tva.!');
       exit;
   end;
	Edit1.Text	:= '';
	if NoticeKi('Val�ban storn�zni akarja az aktu�lis sz�ml�t?', NOT_QUESTION) = 0 then begin
		Screen.Cursor	:= crHourglass;
		{Sz�mla stornoz�sa}
		Query_Run(Query2,'UPDATE SZFEJ SET SA_FLAG =''2'', SA_ALLST = '''+GetSzamlaAllapot(2)+''' '+
			' WHERE SA_UJKOD='+Query1.Fieldbyname('SA_UJKOD').AsString, true);
		{A storn� sz�mla elk�sz�t�se}
		EgyebDlg.SetADOQueryDatabase(Query3);
		//st	:= GetUjszamla;
		//st	:= GetUjszamla2('');

       //hiba:= EgyebDlg.Szamlaszam_ell();
       hiba:= EgyebDlg.Szamlaszam_ell(copy(Query1.FieldByName('SA_KIDAT').AsString,1,4));
       if hiba<>'' then begin
  		    if NoticeKi('Hi�nyz� sz�mlasz�m!'+#13+#10+hiba+#13+#10+'Folytatja?',NOT_QUESTION)<>0 then
               exit;
       end;

		// Query_Run(Query2, 'SELECT MAX(SA_UJKOD) MAXKOD FROM SZFEJ ', true);
		// ujkodszam 	:= Query2.FieldByName('MAXKOD').AsInteger + 1;
       ujkodszam := GetNewIDLoop(modeGetNextCode, 'SZFEJ', 'SA_UJKOD', '');
       if (ujkodszam<=0) then begin
           S:= 'Sikertelen k�d gener�l�s, k�rem pr�b�lja �jra! (SzamlaStorno UJKOD)';
           HibaKiiro(S);
           NoticeKi(S);
           Exit;
       end;

		//st	:= GetUjszamla2(copy(Query1.FieldByName('SA_KIDAT').AsString,3,2));
		st	:= GetUjszamla2(copy(EgyebDlg.MaiDatum,3,2));
       if (st='0') or (st='') then begin
           S:= 'Sikertelen k�d gener�l�s, k�rem pr�b�lja �jra! (SzamlaStorno SAKOD)';
           HibaKiiro(S);
           NoticeKi(S);
           Exit;
       end;

		Query_Run(Query2, 'INSERT INTO SZFEJ (SA_UJKOD, SA_KOD, SA_FLAG, SA_ALLST ) '+
				'VALUES ('+IntToStr(ujkodszam)+','''+st+''', ''1'', '''+GetSzamlaAllapot(1)+''' ) ', true);
		{A fejl�c rekord felt�lt�se}
		Query_Update ( Query2, 'SZFEJ',
		[
		  'SA_STORN',		''''+Query1.FieldByName('SA_KOD').AsString+'''',
		  'SA_DATUM',		''''+EgyebDlg.MaiDatum+'''',
		  'SA_TEDAT',		''''+Query1.FieldByName('SA_TEDAT').AsString +'''',
		  'SA_KIDAT',		''''+EgyebDlg.MaiDatum+'''',
		  'SA_ESDAT',		''''+Query1.FieldByName('SA_ESDAT').AsString +'''',
		  'SA_VEVONEV',		''''+Query1.FieldByName('SA_VEVONEV').AsString +'''',
		  'SA_VEIRSZ',		''''+Query1.FieldByName('SA_VEIRSZ').AsString  +'''',
		  'SA_VEVAROS',		''''+Query1.FieldByName('SA_VEVAROS').AsString +'''',
		  'SA_VECIM',		''''+Query1.FieldByName('SA_VECIM').AsString	+'''',
		  'SA_VEADO',		''''+Query1.FieldByName('SA_VEADO').AsString 	+'''',
		  'SA_EUADO',		''''+Query1.FieldByName('SA_EUADO').AsString 	+'''',
		  'SA_VEBANK',		''''+Query1.FieldByName('SA_VEBANK').AsString 	+'''',
		  'SA_VEKEDV',		SqlSzamString(StringSzam(Query1.FieldByName('SA_VEKEDV').AsString),6,2),
		  'SA_PELDANY',		'0',
		  'SA_FIMOD',		''''+Query1.FieldByName('SA_FIMOD').AsString +'''',
		  'SA_OSSZEG',		'-1*'+ SqlSzamString(StringSzam(Query1.FieldByName('SA_OSSZEG').AsString),12,2),
		  'SA_AFA',			'-1*'+SqlSzamString(StringSzam(Query1.FieldByName('SA_AFA').AsString),12,2),
		  'SA_MEGJ',		''''+Query1.FieldByName('SA_MEGJ').AsString +'''',
		  'SA_JARAT',		''''+Query1.FieldByName('SA_JARAT').AsString +'''',
		  'SA_POZICIO',		''''+Query1.FieldByName('SA_POZICIO').AsString +'''',
		  'SA_RSZ',			''''+Query1.FieldByName('SA_RSZ').AsString 	+'''',
		  'SA_MELL1',		''''+Query1.FieldByName('SA_MELL1').AsString	+'''',
		  'SA_MELL2',		''''+Query1.FieldByName('SA_MELL2').AsString +'''',
		  'SA_MELL3',		''''+Query1.FieldByName('SA_MELL3').AsString +'''',
		  'SA_MELL4',		''''+Query1.FieldByName('SA_KOD').AsString +' sz�mla storn�ja!'+'''',
		  'SA_MELL5',		''''+Query1.FieldByName('SA_MELL5').AsString +'''',
		  'SA_MELL6',		''''+Query1.FieldByName('SA_MELL6').AsString +'''',
		  'SA_MELL7',		''''+Query1.FieldByName('SA_MELL7').AsString +'''',
		  'SA_FEJL1',		''''+Query1.FieldByName('SA_FEJL1').AsString	+'''',
		  'SA_FEJL2',		''''+Query1.FieldByName('SA_FEJL2').AsString	+'''',
		  'SA_FEJL3',		''''+Query1.FieldByName('SA_FEJL3').AsString	+'''',
		  'SA_FEJL4',		''''+Query1.FieldByName('SA_FEJL4').AsString	+'''',
		  'SA_FEJL5',		''''+Query1.FieldByName('SA_FEJL5').AsString	+'''',
		  'SA_FEJL6',		''''+Query1.FieldByName('SA_FEJL6').AsString	+'''',
		  'SA_FEJL7',		''''+Query1.FieldByName('SA_FEJL7').AsString	+'''',
		  'SA_FOKON',		''''+Query1.FieldByName('SA_FOKON').AsString	+'''',
		  'SA_VEVOKOD',		''''+Query1.FieldByName('SA_VEVOKOD').AsString	+'''',
		  'SA_EGYA1',		''''+Query1.FieldByName('SA_EGYA1').AsString	+'''',
		  'SA_EGYA2',		''''+Query1.FieldByName('SA_EGYA2').AsString	+'''',
		  'SA_EGYA3',		''''+Query1.FieldByName('SA_EGYA3').AsString	+'''',
		  'SA_MEGRE',		''''+Query1.FieldByName('SA_MEGRE').AsString	+'''',
		  'SA_ORSZA',		''''+Query1.FieldByName('SA_ORSZA').AsString	+'''',
		  'SA_ORSZ2',		''''+Query1.FieldByName('SA_ORSZ2').AsString	+'''',
		  'SA_RESZE',		'0',
		  'SA_HELYE',		IntToStr(StrToIntDef(Query1.FieldByName('SA_HELYE').AsString,0)),
		  'SA_SAEUR',		IntToStr(StrToIntDef(Query1.FieldByName('SA_SAEUR').AsString,0)),
		  'SA_ATAD',		'0',
         'SA_VALNEM',  ''''+Query1.FieldByName('SA_VALNEM').AsString	+'''',
         'SA_VALOSS',		'-1*'+SqlSzamString(StringSzam(Query1.FieldByName('SA_VALOSS').AsString),12,2),
		  'SA_VALAFA',		'-1*'+SqlSzamString(StringSzam(Query1.FieldByName('SA_VALAFA').AsString),12,2),
         'SA_VALARF',		SqlSzamString(StringSzam(Query1.FieldByName('SA_VALARF').AsString),12,2),
		  'SA_ARFDAT',		''''+Query1.FieldByName('SA_ARFDAT').AsString	+'''',
         'SA_UZPOT',		SqlSzamString(StringSzam(Query1.FieldByName('SA_UZPOT').AsString),12,2),
		  'SA_NYELV',		''''+Query1.FieldByName('SA_NYELV').AsString	+'''',
		  'SA_VEFEJ',		''''+Query1.FieldByName('SA_VEFEJ').AsString	+'''',
		  'SA_TIPUS',		SqlSzamString(StringSzam(Query1.FieldByName('SA_TIPUS').AsString),12,2)
		  ], ' WHERE SA_UJKOD = '+IntToStr(ujkodszam));

//       A storn� viszonylat l�trehoz�sa
		  ViszonylatStorno(Query1.Fieldbyname('SA_UJKOD').AsString, st,ujkodszam);

		{A t�telsorok l�trehoz�sa}
		Query_Run (Query2, 'SELECT * FROM SZSOR WHERE SS_UJKOD = '+Query1.FieldByName('SA_UJKOD').AsString+' ',true);
		while not Query2.EOF do begin
			Query_Insert( Query3, 'SZSOR',
			[
			 'SS_KOD', 			''''+st+'''',
			 'SS_SOR',			Query2.FieldByName('SS_SOR').AsString,
			 'SS_UJKOD',			IntToStr(ujkodszam),
			 'SS_TERKOD', 		''''+Query2.FieldByName('SS_TERKOD').AsString +'''',
			 'SS_TERNEV', 		''''+Query2.FieldByName('SS_TERNEV').AsString +'''',
			 'SS_CIKKSZ', 		''''+Query2.FieldByName('SS_CIKKSZ').AsString +'''',
			 'SS_ITJSZJ', 		''''+Query2.FieldByName('SS_ITJSZJ').AsString +'''',
			 'SS_MEGY', 		''''+Query2.FieldByName('SS_MEGY').AsString +'''',
			 'SS_AFA', 			SqlSzamString(StringSzam(Query2.FieldByName('SS_AFA').AsString),		12,2),
			 'SS_EGYAR', 		SqlSzamString(StringSzam(Query2.FieldByName('SS_EGYAR').AsString),	12,2),
			 'SS_KEDV', 		SqlSzamString(StringSzam(Query2.FieldByName('SS_KEDV').AsString),		12,2),
			 'SS_VALERT', 		SqlSzamString(StringSzam(Query2.FieldByName('SS_VALERT').AsString),	12,2),
			 'SS_VALARF', 		SqlSzamString(StringSzam(Query2.FieldByName('SS_VALARF').AsString),	12,2),
			 'SS_DARAB', 		'-1*'+SqlSzamString(StringSzam(Query2.FieldByName('SS_DARAB').AsString),	12,2),
			 'SS_ARFOL', 		SqlSzamString(StringSzam(Query2.FieldByName('SS_ARFOL').AsString),	12,4),
			 'SS_VALNEM', 		''''+ Query2.FieldByName('SS_VALNEM').AsString +'''',
			 'SS_AFAKOD', 		''''+ Query2.FieldByName('SS_AFAKOD').AsString +'''',
			 'SS_LEIRAS', 		''''+ Query2.FieldByName('SS_LEIRAS').AsString +'''',
			 'SS_KULNEV', 		''''+ Query2.FieldByName('SS_KULNEV').AsString +'''',
			 'SS_KULLEI', 		''''+ Query2.FieldByName('SS_KULLEI').AsString +'''',
			 'SS_KULME', 		''''+ Query2.FieldByName('SS_KULME').AsString  +'''',
			 'SS_NEMEU', 		IntToStr(StrToIntDef(Query2.FieldByName('SS_NEMEU').AsString,0)),
			 'SS_ARDAT', 		''''+ Query2.FieldByName('SS_ARDAT').AsString  +''''
			]);
			Query2.Next;
		end;
		Screen.Cursor	:= crDefault;
		{Az �j t�tel megjelen�t�se}
		ModLocate (Query1, FOMEZO, IntToStr(ujkodszam) );
       NoticeKi('Figyelem! A v�gleges�t�s nem t�rt�nt meg automatikusan, azt k�zzel kell v�grehajtani! ');
	end;
end;

procedure TSzamlaFormDlg.DataSource1DataChange(Sender: TObject; Field: TField);
begin
	// A nyom�gombok szab�lyoz�sa
	case StrToIntDef(Query1.FieldByName('SA_FLAG').AsString,0)  of
  	0:
			begin
			{A sz�mla nem m�dos�that�}
		    	ButtonMod.Enabled := false;
  		  	ButtonMod.Hide;
        	{A sz�mla stornozhat�}
   	   		ButtonStorno.Enabled := true;
		     	ButtonStorno.Show;
	      	ButtonKlon.Enabled := false;
		      ButtonKlon.Hide;
        	{A sz�mla nyomtathat�}
			    ButtonNyomtat.Enabled := true;
		     	ButtonNyomtat.Show;
   			  SorNyomtat.Enabled := true;
      		SorNyomtat.Show;
        	{A sz�mla nem v�gleges�that�}
			    ButtonVegleges.Enabled := false;
		     	ButtonVegleges.Hide;
  		end;
     1:
    		begin
        	{A sz�mla m�dos�that�}
		   	  ButtonMod.Enabled := true;
 			    ButtonMod.Show;
        	{A sz�mla nem stornozhat�}
 	   		  ButtonStorno.Enabled := false;
		     	ButtonStorno.Hide;
	      	ButtonKlon.Enabled := false;
		      ButtonKlon.Hide;
        	{A sz�mla nem nyomtathat�}
	   	  	ButtonNyomtat.Enabled := false;
		     	ButtonNyomtat.Hide;
    	  	SorNyomtat.Enabled := false;
			    SorNyomtat.Hide;
			    {A sz�mla v�gleges�thet�}
 	   		  ButtonVegleges.Enabled := true;
		     	ButtonVegleges.Show;
		  	end;
     2:
     	begin
        	{A sz�mla nem m�dos�that�}
				  ButtonMod.Enabled := false;
  		  	ButtonMod.Hide;
        	{A sz�mla nem stornozhat�}
 	   		  ButtonStorno.Enabled := false;
				  ButtonStorno.Hide;
	      	ButtonKlon.Enabled := true;
		      ButtonKlon.Show;
        	{A sz�mla nyomtathat�}
 	   		  ButtonNyomtat.Enabled := true;
		     	ButtonNyomtat.Show;
  			  SorNyomtat.Enabled := true;
      		SorNyomtat.Show;
        	{A sz�mla nem v�gleges�that�}
 	   		  ButtonVegleges.Enabled := false;
		     	ButtonVegleges.Hide;
        end;
     3:
     	begin
        	{A sz�mla nem m�dos�that�}
				  ButtonMod.Enabled := false;
  		  	ButtonMod.Hide;
        	{A sz�mla nem stornozhat�}
 	   		  ButtonStorno.Enabled := false;
				  ButtonStorno.Hide;
	      	ButtonKlon.Enabled := false;
		      ButtonKlon.Hide;
        	{A sz�mla nyomtathat�}
 	   		  ButtonNyomtat.Enabled := true;
		     	ButtonNyomtat.Show;
  			  SorNyomtat.Enabled := true;
      		SorNyomtat.Show;
        	{A sz�mla nem v�gleges�that�}
 	   		  ButtonVegleges.Enabled := false;
		     	ButtonVegleges.Hide;
        end;

  end;

  if ButtonNyomtat.Enabled then begin
		if Query1.FieldByname('SA_RESZE').AsInteger > 0 then begin
			ButtonNyomtat.Enabled := false;
			SorNyomtat.Enabled := false;
		end;
  end;

  if ( SorNyomtat.Enabled and ( Query1.FieldByName('SA_PELDANY').AsInteger >= EgyebDlg.Peldanyszam) ) then begin
		SorNyomtat.Enabled := false;
  end;

  inherited DataSource1DataChange(Sender, Field);
end;

function TSzamlaFormDlg.KeresChange(mezo, szoveg : string) : string;
var
  uelotag:string;
begin
	Result	:= szoveg;
	if UpperCase(mezo) = 'SA_KOD' then begin
    uelotag:=elotag;
    if SzamlaFormDlg.ComboBox2.ItemIndex>0 then
    begin
      uelotag:=copy(SzamlaFormDlg.ComboBox2.Text,3,2)+copy(elotag,3,1);
  		Result := uelotag + Format('%5.5d',[StrToIntDef(Edit1.Text,0)]);
    end
    else
    begin
      uelotag:='';
		  Result := Edit1.Text;
    end;
		//Result := uelotag + Format('%5.5d',[StrToIntDef(Edit1.Text,0)]);
		//Result := elotag + Format('%5.5d',[StrToIntDef(Edit1.Text,0)]);
		//Result := elotag + Format('%5.5d',[StrToIntDef(szoveg,0)]);
	end;
	if UpperCase(mezo) = 'SA_JARAT' then begin
		Result := Format('%5.5d',[StrToIntDef(szoveg,0)]);
	end;
end;

procedure TSzamlaFormDlg.SorNyomtatClick(Sender: TObject);
begin
	if not voltalt then begin
		voltalt 	:= true;
		Exit;
	end;
	Edit1.Text	:= '';
	// Az �sszes sz�mlap�ld�ny kinyomtat�sa
	Application.CreateForm(TSzamlaNezoDlg, SzamlaNezoDlg);
	SzamlaNezoDlg.SzamlaSorozat(StrToIntDef(Query1.FieldByName('SA_UJKOD').AsString,0));
	SzamlaNezoDlg.Destroy;
	ModLocate (Query1, FOMEZO, Query1.FieldByName(FOMEZO).AsString );
end;

procedure TSzamlaFormDlg.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  ujkod: integer;
begin
	Inherited FormKeyDown(Sender, Key, Shift);
  	if ( EgyebDlg.CtrlGomb and EgyebDlg.user_super ) then begin
		// J�rat v�gleges�tetts�g�nek vizsg�lata
		voltalt	:= true;
		if Query1.FieldByName('SA_JARAT').AsString <> '' then begin
			Query_Run (Query2, 'SELECT JA_FAZIS FROM JARAT WHERE JA_ALKOD = '+ IntToStr(StrToIntdef(Query1.FieldByName('SA_JARAT').AsString,-1))+
               ' AND JA_JKEZD LIKE '''+copy( Query1.FieldByName('SA_DATUM').AsString, 1, 4 )+'%'' ', true);
			if Query2.FieldByname('JA_FAZIS').AsString = '1' then begin
				// Ellen�rz�tt j�rat, m�r nem lehet v�ltoztatni
				NoticeKi('Ellen�rz�tt j�rat!');
				Exit;
			end;
		end;
		// Az �sszes�tett sz�mla ellen�rz�tts�g�nek vizsg�lata
		if Query1.Fieldbyname('SA_RESZE').AsString = '1' then begin
			Query_Run (Query2, 'SELECT SA_FLAG FROM SZFEJ2 WHERE SA_KOD = '''+ Query1.FieldByName('SA_KOD').AsString +''' ', true);
			if Query2.FieldByname('SA_FLAG').AsString = '0' then begin
				// V�gleges�tett sz�mla, m�r nem lehet v�ltoztatni
				NoticeKi('V�gleges�tett �sszes�tett sz�mla!');
				Exit;
			end;
		end;
		// Az �tadotts�g ellen�rz�se
		if StrToIntDef(Query1.FieldByName('SA_ATAD').AsString, 0) > 0 then begin
			NoticeKi('A sz�mla m�r �tad�sra ker�lt!');
			Exit;
		end;
		// M�dos�t�s vissza�ll�t�sa
		if NoticeKi('Val�ban m�dos�tan� a sz�ml�t?', NOT_QUESTION) = 0 then begin
			Query_Run (Query2, 'UPDATE SZFEJ SET SA_FLAG = ''1'', SA_ALLST = '''+GetSzamlaAllapot(1)+''', SA_EDISENT = null '+
				' WHERE SA_UJKOD = '+ Query1.Fieldbyname('SA_UJKOD').AsString, true);
			Query_Run (Query2, 'UPDATE SZFEJ SET SA_PELDANY = 0  WHERE SA_UJKOD = '+ Query1.Fieldbyname('SA_UJKOD').AsString, true);
			ModLocate (Query1, FOMEZO, Query1.FieldByName(FOMEZO).AsString );
		end;
	end;
	if EgyebDlg.CtrlGomb2 then begin
		// P�ld�nysz�m vissza�ll�t�sa
		voltalt	:= true;
		if NoticeKi('Val�ban m�dos�tan� a p�ld�nysz�mot?', NOT_QUESTION) = 0 then begin
			Query_Run (Query2, 'UPDATE SZFEJ SET SA_PELDANY = '+
			IntToStr( StrToIntDef(InputBox('P�ld�nysz�m', 'Az �j p�ld�nysz�m :', '0'),0))+
			' WHERE SA_UJKOD = '+ Query1.Fieldbyname('SA_UJKOD').AsString, true);
			ModLocate (Query1, FOMEZO, Query1.FieldByName(FOMEZO).AsString );
		end;
	end;
(*
	if ( ( ssCtrl in Shift ) and (key = VK_F5) ) then begin
		if NoticeKi('Val�ban t�rli az �tad�s jelz�t?', NOT_QUESTION) = 0 then begin
			Query_Run (Query2, 'UPDATE SZFEJ SET SA_ATAD = 0 WHERE SA_KOD  = '''+ Query1.Fieldbyname('SA_KOD').AsString+''' ', true);
			Query_Run (Query2, 'UPDATE SZFEJ2 SET SA_ATAD = 0 WHERE SA_KOD = '''+ Query1.Fieldbyname('SA_KOD').AsString+''' ', true);
			ModLocate (Query1, FOMEZO, Query1.FieldByName(FOMEZO).AsString );
		end;
	end;
*)

	if ( ( ssCtrl in Shift ) and (key = VK_F8)and EgyebDlg.user_super and (Query1.Fieldbyname('SA_KOD').AsString='') ) then
  begin
    if NoticeKi('Val�ban t�rli a sz�ml�t?', NOT_QUESTION) = 0 then
    begin
      ujkod:=Query1.Fieldbyname('SA_UJKOD').AsInteger;
			Query_Run (Query2, 'DELETE SZFEJ  WHERE SA_UJKOD  = '''+ IntToStr(ujkod)+''' ', true);
			Query_Run (Query2, 'DELETE SZSOR  WHERE SS_UJKOD  = '''+ IntToStr(ujkod)+''' ', true);
			Query_Run (Query2, 'DELETE SZSOR2 WHERE S2_UJKOD  = '''+ IntToStr(ujkod)+''' ', true);
			Query_Run (Query2, 'UPDATE VISZONY SET VI_UJKOD=0 WHERE VI_UJKOD  = '''+ IntToStr(ujkod)+''' ', true);
			ModLocate (Query1, FOMEZO, Query1.FieldByName(FOMEZO).AsString );
    end;
  end;
end;

procedure TSzamlaFormDlg.HelyesbitClick(Sender: TObject);
var
  fazis:integer;
begin
	// Helyesb�t� sz�mla k�sz�t�se
  fazis:=EllenorzottJarat2(Query1.Fieldbyname('SA_JARAT').AsString,copy(Query1.Fieldbyname('SA_TEDAT').AsString,1,4)) ;
  if fazis>0 then  // ellen�rz�tt   v. stornozott
  begin
   //	NoticeKi('Ellen�rz�tt j�rathoz tartozik! M�velet megszak�tva.!');
    exit;
  end;
	Application.CreateForm(TSzamlabeDlg, SzamlaBeDlg);
	SzamlaBeDlg.CheckBox2.Show;
   SzamlaBeDlg.CheckBox2.Checked := true;
   SzamlaBeDlg.CheckBox2.Enabled := false;
   SzamlaBeDlg.Tolto('�j helyesb�t� sz�mla felvitele', '' );   
	SzamlaBeDlg.ShowModal;
	if SzamlabeDlg.ret_kod > '' then begin
		ModLocate (Query1, FOMEZO, SzamlabeDlg.ret_kod );
	end;
	SzamlaBeDlg.Destroy;
end;

procedure TSzamlaFormDlg.BitNavElesClick(Sender: TObject);
begin
   if Query1.FieldByname('SA_RESZE').AsInteger > 0 then begin
       NoticeKi('R�szsz�mla nem k�ldhet� be!');
       Exit;
   end;
   // A sz�mla elk�ld�se a NAV �les ter�letre
   if StrToINtDef(Query1.Fieldbyname('SA_FLAG').AsString, 0) = 1  then begin
       NoticeKi('Figyelem! A rekordot el�sz�r v�gleges�teni kell !');
       Exit;
   end;
   if Pos('DONE', Query1.Fieldbyname('SA_ESTAT').AsString) < 1 then begin
       // Ha nem DONE -val z�rult, lehet v�gleges�teni
       if NoticeKi('Figyelem! Nem NAV rekord! M�gis elk�ldi a sz�ml�t a NAV-nak?', NOT_QUESTION) <> 0 then begin
           Exit;
       end;
   end;

  // A sz�mla elk�ld�se a NAV �lesre
  NAVAdatkuldes(1);

   // �sszes sz�mla elk�ld�se a NAV �lesre
   {While not Query1.Eof do begin
      NAVAdatkuldes(1);
      Query1.Next;
      End;
    }
end;

procedure TSzamlaFormDlg.BitNavtesztClick(Sender: TObject);
begin
   if Query1.FieldByname('SA_RESZE').AsInteger > 0 then begin
       NoticeKi('R�szsz�mla nem k�ldhet� be!');
       Exit;
   end;
   // A sz�mla elk�ld�se a NAV tesztre
   NAVAdatkuldes(0);
   {
   // �sszes sz�mla elk�ld�se a NAV tesztre
   While not Query1.Eof do begin
      NAVAdatkuldes(0);
      Query1.Next;
      End;
    }

end;

procedure TSzamlaFormDlg.BtnJaratClick(Sender: TObject);
begin
   Jarat;
end;

procedure TSzamlaFormDlg.ButtonFokonyvClick(Sender: TObject);
var
	foki	: string;
begin
	foki := FokonyvValaszto(Query1.FieldByName('SA_FOKON').AsString);
	if  foki <> '' then begin
		Query_Run (Query2, 'UPDATE SZFEJ SET SA_FOKON = '''+foki+''' WHERE SA_UJKOD = '+ Query1.Fieldbyname('SA_UJKOD').AsString, true);
		ModLocate (Query1, FOMEZO, Query1.FieldByName(FOMEZO).AsString );
	end;
end;

procedure TSzamlaFormDlg.ViszonylatStorno(regikod, szamla : string; ujkod2 : integer);
var
	ujazon	: integer;
  S: string;
begin
	Query_Run(Query4, 'SELECT * FROM VISZONY WHERE VI_UJKOD = '+regikod);
	if Query4.RecordCount < 1 then begin
		NoticeKi('Hi�nyz� megb�z�s rekord! Nem lehet storn� megb�z�st gener�lni!');
		Exit;
	end;

	// ujazon := GetNextStrCode('VISZONY', 'VI_VIKOD', 0, 5);
  ujazon := GetNewIDLoop(modeGetNextStrCode, 'VISZONY', 'VI_VIKOD', '', 0, 6);
  if (ujazon<=0) then begin
     S:= 'Sikertelen k�d gener�l�s, k�rem pr�b�lja �jra! (SzamlaViszonylatStorno)';
     HibaKiiro(S);
     NoticeKi(S);
     Exit;
     end;
	{Az �j rekord felvitele}
	Query_Insert (Query4, 'VISZONY', [
		'VI_VIKOD', 	''''+IntToStr(ujazon)+'''',
		'VI_UJKOD',		IntToStr(ujkod2),
		'VI_HONNAN',	''''+Query4.FieldByName('VI_HONNAN').AsString+'''',
		'VI_HOVA',		''''+Query4.FieldByName('VI_HOVA').AsString+'''',
		'VI_JAKOD',		''''+Query4.FieldByName('VI_JAKOD').AsString+'''',
		'VI_VEKOD',		''''+Query4.FieldByName('VI_VEKOD').AsString+'''',
		'VI_VENEV',		''''+Query4.FieldByName('VI_VENEV').AsString+'''',
		'VI_VALNEM',	''''+Query4.FieldByName('VI_VALNEM').AsString+'''',
		'VI_KULF', 		Query4.FieldByName('VI_KULF').AsString,
		'VI_ARFOLY',	SqlSzamString(StringSzam(Query4.FieldByName('VI_ARFOLY').AsString),12,4),
		'VI_FDEXP',		SqlSzamString(-1*StringSzam(Query4.FieldByName('VI_FDEXP').AsString),12,4),
		'VI_FDIMP',		SqlSzamString(-1*StringSzam(Query4.FieldByName('VI_FDIMP').AsString),12,4),
		'VI_FDKOV',		SqlSzamString(-1*StringSzam(Query4.FieldByName('VI_FDKOV').AsString),12,4),
		'VI_TIPUS',		Query4.FieldByName('VI_TIPUS').AsString,
		'VI_BEDAT',		''''+Query4.FieldByName('VI_BEDAT').AsString+'''',
		'VI_KIDAT',		''''+Query4.FieldByName('VI_KIDAT').AsString+'''',
		'VI_POZIC',		''''+Query4.FieldByName('VI_POZIC').AsString+'''',
		'VI_SAKOD',		''''+szamla+'''',
		'VI_MEGJ',		''''+'Storn� viszonylat!'+''''
	] );
end;

procedure TSzamlaFormDlg.ButtonKlonClick(Sender: TObject);
var
	ujkodszam,fazis	: integer;
	regikod,ujkod				: string;
begin
	if not voltalt then begin
		voltalt 	:= true;
		Exit;
	end;
//   if  Query1.FieldByname('SA_RESZE').AsInteger > 0 then exit;  // Ha r�ssz�mla, akkor nem lehet kl�nozni.

   fazis:=EllenorzottJarat2(Query1.Fieldbyname('SA_JARAT').AsString,copy(Query1.Fieldbyname('SA_TEDAT').AsString,1,4)) ;
   if fazis>0 then begin // ellen�rz�tt   v. stornozott
       //	NoticeKi('Ellen�rz�tt j�rathoz tartozik! M�velet megszak�tva.!');
       exit;
   end;

	Edit1.Text	:= '';
	if NoticeKi('Val�ban kl�nozni akarja az aktu�lis sz�ml�t?', NOT_QUESTION) = 0 then begin
		if Query1.FieldByname('SA_RESZE').AsInteger > 0 then begin
			if NoticeKi('Figyelem! A kijel�lt elem egy �sszes�tett sz�mla r�sze! Val�ban csak ezt a r�sz sz�ml�t k�v�nja kl�nozni?', NOT_QUESTION) <> 0 then begin
				Exit;
			end;
		end;
		Screen.Cursor	:= crHourglass;
       //regikod:=Query1.FieldByName('SA_KOD').AsString ;
       regikod:=Query1.FieldByName('SA_UJKOD').AsString ;
		{Sz�mla kl�noz�sa}
		ujkodszam	:= KlonSzamla( Query1.FieldByName('SA_UJKOD').AsString, '' );
		if ujkodszam <> 0 then begin
			ModLocate (Query1, FOMEZO, IntToStr(ujkodszam) );
           ujkod:=Query_Select('SZFEJ','SA_UJKOD',IntToStr(ujkodszam),'SA_KOD');
           // �tcsatol�s
           //Atcsatolas(regikod,ujkod);
           Atcsatolas(regikod,IntToStr(ujkodszam));
		end;
		Screen.Cursor	:= crDefault;
	end;
end;

procedure TSzamlaFormDlg.Atcsatolas(esakod, usakod: string);
var
  vikod, evikod, jarat: string;
begin
  if (esakod='')or(usakod='') then exit;
  //Query_Run( QueryAtcsat,  'SELECT * from SZFEJ where SA_KOD='''+esakod+'''');
  Query_Run( QueryAtcsat,  'SELECT * from SZFEJ where SA_UJKOD='''+esakod+'''');
  QueryAtcsat.First;
  while not QueryAtcsat.Eof do
  begin
    jarat:=QueryAtcsat.fieldbyname('SA_JARAT').AsString;
    vikod:= Query_Select2('VISZONY','VI_UJKOD','VI_JARAT',usakod,jarat,'VI_VIKOD') ;
    evikod:=Query_Select2('VISZONY','VI_UJKOD','VI_JARAT',usakod,jarat,'VI_EVIKOD') ;
		Query_Run(EgyebDlg.QueryAlap, 'UPDATE MEGBIZAS SET MB_VIKOD = '''+vikod+''''+' WHERE MB_VIKOD='''+evikod+'''');

    QueryAtcsat.Next;
  end;
  QueryAtcsat.Close;

end;

procedure TSzamlaFormDlg.Jarat;
var
  jakod, sa_ujkod: string;
begin
  sa_ujkod:= Query1.FieldByName('SA_UJKOD').AsString;
  jakod:= Query_Select('VISZONY', 'VI_UJKOD', sa_ujkod, 'VI_JAKOD');
  Application.CreateForm(TJaratbeDlg, JaratbeDlg);
  JaratbeDlg.megtekint:=True;
  JaratbeDlg.Tolto('J�rat megekint�s',jakod);
  JaratbeDlg.ShowModal;
  JaratbeDlg.Destroy;
  JaratbeDlg:=nil;
end;

function TSzamlaFormDlg.NAVAdatkuldes(tipus : integer) : boolean;
// tipus = 0 : teszt;  1 - �les
var
   ujkod       : integer;
   jsz         : TJSzamla;
   kuldheto    : boolean;
   szamla      : string;
   sorstr      : string;
   szszam      : string;
   sorsz       : integer;
   vtszstr     : string;
   vtszcat     : string;
   eredmeny    : boolean;
   origsz      : string;
   volthiba    : boolean;
   proba       : integer;
   token       : string;
   fimod       : string;
   afasz       : double;
   vnem        : string;
   ii          : integer;
   mezstat     : string;
   meztrid     : string;
   NO          : TJNavOnline;
   egyar       : double;
   vagyar      : double;
   szsor       : TJSzamlasor;
   elotag      : string;
   szkod       : string;
   jakod       : string;
   S           : string;
begin
   // A sz�mla elk�ld�se a NAV tesztre
   Result      := false;
   volthiba    := false;
   szamla  := Query1.FieldByName('SA_KOD').AsString;
   if szamla = '' then begin
       // �j sz�ml�n�l most hozzuk l�tre a sz�mlasz�mot
       if NoticeKi('A rendszer legener�lja a sz�mlasz�mot! (A sz�mla m�r nem lehet r�sz-sz�mla!!!) Folytatja?', NOT_QUESTION) <> 0 then begin
           Exit;
       end;
       jakod   := Query_Select('JARAT', 'JA_ALKOD', Query1.Fieldbyname('SA_JARAT').AsString, 'JA_KOD');
       szamla	:= GetUjszamla2(copy(Query1.FieldByName('SA_KIDAT').AsString,3,2));
       if (szamla='0') or (szamla='') then begin
             S:= 'Sikertelen sz�mlak�d gener�l�s, k�rem pr�b�lja �jra! (NAVAdatkuldes)';
             HibaKiiro(S);
             NoticeKi(S);
             Exit;
             end;

		Query_Run(Query2,'UPDATE SZFEJ SET SA_KOD = '''+szamla+''' WHERE SA_UJKOD='+Query1.Fieldbyname('SA_UJKOD').AsString, true);
		Query_Run(Query2,'UPDATE VISZONY SET VI_SAKOD='''+szamla+''' WHERE VI_UJKOD='+Query1.Fieldbyname('SA_UJKOD').AsString, true);
		Query_Run(Query2,'UPDATE SZSOR SET SS_KOD='''+szamla+''' WHERE SS_UJKOD='+Query1.Fieldbyname('SA_UJKOD').AsString, true);
       ModLocate (Query1, 'SA_KOD', szamla );
   end;

   Query_Run(Query4,'SELECT * FROM SZFEJ WHERE SA_KOD = '''+szamla+''' ');
   if Query4.RecordCount = 0 then begin
       NoticeKi('Hi�nyz� sz�mla : '+szamla);
       Exit;
   end;

   EllenlistTorol;

   NO  := TJNavOnline.Create(Self);
   if tipus = 0 then begin
       // Teszt rendszer
       NO.Tolt(EgyebDlg.TempPath+'INI\T_NAVKULDES.INI', EgyebDlg.TempPath+'NAVFILES', 'T_');
       mezstat := 'SA_TSTAT';
       meztrid := 'SA_TTRID';
   end else begin
       // �les rendszer
       NO.Tolt(EgyebDlg.TempPath+'INI\NAVKULDES.INI', EgyebDlg.TempPath+'NAVFILES', '');
       mezstat := 'SA_ESTAT';
       meztrid := 'SA_ETRID';
   end;

   if Trim( Query1.FieldByName('SA_VEADO').AsString) = '' then begin
       NoticeKi('Az ad�sz�m �res !');
       volthiba    := true;
   end;

   EllenlistTorol;
   if NO.GetErrorCode <> 0 then begin
       EllenListAppend(NO.GetErrorStr, false);
       volthiba    := true;
   end;
   // A sz�mla felt�lt�se
   if not NO.ReadElado then begin
       EllenListAppend(NO.GetErrorStr, false);
       volthiba    := true;
   end;
//   SetVevo(tax, name, country, postal, city, address, bank : string) : boolean;
   if not NO.SetVevo(Query1.FieldByName('SA_VEADO').AsString, Query1.FieldByName('SA_VEVONEV').AsString, Query1.FieldByName('SA_ORSZ2').AsString, Query1.FieldByName('SA_VEIRSZ').AsString,
       Query1.FieldByName('SA_VEVAROS').AsString, Query1.FieldByName('SA_VECIM').AsString, Query1.FieldByName('SA_VEBANK').AsString) then begin
       EllenListAppend(NO.GetErrorStr, false);
       volthiba    := true;
   end;
//   SetFejlec(szamlaszam, categ, kiadas, teljesites, vanem, method, hatarido, fajta : string) : boolean;
   // Most csak �tutal�s van, de lehet pl. CASH, CARD, TRANSFER, ...
   szszam := Query1.FieldByName('SA_KOD').AsString;
   fimod   := 'TRANSFER';
   if Query1.FieldByName('SA_FIMOD').AsString = 'k�szp�nz' then begin
       fimod   := 'CASH';
   end;
   jsz     := TJSzamla.Create(Self);
   jsz.FillSzamla( Query1.FieldByName('SA_UJKOD').AsString );
   if jsz.szamlaeuro < 1 then begin
       vnem    := 'HUF';
   end else begin
       vnem    := 'EUR';
   end;

   if not NO.FejTolto(szszam, 'NORMAL', Query1.FieldByName('SA_KIDAT').AsString, Query1.FieldByName('SA_TEDAT').AsString, vnem, fimod, Query1.FieldByName('SA_ESDAT').AsString, 'PAPER' ) then begin
       EllenListAppend(NO.GetErrorStr, false);
       volthiba    := true;
   end;

   // Sorok felt�lt�se
//   SorTolto(sorsz, vtszcat, vtszj, descr, megys : string; menny, egyar, netto, afasz, afa : double) : boolean;
   for ii := 0 to jsz.Szamlasorok.Count-1 do begin
       szsor   := TJSzamlasor(jsz.Szamlasorok[ii]);
       afasz   := szsor.afaszaz/100;
       egyar   := szsor.egysegar;
       vagyar  := szsor.valutaegyar;
       if jsz.szamlaeuro < 1 then begin
           if not NO.SorTolto(IntToStr(szsor.sorszam), szsor.itjszj, szsor.termeknev, szsor.megyseg,
               szsor.mennyiseg, egyar, szsor.sornetto, afasz, szsor.sorafa, szsor.sorafa, 1 ) then begin
               EllenListAppend(NO.GetErrorStr, false);
               volthiba    := true;
           end;
       end else begin
           // J�nnek az EUR �rt�kek
           if not NO.SorTolto(IntToStr(szsor.sorszam), szsor.itjszj, szsor.termeknev, szsor.megyseg,
               szsor.mennyiseg, vagyar, szsor.sorneteur, afasz, szsor.sorafaeur, szsor.sorafa, szsor.valutaarfoly ) then begin
               EllenListAppend(NO.GetErrorStr, false);
               volthiba    := true;
           end;
       end;
   end;

   NO.transactid   := Query1.Fieldbyname(meztrid).AsString;
   NO.invstatus    := Query1.Fieldbyname(mezstat).AsString;
   NO.inv_storno   := Query1.Fieldbyname('SA_STORN').AsString;
   if volthiba then begin
       EllenListMutat('NAV felad�si hib�k', false);
   end else begin
       // Ellen�rizz�k a NAV sz�mokat
       if  Format('%.2', [jsz.ossz_brutto]) <> Format('%.2', [NO.Szamla.iGrossAmount]) then begin
           NoticeKi('Figyelem! Az �tadott v�g�sszeg nem egyezik a sz�mla v�g�sszeggel : '+Format('%.f <> %.f', [jsz.ossz_brutto, NO.Szamla.iGrossAmount]) );
       end;
       if Format('%.2', [jsz.ossz_afa]) <> Format('%.2', [NO.Szamla.iVatAmountHUF]) then begin
           NoticeKi('Figyelem! Az �tadott �FA �sszeg nem egyezik a sz�mla �FA �sszeg�vel : '+Format('%.f <> %.f', [jsz.ossz_afa, NO.Szamla.iVatAmountHUF]) );
       end;
       if ( ( Trim(Query1.FieldByName(mezstat).AsString) = '' ) or ( Trim(Query1.FieldByName(mezstat).AsString) = 'ABORTED' ) )  then begin
           // Eddig nem volt sikeres az elk�ld�s -> Elk�ldj�k a NAV -nak
           NO.runcommand   := 'KULD';
           NO.ShowModal;
           if NO.ret_value <> '' then begin
               // Be�rjuk az elemeket az adatb�zisba
               Query_Run(Query2,'UPDATE SZFEJ SET '+mezstat+' = '''+NO.ret_value+''', '+meztrid+' = '''+NO.transactid+''' WHERE SA_KOD = '''+Query1.Fieldbyname('SA_KOD').AsString+''' ');
               ModLocate (Query1, 'SA_KOD', Query1.FieldByName('SA_KOD').AsString );
               Result  := true;
           end;
       end else begin
           if ( ( Trim(Query1.FieldByName(mezstat).AsString) = 'RECEIVED' ) or ( Trim(Query1.FieldByName(mezstat).AsString) = 'PROCESSING' ) )  then begin
               // Csak a st�tuszt k�rj�k le
               NO.runcommand   := 'STATUS';
               NO.ShowModal;
               if NO.ret_value <> '' then begin
                   // Be�rjuk az elemeket az adatb�zisba
                   Query_Run(Query2,'UPDATE SZFEJ SET '+mezstat+' = '''+NO.ret_value+''', '+meztrid+' = '''+NO.transactid+''' WHERE SA_KOD = '''+Query1.Fieldbyname('SA_KOD').AsString+''' ');
                   ModLocate(Query1, 'SA_KOD', Query1.FieldByName('SA_KOD').AsString );
                   Result  := true;
               end;
           end else begin
               // Csak megnyitjuk a dial�gust -> megn�zhetj�k az eddigi m�veleteket
               NO.runcommand   := '';
               NO.ShowModal;
               Result  := true;
           end;
       end;
   end;
end;

end.


