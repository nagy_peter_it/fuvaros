unit MegrendeloKodMagyaraz;

interface

uses
	SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
	Forms, Dialogs, StdCtrls, Buttons, Mask, DB, DBTables, J_ALFORM, ADODB;

type
	TMegrendeloKodMagyarazDlg = class(TJ_AlformDlg)
	BitKilep: TBitBtn;
	 Query2: TADOQuery;
    Label8: TLabel;
    Label1: TLabel;
    meKod: TMaskEdit;
    meMegjegyzes: TMemo;
    Query1: TADOQuery;
    BitBtn1: TBitBtn;
    Label2: TLabel;
    M1: TMaskEdit;
    Label3: TLabel;
    Label4: TLabel;
    meRef: TMaskEdit;
	 procedure BitKilepClick(Sender: TObject);
	 procedure BitElkuldClick(Sender: TObject);
	 procedure MaskEditClick(Sender: TObject);
	 procedure Tolto(cim, kod : string); override;
    procedure FormCreate(Sender: TObject);
	private

	public
	end;

var
	MegrendeloKodMagyarazDlg: TMegrendeloKodMagyarazDlg;

implementation

uses
 Egyeb, J_SQL, Kozos, Clipbrd;

{$R *.DFM}


procedure TMegrendeloKodMagyarazDlg.BitKilepClick(Sender: TObject);
begin
	ret_kod := '';
	Close;
end;

procedure TMegrendeloKodMagyarazDlg.FormCreate(Sender: TObject);
begin
	EgyebDlg.SeTADOQueryDatabase(Query1);
	EgyebDlg.SeTADOQueryDatabase(Query2);
end;

procedure TMegrendeloKodMagyarazDlg.Tolto(cim, kod : string);
begin
	SetMaskEdits([M1]);
	ret_kod  := kod;
	M1.Text		:= ret_kod;
	if ret_kod <> '' then begin
		if Query_Run (Query1, 'SELECT * FROM MEGRENDELO WHERE MR_ID = '+ret_kod ,true) then begin
			meKod.Text:= FormatMegrendeloKod(Query1.FieldByname('MR_DATUM').AsString, Query1.FieldByname('MR_TERULET').AsString, Query1.FieldByname('MR_SORSZAM').AsInteger);
			meRef.Text	:= Query1.FieldByname('MR_REF').AsString;
			meMegjegyzes.Text	:= Query1.FieldByname('MR_MEGJE').AsString;
		  end; // if
   end;  // if
end;

procedure TMegrendeloKodMagyarazDlg.BitElkuldClick(Sender: TObject);
var
  UjRKID: integer;
  TeruletKod, UjKod, S: string;
begin
	if meMegjegyzes.Text = '' then begin
		NoticeKi('Magyarázat megadása kötelező!');
		meMegjegyzes.Setfocus;
		Exit;
  	end;
  Query_Update (Query1, 'MEGRENDELO',
      [
      'MR_MEGJE',  ''''+meMegjegyzes.Text+'''',
      'MR_NEMHASZNALT',  '1'
      ], ' WHERE MR_ID='+ret_kod);
  Close;
end;

procedure TMegrendeloKodMagyarazDlg.MaskEditClick(Sender: TObject);
begin
	with Sender as TMaskEdit do begin
  	SelectAll;
  end;
end;


end.




