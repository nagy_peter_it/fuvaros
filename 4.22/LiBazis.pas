unit LiBazis;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Qrctrls, Db, quickrpt, DBTables, ExtCtrls, printers, qrprntr, ADODB;

type
  TLiBazisDlg = class(TForm)
    QuickRep1: TQuickRep;
    QRBand1: TQRBand;
    QRSysData1: TQRSysData;
    QRLabel15: TQRLabel;
    QRLabel11: TQRLabel;
    QLNev: TQRLabel;
    QRBand3: TQRBand;
    Query1: TADOQuery;
    QRGroup1: TQRGroup;
    Query1SZ_FOKOD: TStringField;
    Query1SZ_ALKOD: TStringField;
    Query1SZ_MENEV: TStringField;
    Query1SZ_LEIRO: TStringField;
    Query1SZ_KODHO: TBCDField;
    Query1SZ_NEVHO: TBCDField;
    Query1SZ_ERVNY: TIntegerField;
    QRShape1: TQRShape;
    QRLabel1: TQRLabel;
    QRDBText1: TQRDBText;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRLabel4: TQRLabel;
    QRShape3: TQRShape;
    QRLabel5: TQRLabel;
    QRDBText4: TQRDBText;
    QRDBText5: TQRDBText;
    QRDBText3: TQRDBText;
    QRDBText6: TQRDBText;
    QRShape2: TQRShape;
    QRLabel6: TQRLabel;
    procedure FormCreate(Sender: TObject);
    procedure QRGroup1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
  private
  end;
var
  LiBazisDlg: TLiBazisDlg;

implementation

uses
	Egyeb, J_SQL, Kozos;

{$R *.DFM}
procedure TLiBazisDlg.FormCreate(Sender: TObject);
begin
	EgyebDlg.SeTADOQueryDatabase(Query1);
  	Query_Run(Query1, 'SELECT * FROM SZOTAR WHERE SZ_FOKOD <> ''000'' ORDER BY SZ_FOKOD, SZ_ALKOD ');
	QlNev.Caption := EgyebDlg.Read_SZGrid('999','101');
end;

procedure TLiBazisDlg.QRGroup1BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
	QrLabel6.Caption	:= EgyebDlg.Read_SZGrid('000',Query1.FieldByName('SZ_FOKOD').AsString);
end;

end.
