unit FlottaHelyzet;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.Samples.Gauges, Vcl.StdCtrls,
  Vcl.ExtCtrls, Vcl.CheckLst, ADODB, Data.DB;

  type
  TSzakaszHelyzet = (rendben, siessen, elkesett, nincs_adat);

type
  TFlottaHelyzetDlg = class(TForm)
    Panel4: TPanel;
    Label38: TLabel;
    cbSorrend: TComboBox;
    ScrollBox1: TScrollBox;
    Panel7: TPanel;
    Label57: TLabel;
    Label58: TLabel;
    Gauge6: TGauge;
    Label59: TLabel;
    Label60: TLabel;
    Label61: TLabel;
    Label62: TLabel;
    Label63: TLabel;
    Label64: TLabel;
    Label65: TLabel;
    Label66: TLabel;
    Label67: TLabel;
    Button6: TButton;
    Label79: TLabel;
    Label1: TLabel;
    CLIST: TCheckListBox;
    Query1: TADOQuery;
    Button1: TButton;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    cbNincsAdatElrejt: TCheckBox;
    Timer1: TTimer;
    rgMegjelenitendok: TRadioGroup;
    Label6: TLabel;
    Query2: TADOQuery;
    pnlDarabszam: TPanel;
    Panel1: TPanel;
    Image1: TImage;
    Label7: TLabel;
    Image2: TImage;
    Image3: TImage;
    imgSiessen: TImage;
    imgElkesett: TImage;
    imgKocsi: TImage;
    Panel2: TPanel;
    Image4: TImage;
    ADOCommand1: TADOCommand;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure UzenetClick(Sender: TObject);
    procedure MegbizasClick(Sender: TObject);

  private
 		listagkat		: TStringList;
    // procedure AddGepkocsiPanel(GKID: integer; i_Rendszam, i_Kateg, i_Sebesseg, i_Hely, i_Honnan, i_SzakaszKm, i_TavolsagCelig,
    //    i_Hova, i_IndulasMikor, i_RendelkezesreAlloIdo, i_ErkezesMeddig: string; i_Progress: integer; i_GaugeColor, i_GBackColor: TColor;
    //    i_MSID1, i_MSID2: integer);
    procedure AddGepkocsiPanel2(GKID: integer; i_Rendszam, i_Kateg, i_Sebesseg, i_Hely, i_Honnan, i_SzakaszKm, i_TavolsagCelig,
        i_Hova, i_IndulasMikor, i_RendelkezesreAlloIdo, i_ErkezesMeddig: string; i_Progress: integer; i_SzakaszHelyzet: TSzakaszHelyzet;
        i_MSID1, i_MSID2: integer);
    procedure Frissit;
    procedure Frissit_SP;
    procedure SzuresMentes;
    procedure SzuresBetoltes;
  public
    { Public declarations }
  end;

var
  FlottaHelyzetDlg: TFlottaHelyzetDlg;

implementation

{$R *.dfm}

uses Kozos, J_SQL, Egyeb, Math, Uzenetszerkeszto, Megbizasbe, KozosTipusok;

procedure TFlottaHelyzetDlg.AddGepkocsiPanel2(GKID: integer; i_Rendszam, i_Kateg, i_Sebesseg, i_Hely, i_Honnan, i_SzakaszKm, i_TavolsagCelig,
        i_Hova, i_IndulasMikor, i_RendelkezesreAlloIdo, i_ErkezesMeddig: string; i_Progress: integer; i_SzakaszHelyzet: TSzakaszHelyzet;
        i_MSID1, i_MSID2: integer);
var
  Elem, GaugePanel: TPanel;
  MBInfo1, MBInfo2, S: string;
  MBKod1, MBKod2: integer;
  MyImage, MyWarningImage: TImage;

  procedure MyCreateLabel(i_Left, i_Top, i_Width, i_Height: integer; i_Caption: string; i_IsBold: boolean; i_Color: TColor; i_Size: integer);
    begin
     with TLabel.Create(Elem) do begin
        Parent:= Elem;
        AutoSize:= False;  // nehogy egym�sra l�gjanak
        Caption:= i_Caption;
        Left:= i_Left;
        Top:= i_Top;
        Width:= i_Width;
        Height:= i_Height;
        Font.Size:= i_Size;
        if i_IsBold then Font.Style:= [fsBold]; // alap�rtelmezett a "norm�l"
        Font.Color:= i_Color;
        end;
     end;

  procedure MyCreatePanel(i_Left, i_Top, i_Width, i_Height: integer; i_Caption: string; i_IsBold: boolean; i_Color, i_BackColor: TColor);
    var
      MyPanel: TPanel;
    begin
     MyPanel:= TPanel.Create(Elem);
     with MyPanel do begin
        Parent:= Elem;
        AutoSize:= False;  // nehogy egym�sra l�gjanak
        Caption:='';
        BevelInner:= bvNone;
        BevelOuter:= bvLowered;
        Left:= i_Left;
        Top:= i_Top;
        Width:= i_Width;
        Height:= i_Height+2;
        Color:= i_BackColor;
        end;

     with TLabel.Create(MyPanel) do begin
        Parent:= MyPanel;
        Align:= alClient;
        // AutoSize:= False;  // nehogy egym�sra l�gjanak
        Caption:= i_Caption;
        Transparent:= True;
        Left:=0;
        Top:=0;
        Font.Size:=10;
        if i_IsBold then Font.Style:= [fsBold]; // alap�rtelmezett a "norm�l"
        Font.Color:= i_Color;
        end;
     end;


begin
   Elem:= TPanel.Create(Scrollbox1);
   with Elem do begin
      Parent:= Scrollbox1;
      Height:=97;
      Align:= alTop;
      Caption:='';
      end;
   with TButton.Create(Elem) do begin
      Parent:= Elem;
      Caption:= '�zenet';
      Left:=11;
      Top:=3;
      Width:=75;
      Height:=20;
      Tag:=GKID;  // k�s�bb az OnClick-hez
      OnClick:= UzenetClick;
      end;
   MyCreateLabel(90, 5, 65, 16, 'Rendsz�m:', False, clWindowText, 10);
   MyCreateLabel(160, 5, 200, 16, i_Rendszam+' ('+i_Kateg+')', True, clBlue, 10);
   MyCreateLabel(274, 5, 58, 16, 'Sebess�g:', False, clWindowText, 10);
   MyCreateLabel(341, 5, 70, 16, i_Sebesseg, True, clBlue, 10);
   MyCreateLabel(418, 5, 30, 16, 'Hely:', False, clWindowText, 10);
   MyCreateLabel(456, 5, 310, 16, i_Hely, True, clBlue, 10);
   // MyCreateLabel(24, 26, 180, 16, i_Honnan, True, clWindowText);
   MyCreatePanel(11, 26, 190, 16, i_Honnan, True, clWindowText, clAqua);

   MyCreateLabel(213, 26, 121, 16, 'szakasz l�gvonalban:', False, clWindowText, 10);
   MyCreateLabel(339, 26, 60, 16, i_SzakaszKm, True, clWindowText, 10);
   MyCreateLabel(400, 26, 110, 16, 'a c�lig l�gvonalban:', False, clWindowText, 10);
   MyCreateLabel(516, 26, 60, 16, i_TavolsagCelig, True, clWindowText, 10);
   // MyCreateLabel(594, 26, 180, 16, i_Hova, True, clWindowText);
   MyCreatePanel(594, 26, 190, 16, i_Hova, True, clWindowText, clAqua);
   MyCreateLabel(24, 44, 210, 16, 'Indul�s: '+i_IndulasMikor, False, clWindowText, 10);
   // ------ debug: MB_MBKOD
   if i_MSID1>0 then begin
     S:= 'select MS_MBKOD, MS_SORSZ from MEGSEGED where MS_ID='+IntToStr(i_MSID1);
     Query_Run(Query2, S);
     MBKod1:= Query2.FieldByName('MS_MBKOD').AsInteger;
     MBInfo1:= Query2.FieldByName('MS_MBKOD').AsString+'/'+Query2.FieldByName('MS_SORSZ').AsString;
     end
   else begin
     MBKod1:=0;
     MBInfo1:= '??';
     end;
   if i_MSID2>0 then begin
     S:= 'select MS_MBKOD, MS_SORSZ from MEGSEGED where MS_ID='+IntToStr(i_MSID2);
     Query_Run(Query2, S);
     MBKod2:= Query2.FieldByName('MS_MBKOD').AsInteger;
     MBInfo2:= Query2.FieldByName('MS_MBKOD').AsString+'/'+Query2.FieldByName('MS_SORSZ').AsString;
     end
   else begin
     MBKod2:=0;
     MBInfo2:= '??';
     end;

   // cikl�men label
   // S:='('+MBInfo1+', '+MBInfo2+')';
   //MyCreateLabel(213, 46, 126, 16, S, False, clFuchsia, 8);

   with TButton.Create(Elem) do begin
      Parent:= Elem;
      Font.Size:= 8;
      Font.Color:= clFuchsia;
      Caption:= 'MB'+MBInfo1;
      Left:=213;
      Top:=46;
      Width:=65;
      Height:=16;
      Tag:= MBKod1;  // k�s�bb az OnClick-hez
      OnClick:= MegbizasClick;
      end;

   with TButton.Create(Elem) do begin
      Parent:= Elem;
      Font.Size:= 8;
      Font.Color:= clFuchsia;
      Caption:= 'MB'+MBInfo2;
      Left:=279;
      Top:=46;
      Width:=65;
      Height:=16;
      Tag:= MBKod2;  // k�s�bb az OnClick-hez
      OnClick:= MegbizasClick;
      end;

   // ------------------------
   MyCreateLabel(356, 44, 130, 16, 'rendelkez�sre �ll� id�: ', False, clWindowText, 10);
   MyCreateLabel(489, 44, 100, 16, i_RendelkezesreAlloIdo, True, clWindowText, 10);
   MyCreateLabel(594, 44, 210, 16, 'Hat�rid�: '+i_ErkezesMeddig, False, clWindowText, 10);
   // ------------------------------------------------------------
   // ---------------------- "Gauge Plus" ------------------------
   // ------------------------------------------------------------
   GaugePanel:= TPanel.Create(Elem);
   with GaugePanel do begin
      Parent:= Elem;
      ParentColor:= False;
      ParentBackground:= False;
      Left:=16;
      Top:=66;
      Width:=760;
      Height:=20;
      BevelOuter:= bvLowered;
      Caption:= '';
      GaugePanel.Color:= clAqua;
      // Refresh;
      end; // with GaugePanel
   if i_SzakaszHelyzet = nincs_adat then begin
      GaugePanel.Color:= clSilver;
      end
   else begin
       GaugePanel.Color:= clYellow;
       if i_Progress < 0 then i_Progress:=0;  // negat�v sz�mokat ne
       MyImage:= TImage.Create(GaugePanel);
       with MyImage do begin
          Parent:= GaugePanel;
          Top:=3;
          AutoSize:= True;
          Picture:= imgKocsi.Picture; // m�soljuk
          Left:= Floor((GaugePanel.Width - Width)* (i_Progress/100));
          end;
       with TLabel.Create(GaugePanel) do begin
          Parent:= GaugePanel;
          Caption:= IntToStr(i_Progress)+'%';
          Transparent:= True;
          if i_Progress <=9 then
            Left:=MyImage.Left+14
          else Left:=MyImage.Left+6;
          Top:=MyImage.Top-2;
          Font.Size:=8;
          Font.Color:= clWhite;
          end;
       if (i_SzakaszHelyzet = siessen) or (i_SzakaszHelyzet = elkesett) then begin    //      (rendben, siessen, elkesett, nincs_adat);
         MyWarningImage:= TImage.Create(GaugePanel);
         with MyWarningImage do begin
            Parent:= GaugePanel;
            Top:=3;
            AutoSize:= True;
            if i_SzakaszHelyzet = siessen then
              Picture:= imgSiessen.Picture; // m�soljuk
            if i_SzakaszHelyzet = elkesett then
              Picture:= imgElkesett.Picture; // m�soljuk
            if i_Progress<50 then
              Left:= MyImage.Left + MyImage.Width + 6  // a kocsit�l jobbra tessz�k ki
            else
              Left:= MyImage.Left - Width - 6;   // a kocsit�l balra tessz�k ki
            end; // with
         end;  // if siessen vagy elkesett
      end;  // else = van j� adat
   // GaugePanel.Refresh;
   // ------------------------------------------------------------
end;


procedure TFlottaHelyzetDlg.UzenetClick(Sender: TObject);
var
   i, GKKOD, j: integer;
   RENDSZAM, S, DOKOD1, DOKOD2: string;
begin
   Screen.Cursor	:= crHourGlass;
   GKKOD:= (Sender as TButton).Tag;
   RENDSZAM:= Query_Select('GEPKOCSI', 'GK_KOD', IntToStr(GKKOD), 'GK_RESZ');
   S:= 'select DO_KOD from rendszam_sofor where RENDSZ='''+RENDSZAM+'''';
   with Query2 do begin
      Close; SQL.Clear;
      SQL.Add(S);
      Open;
      DOKOD1:= '';
      DOKOD2:= '';
      j:= 1;
      while not eof do begin
        if j=1 then DOKOD1:=Fields[0].AsString;
        if j=2 then DOKOD2:=Fields[0].AsString;
        Inc(j);
        Next;
        end;  // while
      end;  // with
  TruckpitUzenetKuldes(DOKOD1, DOKOD2, '', ''); // Truckpit
end;


procedure TFlottaHelyzetDlg.MegbizasClick(Sender: TObject);
var
   MBKOD: integer;
   meguj: TMegbizasbeDlg;
begin
   MBKOD:= (Sender as TButton).Tag;
   if MBKOD <> 0 then begin
       Application.CreateForm(TMegbizasbeDlg, meguj);
       meguj.Tolto('Megbizas megjelen�t�se', IntToStr(MBKOD));
       meguj.ShowModal;
       if meguj.ret_kod <> '' then begin
           MegbizasKieg(meguj.ret_kod);
           end;
       meguj.Destroy;
   end;
end;


procedure TFlottaHelyzetDlg.Button1Click(Sender: TObject);
begin
    SzuresMentes;
    Frissit_SP;
    Frissit;
end;

procedure TFlottaHelyzetDlg.SzuresMentes;
var
  kategstr: string;
  i: integer;
begin
// forma az .ini-ben:
// [FLOTTAHELYZET]
// sorrend=1   ; combo-n bel�li sorrend
// hianyosrejtes=TRUE
// kategorialista=24EU,24,12   ; sz�veges �rt�kek list�ja
// megjelenitendoszakaszok=1   ; radiogroup-on bel�li sorrend

  OpenLocalIni(1);
	WriteLocalIni('FLOTTAHELYZET', 'sorrend', IntToStr(cbSorrend.ItemIndex));
  WriteLocalIni('FLOTTAHELYZET', 'hianyosrejtes', BoolToStr(cbNincsAdatElrejt.State=cbChecked));
  kategstr:= '';
  for i := 0 to CLIST.Items.Count - 1 do begin
     if CLIST.Checked[i] then
       kategstr:= Felsorolashoz(kategstr, listagkat[i], ';', False);
     end;  // for
  WriteLocalIni('FLOTTAHELYZET', 'kategorialista', kategstr);
  WriteLocalIni('FLOTTAHELYZET', 'megjelenitendoszakaszok', IntToStr(rgMegjelenitendok.ItemIndex));
	CloseLocalIni;
end;

procedure TFlottaHelyzetDlg.SzuresBetoltes;
var
  kategstr, egykateg: string;
  i: integer;
  Found: boolean;
begin
// forma az .ini-ben:
// [FLOTTAHELYZET]
// sorrend=1   ; combo-n bel�li sorrend
// hianyosrejtes=TRUE
// kategorialista=24EU,24,12   ; sz�veges �rt�kek list�ja
// megjelenitendoszakaszok=1   ; radiogroup-on bel�li sorrend

  OpenLocalIni(1);
	cbSorrend.ItemIndex:= StringEgesz(ReadLocalIni('FLOTTAHELYZET', 'sorrend'));
  if StrToBool(ReadLocalIni('FLOTTAHELYZET', 'hianyosrejtes')) then
    cbNincsAdatElrejt.State:=cbChecked
  else cbNincsAdatElrejt.State:=cbUnChecked;
  // kateg lista
  for i := 0 to CLIST.Items.Count - 1 do CLIST.Checked[i]:= False;  // t�rl�s
  kategstr:= ReadLocalIni('FLOTTAHELYZET', 'kategorialista');
  while kategstr <> '' do begin
    egykateg:= EgyebDlg.SepFrontToken(';', kategstr);  // els� elem levesz
    kategstr:= EgyebDlg.SepRest(';', kategstr);        // a t�bbi elem
    i:= 0;
    Found:= false;
    while (i<= CLIST.Items.Count - 1) and (not Found) do begin  // megkeress�k
      if listagkat[i]= egykateg then begin
        CLIST.Checked[i]:= True;  // be�ll�tjuk
        Found:= True;
        end;  // if
      Inc(i);
      end;  // while CLIST
    end;  // while kategstr
 	rgMegjelenitendok.ItemIndex:= StringEgesz(ReadLocalIni('FLOTTAHELYZET', 'megjelenitendoszakaszok'));
	CloseLocalIni;
end;

procedure TFlottaHelyzetDlg.Frissit_SP;
var
  S: string;
begin
  Screen.Cursor	:= crHourGlass;
  S:= 'exec spRefresh_GKSZAKASZ_v3';
  // Query_run(Query2, S, True);
  Command_Run(ADOCommand1, S, True);
  Screen.Cursor	:= crDefault;
end;

procedure TFlottaHelyzetDlg.Frissit;
var
  S, GKKOD, RendelkezesreAlloIdo, ErkezD, ErkezI, MostD, MostI, kategstr: string;
  GColor, GBackColor: TColor;
  SzakaszKm, Dist: double;
  GaugePercent, TotalDiffSec, TotalDiffPerc, DiffOra, DiffPerc, SzuksegesSebesseg, i, darab: integer;
  VanAdat: boolean;
  SzakaszHelyzet: TSzakaszHelyzet;
begin
  // -------- elemek t�rl�se ------------ //
  while ScrollBox1.ControlCount > 0 do ScrollBox1.Controls[0].Free;  // az �sszes al-panel t�rl�se
  // ------------------------------------ //

  S:= 'select GK_KOD, GS_RENDSZAM, GS_HONNAN, GS_HOVA, GS_SZAKASZKM, GS_INDULI, GS_ERKEZI, GS_GKDIST, GS_GKSEBES, GS_GKGEKOD, '+
      ' GS_HONNAN_MSID, GS_HOVA_MSID, GK_GKKAT '+
      ' from GKSZAKASZ, GEPKOCSI where GK_RESZ= GS_RENDSZAM';
  kategstr	:= '';
  if not CLIST.Checked[0] then begin  // ha be vab jel�lve a "Minden", akkor nem kell a sz�r�s
    for i := 1 to CLIST.Items.Count - 1 do begin
        if CLIST.Checked[i] then
           kategstr:= Felsorolashoz(kategstr, ''''+ listagkat[i] + '''', ',', False);
        end;  // for
    if kategstr <> '' then
      S:= S+' and GK_GKKAT in ('+kategstr+') ';
    end; // if

  if cbSorrend.ItemIndex = 0 then begin  // rendsz�m szerint
     S:= S+ ' order by GS_RENDSZAM desc';  // a l�trehoz�s ford�tott sorrendj�ben lesz l�that�
    end;
  if cbSorrend.ItemIndex = 1 then begin  // �rkez�si id� szerint
     S:= S+ ' order by GS_ERKEZI desc';  // a l�trehoz�s ford�tott sorrendj�ben lesz l�that�
    end;
  if cbSorrend.ItemIndex = 2 then begin  // kateg�ria �s rendsz�m szerint
     S:= S+ ' order by GK_GKKAT desc, GS_RENDSZAM desc';  // a l�trehoz�s ford�tott sorrendj�ben lesz l�that�
    end;
  Query_run(Query1, S);
  darab:= 0;
  with Query1 do begin
    while not eof do begin
        VanAdat:= False;
        SzakaszKm:= FieldByName('GS_SZAKASZKM').AsFloat;
        if SzakaszKm>0 then begin  // �rv�nyes adatok vannak
          Dist:= FieldByName('GS_GKDIST').AsFloat;
          GaugePercent := Floor(((SzakaszKm-Dist)/SzakaszKm)*100.00);  // ide csak akkor ker�l, ha SzakaszKm>0
          ErkezD:= EgyebDlg.SepFrontToken(' ', FieldByName('GS_ERKEZI').AsString);  // pl. 2016.10.12.
          ErkezI:= EgyebDlg.SepRest(' ', FieldByName('GS_ERKEZI').AsString);   // pl. 08:45
          MostD:= FormatDateTime('YYYY.MM.DD', Now);
          MostI:= FormatDateTime('HH:mm', Now);
          if (Trim(ErkezD) <> '') and (Trim(ErkezI) <> '') then begin
            TotalDiffSec:= DateTimeDifferenceSec(ErkezD, ErkezI, MostD, MostI); // m�sodpercekben
            if TotalDiffSec > 0 then begin
              TotalDiffPerc:= Floor( TotalDiffSec / 60.0);  // k�nnyebb percekben �tl�tni
              DiffOra:= Floor( TotalDiffPerc / 60.0);
              DiffPerc:= TotalDiffPerc- 60*DiffOra;
              RendelkezesreAlloIdo:=IntToStr(DiffOra)+ ' �ra '+IntToStr(DiffPerc)+ ' perc';
              SzuksegesSebesseg:= Floor(Dist/(TotalDiffPerc/60.0));
              end
            else begin
              RendelkezesreAlloIdo:= 'K�S�S!';
              SzuksegesSebesseg:= 999999;
              end;
            VanAdat:= True;
            end
          else begin // az id�k nincsenek kit�ltve
            RendelkezesreAlloIdo:='<nincs adat>';
            SzuksegesSebesseg:= 0;
            end;
          if SzuksegesSebesseg< EgyebDlg.SIESSEN_SEBESSEG then begin
              GColor:= clBlack;
              GBackColor:= clWhite;
              SzakaszHelyzet:= rendben
              end
          else if SzuksegesSebesseg< EgyebDlg.ELKESETT_SEBESSEG then begin
              // GColor:= clYellow;
              GColor:= clBlack;
              GBackColor:= clYellow;
              SzakaszHelyzet:= siessen;
              end
          else begin
              // GColor:= clRed;
              GColor:= clBlack;
              GBackColor:= clRed;
              SzakaszHelyzet:= elkesett;
              end;
          // -- aki tot�l elk�sett --
          if TotalDiffPerc<=0 then begin
              // GColor:= clRed;
              GColor:= clBlack;
              GBackColor:= clRed;
              SzakaszHelyzet:= elkesett;
              end;
          end
        else begin   // �rv�nytelen adatok vannak
          Dist:= 0;
          GaugePercent := 0;
          GBackColor:= clSilver;
          RendelkezesreAlloIdo:='<nincs adat>';
          SzakaszHelyzet:= nincs_adat;
          end;
        if (not cbNincsAdatElrejt.Checked) or VanAdat then begin
          if  (rgMegjelenitendok.ItemIndex=0)
           or ((rgMegjelenitendok.ItemIndex=1) and ((SzakaszHelyzet=elkesett) or (SzakaszHelyzet=siessen)) and VanAdat)
           or ((rgMegjelenitendok.ItemIndex=2) and (SzakaszHelyzet=elkesett) and VanAdat) then begin
            AddGepkocsiPanel2(FieldByName('GK_KOD').AsInteger,
                FieldByName('GS_RENDSZAM').AsString,
                FieldByName('GK_GKKAT').AsString,
                IntToStr(FieldByName('GS_GKSEBES').AsInteger)+ ' km/h',
                FieldByName('GS_GKGEKOD').AsString,
                FieldByName('GS_HONNAN').AsString,
                FormatFloat('0', SzakaszKm)+ ' km',  // itt nem �rdekes a tizedes
                FormatFloat('0.0', Dist)+ ' km',
                FieldByName('GS_HOVA').AsString,
                FieldByName('GS_INDULI').AsString,
                RendelkezesreAlloIdo,
                FieldByName('GS_ERKEZI').AsString,
                GaugePercent,
                SzakaszHelyzet,
                FieldByName('GS_HONNAN_MSID').AsInteger,
                FieldByName('GS_HOVA_MSID').AsInteger
                );
             Inc(darab);
             end; // if addpanel
           end; // if VanAdat
        Next;
        end;  // while;
    end;  // with
    pnlDarabszam.Caption:= IntToStr(darab)+ ' g�pkocsi';
end;

procedure TFlottaHelyzetDlg.Timer1Timer(Sender: TObject);
begin
   Frissit;
end;

procedure TFlottaHelyzetDlg.FormCreate(Sender: TObject);
var
  i: integer;
  S: string;
begin
   EgyebDlg.SetADOQueryDatabase(Query1);
   EgyebDlg.SetADOQueryDatabase(Query2);
   // behegesztettem... ADOCommand1.Connection:= EgyebDlg.ADOCOnnectionAlap;
   listagkat	:= TStringList.Create;
   S:= 'select distinct GK_GKKAT from POZICIOK, GEPKOCSI where GK_RESZ=PO_RENSZ and PO_DATUM>= GETDATE()-(1) '+
      ' and GK_GKKAT not in (''SZGK.'')';
  CLIST.Clear;
  CLIST.Items.Add('Minden kat.');
	listagkat.Add('Minden kat.');
  if Query_Run(Query2, S) then begin
		while not Query2.EOF do begin
			CLIST.Items.Add(Query2.FieldByName('GK_GKKAT').AsString);
			listagkat.Add(Query2.FieldByName('GK_GKKAT').AsString);
			Query2.Next;
      end;  // while
		end;  // if
   CLIST.ItemIndex		:= 0;
   SzuresBetoltes;
   Frissit;
end;

procedure TFlottaHelyzetDlg.FormDestroy(Sender: TObject);
begin
  if listagkat <>nil then listagkat.Free;
end;

{procedure TFlottaHelyzetDlg.AddGepkocsiPanel(GKID: integer; i_Rendszam, i_Kateg, i_Sebesseg, i_Hely, i_Honnan, i_SzakaszKm, i_TavolsagCelig,
        i_Hova, i_IndulasMikor, i_RendelkezesreAlloIdo, i_ErkezesMeddig: string; i_Progress: integer; i_GaugeColor, i_GBackColor: TColor;
        i_MSID1, i_MSID2: integer);
var
  Elem: TPanel;
  MBInfo1, MBInfo2, S: string;
  MBKod1, MBKod2: integer;

  procedure MyCreateLabel(i_Left, i_Top, i_Width, i_Height: integer; i_Caption: string; i_IsBold: boolean; i_Color: TColor; i_Size: integer);
    begin
     with TLabel.Create(Elem) do begin
        Parent:= Elem;
        AutoSize:= False;  // nehogy egym�sra l�gjanak
        Caption:= i_Caption;
        Left:= i_Left;
        Top:= i_Top;
        Width:= i_Width;
        Height:= i_Height;
        Font.Size:= i_Size;
        if i_IsBold then Font.Style:= [fsBold]; // alap�rtelmezett a "norm�l"
        Font.Color:= i_Color;
        end;
     end;

  procedure MyCreatePanel(i_Left, i_Top, i_Width, i_Height: integer; i_Caption: string; i_IsBold: boolean; i_Color, i_BackColor: TColor);
    var
      MyPanel: TPanel;
    begin
     MyPanel:= TPanel.Create(Elem);
     with MyPanel do begin
        Parent:= Elem;
        AutoSize:= False;  // nehogy egym�sra l�gjanak
        Caption:='';
        BevelInner:= bvNone;
        BevelOuter:= bvLowered;
        Left:= i_Left;
        Top:= i_Top;
        Width:= i_Width;
        Height:= i_Height+2;
        Color:= i_BackColor;
        end;

     with TLabel.Create(MyPanel) do begin
        Parent:= MyPanel;
        Align:= alClient;
        // AutoSize:= False;  // nehogy egym�sra l�gjanak
        Caption:= i_Caption;
        Transparent:= True;
        Left:=0;
        Top:=0;
        Font.Size:=10;
        if i_IsBold then Font.Style:= [fsBold]; // alap�rtelmezett a "norm�l"
        Font.Color:= i_Color;
        end;
     end;


begin
   Elem:= TPanel.Create(Scrollbox1);
   with Elem do begin
      Parent:= Scrollbox1;
      Height:=97;
      Align:= alTop;
      Caption:='';
      end;
   with TButton.Create(Elem) do begin
      Parent:= Elem;
      Caption:= '�zenet';
      Left:=11;
      Top:=3;
      Width:=75;
      Height:=20;
      Tag:=GKID;  // k�s�bb az OnClick-hez
      OnClick:= UzenetClick;
      end;
   MyCreateLabel(90, 5, 65, 16, 'Rendsz�m:', False, clWindowText, 10);
   MyCreateLabel(160, 5, 200, 16, i_Rendszam+' ('+i_Kateg+')', True, clBlue, 10);
   MyCreateLabel(274, 5, 58, 16, 'Sebess�g:', False, clWindowText, 10);
   MyCreateLabel(341, 5, 70, 16, i_Sebesseg, True, clBlue, 10);
   MyCreateLabel(418, 5, 30, 16, 'Hely:', False, clWindowText, 10);
   MyCreateLabel(456, 5, 310, 16, i_Hely, True, clBlue, 10);
   // MyCreateLabel(24, 26, 180, 16, i_Honnan, True, clWindowText);
   MyCreatePanel(11, 26, 190, 16, i_Honnan, True, clWindowText, clAqua);

   MyCreateLabel(213, 26, 121, 16, 'szakasz l�gvonalban:', False, clWindowText, 10);
   MyCreateLabel(339, 26, 60, 16, i_SzakaszKm, True, clWindowText, 10);
   MyCreateLabel(400, 26, 110, 16, 'a c�lig l�gvonalban:', False, clWindowText, 10);
   MyCreateLabel(516, 26, 60, 16, i_TavolsagCelig, True, clWindowText, 10);
   // MyCreateLabel(594, 26, 180, 16, i_Hova, True, clWindowText);
   MyCreatePanel(594, 26, 190, 16, i_Hova, True, clWindowText, clAqua);
   MyCreateLabel(24, 44, 210, 16, 'Indul�s: '+i_IndulasMikor, False, clWindowText, 10);
   // ------ debug: MB_MBKOD
   if i_MSID1>0 then begin
     S:= 'select MS_MBKOD, MS_SORSZ from MEGSEGED where MS_ID='+IntToStr(i_MSID1);
     Query_Run(Query2, S);
     MBKod1:= Query2.FieldByName('MS_MBKOD').AsInteger;
     MBInfo1:= Query2.FieldByName('MS_MBKOD').AsString+'/'+Query2.FieldByName('MS_SORSZ').AsString;
     end
   else begin
     MBKod1:=0;
     MBInfo1:= '??';
     end;
   if i_MSID2>0 then begin
     S:= 'select MS_MBKOD, MS_SORSZ from MEGSEGED where MS_ID='+IntToStr(i_MSID2);
     Query_Run(Query2, S);
     MBKod2:= Query2.FieldByName('MS_MBKOD').AsInteger;
     MBInfo2:= Query2.FieldByName('MS_MBKOD').AsString+'/'+Query2.FieldByName('MS_SORSZ').AsString;
     end
   else begin
     MBKod2:=0;
     MBInfo2:= '??';
     end;

   // cikl�men label
   // S:='('+MBInfo1+', '+MBInfo2+')';
   //MyCreateLabel(213, 46, 126, 16, S, False, clFuchsia, 8);

   with TButton.Create(Elem) do begin
      Parent:= Elem;
      Font.Size:= 8;
      Font.Color:= clFuchsia;
      Caption:= 'MB'+MBInfo1;
      Left:=213;
      Top:=46;
      Width:=65;
      Height:=16;
      Tag:= MBKod1;  // k�s�bb az OnClick-hez
      OnClick:= MegbizasClick;
      end;

   with TButton.Create(Elem) do begin
      Parent:= Elem;
      Font.Size:= 8;
      Font.Color:= clFuchsia;
      Caption:= 'MB'+MBInfo2;
      Left:=279;
      Top:=46;
      Width:=65;
      Height:=16;
      Tag:= MBKod2;  // k�s�bb az OnClick-hez
      OnClick:= MegbizasClick;
      end;

   // ------------------------
   MyCreateLabel(356, 44, 130, 16, 'rendelkez�sre �ll� id�: ', False, clWindowText, 10);
   MyCreateLabel(489, 44, 100, 16, i_RendelkezesreAlloIdo, True, clWindowText, 10);
   MyCreateLabel(594, 44, 210, 16, 'Hat�rid�: '+i_ErkezesMeddig, False, clWindowText, 10);

   with TGauge.Create(Elem) do begin
      Parent:= Elem;
      Left:=16;
      Top:=66;
      Width:=760;
      Height:=18;
      Progress:= i_Progress;
      ForeColor:= i_GaugeColor;
      BackColor:= i_GBackColor
      end;
end;
}

end.

