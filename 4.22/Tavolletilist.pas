unit Tavolletilist;

interface

uses
  WinTypes, WinProcs, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, QuickRpt, QRExpr, Qrctrls, ExtCtrls, printers, DB, DBTables, Grids, ADODB,clipbrd ;

type
  TTavolletiListDlg = class(TForm)
    Rep: TQuickRep;
    QRBand3: TQRBand;
    QRLSzamla: TQRLabel;
    QRBand1: TQRBand;
    QRBand2: TQRBand;
    QRLabel16: TQRLabel;
    QRLabel38: TQRLabel;
    QRLabel6: TQRLabel;
    QRSysData1: TQRSysData;
	 QRLabel7: TQRLabel;
    QRSysData4: TQRSysData;
    QRShape1: TQRShape;
    QRLabel1: TQRLabel;
    QRLabel35: TQRLabel;
    OOIDO: TQRLabel;
    QRLabel21: TQRLabel;
    QRLabel3: TQRLabel;
    ArfolyamGrid: TStringGrid;
    Osszesito: TStringGrid;
    QRLabel2: TQRLabel;
    QRLabel13: TQRLabel;
    QRLabel17: TQRLabel;
    Query2: TADOQuery;
    QRLabel8: TQRLabel;
    ADOCommand1: TADOCommand;
    QRBand5: TQRBand;
    QRLabel19: TQRLabel;
    QJARAT: TQRLabel;
    QNAP: TQRLabel;
    QIDO: TQRLabel;
    QTOLORA: TQRLabel;
    QIGORA: TQRLabel;
    ONAP: TQRLabel;
    OIDO: TQRLabel;
    QRGroup1: TQRGroup;
    QRGroup2: TQRGroup;
    QRLabel4: TQRLabel;
    OONEV: TQRLabel;
    QRLabel5: TQRLabel;
    QRShape3: TQRShape;
    QRLabel9: TQRLabel;
    QRLabel10: TQRLabel;
    QRShape4: TQRShape;
    QNEV: TQRLabel;
    QRShape5: TQRShape;
	 procedure FormCreate(Sender: TObject);
	 procedure	Tolt(dat1: string);
   procedure FillTempTables(dat1, dat2: string);
   function GetNaptarLista(dat1, dat2: string): string;
	procedure QRBand1BeforePrint(Sender: TQRCustomBand;
	   var PrintBand: Boolean);
    procedure QRGroup1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBand2BeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
    procedure QRBand5BeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
    procedure QRGroup2BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
     {
	 procedure QRBand2BeforePrint(Sender: TQRCustomBand;
	   var PrintBand: Boolean);
	 procedure RepBeforePrint(Sender: TCustomQuickRep;
	   var PrintReport: Boolean);
	 procedure RepNeedData(Sender: TObject; var MoreData: Boolean);
	 procedure ChildBand1BeforePrint(Sender: TQRCustomBand;
	   var PrintBand: Boolean);
	 procedure QRBand1AfterPrint(Sender: TQRCustomBand;
	   BandPrinted: Boolean);
	 function	GetLocalArf(datu : string) : double;
   }
  private
		SzummaNapok: integer;
    NapiOrak: double;
    AktualNEV, LegutobbiNap, LegutobbiIg: string;
  public
	  vanadat			: boolean;
  end;

var
  TavolletiListDlg: TTavolletiListDlg;

implementation

uses
	J_SQL, Egyeb, KOzos;

{$R *.DFM}

procedure TTavolletiListDlg.FormCreate(Sender: TObject);
begin
	EgyebDlg.SetADOQueryDatabase(Query2);
  ADOCommand1.Connection:= EgyebDlg.ADOCOnnectionAlap;
	vanadat		:= true;
end;

procedure TTavolletiListDlg.FillTempTables(dat1, dat2: string);
const
  sqlFROM = ' FROM JARAT, JARSOFOR ';
  sqlWHERE = ' WHERE JA_KOD = JS_JAKOD AND JA_EUROS=2 AND JA_FAZIS in (-1, 1) '; // -1: Elsz�molt, 1: ellen�rz�tt
var
	sqlstr, Tavollet_ora_limit: string;
begin
  Tavollet_ora_limit:= EgyebDlg.Read_SZGrid('999', '761');
  if not JoEgesz(Tavollet_ora_limit) then
      NoticeKi('Hib�s �ra hat�r�rt�k: '+Tavollet_ora_limit+'. M�dos�tand� az adatsz�t�rban, 999-es f�k�d, 761-es alk�d!');
  if Tavollet_ora_limit='' then Tavollet_ora_limit:= '6';

  QRLSzamla.Caption:= 'T�voll�ti d�j elsz�mol�s (min. '+ Tavollet_ora_limit+' �ra)';
  //  #NAPI_JARSOFOR: az �sszes j�ratra az adott id�szakban

  Command_run(ADOCommand1, 'IF OBJECT_ID(''tempdb..#NAPI_JARSOFOR'') IS NOT NULL DROP TABLE #NAPI_JARSOFOR', False);
  sqlstr:= 'select * into #NAPI_JARSOFOR from ( ';
  sqlstr:= sqlstr + 'select JS_SOFOR, JA_JKEZD JA_NAP, JA_KEIDO, JA_JVEGE, JA_VEIDO, JA_KOD, JA_ORSZ+''-''+convert(varchar(6),convert(int,JA_ALKOD)) JARATSZAM, ';
  sqlstr:= sqlstr + '(datediff(mi, szami.FuvarosDatumToDatetime(JA_JKEZD, JA_KEIDO), szami.FuvarosDatumToDatetime(JA_JVEGE, JA_VEIDO))+1)/60.0 JARAT_ORAK ';
  sqlstr:= sqlstr + 'from ( ';
  // egynapos j�ratok
  sqlstr:= sqlstr + ' SELECT DISTINCT JS_SOFOR, JA_JKEZD, JA_KEIDO, JA_JVEGE, JA_VEIDO, JA_KOD, JA_ORSZ, JA_ALKOD ';
  sqlstr:= sqlstr + sqlFROM + sqlWHERE;
  sqlstr:= sqlstr + ' AND JA_JKEZD = JA_JVEGE AND JA_JKEZD between '''+dat1+''' and '''+dat2+'''';  // abban a h�napban volt
  sqlstr:= sqlstr + ' UNION ';
  // t�bbnaposok els� napja: aznap �jf�lig
  sqlstr:= sqlstr + ' SELECT DISTINCT JS_SOFOR, JA_JKEZD, JA_KEIDO, JA_JKEZD, ''23:59'', JA_KOD, JA_ORSZ, JA_ALKOD ';
  sqlstr:= sqlstr + sqlFROM + sqlWHERE;
  sqlstr:= sqlstr + ' AND JA_JKEZD <> JA_JVEGE AND JA_JKEZD between '''+dat1+''' and '''+dat2+'''';  // abban a h�napban kezd�d�tt
  sqlstr:= sqlstr + ' UNION ';
  // t�bbnaposok utols� napja: aznap 00:00-t�l
  sqlstr:= sqlstr + ' SELECT DISTINCT JS_SOFOR, JA_JVEGE, ''00:00'', JA_JVEGE, JA_VEIDO, JA_KOD, JA_ORSZ, JA_ALKOD ';
  sqlstr:= sqlstr + sqlFROM + sqlWHERE;
  sqlstr:= sqlstr + ' AND JA_JKEZD <> JA_JVEGE AND JA_JVEGE between '''+dat1+''' and '''+dat2+'''';  // abban a h�napban v�gz�d�tt
  sqlstr:= sqlstr + ' UNION ';
  // t�bbnaposok k�ztes napjai
  sqlstr:= sqlstr + ' SELECT JS_SOFOR, NAPTAR.DATUM, ''00:00'', NAPTAR.DATUM, ''23:59'', JA_KOD, JA_ORSZ, JA_ALKOD ';
  sqlstr:= sqlstr + ' FROM JARAT, JARSOFOR, (';
  sqlstr:= sqlstr + GetNaptarLista(dat1, dat2);
  sqlstr:= sqlstr + ') NAPTAR ' + sqlWHERE;
  sqlstr:= sqlstr + ' AND NAPTAR.DATUM > JA_JKEZD AND NAPTAR.DATUM < JA_JVEGE ';
  sqlstr:= sqlstr + ' AND JA_JKEZD <> JA_JVEGE AND NAPTAR.DATUM between '''+dat1+''' and '''+dat2+'''';  // abba a h�napba esik
  sqlstr:= sqlstr + ' ) a ';
  sqlstr:= sqlstr + ') b ';
  Command_run(ADOCommand1, sqlstr, True);

  //  #NAPI_JARSOFOR_SUM: �sszegz�s azokra, akiknek megvolt a 4 �ra
  Command_run(ADOCommand1, 'IF OBJECT_ID(''tempdb..#NAPI_JARSOFOR_SUM'') IS NOT NULL DROP TABLE #NAPI_JARSOFOR_SUM', False);
  sqlstr:= 'select * into #NAPI_JARSOFOR_SUM  from ( ';
  sqlstr:= sqlstr + ' select JS_SOFOR, JA_NAP, SUM(JARAT_ORAK) SUM_ORA from #NAPI_JARSOFOR ';
  sqlstr:= sqlstr + ' group by JS_SOFOR, JA_NAP having SUM(JARAT_ORAK)>='+Tavollet_ora_limit+') a ';
  Command_run(ADOCommand1, sqlstr, True);
end;

function TTavolletiListDlg.GetNaptarLista(dat1, dat2: string): string;
// kimenet: SELECT '2016.01.01.' DATUM UNION SELECT '2016.01.02.' UNION SELECT '2016.01.03.' ....
var
  S, d: string;
begin
  S:= 'SELECT '''+dat1+''' DATUM ';
  d:= DatumhozNap(dat1, 1, True);
  while d <= dat2 do begin
     S:= S+ ' UNION SELECT '''+d+''' ';
     d:= DatumhozNap(d, 1, True);
     end;  // while
  Result:= S
end;

procedure	TTavolletiListDlg.Tolt(dat1: string);
var
  dat2, S: string;
begin
  dat2:= GetLastDay(dat1); // a h�nap utols� napja
  // Az id�szaki cimke megjelen�t�se
	QrLabel21.Caption := dat1 + ' - ' + dat2;
  FillTempTables(dat1, dat2);

  // mindazon r�szletez� sorok, akik benne vannak a #NAPI_JARSOFOR_SUM t�bl�ban (= legal�bb 4 �r�juk van aznap)
  S:= 'select DO_NAME, d.JA_NAP, d.JA_KEIDO, d.JA_JVEGE, d.JA_VEIDO, d.JARATSZAM, d.JARAT_ORAK '+
      ' from #NAPI_JARSOFOR d, #NAPI_JARSOFOR_SUM s, DOLGOZO '+
      ' where d.JS_SOFOR = s.JS_SOFOR and d.JA_NAP = s.JA_NAP and d.JS_SOFOR=DO_KOD '+
      ' order by 1,2,3 ';
  Query_run(Query2, S);
  if Query2.RecordCount=0 then begin
     vanadat:= False;
     Exit;
     end
  else begin
     vanadat:= True;
     end;

end;

procedure TTavolletiListDlg.QRBand1BeforePrint(Sender: TQRCustomBand;
	   var PrintBand: Boolean);
begin
  if AktualNEV='' then begin   // els� adatsor
    AktualNEV:= Rep.Dataset.FieldByName('DO_NAME').AsString;
    QNEV.Caption:= AktualNEV;
    end;
	QNEV.Caption 	:= Rep.Dataset.FieldByName('DO_NAME').AsString;
	QJARAT.Caption 	:= Rep.Dataset.FieldByName('JARATSZAM').AsString;
	QNAP.Caption 	:= Rep.Dataset.FieldByName('JA_NAP').AsString;
	// QIDO.Caption 	:= FormatFloat('0.00', Rep.Dataset.FieldByName('JARAT_ORAK').AsFloat);
  QIDO.Caption 	:= SzamString(Rep.Dataset.FieldByName('JARAT_ORAK').AsFloat, 5, 2, False);
	QTOLORA.Caption 	:= Rep.Dataset.FieldByName('JA_KEIDO').AsString;
	QIGORA.Caption 	:= Rep.Dataset.FieldByName('JA_VEIDO').AsString;
  NapiOrak:= NapiOrak + Rep.Dataset.FieldByName('JARAT_ORAK').AsFloat;
  // ---- hib�s adatsorok keres�se
  QTOLORA.Color := clWhite; // alaphelyzet
  if (LegutobbiNap <> '') and (LegutobbiIg <> '') then
    if (Rep.Dataset.FieldByName('JA_NAP').AsString = LegutobbiNap)
      and (Rep.Dataset.FieldByName('JA_KEIDO').AsString < LegutobbiIg) then begin
        QTOLORA.Color := clRed;
        end;
  LegutobbiNap:= Rep.Dataset.FieldByName('JA_NAP').AsString;
  LegutobbiIg:= Rep.Dataset.FieldByName('JA_VEIDO').AsString;
end;

procedure TTavolletiListDlg.QRGroup1BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  SzummaNapok:= 0;
  AktualNEV:= '';
  LegutobbiNap:= '';
  LegutobbiIg:= '';
end;

procedure TTavolletiListDlg.QRGroup2BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  NapiOrak := 0;
end;

procedure TTavolletiListDlg.QRBand2BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  OONEV.Caption:= AktualNEV;
  OOIDO.Caption:= FormatFloat('0', SzummaNapok);
end;

procedure TTavolletiListDlg.QRBand5BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  Inc(SzummaNapok);
  ONAP.Caption 	:= Rep.Dataset.FieldByName('JA_NAP').AsString;
  OIDO.Caption:= FormatFloat('0.00', NapiOrak);
end;

end.

