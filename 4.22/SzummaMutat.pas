unit SzummaMutat;

interface

uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, DBTables, DB, StdCtrls, Buttons, Mask, DBCtrls,
  Printers, ExtCtrls,  ADODB , J_FOFORMUJ, Menus, CheckLst, IniFiles;

type
  TSzummaMutatDlg = class(TForm)
	 Query1: TADOQuery;
	 Panel2: TPanel;
    Panel3: TPanel;
   procedure FormCanResize(Sender: TObject; var NewWidth,
        NewHeight: Integer; var Resize: Boolean);
   procedure Tolt(Szoveg: string);
    procedure FormShow(Sender: TObject);
    procedure FormClick(Sender: TObject);
    procedure Panel3Click(Sender: TObject);
	 private
		foselected		: boolean;
       dialnr			: integer;
       mezonev			: string;
       dialpos			: TRect;
       ekeres: string;
  public
		kodkilist   	: TStringList;
		voltenter		: boolean;
		mezo_sorsz		: integer;
		kelluresszures 	: boolean;
       dial_left		: integer;
       dial_top		: integer;
  end;

var
  SzummaMutatDlg: TSzummaMutatDlg;

implementation

uses
	Egyeb, Kozos, J_SQL;
{$R *.DFM}

// �j st�lus: a t�bl�zatb�l vett elemlist�t dobja fel
procedure TSzummaMutatDlg.FormClick(Sender: TObject);
begin
  Close;
end;

procedure TSzummaMutatDlg.FormShow(Sender: TObject);
begin
 	Self.Top:= dial_top;
 	Self.Left:= dial_left;
end;

procedure TSzummaMutatDlg.Panel3Click(Sender: TObject);
begin
  Close;
end;

procedure TSzummaMutatDlg.Tolt(Szoveg: string);
begin
  Panel3.Caption:= Szoveg;
end;

procedure TSzummaMutatDlg.FormCanResize(Sender: TObject; var NewWidth,
  NewHeight: Integer; var Resize: Boolean);
begin
	if NewWidth < 200 then begin
   	Resize	:= false;
   end;
end;

end.


