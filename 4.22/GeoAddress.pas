unit GeoAddress;

interface

uses
  SysUtils, Variants, Classes;

type
  TGeoAddress = class
    private
      FFullAddress: String;
      FCityOnly: String;
      FStreetNumber: String;
      FTimeStamp1: string;
      FTimeStamp2: string;
      FFuvarosEvent: string;  // felrak�s vagy lerak�s
      FOrderNumber: Integer;  // for setting correct address order

    public
      property FullAddress : string read FFullAddress write FFullAddress;
      property CityOnly : string read FCityOnly write FCityOnly;
      property StreetNumber : string read FStreetNumber write FStreetNumber;
      property TimeStamp1 : string read FTimeStamp1 write FTimeStamp1;
      property TimeStamp2 : string read FTimeStamp2 write FTimeStamp2;
      property FuvarosEvent : string read FFuvarosEvent write FFuvarosEvent;
      property OrderNumber : integer read FOrderNumber write FOrderNumber;

      // Constructor
      constructor Create(const FullAddress: String;
                         const CityOnly: String;
                         const StreetNumber: String;
                         const TimeStamp1: string;
                         const TimeStamp2: string;
                         const FuvarosEvent: string;
                         const OrderNumber : Integer);

   end;  // class
implementation

// Address constructor
// --------------------------------------------------------------------------
constructor TGeoAddress.Create(const FullAddress: String;
                           const CityOnly: String;
                           const StreetNumber: String;
                           const TimeStamp1: string;
                           const TimeStamp2: string;
                           const FuvarosEvent: string;
                           const OrderNumber : Integer);
begin
  // Save the passed parameters
  self.FFullAddress:= FullAddress;
  self.FCityOnly:= CityOnly;
  self.FStreetNumber:= StreetNumber;
  self.FTimeStamp1:= TimeStamp1;
  self.FTimeStamp2:= TimeStamp2;
  self.FFuvarosEvent:= FuvarosEvent;
  self.FOrderNumber:= OrderNumber;
end;

end.

