﻿unit Edifact_Export;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Buttons, Vcl.Mask,
  Vcl.ExtCtrls, Data.DB, Data.Win.ADODB, Vcl.CheckLst, Vcl.Grids, IniFiles, J_Datamodule;

type
  TEDIFACT_ExportDlg = class(TForm)
    Panel1: TPanel;
    BitElkuld: TBitBtn;
    BitKilep: TBitBtn;
    Memo1: TMemo;
    Query1: TADOQuery;
    Query2: TADOQuery;
    Query3: TADOQuery;
    Q1: TADOQuery;
    Query4: TADOQuery;
    M10: TMaskEdit;
    Q2: TADOQuery;
    Label1: TLabel;
    CH1: TCheckBox;
    chkTest: TCheckBox;
    Label2: TLabel;
    M11: TMaskEdit;
    procedure FormCreate(Sender: TObject);
    procedure BitKilepClick(Sender: TObject);
    procedure BitElkuldClick(Sender: TObject);
    procedure FormCanResize(Sender: TObject; var NewWidth, NewHeight: Integer;
      var Resize: Boolean);
    // procedure FtpKuldes(fn : string);
    procedure KuldEgyet(const Szamlaszam: string; const SAUJKOD: integer = 0);
    function GetMessageReference: string;
    function ConvertSzam(str: string): string;

  private
       automata    : boolean;
       use_ftp     : boolean;
       mess_ref    : string;   // Message Reference Number +  Interchange control reference  (one message per interchange)
       tetelszam   : integer;
       EDIFACTDIR, SZALLITAS_TERKOD, UZAPOTLEK_TERKOD, SENDER_ID, RECIPIENT_ID, PLANT_NUMBER: string;
       FTPHOST, FTPDIR, FTPUSER, FTPPASSWORD: string;
       FTPPORT: integer;
  public
     function FileIro(const ASzamlaSzam, AKonyvtar: string; memoInfo: TMemo; const LocalLogFile: string; var NewEDIFileName: string;
        const ASAUJKOD: integer = 0): string;
  end;

var
  EDIFACT_ExportDlg: TEDIFACT_ExportDlg;

implementation

uses J_SQL, Kozos, Egyeb, Edifact_Invoic;

{$R *.dfm}

procedure TEDIFACT_ExportDlg.FormCreate(Sender: TObject);
var
   Inifn	    : TIniFile;
   megbstr     : string;
   fn          : string;
   i           : integer;
begin
   // A kezdő értékek feltöltése
   automata    := false;
   CAPTION := 'FAP Export';
   EgyebDlg.SetAdoQueryDatabase(Query1);
   EgyebDlg.SetAdoQueryDatabase(Query2);
   EgyebDlg.SetAdoQueryDatabase(Query3);
   EgyebDlg.SetAdoQueryDatabase(Query4);
   EgyebDlg.SetAdoQueryDatabase(Q1);
   EgyebDlg.SetAdoQueryDatabase(Q2);
   Application.DefaultFont.Name    := 'Arial';
   Application.DefaultFont.Size    := 10;



(*
   v_JIni  := TJIni.Create(self);
   fn      := ExtractFilePath(Application.ExeName) + ChangeFileExt( ExtractFileName(Application.ExeName), '.INI' );
   v_Jini.SetIni( fn);
   if v_Jini.OpenIni then begin
		megbstr     := v_Jini.ReadIni('GENERAL', 'MEGBIZOK', '0547');
		felrakostr  := v_Jini.ReadIni('GENERAL', 'FELRAKOK', '');

       ftphost     := v_Jini.ReadIni('FTP', 'FTPHOST', '');
       ftpport     := v_Jini.ReadIni('FTP', 'FTPPORT', '');
       ftpuser     := v_Jini.ReadIni('FTP', 'FTPUSER', '');
       ftppswd     := v_Jini.ReadIni('FTP', 'FTPPSWD', '');
       ftpdir      := v_Jini.ReadIni('FTP', 'FTPDIR', '');

       use_ftp     := StrToIntDef(v_Jini.ReadIni('GENERAL', 'USEFTP', '0'), 0) > 0;

       v_Jini.CloseIni;
   end else begin
       megbstr := '0547';
   end;
*)

   // A FAP mentés helyének meghatározása
   EDIFACTdir		:= EgyebDlg.Read_SZGrid('480', 'SANMINA_EDI_MAPPA');
   if EDIFACTdir = '' then begin
       // NoticeKi('Figyelem! A SANMINA EDI export helye még nincs megadva (szótár "480", "SANMINA_EDI_MAPPA") , ezért az exportálás az exe könyvtárába történik! ');
       // Bejegyezzük a szótárba
       Query_Run(Query3, 'DELETE FROM SZOTAR WHERE SZ_FOKOD = ''480'' AND SZ_ALKOD = ''SANMINA_EDI_MAPPA'' ', FALSE);
       Query_Run(Query3, 'INSERT INTO SZOTAR ( SZ_FOKOD, SZ_ALKOD, SZ_MENEV, SZ_LEIRO, SZ_ERVNY ) VALUES ( ''480'', ''SANMINA_EDI_MAPPA'', '''', ''A SANMINA EDI export helye'',  1 ) ', FALSE);
       EDIFACTdir  := EgyebDlg.TempPath;
   end;
   EDIFACTdir		:= IncludeTrailingPathDelimiter(EDIFACTdir);
   SZALLITAS_TERKOD := EgyebDlg.Read_SZGrid('480', 'SZALLITAS_TERKOD');
   UZAPOTLEK_TERKOD := EgyebDlg.Read_SZGrid('480', 'UZAPOTLEK_TERKOD');
   SENDER_ID := EgyebDlg.Read_SZGrid('480', 'SENDER_ID');
   RECIPIENT_ID := EgyebDlg.Read_SZGrid('480', 'RECIPIENT_ID');
   PLANT_NUMBER := EgyebDlg.Read_SZGrid('480', 'PLANT_NUMBER');

   FTPHOST := EgyebDlg.Read_SZGrid('480', 'FTPHOST');
   FTPPORT := StrToIntDef(EgyebDlg.Read_SZGrid('480', 'FTPPORT'), 0);
   FTPDIR := EgyebDlg.Read_SZGrid('480', 'FTPDIR');
   FTPUSER := DecodeString(EgyebDlg.Read_SZGrid('480', 'FTPUSER'));
   FTPPASSWORD := DecodeString(EgyebDlg.Read_SZGrid('480', 'FTPPASSWORD'));

end;

{procedure TEDIFACT_ExportDlg.BitElkuldClick(Sender: TObject);
var
   str, S, NewEDIFileName: string;
begin
   str    := M10.Text;
   if str = '' then begin
      NoticeKi('Számlaszám megadása kötelező!');
      Exit;
      end;

   S:= FileIro(M10.Text, EDIFACTdir, Memo1, '', NewEDIFileName);
   if S='' then begin
      Memo1.Lines.Add('Sikeres export ('+NewEDIFileName+')!');
      if CH1.Checked then begin
       EgyebDlg.FTP_Upload(NewEDIFileName, FTPHOST, FTPUSER, FTPPASSWORD, FTPDIR, FTPPORT);
       Memo1.Lines.Add('Sikeres FTP feltöltés!');
       end;
      end
   else begin
      Memo1.Lines.Add(S);
      end;
end;
}

procedure TEDIFACT_ExportDlg.BitElkuldClick(Sender: TObject);
var
   str, S, NewEDIFileName, Szamlaszam, ujkod: string;
   i: integer;
begin
   str:= M10.Text;
   ujkod:= M11.Text;
   if str = '' then begin
      NoticeKi('Számlaszám megadása kötelező!');
      Exit;
      end;
   for i:=1 to EgyebDlg.SepListLength(',', str) do begin
        Szamlaszam:= EgyebDlg.GetSepListItem(',', str, i);
        Memo1.Lines.Add('Feldolgozás: '+Szamlaszam);
        if ujkod <> '' then
          KuldEgyet (Szamlaszam, StrToInt(ujkod))
        else KuldEgyet (Szamlaszam);
        end;  // for
end;

procedure TEDIFACT_ExportDlg.KuldEgyet(const Szamlaszam: string; const SAUJKOD: integer = 0);
var
   str, S, NewEDIFileName: string;
begin
   S:= FileIro(Szamlaszam, EDIFACTdir, Memo1, '', NewEDIFileName, SAUJKOD);
   if S='' then begin
      Memo1.Lines.Add('Sikeres export: '+Szamlaszam+' ('+NewEDIFileName+')!');
      if CH1.Checked then begin
       EgyebDlg.FTP_Upload(NewEDIFileName, FTPHOST, FTPUSER, FTPPASSWORD, FTPDIR, FTPPORT);
       Memo1.Lines.Add('Sikeres FTP feltöltés!');
       end;
      end
   else begin
      Memo1.Lines.Add(S);
      end;
end;


procedure TEDIFACT_ExportDlg.BitKilepClick(Sender: TObject);
begin
   Close;
end;

procedure TEDIFACT_ExportDlg.FormCanResize(Sender: TObject; var NewWidth,
  NewHeight: Integer; var Resize: Boolean);
begin
   if ( ( NewWidth < 700 ) or (NewHeight < 400) ) then begin
       Resize := false;
   end;
end;



function TEDIFACT_ExportDlg.FileIro(const ASzamlaSzam, AKonyvtar: string; memoInfo: TMemo; const LocalLogFile: string; var NewEDIFileName: string;
        const ASAUJKOD: integer = 0): string;
var
  file1 : TextFile;
  i, memossz, felrakoszam, ii, seged1, sorszam, paletta: Integer;
  folytat, tobbloadid, kellship: Boolean;
  fn, sor, str, megb, sqlstr, mbkod, rendszam, gkkat, EDIgkkat: string;
  loadid, pozicio, szamladatum, saujkod, cegnev, cim, varos, irsz, orszag, S: string;
  sajatadosz, szadoszam, felrakodat, lerakodat, leiras, loadidstr, fuvaroskod: string;
  netto, ossznetto, afa, osszafa, suly : double;
  JSzamlaSor	: TJSzamlaSor;
  SzamlaTetel: TSzamlaTetel;
  EdifactInvoic: TEdifactInvoic;

  function FileNyito(fn1 : string) : boolean;
    begin
      {$I-}
      AssignFile(file1, fn1);
      Rewrite(file1);
      {$I+}
      if IOResult <> 0 then Result:= False
      else Result := true;
    end;

  procedure InfoKiiras(S: string);
   begin
    if memoInfo <> nil then  memoInfo.Lines.Add(S);
   end;

begin
  Result := '';
  if ASAUJKOD=0 then begin  // ha nincs megadva SA_UJKOD (teszteléshez, kézi generáláshoz használhatjuk)
    Query_Run(Query1, 'select COUNT(*) from SZFEJ where SA_KOD = ''' + ASzamlaSzam + ''' ');
    if Query1.Fields[0].AsInteger > 1 then begin
        // Raise Exception.Create('Gyűjtőszámlára nem kérhető EDI export!');
        Result:= 'Figyelmeztetés: Gyűjtőszámlára nem kérhető EDI export!';
        Exit;
        end;
    end;
  EdifactInvoic:= TEdifactInvoic.Create;
  try
       EdifactInvoic.FKuldoID:= SENDER_ID;
       EdifactInvoic.FFogadoID:= RECIPIENT_ID;
       EdifactInvoic.FPlantNumber:= PLANT_NUMBER;
      // A számlaszám alapján megkeressük a megbízó kódot
       S:='SELECT MB_MBKOD FROM MEGBIZAS, VISZONY, SZFEJ WHERE MB_VIKOD = VI_VIKOD AND VI_UJKOD = SA_UJKOD AND SA_KOD = ''' + ASzamlaSzam + ''' ';
       if ASAUJKOD<>0 then begin
          S:= S+' and SA_UJKOD='+IntToStr(ASAUJKOD);
          end;
       Query_Run(Query1, S);
       megb:= Query1.FieldByName('MB_MBKOD').AsString;

       Query_Run(Query1, 'select MB_MBKOD, MB_VEKOD, MB_POZIC, MB_LECEG, MB_EDIJ, MB_FORSZ, MB_LORSZ, MB_FERAK, SA_KOD, JA_ORSZ, JA_ALKOD, MB_JASZA, MB_FUDIJ, MB_FUNEM, VI_HONNAN, '+
           ' VI_HOVA, SA_KOD, SA_UJKOD, SA_KIDAT, SA_VEVONEV, SA_VECIM, SA_VEVAROS, SA_VEIRSZ, SA_VEVOKOD, SA_EUADO, MB_FUTID, MB_RENSZ, MB_GKKAT '+
           ' from szfej, viszony, megbizas, jarat '+
           ' where vi_ujkod = sa_ujkod and vi_jakod = ja_kod and vi_vikod = mb_vikod and ja_kod = mb_jakod and MB_MBKOD = ''' + megb + ''' ');
       if Query1.RecordCount < 1 then begin
           // Raise Exception.Create('Nem létező megbízás + járat + viszony + szfej kapcsolat!');
           result:= 'Hiba: Nem létező megbízás + járat + viszony + szfej kapcsolat!';
           Exit;
       end;
       if Query1.RecordCount > 1 then begin
           // Raise Exception.Create('Túl sok megbízás + járat + viszony + szfej kapcsolat!');
           result:= 'Hiba: Túl sok megbízás + járat + viszony + szfej kapcsolat!';
           Exit;
       end;

       InfoKiiras('Exporting...');
       mbkod := IntToStr(StrToIntDef(Query1.FieldByName('MB_MBKOD').AsString, 0));
       rendszam:= Query1.FieldByName('MB_RENSZ').AsString;
       gkkat:= Query1.FieldByName('MB_GKKAT').AsString;
       if LocalLogFile <> '' then KozosWriteLogFile(LocalLogFile, '  Számla: '+ASzamlaSzam+', megbízás: '+mbkod+', rendszám: '+rendszam);
       //loadid      := '';
       kellship    := false;
       Query_Run(Query2, 'SELECT MT_LOADID, MT_SORSZ, MT_FSULY, MT_POZIC, MT_FEPAL FROM MEGTETEL WHERE MT_MBKOD = ' + mbkod+' ORDER BY MT_SORSZ');
       if Query2.RecordCount < 1 then begin
           if Query1.FieldByName('MB_POZIC').AsString = 'Non SAP' then begin
               loadid  := 'NON SAP';
           end else begin
               if Query1.FieldByName('MB_POZIC').AsString <> '' then begin
                   loadid      := Query1.FieldByName('MB_POZIC').AsString;
                   kellship    := true;
                  end
               // else begin
               //    Raise Exception.Create('Nem létező load ID!');
               //    Exit;
               //    end;
           end;
       end;
       if loadid = '' then begin
           loadid      := Query2.FieldByName('MT_LOADID').AsString;
       end;
       tobbloadid  := false;
       loadidstr   := '';
       if Query2.RecordCount > 1 then begin
           tobbloadid  := true;
           // A többi loadid összegyűjtése
           Query2.Next;
           while not Query2.Eof do begin
               loadidstr   := loadidstr + Query2.FieldByName('MT_LOADID').AsString + ',';
               Query2.Next;
           end;
           Query2.First;
       end;

       // Köbös súly meghatározása
       Query_Run(Query3, 'SELECT MS_FEKOB FROM MEGSEGED WHERE MS_MBKOD = ' + mbkod+ ' AND MS_SORSZ = '+IntToSTr(StrToIntDef( Query2.FieldByName('MT_SORSZ').AsString, 0 )));
       if Query3.RecordCount > 0  then begin
           suly     := Query3.FieldByName('MS_FEKOB').AsFloat;
       end;
       if suly = 0 then begin
           suly     := Query2.FieldByName('MT_FSULY').AsFloat;
       end;
       if tobbloadid  then begin
           Query_Run(Query3, 'SELECT SUM(MS_FEKOB) AS OSSZSULY FROM MEGSEGED WHERE MS_MBKOD = ' + mbkod );
           suly     := Query3.FieldByName('OSSZSULY').AsFloat;
           if suly = 0 then begin
               Query_Run(Query3, 'SELECT SUM(MT_FSULY) AS OSSZSULY FROM MEGTETEL WHERE MT_MBKOD = '+mbkod);
               suly     := Query3.FieldByName('OSSZSULY').AsFloat;
           end;
       end;
       pozicio  := Query2.FieldByName('MT_POZIC').AsString;
       paletta  := StrToIntDef(Query2.FieldByName('MT_FEPAL').AsString,0);
       // Ha a paletta vagy a súly 0 akkor összegezzük a megsegéd -ből a felrakó adatokat.
       if ( (paletta = 0) or (suly = 0) ) then begin
           Query_Run(Query3, 'SELECT SUM(MS_FEPAL) AS OSSZPAL,  SUM(MS_FSULY) AS OSSZSULY FROM MEGSEGED WHERE MS_MBKOD = ' + mbkod );
           paletta  := StrToIntDef(Query3.FieldByName('OSSZPAL').AsString,0);
           suly     := Query3.FieldByName('OSSZSULY').AsFloat;
       end;

       // InfoKiiras('Paletta - súly : ' + Format('%d db  -  %.2f kg', [paletta, suly]));
       // InfoKiiras('A loadid       : ' + loadid);
       // InfoKiiras('A pozíció      : ' + pozicio);
       saujkod      := Query1.FieldByName('SA_UJKOD').AsString;
       szamladatum  := Query1.FieldByName('SA_KIDAT').AsString;
       // InfoKiiras('A számlaszám   : ' + ASzamlaSzam);
       Query_Run(Query3, 'SELECT S2_S2KOD FROM SZSOR2 WHERE S2_UJKOD = '+ saujkod);
       if Query3.RecordCount > 0 then begin
           // Van hozzá összesített számla
           Query_Run(Query3, 'SELECT SA_KIDAT FROM SZFEJ2 WHERE SA_UJKOD = ' + Query3.FieldByName('S2_S2KOD').AsString);
           szamladatum := Query3.FieldByName('SA_KIDAT').AsString;
           // InfoKiiras('      összesített számla' );
       end;
       // InfoKiiras('A kiáll. dátuma: ' + szamladatum);
       EdifactInvoic.FSzamladatum := copy(szamladatum, 1, 4) + copy(szamladatum, 6, 2) + copy(szamladatum, 9, 2);

       // A számla kibocsátó feltöltése
       EdifactInvoic.FSzamlaKibocsato.Kod:= EgyebDlg.Read_SZGrid('999', 'EDI_ADOSZAM');
       EdifactInvoic.FSzamlaKibocsato.nev := EgyebDlg.Read_SZGrid('999', 'EDI_CEG');
       EdifactInvoic.FSzamlaKibocsato.cim := EgyebDlg.Read_SZGrid('999', 'EDI_CIM');
       EdifactInvoic.FSzamlaKibocsato.varos := EgyebDlg.Read_SZGrid('999', 'EDI_VAROS');
       EdifactInvoic.FSzamlaKibocsato.irsz := EgyebDlg.Read_SZGrid('999', 'EDI_IRSZ');
       EdifactInvoic.FSzamlaKibocsato.orszag := EgyebDlg.Read_SZGrid('999', 'EDI_ORSZAG');

       EdifactInvoic.FReferenciak.sajatAdoszam   := EgyebDlg.Read_SZGrid('999', 'EDI_ADOSZAM');
       // A számla címzettje feltöltése

       EdifactInvoic.FVevo.nev := Nevbol_extrainfo_levesz(Query1.FieldByName('SA_VEVONEV').AsString);  // két azonos nevű TE Connectivity cég miatt
       cim:= Query1.FieldByName('SA_VECIM').AsString;
       if copy(cim, Length(cim), 1) = '.' then begin
          cim := copy(cim, 1, Length(cim)- 1);
          end;
       EdifactInvoic.FVevo.cim := cim;
       EdifactInvoic.FVevo.varos := Query1.FieldByName('SA_VEVAROS').AsString;
       EdifactInvoic.FVevo.irsz := Query1.FieldByName('SA_VEIRSZ').AsString;
       EdifactInvoic.FVevo.orszag := Query_Select('VEVO', 'V_KOD', Query1.FieldByName('SA_VEVOKOD').AsString, 'V_ORSZ');
       EdifactInvoic.FVevo.kod := Query1.FieldByName('SA_VEVOKOD').AsString;

       // Az adószám összerakása
       EdifactInvoic.FReferenciak.vevoAdoszam := Query_Select('VEVO', 'V_KOD', Query1.FieldByName('SA_VEVOKOD').AsString, 'VE_AORSZ')+Query1.FieldByName('SA_EUADO').AsString;

       // Az első felrakás meghatározása
       Query_Run(Query4, 'SELECT MS_FETDAT, MS_TEFNEV, MS_TEFCIM, MS_TEFTEL, MS_TEFIR, MS_TEFOR FROM MEGSEGED where ms_mbkod = '+mbkod+' AND MS_TIPUS = 0 ORDER BY MS_FETDAT, MS_FETIDO');
       felrakodat   := Query4.FieldByName('MS_FETDAT').AsString;
       felrakodat   := copy(felrakodat, 1, 4) + copy(felrakodat, 6, 2) + copy(felrakodat, 9, 2);
       if felrakodat = '' then begin
           // Raise Exception.Create('Hiányzó felrakás dátuma!');
           result:= 'Hiba: Hiányzó felrakás dátuma!';
           Exit;
           end;
       EdifactInvoic.FFelrakoDatum:= felrakodat;
       // Az utolsó lerakás meghatározása
       Query_Run(Query2, 'SELECT MS_LETDAT, MS_TENNEV, MS_TENCIM, MS_TENTEL, MS_TENIR, MS_TENOR FROM MEGSEGED where ms_mbkod = '+mbkod+' AND MS_TIPUS = 1 ORDER BY MS_LETDAT, MS_LETIDO');
       Query2.Last;
       lerakodat    := Query2.FieldByName('MS_LETDAT').AsString;
       lerakodat    := copy(lerakodat, 1, 4) + copy(lerakodat, 6, 2) + copy(lerakodat, 9, 2);
       if lerakodat = '' then begin
           // Raise Exception.Create('Hiányzó lerakás dátuma!');
           result:= 'Hiba: Hiányzó lerakás dátuma!';
           Exit;
           end;
       EdifactInvoic.FLerakoDatum:= lerakodat;
       // A felrakó adatai
       EdifactInvoic.FFelrako.nev := Nevbol_extrainfo_levesz(Query4.FieldByName('MS_TEFNEV').AsString);  // két azonos nevű TE Connectivity cég miatt
       EdifactInvoic.FFelrako.cim := Query4.FieldByName('MS_TEFCIM').AsString;
       EdifactInvoic.FFelrako.varos := Query4.FieldByName('MS_TEFTEL').AsString;
       EdifactInvoic.FFelrako.irsz := Query4.FieldByName('MS_TEFIR').AsString;
       EdifactInvoic.FFelrako.orszag := Query4.FieldByName('MS_TEFOR').AsString;
       EdifactInvoic.FFelrako.kod:= GetFelrakoKod(EdifactInvoic.FFelrako.nev, EdifactInvoic.FFelrako.orszag, EdifactInvoic.FFelrako.irsz,
              EdifactInvoic.FFelrako.varos, EdifactInvoic.FFelrako.cim);
       // A lerakó adatai
       EdifactInvoic.FLerako.nev := Nevbol_extrainfo_levesz(Query2.FieldByName('MS_TENNEV').AsString);  // két azonos nevű TE Connectivity cég miatt
       EdifactInvoic.FLerako.cim := Query2.FieldByName('MS_TENCIM').AsString;
       EdifactInvoic.FLerako.varos := Query2.FieldByName('MS_TENTEL').AsString;
       EdifactInvoic.FLerako.irsz := Query2.FieldByName('MS_TENIR').AsString;
       EdifactInvoic.FLerako.orszag := Query2.FieldByName('MS_TENOR').AsString;
       EdifactInvoic.FLerako.kod:= GetFelrakoKod(EdifactInvoic.FLerako.nev, EdifactInvoic.FLerako.orszag, EdifactInvoic.FLerako.irsz,
              EdifactInvoic.FLerako.varos, EdifactInvoic.FLerako.cim);

       EdifactInvoic.FSzamlaDeviza:= 'EUR';
       EdifactInvoic.FRakomanyAdatok.suly:= suly;
       EdifactInvoic.FRakomanyAdatok.paletta:= paletta;

       EdifactInvoic.FGepkocsiAdatok.rendszam:= rendszam;
       EDIgkkat:= gkkat;  // ha nem illeszkedik egyik mintára sem, akkor a "Fuvaros kódot" adjuk át
       if copy(gkkat,1,2)='24' then EDIgkkat:= '24T';
       if copy(gkkat,1,3)='1,5' then EDIgkkat:= '1.5T';
       if copy(gkkat,1,3)='3,5' then EDIgkkat:= '3.5T';
       EdifactInvoic.FGepkocsiAdatok.kategoria:= EDIgkkat;
       EdifactInvoic.FReferenciak.pozicio:= pozicio;
       // EdifactInvoic.FReferenciak.loadid:= loadid;
       EdifactInvoic.FReferenciak.szamlaszam:= ASzamlaSzam;
       // EdifactInvoic.FReferenciak.lanid:= Query_Select('FUVARTABLA', 'FT_FTKOD', Query1.FieldByName('MB_FUTID').AsString, 'FT_LANID');

       // A tételek rögzítése
       ossznetto   := 0;
       osszafa     := 0;
       // A számla sor adatok kiírása
       JSzamla := TJSzamla.Create(Self);
       JSzamla.FillSzamla(saujkod);
       for i := 0 to JSzamla.Szamlasorok.Count-1 do begin
           JSzamlaSor  := (JSzamla.Szamlasorok[i] as TJSzamlaSor);
           if JSzamlaSor.sorneteur > 0 then begin
             leiras := JSzamlaSor.termeknev;
             fuvaroskod := JSzamlaSor.termekkod;
             SzamlaTetel.leiras :=  leiras;
             if Pos (fuvaroskod, SZALLITAS_TERKOD) > 0 then begin
                 SzamlaTetel.leiras := 'Road transportation';
                 SzamlaTetel.termekkod := '400';  // Freight
                 end;
             if Pos (fuvaroskod, UZAPOTLEK_TERKOD) > 0 then begin
                 SzamlaTetel.leiras  := 'Fuel surcharge';
                 SzamlaTetel.termekkod := '405';  // Fuel surcharge
             end;
             SzamlaTetel.netto   := JSzamlaSor.sorneteur;
             SzamlaTetel.afa     := JSzamlasor.sorafaeur;
             SetLength(EdifactInvoic.FSzamlaTetelek, Length(EdifactInvoic.FSzamlaTetelek)+1);
             EdifactInvoic.FSzamlaTetelek[Length(EdifactInvoic.FSzamlaTetelek)-1]:= SzamlaTetel;
             ossznetto   := ossznetto + SzamlaTetel.netto;
             osszafa     := osszafa   + SzamlaTetel.afa;
             end;  // if >0
       end; // for
       // Az összesített sorok megjelenítése
       EdifactInvoic.FOsszNetto := ossznetto;
       EdifactInvoic.FOsszAfa := osszafa;
       EdifactInvoic.FControlReference:= GetMessageReference;
       EdifactInvoic.FMessageReference:=  EdifactInvoic.FControlReference;  // one message / interchange

       // NewEDIFileName:= AKonyvtar+EdifactInvoic.FControlReference+'.edi';
       NewEDIFileName:= AKonyvtar+'EDI'+FormatDateTime('YYYYMMDD', Now)+FormatDateTime('HHmmsszz', Now)+'.TXT';
       if not FileNyito(NewEDIFileName) then begin
         Raise Exception.Create('Hiba: Sikertelen file megnyitás: '+NewEDIFileName);
         end;
       Write(file1, EdifactInvoic.getDataOutput(chkTest.Checked));
       CloseFile(file1);
       if EdifactInvoic <> nil then EdifactInvoic.Free;
       Result := '';  // minden rendben volt
       // sleep(1000);  // az állomány nevében másodperc szerepel, ne csússzanak egymásba
  except
     on E: exception do begin
        Result:= E.Message;
        if EdifactInvoic <> nil then EdifactInvoic.Free;
        end;  // on E
     end;  // try-except
end;

function TEDIFACT_ExportDlg.ConvertSzam(str: string): string;
begin
   Result := StringReplace(str,',','.', [rfReplaceAll]);;
end;

function TEDIFACT_ExportDlg.GetMessageReference: string;
begin
   Query_Run(Query4, 'SELECT SZ_MENEV FROM SZOTAR WHERE SZ_FOKOD = ''480'' AND SZ_ALKOD = ''LAST_REFERENCE'' ');
   if Query4.RecordCount = 0 then begin
       Query_Run(Query4, 'INSERT INTO SZOTAR (SZ_FOKOD, SZ_ALKOD, SZ_MENEV ) VALUES ( ''480'', ''LAST_REFERENCE'', ''1'' )');
       result := '000001';
       end
   else begin
       result := Format('%6.6d', [StrToIntDef(Query4.FieldByName('SZ_MENEV').AsString, 0)+1]);
       end;
   Query_Run(Query4, 'UPDATE SZOTAR SET SZ_MENEV = '''+result+''' WHERE SZ_FOKOD = ''480'' AND SZ_ALKOD = ''LAST_REFERENCE'' ');

end;

end.
