unit Keszlet;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ADODB, ComCtrls, ExtCtrls, DBCtrls, Grids, DBGrids,
  StdCtrls, Mask;

type
  TFKeszlet = class(TForm)
    Panel1: TPanel;
    TabControl1: TTabControl;
    TKMOZGAS: TADODataSet;
    TKESZLET: TADOQuery;
    TKMOZGASKM_ID: TAutoIncField;
    TKMOZGASKM_RAKKOD: TStringField;
    TKMOZGASKM_RAKNEV: TStringField;
    TKMOZGASKM_TKOD: TStringField;
    TKMOZGASKM_TNEV: TStringField;
    TKMOZGASKM_ME: TStringField;
    TKMOZGASKM_GKRESZ: TStringField;
    TKMOZGASKM_GKKAT: TStringField;
    TKMOZGASKM_GKEUROB: TIntegerField;
    TKMOZGASKM_KMORA: TIntegerField;
    TKMOZGASKM_MENNY: TBCDField;
    TKMOZGASKM_EMENNY: TBCDField;
    TKMOZGASKM_KIBE: TStringField;
    TKMOZGASKM_MNEM: TStringField;
    TKMOZGASKM_MEGJ: TStringField;
    TKMOZGASKM_MODDAT: TDateTimeField;
    TKMOZGASKM_USER: TStringField;
    DBGrid1: TDBGrid;
    DBNavigator1: TDBNavigator;
    DSKMozgas: TDataSource;
    DBEdit1: TDBEdit;
    TTermek: TADOQuery;
    TGEPKOCSI: TADOQuery;
    TSZOTAR: TADOQuery;
    TSZOTARSZ_FOKOD: TStringField;
    TSZOTARSZ_ALKOD: TStringField;
    TSZOTARSZ_MENEV: TStringField;
    TSZOTARSZ_LEIRO: TStringField;
    TSZOTARSZ_KODHO: TBCDField;
    TSZOTARSZ_NEVHO: TBCDField;
    TSZOTARSZ_ERVNY: TIntegerField;
    TTermekT_TERKOD: TStringField;
    TTermekT_TERNEV: TStringField;
    TTermekT_LEIRAS: TStringField;
    TTermekT_CIKKSZAM: TStringField;
    TTermekT_ITJSZJ: TStringField;
    TTermekT_AFA: TStringField;
    TTermekT_EGYAR: TBCDField;
    TTermekT_MEGJ: TStringField;
    TTermekT_ME: TStringField;
    TTermekT_KKEZ: TIntegerField;
    TTermekT_KESZLET: TBCDField;
    TGEPKOCSIGK_KOD: TStringField;
    TGEPKOCSIGK_RESZ: TStringField;
    TGEPKOCSIGK_TIPUS: TStringField;
    TGEPKOCSIGK_MEGJ: TStringField;
    TGEPKOCSIGK_NORMA: TBCDField;
    TGEPKOCSIGK_DIJ1: TBCDField;
    TGEPKOCSIGK_DIJ2: TBCDField;
    TGEPKOCSIGK_SULYF: TIntegerField;
    TGEPKOCSIGK_KLIMA: TIntegerField;
    TGEPKOCSIGK_SULYL: TBCDField;
    TGEPKOCSIGK_APETE: TBCDField;
    TGEPKOCSIGK_APENY: TBCDField;
    TGEPKOCSIGK_NORTE: TBCDField;
    TGEPKOCSIGK_NORNY: TBCDField;
    TGEPKOCSIGK_ALVAZ: TStringField;
    TGEPKOCSIGK_MOTOR: TStringField;
    TGEPKOCSIGK_EVJAR: TStringField;
    TGEPKOCSIGK_FORHE: TStringField;
    TGEPKOCSIGK_FEHER: TStringField;
    TGEPKOCSIGK_RAKSU: TBCDField;
    TGEPKOCSIGK_TERAK: TBCDField;
    TGEPKOCSIGK_RAME1: TBCDField;
    TGEPKOCSIGK_RAME2: TBCDField;
    TGEPKOCSIGK_RAME3: TBCDField;
    TGEPKOCSIGK_EUPAL: TBCDField;
    TGEPKOCSIGK_KULSO: TIntegerField;
    TGEPKOCSIGK_SULYA: TBCDField;
    TGEPKOCSIGK_OSULY: TBCDField;
    TGEPKOCSIGK_POTOS: TIntegerField;
    TGEPKOCSIGK_POKOD: TStringField;
    TGEPKOCSIGK_NEMZE: TIntegerField;
    TGEPKOCSIGK_GARKM: TBCDField;
    TGEPKOCSIGK_GARFI: TIntegerField;
    TGEPKOCSIGK_GARNO: TStringField;
    TGEPKOCSIGK_ARHIV: TIntegerField;
    TGEPKOCSIGK_GKKAT: TStringField;
    TGEPKOCSIGK_FAJTA: TStringField;
    TGEPKOCSIGK_SONEV: TStringField;
    TGEPKOCSIGK_HOZZA: TIntegerField;
    TGEPKOCSIGK_LIVAN: TIntegerField;
    TGEPKOCSIGK_LICEG: TStringField;
    TGEPKOCSIGK_LIDAT: TStringField;
    TGEPKOCSIGK_LIHON: TIntegerField;
    TGEPKOCSIGK_LISZE: TStringField;
    TGEPKOCSIGK_LIOSZ: TBCDField;
    TGEPKOCSIGK_LINEM: TStringField;
    TGEPKOCSIGK_LIMEG: TStringField;
    TGEPKOCSIGK_TANKL: TBCDField;
    TGEPKOCSIGK_TARTL: TBCDField;
    TGEPKOCSIGK_RESZR: TIntegerField;
    TGEPKOCSIGK_KIVON: TIntegerField;
    TGEPKOCSIGK_VETAR: TBCDField;
    TGEPKOCSIGK_VETVN: TStringField;
    TGEPKOCSIGK_EUROB: TIntegerField;
    TGEPKOCSIGK_SFORH: TStringField;
    TGEPKOCSIGK_SOBEL: TStringField;
    TGEPKOCSIGK_UT_DA: TStringField;
    TGEPKOCSIGK_UT_KM: TBCDField;
    TGEPKOCSIGK_FORKI: TStringField;
    TKMOZGASKM_DATUM: TDateTimeField;
    procedure TabControl1Change(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure TKMOZGASAfterInsert(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FKeszlet: TFKeszlet;
  _rkod, _rnev, _mkod, _mnev: TStringList;

implementation

uses Egyeb;

{$R *.dfm}

procedure TFKeszlet.TabControl1Change(Sender: TObject);
var
  rkod: string;
begin
  TKMOZGAS.Close;
  if TabControl1.TabIndex=0 then
  begin
    TKMOZGAS.CommandText:='select * from KMOZGAS order by km_datum';
  end
  else
  begin
    rkod:=_rkod[ _rnev.IndexOf(TabControl1.Tabs[TabControl1.tabindex]) ]  ;
    TKMOZGAS.CommandText:='select * from KMOZGAS where km_rakkod='+rkod+' order by km_datum';
  end;
  TKMOZGAS.Open;
  DBGrid1.ReadOnly:= TabControl1.TabIndex=0;


end;

procedure TFKeszlet.FormCreate(Sender: TObject);
begin
  TSZOTAR.Close;
  TSZOTAR.Parameters[0].Value:='140';  //raktárak
  TSZOTAR.Open;
  TSZOTAR.last;
  TSZOTAR.First;
  _rkod:=TStringList.Create;
  _rnev:=TStringList.Create;
  while not TSZOTAR.Eof do
  begin
    _rkod.Add(TSZOTARSZ_ALKOD.Value);
    _rnev.add(TSZOTARSZ_MENEV.Value);
    TabControl1.Tabs.Add(TSZOTARSZ_MENEV.Value) ;
    TSZOTAR.Next;
  end;
  /////////////////
  TSZOTAR.Close;
  TSZOTAR.Parameters[0].Value:='141';  //mozgásnem
  TSZOTAR.Open;
  TSZOTAR.last;
  _mkod:=TStringList.Create;
  _mnev:=TStringList.Create;
  DBGrid1.Columns[5].PickList.Clear;
  TSZOTAR.First;
  while not TSZOTAR.Eof do
  begin
    _mkod.Add(TSZOTARSZ_ALKOD.Value);
    _mnev.Add(TSZOTARSZ_MENEV.Value);
    DBGrid1.Columns[5].PickList.Add(TSZOTARSZ_MENEV.Value);
    TSZOTAR.Next;
  end;
  TSZOTAR.Close;
  ////////////////
  TTermek.Close;
  TTermek.Open;
  TTermek.First;
  while not TTermek.Eof do
  begin
    DBGrid1.Columns[2].PickList.add(TTermekT_TERNEV.Value);
    TTermek.Next;
  end;
  TTermek.Close;
  ////////////////
  TGEPKOCSI.Close;
  TGEPKOCSI.Open;
  TGEPKOCSI.First;
  while not TGEPKOCSI.Eof do
  begin
    DBGrid1.Columns[3].PickList.add(TGEPKOCSIGK_RESZ.Value);
    TGEPKOCSI.Next;
  end;
  TGEPKOCSI.Close;

end;

procedure TFKeszlet.FormActivate(Sender: TObject);
begin
  TabControl1.TabIndex:=0;
  TabControl1.OnChange(self);
end;

procedure TFKeszlet.TKMOZGASAfterInsert(DataSet: TDataSet);
begin
 // TKMOZGASKM_DATUM.Value:=date;
end;

end.
