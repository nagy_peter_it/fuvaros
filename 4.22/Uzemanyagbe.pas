unit Uzemanyagbe;

interface

uses
	SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
	Forms, Dialogs, StdCtrls, Buttons, Mask, DB, DBTables, J_ALFORM, ADODB;

type
	TUzemanyagbeDlg = class(TJ_AlformDlg)
	BitElkuld: TBitBtn;
	BitKilep: TBitBtn;
	Label2: TLabel;
	MaskEdit2: TMaskEdit;
	MaskEdit7: TMaskEdit;
	Label8: TLabel;
	Label9: TLabel;
	Label12: TLabel;
  MaskEdit10: TMaskEdit;
    BitBtn7: TBitBtn;
    Query1: TADOQuery;
    Label1: TLabel;
    Label3: TLabel;
    MaskEdit1: TMaskEdit;
    BitBtn1: TBitBtn;
	procedure BitKilepClick(Sender: TObject);
	procedure FormCreate(Sender: TObject);
	procedure Tolto(cim : string; teko : string);override;
	procedure BitElkuldClick(Sender: TObject);
  procedure MaskEditClick(Sender: TObject);
  procedure MaskEditExit(Sender: TObject);
	procedure MaskEditKeyPress(Sender: TObject; var Key: Char);
    procedure Modosit(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit10Exit(Sender: TObject);
    procedure BitBtn7Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
	private
     modosult 	: boolean;
	public
	end;

var
	UzemanyagbeDlg: TUzemanyagbeDlg;

implementation

uses
	Egyeb, J_SQL, Kozos;
{$R *.DFM}

procedure TUzemanyagbeDlg.BitKilepClick(Sender: TObject);
begin
	if modosult then begin
  		if NoticeKi('M�dosultak az adatok . Ki akar l�pni ment�s n�lk�l?', NOT_QUESTION ) = 0 then begin
			ret_kod := '';
			Close;
     	end;
  	end else begin
		ret_kod := '';
		Close;
  	end;
end;

procedure TUzemanyagbeDlg.FormCreate(Sender: TObject);
begin
	EgyebDlg.SetADOQueryDatabase(Query1);
	SetMaskEdits([MaskEdit2]);
	ret_kod 	:= '';
  	modosult 	:= false;
end;

procedure TUzemanyagbeDlg.Tolto(cim : string; teko:string);
begin
	ret_kod 		:= teko;
	Caption 		:= cim;
	MaskEdit2.Text 	:= teko;
	MaskEdit7.Text 	:= '';
	MaskEdit10.Text := EgyebDlg.MaiDatum;
	if ret_kod <> '' then begin		{M�dos�t�s}
     	if Query_Run (Query1, 'SELECT * FROM UZEMANYAG WHERE UA_UAKOD = '+teko,true) then begin
			MaskEdit7.Text 		:= SzamString(Query1.FieldByName('UA_ERTEK').AsFloat,12,2);
			MaskEdit10.Text 	:= Query1.FieldByName('UA_DATUM').AsString;
			MaskEdit1.Text 		:= Query1.FieldByName('UA_DATKI').AsString;
		end;
	end;
  	modosult 	:= false;
end;

procedure TUzemanyagbeDlg.BitElkuldClick(Sender: TObject);
begin
	if not Jodatum2(MaskEdit10 ) then begin
		NoticeKi('Hib�s kezd� d�tum megad�sa!');
		MaskEdit10.Text := '';
		MaskEdit10.Setfocus;
     	Exit;
  	end;
	if not Jodatum2(MaskEdit1) then begin
		NoticeKi('Hib�s befejez� d�tum megad�sa!');
		MaskEdit1.Text := '';
		MaskEdit1.Setfocus;
     	Exit;
  	end;
	if ret_kod = '' then begin
		Query_Run (Query1, 'SELECT * FROM UZEMANYAG WHERE UA_DATUM = '''+MaskEdit10.Text+''' ',true);
       if Query1.RecordCount > 0 then begin
			NoticeKi('A megadott d�tummal m�r l�tezik �zemanyag �r!');
           Exit;
       end;
     	ret_kod	:= IntToStr(GetNextCode('UZEMANYAG', 'UA_UAKOD', 0, 0));
     	Query_Run ( Query1, 'INSERT INTO UZEMANYAG (UA_UAKOD) VALUES (' +ret_kod+ ' )',true);
  	end;
  	Query_Update (Query1, 'UZEMANYAG',
    	[
     	'UA_ERTEK', SqlSzamString(StringSzam(MaskEdit7.Text),12,4),
     	'UA_DATUM', ''''+MaskEdit10.Text+'''',
     	'UA_DATKI', ''''+MaskEdit1.Text+''''
     	],
     	' WHERE UA_UAKOD = '+ret_kod );
  	Query1.Close;
  	Close;
end;

procedure TUzemanyagbeDlg.MaskEditClick(Sender: TObject);
begin
	with Sender as TMaskEdit do begin
  		SelectAll;
  	end;
end;

procedure TUzemanyagbeDlg.MaskEditExit(Sender: TObject);
begin
	with Sender as TMaskEdit do begin
  		Text := StrSzamStr(Text,12,2);
  	end;
end;

procedure TUzemanyagbeDlg.MaskEditKeyPress(Sender: TObject; var Key: Char);
begin
  with Sender as TMaskEdit do begin
  		Key := EgyebDlg.Jochar(Text,Key,12,2);
  end;
end;

procedure TUzemanyagbeDlg.Modosit(Sender: TObject);
begin
	modosult := true;
end;

procedure TUzemanyagbeDlg.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
	EgyebDlg.FormReturn(Key);
end;

procedure TUzemanyagbeDlg.MaskEdit10Exit(Sender: TObject);
begin
	DatumExit(Sender, true);
end;

procedure TUzemanyagbeDlg.BitBtn7Click(Sender: TObject);
begin
	EgyebDlg.Calendarbe(MaskEdit10);
end;

procedure TUzemanyagbeDlg.BitBtn1Click(Sender: TObject);
begin
	EgyebDlg.Calendarbe(MaskEdit1);
end;

end.
