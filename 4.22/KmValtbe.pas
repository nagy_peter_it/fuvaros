unit KmValtbe;

interface

uses
	SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
	Forms, Dialogs, StdCtrls, Buttons, Mask, DB, DBTables, J_ALFORM, ADODB ;

type
	TKmValtbeDlg = class(TJ_AlformDlg)
	BitElkuld: TBitBtn;
	BitKilep: TBitBtn;
	Label8: TLabel;
	 M3: TMaskEdit;
	 Label3: TLabel;
	 M4: TMaskEdit;
	 Label6: TLabel;
	 M2: TMaskEdit;
	 Query1: TADOQuery;
	 Query2: TADOQuery;
	 Label42: TLabel;
	 M1: TMaskEdit;
	 BitBtn5: TBitBtn;
	 Label1: TLabel;
	 Label2: TLabel;
	procedure BitKilepClick(Sender: TObject);
	procedure FormCreate(Sender: TObject);
	procedure BitElkuldClick(Sender: TObject);
	 procedure FormKeyDown(Sender: TObject; var Key: Word;
	   Shift: TShiftState);
	 procedure M1Exit(Sender: TObject);
	 procedure MaskKeyPress(Sender: TObject; var Key: Char);
	 procedure Tolto(cim, kod : string); override;
	 procedure BitBtn5Click(Sender: TObject);
	private
	public
		rendszam	: string;
	end;

var
	KmValtbeDlg: TKmValtbeDlg;

implementation

uses
	Egyeb, J_SQL, J_VALASZTO, Kozos  ;
{$R *.DFM}

procedure TKmValtbeDlg.BitKilepClick(Sender: TObject);
begin
	ret_kod := '';
	Close;
end;

procedure TKmValtbeDlg.FormCreate(Sender: TObject);
begin
	EgyebDlg.SetADOQueryDatabase(Query1);
	EgyebDlg.SetADOQueryDatabase(Query2);
	ret_kod  := '';
end;

procedure TKmValtbeDlg.Tolto(cim, kod : string);
begin
	Caption 	:= cim;
	ret_kod  	:= kod;
	if ret_kod <> '' then begin
		if Query_Run (Query1, 'SELECT * FROM KMCHANGE WHERE KC_KCKOD = '+ret_kod ,true) then begin
			rendszam		:= Query1.FieldByName('KC_RENSZ').AsString;
			M1.Text			:= Query1.FieldByname('KC_DATUM').AsString;
			M2.Text			:= Query1.FieldByname('KC_KMOR1').AsString;
			M3.Text			:= Query1.FieldByname('KC_KMOR2').AsString;
			M4.Text			:= Query1.FieldByname('KC_MEGJE').AsString;
		end;
	end;
end;

procedure TKmValtbeDlg.BitElkuldClick(Sender: TObject);
begin

	// Az adatok ellenőrzése

	Query_Run ( Query2, 'SELECT KS_KMORA, KS_DATUM FROM KOLTSEG WHERE KS_RENDSZ = '''+rendszam+''' AND KS_TIPUS = 0 ORDER BY KS_DATUM');
	while not Query2.Eof do begin
		if ( ( Query2.FieldByName('KS_DATUM').AsString > M1.Text ) and (Stringszam(M2.Text) > StringSzam(Query2.FieldByName('KS_KMORA').AsString) ) ) then begin
			NoticeKi('Érvénytelen adatok megadása : '+Query2.FieldByName('KS_DATUM').AsString+' - '+Query2.FieldByName('KS_KMORA').AsString);
			Exit;
		end;
		if ( ( Query2.FieldByName('KS_DATUM').AsString < M1.Text ) and (Stringszam(M2.Text) < StringSzam(Query2.FieldByName('KS_KMORA').AsString) ) ) then begin
			NoticeKi('Érvénytelen adatok megadása : '+Query2.FieldByName('KS_DATUM').AsString+' - '+Query2.FieldByName('KS_KMORA').AsString);
			Exit;
		end;
		Query2.Next;
	end;

	if ret_kod = '' then begin
		{Új súly létrehozása}
		ret_kod := IntToStr(GetNextCode('KMCHANGE', 'KC_KCKOD', 1));
		Query_Run ( Query1, 'INSERT INTO KMCHANGE (KC_KCKOD) VALUES (' +ret_kod+ ' )',true);
	end;
	{Update rekord}
	Query_Update( Query1, 'KMCHANGE',
		[
		'KC_RENSZ', 	''''+rendszam+'''',
		'KC_DATUM', 	''''+M1.Text+'''',
		'KC_KMOR1',		SqlSzamString(StringSzam(M2.Text),12,2),
		'KC_KMOR2',  	SqlSzamString(StringSzam(M3.Text),12,2),
		'KC_MEGJE', 	''''+M4.Text+''''
		], ' WHERE KC_KCKOD = '+ret_kod);
	Close;
end;

procedure TKmValtbeDlg.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
	EgyebDlg.FormReturn(Key);
end;

procedure TKmValtbeDlg.M1Exit(Sender: TObject);
begin
	SzamExit(Sender);
end;

procedure TKmValtbeDlg.MaskKeyPress(Sender: TObject; var Key: Char);
begin
	with Sender as TMaskEdit do begin
		Key := EgyebDlg.Jochar(Text,Key,10,Tag);
	end;
end;

procedure TKmValtbeDlg.BitBtn5Click(Sender: TObject);
begin
	EgyebDlg.Calendarbe(M1);
end;

end.
