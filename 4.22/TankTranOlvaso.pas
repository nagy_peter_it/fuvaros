unit TankTranOlvaso;


interface

uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, DBTables, DB, StdCtrls, Buttons, Mask, DBCtrls,
  ExtCtrls, ADODB, FileCtrl, StrUtils, Grids, Generics.Collections, Math,
  XMLIntf, XMLDoc, KozosTipusok, System.Variants, IniFiles;

type
  TTankTranOlvasoDlg = class(TForm)
	 Memo1: TMemo;
    GridPanel1: TGridPanel;
    BtnReadfile: TBitBtn;
    Panel1: TPanel;
    Label4: TLabel;
    M1: TMaskEdit;
    BitBtn5: TBitBtn;
    SG1: TStringGrid;
    Query1: TADOQuery;
    Query2: TADOQuery;
    OpenDialog1: TOpenDialog;
    QueryMain: TADOQuery;
    BtnKilep: TBitBtn;
    BtnDBRead: TBitBtn;
    ADOConnectionCel: TADOConnection;
    qUNICel: TADOQuery;
    ADOConnectionForras: TADOConnection;
	 procedure FormCreate(Sender: TObject);
	 procedure BitBtn6Click(Sender: TObject);
	 procedure BitBtn5Click(Sender: TObject);
	 procedure BtnReadfileClick(Sender: TObject);
   // function XMLFeldolgozas(const XMLString: string): string;
    procedure FormDestroy(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure BtnDBReadClick(Sender: TObject);
    function StoreTankolas(const MyQuery: TAdoQuery; const DatabaseType, PaymentID, Datum, Idopont: string;
         const TermekKod: integer; const Rendszam, PAN: string; const KMOraAllas: integer;
         const Mennyiseg, NettoOsszeg: double; const VevoKod, Vevoszam, VevoNev, GKVID, GKVNEV: string): boolean;
    function GetStoreTankolasSQL(const MyQuery: TAdoQuery; const DatabaseType, PaymentID, Datum, Idopont: string;
        const TermekKod: integer; const Rendszam, PAN: string; const KMOraAllas: integer;
        const Mennyiseg, NettoOsszeg: double; const VevoKod, Vevoszam, VevoNev, GKVID, GKVNEV: string): string;
    function StoreUzemanyagAr(const MyQuery: TAdoQuery; const DatabaseType: string;
            const Tipus: integer; const Datum: string; const Egysegar: double): boolean;
    function GetStoreUzemanyagArSQL(const MyQuery: TAdoQuery; const DatabaseType: string;
            const Tipus: integer; const Datum: string; const Egysegar: double): string;
    function GetMaxPaymentID(MyQuery: TAdoQuery; const DatabaseType, DatabaseName: string): string;
    function GetMaxArDatum(MyQuery: TAdoQuery; const DatabaseType, DatabaseName: string): string;
    function InsertFuvarosSzamoltSzint(DATUM, KUTASDAT: string; TARTALY: integer; LITER: double): string;
    function InsertFuvarosMertSzint(DATUM, KUTASDAT: string; TARTALY: integer; LITER: double): string;

  private
		kilepes		: boolean;
		alnev		: string;
    TelefonTulajok: TDictionary<string,string>;
  public
    function TankolasAtad(const logfn: string): string;
    function UzemanyagArAtad: string;
    function TankSzintAtad: string;
  end;
const
  const_Hecpoll_ID_prefix = 'HECPOLL_ID_';
  const_Hecpoll_DieselArticleCode = 3;
  const_Hecpoll_AdBlueArticleCode = 8;
var
  TankTranOlvasoDlg: TTankTranOlvasoDlg;

implementation

uses
	Egyeb, Kozos, J_SQL, J_Excel;
{$R *.DFM}

procedure TTankTranOlvasoDlg.FormCreate(Sender: TObject);
begin
	M1.Text	:= '';
	kilepes	:= true;
	EgyebDlg.SetADOQueryDatabase(Query1);
	EgyebDlg.SetADOQueryDatabase(Query2);
end;

procedure TTankTranOlvasoDlg.FormDestroy(Sender: TObject);
begin
  // if TelefonTulajok <> nil then TelefonTulajok.Free;
end;

procedure TTankTranOlvasoDlg.BitBtn6Click(Sender: TObject);
begin
	if not kilepes then begin
		kilepes	:= true;
		// BtnExport.Show;
	end else begin
		Close;
	end;
end;

procedure TTankTranOlvasoDlg.BitBtn1Click(Sender: TObject);
begin

	{Screen.Cursor	:= crHourGlass;
	HonapEllenorzes(M5.Text);
	Screen.Cursor	:= crDefault;
  }
end;

{
procedure TTelSzamlaOlvasoPDFDlg.HonapEllenorzes(Honap: string);
var
  S, TelSzam: string;
  OsszesElofizetes: TStringList;
  VoltMarUjszamla, VoltMarMegszuntszamla: boolean;
  i: integer;
begin
  VoltMarUjszamla:= False;
  VoltMarMegszuntszamla:= False;
  OsszesElofizetes:=TStringList.Create;
  try
      S:= 'select TE_TELSZAM from TELELOFIZETES where TE_STATUSKOD=1';
      Query_Run(EgyebDlg.Query1, S, False);
      while not EgyebDlg.Query1.Eof do begin
        OsszesElofizetes.Add(EgyebDlg.Query1.Fields[0].AsString);
        EgyebDlg.Query1.Next;
        end;  // while
      // --- ismeretlen (�j) telefonsz�mok ---
      S:= 'select TS_TELSZAM from TELSZAMLAK where TS_TELSZAM like ''+%'' and TS_HONAP= '''+Honap+'''';
      Query_Run(EgyebDlg.Query1, S, False);
      Memo1.Lines.Clear;
      while not EgyebDlg.Query1.Eof do
        with EgyebDlg.Query1 do begin
          TelSzam:= Fields[0].AsString;
          if OsszesElofizetes.IndexOf(TelSzam) = -1 then begin
             if not VoltMarUjszamla then begin
                Memo1.Lines.Add('-------------------  �j el�fizet�sek a telefonsz�ml�n   ------------------');
                VoltMarUjszamla:= True;
                end;
             Memo1.Lines.Add(TelSzam);
             end  // if
          else begin // megtal�ltuk
            OsszesElofizetes.Delete(OsszesElofizetes.IndexOf(TelSzam));
            end;
          Next;
         end;  // with + while
      // --- megsz�nt telefonsz�mok ---
      for i:=0 to OsszesElofizetes.Count-1 do begin  // ami itt maradt, az nincs a telefonsz�ml�n
         if not VoltMarMegszuntszamla then begin
            Memo1.Lines.Add('-------------------  Megsz�nt el�fizet�sek (nem szerepelnek a telefonsz�ml�n)   ------------------');
            VoltMarMegszuntszamla:= True;
            end;
         Memo1.Lines.Add(OsszesElofizetes[i]);
        end;  // for

      Memo1.Lines.Add('-------------------   lista v�ge   ------------------');
  finally
    if OsszesElofizetes <> nil then OsszesElofizetes.Free;
    end; // try-finally
end;
 }


procedure TTankTranOlvasoDlg.BitBtn5Click(Sender: TObject);
begin
	// Az �llom�ny kiv�laszt�sa
	// OpenDialog1.Filter		:= 'PDF �llom�nyok (*.pdf)';
	// OpenDialog1.DefaultExt  := 'PDF';
	OpenDialog1.Filter		:= 'XML �llom�nyok (*.xml)';
	OpenDialog1.DefaultExt  := 'XML';
	OpenDialog1.InitialDir	:= EgyebDlg.DocumentPath;
	if OpenDialog1.Execute then begin
		M1.Text := OpenDialog1.FileName;
	end;
end;

procedure TTankTranOlvasoDlg.BtnDBReadClick(Sender: TObject);
var
  S: string;
begin
  Screen.Cursor	:= crHourGlass;
  S:= TankSzintAtad;
  if S <> '' then begin
     NoticeKi('Hiba a tank szint feldolgoz�sban: '+S);
     end
  else begin
     NoticeKi('A tank szint beolvas�s befejez�d�tt!');
     end;
   S:= TankolasAtad('');
  if S <> '' then begin
     NoticeKi('Hiba a tankol�s feldolgoz�sban: '+S);
     end
  else begin
     NoticeKi('A tankol�s beolvas�s befejez�d�tt!');
     end;
  {S:= UzemanyagArAtad;
  if S <> '' then begin
     NoticeKi('Hiba az �zemanyag �r feldolgoz�sban: '+S);
     end
  else begin
     NoticeKi('Az �zemanyag �r beolvas�s befejez�d�tt!');
     end;
     }
  Screen.Cursor	:= crDefault;
end;


function TTankTranOlvasoDlg.GetMaxPaymentID(MyQuery: TADOQuery; const DatabaseType, DatabaseName: string): string;
var
  S, MaxSor, szam_kezdete: string;
  Siker: boolean;
begin
   szam_kezdete:= IntToStr(length(const_Hecpoll_ID_prefix)+1);
   if DatabaseType='FUVAROS' then begin
      S:= 'select max(convert(int, substring(KS_SOR, '+szam_kezdete+', 999))) from '+DatabaseName+'..KOLTSEG_GYUJTO '+
          ' where KS_MEGJ=''Tankol�s import'' and KS_SOR like '''+const_Hecpoll_ID_prefix+'%''';
      end;
   if DatabaseType='TANKOLAS' then begin
      S:= 'select max(convert(int, substring(TA_SOR, '+szam_kezdete+', 999))) from '+DatabaseName+'..TANKOLAS '+
          ' where TA_SOR like '''+const_Hecpoll_ID_prefix+'%''';
      end;
   // HibaKiiro('SQL: '+S);
   Siker:= Query_Run(MyQuery, S, False);
   if not Siker then begin
     HibaKiiro('Hib�s SQL! '+S);
     end;
   S:= MyQuery.Fields[0].AsString;  // pl. 'HECPOLL_ID_46879'
   // HibaKiiro('Result: '+S);
   if S='' then
      Result:= '0'
   // else Result:= copy(S, length(const_Hecpoll_ID_prefix)+1, 999);  // -> 46879
   else Result:= S;
end;

function TTankTranOlvasoDlg.GetMaxArDatum(MyQuery: TADOQuery; const DatabaseType, DatabaseName: string): string;
var
  S, MaxSor: string;
begin
   if DatabaseType='FUVAROS' then begin
      S:= 'select max(TT_DATUM) from '+DatabaseName+'..TETANAR ';
      end;
   Query_Run(MyQuery, S, False);
   Result:= MyQuery.Fields[0].AsString;
end;


function TTankTranOlvasoDlg.TankolasAtad(const logfn: string): string;
var
  S: string;
  Datum, Idopont, Rendszam, Ugyfelkod, Res: string;
  TermekSzam, PAN, SoforNeve, MaxPaymentId, PaymentID, lgstr, VevoKod, Vevoszam, VevoNev, GKVID, GKVNEV: string;
  ForrasDatabase, ForrasDatabase_user, ForrasDatabase_pwd: string;
  KMOraAllas, i, TermekKod: integer;
  Mennyiseg, NettoOsszeg, AFASzazalek, Egysegar: double;
  Siker: boolean;
  Tankolas_DBLista, tankinifile, DatabaseType, DatabaseName, Database_Ugyfellista, Database_user, Database_pwd: string;
  DBLista: TStringList;
  TankIni: TIniFile;
begin
  tankinifile:= ExtractFilePath(Application.ExeName)+'INI\'+ChangeFileExt(ExtractFileName(Application.ExeName), '.INI' );
	TankIni:= TIniFile.Create(tankinifile);
  // a HECPOLL adatb�zis el�r�s
  lgstr:= TankIni.ReadString( 'DATABASE', 'TANKOLAS_LOGIN_STR',  '');
  ForrasDatabase:= Trim(TankIni.ReadString( 'DATABASE', 'TANKOLAS_FORRAS', ''));
  ForrasDatabase_user:= Trim(DecodeString(TankIni.ReadString( 'DATABASE', 'TANKOLAS_FORRAS_USER',  '')));
  ForrasDatabase_pwd:= Trim(DecodeString(TankIni.ReadString( 'DATABASE', 'TANKOLAS_FORRAS_PSWD',  '')));
  ADOConnectionForras.Close;
  ADOConnectionForras.ConnectionString:= lgstr+';User ID= '+ForrasDatabase_user+';Password='+ForrasDatabase_pwd+';Initial Catalog='+ForrasDatabase+';';
  ADOConnectionForras.DefaultDatabase:= ForrasDatabase;
  ADOConnectionForras.Open;
  // c�l adatb�zisok
  Tankolas_DBLista:= TankIni.ReadString('DATABASE','TANKOLAS_DB_LISTA', '');  // pl. DAT_DEV,DKDAT_TESZT
  DBLista:= TStringList.Create;
  try
   try
    StringParse(Tankolas_DBLista, ',', DBLista);
    for i:=0 to DBLista.Count-1 do begin
       DatabaseName:= DBLista[i];
       // a c�l adatb�zis kapcsolat el�k�sz�t�se
       // DatabaseType:= Trim(TankIni.ReadString( 'TANKEXPORT'+IntToStr(i+1), 'TANKOLAS_DB_TIPUS', ''));
       DatabaseType:= Trim(TankIni.ReadString( 'DATABASE-'+DatabaseName, 'DB_TIPUS', ''));
       Database_Ugyfellista:= Trim(TankIni.ReadString( 'TANKEXPORT'+IntToStr(i+1), 'TANKOLAS_UGYFELLISTA', ''));
       Database_user:= Trim(DecodeString(TankIni.ReadString( 'DATABASE-'+DatabaseName, 'USER',  '')));
       Database_pwd:= Trim(DecodeString(TankIni.ReadString( 'DATABASE-'+DatabaseName, 'PSWD',  '')));
       ADOConnectionCel.Close;
       ADOConnectionCel.ConnectionString:= lgstr+';User ID= '+Database_user+';Password='+Database_pwd+';Initial Catalog='+DatabaseName+';';
       ADOConnectionCel.DefaultDatabase:= DatabaseName;
       ADOConnectionCel.Open;

       MaxPaymentId:= GetMaxPaymentID(qUniCel, DatabaseType, DatabaseName);
       // HibaKiiro('MaxID='+MaxPaymentId+ ' '+DatabaseType+'/'+DatabaseName);
       S:= 'select payments.ID_PAYMENTS, payments.CustomersID, payments.CustomerNumber, customers.Lastname MyCustomerName, '+
            ' payments.VehicleLicensePlate, payments.EmployeeNumber, employees.Lastname MyEmployeeName, '+
            ' convert(varchar(10), payments.TransDateTime, 102)+''.'' TransDate, '+
            ' convert(varchar(5), payments.TransDateTime, 108) TransTime, '+
            ' payments.TransArticleCode, payments.TransArticleDescription, payments.Mileage, payments.TransQuantity, '+
            ' payments.TransAmountNet, '+
            ' payments.TransQuantity * szami.Hectronic_uzemanyagar (payments.TransDateTime, TransArticleID) Szamolt_ar_net, '+
            ' payments.CardPAN, payments.CardPAN2, payments.TransArticleID '+
            ' from [kutkezelo\hectronic].HECPOLL.dbo.PAYMENTS '+
            // ' left join [kutkezelo\hectronic].HECPOLL.dbo.customers on convert(int, customers.Number) = convert(int, PAYMENTS.CustomerNumber) '+
            ' left join [kutkezelo\hectronic].HECPOLL.dbo.customers on customers.ID_CUSTOMERS = PAYMENTS.CustomersID '+
			      // ' left join [kutkezelo\hectronic].HECPOLL.dbo.employees on convert(int, employees.Number) = convert(int, PAYMENTS.EmployeeNumber) '+
            ' left join [kutkezelo\hectronic].HECPOLL.dbo.employees on employees.ID_EMPLOYEES = PAYMENTS.EmployeesID '+
            ' where payments.id_payments > '+ MaxPaymentId;
       if Database_Ugyfellista <> 'ALL' then
           S:= S+ ' and payments.CustomersID in ('+Database_Ugyfellista+')';
       S:= S+ ' order by ID_PAYMENTS';
       Siker:= Query_Run(QueryMain, S, False, True); // napl�zza!
       // Writelog_NoThread(logfn, '�j rekordok ('+DatabaseName+'): '+ IntToStr(QueryMain.RecordCount)+' db ['+S+']');
       Writelog_NoThread(logfn, '�j rekordok ('+DatabaseName+'): '+ IntToStr(QueryMain.RecordCount)+' db (ID= '+MaxPaymentId+' �ta)');
       if not Siker then
          Result:= 'SQL SELECT hiba: '+S;
       if siker then begin  // SELECT
           while (not QueryMain.Eof) and Siker do begin
             PaymentID:= QueryMain.FieldByName('ID_PAYMENTS').AsString;
             Datum:= QueryMain.FieldByName('TransDate').AsString;
             Idopont:= QueryMain.FieldByName('TransTime').AsString;
             TermekKod:= QueryMain.FieldByName('TransArticleCode').AsInteger;
             Rendszam:= QueryMain.FieldByName('VehicleLicensePlate').AsString;
             PAN:= QueryMain.FieldByName('CardPAN').AsString;
             KMOraAllas:= QueryMain.FieldByName('Mileage').AsInteger;
             Mennyiseg:= QueryMain.FieldByName('TransQuantity').AsFloat;

             // NettoOsszeg:= QueryMain.FieldByName('TransAmountNet').AsFloat;
             // BruttoOsszeg:= QueryMain.FieldByName('TransAmountNet').AsFloat + QueryMain.FieldByName('TransAmountTax').AsFloat;

             NettoOsszeg:= QueryMain.FieldByName('Szamolt_ar_net').AsFloat;
             // a BruttoOsszeg-et nem t�roljuk!

             GKVID:= QueryMain.FieldByName('EmployeeNumber').AsString;
             GKVNEV:= QueryMain.FieldByName('MyEmployeeName').AsString;
             VevoKod:= QueryMain.FieldByName('CustomersID').AsString;
             Vevoszam:= QueryMain.FieldByName('CustomerNumber').AsString;
             VevoNev:= QueryMain.FieldByName('MyCustomerName').AsString;
             if VevoKod='' then VevoKod:='0';
             if Vevoszam='' then Vevoszam:='0';
             if GKVID='' then GKVID:='0';
             Siker:= StoreTankolas(qUniCel, DatabaseType, PaymentID, Datum, Idopont, TermekKod, Rendszam, PAN, KMOraAllas, Mennyiseg,
                  NettoOsszeg, VevoKod, Vevoszam, VevoNev, GKVID, GKVNEV);
             QueryMain.Next;
             end;  // while
          if not Siker then
            Result:= 'Sikertelen tankol�s t�rol�s (['+DatabaseType+'] ' + DatabaseName+'): PaymentID = '+PaymentID+', PAN = '+PAN;
          end; // if sikeres SELECT
      end;  // for
    except
      on E: Exception do begin
        Result:= '"TankolasAtad" hiba: '+E.Message;
        end;
      end;  // try-except
  finally
      if DBLista <> nil then DBLista.Free;
      end;  // rey-finally
end;

function TTankTranOlvasoDlg.UzemanyagArAtad: string;
var
  Datum, lgstr, Res, S: string;
  ForrasDatabase, ForrasDatabase_user, ForrasDatabase_pwd, MaxArDatum: string;
  Tipus, KMOraAllas, i: integer;
  Egysegar, Database_engedmeny: double;
  Siker: boolean;
  UzemanyagAr_DBLista, tankinifile, DatabaseType, DatabaseName, Database_Ugyfellista, Database_user, Database_pwd: string;
  DBLista: TStringList;
  TankIni: TIniFile;
begin
  tankinifile:= ExtractFilePath(Application.ExeName)+'INI\'+ChangeFileExt(ExtractFileName(Application.ExeName), '.INI' );
	TankIni:= TIniFile.Create(tankinifile);
  // a HECPOLL adatb�zis el�r�s
  lgstr:= TankIni.ReadString( 'DATABASE', 'TANKOLAS_LOGIN_STR',  '');
  ForrasDatabase:= Trim(TankIni.ReadString( 'DATABASE', 'TANKOLAS_FORRAS', ''));
  ForrasDatabase_user:= Trim(DecodeString(TankIni.ReadString( 'DATABASE', 'TANKOLAS_FORRAS_USER',  '')));
  ForrasDatabase_pwd:= Trim(DecodeString(TankIni.ReadString( 'DATABASE', 'TANKOLAS_FORRAS_PSWD',  '')));
  ADOConnectionForras.Close;
  ADOConnectionForras.ConnectionString:= lgstr+';User ID= '+ForrasDatabase_user+';Password='+ForrasDatabase_pwd+';Initial Catalog='+ForrasDatabase+';';
  ADOConnectionForras.DefaultDatabase:= ForrasDatabase;
  ADOConnectionForras.Open;
  // c�l adatb�zisok
  UzemanyagAr_DBLista:= TankIni.ReadString('DATABASE','UZEMANYAGAR_DB_LISTA', '');  // pl. DAT_DEV,DKDAT_TESZT
  DBLista:= TStringList.Create;
  try
    StringParse(UzemanyagAr_DBLista, ',', DBLista);
    for i:=0 to DBLista.Count-1 do begin
       DatabaseName:= DBLista[i];
       // a c�l adatb�zis kapcsolat el�k�sz�t�se
       DatabaseType:= Trim(TankIni.ReadString( 'DATABASE-'+DatabaseName, 'DB_TIPUS', ''));
       Database_engedmeny:= StringSzam(TankIni.ReadString( 'UZEMANYAGAREXPORT'+IntToStr(i+1), 'ENGEDMENY', ''));
       Database_user:= Trim(DecodeString(TankIni.ReadString( 'DATABASE-'+DatabaseName, 'USER',  '')));
       Database_pwd:= Trim(DecodeString(TankIni.ReadString( 'DATABASE-'+DatabaseName, 'PSWD',  '')));
       ADOConnectionCel.Close;
       ADOConnectionCel.ConnectionString:= lgstr+';User ID= '+Database_user+';Password='+Database_pwd+';Initial Catalog='+DatabaseName+';';
       ADOConnectionCel.DefaultDatabase:= DatabaseName;
       ADOConnectionCel.Open;

       MaxArDatum:= GetMaxArDatum(qUniCel, DatabaseType, DatabaseName);
       S:= 'select p.ArticlesID, p.StationsID, '+
            ' convert(varchar(10), ActivationDateTime, 102)+''.'' ActivationDate, '+
            ' a.NetPrice, a.TaxRate, a.Price GrossPrice '+
            ' from [kutkezelo\hectronic].HECPOLL.dbo.ARTICLEPRICES p '+
            ' inner join [kutkezelo\hectronic].HECPOLL.dbo.ARTICLEPRICEACTIVATIONS a on a.ArticlePricesID = p.ID_ARTICLEPRICES '+
            ' where convert(varchar(10), ActivationDateTime, 102)+''.'' > '''+ MaxArDatum+''''+
            ' order by ActivationDateTime';
       Query_Run(QueryMain, S, False);
       Siker:= True;
       while (not QueryMain.Eof) and Siker do begin
         Tipus:= QueryMain.FieldByName('ArticlesID').AsInteger;
         Datum:= QueryMain.FieldByName('ActivationDate').AsString;
         Egysegar:= QueryMain.FieldByName('NetPrice').AsFloat - Database_engedmeny;
         Siker:= StoreUzemanyagAr(qUniCel, DatabaseType, Tipus, Datum, Egysegar);
         QueryMain.Next;
         end;  // while
      if not Siker then
        Result:= 'Sikertelen �zemanyag �r t�rol�s (['+DatabaseType+'] ' + DatabaseName+'): T�pus = '+IntToStr(Tipus)+', D�tum = '+Datum;
      end;  // for

  finally
      if DBLista <> nil then DBLista.Free;
      end;  // rey-finally
end;


function TTankTranOlvasoDlg.TankSzintAtad: string;
var
  lgstr, Res, S: string;
  ForrasDatabase, ForrasDatabase_user, ForrasDatabase_pwd, MaxArDatum: string;
  i: integer;
  Egysegar, Database_engedmeny: double;
  Siker: boolean;
  TANKSZINT_DBLista, tankinifile, DatabaseType, DatabaseName, Database_Ugyfellista, Database_user, Database_pwd: string;
  DBLista: TStringList;
  TankIni: TIniFile;

  DATUM, KUTASDAT, ResSumma: string;
  TARTALY: integer;
  LITER: double;
begin
  tankinifile:= ExtractFilePath(Application.ExeName)+'INI\'+ChangeFileExt(ExtractFileName(Application.ExeName), '.INI' );
  TankIni:= TIniFile.Create(tankinifile);
  // a HECPOLL adatb�zis el�r�s
  lgstr:= TankIni.ReadString( 'DATABASE', 'TANKOLAS_LOGIN_STR',  '');
  ForrasDatabase:= Trim(TankIni.ReadString( 'DATABASE', 'TANKOLAS_FORRAS', ''));
  ForrasDatabase_user:= Trim(DecodeString(TankIni.ReadString( 'DATABASE', 'TANKOLAS_FORRAS_USER',  '')));
  ForrasDatabase_pwd:= Trim(DecodeString(TankIni.ReadString( 'DATABASE', 'TANKOLAS_FORRAS_PSWD',  '')));
  ADOConnectionForras.Close;
  ADOConnectionForras.ConnectionString:= lgstr+';User ID= '+ForrasDatabase_user+';Password='+ForrasDatabase_pwd+';Initial Catalog='+ForrasDatabase+';';
  ADOConnectionForras.DefaultDatabase:= ForrasDatabase;
  ADOConnectionForras.Open;
  // c�l adatb�zisok
  TANKSZINT_DBLista:= TankIni.ReadString('DATABASE','TANKSZINT_DB_LISTA', '');  // pl. DAT_DEV,DKDAT_TESZT
  DBLista:= TStringList.Create;
  try
   try
    StringParse(TANKSZINT_DBLista, ',', DBLista);
    for i:=0 to DBLista.Count-1 do begin
       DatabaseName:= DBLista[i];
       // a c�l adatb�zis kapcsolat el�k�sz�t�se
       DatabaseType:= Trim(TankIni.ReadString( 'DATABASE-'+DatabaseName, 'DB_TIPUS', ''));
       Database_user:= Trim(DecodeString(TankIni.ReadString( 'DATABASE-'+DatabaseName, 'USER',  '')));
       Database_pwd:= Trim(DecodeString(TankIni.ReadString( 'DATABASE-'+DatabaseName, 'PSWD',  '')));
       ADOConnectionCel.Close;
       ADOConnectionCel.ConnectionString:= lgstr+';User ID= '+Database_user+';Password='+Database_pwd+';Initial Catalog='+DatabaseName+';';
       ADOConnectionCel.DefaultDatabase:= DatabaseName;
       ADOConnectionCel.Open;
       // ----------------------- sz�molt �rt�kek t�rol�sa --------------------------
       // mikori a legut�bbi k�toszlop lek�rdez�s
       S:= 'select convert(varchar(11), max(EventDateTime), 102)+''. ''+convert(varchar(10),max(EventDateTime),108) KUTASDAT '+
           ' from [kutkezelo\hectronic].HECPOLL.dbo.events '+
           ' where Source like ''J&E Diesel Kft. / JE%'' '+
           ' and Description=''Polling terminal executed successful'' ';
       Query_Run(QueryMain, S, False);
       KUTASDAT:= QueryMain.FieldByName('KUTASDAT').AsString;
       S:= 'select TANKS.ID_TANKS TARTALY, TANKS.CalcVolume LITER from [kutkezelo\hectronic].HECPOLL.dbo.TANKS '+
        ' where ID_TANKS in (1,2,3) ';
       Query_Run(QueryMain, S, False);
       Siker:= True;
       while (not QueryMain.Eof) and Siker do begin
          DATUM:= FormatDateTime('YYYY.MM.DD. HH:MM:SS', Now);  // a most �rz�kelt legut�bbi �rt�ket t�roljuk el
          TARTALY:= QueryMain.FieldByName('TARTALY').AsInteger;
          LITER:= QueryMain.FieldByName('LITER').AsFloat;
          Res:= InsertFuvarosSzamoltSzint(DATUM, KUTASDAT, TARTALY, LITER);
          if Res <> '' then
            ResSumma:= ResSumma + ' ' + Res;
         QueryMain.Next;
         end;  // while
       // ----------------------- m�rt �rt�kek t�rol�sa --------------------------
       S:= 'select Liter, Mikor, '+
        ' convert(varchar(11), Mikor, 102)+''. ''+convert(varchar(10),Mikor,108) KUTASDAT '+
        ' from '+
        '	(select tankdatas.TanksID, tankdatas.Volume*2/100 Liter, tankdatas.DateTime Mikor, '+
        ' ROW_NUMBER() OVER(ORDER BY DateTime DESC) AS Sorszam '+
        '	from [kutkezelo\hectronic].HECPOLL.dbo.TANKDATAS where TanksID=1) a '+
        ' where Sorszam=1 ';
       Query_Run(QueryMain, S, False);
       Siker:= True;
       while (not QueryMain.Eof) and Siker do begin
          DATUM:= FormatDateTime('YYYY.MM.DD. HH:MM:SS', Now);  // a most �rz�kelt legut�bbi �rt�ket t�roljuk el
          // KUTASDAT:= QueryMain.FieldByName('Mikor').AsString;
          KUTASDAT:= QueryMain.FieldByName('KUTASDAT').AsString;
          TARTALY:= 1;
          LITER:= QueryMain.FieldByName('LITER').AsFloat;
          Res:= InsertFuvarosMertSzint(DATUM, KUTASDAT, TARTALY, LITER);
          if Res <> '' then
            ResSumma:= ResSumma + ' ' + Res;
         QueryMain.Next;
         end;  // while

      if not Siker then
        Result:= 'Sikertelen �zemanyag szint t�rol�s (['+DatabaseType+'] ' + DatabaseName+'): TARTALY = '+IntToStr(TARTALY)+', D�tum = '+Datum;
      end;  // for
    except
        on E: exception do
          ResSumma:= ResSumma + ' !Exception:' +E.Message;
      end;  // try-except
   finally
      if DBLista <> nil then DBLista.Free;
      end;  // rey-finally
 Result:= Trim(ResSumma);
end;

function TTankTranOlvasoDlg.InsertFuvarosSzamoltSzint(DATUM, KUTASDAT: string; TARTALY: integer; LITER: double): string;
var
  S: string;
begin
  S:= 'insert into KUTALLAPOT (KU_IDOPONT, KU_KUTASDAT, KU_TARTALY, KU_LITER) '+
      ' values ('''+ DATUM + ''', '+
      ''''+ KUTASDAT + ''', '+
      IntToStr(TARTALY)+', '+
      SqlSzamString(LITER, 10, 2)+')';
  if not Query_Run(qUniCel, S, false) then begin
     Result:= 'Hiba a szint t�rol�sn�l! '+S;
     end;
end;

function TTankTranOlvasoDlg.InsertFuvarosMertSzint(DATUM, KUTASDAT: string; TARTALY: integer; LITER: double): string;
var
  S: string;
begin
  S:= 'insert into KUTSZINTMERO (KM_IDOPONT, KM_KUTASDAT, KM_TARTALY, KM_LITER) '+
      ' values ('''+ DATUM + ''', '+
      ''''+ KUTASDAT + ''', '+
      IntToStr(TARTALY)+', '+
      SqlSzamString(LITER, 10, 2)+')';
  if not Query_Run(qUniCel, S, false) then begin
     Result:= 'Hiba a m�rt szint t�rol�sn�l! '+S;
     end;
end;

procedure TTankTranOlvasoDlg.BtnReadfileClick(Sender: TObject);
var
  S: string;
begin
{  if (M1.Text='') then begin
     NoticeKi('�llom�ny megad�sa k�telez�!') ;
     exit;
     end;
	Screen.Cursor	:= crHourGlass;
	// BtnExport.Hide;
  S:= LoadBinaryStringFromFile(M1.Text);
  if S<>'' then begin
  	S:= XMLFeldolgozas(S);
    if S <> '' then begin
        NoticeKi('Hiba a feldolgoz�sban: '+S);
        end
    else begin
        NoticeKi('A beolvas�s befejez�d�tt!');
        end
    end // van bemeneti �llom�ny
  else begin
    NoticeKi('�res bemeneti �llom�ny!');
    end;
  Screen.Cursor	:= crDefault;
//	Close;
}
end;

{
function TTankTranOlvasoDlg.XMLFeldolgozas(const XMLString: string): string;
const
  TransactionsElement = 'Transactions';
  DocumentDataElement = 'DocumentData';
  SaleElement  = 'Sale';
  CustomerAttribute = 'Customer';
  DateElement  = 'Date';
  YearAttribute  = 'Year';
  MonthAttribute = 'Month';
  DayAttribute = 'Day';
  TimeElement = 'Time';
  HourAttribute = 'Hour';
  MinuteAttribute = 'Minute';
  ArticleSaleElement = 'ArticleSale';
  QuantityAttribute = 'Quantity';
  SglPriceAttribute = 'SglPrice';
  AmountAttribute = 'Amount';
  TaxrateAttribute = 'Taxrate';
  TankNumberAttribute = 'TankNumber';
  PaymentElement = 'Payment';
  PANAttribute = 'PAN';
  OdometerAttribute = 'Odometer';
  EmployeeElement = 'Employee';
  FirstnameAttribute = 'Firstname';
  LastnameAttribute = 'Lastname';
  VehicleElement = 'Vehicle';
  LicensePlateAttribute = 'LicensePlate';

var
	i: integer;
  LDocument: IXMLDocument;
  LNodeElement, LTransactionsElement, LDocumentDataElement, LSaleNode, LDatumElement, LIdopontElement: IXMLNode;
  LTermekElement, LFizetesElement, LSoforElement, LJarmuElement: IXMLNode;
  Datum, Idopont, Rendszam, Ugyfelkod, S: string;
  TermekSzam, PAN, SoforNeve: string;
  KMOraAllas: integer;
  Mennyiseg, NettoOsszeg, AFASzazalek, BruttoOsszeg, Egysegar: double;

  function MyGetChildText(ParentNode: IXMLNode; NodeName: string): string;
    begin
       if ParentNode.ChildNodes.FindNode(NodeName) <> nil then
           Result:= ParentNode.ChildNodes.FindNode(NodeName).Text
       else Result:='';
    end;

  function MyGetAttribute(ParentNode: IXMLNode; AttributeName: string): string;
    var
      MyAttr: OleVariant;
    begin
       MyAttr:= ParentNode.Attributes[AttributeName];
       // if not VarIsClear(MyAttr) then
       if MyAttr <> Null then
           Result:= MyAttr
       else Result:='';
    end;

  function MyFindNode(ParentNode: IXMLNode; NodeName: string; Kotelezo: boolean = True): IXMLNode;
    var
      S: string;
    begin
       Result:= ParentNode.ChildNodes.FindNode(NodeName);
       if Kotelezo and (Result = nil) then begin
          S:= 'Nem feldolgozhat� XML szerkezet: "'+ParentNode.NodeName+'" alatt nem tal�lhat� "'+NodeName+'"';
          Raise Exception.Create(S);
          end;
    end;


begin
  Memo1.Clear;
  LDocument := TXMLDocument.Create(nil);
  try
      LDocument.LoadFromXml(XMLString);
      LTransactionsElement := LDocument.ChildNodes.FindNode(TransactionsElement);
      LDocumentDataElement := MyFindNode(LTransactionsElement, DocumentDataElement);
      for i:= 0 to LDocumentDataElement.ChildNodes.Count - 1 do begin
        LSaleNode:= LDocumentDataElement.ChildNodes.Get(i);
        if LSaleNode.NodeName = SaleElement then begin
          Ugyfelkod:= MyGetAttribute(LSaleNode, CustomerAttribute);
          LDatumElement := MyFindNode(LSaleNode, DateElement);
          Datum:= MyGetAttribute(LDatumElement, YearAttribute)+'.'+
                  MyGetAttribute(LDatumElement, MonthAttribute)+'.'+
                  MyGetAttribute(LDatumElement, DayAttribute)+'.';
          LIdopontElement := MyFindNode(LSaleNode, TimeElement);
          Idopont:= MyGetAttribute(LIdopontElement, HourAttribute)+':'+
                    MyGetAttribute(LIdopontElement, MinuteAttribute);
          LTermekElement := MyFindNode(LSaleNode, ArticleSaleElement);
          Mennyiseg:= StringSzam(MyGetAttribute(LTermekElement, QuantityAttribute));
          Egysegar:= StringSzam(MyGetAttribute(LTermekElement, SglPriceAttribute));
          NettoOsszeg:= StringSzam(MyGetAttribute(LTermekElement, AmountAttribute));
          AFASzazalek:= StringEgesz(MyGetAttribute(LTermekElement, TaxrateAttribute));
          TermekSzam:= MyGetAttribute(LTermekElement, TankNumberAttribute);
          LFizetesElement := MyFindNode(LSaleNode, PaymentElement);
          PAN:= MyGetAttribute(LFizetesElement, PANAttribute);
          KMOraAllas:= StringEgesz(MyGetAttribute(LFizetesElement, OdometerAttribute));
          LSoforElement := MyFindNode(LSaleNode, EmployeeElement, False);
          if LSoforElement <> nil then begin
            SoforNeve:= trim(MyGetAttribute(LSoforElement, FirstnameAttribute)+' '+
                        MyGetAttribute(LSoforElement, LastnameAttribute));
            end
          else begin
            SoforNeve:= '';
            end;
          LJarmuElement := MyFindNode(LSaleNode, VehicleElement, False);
          if LJarmuElement <> nil then begin
            Rendszam:= MyGetAttribute(LJarmuElement, LicensePlateAttribute);
            end
          else begin
            Rendszam:= '';
            end;
          BruttoOsszeg:= NettoOsszeg * (1+ AFASzazalek/100);
          StoreTankolas('', Datum, Idopont, TermekSzam, Rendszam, PAN, KMOraAllas, Mennyiseg, NettoOsszeg, BruttoOsszeg);
          end;  // if
        end;  // for i
      // LDocument.Destroy;
  except
      on E: Exception do begin
        S:= 'XML feldolgoz�si hiba: '+E.Message;
        Result:= S;
        end;  // on E
      end;  // try-except

end;
}

function TTankTranOlvasoDlg.GetStoreTankolasSQL(const MyQuery: TAdoQuery; const DatabaseType, PaymentID, Datum, Idopont: string;
        const TermekKod: integer; const Rendszam, PAN: string; const KMOraAllas: integer;
        const Mennyiseg, NettoOsszeg: double; const VevoKod, Vevoszam, VevoNev, GKVID, GKVNEV: string): string;
var
  S, EgyediStr, UzemanyagTipus_Fuvaros, UzemanyagTipus_Tankolas, Megjegyzes_Fuvaros, UzemanyagKod_Tankolas: string;
  ktkod: integer;
begin
  // EgyediStr:= PAN+'/'+Rendszam+'/'+Datum+Idopont;
  EgyediStr:=const_Hecpoll_ID_prefix+PaymentID;
  if TermekKod = const_Hecpoll_DieselArticleCode then begin  // diesel
    UzemanyagTipus_Fuvaros:= '0';
    Megjegyzes_Fuvaros:= '�zemanyag k�lts�g';   // H�t� tankol�s??
    UzemanyagTipus_Tankolas:= 'Diesel';
    UzemanyagKod_Tankolas:= '10011';
    end;
  if TermekKod = const_Hecpoll_AdBlueArticleCode then begin  // adblue
    UzemanyagTipus_Fuvaros:= '2';
    Megjegyzes_Fuvaros:= 'AD Blue';
    UzemanyagTipus_Tankolas:= 'AdBlue';
    UzemanyagKod_Tankolas:= '10015';
    end;
  if DatabaseType = 'FUVAROS' then begin
      Query_Run(MyQuery, 'SELECT KS_KTKOD FROM KOLTSEG_GYUJTO WHERE KS_SOR='''+EgyediStr+''' ', False);
      if MyQuery.RecordCount > 0 then begin  // L�tez� rekord, friss�teni kell
        ktkod:= MyQuery.Fields[0].AsInteger;
        Result:= Query_Update_FormatSQL ('KOLTSEG_GYUJTO',
          [
          'KS_SOR', ''''+EgyediStr+'''',
          'KS_DATUM', ''''+Datum+'''',
          'KS_IDO', ''''+Idopont+'''',
          'KS_RENDSZ', ''''+Rendszam+'''',
          'KS_KMORA', SqlSzamString(KMOraAllas,18,0),
          'KS_UZMENY', SqlSzamString(Mennyiseg,18,2),
          'KS_OSSZEG', SqlSzamString(NettoOsszeg,18,2),
          'KS_VALNEM', ''''+'HUF'+'''',
          'KS_ARFOLY', '1',
          'KS_LEIRAS', ''''+Megjegyzes_Fuvaros+'''',
          'KS_MEGJ', ''''+'Tankol�s import'+'''',
          'KS_TELE', '1', // honnan tudjuk majd, hogy teletank?
          'KS_TELES', ''''+'N'+'''',  // ezt nem tudom mi�rt
          'KS_ERTEK', SqlSzamString(NettoOsszeg,18,2),
          'KS_FIMOD', ''''+'Telep'+'''',
          'KS_TETAN', '1',  // telepi tankol�s
          'KS_TIPUS', UzemanyagTipus_Fuvaros,
          'KS_SONEV', ''''+''+'''',
          'KS_ORSZA', ''''+'HU'+'''',
          'KS_KTIP', '1',
          'KS_NC_KM', '0',
          'KS_NC_USZ', '0',
          'KS_NC_UME', '0',
          'KS_NC_DAT', ''''+''+'''',
          'KS_NC_IDO', ''''+''+''''
          ], ' WHERE KS_KTKOD='+IntToStr(ktkod));
        end // l�tez� rekord
      else begin  // �j rekord l�trehoz�sa
        Query_Run(MyQuery, 'SELECT MAX(KS_KTKOD+1) MAXKOD FROM KOLTSEG_GYUJTO', False);
        if MyQuery.RecordCount > 0 then
          ktkod	:= StrToIntDef(MyQuery.Fields[0].AsString,0)
        else ktkod:=1;
        Result:= Query_Insert_FormatSQL ('KOLTSEG_GYUJTO',
          [
          'KS_KTKOD', IntToStr(ktkod),
          'KS_SOR', ''''+EgyediStr+'''',
          'KS_DATUM', ''''+Datum+'''',
          'KS_IDO', ''''+Idopont+'''',
          'KS_RENDSZ', ''''+Rendszam+'''',
          'KS_KMORA', SqlSzamString(KMOraAllas,18,0),
          'KS_UZMENY', SqlSzamString(Mennyiseg,18,2),
          'KS_OSSZEG', SqlSzamString(NettoOsszeg,18,2),
          'KS_VALNEM', ''''+'HUF'+'''',
          'KS_ARFOLY', '1',
          'KS_LEIRAS', ''''+Megjegyzes_Fuvaros+'''',  // ez lehet ADBlue is majd, ha tudjuk a tart�lyokat
          'KS_MEGJ', ''''+'Tankol�s import'+'''',
          'KS_TELE', '1', // honnan tudjuk majd, hogy teletank?
          'KS_TELES', ''''+'N'+'''',  // ezt nem tudom mi�rt
          'KS_ERTEK', SqlSzamString(NettoOsszeg,18,2),
          'KS_FIMOD', ''''+'Telep'+'''',
          'KS_TETAN', '1',  // telepi tankol�s
          'KS_TIPUS', UzemanyagTipus_Fuvaros,
          'KS_SONEV', ''''+''+'''',
          'KS_ORSZA', ''''+'HU'+'''',
          'KS_KTIP', '1',
          'KS_NC_KM', '0',
          'KS_NC_USZ', '0',
          'KS_NC_UME', '0',
          'KS_NC_DAT', ''''+''+'''',
          'KS_NC_IDO', ''''+''+''''
          ]);
        end; // else , �j rekord
    end;  // if FUVAROS
  if DatabaseType = 'TANKOLAS' then begin
      Query_Run(MyQuery, 'SELECT TA_SORSZ FROM TANKOLAS WHERE TA_SOR='''+EgyediStr+''' ', False);
      if MyQuery.RecordCount > 0 then begin  // L�tez� rekord, friss�teni kell
        ktkod:= MyQuery.Fields[0].AsInteger;
        Result:= Query_Update_FormatSQL ('TANKOLAS',
          [
          'TA_SOR', ''''+EgyediStr+'''',
          'TA_DATUM', ''''+Datum+'''',
          'TA_IDO', ''''+Idopont+'''',
          'TA_RENSZ', ''''+Rendszam+'''',
          'TA_KM', SqlSzamString(KMOraAllas,18,0),
          'TA_MENNY', SqlSzamString(Mennyiseg,18,2),
          'TA_NEERT', SqlSzamString(NettoOsszeg,18,2),
          'TA_PAN', ''''+PAN+'''',
          'TA_VENEV', ''''+VevoNev+'''',
          'TA_VEKOD', VevoKod,
          'TA_VESZA',  Vevoszam,
          'TA_TEKOD', UzemanyagKod_Tankolas,
          'TA_TENEV', ''''+UzemanyagTipus_Tankolas+'''',
          'TA_SSZ', '0',
          'TA_GKVID', GKVID,
          'TA_GKNEV', ''''+GKVNEV+''''
          ], ' WHERE TA_SORSZ='+IntToStr(ktkod));
        end // l�tez�
      else begin  // �j rekord l�trehoz�sa
        Query_Run(MyQuery, 'select max(TA_SORSZ)+1 from TANKOLAS', False);
        if MyQuery.RecordCount > 0 then
            ktkod:= MyQuery.Fields[0].AsInteger
        else ktkod:=1;
        Result:= Query_Insert_FormatSQL ('TANKOLAS',
          [
          'TA_SORSZ', IntToStr(ktkod),
          'TA_SOR', ''''+EgyediStr+'''',
          'TA_DATUM', ''''+Datum+'''',
          'TA_IDO', ''''+Idopont+'''',
          'TA_RENSZ', ''''+Rendszam+'''',
          'TA_KM', SqlSzamString(KMOraAllas,18,0),
          'TA_MENNY', SqlSzamString(Mennyiseg,18,2),
          'TA_NEERT', SqlSzamString(NettoOsszeg,18,2),
          'TA_PAN', ''''+PAN+'''',
          'TA_VENEV', ''''+VevoNev+'''',
          'TA_VEKOD', VevoKod,
          'TA_VESZA',  Vevoszam,
          'TA_TEKOD', UzemanyagKod_Tankolas,
          'TA_TENEV', ''''+UzemanyagTipus_Tankolas+'''',
          'TA_SSZ', '0',
          'TA_GKVID', GKVID,
          'TA_GKNEV', ''''+GKVNEV+''''
           ]);
        end;  // else, �j rekord
      end;  // TANKOLAS
end;

function TTankTranOlvasoDlg.StoreTankolas(const MyQuery: TAdoQuery; const DatabaseType, PaymentID, Datum, Idopont: string;
        const TermekKod: integer; const Rendszam, PAN: string; const KMOraAllas: integer; const Mennyiseg, NettoOsszeg: double;
        const VevoKod, Vevoszam, VevoNev, GKVID, GKVNEV: string): boolean;
var
  SQL: string;
  ktkod: integer;
begin
  SQL:= GetStoreTankolasSQL(MyQuery, DatabaseType, PaymentID, Datum, Idopont, TermekKod, Rendszam, PAN, KMOraAllas, Mennyiseg,
        NettoOsszeg, VevoKod, Vevoszam, VevoNev, GKVID, GKVNEV);
	Result:= Query_Run(MyQuery, SQL, False, True); // hiba napl�z�s legyen
end;


function TTankTranOlvasoDlg.GetStoreUzemanyagArSQL(const MyQuery: TAdoQuery; const DatabaseType: string;
            const Tipus: integer; const Datum: string; const Egysegar: double): string;
var
  S, EgyediStr, UzemanyagTipus_Fuvaros, UzemanyagTipus_Tankolas, Megjegyzes_Fuvaros, UzemanyagKod_Tankolas: string;
  ttkod: integer;
begin
  if Tipus = const_Hecpoll_DieselArticleCode then begin  // diesel
    UzemanyagTipus_Fuvaros:= '0';
    end;
  if Tipus = const_Hecpoll_AdBlueArticleCode then begin // adblue
    UzemanyagTipus_Fuvaros:= '1';
    end;
  if DatabaseType = 'FUVAROS' then begin
      Query_Run(MyQuery, 'SELECT TT_TTKOD FROM TETANAR WHERE TT_DATUM='''+Datum+''' and TT_TIPUS='+UzemanyagTipus_Fuvaros, False);
      if MyQuery.RecordCount > 0 then begin  // L�tez� rekord, friss�teni kell
        ttkod:= MyQuery.Fields[0].AsInteger;
        Result:= Query_Update_FormatSQL ('TETANAR',
          [
          'TT_DATUM', ''''+Datum+'''',
          'TT_TIPUS', UzemanyagTipus_Fuvaros,
          'TT_EGYAR', SqlSzamString(Egysegar,18,2),
          'TT_MEGJE', ''''+'Automatikus �tv�tel'+''''
          ], ' WHERE TT_TTKOD='+IntToStr(ttkod));
        end // l�tez� rekord
      else begin  // �j rekord l�trehoz�sa
        Query_Run(MyQuery, 'SELECT MAX(TT_TTKOD+1) MAXKOD FROM TETANAR', False);
        if MyQuery.RecordCount > 0 then
          ttkod	:= StrToIntDef(MyQuery.Fields[0].AsString,0)
        else ttkod:=1;
        Result:= Query_Insert_FormatSQL ('TETANAR',
          [
          'TT_TTKOD', IntToStr(ttkod),
          'TT_DATUM', ''''+Datum+'''',
          'TT_TIPUS', UzemanyagTipus_Fuvaros,
          'TT_EGYAR', SqlSzamString(Egysegar,18,2),
          'TT_MEGJE', ''''+'Automatikus �tv�tel'+''''
          ]);
        end; // else , �j rekord
    end;  // if FUVAROS
end;

function TTankTranOlvasoDlg.StoreUzemanyagAr(const MyQuery: TAdoQuery; const DatabaseType: string;
            const Tipus: integer; const Datum: string; const Egysegar: double): boolean;
var
  SQL: string;
begin
  SQL:= GetStoreUzemanyagArSQL(MyQuery, DatabaseType, Tipus, Datum, Egysegar);
	Result:= Query_Run(MyQuery, SQL, False, True);
end;



{function TTankTranOlvasoDlg.StoreTankolas(const Datum, Idopont, Rendszam, PAN: string; const KMOraAllas: integer;
        const Mennyiseg, NettoOsszeg, BruttoOsszeg: double): integer;
var
  S, EgyediStr: string;
  ktkod: integer;
begin
  EgyediStr:= PAN+'/'+Rendszam+'/'+Datum+Idopont;
	Query_Run(Query1, 'SELECT KS_KTKOD FROM KOLTSEG_GYUJTO WHERE KS_SOR='''+EgyediStr+''' ');
	if Query1.RecordCount > 0 then begin  // L�tez� rekord, friss�teni kell
    ktkod:= Query1.Fields[0].AsInteger;
		Query_Update (Query2, 'KOLTSEG_GYUJTO',
			[
			'KS_DATUM', ''''+Datum+'''',
			'KS_IDO', ''''+Idopont+'''',
			'KS_RENDSZ', ''''+Rendszam+'''',
			'KS_KMORA', SqlSzamString(KMOraAllas,18,0),
      'KS_UZMENY', SqlSzamString(Mennyiseg,18,2),
      'KS_OSSZEG', SqlSzamString(NettoOsszeg,18,2),
 			'KS_VALNEM', ''''+'HUF'+'''',
      'KS_ARFOLY', '1',
      'KS_LEIRAS', ''''+'�zemanyag k�lts�g'+'''',  // ez lehet ADBlue is majd, ha tudjuk a tart�lyokat
      'KS_MEGJ', ''''+'Tankol�s import'+'''',
      'KS_TELE', '1', // honnan tudjuk majd, hogy teletank?
      'KS_TELES', ''''+'N'+'''',  // ezt nem tudom mi�rt
      'KS_ERTEK', SqlSzamString(NettoOsszeg,18,2),
      'KS_FIMOD', ''''+'Telep'+'''',
      'KS_TETAN', '1',  // telepi tankol�s
      'KS_TIPUS', '0',
      'KS_SONEV', ''''+''+'''',
      'KS_ORSZA', ''''+'HU'+'''',
      'KS_KTIP', '1',
      'KS_NC_KM', '0',
      'KS_NC_USZ', '0',
      'KS_NC_UME', '0',
      'KS_NC_DAT', ''''+''+'''',
      'KS_NC_IDO', ''''+''+''''
			], ' WHERE KS_KTKOD='+IntToStr(ktkod));
		Memo1.Lines.Add('L�tez� k�lts�g friss�t�se ['+EgyediStr+'].');
    end
  else begin  // �j rekord l�trehoz�sa
    ktkod	:= GetNextCode('KOLTSEG_GYUJTO', 'KS_KTKOD', 1, 0);
		Query_Insert (Query2, 'KOLTSEG_GYUJTO',
			[
      'KS_KTKOD', IntToStr(ktkod),
      'KS_SOR', ''''+EgyediStr+'''',
			'KS_DATUM', ''''+Datum+'''',
			'KS_IDO', ''''+Idopont+'''',
			'KS_RENDSZ', ''''+Rendszam+'''',
			'KS_KMORA', SqlSzamString(KMOraAllas,18,0),
      'KS_UZMENY', SqlSzamString(Mennyiseg,18,2),
      'KS_OSSZEG', SqlSzamString(NettoOsszeg,18,2),
 			'KS_VALNEM', ''''+'HUF'+'''',
      'KS_ARFOLY', '1',
      'KS_LEIRAS', ''''+'�zemanyag k�lts�g'+'''',  // ez lehet ADBlue is majd, ha tudjuk a tart�lyokat
      'KS_MEGJ', ''''+'Tankol�s import'+'''',
      'KS_TELE', '1', // honnan tudjuk majd, hogy teletank?
      'KS_TELES', ''''+'N'+'''',  // ezt nem tudom mi�rt
      'KS_ERTEK', SqlSzamString(NettoOsszeg,18,2),
      'KS_FIMOD', ''''+'Telep'+'''',
      'KS_TETAN', '1',  // telepi tankol�s
      'KS_TIPUS', '0',
      'KS_SONEV', ''''+''+'''',
      'KS_ORSZA', ''''+'HU'+'''',
      'KS_KTIP', '1',
      'KS_NC_KM', '0',
      'KS_NC_USZ', '0',
      'KS_NC_UME', '0',
      'KS_NC_DAT', ''''+''+'''',
      'KS_NC_IDO', ''''+''+''''
			], True);
		Memo1.Lines.Add('�j k�lts�g ['+EgyediStr+']');
	end;
  Result:= ktkod;
end;
}

end.

