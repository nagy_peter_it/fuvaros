object FDB_Import: TFDB_Import
  Left = 970
  Top = 422
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = 'Adatb'#225'zis import'#225'l'#225'sa'
  ClientHeight = 362
  ClientWidth = 467
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poMainFormCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 16
  object Label1: TLabel
    Left = 16
    Top = 40
    Width = 35
    Height = 20
    Caption = 'C'#233'l:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label2: TLabel
    Left = 16
    Top = 96
    Width = 62
    Height = 20
    Caption = 'Forr'#225's:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Button2: TButton
    Left = 368
    Top = 24
    Width = 81
    Height = 25
    Caption = 'START'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 0
    OnClick = Button2Click
  end
  object Edit1: TEdit
    Left = 112
    Top = 24
    Width = 241
    Height = 24
    ReadOnly = True
    TabOrder = 1
    Text = 'Edit1'
  end
  object Edit2: TEdit
    Left = 112
    Top = 56
    Width = 241
    Height = 24
    ReadOnly = True
    TabOrder = 2
    Text = 'Edit1'
  end
  object ComboBox1: TComboBox
    Left = 112
    Top = 96
    Width = 241
    Height = 24
    DropDownCount = 20
    TabOrder = 3
    Text = 'ComboBox1'
  end
  object ListBox1: TListBox
    Left = 16
    Top = 128
    Width = 433
    Height = 225
    TabOrder = 4
  end
  object Button1: TButton
    Left = 368
    Top = 96
    Width = 81
    Height = 25
    Caption = 'M'#233'gsem'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 5
    OnClick = Button1Click
  end
  object ADOQuery2: TADOQuery
    Connection = EgyebDlg.ADOConnectionKozos
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'use master'
      'exec sp_databases')
    Left = 160
    Top = 172
    object ADOQuery2DATABASE_NAME: TWideStringField
      FieldName = 'DATABASE_NAME'
      ReadOnly = True
      Size = 128
    end
    object ADOQuery2DATABASE_SIZE: TIntegerField
      FieldName = 'DATABASE_SIZE'
      ReadOnly = True
    end
    object ADOQuery2REMARKS: TStringField
      FieldName = 'REMARKS'
      ReadOnly = True
      Size = 254
    end
  end
  object ADOQuery1: TADOQuery
    Connection = ADOConnection1
    Parameters = <>
    Left = 128
    Top = 172
  end
  object ADOQuery3: TADOQuery
    Connection = EgyebDlg.ADOConnectionKozos
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'exec sp_who')
    Left = 192
    Top = 172
    object ADOQuery3spid: TSmallintField
      FieldName = 'spid'
      ReadOnly = True
    end
    object ADOQuery3ecid: TSmallintField
      FieldName = 'ecid'
      ReadOnly = True
    end
    object ADOQuery3status: TWideStringField
      FieldName = 'status'
      ReadOnly = True
      FixedChar = True
      Size = 30
    end
    object ADOQuery3loginame: TWideStringField
      FieldName = 'loginame'
      ReadOnly = True
      Size = 128
    end
    object ADOQuery3hostname: TWideStringField
      FieldName = 'hostname'
      ReadOnly = True
      FixedChar = True
      Size = 128
    end
    object ADOQuery3blk: TStringField
      FieldName = 'blk'
      ReadOnly = True
      FixedChar = True
      Size = 5
    end
    object ADOQuery3dbname: TWideStringField
      FieldName = 'dbname'
      ReadOnly = True
      Size = 128
    end
    object ADOQuery3cmd: TWideStringField
      FieldName = 'cmd'
      ReadOnly = True
      FixedChar = True
      Size = 16
    end
    object ADOQuery3request_id: TIntegerField
      FieldName = 'request_id'
      ReadOnly = True
    end
  end
  object ADOConnection1: TADOConnection
    ConnectionString = 
      'Provider=SQLNCLI.1;Password=Admin12;Persist Security Info=True;U' +
      'ser ID=sa;Initial Catalog=FUVARTESZTKOZ2012;Data Source=sql'
    DefaultDatabase = 'FUVARTESZTKOZ2012'
    LoginPrompt = False
    Provider = 'SQLNCLI.1'
    Left = 360
    Top = 56
  end
end
