unit TelElofizetFm;

interface

uses
	SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
	StdCtrls, Forms, ExtCtrls, Dialogs, J_FOFORMUJ;

type
	TTelElofizetFormDlg = class(TJ_FOFORMUJDLG)
	  procedure FormCreate(Sender: TObject);override;
	 procedure Adatlap(cim, kod : string);override;
	 procedure ButtonModClick(Sender: TObject);override;
	end;

var
	TelElofizetFormDlg : TTelElofizetFormDlg;

implementation

uses
	Egyeb, TelElofizetbe, J_SQL;

{$R *.DFM}

procedure TTelElofizetFormDlg.FormCreate(Sender: TObject);
var
  S: string;
begin
	Inherited FormCreate(Sender);
  S:= 'select  TE_ID, TE_TELSZAM, sz2.SZ_MENEV SZAMLACSOPORT, sz.SZ_MENEV STATUSZ, /* sz3.SZ_MENEV MOBILNET,*/ '+
    ' ISNULL(d1.DO_NAME, '''') ELOFIZETES_DOLGOZO, ISNULL(d2.DO_NAME,'''') TELEFON_DOLGOZO, '+
    ' TE_PIN, TE_DOELS, TE_TKID, /* TE_ROAMING, */ TE_HIVSZUR, TE_TECH, TE_KIADDAT, TE_MEGJE, TE_STATUSKOD, ISNULL(c.KESZULEKTIPUS,'''') KESZULEKTIPUS, '+
    ' case when TE_KIADHATO = 1 then ''Igen'' else ''Nem'' end TE_KIADHATO, '+
    ' TE_APPLEID, TE_APPLEJELSZO '+
    ' from SZOTAR sz, SZOTAR sz2, /*SZOTAR sz3,*/ TELELOFIZETES e '+
    '   left outer join DOLGOZO d1 ON TE_DOKOD = d1.DO_KOD '+
    // r�gi
    // '   left outer join (select TELKESZULEK ON TK_ID = TE_TKID '+
    '   left outer join (select TK_ID, TK_DOKOD, sz4.SZ_MENEV KESZULEKTIPUS from TELKESZULEK, SZOTAR sz4 '+
    '           where sz4.SZ_ALKOD = TK_TIPUSKOD and sz4.SZ_FOKOD=146) c'+
    '    ON TK_ID = TE_TKID '+
    ' left outer join DOLGOZO d2 ON TK_DOKOD = d2.DO_KOD '+
    ' where sz.SZ_ALKOD = TE_STATUSKOD and sz.SZ_FOKOD=144 '+
    ' and sz2.SZ_ALKOD = TE_SZAMLACSOPKOD and sz2.SZ_FOKOD=145 '+
    ' /* and sz3.SZ_ALKOD = TE_MOBILNETKOD and sz3.SZ_FOKOD=143 */';
  S:= 'select * from ('+S+ ') a where 1=1';  // a sz�r�sekhez be kell "csomagolni"

	AddSzuromezoRovid('SZAMLACSOPORT',  '(select distinct SZ_MENEV SZAMLACSOPORT from SZOTAR where SZ_FOKOD=145) a');
	AddSzuromezoRovid('STATUSZ', '(select distinct SZ_MENEV STATUSZ from SZOTAR where SZ_FOKOD=144) a');
 	// AddSzuromezoRovid('MOBILNET', '(select distinct SZ_MENEV MOBILNET from SZOTAR where SZ_FOKOD=143) a');
  AddSzuromezoRovid('ELOFIZETES_DOLGOZO',  '(select distinct DO_NAME ELOFIZETES_DOLGOZO from DOLGOZO) a', '1');
  AddSzuromezoRovid('TELEFON_DOLGOZO',  '(select distinct DO_NAME TELEFON_DOLGOZO from DOLGOZO) a', '1');
  AddSzuromezoRovid('KESZULEKTIPUS',  '', '1');
  AddSzuromezoRovid('TE_TECH',  'TELELOFIZETES');
  AddSzuromezoRovid('TE_KIADHATO', '');
	// ?? AddSzuromezo('DO_NAME', 'SELECT DISTINCT DO_NAME FROM TELELOFIZETES LEFT OUTER JOIN DOLGOZO ON TE_DOKOD = DO_KOD');
	FelTolto('�j el�fizet�s felvitele ', 'El�fizet�s adatok m�dos�t�sa ',
			'Telefon el�fizet�sek list�ja', 'TE_ID', S, 'TELELOFIZETES', QUERY_KOZOS_TAG );
	nevmezo	:= 'TK_ID';
	width	:= DLG_WIDTH;
	height	:= DLG_HEIGHT;
 //  uresszures:= True;
  ButtonTor.Enabled :=False;
end;

procedure TTelElofizetFormDlg.Adatlap(cim, kod : string);
begin
	Application.CreateForm(TTelElofizetbeDlg, AlForm);
	Inherited Adatlap(cim, kod );
end;

procedure TTelElofizetFormDlg.ButtonModClick(Sender: TObject);
var
	tulaj	: string;
begin
	if not voltalt then begin
 		voltalt 	:= true;
 		Exit;
 	end;
	if ButtonMod.Enabled then begin
  		Adatlap(MODOSIT,Query1.FieldByName(FOMEZO).AsString);
      end;
end;

end.

