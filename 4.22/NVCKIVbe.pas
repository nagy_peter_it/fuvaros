unit NVCKIVbe;

interface

uses
	SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
	Forms, Dialogs, StdCtrls, Buttons, Mask, DB, DBTables, J_ALFORM, ADODB;

type
	TNVCKIVbeDlg = class(TJ_AlformDlg)
	BitElkuld: TBitBtn;
	BitKilep: TBitBtn;
    Label2: TLabel;
    M1: TMaskEdit;
	 Query1: TADOQuery;
	 Label9: TLabel;
    DATTOL: TMaskEdit;
    BitBtn8: TBitBtn;
    M31: TMaskEdit;
    Label46: TLabel;
    BitBtn13: TBitBtn;
    IDOTOL: TMaskEdit;
    Label1: TLabel;
    DATIG: TMaskEdit;
    BitBtn1: TBitBtn;
    IDOIG: TMaskEdit;
	 procedure BitKilepClick(Sender: TObject);
	 procedure FormCreate(Sender: TObject);
	 procedure BitElkuldClick(Sender: TObject);
	 procedure MaskEditClick(Sender: TObject);
	 procedure FormKeyDown(Sender: TObject; var Key: Word;
	   Shift: TShiftState);
	 procedure Tolto(cim, kod : string); override;
	 procedure BitBtn1Click(Sender: TObject);
   procedure BitBtn8Click(Sender: TObject);
    procedure BitBtn13Click(Sender: TObject);
    procedure IDOTOLExit(Sender: TObject);
    procedure IDOIGExit(Sender: TObject);
	private
		regisofor	: string;
	public
	end;

var
	NVCKIVbeDlg: TNVCKIVbeDlg;

implementation

uses
 Egyeb, J_SQL, Kozos,  J_VALASZTO;

{$R *.DFM}

procedure TNVCKIVbeDlg.BitKilepClick(Sender: TObject);
begin
	ret_kod := '';
	Close;
end;

procedure TNVCKIVbeDlg.FormCreate(Sender: TObject);
begin
	EgyebDlg.SeTADOQueryDatabase(Query1);
	ret_kod 				:= '';
end;

procedure TNVCKIVbeDlg.Tolto(cim, kod : string);
begin
	Caption:= cim;
	ret_kod:= kod;
	M1.Text:= ret_kod;
	if ret_kod <> '' then begin
		if Query_Run (Query1, 'SELECT * FROM NVCKIVETEL WHERE KI_KOD = '+ret_kod ,true) then begin
			M31.Text  := Query1.FieldByname('KI_RENSZ').AsString;
			DATTOL.Text	:= Query1.FieldByname('KI_DATTOL').AsString;
			IDOTOL.Text		:= Query1.FieldByname('KI_IDOTOL').AsString;
			DATIG.Text		:= Query1.FieldByname('KI_DATIG').AsString;
			IDOIG.Text		:= Query1.FieldByname('KI_IDOIG').AsString;
		end;
	end;
end;

procedure TNVCKIVbeDlg.BitElkuldClick(Sender: TObject);
var
  S: string;
begin

	if M31.Text = '' then begin
		NoticeKi('Rendsz�m megad�sa k�telez�!');
		M31.Setfocus;
		Exit;
	end;
	if DATTOL.Text = '' then begin
		NoticeKi('Id�szak kezdet�nek megad�sa k�telez�!');
		DATTOL.Setfocus;
		Exit;
	end;
	if IDOTOL.Text = '' then begin
		NoticeKi('Id�szak kezdet�nek megad�sa k�telez�!');
		IDOTOL.Setfocus;
		Exit;
	end;
	if DATIG.Text = '' then begin
		NoticeKi('Id�szak v�g�nek megad�sa k�telez�!');
		DATIG.Setfocus;
		Exit;
	end;
	if IDOIG.Text = '' then begin
		NoticeKi('Id�szak v�g�nek megad�sa k�telez�!');
		IDOIG.Setfocus;
		Exit;
	end;

	if ret_kod = '' then begin // �j adatsor
    {  identity megold�s
    S:='INSERT INTO NVCKIVETEL (KI_RENSZ, KI_DATTOL, KI_IDOTOL, KI_DATIG, KI_IDOIG) ';
    S:=S+' OUTPUT inserted.KI_KOD ';  // visszakapjuk az �rt�k�t
    S:=S+' VALUES ( '''+M31.Text+''', '''+DATTOL.Text+''', '''+IDOTOL.Text+''', '''+DATIG.Text+''', '''+IDOIG.Text+''' )';
		Query_Run (Query1, S ,true);
		ret_kod		:= Query1.Fields[0].AsString;
    }
    ret_kod:= IntToStr(GetNewIDLoop(modeGetNextCode, 'NVCKIVETEL', 'KI_KOD', ''));
    if (ret_kod='0') or (ret_kod='') then begin
     S:= 'Sikertelen k�d gener�l�s, k�rem pr�b�lja �jra! (NVCKivetel)';
     HibaKiiro(S);
     NoticeKi(S);
     Exit;
     end;

    S:='INSERT INTO NVCKIVETEL (KI_KOD, KI_RENSZ, KI_DATTOL, KI_IDOTOL, KI_DATIG, KI_IDOIG) ';
    S:=S+' VALUES ( '+ret_kod+', '''+M31.Text+''', '''+DATTOL.Text+''', '''+IDOTOL.Text+''', '''+DATIG.Text+''', '''+IDOIG.Text+''' )';
		Query_Run (Query1, S ,true);

  	end
  else begin  // update
	  S:='UPDATE NVCKIVETEL SET KI_RENSZ = '''+M31.Text+''',KI_DATTOL='''+DATTOL.Text+''' ,KI_IDOTOL = '''+IDOTOL.Text+''', ';
		S:=S+	' KI_DATIG = '''+DATIG.Text+''', KI_IDOIG = '''+IDOIG.Text+''' WHERE KI_KOD = '+ret_kod;
   	Query_Run(Query1, S);
    end;  // else
	Close;
end;

procedure TNVCKIVbeDlg.MaskEditClick(Sender: TObject);
begin
	with Sender as TMaskEdit do begin
  	SelectAll;
  end;
end;

procedure TNVCKIVbeDlg.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
	EgyebDlg.FormReturn(Key);
end;

procedure TNVCKIVbeDlg.IDOIGExit(Sender: TObject);
begin
   IdoExit(Sender, false);
end;

procedure TNVCKIVbeDlg.IDOTOLExit(Sender: TObject);
begin
   IdoExit(Sender, false);
end;

procedure TNVCKIVbeDlg.BitBtn13Click(Sender: TObject);
begin
    GepkocsiValaszto(M31);
end;


procedure TNVCKIVbeDlg.BitBtn1Click(Sender: TObject);
begin
	EgyebDlg.Calendarbe(DATIG);
end;

procedure TNVCKIVbeDlg.BitBtn8Click(Sender: TObject);
begin
	EgyebDlg.Calendarbe(DATTOL);
end;

end.







