unit Dolgozoujfm;

interface

uses
	SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
	StdCtrls, Forms, DBCtrls, DB, DBGrids, DBTables, Grids, ExtCtrls, Buttons,
	ShellApi, Dialogs, J_FOFORMUJ, ADODB, Mask, Menus, Variants;

type
	TDolgozoUjFmDlg = class(TJ_FOFORMUJDLG)
    BitBtn1: TBitBtn;
	  procedure FormCreate(Sender: TObject);override;
	 procedure ButtonTorClick(Sender: TObject); override;
	 procedure Adatlap(cim, kod : string);override;
	 procedure ButtonKuldClick(Sender: TObject);override;
    procedure BitBtn1Click(Sender: TObject);
	 private
	 public
	end;

var
	DolgozoUjFmDlg : TDolgozoUjFmDlg;

implementation

uses
	Egyeb, J_SQL, DolgozoBe2, Kozos, DolgServ, KulcsDolgozoOlvaso;

{$R *.DFM}

procedure TDolgozoUjFmDlg.FormCreate(Sender: TObject);
begin
	Inherited FormCreate(Sender);
	kellujures	:= false;
	AddSzuromezoRovid( 'DO_ARHIV', 'DOLGOZO' );
	AddSzuromezoRovid( 'DO_NEMZE', 'DOLGOZO' );
	AddSzuromezoRovid( 'DO_JOKAT', 'DOLGOZO' );
	AddSzuromezoRovid( 'DO_CHKUL', 'DOLGOZO' );
	AddSzuromezoRovid( 'DO_RENDSZ', 'DOLGOZO' );
	AddSzuromezoRovid( 'DO_GKKAT', 'DOLGOZO','1' );
	AddSzuromezoRovid( 'DO_CHMOZ', 'DOLGOZO' );
	AddSzuromezoRovid( 'DO_KULSO', 'DOLGOZO' );
	AddSzuromezoRovid( 'DO_CSNEV', 'DOLGOZO', '1' );
	AddSzuromezoRovid( 'DO_ALKAL', 'DOLGOZO' );
  AddSzuromezoRovid( 'DO_NEME', 'DOLGOZO' );
 	AddSzuromezoRovid( 'DO_BEOSZTAS', '', '1');
 	AddSzuromezoRovid( 'DO_KOZOSLISTA', '', '1');
 	AddSzuromezoRovid( 'DO_ICLOUDMAIL', '', '1');
 	AddSzuromezoRovid( 'DO_VCDADD', '', '1');
 	AddSzuromezoRovid( 'DO_KIOKA', '', '1');

	BitBtn1.Parent		:= PanelBottom;

  //	alapstr		:= 'Select * from DOLGOZO , DOLGOZOMEG , GEPKOCSI WHERE (DO_KOD = DM_DOKOD)and(DO_RENDSZ=GK_RESZ)';
         // left outer join szami.DOLGOZOERV on DE_ALKOD=DO_KOD and de_alkod='170'
	// alapstr		:= 'Select * from   DOLGOZOMEG , GEPKOCSI,DOLGOZO left join DOLGOZOERV on DE_DOKOD=DO_KOD and de_alkod='''+'170'+''' WHERE (DO_KOD = DM_DOKOD)and(DO_RENDSZ=GK_RESZ) ';
  alapstr		:= 'Select * from  DOLGOZO '+
    ' left outer join DOLGOZOMEG on DO_KOD = DM_DOKOD '+
    ' left outer join GEPKOCSI on DO_RENDSZ=GK_RESZ '+
    ' left outer join DOLGOZOERV on DE_DOKOD=DO_KOD and de_alkod=''170'' ';

	FelTolto('�j dolgoz� felvitele ', 'Dolgoz�i adatok m�dos�t�sa ',
			'Dolgoz�k list�ja', 'DO_KOD', alapstr, 'DOLGOZO', QUERY_KOZOS_TAG );
	kulcsmezo			:= 'DO_KOD';
	nevmezo				:= 'DO_NAME';
	width				:= DLG_WIDTH;
	height				:= DLG_HEIGHT;

  if EgyebDlg.v_formatum=0  then // JS
    ButtonUj.Enabled:=False;
end;

procedure TDolgozoUjFmDlg.Adatlap(cim, kod : string);
begin
	Application.CreateForm(TDolgozobe2Dlg, AlForm);
	Inherited Adatlap(cim, kod );
end;

procedure TDolgozoUjFmDlg.ButtonTorClick(Sender: TObject);
begin
  if not EgyebDlg.user_super then
  exit;
	if not voltalt then begin
		voltalt 	:= true;
		Exit;
	end;
	if NoticeKi( TORLOTEXT, NOT_QUESTION ) = 0 then begin
		Query_Run(Query2, 'DELETE from DOLGOZO where DO_KOD 		= '''+Query1.FieldByName('DO_KOD').AsString+''' ');
		Query_Run(Query2, 'DELETE FROM DOLGOZOMEG WHERE DM_DOKOD 	= '''+Query1.FieldByName('DO_KOD').AsString+''' ');
		Query_Run(Query2, 'DELETE FROM DOLGOZOERV WHERE DE_DOKOD 	= '''+Query1.FieldByName('DO_KOD').AsString+''' ');
		ModLocate(Query1, FOMEZO, Query1.FieldByName(FOMEZO).AsString );
	end;
	Label3.Caption	:= '('+IntToStr(Query1.RecordCount)+')';
	Label3.Update;
end;

procedure TDolgozoUjFmDlg.ButtonKuldClick(Sender: TObject);
begin
	ret_vnev	:= Query1.FieldByName('DO_NAME').AsString;
	Inherited ButtonKuldClick(Sender);
end;

procedure TDolgozoUjFmDlg.BitBtn1Click(Sender: TObject);
var
   OldCursor: TCursor;
begin
  if EgyebDlg.DOLGOZO_ATAD_FORMA = 'SERVANTES' then begin
     Application.CreateForm(TFDolgServ, FDolgServ);
     FDolgServ.ShowModal;
     FDolgServ.Free;
     end;
  if EgyebDlg.DOLGOZO_ATAD_FORMA = 'KULCS' then begin
     Application.CreateForm(TKulcsDolgozoOlvasoDlg, KulcsDolgozoOlvasoDlg);
     KulcsDolgozoOlvasoDlg.ShowModal;
     KulcsDolgozoOlvasoDlg.Free;
     end;
 	 ModLocate(Query1, FOMEZO, Query1.FieldByName(FOMEZO).AsString );
end;

end.


