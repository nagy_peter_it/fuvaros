unit MegGeneralas;

interface

uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, DBTables, DB, StdCtrls, Buttons, Mask, DBCtrls, ExtCtrls,
  ADODB, DateUtils;

type
  TMeggeneralasDlg = class(TForm)
    Label1: TLabel;
    BitBtn6: TBitBtn;
    MaskEdit1: TMaskEdit;
	 MaskEdit2: TMaskEdit;
    Label2: TLabel;
    BitBtn10: TBitBtn;
    BitBtn1: TBitBtn;
    BitElkuld: TBitBtn;
    Query1: TADOQuery;
    Query2: TADOQuery;
    Label3: TLabel;
    Query3: TADOQuery;
    procedure FormCreate(Sender: TObject);
    procedure BitBtn6Click(Sender: TObject);
	 procedure MaskEdit1Click(Sender: TObject);
    procedure BitBtn10Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure MaskEdit1Exit(Sender: TObject);
    procedure MaskEdit2Exit(Sender: TObject);
    procedure BitElkuldClick(Sender: TObject);
	 procedure Generalas(dat, mbkod : string);
  private
  public
  		voltgeneralas	: boolean;
  end;

var
  MeggeneralasDlg: TMeggeneralasDlg;

implementation

uses
	Egyeb, Kozos, J_SQL;
{$R *.DFM}

procedure TMeggeneralasDlg.FormCreate(Sender: TObject);
begin
	EgyebDlg.SetADOQueryDatabase(Query1);
	EgyebDlg.SetADOQueryDatabase(Query2);
	EgyebDlg.SetADOQueryDatabase(Query3);
   // Alapb�l be�ll�tjuk a k�vetkez� h�napot
	MaskEdit1.Text := copy(DatumHozNap(copy(EgyebDlg.MaiDatum,1,8)+'01.', 31, false), 1, 8)+'01.';
  	EgyebDlg.ElsoNapEllenor(MaskEdit1, MaskEdit2);
   Label3.Caption	:= '';
   Label3.Update;
end;

procedure TMeggeneralasDlg.BitBtn6Click(Sender: TObject);
begin
	voltgeneralas	:= false;
	Close;
end;

procedure TMeggeneralasDlg.MaskEdit1Click(Sender: TObject);
begin
	with Sender as TMaskEdit do begin
  		SelectAll;
  	end;
end;

procedure TMeggeneralasDlg.BitBtn10Click(Sender: TObject);
begin
	// EgyebDlg.Calendarbe(MaskEdit1);
 	// EgyebDlg.ElsoNapEllenor(MaskEdit1, MaskEdit2);
  EgyebDlg.CalendarBe_idoszak(MaskEdit1, MaskEdit2);
end;

procedure TMeggeneralasDlg.BitBtn1Click(Sender: TObject);
begin
	EgyebDlg.Calendarbe(MaskEdit2, True);
end;

procedure TMeggeneralasDlg.MaskEdit1Exit(Sender: TObject);
begin
	DatumExit(MaskEdit1, false);
  	EgyebDlg.ElsoNapEllenor(MaskEdit1, MaskEdit2);
end;

procedure TMeggeneralasDlg.MaskEdit2Exit(Sender: TObject);
begin
	DatumExit(MaskEdit2, false);
end;

procedure TMeggeneralasDlg.BitElkuldClick(Sender: TObject);
var
	datum	: string;
   napszam	: integer;
   td		: TDateTime;
   napok	: integer;
begin
	if not Datumexit(MaskEdit1, true) then begin
       Exit;
   end;
	if not Datumexit(MaskEdit2, true) then begin
       Exit;
   end;
	// A d�tumok ellen�rz�se
	Query_Run(Query1, 'SELECT * FROM SABLON ORDER BY SB_MBKOD ');
   datum	:= MaskEdit1.Text;
	while datum <= MaskEdit2.Text do begin
   	Query1.First;
   	while not Query1.Eof do begin
   		Label3.Caption	:= datum + ' -> ' + Query1.FieldByName('SB_MBKOD').AsString;
       	Label3.Update;
           // Megn�zz�k, hogy kell-e gener�l�s
           case StrToIntDef(Query1.FieldByName('SB_MIKOR').AsString, -1) of
           	0 : 	// Naponta kell gener�lni
               	Generalas(datum, Query1.FieldByName('SB_MBKOD').AsString);
               1 :     // Heti gener�l�sa
               	begin
                       td 	:= GetDateFromStr(datum);
                   	if td <> 0 then begin
                   		napszam	:= DayOfTheWeek(td);
                     		napok	:= StrToIntDef(Query1.FieldByName('SB_MIKO2').AsString, 0);
                           case napszam of
                           	1 :
                                   if ( napok mod 10 ) > 0 then begin
               						Generalas(datum, Query1.FieldByName('SB_MBKOD').AsString);
									end;
                           	2 :
                                   if ( napok mod 100 ) > 1 then begin
               						Generalas(datum, Query1.FieldByName('SB_MBKOD').AsString);
                                   end;
                           	3 :
                                   if ( napok mod 1000 ) > 11 then begin
               						Generalas(datum, Query1.FieldByName('SB_MBKOD').AsString);
                                   end;
                           	4 :
                                   if ( napok mod 10000 ) > 111 then begin
               						Generalas(datum, Query1.FieldByName('SB_MBKOD').AsString);
                                   end;
                           	5 :
                                   if ( napok mod 100000 ) > 1111 then begin
               						Generalas(datum, Query1.FieldByName('SB_MBKOD').AsString);
                                   end;
                           	6 :
                                   if ( napok mod 1000000 ) > 11111 then begin
										Generalas(datum, Query1.FieldByName('SB_MBKOD').AsString);
                                   end;
                           	7 :
                                   if ( napok mod 10000000 ) > 111111 then begin
               						Generalas(datum, Query1.FieldByName('SB_MBKOD').AsString);
                                   end;
                           end;
                       end;
                   end;
               2 :		// Havi gener�l�sa
               	begin
                   	napszam	:= StrToIntDef(copy(datum,9,2),0);
                       if napszam = StrToIntDef(Query1.FieldByName('SB_MIKO2').AsString, -1) then begin
                       	Generalas(datum, Query1.FieldByName('SB_MBKOD').AsString);
                       end;
                   end;
           end;
           Query1.Next;
       end;
		datum	:= DatumHozNap(datum,1,false);
   end;
   Label3.Caption	:= '';
   Label3.Update;
	voltgeneralas	:= true;
   Close;
end;

procedure TMeggeneralasDlg.Generalas(dat, mbkod : string);
var
	ujkod	: string;
	mskod	: integer;
   feldat	: string;
   fetdat	: string;
   lerdat	: string;
   letdat	: string;
   akdat	: string;
   dkul	: integer;
   tinev	: string;
	statusz	: integer;
	rensz	: string;
	potsz	: string;
	dokod	: string;
	doko2	: string;
	gkkat	: string;
	snev1	: string;
	snev2	: string;
  msid: string;
begin
	// El�sz�r ellen�rizz�k, hogy az adott d�tummal m�r gener�ltunk-e
   Query_Run(Query2, 'SELECT * FROM MEGBIZAS WHERE MB_SABLO = ''G'' AND MB_DATUM = '''+dat+''' AND MB_SAKOD = '+mbkod);
   if Query2.RecordCount > 0 then begin
   	// M�r l�tezik a legener�land� elem
       Exit;
   end;
	// Legener�ljuk a megfelel� megb�z�st
   Query_Run(Query3, 'SELECT * FROM MEGBIZAS WHERE MB_MBKOD = '+mbkod);
   if Query3.RecordCount < 1  then begin
   	// Nem l�tezik az eredeti elem
		Exit;
   end;
	ujkod := IntToStr(GetNextCode('MEGBIZAS', 'MB_MBKOD', 1, 0));
	rensz	:= Query3.FieldByName('MB_RENSZ').AsString;
	potsz	:= Query3.FieldByName('MB_POTSZ').AsString;
	dokod	:= Query3.FieldByName('MB_DOKOD').AsString;
	doko2	:= Query3.FieldByName('MB_DOKO2').AsString;
	gkkat	:= Query3.FieldByName('MB_GKKAT').AsString;
	snev1	:= Query3.FieldByName('MB_SNEV1').AsString;
	snev2	:= Query3.FieldByName('MB_SNEV2').AsString;
	Query_Insert (Query2, 'MEGBIZAS', [
		'MB_MBKOD', ujkod,
		'MB_VEKOD', ''''+Query3.FieldByName('MB_VEKOD').AsString+'''',
		'MB_POZIC', ''''+Query3.FieldByName('MB_POZIC').AsString+'''',
		'MB_ALVAL', ''''+Query3.FieldByName('MB_ALVAL').AsString+'''',
		'MB_RENSZ', ''''+rensz+'''',
		'MB_POTSZ', ''''+potsz+'''',
		'MB_DOKOD', ''''+dokod+'''',
		'MB_DOKO2', ''''+doko2+'''',
		'MB_FUDIJ', SqlSzamString(StringSzam(Query3.FieldByName('MB_FUDIJ').AsString),12,2),
		'MB_FUNEM', ''''+Query3.FieldByName('MB_FUNEM').AsString+'''',
       'MB_ALDIJ', SqlSzamString(StringSzam(Query3.FieldByName('MB_ALDIJ').AsString),12,2),
       'MB_ALNEM', ''''+Query3.FieldByName('MB_ALNEM').AsString+'''',
       'MB_STATU', ''''+'100'+'''',
       'MB_DATUM', ''''+dat+'''',
		'MB_MEGJE', ''''+Query3.FieldByName('MB_MEGJE').AsString+'''',
       'MB_EXPOR', Query3.FieldByName('MB_EXPOR').AsString,
       'MB_VENEV', ''''+Query3.FieldByName('MB_VENEV').AsString+'''',
		'MB_GKKAT', ''''+gkkat+'''',
       'MB_SABLO', ''''+'G'+'''',
       'MB_ALNEV', ''''+Query3.FieldByName('MB_ALNEV').AsString+'''',
		'MB_SNEV1', ''''+snev1+'''',
		'MB_SNEV2', ''''+snev2+'''',
		'MB_EXSTR', ''''+Query3.FieldByName('MB_EXSTR').AsString+'''',
		'MB_SOSFL', '0',
		'MB_SOSKM', '0',
		'MB_SAKOD', mbkod
		] );
	// A felrak�sok / lerak�sok gener�l�sa
   Query_Run(Query3, 'SELECT * FROM MEGSEGED WHERE MS_MBKOD = '+mbkod);
   while not Query3.Eof do begin
   	feldat	:= dat;
       fetdat	:= '';
       if Query3.FieldByName('MS_FETDAT').AsString <> '' then begin
			dkul	:=	DatumKul(Query3.FieldByName('MS_FETDAT').AsString, Query3.FieldByName('MS_FELDAT').AsString);
           if dkul > -1 then begin
       		fetdat	:= DatumHozNap(dat, dkul, true);
           end;
		end;
       lerdat	:= '';
       if Query3.FieldByName('MS_LERDAT').AsString <> '' then begin
       	dkul	:=	DatumKul(Query3.FieldByName('MS_LERDAT').AsString, Query3.FieldByName('MS_FELDAT').AsString);
			if dkul > -1 then begin
       		lerdat	:= DatumHozNap(dat, dkul, true);
           end;
		end;
		letdat	:= '';
		if Query3.FieldByName('MS_LETDAT').AsString <> '' then begin
       	dkul	:=	DatumKul(Query3.FieldByName('MS_LETDAT').AsString, Query3.FieldByName('MS_FELDAT').AsString);
           if dkul > -1 then begin
       		letdat	:= DatumHozNap(dat, dkul, true);
           end;
		end;
       if Query3.FieldByName('MS_TIPUS').AsString = '0' then begin
       	// Felrak�s
           akdat	:= feldat;
           if fetdat <> '' then begin
               akdat	:= fetdat;
           end;
       end else begin
       	// Lerak�s
           akdat	:= lerdat;
           if letdat <> '' then begin
               akdat	:= letdat;
           end;
       end;
		mskod 	:= GetNextCode('MEGSEGED', 'MS_MSKOD', 1, 0);
       tinev	:= EgyebDlg.felrakas_str;
       if Query3.FieldByName('MS_TIPUS').AsString = '1' then begin
       	tinev	:= EgyebDlg.lerakas_str;
       end;
       Query_Insert (Query2, 'MEGSEGED', [
       	'MS_MSKOD',		IntToStr(mskod),
       	'MS_MBKOD', 	ujkod,
			'MS_SORSZ', 	Query3.FieldByName('MS_SORSZ').AsString,
           'MS_FELNEV', 	''''+Query3.FieldByName('MS_FELNEV').AsString+'''',
           'MS_FELIR', 	''''+Query3.FieldByName('MS_FELIR').AsString+'''',
           'MS_FELTEL', 	''''+Query3.FieldByName('MS_FELTEL').AsString+'''',
           'MS_FELCIM', 	''''+Query3.FieldByName('MS_FELCIM').AsString+'''',
           'MS_FELSE1',	''''+Query3.FieldByName('MS_FELSE1').AsString+'''',
           'MS_FELSE2',	''''+Query3.FieldByName('MS_FELSE2').AsString+'''',
           'MS_FELSE3',	''''+Query3.FieldByName('MS_FELSE3').AsString+'''',
           'MS_FELSE4',	''''+Query3.FieldByName('MS_FELSE4').AsString+'''',
           'MS_FELSE5',	''''+Query3.FieldByName('MS_FELSE5').AsString+'''',
           'MS_FELDAT', 	''''+feldat+'''',
			'MS_FELIDO', 	''''+Query3.FieldByName('MS_FELIDO').AsString+'''',
			'MS_FETDAT', 	''''+fetdat+'''',
           'MS_FETIDO', 	''''+Query3.FieldByName('MS_FETIDO').AsString+'''',
           'MS_FSULY', 	SqlSzamString(StringSzam(Query3.FieldByName('MS_FSULY').AsString),8,2),
           'MS_FEPAL', 	SqlSzamString(StringSzam(Query3.FieldByName('MS_FEPAL').AsString),8,2),
           'MS_LERNEV', 	''''+Query3.FieldByName('MS_LERNEV').AsString+'''',
           'MS_LELIR', 	''''+Query3.FieldByName('MS_LELIR').AsString+'''',
           'MS_LERTEL', 	''''+Query3.FieldByName('MS_LERTEL').AsString+'''',
           'MS_LERCIM', 	''''+Query3.FieldByName('MS_LERCIM').AsString+'''',
           'MS_LERSE1',	''''+Query3.FieldByName('MS_LERSE1').AsString+'''',
           'MS_LERSE2',	''''+Query3.FieldByName('MS_LERSE2').AsString+'''',
           'MS_LERSE3',	''''+Query3.FieldByName('MS_LERSE3').AsString+'''',
           'MS_LERSE4',	''''+Query3.FieldByName('MS_LERSE4').AsString+'''',
			'MS_LERSE5',	''''+Query3.FieldByName('MS_LERSE5').AsString+'''',
           'MS_LERDAT', 	''''+lerdat+'''',
           'MS_LERIDO', 	''''+Query3.FieldByName('MS_LERIDO').AsString+'''',
           'MS_LETDAT', 	''''+letdat+'''',
			'MS_LETIDO', 	''''+Query3.FieldByName('MS_LETIDO').AsString+'''',
			'MS_FORSZ',		''''+Query3.FieldByName('MS_FORSZ').AsString+'''',
			'MS_ORSZA',		''''+Query3.FieldByName('MS_ORSZA').AsString+'''',
			'MS_EXPOR',		Query3.FieldByName('MS_EXPOR').AsString,
			'MS_EXSTR', 	''''+Query3.FieldByName('MS_EXSTR').AsString+'''',
			'MS_LSULY', 	SqlSzamString(StringSzam(Query3.FieldByName('MS_LSULY').AsString),8,2),
			'MS_LEPAL', 	SqlSzamString(StringSzam(Query3.FieldByName('MS_LEPAL').AsString),8,2),
			'MS_DATUM',     ''''+akdat+'''',
			'MS_IDOPT',     ''''+Query3.FieldByName('MS_IDOPT').AsString+'''',
			'MS_TIPUS', 	Query3.FieldByName('MS_TIPUS').AsString,
			'MS_TEIDO', 	''''+Query3.FieldByName('MS_TEIDO').AsString+'''',
			'MS_TINEV',		''''+tinev+'''',
			'MS_AKTNEV', 	''''+Query3.FieldByName('MS_AKTNEV').AsString+'''',
			'MS_AKTTEL', 	''''+Query3.FieldByName('MS_AKTTEL').AsString+'''',
			'MS_AKTCIM', 	''''+Query3.FieldByName('MS_AKTCIM').AsString+'''',
			'MS_AKTOR', 	''''+Query3.FieldByName('MS_AKTOR').AsString+'''',
			'MS_AKTIR', 	''''+Query3.FieldByName('MS_AKTIR').AsString+'''',
			'MS_RENSZ', 	''''+rensz+'''',
			'MS_POTSZ', 	''''+potsz+'''',
			'MS_DOKOD', 	''''+dokod+'''',
			'MS_DOKO2', 	''''+doko2+'''',
			'MS_GKKAT', 	''''+gkkat+'''',
			'MS_SNEV1', 	''''+snev1+'''',
			'MS_SNEV2', 	''''+snev2+'''',
			'MS_KIFON', 	Query3.FieldByName('MS_KIFON').AsString,
			'MS_FETED', 	''''+Query3.FieldByName('MS_FETED').AsString+'''',
			'MS_FETEI', 	''''+Query3.FieldByName('MS_FETEI').AsString+'''',
			'MS_LETED', 	''''+Query3.FieldByName('MS_LETED').AsString+'''',
			'MS_LETEI', 	''''+Query3.FieldByName('MS_LETEI').AsString+'''',
			'MS_FELARU', 	''''+Query3.FieldByName('MS_FELARU').AsString+''''
		] );
		// A st�tuszok be�ll�t�sa

    msid:= Query_Select('MEGSEGED', 'MS_MSKOD', mskod, 'MS_ID');
		statusz	:= GetFelrakoStatus(msid);
		SetFelrakoStatus(msid, statusz);
		Query3.Next;
	end;
end;

end.


