unit JogForm;

interface

uses
	SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
	Forms, Dialogs, StdCtrls, Buttons, Mask, DB, DBTables, Grids,
  ComCtrls, ExtCtrls, Spin, checklst, ADODB;

type
	TJogFormDlg = class(TForm)
    GroupBox3: TGroupBox;
    FelhGrid: TStringGrid;
    Query1: TADOQuery;
    Query2: TADOQuery;
    Panel1: TPanel;
    Label10: TLabel;
    ComboFelh: TComboBox;
    Panel2: TPanel;
    GroupBox1: TGroupBox;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    BitBtn3: TBitBtn;
    BitBtn4: TBitBtn;
    BitElkuld: TBitBtn;
    BitKilep: TBitBtn;
    BitBtn5: TBitBtn;
    Panel3: TPanel;
    JogGrid: TStringGrid;
	  procedure BitKilepClick(Sender: TObject);
	  procedure FormCreate(Sender: TObject);
    procedure BitElkuldClick(Sender: TObject);
    procedure ComboFelhChange(Sender: TObject);
    procedure JogTolto;
    procedure Mentes;
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure BitBtn4Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure BitBtn5Click(Sender: TObject);
	  private
    		modosult 		: boolean;
           utolsoindex		: integer;
	  public
	end;

var
	JogFormDlg: TJogFormDlg;

implementation

uses
	Egyeb, J_SQL, Kozos, FelForm;

{$R *.DFM}

procedure TJogFormDlg.BitKilepClick(Sender: TObject);
begin
	if modosult then begin
		if NoticeKi( 'M�dosultak az adatok. Elmenti a v�ltoztat�sokat?', NOT_QUESTION ) = 0 then begin
    		Mentes;
   	end;
  	end;
	Close;
end;

procedure TJogFormDlg.FormCreate(Sender: TObject);
var
	i       : integer;
   sorsz   : integer;
begin
	EgyebDlg.SeTADOQueryDatabase(Query1);
	EgyebDlg.SeTADOQueryDatabase(Query2);
  	ComboFelh.Clear;
  	FelhGrid.RowCount := 1;
  	if not Query_Run(Query1,'SELECT * from jelszo where JE_VALID > 0 ORDER  BY JE_FENEV') then begin
  		Exit;
  	end;
  	Query1.First;
  	i	    := 1;
   sorsz   := 0;
  	while not Query1.EOF do begin
  		FelhGrid.RowCount := i;
     	FelhGrid.Cells[0,i-1] := Query1.FieldByName('JE_FEKOD').AsString;
     	FelhGrid.Cells[1,i-1] := Query1.FieldByName('JE_FENEV').AsString;
       if Query1.FieldByName('JE_FENEV').AsString = EgyebDlg.user_name then begin
           sorsz   := i-1;
       end;
     	FelhGrid.Cells[2,i-1] := Query1.FieldByName('JE_SUPER').AsString;
     	if Query1.FieldByName('JE_SUPER').AsInteger > 0 then begin
     		FelhGrid.Cells[1,i-1] := FelhGrid.Cells[1,i-1] + '(Rendszergazda)'
     	end;
  		Query1.Next;
     	Inc(i);
  	end;
  	ComboFelh.Items.Assign(FelhGrid.Cols[1]);
   // Keress�k meg a bejelentkez� usert
  	ComboFelh.ItemIndex 	:= sorsz;
  	modosult 				:= false;

  	// A JogGrid cimk�inek be�ll�t�sa
  	JogGrid.Cells[0,0] 		:= 'K�d';
  	JogGrid.Cells[1,0] 		:= 'Elnevez�s';
  	JogGrid.Cells[2,0] 		:= 'Jog';
//  	JogGrid.ColWidths[0] 	:= -1;
   utolsoindex				:= 0;
end;

procedure TJogFormDlg.BitElkuldClick(Sender: TObject);
begin
	Mentes;
	modosult := false;
end;

procedure TJogFormDlg.ComboFelhChange(Sender: TObject);
begin
	if modosult then begin
		if NoticeKi( 'M�dosultak az adatok. Elmenti a v�ltoztat�sokat?', NOT_QUESTION ) = 0 then begin
      	   	ComboFelh.ItemIndex := utolsoindex ;
     		Mentes;
     	end;
  	end;
	JogTolto;
end;

procedure TJogFormDlg.JogTolto;
var
	i 			: integer;
begin
	// A felhaszn�l�hoz kapcsol�d� jogosults�gok bet�lt�se
  if not Query_Run(Query1,'SELECT * from Dialogs ORDER BY DL_DLKOD') then begin
  		Exit;
  end;
  Query1.First;
  i := 1;
  while not Query1.EOF do begin
  		JogGrid.RowCount 		:= i+1;
     	JogGrid.Cells[0,i] 	:= Query1.FieldByName('DL_DLKOD').AsString;
     	JogGrid.Cells[1,i] 	:= Query1.FieldByName('DL_LEIRS').AsString;
     	JogGrid.Cells[2,i] 	:= 'Teljes';
  		Query1.Next;
     	Inc(i);
  end;

  for i := 1 to JogGrid.RowCount - 1 do begin
  		if not Query_Run(Query1,'SELECT * from jogos where JO_DIKOD = '+JogGrid.Cells[0,i]+
     		' AND JO_FEKOD = '''+FelhGrid.Cells[0,ComboFelh.ItemIndex]+''' ') then begin
        	Exit;
     	end;
     	if Query1.RecordCount > 0 then begin
     		case Query1.FieldByName('JO_JOGOS').AsInteger of
        		1: // Nincs jog
           		begin
     					JogGrid.Cells[2,i] 	:= 'Nincs jog';
            	end;
           	2: // Csak olvas�s
           		begin
     					JogGrid.Cells[2,i] 	:= 'Csak olvas�s';
            	end;
           	3: // M�dos�t�s
           		begin
     					JogGrid.Cells[2,i] 	:= 'M�dos�t�s';
            	end;
        	end;
     	end;
  	end;

  	if UpperCase(FelhGrid.Cells[2,ComboFelh.ItemIndex]) = 'TRUE' then begin
     	BitBtn1.Enabled  := false;
     	BitBtn2.Enabled 	:= false;
     	BitBtn3.Enabled 	:= false;
     	BitBtn4.Enabled 	:= false;
      BitElkuld.Enabled := false;
  	end else begin
     	BitBtn1.Enabled 	:= true;
     	BitBtn2.Enabled 	:= true;
     	BitBtn3.Enabled 	:= true;
     	BitBtn4.Enabled 	:= true;
      BitElkuld.Enabled := true;
  	end;
   modosult	:= false;
   utolsoindex	:= ComboFelh.ItemIndex;
end;

procedure TJogFormDlg.BitBtn1Click(Sender: TObject);
var
	i : integer;
begin
	for i := JogGrid.Selection.Top to JogGrid.Selection.Bottom do begin
  		JogGrid.Cells[2,i] 	:= 'Nincs jog';
  	end;
  	Modosult := true;
end;

procedure TJogFormDlg.BitBtn2Click(Sender: TObject);
var
	i : integer;
begin
	for i := JogGrid.Selection.Top to JogGrid.Selection.Bottom do begin
     JogGrid.Cells[2,i] 	:= 'Csak olvas�s';
  	end;
	Modosult := true;
end;

procedure TJogFormDlg.BitBtn3Click(Sender: TObject);
var
	i : integer;
begin
	for i := JogGrid.Selection.Top to JogGrid.Selection.Bottom do begin
     	JogGrid.Cells[2,i] 	:= 'M�dos�t�s';
  	end;
  	Modosult := true;
end;

procedure TJogFormDlg.BitBtn4Click(Sender: TObject);
var
	i : integer;
begin
	for i := JogGrid.Selection.Top to JogGrid.Selection.Bottom do begin
     	JogGrid.Cells[2,i] 	:= 'Teljes';
  	end;
  	Modosult := true;
end;

procedure TJogFormDlg.FormShow(Sender: TObject);
begin
	JogTolto;
  	// Jogosults�gok lekezel�se
	//  EgyebDlg.SetJogok2(Tag, [BitBtn1, BitBtn2, BitBtn3, BitBtn4]);
end;

procedure TJogFormDlg.Mentes;
var
	i 				: integer;
  	jogszam		: integer;
begin
	// Elmenti a be�ll�tott adatokat
  	// A megl�v� elemek t�rl�se
  	if not Query_Run(Query1,'DELETE FROM JOGOS WHERE JO_FEKOD = '''+FelhGrid.Cells[0,ComboFelh.ItemIndex] +''' ') then begin
  		Exit;
  	end;
  	// Az �j elemek felvitele
	for i := 1 to JogGrid.RowCount - 1 do begin
  		jogszam := RG_ALLRIGHT;
  		if UpperCase(JogGrid.Cells[2,i]) <> 'TELJES' then begin
     		if JogGrid.Cells[2,i] = 'Nincs jog' then begin
        		Jogszam	:= RG_NORIGHT;
        	end;
     		if JogGrid.Cells[2,i] = 'Csak olvas�s' then begin
        		Jogszam	:= RG_READONLY;
        	end;
     		if JogGrid.Cells[2,i] = 'M�dos�t�s' then begin
        		Jogszam	:= RG_MODIFY;
        	end;
  			if not Query_Run(Query1,'INSERT INTO JOGOS (JO_DIKOD, JO_FEKOD, JO_JOGOS) '+
        		' VALUES ('+JogGrid.Cells[0,i] +', '''+FelhGrid.Cells[0,ComboFelh.ItemIndex]+''', '+IntToStr(jogszam)+') ') then begin
  				Exit;
  			end;
     	end;
  	end;
	NoticeKi( 'A jogosults�gok t�rol�sa megt�rt�nt!', NOT_MESSAGE );
end;

procedure TJogFormDlg.BitBtn5Click(Sender: TObject);
var
	sup		: string;
  fekod_honnan, fekod_hova: string;
  S: string;
begin
	// Bet�lt�nk egy m�sik felhaszn�l�t, �s annak a jogait adjuk ide
   if FelhGrid.Cells[2,ComboFelh.ItemIndex] = '1' then begin
		NoticeKi('Supervisor felhaszn�l� jogait nem v�ltoztathatjuk meg!');
       Exit;
   end;
   Application.CreateForm(TFelFormDlg, FelFormDlg);
   FelFormDlg.valaszt		:= true;
   FelFormDlg.ShowModal;
   if FelFormDlg.ret_vkod <> '' then begin
		// Megn�zz�k, hogy ne legyen supervisor
       sup		:= Query_Select ('JELSZO', 'JE_FEKOD', FelFormDlg.ret_vkod , 'JE_SUPER');
       fekod_honnan	:= FelFormDlg.ret_vkod;
   end;
   FelFormDlg.Destroy;
   if sup = '1' then begin
		NoticeKi('Supervisor felhaszn�l� jogai nem �r�k�thet�k!');
       Exit;
   end;
   if NoticeKi('Val�ban �r�k�teni szeretn� a kiv�lasztott felhaszn�l� jogait �s t�bl�zat be�ll�t�sait?', NOT_QUESTION) = 0 then begin
    fekod_hova:= FelhGrid.Cells[0,ComboFelh.ItemIndex];
   	Query_Run(Query1, 'DELETE FROM JOGOS WHERE JO_FEKOD = '''+fekod_hova+''' ');
   	{ Query_Run(Query1, 'SELECT * FROM JOGOS WHERE JO_FEKOD = '''+fekod+''' ');
       while not Query1.Eof do begin
       	Query_Run(Query2, 'INSERT INTO JOGOS ( JO_DIKOD, JO_FEKOD, JO_JOGOS ) VALUES ( '+
           	Query1.FieldByName('JO_DIKOD').AsString +', '''+FelhGrid.Cells[0,ComboFelh.ItemIndex]+''','+
               Query1.FieldByName('JO_JOGOS').AsString+' ) ' );
       	Query1.Next;
       end; }
    S:='insert into jogos (JO_DIKOD, JO_FEKOD, JO_JOGOS) select JO_DIKOD, '''+fekod_hova+''',';
    S:=S+' JO_JOGOS from jogos where JO_FEKOD='''+fekod_honnan+'''';
    Query_Run(Query1, S);

    Query_Run(Query1, 'DELETE FROM MEZOK WHERE MZ_FEKOD = '''+fekod_hova+''' ');
    S:='insert into MEZOK (MZ_DIKOD, MZ_FEKOD, MZ_SORSZ, MZ_TIPUS, MZ_MZNEV, MZ_MKIND, MZ_LABEL, MZ_WIDTH, MZ_MSIZE, MZ_VISIB, ';
    S:=S+' MZ_MZTAG, MZ_ALIGN, MZ_MEGJE, MZ_FORMA, MZ_HIDEN, MZ_GROUP) ';
    S:=S+' select MZ_DIKOD, '''+fekod_hova+''', MZ_SORSZ, MZ_TIPUS, MZ_MZNEV, MZ_MKIND, MZ_LABEL, MZ_WIDTH, MZ_MSIZE, MZ_VISIB, ';
    S:=S+' MZ_MZTAG, MZ_ALIGN, MZ_MEGJE, MZ_FORMA, MZ_HIDEN, MZ_GROUP from MEZOK where MZ_FEKOD='''+fekod_honnan+'''';
    Query_Run(Query1, S);
   end;
   JogTolto;
end;

end.
