unit KtsgDel;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls, ADODB;

type
  TFKtsgDel = class(TForm)
    ComboBox1: TComboBox;
    DateTimePicker1: TDateTimePicker;
    DateTimePicker2: TDateTimePicker;
    Button1: TButton;
    Button2: TButton;
    ADOCommand1: TADOCommand;
    procedure Button2Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FKtsgDel: TFKtsgDel;

implementation

uses Egyeb, J_SQL;

{$R *.dfm}

procedure TFKtsgDel.Button2Click(Sender: TObject);
begin
  close
  
end;

procedure TFKtsgDel.Button1Click(Sender: TObject);
var
  S: string;
begin
  if ComboBox1.ItemIndex=0 then
    exit;

  if MessageBox(0, 'Mehet a t�rl�s?', '', MB_ICONQUESTION or MB_YESNO or MB_DEFBUTTON2)=IDNO then
    exit;

  Try
    // ADOCommand1.CommandText:='ALTER TABLE KOLTSEG DISABLE TRIGGER KOLTSEG_trd_UZA';
    S:='ALTER TABLE KOLTSEG DISABLE TRIGGER KOLTSEG_trd_UZA';
    // ShowMessage(ADOCommand1.CommandText);
    // ADOCommand1.Execute;
    Command_Run(ADOCommand1, S, true);

    // ADOCommand1.CommandText:='delete from koltseg where ks_ktip='+IntToStr(ComboBox1.ItemIndex)+' and ks_datum>='''+DateToStr( DateTimePicker1.Date)+''' and ks_datum<='''+DateToStr( DateTimePicker2.Date)+'''';
    S:='delete from koltseg where ks_ktip='+IntToStr(ComboBox1.ItemIndex)+' and ks_datum>='''+DateToStr( DateTimePicker1.Date)+''' and ks_datum<='''+DateToStr( DateTimePicker2.Date)+'''';
    // ShowMessage(ADOCommand1.CommandText);
    // ADOCommand1.Execute;
    Command_Run(ADOCommand1, S, true);

    // ADOCommand1.CommandText:='ALTER TABLE KOLTSEG ENABLE TRIGGER KOLTSEG_trd_UZA';
    S:='ALTER TABLE KOLTSEG ENABLE TRIGGER KOLTSEG_trd_UZA';
    // ShowMessage(ADOCommand1.CommandText);
    // ADOCommand1.Execute;
    Command_Run(ADOCommand1, S, true);

    // ADOCommand1.CommandText:='delete from koltseg_gyujto where ks_ktip='+IntToStr(ComboBox1.ItemIndex)+' and ks_datum>='''+DateToStr( DateTimePicker1.Date)+''' and ks_datum<='''+DateToStr( DateTimePicker2.Date)+'''';
    S:='delete from koltseg_gyujto where ks_ktip='+IntToStr(ComboBox1.ItemIndex)+' and ks_datum>='''+DateToStr( DateTimePicker1.Date)+''' and ks_datum<='''+DateToStr( DateTimePicker2.Date)+'''';
    // ShowMessage(ADOCommand1.CommandText);
    // ADOCommand1.Execute;
    Command_Run(ADOCommand1, S, true);

    ShowMessage('K�sz!');
  Except
    ShowMessage('HIBA');
  End;
end;

end.
