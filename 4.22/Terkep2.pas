unit Terkep2;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OleCtrls, SHDocVw,MSHTML, ExtCtrls, StdCtrls, Mask, DBCtrls, cefvcl, ceflib;

const
   WM_ONSHOWCALLBACK = WM_USER + 565;

type
  TFTerkep2 = class(TForm)
    Button1: TButton;
    Panel1: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    DBText1: TDBText;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    Panel2: TPanel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Edit2: TEdit;
    Edit1: TEdit;
    Chromium1: TChromium;
    pnlStatus: TPanel;
    procedure FormCreate(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    // procedure WebBrowser1ProgressChange(Sender: TObject; Progress,
    //  ProgressMax: Integer);
    procedure Button1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Chromium1ConsoleMessage(Sender: TObject;
      const browser: ICefBrowser; const message, source: ustring; line: Integer;
      out Result: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FTerkep2: TFTerkep2;
  done: boolean;

implementation

uses Terkep;

{$R *.dfm}

procedure TFTerkep2.FormCreate(Sender: TObject);
// var
//  aStream     : TMemoryStream;
begin
   {
    WebBrowser1.Navigate('about:blank');
    while WebBrowser1.ReadyState < READYSTATE_INTERACTIVE do Application.ProcessMessages;
    if Assigned(WebBrowser1.Document) then
    begin
      aStream := TMemoryStream.Create;
    end;
    }
end;

procedure TFTerkep2.Chromium1ConsoleMessage(Sender: TObject;
  const browser: ICefBrowser; const message, source: ustring; line: Integer;
  out Result: Boolean);
begin
   pnlStatus.Caption:=message;
   pnlStatus.Refresh;
   Result:= True;
end;

procedure TFTerkep2.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  // WebBrowser1.Stop;
  Chromium1.Browser.StopLoad;
end;

{
procedure TFTerkep2.WebBrowser1ProgressChange(Sender: TObject; Progress,
  ProgressMax: Integer);
begin
 if ProgressMax = 0 then
  begin
    Caption := '';
    Exit;
  end;
  try
    if (Progress <> -1) and (Progress <= ProgressMax) then
      Caption :='Bet�lt�s... '+ IntToStr((Progress * 100) div ProgressMax) + '%'
    else
      Caption := '';
  except
    on EDivByZero do Exit;
  end;
  Application.ProcessMessages;

end;
 }

procedure TFTerkep2.Button1Click(Sender: TObject);
begin
  close;
end;

procedure TFTerkep2.FormShow(Sender: TObject);
begin
   if not done then begin
      // I want to have the application show its window, then start loading the webpage.
      PostMessage(Handle, WM_ONSHOWCALLBACK, 0, 0);
      done := true;
   end;
   while Chromium1.Browser.IsLoading do Application.ProcessMessages;

end;

end.
