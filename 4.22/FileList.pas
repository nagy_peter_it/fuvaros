unit FileList;

interface

uses
	WinTypes, WinProcs, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
	StdCtrls, ExtCtrls, Lgauge, quickrpt, Qrctrls, printers, ShellAPI, qrprntr,
  Db, DBTables;
type
	TFileListDlg = class(TForm)
    SorBand: TQRBand;
    Rep: TQuickRep;
    SorLabel: TQRLabel;
    FejlecBand: TQRChildBand;
    CimLabel1: TQRLabel;
    OldalFejlecBand: TQRBand;
    QRLabel5: TQRLabel;
    QRSysData4: TQRSysData;
    QRLabel6: TQRLabel;
    QRFocim1: TQRLabel;
    CimLabel2: TQRLabel;
    CimLabel3: TQRLabel;
    CimLabel4: TQRLabel;
    CimLabel5: TQRLabel;
    CimLabel6: TQRLabel;
    CimLabel7: TQRLabel;
    CimLabel8: TQRLabel;
    CimLabel9: TQRLabel;
    CimLabel10: TQRLabel;
    QRShape1: TQRShape;
    QRLabel1: TQRLabel;
    ChildBand1: TQRChildBand;
    QRShape3: TQRShape;
    QRLabel2: TQRLabel;
    LabLecBand: TQRBand;
    LabLabel1: TQRLabel;
    LabLabel2: TQRLabel;
    LabLabel3: TQRLabel;
    LabLabel4: TQRLabel;
    LabLabel5: TQRLabel;
    LabLabel6: TQRLabel;
    LabLabel7: TQRLabel;
    LabLabel8: TQRLabel;
    LabLabel9: TQRLabel;
    LabLabel10: TQRLabel;
	procedure FormCreate(Sender: TObject);
    procedure RepNeedData(Sender: TObject; var MoreData: Boolean);
    procedure SorBandAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure FejlecBandBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure OldalFejlecBandBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure FormDestroy(Sender: TObject);
    procedure ChildBand1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure RepBeforePrint(Sender: TCustomQuickRep;
      var PrintReport: Boolean);
    procedure LabLecBandBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
	private
		aFile : Text;
		fileszam			: integer;
		newPageFlag 	: boolean;
		byteszam			: longint;
		filehossz		: longint;
     elso				: boolean;
     lapszam			: integer;
		fejsorszam 		: integer;
     sorszam			: integer;
		labsorszam 		: integer;
	public
	  repfilename 		: string;
	  focim				: string;
     cegnev				: string;
     kezdofej			: boolean;
     fejlecek			: TStringList;
     lablecek 			: TStringList;
     vanvonal			: boolean;
     vanszuro			: boolean;
     vanlablec			: boolean;
	end;

var
	FileListDlg: TFileListDlg;

implementation

uses
	Egyeb, J_SQL;

{$R *.DFM}

procedure TFileListDlg.FormCreate(Sender: TObject);
begin
	newPageFlag	:= false;
	repfilename := '';
	focim 		:= '';
  cegnev		:= '';
  elso 			:= true;
  kezdofej		:= true;
  lapszam 		:= 0;
  sorszam 		:= 0;
  fejlecek 	:= TStringList.Create;
  lablecek 	:= TStringList.Create;
  vanvonal		:= true;
  vanszuro		:= false;
 	QrLabel2.Caption := EgyebDlg.Read_SZGrid( '999', '100' );
end;

procedure TFileListDlg.RepNeedData(Sender: TObject; var MoreData: Boolean);
var
	Line : String;
begin
  MoreData:=False;
  if filehossz = 0 then begin
  		filehossz := 1;
  end;
	FormGaugeDlg.Pozicio ( ( byteszam * 100 ) div filehossz ) ;
  	if FormGaugeDlg.kilepes  or eof(aFile) then begin
  	 	CloseFile(aFile);
   	FormGaugeDlg.Destroy;
      Exit;
  	end;
	if not eof(aFile) then begin
		Readln(aFile,Line);
		byteszam := byteszam + Length(Line)+2;
		if Pos(Chr(12), Line) > 0 then begin
			Line[Pos(Chr(12), Line)] := ' ';
			Readln(aFile,Line);
			byteszam := byteszam + Length(Line)+2;
//        OldalFejlecBand.ForceNewPage 	 := true;
        SorBand.ForceNewPage 	 := true;
		end;
	   SorLabel.Caption:=Line;
	   MoreData:=True;
	end;
end;

procedure TFileListDlg.SorBandAfterPrint(Sender: TQRCustomBand;
  BandPrinted: Boolean);
begin
//  OldalFejlecBand.ForceNewPage 	:= false;
  SorBand.ForceNewPage 	:= false;
end;

procedure TFileListDlg.FejlecBandBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
 	PrintBand := true;
  // A fejl�c sorok megl�t�nek ellen�rz�se �s a fejl�c sz�ks�gess�ge
	if ( ( ( not kezdofej ) and ( lapszam = 1 ) ) or ( fejlecek.Count < 1 ) ) then begin
  	PrintBand := false;
     Exit;
  end;
  QrShape1.Visible := VanVonal;
  if not vanvonal then begin
  	QrShape1.Width := 0;
  end;
end;

procedure TFileListDlg.OldalFejlecBandBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
	Inc(lapszam);
  QrLabel1.Caption := EgyebDlg.MaiDatum;
end;

procedure TFileListDlg.FormDestroy(Sender: TObject);
begin
	fejlecek.Free;
   lablecek.Free;
end;

procedure TFileListDlg.ChildBand1BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
	PrintBand := false;
	inc(sorszam);
	if sorszam = fejsorszam then begin
		PrintBand 	:= true;
     sorszam 		:= 0;
  end;
  PrintBand := PrintBand and vanvonal;
end;

procedure TFileListDlg.RepBeforePrint(Sender: TCustomQuickRep;
  var PrintReport: Boolean);
var
	aOpenDialog : TOpenDialog;
  i           : integer;
begin
	if repfilename = '' then begin
		aOpenDialog:=TopenDialog.Create(self);
		with aOpenDialog do begin
			Filter:='Pascal files (*.PAS)|*.PAS|Text files (*.TXT)|*.TXT';
			Options:=[ofFileMustExist];
			if not execute then begin
           Printer.Abort;
			end else  begin
				AssignFile(aFile,Filename);
				Reset(afile);
				Rep.ReportTitle:=Filename;
			end;
			Free;
		end;
	end else begin
		filehossz := 1;
		fileszam := FileOpen( repfilename, fmOpenRead );
		if fileszam > 0 then begin
			filehossz := FileSeek(fileszam,0,2) + 1;
			FileClose(fileszam);
		end;
		AssignFile(aFile,repfilename);
		Reset(afile);
		Rep.ReportTitle:=repfilename;
	end;

 	if ( ( Rep.Page.Orientation = poLandscape ) and (elso) ) then begin
  		elso := false;
  		QRLabel5.Left 		:= QRLabel5.Left + 340;
  		QRLabel6.Left 		:= QRLabel6.Left + 340;
     	QRLabel1.Left 		:= QRLabel1.Left + 340;
     	QRSysData4.Left 	:= QRSysData4.Left + 340;
  		QRFocim1.Width 	:= QRFocim1.Width + 340;
     	QRShape1.Width 	:= QRShape1.Width + 340;
     	QRShape3.Width 	:= QRShape3.Width + 340;
   end;

  	Rep.ResetPageFooterSize;;
  	OldalFejlecBand.ForceNewPage 	 := true;

	Application.CreateForm(TFormGaugeDlg, FormGaugeDlg);
	FormGaugeDlg.GaugeNyit('Lista elk�sz�t�se' , 'Egy kis t�relmet k�rek, dolgozom ...');

  	{A f�c�mek �s a cimk�k nyomtat�sa}
  	QrFocim1.Caption 	:= focim;
	Rep.ReportTitle		:= focim;

  // A fejl�c sorok �s magass�g be�ll�t�sa
  fejsorszam := fejlecek.Count;
  if fejsorszam > 10 then begin
  	fejsorszam := 10;
  end;

  // A sorok be�ll�t�sa
	for i := 1 to 10 do begin
		TQrLabel(FileListDlg.FindComponent('Cimlabel'+IntToStr(i))).Caption :='';
  end;
	for i := 0 to fejsorszam - 1 do begin
		TQrLabel(FileListDlg.FindComponent('Cimlabel'+IntToStr(i+1))).Caption :=fejlecek[i];
  end;
  FejlecBand.Height := fejsorszam * 16 + 2;		// A BAND magass�g�nak be�ll�t�sa
  QrShape1.Top		:= fejsorszam * 16 ;
  sorszam 				:= 0;

  // A l�bl�cek be�ll�t�sa
  labsorszam 	:= lablecek.Count;
  vanlablec		:= true;
  if labsorszam < 1 then begin
  		vanlablec	:= false;
  end;
  if labsorszam > 10 then begin
  	labsorszam := 10;
  end;

  // A sorok be�ll�t�sa
	for i := 1 to 10 do begin
		TQrLabel(FileListDlg.FindComponent('LabLabel'+IntToStr(i))).Caption :='';
  end;
	for i := 0 to labsorszam - 1 do begin
		TQrLabel(FileListDlg.FindComponent('Lablabel'+IntToStr(i+1))).Caption :=lablecek[i];
  end;
  LablecBand.Height := labsorszam * 16 + 2;		// A BAND magass�g�nak be�ll�t�sa
end;

procedure TFileListDlg.LabLecBandBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
	if not vanlablec then begin
   	PrintBand	:= false;
   end;
end;

end.
