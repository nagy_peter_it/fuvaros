﻿unit GoogleDistance;
{******************************************************************************
   Távolság meghatározása a Google Distance Matrix API segítségével.
*******************************************************************************}

interface

uses
   Classes, SysUtils, Variants,  Vcl.ExtCtrls,
   GooglePoints, json, J_SQL, Kozos, HTTPRutinok;
type
   TGraph = array[1..MaxPoints, 1..MaxPoints] of integer;  // az útvonalpontok távolsága párosával
   TRouteInfo = record
            RouteLength: integer;  // km-ben
            RouteDesc: string;
            end;  // record
   TRouteDescMode = (RDM_All, RDM_DestOnly);  // Az összes útvonalpontot tartalmazza vagy csak az utolsót (szakaszonkénti lekérdezéshez)

const
    ELEMENT_NOT_FOUND = -1;

type
   TGoogleDistance = Class (TComponent)
      public
       Sources, Destinations: TPoints;
       constructor Create(AOwner: TComponent); override;
       destructor Destroy; override;
       function CalculateDistance(LogKell: boolean; MBKOD: integer): string;
       function GetRouteInfo(RouteDescMode: TRouteDescMode): TRouteInfo;
     private
       DistGraph: TGraph;
       function AddPoints(PointList: TPoints; TeljesCim: boolean): string;
       function GetDistanceMatrix(LogKell, TeljesCim: boolean; MBKOD: integer): string;
       // function HttpGet(url: String): string;
       function FillMatrix(JSONText: string; TeljesCim: boolean): string;
       function NemFeldolgozandoJSON(JSONText: string): boolean;
       // function EncodeAsUTF8(str: string): String;
       function Plus2Space(S: string): string;
   end;

implementation
   uses Egyeb;

constructor TGoogleDistance.Create(AOwner: TComponent);
begin
   Inherited Create(AOwner);
   Sources:= TPoints.Create(Self);
   Destinations:= TPoints.Create(Self);
end;

destructor TGoogleDistance.Destroy;
begin
   Sources.Free;
   Destinations.Free;
   inherited Destroy;
end;


function TGoogleDistance.AddPoints(PointList: TPoints; TeljesCim: boolean): string;
var
   i: integer;
   S: string;
begin
  S:= '';
  for i := 1 to PointList.PointCount do begin
     S:= S+ PointList.PositionList[i,1];
     if TeljesCim then
          S:= S+ '+'+PointList.PositionList[i,2];  // + utca-házszám
     if i <> PointList.PointCount then  S:=S + '|';  // szeparátor
     end;  // for
  Result:=S;
end;

function TGoogleDistance.CalculateDistance(LogKell: boolean; MBKOD: integer): string;
var
  JSON, MyResult: string;
begin
  if (Sources.PointCount > 0) and (Destinations.PointCount > 0) then begin
    JSON:= GetDistanceMatrix(LogKell, True, MBKOD); // teljes címmel
    MyResult:= FillMatrix(JSON, True);
    if MyResult<>'' then begin  // van hibás cím
      JSON:= GetDistanceMatrix(LogKell, False, MBKOD); // csak ország-város, de csak azoknál, amit nem ismert fel!!
      MyResult:= FillMatrix(JSON, False);
      end;  // if
    Result:=MyResult;
    end;
end;


function TGoogleDistance.NemFeldolgozandoJSON(JSONText: string): boolean;
begin
  if (Pos('HHiba', JSONText) > 0)  // ezen átment valahogyan ... (copy(JSONText, 1, 5) = 'HHiba')
    or (Pos('UNKNOWN_ERROR', JSONText) > 0)
    or (Pos('Connection timed out', JSONText) > 0)
    or (Pos('Host not found', JSONText) > 0) then Result:=True
  else Result:=False;
end;

// Többszörös lekérdezés: minden párra visszadja: Origin1-Dest1, Origin1-Dest2, Origin2-Dest1, Origin2-Dest2
// https://maps.googleapis.com/maps/api/distancematrix/json?origins=47.6371040344238,17.6467876434326|Debrecen+Hungary&destinations=Kaposvár+Hungary|Fonyód+Hungary&mode=driving&language=en

function TGoogleDistance.GetDistanceMatrix(LogKell, TeljesCim: boolean; MBKOD: integer): string;
var
   // str, json_output: string;
   UTF8Url, json_output, Honnan: string;
   str, resultstring, StoreResult: string;
   TotalPoints: integer;
begin
   str:='https://maps.googleapis.com/maps/api/distancematrix/json?';
   str:=str+'origins=';
   str:=str+ AddPoints(Sources, TeljesCim);
   str:=str+'&destinations=';
   str:=str+ AddPoints(Destinations, TeljesCim);
   str:=str+'&mode=driving&language=en';
   str:=str+'&key='+EgyebDlg.MATRIXAPIKEY;  // az .INI-ből, API key for jsspeedmatrix@gmail.com
   UTF8Url:=StringEncodeAsUTF8(str);
   json_output:=FindURLQuery(UTF8Url);
   Honnan:='CACHE';
   if json_output='' then begin
      json_output:= HttpGet(UTF8Url);
      if Pos('exceeded', json_output)=0 then
          Honnan:='QUERY'
      else Honnan:='Q----';  // ezzel jelezzük a logban, hogy túlfutott az ingyenes limiten
      TotalPoints:= Sources.PointCount * Destinations.PointCount;
      if Honnan='QUERY' then begin // sikeres lekérdezés esetén tárolunk
         if not NemFeldolgozandoJSON(json_output) then  // ne tároljuk, ha pl. "HTTP/1.1 500 Internal Server Error"  vagy egyéb zűr
           StoreResult:=StoreURLQuery(UTF8Url, json_output, TotalPoints);  // ha a JSON nem fér bele a táblába, nem tároljuk, majd lekérdezzük újra, ez van.
         end;
      end;
   if LogKell then begin
      EgyebDlg.AppendLog('GOOGLEMATRIX','['+Honnan+' '+StoreResult+'] [MB='+IntToStr(MBKOD)+'] '+UTF8Url);
      end;
   Result:= json_output;
end;

function TGoogleDistance.Plus2Space(S: string): string;
    begin
      Result:= StringReplace(S, '+', ' ', [rfReplaceAll]);
    end;

{function TGoogleDistance.HttpGet(url: String): string;
var
  responseStream : TMemoryStream;
  html: string;
  HTTP: TIdHTTP;
  LHandler: TIdSSLIOHandlerSocketOpenSSL;
begin
  try          //  HTTPEncode ?
      try
        LHandler := TIdSSLIOHandlerSocketOpenSSL.Create(nil);
        responseStream := TMemoryStream.Create;
        HTTP := TIdHTTP.Create(nil);
        HTTP.IOHandler:=LHandler;
        HTTP.HTTPOptions := [hoForceEncodeParams];
        HTTP.Get(url, responseStream);
        SetString(html, PAnsiChar(responseStream.Memory), responseStream.Size);
        result := html;
      except
        on E: Exception do
            result := 'Hiba a HTTP lekérdezésben: '+E.Message;
      end;
    finally
      try
        HTTP.Disconnect;
        HTTP.Free;
        LHandler.Free;
      except
      end;
    end;
end;
}

function TGoogleDistance.FillMatrix(JSONText: string; TeljesCim: boolean): string;
var
  JSON: TJSON;
  row, element, address: TJSON;
  ActSource, ActDest, ActElem, i, j: integer;
  UnknownPlaces: TStringList;
  retvalue, ActValue, HibaHely, Hibauzenet: string;
  FoundGood: boolean;

begin
  if NemFeldolgozandoJSON(JSONText) then begin
     result:='HTTP hiba!';
     Exit;   // exit function
     end;
  UnknownPlaces:=TStringList.Create;
  UnknownPlaces.Sorted := True;
  UnknownPlaces.Duplicates := dupIgnore;
  // minden párra visszadja a távolságot, 2x2p: Origin1-Dest1, Origin1-Dest2, Origin2-Dest1, Origin2-Dest2
  HibaHely:='01';
  try
   try
     try
        JSON := TJSON.Parse(JSONText);
     except
         on E: exception do begin
            Hibakiiro('JSON Parse error ('+E.Message+'), text='+JSONText);
            retvalue:='JSON Parse error';
            end; // on E
        end;  // try-esxept
     HibaHely:='02';
     // A string array feldolgozás még nem működik jól a TJSON-ban (a FOR csak az első elemre loop-ol),
     // így más megoldást választok
    {
    ActElem:=1;
    // ismeretlen címek listába helyezése, duplikáció szűréssel
    for address in JSON['destination_addresses'] do begin
        ActValue:=address.AsString;
        if ActValue='' then begin
          UnknownPlaces.Add(Destinations.PositionList[ActElem]);
          end;  // if
        Inc(ActElem);
        end;  // for
   ActElem:=1;
   for address in JSON['origin_addresses'] do begin
        if address.AsString='' then begin
          UnknownPlaces.Add(Sources.PositionList[ActElem]);
          end;  // if
        Inc(ActElem);
        end;  // for
   // lista -> string
   for i:=0 to UnknownPlaces.Count - 1 do begin
      retvalue:=retvalue+' ['+UnknownPlaces[i]+']';
      end;  // for
      }
    ActSource:=1;
    for row in JSON['rows'] do begin
       if ActSource > MaxPoints then Raise Exception.Create('ActSource > MaxPoints');
       ActDest:=1;
       for element in row['elements'] do begin
           if ActDest > MaxPoints then Raise Exception.Create('ActDest > MaxPoints');
           if element['status'].AsString = 'OK' then begin
              DistGraph[ActSource, ActDest]:= Round(element['distance']['value'].AsInteger/1000);  // meter -> km
              end
           else begin    // status not OK       // unknown point
              if element['status'].AsString = 'NOT_FOUND' then begin
                  DistGraph[ActSource, ActDest]:= ELEMENT_NOT_FOUND;
                  end;
              end;  // else
           Inc(ActDest);
           end;  // for elements
       Inc(ActSource);
       end; // for rows

   // végignézem a mátrixot: az ismeretlen cím egész sora vagy oszlopa ELEMENT_NOT_FOUND értékű!
    HibaHely:='03';
   // sorok (Sources)
   if Sources.PointCount > MaxPoints then Raise Exception.Create('Sources.PointCount > MaxPoints');
   if Destinations.PointCount > MaxPoints then Raise Exception.Create('Destinations.PointCount > MaxPoints');
   for i:=1 to Sources.PointCount do begin
      FoundGood:=False;
      for j:=1 to Destinations.PointCount do begin
         if DistGraph[i, j]<>ELEMENT_NOT_FOUND then FoundGood:=True;
         end;  // inner for
      if not FoundGood then begin
        if TeljesCim then UnknownPlaces.Add(Plus2Space(Sources.PositionList[i,1]+' '+Sources.PositionList[i,2]))
        else UnknownPlaces.Add(Plus2Space(Sources.PositionList[i,1]));
        Sources.PositionList[i,3]:='n';  // unknown address
        end;  // if
      end;  // outer for
   // sorok (Sources)
   HibaHely:='04';
   for j:=1 to Destinations.PointCount do begin
      FoundGood:=False;
      for i:=1 to Sources.PointCount do begin
         if DistGraph[i, j] <> ELEMENT_NOT_FOUND then FoundGood:=True;
         end;  // inner for
      if not FoundGood then begin
        if TeljesCim then UnknownPlaces.Add(Plus2Space(Destinations.PositionList[j,1]+' '+Destinations.PositionList[j,2]))
        else UnknownPlaces.Add(Plus2Space(Destinations.PositionList[j,1]));
        Destinations.PositionList[j,3]:='n';  // unknown address
        end;  // if
      end;  // outer for
   HibaHely:='05';
   // lista -> string
   for i:=0 to UnknownPlaces.Count - 1 do begin
      retvalue:=retvalue+' ['+UnknownPlaces[i]+']';
      end;  // for
   HibaHely:='06';
   except
      on E: exception do begin
          Hibauzenet:= 'Hiba a "FillMatrix" eljárásban ('+HibaHely+'): '+E.Message;
          NoticeKi(Hibauzenet);
          Hibakiiro(Hibauzenet+' JSON='+JSONText);
          retvalue:=Hibauzenet;
          end; // on E
      end;  // try-except
  finally
    if JSON<>nil then JSON.free;
    if UnknownPlaces<>nil then UnknownPlaces.free;
    end;  // try-finally
  result:=retvalue;
end;


// Ha nem találtuk az utca-házszám címet (városközpontra számoltunk), akkor a listában is így szerepeltetjük
// pl. DE 98714 Dilingen Karlsplatz 43 ->(215) DE 68711 Karlstein-> (425) HU 9700 Sopron Ipar u. 12.
function TGoogleDistance.GetRouteInfo(RouteDescMode: TRouteDescMode): TRouteInfo;
var
   i, RLen: integer;
   RDesc, Cim1, Cim2: string;
   RInfo: TRouteInfo;
begin
  if (Sources.PointCount > 0) and (Destinations.PointCount > 0) then begin
     RLen:=0;
     RDesc:='';
     for i:=1 to Sources.PointCount do begin
        RLen:= RLen + DistGraph[i, i];
        if i=1 then begin  // csak először írjuk ki, hogy ne ismétlődjön a listában, hisz ugyaninnen megyünk tovább.
          if Sources.PositionList[i,3]='y' then begin   // a teljes címmel dolgoztunk
            Cim1:=Sources.PositionList[i,1]+' '+Sources.PositionList[i,2]
            end
          else Cim1:=Sources.PositionList[i,1];  // csak város
          end
        else begin
            Cim1:='';
            end;
        if Destinations.PositionList[i,3]='y' then begin   // a teljes címmel dolgoztunk
            Cim2:=Destinations.PositionList[i,1]+' '+Destinations.PositionList[i,2]
            end
        else Cim2:=Destinations.PositionList[i,1];  // csak város
        if RouteDescMode = RDM_All then
            RDesc:= RDesc+Cim1+' ->('+FormatFloat('0', DistGraph[i, i])+') '+Cim2;
        if RouteDescMode = RDM_DestOnly then begin
            if i = Sources.PointCount then  // az utolsó pár
               RDesc:= ' ->('+FormatFloat('0', DistGraph[i, i])+') '+Cim2;
            end;
        end;
     RInfo.RouteLength:= RLen;
     RInfo.RouteDesc:= Plus2Space(RDesc); // itt egyben kicseréljük őket
     end
  else begin
     RInfo.RouteLength:=0;
     RInfo.RouteDesc:= 'Nincsenek útvonalpontok!'; // itt egyben kicseréljük őket
     end;  // else
  result:=RInfo;
end;

// ez a régi változat, ami a LabelList alapján dolgozott.
{function TGoogleDistance.GetRouteInfo(): TRouteInfo;
var
   i, RLen: integer;
   RDesc: string;
   RInfo: TRouteInfo;
begin
   RLen:=0;
   RDesc:='';
   for i:=1 to Sources.PointCount do begin
      RLen:= RLen + DistGraph[i, i];
      RDesc:= RDesc+Sources.LabelList[i]+'->'+Destinations.LabelList[i];
      RDesc:= RDesc+'('+FormatFloat('0', DistGraph[i, i])+') ';
      end;
   RInfo.RouteLength:= RLen;
   RInfo.RouteDesc:= RDesc;
   result:=RInfo;
end;}

end.
