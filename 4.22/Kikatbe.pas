unit Kikatbe;

interface

uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, DBTables, DB, StdCtrls, Buttons, Mask, DBCtrls,
  ExtCtrls, ADODB;

type
  TKikatbeDlg = class(TForm)
    BitBtn6: TBitBtn;
    Label1: TLabel;
    M1: TMaskEdit;
    Label2: TLabel;
    M2: TMaskEdit;
    M0: TMaskEdit;
    Label3: TLabel;
    BitElkuld: TBitBtn;
    Label4: TLabel;
    Label5: TLabel;
    M5: TMaskEdit;
    Label6: TLabel;
    M6: TMaskEdit;
    Label7: TLabel;
    Label8: TLabel;
    M3: TMaskEdit;
    Label9: TLabel;
    M4: TMaskEdit;
    Label10: TLabel;
    Label11: TLabel;
    M7: TMaskEdit;
    Label12: TLabel;
    M8: TMaskEdit;
    Label13: TLabel;
    Query1: TADOQuery;
    M90: TMaskEdit;
    Label14: TLabel;
    Label15: TLabel;
    M9: TMaskEdit;
    M10: TMaskEdit;
    Label16: TLabel;
    Label17: TLabel;
    M11: TMaskEdit;
    M12: TMaskEdit;
    Label18: TLabel;
    Label19: TLabel;
    M13: TMaskEdit;
    M14: TMaskEdit;
    Label20: TLabel;
    Label21: TLabel;
    Label22: TLabel;
    Label23: TLabel;
    M3_EUR: TMaskEdit;
    M4_EUR: TMaskEdit;
    Label24: TLabel;
    Label25: TLabel;
    Label26: TLabel;
    M9_EUR: TMaskEdit;
    M10_EUR: TMaskEdit;
    Label27: TLabel;
    Label28: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure BitBtn6Click(Sender: TObject);
    procedure Tolt(katkod : string);
    procedure BitElkuldClick(Sender: TObject);
    procedure M1Exit(Sender: TObject);
    procedure M1KeyPress(Sender: TObject; var Key: Char);
    procedure M1Click(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
  		kategoria	: string;
  public
  end;

var
  KikatbeDlg: TKikatbeDlg;

implementation

uses
	Egyeb, Kozos, J_SQL;
{$R *.DFM}

procedure TKikatbeDlg.FormCreate(Sender: TObject);
begin
  	EgyebDlg.SetADOQueryDatabase(Query1);
  	SetMaskEdits([M0]);
   kategoria	:= '';
end;

procedure TKikatbeDlg.BitBtn6Click(Sender: TObject);
begin
	Close;
end;

procedure TKikatbeDlg.Tolt(katkod : string);
begin
	// A kaeg�ri�hoz tartoz� adatok bet�lt�se
	kategoria	:= katkod;
	M0.Text		:= EgyebDlg.Read_Szgrid('340', katkod);
	if katkod <> '' then begin
		Query_Run(Query1, 'SELECT * FROM KATINTER WHERE KI_GKKAT = '''+katkod+''' ');
		if Query1.RecordCount > 0 then begin
			M1.Text		:= Query1.FieldByName('KI_KMTOL').AsString;
			M2.Text		:= Query1.FieldByName('KI_KMEIG').AsString;
			M3.Text		:= Query1.FieldByName('KI_NYTOL').AsString;
			M4.Text		:= Query1.FieldByName('KI_NYEIG').AsString;
      M3_EUR.Text:= Query1.FieldByName('KI_NYTOL_EUR').AsString;
			M4_EUR.Text:= Query1.FieldByName('KI_NYEIG_EUR').AsString;
			M5.Text		:= Query1.FieldByName('KI_ATTOL').AsString;
			M6.Text		:= Query1.FieldByName('KI_ATLIG').AsString;
			M7.Text		:= Query1.FieldByName('KI_MTTOL').AsString;
			M8.Text		:= Query1.FieldByName('KI_MTAIG').AsString;
			M9.Text		:= Query1.FieldByName('KI_KTTOL').AsString;
			M10.Text  := Query1.FieldByName('KI_KTGIG').AsString;
			M9_EUR.Text:= Query1.FieldByName('KI_KTTOL_EUR').AsString;
			M10_EUR.Text:= Query1.FieldByName('KI_KTGIG_EUR').AsString;
			M11.Text  := Query1.FieldByName('KI_EUTOL').AsString;
			M12.Text  := Query1.FieldByName('KI_EURIG').AsString;
			M13.Text  := Query1.FieldByName('KI_FTKM1').AsString;
			M14.Text  := Query1.FieldByName('KI_FTKM2').AsString;
			M90.Text 	:= Query1.FieldByName('KI_MEGJE').AsString;
		end;
	end;
end;

procedure TKikatbeDlg.BitElkuldClick(Sender: TObject);
begin
	// Az adatok t�rol�sa
	Query_Run(Query1, 'DELETE FROM KATINTER WHERE KI_GKKAT = '''+kategoria+''' ');
	Query_Insert (Query1, 'KATINTER',
	  [
	  'KI_GKKAT', ''''+kategoria+'''',
	  'KI_KMTOL', SqlSzamString(StringSzam(M1.Text),12,0),
	  'KI_KMEIG', SqlSzamString(StringSzam(M2.Text),12,0),
	  'KI_NYTOL', SqlSzamString(StringSzam(M3.Text),12,2),
	  'KI_NYEIG', SqlSzamString(StringSzam(M4.Text),12,2),
    'KI_NYTOL_EUR', SqlSzamString(StringSzam(M3_EUR.Text),12,2),
	  'KI_NYEIG_EUR', SqlSzamString(StringSzam(M4_EUR.Text),12,2),
	  'KI_ATTOL', SqlSzamString(StringSzam(M5.Text),12,2),
	  'KI_ATLIG', SqlSzamString(StringSzam(M6.Text),12,2),
	  'KI_MTTOL', SqlSzamString(StringSzam(M7.Text),12,2),
	  'KI_MTAIG', SqlSzamString(StringSzam(M8.Text),12,2),
	  'KI_KTTOL', SqlSzamString(StringSzam(M9.Text),12,2),
	  'KI_KTGIG', SqlSzamString(StringSzam(M10.Text),12,2),
	  'KI_KTTOL_EUR', SqlSzamString(StringSzam(M9_EUR.Text),12,2),
	  'KI_KTGIG_EUR', SqlSzamString(StringSzam(M10_EUR.Text),12,2),
	  'KI_EUTOL', SqlSzamString(StringSzam(M11.Text),12,2),
	  'KI_EURIG', SqlSzamString(StringSzam(M12.Text),12,2),
	  'KI_FTKM1', SqlSzamString(StringSzam(M13.Text),12,2),
	  'KI_FTKM2', SqlSzamString(StringSzam(M14.Text),12,2),
	  'KI_MEGJE', ''''+M90.Text+''''
	  ] );
	Close;
end;

procedure TKikatbeDlg.M1Exit(Sender: TObject);
begin
	SzamExit(Sender);
end;

procedure TKikatbeDlg.M1KeyPress(Sender: TObject; var Key: Char);
begin
	SzamKeyPress(Sender, Key);
end;

procedure TKikatbeDlg.M1Click(Sender: TObject);
begin
	with Sender as TMaskEdit do begin
  		SelectAll;
  	end;
end;

procedure TKikatbeDlg.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
	EgyebDlg.FormReturn(Key);
end;

end.


