unit AlGepfm;

interface

uses
	SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
	StdCtrls, Forms, DBCtrls, DB, DBGrids, DBTables, Grids, ExtCtrls, Buttons,
	ShellApi, Dialogs, J_FOFORMUJ, ADODB, Mask, Menus, Variants;

type
	TAlGepFmDlg = class(TJ_FOFORMUJDLG)
	  procedure FormCreate(Sender: TObject);override;
	 procedure Adatlap(cim, kod : string);override;
	 procedure ButtonKuldClick(Sender: TObject);override;
	 private
	 public
	end;

var
	AlGepFmDlg : TAlGepFmDlg;

implementation

uses
	Egyeb, J_SQL, AlGepbe, Kozos;

{$R *.DFM}

procedure TAlGepFmDlg.FormCreate(Sender: TObject);
begin
	Inherited FormCreate(Sender);
	kellujures	:= false;
	AddSzuromezoRovid( 'AG_ARHIV', 'ALGEPKOCSI' );
	AddSzuromezoRovid( 'AG_GKKAT', 'ALGEPKOCSI' );
	AddSzuromezoRovid( 'AG_TIPUS', 'ALGEPKOCSI' );
	AddSzuromezoRovid( 'AG_RENSZ', 'ALGEPKOCSI' );
	AddSzuromezoRovid( 'AG_POTOS', 'ALGEPKOCSI' );
	alapstr		:= 'SELECT * FROM ALGEPKOCSI ';
	FelTolto('�j alv�llalkoz�i g�pkocsi felvitele ', 'Alv�llalkoz�i g�pkocsi adatok m�dos�t�sa ',
			'ALv�llalkoz�i g�pkocsik list�ja', 'AG_AGKOD', alapstr, 'ALGEPKOCSI', QUERY_KOZOS_TAG );
	kulcsmezo			:= 'AG_AGKOD';
	nevmezo				:= 'AG_RENSZ';
	width				:= DLG_WIDTH;
	height				:= DLG_HEIGHT;
end;

procedure TAlGepFmDlg.ButtonKuldClick(Sender: TObject);
begin
	ret_vnev	:= Query1.FieldByName('AG_RENSZ').AsString;
	Inherited ButtonKuldClick(Sender);
end;

procedure TAlGepFmDlg.Adatlap(cim, kod : string);
begin
	Application.CreateForm(TAlGepbeDlg, AlForm);
	Inherited Adatlap(cim, kod );
end;

end.


