unit Ossznyil;

interface

uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls, Shellapi,
  Forms, Dialogs, DBTables, DB, StdCtrls, Buttons, Mask, DBCtrls, ExtCtrls,
  ADODB, CheckLst, Folista, DateUtils, IOUtils, Clipbrd, Vcl.ComCtrls,
  CheckCombo;

type
  TOssznyilDlg = class(TForm)
    BitBtn4: TBitBtn;
    BitBtn6: TBitBtn;
    Label5: TLabel;
    Query1: TADOQuery;
	 Query2: TADOQuery;
    Query3: TADOQuery;
    Label6: TLabel;
    Query11: TADOQuery;
    BitBtn9: TBitBtn;
    OpenDialog1: TOpenDialog;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Label3: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    Label4: TLabel;
    M4: TMaskEdit;
    MD1: TMaskEdit;
    MD2: TMaskEdit;
    BitBtn2: TBitBtn;
    BitBtn3: TBitBtn;
    M1: TMaskEdit;
    BitBtn5: TBitBtn;
    M0: TMaskEdit;
    BitBtn7: TBitBtn;
    M40: TMaskEdit;
    BitBtn32: TBitBtn;
    BitBtn8: TBitBtn;
    Label8: TLabel;
    Label9: TLabel;
    MPartnerNev: TMaskEdit;
    BitBtn10: TBitBtn;
    BitBtn11: TBitBtn;
    MPartnerKod: TMaskEdit;
    chkCsakSajat: TCheckBox;
    Panel1: TPanel;
    Label7: TLabel;
    CBGkat: TComboBox;
    BitBtn1: TBitBtn;
    CL1: TCheckListBox;
    CheckBox2: TCheckBox;
    CheckBox3: TCheckBox;
    CheckBox4: TCheckBox;
    chkTermelo: TCheckBox;
    ComboBox1: TComboBox;
    lbViszonylatSQL: TListBox;
    cbViszonylat: TCheckedComboBox;
    procedure FormCreate(Sender: TObject);
	 procedure BitBtn4Click(Sender: TObject);
	 procedure BitBtn6Click(Sender: TObject);
	 procedure BitBtn1Click(Sender: TObject);
	 procedure MD1Exit(Sender: TObject);
	 procedure MD2Exit(Sender: TObject);
	 procedure BitBtn2Click(Sender: TObject);
	 procedure BitBtn3Click(Sender: TObject);
	 procedure BitBtn5Click(Sender: TObject);
	 procedure M1KeyDown(Sender: TObject; var Key: Word;
	   Shift: TShiftState);
	 procedure FormKeyDown(Sender: TObject; var Key: Word;
	   Shift: TShiftState);
	 procedure BitBtn7Click(Sender: TObject);
	 procedure BitBtn32Click(Sender: TObject);
	 procedure BitBtn8Click(Sender: TObject);
	 procedure Ellenorzes;
	 procedure M1Exit(Sender: TObject);
	 procedure JaratIro;
	//  procedure CheckBox1Click(Sender: TObject);
	 procedure KategTolto(Sender: TObject);
	 procedure CL1Click(Sender: TObject);
    procedure BitBtn9Click(Sender: TObject);
   procedure Lista(MelyikOsszList: string);
    procedure ComboBox1Change(Sender: TObject);
    procedure BitBtn10Click(Sender: TObject);
    procedure BitBtn11Click(Sender: TObject);
    procedure ViszonylatTolt(const PartnerKod: string);
  private
		jaratkod		: string;
		ujrahiv			: boolean;
		vegstr			: string;
		listagkat		: TStringList;
  public
  end;

var
  OssznyilDlg: TOssznyilDlg;

implementation

uses
	J_VALASZTO, Osszlis, Osszlis2Uj, J_SQL, Egyeb, Kozos,  GepkocsiUjfm,
   KiKatbe, Uzema, Jaratbe, JaratFm, Valvevo2;

{$R *.DFM}

procedure TOssznyilDlg.FormCreate(Sender: TObject);
var
	i : integer;
begin
 	M4.Text 		:= '';
  	jaratkod    	:= '';
  	MD1.Text		:= '';
  	MD2.Text		:= '';
   Label6.Caption	:= '';
   vegstr			:= '';
  	Label5.Hide;
  	SetMaskEdits([M4]);
	EgyebDlg.SetADOQueryDatabase(Query1);
	EgyebDlg.SetADOQueryDatabase(Query11);
	EgyebDlg.SetADOQueryDatabase(Query2);
	EgyebDlg.SetADOQueryDatabase(Query3);
	listagkat	:= TStringList.Create;
	SzotarTolt(CBGkat, '340' , listagkat, false, true );
   CL1.Clear;
   for i := 0 to listagkat.Count - 1 do begin
   	CBGkat.Items[i]	:= listagkat[i];
       CL1.Items.Add(listagkat[i]);
   end;
   CBGkat.Items[0]		:= 'Minden kat.';
   CL1.Items[0]		:= 'Minden kat.';
	CL1.ItemIndex		:= 0;
	CBGkat.ItemIndex	:= 0;
	// CheckBox1Click(Sender);
  ComboBox1Change(Sender);
	EgyebDlg.Kilepes	:= true;
end;

procedure TOssznyilDlg.BitBtn4Click(Sender: TObject);
begin
  Lista('r�gi');
end;

procedure TOssznyilDlg.BitBtn9Click(Sender: TObject);
begin
  Lista('�j');
end;

procedure TOssznyilDlg.Lista(MelyikOsszList: string);
var
  kedat		: string;
  vedat		: string;
  folyt		: boolean;
  kategstr		: string;
  i			: integer;
  str, ExportFileName, vevokod, viszonysql: string;
  Jaratuj		: TJaratbeDlg;
  Tobb_jarat, LettAdat, csakSajatAuto: boolean;
begin
	{A lista �ssze�ll�t�sa a sz�r�si elemek alapj�n}

	EgyebDlg.kilepes	:= false;
	Ellenorzes;
	jaratkod			:= M0.Text;
	kedat				:= MD1.Text;
	vedat				:= MD2.Text;
	if ( ( kedat <> '' ) and (vedat = '') ) then begin
		vedat		:= kedat;
		MD2.Text    := MD1.Text;
	end;
	if kedat = '' then begin
		kedat			:= '2000.01.01.';
	end;
	if vedat = '' then begin
		vedat			:= EgyebDlg.MaiDatum;
	end;

	if not Jodatum(kedat) then begin
		NoticeKi('Rossz d�tum megad�sa!');
		MD1.SetFocus;
		EgyebDlg.kilepes	:= true;
		Exit;
	end;
	if not Jodatum(vedat) then begin
		NoticeKi('Rossz d�tum megad�sa!');
		MD2.SetFocus;
		EgyebDlg.kilepes	:= true;
		Exit;
	end;
//	EgyebDlg.klikkeles	:= Haromklikk;

  csakSajatAuto:= chkCsakSajat.Checked;

  vevokod:= MPartnerKod.Text;
  // if cbViszonylat.ItemIndex >=0 then
  //  viszonysql:= lbViszonylatSQL.Items[cbViszonylat.ItemIndex]

  if cbViszonylat.GetText<>'' then begin
      viszonysql:= ' and VI_HONNAN+ '' - ''+ VI_HOVA in ('+cbViszonylat.GetText+') ';
      end
  else viszonysql:= '';

   kategstr	:= '';
   for i := 0 to CL1.Items.Count - 1 do begin
			if CL1.Checked[i] then begin
				kategstr	:= kategstr + ''''+ listagkat[i] + ''', ';
			end;
		end;

  // --------------------------------------------------------------------- //
  //                      E X C E L    E X P O R T
  // --------------------------------------------------------------------- //
  if (ComboBox1.ItemIndex=2) then begin      // Excel
			Screen.Cursor := crHourglass;
			Application.CreateForm(TOsszlisDlg, OsszlisDlg);
      try
        OsszlisDlg.Labi	:= Label6;
        if kategstr = '' then begin
  			  OsszlisDlg.kateg	:= '';
    		end else begin
    			OsszlisDlg.kateg	:= kategstr + '''zzzz''';
  	    	end;
        // OsszlisDlg.Tolt(jaratkod, M4.Text, kedat, vedat);
        OsszlisDlg.Tolt(jaratkod, M4.Text, kedat, vedat, vevokod, viszonysql, csakSajatAuto);

        // ExportFileName:= EgyebDlg.TempPath+'LIST\'+'temp.xls';
        try
          ExportFileName:= TPath.GetTempFileName+'.xls';  // Generates a unique temporary file
        except
          NoticeKi('A temp k�nyvt�r nem el�rhet�, k�rlek v�lassz �llom�ny nevet!');
          OpenDialog1.Filter		:= 'XLS �llom�nyok (*.xls)|*.XLS';
	        OpenDialog1.DefaultExt  := 'XLS';
          OpenDialog1.InitialDir	:= EgyebDlg.DocumentPath;
	        if OpenDialog1.Execute then begin
		         ExportFileName:= OpenDialog1.FileName;
             end;
          end;  // try-except

        OsszlisDlg.ExportToExcel(ExportFileName);
        LettAdat:= OsszlisDlg.vanadat;
        if not LettAdat	then begin
            NoticeKi('A be�ll�tott felt�telekhez nincs j�rat!');
            end
        else begin
            Clipboard.Clear;
            Clipboard.AsText:=ExportFileName;
            NoticeKi('Az �llom�ny k�sz ('+ExportFileName+'), megnyitom...');
            ShellExecute(Application.Handle,'open',PChar(ExportFileName), '', '', SW_SHOWNORMAL );
            // NoticeKi('Export�l�s k�sz!');
            end;
      	  // end;  // if OpenDialog
      finally
        OsszlisDlg.Release;
        Screen.Cursor 	:= crDefault;
        end;  // try-finally
      end;  // if Excel export
  // --------------------------------------------------------------------- //
  //                           R � S Z L E T E S
  // --------------------------------------------------------------------- //
  if (ComboBox1.ItemIndex=1) then begin      // r�szletes
		ujrahiv	:= true;
		while ujrahiv do begin
			{A lista �ssze�ll�t�sa a sz�r�si elemek alapj�n}
			ujrahiv	:= false;
			EgyebDlg.ListaValasz := 0;
			Screen.Cursor := crHourglass;
			Application.CreateForm(TOsszlisDlg, OsszlisDlg);
			OsszlisDlg.Labi	:= Label6;
      if kategstr = '' then begin
  			OsszlisDlg.kateg	:= '';
  		end else begin
  			OsszlisDlg.kateg	:= kategstr + '''zzzz''';
  		end;
			// OsszlisDlg.Tolt(jaratkod, M4.Text, kedat, vedat);
      OsszlisDlg.Tolt(jaratkod, M4.Text, kedat, vedat, vevokod, viszonysql, csakSajatAuto);

			Screen.Cursor 	:= crDefault;
			if ( ( OsszlisDlg.vanadat  ) and (not EgyebDlg.kilepes ) )	then begin
            // H�ny j�rat �rintett?
            Tobb_jarat:= False;
            if Pos(',', M0.Text)>0 then Tobb_jarat:= True    // pl. 'HU-14,DE-12,HU-11,'
            else begin
                if OsszlisDlg.erintett_jaratok>1 then
                  Tobb_jarat:= True
                end;  // else

               // Query_Run(Query11, 'SELECT JA_KOD FROM JARAT WHERE JA_KOD IN ('+M0.Text+') ');
               if Not(Tobb_jarat) then begin
                   EgyebDlg.kelllistagomb	:= false;
                   EgyebDlg.ListaGomb	   	:= '';
                   EgyebDlg.kelllistagomb3	:= false;
                   EgyebDlg.ListaGomb3	   	:= '';
                   if GetRightTag(552) <> RG_NORIGHT then begin
                       EgyebDlg.kelllistagomb	:= true;
                       EgyebDlg.ListaGomb	   	:= '&Ellen�rz�s';
                   end;
                   EgyebDlg.kelllistagomb2	:= true;
                   EgyebDlg.ListaGomb2	   	:= '&M�dos�t�s';
                   if Label5.Visible = true then begin
                       EgyebDlg.kelllistagomb2	:= false;
                       EgyebDlg.kelllistagomb	:= true;
                       EgyebDlg.ListaGomb		:= '&Megtekint�s';
                   end;
                   // Ha csak egyetlen j�ratot v�lasztottunk, akkor j�het az �zemanyag kimutat�s gomb
                   if M1.Text <> '' then begin
                       EgyebDlg.kelllistagomb3	:= true;
                       EgyebDlg.ListaGomb3		:= '�a.kimut.';
                   end;
               end;
				OsszlisDlg.Rep.Preview;
				OsszlisDlg.Destroy;
			end else begin
        OsszlisDlg.Destroy;
				if not EgyebDlg.kilepes then begin
					NoticeKi('Nincs a megadott felt�teleknek megfelel� t�tel!');
				end;
			end;
      if (copy(jaratkod,1,1)='''') and (copy(jaratkod,length(jaratkod),1)='''') then begin // ha aposztr�fok k�z�tt van
        jaratkod:= copy(jaratkod,2,Length(jaratkod)-2);
        end;
			if EgyebDlg.ListaValasz = 1 then begin
				Label6.Caption	:= '';
				Label6.Update;
				if EgyebDlg.ListaGomb = '&Megtekint�s' then begin
					// Csak megtekintj�k a j�ratot
					{Meg kell h�vni a j�rat m�dos�t�st}
					Application.CreateForm(TJaratbeDlg, JaratbeDlg);
					JaratbeDlg.megtekint	:= true;
					// JaratbeDlg.Tolto('J�rat megtekint�se',copy(jaratkod,2,Length(jaratkod)-2));
          JaratbeDlg.Tolto('J�rat megtekint�se', jaratkod);
					JaratbeDlg.ShowModal;
					JaratbeDlg.Destroy;
                   JaratbeDlg:=nil;
				end else begin
					{A j�rathoz kapcsol�d� sz�ml�k ellen�rz�se}
					folyt	:= true;
					// Query_Run(Query2,'SELECT VI_UJKOD FROM VISZONY WHERE VI_JAKOD = '''+copy(jaratkod,2,Length(jaratkod)-2)+''' ',true);
          Query_Run(Query2,'SELECT VI_UJKOD FROM VISZONY WHERE VI_JAKOD = '''+jaratkod+''' ',true);
					while not Query2.EOF do begin
						if Query2.FieldByname('VI_UJKOD').AsString <> '' then begin
							Query_Run(Query3,'SELECT SA_FLAG FROM SZFEJ WHERE SA_UJKOD = '+Query2.FieldByname('VI_UJKOD').AsString,true);
							if Query3.FieldByname('SA_FLAG').AsString = '1' then begin
								folyt		:= false;	{M�g van nem v�gleges�tett sz�mla}
							end;
						end;
						Query2.Next;
					end;
					if not folyt then begin
						NoticeKi('M�g van a j�rathoz kapcsolt nem v�gleges�tett sz�mla!');
						Exit;
					end;
{
					if not JaratbeDlg.Mentes then begin
						Exit;
					end;
          }
					{Ellen�rz�tt� kell tenni a rekordot}
					if vegstr <> '' then begin
						// Ellen�rizz�k, hogy van-e a j�rat ut�n tele tank.
						Query_Run(Query1, 'select * from JARAT '+ vegstr,true);
						while not Query1.Eof do begin
							str	:= Query_Select('ALJARAT', 'AJ_JAKOD', Query1.FieldByName('JA_KOD').AsString, 'AJ_ALNEV');
							if str <> '' then begin
								// Alv�llalkoz�i j�rat, mehet az ellen�rz�s
								Query_Run(Query11, 'UPDATE JARAT SET JA_FAZIS = 1, JA_FAZST = ''Ellen�rz�tt''  WHERE JA_KOD = '+ Query1.FieldByName('JA_KOD').AsString,true);
							end else begin
								Query_Run(Query11, 'SELECT MIN(KS_KMORA) KMORA FROM KOLTSEG WHERE KS_RENDSZ = '''+Query1.FieldByName('JA_RENDSZ').AsString+''' '+
									' AND KS_KMORA >= '+IntToStr(StrToIntDef(Query1.FieldByName('JA_ZAROKM').AsString,0))+
									' AND KS_TELE  = 1 ' +
									' AND KS_TIPUS = 0 ' );
								if StrToIntDef(Query11.FieldByName('KMORA').AsString, 0) < StrToIntDef(Query1.FieldByName('JA_ZAROKM').AsString,0) then begin
									NoticeKi('Hi�nyz� z�r� km ut�ni tankol�s : ' +
										GetJaratszamFromDb(Query1)+' - '+IntToStr(StrToIntDef(Query1.FieldByName('JA_ZAROKM').AsString,0)));
								end else begin
									// Van k�s�bbi tele tankol�s, mehet az ellen�rz�s
									Query_Run(Query11, 'UPDATE JARAT SET JA_FAZIS = 1, JA_FAZST = ''Ellen�rz�tt''  WHERE JA_KOD = '+ Query1.FieldByName('JA_KOD').AsString,true);
								end;
							end;
							Query1.Next;
						end;
					end;
					{Ki�rjuk, hogy ez m�r ellen�rz�tt}
					Label5.Show;
				end;
			end;
			if EgyebDlg.ListaValasz = 2 then begin
				{Meg kell h�vni a j�rat m�dos�t�st}
				Application.CreateForm(TJaratbeDlg, JaratUj);
				// JaratUj.Tolto('J�rat m�dos�t�sa',copy(jaratkod,2,Length(jaratkod)-2));
        JaratUj.Tolto('J�rat m�dos�t�sa', jaratkod);
				JaratUj.ShowModal;
				JaratUj.Destroy;
				ujrahiv	:= true;
//				OsszlisDlg.Tolt(jaratkod, M4.Text, kedat, vedat);
			end;
			if EgyebDlg.ListaValasz = 3 then begin
				{Meg kell h�vni az �zemanyag kimutat�st}
				Application.CreateForm(TUzemaDlg, UzemaDlg);
				Query_Run(Query11, 'SELECT * FROM JARAT WHERE JA_KOD = '+ jaratkod,true);
				UzemaDlg.Tolt(Query11.FieldByName('JA_RENDSZ').AsString);
				if UzemaDlg.vanadat then begin
					// A kezd� �s befejez� lista be�ll�t�sa
					i := 0;
					while i < UzemaDlg.ListKezd.Items.Count do begin
						str	:= UzemaDlg.ListKezd.Items[i];
						str		:= copy(str, Pos('( ', str)+2, 999);
						str		:= copy(str, 1, Pos(' ', str)-1);
						if StrToIntDef(Query11.FieldByName('JA_NYITKM').AsString, 0) <= StrToIntDef(str, 0) then begin
							UzemaDlg.ListKezd.ItemIndex := i-1;
							i := UzemaDlg.ListKezd.Items.Count;
						end;
						Inc(i);
					end;
					i := UzemaDlg.ListVege.Items.Count - 1;
					while i > 0  do begin
						str	:= UzemaDlg.ListVege.Items[i];
						str		:= copy(str, Pos('( ', str)+2, 999);
						str		:= copy(str, 1, Pos(' ', str)-1);
						if StrToIntDef(Query11.FieldByName('JA_ZAROKM').AsString, 0) >= StrToIntDef(str, 0) then begin
							UzemaDlg.ListVege.ItemIndex := i+1;
							i := 0;
						end;
						Dec(i);
					end;
					UzemaDlg.ShowModal;
					UzemaDlg.Destroy;
				end;
				ujrahiv	:= true;
			end;
		end;	// Itt a 'WHILE ujrahiv...' V�GE
		EgyebDlg.kelllistagomb3	:= false;
		EgyebDlg.ListaGomb3	   	:= '';
	  end;

 if (ComboBox1.ItemIndex=0) then begin      // �SSZES�T�
  // --------------------------------------------------------------------- //
  //                        � S S Z E S � T �
  // --------------------------------------------------------------------- //
		// Nem r�szletes lit�t k�sz�t�nk, csak g�pkocsink�nti �sszesent

		Screen.Cursor := crHourglass;
    if MelyikOsszList='r�gi' then begin // 2015 szeptemberben k�sz�l az �j, de m�g szeretn�nk a r�git is haszn�lni
    		{Application.CreateForm(TOsszlis2Dlg, Osszlis2Dlg);
        Osszlis2Dlg.csakaproblemas:=CheckBox4.Checked;
  	  	if kategstr = '' then begin
    			Osszlis2Dlg.kateg	:= '';
    		end else begin
    			Osszlis2Dlg.kateg	:= kategstr + '''zzzz''';
    		end;
    		Osszlis2Dlg.Tolt(jaratkod, M4.Text, kedat, vedat);  // "r�gi" riport: itt nem vezetj�k �t a vev� - viszonylat sz�r�st
    		Screen.Cursor := crDefault;
    		if Osszlis2Dlg.vanadat	then begin
    			EgyebDlg.kelllistagomb	:= false;
    			if Osszlis2Dlg.SGEllen.Cells[0,0] <> '' then begin
    				EgyebDlg.kelllistagomb	:= true;
    				EgyebDlg.ListaGomb	   	:= '&Ellen�rz�s';
    			end;
    			Osszlis2Dlg.Rep.Preview;
    			if EgyebDlg.ListaValasz = 1 then begin
    				Label6.Caption	:= '';
    				Label6.Update;
    				Osszlis2Dlg.Ellenorzes;
    			end;
    		end else begin
    			NoticeKi('Nincs a megadott felt�teleknek megfelel� t�tel!');
    		end;
    		Osszlis2Dlg.Destroy;
        }
        end  // if "r�gi"
    else begin
      // ********************************************* //
      //           �j �sszes�tett lista                //
      // ********************************************* //
      Application.CreateForm(TOsszlis2DlgUj, Osszlis2DlgUj);
      Osszlis2DlgUj.csakaproblemas:=CheckBox4.Checked;
      Osszlis2DlgUj.csakatermelo:=chkTermelo.Checked;
	  	if kategstr = '' then begin
  			Osszlis2DlgUj.kateg	:= '';
  		end else begin
  			Osszlis2DlgUj.kateg	:= kategstr + '''zzzz''';
  		end;
  		// Osszlis2DlgUj.Tolt(jaratkod, M4.Text, kedat, vedat);
      Osszlis2DlgUj.Tolt(jaratkod, M4.Text, kedat, vedat, vevokod, viszonysql, csakSajatAuto);
  		Screen.Cursor := crDefault;
  		if Osszlis2DlgUj.vanadat	then begin
  			EgyebDlg.kelllistagomb	:= false;
  			if Osszlis2DlgUj.SGEllen.Cells[0,0] <> '' then begin
  				EgyebDlg.kelllistagomb	:= true;
  				EgyebDlg.ListaGomb	   	:= '&Ellen�rz�s';
  			end;
  			Osszlis2DlgUj.Rep.Preview;
  			if EgyebDlg.ListaValasz = 1 then begin
  				Label6.Caption	:= '';
  				Label6.Update;
  				Osszlis2DlgUj.Ellenorzes;
  			end;
  		end else begin
  			NoticeKi('Nincs a megadott felt�teleknek megfelel� t�tel!');
  		end;
  		Osszlis2DlgUj.Destroy;
      end; // if "�j"
	end;
	Label6.Caption	:= '';
	Label6.Update;
	Ellenorzes;
	EgyebDlg.kelllistagomb	:= false;
	EgyebDlg.kelllistagomb2	:= false;
	EgyebDlg.kilepes		:= true;
	if ( ( M1.Visible ) and ( M1.Enabled ) ) then begin
		M1.SetFocus;
	end;
end;

procedure TOssznyilDlg.BitBtn6Click(Sender: TObject);
begin
	if not EgyebDlg.kilepes then begin
		EgyebDlg.kilepes	:= true;
	end else begin
		Close;
	end;
end;

procedure TOssznyilDlg.BitBtn10Click(Sender: TObject);
begin
	PartnerValaszto (MPartnerKod, MPartnerNev);
  Application.ProcessMessages;
  Screen.Cursor := crHourglass;
  ViszonylatTolt(MPartnerKod.Text);
	Screen.Cursor := crDefault;
end;

procedure TOssznyilDlg.ViszonylatTolt(const PartnerKod: string);
begin
  if JoEgesz(PartnerKod) then begin
    lbViszonylatSQL.Clear;
    cbViszonylat.Items.Clear;
    Query_Run(Query1, 'select distinct VI_HONNAN, VI_HOVA from viszony where VI_VEKOD in ('+PartnerKod+')');
    while not Query1.Eof do begin
      cbViszonylat.Items.Add(Query1.FieldByName('VI_HONNAN').AsString + ' - '+Query1.FieldByName('VI_HOVA').AsString);
      // lbViszonylatSQL.Items.Add(' and VI_HONNAN='''+Query1.FieldByName('VI_HONNAN').AsString+''' and VI_HOVA='''+Query1.FieldByName('VI_HOVA').AsString+'''');
      // lehetne  VI_HONNAN+ '-'+VI_HOVA-ra sz�rni az SQL-ben
      Query1.Next;
      end; // while
    end; // if
end;

procedure TOssznyilDlg.BitBtn11Click(Sender: TObject);
begin
	MPartnerKod.Text		:= '';
	MPartnerNev.Text		:= '';
end;

procedure TOssznyilDlg.BitBtn1Click(Sender: TObject);
begin
	GepkocsiValaszto(M4);
end;

procedure TOssznyilDlg.MD1Exit(Sender: TObject);
begin
	DatumExit(Sender, false);
  	EgyebDlg.ElsoNapEllenor(MD1, MD2);
   Ellenorzes;
end;

procedure TOssznyilDlg.MD2Exit(Sender: TObject);
begin
	DatumExit(Sender, false);
   Ellenorzes;
end;

procedure TOssznyilDlg.BitBtn2Click(Sender: TObject);
begin
	// EgyebDlg.Calendarbe(MD1);
	// EgyebDlg.ElsoNapEllenor(MD1, MD2);
  EgyebDlg.CalendarBe_idoszak(MD1, MD2);
  Ellenorzes;
end;

procedure TOssznyilDlg.BitBtn3Click(Sender: TObject);
begin
	EgyebDlg.Calendarbe(MD2);
  EgyebDlg.UtolsoNap(MD2);
  // MD2.Text:=DateToStr(EndOfTheMonth(StrToDate(MD2.Text)));
  Ellenorzes;
end;

procedure TOssznyilDlg.BitBtn5Click(Sender: TObject);
var
   i : integer;
begin
   // Bev�laszthatunk t�bb j�ratot is
	Application.CreateForm(TJaratFormDlg,JaratFormDlg);
	JaratFormDlg.valaszt	:= true;
   JaratFormDlg.multiselect:= true;
	JaratFormDlg.ShowModal;
	if JaratformDlg.ret_vkod <> '' then begin
       M0.Text := '';
       M1.Text := '';
       for i := 0 to JaratformDlg.selectlist.Count - 1 do begin
           M0.Text := M0.Text + ''''+JaratformDlg.selectlist[i]+''',';
           M1.Text := M1.Text + GetJaratszam(JaratformDlg.selectlist[i])+',';
       end;
       M0.Text := copy(M0.Text, 1, Length(M0.Text)-1);
       M1.Text := copy(M1.Text, 1, Length(M1.Text)-1);
   	Ellenorzes;
	end else begin
		M0.Text		:= '';
		M1.Text 	:= '';
	end;
	JaratformDlg.Destroy;

(*
	JaratValaszto( M0, M1);
	jaratkod	:= M0.Text;
  	if jaratkod <> '' then begin
   	Ellenorzes;
     	BitBtn4.SetFocus;
  	end;
*)
end;

procedure TOssznyilDlg.M1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
	if Key = VK_RETURN then begin
		JaratIro;
		BitBtn6.SetFocus;
   end;
end;

procedure TOssznyilDlg.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
	EgyebDlg.FormReturn(Key);
end;

procedure TOssznyilDlg.BitBtn7Click(Sender: TObject);
var
	rendsza : string;
begin
	// T�bb g�pkocsi kiv�laszt�sa
	TobbGepkocsiValaszto( M4, M40, rendsza) ;
	Ellenorzes;
end;

procedure TOssznyilDlg.BitBtn32Click(Sender: TObject);
begin
	// Ki�r�tj�k a rendsz�mokat
	M4.Text		:= '';
  	M40.Text	:= '';
   Ellenorzes;
end;

procedure TOssznyilDlg.BitBtn8Click(Sender: TObject);
begin
	// Ki�r�tj�k a j�ratot
	M1.Text		:= '';
	M0.Text		:= '';
   Ellenorzes;
end;

procedure TOssznyilDlg.Ellenorzes;
var
	jarat	: string;
   dat1	: string;
   dat2	: string;
	rendsz	: string;
   str		: string;
begin
  	{Megn�zz�k, hogy a sz�r�si felt�telekben szerepl� valamennyi j�rat ellen�rz�tt-e}
  	jarat		:= M0.Text;
  	dat1		:= MD1.Text;
	dat2		:= MD2.Text;
	if dat1 = '' then begin
  		dat1	:= '2000.01.01.';
  	end;
  	if dat2 = '' then begin
  		dat2	:= EgyebDlg.MaiDatum;
  	end;
  	if not Jodatum(dat1) then begin
     	Exit;
  	end;
  	if not Jodatum(dat2) then begin
		Exit;
  	end;
   vegstr	:= ' WHERE JA_JKEZD >= '''+dat1+''' AND JA_JKEZD <= '''+dat2+''' ';
  	if jarat <> '' then begin
		vegstr	:= vegstr + ' AND JA_KOD IN ('+jarat+') ';
	end else begin
		vegstr	:= vegstr + ' AND JA_FAZIS <> 2 ';	// Storn�zott j�ratok nem kellenek
	end;
   rendsz	:= M4.Text;
  	if rendsz <> '' then begin
     	vegstr	:= vegstr + ' AND JA_RENDSZ IN ( ';
		str		:= rendsz;
     	while Pos(',',str) > 0 do begin
     		vegstr	:= vegstr + '''' + copy(str,1,Pos(',',str)-1) + ''',';
        	str		:= copy(str, Pos(',',str)+1,999);
     	end;
		if str <> '' then begin
     		vegstr	:= vegstr + '''' + str + ''',';
       end;
     	vegstr	:= copy(vegstr, 1, Length(vegstr)-1) +' ) ';
  	end;
	Query_Run(Query1, 'SELECT JA_KOD FROM JARAT ' + vegstr + ' AND JA_FAZIS <> ''1'' ' );
   Label5.Hide;
   if Query1.RecordCount < 1 then begin
		Query_Run(Query1, 'SELECT JA_KOD FROM JARAT ' + vegstr + ' AND JA_FAZIS = ''1'' ' );
		if Query1.RecordCount > 0 then begin
       	Label5.Show;
       end;
	end;
   Label5.Update;
end;

procedure TOssznyilDlg.M1Exit(Sender: TObject);
begin
	JaratIro;
end;

procedure TOssznyilDlg.JaratIro;
var
	str,jev	: string;
begin
	{Megkeress�k a megadott sz�m� j�ratot}
	if M1.Text = '' then begin
		M0.Text		:= '';
		jaratkod	:= '';
	end else begin
		str	:= M1.Text;
		if Pos('-', str) > 0 then begin
			str	:= copy(str, Pos('-', str) + 1, 999 ) ;
		end;
    jev:=copy(MD1.Text,1,4);
    if jev='' then
       jev:= EgyebDlg.EvSzam  ;
		EgyebDlg.SetADOQueryDatabase(Query1);
		//if Query_Run(Query1, 'SELECT JA_KOD, JA_ORSZ, JA_ALKOD, JA_FAZIS FROM JARAT WHERE JA_ALKOD = '+
		//	IntToStr(StrToIntDef(str,0)) ,true) then begin
		if Query_Run(Query1, 'SELECT JA_KOD, JA_ORSZ, JA_ALKOD, JA_FAZIS FROM JARAT WHERE JA_ALKOD = '+
			IntToStr(StrToIntDef(str,0))+' and SUBSTRING(JA_JKEZD,1,4)='''+jev+''' ' ,true) then begin
			M1.Text 	:= '';
			if Query1.RecordCount > 0 then begin
				M1.Text 	:= GetJaratszamFromDb(Query1);
				jaratkod	:= Query1.FieldByname('JA_KOD').AsString;
				M0.Text		:= jaratkod;
				Ellenorzes;
			end;
		end;
		Query1.Close;
	end;
end;

{procedure TOssznyilDlg.CheckBox1Click(Sender: TObject);
begin
	if CheckBox1.Checked then begin  // r�szletes lista
		CBGKat.ItemIndex	:= 0;
		CBGKat.Enabled		:= false;
		BitBtn1.Enabled		:= false;
		CL1.Enabled			:= false;
	end else begin
		CBGKat.Enabled		:= true;
		BitBtn1.Enabled		:= true;
		CL1.Enabled			:= true;
	end;
  CheckBox4.Enabled:=not CheckBox1.Checked;
  chkTermelo.Enabled:=not CheckBox1.Checked;  // ez a sz�r�s csak a nem r�szletes list�hoz �rv�nyes

end;
}

procedure TOssznyilDlg.KategTolto(Sender: TObject);
begin
	if CL1.ItemIndex = 0 then begin
		NoticeKi('Intervallumokat csak egyes kateg�ri�khoz rendelhet�nk, az �sszeshez nem! ');
		Exit;
	end;
	// Bet�ltj�k a kateg�ri�hoz tartoz� intervallumokat
	Application.CreateForm(TKikatbeDlg, KikatbeDlg);
	KikatbeDlg.Tolt(listagkat[CL1.ItemIndex]);
	KikatbeDlg.ShowModal;
	KikatbeDlg.Destroy;
end;

procedure TOssznyilDlg.CL1Click(Sender: TObject);
var
	i : integer;
begin
	// Ellen�rzi, hogy az els� kiv�lasztott-e
	if CL1.Checked[0] then begin
		for i := 1 to CL1.Items.Count - 1 do begin
			CL1.Checked[i]	:= true;
		end;
	end;
end;

procedure TOssznyilDlg.ComboBox1Change(Sender: TObject);
begin
  {case ComboBox1.ItemIndex of
     0: begin  // �sszes�t�
    		CBGKat.Enabled		:= true;
    		BitBtn1.Enabled		:= true;
  	  	CL1.Enabled			:= true;
        CheckBox4.Enabled:= False;
        chkTermelo.Enabled:= False;
        end;
     1,2: begin  // r�szletes, Excel
        CBGKat.ItemIndex	:= 0;
    		CBGKat.Enabled		:= false;
    		BitBtn1.Enabled		:= false;
    		CL1.Enabled			:= false;
        CheckBox4.Enabled:= True;
        chkTermelo.Enabled:= True;
        end;
     end;  // case
     }

  	 CBGKat.Enabled		:= true;
  	 BitBtn1.Enabled		:= true;
     CL1.Enabled			:= true;
     case ComboBox1.ItemIndex of
     0: begin  // �sszes�t�
        CheckBox4.Enabled:= False;
        chkTermelo.Enabled:= False;
        end;
     1,2: begin  // r�szletes, Excel
        CheckBox4.Enabled:= True;
        chkTermelo.Enabled:= True;
        end;
     end;  // case
end;

end.


