unit NVCKIVFm;

interface

uses
	SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
	StdCtrls, Forms, ExtCtrls, Dialogs, J_FOFORMUJ;

type
	TNVCKIVFormDlg = class(TJ_FOFORMUJDLG)
	  procedure FormCreate(Sender: TObject);override;
	 procedure Adatlap(cim, kod : string);override;
	end;

var
	NVCKIVFormDlg : TNVCKIVFormDlg;

implementation

uses
	Egyeb, NVCKIVbe, J_SQL;

{$R *.DFM}

procedure TNVCKIVFormDlg.FormCreate(Sender: TObject);
begin
	Inherited FormCreate(Sender);
	AddSzuromezoRovid('KI_RENSZ',  'NVCKIVETEL');
	// AddSzuromezo('DO_NAME', 'SELECT DISTINCT DO_NAME FROM TELEFON LEFT OUTER JOIN DOLGOZO ON TE_TULAJ = DO_KOD');
	FelTolto('Igazolt gyorshajtás felvitele ', 'Igazolt gyorshajtás adatok módosítása ',
			'Igazolt gyorshajtások listája', 'KI_KOD', 'SELECT * FROM NVCKIVETEL ','NVCKIVETEL', QUERY_ADAT_TAG );
	nevmezo	:= 'KI_RENSZ';
	width	:= DLG_WIDTH;
	height	:= DLG_HEIGHT;
end;

procedure TNVCKIVFormDlg.Adatlap(cim, kod : string);
begin
	Application.CreateForm(TNVCKIVbeDlg, AlForm);
	Inherited Adatlap(cim, kod );
end;

end.

