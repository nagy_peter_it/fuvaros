unit Szamlabe;

interface

uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, StdCtrls, Buttons, Mask, Grids, DBTables, DB, J_ALFORM,
  ADODB, ExtCtrls, KozosTipusok; //acPNG,


const
   TERMEK_DARAB = 11;
  // A megfelel� term�kk�dokhoz tartoz� b�zissz�t�r alk�dok a k�l�nb�z� nyelveken
   TERMEKEK : array [1..3, 1..TERMEK_DARAB] of string =
       (
       ('677','679','671','672','673','674','675','676','680','681','TERMEK1_HUN'),    // HUN
       ('777','779','771','772','773','774','775','776','780','781','TERMEK1_ENG'),    // ENG
       ('877','879','871','872','873','874','875','876','880','881','TERMEK1_GER')     // GER
       );


type
  TSzamlaBeDlg = class(TJ_AlformDlg)
    Panel1: TPanel;
    ButtonKilep: TBitBtn;
    GroupBox3: TGroupBox;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    MaskEdit10: TMaskEdit;
    MaskEdit11: TMaskEdit;
    MaskEdit9: TMaskEdit;
    Panel2: TPanel;
    ButtonKuld: TBitBtn;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label17: TLabel;
    MaskEdit1: TMaskEdit;
    MaskEdit2: TMaskEdit;
    MaskEdit3: TMaskEdit;
    MaskEdit4: TMaskEdit;
    ButValVevo: TBitBtn;
    MaskEdit5: TMaskEdit;
    BitBtn12: TBitBtn;
    MaskEdit18: TMaskEdit;
    GroupBox2: TGroupBox;
    Label2: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label13: TLabel;
    Label12: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label11: TLabel;
    Label16: TLabel;
    MaskEdit6: TMaskEdit;
    MaskEdit7: TMaskEdit;
    MaskEdit8: TMaskEdit;
    ComboBox1: TComboBox;
    MaskEdit13: TMaskEdit;
    MaskEdit12: TMaskEdit;
    MaskEdit14: TMaskEdit;
    MaskEdit15: TMaskEdit;
    BitBtn4: TBitBtn;
    BitBtn10: TBitBtn;
    BitBtn8: TBitBtn;
    BitBtn9: TBitBtn;
    BitBtn6: TBitBtn;
    CheckBox2: TCheckBox;
    MaskEdit16: TMaskEdit;
    BitBtn7: TBitBtn;
    BitBtn32: TBitBtn;
    CheckBox3: TCheckBox;
    BitBtn11: TBitBtn;
    MaskEdit17: TMaskEdit;
    BitBtn5: TBitBtn;
    Query1: TADOQuery;
    Query2: TADOQuery;
    Query3: TADOQuery;
    Query4: TADOQuery;
    QFej: TADOQuery;
    QSor: TADOQuery;
    QueryDij: TADOQuery;
    Panel3: TPanel;
    GroupBox4: TGroupBox;
    SorGrid: TStringGrid;
    Panel4: TPanel;
    SpeedButton5: TSpeedButton;
    SpeedButton4: TSpeedButton;
    SpeedButton3: TSpeedButton;
    SpeedButton6: TSpeedButton;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    BitBtn3: TBitBtn;
    BitSzamla: TBitBtn;
    Label18: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    MaskEdit100: TMaskEdit;
    MaskEdit110: TMaskEdit;
    MaskEdit90: TMaskEdit;
    Label21: TLabel;
    Label22: TLabel;
    MaskEdit19: TMaskEdit;
    BitBtn13: TBitBtn;
    Label23: TLabel;
    Label40: TLabel;
    cbNyelv: TComboBox;
    GroupBox5: TGroupBox;
    Label42: TLabel;
    Label43: TLabel;
    Label44: TLabel;
    Label45: TLabel;
    Label46: TLabel;
    Label47: TLabel;
    Label48: TLabel;
    Label49: TLabel;
    M1: TMaskEdit;
    M2: TMaskEdit;
    M4: TMaskEdit;
    M5: TMaskEdit;
    M6: TMaskEdit;
    M8: TMaskEdit;
    M7: TMaskEdit;
    M3: TMaskEdit;
    imgValutaZarolva: TImage;
    Label24: TLabel;
    OrszagCombo: TComboBox;
    procedure ButtonKilepClick(Sender: TObject);
    procedure Tolto(cim: string; teko: string); override;
    procedure Tolt2(cim: string; jarat, viszony: string);
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure ButValVevoClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure ButtonKuldClick(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Osszesit;
    procedure Modosit(Sender: TObject);
    procedure MaskEdit7Exit(Sender: TObject);
    procedure BitBtn4Click(Sender: TObject);
    procedure BitBtn5Click(Sender: TObject);
    function Elkuldes: boolean;
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure SorBeolvaso (SorBeolvasoMod: TSorBeolvasoMod);
    procedure SorAdoUj(szoveg: string; ossz: double);
    procedure BitBtn10Click(Sender: TObject);
    procedure BitBtn8Click(Sender: TObject);
    procedure BitBtn9Click(Sender: TObject);
    procedure SorAdo2;
    procedure Tiltas;
    procedure BitBtn6Click(Sender: TObject);
    procedure CheckBox3Click(Sender: TObject);
    procedure CheckBox2Click(Sender: TObject);
    procedure BitSzamlaClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure BitBtn32Click(Sender: TObject);
    procedure BitBtn7Click(Sender: TObject);
    procedure BitBtn11Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure SpeedButton5Click(Sender: TObject);
    procedure SpeedButton6Click(Sender: TObject);
    procedure BitBtn12Click(Sender: TObject);
    procedure PotlekBeszuras(uapotleksz, ossz: double);
    procedure MaskEdit8Exit(Sender: TObject);
    procedure MaskEdit19Exit(Sender: TObject);
    procedure BitBtn13Click(Sender: TObject);
    procedure cbNyelvChange(Sender: TObject);
    procedure NyelvFrissit;
    procedure MaskEdit4Change(Sender: TObject);
    procedure FizHatarido_beallit;
  private
    sorszam: integer;
    modlist: TStringList;
    orszaglist : TStringLIst;
    sorstr: TStringList;
    nyelvlist: TStringList;
    modosult: boolean;
    mell1, mell2, mell3, mell4, mell5, mell6, mell7: string;
    fej1, fej2, fej3, fej4, fej5, fej6, fej7: string;
    nemell: array [1 .. 19] of string;
    lejarat: integer;
    jaratkod: string;
    vevoafa: string;
    fejuj13, fejuj14, fejuj15, fejuj23, fejuj24, fejuj25, fejuj26: string;
    eurmegj1, eurmegj2, eurmegj3: string;
    vevokod: string;
    arfdat: string;
    cmrszamok: string;
    viszonylat: string;
    kelldatum: boolean;
    vevobank: boolean;
    nyelvazon: integer;
    szamlavaluta: string;
    // a VISZONY-ban vagy SZFEJ-ben megadott eredeti sz�mla valutanem
  public
    potkocsistr: string;
    GKKAT: string;
  end;

var
  SzamlaBeDlg: TSzamlaBeDlg;

const
  GRIDOSZLOP = 22;

implementation

uses

  Egyeb, VevoUjfm, Mellek, Jaratfm, J_SQL, Kozos, SzamSorbe,
  SzamlaFm, J_Valaszto, Vevobe, SzamlaNezo, Kozos_Local, Math;

// RandVal, SzamPe02, ,

{$R *.DFM}

procedure TSzamlaBeDlg.ButtonKilepClick(Sender: TObject);
begin
  ret_kod := '';
  if modosult then begin
    if NoticeKi('M�dosultak az adatok. Ki akar l�pni ment�s n�lk�l?',
      NOT_QUESTION) = 0 then begin
      Close;
    end;
  end
  else begin
    Close;
  end;
end;

procedure TSzamlaBeDlg.Tolto(cim: string; teko: string);
var
  i: integer;
  cmrdb: integer;
  nyelv: string;
begin
  ret_kod := teko;
  Caption := cim;
  potkocsistr := '';
  MaskEdit1.Text := '';
  MaskEdit2.Text := '';
  MaskEdit3.Text := '';
  MaskEdit4.Text := '';
  MaskEdit5.Text := '';
  MaskEdit18.Text := '';
  MaskEdit6.Text := EgyebDlg.MaiDatum;
  // if (not EgyebDlg.AUTOSZAMLA)or(EgyebDlg.v_formatum<>SZAMLA_FORMA_JS) then
  if (not EgyebDlg.AUTOSZAMLA) or (kelldatum) then
    MaskEdit7.Text := EgyebDlg.MaiDatum;
  MaskEdit8.Text := DatumHoznap(MaskEdit7.Text, lejarat, true);
  MaskEdit9.Text := '';
  MaskEdit10.Text := '';
  MaskEdit11.Text := '';
  MaskEdit90.Text := '';
  MaskEdit100.Text := '';
  MaskEdit110.Text := '';
  MaskEdit12.Text := '';
  MaskEdit13.Text := '';
  MaskEdit14.Text := '';
  MaskEdit15.Text := '';
  for i := 1 to 18 do begin
    nemell[i] := '';
  end;
  if ret_kod <> '' then begin
    if Query_Run(QFej, 'SELECT * FROM SZFEJ WHERE SA_UJKOD = ' + ret_kod, true) then begin
      if QFej.RecordCount > 0 then begin
        vevokod := '';
        arfdat := '';
        Query_Run(QSor, 'SELECT * FROM VISZONY WHERE VI_UJKOD = ' + ret_kod);
        if QSor.RecordCount > 0 then begin
          vevokod := QSor.FieldByName('VI_VEKOD').AsString;
          arfdat := QSor.FieldByName('VI_BEDAT').AsString;
          if StrToIntDef(Query_Select('VEVO', 'V_KOD', vevokod, 'V_ARNAP'), 0) > 0
          then begin
            arfdat := QSor.FieldByName('VI_KIDAT').AsString;
          end;
          viszonylat := QSor.FieldByName('VI_VIKOD').AsString;
        end;
        if QFej.FieldByName('SA_VEVOKOD').AsString <> '' then begin
          vevokod := QFej.FieldByName('SA_VEVOKOD').AsString;
          vevobank := Query_Select('VEVO', 'V_KOD', vevokod, 'VE_VBANK') <> '';
        end;
        MaskEdit1.Text := QFej.FieldByName('SA_VEVONEV').AsString;
        MaskEdit2.Text := QFej.FieldByName('SA_VEIRSZ').AsString;
        MaskEdit3.Text := QFej.FieldByName('SA_VEVAROS').AsString;
        MaskEdit4.Text := QFej.FieldByName('SA_VECIM').AsString;
        M1.Text := QFej.FieldByName('SA_KERULET').AsString;
        M2.Text := QFej.FieldByName('SA_KOZTER').AsString;
        M3.Text := QFej.FieldByName('SA_JELLEG').AsString;
        M4.Text := QFej.FieldByName('SA_HAZSZAM').AsString;
        M5.Text := QFej.FieldByName('SA_EPULET').AsString;
        M6.Text := QFej.FieldByName('SA_LEPCSO').AsString;
        M7.Text := QFej.FieldByName('SA_SZINT').AsString;
        M8.Text := QFej.FieldByName('SA_AJTO').AsString;
        MaskEdit5.Text := QFej.FieldByName('SA_VEADO').AsString;
        MaskEdit18.Text := QFej.FieldByName('SA_EUADO').AsString;
        ComboBox1.Text := QFej.FieldByName('SA_FIMOD').AsString;
        MaskEdit6.Text := QFej.FieldByName('SA_TEDAT').AsString;
        MaskEdit7.Text := QFej.FieldByName('SA_KIDAT').AsString;
        MaskEdit8.Text := QFej.FieldByName('SA_ESDAT').AsString;
        MaskEdit19.Text := QFej.FieldByName('SA_ARFDAT').AsString;
        Label23.Caption := SzamString(QFej.FieldByName('SA_VALARF')
          .AsFloat, 8, 2);
        if Query_Run(Query3, 'SELECT * FROM VEVO WHERE V_KOD = ''' + vevokod +
          ''' ', true) then
          if StrToIntDef(Query3.FieldByName('V_LEJAR').AsString, 0) > 0 then
            lejarat := StrToIntDef(Query3.FieldByName('V_LEJAR').AsString, 0);
        if MaskEdit7.Text = '' then begin
          // if (not EgyebDlg.AUTOSZAMLA)or(EgyebDlg.v_formatum<>SZAMLA_FORMA_JS) then
          if (not EgyebDlg.AUTOSZAMLA) or (kelldatum) then begin
            // if Query_Run (Query3, 'SELECT * FROM VEVO WHERE V_KOD = '''+vevokod+''' ',true) then
            // if StrToIntDef(Query3.FieldByName('V_LEJAR').AsString,0) > 0 then
            // lejarat	:= StrToIntDef(Query3.FieldByName('V_LEJAR').AsString,0);

            MaskEdit7.Text := EgyebDlg.MaiDatum;
            MaskEdit8.Text := DatumHoznap(MaskEdit7.Text, lejarat, true);
          end;
        end;
        MaskEdit9.Text := SzamString(QFej.FieldByName('SA_OSSZEG').AsFloat, 12, 0);
        MaskEdit10.Text := SzamString(QFej.FieldByName('SA_AFA').AsFloat, 12, 0);
        MaskEdit11.Text := SzamString(QFej.FieldByName('SA_OSSZEG').AsFloat +
          QFej.FieldByName('SA_AFA').AsFloat, 12, 0);
        MaskEdit90.Text := SzamString(QFej.FieldByName('SA_VALOSS').AsFloat, 12, 2);
        MaskEdit100.Text := SzamString(QFej.FieldByName('SA_VALAFA').AsFloat, 12, 2);
        MaskEdit110.Text := SzamString(QFej.FieldByName('SA_VALOSS').AsFloat +
          QFej.FieldByName('SA_VALAFA').AsFloat, 12, 2);
        MaskEdit12.Text := QFej.FieldByName('SA_JARAT').AsString;
        MaskEdit13.Text := QFej.FieldByName('SA_MEGJ').AsString;
        MaskEdit14.Text := QFej.FieldByName('SA_POZICIO').AsString;
        MaskEdit15.Text := QFej.FieldByName('SA_RSZ').AsString;
        MaskEdit16.Text := QFej.FieldByName('SA_FOKON').AsString;
        MaskEdit17.Text := QFej.FieldByName('SA_MEGRE').AsString;
        CheckBox2.Checked := (QFej.FieldByName('SA_HELYE').AsString = '1');
        ComboSet(OrszagCombo, QFej.FieldByName('SA_ORSZ2').AsString, orszaglist);
        nyelv := QFej.FieldByName('SA_NYELV').AsString;
        if (nyelv = '') then
          nyelv := Query_Select('VEVO', 'V_KOD', vevokod, 'VE_NYELV');
        ComboSet(cbNyelv, nyelv, nyelvlist);
        if QFej.FieldByName('SA_SAEUR').AsString = '1' then
          szamlavaluta := 'EUR'
        else
          szamlavaluta := 'HUF';

        CheckBox3.Checked := (szamlavaluta = 'EUR');

        if EgyebDlg.szamla_devizanem_valtas_engedelyez then begin
          CheckBox3.Enabled := true;
          imgValutaZarolva.Visible := False;
        end
        else begin
          if szamlavaluta <> 'HUF' then begin
            CheckBox3.Enabled := CheckBox3.Checked;
            // ha EUR a sz�mla, akkor lehessen HUF-ba kapcsolni, de ford�tva ne
            imgValutaZarolva.Visible := not(CheckBox3.Enabled);
          end
          else begin // HUF: z�rolva a sz�mla devizaneme
            CheckBox3.Enabled := False;
            imgValutaZarolva.Visible := true;
          end; // else
        end;
        eurmegj1 := QFej.FieldByName('SA_EGYA1').AsString;
        eurmegj2 := QFej.FieldByName('SA_EGYA2').AsString;
        eurmegj3 := QFej.FieldByName('SA_EGYA3').AsString;

        mell1 := QFej.FieldByName('SA_MELL1').AsString;
        mell2 := QFej.FieldByName('SA_MELL2').AsString;
        mell3 := QFej.FieldByName('SA_MELL3').AsString;
        mell4 := QFej.FieldByName('SA_MELL4').AsString;
        mell5 := QFej.FieldByName('SA_MELL5').AsString;
        mell6 := QFej.FieldByName('SA_MELL6').AsString;
        mell7 := QFej.FieldByName('SA_MELL7').AsString;

        if QFej.FieldByName('SA_FEJL1').AsString <> '' then begin
          { Van a fejl�cben valami, t�lts�k be }
          fej1 := QFej.FieldByName('SA_FEJL1').AsString;
          fej2 := QFej.FieldByName('SA_FEJL2').AsString;
          fej3 := QFej.FieldByName('SA_FEJL3').AsString;
          fej4 := QFej.FieldByName('SA_FEJL4').AsString;
          fej5 := QFej.FieldByName('SA_FEJL5').AsString;
          fej6 := QFej.FieldByName('SA_FEJL6').AsString;
          fej7 := QFej.FieldByName('SA_FEJL7').AsString;
        end;

        { // A vev�n�v alapj�n megkeress�k a vev�t
          Query_Run (Query3, 'SELECT * FROM VEVO WHERE V_KOD = '''+vevokod+''' ',true);
          if Query3.RecordCount > 0 then begin
          // A vev�h�z tartoz� bank bet�lt�se
          bakod  			:= Trim(Query3.FieldByName('VE_VBANK').AsString);
          fejuj13			:= EgyebDlg.Read_SZGrid('320', bakod);
          fejuj14			:= EgyebDlg.Read_SZGrid('320', IntToStr(StrToIntDef(bakod,0) + 1));
          fejuj15			:= EgyebDlg.Read_SZGrid('320', IntToStr(StrToIntDef(bakod,0) + 2));
          fejuj23			:= EgyebDlg.Read_SZGrid('320', IntToStr(StrToIntDef(bakod,0) + 4));
          fejuj24			:= EgyebDlg.Read_SZGrid('320', IntToStr(StrToIntDef(bakod,0) + 5));
          fejuj25			:= EgyebDlg.Read_SZGrid('320', IntToStr(StrToIntDef(bakod,0) + 6));
          fejuj26			:= EgyebDlg.Read_SZGrid('320', IntToStr(StrToIntDef(bakod,0) + 7));
          end;
        }

        // Elj�r�s, amely figyeli hogy van e �rfolyam. Ha nincs, akkor p�tolja.

        Query_Run(QSor, 'SELECT * FROM SZSOR WHERE SS_UJKOD = ' + ret_kod +
          ' order by SS_SOR', true);
        if QSor.RecordCount > 0 then begin
          while not QSor.EOF do begin
            if sorszam > 0 then begin
              SorGrid.RowCount := SorGrid.RowCount + 1;
            end;
            SorGrid.Cells[0, SorGrid.RowCount - 1] :=
              QSor.FieldByName('SS_SOR').AsString;
            SorGrid.Cells[1, SorGrid.RowCount - 1] :=
              QSor.FieldByName('SS_TERKOD').AsString;
            SorGrid.Cells[2, SorGrid.RowCount - 1] :=
              QSor.FieldByName('SS_TERNEV').AsString;
            SorGrid.Cells[3, SorGrid.RowCount - 1] :=
              QSor.FieldByName('SS_ITJSZJ').AsString;
            SorGrid.Cells[4, SorGrid.RowCount - 1] :=
              QSor.FieldByName('SS_VALNEM').AsString;
            SorGrid.Cells[5, SorGrid.RowCount - 1] :=
              SzamString(QSor.FieldByName('SS_VALERT').AsFloat, 12, 2);
            SorGrid.Cells[6, SorGrid.RowCount - 1] :=
              EgyebDlg.Read_SZGrid('101', QSor.FieldByName('SS_AFAKOD')
              .AsString);
            SorGrid.Cells[7, SorGrid.RowCount - 1] :=
              SzamString(QSor.FieldByName('SS_VALARF').AsFloat, 12, 4);
            SorGrid.Cells[8, SorGrid.RowCount - 1] := '';
            SorGrid.Cells[9, SorGrid.RowCount - 1] :=
              SzamString(QSor.FieldByName('SS_EGYAR').AsFloat, 12, 0);
            SorGrid.Cells[10, SorGrid.RowCount - 1] :=
              QSor.FieldByName('SS_TERKOD').AsString;
            SorGrid.Cells[11, SorGrid.RowCount - 1] :=
              QSor.FieldByName('SS_LEIRAS').AsString;
            SorGrid.Cells[12, SorGrid.RowCount - 1] :=
              QSor.FieldByName('SS_AFAKOD').AsString;
            SorGrid.Cells[13, SorGrid.RowCount - 1] :=
              QSor.FieldByName('SS_KULNEV').AsString;
            SorGrid.Cells[14, SorGrid.RowCount - 1] :=
              QSor.FieldByName('SS_KULLEI').AsString;
            SorGrid.Cells[15, SorGrid.RowCount - 1] :=
              QSor.FieldByName('SS_DARAB').AsString;
            SorGrid.Cells[16, SorGrid.RowCount - 1] :=
              QSor.FieldByName('SS_MEGY').AsString;
            SorGrid.Cells[17, SorGrid.RowCount - 1] :=
              SzamString(QSor.FieldByName('SS_EGYAR').AsFloat, 12, 0);
            SorGrid.Cells[18, SorGrid.RowCount - 1] :=
              QSor.FieldByName('SS_KULME').AsString;
            SorGrid.Cells[19, SorGrid.RowCount - 1] :=
              QSor.FieldByName('SS_ARDAT').AsString;
            SorGrid.Cells[20, SorGrid.RowCount - 1] :=
              QSor.FieldByName('SS_ARFOL').AsString;
            SorGrid.Cells[21, SorGrid.RowCount - 1] :=
              QSor.FieldByName('SS_NEMEU').AsString;
            inc(sorszam);
            QSor.Next;
          end;
        end;
        { Enged�lyezi a m�dos�t�s �s t�rl�s buttonokat }
        if sorszam > 0 then begin
          BitBtn2.Enabled := true;
          BitBtn3.Enabled := true;
        end;
        Osszesit;
      end;
      // Bet�ltj�k a viszonyhoz kapcsol�d� CMR sz�mokat
      cmrszamok := '';
      cmrdb := 0;

      if (viszonylat <> '') and
        (Query_Run(Query2, 'SELECT CM_CSZAM FROM CMRDAT WHERE CM_VIKOD = ''' +
        viszonylat + ''' ORDER BY CM_CSZAM ', true)) then begin
        Query2.First;
        while not Query2.EOF do begin
          cmrszamok := cmrszamok + Query2.FieldByName('CM_CSZAM')
            .AsString + ',';
          inc(cmrdb);
          Query2.Next;
        end;
        if Length(cmrszamok) > 1 then begin
          cmrszamok := copy(cmrszamok, 1, Length(cmrszamok) - 1);
        end;
      end;
      if cmrdb > 0 then begin
        cmrszamok := IntToStr(cmrdb) + ' db cmr : ' + cmrszamok;
        if mell5 = EmptyStr then
          mell5 := mell5 + cmrszamok;
        // else if mell6=EmptyStr then	mell6		:= mell6 + cmrszamok
        // else if mell7=EmptyStr then	mell7		:= mell7 + cmrszamok
      end;
    end;
  end;
  modosult := False;
  if megtekint then begin
    Tiltas;
  end;
end;

procedure TSzamlaBeDlg.BitBtn1Click(Sender: TObject);
var
  ii: integer;
begin
  Application.CreateForm(TSzamSorbeDlg, SzamSorbeDlg);
  { Az �rfolyam d�tuma a teljes�t�sb�l j�n }
  SzamSorbeDlg.datum := MaskEdit6.Text;
  sorstr.Clear;
  sorstr.Add(IntToStr(sorszam + 1));
  for ii := 1 to GRIDOSZLOP do begin
    sorstr.Add('');
  end;
  SzamSorbeDlg.vevokod := vevokod;
  SzamSorbeDlg.Tolt(sorstr);
  SzamSorbeDlg.ShowModal;
  if sorstr.Count > 0 then begin
    modosult := true;
    if sorszam > 0 then begin
      SorGrid.RowCount := SorGrid.RowCount + 1;
    end;
    for ii := 0 to GRIDOSZLOP - 1 do begin
      SorGrid.Cells[ii, SorGrid.RowCount - 1] := sorstr[ii];
    end;
    inc(sorszam);
    { Enged�lyezi a m�dos�t�s �s t�rl�s buttonokat }
    BitBtn2.Enabled := true;
    BitBtn3.Enabled := true;
    SorGrid.Row := SorGrid.RowCount - 1;
  end;
  SzamSorbeDlg.Destroy;
  Osszesit;
end;

procedure TSzamlaBeDlg.BitBtn3Click(Sender: TObject);
var
  j: integer;
  i: integer;
begin
  if NoticeKi('Val�ban t�r�lni szeretn� a kijel�lt t�telt?', NOT_QUESTION) = 0
  then begin
    { A bels� t�bla karbantart�sa a sor kit�rl�s�vel }
    modosult := true;
    if SorGrid.Row < (SorGrid.RowCount - 1) then begin
      for j := SorGrid.Row to SorGrid.RowCount - 1 do begin
        for i := 1 to GRIDOSZLOP - 1 do begin
          SorGrid.Cells[i, j] := SorGrid.Cells[i, j + 1];
        end;
      end;
    end;
    for i := 0 to GRIDOSZLOP - 1 do begin
      SorGrid.Cells[i, SorGrid.RowCount - 1] := '';
    end;
    if sorszam > 1 then begin
      SorGrid.RowCount := SorGrid.RowCount - 1;
    end;
    Dec(sorszam);
    if SorGrid.Row > 1 then begin
      SorGrid.Row := SorGrid.Row - 1;
    end;
    if sorszam = 0 then begin
      { Tiltja a m�dos�t�s �s t�rl�s buttonokat }
      BitBtn2.Enabled := False;
      BitBtn3.Enabled := False;
    end;
    SorGrid.Refresh;
    BitBtn1.Enabled := true;
  end;
  Osszesit;
end;

procedure TSzamlaBeDlg.ButValVevoClick(Sender: TObject);
begin
  Application.CreateForm(TVevoUjFmDlg, VevoUjFmDlg);
  VevoUjFmDlg.valaszt := true;
  VevoUjFmDlg.ShowModal;
  if VevoUjFmDlg.ret_vkod <> '' then begin
    if VevoUjFmDlg.felista > 0 then begin
      if NoticeKi('Feketelist�s vev�! Folytatja?', NOT_QUESTION) <> 0 then begin
        VevoUjFmDlg.Destroy;
        Exit;
      end;
    end;
    if Query_Run(Query3, 'SELECT * FROM VEVO WHERE V_KOD = ''' +
      VevoUjFmDlg.ret_vkod + ''' ', true) then begin
      vevokod := VevoUjFmDlg.ret_vkod;
      MaskEdit1.Text := Query3.FieldByName('V_NEV').AsString;
      MaskEdit2.Text := Query3.FieldByName('V_IRSZ').AsString;
      MaskEdit3.Text := Query3.FieldByName('V_VAROS').AsString;
      MaskEdit4.Text := Query3.FieldByName('V_CIM').AsString;
      M1.Text := Query3.FieldByName('VE_KERULET').AsString;
      M2.Text := Query3.FieldByName('VE_KOZTER').AsString;
      M3.Text := Query3.FieldByName('VE_JELLEG').AsString;
      M4.Text := Query3.FieldByName('VE_HAZSZAM').AsString;
      M5.Text := Query3.FieldByName('VE_EPULET').AsString;
      M6.Text := Query3.FieldByName('VE_LEPCSO').AsString;
      M7.Text := Query3.FieldByName('VE_SZINT').AsString;
      M8.Text := Query3.FieldByName('VE_AJTO').AsString;
      MaskEdit5.Text := Query3.FieldByName('V_ADO').AsString;
      MaskEdit18.Text := Query3.FieldByName('VE_EUADO').AsString;
      ComboSet(OrszagCombo, Query3.FieldByName('V_ORSZ').AsString, orszaglist);

      // 2015-01-19: csak az SA_SAEUR (bet�lt�skor) �s a cbNyelv(bevitelkor) szab�lyozza az "EUR sz�mla"-t
      // 2015-03-12: SA_SAEUR (bet�lt�skor) �s a V_NEMET (bevitelkor) szab�lyozza az "EUR sz�mla"-t

      if szamlavaluta <> 'HUF' then begin
        CheckBox3.Checked := (Query3.FieldByName('V_NEMET').AsInteger > 0);
        imgValutaZarolva.Visible := not(CheckBox3.Enabled);
      end
      else begin // HUF: z�rolva a sz�mla devizaneme
        CheckBox3.Checked := False;
        CheckBox3.Enabled := False;
        imgValutaZarolva.Visible := true;
      end; // else
      // feloldjuk a z�r�st, ha nem haszn�ljuk ezt a funkci�t
      if EgyebDlg.szamla_devizanem_valtas_engedelyez then begin
        CheckBox3.Enabled := true;
        imgValutaZarolva.Visible := False;
      end;

      ComboSet(cbNyelv, Query3.FieldByName('VE_NYELV').AsString, nyelvlist);

      // CheckBox3.Checked:=CheckBox1.Checked;
        {
        if StrToIntDef(Query3.FieldByName('V_LEJAR').AsString, 0) > 0 then begin
          lejarat := StrToIntDef(Query3.FieldByName('V_LEJAR').AsString, 0);
          MaskEdit8.Text := DatumHoznap(MaskEdit7.Text, lejarat, true);
        end;
        }
      FizHatarido_beallit;
      ComboBox1.SetFocus;
      { // A vev�h�z tartoz� bank bet�lt�se
        bakod  			:= Trim(Query3.FieldByName('VE_VBANK').AsString);
        fejuj13			:= EgyebDlg.Read_SZGrid('320', bakod);
        fejuj14			:= EgyebDlg.Read_SZGrid('320', IntToStr(StrToIntDef(bakod,0) + 1));
        fejuj15			:= EgyebDlg.Read_SZGrid('320', IntToStr(StrToIntDef(bakod,0) + 2));
        fejuj23			:= EgyebDlg.Read_SZGrid('320', IntToStr(StrToIntDef(bakod,0) + 4));
        fejuj24			:= EgyebDlg.Read_SZGrid('320', IntToStr(StrToIntDef(bakod,0) + 5));
        fejuj25			:= EgyebDlg.Read_SZGrid('320', IntToStr(StrToIntDef(bakod,0) + 6));
        fejuj26			:= EgyebDlg.Read_SZGrid('320', IntToStr(StrToIntDef(bakod,0) + 7));
      }
      // CheckBox1Click(Sender);
    end
    else begin
      NoticeKi('Hib�s vev�k�d!');
      MaskEdit1.Text := '';
      MaskEdit2.Text := '';
      MaskEdit3.Text := '';
      MaskEdit4.Text := '';
      MaskEdit5.Text := '';
      MaskEdit18.Text := '';
    end;
  end;
  VevoUjFmDlg.Destroy;
end;

procedure TSzamlaBeDlg.FizHatarido_beallit;
var
  str: string;
begin
  Query_Log_Str('Fizetesi_hatarido_beallit_1, vevokod='+vevokod, 0, false, true);
  if length(vevokod) >= 4 then begin  // van �rv�nyes vev�k�d
    str := Query_Select('VEVO', 'V_KOD', vevokod, 'V_LEJAR');
    Query_Log_Str('Fizetesi_hatarido_beallit_2, str='+str, 0, false, true);
   if str <> '' then begin
      lejarat := StrToIntDef(str, 0);
      Query_Log_Str('Fizetesi_hatarido_beallit_3, lejarat='+IntToStr(lejarat), 0, false, true);
      end; // if
   end; // if
  if lejarat=0 then lejarat := Round(StringSzam(EgyebDlg.Read_SZGrid('999', '602')));
  Query_Log_Str('Fizetesi_hatarido_beallit_4, lejarat �jra='+IntToStr(lejarat), 0, false, true);
  MaskEdit8.Text := DatumHoznap(MaskEdit7.Text, lejarat, true);
  Query_Log_Str('Fizetesi_hatarido_beallit_5, MaskEdit8.Text='+MaskEdit8.Text, 0, false, true);
end;

procedure TSzamlaBeDlg.cbNyelvChange(Sender: TObject);
var
  tekod: string;
  maskod: string;
  cserekod: string;
  i: integer;
  j, k: integer;
begin
  NyelvFrissit;
  // A nyelvi sz�vegek lecser�l�se a sorokban
  for i := 1 to SorGrid.RowCount - 1 do begin
    tekod := SorGrid.Cells[1, i];
    cserekod := '';
    for j := 1 to 3 do begin
      for k := 1 to TERMEK_DARAB do begin
        maskod := EgyebDlg.Read_SZGrid('999', TERMEKEK[j, k]);
        if maskod = tekod then begin
          cserekod := EgyebDlg.Read_SZGrid('999', TERMEKEK[nyelvazon, k]);
        end;
      end;
    end;
    if cserekod <> '' then begin
      // Ki lehet cser�lni a sz�vegeket
      if Query_Run(Query4, 'SELECT * FROM TERMEK WHERE T_TERKOD = ''' + cserekod
        + ''' ', true) then begin
        SorGrid.Cells[1, i] := cserekod;
        SorGrid.Cells[2, i] := Query4.FieldByName('T_TERNEV').AsString;
        SorGrid.Cells[11, i] := Query4.FieldByName('T_LEIRAS').AsString;
      end;
    end;

  end;

end;

procedure TSzamlaBeDlg.NyelvFrissit;
var
  vbank: integer;
  NyelvKod: string;
begin
  NyelvKod := nyelvlist[cbNyelv.ItemIndex];
  // 2015-03-12: V_NEMET alapj�n vessz�k az alap�rtelmez�st
  // if NyelvKod='HUN' then
  // CheckBox3.Checked:=False    // nem EUR sz�mla
  // else
  // CheckBox3.Checked:=True;    // EUR sz�mla

  fej1 := EgyebDlg.Read_SZGrid('440', NyelvKod + '01'); // pl. 'GER01'
  fej2 := EgyebDlg.Read_SZGrid('440', NyelvKod + '02');
  fej3 := EgyebDlg.Read_SZGrid('440', NyelvKod + '03');
  fej4 := EgyebDlg.Read_SZGrid('440', NyelvKod + '04');
  fej5 := EgyebDlg.Read_SZGrid('440', NyelvKod + '05');
  fej6 := EgyebDlg.Read_SZGrid('440', NyelvKod + '06');
  fej7 := EgyebDlg.Read_SZGrid('440', NyelvKod + '07');

  mell1 := EgyebDlg.Read_SZGrid('450', NyelvKod + '01');
  mell2 := EgyebDlg.Read_SZGrid('450', NyelvKod + '02');
  mell3 := EgyebDlg.Read_SZGrid('450', NyelvKod + '03');
  mell4 := EgyebDlg.Read_SZGrid('450', NyelvKod + '04');

  if (NyelvKod <> 'HUN') and (fejuj23 <> '') then begin
    fej3 := fejuj23;
    fej4 := fejuj24;
    fej5 := fejuj25;
    fej6 := fejuj26;
  end; // if

  if vevobank then begin
    vbank := StrToIntDef(Query_Select('VEVO', 'V_KOD', vevokod, 'VE_VBANK'), 0);
    if NyelvKod = 'HUN' then begin
      fej3 := EgyebDlg.Read_SZGrid('320', IntToStr(vbank));
      fej4 := EgyebDlg.Read_SZGrid('320', IntToStr(vbank + 1));
      fej5 := EgyebDlg.Read_SZGrid('320', IntToStr(vbank + 2));
    end; // HUN
    if (NyelvKod = 'GER') or (NyelvKod = 'ENG') then begin
      fej3 := EgyebDlg.Read_SZGrid('320', IntToStr(vbank + 4));
      fej4 := EgyebDlg.Read_SZGrid('320', IntToStr(vbank + 5));
      fej5 := EgyebDlg.Read_SZGrid('320', IntToStr(vbank + 6));
    end; // GER
    // **********************
    // 'ENG' m�g nem megoldott
    // **********************
  end; // if

  nyelvazon := ((Pos(NyelvKod, 'HUNENGGER') - 1) div 3) + 1;
  // 1 = HUN; 2 = ENG; 3 = GER
end;

procedure TSzamlaBeDlg.FormDestroy(Sender: TObject);
begin
  modlist.Free;
  sorstr.Free;
  nyelvlist.Free;
end;

procedure TSzamlaBeDlg.ButtonKuldClick(Sender: TObject);
begin
  if Elkuldes then begin
    Close;
  end;
end;

procedure TSzamlaBeDlg.BitBtn2Click(Sender: TObject);
var
  ii: integer;
  i: integer;
begin
  sorstr.Clear;
  Application.CreateForm(TSzamSorbeDlg, SzamSorbeDlg);
  { Az �rfolyam d�tuma a teljes�t�sb�l j�n }
  SzamSorbeDlg.datum := MaskEdit6.Text;
  for i := 0 to GRIDOSZLOP - 1 do begin
    sorstr.Add(SorGrid.Cells[i, SorGrid.Row]);
  end;
  SzamSorbeDlg.vevokod := vevokod;
  SzamSorbeDlg.Tolt(sorstr);
  SzamSorbeDlg.ShowModal;
  if sorstr.Count > 0 then begin
    for ii := 0 to GRIDOSZLOP - 1 do begin
      SorGrid.Cells[ii, SorGrid.Row] := sorstr[ii];
    end;
    modosult := true;
  end;
  SzamSorbeDlg.Destroy;
  Osszesit;
end;

procedure TSzamlaBeDlg.FormCreate(Sender: TObject);
begin
   modosult := False;
   kelldatum := StringSzam(EgyebDlg.Read_SZGrid('999', '923')) = 1;
   EgyebDlg.SetADOQueryDatabase(Query1);
   EgyebDlg.SetADOQueryDatabase(Query2);
   EgyebDlg.SetADOQueryDatabase(QFej);
   EgyebDlg.SetADOQueryDatabase(QSor);
   EgyebDlg.SetADOQueryDatabase(Query3);
   EgyebDlg.SetADOQueryDatabase(Query4);
   EgyebDlg.SetADOQueryDatabase(QueryDij);
   SetMaskEdits([MaskEdit16]);
   SorGrid.ColCount := GRIDOSZLOP;
   SorGrid.Cells[0, 0] := 'Ssz.';
   SorGrid.Cells[1, 0] := 'Term/szolg.';
   SorGrid.Cells[2, 0] := 'Megnevez�s';
   SorGrid.Cells[3, 0] := 'VTSZ/SZJ';
   SorGrid.Cells[4, 0] := 'Valutanem';
   SorGrid.Cells[5, 0] := 'Valut�s egys.';
   SorGrid.Cells[6, 0] := '�FA %';
   SorGrid.Cells[7, 0] := '�rfolyam';
   SorGrid.Cells[8, 0] := 'Kedv. %';
   SorGrid.Cells[9, 0] := '�rt�k';
   SorGrid.Cells[10, 0] := 'T/SZ';
   SorGrid.Cells[11, 0] := 'Le�r�s';
   SorGrid.Cells[12, 0] := '�FA k�d';
   SorGrid.Cells[13, 0] := 'Idegen n�v';
   SorGrid.Cells[14, 0] := 'Idegen le�r�s';
   SorGrid.Cells[15, 0] := 'Mennyis�g';
   SorGrid.Cells[16, 0] := 'Me.egys.';
   SorGrid.Cells[17, 0] := 'Egys�g�r';
   SorGrid.Cells[18, 0] := 'K.megy.';
   SorGrid.Cells[19, 0] := '�rf.d�tum';
   SorGrid.Cells[20, 0] := '�rf.(EUR)';
   SorGrid.Cells[21, 0] := 'NEMEU FLAG';
   SorGrid.ColWidths[0] := 30;
   SorGrid.ColWidths[1] := 100;
   SorGrid.ColWidths[2] := 200;
   SorGrid.ColWidths[3] := 100;
   SorGrid.ColWidths[4] := 50;
   SorGrid.ColWidths[5] := 100;
   SorGrid.ColWidths[6] := 50;
   SorGrid.ColWidths[7] := 100;
   SorGrid.ColWidths[8] := -1;
   SorGrid.ColWidths[9] := 100;
   SorGrid.ColWidths[10] := -1;
   SorGrid.ColWidths[11] := 200;
   SorGrid.ColWidths[12] := -1;
   SorGrid.ColWidths[13] := 100;
   SorGrid.ColWidths[14] := 100;
   SorGrid.ColWidths[15] := 50;
   SorGrid.ColWidths[16] := 50;
   SorGrid.ColWidths[17] := 100;
   SorGrid.ColWidths[18] := 50;
   SorGrid.ColWidths[19] := 100;
   SorGrid.ColWidths[20] := 100;
   SorGrid.ColWidths[21] := 10;
   SorGrid.RowCount := 2;
   sorszam := 0;
   nyelvazon := 1;
   BitBtn2.Enabled := true;
   BitBtn3.Enabled := true;
   if sorszam = 0 then begin
       { Tiltja a m�dos�t�s �s t�rl�s buttonokat }
       BitBtn2.Enabled := False;
       BitBtn3.Enabled := False;
   end;
   modlist := TStringList.Create;
   sorstr := TStringList.Create;
   orszaglist := TStringList.Create;
   nyelvlist := TStringList.Create;
   ComboBox1.Clear;
   SzotarTolt(ComboBox1, '100', modlist, False);
   ComboBox1.ItemIndex := 0;
   SzotarTolt(cbNyelv, '430', nyelvlist, False);
	SzotarTolt(OrszagCombo,  '300' , orszaglist, false );
   ComboSet(cbNyelv, DefaultSzamlaNyelv, nyelvlist);
   mell1 := EgyebDlg.Read_SZGrid('999', '401');
   mell2 := EgyebDlg.Read_SZGrid('999', '402');
   mell3 := EgyebDlg.Read_SZGrid('999', '403');
   mell4 := EgyebDlg.Read_SZGrid('999', '404');
   mell5 := EgyebDlg.Read_SZGrid('999', '406');
   mell6 := '';
   mell7 := '';
   lejarat := Round(StringSzam(EgyebDlg.Read_SZGrid('999', '602')));
   jaratkod := '';
   vevoafa := '';
   // CheckBox1Click(Sender);
   CheckBox2.Enabled := False;
   fejuj13 := '';
   fejuj14 := '';
   fejuj15 := '';
   fejuj23 := '';
   fejuj24 := '';
   fejuj25 := '';
   fejuj26 := '';
   // Az eur�s sz�ml�n megjelen� egy�b adatok be�ll�t�sa
   eurmegj1 := '';
   eurmegj2 := '';
   eurmegj3 := '';
   vevokod := '';
   arfdat := '';

   SetMaskEdits([MaskEdit9, MaskEdit10, MaskEdit11]);
   SetMaskEdits([MaskEdit90, MaskEdit100, MaskEdit110]);

end;

procedure TSzamlaBeDlg.Osszesit;
var
  netto, afa: double;
  ertek: double;
  vnetto, vafa: double;
  ii: integer;
  vnem, safa: string;
begin
  netto := 0;
  afa := 0;
  vnetto := 0;
  vafa := 0;
  vnem := '';
  for ii := 1 to SorGrid.RowCount - 1 do begin
    ertek := StringSzam(SorGrid.Cells[17, ii]) *
      StringSzam(SorGrid.Cells[15, ii]);
    SorGrid.Cells[9, ii] := SzamString(ertek, 12, 0);
    netto := netto + ertek;
    safa := SorGrid.Cells[6, ii];
    afa := afa + Kerekit(StringSzam(SorGrid.Cells[6, ii]) * ertek / 100);
    vnetto := vnetto + StringSzam(SorGrid.Cells[15, ii]) *
      StringSzam(SorGrid.Cells[5, ii]);
    vafa := vafa + StringSzam(SorGrid.Cells[15, ii]) *
      StringSzam(SorGrid.Cells[5, ii]) * StringSzam(SorGrid.Cells[6, ii]) / 100;
    if SorGrid.Cells[4, ii] <> '' then
      vnem := SorGrid.Cells[4, ii];
  end;
  MaskEdit9.Text := SzamString(netto, 12, 0);
  MaskEdit10.Text := SzamString(afa, 12, 0);
  MaskEdit11.Text := SzamString(netto + afa, 12, 0);

  MaskEdit90.Text := SzamString(vnetto, 12, 2);
  MaskEdit100.Text := SzamString(vafa, 12, 2);
  MaskEdit110.Text := SzamString(vnetto + vafa, 12, 2);
  Label22.Caption := vnem;

end;

procedure TSzamlaBeDlg.Modosit(Sender: TObject);
begin
  modosult := true;
end;

procedure TSzamlaBeDlg.MaskEdit7Exit(Sender: TObject);
begin
  DatumExit(Sender, False);
  MaskEdit8.Text := DatumHoznap(MaskEdit7.Text, lejarat, true);
end;

procedure TSzamlaBeDlg.BitBtn4Click(Sender: TObject);
begin
  Application.CreateForm(TMellekForm, MellekForm);
  MellekForm.Tolt('Mell�kletek', mell1, mell2, mell3, mell4, mell5, mell6,
    mell7, 7);
  MellekForm.ToltUj;
  MellekForm.ShowModal;
  if not MellekForm.Kilepes then begin
    mell1 := MellekForm.mell1;
    mell2 := MellekForm.mell2;
    mell3 := MellekForm.mell3;
    mell4 := MellekForm.mell4;
    mell5 := MellekForm.mell5;
    mell6 := MellekForm.mell6;
    mell7 := MellekForm.mell7;
  end;
  MellekForm.Destroy;
end;

procedure TSzamlaBeDlg.BitBtn5Click(Sender: TObject);
var
  torlendo: boolean;
begin
  torlendo := False;
  if ret_kod = '' then begin
    torlendo := true; { Megtekint�s ut�n t�r�lni kell a rekordokat }
  end;
  if Elkuldes then begin
    // A sz�mla megjelen�t�se nem nyomtathat� form�ban
    Application.CreateForm(TSzamlaNezoDlg, SzamlaNezoDlg);
    SzamlaNezoDlg.SzamlaNyomtatas(StrToIntDef(ret_kod, 0), 0);
    SzamlaNezoDlg.Destroy;
    modosult := False;
    if torlendo then begin
      { A sz�mla rekordok t�rl�se }
      Query_Run(Query1, 'DELETE  FROM  SZFEJ WHERE SA_UJKOD = ' +
        ret_kod, true);
      Query_Run(Query1, 'DELETE  FROM  SZSOR WHERE SS_UJKOD = ' +
        ret_kod, true);
      ret_kod := '';
    end;
  end;
end;

function TSzamlaBeDlg.Elkuldes: boolean;
var
  ii: integer;
  szamlaszam: string;
  terkod: string;
  megnev: string;
  potlekmegnev1, potlekmegnev2, potlekmegnev3: string;
  terkod1, terkod2, terkod3: string;
  ossz, afa: double;
  uapotlek: double;
  tenypotlek: double;
  vnem: string;
  extrafej: string;
  vanpotlek: boolean;
  arfolyam, aarf, narf, egyar: double;
  resze, TeljDatum, S: string;
begin
  Result := False;

  if megtekint then begin
    Result := true;
    Exit;
  end;

  if MaskEdit1.Text = '' then begin
    NoticeKi('A megrendel� neve m�g �res!');
    // MaskEdit1.SetFocus;  // nem lehet "disabled" mez�re ugrani
    Exit;
  end;

  if MaskEdit2.Text = '' then begin
    NoticeKi('Az ir�ny�t�sz�m m�g �res!');
    MaskEdit2.SetFocus;
    Exit;
  end;

  if MaskEdit3.Text = '' then begin
    NoticeKi('A telep�l�s neve m�g �res!');
    MaskEdit3.SetFocus;
    Exit;
  end;

  if M2.Text = '' then begin
    NoticeKi('A k�zter�let m�g �res!');
    M2.SetFocus;
    Exit;
  end;

  if MaskEdit1.Text <> Query_Select('VEVO', 'V_KOD', vevokod, 'V_NEV') then
  begin
    NoticeKi('Vev�k�d - vev�n�v elt�r�s!');
    // MaskEdit1.SetFocus;   // nem lehet "disabled" mez�re ugrani
    Exit;
  end;

  if MaskEdit6.Text = '' then begin
    NoticeKi('A teljes�t�s d�tuma m�g nincs megadva!');
    MaskEdit6.SetFocus;
    Exit;
  end;
  // 2016.01.11. NP
  TeljDatum := MaskEdit6.Text;
  if JoDatum(TeljDatum) and (TeljDatum > EgyebDlg.MaiDatum) and
    not(EgyebDlg.MEGENGEDO_SZAMLA_ARFOLYAM_ELLENORZES) then begin
    NoticeKi('A sz�mla teljes�t�s d�tuma nem lehet j�v�beli! (Telj. d�tuma: ' +
      TeljDatum + ', mai d�tum: ' + EgyebDlg.MaiDatum + ')');
    try
      MaskEdit6.SetFocus;
    except
    end;
    Exit;
  end;

  if ((MaskEdit5.Text = '') and (MaskEdit18.Text = '')) then begin
    NoticeKi('Valamelyik ad�sz�m kit�lt�se k�telez�!');
    MaskEdit5.SetFocus;
    Exit;
  end;

  if (MaskEdit7.Text <> '') and (MaskEdit8.Text <> '') and
    (MaskEdit7.Text = MaskEdit8.Text) then begin
      // NoticeKi('A fizet�si hat�rid� nem egyezhet meg a sz�mla kelt�vel!');
      // MaskEdit8.SetFocus;
      // Exit;
      // **** ink�bb be�ll�tjuk *****
      Query_Log_Str('Fizetesi_hatarido_egyez�s!, SA_UJKOD='+ret_kod, 0, false, true);
      FizHatarido_beallit;
  end;

  if (MaskEdit7.Text <> '') and (StrToDate(MaskEdit7.Text) < date) then
    NoticeKi('FIGYELEM! A sz�mla kelte a mai nap el�tti!');

  (*
    // A poz�ci�sz�m ellen�rz�se
    if not Jopozicio(MaskEdit14.Text) then begin
    if NoticeKi('A poz�ci�sz�m val�sz�n�leg hib�s (hi�nyz� vessz�)! Folytatja?', NOT_QUESTION) <> 0 then begin
    try
    MaskEdit14.SetFocus;
    except
    end;
    Exit;
    end;
    end;
  *)
  // A valutanemek egyedis�g�nek vizsg�lata
  if SorGrid.Cells[2, 1] <> '' then begin
    ii := 1;
    vnem := '';
    while ii < SorGrid.RowCount do begin
      if vnem = '' then begin
        if SorGrid.Cells[4, ii] <> '' then begin
          vnem := SorGrid.Cells[4, ii];
        end;
      end
      else begin
        if ((SorGrid.Cells[4, ii] <> '') and (SorGrid.Cells[4, ii] <> vnem))
        then begin
          // T�BB VALUTANEM VAN A SZ�ML�N
          NoticeKi('T�bb k�l�nb�z� valutanem van a sz�ml�n : ' + vnem + '; ' +
            SorGrid.Cells[4, ii]);
          Exit;
        end;
      end;
      inc(ii);
    end;
  end;

  // Az �zemanyag p�tl�k ellen�rz�se
  Query_Run(Query3, 'SELECT * FROM VEVO WHERE V_KOD = ''' + vevokod +
    ''' ', true);
  if Query3.RecordCount > 0 then begin // van ilyen vev�
    uapotlek := GetUzemanyagPotlekSz(Query3.FieldByName('V_KOD').AsString,
      GetApehUzemanyag(MaskEdit6.Text), MaskEdit6.Text, true);
    if uapotlek <> 0 then begin // kell p�tl�k
      { megnev	:= '�zemanyag p�tl�k';
        terkod	:= EgyebDlg.Read_SZGrid('999',TERMEKEK[nyelvazon, 10]);
        if terkod <> '' then begin
        if Query_Run (Query4, 'SELECT * FROM TERMEK WHERE T_TERKOD = '''+terkod+''' ',true) then begin
        megnev	:= Query4.FieldByName('T_TERNEV').AsString;
        end;
        end; }
      // mindh�rom nyelv� �zemanyagp�tl�kra vizsg�ljuk, mert a sz�mla nyelve lehet m�s mint a SZSOR gener�l�s�n�l haszn�lt nyelv
      terkod1 := EgyebDlg.Read_SZGrid('999', TERMEKEK[1, 10]);
      terkod2 := EgyebDlg.Read_SZGrid('999', TERMEKEK[2, 10]);
      terkod3 := EgyebDlg.Read_SZGrid('999', TERMEKEK[3, 10]);
      if terkod1 <> '' then begin
        if Query_Run(Query4, 'SELECT * FROM TERMEK WHERE T_TERKOD = ''' +
          terkod1 + ''' ', true) then begin
          potlekmegnev1 := Query4.FieldByName('T_TERNEV').AsString;
        end;
      end;
      if terkod2 <> '' then begin
        if Query_Run(Query4, 'SELECT * FROM TERMEK WHERE T_TERKOD = ''' +
          terkod2 + ''' ', true) then begin
          potlekmegnev2 := Query4.FieldByName('T_TERNEV').AsString;
        end;
      end;
      if terkod3 <> '' then begin
        if Query_Run(Query4, 'SELECT * FROM TERMEK WHERE T_TERKOD = ''' +
          terkod3 + ''' ', true) then begin
          potlekmegnev3 := Query4.FieldByName('T_TERNEV').AsString;
        end;
      end;

      ossz := 0;
      tenypotlek := 0;
      vanpotlek := False;
      for ii := 0 to SorGrid.RowCount - 1 do begin
        if (Pos(potlekmegnev1, SorGrid.Cells[2, ii]) > 0) or
          (Pos(potlekmegnev2, SorGrid.Cells[2, ii]) > 0) or
          (Pos(potlekmegnev3, SorGrid.Cells[2, ii]) > 0) then begin
          // if SorGrid.Cells[2,ii] 	= megnev then begin
          tenypotlek := StringSzam(SorGrid.Cells[5, ii]);
          vanpotlek := true;
        end
        else begin
          ossz := ossz + StringSzam(SorGrid.Cells[5, ii]);
        end;
      end;
      if vanpotlek then begin
        // if ( Abs(tenypotlek - ( ossz * uapotlek / 100 )) * 1000 > 0  ) then begin
        if (Abs(RoundTo(tenypotlek, -2) - RoundTo((ossz * uapotlek / 100), -2))
          > 0) then begin
          // Elt�r�s a p�tl�kn�l!
          if NoticeKi
            ('Figyelem! Az �zemanyag p�tl�k �sszege nem egyezik a sz�zal�kos �rt�kkel! '
            + Format('( %f <> %f )', [tenypotlek, ossz * uapotlek / 100]) +
            ' Folytatja?', NOT_QUESTION) <> 0 then begin
            Exit;
          end;
        end;
      end
      else begin
        if NoticeKi('Figyelem! Hi�nyz� �zemanyag p�tl�k! Folytatja? ',
          NOT_QUESTION) <> 0 then begin
          // Beillesztj�k a p�tl�kot �s kil�p�nk �jra a szerkesztobe
          PotlekBeszuras(uapotlek, ossz * uapotlek / 100);
          Exit;
        end;
      end;
    end;
  end;
  /// ///////////////////////////////
  // Az �rfolyamok ellen�rz�se
  for ii := 1 to SorGrid.RowCount - 1 do begin
    aarf := StringSzam(SorGrid.Cells[7, ii]);
    vnem := SorGrid.Cells[4, ii];
    if (aarf > 0) and (vnem <> '') then begin
      // Az �rfolyam > 0
      narf := EgyebDlg.ArfolyamErtek(vnem, arfdat, true);

      if aarf <> narf then begin // aktu�lis, napi �rfolyam
        if NoticeKi('A megadott �rfolyam elt�r az aktu�lis napi �rfolyamt�l:' +
          Format('%.4f<>%.4f (%s)', [aarf, narf, arfdat]) + ' Folytatja?',
          NOT_QUESTION) <> 0 then begin
          SorGrid.Row := ii;
          Exit;
        end
        else begin
          Label23.Caption := FloatToStr(aarf);
          // Break;
        end;
      end;
    end;
  end;

  szamlaszam := '';
  resze := '0';
  if ret_kod = '' then begin
    { �j rekord felv�tele }
    // Query_Run(QFej, 'SELECT MAX(SA_UJKOD) MAXKOD FROM SZFEJ ', true);
    // ret_kod 	:= InttoStr(QFej.FieldByName('MAXKOD').AsInteger + 1);

    ret_kod := IntToStr(GetNewIDLoop(modeGetNextCode, 'SZFEJ', 'SA_UJKOD', ''));
    if (ret_kod='0') or (ret_kod='') then begin
       S:= 'Sikertelen k�d gener�l�s, k�rem pr�b�lja �jra! (SzamlaElkuldes)';
       HibaKiiro(S);
       NoticeKi(S);
       Exit;
       end;

    Query_Run(QFej, 'INSERT INTO SZFEJ (SA_UJKOD, SA_FLAG, SA_ALLST  ) ' +
      'VALUES (' + ret_kod + ', ''1'', ''' + GetSzamlaAllapot(1) +
      ''' ) ', true);
  end
  else begin
    Query_Run(QFej, 'SELECT SA_KOD,SA_RESZE FROM SZFEJ WHERE SA_UJKOD = ' +
      ret_kod, true);
    szamlaszam := QFej.FieldByName('SA_KOD').AsString;
    resze := QFej.FieldByName('SA_RESZE').AsString;
  end;
  /// //////////////////////
  if (resze = '0') and (not EgyebDlg.Szamla_szam_datum_ell(szamlaszam,
    MaskEdit7.Text, False)) then begin
    NoticeKi('L�tezik kisebb sz�mlasz�m nagyobb d�tummal!');
    Exit;
  end;
  /// /////////////////////
  if not CheckBox3.Checked then begin
    eurmegj1 := '';
    eurmegj2 := '';
    eurmegj3 := '';
  end;

  // Az extra vev�i sor meghat�roz�sa
  extrafej := Query_Select('VEVO', 'V_KOD', vevokod, 'VE_VEFEJ');

  if fej1 = '' then begin
    // CheckBox1.OnClick(self);
    // CheckBox3.OnClick(self);
    NyelvFrissit;
  end;

	Query_Update ( Query1, 'SZFEJ', [
		'SA_KOD',		''''+szamlaszam+'''',
		'SA_DATUM',		''''+EgyebDlg.MaiDatum+'''',
		'SA_TEDAT',		''''+MaskEdit6.Text+'''',
		'SA_KIDAT',		''''+MaskEdit7.Text+'''',
		'SA_ESDAT',		''''+MaskEdit8.Text+'''',
		'SA_VEVOKOD',	''''+vevokod+'''',
		'SA_VEVONEV',	''''+MaskEdit1.Text+'''',
		'SA_VEIRSZ',	''''+MaskEdit2.Text+'''',
		'SA_VEVAROS',	''''+MaskEdit3.Text+'''',
		'SA_VECIM',		''''+MaskEdit4.Text+'''',
       'SA_KERULET',	''''+M1.Text+'''',
       'SA_KOZTER',	''''+M2.Text+'''',
       'SA_JELLEG',	''''+M3.Text+'''',
       'SA_HAZSZAM',	''''+M4.Text+'''',
       'SA_EPULET',	''''+M5.Text+'''',
       'SA_LEPCSO',	''''+M6.Text+'''',
       'SA_SZINT',		''''+M7.Text+'''',
       'SA_AJTO',		''''+M8.Text+'''',
		'SA_VEADO',		''''+MaskEdit5.Text+'''',
		'SA_EUADO',		''''+MaskEdit18.Text+'''',
		'SA_ARFDAT', 	''''+MaskEdit19.Text+'''',
		'SA_VALARF',	SqlSzamString(StringSzam(Label23.Caption),12,2),
		'SA_VEKEDV',	'0',
		'SA_VEFEJ', 	''''+extrafej+'''',
		'SA_FIMOD',		''''+Combobox1.Text+'''',
		'SA_OSSZEG',	SqlSzamString(StringSzam(MaskEdit9.Text),12,2),
		'SA_AFA',		SqlSzamString(StringSzam(MaskEdit10.Text),12,2),
		'SA_MEGJ',		''''+copy(MaskEdit13.Text,1,80)+'''',
		'SA_JARAT',		''''+MaskEdit12.Text+'''',
		'SA_POZICIO',	''''+MaskEdit14.Text+'''',
		'SA_RSZ',		''''+MaskEdit15.Text+' '+GKKAT+'''',
		'SA_MEGRE',		''''+MaskEdit17.Text+'''',
		'SA_MELL1',		''''+mell1+'''',
		'SA_MELL2',		''''+mell2+'''',
		'SA_MELL3',		''''+mell3+'''',
		'SA_MELL4',		''''+mell4+'''',
		'SA_MELL5',		''''+mell5+'''',
		'SA_MELL6',		''''+mell6+'''',
		'SA_MELL7',		''''+mell7+'''',
		'SA_FEJL1',		''''+fej1+'''',
		'SA_FEJL2',		''''+fej2+'''',
		'SA_FEJL3',		''''+fej3+'''',
		'SA_FEJL4',		''''+fej4+'''',
		'SA_FEJL5',		''''+fej5+'''',
		'SA_FEJL6',		''''+fej6+'''',
		'SA_FEJL7',		''''+fej7+'''',
		'SA_FOKON',     ''''+MaskEdit16.Text+'''',
		'SA_HELYE',		BoolToStr01(Checkbox2.Checked),
		'SA_SAEUR',		BoolToStr01(Checkbox3.Checked),
    'SA_ORSZ2',     ''''+orszaglist[OrszagCombo.ItemIndex]+'''',
    // 'SA_SZLAVAL',		''''+szamlavaluta+'''',
		'SA_EGYA1',		''''+eurmegj1+'''',
		'SA_EGYA2',		''''+eurmegj2+'''',
		'SA_EGYA3',		''''+eurmegj3+'''',
    'SA_NYELV',		''''+nyelvlist[cbNyelv.ItemIndex]+'''',
		'SA_ATAD', 		'0',
		'SA_ATDAT', 	''''+''+'''',
		'SA_RESZE', 	resze,
		'SA_TIPUS',		'100',
		'SA_UZPOT',		SqlSzamString(tenypotlek,12,2)
	], ' WHERE SA_UJKOD = '+ret_kod);

  // Az orsz�g k�dj�nak be�r�sa
  FillOrszagkod(MaskEdit12.Text, EgyebDlg.MaiDatum);

  { SZSOR karbantart�sa }
  Query_Run(QSor, 'DELETE FROM SZSOR WHERE SS_UJKOD = ' + ret_kod, true);
  if SorGrid.RowCount > 1 then begin
    ossz := 0;
    afa := 0;
    vnem := '';
    /// //////////////////////////////////////////////////////////////
    arfolyam := StringSzam(SorGrid.Cells[20, ii]);
    // arfolyam:=  StringSzam(Label23.Caption)  ;
    Query_Run(QSor, 'SELECT * FROM VISZONY WHERE VI_UJKOD = ' + ret_kod);
    if QSor.RecordCount > 0 then begin
      vevokod := QSor.FieldByName('VI_VEKOD').AsString;
      arfdat := QSor.FieldByName('VI_BEDAT').AsString;
      if StrToIntDef(Query_Select('VEVO', 'V_KOD', vevokod, 'V_ARNAP'), 0) > 0
      then begin
        arfdat := QSor.FieldByName('VI_KIDAT').AsString;
      end;
      arfolyam := EgyebDlg.ArfolyamErtek('EUR', arfdat, true);
      // arfolyam:=  StringSzam(Label23.Caption)  ;
    end;
    /// /////////////////////////////////////////////////////////////
    for ii := 1 to SorGrid.RowCount - 1 do begin
      egyar := StringSzam(SorGrid.Cells[7, ii]);
			Query_Insert( Query1, 'SZSOR',
				[
				'SS_UJKOD',  	ret_kod,
				'SS_KOD',       ''''+szamlaszam+'''',
				'SS_SOR',       IntToStr(ii),
				'SS_TERKOD',    ''''+SorGrid.Cells[1,ii]+'''',
				'SS_TERNEV',    ''''+SorGrid.Cells[2,ii]+'''',
				'SS_ITJSZJ',	''''+SorGrid.Cells[3,ii]+'''',
				'SS_VALNEM',	''''+SorGrid.Cells[4,ii]+'''',
				'SS_VALERT',    SqlSzamString(StringSzam(SorGrid.Cells[5,ii]),12,2),
				'SS_AFA',       SqlSzamString(StringSzam(SorGrid.Cells[6,ii]),12,2),
				'SS_VALARF',    SqlSzamString(StringSzam(SorGrid.Cells[7,ii]),12,2),
				'SS_KEDV',		'0',
				'SS_EGYAR',     SqlSzamString(StringSzam(SorGrid.Cells[17,ii]),12,2),
				'SS_LEIRAS',    ''''+SorGrid.Cells[11,ii]+'''',
				'SS_AFAKOD',    ''''+SorGrid.Cells[12,ii]+'''',
				'SS_KULNEV',    ''''+SorGrid.Cells[13,ii]+'''',
				'SS_KULLEI',    ''''+SorGrid.Cells[14,ii]+'''',
				'SS_DARAB',     SqlSzamString(StringSzam(SorGrid.Cells[15,ii]),9,2),
				'SS_MEGY',      ''''+SorGrid.Cells[16,ii]+'''',
				'SS_KULME',     ''''+SorGrid.Cells[18,ii]+'''',
//				'SS_ARDAT',		''''+SorGrid.Cells[19,ii]+'''',
        'SS_ARDAT',		''''+arfdat+'''',    // NagyP 2015.02.16 - a d�tum is legyen konzisztens az �rfolyammal
//				'SS_ARFOL',     SqlSzamString(StringSzam(SorGrid.Cells[20,ii]),9,2),
				'SS_ARFOL',     SqlSzamString(arfolyam,9,2),
				'SS_NEMEU',     IntToStr(StrToIntDef(SorGrid.Cells[21,ii], 0))
				]);
      // if StringSzam(SorGrid.Cells[7,ii]) <>0 then
      // begin
      if SorGrid.Cells[4, ii] <> '' then
        vnem := SorGrid.Cells[4, ii];
      ossz := ossz + StringSzam(SorGrid.Cells[5, ii]) *
        StringSzam(SorGrid.Cells[15, ii]);
      afa := afa + StringSzam(SorGrid.Cells[5, ii]) *
        StringSzam(SorGrid.Cells[15, ii]) *
        StringSzam(SorGrid.Cells[6, ii]) / 100;
      // end;
    end;
  end;
  //
	Query_Update ( Query1, 'SZFEJ', [
		'SA_VALNEM',		''''+vnem+'''',
		'SA_VALOSS',	SqlSzamString(ossz,12,2),
		'SA_VALAFA',	SqlSzamString(afa,12,2)
	], ' WHERE SA_UJKOD = '+ret_kod);

  /// //////////////
  { Query_Update (Query1, 'VISZONY', [
    //		'VI_VALNEM',	''''+listavnem[CValuta.ItemIndex]+'''',
    //		'VI_KULF', 		BoolToStr01(RG2.ItemIndex = 1),
    //		'VI_ARFOLY',	SqlSzamString(StringSzam(M3.Text),12,4),
    'VI_FDEXP',		SqlSzamString(StringSzam(MaskEdit90.Text),12,4)
    //		'VI_FDIMP',		SqlSzamString(StringSzam(M5.Text),12,4),
    ], ' WHERE VI_UJKOD = '''+ret_kod+''' ' );
  }

  Result := true;
end;

procedure TSzamlaBeDlg.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  EgyebDlg.FormReturn(Key);
end;

procedure TSzamlaBeDlg.SorBeolvaso (SorBeolvasoMod: TSorBeolvasoMod);
var
  belafa: string;
  kulafa: string;
  megnev: string;
  leiras: string;
  terkod: string;
  uapotlek: double;
  afastr: string;
  afasz: double;
  afakod: integer;
  vtsz: string;
  telflag: integer;
  ossz: double;
  i: integer;
  arfolyam: double;
  S: string;
begin
  { Ki�r�tj�k a t�bl�zatot }
  SorGrid.RowCount := 2;
  SorGrid.Rows[1].Clear;
  sorszam := 0;

  { Beolvassuk a b�zissz�t�rb�l a nemzetk�zi fuvaroz�s sorokat }
  megnev := '';
  leiras := '';
  vevokod := Query1.FieldByName('VI_VEKOD').AsString;
  telflag := StrToIntDef(Query_Select('VEVO', 'V_KOD', vevokod, 'V_ARNAP'), 0);
  arfdat := Query1.FieldByName('VI_BEDAT').AsString;
  if telflag > 0 then begin
    arfdat := Query1.FieldByName('VI_KIDAT').AsString;
  end;
  // ----- az els� sor a sz�mla t�telek k�z�tt ----- //
  if SorBeolvasoMod = normal then begin // norm�l sz�mla
      if Query1.FieldByName('VI_KULF').AsInteger > 0 then begin
        { Nemzetk�zi fuvaroz�s }
        terkod := EgyebDlg.Read_SZGrid('999', TERMEKEK[nyelvazon, 1]);
      end
      else begin
        { Belf�ldi fuvaroz�s }
        terkod := EgyebDlg.Read_SZGrid('999', TERMEKEK[nyelvazon, 2]);
      end;
      if terkod <> '' then begin
        if Query_Run(Query4, 'SELECT * FROM TERMEK WHERE T_TERKOD = ''' + terkod +
          ''' ', true) then begin
          megnev := Query4.FieldByName('T_TERNEV').AsString;
          leiras := Query4.FieldByName('T_LEIRAS').AsString;
        end;
      end;
      if megnev <> '' then begin
        { Besz�rjuk a sz�veget az els� sorba }
        SorGrid.Cells[0, 1] := IntToStr(1);
        SorGrid.Cells[1, 1] := terkod;
        SorGrid.Cells[2, 1] := megnev;
        SorGrid.Cells[11, 1] := leiras;
        sorszam := 1;
      end;

      end;
  // else begin   // stand trailer sz�mla
  //     terkod := EgyebDlg.Read_SZGrid('999', TERMEKEK[nyelvazon, 11]);
  //     end;

  belafa := EgyebDlg.Read_SZGrid('999', '612');
  kulafa := EgyebDlg.Read_SZGrid('999', '613');

  { Csak a k�z�ti fuvaroz�shoz j�r a vev�i �FA }
  if Query1.FieldByName('VI_KULF').AsInteger > 0 then begin
    vevoafa := '';
  end;
  if SorBeolvasoMod = normal then begin   // norm�l sz�mla
      case Query1.FieldByName('VI_TIPUS').AsInteger of
        0: { Export fuvar } begin
            if Query1.FieldByName('VI_KULF').AsInteger > 0 then begin
              SorAdoUj(TERMEKEK[nyelvazon, 3],
                Query1.FieldByName('VI_FDIMP').AsFloat);
              SorAdoUj(TERMEKEK[nyelvazon, 4],
                Query1.FieldByName('VI_FDKOV').AsFloat);
            end
            else begin
              SorAdoUj(TERMEKEK[nyelvazon, 9],
                Query1.FieldByName('VI_FDEXP').AsFloat);
            end;
            MaskEdit13.Text := copy(Query1.FieldByName('VI_HONNAN').AsString + ' - '
              + Query1.FieldByName('VI_HOVA').AsString, 1, 80);
          end;
        1: { Import fuvar } begin
            if Query1.FieldByName('VI_KULF').AsInteger > 0 then begin
              SorAdoUj(TERMEKEK[nyelvazon, 5],
                Query1.FieldByName('VI_FDIMP').AsFloat);
              SorAdoUj(TERMEKEK[nyelvazon, 6],
                Query1.FieldByName('VI_FDKOV').AsFloat);
            end
            else begin
              SorAdoUj(TERMEKEK[nyelvazon, 9],
                Query1.FieldByName('VI_FDEXP').AsFloat);
            end;
            MaskEdit13.Text := copy(Query1.FieldByName('VI_HONNAN').AsString + ' - '
              + Query1.FieldByName('VI_HOVA').AsString, 1, 80);
          end;
        2: { K�rfuvar } begin
            if Query1.FieldByName('VI_KULF').AsInteger > 0 then begin
              SorAdoUj(TERMEKEK[nyelvazon, 7],
                Query1.FieldByName('VI_FDIMP').AsFloat);
              SorAdoUj(TERMEKEK[nyelvazon, 8],
                Query1.FieldByName('VI_FDKOV').AsFloat);
            end
            else begin
              SorAdoUj(TERMEKEK[nyelvazon, 9],
                Query1.FieldByName('VI_FDEXP').AsFloat);
            end;
            MaskEdit13.Text := copy(Query1.FieldByName('VI_HONNAN').AsString + ' - '
              + Query1.FieldByName('VI_HOVA').AsString + ' - ' +
              Query1.FieldByName('VI_HONNAN').AsString, 1, 80);
          end;
        end;  // case
      end // if
  else begin// stand trailer sz�mla
      SorAdoUj(TERMEKEK[nyelvazon, 11], Query1.FieldByName('VI_FDIMP').AsFloat); // ugyanaz a (teljes) �sszeg van VI_FDEXP-ben is
      // [NP 2018-05-07] Ez itt nem teljesen konform, szebb lett volna a VISZONY t�bl�ba is betenni az �ru le�r�s�t, �s itt onnan
      // kiolvasni. Nem szeretn�k ennyire m�lyen beleny�lni.
      S:= 'select isnull(MS_FELARU,'''') MS_FELARU from megbizas left join megseged on MS_MBKOD=MB_MBKOD '+
          ' where MB_VIKOD='+Query1.FieldByName('VI_VIKOD').AsString+' and MS_SORSZ=1 and MS_TIPUS=1';
      if Query_Run(Query4, S, true) then begin
        MaskEdit13.Text := copy(Query4.Fields[0].AsString, 1, 80);
        end;
      end;  // else

  // Az �zemanyag p�tl�k elsz�mol�s�hoz tartoz� szolg�ltat�s besz�r�sa
  if SorBeolvasoMod = normal then begin   // norm�l sz�mla
      uapotlek := GetUzemanyagPotlekSz(vevokod, GetApehUzemanyag(MaskEdit6.Text),
        MaskEdit6.Text, true);
      if uapotlek <> 0 then begin
        // Az osszeg meghat�roz�sa
        ossz := 0;
        for i := 0 to SorGrid.RowCount - 1 do begin
          ossz := ossz + StringSzam(SorGrid.Cells[5, i]);
          end; // for
        ossz := ossz * uapotlek / 100;
        PotlekBeszuras(uapotlek, ossz);
        end; // if
    end; // if

  // A vev�h�z kapcsol�d� egy�b d�jak beolvas�sa
  Query_Run(QueryDij, 'SELECT * FROM VEVODIJ WHERE VD_VEKOD = ''' + vevokod +
    ''' ORDER BY VD_SORSZ');
  while not QueryDij.EOF do begin
    ossz := StringSzam(QueryDij.FieldByName('VD_EGYAR').AsString);
    // A megfelel� sor besz�r�sa
    if sorszam > 0 then begin
      SorGrid.RowCount := SorGrid.RowCount + 1;
    end;
    megnev := GetSzoveg('47', 'Vev�i d�j', nyelvlist[cbNyelv.ItemIndex]);
    leiras := '';
    vtsz := '';
    terkod := QueryDij.FieldByName('VD_TEKOD').AsString;
    if terkod <> '' then begin
      if Query_Run(Query4, 'SELECT * FROM TERMEK WHERE T_TERKOD = ''' + terkod +
        ''' ', true) then begin
        megnev := Query4.FieldByName('T_TERNEV').AsString;
        leiras := Query4.FieldByName('T_LEIRAS').AsString;
        afastr := Query4.FieldByName('T_AFA').AsString;
        vtsz := Query4.FieldByName('T_ITJSZJ').AsString;
      end;
    end;
    SorGrid.Cells[2, SorGrid.RowCount - 1] := terkod;
    SorGrid.Cells[2, SorGrid.RowCount - 1] := megnev;
    SorGrid.Cells[3, SorGrid.RowCount - 1] := vtsz;
    SorGrid.Cells[0, SorGrid.RowCount - 1] := IntToStr(sorszam + 1);
    SorGrid.Cells[4, SorGrid.RowCount - 1] :=
      QueryDij.FieldByName('VD_VANEM').AsString;
    SorGrid.Cells[5, SorGrid.RowCount - 1] := Format('%12.2f', [ossz]);
    afakod := StrToIntDef(afastr, 0);
    if vevoafa <> '' then begin
      afakod := StrToIntDef(vevoafa, 0);
    end;
    case afakod of
      101:
        afasz := 25;
      102:
        afasz := 15;
      106:
        afasz := 27;
      110:
        afasz := 20;
    else
      afasz := 0;
    end;
    arfolyam := EgyebDlg.ArfolyamErtek(QueryDij.FieldByName('VD_VANEM')
      .AsString, arfdat, true);
    SorGrid.Cells[6, SorGrid.RowCount - 1] := Format('%6.2f', [afasz]);
    SorGrid.Cells[7, SorGrid.RowCount - 1] := SzamString(arfolyam, 12, 4);
    SorGrid.Cells[11, SorGrid.RowCount - 1] := leiras;
    SorGrid.Cells[12, SorGrid.RowCount - 1] := IntToStr(afakod);
    SorGrid.Cells[15, SorGrid.RowCount - 1] := '1';
    SorGrid.Cells[16, SorGrid.RowCount - 1] :=
      GetMegys(nyelvlist[cbNyelv.ItemIndex], 'db');
    SorGrid.Cells[17, SorGrid.RowCount - 1] :=
      Format('%12.2f', [ossz * arfolyam]);
    SorGrid.Cells[9, SorGrid.RowCount - 1] :=
      Format('%12.2f', [ossz * arfolyam]);
    SorGrid.Cells[19, SorGrid.RowCount - 1] := arfdat;
    if Query_Run(Query3,
      'SELECT AR_ERTEK FROM ARFOLYAM WHERE AR_VALNEM = ''EUR'' AND AR_DATUM = '''
      + arfdat + ''' ', true) then begin
      if Query3.RecordCount > 0 then begin
        SorGrid.Cells[20, SorGrid.RowCount - 1] :=
          Format('%12.4f', [Query3.FieldByName('AR_ERTEK').AsFloat]);
      end;
    end;
    SorGrid.Cells[21, SorGrid.RowCount - 1] := '0';
    inc(sorszam);
    QueryDij.Next;
  end;
  if sorszam > 0 then begin
    BitBtn2.Enabled := true;
    BitBtn3.Enabled := true;
  end;
end;

procedure TSzamlaBeDlg.BitBtn10Click(Sender: TObject);
begin
  EgyebDlg.Calendarbe(MaskEdit6);
end;

procedure TSzamlaBeDlg.BitBtn8Click(Sender: TObject);
begin
  EgyebDlg.Calendarbe(MaskEdit7);
  MaskEdit7.OnExit(MaskEdit7);
end;

procedure TSzamlaBeDlg.BitBtn9Click(Sender: TObject);
begin
  EgyebDlg.Calendarbe(MaskEdit8);
end;

procedure TSzamlaBeDlg.Tolt2(cim: string; jarat, viszony: string);
var
  i: integer;
  rekod: string;
  renev: string;
  telflag: integer;
  cmrdb: integer;
  MegbizasStandTrailer: boolean;
  mbkod, aru_leiras: string;
  // nyelv       : string;
begin
  { Bet�lt�nk egy j�rathoz tartoz� viszonyt }
  ret_kod := '';
  Caption := cim;
  viszonylat := viszony;
  MaskEdit1.Text := '';
  MaskEdit2.Text := '';
  MaskEdit3.Text := '';
  MaskEdit4.Text := '';
  MaskEdit5.Text := '';
  MaskEdit18.Text := '';
  MaskEdit6.Text := EgyebDlg.MaiDatum;
  // if not EgyebDlg.AUTOSZAMLA then
  // if (not EgyebDlg.AUTOSZAMLA)or(EgyebDlg.v_formatum<>SZAMLA_FORMA_JS) then
  if (not EgyebDlg.AUTOSZAMLA) or (kelldatum) then
    MaskEdit7.Text := EgyebDlg.MaiDatum;
  MaskEdit8.Text := DatumHoznap(MaskEdit7.Text, lejarat, true);
  MaskEdit9.Text := '';
  MaskEdit10.Text := '';
  MaskEdit11.Text := '';
  MaskEdit90.Text := '';
  MaskEdit100.Text := '';
  MaskEdit110.Text := '';
  MaskEdit12.Text := '';
  MaskEdit13.Text := '';
  MaskEdit14.Text := '';
  MaskEdit15.Text := '';
  for i := 1 to 18 do begin
    nemell[i] := '';
  end;
  { Megvan a megfelel� j�rat }
  if Query_Run(Query1, 'SELECT * FROM JARAT WHERE JA_KOD = ''' + jarat + ''' ',
    true) then begin
    MaskEdit15.Text := Query1.FieldByName('JA_RENDSZ').AsString;
    // Megkeress�k a j�rathoz tartoz� p�tkocsikat is
    if potkocsistr <> '' then begin
      MaskEdit15.Text := copy(Query1.FieldByName('JA_RENDSZ').AsString + ' ' +
        potkocsistr, 1, 40);
    end;
    MaskEdit12.Text := '';
    jaratkod := Query1.FieldByName('JA_KOD').AsString;
    if Query_Run(Query1, 'SELECT * FROM JARAT WHERE JA_KOD = ''' + jaratkod +
      ''' ', true) then begin
      MaskEdit12.Text := Format('%5.5d',
        [StrToIntDef(Query1.FieldByName('JA_ALKOD').AsString, 0)]);
    end;
    MaskEdit13.Text := Query1.FieldByName('JA_MEGJ').AsString;
    { Kiv�lasztjuk a j�rathoz kapcsol�d� megrendel�t, ha t�bb van }
    if Query_Run(Query1, 'SELECT * FROM VISZONY WHERE VI_VIKOD = ''' + viszony +
      ''' ', true) then begin
      rekod := Query1.FieldByName('VI_VEKOD').AsString;
      renev := Query1.FieldByName('VI_VENEV').AsString;
      vevokod := Query1.FieldByName('VI_VEKOD').AsString;
      // NagyP 2015.02.16. - az �rfolyam d�tum sz�m�t�shoz kell.
      MaskEdit14.Text := Query1.FieldByName('VI_POZIC').AsString;
      { A megrendel�k lekezel�se }
      if renev <> '' then begin
        MaskEdit1.Text := renev;
        { A megrendel� be�r�sa a fejl�cbe }
        if Query_Run(Query3, 'SELECT * FROM VEVO WHERE V_KOD = ''' + rekod +
          ''' ', true) then begin
          MaskEdit2.Text := Query3.FieldByName('V_IRSZ').AsString;
          MaskEdit3.Text := Query3.FieldByName('V_VAROS').AsString;
          MaskEdit4.Text := Query3.FieldByName('V_CIM').AsString;
          M1.Text := Query3.FieldByName('VE_KERULET').AsString;
          M2.Text := Query3.FieldByName('VE_KOZTER').AsString;
          M3.Text := Query3.FieldByName('VE_JELLEG').AsString;
          M4.Text := Query3.FieldByName('VE_HAZSZAM').AsString;
          M5.Text := Query3.FieldByName('VE_EPULET').AsString;
          M6.Text := Query3.FieldByName('VE_LEPCSO').AsString;
          M7.Text := Query3.FieldByName('VE_SZINT').AsString;
          M8.Text := Query3.FieldByName('VE_AJTO').AsString;
          MaskEdit5.Text := Query3.FieldByName('V_ADO').AsString;
          MaskEdit18.Text := Query3.FieldByName('VE_EUADO').AsString;
          ComboSet(OrszagCombo, Query3.FieldByName('V_ORSZ').AsString, orszaglist);  // 2018.08.07.
          { // A vev�h�z tartoz� bank bet�lt�se
            bakod  			:= Trim(Query3.FieldByName('VE_VBANK').AsString);
            fejuj13			:= EgyebDlg.Read_SZGrid('320', bakod);
            fejuj14			:= EgyebDlg.Read_SZGrid('320', IntToStr(StrToIntDef(bakod,0) + 1));
            fejuj15			:= EgyebDlg.Read_SZGrid('320', IntToStr(StrToIntDef(bakod,0) + 2));
            fejuj23			:= EgyebDlg.Read_SZGrid('320', IntToStr(StrToIntDef(bakod,0) + 4));
            fejuj24			:= EgyebDlg.Read_SZGrid('320', IntToStr(StrToIntDef(bakod,0) + 5));
            fejuj25			:= EgyebDlg.Read_SZGrid('320', IntToStr(StrToIntDef(bakod,0) + 6));
            fejuj26			:= EgyebDlg.Read_SZGrid('320', IntToStr(StrToIntDef(bakod,0) + 7));
          }
          // CheckBox1.Checked	:= false;
          // CheckBox1Click(self);

          // itt nem kell...k�s�bb h�vjuk ComboSet (cbNyelv, Query3.FieldByName('VE_NYELV').AsString, nyelvlist);
          vevoafa := Query3.FieldByName('V_AFAKO').AsString;
          {if StrToIntDef(Query3.FieldByName('V_LEJAR').AsString, 0) > 0 then
          begin
            lejarat := StrToIntDef(Query3.FieldByName('V_LEJAR').AsString, 0);
            MaskEdit8.Text := DatumHoznap(MaskEdit7.Text, lejarat, true);
            end;}
          FizHatarido_beallit;
        end;
        { A megrendel� �s a t�telek beolvas�sa }
        if Query_Run(Query1, 'SELECT * FROM VISZONY WHERE VI_VIKOD = ''' +
          viszony + ''' ', true) then begin
          // A vev� adatai alapj�n eld�nt�m, hogy a felrak�s vagy a lerak�s d�tuma kell-e?
          telflag := StrToIntDef(Query_Select('VEVO', 'V_KOD',
            Query1.FieldByName('VI_VEKOD').AsString, 'V_TENAP'), 0);

          // ----- 2017-08-31 M�r a VISZONY l�trej�ttekor eld�nt�tt�k a valutanemet
          szamlavaluta := Query1.FieldByName('VI_SZLAVAL').AsString;
          if szamlavaluta <> 'HUF' then begin
            // CheckBox3.Checked	:= ( QFej.FieldByName('SA_SAEUR').AsString = '1' );
            CheckBox3.Checked := true;
            CheckBox3.Enabled := true;
            imgValutaZarolva.Visible := False;
          end
          else begin // HUF: z�rolva a sz�mla devizaneme
            CheckBox3.Checked := False;
            CheckBox3.Enabled := False;
            imgValutaZarolva.Visible := true;
          end; // else
          // ----- 2017-08-31 M�r a VISZONY l�trej�ttekor eld�nt�tt�k a valutanemet
          // if Query3.FieldByName('V_NEMET').AsInteger > 0 then begin  // NP 2015-03-12, m�gis ez alapj�n lesz.
          // CheckBox3.Checked := true;
          // end
          // else begin
          // CheckBox3.Checked := false;
          // end;

          // A nyelv be�ll�t�sa
          /// felesleges m�g egyszer lek�rdezni
          // nyelv   := Query_Select('VEVO','V_KOD',Query1.FieldByName('VI_VEKOD').AsString,'VE_NYELV');
          // ComboSetSzoveg ( CBNyelv, nyelv );
          ComboSet(cbNyelv, Query3.FieldByName('VE_NYELV').AsString, nyelvlist);
          NyelvFrissit;
          // friss�teni kell, hogy a "Sorbeolvaso" m�r helyes "nyelvazon"-t haszn�ljon
          MaskEdit6.Text := Query1.FieldByName('VI_BEDAT').AsString;
          if telflag > 0 then begin
            MaskEdit6.Text := Query1.FieldByName('VI_KIDAT').AsString;
          end;
          // Az eur�s sz�mla seg�datatainak be�ll�t�sa
          eurmegj1 := 'EWC 191212 sonstige abf�lle';
          eurmegj2 := 'Aufladung: ' + Query1.FieldByName('VI_BEDAT').AsString;
          eurmegj3 := 'Ausladung: ' + Query1.FieldByName('VI_KIDAT').AsString;
          telflag := StrToIntDef(Query_Select('VEVO', 'V_KOD',
            Query1.FieldByName('VI_VEKOD').AsString, 'V_ARNAP'), 0);
          arfdat := Query1.FieldByName('VI_BEDAT').AsString;
          if telflag > 0 then begin
            arfdat := Query1.FieldByName('VI_KIDAT').AsString;
          end;
          mbkod:= Query_SelectString ('', 'select MB_MBKOD from megbizas where MB_VIKOD='+ Query1.FieldByName('VI_VIKOD').AsString);
          if (mbkod <> '') and JoEgesz(mbkod) then
            MegbizasStandTrailer:= EgyebDlg.IsMegbizasStandTailer(StringEgesz(mbkod), aru_leiras)
          else MegbizasStandTrailer:= False;
          Query1.First;
          if MegbizasStandTrailer then
            SorBeolvaso (standtrailer)
          else SorBeolvaso (normal);
          Query1.Close;
          { Az egy�b k�lts�gek beolvas�sa }
          if Query_Run(Query2, 'SELECT * FROM KOLTSEG WHERE KS_VIKOD = ''' +
            viszony + ''' ', true) then begin
            Query2.First;
            while not Query2.EOF do begin
              SorAdo2;
              Query2.Next;
            end;
            Query2.Close;
          end;
          Osszesit;
        end;
        // Bet�ltj�k a viszonyhoz kapcsol�d� CMR sz�mokat
        cmrszamok := '';
        cmrdb := 0;
        if Query_Run(Query2, 'SELECT CM_CSZAM FROM CMRDAT WHERE CM_VIKOD = ''' +
          viszony + ''' ORDER BY CM_CSZAM ', true) then begin
          Query2.First;
          while not Query2.EOF do begin
            cmrszamok := cmrszamok + Query2.FieldByName('CM_CSZAM').AsString + ',';
            inc(cmrdb);
            Query2.Next;
          end;
          if Length(cmrszamok) > 1 then begin
            cmrszamok := copy(cmrszamok, 1, Length(cmrszamok) - 1);
          end;
        end;
        if cmrdb > 0 then begin
          cmrszamok := IntToStr(cmrdb) + ' db cmr : ' + cmrszamok;
          if mell5 = EmptyStr then
            mell5 := mell5 + cmrszamok
          else if mell6 = EmptyStr then
            mell6 := mell6 + cmrszamok
          else if mell7 = EmptyStr then
            mell7 := mell7 + cmrszamok
        end;
      end;
    end;
  end;
  modosult := False;
  if megtekint then begin
    Tiltas;
  end;
end;

procedure TSzamlaBeDlg.SorAdo2;
var
  ossz: double;
begin
  { A k�lts�g besz�r�sa }
  if sorszam > 0 then begin
    SorGrid.RowCount := SorGrid.RowCount + 1;
  end;
  ossz := Query2.FieldByName('KS_ERTEK').AsFloat -
    Query2.FieldByName('KS_AFAERT').AsFloat;
  if Query2.FieldByName('KS_UZMENY').AsFloat > 0 then begin
    { Tankol�si k�lts�gr�l van sz� }
    SorGrid.Cells[2, SorGrid.RowCount - 1] :=
      GetSzoveg('46', '�zemanyag k�lts�g', nyelvlist[cbNyelv.ItemIndex]);
    if SorGrid.Cells[2, SorGrid.RowCount - 1] = '' then begin
      SorGrid.Cells[2, SorGrid.RowCount - 1] := '�zemanyag k�lts�g';
    end;
  end
  else begin
    { Egy�b k�lts�gr�l van sz� }
    SorGrid.Cells[2, SorGrid.RowCount - 1] :=
      Query2.FieldByName('KS_LEIRAS').AsString;
  end;
  SorGrid.Cells[0, SorGrid.RowCount - 1] := IntToStr(sorszam + 1);
  SorGrid.Cells[4, SorGrid.RowCount - 1] :=
    Query2.FieldByName('KS_VALNEM').AsString;
  SorGrid.Cells[5, SorGrid.RowCount - 1] :=
    Format('%12.2f', [Query2.FieldByName('KS_OSSZEG').AsFloat]);
  SorGrid.Cells[6, SorGrid.RowCount - 1] :=
    Query2.FieldByName('KS_AFASZ').AsString;
  SorGrid.Cells[7, SorGrid.RowCount - 1] :=
    Query2.FieldByName('KS_ARFOLY').AsString;
  SorGrid.Cells[9, SorGrid.RowCount - 1] := Format('%12.2f', [ossz]);
  SorGrid.Cells[12, SorGrid.RowCount - 1] :=
    Query2.FieldByName('KS_AFAKOD').AsString;
  SorGrid.Cells[15, SorGrid.RowCount - 1] := '1';
  SorGrid.Cells[16, SorGrid.RowCount - 1] :=
    GetMegys(nyelvlist[cbNyelv.ItemIndex], 'db');
  SorGrid.Cells[17, SorGrid.RowCount - 1] := Format('%12.2f', [ossz]);
  SorGrid.Cells[19, SorGrid.RowCount - 1] :=
    Query2.FieldByName('KS_DATUM').AsString;
  if Query_Run(Query3,
    'SELECT AR_ERTEK FROM ARFOLYAM WHERE AR_VALNEM = ''EUR'' ' +
    'AND AR_DATUM = ''' + Query2.FieldByName('KS_DATUM').AsString + ''' ', true)
  then begin
    if Query3.RecordCount > 0 then begin
      SorGrid.Cells[20, SorGrid.RowCount - 1] :=
        Format('%12.4f', [Query3.FieldByName('AR_ERTEK').AsFloat]);
    end;
  end;
  SorGrid.Cells[21, SorGrid.RowCount - 1] := '0';
  inc(sorszam);
end;

procedure TSzamlaBeDlg.SorAdoUj(szoveg: string; ossz: double);
{ Ez a fv az �FA sz�veget a szolg�ltat�sb�l veszi }
var
  megnev: string;
  leiras: string;
  terkod: string;
  afastr: string;
  afasz: double;
  afakod: integer;
  vtsz: string;
  telflag: integer;
begin
  if sorszam > 0 then begin
    SorGrid.RowCount := SorGrid.RowCount + 1;
  end;
  megnev := '';
  leiras := '';
  vtsz := '';
  terkod := EgyebDlg.Read_SZGrid('999', szoveg);
  if terkod <> '' then begin
    if Query_Run(Query4, 'SELECT * FROM TERMEK WHERE T_TERKOD = ''' + terkod +
      ''' ', true) then begin
      megnev := Query4.FieldByName('T_TERNEV').AsString;
      leiras := Query4.FieldByName('T_LEIRAS').AsString;
      afastr := Query4.FieldByName('T_AFA').AsString;
      vtsz := Query4.FieldByName('T_ITJSZJ').AsString;
    end;
  end;
  SorGrid.Cells[1, SorGrid.RowCount - 1] := terkod;
  SorGrid.Cells[2, SorGrid.RowCount - 1] := megnev;
  SorGrid.Cells[3, SorGrid.RowCount - 1] := vtsz;
  SorGrid.Cells[0, SorGrid.RowCount - 1] := IntToStr(sorszam + 1);
  SorGrid.Cells[4, SorGrid.RowCount - 1] :=
    Query1.FieldByName('VI_VALNEM').AsString;
  SorGrid.Cells[5, SorGrid.RowCount - 1] := Format('%12.2f', [ossz]);
  afakod := StrToIntDef(afastr, 0);
  if vevoafa <> '' then begin
    afakod := StrToIntDef(vevoafa, 0);
  end;
  case afakod of
    101:
      afasz := 25;
    102:
      afasz := 15;
    106:
      afasz := 27;
    110:
      afasz := 20;
  else
    afasz := 0;
  end;
  SorGrid.Cells[6, SorGrid.RowCount - 1] := Format('%6.2f', [afasz]);
  SorGrid.Cells[7, SorGrid.RowCount - 1] :=
    Query1.FieldByName('VI_ARFOLY').AsString;
  SorGrid.Cells[11, SorGrid.RowCount - 1] := leiras;
  SorGrid.Cells[12, SorGrid.RowCount - 1] := IntToStr(afakod);
  SorGrid.Cells[15, SorGrid.RowCount - 1] := '1';
  SorGrid.Cells[16, SorGrid.RowCount - 1] :=
    GetMegys(nyelvlist[cbNyelv.ItemIndex], 'db');
  SorGrid.Cells[17, SorGrid.RowCount - 1] :=
    Format('%12.2f', [ossz * Query1.FieldByName('VI_ARFOLY').AsFloat]);
  SorGrid.Cells[9, SorGrid.RowCount - 1] :=
    Format('%12.2f', [ossz * Query1.FieldByName('VI_ARFOLY').AsFloat]);
  telflag := StrToIntDef(Query_Select('VEVO', 'V_KOD', vevokod, 'V_ARNAP'), 0);
  arfdat := Query1.FieldByName('VI_BEDAT').AsString;
  if telflag > 0 then begin
    arfdat := Query1.FieldByName('VI_KIDAT').AsString;
  end;
  SorGrid.Cells[19, SorGrid.RowCount - 1] := arfdat;
  if Query_Run(Query3,
    'SELECT AR_ERTEK FROM ARFOLYAM WHERE AR_VALNEM = ''EUR'' ' +
    'AND AR_DATUM = ''' + arfdat + ''' ', true) then begin
    if Query3.RecordCount > 0 then begin
      SorGrid.Cells[20, SorGrid.RowCount - 1] :=
        Format('%12.4f', [Query3.FieldByName('AR_ERTEK').AsFloat]);
    end;
  end;
  SorGrid.Cells[21, SorGrid.RowCount - 1] := '0';
  inc(sorszam);
end;

procedure TSzamlaBeDlg.BitBtn6Click(Sender: TObject);
begin
  { Fejl�c beolvas�sa }
  Application.CreateForm(TMellekForm, MellekForm);
  MellekForm.Tolt('A fejl�c adatai', fej1, fej2, fej3, fej4, fej5,
    fej6, fej7, 7);
  MellekForm.ShowModal;
  if not MellekForm.Kilepes then begin
    fej1 := MellekForm.mell1;
    fej2 := MellekForm.mell2;
    fej3 := MellekForm.mell3;
    fej4 := MellekForm.mell4;
    fej5 := MellekForm.mell5;
    fej6 := MellekForm.mell6;
    fej7 := MellekForm.mell7;
  end;
  MellekForm.Destroy;
end;

procedure TSzamlaBeDlg.CheckBox3Click(Sender: TObject);
begin
  // R�gebben: a checkbox nincs enged�lyezve, a cbNyelv vez�rli!
  // 2015-02-19: enged�lyezett, de nem kell  NyelvFrissit-et h�vni
  // NyelvFrissit;
end;

procedure TSzamlaBeDlg.CheckBox2Click(Sender: TObject);
begin
  if CheckBox2.Checked then begin
    // Helyesb�t� sz�mla -> a sz�mla gomb kell
    BitSzamla.Show;
  end
  else begin
    // Nem helyesb�t� sz�mla -> a sz�mla gomb NEM kell
    BitSzamla.Hide;
  end;
end;

procedure TSzamlaBeDlg.BitSzamlaClick(Sender: TObject);
var
  KimSzamlaDlg2: TSzamlaFormDlg;
  szamlakod: string;
  str: string;
  ii: integer;
begin
  if SorGrid.Cells[2, 1] <> '' then begin
    if NoticeKi('A t�telek t�bl�zata nem �res. Folytatja?', NOT_QUESTION) <> 0
    then begin
      Exit;
    end;
  end;
  // A helyesb�tend� sz�mla kiv�laszt�sa
  Application.CreateForm(TSzamlaFormDlg, KimSzamlaDlg2);
  KimSzamlaDlg2.valaszt := true;
  KimSzamlaDlg2.ShowModal;
  szamlakod := '';
  if KimSzamlaDlg2.ret_vkod <> '' then begin
    szamlakod := KimSzamlaDlg2.ret_vkod;
  end;
  KimSzamlaDlg2.Destroy;
  if szamlakod = '' then begin
    // Kil�p�ssel j�tt�nk ki
    Exit;
  end;
  // A sz�mla fejl�c�nek beolvas�sa
  Query_Run(QFej, 'SELECT * FROM SZFEJ WHERE SA_UJKOD = ' + szamlakod, true);
  if QFej.RecordCount > 0 then begin
    MaskEdit1.Text := QFej.FieldByName('SA_VEVONEV').AsString;
    MaskEdit2.Text := QFej.FieldByName('SA_VEIRSZ').AsString;
    MaskEdit3.Text := QFej.FieldByName('SA_VEVAROS').AsString;
    MaskEdit4.Text := QFej.FieldByName('SA_VECIM').AsString;
    M1.Text := QFej.FieldByName('SA_KERULET').AsString;
    M2.Text := QFej.FieldByName('SA_KOZTER').AsString;
    M3.Text := QFej.FieldByName('SA_JELLEG').AsString;
    M4.Text := QFej.FieldByName('SA_HAZSZAM').AsString;
    M5.Text := QFej.FieldByName('SA_EPULET').AsString;
    M6.Text := QFej.FieldByName('SA_LEPCSO').AsString;
    M7.Text := QFej.FieldByName('SA_SZINT').AsString;
    M8.Text := QFej.FieldByName('SA_AJTO').AsString;
    MaskEdit5.Text := QFej.FieldByName('SA_VEADO').AsString;
    MaskEdit18.Text := QFej.FieldByName('SA_EUADO').AsString;
    ComboBox1.Text := QFej.FieldByName('SA_FIMOD').AsString;
    MaskEdit6.Text := QFej.FieldByName('SA_TEDAT').AsString;
    // A d�tum �t�ll�t�sa
    // if not EgyebDlg.AUTOSZAMLA then
    // if (not EgyebDlg.AUTOSZAMLA)or(EgyebDlg.v_formatum<>SZAMLA_FORMA_JS) then
    if (not EgyebDlg.AUTOSZAMLA) or (kelldatum) then
      MaskEdit7.Text := EgyebDlg.MaiDatum;

    {str := Query_Select('VEVO', 'V_KOD', vevokod, 'V_LEJAR');
    if str <> '' then begin
      lejarat := StrToIntDef(str, 0);
      end;
    if lejarat=0 then lejarat := Round(StringSzam(EgyebDlg.Read_SZGrid('999', '602')));
    MaskEdit8.Text := DatumHoznap(MaskEdit7.Text, lejarat, true);
    }
    FizHatarido_beallit;

    MaskEdit12.Text := QFej.FieldByName('SA_JARAT').AsString;
    MaskEdit13.Text := QFej.FieldByName('SA_MEGJ').AsString;
    MaskEdit14.Text := QFej.FieldByName('SA_POZICIO').AsString;
    MaskEdit15.Text := QFej.FieldByName('SA_RSZ').AsString;

    mell1 := QFej.FieldByName('SA_MELL1').AsString;
    mell2 := QFej.FieldByName('SA_MELL2').AsString;
    mell3 := QFej.FieldByName('SA_MELL3').AsString;
    mell4 := QFej.FieldByName('SA_MELL4').AsString;
    mell5 := QFej.FieldByName('SA_MELL5').AsString;
    mell6 := QFej.FieldByName('SA_MELL6').AsString;
    mell7 := QFej.FieldByName('SA_MELL7').AsString;

    if QFej.FieldByName('SA_FEJL1').AsString <> '' then begin
      { Van a fejl�cben valami, t�lts�k be }
      fej1 := QFej.FieldByName('SA_FEJL1').AsString;
      fej2 := QFej.FieldByName('SA_FEJL2').AsString;
      fej3 := QFej.FieldByName('SA_FEJL3').AsString;
      fej4 := QFej.FieldByName('SA_FEJL4').AsString;
      fej5 := QFej.FieldByName('SA_FEJL5').AsString;
      fej6 := QFej.FieldByName('SA_FEJL6').AsString;
      fej7 := QFej.FieldByName('SA_FEJL7').AsString;
    end;
  end;

  // Ki�r�tj�k a t�bl�zatot �s beolvassuk a kiv�lasztott sz�ml�t +-- -al
  sorszam := 2;
  SorGrid.RowCount := 3;
  SorGrid.Rows[1].Clear;
  BitBtn5.Enabled := False;
  BitBtn6.Enabled := False;

  SorGrid.Cells[0, 1] := '1';
  for ii := 1 to GRIDOSZLOP - 1 do begin
    SorGrid.Cells[ii, 1] := '';
  end;
  SorGrid.Cells[15, 1] := '0';
  SorGrid.Cells[2, 1] := 'Helyesb�t� sz�mla a ' + QFej.FieldByName('SA_KOD')
    .AsString + ' sz�ml�hoz';
  SorGrid.Cells[0, 2] := '2';
  for ii := 1 to GRIDOSZLOP - 1 do begin
    SorGrid.Cells[ii, 2] := '';
  end;
  SorGrid.Cells[15, 2] := '0';

  // A sz�mlasorok beolvas�sa
  Query_Run(QSor, 'SELECT * FROM SZSOR WHERE SS_UJKOD = ' + szamlakod +
    ' order by SS_SOR', true);
  if QSor.RecordCount > 0 then begin
    // A sorok felvitele pozit�v el�jellel
    QSor.First;
    while not QSor.EOF do begin
      SorGrid.RowCount := SorGrid.RowCount + 1;
      SorGrid.Cells[0, SorGrid.RowCount - 1] := IntToStr(sorszam + 1);
      SorGrid.Cells[1, SorGrid.RowCount - 1] :=
        QSor.FieldByName('SS_TERKOD').AsString;
      SorGrid.Cells[2, SorGrid.RowCount - 1] :=
        QSor.FieldByName('SS_TERNEV').AsString;
      SorGrid.Cells[3, SorGrid.RowCount - 1] :=
        QSor.FieldByName('SS_ITJSZJ').AsString;
      SorGrid.Cells[4, SorGrid.RowCount - 1] :=
        QSor.FieldByName('SS_VALNEM').AsString;
      SorGrid.Cells[5, SorGrid.RowCount - 1] :=
        SzamString(QSor.FieldByName('SS_VALERT').AsFloat, 12, 2);
      SorGrid.Cells[6, SorGrid.RowCount - 1] :=
        EgyebDlg.Read_SZGrid('101', QSor.FieldByName('SS_AFAKOD').AsString);
      SorGrid.Cells[7, SorGrid.RowCount - 1] :=
        SzamString(QSor.FieldByName('SS_VALARF').AsFloat, 12, 4);
      SorGrid.Cells[8, SorGrid.RowCount - 1] := '';
      SorGrid.Cells[9, SorGrid.RowCount - 1] :=
        SzamString(QSor.FieldByName('SS_EGYAR').AsFloat, 12, 0);
      SorGrid.Cells[10, SorGrid.RowCount - 1] :=
        QSor.FieldByName('SS_TERKOD').AsString;
      SorGrid.Cells[11, SorGrid.RowCount - 1] :=
        QSor.FieldByName('SS_LEIRAS').AsString;
      SorGrid.Cells[12, SorGrid.RowCount - 1] :=
        QSor.FieldByName('SS_AFAKOD').AsString;
      SorGrid.Cells[13, SorGrid.RowCount - 1] :=
        QSor.FieldByName('SS_KULNEV').AsString;
      SorGrid.Cells[14, SorGrid.RowCount - 1] :=
        QSor.FieldByName('SS_KULLEI').AsString;
      SorGrid.Cells[15, SorGrid.RowCount - 1] :=
        QSor.FieldByName('SS_DARAB').AsString;
      SorGrid.Cells[16, SorGrid.RowCount - 1] :=
        QSor.FieldByName('SS_MEGY').AsString;
      SorGrid.Cells[17, SorGrid.RowCount - 1] :=
        SzamString(QSor.FieldByName('SS_EGYAR').AsFloat, 12, 0);
      SorGrid.Cells[18, SorGrid.RowCount - 1] :=
        QSor.FieldByName('SS_KULME').AsString;
      SorGrid.Cells[19, SorGrid.RowCount - 1] :=
        QSor.FieldByName('SS_ARDAT').AsString;
      SorGrid.Cells[20, SorGrid.RowCount - 1] :=
        QSor.FieldByName('SS_ARFOL').AsString;
      SorGrid.Cells[21, SorGrid.RowCount - 1] :=
        QSor.FieldByName('SS_NEMEU').AsString;
      inc(sorszam);
      QSor.Next;
    end;

    // A sorok felvitele negat�v el�jellel
    QSor.First;
    while not QSor.EOF do begin
      SorGrid.RowCount := SorGrid.RowCount + 1;
      SorGrid.Cells[0, SorGrid.RowCount - 1] := IntToStr(sorszam + 1);
      SorGrid.Cells[1, SorGrid.RowCount - 1] :=
        QSor.FieldByName('SS_TERKOD').AsString;
      SorGrid.Cells[2, SorGrid.RowCount - 1] :=
        QSor.FieldByName('SS_TERNEV').AsString;
      SorGrid.Cells[3, SorGrid.RowCount - 1] :=
        QSor.FieldByName('SS_ITJSZJ').AsString;
      SorGrid.Cells[4, SorGrid.RowCount - 1] :=
        QSor.FieldByName('SS_VALNEM').AsString;
      SorGrid.Cells[5, SorGrid.RowCount - 1] :=
        SzamString(-1 * QSor.FieldByName('SS_VALERT').AsFloat, 12, 2);
      SorGrid.Cells[6, SorGrid.RowCount - 1] :=
        EgyebDlg.Read_SZGrid('101', QSor.FieldByName('SS_AFAKOD').AsString);
      SorGrid.Cells[7, SorGrid.RowCount - 1] :=
        SzamString(QSor.FieldByName('SS_VALARF').AsFloat, 12, 4);
      SorGrid.Cells[8, SorGrid.RowCount - 1] := '';
      SorGrid.Cells[9, SorGrid.RowCount - 1] :=
        SzamString(-1 * QSor.FieldByName('SS_EGYAR').AsFloat, 12, 0);
      SorGrid.Cells[10, SorGrid.RowCount - 1] :=
        QSor.FieldByName('SS_TERKOD').AsString;
      SorGrid.Cells[11, SorGrid.RowCount - 1] :=
        QSor.FieldByName('SS_LEIRAS').AsString;
      SorGrid.Cells[12, SorGrid.RowCount - 1] :=
        QSor.FieldByName('SS_AFAKOD').AsString;
      SorGrid.Cells[13, SorGrid.RowCount - 1] :=
        QSor.FieldByName('SS_KULNEV').AsString;
      SorGrid.Cells[14, SorGrid.RowCount - 1] :=
        QSor.FieldByName('SS_KULLEI').AsString;
      SorGrid.Cells[15, SorGrid.RowCount - 1] :=
        QSor.FieldByName('SS_DARAB').AsString;
      SorGrid.Cells[16, SorGrid.RowCount - 1] :=
        QSor.FieldByName('SS_MEGY').AsString;
      SorGrid.Cells[17, SorGrid.RowCount - 1] :=
        SzamString(-1 * QSor.FieldByName('SS_EGYAR').AsFloat, 12, 0);
      SorGrid.Cells[18, SorGrid.RowCount - 1] :=
        QSor.FieldByName('SS_KULME').AsString;
      SorGrid.Cells[19, SorGrid.RowCount - 1] :=
        QSor.FieldByName('SS_ARDAT').AsString;
      SorGrid.Cells[20, SorGrid.RowCount - 1] :=
        QSor.FieldByName('SS_ARFOL').AsString;
      SorGrid.Cells[21, SorGrid.RowCount - 1] :=
        QSor.FieldByName('SS_NEMEU').AsString;
      inc(sorszam);
      QSor.Next;
    end;

    // Egy �res sor besz�r�sa
    SorGrid.RowCount := SorGrid.RowCount + 1;
    SorGrid.Cells[0, sorszam + 1] := IntToStr(sorszam + 1);
    for ii := 1 to GRIDOSZLOP - 1 do begin
      SorGrid.Cells[ii, sorszam + 1] := '';
    end;
    SorGrid.Cells[15, sorszam + 1] := '0';
    inc(sorszam);

    // �jra felvissz�k a sorokat negat�v el�jellel
    QSor.First;
    while not QSor.EOF do begin
      SorGrid.RowCount := SorGrid.RowCount + 1;
      SorGrid.Cells[0, SorGrid.RowCount - 1] := IntToStr(sorszam + 1);
      SorGrid.Cells[1, SorGrid.RowCount - 1] :=
        QSor.FieldByName('SS_TERKOD').AsString;
      SorGrid.Cells[2, SorGrid.RowCount - 1] :=
        QSor.FieldByName('SS_TERNEV').AsString;
      SorGrid.Cells[3, SorGrid.RowCount - 1] :=
        QSor.FieldByName('SS_ITJSZJ').AsString;
      SorGrid.Cells[4, SorGrid.RowCount - 1] :=
        QSor.FieldByName('SS_VALNEM').AsString;
      SorGrid.Cells[5, SorGrid.RowCount - 1] :=
        SzamString(-1 * QSor.FieldByName('SS_VALERT').AsFloat, 12, 2);
      SorGrid.Cells[6, SorGrid.RowCount - 1] :=
        EgyebDlg.Read_SZGrid('101', QSor.FieldByName('SS_AFAKOD').AsString);
      SorGrid.Cells[7, SorGrid.RowCount - 1] :=
        SzamString(QSor.FieldByName('SS_VALARF').AsFloat, 12, 4);
      SorGrid.Cells[8, SorGrid.RowCount - 1] := '';
      SorGrid.Cells[9, SorGrid.RowCount - 1] :=
        SzamString(-1 * QSor.FieldByName('SS_EGYAR').AsFloat, 12, 0);
      SorGrid.Cells[10, SorGrid.RowCount - 1] :=
        QSor.FieldByName('SS_TERKOD').AsString;
      SorGrid.Cells[11, SorGrid.RowCount - 1] :=
        QSor.FieldByName('SS_LEIRAS').AsString;
      SorGrid.Cells[12, SorGrid.RowCount - 1] :=
        QSor.FieldByName('SS_AFAKOD').AsString;
      SorGrid.Cells[13, SorGrid.RowCount - 1] :=
        QSor.FieldByName('SS_KULNEV').AsString;
      SorGrid.Cells[14, SorGrid.RowCount - 1] :=
        QSor.FieldByName('SS_KULLEI').AsString;
      SorGrid.Cells[15, SorGrid.RowCount - 1] :=
        QSor.FieldByName('SS_DARAB').AsString;
      SorGrid.Cells[16, SorGrid.RowCount - 1] :=
        QSor.FieldByName('SS_MEGY').AsString;
      SorGrid.Cells[17, SorGrid.RowCount - 1] :=
        SzamString(-1 * QSor.FieldByName('SS_EGYAR').AsFloat, 12, 0);
      SorGrid.Cells[18, SorGrid.RowCount - 1] :=
        QSor.FieldByName('SS_KULME').AsString;
      SorGrid.Cells[19, SorGrid.RowCount - 1] :=
        QSor.FieldByName('SS_ARDAT').AsString;
      SorGrid.Cells[20, SorGrid.RowCount - 1] :=
        QSor.FieldByName('SS_ARFOL').AsString;
      SorGrid.Cells[21, SorGrid.RowCount - 1] :=
        QSor.FieldByName('SS_NEMEU').AsString;
      inc(sorszam);
      QSor.Next;
    end;

    if sorszam > 0 then begin
      BitBtn5.Enabled := true;
      BitBtn6.Enabled := true;
    end;
  end;
  Osszesit;
end;

procedure TSzamlaBeDlg.FormShow(Sender: TObject);
var
  ck: boolean;
begin
  // CheckBox1Click(Sender);
  ck := CheckBox3.Checked;
  NyelvFrissit;
  CheckBox3.Checked := ck;
  CheckBox2Click(Sender);
  // CheckBox3Click(Sender);
end;

procedure TSzamlaBeDlg.BitBtn32Click(Sender: TObject);
begin
  MaskEdit16.Text := '';
end;

procedure TSzamlaBeDlg.BitBtn7Click(Sender: TObject);
var
  foki: string;
begin
  foki := FokonyvValaszto(MaskEdit16.Text);
  if foki <> '' then begin
    MaskEdit16.Text := foki;
  end;
end;

procedure TSzamlaBeDlg.BitBtn11Click(Sender: TObject);
begin
  Application.CreateForm(TMellekForm, MellekForm);
  MellekForm.Tolt('Eur�s sz�mla egy�b adatai', eurmegj1, eurmegj2, eurmegj3, '',
    '', '', '', 3);
  MellekForm.ShowModal;
  if not MellekForm.Kilepes then begin
    eurmegj1 := MellekForm.mell1;
    eurmegj2 := MellekForm.mell2;
    eurmegj3 := MellekForm.mell3;
  end;
  MellekForm.Destroy;
end;

procedure TSzamlaBeDlg.SpeedButton3Click(Sender: TObject);
var
  ii: integer;
  j: integer;
begin
  // A kiv�lasztott elemet beteszi a lista elej�re
  if SorGrid.Cells[0, 1] <> '' then begin
    if SorGrid.Row > 1 then begin
      sorstr.Clear;
      for ii := 0 to GRIDOSZLOP - 1 do begin
        sorstr.Add(SorGrid.Cells[ii, SorGrid.Row]);
      end;
      for j := SorGrid.Row downto 2 do begin
        for ii := 1 to GRIDOSZLOP - 1 do begin
          SorGrid.Cells[ii, j] := SorGrid.Cells[ii, j - 1];
        end;
      end;
      for ii := 1 to GRIDOSZLOP - 1 do begin
        SorGrid.Cells[ii, 1] := sorstr[ii];
      end;
      SorGrid.Row := 1;
    end;
  end;
end;

procedure TSzamlaBeDlg.SpeedButton4Click(Sender: TObject);
var
  ii: integer;
  str: string;
begin
  // A kiv�lasztott elemet beteszi a lista elej�re
  if SorGrid.Cells[0, 1] <> '' then begin
    if SorGrid.Row > 1 then begin
      for ii := 1 to GRIDOSZLOP - 1 do begin
        str := SorGrid.Cells[ii, SorGrid.Row];
        SorGrid.Cells[ii, SorGrid.Row] := SorGrid.Cells[ii, SorGrid.Row - 1];
        SorGrid.Cells[ii, SorGrid.Row - 1] := str;
      end;
      SorGrid.Row := SorGrid.Row - 1;
    end;
  end;
end;

procedure TSzamlaBeDlg.SpeedButton5Click(Sender: TObject);
var
  ii: integer;
  str: string;
begin
  // A kiv�lasztott elemet beteszi a lista elej�re
  if SorGrid.Cells[0, 1] <> '' then begin
    if SorGrid.Row < SorGrid.RowCount - 1 then begin
      for ii := 1 to GRIDOSZLOP - 1 do begin
        str := SorGrid.Cells[ii, SorGrid.Row];
        SorGrid.Cells[ii, SorGrid.Row] := SorGrid.Cells[ii, SorGrid.Row + 1];
        SorGrid.Cells[ii, SorGrid.Row + 1] := str;
      end;
      SorGrid.Row := SorGrid.Row + 1;
    end;
  end;
end;

procedure TSzamlaBeDlg.SpeedButton6Click(Sender: TObject);
var
  ii: integer;
  j: integer;
begin
  // A kiv�lasztott elemet beteszi a lista elej�re
  if SorGrid.Cells[0, 1] <> '' then begin
    if SorGrid.Row < SorGrid.RowCount - 1 then begin
      sorstr.Clear;
      for ii := 0 to GRIDOSZLOP - 1 do begin
        sorstr.Add(SorGrid.Cells[ii, SorGrid.Row]);
      end;
      for j := SorGrid.Row to SorGrid.RowCount - 2 do begin
        for ii := 1 to GRIDOSZLOP - 1 do begin
          SorGrid.Cells[ii, j] := SorGrid.Cells[ii, j + 1];
        end;
      end;
      for ii := 1 to GRIDOSZLOP - 1 do begin
        SorGrid.Cells[ii, SorGrid.RowCount - 1] := sorstr[ii];
      end;
      SorGrid.Row := SorGrid.RowCount - 1;
    end;
  end;
end;

procedure TSzamlaBeDlg.BitBtn12Click(Sender: TObject);
var
  vkod: string;
begin
  // Beolvassuk a vev�t
  if MaskEdit1.Text = '' then begin
    NoticeKi('M�g nincs vev� kiv�lasztva!');
    Exit;
  end;
  vkod := vevokod;
  if vevokod = '' then begin
    vkod := Query_Select('VEVO', 'V_NEV', MaskEdit1.Text, 'V_KOD');
  end;
  if vkod <> '' then begin
    Application.CreateForm(TVevobeDlg, VevobeDlg);
    VevobeDlg.Tolto('Vev�i adatok ellen�rz�se', vkod);
    VevobeDlg.BitElkuld.Hide;
    VevobeDlg.ShowModal;
    VevobeDlg.Destroy;
  end;
end;

procedure TSzamlaBeDlg.Tiltas;
begin
  GroupBox1.Enabled := False;
  GroupBox2.Enabled := False;
  GroupBox3.Enabled := False;
  ButtonKuld.Enabled := False;
  SpeedButton3.Hide;
  SpeedButton4.Hide;
  SpeedButton5.Hide;
  SpeedButton6.Hide;
  SorGrid.Width := BitBtn1.Left - 10;
  BitBtn1.Enabled := False;
  BitBtn2.Enabled := False;
  BitBtn3.Enabled := False;
  BitSzamla.Enabled := False;
end;

procedure TSzamlaBeDlg.PotlekBeszuras(uapotleksz, ossz: double);
var
  megnev: string;
  leiras: string;
  terkod: string;
  afastr: string;
  afasz: double;
  afakod: integer;
  vtsz: string;
begin
  // A megfelel� sor besz�r�sa
  Query_Run(Query1, 'SELECT * FROM VISZONY WHERE VI_VIKOD = ''' + viszonylat +
    ''' ', true);
  if Query1.RecordCount = 0 then begin
    Exit;
  end;
  if sorszam > 0 then begin
    SorGrid.RowCount := SorGrid.RowCount + 1;
  end;
  // megnev	:= '�zemanyag p�tl�k ('+Format('%.1f',[uapotleksz])+'%)';
  megnev := GetSzoveg('45', '�zemanyag p�tl�k :', nyelvlist[cbNyelv.ItemIndex])
    + ' (' + Format('%.2f', [uapotleksz]) + '%)';
  leiras := '';
  vtsz := '';
  terkod := EgyebDlg.Read_SZGrid('999', TERMEKEK[nyelvazon, 10]);
  if terkod <> '' then begin
    if Query_Run(Query4, 'SELECT * FROM TERMEK WHERE T_TERKOD = ''' + terkod +
      ''' ', true) then begin
      // megnev	:= Query4.FieldByName('T_TERNEV').AsString+' ('+Format('%.1f',[uapotleksz])+'%)';
      megnev := Query4.FieldByName('T_TERNEV').AsString + ' (' +
        Format('%.2f', [uapotleksz]) + '%)';
      leiras := Query4.FieldByName('T_LEIRAS').AsString;
      afastr := Query4.FieldByName('T_AFA').AsString;
      vtsz := Query4.FieldByName('T_ITJSZJ').AsString;
    end;
  end;
  SorGrid.Cells[1, SorGrid.RowCount - 1] := terkod;
  SorGrid.Cells[2, SorGrid.RowCount - 1] := megnev;
  SorGrid.Cells[3, SorGrid.RowCount - 1] := vtsz;
  SorGrid.Cells[0, SorGrid.RowCount - 1] := IntToStr(sorszam + 1);
  SorGrid.Cells[4, SorGrid.RowCount - 1] :=
    Query1.FieldByName('VI_VALNEM').AsString;
  SorGrid.Cells[5, SorGrid.RowCount - 1] := Format('%12.2f', [ossz]);
  afakod := StrToIntDef(afastr, 0);
  if vevoafa <> '' then begin
    afakod := StrToIntDef(vevoafa, 0);
  end;
  case afakod of
    101:
      afasz := 25;
    102:
      afasz := 15;
    106:
      afasz := 27;
    110:
      afasz := 20;
  else
    afasz := 0;
  end;
  SorGrid.Cells[6, SorGrid.RowCount - 1] := Format('%6.2f', [afasz]);
  SorGrid.Cells[7, SorGrid.RowCount - 1] :=
    Query1.FieldByName('VI_ARFOLY').AsString;
  SorGrid.Cells[11, SorGrid.RowCount - 1] := leiras;
  SorGrid.Cells[12, SorGrid.RowCount - 1] := IntToStr(afakod);
  SorGrid.Cells[15, SorGrid.RowCount - 1] := '1';
  SorGrid.Cells[16, SorGrid.RowCount - 1] :=
    GetMegys(nyelvlist[cbNyelv.ItemIndex], 'db');
  SorGrid.Cells[17, SorGrid.RowCount - 1] :=
    Format('%12.2f', [ossz * Query1.FieldByName('VI_ARFOLY').AsFloat]);
  SorGrid.Cells[9, SorGrid.RowCount - 1] :=
    Format('%12.2f', [ossz * Query1.FieldByName('VI_ARFOLY').AsFloat]);
  SorGrid.Cells[19, SorGrid.RowCount - 1] := arfdat;
  if Query_Run(Query3,
    'SELECT AR_ERTEK FROM ARFOLYAM WHERE AR_VALNEM = ''EUR'' AND AR_DATUM = '''
    + arfdat + ''' ', true) then begin
    if Query3.RecordCount > 0 then begin
      SorGrid.Cells[20, SorGrid.RowCount - 1] :=
        Format('%12.4f', [Query3.FieldByName('AR_ERTEK').AsFloat]);
    end;
  end;
  SorGrid.Cells[21, SorGrid.RowCount - 1] := '0';
  inc(sorszam);
end;

procedure TSzamlaBeDlg.MaskEdit8Exit(Sender: TObject);
begin
  DatumExit(Sender, False);
end;

procedure TSzamlaBeDlg.MaskEdit19Exit(Sender: TObject);
begin
  DatumExit(Sender, False);
end;

procedure TSzamlaBeDlg.MaskEdit4Change(Sender: TObject);
begin
  modosult := true;
  M2.Text := MaskEdit4.Text;
end;

procedure TSzamlaBeDlg.BitBtn13Click(Sender: TObject);
begin
  EgyebDlg.Calendarbe(MaskEdit19);
end;

end.
