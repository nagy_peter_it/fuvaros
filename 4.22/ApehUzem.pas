unit ApehUzem;

interface

uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, DBTables, DB, StdCtrls, Buttons, Mask, DBCtrls, ExtCtrls,
  QuickRpt;

type
  TApehUzemDlg = class(TForm)
    BitBtn4: TBitBtn;
    BitBtn6: TBitBtn;
    Label3: TLabel;
    M4: TMaskEdit;
    BitBtn1: TBitBtn;
    Label1: TLabel;
    MaskEdit1: TMaskEdit;
    Label2: TLabel;
    MaskEdit2: TMaskEdit;
    BitBtn2: TBitBtn;
    BitBtn3: TBitBtn;
    Label4: TLabel;
    ComboBox1: TComboBox;
    BitBtn32: TBitBtn;
    M40: TMaskEdit;
    QRCompositeReport1: TQRCompositeReport;
    procedure FormCreate(Sender: TObject);
    procedure BitBtn4Click(Sender: TObject);
    procedure BitBtn6Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure MaskEdit1Exit(Sender: TObject);
    procedure MaskEdit2Exit(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure BitBtn32Click(Sender: TObject);
    procedure QRCompositeReport1AddReports(Sender: TObject);
  private
  		soforok 	: string;
  public
  end;

var
  ApehUzemDlg: TApehUzemDlg;

implementation

uses
	DolgozoUjFm, ApehUzlis, ApehUzlisOssz, Egyeb, Kozos;
{$R *.DFM}

procedure TApehUzemDlg.FormCreate(Sender: TObject);
begin
	M4.Text := '';
   BitBtn32Click(self);
end;

procedure TApehUzemDlg.BitBtn4Click(Sender: TObject);
begin
(*
	if MaskEdit1.Text <> '' then begin
		MaskEdit1.Text := copy(MaskEdit1.Text, 1, 8) + '01.';
	end;
*)
	if not Jodatum2(MaskEdit1) then begin
		NoticeKi('Rossz d�tum megad�sa!');
		MaskEdit1.SetFocus;
		Exit;
	end;
	if not Jodatum2(MaskEdit2) then begin
		NoticeKi('Rossz d�tum megad�sa!');
		MaskEdit2.SetFocus;
		Exit;
	end;

//  	EgyebDlg.ElsoNapEllenor(MaskEdit1, MaskEdit2);
	Screen.Cursor	:= crHourGlass;
	Application.CreateForm(TApehUzlisDlg, ApehUzlisDlg);
	ApehUzlisDlg.Tolt(soforok, MaskEdit1.Text, MaskEdit2.Text );
	Screen.Cursor	:= crDefault;
	if ApehUzlisDlg.vanadat	then begin
		Application.CreateForm(TApehUzlisOsszDlg, ApehUzlisOsszDlg);
		ApehUzlisOsszDlg.Tolt(ApehUzlisDlg.SG, MaskEdit1.Text +' - '+ MaskEdit2.Text);
		QRCompositeReport1.Reports.Clear;
		QRCompositeReport1.Preview;
		ApehUzlisOsszDlg.Destroy;
	end else begin
		NoticeKi('Nincs megfelel� adat!');
	end;
  	ApehUzlisDlg.Destroy;
end;

procedure TApehUzemDlg.BitBtn6Click(Sender: TObject);
begin
	Close;
end;

procedure TApehUzemDlg.BitBtn1Click(Sender: TObject);
var
	i : integer;
begin
	// A sof�r�k kiv�laszt�sa
	Application.CreateForm(TDolgozoUjFmDlg, DolgozoUjFmDlg);
	DolgozoUjFmDlg.valaszt		:= true;
	DolgozoUjFmDlg.multiselect	:= true;
	DolgozoUjFmDlg.ShowModal;
   soforok						:= '';
	if DolgozoUjFmDlg.selectlist.Count > 0 then begin
       M4.Text		:= '';
       M40.Text	:= '';
		for i := 0 to DolgozoUjFmDlg.selectlist.Count - 1 do begin
			M4.Text		:= M4.Text  + DolgozoUjFmDlg.SelectNevlist[i]+',';
			M40.Text	:= M40.Text + DolgozoUjFmDlg.Selectlist[i]+',';
			soforok		:= soforok 	+ ''''+DolgozoUjFmDlg.Selectlist[i]+''''+',';
       end;
       soforok			:= soforok + '''zzzz''';
	end;
	DolgozoUjFmDlg.Destroy;
end;

procedure TApehUzemDlg.MaskEdit1Exit(Sender: TObject);
begin
	if ( ( MaskEdit1.Text <> '' ) and ( not Jodatum2(MaskEdit1 ) ) ) then begin
		NoticeKi('Hib�s d�tum megad�sa!');
		MaskEdit1.Text := '';
		MaskEdit1.Setfocus;
	end;
	MaskEdit1.Text := copy(MaskEdit1.Text,1,8)+'01.';
	EgyebDlg.ElsoNapEllenor(MaskEdit1, MaskEdit2);
end;

procedure TApehUzemDlg.MaskEdit2Exit(Sender: TObject);
begin
	if ( ( MaskEdit2.Text <> '' ) and ( not Jodatum2(MaskEdit2 ) ) ) then begin
		NoticeKi('Hib�s d�tum megad�sa!');
		MaskEdit2.Text := '';
		MaskEdit2.Setfocus;
	end;
	// Be�ll�tjuk a h�nap utols� napj�t
	MaskEdit2.Text := GetLastDay(MaskEdit2.Text);
end;

procedure TApehUzemDlg.BitBtn2Click(Sender: TObject);
begin
	// EgyebDlg.Calendarbe(MaskEdit1);
	// MaskEdit1.Text := copy(MaskEdit1.Text,1,8)+'01.';
  // EgyebDlg.ElsoNapEllenor(MaskEdit1, MaskEdit2);
  EgyebDlg.CalendarBe_idoszak(MaskEdit1, MaskEdit2);

end;

procedure TApehUzemDlg.BitBtn3Click(Sender: TObject);
begin
	EgyebDlg.Calendarbe(MaskEdit2);
	// Be�ll�tjuk a h�nap utols� napj�t
	MaskEdit2.Text := GetLastDay(MaskEdit2.Text);
end;

procedure TApehUzemDlg.BitBtn32Click(Sender: TObject);
begin
	// Ki�r�tj�k a sof�r�ket
	M4.Text		:= '';
  	M40.Text	:= '';
   soforok		:= '';
end;

procedure TApehUzemDlg.QRCompositeReport1AddReports(Sender: TObject);
begin
	with QRCompositeReport1.Reports do begin
  		Add(ApehUzlisDlg.Rep);
       Add(ApehUzlisOsszDlg.Report);
   end;
end;

end.


