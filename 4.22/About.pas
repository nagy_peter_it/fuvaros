unit About;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, Buttons, ComCtrls;

type
  TAboutDlg = class(TForm)
    RichEdit1: TRichEdit;
    BitElkuld: TBitBtn;
    Image1: TImage;
    procedure FormCreate(Sender: TObject);
    procedure BitElkuldClick(Sender: TObject);
  private
  public
  end;

var
  AboutDlg: TAboutDlg;

implementation

uses
	Egyeb;
{$R *.dfm}

procedure TAboutDlg.FormCreate(Sender: TObject);
var
	fn : string;
begin
	fn := ExtractFilePath(Application.ExeName)+'NYITOKEP.jpg';
 	if FileExists(fn) then begin
		Image1.Picture.LoadFromFile(fn);
  end;
  with RichEdit1 do begin
  	Clear;
     Lines.Add('Program :');
     Lines.Add('');
     Lines.Add('  DM_D7 - Sz�m�t�g�pes Fuvaroz�si Rendszer');
     Lines.Add('');
     Lines.Add('');
     Lines.Add('Verzi� :');
     Lines.Add('    '+SW_VERZIO);
     Lines.Add('');
     Lines.Add('K�sz�tette :');
     Lines.Add('   DMK COMP Kft.');
     Lines.Add('   2800. Tatab�nya, T�lgyfa u. 1.');
     Lines.Add('   Tel.: 06-30-6941088');
  end;
end;

procedure TAboutDlg.BitElkuldClick(Sender: TObject);
begin
	Close;
end;

end.
