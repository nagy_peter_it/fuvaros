unit Telepgener;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, Contnrs, DB, ADODB, Mask, ExtCtrls;

type

  	TTelepGenerDlg = class(TForm)
    Memo1: TMemo;
    QueryDat1: TADOQuery;
    QueryDat2: TADOQuery;
    QueryKoz2: TADOQuery;
    QueryKoz1: TADOQuery;
    Panel1: TPanel;
    Label2: TLabel;
    Label1: TLabel;
    BitKilep: TBitBtn;
    BitElkuld: TBitBtn;
    MaskEdit1: TMaskEdit;
    Label3: TLabel;
    MaskEdit2: TMaskEdit;
    BitBtn2: TBitBtn;
    Label4: TLabel;
    BitBtn1: TBitBtn;
    Label5: TLabel;
    M4: TMaskEdit;
    M40: TMaskEdit;
    BitBtn3: TBitBtn;
    BitBtn32: TBitBtn;
    procedure FormCreate(Sender: TObject);
    procedure BitKilepClick(Sender: TObject);
    procedure BitElkuldClick(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure MaskEdit2Exit(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure BitBtn32Click(Sender: TObject);
  private
  		rendszamok	: string;
  public

  end;


var
  TelepGenerDlg: TTelepGenerDlg;

implementation

uses
	Egyeb, J_SQL, Kozos, J_Valaszto, GepkocsiUjFm;

{$R *.dfm}

procedure TTelepGenerDlg.FormCreate(Sender: TObject);
begin
  	EgyebDlg.SetADOQueryDatabase(QueryDat1);
  	EgyebDlg.SetADOQueryDatabase(QueryDat2);
  	EgyebDlg.SetADOQueryDatabase(QueryKoz1);
  	EgyebDlg.SetADOQueryDatabase(QueryKoz2);
   rendszamok      := '';
end;


procedure TTelepGenerDlg.BitKilepClick(Sender: TObject);
begin
	Close;
end;

procedure TTelepGenerDlg.BitElkuldClick(Sender: TObject);
var
	kezdat		: string;
   vegdat		: string;
   osszkm		: double;
   jaratkm		: double;
   sorsz		: integer;
   osszli		: double;
   arany		: double;
   tenyli		: double;
   maradek		: double;
   ujkodszam	: integer;
   lastkod		: integer;
   kmora		: integer;
begin
	if rendszamok = '' then begin
		NoticeKi('Hi�nyz� rendsz�mok!');
		MaskEdit2.Setfocus;
       Exit;
   end;
	if not Jodatum2(MaskEdit2 ) then begin
		NoticeKi('Hib�s d�tum megad�sa!');
		MaskEdit2.Text := '';
		MaskEdit2.Setfocus;
       Exit;
   end;
   osszli	:= StringSzam(MaskEdit1.Text);
	if osszli < 1 then begin
		NoticeKi('Hib�s liter megad�sa!');
		MaskEdit1.Text := '';
		MaskEdit1.Setfocus;
       Exit;
   end;
	Memo1.Clear;
	// A gener�l�s elkezd�se
   Memo1.Lines.Add('Telepi tankol�sok gener�l�sa '+copy(MaskEdit2.Text, 1, 8)+ ' h�napra : ');
   kezdat	:= copy(MaskEdit2.Text, 1, 8)+'01.';
   vegdat	:= copy(MaskEdit2.Text, 1, 8)+'32.';
   Query_Run(QueryDat1, 'SELECT * FROM JARAT WHERE JA_JKEZD >= '''+kezdat+
   	''' AND JA_JKEZD <= '''+vegdat+''' '+
       ' AND JA_RENDSZ IN ('+rendszamok+')'+
       ' ORDER BY JA_ALKOD  ');
   if QueryDat1.RecordCount < 1 then begin
   	Memo1.Lines.Add('');
		Memo1.Lines.Add('Nincs megfelel� elem!');
       Exit;
   end;
   Query_Run(QueryDat2, 'SELECT SUM(JA_ZAROKM - JA_NYITKM) OSSZEG FROM JARAT WHERE JA_JKEZD >= '''+kezdat+
   	''' AND JA_JKEZD <= '''+vegdat+''' '+
       ' AND JA_RENDSZ IN ('+rendszamok+')'
       );
   osszkm	:= StringSzam(QueryDat2.FieldByName('OSSZEG').AsString);
   Memo1.Lines.Add('Az �sszes j�ratkilom�ter : '+Format('%.0f', [osszkm]));
   if osszkm < 1 then begin
   	osszkm	:= 1;
   end;
   Memo1.Lines.Add('');
   Memo1.Lines.Add('Az �rintett j�ratok : ');
   Memo1.Lines.Add('--------------------- ');
   sorsz		:= 1;
   tenyli		:= 0;
   maradek		:= 0;
   lastkod		:= 0;
   ujkodszam	:= GetNextCode('KOLTSEG', 'KS_KTKOD', 1, 0);
   while not QueryDat1.Eof do begin
   	jaratkm := StrToIntDef(QueryDat1.FieldByName('JA_ZAROKM').AsString,0)- StrToIntDef(QueryDat1.FieldByName('JA_NYITKM').AsString,0);
       arany	:= jaratkm * 100 / osszkm;
		if ( arany * osszli / 100 ) > 10 then begin
       	// A t�z litern�l nagyobbakat kiadjuk
           Memo1.Lines.Add(Format('%5d.',[sorsz])+' J�rat : '+copy(QueryDat1.FieldByName('JA_ORSZ').AsString + '-' + Format('%5.5d',[QueryDat1.FieldByName('JA_ALKOD').AsInteger])+'               ', 1, 11)+
               ' Km : '+ Format('%8.0f -> %8.3f %%  Telepi tankol�s : %8.2f liter', [jaratkm, arany, maradek + arany * osszli / 100]));
			tenyli 	:= tenyli + StringSzam(Format ('%8.2f', [maradek + arany * osszli / 100]) );
           kmora	:= StrToIntDef(QueryDat1.FieldByName('JA_NYITKM').AsString, 0);
           if QueryDat1.FieldByName('JA_ZAROKM').AsString >= QueryDat1.FieldByName('JA_NYITKM').AsString then begin
           	Inc(kmora);
           end;
           Query_Insert (QueryDat2, 'KOLTSEG',
               [
               'KS_KTKOD',		IntToStr(ujkodszam),
               'KS_JAKOD', 	''''+QueryDat1.FieldByName('JA_KOD').AsString+'''',
               'KS_DATUM', 	''''+QueryDat1.FieldByName('JA_JKEZD').AsString +'''',
               'KS_RENDSZ', 	''''+QueryDat1.FieldByName('JA_RENDSZ').AsString +'''',
               'KS_KMORA', 	IntToStr(kmora),
               'KS_UZMENY', 	SqlSzamString(maradek + arany * osszli / 100,12,2),
               'KS_LEIRAS', 	''''+ '' +'''',
               'KS_VALNEM', 	''''+'HUF'+'''',
               'KS_ARFOLY',	'1',
               'KS_OSSZEG',	'0',
               'KS_ERTEK',		'0',
               'KS_AFASZ',		'0',
               'KS_AFAERT',	'0',
               'KS_AFAKOD',	''''+'103'+'''',
               'KS_FIMOD',		''''+'KP'+'''',
               'KS_ATLAG',		'0',
               'KS_THELY',     ''''+''+'''',
               'KS_TELE',		'0',
               'KS_TELES',		''''+' N '+'''',
				'KS_TETAN',		'1',
               'KS_TIPUS', 	'0',
               'KS_MEGJ',		''''+'Telepi tankol�s...'+''''
           ]);
           lastkod	:= ujkodszam;
			Inc(ujkodszam);
           Inc(sorsz);
           maradek	:= 0;
       end else begin
       	maradek	:= maradek + ( arany * osszli / 100 );
       end;
   	QueryDat1.Next;
   end;
   Memo1.Lines.Add('');
   Memo1.Lines.Add('Az �sszes telepi tankol�s : '+Format('%.2f', [tenyli]));
	if osszli <> tenyli then begin
		Query_Update (QueryDat2, 'KOLTSEG',
           [
              'KS_UZMENY', 'KS_UZMENY + ' + SqlSzamString(osszli - tenyli ,12,2)
           ], ' WHERE KS_KTKOD = '+IntToStr(lastkod) );
   	Memo1.Lines.Add('');
   	Memo1.Lines.Add('A korrekci�s t�tel : '+Format('%.2f', [osszli - tenyli]));
   end;
  	Memo1.Lines.Add('');
	Memo1.Lines.Add('A m�velet befejez�d�tt!');
end;

procedure TTelepGenerDlg.BitBtn2Click(Sender: TObject);
begin
	EgyebDlg.Calendarbe(MaskEdit2);
end;

procedure TTelepGenerDlg.MaskEdit2Exit(Sender: TObject);
begin
	if ( ( MaskEdit2.Text <> '' ) and ( not Jodatum2(MaskEdit2 ) ) ) then begin
		NoticeKi('Hib�s d�tum megad�sa!');
		MaskEdit2.Text := '';
		MaskEdit2.Setfocus;
	end;
end;

procedure TTelepGenerDlg.BitBtn1Click(Sender: TObject);
var
	kezdat		: string;
   vegdat		: string;
begin
	if not Jodatum2(MaskEdit2 ) then begin
		NoticeKi('Hib�s d�tum megad�sa!');
		MaskEdit2.Text := '';
		MaskEdit2.Setfocus;
       Exit;
   end;
   Memo1.Lines.Add('Telepi tankol�sok t�rl�se '+copy(MaskEdit2.Text, 1, 8)+ ' h�napra : ');
   kezdat	:= copy(MaskEdit2.Text, 1, 8)+'01.';
   vegdat	:= copy(MaskEdit2.Text, 1, 8)+'32.';
   Query_Run(QueryDat1, 'SELECT * FROM KOLTSEG WHERE KS_DATUM >= '''+kezdat+
   	''' AND KS_DATUM <= '''+vegdat+''' '+
       ' AND KS_MEGJ = ''Telepi tankol�s...'' ' );
   if QueryDat1.RecordCount < 1 then begin
   	Memo1.Lines.Add('');
		Memo1.Lines.Add('Nincs megfelel� elem!');
       Exit;
   end;
   if NoticeKi(IntToStr(QueryDat1.RecordCount)+' db elemet tal�ltunk. Folytatja a t�rl�st?', NOT_QUESTION) = 0 then begin
       Query_Run(QueryDat1, 'DELETE FROM KOLTSEG WHERE KS_DATUM >= '''+kezdat+
           ''' AND KS_DATUM <= '''+vegdat+''' '+
           ' AND KS_MEGJ = ''Telepi tankol�s...'' ' );
   end;
  	Memo1.Lines.Add('');
	Memo1.Lines.Add('A t�rl�si m�velet befejez�d�tt!');
end;

procedure TTelepGenerDlg.BitBtn3Click(Sender: TObject);
begin
	// T�bb g�pkocsi kiv�laszt�sa
	TobbGepkocsiValaszto( M4, M40, rendszamok) ;
end;

procedure TTelepGenerDlg.BitBtn32Click(Sender: TObject);
begin
	// Ki�r�tj�k a rendsz�mokat
	M4.Text		:= '';
  	M40.Text	:= '';
   rendszamok	:= '';
end;

end.
