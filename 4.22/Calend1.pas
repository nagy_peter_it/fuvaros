unit Calend1;

interface

uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, Grids, StdCtrls, Spin, Buttons, ComCtrls, ExtCtrls,
  DateUtils;

type
  TCalendarDlg = class(TForm)
    Panel1: TPanel;
    SpeedButton1: TSpeedButton;
    SpinEdit1: TSpinEdit;
    HoBox: TComboBox;
    SpeedButton2: TSpeedButton;
    StatusBar1: TStatusBar;
    SpeedButton3: TSpeedButton;
    SpeedButton4: TSpeedButton;
	 Label1: TLabel;
	 Panel2: TPanel;
	 HoGrid: TStringGrid;
	 SpeedButton5: TSpeedButton;
	 procedure FormCreate(Sender: TObject);
	 procedure Honapiro;
   function HetNapja(Ev, Het, HanyadikNap: integer): string;
	 procedure HoBoxChange(Sender: TObject);
	 procedure SpinEdit1Change(Sender: TObject);
	 procedure SpeedButton2Click(Sender: TObject);
	 procedure SpeedButton1Click(Sender: TObject);
	 procedure HoGridDblClick(Sender: TObject);
	 procedure Tolt(datstr	: string);
	 procedure HoGridDrawCell(Sender: TObject; Col, Row: Longint;
	   Rect: TRect; State: TGridDrawState);
	 procedure HoGridClick(Sender: TObject);
	 procedure SpeedButton3Click(Sender: TObject);
	 procedure SpeedButton4Click(Sender: TObject);
	 procedure 	DisplayHint2(Sender: TObject);
	 function Getnevnap(honr, nanr : integer) : string;
	 procedure HoGridMouseMove(Sender: TObject; Shift: TShiftState; X,
	   Y: Integer);
	 procedure FormDestroy(Sender: TObject);
	 procedure SpeedButton5Click(Sender: TObject);
	 procedure FormKeyDown(Sender: TObject; var Key: Word;
	   Shift: TShiftState);
	 procedure SGHoClick(Sender: TObject);
  private
	 evszam		: integer;
	 hoszam		: integer;
	 napszam   	: integer;
	 kiemelt	: integer;
	 hogombok	: array[1..12] of TBitBtn;
  public
	 ret_date: string;
	 ret_date2: string;
  end;

var
  CalendarDlg: TCalendarDlg;

implementation
{$R *.DFM}

uses
	Kozos;

type
	napszam	= array[1..12] of integer;
   nevnap	= record
   	h	: integer;
       n	: integer;
       v	: string;
   end;
	nevnaplista	= array[1..365] of nevnap;
   honapnev	= array[1..12] of string;

const
	napok 	 : napszam  	= (31,28,31,30,31,30,31,31,30,31,30,31);
   honapok	 : honapnev     = ( 'janu�r', 'febru�r', 'm�rcius', '�prilis', 'm�jus', 'j�nius', 'j�lius', 'augusztus', 'szeptember', 'okt�ber', 'november', 'december');
   nevnapok : nevnaplista 	= (
            (h:1; n:1; v:'Fruzsina'),(h:1; n:2; v:'�bel'),(h:1; n:3; v:'Benj�min, Genov�va'),(h:1; n:4; v:'Le�na, Titusz'),(h:1; n:5; v:'Simon'),(h:1; n:6; v:'Boldizs�r'),(h:1; n:7; v:'Attila, Ram�na'),(h:1; n:8; v:'Gy�ngyv�r'),(h:1; n:9; v:'Marcell'),(h:1; n:10; v:'Mel�nia'),
            (h:1; n:11; v:'�gota'),(h:1; n:12; v:'Ern�'),(h:1; n:13; v:'Veronika'),(h:1; n:14; v:'B�dog'),(h:1; n:15; v:'L�r�nd, L�r�nt'),(h:1; n:16; v:'Guszt�v'),(h:1; n:17; v:'Antal, Ant�nia'),(h:1; n:18; v:'Piroska'),(h:1; n:19; v:'M�ri�, S�ra'),(h:1; n:20; v:'F�bi�n, Sebesty�n'),
            (h:1; n:21; v:'�gnes'),(h:1; n:22; v:'Art�r, Vince'),(h:1; n:23; v:'Rajmund, Zelma'),(h:1; n:24; v:'Tim�t'),(h:1; n:25; v:'P�l'),(h:1; n:26; v:'Paula, Vanda'),(h:1; n:27; v:'Angelika'),(h:1; n:28; v:'Karola, K�roly'),(h:1; n:29; v:'Ad�l'),(h:1; n:30; v:'Martina'),(h:1; n:31; v:'Gerda, Marcella'),
            (h:2; n:1; v:'Ign�c'),(h:2; n:2; v:'Aida, Karolina'),(h:2; n:3; v:'Bal�zs'),(h:2; n:4; v:'Csenge, R�hel'),(h:2; n:5; v:'�gota, Ingrid'),(h:2; n:6; v:'D�ra, Dorottya'),(h:2; n:7; v:'R�me�, T�dor'),(h:2; n:8; v:'Aranka'),(h:2; n:9; v:'Abig�l, Alex'),(h:2; n:10; v:'Elvira'),
            (h:2; n:11; v:'Bertold, Marietta'),(h:2; n:12; v:'L�dia, L�via'),(h:2; n:13; v:'Ella, Linda'),(h:2; n:14; v:'B�lint, Valentin'),(h:2; n:15; v:'Georgina, Kolos'),(h:2; n:16; v:'Julianna, Lilla'),(h:2; n:17; v:'Don�t'),(h:2; n:18; v:'Bernadett'),(h:2; n:19; v:'Zsuzsanna'),
            (h:2; n:20; v:'Alad�r, �lmos'),(h:2; n:21; v:'Eleon�ra'),(h:2; n:22; v:'Gerzson'),(h:2; n:23; v:'Alfr�d'),(h:2; n:24; v:'M�ty�s'),(h:2; n:25; v:'G�za'),(h:2; n:26; v:'Edina'),(h:2; n:27; v:'�kos, B�tor'),(h:2; n:28; v:'Elem�r'),(h:3; n:1; v:'Albin'),(h:3; n:2; v:'Lujza'),
			 (h:3; n:3; v:'Korn�lia'),(h:3; n:4; v:'K�zm�r'),(h:3; n:5; v:'Adorj�n, Adri�n'),(h:3; n:6; v:'Inez, Leon�ra'),(h:3; n:7; v:'Tam�s'),(h:3; n:8; v:'Zolt�n'),(h:3; n:9; v:'Fanni, Franciska'),(h:3; n:10; v:'Ildik�'),(h:3; n:11; v:'Szil�rd'),(h:3; n:12; v:'Gergely'),(h:3; n:13; v:'Ajtony, Kriszti�n'),
            (h:3; n:14; v:'Matild'),(h:3; n:15; v:'Krist�f'),(h:3; n:16; v:'Henrietta'),(h:3; n:17; v:'Gertr�d, Patrik'),(h:3; n:18; v:'Ede, S�ndor'),(h:3; n:19; v:'B�nk, J�zsef'),(h:3; n:20; v:'Klaudia'),(h:3; n:21; v:'Benedek'),(h:3; n:22; v:'Be�ta, Izolda'),(h:3; n:23; v:'Em�ke'),
            (h:3; n:24; v:'G�bor, Karina'),(h:3; n:25; v:'Ir�n, �risz'),(h:3; n:26; v:'Em�nuel'),(h:3; n:27; v:'Hajnalka'),(h:3; n:28; v:'Gedeon, Johanna'),(h:3; n:29; v:'Auguszta'),(h:3; n:30; v:'Zal�n'),(h:3; n:31; v:'�rp�d'),(h:4; n:1; v:'Hug�'),(h:4; n:2; v:'�ron'),(h:4; n:3; v:'Buda, Rich�rd'),
			 (h:4; n:4; v:'Izidor'),(h:4; n:5; v:'Vince'),(h:4; n:6; v:'B�borka, Vilmos'),(h:4; n:7; v:'Herman'),(h:4; n:8; v:'D�nes'),(h:4; n:9; v:'Erhard'),(h:4; n:10; v:'Zsolt'),(h:4; n:11; v:'Le�, Szaniszl�'),(h:4; n:12; v:'Gyula'),(h:4; n:13; v:'Ida'),(h:4; n:14; v:'Tibor'),
            (h:4; n:15; v:'Anaszt�zia, Tas'),(h:4; n:16; v:'Csongor'),(h:4; n:17; v:'Rudolf'),(h:4; n:18; v:'Andrea, Ilma'),(h:4; n:19; v:'Emma'),(h:4; n:20; v:'Tivadar'),(h:4; n:21; v:'Konr�d'),(h:4; n:22; v:'Csilla, No�mi'),(h:4; n:23; v:'B�la'),(h:4; n:24; v:'Gy�rgy'),(h:4; n:25; v:'M�rk'),
            (h:4; n:26; v:'Ervin'),(h:4; n:27; v:'Zita'),(h:4; n:28; v:'Val�ria'),(h:4; n:29; v:'P�ter'),(h:4; n:30; v:'Katalin, Kitti'),(h:5; n:1; v:'F�l�p, Jakab'),(h:5; n:2; v:'Zsigmond'),(h:5; n:3; v:'Irma, T�mea'),(h:5; n:4; v:'Fl�ri�n, M�nika'),(h:5; n:5; v:'Adri�n, Gy�rgyi'),
            (h:5; n:6; v:'Frida, Ivett'),(h:5; n:7; v:'Gizella'),(h:5; n:8; v:'Mih�ly'),(h:5; n:9; v:'Gergely'),(h:5; n:10; v:'�rmin, P�lma'),(h:5; n:11; v:'Ferenc'),(h:5; n:12; v:'Pongr�c'),(h:5; n:13; v:'Imola, Szerv�c'),(h:5; n:14; v:'Bonif�c'),(h:5; n:15; v:'Szonja, Zs�fia'),
            (h:5; n:16; v:'Botond, M�zes'),(h:5; n:17; v:'Paszk�l'),(h:5; n:18; v:'Alexandra, Erik'),(h:5; n:19; v:'Iv�, Mil�n'),(h:5; n:20; v:'Bern�t, Fel�cia'),(h:5; n:21; v:'Konstantin'),(h:5; n:22; v:'J�lia, Rita'),(h:5; n:23; v:'Dezs�'),(h:5; n:24; v:'Eliza, Eszter'),(h:5; n:25; v:'Orb�n'),
            (h:5; n:26; v:'Evelin, F�l�p'),(h:5; n:27; v:'Hella'),(h:5; n:28; v:'Csan�d, Emil'),(h:5; n:29; v:'Magdolna'),(h:5; n:30; v:'Janka, Zsanett'),(h:5; n:31; v:'Ang�la, Petronella'),(h:6; n:1; v:'T�nde'),(h:6; n:2; v:'Anita, K�rmen'),(h:6; n:3; v:'Klotild'),(h:6; n:4; v:'Bulcs�'),
            (h:6; n:5; v:'Fatime'),(h:6; n:6; v:'Cintia, Norbert'),(h:6; n:7; v:'R�bert'),(h:6; n:8; v:'Med�rd'),(h:6; n:9; v:'F�lix'),(h:6; n:10; v:'Gr�ta, Margit'),(h:6; n:11; v:'Barnab�s'),(h:6; n:12; v:'Vill�'),(h:6; n:13; v:'Anett, Antal'),(h:6; n:14; v:'Vazul'),(h:6; n:15; v:'Jol�n, Vid'),
            (h:6; n:16; v:'Jusztin'),(h:6; n:17; v:'Alida, Laura'),(h:6; n:18; v:'Arnold, Levente'),(h:6; n:19; v:'Gy�rf�s'),(h:6; n:20; v:'Rafael'),(h:6; n:21; v:'Alajos, Leila'),(h:6; n:22; v:'Paulina'),(h:6; n:23; v:'Zolt�n'),(h:6; n:24; v:'Iv�n'),(h:6; n:25; v:'Vilmos'),(h:6; n:26; v:'J�nos, P�l'),
            (h:6; n:27; v:'L�szl�'),(h:6; n:28; v:'Ir�n, Levente'),(h:6; n:29; v:'P�ter, P�l'),(h:6; n:30; v:'P�l'),(h:7; n:1; v:'Annam�ria, Tiham�r'),(h:7; n:2; v:'Ott�'),(h:7; n:3; v:'Korn�l, Soma'),(h:7; n:4; v:'Ulrik'),(h:7; n:5; v:'Emese, Sarolta'),(h:7; n:6; v:'Csaba'),(h:7; n:7; v:'Apoll�nia'),
			 (h:7; n:8; v:'Ell�k'),(h:7; n:9; v:'Lukr�cia'),(h:7; n:10; v:'Am�lia'),(h:7; n:11; v:'Lili, N�ra'),(h:7; n:12; v:'Dalma, Izabella'),(h:7; n:13; v:'Jen�'),(h:7; n:14; v:'�rs, Stella'),(h:7; n:15; v:'Henrik, Roland'),(h:7; n:16; v:'Valter'),(h:7; n:17; v:'Elek, Endre'),(h:7; n:18; v:'Frigyes'),
			 (h:7; n:19; v:'Em�lia'),(h:7; n:20; v:'Ill�s'),(h:7; n:21; v:'D�niel, Daniella'),(h:7; n:22; v:'Magdolna'),(h:7; n:23; v:'Lenke'),(h:7; n:24; v:'Kincs�, Kinga'),(h:7; n:25; v:'Jakab, Krist�f'),(h:7; n:26; v:'Anik�, Anna'),(h:7; n:27; v:'Lili�na, Olga'),(h:7; n:28; v:'Szabolcs'),
			 (h:7; n:29; v:'Fl�ra, M�rta'),(h:7; n:30; v:'Judit, X�nia'),(h:7; n:31; v:'Oszk�r'),(h:8; n:1; v:'Bogl�rka'),(h:8; n:2; v:'Lehel'),(h:8; n:3; v:'Hermina'),(h:8; n:4; v:'Dominika, Domonkos'),(h:8; n:5; v:'Krisztina'),(h:8; n:6; v:'Berta, Bettina'),(h:8; n:7; v:'Ibolya'),(h:8; n:8; v:'L�szl�'),
			 (h:8; n:9; v:'Em�d'),(h:8; n:10; v:'L�rinc'),(h:8; n:11; v:'Tiborc, Zsuzsanna'),(h:8; n:12; v:'Kl�ra'),(h:8; n:13; v:'Ipoly'),(h:8; n:14; v:'Marcell'),(h:8; n:15; v:'M�ria'),(h:8; n:16; v:'�brah�m'),(h:8; n:17; v:'J�cint'),(h:8; n:18; v:'Ilona'),(h:8; n:19; v:'Huba'),(h:8; n:20; v:'Istv�n'),
			 (h:8; n:21; v:'Hajna, S�muel'),(h:8; n:22; v:'Menyh�rt, Mirjam'),(h:8; n:23; v:'Bence'),(h:8; n:24; v:'Bertalan'),(h:8; n:25; v:'Lajos, Patr�cia'),(h:8; n:26; v:'Izs�'),(h:8; n:27; v:'G�sp�r'),(h:8; n:28; v:'�goston'),(h:8; n:29; v:'Beatrix, Erna'),(h:8; n:30; v:'R�zsa'),(h:8; n:31; v:'Bella, Erika'),
			 (h:9; n:1; v:'Egon, Egyed'),(h:9; n:2; v:'Dorina, Rebeka'),(h:9; n:3; v:'Hilda'),(h:9; n:4; v:'Roz�lia'),(h:9; n:5; v:'L�rinc, Viktor'),(h:9; n:6; v:'Zakari�s'),(h:9; n:7; v:'Regina'),(h:9; n:8; v:'Adrienn, M�ria'),(h:9; n:9; v:'�d�m'),(h:9; n:10; v:'Hunor, Nikolett'),
			 (h:9; n:11; v:'Teod�ra'),(h:9; n:12; v:'M�ria'),(h:9; n:13; v:'Korn�l'),(h:9; n:14; v:'Rox�na, Szer�na'),(h:9; n:15; v:'Enik�, Melitta'),(h:9; n:16; v:'Edit'),(h:9; n:17; v:'Zs�fia'),(h:9; n:18; v:'Di�na'),(h:9; n:19; v:'Vilhelmina'),(h:9; n:20; v:'Friderika'),(h:9; n:21; v:'M�t�, Mirella'),
			 (h:9; n:22; v:'M�ric'),(h:9; n:23; v:'Tekla'),(h:9; n:24; v:'Gell�rt, Merc�desz'),(h:9; n:25; v:'Eufrozina, Kende'),(h:9; n:26; v:'Jusztina, P�l'),(h:9; n:27; v:'Adalbert'),(h:9; n:28; v:'Vencel'),(h:9; n:29; v:'Mih�ly'),(h:9; n:30; v:'Jeromos'),(h:10; n:1; v:'Malvin'),(h:10; n:2; v:'Petra'),
			 (h:10; n:3; v:'Helga'),(h:10; n:4; v:'Ferenc'),(h:10; n:5; v:'Aur�l'),(h:10; n:6; v:'Br�n�, Ren�ta'),(h:10; n:7; v:'Am�lia'),(h:10; n:8; v:'Kopp�ny'),(h:10; n:9; v:'D�nes'),(h:10; n:10; v:'Gedeon'),(h:10; n:11; v:'Brigitta'),(h:10; n:12; v:'Miksa'),(h:10; n:13; v:'Ede, K�lm�n'),
			 (h:10; n:14; v:'Hel�n'),(h:10; n:15; v:'Ter�z'),(h:10; n:16; v:'G�l'),(h:10; n:17; v:'Hedvig'),(h:10; n:18; v:'Luk�cs'),(h:10; n:19; v:'N�ndor'),(h:10; n:20; v:'Vendel'),(h:10; n:21; v:'Orsolya'),(h:10; n:22; v:'El�d'),(h:10; n:23; v:'Gy�ngyi'),(h:10; n:24; v:'Salamon'),(h:10; n:25; v:'Bianka, Blanka'),
			 (h:10; n:26; v:'D�m�t�r'),(h:10; n:27; v:'Szabina'),(h:10; n:28; v:'Simon, Szimonetta'),(h:10; n:29; v:'N�rcisz'),(h:10; n:30; v:'Alfonz'),(h:10; n:31; v:'Farkas'),(h:11; n:1; v:'Marianna'),(h:11; n:2; v:'Achilles'),(h:11; n:3; v:'Gy�z�'),(h:11; n:4; v:'K�roly'),(h:11; n:5; v:'Imre'),
			 (h:11; n:6; v:'L�n�rd'),(h:11; n:7; v:'Rezs�'),(h:11; n:8; v:'Zsombor'),(h:11; n:9; v:'Tivadar'),(h:11; n:10; v:'R�ka'),(h:11; n:11; v:'M�rton'),(h:11; n:12; v:'J�n�s, Ren�t�'),(h:11; n:13; v:'Szilvia'),(h:11; n:14; v:'Aliz'),(h:11; n:15; v:'Albert, Lip�t'),(h:11; n:16; v:'�d�n'),
			 (h:11; n:17; v:'Gerg�, Hortenzia'),(h:11; n:18; v:'Jen�'),(h:11; n:19; v:'Erzs�bet'),(h:11; n:20; v:'Jol�n'),(h:11; n:21; v:'Oliv�r'),(h:11; n:22; v:'Cec�lia'),(h:11; n:23; v:'Kelemen, Klementina'),(h:11; n:24; v:'Emma'),(h:11; n:25; v:'Katalin'),(h:11; n:26; v:'Vir�g'),(h:11; n:27; v:'Virgil'),
			 (h:11; n:28; v:'Stef�nia'),(h:11; n:29; v:'Taksony'),(h:11; n:30; v:'Andor, Andr�s'),(h:12; n:1; v:'Elza'),(h:12; n:2; v:'Melinda, Vivien'),(h:12; n:3; v:'Ferenc, Ol�via'),(h:12; n:4; v:'Barbara, Borb�la'),(h:12; n:5; v:'Vilma'),(h:12; n:6; v:'Mikl�s'),(h:12; n:7; v:'Ambrus'),(h:12; n:8; v:'M�ria'),
			 (h:12; n:9; v:'Nat�lia'),(h:12; n:10; v:'Judit'),(h:12; n:11; v:'�rp�d'),(h:12; n:12; v:'Gabriella'),(h:12; n:13; v:'Luca, Ot�lia'),(h:12; n:14; v:'Szil�rda'),(h:12; n:15; v:'Val�r'),(h:12; n:16; v:'Aletta, Etelka'),(h:12; n:17; v:'L�z�r, Olimpia'),(h:12; n:18; v:'Auguszta'),(h:12; n:19; v:'Viola'),
			 (h:12; n:20; v:'Teofil'),(h:12; n:21; v:'Tam�s'),(h:12; n:22; v:'Z�n�'),(h:12; n:23; v:'Vikt�ria'),(h:12; n:24; v:'�d�m, �va'),(h:12; n:25; v:'Eug�nia'),(h:12; n:26; v:'Istv�n'),(h:12; n:27; v:'J�nos'),(h:12; n:28; v:'Kamilla'),(h:12; n:29; v:'Tamara, Tam�s'),(h:12; n:30; v:'D�vid'), (h:12; n:31; v:'Szilveszter') );

procedure TCalendarDlg.FormCreate(Sender: TObject);
var
	datestr	: string;
	i		: integer;
begin
	HoGrid.Cells[0,0] 		:= 'h�t';
	HoGrid.Cells[1,0] 		:= 'h';
	HoGrid.Cells[2,0] 		:= 'k';
	HoGrid.Cells[3,0] 		:= 'sze';
	HoGrid.Cells[4,0] 		:= 'cs';
	HoGrid.Cells[5,0] 		:= 'p';
	HoGrid.Cells[6,0] 		:= 'szo';
	HoGrid.Cells[7,0] 		:= 'v';
	datestr	   				:= FormatDateTime('yyyy.mm.dd',Date);
	evszam	   				:= StrToIntDef(copy(datestr,1,4),2000);
	hoszam	   				:= StrToIntDef(copy(datestr,6,2),1);
	napszam	   				:= StrToIntDef(copy(datestr,9,2),1);
	SpinEdit1.Value     	:= evszam;
	HoBox.ItemIndex			:= hoszam-1;
	HoGrid.DefaultColWidth	:= Panel2.Width 	DIV 8 - 1;
	CalendarDlg.ClientWidth	:= HoGrid.Width;
//   HoGrid.DefaultRowHeight	:= Panel2.Height 	DIV 8 - 1;
	ret_date				:= '';
	ret_date2				:= '';
	kiemelt					:= -1;
	Application.OnHint 		:= DisplayHint2;
	for i := 1 to 12 do begin
		hogombok[i]			:= TBitBtn.Create(Panel1);
		hogombok[i].Parent  := Panel1;
		hogombok[i].Name	:= 'Gomb'+IntToStr(i);
		hogombok[i].Caption	:= IntToStr(i);
		hogombok[i].Height	:= Panel1.Height - HoBox.Top - HoBox.Height - 4;
		hogombok[i].Top		:= HoBox.Top + HoBox.Height +2;
		Hogombok[i].Width	:= ( Panel1.Width div 12 ) - 1;
		Hogombok[i].OnClick	:= SGHoClick;
	end;
	ObjektumSorolo(Panel1.Width, [hogombok[1], hogombok[2], hogombok[3], hogombok[4], hogombok[5], hogombok[6],
		hogombok[7], hogombok[8], hogombok[9], hogombok[10], hogombok[11], hogombok[12]])
end;

procedure TCalendarDlg.Honapiro;
var
	i 			: integer;
	maxnap		: integer;
	datum		: TDateTime;
	sor			: integer;
	oszlop		: integer;
begin
	{A grid t�rl�se}
	for i := 1 to 7 do begin
		HoGrid.Rows[i].Clear;
	end;
	maxnap := napok[hoszam];
	if ((hoszam = 2) and ( evszam mod 4 = 0) and ( ( evszam mod 100 <> 0 ) or (evszam mod 400 = 0)) ) then begin
		maxnap	:= 29;
	end;
	try
   	HoGrid.RowCount	:= 7;
		napszam			:= 1;
       datum 			:= EncodeDate(WORD(evszam), WORD(hoszam), WORD(napszam));
       oszlop 			:= DayOfWeek(datum);	// 1 - vas�rnap; 2 - h�tf�; ...
       if oszlop = 1 then begin
       	oszlop		:= 8;
       end;
       Dec(oszlop);						// 1 - h�tf�; 2 - kedd; ...
       sor      	:= 1;
       for i := 1 to maxnap do begin
           HoGrid.Cells[oszlop,sor] := IntToStr(i);
           Inc(oszlop);
           if oszlop = 8 then begin
               oszlop := 1;
               Inc(sor);
           end;
   	end;
       if HoGrid.Cells[1, 7] = '' then begin
       	// Ez a sor m�r nem kell
           HoGrid.RowCount	:= 7;
       end;
       if HoGrid.Cells[1, 6] = '' then begin
       	// Ez a sor m�r nem kell
           HoGrid.RowCount	:= 6;
       end;
       // A sorok sz�th�z�sa
//       HoGrid.DefaultRowHeight	:= ( Panel2.Height div hoGrid.RowCount ) - 1;
		CalendarDlg.Height	:= Panel1.Height + StatusBar1.Height + ( HoGrid.RowCount + 1 ) * HoGrid.DefaultRowHeight + 15;
       HoGrid.Height		:= Panel2.Height;

       // A  hetek ki�r�sa
      	datum 		:= EncodeDate(WORD(evszam), WORD(hoszam), 1);
      	HoGrid.Cells[0, 1]  := IntToStr(WeekOfTheYear(datum));
		for i := 2 to HoGrid.RowCount - 1 do begin
       	datum 		:= EncodeDate(WORD(evszam), WORD(hoszam), WORD(StrToIntDef(HoGrid.Cells[1, i ], 0)));
       	HoGrid.Cells[0, i ]  := IntToStr(WeekOfTheYear(datum));
		end;

		// Az �v �s h�nap ki�r�sa a fejl�cben
       label1.Caption	:= IntToStr(evszam)+'. '+honapok[hoszam];
       label1.Update;

  	except
  	end;
end;

procedure TCalendarDlg.HoBoxChange(Sender: TObject);
begin
	hoszam	:= HoBox.ItemIndex+1;
  	Honapiro;
end;

procedure TCalendarDlg.SpinEdit1Change(Sender: TObject);
begin
	evszam	:= SpinEdit1.Value;
  	Honapiro;
end;

procedure TCalendarDlg.SpeedButton2Click(Sender: TObject);
begin
	// A h�nap n�vel�se
	Inc(hoszam);
  	if hoszam > 12 then begin
  		Inc(evszam);
     	SpinEdit1.Value	:= evszam;
     	hoszam	:= 1;
  	end;
  	HoBox.ItemIndex	:= hoszam-1;
  	Honapiro;
end;

procedure TCalendarDlg.SpeedButton1Click(Sender: TObject);
begin
	// A h�nap cs�kkent�se
   Dec(hoszam);
	if hoszam < 1 then begin
  		Dec(evszam);
     	SpinEdit1.Value	:= evszam;
     	hoszam	:= 12;
  	end;
  	HoBox.ItemIndex	:= hoszam-1;
  	Honapiro;
end;

procedure TCalendarDlg.HoGridDblClick(Sender: TObject);
var
  MyDate1, MyDate2: TDateTime;
  Het: integer;
begin
  ret_date	:= '';
	if HoGrid.Col = 0 then begin   // h�tre kattint
    Het:= StrToIntDef(HoGrid.Cells[HoGrid.Col,HoGrid.Row],0);
		ret_date:= HetNapja(evszam, Het, 1);
    ret_date2:= HetNapja(evszam, Het, 7);
		Close;
   end;
	if (HoGrid.Col >= 1) and (HoGrid.Cells[HoGrid.Col,HoGrid.Row] <> '') then begin  // napra kattint
		ret_date	:= Format('%4.4d.%2.2d.%2.2d.',[evszam, hoszam, StrToIntDef(HoGrid.Cells[HoGrid.Col,HoGrid.Row],0)]);
    ret_date2	:= ret_date;  // egy nap
		Close;
	end;
end;

function TCalendarDlg.HetNapja(Ev, Het, HanyadikNap: integer): string;
var
  MyDate: TDateTime;
begin
  MyDate:= StartOfAWeek(Ev, Het, HanyadikNap);
  Result:= FormatDateTime('yyyy.mm.dd.', MyDate);
end;

procedure TCalendarDlg.Tolt(datstr	: string);
begin
	ret_date			:= '';
	evszam				:= StrToIntDef(copy(datstr,1,4),2000);
	hoszam				:= StrToIntDef(copy(datstr,6,2),1);
	napszam				:= StrToIntDef(copy(datstr,9,2),1);
	kiemelt				:= napszam;
	if 1 > hoszam then begin
		hoszam	:= 1;
	end;
	if hoszam > 12 then begin
		hoszam	:= 12;
	end;
	HoBox.ItemIndex		:= hoszam-1;
	SpinEdit1.Value     := evszam;
	HonapIro;
end;

procedure TCalendarDlg.HoGridDrawCell(Sender: TObject; Col, Row: Longint;
  Rect: TRect; State: TGridDrawState);
var
	siz	: TSize;
  Nap: integer;
  Kivalasztott: boolean;
begin
	// A kiemel�sek megt�tele
   Nap:= StrToIntDef(HoGrid.Cells[Col, Row],-1);
   Kivalasztott:= ((kiemelt<100) and (Nap = kiemelt))
                 or ((kiemelt>=100) and (Row = kiemelt-100));     // 101, 102, stb.: ez a sor (h�t) kiv�laszt�s
   HoGrid.Canvas.Font.Color		:= clBlack;
  	if ( Kivalasztott and (col > 0) ) then begin
		HoGrid.Canvas.Brush.Color 	:= clBlue;
  	end else begin
  		if Row > 0 then begin
			HoGrid.Canvas.Brush.Color 	:= clWhite;
		end;
		if Col = 0 then begin
			HoGrid.Canvas.Brush.Color 	:= clLime;
		end;
		if Col > 5 then begin
			HoGrid.Canvas.Brush.Color 	:= clAqua;
			HoGrid.Canvas.Font.Color	:= clRed;
		end;
		if Row < 1 then begin
			HoGrid.Canvas.Brush.Color 	:= TILTOTTSZIN;
		end;
	end;

	HoGrid.Canvas.Rectangle(Rect.Left,Rect.Top, Rect.Right, Rect.Bottom);
	siz := HoGrid.Canvas.TextExtent(HoGrid.Cells[Col, Row]);
	HoGrid.Canvas.TextOut(
		Rect.Left+( HoGrid.DefaultColWidth-siz.cx) div 2,
		Rect.Top +( HoGrid.DefaultRowHeight-siz.cy) div 2,
		HoGrid.Cells[Col, Row]);
end;

procedure TCalendarDlg.HoGridClick(Sender: TObject);
begin
	if ( ( HoGrid.Cells[HoGrid.Col,HoGrid.Row] <> '' )  and (HoGrid.Row > 0) ) then begin
    if (HoGrid.Col > 0) then begin  // napra kattint
  		kiemelt	:= StrToIntDef(HoGrid.Cells[HoGrid.Col, HoGrid.Row],-1);
      end;
    if (HoGrid.Col = 0) then begin  // h�tre kattint
  		kiemelt	:= 100+HoGrid.Row;  //  101, 102, stb.: ez a sor (h�t) kiv�laszt�s
      end;
		HoGrid.Refresh;
	end;
end;

procedure TCalendarDlg.SpeedButton3Click(Sender: TObject);
begin
	// Az �vsz�m cs�kkent�se
	Dec(evszam);
	SpinEdit1.Value     	:= evszam;
	Honapiro;
end;

procedure TCalendarDlg.SpeedButton4Click(Sender: TObject);
begin
	// Az �vsz�m n�vel�se
	Inc(evszam);
	SpinEdit1.Value     	:= evszam;
	Honapiro;
end;

procedure TCalendarDlg.HoGridMouseMove(Sender: TObject; Shift: TShiftState;
  X, Y: Integer);
var
	rownr, colnr 	: integer;
	sorstr			: string;
begin
	// Mozog a kurzor, ki�rjuk az aktu�lis napot
	sorstr	:= '';
	HoGrid.MouseToCell(X, Y, colnr, rownr);
	if ( ( colnr < 0 ) or (rownr < 0) ) then begin
		Exit;
	end;
	if ( (HoGrid.Cells[colnr, rownr] <> '' ) and ( colnr > 0 ) and (rownr > 0) ) then begin
		// A hetek oszlop�ra nem kell hint
		sorstr := IntToStr(evszam)+'. '+honapok[hoszam]+' '+HoGrid.Cells[colnr, rownr]+'.'+
			'  ('+Getnevnap(hoszam, StrToIntdef(HoGrid.Cells[colnr, rownr],0))+')';
	end;
	Application.CancelHint;
	hoGrid.Hint := sorstr;;
end;

procedure TCalendarDlg.DisplayHint2(Sender: TObject);
begin
//	StatusBar1.SimpleText	:=  ' ' + Application.Hint;
	StatusBar1.Panels[0].Alignment	:= taCenter;
	StatusBar1.Panels[0].Text 		:=  ' ' + Application.Hint;
end;

function TCalendarDlg.Getnevnap(honr, nanr : integer) : string;
var
	nv 	: nevnap;
	i	: integer;
begin
	Result	:= '';
	i		:= 1;
	while i <= 365 do begin
		nv	:= nevnapok[i];
		if ( ( nv.h = honr ) and ( nv.n = nanr ) ) then begin
			Result	:= nv.v;
			i		:= 365;
		end;
		Inc(i);
	end;
end;

procedure TCalendarDlg.FormDestroy(Sender: TObject);
begin
	Application.OnHint 		:= nil;
end;

procedure TCalendarDlg.SpeedButton5Click(Sender: TObject);
var
	datstr	: string;
begin
  //KG 20111006
  ret_date:=DateToStr(date);
  ret_date2	:= ret_date;
  close;
  exit;
  //////////////////////////////

	// R�ugrunk a mai napra
	datstr	   		   	:= FormatDateTime('yyyy.mm.dd',Date);
	evszam				:= StrToIntDef(copy(datstr,1,4),2000);
	hoszam				:= StrToIntDef(copy(datstr,6,2),1);
	napszam				:= StrToIntDef(copy(datstr,9,2),1);

		ret_date	:= Format('%4.4d.%2.2d.%2.2d.',[evszam, hoszam, StrToIntDef(HoGrid.Cells[HoGrid.Col,HoGrid.Row],0)]);
		Close;

	kiemelt				:= napszam;
	HoBox.ItemIndex		:= hoszam-1;
	SpinEdit1.Value     := evszam;
	HonapIro;
end;

procedure TCalendarDlg.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
	if Key = 27 then begin
		ret_date	:= '';
		Close;
	end;
end;

procedure TCalendarDlg.SGHoClick(Sender: TObject);
var
	maxnap		: integer;
	honap		: integer;
begin
	// Ki lehet l�pni, mert megvan a h�nap ?
	honap	:= StrToIntDef((Sender as TBitBtn).Caption, 0);
	if honap = 0 then begin
		Exit;
	end;
	ret_date	:= Format('%4.4d.%2.2d.01.',[evszam, honap]);
	maxnap := napok[honap];
	if ((honap = 2) and ( evszam mod 4 = 0) and ( ( evszam mod 100 <> 0 ) or (evszam mod 400 = 0)) ) then begin
		maxnap	:= 29;
	end;
	ret_date2	:= Format('%4.4d.%2.2d.%2.2d.',[evszam, honap, maxnap]);
	Close;
end;

end.
