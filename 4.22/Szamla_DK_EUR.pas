unit Szamla_DK_EUR;

interface

uses
  WinTypes, WinProcs, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, QuickRpt, QRExpr, Qrctrls, ExtCtrls, printers, DB, DBTables,
  ADODB, J_DataModule, System.Contnrs ;

type
  TSZAMLA_DK_EURForm = class(TForm)
    Rep: TQuickRep;
    QRBand3: TQRBand;
    QRLNEV: TQRLabel;
    QRLCIMB: TQRLabel;
    QRLSzamla: TQRLabel;
    QRBand1: TQRBand;
    QRBand2: TQRBand;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel5: TQRLabel;
    QRShape1: TQRShape;
    QRShape2: TQRShape;
    QRLabel6: TQRLabel;
    QRLabel7: TQRLabel;
    QRLIRSZ: TQRLabel;
    QRLabel8: TQRLabel;
	 QRLabel9: TQRLabel;
    QRLabel10: TQRLabel;
    QRLabel11: TQRLabel;
    QRLabel12: TQRLabel;
    QRLabel13: TQRLabel;
	 QRLabel14: TQRLabel;
    QRShape3: TQRShape;
    QRLabel15: TQRLabel;
    QRShape4: TQRShape;
    QRLabel16: TQRLabel;
    QRShape5: TQRShape;
    QRLabel17: TQRLabel;
    QRShape6: TQRShape;
    QRLabel18: TQRLabel;
    QRShape7: TQRShape;
    QRLabel20: TQRLabel;
    QRLabel21: TQRLabel;
    QRLabel22: TQRLabel;
    QRLabel23: TQRLabel;
    QRLabel24: TQRLabel;
    QRShape13: TQRShape;
    QRLabel25: TQRLabel;
    QRLabel26: TQRLabel;
    QRLabel27: TQRLabel;
    QRLabel28: TQRLabel;
    QRLabel29: TQRLabel;
    QRLabel1: TQRLabel;
    QRLabel34: TQRLabel;
	 QRLabel36: TQRLabel;
    QRLabel37: TQRLabel;
    QRLabel38: TQRLabel;
    QRLabel39: TQRLabel;
    QRLabel40: TQRLabel;
    QRLabel41: TQRLabel;
    QRLabel42: TQRLabel;
    QRLabel43: TQRLabel;
    QRLabel44: TQRLabel;
    QRLabel45: TQRLabel;
	 QRLabel48: TQRLabel;
    LabelO1: TQRLabel;
    QRLabel47: TQRLabel;
    QRLabel49: TQRLabel;
    QRLabel50: TQRLabel;
    QRLabel51: TQRLabel;
    QRLabel52: TQRLabel;
    QRLabel53: TQRLabel;
    QRLabel54: TQRLabel;
    QRLabel56: TQRLabel;
    QRLabel57: TQRLabel;
    QRLabel59: TQRLabel;
    QRLabel60: TQRLabel;
    QRLabel61: TQRLabel;
    QRLabel62: TQRLabel;
    QRLabel63: TQRLabel;
    QRLabel66: TQRLabel;
    QRLabel67: TQRLabel;
	 QRLabel68: TQRLabel;
    QRLabel70: TQRLabel;
    QRLabel30: TQRLabel;
    QRLabel31: TQRLabel;
    QRLabel69: TQRLabel;
    QRLabel71: TQRLabel;
    QRLabel74: TQRLabel;
    QRLabel32: TQRLabel;
    QRShape33: TQRShape;
    QRShape34: TQRShape;
    QRLabel81: TQRLabel;
    QRLabel72: TQRLabel;
    QRLabel80: TQRLabel;
    QRBand4: TQRBand;
    QRLabel82: TQRLabel;
	 QRLabel83: TQRLabel;
    QRLabel84: TQRLabel;
    QRLabel101: TQRLabel;
    QRLabel65: TQRLabel;
    QueryKoz: TADOQuery;
    QueryAl: TADOQuery;
    QueryFej: TADOQuery;
    QuerySor: TADOQuery;
    QRLabel8A: TQRLabel;
    QRLabel64: TQRLabel;
    QRLabel85: TQRLabel;
    QRShape10: TQRShape;
    QRShape11: TQRShape;
	 QRShape12: TQRShape;
    QRShape14: TQRShape;
    QRShape15: TQRShape;
    QRShape16: TQRShape;
    QRShape17: TQRShape;
    QRShape18: TQRShape;
    QRLabel33: TQRLabel;
    QRLabel35: TQRLabel;
    QRLabel73: TQRLabel;
    QRLabel75: TQRLabel;
    QRShape8: TQRShape;
    QRShape9: TQRShape;
    QRLabel46: TQRLabel;
    QRLabel55: TQRLabel;
    QRLabel19: TQRLabel;
    QRLabel58: TQRLabel;
    QRShape19: TQRShape;
    QRLabel76: TQRLabel;
    QRLabel77: TQRLabel;
    QRShape26: TQRShape;
	 QRLabel78: TQRLabel;
	 QRLabel79: TQRLabel;
	 QRShape27: TQRShape;
	 QRLabel86: TQRLabel;
	 SZ_CIM1: TQRLabel;
	 SZ_BOLD1: TQRLabel;
	 SZ_NORM1: TQRLabel;
    QRLabel87: TQRLabel;
    QRLabel88: TQRLabel;
	 procedure FormCreate(Sender: TObject);
	 procedure QRBand3BeforePrint(Sender: TQRCustomBand;
	   var PrintBand: Boolean);
	 function	Tolt(szkod	: string) : boolean;
	 procedure QRBand1BeforePrint(Sender: TQRCustomBand;
	   var PrintBand: Boolean);
	 procedure QRBand2BeforePrint(Sender: TQRCustomBand;
	   var PrintBand: Boolean);
	 procedure RepBeforePrint(Sender: TCustomQuickRep;
	   var PrintReport: Boolean);
  private
		oldalszam		: integer;
		arfoly			: double;
		JSzamla     	: TJSzamla;
  public
  end;

var
  SZAMLA_DK_EURForm: TSZAMLA_DK_EURForm;

implementation

uses
	Egyeb, J_SQL, kozos;

{$R *.DFM}

procedure TSZAMLA_DK_EURForm.FormCreate(Sender: TObject);
begin
	EgyebDlg.SetADOQueryDatabase(QueryKoz);
	EgyebDlg.SetADOQueryDatabase(QueryAl);
	EgyebDlg.SetADOQueryDatabase(QueryFej);
	EgyebDlg.SetADOQueryDatabase(QuerySor);
	oldalszam	:= 1;
end;

procedure TSZAMLA_DK_EURForm.QRBand3BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
var
	orsz	: string;
begin
 	if QueryFej.FieldByName('SA_PELDANY').AsInteger < EgyebDlg.PeldanySzam then begin
 		QrLabel66.Caption := 'P�ld�ny : '+
		Format('%3.0d',[QueryFej.FieldByName('SA_PELDANY').AsInteger + 1])+' / '+
			IntToStr(EgyebDlg.Peldanyszam)+'  ';
		QrLabel19.Caption := 'Eredeti';
	end else begin
		QrLabel19.Caption := 'M�solat';
		QrLabel66.Caption := '';
	end;
	QrLabel81.Caption := 'Oldal : '+Format('%5d',[oldalszam]) + '  ';
	Inc(oldalszam);
	{A fejl�c adatainak ki�r�sa}
	QrLNEV.Caption 		:= EgyebDlg.Read_SZGrid('CEG', '100');
	QrLIRSZ.Caption    	:= EgyebDlg.Read_SZGrid('CEG', '102');
	QrLabel8.Caption 	:= EgyebDlg.Read_SZGrid('CEG', '104');
	QrLabel9.Caption 	:= EgyebDlg.Read_SZGrid('CEG', '106');
	QrLabel10.Caption 	:= EgyebDlg.Read_SZGrid('CEG', '107');
	QrLabel8A.Caption 	:= EgyebDlg.Read_SZGrid('CEG', '108');
	QrLabel80.Caption 	:= EgyebDlg.Read_SZGrid('CEG', '109');
	if QueryFej.FieldByName('SA_FEJL1').AsString <> '' then begin
		{L�tezik fejl�c adat, azt kell betenni a sz�ml�ba}
		QrLNEV.Caption 		:= QueryFej.FieldByName('SA_FEJL1').AsString;
		QrLIRSZ.Caption 	:= QueryFej.FieldByName('SA_FEJL2').AsString;
		QrLabel8.Caption 	:= QueryFej.FieldByName('SA_FEJL3').AsString;
		QrLabel9.Caption 	:= QueryFej.FieldByName('SA_FEJL4').AsString;
		QrLabel10.Caption 	:= QueryFej.FieldByName('SA_FEJL5').AsString;
		QrLabel8A.Caption 	:= QueryFej.FieldByName('SA_FEJL6').AsString;
		QrLabel80.Caption 	:= QueryFej.FieldByName('SA_FEJL7').AsString;
	end;
	QrLabel11.Caption 		:= QueryFej.FieldByName('SA_VEVONEV').AsString;
	orsz					:= Query_Select('VEVO', 'V_NEV', QueryFej.FieldByName('SA_VEVONEV').AsString, 'V_ORSZ');
	QrLabel12.Caption 		:= orsz + '-' + QueryFej.FieldByName('SA_VEIRSZ').AsString;
	QrLabel13.Caption 		:= QueryFej.FieldByName('SA_VEVAROS').AsString;
	QrLabel14.Caption 		:= QueryFej.FieldByName('SA_VECIM').AsString;
	if Pos('.', QrLabel14.Caption) < 1 then begin
		QrLabel14.Caption := QrLabel14.Caption + '.';
	end;
	QrLabel101.Caption 		:= JSzamla.szamlaadoszam;
(*
	QrLabel101.Caption := QueryFej.FieldByName('SA_EUADO').AsString;
	if QrLabel101.Caption = '' then begin
		QrLabel101.Caption := QueryFej.FieldByName('SA_VEADO').AsString;
	end;
*)
	QrLabel20.Caption 	:= QueryFej.FieldByName('SA_FIMOD').AsString;
	QrLabel85.Caption 	:= QueryFej.FieldByName('SA_MEGRE').AsString;
	QrLabel21.Caption 	:= QueryFej.FieldByName('SA_TEDAT').AsString;
	QrLabel22.Caption 	:= QueryFej.FieldByName('SA_KIDAT').AsString;
	QrLabel23.Caption 	:= QueryFej.FieldByName('SA_ESDAT').AsString;
	QrLabel24.Caption 	:= 'Sorsz�m : '+ QueryFej.FieldByName('SA_KOD').AsString;
	QrLabel25.Caption 	:= QueryFej.FieldByName('SA_EGYA1').AsString;
	QrLabel56.Caption 	:= QueryFej.FieldByName('SA_EGYA2').AsString;
	QrLabel57.Caption 	:= QueryFej.FieldByName('SA_EGYA3').AsString;

	QrLabel76.Caption := QueryFej.FieldByName('SA_MEGJ').AsString;
	QrLabel78.Caption := QueryFej.FieldByName('SA_JARAT').AsString;
	{A j�ratsz�m alapj�n megkeress�k a bet�jelet is }
	if Query_Run(QueryAl, 'SELECT * FROM JARAT WHERE JA_ALKOD = '+
		IntToStr(Round(StringSzam(QueryFej.FieldByName('SA_JARAT').AsString)))+
       ' AND JA_JKEZD LIKE '''+ copy( QueryFej.FieldByName('SA_DATUM').AsString ,1,4) +'%'' ',true) then begin
			QrLabel78.Caption := QueryAl.FieldByName('JA_ORSZ').AsString + ' - ' +
			QueryFej.FieldByName('SA_JARAT').AsString;
	end;
	QrLabel86.Caption := QueryFej.FieldByName('SA_RSZ').AsString;
	QrLabel63.Caption := QueryFej.FieldByName('SA_POZICIO').AsString;
end;

function	TSZAMLA_DK_EURForm.Tolt(szkod	: string) : boolean;
begin
	Result	:= false;
	Query_Run(QueryFej, 'SELECT * FROM SZFEJ WHERE SA_UJKOD = '+szkod);
	if QueryFej.RecordCount < 1 then begin
		NoticeKi('Hi�nyz� sz�mlarekord : ('+szkod+')');
		Exit;
	end;
	if StrToIntDef(QueryFej.FieldByName('SA_RESZE').AsString, 0) > 0 then begin
		QRLSzamla.Caption	:= 'FIGYELEM!!! R�SZSZ�MLA!!!';
	end;
	JSzamla	:= TJSzamla.Create(Self);
	JSzamla.FillSzamla(QueryFej.FieldByName('SA_UJKOD').AsString);
	QrLabel82.Caption := 'K�sz�tette : '+' DMK COMP Kft., Tatab�nya';
	Result		:= true;
	Query_Run(QuerySor, 'SELECT * FROM SZSOR WHERE SS_UJKOD = '+szkod+' ORDER BY SS_SOR');
end;

procedure TSZAMLA_DK_EURForm.QRBand1BeforePrint(Sender: TQRCustomBand;
	   var PrintBand: Boolean);
var
	sor_szama	: integer;
	JSzamlaSor	: TJSzamlaSor;
begin
	if JSzamla.eredmenyflag then begin
		sor_szama	:= StrToIntDef(QuerySor.FieldByName('SS_SOR').AsString,-1) -1;
		JSzamlasor	:= (JSzamla.SzamlaSorok[sor_szama] as TJszamlaSor);
	end else begin
		Exit;
	end;
	if QuerySor.FieldByName('SS_ITJSZJ').AsString = '' then begin
		QrLabel1.Caption := QuerySor.FieldByName('SS_TERNEV').AsString;
	end else begin
		QrLabel1.Caption := QuerySor.FieldByName('SS_ITJSZJ').AsString+', '+QuerySor.FieldByName('SS_TERNEV').AsString;
	end;
	{A k�lf�ldi le�r�s megjelen�t�se}
	if QuerySor.FieldByName('SS_EGYAR').AsFloat = 0 then begin
		QrLabel32.Caption := '';
		QrLabel37.Caption := '';
		QrLabel68.Caption := '';
		QrLabel61.Caption := '';
		QrLabel34.Caption := '';
		QrLabel36.Caption := '';
		QrLabel72.Caption := '';
	end else begin
		QrLabel32.Caption := EgyebDlg.Read_SZGrid('101',QuerySor.FieldByName('SS_AFAKOD').AsString);
		QrLabel37.Caption := Format('%.2f',[StringSzam(QuerySor.FieldByName('SS_DARAB').AsString)]) ;
		QrLabel68.Caption := QuerySor.FieldByName('SS_MEGY').AsString;
		if QuerySor.FieldByName('SS_KULME').AsString <> '' then begin
			QrLabel68.Caption := QrLabel68.Caption + '/'+QuerySor.FieldByName('SS_KULME').AsString;
		end;
		QrLabel61.Caption := Format('%.2f',[QuerySor.FieldByName('SS_VALERT').AsFloat]);
		QrLabel34.Caption := Format('%.2f',[JSzamlasor.sorneteur]);
		QrLabel36.Caption := Format('%.2f',[JSzamlasor.sorafaeur]);
		QrLabel32.Caption := EgyebDlg.Read_SZGrid('101', QuerySor.FieldByName('SS_AFAKOD').AsString );
		QrLabel72.Caption := Format('%.2f',[JSzamlasor.sorbruteur]);
		if StringSZam(QuerySor.FieldByName('SS_ARFOL').AsString) > 0 then begin
			arfoly   	:= StringSZam(QuerySor.FieldByName('SS_ARFOL').AsString);
		end;
	end;
end;

procedure TSZAMLA_DK_EURForm.QRBand2BeforePrint(Sender: TQRCustomBand;
	   var PrintBand: Boolean);
var
	egesz	: string;
	tized	: string;
begin
	if ( JSzamla.GetAfaSorszam('101') > 0 ) then begin
		QrLabel42.Caption := '25'+copy(QrLabel42.Caption,3,99);
		QrLabel41.Caption := '25'+copy(QrLabel41.Caption,3,99);
	end;
//	if QueryFej.FieldByName('SA_KIDAT').AsString >= '2009.07.01.' then begin
//		QrLabel42.Caption := '25'+copy(QrLabel42.Caption,3,99);
//		QrLabel41.Caption := '25'+copy(QrLabel41.Caption,3,99);
//	end;
	if QueryFej.FieldByName('SA_KIDAT').AsString >= '2012.01.01.' then begin
		QrLabel42.Caption := '27'+copy(QrLabel42.Caption,3,99);
		QrLabel41.Caption := '27'+copy(QrLabel41.Caption,3,99);
	end;
	if ( JSzamla.GetAfaSorszam('110') > 0 ) then begin
		QrLabel42.Caption := '20'+copy(QrLabel42.Caption,3,99);
		QrLabel41.Caption := '20'+copy(QrLabel41.Caption,3,99);
	end;
	if ( JSzamla.GetAfaSorszam(JSzamla.afakod27) > 0 ) then begin
		QrLabel42.Caption := '27'+copy(QrLabel42.Caption,3,99);
		QrLabel41.Caption := '27'+copy(QrLabel41.Caption,3,99);
	end;

	LabelO1.Caption 	:= Format('%.2f EUR',[JSzamla.ossz_nettoeur]);
	QrLabel75.Caption 	:= Format('%.2f EUR',[JSzamla.ossz_afaeur]);
	QrLabel50.Caption 	:= Format('%.2f EUR',[JSzamla.ossz_bruttoeur]);

	QrLabel47.Caption 	:= Format('%.2f EUR',[( JSzamla.Afasorok[JSzamla.GetAfaSorszam('102')] as TJAfasor ).afaerteur]);
	QrLabel49.Caption 	:= Format('%.2f EUR',[( JSzamla.Afasorok[JSzamla.GetAfaSorszam('110')] as TJAfasor ).afaerteur]);
	QrLabel51.Caption 	:= Format('%.2f EUR',[( JSzamla.Afasorok[JSzamla.GetAfaSorszam('102')] as TJAfasor ).afaaleur]);
	QrLabel52.Caption 	:= Format('%.2f EUR',[( JSzamla.Afasorok[JSzamla.GetAfaSorszam('110')] as TJAfasor ).afaaleur]);
	QrLabel53.Caption 	:= Format('%.2f EUR',[( JSzamla.Afasorok[JSzamla.GetAfaSorszam('103')] as TJAfasor ).afaaleur]);
	QrLabel54.Caption 	:= Format('%.2f EUR',[( JSzamla.Afasorok[JSzamla.GetAfaSorszam('104')] as TJAfasor ).afaaleur]);
	QrLabel84.Caption 	:= Format('%.2f EUR',[( JSzamla.Afasorok[JSzamla.GetAfaSorszam('105')] as TJAfasor ).afaaleur]);
	if ( JSzamla.GetAfaSorszam('101') > 0 ) then begin 	// Van 25 %-os �FA
		QrLabel49.Caption 	:= Format('%.2f EUR',[( JSzamla.Afasorok[JSzamla.GetAfaSorszam('101')] as TJAfasor ).afaerteur]);
		QrLabel52.Caption 	:= Format('%.2f EUR',[( JSzamla.Afasorok[JSzamla.GetAfaSorszam('101')] as TJAfasor ).afaaleur]);
	end;
	if ( JSzamla.GetAfaSorszam(JSzamla.afakod27) > 0 ) then begin 	// Van 25 %-os �FA
		QrLabel49.Caption 	:= Format('%.2f EUR',[( JSzamla.Afasorok[JSzamla.GetAfaSorszam(JSzamla.afakod27)] as TJAfasor ).afaerteur]);
		QrLabel52.Caption 	:= Format('%.2f EUR',[( JSzamla.Afasorok[JSzamla.GetAfaSorszam(JSzamla.afakod27)] as TJAfasor ).afaaleur]);
	end;
	egesz	:= Format('%.0f',[Int(StringSzam(copy(QrLabel50.Caption, 1, Pos(' ', QrLabel50.Caption)-1)))]);
	tized	:= IntToStr(Round(100 * StringSzam(copy(QrLabel50.Caption, 1, Pos(' ', QrLabel50.Caption)-1))) MOD 100);
	QrLabel48.Caption 	:= 'azaz ' + SzamText(egesz)+' Euro '+ SzamText(tized)+' eur�cent';
	QrLabel46.Caption 	:= Format('( 1 EUR = %.2f Ft => '+QrLabel50.Caption+' = %.0f Ft )',
		[arfoly, StringSzam(copy(QrLabel50.Caption, 1, Pos(' ', QrLabel50.Caption)-1)) * arfoly ]);
	QrLabel55.Caption 	:= Format('�fa : %.0f Ft',
		[StringSzam(copy(QrLabel75.Caption, 1, Pos(' ', QrLabel75.Caption)-1)) * arfoly ]);
	QrLabel4.Caption 	:= QueryFej.FieldByName('SA_MELL1').AsString;
	QrLabel5.Caption 	:= QueryFej.FieldByName('SA_MELL2').AsString;
	QrLabel59.Caption 	:= QueryFej.FieldByName('SA_MELL3').AsString;
	QrLabel60.Caption 	:= QueryFej.FieldByName('SA_MELL4').AsString;
	QrLabel65.Caption 	:= QueryFej.FieldByName('SA_MELL5').AsString;
	QrLabel87.Caption 	:= QueryFej.FieldByName('SA_MELL6').AsString;
	QrLabel88.Caption 	:= QueryFej.FieldByName('SA_MELL7').AsString;
end;

procedure TSZAMLA_DK_EURForm.RepBeforePrint(Sender: TCustomQuickRep;
	   var PrintReport: Boolean);
begin
  oldalszam	:= 1;
end;

end.
