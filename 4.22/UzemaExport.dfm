object UzemaExportDlg: TUzemaExportDlg
  Left = 0
  Top = 0
  Caption = #220'zemanyag statisztika export'
  ClientHeight = 117
  ClientWidth = 434
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Scaled = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 140
    Top = 26
    Width = 4
    Height = 13
    Caption = '-'
  end
  object Gauge1: TGauge
    Left = 32
    Top = 96
    Width = 393
    Height = 17
    Progress = 0
  end
  object ListaGrid: TStringGrid
    Left = 29
    Top = 119
    Width = 396
    Height = 186
    ColCount = 13
    FixedCols = 0
    FixedRows = 0
    TabOrder = 0
    Visible = False
  end
  object ebFrom: TMaskEdit
    Left = 29
    Top = 20
    Width = 105
    Height = 24
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    MaxLength = 11
    ParentFont = False
    TabOrder = 1
    Text = '2016.01.01.'
  end
  object ebTo: TMaskEdit
    Left = 150
    Top = 20
    Width = 105
    Height = 24
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    MaxLength = 11
    ParentFont = False
    TabOrder = 2
    Text = '2016.01.20.'
  end
  object Button1: TButton
    Left = 280
    Top = 20
    Width = 145
    Height = 25
    Caption = 'Export'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 3
    OnClick = Button1Click
  end
  object pnlInfo: TPanel
    Left = 32
    Top = 56
    Width = 393
    Height = 25
    BevelOuter = bvLowered
    TabOrder = 4
  end
  object Query3: TADOQuery
    Parameters = <>
    Left = 239
    Top = 175
  end
  object Query2: TADOQuery
    Tag = 1
    Parameters = <>
    Left = 191
    Top = 175
  end
  object Query5: TADOQuery
    Tag = 1
    Parameters = <>
    Left = 191
    Top = 231
  end
  object Query1: TADOQuery
    Tag = 1
    Parameters = <>
    Left = 143
    Top = 175
  end
  object QueryGk: TADOQuery
    Tag = 1
    Parameters = <>
    Left = 135
    Top = 231
  end
end
