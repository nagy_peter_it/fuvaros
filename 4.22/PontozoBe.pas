unit PontozoBe;

interface

uses
	SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
	Forms, Dialogs, StdCtrls, Buttons, Mask, DB, DBTables, Egyeb, J_SQL,
   VevoUjfm, ExtCtrls, Kozos, ADODB,J_Alform, CheckLst, Vcl.ComCtrls;

type
	TPontozoBeDlg = class(TJ_AlformDlg)
	BitElkuld: TBitBtn;
	BitKilep: TBitBtn;
	Label2: TLabel;
	Label12: TLabel;
    Label1: TLabel;
    Label4: TLabel;
    Query1: TADOQuery;
    Query2: TADOQuery;
    M0: TMaskEdit;
    CDolgozo: TComboBox;
    Label3: TLabel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    LV1: TListBox;
    LV2: TListBox;
	procedure BitKilepClick(Sender: TObject);
	procedure FormCreate(Sender: TObject);
	procedure Tolto(cim : string; teko:string); override;
	procedure BitElkuldClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormDestroy(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure CDolgozoChange(Sender: TObject);
    procedure Szamkiiro;
	private
       listacsop       : TStringLIst;
       listadolg       : TStringLIst;
	public
	end;

var
	PontozoBeDlg: TPontozoBeDlg;

implementation

uses
	Kozos_Local, Math;
{$R *.DFM}

procedure TPontozoBeDlg.BitKilepClick(Sender: TObject);
begin
	ret_kod := '';
   Close;
end;

procedure TPontozoBeDlg.CDolgozoChange(Sender: TObject);
begin
   Tolto(Caption, ret_kod);
end;

procedure TPontozoBeDlg.FormCreate(Sender: TObject);
var
	str	: string;
	sorsz : integer;
begin
	EgyebDlg.SetADOQueryDatabase(Query1);
	EgyebDlg.SetADOQueryDatabase(Query2);
	ret_kod 	:= '';
	listacsop	:= TStringList.Create;
	listadolg	:= TStringList.Create;
   Query_Run (Query2, 'SELECT SZ_ALKOD, SZ_MENEV FROM SZOTAR WHERE SZ_FOKOD = ''400'' ORDER BY SZ_MENEV ');
   while not Query2.Eof do begin
       listacsop.Add(Query2.FieldByName('SZ_ALKOD').AsString);
//       CL1.Items.Add(Query2.FieldByName('SZ_MENEV').AsString);
       Query2.Next;
   end;
   Query_Run (Query2, 'SELECT JE_FEKOD, JE_FENEV FROM JELSZO ORDER BY JE_FENEV');
   while not Query2.Eof do begin
       listadolg.Add(Query2.FieldByName('JE_FEKOD').AsString);
       CDolgozo.Items.Add(Query2.FieldByName('JE_FENEV').AsString);
       Query2.Next;
   end;
   CDolgozo.ItemIndex  := 0;
   // Az �sszes dolgoz� berak�sa az LV1 -be
   LV1.Clear;
(*
   // A nem csoporthoz tartoz� dolgoz�k nem kellenek
   Query_Run (Query2, 'SELECT * FROM DOLGOZO WHERE DO_KOD NOT IN (SELECT DC_DOKOD FROM DOLGOZOCSOP) AND DO_ARHIV < 1');
   while not Query2.Eof do begin
       LV1.Items.Add( ' ' + Query2.FieldByName('DO_NAME').AsString + ' ('+ Query2.FieldByName('DO_KOD').AsString +')');
       Query2.Next;
   end;
*)
   Query_Run (Query2, 'SELECT * FROM DOLGOZO, DOLGOZOCSOP WHERE DO_KOD= DC_DOKOD  AND DO_ARHIV < 1 AND DC_DATTOL = (SELECT MAX(DC_DATTOL) FROM DOLGOZOCSOP DCS2 WHERE DCS2.DC_DOKOD = DO_KOD )');
   while not Query2.Eof do begin
       LV1.Items.Add( Query2.FieldByName('DC_CSNEV').AsString + ' - ' + Query2.FieldByName('DO_NAME').AsString + ' ('+ Query2.FieldByName('DO_KOD').AsString +')');
       Query2.Next;
   end;
end;

procedure TPontozoBeDlg.Tolto(cim : string; teko:string);
var
   i       : integer;
   dokod   : string;
   fekod   : string;
begin
	ret_kod 		:= teko;
	Caption 		:= cim;
	M0.Text 		:= teko;
   if teko <> '' then begin
       if Query_Run (Query2, 'SELECT * FROM PONTOZO WHERE PO_POKOD = '+teko,true) then begin
           CDolgozo.ItemIndex 	:= Max(0, listadolg.IndexOf(Query2.FieldByName('PO_FEKOD').AsString));
       end;
   end;
   fekod   := listadolg[CDolgozo.ItemIndex];
   // El�sz�r mindenki jobbra
   for i := 0 to LV2.Count - 1 do begin
       LV1.Items.Add(LV2.Items[i]);
   end;
   LV2.SelectAll;
   LV2.DeleteSelected;
   // �thelyezz�k a hozz� tartoz� dolgoz�kat a m�sik t�bl�ba
   if Query_Run (Query2, 'SELECT PO_DOKOD FROM PONTOZO WHERE PO_FEKOD = '''+fekod+''' ',true) then begin
       i := 0;
       while i <= LV1.Count - 1 do begin
           dokod := copy(LV1.Items[i], Pos('(', LV1.Items[i])+1, 999 );
           dokod := copy(dokod, 1, Pos(')', dokod)-1);
           if Query2.Locate ('PO_DOKOD', dokod, []) then begin
               // �t kell helyezni a dolgoz�t
               LV2.Items.Add(LV1.Items[i]);
               LV1.Selected[i] := true;
               LV1.DeleteSelected;
           end else begin
               Inc(i);
           end;
       end;
   end;
   if teko <> '' then begin
       CDolgozo.Enabled    := false;
   end;
   SetMaskEdits([M0]);
   Szamkiiro;
end;

procedure TPontozoBeDlg.BitBtn1Click(Sender: TObject);
var
   i : integer;
begin
   // �ttessz�k a kijel�lt elemeket
   for i := 0 to LV1.Count - 1 do begin
       if LV1.Selected[i] then begin
           // �trak�s
           LV2.Items.Add(LV1.Items[i]);
       end;
   end;
   LV1.DeleteSelected;
   Szamkiiro;
end;

procedure TPontozoBeDlg.BitBtn2Click(Sender: TObject);
var
   i : integer;
begin
   // �ttessz�k a kijel�lt elemeket
   for i := 0 to LV2.Count - 1 do begin
       if LV2.Selected[i] then begin
           // �trak�s
           LV1.Items.Add(LV2.Items[i]);
       end;
   end;
   LV2.DeleteSelected;
   Szamkiiro;
end;

procedure TPontozoBeDlg.BitElkuldClick(Sender: TObject);
var
   i       : integer;
   ujkod   : integer;
   cskod   : string;
   dokod   : string;
begin
   Screen.Cursor   := crHourglass;
   Query_Run ( Query1, 'SELECT DCS1.DC_DOKOD, DCS1.DC_CSKOD FROM DOLGOZOCSOP DCS1 WHERE  DC_DATTOL = (SELECT MAX(DC_DATTOL) FROM DOLGOZOCSOP DCS2 WHERE DCS2.DC_DOKOD = DCS1.DC_DOKOD ) ORDER BY DCS1.DC_DOKOD');
   Query_Run ( Query2, 'DELETE FROM PONTOZO WHERE PO_FEKOD = '''+listadolg[CDolgozo.ItemIndex]+''' ',true);
   ujkod := GetNextStrCode('PONTOZO', 'PO_POKOD', 1, 5,True);
   for i := 0 to LV2.Count - 1 do begin
       dokod := copy(LV2.Items[i], Pos('(', LV2.Items[i])+1, 999 );
       dokod := copy(dokod, 1, Pos(')', dokod)-1);
       cskod   := '';
       // A dolgoz�i csoport meghat�roz�sa
       if Query1.Locate('DC_DOKOD', dokod, [])  then begin
           cskod   := Query1.FieldByName('DC_CSKOD').AsString
       end;
       ret_kod := IntToStr(ujkod+i);
       Query_Insert(Query2, 'PONTOZO', [
           'PO_POKOD', ret_kod,
           'PO_DOKOD', ''''+dokod+'''',
           'PO_FEKOD',	''''+listadolg[CDolgozo.ItemIndex]+'''',
           'PO_ALKOD',	''''+cskod+''''
       ]);
   end;
   Screen.Cursor   := crDefault;
   Close;
end;

procedure TPontozoBeDlg.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
	EgyebDlg.FormReturn(Key);
end;

procedure TPontozoBeDlg.FormDestroy(Sender: TObject);
begin
   listacsop.Destroy;
   listadolg.Destroy;
end;

procedure TPontozoBeDlg.Szamkiiro;
begin
   Label4.Caption  := 'Kijel�lt dolgoz�k : ('+IntToStr(LV2.Count)+')';
   Label4.Update;
   Label3.Caption  := 'Nem kijel�lt dolgoz�k : ('+IntToStr(LV1.Count)+')';
   Label3.Update;
end;

end.
