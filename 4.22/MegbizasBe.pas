﻿unit MegbizasBe;

interface

uses
	SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
	Forms, Dialogs, StdCtrls, Buttons, Mask, DB, DBTables,
  VevoUjfm, ExtCtrls, Kozos, ADODB, J_Alform, Grids, DBGrids,Shellapi, Clipbrd,
  QrPdfFilt, Menus, KozosTipusok, Vcl.ComCtrls ;

type
	TMegbizasbeDlg = class(TJ_AlformDlg)
	Label12: TLabel;
    Query1: TADOQuery;
    Query2: TADOQuery;
    GroupBox3: TGroupBox;
    DBGrid5: TDBGrid;
	 Panel12: TPanel;
    BitBtn6: TBitBtn;
    BitBtn31: TBitBtn;
    BitBtn30: TBitBtn;
    DSFelrako: TDataSource;
    QueryFelrako: TADOQuery;
    GroupBox5: TGroupBox;
	 M1: TMaskEdit;
    Label14: TLabel;
	 Querykoz1: TADOQuery;
    QueryFelrakoMS_MBKOD: TIntegerField;
    QueryFelrakoMS_SORSZ: TIntegerField;
    QueryFelrakoMS_FELNEV: TStringField;
    QueryFelrakoMS_FELTEL: TStringField;
    QueryFelrakoMS_FELCIM: TStringField;
    QueryFelrakoMS_FELDAT: TStringField;
    QueryFelrakoMS_FELIDO: TStringField;
    QueryFelrakoMS_FETDAT: TStringField;
    QueryFelrakoMS_FETIDO: TStringField;
    QueryFelrakoMS_LERNEV: TStringField;
    QueryFelrakoMS_LERTEL: TStringField;
    QueryFelrakoMS_LERCIM: TStringField;
    QueryFelrakoMS_LERDAT: TStringField;
    QueryFelrakoMS_LERIDO: TStringField;
    QueryFelrakoMS_LETDAT: TStringField;
    QueryFelrakoMS_LETIDO: TStringField;
    QueryFelrakoMS_FELARU: TStringField;
    QueryFelrakoMS_FSULY: TBCDField;
    QueryFelrakoMS_FEPAL: TBCDField;
    QueryFelrakoMS_LSULY: TBCDField;
    QueryFelrakoMS_LEPAL: TBCDField;
    V: TPanel;
    BitElkuld: TBitBtn;
	 BitKilep: TBitBtn;
    Label3: TLabel;
    M7: TMaskEdit;
    Label17: TLabel;
	 CBStatusz: TComboBox;
    RadioButton1: TRadioButton;
    RadioButton2: TRadioButton;
	 BitBtn21: TBitBtn;
    QueryFelrakoMS_FELIR: TStringField;
    QueryFelrakoMS_LELIR: TStringField;
    QueryFelrakoMS_ORSZA: TStringField;
	 CheckBox1: TCheckBox;
    BitBtn2: TBitBtn;
    QueryFelrakoMS_FORSZ: TStringField;
    BitBtn10: TBitBtn;
    BitBtn15: TBitBtn;
    QueryFelrakoMS_FAJKO: TIntegerField;
    QueryFelrakoMS_FAJTA: TStringField;
    MaskEdit4: TMaskEdit;
    CHB3: TCheckBox;
	 Label15: TLabel;
    Label16: TLabel;
    MaskEdit5: TMaskEdit;
    MaskEdit6: TMaskEdit;
    BitBtn16: TBitBtn;
    BitBtn17: TBitBtn;
    MaskEdit7: TMaskEdit;
    MaskEdit8: TMaskEdit;
    QueryFt: TADOQuery;
    CheckBox2: TCheckBox;
    QueryFelrakoMS_RENSZ: TStringField;
    CheckBox3: TCheckBox;
    CheckBox4: TCheckBox;
    SpeedButton4: TSpeedButton;
    SpeedButton5: TSpeedButton;
    CheckBox6: TCheckBox;
    QueryFelrakoMS_EXSTR: TStringField;
    Edit1: TEdit;
    BitBtn29: TBitBtn;
    SQLCommand: TADOCommand;
    QueryFelrakoMS_FELIDO2: TStringField;
    QueryFelrakoMS_LERIDO2: TStringField;
    QueryFelrakoMS_FERKIDO: TStringField;
    QueryFelrakoMS_LERKIDO: TStringField;
    CheckBox7: TCheckBox;
    CheckBox8: TCheckBox;
    ComboBox1: TComboBox;
    Label19: TLabel;
    QueryFelrakoMS_LERDAT2: TStringField;
    QueryFelrakoMS_FELDAT2: TStringField;
    QueryFelrakoMS_CSPAL: TIntegerField;
    Query3: TADOQuery;
    Query3SZ_FOKOD: TStringField;
    Query3SZ_ALKOD: TStringField;
    Query3SZ_MENEV: TStringField;
    Query3SZ_LEIRO: TStringField;
    Query3SZ_KODHO: TBCDField;
    Query3SZ_NEVHO: TBCDField;
    Query3SZ_ERVNY: TIntegerField;
    Query4: TADOQuery;
    Query4MAX: TStringField;
    QueryFelrakoMS_FPAFEL: TIntegerField;
    QueryFelrakoMS_LPAFEL: TIntegerField;
    CheckBox9: TCheckBox;
    ComboBox2: TComboBox;
    QueryS: TADOQuery;
    QuerySSB_MBKOD: TIntegerField;
    QuerySSB_MIKOR: TIntegerField;
    QuerySSB_MIKO2: TIntegerField;
    QuerySSB_MEGJE: TStringField;
    MEGSEGED2: TADOQuery;
    Query11: TADOQuery;
    QueryFelrakoMS_AZTIP: TStringField;
    Query12: TADOQuery;
    PopupMenu1: TPopupMenu;
    Sablontrlse1: TMenuItem;
    BitBtn101: TBitBtn;
    BitBtn102: TBitBtn;
    Label21: TLabel;
    MEGSEGED2MS_MBKOD: TIntegerField;
    MEGSEGED2MS_SORSZ: TIntegerField;
    MEGSEGED2MS_FELNEV: TStringField;
    MEGSEGED2MS_FELTEL: TStringField;
    MEGSEGED2MS_FELCIM: TStringField;
    MEGSEGED2MS_FELDAT: TStringField;
    MEGSEGED2MS_FELIDO: TStringField;
    MEGSEGED2MS_FETDAT: TStringField;
    MEGSEGED2MS_FETIDO: TStringField;
    MEGSEGED2MS_LERNEV: TStringField;
    MEGSEGED2MS_LERTEL: TStringField;
    MEGSEGED2MS_LERCIM: TStringField;
    MEGSEGED2MS_LERDAT: TStringField;
    MEGSEGED2MS_LERIDO: TStringField;
    MEGSEGED2MS_LETDAT: TStringField;
    MEGSEGED2MS_LETIDO: TStringField;
    MEGSEGED2MS_FELARU: TStringField;
    MEGSEGED2MS_FSULY: TBCDField;
    MEGSEGED2MS_FEPAL: TBCDField;
    MEGSEGED2MS_LSULY: TBCDField;
    MEGSEGED2MS_LEPAL: TBCDField;
    MEGSEGED2MS_FELSE1: TStringField;
    MEGSEGED2MS_FELSE2: TStringField;
    MEGSEGED2MS_FELSE3: TStringField;
    MEGSEGED2MS_FELSE4: TStringField;
    MEGSEGED2MS_FELSE5: TStringField;
    MEGSEGED2MS_LERSE1: TStringField;
    MEGSEGED2MS_LERSE2: TStringField;
    MEGSEGED2MS_LERSE3: TStringField;
    MEGSEGED2MS_LERSE4: TStringField;
    MEGSEGED2MS_LERSE5: TStringField;
    MEGSEGED2MS_ORSZA: TStringField;
    MEGSEGED2MS_FORSZ: TStringField;
    MEGSEGED2MS_TIPUS: TIntegerField;
    MEGSEGED2MS_FELIR: TStringField;
    MEGSEGED2MS_LELIR: TStringField;
    MEGSEGED2MS_DATUM: TStringField;
    MEGSEGED2MS_IDOPT: TStringField;
    MEGSEGED2MS_MSKOD: TIntegerField;
    MEGSEGED2MS_TINEV: TStringField;
    MEGSEGED2MS_STATK: TStringField;
    MEGSEGED2MS_STATN: TStringField;
    MEGSEGED2MS_TEIDO: TStringField;
    MEGSEGED2MS_TENNEV: TStringField;
    MEGSEGED2MS_TENTEL: TStringField;
    MEGSEGED2MS_TENCIM: TStringField;
    MEGSEGED2MS_TENYL1: TStringField;
    MEGSEGED2MS_TENYL2: TStringField;
    MEGSEGED2MS_TENYL3: TStringField;
    MEGSEGED2MS_TENYL4: TStringField;
    MEGSEGED2MS_TENYL5: TStringField;
    MEGSEGED2MS_TENOR: TStringField;
    MEGSEGED2MS_TENIR: TStringField;
    MEGSEGED2MS_AKTNEV: TStringField;
    MEGSEGED2MS_AKTTEL: TStringField;
    MEGSEGED2MS_AKTCIM: TStringField;
    MEGSEGED2MS_AKTOR: TStringField;
    MEGSEGED2MS_AKTIR: TStringField;
    MEGSEGED2MS_EXSTR: TStringField;
    MEGSEGED2MS_EXPOR: TIntegerField;
    MEGSEGED2MS_TEFNEV: TStringField;
    MEGSEGED2MS_TEFTEL: TStringField;
    MEGSEGED2MS_TEFCIM: TStringField;
    MEGSEGED2MS_TEFL1: TStringField;
    MEGSEGED2MS_TEFL2: TStringField;
    MEGSEGED2MS_TEFL3: TStringField;
    MEGSEGED2MS_TEFL4: TStringField;
    MEGSEGED2MS_TEFL5: TStringField;
    MEGSEGED2MS_TEFOR: TStringField;
    MEGSEGED2MS_TEFIR: TStringField;
    MEGSEGED2MS_REGIK: TIntegerField;
    MEGSEGED2MS_FAJKO: TIntegerField;
    MEGSEGED2MS_FAJTA: TStringField;
    MEGSEGED2MS_RENSZ: TStringField;
    MEGSEGED2MS_POTSZ: TStringField;
    MEGSEGED2MS_DOKOD: TStringField;
    MEGSEGED2MS_DOKO2: TStringField;
    MEGSEGED2MS_GKKAT: TStringField;
    MEGSEGED2MS_SNEV1: TStringField;
    MEGSEGED2MS_SNEV2: TStringField;
    MEGSEGED2MS_KIFON: TIntegerField;
    MEGSEGED2MS_FETED: TStringField;
    MEGSEGED2MS_FETEI: TStringField;
    MEGSEGED2MS_LETED: TStringField;
    MEGSEGED2MS_LETEI: TStringField;
    MEGSEGED2MS_ID: TIntegerField;
    MEGSEGED2MS_NORID: TIntegerField;
    MEGSEGED2MS_FERKDAT: TStringField;
    MEGSEGED2MS_LERKDAT: TStringField;
    MEGSEGED2MS_FERKIDO: TStringField;
    MEGSEGED2MS_LERKIDO: TStringField;
    MEGSEGED2MS_FETEI2: TStringField;
    MEGSEGED2MS_LETEI2: TStringField;
    MEGSEGED2MS_FELIDO2: TStringField;
    MEGSEGED2MS_LERIDO2: TStringField;
    MEGSEGED2MS_LOADID: TStringField;
    MEGSEGED2MS_M_MAIL: TDateTimeField;
    MEGSEGED2MS_S_MAIL: TDateTimeField;
    MEGSEGED2MS_FETED2: TStringField;
    MEGSEGED2MS_LETED2: TStringField;
    MEGSEGED2MS_FELDAT2: TStringField;
    MEGSEGED2MS_LERDAT2: TStringField;
    MEGSEGED2MS_FPAFEL: TIntegerField;
    MEGSEGED2MS_FPALER: TIntegerField;
    MEGSEGED2MS_FPAIGAZT: TIntegerField;
    MEGSEGED2MS_FPAIGAZL: TStringField;
    MEGSEGED2MS_FPAMEGJ: TStringField;
    MEGSEGED2MS_LPAFEL: TIntegerField;
    MEGSEGED2MS_LPALER: TIntegerField;
    MEGSEGED2MS_LPAIGAZT: TIntegerField;
    MEGSEGED2MS_LPAIGAZL: TStringField;
    MEGSEGED2MS_LPAMEGJ: TStringField;
    MEGSEGED2MS_ID2: TIntegerField;
    MEGSEGED2MS_CSPAL: TIntegerField;
    MEGSEGED2MS_JAKOD: TStringField;
    MEGSEGED2MS_JASZA: TStringField;
    MEGSEGED2MS_AZTIP: TStringField;
    MEGSEGED2MS_HFOK1: TStringField;
    MEGSEGED2MS_HFOK2: TStringField;
    MEGSEGED2MS_FCPAL: TBCDField;
    MEGSEGED2MS_LCPAL: TBCDField;
    MEGSEGED2MS_TAVKM: TBCDField;
    MEGSEGED2MS_DARU: TIntegerField;
    CheckBox10: TCheckBox;
    QueryFelrakoMS_FELSE1: TStringField;
    QueryFelrakoMS_FELSE2: TStringField;
    QueryFelrakoMS_FELSE3: TStringField;
    QueryFelrakoMS_FELSE4: TStringField;
    QueryFelrakoMS_FELSE5: TStringField;
    QueryFelrakoMS_LERSE1: TStringField;
    QueryFelrakoMS_LERSE2: TStringField;
    QueryFelrakoMS_LERSE3: TStringField;
    QueryFelrakoMS_LERSE4: TStringField;
    QueryFelrakoMS_LERSE5: TStringField;
    pnlTeljesFuvardij: TPanel;
    Label22: TLabel;
    Label23: TLabel;
    Label24: TLabel;
    pnlEURKm: TPanel;
    pnlEURKmAtallas: TPanel;
    Label25: TLabel;
    Label26: TLabel;
    pnlBecsultKm: TPanel;
    Label27: TLabel;
    pnlSzamitas: TPanel;
    QueryFelrakoMS_EKAER: TStringField;
    meAtallasKm: TMaskEdit;
    QueryLocal: TADOQuery;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    TabSheet3: TTabSheet;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label6: TLabel;
    Label5: TLabel;
    Label18: TLabel;
    Label20: TLabel;
    M11: TMaskEdit;
    M12: TMaskEdit;
    BitBtn1: TBitBtn;
    BitBtn32: TBitBtn;
    M14: TMaskEdit;
    CValuta: TComboBox;
    M13: TMaskEdit;
    M15: TMaskEdit;
    CheckBox5: TCheckBox;
    BitBtn22: TBitBtn;
    M120: TMaskEdit;
    CB_BORD: TComboBox;
    E_BORD: TEdit;
    E_BORD_PLUS: TEdit;
    CheckBox51: TCheckBox;
    M130: TMaskEdit;
    CVnem: TComboBox;
    BitBtn23: TBitBtn;
    GroupBox2: TGroupBox;
    Label4: TLabel;
    Label8: TLabel;
    M21: TMaskEdit;
    M22: TMaskEdit;
    BitBtn4: TBitBtn;
    BitBtn5: TBitBtn;
    CValuta2: TComboBox;
    M23: TMaskEdit;
    M25: TMaskEdit;
    M26: TMaskEdit;
    M27: TMaskEdit;
    GroupBox4: TGroupBox;
    Label2: TLabel;
    Label7: TLabel;
    Label9: TLabel;
    Label11: TLabel;
    Label10: TLabel;
    Label13: TLabel;
    M31: TMaskEdit;
    BitBtn3: TBitBtn;
    BitBtn7: TBitBtn;
    M32: TMaskEdit;
    BitBtn8: TBitBtn;
    BitBtn9: TBitBtn;
    M33: TMaskEdit;
    M34: TMaskEdit;
    BitBtn11: TBitBtn;
    BitBtn12: TBitBtn;
    M35: TMaskEdit;
    M36: TMaskEdit;
    BitBtn13: TBitBtn;
    BitBtn14: TBitBtn;
    CBGkat: TComboBox;
    MaskEdit1: TMaskEdit;
    MaskEdit2: TMaskEdit;
    MaskEdit3: TMaskEdit;
    BitBtn18: TBitBtn;
    BitBtn19: TBitBtn;
    BitBtn20: TBitBtn;
    Button1: TButton;
    Button2: TButton;
    Label28: TLabel;
    M16: TMaskEdit;
    BitBtn64: TBitBtn;
    CheckBox11: TCheckBox;
    QueryFelrakoMS_TEFNEV: TStringField;
    QueryFelrakoMS_TEFOR: TStringField;
    QueryFelrakoMS_TEFIR: TStringField;
    QueryFelrakoMS_TEFTEL: TStringField;
    QueryFelrakoMS_TEFCIM: TStringField;
    QueryFelrakoMS_TEFL1: TStringField;
    QueryFelrakoMS_TEFL2: TStringField;
    QueryFelrakoMS_TEFL3: TStringField;
    QueryFelrakoMS_TEFL4: TStringField;
    QueryFelrakoMS_TEFL5: TStringField;
    QueryFelrakoMS_TENNEV: TStringField;
    QueryFelrakoMS_TENOR: TStringField;
    QueryFelrakoMS_TENIR: TStringField;
    QueryFelrakoMS_TENTEL: TStringField;
    QueryFelrakoMS_TENCIM: TStringField;
    QueryFelrakoMS_TENYL1: TStringField;
    QueryFelrakoMS_TENYL2: TStringField;
    QueryFelrakoMS_TENYL3: TStringField;
    QueryFelrakoMS_TENYL4: TStringField;
    QueryFelrakoMS_TENYL5: TStringField;
	procedure BitKilepClick(Sender: TObject);
	procedure FormCreate(Sender: TObject);
	procedure Tolto(cim : string; teko:string);override;
	procedure Tolto3(cim : string; teko:string);
	procedure BitElkuldClick(Sender: TObject);
  procedure MaskEditClick(Sender: TObject);
    procedure Modosit(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure BitBtn1Click(Sender: TObject);
    procedure MaskKeyPress(Sender: TObject; var Key: Char);
    procedure M2Exit(Sender: TObject);
    procedure M3Exit(Sender: TObject);
	 procedure Button1Click(Sender: TObject);
	 procedure BitBtn3Click(Sender: TObject);
	 procedure BitBtn30Click(Sender: TObject);
	 procedure BitBtn32Click(Sender: TObject);
    procedure BitBtn5Click(Sender: TObject);
    procedure BitBtn4Click(Sender: TObject);
    procedure GombEllenor;
    procedure BitBtn31Click(Sender: TObject);
    procedure BitBtn11Click(Sender: TObject);
	 procedure BitBtn8Click(Sender: TObject);
    procedure BitBtn9Click(Sender: TObject);
    procedure BitBtn7Click(Sender: TObject);
    procedure BitBtn13Click(Sender: TObject);
    procedure BitBtn12Click(Sender: TObject);
    procedure BitBtn14Click(Sender: TObject);
	 function  Elkuldes : boolean;
    procedure BitBtn6Click(Sender: TObject);
	 procedure FormCanResize(Sender: TObject; var NewWidth,
      NewHeight: Integer; var Resize: Boolean);
    procedure FormDestroy(Sender: TObject);
    procedure BitBtn21Click(Sender: TObject);
	 procedure Felrakonyitas;
   procedure GKKATEG_valtozott(Ujkateg: string);
   procedure UtvonalSzamitas;
   procedure TeljesDijSzamitas;
   procedure EURKm_Szinezes;
   function RakodasIdoKiiro(Datum1, Datum2, Ido1, Ido2: string): string;
    procedure BitBtn2Click(Sender: TObject);
	 procedure CheckBox1Click(Sender: TObject);
	 procedure GepkocsiOlvaso;
   procedure AlGepkocsiOlvaso;
	 procedure M31Exit(Sender: TObject);
	 procedure Button2Click(Sender: TObject);
	 procedure BitBtn10Click(Sender: TObject);
	 procedure BitBtn15Click(Sender: TObject);
	 procedure DSFelrakoDataChange(Sender: TObject; Field: TField);
	 procedure CHB3Click(Sender: TObject);
	 procedure MaskEdit4Exit(Sender: TObject);
	 procedure MaskEdit4KeyPress(Sender: TObject; var Key: Char);
	 procedure BitBtn17Click(Sender: TObject);
	 procedure BitBtn16Click(Sender: TObject);
	 procedure PozicioIro;
    procedure BitBtn18Click(Sender: TObject);
    procedure CheckBox5Click(Sender: TObject);
    procedure DBGrid5DblClick(Sender: TObject);
    procedure BitBtn19Click(Sender: TObject);
    procedure BitBtn20Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure SpeedButton5Click(Sender: TObject);
    procedure DBGrid5DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure BitBtn22Click(Sender: TObject);
    procedure BitBtn29Click(Sender: TObject);
    procedure M13Enter(Sender: TObject);
    procedure M13Exit(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure QueryFelrakoAfterScroll(DataSet: TDataSet);
    procedure ComboBox2Change(Sender: TObject);
    procedure Sablontrlse1Click(Sender: TObject);
    procedure PopupMenu1Popup(Sender: TObject);
    procedure BitBtn102Click(Sender: TObject);
    procedure BitBtn101Click(Sender: TObject);
    procedure CValutaChange(Sender: TObject);
    procedure CVnemChange(Sender: TObject);
    procedure CValuta2Change(Sender: TObject);
    procedure BitBtn23Click(Sender: TObject);
    procedure SendFeketeVevo;
    procedure pnlSzamitasClick(Sender: TObject);
    procedure meAtallasKmExit(Sender: TObject);
    procedure M130Exit(Sender: TObject);
    procedure M130Enter(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure CheckBox6Click(Sender: TObject);
    procedure ModosultAdatokViszonyba(MBKOD: string);
    function VedettMezoValtozas(RegiErtek, UjErtek: string; var KellSzamolas: boolean): string;
    procedure SetActiveTab(i: integer);
    procedure BitBtn64Click(Sender: TObject);
    procedure M21Change(Sender: TObject);
	private
		statusszin	: array [1..10, 1..2] of string;
		modosult 	: boolean;
		listavnem	: TStringList;
		listastatu	: TStringList;
 		felelos_fekodlist: TStringList;
		sablon		: string;
		listagkat	: TStringList;
    kellpoz: boolean;
    FGC : TGridCoord;
    FSource: variant;
    efuvardij, esedij, elozo_megbizo_kod: string;
    PDFFILE:string;
    MBKOD: Tstringlist;
    RegiRendszam, RegiPotszam: string;
    jakod: string;
    KategNormaAlso, KategNormaFelso: double;  // EUR/km
    UresKMSzazalek: integer;  // %, GK kategóriánként
    function PDFMentes : boolean;
    function SablonMentes(embkod,mbkod, snev: string) : Boolean;
    procedure UpdatePozicio(mbkod, pozicio: string);
    procedure RendszamValtozott;
	public
		voltenter	: boolean;
		KELL_LAN_ID, ELLENORZOTTJAR, BORDNO	: boolean;
    HutosLehet: boolean;
	end;

var
	MegbizasbeDlg: TMegbizasbeDlg;

implementation

uses
	Egyeb, J_Valaszto, Segedbe2, AlvallalUjFm, LevelBe, FuvarTablaFm, Kozos_Local,
  GKErvenyesseg, Windows, Fuviszli, StrUtils, MegbizasUjFm, Math, J_SQL, Info,
  Uzenetszerkeszto;

{$R *.DFM}

procedure TMegbizasbeDlg.BitKilepClick(Sender: TObject);
begin
   voltenter	:= false;
	if modosult then begin
		if NoticeKi('Módosultak az adatok. Ki akar lépni mentés nélkül?', NOT_QUESTION) = 0 then begin
			ret_kod := '';
			Close;
     	end;
  	end else begin
  		ret_kod := '';
		Close;
  	end;
end;

procedure TMegbizasbeDlg.FormCreate(Sender: TObject);
var
	str		: string;
	sorsz,i 	: integer;
begin
  // HutosLehet:= (copy(EgyebDlg.v_alap_db,1,3)='DAT')or(copy(EgyebDlg.v_alap_db,1,13)='FUVARTESZTDAT') ;
  if EgyebDlg.Read_SZGRID('999','926') <> 'y' then  // ha nem találja, akkor ''-et ad vissza (DK-nál)
     HutosLehet:=False
  else HutosLehet:=True;

	// A státusszínek feltöltése
	for i := 1 to 10 do begin
		statusszin[i, 1] := '';
		statusszin[i, 2] := IntToStr(clAqua);
	end;
	Query_Run(EgyebDlg.QueryKozos, 'SELECT SZ_ALKOD, SZ_KODHO FROM SZOTAR WHERE SZ_FOKOD = ''960'' ' );
	i := 1;
	while ( ( i <= 10 ) and ( not EgyebDlg.QueryKozos.Eof ) ) do begin
		statusszin[i, 1] := EgyebDlg.QueryKozos.FieldByName('SZ_ALKOD').AsString;
		statusszin[i, 2] := EgyebDlg.QueryKozos.FieldByName('SZ_KODHO').AsString;
		Inc(i);
		EgyebDlg.QueryKozos.Next;
	end;

  ComboBox1.Items.Clear;
  felelos_fekodlist:= TStringList.Create;
	Query_Run(EgyebDlg.QueryKozos, 'SELECT SZ_MENEV, SZ_EGYEB1 FROM SZOTAR WHERE SZ_FOKOD ='''+ '330'''+' and LEN(sz_alkod)=3 and SZ_ERVNY=1 order by sz_menev' );
	while  not EgyebDlg.QueryKozos.Eof do
  begin
    ComboBox1.Items.Add(EgyebDlg.QueryKozos.FieldByName('SZ_MENEV').AsString);
    felelos_fekodlist.Add(EgyebDlg.QueryKozos.FieldByName('SZ_EGYEB1').AsString); // felhasználó kód
		EgyebDlg.QueryKozos.Next;
	end;

  for i := 0 to ComboBox1.Items.Count - 1 do begin
			if felelos_fekodlist[i] = EgyebDlg.user_code then begin  // kód alapján szeretjük
				ComboBox1.ItemIndex	:= i;
			  end;  // if
		  end;  // for

  //  Application.CreateForm(TFGKErvenyesseg, FGKErvenyesseg);
//  if not MegbizasbeDlg.Visible then  //
//    Application.CreateForm(TFGKErvenyesseg, FGKErvenyesseg);
	BitBtn18.Visible 	:= EgyebDlg.NavC_Kapcs_OK;
	voltenter	:= false;
	EgyebDlg.SetADOQueryDatabase(Query1);
	EgyebDlg.SetADOQueryDatabase(QueryFt);
	EgyebDlg.SetADOQueryDatabase(Querykoz1);
	EgyebDlg.SetADOQueryDatabase(Query2);
 	EgyebDLg.SetADOQueryDatabase(QueryFelrako);
	SetMaskEdits([M1, MaskEdit5, MaskEdit6, MaskEdit7, M11, M12, M15, M21, M22, M25, M26, M27, M33, M34, M35, M36, MaskEdit1, MaskEdit2, MaskEdit3, M120]);
	ret_kod 	:= '';
	KELL_LAN_ID	:= false;
	listavnem	:= TStringList.Create;
	SzotarTolt(CValuta,  '110' , listavnem, false );
	SzotarTolt(CVnem,    '110' , listavnem, false );
	SzotarTolt(CValuta2, '110' , listavnem, false );
	listagkat	:= TStringList.Create;
	// SzotarTolt(CBGkat, '340' , listagkat, false, true );
	SzotarTolt(CBGkat, '340' , listagkat, false, false );
	for sorsz := 0 to listagkat.Count - 1 do begin
		CBGkat.Items[sorsz]	:= listagkat[sorsz];
	end;
	// CBGkat.ItemIndex	:= 0;
  CBGkat.ItemIndex	:= -1;
	{Alapértelmezett valutanem beállítása}
	str	:= EgyebDlg.Read_SZGrid('999','661');
	ComboSet (CValuta,  str, listavnem);
	ComboSet (CVnem,    str, listavnem);
	ComboSet (CValuta2, str, listavnem);
	listastatu			:= TStringList.Create;
	SzotarTolt(CBStatusz, '950' , listastatu, false );
	CHB3Click(Sender);
	// A fuvartábla adatok eltüntetése
	if EgyebDlg.VANLANID < 1 then begin
		MaskEdit5.Hide;
		MaskEdit6.Hide;
		MaskEdit7.Hide;
		BitBtn16.Hide;
		BitBtn17.Hide;
		Label16.Hide;
	end;
  ////////////////////
  QueryS.Open;
  QueryS.First;
  ComboBox2.Items.Clear;
  MBKOD:=TStringList.Create;
  if QueryS.Eof then
   ComboBox2.Items.Add('*Nincs sablon!*')
  else
   ComboBox2.Items.Add('*Válassz sablont!*')  ;
  while not QueryS.Eof do
  begin
   //ComboBox2.Items.Add(QuerySSB_MEGJE.Value+' ('+QuerySSB_MBKOD.AsString+')');
   ComboBox2.Items.Add(QuerySSB_MEGJE.Value);
   MBKOD.Add(QuerySSB_MBKOD.AsString);
   QueryS.Next;
  end;
  QueryS.Close;

  RegiRendszam:= '';
  RegiPotszam:= '';

  ////////////////////
//  CheckBox1.Enabled:=(EgyebDlg.GetRightTag(571) <> RG_NORIGHT ) or EgyebDlg.user_super;

	modosult 	:= false;
end;

procedure TMegbizasbeDlg.SetActiveTab(i: integer);
begin
  case i of
  1:  PageControl1.ActivePage:= TabSheet1;
  2:  PageControl1.ActivePage:= TabSheet2;
  3:  PageControl1.ActivePage:= TabSheet3;
  end;  // case
end;

procedure TMegbizasbeDlg.Tolto(cim : string; teko:string);
var
  sorsz, JaratStatusz: integer;
  //jasza:string;
begin
	ret_kod 	:= teko;
	Caption    	:= cim;
	M1.Text		:= teko;
	CheckBox1.Checked	:= false;
	CHB3.Checked		:= false;
	MaskEdit4.Text		:= '';
  ComboBox1.ItemIndex:=ComboBox1.Items.IndexOf(EgyebDlg.user_name)   ;
  SetActiveTab(EgyebDlg.v_megbizas_default_ful);

{  ////////////////////
  QueryS.Open;
  QueryS.First;
  ComboBox2.Items.Clear;
  MBKOD:=TStringList.Create;
  if QueryS.Eof then
   ComboBox2.Items.Add('*Nincs sablon!*')
  else
   ComboBox2.Items.Add('*Válassz sablont!*')  ;
  while not QueryS.Eof do
  begin
   if ret_kod='' then
     ComboBox2.Items.Add(QuerySSB_MEGJE.Value+' ('+QuerySSB_MBKOD.AsString+')')
   else
     ComboBox2.Items.Add(QuerySSB_MEGJE.Value);
   MBKOD.Add(QuerySSB_MBKOD.AsString);
   QueryS.Next;
  end;
  QueryS.Close;
  ////////////////////
 }

	if ret_kod <> '' then begin		{Módosítás}
    ComboBox2.items[0]:='*Mentéshez válassz nevet, vagy írj be újat*';
    ComboBox2.ItemIndex:=0;
    //ComboBox2.Text:='*Mentéshez válassz nevet, vagy írj be újat*';
		// Az alapadatok beolvasása
		Query_Run(Query1, 'SELECT * FROM MEGBIZAS WHERE MB_MBKOD = '+IntToStr(StrToIntDef(M1.Text,0)) ,true);
    jakod:= Query1.FieldByName('MB_JAKOD').AsString;
		if Query1.RecordCount > 0 then begin
      JaratStatusz:= EllenorzottJarat( Query1.FieldByName('MB_JAKOD').AsString);  // -1: Elszámolt, 0: Normál  1: ellenőrzött 2: sztornózott  -2 feloldott
      ELLENORZOTTJAR:=(Query1.FieldByName('MB_JAKOD').AsString<>'')
          and (JaratStatusz = 1) // csak ha "ellenőrzött". Sztornózott járatnál lehet módosítani
          and (not EgyebDlg.user_super) and ((GetRightTag(598, False) = RG_NORIGHT));  // nem megbízás admin
      Panel12.Enabled:=not ELLENORZOTTJAR;
      BitElkuld.Enabled:=not ELLENORZOTTJAR;
			M11.Text	:= Query1.FieldByName('MB_VEKOD').AsString;

      BORDNO:= Query1.FieldByName('MB_BORDNO1').AsString<>'';
      Query3.Close;
      Query3.Parameters[0].Value:=M11.Text;
      Query3.Open;
      Query3.First;
      CB_BORD.Items.Clear;
      while not Query3.Eof do
      begin
        CB_BORD.Items.Add(Query3SZ_ALKOD.Value);
        Query3.Next;
      end;
      Query3.Close;
      if CB_BORD.Items.Count>0 then
        BORDNO:=True;
			CB_BORD.Text:= Query1.FieldByName('MB_BORDNO1').AsString;
			E_BORD.Text:= Query1.FieldByName('MB_BORDNO2').AsString;
			E_BORD_PLUS.Text:= Query1.FieldByName('MB_BORDNO3').AsString;

			// A vevő adatainak beolvasása
			Query_Run(Querykoz1,'SELECT * FROM VEVO WHERE V_KOD = '''+M11.Text+''' ',true);
			if QueryKoz1.RecordCount > 0 then begin
				M12.Text	:= QueryKoz1.FieldByName('V_NEV').AsString;
				M120.Text	:= QueryKoz1.FieldByName('V_FAX').AsString;
				M15.Text	:= QueryKoz1.FieldByName('V_IRSZ').AsString + ' '+QueryKoz1.FieldByName('V_VAROS').AsString + ', '+
                   QueryKoz1.FieldByName('V_CIM').AsString;
        kellpoz   := QueryKoz1.FieldByName('VE_PKELL').AsInteger>0;
        if BORDNO then
          kellpoz:=False;
        //CheckBox5.Visible:=kellpoz;
			end;
      CheckBox5.Visible:=kellpoz;
      CheckBox51.Visible:=BORDNO;
      M14.Visible:=not BORDNO;
      CB_BORD.Visible:=BORDNO;
      E_BORD.Visible:=BORDNO;
      E_BORD_PLUS.Visible:=BORDNO;

      ComboBox1.ItemIndex:=ComboBox1.Items.IndexOf(Query1.FieldByName('MB_FENEV').AsString)   ;
			M14.Text	:= Query1.FieldByName('MB_POZIC').AsString;
 			M16.Text	:= Query1.FieldByName('MB_INTREF').AsString;
			M21.Text	:= Query1.FieldByName('MB_ALVAL').AsString;
			M22.Text	:= Query1.FieldByName('MB_ALNEV').AsString;
			CheckBox2.Checked	:= StrToIntDef(Query1.FieldByName('MB_KISCS').AsString, 0) > 0 ;
			CheckBox3.Checked	:= StrToIntDef(Query1.FieldByName('MB_VAM').AsString, 0) > 0 ;
			CheckBox4.Checked	:= StrToIntDef(Query1.FieldByName('MB_ADR').AsString, 0) > 0 ;
			CheckBox5.Checked	:= StrToIntDef(Query1.FieldByName('MB_NOPOZ').AsString, 0) > 0 ;
			CheckBox51.Checked	:= not (Query1.FieldByName('MB_BORDNO1').AsString <> EmptyStr ) ;
			CheckBox6.Checked	:= StrToIntDef(Query1.FieldByName('MB_EDIJ').AsString, 0) > 0 ;
			CheckBox7.Checked	:= StrToIntDef(Query1.FieldByName('MB_HUTOS').AsString, 0) > 0 ;
			CheckBox8.Checked	:= StrToIntDef(Query1.FieldByName('MB_FELFUG').AsString, 0) > 0 ;
			CheckBox9.Checked	:= StrToIntDef(Query1.FieldByName('MB_NUZPOT').AsString, 0) > 0 ;
			CheckBox10.Checked	:= StrToIntDef(Query1.FieldByName('MB_KOBOS').AsString, 0) > 0 ;
			CheckBox11.Checked	:= StrToIntDef(Query1.FieldByName('MB_NOCSOP').AsString, 0) > 0 ;

			// A vevő adatainak beolvasása
			M25.Text	:= '';
			M26.Text	:= '';
			M27.Text	:= '';
			if M21.Text <> '' then begin
        if EgyebDlg.v_evszam<'2013' then
				  Query_Run(Querykoz1,'SELECT * FROM ALVALLAL WHERE V_KOD = '''+M21.Text+''' ',true)
        else
				  Query_Run(Querykoz1,'SELECT * FROM VEVO WHERE V_KOD = '''+M21.Text+''' ',true);
				if QueryKoz1.RecordCount > 0 then begin
					M25.Text	:= QueryKoz1.FieldByName('V_IRSZ').AsString;
					M26.Text	:= QueryKoz1.FieldByName('V_VAROS').AsString;
					M27.Text	:= QueryKoz1.FieldByName('V_CIM').AsString;
				end;
			end;
			M31.Text	:= Query1.FieldByName('MB_RENSZ').AsString;
			M32.Text	:= Query1.FieldByName('MB_POTSZ').AsString;
       	M33.Text	:= Query1.FieldByName('MB_DOKOD').AsString;
           M34.Text	:= Query1.FieldByName('MB_SNEV1').AsString;
       	M35.Text	:= Query1.FieldByName('MB_DOKO2').AsString;
			M36.Text	:= Query1.FieldByName('MB_SNEV2').AsString;
       	M13.Text	:= Query1.FieldByName('MB_FUDIJ').AsString;
       	M130.Text	:= Query1.FieldByName('MB_SEDIJ').AsString;
			ComboSet (CBGkat, Query1.FieldByName('MB_GKKAT').AsString, listagkat);
      GKKATEG_valtozott(Query1.FieldByName('MB_GKKAT').AsString);
  			CValuta.ItemIndex   := Max(0, listavnem.IndexOf(Query1.FieldByName('MB_FUNEM').AsString));
  			CVnem.ItemIndex     := Max(0, listavnem.IndexOf(Query1.FieldByName('MB_SENEM').AsString));
       	M23.Text	        := Query1.FieldByName('MB_ALDIJ').AsString;
			CValuta2.ItemIndex := Max(0, listavnem.IndexOf(Query1.FieldByName('MB_ALNEM').AsString));

      pnlTeljesFuvardij.Caption:=Query1.FieldByName('MB_EUDIJ').AsString;
      pnlBecsultKm.Caption:=Query1.FieldByName('MB_TAVKM').AsString;
      pnlSzamitas.Caption:=Query1.FieldByName('MB_TAVMI').AsString;
      meAtallasKm.Text:=Query1.FieldByName('MB_ATAKM').AsString;
      // pnlEURKm.Caption:=Query1.FieldByName('MB_EURKM').AsString;
      pnlEURKm.Caption:=FloatToStrF(Query1.FieldByName('MB_EURKM').AsFloat, ffFixed, 10, 3); // 3 tizedessel
      // pnlEURKmUres.Caption:=Query1.FieldByName('MB_EURKMU').AsString;
      pnlEURKmAtallas.Caption:=FloatToStrF(Query1.FieldByName('MB_EURKMU').AsFloat, ffFixed, 10, 3); // 3 tizedessel
      EURKm_Szinezes;

      RegiRendszam:= Query1.FieldByName('MB_RENSZ').AsString;
      RegiPotszam:= Query1.FieldByName('MB_POTSZ').AsString;

			sorsz := listastatu.IndexOf(Query1.FieldByName('MB_STATU').AsString);
			if sorsz < 0 then begin
				sorsz := 0;
			end;
			CBStatusz.ItemIndex := sorsz;
			M7.Text		:= Query1.FieldByName('MB_MEGJE').AsString;
			RadioButton1.Checked := true;
			if Query1.FieldByName('MB_EXPOR').AsString = '0' then begin
				RadioButton2.Checked := true;
			end;
			sablon	:= Query1.FieldByName('MB_SABLO').AsString;
			if sablon = 'G' then begin
				// A sablon beállítás tiltása generált esetben
				CheckBox1.Enabled	:= false;
//				Bitbtn2.Enabled		:= false;
			end;
			if sablon = 'S' then begin
				CheckBox1.Visible	:= true;
				CheckBox1.Enabled	:= False;
				CheckBox1.Checked	:= true;
        Edit1.Visible:=True;
        Edit1.Enabled:=False;
        Edit1.Text:=Query_Select('SABLON','SB_MBKOD',ret_kod,'SB_MEGJE');
			end;
			if StrToIntDef(Query1.FieldByName('MB_SOSFL').AsString, 0) > 0 then begin
				CHB3.Checked		:= true;
				MaskEdit4.Text		:= Query1.FieldByName('MB_SOSKM').AsString;
			end;
			// A fuvartábla adatok beolvasása
			Query_Run(QueryFt, 'SELECT * FROM FUVARTABLA WHERE FT_FTKOD = '+IntToStr(StrToIntDef(Query1.FieldByName('MB_FUTID').AsString,0)) ,true);
			if QueryFt.RecordCount > 0 then begin
				MaskEdit5.Text	:= QueryFt.FieldByName('FT_LANID').AsString;
				MaskEdit7.Text	:= QueryFt.FieldByName('FT_KATEG').AsString;
				MaskEdit6.Text	:= QueryFt.FieldByName('FT_DESCR').AsString;
				MaskEdit8.Text	:= QueryFt.FieldByName('FT_FTKOD').AsString;
			end;
		end;
		// A felrakók/lerakók beolvasása
		Felrakonyitas;
		CheckBox1Click(Self);

	end;
//	CheckBox1.Visible	:= false;
	BitBtn2.Visible		:= false;
	GombEllenor;
	PozicioIro;
	modosult 	:= false;
end;

procedure TMegbizasbeDlg.BitElkuldClick(Sender: TObject);
const
  UseLogThread = False;
var
  UjRendszam: string;
begin
   if ret_kod <> '' then begin     // Módosításnál ellenőrizzük az ürességet
       Query_Run(Query2, 'SELECT COUNT(*) DB FROM MEGSEGED WHERE MS_MBKOD = '''+ret_kod+''' ');
       if StrToIntDef(Query2.FieldByName('DB').AsString, 0) < 1 then begin
           // Nincs több elem , törölni kell a teljes megbízást
           Query_Run(Query2, 'DELETE FROM MEGBIZAS WHERE MB_MBKOD = '''+ret_kod+''' ');
           Query_Run(Query2, 'DELETE FROM MEGLEVEL WHERE ML_MBKOD = '''+ret_kod+''' ');
           Query_Run(Query2, 'DELETE FROM SABLON   WHERE SB_MBKOD = '''+ret_kod+''' ');
           Close;
           Exit;
           end;
   end;

    Query_Log_Str('===== MEGBIZAS elkuld kezd', 0, false, UseLogThread);
    UjRendszam:= '';
     if M31.Text <> RegiRendszam then
        UjRendszam:= Felsorolashoz(UjRendszam, M31.Text, ',', False);
     if M32.Text <> RegiPotszam then
        UjRendszam:= Felsorolashoz(UjRendszam, M32.Text, ',', False);
     if UjRendszam <> '' then begin
        EKAERFigyelmeztetes(UjRendszam, ret_kod);
        end;

   if (CB_BORD.Visible) and (CB_BORD.Text='')and(not CheckBox51.Checked) then begin
       NoticeKi('A BORDERO számot meg kell adni!') ;
       CB_BORD.SetFocus;
       exit;
   end;
   if (sablon='S') and (Edit1.Text='') then begin
       NoticeKi('A sablonnak nincs neve!') ;
       exit;
   end;

  if CBGkat.ItemIndex = -1 then begin
    NoticeKi('Gk. kategória megadása kötelező!');
	  CBGkat.SetFocus;
		Exit;
	end;
  Query_Log_Str('MEGBIZAS elkuld - Elkuldes', 0, false, UseLogThread);
	if not Elkuldes then begin
		Exit;
   end;

	// A gépkocsi/pótkocsi statisztikák frissítése
	if M31.Text <> '' then begin
      Query_Log_Str('MEGBIZAS elkuld - GepStatTolto előtt', 0, false, UseLogThread);
			GepStatTolto(M31.Text);
      Query_Log_Str('MEGBIZAS elkuld - GepStatTolto után', 0, false, UseLogThread);
	end;
	if M32.Text <> '' then begin
      Query_Log_Str('MEGBIZAS elkuld - PotStatTolto előtt', 0, false, UseLogThread);
			PotStatTolto(M32.Text);
      Query_Log_Str('MEGBIZAS elkuld - PotStatTolto után', 0, false, UseLogThread);
	end;

   // a "FormClose"-ba tettem, hogy bármely kilépésnél megtartsa a konzisztenciát (pl. MB_TEDAT)
   // MegbizasKieg(ret_kod);   // ide tettem a Megbizasujfm-ből, hogy ne a "ModLocate" eredményétől függjön
   voltenter	:= true;
   Query_Log_Str('===== MEGBIZAS elkuld vegez', 0, false, UseLogThread);
   Close;
end;

procedure TMegbizasbeDlg.MaskEditClick(Sender: TObject);
begin
	with Sender as TMaskEdit do begin
  		SelectAll;
  	end;
end;

procedure TMegbizasbeDlg.Modosit(Sender: TObject);
begin
	modosult := true;
end;

procedure TMegbizasbeDlg.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
	EgyebDlg.FormReturn(Key);
end;

procedure TMegbizasbeDlg.BitBtn1Click(Sender: TObject);
var
  eredetiErtek, Ertek: string;
begin
  // if (jakod<>'') and ( Query_Select('VISZONY','VI_JAKOD',jakod,'VI_UJKOD')<>'') then begin
  if M1.Text <> '' then begin
    if (VanHozzaIgaziSzamla(M1.Text) <> '') then begin
      if EgyebDlg.user_super or (GetRightTag(598, False) <> RG_NORIGHT) then begin
         if NoticeKi('A megbízáshoz számla tartozik! A megbízó korrekcióként módosítható, a számlán szereplő megbízóra! Módosítani kívánja a megbízót?', NOT_QUESTION) <> 0 then begin
           	Exit;
            end;  // if
          end  // admin a felhasználó
      else begin
          NoticeKi('A megbízáshoz számla tartozik, ezért a megbízó nem módosítható! Először törölje a megbízást a járatból.');
          Exit;
          end;  // else
      end;  // if
    end; // if

  eredetiErtek:= M11.Text;
	{A megbízó beolvasása}
	Application.CreateForm(TVevoUjFmDlg, VevoUjFmDlg);
	VevoUjFmDlg.valaszt := true;
	VevoUjFmDlg.ShowModal;
	if VevoUjFmDlg.ret_vkod <> '' then begin
		if VevoUjFmDlg.felista > 0 then begin
			if NoticeKi('Feketelistás vevő! Folytatja?', NOT_QUESTION) <> 0 then begin
				VevoUjFmDlg.Destroy;
				Exit;
			end;
		end;
		M11.Text 	:= VevoUjFmDlg.ret_vkod;
		M12.Text 	:= VevoUjFmDlg.ret_vnev;
    if M7.Text='' then
    begin
      M7.Text:= Query_Select('VEVO','V_KOD',VevoUjFmDlg.ret_vkod,'V_MEGJ') ;
    end;
    if EgyebDlg.figy_megbizas_lejartszamla then
        EgyebDlg.Lejartszamlak(M11.Text,True,'50');

      Query3.Close;
      Query3.Parameters[0].Value:=M11.Text;
      Query3.Open;
      Query3.First;
      CB_BORD.Items.Clear;
      while not Query3.Eof do
      begin
        CB_BORD.Items.Add(Query3SZ_ALKOD.Value);
        Query3.Next;
      end;
      Query3.Close;
      BORDNO:= CB_BORD.Items.Count>0 ;

		// A vevő adatainak beolvasása
		Query_Run(Querykoz1,'SELECT * FROM VEVO WHERE V_KOD = '''+VevoUjFmDlg.ret_vkod+''' ',true);
		if QueryKoz1.RecordCount > 0 then begin
           M15.Text	:= QueryKoz1.FieldByName('V_IRSZ').AsString + ' '+QueryKoz1.FieldByName('V_VAROS').AsString + ', '+
                   QueryKoz1.FieldByName('V_CIM').AsString;
			M120.Text	:= QueryKoz1.FieldByName('V_FAX').AsString;
      kellpoz   := QueryKoz1.FieldByName('VE_PKELL').AsInteger>0;
      if BORDNO then
          kellpoz:=False;
		end;
    M13.SetFocus;
    CheckBox5.Visible:=kellpoz;
    CheckBox51.Visible:=BORDNO;
    M14.Visible:=not BORDNO;
    CB_BORD.Visible:=BORDNO;
    E_BORD.Visible:=BORDNO;
    E_BORD_PLUS.Visible:=BORDNO;
    TeljesDijSzamitas;  // az üzemanyag pótlék miatt
    if (M11.Text <> eredetiErtek) and (M1.Text<>'') then begin
      LezaratlanSzamlaTorles(M1.Text);
      end;
	end;
	VevoUjFmDlg.Destroy;
end;

procedure TMegbizasbeDlg.MaskKeyPress(Sender: TObject; var Key: Char);
begin
  with Sender as TMaskEdit do begin
		Key := EgyebDlg.Jochar(Text,Key,12,Tag);
  end;
end;

procedure TMegbizasbeDlg.M21Change(Sender: TObject);
begin
  if M21.Text <> '' then
    BitBtn64.Enabled:= True
  else BitBtn64.Enabled:= False;
end;

procedure TMegbizasbeDlg.M2Exit(Sender: TObject);
begin
	DatumExit(Sender, false);
end;

procedure TMegbizasbeDlg.M3Exit(Sender: TObject);
begin
	DatumExit(Sender, false);
end;

procedure TMegbizasbeDlg.Button1Click(Sender: TObject);
var
	 kuldott_szoveg, kuldott_cimzettek: string;
   emailtorzs, APIKEY, APIPassword, SendingPhone: string;
   sorsz		: integer;
   emailkod, emailcim, S, SMSBody, Sorveg: string;
   statusz, i: integer;
   VoltKuldes: boolean;

   function MyAppendString(Mihez, Mit: string): string;
     begin
       if Trim(Mit)<>'' then
         Result:= Mihez + Mit + ' '
       else Result:= Mihez;
     end;

   function RakodasHelyKiiro(NevMezo, OrszagMezo, IranyitoszamMezo, VarosMezo, CimMezo, ReszletMezo: string): string;
      var
          i: integer;
      begin
         if EgyebDlg.Telephely_e(QueryFelrako.FieldByName(NevMezo).AsString, QueryFelrako.FieldByName(OrszagMezo).AsString,
              QueryFelrako.FieldByName(IranyitoszamMezo).AsString, QueryFelrako.FieldByName(VarosMezo).AsString,
              QueryFelrako.FieldByName(CimMezo).AsString) then begin
            Result:= 'Telephely';
            end
         else begin
            Result:= EgyebDlg.RakodoNevFormat( QueryFelrako.FieldByName(NevMezo).AsString) + '; Cím: '+
            QueryFelrako.FieldByName(OrszagMezo).AsString  + ' ' +QueryFelrako.FieldByName(IranyitoszamMezo).AsString + ' ' +
            QueryFelrako.FieldByName(VarosMezo).AsString + ', ' +
            QueryFelrako.FieldByName(CimMezo).AsString + ' ';
            for i:=1 to 5 do begin
                Result:= MyAppendString(Result, QueryFelrako.FieldByName(ReszletMezo+IntToStr(i)).AsString);
                end; // for
            end;
          Result:= Result + ';'
      end;
begin
	if not Elkuldes then begin
		Exit;
   end;

  // telefon	:= '';

  if EgyebDlg.SMS_KULDES_FORMA='MYSMS' then Sorveg:=''
  else Sorveg:=Chr(13)+ Chr(10);

    if (M33.Text = '') and (M35.Text = '')  and (M21.Text = '') then begin
      Exit;  // nincs kinek küldeni: se sofőr, se alvállalkozó
      end;


     // A levél törzsének összerakása
  //   emailtorzs	:= 'Kód : '+M1.Text + Chr(13)+ Chr(10);
     emailtorzs	:= '';
     if M32.Text<>'' then
       emailtorzs	:= emailtorzs + 'Pótkocsi: '+ M32.Text +'  '+Sorveg;
  //   emailtorzs	:= emailtorzs + 'Megb.: '+ M11.Text + ' ' + M12.Text + ' Pozíció : '+ M14.Text + Chr(13)+ Chr(10);
     if QueryFelrako.RecordCount > 0 then begin
      QueryFelrako.First;
      sorsz	:= 1;
      while not QueryFelrako.Eof do begin
        with QueryFelrako do begin
          if QueryFelrako.RecordCount=1 then
             emailtorzs:= emailtorzs + 'fel: '
          else emailtorzs:= emailtorzs + IntToStr(sorsz)+'.fel: ';
            // Felrakás ideje - Felrakó címe - palettaszám,súly
          emailtorzs:=emailtorzs+ RakodasIdoKiiro(FieldByName('MS_FELDAT').AsString, FieldByName('MS_FELDAT2').AsString,
                                                  FieldByName('MS_FELIDO').AsString, FieldByName('MS_FELIDO2').AsString);
          emailtorzs:=emailtorzs+ RakodasHelyKiiro('MS_TEFNEV', 'MS_TEFOR', 'MS_TEFIR', 'MS_TEFTEL', 'MS_TEFCIM', 'MS_TEFL');
          emailtorzs:=emailtorzs+ 'Áru: ';
          emailtorzs:=emailtorzs+FieldByName('MS_FEPAL').AsString + ' pal.,';
          emailtorzs:=emailtorzs+ IfThen((FieldByName('MS_CSPAL').AsInteger=1)and(QueryFelrako.FieldByName('MS_FPAFEL').AsInteger>0),'('+FieldByName('MS_FPAFEL').AsString+' CSERE),','') +FieldByName('MS_FSULY').AsString  + ' kg; ';
          if Trim(FieldByName('MS_FELARU').AsString) <> '' then
              emailtorzs:=emailtorzs+FieldByName('MS_FELARU').AsString+'; ';
          if Trim(FieldByName('MS_EKAER').AsString)<>'' then
              emailtorzs:=emailtorzs+ 'EKAER: '+FieldByName('MS_EKAER').AsString;
          emailtorzs:=emailtorzs+ ' / '+ Sorveg;

          if QueryFelrako.RecordCount=1 then
                      emailtorzs	:= emailtorzs + 'ler.: '
          else emailtorzs	:= emailtorzs + IntToStr(sorsz)+'.ler.: ';

            // Lerakás ideje - Felrakó címe - palettaszám,súly
          emailtorzs:=emailtorzs+ RakodasIdoKiiro(FieldByName('MS_LERDAT').AsString, FieldByName('MS_LERDAT2').AsString,
                                                  FieldByName('MS_LERIDO').AsString, FieldByName('MS_LERIDO2').AsString);
          emailtorzs:=emailtorzs+ RakodasHelyKiiro('MS_TENNEV', 'MS_TENOR', 'MS_TENIR', 'MS_TENTEL', 'MS_TENCIM', 'MS_TENYL');
          if QueryFelrako.RecordCount>1 then begin   // 1 fel-1 le esetben nem kell külön a lerakandó áru
             emailtorzs:=emailtorzs+ FieldByName('MS_LEPAL').AsString + ' pal.,';
             emailtorzs:=emailtorzs+ IfThen((FieldByName('MS_CSPAL').AsInteger=1)and(FieldByName('MS_LPAFEL').AsInteger>0),'('+FieldByName('MS_LPAFEL').AsString+' CSERE),','') +FieldByName('MS_LSULY').AsString  + ' kg; ';
             end;  // if
          emailtorzs:=emailtorzs+ ' / '+ Sorveg;
          Inc(sorsz);
          Next;
          end;  // with
      end; // while
     end; // if

    VoltKuldes:= False;
    if EgyebDlg.SMS_KULDES_FORMA='MYSMS' then begin
      // SMS küldése
      // UzenetSzerkesztoDlg:= TUzenetSzerkesztoDlg.Create(nil);
      // try
       // SMSBody:= 'Megbízási értesítő: '+ emailtorzs;  // Már nem kell: 2016-08-12
       UzenetSzerkesztoDlg.Reset;
       SMSBody:= emailtorzs;
       UzenetSzerkesztoDlg.SetSzoveg(SMSBody);
       UzenetSzerkesztoDlg.ShowModal;
       VoltKuldes := UzenetSzerkesztoDlg.VoltKuldes;
       kuldott_szoveg:= UzenetSzerkesztoDlg.KuldottSzoveg;
       kuldott_cimzettek:= UzenetSzerkesztoDlg.KuldottCimzettek;
      // finally
      //  if UzenetSzerkesztoDlg <> nil then UzenetSzerkesztoDlg.Release;
      //  end;  // try-finally

      end;

   if EgyebDlg.SMS_KULDES_FORMA='SMSEAGLE' then begin
       if (M33.Text <> '') then begin // saját sofőrök --> Truckpit
           TruckpitUzenetKuldes(M33.Text, M35.Text, emailtorzs, ret_kod);
           VoltKuldes:= False; // a TruckpitUzenetKuldes tárolja is a megbízáshoz, ha van kód
           end
       else if (M21.Text	<> '') then begin  // alvállakozó: SMS vagy email
          UzenetSzerkesztoDlg.Reset;
          UzenetSzerkesztoDlg.AddPartnerCimzett(M21.Text, cimzett);
          SMSBody:= emailtorzs;
          UzenetSzerkesztoDlg.EagleSMSTipus:= uzleti;
          UzenetSzerkesztoDlg.SetSzoveg(SMSBody);
          UzenetSzerkesztoDlg.ShowModal;
          VoltKuldes := UzenetSzerkesztoDlg.VoltKuldes;
          kuldott_szoveg:= UzenetSzerkesztoDlg.KuldottSzoveg;
          kuldott_cimzettek:= UzenetSzerkesztoDlg.KuldottCimzettek;
          end;  // else if
      end;

   if VoltKuldes then begin
          // HibaKiiro('** SofőrSMS: 3');
         // A levél tárolása
         EgyebDlg.StoreMegbizasLevel(ret_kod, kuldott_cimzettek, kuldott_szoveg);
         // HibaKiiro('** SofőrSMS: 4');
          end; // if
     // A megbízáshoz tartozó felrakások/lerakások státuszának állítása
    Query_Run(Query11, 'SELECT * FROM MEGSEGED WHERE MS_MBKOD = '+ret_kod);
    // HibaKiiro('** SofőrSMS: 5');
    if Query11.RecordCount > 0 then begin
         // HibaKiiro('** SofőrSMS: 6');
         while not Query11.Eof do begin
            statusz	:= GetFelrakoStatus(Query11.FieldByName('MS_ID').AsString);
            // HibaKiiro('** SofőrSMS: 7');
            SetFelrakoStatus(Query11.FieldByName('MS_ID').AsString, statusz);
            // HibaKiiro('** SofőrSMS: 8');
            Query11.Next;
            end;  // while
        end;  // if
    // HibaKiiro('** SofőrSMS: 9 VÉGE');
end;

function TMegbizasbeDlg.RakodasIdoKiiro(Datum1, Datum2, Ido1, Ido2: string): string;
var
   Res: string;
begin
   // Res:= Datum1+' '+Ido1;
   Res:= copy(Datum1, 6, 999)+' '+Ido1;  // nem kell az évszám
   if Datum2 <> Datum1 then begin
	    // Res:= Res+' - ' + Trim(Datum2+' '+Ido2);
      Res:= Res+' - ' + Trim(copy(Datum2, 6, 999)+' '+Ido2);   // nem kell az évszám
	    end
   else begin
      if Ido2 <> Ido1 then // nem kell a 08:00 - 08:00 forma
  	    Res:= Res + IfThen(Ido2<>'','-')+Ido2;
	    end;
   Res:= Res+'; ';
   Result:= Res;
end;

procedure TMegbizasbeDlg.BitBtn3Click(Sender: TObject);
var
  nap:integer;
  dat:Tdate;
begin
	if M22.Text = '' then begin   // nincs alvállakozó
		GepkocsiValaszto(M31, GK_TRAKTOR, False);
		GepkocsiOlvaso;
    RendszamValtozott;
    //////////////////////////////
    if M31.Text=EmptyStr then exit;
    nap:=0;
    dat:=date;
    if QueryFelrako.Active then
    begin
     QueryFelrako.First;
     while not QueryFelrako.Eof do
     begin
      if (QueryFelrakoMS_LETDAT.AsString<>'') and ( QueryFelrakoMS_LETDAT.AsDateTime>dat) then
        dat:=QueryFelrakoMS_LETDAT.AsDateTime;
      QueryFelrako.Next;
     end;
    end;
    if dat>date then
      nap:=Trunc(dat-date);
    if FGKErvenyesseg.Ervenyesseg(M31.Text,nap,PLUSZNAP) then
      FGKErvenyesseg.ShowModal;
    //BitBtn19.OnClick(self);
	end else begin
		// Ha van megadva alvállalkozó, akkor az alváállalkozói gépkocsikból választunk
		AlGepkocsiValaszto(M31, M21.Text, GK_TRAKTOR, False);
		// A kategória kitöltése
  	AlGepkocsiOlvaso;
	end;
end;

procedure TMegbizasbeDlg.BitBtn30Click(Sender: TObject);
var
	vanujsor	: string;
begin
  FTDESC:=MaskEdit6.Text;
	if not Elkuldes then begin
		Exit;
	end;
  Screen.Cursor:=crHourGlass;
  Application.ProcessMessages;
  EgyebDlg.MBujtetel:=True;
	Application.CreateForm(TSegedbe2Dlg,Segedbe2Dlg);
  Segedbe2Dlg.hutos:=(HutosLehet)and( (CheckBox7.Checked)or(CBGkat.Text='24EU'));
	Segedbe2Dlg.vevokod	:= '';
  Segedbe2Dlg.kellpoz:=kellpoz;
	try
	if QueryFelrako.RecordCount < 1 then begin
		Segedbe2Dlg.vevokod	:= M11.Text;
	end;
	except
		Segedbe2Dlg.vevokod	:= M11.Text;
	end;
	Segedbe2Dlg.tolt('Új felrakás/lerakás létrehozása', M1.Text, '', '');
  Screen.Cursor:=crDefault;
  Application.ProcessMessages;
	Segedbe2Dlg.ShowModal;
	if Segedbe2Dlg.ret_kod <> '' then begin
		// A felrakók/lerakók beolvasása
		vanujsor	:= Segedbe2Dlg.ujmssorsz;
		Felrakonyitas;
		QueryFelrako.Last;
		modosult	:= true;
    UtvonalSzamitas;
	end;
	Segedbe2Dlg.Destroy;
	GombEllenor;
end;

procedure TMegbizasbeDlg.BitBtn32Click(Sender: TObject);
begin
	M11.Text	:= '';
	M12.Text	:= '';
	M120.Text	:= '';
	M15.Text	:= '';
end;

procedure TMegbizasbeDlg.BitBtn5Click(Sender: TObject);
begin
	M21.Text	:= '';
	M22.Text	:= '';
	M25.Text	:= '';
	M26.Text	:= '';
	M27.Text	:= '';
end;

procedure TMegbizasbeDlg.BitBtn4Click(Sender: TObject);
begin
	{Alvállalkozó beolvasása}
 if EgyebDlg.v_evszam<'2013' then begin
	Application.CreateForm(TAlvallalUjFmDlg,AlvallalUjFmDlg);
	AlvallalUjFmDlg.valaszt	:= true;
	AlvallalUjFmDlg.ShowModal;
	if AlvallalUjFmDlg.ret_vkod <> '' then begin
		M21.Text	:= AlvallalUjFmDlg.ret_vkod;
		M22.Text	:= AlvallalUjFmDlg.ret_vnev;
		// A vevő adatainak beolvasása
		Query_Run(Querykoz1,'SELECT * FROM ALVALLAL WHERE V_KOD = '''+AlvallalUjFmDlg.ret_vkod+''' ',true);
		if QueryKoz1.RecordCount > 0 then begin
			M25.Text	:= QueryKoz1.FieldByName('V_IRSZ').AsString;
			M26.Text	:= QueryKoz1.FieldByName('V_VAROS').AsString;
			M27.Text	:= QueryKoz1.FieldByName('V_CIM').AsString;
		end;
	end;
  	AlvallalUjFmDlg.Destroy;
 end
 else
 begin
  Application.CreateForm(TVevoUjFmDlg, VevoUjFmDlg);
	VevoUjFmDlg.valaszt	:= true;
	VevoUjFmDlg.ShowModal;
	if VevoUjFmDlg.ret_vkod <> '' then begin
		M21.Text	:= VevoUjFmDlg.ret_vkod;
		M22.Text	:= VevoUjFmDlg.ret_vnev;
		// A vevő adatainak beolvasása
		Query_Run(Querykoz1,'SELECT * FROM VEVO WHERE V_KOD = '''+VevoUjFmDlg.ret_vkod+''' ',true);
		if QueryKoz1.RecordCount > 0 then begin
			M25.Text	:= QueryKoz1.FieldByName('V_IRSZ').AsString;
			M26.Text	:= QueryKoz1.FieldByName('V_VAROS').AsString;
			M27.Text	:= QueryKoz1.FieldByName('V_CIM').AsString;
		end;
	end;
  	VevoUjFmDlg.Destroy;
 end;
end;

procedure TMegbizasbeDlg.GombEllenor;
begin
	BitBtn31.Enabled	:= false;
	BitBtn6.Enabled		:= false;
//	BitBtn21.Enabled	:= false;
	try
		if QueryFelrako.RecordCount > 0 then begin
			BitBtn31.Enabled	:= true;
			BitBtn6.Enabled		:= true;
//			BitBtn21.Enabled	:= true;
		end;
	except
	end;
end;
procedure TMegbizasbeDlg.BitBtn31Click(Sender: TObject);
var
	vanujsor	: string;
  sorsz:integer;
begin
  FTDESC:=MaskEdit6.Text;
	if not elkuldes then begin
		Exit;
	end;
  sorsz:= QueryFelrakoMS_SORSZ.Value;
  Screen.Cursor:=crHourGlass;
  Application.ProcessMessages;
  EgyebDlg.MBujtetel:=False;
	Application.CreateForm(TSegedbe2Dlg,Segedbe2Dlg);
  Segedbe2Dlg.hutos:=(HutosLehet)and( (CheckBox7.Checked)or(CBGkat.Text='24EU'));
  Segedbe2Dlg.kellpoz:=kellpoz;
{
  Segedbe2Dlg.tipusflag:=0;
  if  QueryFelrako.FieldByname('MS_AZTIP').AsString='AF' then Segedbe2Dlg.tipusflag:= 1;
  if  QueryFelrako.FieldByname('MS_AZTIP').AsString='AL' then Segedbe2Dlg.tipusflag:= -1;
 }
	Segedbe2Dlg.tolt('Felrakás/lerakás módosítása',
		M1.Text, QueryFelrako.FieldByname('MS_SORSZ').AsString, '0');
  Screen.Cursor:=crDefault;
  Application.ProcessMessages;
(*
	if StrToIntDef(QueryFelrako.FieldByName('MS_FAJKO').AsString,0) = 3 then begin
		// Lerakásról van szó -> a felrakási adatok nem módosíthatók
		Segedbe2Dlg.Tipusflag	:= 1;
	end;
	if StrToIntDef(QueryFelrako.FieldByName('MS_FAJKO').AsString,0) = 2 then begin
		// Lerakásról van szó -> a felrakási adatok nem módosíthatók
		Segedbe2Dlg.Tipusflag	:= -1;
	end;
	if StrToIntDef(QueryFelrako.FieldByName('MS_FAJKO').AsString,0) = 1 then begin
		// Lerakásról van szó -> a felrakási adatok nem módosíthatók
		Segedbe2Dlg.Tipusflag	:= -1;
	end;
*)
  if Segedbe2Dlg <> nil then begin
  	Segedbe2Dlg.ShowModal;
  	if Segedbe2Dlg.ret_kod <> '' then begin
  		// A felrakók/lerakók beolvasása
  		vanujsor	:= Segedbe2Dlg.ujmssorsz;
  		Felrakonyitas;
  		QueryFelrako.First;
  		modosult	:= true;
  //		while ( ( not QueryFelrako.EOf ) and (QueryFelrako.FieldByname('MS_SORSZ').AsString <> Segedbe2Dlg.ret_sorsz) ) do begin
  		while ( ( not QueryFelrako.EOf ) and (QueryFelrako.FieldByname('MS_SORSZ').AsInteger <> sorsz) ) do begin
  			QueryFelrako.Next;
    		end;  // while
      UtvonalSzamitas;
    	end;  // if ret_kod
  	if Segedbe2Dlg <> nil then Segedbe2Dlg.Release;
    end;  // if
  SetMegbizasModositoUser(M1.Text);
	GombEllenor;
	if vanujsor <> '' then begin
		// Megnyitjuk az új elemet
		Application.CreateForm(TSegedbe2Dlg,Segedbe2Dlg);
    Segedbe2Dlg.hutos:=(HutosLehet)and( (CheckBox7.Checked)or(CBGkat.Text='24EU'));
    Segedbe2Dlg.kellpoz:=kellpoz;
		Segedbe2Dlg.tolt('Felrakás/lerakás módosítása', M1.Text, vanujsor, '0');
    if Segedbe2Dlg <> nil then begin
  		Segedbe2Dlg.ShowModal;
  		if Segedbe2Dlg.ret_kod <> '' then begin
  			// A felrakók/lerakók beolvasása
  			Felrakonyitas;
  			QueryFelrako.First;
  			modosult	:= true;
  			while ( ( not QueryFelrako.EOf ) and (QueryFelrako.FieldByname('MS_SORSZ').AsString <> Segedbe2Dlg.ret_sorsz) ) do begin
  				QueryFelrako.Next;
  			end;
  		end;
  		if Segedbe2Dlg <> nil then Segedbe2Dlg.Release;
      end;  // if
    UtvonalSzamitas;
		GombEllenor;
	end;
end;

procedure TMegbizasbeDlg.BitBtn11Click(Sender: TObject);
begin
	// A sofőr kiválasztása
	DolgozoValaszto(M33, M34);
   if M35.Text = M33.Text then begin
		NoticeKi('Azonos sofőr kiválasztása!');
       M33.Text	:= '';
       M34.Text	:= '';
   end;
end;

procedure TMegbizasbeDlg.BitBtn8Click(Sender: TObject);
var
  nap:integer;
  dat:Tdate;
begin
	if M22.Text = '' then begin
		GepkocsiValaszto(M32, GK_POTKOCSI);
    //////////////////////////////
    if M32.Text=EmptyStr then exit;
    nap:=0;
    dat:=date;
    if QueryFelrako.Active then
    begin
     QueryFelrako.First;
     while not QueryFelrako.Eof do begin
      if QueryFelrakoMS_LETDAT.AsString<>'' then
        if QueryFelrakoMS_LETDAT.AsDateTime>dat then
          dat:=QueryFelrakoMS_LETDAT.AsDateTime;
      QueryFelrako.Next;
     end;
    end;
    if  EgyebDlg.HutosGk(M32.Text) then
      CheckBox7.Checked:=True;
    if dat>date then
      nap:=Trunc(dat-date);
    if FGKErvenyesseg.Ervenyesseg(M32.Text,nap,PLUSZNAP) then
      FGKErvenyesseg.ShowModal;
//    BitBtn20.OnClick(self);
	end else begin
		// Ha van megadva alvállalkozó, akkor az alváállalkozói gépkocsikból választunk
		AlGepkocsiValaszto(M32, M21.Text, GK_POTKOCSI);
	end;
end;

procedure TMegbizasbeDlg.BitBtn9Click(Sender: TObject);
begin
   M32.Text	:= '';
end;

procedure TMegbizasbeDlg.BitBtn7Click(Sender: TObject);
begin
	M31.Text	:= '';
end;

procedure TMegbizasbeDlg.BitBtn13Click(Sender: TObject);
begin
	DolgozoValaszto(M35, M36);
   if M35.Text = M33.Text then begin
		NoticeKi('Azonos sofőr kiválasztása!');
       M35.Text	:= '';
       M36.Text	:= '';
   end;
end;

procedure TMegbizasbeDlg.BitBtn12Click(Sender: TObject);
begin
	M33.Text	:= '';
	M34.Text	:= '';
end;

procedure TMegbizasbeDlg.BitBtn14Click(Sender: TObject);
begin
	M35.Text	:= '';
	M36.Text	:= '';
end;

function TMegbizasbeDlg.Elkuldes : boolean;
const
  UseLogThread = False;
var
	st 			: string;
	exportstr	: string;
	gkkat		: string;
	exkiirt		: string;
  borderno2   : string;
  pozicio     : string;
  bno2        : integer;
  JaratStatus: integer;
  jaratkod: string;
  figyelmeztet_limit: double;
  MBDeviza: string;
begin
	if EgyebDlg.VANLANID > 0 then begin
		if KELL_LAN_ID then begin
			// A LAN_ID meglétének vizsgálata
			if MaskEdit8.Text = '' then begin
				NoticeKi('LAN ID megadása kötelező!');
				Exit;
			  end;
		  end;
	  end;
  figyelmeztet_limit:= StrToFloatDef(EgyebDlg.Read_SZGrid('991', 'MBDIJ_LIMIT'), 4000);
  MBDeviza:= listavnem[CValuta.ItemIndex];
  if (MBDeviza='EUR') and (StrToFloatDef( M13.Text,0) > figyelmeztet_limit) then begin
    if NoticeKi('Szokatlanul nagy a fuvardíj ('+M13.Text+' EUR)! Biztosan helyes a devizanem?', NOT_QUESTION) <> 0 then begin
			 Exit; // function
     	 end;
     end;

	if (ComboBox1.Text = '') then begin
    jaratkod:= Query_Select('MEGBIZAS', 'MB_MBKOD', M1.Text, 'MB_JAKOD');
    JaratStatus:= EllenorzottJarat(jaratkod, False);
    if (JaratStatus=0) or (JaratStatus=-1) or (JaratStatus=-2) then begin  // csak normál, elszámolt vagy feloldott esetén
  		NoticeKi('A Felelős megadása kötelező!');
  		ComboBox1.SetFocus;
   		Exit;
      end;
	  end;

	if (Checkbox1.Checked)and(CheckBox1.Enabled)and(Edit1.Text='') then begin
		NoticeKi('A sablon mentéséhez a nevét meg kell adni!');
		Edit1.SetFocus;
		Exit;
	end;

	if ret_kod = '' then begin
		{Új rekord felvitele}
		ret_kod := IntToStr(GetNextCode('MEGBIZAS', 'MB_MBKOD', 1, 0));
		Query_Run ( Query2, 'INSERT INTO MEGBIZAS (MB_MBKOD, MB_DATUM) VALUES (' +ret_kod+ ', '''+EgyebDlg.MaiDatum+''' )',true);
		M1.Text	:= ret_kod;
       // A vevő ellenőrzése
    if StrToIntDef(Query_Select('VEVO', 'V_KOD', M11.Text, 'VE_FELIS'), 0) > 0 then begin
       SendFeketevevo;
       end;
     end
  else begin
     // Ha módosítunk és feketére váltunk akkor is küldjük el.
     if StrToIntDef(Query_Select('VEVO', 'V_KOD', M11.Text, 'VE_FELIS'), 0) > 0 then begin
        // Megnézzük hogy eddig fekete volt-e
		    Query_Run ( Query2, 'SELECT * FROM MEGBIZAS WHERE MB_MBKOD = '+ret_kod ,true);
        if StrToIntDef(Query_Select('VEVO', 'V_KOD', Query2.FieldByName('MB_VEKOD').AsString, 'VE_FELIS'), 0) = 0 then begin
           // Megváltozott a vevő
           SendFeketevevo;
           end;
        end;
	   end;
  //pozicio:=M14.Text;
  if (CB_BORD.Text<>'') and (E_BORD.Text='') then begin
    Query4.Close;
    Query4.Parameters[0].Value:=CB_BORD.Text;
    Query4.Open;
    borderno2:= Query4MAX.Value;
    if borderno2='' then begin
       E_BORD.Text:='0001'
    end else begin
       bno2:= StrToInt(borderno2)+1;
       borderno2:=IntToStr(bno2);
       borderno2:=Stringofchar('0',4-length(borderno2)) +borderno2;
       E_BORD.Text:=borderno2;
    end;
    Query4.Close;
    //pozicio:=CB_BORD.Text+'-'+E_BORD.Text;
  end;
  if BORDNO then begin
    // pozicio:=CB_BORD.Text+'-'+E_BORD.Text+' ; '+E_BORD_PLUS.Text
    if trim(CB_BORD.Text+E_BORD.Text) = '' then
      pozicio:= E_BORD_PLUS.Text
    else
      pozicio:= CB_BORD.Text+'-'+E_BORD.Text+' ; '+E_BORD_PLUS.Text
  end else begin
    pozicio:=M14.Text;
    end;
	{A régi rekord módosítása}
	st			:= '0';
	// Meghatározzuk a dátumokat a felrakás/lerakás -ból
	exportstr	:= '1';
	exkiirt		:= 'export';
	if RadioButton2.Checked = true then begin
		exportstr	:= '0';
		exkiirt		:= 'import';
	  end;
  // GKKAT
  if CBGkat.ItemIndex>=0 then begin
	  gkkat	:= listagkat[CBGkat.ItemIndex];
	  if (M31.Text <> '') and (gkkat = '') then begin
  		gkkat	:= Query_Select('GEPKOCSI', 'GK_RESZ', M31.Text, 'GK_GKKAT');
	    end;  // if
  	end;  // if
  Query_Log_Str('MEGBIZAS elkuld - KULDES - update megbizas (1)', 0, false, UseLogThread);
	Query_Update (Query2, 'MEGBIZAS', [
		'MB_VEKOD',     ''''+M11.Text+'''',
//		'MB_POZIC',     ''''+M14.Text+'''',
		'MB_POZIC',     ''''+pozicio+'''',
    'MB_INTREF',    ''''+M16.Text+'''',
		'MB_ALVAL',     ''''+M21.Text+'''',
		'MB_ALNEV',     ''''+M22.text+'''',
		'MB_RENSZ',     ''''+M31.Text+'''',
		'MB_POTSZ',     ''''+M32.Text+'''',
		'MB_DOKOD',     ''''+M33.text+'''',
		'MB_SNEV1',     ''''+M34.text+'''',
		'MB_DOKO2',     ''''+M35.text+'''',
		'MB_SNEV2',     ''''+M36.text+'''',
		'MB_KISCS',     BoolToStr01(CheckBox2.Checked),
		'MB_VAM',       BoolToStr01(CheckBox3.Checked),
		'MB_ADR',       BoolToStr01(CheckBox4.Checked),
		'MB_NOPOZ',     BoolToStr01(CheckBox5.Checked),
		'MB_EDIJ',      BoolToStr01(CheckBox6.Checked),
		'MB_HUTOS',     BoolToStr01(CheckBox7.Checked),
		'MB_FELFUG',    BoolToStr01(CheckBox8.Checked),
		'MB_NUZPOT',    BoolToStr01(CheckBox9.Checked),
		'MB_KOBOS',     BoolToStr01(CheckBox10.Checked),
		'MB_NOCSOP',    BoolToStr01(CheckBox11.Checked),
		'MB_FUDIJ',     SqlSzamString(StringSzam(M13.Text),12,2),
		'MB_FUNEM',     ''''+listavnem[CValuta.ItemIndex]+'''',
		'MB_SEDIJ',     SqlSzamString(StringSzam(M130.Text),12,2),
		'MB_SENEM',     ''''+listavnem[CVnem.ItemIndex]+'''',
		'MB_ALDIJ',     SqlSzamString(StringSzam(M23.Text),12,2),
		'MB_ALNEM',     ''''+listavnem[CValuta2.ItemIndex]+'''',
		'MB_STATU',     ''''+listastatu[CBStatusz.ItemIndex]+'''',

 		'MB_TAVKM',     SqlSzamString(StringSzam(pnlBecsultKm.Caption),12,2),
 		'MB_TAVMI',     ''''+pnlSzamitas.Caption+'''',
 		'MB_ATAKM',     SqlSzamString(StringSzam(meAtallasKm.Text),12,2),
 		'MB_EUDIJ',     SqlSzamString(StringSzam(pnlTeljesFuvardij.Caption),12,2),
 		'MB_EURKM',     SqlSzamString(StringSzam(pnlEURKm.Caption),12,3),
 		'MB_EURKMU',    SqlSzamString(StringSzam(pnlEURKmAtallas.Caption),12,3),

//		'MB_DATUM',     ''''+EgyebDlg.MaiDatum+'''',
		'MB_MEGJE',     ''''+M7.Text+'''',
		'MB_EXPOR',     exportstr,
		'MB_EXSTR',     ''''+exkiirt+'''',
		'MB_VENEV',     ''''+M12.Text+'''',
		'MB_FEKOD',     ''''+EgyebDlg.user_code+'''',
		'MB_FENEV',     ''''+ComboBox1.Text+'''',
//		'MB_FENEV',     ''''+EgyebDlg.user_name+'''',
		'MB_SOSFL',     BoolToStr01(CHB3.Checked),
		'MB_SOSKM',     IntToStr(StrToIntDef(MaskEdit4.Text, 0)),
		'MB_GKKAT',     ''''+gkkat+'''',
		'MB_FUTID',     IntToStr(StrToIntDef(MaskEdit8.Text,0)),
		'MB_SABLO',     ''''+sablon+'''',
		'MB_BORDNO1',   ''''+CB_BORD.Text+'''',
		'MB_BORDNO2',   ''''+E_BORD.Text+'''',
		'MB_BORDNO3',   ''''+E_BORD_PLUS.Text+''''
		], ' WHERE MB_MBKOD = '+ret_kod );

	// A gépkocsi adatok frissítése a MEGSEGED táblában
  Query_Log_Str('MEGBIZAS elkuld - KULDES - update megseged (1)', 0, false, UseLogThread);
	Query_Update (Query2, 'MEGSEGED',
		[
		'MS_RENSZ', 	''''+M31.Text+'''',
		'MS_POTSZ', 	''''+M32.Text+'''',
		'MS_DOKOD', 	''''+M33.Text+'''',
		'MS_DOKO2', 	''''+M35.Text+'''',
		'MS_GKKAT', 	''''+gkkat+'''',
		'MS_SNEV1', 	''''+M34.Text+'''',
		'MS_SNEV2', 	''''+M36.Text+''''
		],
		' WHERE MS_MBKOD = '+ret_kod+' AND MS_FAJKO = 0 ');

   // poziciószám beírása a létező viszonyhoz, számlához  (ha a számla nem véglegesített)
   if pozicio<>'' then begin
       Query_Log_Str('MEGBIZAS elkuld - KULDES - UpdatePozicio', 0, false, UseLogThread);
       UpdatePozicio(ret_kod,pozicio);
   end;
   // ha van hozzá számlázatlan viszonylat, akkor ott frissíti az összeget
   Query_Log_Str('MEGBIZAS elkuld - KULDES - ModosultAdatokViszonyba? ('+ret_kod+')', 0, false, UseLogThread);
   ModosultAdatokViszonyba(ret_kod);

   // sablon mentése
   if (Checkbox1.Checked)and(CheckBox1.Enabled)and(Edit1.Text<>'') then begin
       //  SablonMentes(ret_kod, Edit1.Text);
   end;
   // ComboBox2.Visible:=False;
	Result	:= true;
end;

procedure TMegbizasbeDlg.ModosultAdatokViszonyba(MBKOD: string);
var
   S, VIKOD, MB_VALNEM, VI_VALNEM: string;
   VI_OSSZEG, MB_OSSZEG, SEDIJ_konvertalva, VI_FDEXP, VI_FDKOV, VI_FDIMP, ARANY: double;
   VI_KULF: integer;
   FUDIJ, SEDIJ: double;
   FUNEM, SENEM, arfdatum, VEKOD, VENEV, VI_VEKOD, VI_VENEV: string;
begin
  S:= 'select MB_FUDIJ, MB_FUNEM, MB_SEDIJ, MB_SENEM, VI_VIKOD, VI_FDEXP, VI_FDIMP, VI_VALNEM, VI_FDKOV, VI_KULF, '+
    ' MB_VEKOD, MB_VENEV, VI_VEKOD, VI_VENEV '+
    ' from MEGBIZAS '+
    ' left join VISZONY on VI_VIKOD = MB_VIKOD '+
    ' left outer join SZFEJ on VI_UJKOD=SA_UJKOD '+
    ' where VI_VIKOD is not null '+
    ' and isnull(SA_FLAG, 1) in (1) and isnull(SA_RESZE, 0) = 0 '+  // nincs számla vagy módosítható
//     ' 	and VI_UJKOD is null '+
    ' and MB_MBKOD = '+MBKOD;
  Query_Run(QueryLocal, S);
  if not QueryLocal.eof then with QueryLocal do begin
    VIKOD:= FieldByName('VI_VIKOD').AsString;
    VI_OSSZEG:= FieldByName('VI_FDKOV').AsFloat + FieldByName('VI_FDIMP').AsFloat;
    VI_VALNEM:= FieldByName('VI_VALNEM').AsString;
    VI_FDIMP:= FieldByName('VI_FDIMP').AsFloat;
    VI_FDKOV:= FieldByName('VI_FDKOV').AsFloat;
    VI_KULF:= FieldByName('VI_KULF').AsInteger;
    FUDIJ:= FieldByName('MB_FUDIJ').AsFloat;
    SEDIJ:= FieldByName('MB_SEDIJ').AsFloat;
    FUNEM:= FieldByName('MB_FUNEM').AsString;
    SENEM:= FieldByName('MB_SENEM').AsString;
    VEKOD:= FieldByName('MB_VEKOD').AsString;
    VENEV:= FieldByName('MB_VENEV').AsString;
    VI_VEKOD:= FieldByName('VI_VEKOD').AsString;
    VI_VENEV:= FieldByName('VI_VENEV').AsString;

    // volt egy kezdeményezés, hogy kerüljön be az egyéb díj is a VISZONY-ba
    {if SENEM = FUNEM then begin
      MB_OSSZEG:= FUDIJ + SEDIJ;
      end
    else begin
      arfdatum:= GetArfolyamDat(MBKOD);
      SEDIJ_konvertalva:= SEDIJ * EgyebDlg.ArfolyamErtek(SENEM, arfdatum, true, true) / EgyebDlg.ArfolyamErtek(FUNEM, arfdatum, true, true); // keresztárfolyammal
      MB_OSSZEG:= FUDIJ + SEDIJ_konvertalva;
      end;}

    MB_OSSZEG:= FUDIJ;

    if (MB_OSSZEG <> VI_OSSZEG) or (FUNEM <> VI_VALNEM) then begin  // változás van
        if VI_KULF = 0 then begin  // nincs megosztva
           Query_Update(QueryLocal, 'VISZONY', [
          		'VI_VALNEM',	''''+FUNEM+'''',
          		'VI_FDEXP',		SqlSzamString(MB_OSSZEG,12,4)
            	], ' WHERE VI_VIKOD = '+VIKOD);
            end
        else begin // megosztott
            ARANY:= VI_FDKOV / VI_OSSZEG;  // hányadrésze az export összeg
            VI_FDKOV:= MB_OSSZEG * ARANY;
            VI_FDIMP:= MB_OSSZEG - VI_FDKOV;
            Query_Update(QueryLocal, 'VISZONY', [
          		'VI_VALNEM',	''''+FUNEM+'''',
              'VI_FDEXP',		SqlSzamString(MB_OSSZEG,12,4),
          		'VI_FDKOV',		SqlSzamString(VI_FDKOV,12,4),
          		'VI_FDIMP',		SqlSzamString(VI_FDIMP,12,4)
            	], ' WHERE VI_VIKOD = '+VIKOD);
            end;  // else
        end; // változás van

    if (VEKOD <> VI_VEKOD) or (VENEV <> VI_VENEV) then begin  // vevő változás van
       Query_Update(QueryLocal, 'VISZONY', [
          'VI_VEKOD',	''''+VEKOD+'''',
          'VI_VENEV',	''''+VENEV+''''
          ], ' WHERE VI_VIKOD = '+VIKOD);
        end;
    end;  // with

end;

procedure TMegbizasbeDlg.BitBtn64Click(Sender: TObject);
var
   Rendszam: string;
begin
   Rendszam:= InputBox('Alvállalkozó pót megadása', 'Rendszám: ', '');
   M32.text:= Rendszam;
end;

procedure TMegbizasbeDlg.BitBtn6Click(Sender: TObject);
var
  sorsz: integer;
  msid1_1,msid1_2: string;
begin
	// A törlés végrehajtható-e?
	if NoticeKi('Valóban törölni kívánja a kijelölt rekordot? ', NOT_QUESTION) = 0 then begin
   // tetel:=Query_Select('MEGSEGED','MS_MBKOD',IntToStr(StrToIntDef(M1.Text,0)),'count(MS_MBKOD)');
    if  QueryFelrakoMS_FAJKO.Value=0 then  // normál
    begin
	  	Query_Run ( Query2, 'select MS_ID FROM MEGSEGED WHERE MS_MBKOD = '+IntToStr(StrToIntDef(M1.Text,0))+' and MS_SORSZ='+IntToStr( QueryFelrakoMS_SORSZ.Value),true);
      if Query2.RecordCount>0 then  //
      begin
        //Query2.First;
        msid1_1:=Query2.FieldByName('MS_ID').AsString ;
		    Query_Run ( Query11, 'select MS_SORSZ,MS_ID FROM MEGSEGED WHERE MS_MBKOD = '+IntToStr(StrToIntDef(M1.Text,0))+
        ' and MS_ID2='+IntToStr(StrToIntdef(msid1_1, 0))+' and MS_SORSZ<>'+IntToStr( QueryFelrakoMS_SORSZ.Value)+' order by MS_SORSZ' ,true);
        sorsz:=Query11.FieldByName('MS_SORSZ').AsInteger   ;
        if sorsz>0 then  // hoztak létre erről azonos fel v. lerakót
        begin
          // átsorszámozás  ; ID2 törlése
          //Query_Run(Query11,'update megseged set ms_id2=Null,ms_aztip=Null,ms_tipus=0'+' where ms_id='+Query11.FieldByName('MS_ID').AsString );
          Query_Run(Query11,'update megseged set ms_id2=Null,ms_aztip=Null where ms_id='+Query11.FieldByName('MS_ID').AsString );
          Query_Run(Query11,'update megseged set ms_id2=Null,ms_aztip=Null,ms_sorsz='+IntToStr(sorsz)+' where ms_id='+msid1_1);
        end
        else  // törölhető
    	    Query_Run ( Query11, 'DELETE FROM MEGSEGED WHERE MS_ID = ' +IntToStr(StrToIntdef(msid1_1, 0)),true);

        // a párja
        Query2.Next;
        if not Query2.Eof then
        begin
         msid1_2:=Query2.FieldByName('MS_ID').AsString ;
  		    Query_Run ( Query11, 'select MS_SORSZ,MS_ID FROM MEGSEGED WHERE MS_MBKOD = '+IntToStr(StrToIntDef(M1.Text,0))+
         ' and MS_ID2='+IntToStr(StrToIntdef(msid1_2, 0))+' and MS_SORSZ<>'+IntToStr( QueryFelrakoMS_SORSZ.Value)+' order by MS_SORSZ' ,true);
         sorsz:=Query11.FieldByName('MS_SORSZ').AsInteger   ;
         if sorsz>0 then  // hoztak létre erről azonos fel v. lerakót
         begin
          // átsorszámozás  ; ID2 törlése
          //Query_Run(Query11,'update megseged set ms_id2=Null,ms_aztip=Null,ms_tipus=0'+' where ms_id='+Query11.FieldByName('MS_ID').AsString );
          Query_Run(Query11,'update megseged set ms_id2=Null,ms_aztip=Null where ms_id='+Query11.FieldByName('MS_ID').AsString );
          Query_Run(Query11,'update megseged set ms_id2=Null,ms_aztip=Null,ms_sorsz='+IntToStr(sorsz)+' where ms_id='+msid1_2 );
         end
         else  // törölhető
    	    Query_Run ( Query11, 'DELETE FROM MEGSEGED WHERE MS_ID = ' +IntToStr(StrToIntdef(msid1_2, 0)),true);

        end;
      end;
    end
    else
	    Query_Run ( Query2, 'DELETE FROM MEGSEGED WHERE MS_MBKOD = '+IntToStr(StrToIntDef(M1.Text,0))+
			  ' AND MS_SORSZ = '+QueryFelrako.FieldByName('MS_SORSZ').AsString,true);

	  Felrakonyitas;
    UtvonalSzamitas;
  end;
end;

procedure TMegbizasbeDlg.FormCanResize(Sender: TObject; var NewWidth,
  NewHeight: Integer; var Resize: Boolean);
begin
	if ( ( NewWidth < 1024 ) or (NewHeight < 768 ) ) then begin
   	Resize	:= false;
   end;
end;

procedure TMegbizasbeDlg.FormClose(Sender: TObject; var Action: TCloseAction);
begin
   // azért, hogy bármely kilépésnél megtartsa a konzisztenciát (pl. MB_TEDAT)
   MegbizasKieg(ret_kod);
   Query_Log_Str('===== MegbizasKieg vegez', 0, false, False);
end;

procedure TMegbizasbeDlg.FormDestroy(Sender: TObject);
begin
	listavnem.Free;
	listastatu.Free;
   FTDESC:='';
   try
   MegbizasUjFmDlg.Timer1.Enabled:=True;;
   except
   end;
 // FGKErvenyesseg.Free;
end;

procedure TMegbizasbeDlg.BitBtn21Click(Sender: TObject);
begin
 { Query_Run(Query11,'select ms_id2 from megseged where ms_mbkod='+QueryFelrakoMS_MBKOD.AsString+' and ms_sorsz='+QueryFelrakoMS_SORSZ.AsString);
  if Query11.RecordCount<2 then  // azonos fel v. lerakó
  begin
      NoticeKi('Erről nem lehet körfuvart készíteni!');
      exit;
  end;  }
	// Lérehozzuk a kiválasztott elemhez tartozó körfuvart
	Application.CreateForm(TSegedbe2Dlg,Segedbe2Dlg);
  Segedbe2Dlg.hutos:=(HutosLehet)and( (CheckBox7.Checked)or(CBGkat.Text='24EU'));
	Segedbe2Dlg.vevokod	:= '';
  Segedbe2Dlg.kellpoz:=kellpoz;
   try
   if QueryFelrako.RecordCount < 1 then begin
   	Segedbe2Dlg.vevokod	:= M11.Text;
   end;
   except
   	Segedbe2Dlg.vevokod	:= M11.Text;
	end;
	Segedbe2Dlg.kellelore	:= false;
	Segedbe2Dlg.tolt2('Új felrakás/lerakás létrehozása',
   	M1.Text, QueryFelrako.FieldByname('MS_SORSZ').AsString, '0');
	Segedbe2Dlg.ShowModal;
	if Segedbe2Dlg.ret_kod <> '' then begin
       // A felrakók/lerakók beolvasása
   		Felrakonyitas;
     	QueryFelrako.Last;
      modosult	:= true;
      UtvonalSzamitas;
	end;
	Segedbe2Dlg.Destroy;
	GombEllenor;
end;

procedure TMegbizasbeDlg.Felrakonyitas;
begin
	Query_Run(QueryFelrako, 'SELECT DISTINCT MS_MBKOD, MS_SORSZ, MS_FAJKO, MS_FAJTA, MS_FELNEV, MS_FELTEL, MS_FELCIM, MS_FELDAT, '+
		'MS_FELIDO,MS_FELIDO2, MS_FETDAT, MS_FETIDO, MS_LERNEV, MS_LERTEL, MS_LERCIM, MS_LERDAT, MS_LERIDO, MS_LERIDO2, MS_LETDAT, '+
		'MS_FELSE1, MS_FELSE2, MS_FELSE3, MS_FELSE4, MS_FELSE5, MS_LERSE1, MS_LERSE2, MS_LERSE3, MS_LERSE4, MS_LERSE5,'+
		'MS_LETIDO, MS_FELARU, MS_FSULY, MS_FEPAL, MS_LSULY, MS_LEPAL, MS_ORSZA, MS_FELIR, MS_LELIR, MS_FORSZ, MS_RENSZ,'+
    'MS_EXSTR, MS_FERKIDO, MS_LERKIDO, MS_LERDAT2, MS_FELDAT2, MS_CSPAL, MS_FPAFEL, MS_LPAFEL, MS_AZTIP, MS_EKAER, '+
    'MS_TEFNEV, MS_TEFOR, MS_TEFIR, MS_TEFTEL, MS_TEFCIM, MS_TEFL1, MS_TEFL2, MS_TEFL3, MS_TEFL4, MS_TEFL5, '+
    'MS_TENNEV, MS_TENOR, MS_TENIR, MS_TENTEL, MS_TENCIM, MS_TENYL1, MS_TENYL2, MS_TENYL3, MS_TENYL4, MS_TENYL5 '+
       'FROM MEGSEGED WHERE MS_MBKOD = '+IntToStr(StrToIntDef(M1.Text,0))+' ORDER BY MS_SORSZ',true);

{	Query_Run(QueryFelrako, 'SELECT DISTINCT MS_MBKOD, MS_SORSZ, MS_FAJKO, MS_FAJTA,MS_STATN, MS_FELNEV, MS_FELTEL, MS_FELCIM, MS_FELDAT, '+
		'MS_FELIDO, MS_FETDAT, MS_FETIDO, MS_LERNEV, MS_LERTEL, MS_LERCIM, MS_LERDAT, MS_LERIDO, MS_LETDAT, '+
		'MS_FELSE1, MS_FELSE2, MS_FELSE3, MS_FELSE4, MS_FELSE5, MS_LERSE1, MS_LERSE2, MS_LERSE3, MS_LERSE4, MS_LERSE5,'+
		'MS_LETIDO, MS_FELARU, MS_FSULY, MS_FEPAL, MS_LSULY, MS_LEPAL, MS_ORSZA, MS_FELIR, MS_LELIR, MS_FORSZ, MS_RENSZ '+
       'FROM MEGSEGED WHERE MS_MBKOD = '+IntToStr(StrToIntDef(M1.Text,0))+' ORDER BY MS_SORSZ',true);
 }
       //		'MS_LETIDO, MS_FELARU, MS_FSULY, MS_FEPAL, MS_LSULY, MS_LEPAL, MS_ORSZA, MS_FELIR, MS_LELIR, MS_FORSZ, MS_RENSZ,MS_STATK,MS_TIPUS '+
   {
  QueryFelrako.Close;
  QueryFelrako.Parameters[0].Value:=IntToStr(StrToIntDef(M1.Text,0));
  QueryFelrako.open;
  QueryFelrako.first;
  }


end;

procedure TMegbizasbeDlg.GKKATEG_valtozott(Ujkateg: string);
var
    S: string;
begin
    S:=EgyebDlg.Read_SZGrid('128', Ujkateg);
     if trim(S)<>'' then
        UresKMSzazalek:= StrToInt(S)
     else UresKMSzazalek:=0;

     S:=Query_select('KATINTER', 'KI_GKKAT', Ujkateg, 'KI_EUTOL');
     if trim(S)<>'' then
        KategNormaAlso:=StrToFloat(S)
     else KategNormaAlso:=0;

     S:=Query_select('KATINTER', 'KI_GKKAT', Ujkateg, 'KI_EURIG');
     if trim(S)<>'' then
        KategNormaFelso:=StrToFloat(S)
     else KategNormaFelso:=0;
end;

procedure TMegbizasbeDlg.TeljesDijSzamitas;
var
   // teljesdij_EUR, fuvardij_EUR, egyebdij_EUR, OsszKM, EURKm: double;
   vevokod, datum, S, EURKm_leiras, V_ARNAP: string;
   OldCursor: TCursor;
   OutRecord: T_TeljesDijSzamitasRec;
begin
   OldCursor:= Screen.Cursor;  // elmentjük
   Screen.Cursor	:= crHourGlass;
   if trim(M1.Text)<>'' then begin
     vevokod:= M11.Text;
     V_ARNAP:= Query_Select('VEVO','V_KOD',vevokod,'V_ARNAP');
     if V_ARNAP='0' then begin  // árfolyam: felrakás napja
       // a MEGSEGED felrakás dátumai közül a legkisebb, lehet előrakás is!  (nem kell MS_FAJTA=''normál'' )
       // S:='select min(MS_FELDAT) MIKOR from MEGSEGED where MS_MBKOD = '+IntToStr(StrToIntDef(M1.Text,0));
       S:='select min(case when MS_FETDAT is null then MS_FELDAT else MS_FETDAT end) MIKOR from MEGSEGED where MS_MBKOD = '+IntToStr(StrToIntDef(M1.Text,0));
       end
     else begin
       // a MEGSEGED lerakás dátumai közül a legnagyobb
       // S:= 'select max(MS_LERDAT) from MEGSEGED where MS_MBKOD = '+IntToStr(StrToIntDef(M1.Text,0));
       S:= 'select max(case when MS_LETDAT is null then MS_LERDAT else MS_LETDAT end) from MEGSEGED where MS_MBKOD = '+IntToStr(StrToIntDef(M1.Text,0));
       end;
     Query_run(Query12, S);
     if not Query12.Eof then // ha egyáltalán van már MEGSEGED adatsor
       datum:=Query12.Fields[0].AsString
     else datum:= EgyebDlg.MaiDatum;
     end
   else begin  // még nincs megbízás kód
     datum:= EgyebDlg.MaiDatum;
     end;

   OutRecord:= EgyebDlg.TeljesDijSzamitas_mag(StrToIntDef(M1.Text,0),  // megbízás kód, a csoport többi tagjának díjához
                  datum, vevokod,
                  StringSzam(pnlBecsultKm.Caption), // Össz KM, 0 ha NINCS_ADAT
                  StringSzam(meAtallasKm.Text), // átállás KM, 0 ha NINCS_ADAT
                  StringSzam(M13.text), // Fuvardíj
                  StringSzam(M130.text),  // Egyéb díj
                  StringSzam(M23.text),  // Alvállalkozói díj
                  listavnem[CValuta.ItemIndex], // Fuvardíj devizanem
                  listavnem[CVnem.ItemIndex],  // Egyéb díj devizanem
                  listavnem[CValuta2.ItemIndex]);  // Alvállalkozói díj devizanem

     // -------- megjelenítés ------------ //
     pnlTeljesFuvardij.Caption:= FormatFloat('0.00', OutRecord.teljesdij_EUR);
     if OutRecord.teljesdij_leiras <> '' then begin
        pnlTeljesFuvardij.ShowHint:= True;
        pnlTeljesFuvardij.Hint:= OutRecord.teljesdij_leiras;
        end
     else begin
        pnlTeljesFuvardij.ShowHint:= False;
        end;  // else

     if OutRecord.EURKm > 0 then begin
       if (KategNormaAlso = 0) and (KategNormaFelso = 0) then begin  // nincs norma = ismeretlen kategória
          EURKm_leiras:='Ismeretlen kategória!';
          end
       else begin
          EURKm_leiras:=OutRecord.EURKm_leiras;  // Összesített teljes fuvardíj: az esetleges részmegbízás-társak is szerepelnek
          EURKm_leiras:=EURKm_leiras+'Kategória norma: ';
          EURKm_leiras:=EURKm_leiras + FormatFloat('0.00', KategNormaAlso)+' - '+ FormatFloat('0.00', KategNormaFelso);
          end;  // else
       end
     else begin
        EURKm_leiras:='Az összes km értéke 0!';
       end;  // else

     if pnlBecsultKm.Caption <> NINCS_ADAT then begin
         pnlEURKm.Caption:= FormatFloat('0.000', OutRecord.EURKm);
         pnlEURKm.Hint:= EURKm_leiras;
         pnlEURKmAtallas.Caption:= FormatFloat('0.000', OutRecord.EURKmAtallas);
         pnlEURKmAtallas.Hint:= EURKm_leiras;
         EURKm_Szinezes;
         end
     else begin
         pnlEURKm.Caption:= NINCS_ADAT;
         pnlEURKm.Hint:='';
         pnlEURKmAtallas.Caption:= NINCS_ADAT;
         pnlEURKmAtallas.Hint:='';
         end;
   Screen.Cursor	:= OldCursor;
end;


procedure TMegbizasbeDlg.EURKm_Szinezes;
   procedure ChangePanelColor(P: TPanel; NewColor: TColor);
     begin
        P.Color:=NewColor;
        P.Refresh;
     end;
   procedure Szinezes_mag(Melyik: TPanel);
     var
        EURKm: double;
     begin
     if Trim(Melyik.Caption)= '' then begin
        ChangePanelColor(Melyik, clBtnFace); // szürke, alap
        Exit;  // nincs szinezés
        end;  // if
     EURKm:= StringSzam(Melyik.Caption);
     if (EURKm >= KategNormaAlso) and (EURKm <= KategNormaFelso) then begin  // a tartományban, rendben van
        ChangePanelColor(Melyik, clBtnFace); // szürke, alap
        end
     else begin
      if EURKm < KategNormaAlso then  begin  // kisebb az alsó határnál: vörös
          ChangePanelColor(Melyik, clRed);
          end;
       if EURKm > KategNormaFelso then  begin  // nagyobb a felső határnál: zöld
          ChangePanelColor(Melyik, clLime);
          end;
      end;  // else
    if Melyik.Hint='' then begin
       Melyik.Hint:='Kategória norma: '+FormatFloat('0.00', KategNormaAlso)+' - '+ FormatFloat('0.00', KategNormaFelso);  // az üresfutás % változhatott a számolás óta
      end;  // if
    end;

begin
   Szinezes_mag(pnlEURKm);
   Szinezes_mag(pnlEURKmAtallas);
end;


procedure TMegbizasbeDlg.UtvonalSzamitas;
var
   S: string;
   UTINFO: TUtvonalInfo;
   OldCursor: TCursor;
   MBKOD: integer;
   EUDIJ: double;
begin
   OldCursor:= Screen.Cursor;  // elmentjük
   Screen.Cursor	:= crHourGlass;
   MBKOD:= StrToIntDef(M1.Text,0);
   EUDIJ:= StringSzam(pnlTeljesFuvardij.Caption);
   UTINFO:= EgyebDlg.Megbizas_valtozas (MBKOD, EUDIJ, mbMODOSIT);
   if UTINFO.Hibauzenet='' then begin
        pnlBecsultKM.Caption:= FormatFloat('0',UTINFO.Kilometer);
        meAtallasKm.Text:= FormatFloat('0',UTINFO.Atallas);
        pnlSzamitas.Caption:=UTINFO.Leiras;
        pnlSzamitas.Hint:=UTINFO.Leiras;
        end
    else begin
        pnlBecsultKM.Caption:= NINCS_ADAT;
        meAtallasKm.Text:= NINCS_ADAT;
        pnlSzamitas.Caption:= UTINFO.Hibauzenet;
        pnlSzamitas.Hint:='';
        end;  // else

    TeljesDijSzamitas;  // frissíteni kell az EUR/Km-t is
    Screen.Cursor	:= OldCursor;
end;

procedure TMegbizasbeDlg.BitBtn2Click(Sender: TObject);
begin
	// A sablon dialógus megjelenítése
{
	if not Elkuldes then begin
		Exit;
	end;
	Application.CreateForm(TSablonbeDlg, SablonbeDlg);
 //	SablonbeDlg.Tolto(M1.Text);
	SablonbeDlg.ShowModal;
	SablonbeDlg.Destroy;
            }
end;

procedure TMegbizasbeDlg.CheckBox1Click(Sender: TObject);
begin
(*
	if CheckBox1.Checked then begin
		BitBtn2.Enabled	:= true;
	end else begin
		BitBtn2.Enabled	:= false;
	end;
*)
  Edit1.Visible:=CheckBox1.Checked;
  //ComboBox2.Visible:=not Edit1.Visible and (ret_kod='');

  if CheckBox1.Checked then
   // sablon:='S'
  else
    sablon:='';
end;

procedure TMegbizasbeDlg.M31Exit(Sender: TObject);

begin
 	if M22.Text = '' then begin   // nincs alvállakozó
  	GepkocsiOlvaso;
    end  // if
  else begin
    AlGepkocsiOlvaso;
    end;
  RendszamValtozott;
end;

procedure TMegbizasbeDlg.RendszamValtozott;
var
  MBKOD: integer;
begin
  MBKOD:=StrToIntDef(M1.Text,0);
  if MBKOD>0 then begin
    // el kell menteni, mert a "Megbizas_valtozas" a mentett rendszám alapján keresi a többi részmegbízást
    SQLFuttat('MEGBIZAS', 'UPDATE MEGBIZAS SET MB_RENSZ='''+M31.Text+''' where MB_MBKOD='+IntToStr(MBKOD));
    UtvonalSzamitas;  // a csoport szerkezet változhatott
    end;
end;

procedure TMegbizasbeDlg.GepkocsiOlvaso;
var
	potk	: string;
begin
	if M31.Text <> '' then begin
		{A sofőr beolvasása}
		Query_Run(QueryKoz1,'SELECT * FROM DOLGOZO WHERE DO_RENDSZ = '''+M31.Text+''' AND DO_ARHIV < 1 ',true);
		if QueryKoz1.RecordCount > 0 then begin
			// A sofőr adatok felvitele
			M33.Text	:= Querykoz1.FieldByName('DO_KOD').AsString;
			M34.Text	:= Querykoz1.FieldByName('DO_NAME').AsString;
		end;
		if QueryKoz1.RecordCount > 1 then begin
			// A sofőr adatok felvitele
      Querykoz1.Next;
			M35.Text	:= Querykoz1.FieldByName('DO_KOD').AsString;
			M36.Text	:= Querykoz1.FieldByName('DO_NAME').AsString;
		end;

		// A kapcsolódó pótkocsik beolvasása
		Query_Run(Querykoz1,'SELECT * FROM GEPKOCSI WHERE GK_RESZ = '''+M31.Text+''' ',true);
		if Querykoz1.RecordCount > 0 then begin
			potk        := Query_Select('GEPKOCSI', 'GK_KOD', Querykoz1.FieldByName('GK_POKOD').AsString, 'GK_RESZ');
			if (M32.Text <> '') and (potk<>'') then begin
				if M32.Text <> potk then begin
					if NoticeKi('Figyelem! A gépkocsihoz tartozó pótkocsi nem egyezik meg a jelenlegivel. Átírjuk a mostanit?', NOT_QUESTION) <> 0 then begin
						Exit;
					end;
    			M32.Text	:= potk;
				end;
			end;
			ComboSet (CBGkat, QueryKoz1.FieldByName('GK_GKKAT').AsString, listagkat);
      GKKATEG_valtozott(QueryKoz1.FieldByName('GK_GKKAT').AsString);
		end;
	end;
	PozicioIro;
end;

procedure TMegbizasbeDlg.AlGepkocsiOlvaso;
var
	potk	: string;
begin
	if M31.Text <> '' then begin
		Query_Run(Querykoz1,'SELECT AG_GKKAT FROM ALGEPKOCSI WHERE AG_RENSZ = '''+M31.Text+''' ',true);
		if Querykoz1.RecordCount > 0 then begin
			ComboSet (CBGkat, QueryKoz1.FieldByName('AG_GKKAT').AsString, listagkat);
      GKKATEG_valtozott(QueryKoz1.FieldByName('AG_GKKAT').AsString);
		end;
	end;
end;


procedure TMegbizasbeDlg.Button2Click(Sender: TObject);
begin
	// A legutolsó levél megtekintése
	if M1.Text <> '' then begin
		Application.CreateForm(TLevelbeDlg, LevelbeDlg);
		LevelbeDlg.Tolto('', M1.Text);
       if LevelbeDlg.vanadat then begin
           LevelbeDlg.ShowModal;
       end else begin
			NoticeKi('Még nem küldtünk levelet!');
       end;
       LevelbeDlg.Destroy;
   end;
end;

procedure TMegbizasbeDlg.BitBtn10Click(Sender: TObject);
begin
  Query_Run(Query11,'select ms_id2,ms_tipus from megseged where ms_mbkod='+QueryFelrakoMS_MBKOD.AsString+' and ms_sorsz='+QueryFelrakoMS_SORSZ.AsString);
  if (Query11.RecordCount<2)and(Query11.FieldByname('MS_TIPUS').AsString='1') then  // azonos fel v. lerakó
  begin
      NoticeKi('Erről nem lehet lerakót készíteni!');
      exit;
  end;
	Application.CreateForm(TSegedbe2Dlg,Segedbe2Dlg);
  Segedbe2Dlg.hutos:=(HutosLehet)and( (CheckBox7.Checked)or(CBGkat.Text='24EU'));
	Segedbe2Dlg.Tipusflag	:= 1;
	Segedbe2Dlg.Tolt('Azonos felrakó rögzítése',
	M1.Text, QueryFelrako.FieldByname('MS_SORSZ').AsString, '0');
  Segedbe2Dlg.kellpoz:=kellpoz;
	Segedbe2Dlg.Urito;
	Segedbe2Dlg.ShowModal;
	if Segedbe2Dlg.ret_kod <> '' then begin
		// A felrakók/lerakók beolvasása
		Felrakonyitas;
		QueryFelrako.First;
		modosult	:= true;
		while ( ( not QueryFelrako.EOf ) and (QueryFelrako.FieldByname('MS_SORSZ').AsString <> Segedbe2Dlg.ret_sorsz) ) do begin
			QueryFelrako.Next;
		  end;
    UtvonalSzamitas;
	end;
	Segedbe2Dlg.Destroy;
	GombEllenor;
end;

procedure TMegbizasbeDlg.BitBtn15Click(Sender: TObject);
begin
  Query_Run(Query11,'select ms_id2,ms_tipus from megseged where ms_mbkod='+QueryFelrakoMS_MBKOD.AsString+' and ms_sorsz='+QueryFelrakoMS_SORSZ.AsString);
  if (Query11.RecordCount<2)and(Query11.FieldByname('MS_TIPUS').AsString='0') then  // azonos fel v. lerakó
  begin
      NoticeKi('Erről nem lehet felrakót készíteni!');
      exit;
  end;
	Application.CreateForm(TSegedbe2Dlg,Segedbe2Dlg);
  Segedbe2Dlg.hutos:=(HutosLehet)and( (CheckBox7.Checked)or(CBGkat.Text='24EU'));
	Segedbe2Dlg.Tipusflag	:= -1;
  Segedbe2Dlg.kellpoz:=kellpoz;
	Segedbe2Dlg.Tolt('Azonos lerakó rögzítése',
	M1.Text, QueryFelrako.FieldByname('MS_SORSZ').AsString, '0');
	Segedbe2Dlg.Urito;
	Segedbe2Dlg.ShowModal;
	if Segedbe2Dlg.ret_kod <> '' then begin
		// A felrakók/lerakók beolvasása
		Felrakonyitas;
		QueryFelrako.First;
		modosult	:= true;
		while ( ( not QueryFelrako.EOf ) and (QueryFelrako.FieldByname('MS_SORSZ').AsString <> Segedbe2Dlg.ret_sorsz) ) do begin
			QueryFelrako.Next;
  		end;
    UtvonalSzamitas;
	end;
	Segedbe2Dlg.Destroy;
	GombEllenor;
end;

procedure TMegbizasbeDlg.DSFelrakoDataChange(Sender: TObject;
  Field: TField);
begin
	try
		BitBtn10.Enabled	:= true;
		BitBtn15.Enabled	:= true;
		BitBtn21.Enabled	:= true;
		if StrToIntDef(QueryFelrako.FieldByName('MS_FAJKO').AsString,0) > 0 then begin
			// Nem kellenek az "Azonos..." nyomógombok
			BitBtn10.Enabled	:= false;
			BitBtn15.Enabled	:= false;
			BitBtn21.Enabled	:= false;
		end;
	except
	end;
end;

procedure TMegbizasbeDlg.CHB3Click(Sender: TObject);
begin
	if CHB3.Checked then begin
		MaskEdit4.Enabled	:= true;
	end else begin
		MaskEdit4.Text		:= '';
		MaskEdit4.Enabled	:= false;
	end;
end;

procedure TMegbizasbeDlg.MaskEdit4Exit(Sender: TObject);
begin
	SzamExit(Sender);
  TeljesDijSzamitas;
end;

procedure TMegbizasbeDlg.MaskEdit4KeyPress(Sender: TObject; var Key: Char);
begin
  with Sender as TMaskEdit do begin
		Key := EgyebDlg.Jochar(Text,Key,10,Tag);
  end;
end;

procedure TMegbizasbeDlg.meAtallasKmExit(Sender: TObject);
begin
	SzamExit(Sender);
  TeljesDijSzamitas;
end;

procedure TMegbizasbeDlg.BitBtn17Click(Sender: TObject);
begin
	if MaskEdit5.Text <> '' then begin
		if NoticeKi('Valóban törölni kívánja a fuvartábla adatokat? ', NOT_QUESTION) <> 0 then begin
			Exit;
		end;
	end;
	MaskEdit5.Text	:= '';
	MaskEdit6.Text	:= '';
	MaskEdit7.Text	:= '';
	MaskEdit8.Text	:= '';
end;

procedure TMegbizasbeDlg.BitBtn16Click(Sender: TObject);

var
   lettszurosor: boolean;
   FelrakLerak: TFelrakLerak;
   S, SQL, SQL1, SQL2: string;

   function DarabEllenorzes(SQL, SQL1, SQL2: string): string;
     var
        db1, db2, db12: integer;
     begin
        // hány adatsor lesz a táblában?
        db12:=StrToInt(Query_SelectString('FUVARTABLA', 'select count(*) from FUVARTABLA WHERE '+SQL+' AND '+SQL1+' AND '+SQL2));
        if db12>0 then begin  // jó lesz
           Result:=SQL1+' AND '+SQL2;
           end
        else begin
          db1:=StrToInt(Query_SelectString('FUVARTABLA', 'select count(*) from FUVARTABLA WHERE '+SQL+' AND '+SQL1));  // hány adatsor van külön a felrakóra
          db2:=StrToInt(Query_SelectString('FUVARTABLA', 'select count(*) from FUVARTABLA WHERE '+SQL+' AND '+SQL2));  // hány adatsor van külön a lerakóra
          // az eredmény függvényében visszajelzés
          if (db1>0) and (db2>0) then begin
              NoticeKi('Ehhez a felrakóhoz és lerakóhoz nem azonosítható sor a fuvartáblában! Megjelenítjük a felrakóhoz tartozó összes adatsort.');
              Result:=SQL1;  // hogy ne kelljen a teljes táblából válogatnia
              end;
          if (db1=0) and (db2>0) then begin
              NoticeKi('A felrakóhoz nem azonosítható sor a fuvartáblában! Megjelenítjük az összes felrakót.');
              Result:=SQL2;
              end;
          if (db1>0) and (db2=0) then begin
              NoticeKi('A lerakóhoz nem azonosítható sor a fuvartáblában! Megjelenítjük az összes lerakót.');
              Result:=SQL1;
              end;
          if (db1=0) and (db2=0) then begin
              NoticeKi('Sem a felrakóhoz, sem a lerakóhoz nem azonosítható sor a fuvartáblában! Megjelenítjük az összes adatsort.');
              Result:='';
              end;
          end  // else
     end;

begin
	// A FUVARTABLA.ID beolvasása
  //jasza:= Query1.FieldByName('MB_JAKOD').AsString  ;

  lettszurosor:= False;
	Application.CreateForm(TFuvartablaFmDlg, FuvartablaFmDlg);

	FuvartablaFmDlg.valaszt := true;
  // fuvartábla előszűrése felrakó és lerakó alapján
  if ret_kod<>'' then begin
    FelrakLerak:= EgyebDlg.GetFelrakoLerako(StrToInt(ret_kod));
    if FelrakLerak.Ervenyes then begin
      SQL:='FT_TOL=(select max(FT_TOL) from FUVARTABLA where FT_TOL<='''+FelrakLerak.FelDatum+''') ';  // a megfelelő időszak
      FuvartablaFmDlg.alapstr:= FuvartablaFmDlg.alapstr + ' WHERE '+SQL;

      S:= FelrakLerak.FelOrszag+'('+FelrakLerak.FelIranyito+')';
      SQL1:='SUBSTRING('''+S+''',1,DATALENGTH(FT_KEZDO)-1)=SUBSTRING(FT_KEZDO,1,DATALENGTH(FT_KEZDO)-1) ';
      S:= FelrakLerak.LeOrszag+'('+FelrakLerak.LeIranyito+')';
      SQL2:='SUBSTRING('''+S+''',1,DATALENGTH(FT_VEGZO)-1)=SUBSTRING(FT_VEGZO,1,DATALENGTH(FT_VEGZO)-1) ';
      EgyebDlg.SetSzuroStr('188', DarabEllenorzes(SQL, SQL1, SQL2));  // 188: a TFuvartablaFmDlg tag-je!!!
      lettszurosor:= True;
      // FuvartablaFmDlg.alapstr:= FuvartablaFmDlg.alapstr + ' AND '+SQL;
      end; // if Ervenyes
    end;  // if ret_kod
  // fejléces szűrés törlése, ha van
  FuvartablaFmDlg.SzuresUrites;
  FuvartablaFmDlg.SQL_ToltoFo(FuvartablaFmDlg.GetOrderBy(true));  // a dátum + kezdő-végző szűrés érvényesítéséhez
	// Ha van kitöltött LAN-ID, ugorjunk rá.
	if MaskEdit8.Text	<> '' then begin
		FuvartablaFmDlg.Query1.Locate('FT_FTKOD', MaskEdit8.Text, [] );
	  end;
	FuvartablaFmDlg.ShowModal;
  if lettszurosor then begin      // levesszük a plusz sort
      GridTorles(EgyebDlg.SzuroGrid, EgyebDlg.SzuroGrid.RowCount - 1);
      end;
	if FuvartablaFmDlg.ret_vkod <> '' then begin
		Query_Run(Query11,'SELECT * FROM FUVARTABLA WHERE FT_FTKOD = '''+FuvartablaFmDlg.ret_vkod+''' ',true);
		if Query11.RecordCount > 0 then begin
			MaskEdit5.Text	:= Query11.FieldByName('FT_LANID').AsString;
			MaskEdit7.Text	:= Query11.FieldByName('FT_KATEG').AsString;
			MaskEdit6.Text	:= Query11.FieldByName('FT_DESCR').AsString;
			MaskEdit8.Text	:= Query11.FieldByName('FT_FTKOD').AsString;
			// A fuvardíj betöltése
			if StringSzam( M13.Text	) = 0 then begin
				// Beírjuk a megfelelő adatokat
				M13.Text	:= Query11.FieldByName('FT_FTEUR').AsString;
				ComboSet (CValuta2, 'EUR', listavnem);
			end else begin
				// Összehasonlítjuk a meglévővel
        efuvardij:=M13.Text;
				if StringSzam( M13.Text	) <> StringSzam( Query11.FieldByName('FT_FTEUR').AsString ) then begin
					if NoticeKi('Figyelem! A kiválasztott fuvardíj eltér a jelenlegitől. Kicseréljem az értékeket? ', NOT_QUESTION) = 0 then begin
						M13.Text	:= Query11.FieldByName('FT_FTEUR').AsString;
						ComboSet (CValuta2, 'EUR', listavnem);
					end;
          // if (jakod<>'')and( Query_Select('VISZONY','VI_JAKOD',jakod,'VI_UJKOD')<>'') then begin
          if M1.Text <> '' then begin
            if (VanHozzaIgaziSzamla(M1.Text) <> '') then begin
              NoticeKi('A megbízáshoz számla tartozik, ezért a fuvardíj nem módosítható! Először törölje a megbízást a járatból.')  ;
              M13.Text:=efuvardij;
              end;
            end;
        //  M13Exit(self);
				end;
			end;
		end;
	end;
  EgyebDlg.SetSzuroStr('188', '');  // töröljük a szűrést, 188: a TFuvartablaFmDlg tag-je!!!
	FuvartablaFmDlg.Destroy;
end;

procedure TMegbizasbeDlg.PozicioIro;
var
	pozic 	: RPozicio;
begin
	// A pozíció kiírása
	GetLocation(M31.Text, pozic);
	MaskEdit1.Text	:= pozic.location;
	MaskEdit2.Text	:= pozic.datum;
	MaskEdit3.Text	:= 'Seb:'+IntToStr(pozic.sebesseg)+' '+pozic.digit;
end;

procedure TMegbizasbeDlg.BitBtn18Click(Sender: TObject);
var
  rendsz,rajt,cel,rc:string;
begin
  rc:='0';
  if QueryFelrakoMS_FETIDO.Value<>'' then rc:='1';
  if QueryFelrakoMS_LETIDO.Text<>'' then rc:='2';

  rajt:=QueryFelrakoMS_FORSZ.AsString;
  rajt:=rajt+', '+QueryFelrakoMS_FELIR.AsString;
  rajt:=rajt+', '+QueryFelrakoMS_FELTEL.AsString;
  rajt:=rajt+', '+QueryFelrakoMS_FELCIM.AsString;

  cel:=QueryFelrakoMS_ORSZA.AsString;
  cel:=cel+', '+QueryFelrakoMS_LELIR.AsString;
  cel:=cel+', '+QueryFelrakoMS_LERTEL.AsString;
  cel:=cel+', '+QueryFelrakoMS_LERCIM.AsString;

  rendsz:=QueryFelrakoMS_RENSZ.Value;
  //if QueryFelrakoMS_RENSZ.Value<>'' then
   EgyebDlg.GoogleMap(rendsz,rajt,cel,rc,False)     ;
end;

procedure TMegbizasbeDlg.CheckBox5Click(Sender: TObject);
begin
  if CheckBox5.Checked then
  begin
    M14.Text:='Non SAP';
	  SetMaskEdits([M14]);
  end
  else
  begin
	  SetMaskEdits([M14],1);
  end;
end;

procedure TMegbizasbeDlg.CheckBox6Click(Sender: TObject);
const
   egyedi_szoveg = 'Rész / egyedi díj! ';
begin
   if CheckBox6.Checked then begin
      M7.text:= egyedi_szoveg+M7.text;
      end
   else begin
      if pos(egyedi_szoveg, M7.text) = 1 then  // ezzel kezdődik, ahogy az automata berakta
        M7.text:= copy(M7.text, length(egyedi_szoveg)+1, 99999);
      end;
end;

procedure TMegbizasbeDlg.DBGrid5DblClick(Sender: TObject);
begin
  if BitBtn31.Visible and BitBtn31.Enabled then
    BitBtn31.OnClick(self);
end;

procedure TMegbizasbeDlg.BitBtn19Click(Sender: TObject);
var
  nap:integer;
  dat:Tdate;
begin
  if M31.Text=EmptyStr then exit;
  nap:=0;
  dat:=date;
  if QueryFelrako.Active then
  begin
   QueryFelrako.First;
   while not QueryFelrako.Eof do
   begin
    if QueryFelrakoMS_LETDAT.AsDateTime>dat then
      dat:=QueryFelrakoMS_LETDAT.AsDateTime;
    QueryFelrako.Next;
   end;
  end;
  if dat>date then
    nap:=Trunc(dat-date);
  //FGKErvenyesseg.Ervenyesseg(M31.Text,nap) ;
  //if FGKErvenyesseg.Ervenyesseg(M31.Text,nap,PLUSZNAP)or(QueryFelrako.Active) then
  //if FGKErvenyesseg.Ervenyesseg(M31.Text,nap,PLUSZNAP) then
  FGKErvenyesseg.Ervenyesseg(M31.Text,nap,PLUSZNAP);
  FGKErvenyesseg.ShowModal;

end;

procedure TMegbizasbeDlg.BitBtn20Click(Sender: TObject);
var
  nap:integer;
  dat:Tdate;
begin
  if M32.Text=EmptyStr then exit;
  nap:=0;
  dat:=date;
  if QueryFelrako.Active then
  begin
   QueryFelrako.First;
   while not QueryFelrako.Eof do
   begin
    if QueryFelrakoMS_LETDAT.AsDateTime>dat then
      dat:=QueryFelrakoMS_LETDAT.AsDateTime;
    QueryFelrako.Next;
   end;
  end;
  if dat>date then
    nap:=Trunc(dat-date);
  //FGKErvenyesseg.Ervenyesseg(M32.Text,nap) ;
  //if (FGKErvenyesseg.Ervenyesseg(M32.Text,nap,PLUSZNAP) or QueryFelrako.Active)  then
  FGKErvenyesseg.Ervenyesseg(M32.Text,nap,PLUSZNAP);
  FGKErvenyesseg.ShowModal;

end;

procedure TMegbizasbeDlg.SpeedButton4Click(Sender: TObject);
var
  er,ur:TBookmark;
  esorszam, usorszam:integer;
  mbkod    : string;
begin

  DBGrid5.SetFocus;
  mbkod:= QueryFelrakoMS_MBKOD.AsString;
  if (QueryFelrako.RecNo=1) then   exit;
  er:=QueryFelrako.GetBookmark;
  esorszam:=QueryFelrakoMS_SORSZ.Value;
  QueryFelrako.Prior ;
  if QueryFelrako.Bof then exit;

 Try
  usorszam:=QueryFelrakoMS_SORSZ.Value;
  if not Query_Run(Query11,'update megseged set ms_sorsz='+'-1'+' where ms_mbkod='+mbkod+' and ms_sorsz='+IntToStr(usorszam)) then Raise Exception.Create('');
  ur:=QueryFelrako.GetBookmark;
  QueryFelrako.GotoBookmark(er);
  if not Query_Run(Query11,'update megseged set ms_sorsz='+IntToStr(usorszam)+' where ms_mbkod='+mbkod+' and ms_sorsz='+IntToStr(esorszam)) then Raise Exception.Create('');
  if not Query_Run(Query11,'update megseged set ms_sorsz='+IntToStr(esorszam)+' where ms_mbkod='+mbkod+' and ms_sorsz='+'-1') then Raise Exception.Create('');
 Except
 End;
  QueryFelrako.Close;
  QueryFelrako.Open;
  QueryFelrako.GotoBookmark(ur);

end;

procedure TMegbizasbeDlg.SpeedButton5Click(Sender: TObject);
var
  er,ur:TBookmark;
  esorszam, usorszam:integer;
  mbkod: string;
begin
  DBGrid5.SetFocus;
  mbkod:= QueryFelrakoMS_MBKOD.AsString;
  if (QueryFelrako.RecNo=QueryFelrako.RecordCount) then   exit;
  er:=QueryFelrako.GetBookmark;
  esorszam:=QueryFelrakoMS_SORSZ.Value;
  QueryFelrako.next ;
  if QueryFelrako.eof then exit;

 Try
  usorszam:=QueryFelrakoMS_SORSZ.Value;
  if not Query_Run(Query11,'update megseged set ms_sorsz='+'-1'+' where ms_mbkod='+mbkod+' and ms_sorsz='+IntToStr(usorszam)) then Raise Exception.Create('');
{  QueryFelrako.Edit;
  QueryFelrakoMS_SORSZ.Value:=esorszam;
  QueryFelrako.Post;   }
  ur:=QueryFelrako.GetBookmark;
  QueryFelrako.GotoBookmark(er);
  if not Query_Run(Query11,'update megseged set ms_sorsz='+IntToStr(usorszam)+' where ms_mbkod='+mbkod+' and ms_sorsz='+IntToStr(esorszam)) then Raise Exception.Create('');
  if not Query_Run(Query11,'update megseged set ms_sorsz='+IntToStr(esorszam)+' where ms_mbkod='+mbkod+' and ms_sorsz='+'-1') then Raise Exception.Create('');
{  QueryFelrako.Edit;
  QueryFelrakoMS_SORSZ.Value:=usorszam;
  QueryFelrako.Post;  }
 Except
 End;

  QueryFelrako.Close;
  QueryFelrako.Open;
  QueryFelrako.GotoBookmark(ur);

end;

procedure TMegbizasbeDlg.DBGrid5DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
var
  tido, aido : TdateTime;
  nemteljesult : Boolean;
begin
{
	stkod	:= '';
	try
		stkod	:= QueryFelrakoMS_STATK.Value;
	except
	end;
	if stkod <> '' then begin
		i := 1;
		while i <= 10 do begin
			if statusszin[i, 1] = stkod then begin
				szin	:= StrToIntDef(statusszin[i, 2], clAqua);
				i		:= 10;
			end;
      Inc(i);
		end;
   end;
	 // Ha felrakott exportunk van és ez lerakás és nincs lerakási dátum, akkor a szín = szabad export = 110
   if ( ( stkod = '130' ) and (QueryFelrakoMS_TIPUS.AsString = '1' ) and (QueryFelrakoMS_LETIDO.AsString = '' ) ) then begin
		stkod := '110';
		i := 1;
		while i <= 10 do begin
			if statusszin[i, 1] = stkod then begin
				szin	:= StrToIntDef(statusszin[i, 2], clAqua);
				i		:= 10;
			end;
			Inc(i);
		end;
	end;
	if ( ( stkod = '230' ) and (QueryFelrakoMS_TIPUS.AsString = '1' ) and (QueryFelrakoMS_LETIDO.AsString = '' ) ) then begin
		stkod := '210';
		i := 1;
		while i <= 10 do begin
			if statusszin[i, 1] = stkod then begin
				szin	:= StrToIntDef(statusszin[i, 2], clAqua);
				i		:= 10;
			end;
			Inc(i);
		end;
	end;
	DBGrid5.Canvas.Brush.Color		:= szin;
//	DBGrid5.DefaultDrawDataCell(Rect, Column.Field, State);
 //	alapszin						:= szin;

  DBGrid5.DefaultDrawColumnCell(Rect,DataCol,Column , State);
 }

  ///////////////////////       ha nem történt meg időben a fel-lerakás, akkor piros lesz a betűszín
  nemteljesult:=False;
  if (QueryFelrakoMS_FETIDO.Value = '')and(QueryFelrakoMS_FERKIDO.Value = '') then     // nincs kitöltve a tény idő
  begin
    if QueryFelrakoMS_FELIDO2.Value <> '' then
      if QueryFelrakoMS_FELDAT2.Value <> '' then
        tido:= StrToDateTimeDef(QueryFelrakoMS_FELDAT2.Value+' '+QueryFelrakoMS_FELIDO2.Value+':00',now)
      else
        tido:= StrToDateTimeDef(QueryFelrakoMS_FELDAT.Value+' '+QueryFelrakoMS_FELIDO2.Value+':00',now)
    else
      tido:= StrToDateTimeDef(QueryFelrakoMS_FELDAT.Value+' '+QueryFelrakoMS_FELIDO.Value+':00',now);

  {
    ido:= QueryFelrakoMS_FELIDO.Value;
    if QueryFelrakoMS_FELIDO2.Value <> '' then
      ido:= QueryFelrakoMS_FELIDO2.Value;
    tido:= StrToDateTimeDef(QueryFelrakoMS_FELDAT.Value+' '+ido+':00',now);  }

    aido:= Now;
    nemteljesult:= nemteljesult OR (aido > tido) ;
  end;
  if (QueryFelrakoMS_LETIDO.Value = '')and(QueryFelrakoMS_LERKIDO.Value = '') then     // nincs kitöltve a tény idő
  begin
    if QueryFelrakoMS_LERIDO2.Value <> '' then
      if QueryFelrakoMS_LERDAT2.Value <> '' then
        tido:= StrToDateTimeDef(QueryFelrakoMS_LERDAT2.Value+' '+QueryFelrakoMS_LERIDO2.Value+':00',now)
      else
        tido:= StrToDateTimeDef(QueryFelrakoMS_LERDAT.Value+' '+QueryFelrakoMS_LERIDO2.Value+':00',now)
    else
      tido:= StrToDateTimeDef(QueryFelrakoMS_LERDAT.Value+' '+QueryFelrakoMS_LERIDO.Value+':00',now);
{
    ido:= QueryFelrakoMS_LERIDO.Value;
    if QueryFelrakoMS_LERIDO2.Value <> '' then
      ido:= QueryFelrakoMS_LERIDO2.Value;
    tido:= StrToDateTimeDef(QueryFelrakoMS_LERDAT.Value+' '+ido+':00',now);          }

    aido:= Now;
    nemteljesult:= nemteljesult OR (aido > tido) ;
  end;

  if nemteljesult then
    	DBGrid5.Canvas.Font.Color		:= clRed
  else
    	DBGrid5.Canvas.Font.Color		:= clBlack;
  //////////////////////
  DBGrid5.DefaultDrawColumnCell(Rect,DataCol,Column , State);

end;

procedure TMegbizasbeDlg.BitBtn22Click(Sender: TObject);
var
	dir0	: string;
begin
  if M1.Text='' then
  begin
		NoticeKi('Nincs Megbízáskód!');
    exit;
  end;
	dir0	:= EgyebDlg.Read_SZGrid('999', '725');
	if dir0 = '' then begin
			NoticeKi('A "Megbízás" könyvtár nincs megadva!!');
			Exit;
	end else begin
		if not DirectoryExists(dir0) then begin
			NoticeKi('Nem létező alapkönyvtár : '+dir0);
			Exit;
		end;
	end;
	// A DIR0 egy létező könyvtár
	if ( ( copy(dir0, Length(dir0), 1) <> '\' ) and ( copy(dir0, Length(dir0), 1) <> '/' ) ) then begin
		dir0	:= dir0 + '\';
	end;
	// A név hozzárakása a dir0 -hoz
	dir0	:= dir0 + M1.text;
	if not DirectoryExists(dir0) then begin
    if not CreateDir(dir0) then
    begin
		  NoticeKi('Nem létező  Megbízás könyvtár : "'+dir0+'"');
		  Exit;
    end;
	end;
//	if (not DirectoryExists(dir0+'\Archiv'))and(not DirectoryExists(dir0+'\Archív')) then begin
{	if (not DirectoryExists(dir0+'\Archív')) then begin
    if not CreateDir(dir0+'\Archív') then
    begin
		  NoticeKi('Nem létező  Archív könyvtár : "'+dir0+'\Archív'+'"');
		 // Exit;
    end;
	end;      }
	// Nyissuk meg a könyvtárat
	ShellExecute(Application.Handle,'open',PChar(dir0), '', '', SW_SHOWNORMAL );

end;

function TMegbizasbeDlg.PDFMentes: boolean;
var
	dir0	: string;
	aPDFFilt    : TQRPDFDocumentFilter;
	fn          : string;
  sorsz: integer       ;
begin
  // könyvtár ellenőrzése
   Result  := false;
	dir0	:= EgyebDlg.Read_SZGrid('999', '725');
	if dir0 = '' then begin
			NoticeKi('A "Megbízás" könyvtár nincs megadva!!');
			Exit;
	end else begin
		if not DirectoryExists(dir0) then begin
			NoticeKi('Nem létező alapkönyvtár : '+dir0);
			Exit;
		end;
	end;
	// A DIR0 egy létező könyvtár
	if ( ( copy(dir0, Length(dir0), 1) <> '\' ) and ( copy(dir0, Length(dir0), 1) <> '/' ) ) then begin
		dir0	:= dir0 + '\';
	end;
	// A név hozzárakása a dir0 -hoz
	dir0	:= dir0 + M1.text;
	if not DirectoryExists(dir0) then begin
    if not CreateDir(dir0) then
    begin
		  NoticeKi('Nem létező  Megbízás könyvtár : "'+dir0+'"');
		  Exit;
    end;
	end;
//	if (not DirectoryExists(dir0+'\Archiv'))and(not DirectoryExists(dir0+'\Archív')) then begin
 {	if (not DirectoryExists(dir0+'\Archív')) then begin
    if not CreateDir(dir0+'\Archív') then
    begin
		  NoticeKi('Nem létező  Archív könyvtár : "'+dir0+'\Archív'+'"');
		 // Exit;
    end;
	end;  }
  //////////////////////////////

	sorsz			:= 2;
	fn				:= 'MB_Visszaigazolás' + '.PDF';
	// Az állomány nevének ellenőrzése
	while FileExists(dir0+'\'+fn) do begin
		fn	:= 'MB_Visszaigazolás'+IntToStr(sorsz) + '.PDF';
		Inc(sorsz);
	end;
	aPDFFilt 				:= TQRPDFDocumentFilter.Create(dir0+'\'+fn);
	aPDFFilt.CompressionOn 	:= true;

	FuviszliDlg.Rep.ExportToFilter(aPDFFilt);
  Result:=True;
  PDFFILE:=dir0+'\'+fn;
  if not FileExists(dir0+'\'+fn) then begin
		  NoticeKi('Figyelem! A PDF állomány létrehozása nem sikerült!');
      Result:=False;
		 // Exit;
	end;

end;

procedure TMegbizasbeDlg.pnlSzamitasClick(Sender: TObject);
begin
  ShowInfoString(pnlSzamitas.Caption);
end;

procedure TMegbizasbeDlg.BitBtn29Click(Sender: TObject);
var
  FAX, EMAIL, body : string;
begin
  if M1.Text='' then
  begin
		NoticeKi('Nincs Megbízáskód!');
    exit;
  end;
  //////////////////////////////
  FAX:=M120.Text;
  if FAX<>'' then
  begin
   Try
    Clipboard.Clear;
    Clipboard.AsText:=FAX;
    NoticeKi('FAX szám a vágólapra téve. CTRL-V (Beillesztés) billentyű leütéssel beilleszthető.');
   Except
   End;
  end;
	//A Fuvarozási megbízás nyomtatása
	EgyebDlg.kelllistagomb	:= true;
	EgyebDlg.ListaGomb	   	:= 'Email küldés';

  Application.CreateForm(TFuviszliDlg, FuviszliDlg);
  FuviszliDlg.STORNO:=False;
  FuviszliDlg.VKOD:=M11.Text;
  FuviszliDlg.VPOZ:=M14.Text;
  FuviszliDlg.GKRENDSZ:=M31.Text;
  FuviszliDlg.GKKAT:=CBGkat.Text;
  FuviszliDlg.FDIJ:=M13.Text+' '+CValuta.Text;
  FuviszliDlg.USERNAME:=Query1.FieldByName('MB_FENEV').AsString;

	FuviszliDlg.Tolt(M1.Text);
	FuviszliDlg.Rep.Preview;
//	FuviszliDlg.Destroy;
	if EgyebDlg.ListaValasz = 1 then begin
		// Itt van az elküldés
    //STORNOKULDVE:=True;
    if PDFMentes then
    begin
      EMAIL:=Query_Select('VEVO','V_KOD',M11.Text,'VE_EMAIL');
    	if EMAIL = '' then begin
		    // A vevő emailcímének megadása
    		EMAIL := InputBox('Email cím megadása', 'A vevő email címe : ', '');
		    if EMAIL <> '' then begin
    			// A cím visszaírása
		    	Query_Run(EgyebDlg.QueryKozos, 'UPDATE VEVO SET VE_EMAIL = '''+copy(EMAIL, 1, 60)+''' WHERE V_KOD = '''+M11.Text+''' ', FALSE);
    		end;
    	end;
    	body	:= 'Tisztelt Cím,'+Chr(13)+Chr(10)+Chr(13)+Chr(10)+'Mellékelten küldjük a fuvarozási megbízás visszaigazolását.';
    	// Email küldése
	    CreateOutlookEmailAndOpen(EMAIL, 'Fuvarozási megbízás visszaigazolás', body, PDFFILE);

    end;
{    if not FileExists(dir0+'\'+fn) then begin
		  NoticeKi('Figyelem! A PDF állomány létrehozása nem sikerült!');
		  Exit;
	  end;
 }
	end;
	FuviszliDlg.Destroy;

end;

procedure TMegbizasbeDlg.M130Enter(Sender: TObject);
begin
  esedij:= M130.Text;
end;

procedure TMegbizasbeDlg.M130Exit(Sender: TObject);
var
    S, SQLText, Ertek: string;
    SEDIJ: double;
    Megvan, KellSzamolas, VanJoga: boolean;
begin
	SzamExit(M130);
  KellSzamolas:= True;
  Megvan:= False;
  // volt egy furcsa hiba: "Query1: Field 'MB_SEDIJ' not found."
  try
    SEDIJ:= Query1.FieldByName('MB_SEDIJ').AsFloat;
    Megvan:= True;
  except
    SQLText:= trim(Query1.SQL.Text);
    if SQLText <> '' then begin
      S:= 'A lekérdezés adattartalma egészen váratlan. Kérlek értesítsd a fejlesztőt! SQL ='+ SQLText;
      NoticeKi(S);
      HibaKiiro(S);
      end;  // if
    end;  // try-except
  if Megvan then begin
    if (ret_kod<>'')and(StrToFloatDef( M130.Text,0) <> StrToFloatDef( esedij,0)) then begin
      // if (jakod<>'') and ( Query_Select('VISZONY','VI_JAKOD',jakod,'VI_UJKOD')<>'') then begin
      if M1.Text <> '' then begin
        Ertek:= VedettMezoValtozas(esedij, M130.Text, KellSzamolas);
        if Ertek <> M130.Text then
            M130.Text:= Ertek;
       { if (VanHozzaIgaziSzamla(M1.Text) <> '') then begin
          VanJoga:= (GetRightTag(598, False) <> RG_NORIGHT);
           if VanJoga then begin
              if NoticeKi('A megbízáshoz számla tartozik! Biztosan módosítani akarja az egyéb díjat?', NOT_QUESTION ) = 0 then begin
                 KellSzamolas:= True;
                 end
              else begin
                 M130.Text:=esedij;
                 KellSzamolas:= False;
                 end;
              end
           else begin
              NoticeKi('A megbízáshoz számla tartozik, ezért a fuvardíj csak "megbízás adminisztrátor" jogkörrel módosítható!');
              M130.Text:=esedij;
              KellSzamolas:= False;
              end;
          end;  // if }
        end;
      end;
    end; // if Megvan
  if KellSzamolas then
    TeljesDijSzamitas;
end;

function TMegbizasbeDlg.VedettMezoValtozas(RegiErtek, UjErtek: string; var KellSzamolas: boolean): string;
var
  SzamlaInfo: string;
  VanJoga: boolean;
begin
 Result:= UjErtek;  // alapértelmezett az, hogy nem változik
 SzamlaInfo:= VanHozzaIgaziSzamla(M1.Text);
 if (SzamlaInfo <> '') then begin
    VanJoga:= (GetRightTag(598, False) <> RG_NORIGHT);
     if VanJoga then begin
        if NoticeKi('A megbízáshoz számla tartozik ('+SzamlaInfo+')! Biztosan módosítani akarja ezt az adatot?', NOT_QUESTION ) = 0 then begin
           KellSzamolas:= True;
           end
        else begin
           Result:=RegiErtek;
           KellSzamolas:= False;
           end;
        end // if van joga
     else begin
        NoticeKi('A megbízáshoz számla tartozik ('+SzamlaInfo+'), ezért ez az adat csak "megbízás adminisztrátor" jogkörrel módosítható!');
        Result:=RegiErtek;
        KellSzamolas:= False;
        end;  // else: nem volt joga
    end  // if SzamlaInfo <> ''
 else begin  // nincs véglegesített számla, de attól nem véglegesített lehet: azt töröljük
    LezaratlanSzamlaTorles(M1.Text);
    end;

end;


procedure TMegbizasbeDlg.M13Enter(Sender: TObject);
begin
  efuvardij:= M13.Text;
end;

procedure TMegbizasbeDlg.M13Exit(Sender: TObject);
var
//  jasza:string;
    S, SQLText, Ertek: string;
    FUDIJ: double;
    Megvan, KellSzamolas, VanJoga: boolean;
begin
	SzamExit(M13);
  KellSzamolas:= True;
  Megvan:= False;
  // volt egy furcsa hiba: "Query1: Field 'MB_FUDIJ' not found."
  try
    FUDIJ:= Query1.FieldByName('MB_FUDIJ').AsFloat;
    Megvan:= True;
  except
    SQLText:= trim(Query1.SQL.Text);
    if SQLText <> '' then begin
      S:= 'A lekérdezés adattartalma egészen váratlan. Kérlek értesítsd a fejlesztőt! SQL ='+ SQLText;
      NoticeKi(S);
      HibaKiiro(S);
      end;  // if
    end;  // try-except
  if Megvan then begin
    if (ret_kod<>'')and(StrToFloatDef( M13.Text,0) <> StrToFloatDef( efuvardij,0)) then begin
      // if (jakod<>'') and ( Query_Select('VISZONY','VI_JAKOD',jakod,'VI_UJKOD')<>'') then begin
      if M1.Text <> '' then begin
        Ertek:= VedettMezoValtozas(efuvardij, M13.Text, KellSzamolas);
        if Ertek <> M13.Text then
            M13.Text:= Ertek;
        {if (VanHozzaIgaziSzamla(M1.Text) <> '') then begin
           VanJoga:= (GetRightTag(598, False) <> RG_NORIGHT);
           if VanJoga then begin
              if NoticeKi('A megbízáshoz számla tartozik! Biztosan módosítani akarja a fuvardíjat?', NOT_QUESTION ) = 0 then begin
                 KellSzamolas:= True;
                 end
              else begin
                 M13.Text:=efuvardij;
                 KellSzamolas:= False;
                 end;
              end
           else begin
              NoticeKi('A megbízáshoz számla tartozik, ezért a fuvardíj csak "megbízás adminisztrátor" jogkörrel módosítható!');
              M13.Text:=efuvardij;
              KellSzamolas:= False;
              end; // else (nincs joga)
          end  // van hozzá számla
        else begin // nincs hozzá számla
          // ModosultDijViszonyba(ret_kod);
          end; } // else = nincs hozzá számla
        end;
      end;
    end; // if Megvan
  if KellSzamolas then
    TeljesDijSzamitas;
end;

function TMegbizasbeDlg.SablonMentes(embkod,mbkod, snev: string): Boolean;
var
  S: string;
begin
  Try
   if embkod<>'' then
   begin
  	// SQLCommand.CommandText:='DELETE FROM SABLON where SB_MBKOD='+embkod;
    // SQLCommand.Execute;
    S:='DELETE FROM SABLON where SB_MBKOD='+embkod;
    Command_Run(SQLCommand, S, true);
  	// SQLCommand.CommandText:='DELETE FROM MEGBIZAS2 where MB_MBKOD='+embkod;
    // SQLCommand.Execute;
    S:='DELETE FROM MEGBIZAS2 where MB_MBKOD='+embkod;
    Command_Run(SQLCommand, S, true);
  	// SQLCommand.CommandText:='DELETE FROM MEGSEGED2 where ms_mbkod='+embkod;
    // SQLCommand.Execute;
    S:='DELETE FROM MEGSEGED2 where ms_mbkod='+embkod;
    Command_Run(SQLCommand, S, true);
   end;
  	// SQLCommand.CommandText:='DELETE FROM SABLON where SB_MBKOD='+mbkod;
    // SQLCommand.Execute;
    S:='DELETE FROM SABLON where SB_MBKOD='+mbkod;
    Command_Run(SQLCommand, S, true);
  	// SQLCommand.CommandText:='DELETE FROM MEGBIZAS2 where MB_MBKOD='+mbkod;
    // SQLCommand.Execute;
    S:='DELETE FROM MEGBIZAS2 where MB_MBKOD='+mbkod;
    Command_Run(SQLCommand, S, true);

  	// SQLCommand.CommandText:='DELETE FROM MEGSEGED2 where ms_mbkod='+mbkod;
    // SQLCommand.Execute;
    S:='DELETE FROM MEGSEGED2 where ms_mbkod='+mbkod;
    Command_Run(SQLCommand, S, true);

  	// SQLCommand.CommandText:='INSERT INTO SABLON (SB_MBKOD,SB_MEGJE) VALUES ('+mbkod+','+  ''''+snev+''')';
    // SQLCommand.Execute;
    S:='INSERT INTO SABLON (SB_MBKOD,SB_MEGJE) VALUES ('+mbkod+','+  ''''+snev+''')';
    Command_Run(SQLCommand, S, true);

    // ---- ez így volt NagyP -----
  	//SQLCommand.CommandText:='UPDATE MEGBIZAS set MB_SABLO='''+'S'''+' where mb_mbkod='''+mbkod+'''';
    //SQLCommand.Execute;
    //SQLCommand.CommandText:='insert MEGBIZAS2 select * from MEGBIZAS where mb_mbkod='''+mbkod+'''';
    // ---- ez így volt NagyP -----

    // SQLCommand.CommandText:='insert MEGBIZAS2 select * from MEGBIZAS where mb_mbkod='+mbkod;
    // SQLCommand.Execute;
    S:='insert MEGBIZAS2 select * from MEGBIZAS where mb_mbkod='+mbkod;
    Command_Run(SQLCommand, S, true);

    Query_Run(Query11, 'insert MEGSEGED2 (MS_AKTCIM, MS_AKTIR, MS_AKTNEV, MS_AKTOR, MS_AKTTEL, MS_AZTIP, MS_CSPAL, MS_DARU, MS_DATUM, MS_DOKO2, MS_DOKOD, MS_EXPOR, MS_EXSTR, MS_FAJKO, MS_FAJTA, '+
       'MS_FCPAL, MS_FDARU, MS_FELARU, MS_FELCIM, MS_FELDAT, MS_FELDAT2, MS_FELIDO, MS_FELIDO2, MS_FELIR, MS_FELNEV, MS_FELSE1, MS_FELSE2, MS_FELSE3, MS_FELSE4, MS_FELSE5, MS_FELTEL, MS_FEPAL, '+
       'MS_FERKDAT, MS_FERKIDO, MS_FETDAT, MS_FETED, MS_FETED2, MS_FETEI, MS_FETEI2, MS_FETIDO, MS_FORSZ, MS_FPAFEL, MS_FPAIGAZL, MS_FPAIGAZT, MS_FPALER, MS_FPAMEGJ, MS_FSULY, MS_GKKAT, MS_HFOK1,'+
       'MS_HFOK2, MS_ID, MS_ID2, MS_IDOPT, MS_JAKOD, MS_JASZA, MS_KESES, MS_KIFON, MS_LCPAL, MS_LDARU, MS_LELIR, MS_LEPAL, MS_LERCIM, MS_LERDAT, MS_LERDAT2, MS_LERIDO, MS_LERIDO2, MS_LERKDAT, '+
       'MS_LERKIDO, MS_LERNEV, MS_LERSE1, MS_LERSE2, MS_LERSE3, MS_LERSE4, MS_LERSE5, MS_LERTEL, MS_LETDAT, MS_LETED, MS_LETED2, MS_LETEI, MS_LETEI2, MS_LETIDO, MS_LOADID, MS_LPAFEL, MS_LPAIGAZL, '+
       'MS_LPAIGAZT, MS_LPALER, MS_LPAMEGJ, MS_LSULY, MS_M_MAIL, MS_MBKOD, MS_MSKOD, MS_NORID, MS_ORSZA, MS_POTSZ, MS_REGIK, MS_RENSZ, MS_S_MAIL, MS_SNEV1, MS_SNEV2, MS_SORSZ, MS_STATK, MS_STATN, '+
       'MS_TAVKM, MS_TEFCIM, MS_TEFIR, MS_TEFL1, MS_TEFL2, MS_TEFL3, MS_TEFL4, MS_TEFL5, MS_TEFNEV, MS_TEFOR, MS_TEFTEL, MS_TEIDO, MS_TENCIM, MS_TENIR, MS_TENNEV, MS_TENOR, MS_TENTEL, MS_TENYL1, '+
       'MS_TENYL2, MS_TENYL3, MS_TENYL4, MS_TENYL5, MS_TINEV, MS_TIPUS) select MS_AKTCIM, MS_AKTIR, MS_AKTNEV, MS_AKTOR, MS_AKTTEL, MS_AZTIP, MS_CSPAL, MS_DARU, MS_DATUM, MS_DOKO2, MS_DOKOD, MS_EXPOR, MS_EXSTR, MS_FAJKO, MS_FAJTA, '+
       'MS_FCPAL, MS_FDARU, MS_FELARU, MS_FELCIM, MS_FELDAT, MS_FELDAT2, MS_FELIDO, MS_FELIDO2, MS_FELIR, MS_FELNEV, MS_FELSE1, MS_FELSE2, MS_FELSE3, MS_FELSE4, MS_FELSE5, MS_FELTEL, MS_FEPAL, '+
       'MS_FERKDAT, MS_FERKIDO, MS_FETDAT, MS_FETED, MS_FETED2, MS_FETEI, MS_FETEI2, MS_FETIDO, MS_FORSZ, MS_FPAFEL, MS_FPAIGAZL, MS_FPAIGAZT, MS_FPALER, MS_FPAMEGJ, MS_FSULY, MS_GKKAT, MS_HFOK1,'+
       'MS_HFOK2, MS_ID, MS_ID2, MS_IDOPT, MS_JAKOD, MS_JASZA, MS_KESES, MS_KIFON, MS_LCPAL, MS_LDARU, MS_LELIR, MS_LEPAL, MS_LERCIM, MS_LERDAT, MS_LERDAT2, MS_LERIDO, MS_LERIDO2, MS_LERKDAT, '+
       'MS_LERKIDO, MS_LERNEV, MS_LERSE1, MS_LERSE2, MS_LERSE3, MS_LERSE4, MS_LERSE5, MS_LERTEL, MS_LETDAT, MS_LETED, MS_LETED2, MS_LETEI, MS_LETEI2, MS_LETIDO, MS_LOADID, MS_LPAFEL, MS_LPAIGAZL, '+
       'MS_LPAIGAZT, MS_LPALER, MS_LPAMEGJ, MS_LSULY, MS_M_MAIL, MS_MBKOD, MS_MSKOD, MS_NORID, MS_ORSZA, MS_POTSZ, MS_REGIK, MS_RENSZ, MS_S_MAIL, MS_SNEV1, MS_SNEV2, MS_SORSZ, MS_STATK, MS_STATN, '+
       'MS_TAVKM, MS_TEFCIM, MS_TEFIR, MS_TEFL1, MS_TEFL2, MS_TEFL3, MS_TEFL4, MS_TEFL5, MS_TEFNEV, MS_TEFOR, MS_TEFTEL, MS_TEIDO, MS_TENCIM, MS_TENIR, MS_TENNEV, MS_TENOR, MS_TENTEL, MS_TENYL1, '+
       'MS_TENYL2, MS_TENYL3, MS_TENYL4, MS_TENYL5, MS_TINEV, MS_TIPUS from MEGSEGED where ms_fajko=0 and ms_mbkod='+mbkod);      // csak a normál

    Result:=True;
  Except
    Result:=False;
   // NoticeKi('A sablon mentése nem sikerült!') ;
  End;
end;

procedure TMegbizasbeDlg.FormShow(Sender: TObject);
var
  nev: string;
begin
   try
   MegbizasUjFmDlg.Timer1.Enabled:=False;
   except
   end;
   BitBtn2.Visible:= (M1.Text='');
   nev:=Query_Select2('SZOTAR','SZ_FOKOD','SZ_ALKOD','340',CBGkat.Text,'SZ_LEIRO') ;
   CheckBox8.Enabled:=EgyebDlg.user_super or (nev=EgyebDlg.user_name);
  // ComboBox2.Visible:= ret_kod='';
end;

procedure TMegbizasbeDlg.QueryFelrakoAfterScroll(DataSet: TDataSet);
begin
{
  BitBtn10.Enabled:= (QueryFelrakoMS_ID2.IsNull)or((QueryFelrakoMS_TIPUS.Value=1)and(not QueryFelrakoMS_ID2.IsNull));
  BitBtn15.Enabled:= (QueryFelrakoMS_ID2.IsNull)or((QueryFelrakoMS_TIPUS.Value=1)and(not QueryFelrakoMS_ID2.IsNull));
  BitBtn15.Enabled:=QueryFelrakoMS_TIPUS.Value=0;

  BitBtn21.Enabled:=QueryFelrakoMS_TIPUS.Value=0;
 }
end;

procedure TMegbizasbeDlg.ComboBox2Change(Sender: TObject);
var
  mb, ma: string;
  sorsz, sorsz2: integer;
	ujkodszam	: integer;
	statusz		: integer;
  id,id2: string;
  sorsz3,tipus: string;
begin
  exit;

  if MessageBox(0, 'Mehet a megbízás létrehozása sablomról?', '', MB_ICONQUESTION or MB_YESNO or MB_DEFBUTTON2)=IDNO then
    exit;
 // ComboBox2.Visible:= ComboBox2.ItemIndex < 1;
  if ComboBox2.ItemIndex<1 then exit;
  mb:= MBKOD[ComboBox2.ItemIndex-1];
  Tolto3('Új megbízás készítése sablonról - '+ComboBox2.Text,mb);
  ret_kod:='';
	if not Elkuldes then begin        // MEGBIZAS tárolása
		Exit;
	end;
  ma:=DateToStr(date);
  // tételek
  MEGSEGED2.Close;
  MEGSEGED2.Parameters[0].Value:=mb;
  MEGSEGED2.Open;
  MEGSEGED2.First;
  sorsz:=1;
  sorsz2:= MEGSEGED2MS_SORSZ.Value;
  while not MEGSEGED2.Eof do
  begin
    if sorsz2<>MEGSEGED2MS_SORSZ.Value then
    begin
      inc(sorsz);
      sorsz2:=sorsz;
    end;

    id2:=MEGSEGED2MS_ID2.AsString;
    if (id2<>'0')and(id2<>'') then  // van ID2 ; AF v. AL
    begin
      sorsz3:= Query_Select('MEGSEGED2','MS_ID',id2,'MS_SORSZ') ;
      tipus:= Query_Select('MEGSEGED2','MS_ID',id2,'MS_TIPUS') ;

    	Query_Run(Query11, 'SELECT MS_ID FROM MEGSEGED WHERE MS_MBKOD = '+ret_kod+' and ms_sorsz='+sorsz3+' and ms_tipus='+tipus);
	    id:=Query11.FieldByName('MS_ID').AsString ;
      id2:=id;
    end;
    if (id2='0')or(id2='') then
      id2:='Null';

		ujkodszam 	:= GetNextCode('MEGSEGED', 'MS_MSKOD', 1, 0);
		Query_Run ( Query11, 'INSERT INTO MEGSEGED (MS_MSKOD, MS_MBKOD, MS_SORSZ) VALUES ('
        + IntToStr(ujkodszam) + ',' + IntToStr(StrToIntDef(ret_kod,0)) + ',' + IntToStr(sorsz) +' )',true);
		Query_Update (Query11, 'MEGSEGED',
		[
		'MS_TIPUS', 	''''+MEGSEGED2MS_TIPUS.AsString+'''',
		'MS_FELNEV', 	''''+MEGSEGED2MS_FELNEV.Value+'''',
		'MS_FELIR', 	''''+MEGSEGED2MS_FELIR.Value+'''',
		'MS_FELTEL', 	''''+MEGSEGED2MS_FELTEL.Value+'''',
		'MS_FELCIM', 	''''+MEGSEGED2MS_FELCIM.Value+'''',
		'MS_FELSE1',	''''+MEGSEGED2MS_FELSE1.Value+'''',
		'MS_FELSE2',	''''+MEGSEGED2MS_FELSE2.Value+'''',
		'MS_FELSE3',	''''+MEGSEGED2MS_FELSE3.Value+'''',
		'MS_FELSE4',	''''+MEGSEGED2MS_FELSE4.Value+'''',
		'MS_FELSE5',	''''+MEGSEGED2MS_FELSE5.Value+'''',
		'MS_LOADID',	''''+MEGSEGED2MS_LOADID.Value+'''',
		'MS_FETED',		''''+''+'''',
		'MS_FETED2',	''''+''+'''',
		'MS_FETEI',		''''+''+'''',
		'MS_FETEI2',	''''+''+'''',
		'MS_FELDAT', 	''''+''+'''',
		'MS_FELDAT2', ''''+''+'''',
		'MS_FELIDO', 	''''+''+'''',
		'MS_FELIDO2', ''''+''+'''',
		'MS_FERKDAT', ''''+''+'''',
		'MS_FERKIDO', ''''+''+'''',
		'MS_FETDAT', 	''''+''+'''',
		'MS_FETIDO', 	''''+''+'''',
		'MS_TEIDO', 	''''+''+'''',
		'MS_FSULY', 	'0',
		'MS_FEPAL', 	'0',
		'MS_LERNEV', 	''''+MEGSEGED2MS_LERNEV.Value+'''',
		'MS_LELIR', 	''''+MEGSEGED2MS_LELIR.Value+'''',
		'MS_LERTEL', 	''''+MEGSEGED2MS_LERTEL.Value+'''',
		'MS_LERCIM', 	''''+MEGSEGED2MS_LERCIM.Value+'''',
		'MS_LERSE1',	''''+MEGSEGED2MS_LERSE1.Value+'''',
		'MS_LERSE2',	''''+MEGSEGED2MS_LERSE2.Value+'''',
		'MS_LERSE3',	''''+MEGSEGED2MS_LERSE3.Value+'''',
		'MS_LERSE4',	''''+MEGSEGED2MS_LERSE4.Value+'''',
		'MS_LERSE5',	''''+MEGSEGED2MS_LERSE5.Value+'''',
		'MS_LETED',		''''+''+'''',
		'MS_LETED2',	''''+''+'''',
		'MS_LETEI',		''''+''+'''',
		'MS_LETEI2',	''''+''+'''',
		'MS_TENNEV', 	''''+MEGSEGED2MS_TENNEV.Value+'''',
		'MS_TENOR',		''''+MEGSEGED2MS_TENOR.Value+'''',
		'MS_TENIR', 	''''+MEGSEGED2MS_TENIR.Value+'''',
		'MS_TENTEL', 	''''+MEGSEGED2MS_TENTEL.Value+'''',
		'MS_TENCIM', 	''''+MEGSEGED2MS_TENCIM.Value+'''',
		'MS_TENYL1',	''''+MEGSEGED2MS_TENYL1.Value+'''',
		'MS_TENYL2',	''''+MEGSEGED2MS_TENYL2.Value+'''',
		'MS_TENYL3',	''''+MEGSEGED2MS_TENYL3.Value+'''',
		'MS_TENYL4',	''''+MEGSEGED2MS_TENYL4.Value+'''',
		'MS_TENYL5',	''''+MEGSEGED2MS_TENYL5.Value+'''',

		'MS_TEFNEV', 	''''+MEGSEGED2MS_TEFNEV.Value+'''',
		'MS_TEFOR',		''''+MEGSEGED2MS_TEFOR.Value+'''',
		'MS_TEFIR', 	''''+MEGSEGED2MS_TEFIR.Value+'''',
		'MS_TEFTEL', 	''''+MEGSEGED2MS_TEFTEL.Value+'''',
		'MS_TEFCIM', 	''''+MEGSEGED2MS_TEFCIM.Value+'''',
		'MS_TEFL1',		''''+MEGSEGED2MS_TEFL1.Value+'''',
		'MS_TEFL2',		''''+MEGSEGED2MS_TEFL2.Value+'''',
		'MS_TEFL3',		''''+MEGSEGED2MS_TEFL3.Value+'''',
		'MS_TEFL4',		''''+MEGSEGED2MS_TEFL4.Value+'''',
		'MS_TEFL5',		''''+MEGSEGED2MS_TEFL5.Value+'''',

		'MS_LERDAT', 	''''+''+'''',
		'MS_LERDAT2', ''''+''+'''',
		'MS_LERIDO2', ''''+''+'''',
		'MS_LERIDO', 	''''+''+'''',
		'MS_LERKDAT', ''''+''+'''',
		'MS_LERKIDO', ''''+''+'''',
		'MS_LETDAT', 	''''+''+'''',
		'MS_LETIDO', 	''''+''+'''',
		'MS_FORSZ',		''''+MEGSEGED2MS_FORSZ.Value+'''',
		'MS_ORSZA',		''''+MEGSEGED2MS_ORSZA.Value+'''',
		'MS_EXPOR',		IntToStr(MEGSEGED2MS_EXPOR.Value),
		'MS_EXSTR', 	''''+MEGSEGED2MS_EXSTR.Value+'''',
		'MS_LSULY', 	'0',
		'MS_LEPAL', 	'0',
		'MS_DATUM',     ''''+ma+'''',
		'MS_IDOPT',     ''''+''+'''',
		'MS_TINEV', 	''''+MEGSEGED2MS_TINEV.Value+'''',
		'MS_AKTNEV', 	''''+MEGSEGED2MS_AKTNEV.Value+'''',
		'MS_AKTTEL', 	''''+MEGSEGED2MS_AKTTEL.Value+'''',
		'MS_AKTCIM', 	''''+MEGSEGED2MS_AKTCIM.Value+'''',
		'MS_AKTOR', 	''''+MEGSEGED2MS_AKTOR.Value+'''',
		'MS_AKTIR', 	''''+MEGSEGED2MS_AKTIR.Value+'''',
		'MS_REGIK',		'0',

    'MS_FPAFEL',  '0',
    'MS_FPALER',  '0',
		'MS_FPAMEGJ',	''''+''+'''',
		'MS_FPAIGAZT',BoolToStr01(False),
		'MS_FPAIGAZL',	''''+''+'''',
    'MS_LPAFEL',  '0',
    'MS_LPALER',  '0',
		'MS_LPAMEGJ',	''''+''+'''',
		'MS_LPAIGAZT',BoolToStr01(False),
		'MS_LPAIGAZL',	''''+''+'''',

		'MS_RENSZ', 	''''+MEGSEGED2MS_RENSZ.Value+'''',
		'MS_POTSZ', 	''''+MEGSEGED2MS_POTSZ.Value+'''',
		'MS_DOKOD', 	''''+MEGSEGED2MS_DOKOD.Value+'''',
		'MS_DOKO2', 	''''+MEGSEGED2MS_DOKO2.Value+'''',
		'MS_GKKAT', 	''''+MEGSEGED2MS_GKKAT.Value+'''',
		'MS_SNEV1', 	''''+MEGSEGED2MS_SNEV1.Value+'''',
		'MS_SNEV2', 	''''+MEGSEGED2MS_SNEV2.Value+'''',
		'MS_KIFON',		IntToStr(MEGSEGED2MS_KIFON.Value),
		//'MS_LDARU',		IntToStr(MEGSEGED2MS_LDARU.Value),
		//'MS_FDARU',		IntToStr(MEGSEGED2MS_FDARU.Value),
		'MS_DARU',		IntToStr(MEGSEGED2MS_DARU.Value),

		'MS_AZTIP', 	''''+MEGSEGED2MS_AZTIP.Value+'''',
		'MS_ID2'  ,		id2,

		'MS_CSPAL',		IntToStr(MEGSEGED2MS_CSPAL.Value),
		'MS_FAJKO', 	IntToStr(MEGSEGED2MS_FAJKO.Value),
		'MS_FAJTA',		''''+MEGSEGED2MS_FAJTA.Value+'''',
		'MS_FELARU', 	''''+MEGSEGED2MS_FELARU.Value+''''
		],
		' WHERE MS_MBKOD = '+ret_kod+' AND MS_MSKOD = '+IntToStr(ujkodszam) );

    MEGSEGED2.Next;
  end;
  MEGSEGED2.Close;
	// A státuszok frissítése
	Query_Run(Query11, 'SELECT * FROM MEGSEGED WHERE MS_MBKOD = '+ret_kod);
	if Query11.RecordCount > 0 then begin
		while not Query11.Eof do begin
			statusz	:= GetFelrakoStatus(Query11.FieldByName('MS_ID').AsString);
			SetFelrakoStatus(Query11.FieldByName('MS_ID').AsString, statusz);
			Query11.Next;
		end;
	end;
	Felrakonyitas;
  GombEllenor;
end;

procedure TMegbizasbeDlg.Tolto3(cim, teko: string);
var
  sorsz		: integer;
begin
	ret_kod 	:= teko;
	Caption    	:= cim;
	//M1.Text		:= teko;
	CheckBox1.Checked	:= false;
	CHB3.Checked		:= false;
	MaskEdit4.Text		:= '';
  ComboBox1.ItemIndex:=ComboBox1.Items.IndexOf(EgyebDlg.user_name);
  SetActiveTab(EgyebDlg.v_megbizas_default_ful);
	if ret_kod <> '' then begin		{Módosítás}
		// Az alapadatok beolvasása
		Query_Run(Query1, 'SELECT * FROM MEGBIZAS2 WHERE MB_MBKOD = '+teko ,true);
    //jasza:= Query1.FieldByName('MB_JAKOD').AsString;
		if Query1.RecordCount > 0 then begin
      ELLENORZOTTJAR:=False;  //(Query1.FieldByName('MB_JAKOD').AsString<>'')and(EllenorzottJarat( Query1.FieldByName('MB_JAKOD').AsString)>0);
      Panel12.Enabled:=not ELLENORZOTTJAR;
      BitElkuld.Enabled:=not ELLENORZOTTJAR;
			M11.Text	:= Query1.FieldByName('MB_VEKOD').AsString;

      //BORDNO:= Query1.FieldByName('MB_BORDNO1').AsString<>'';
      Query3.Close;
      Query3.Parameters[0].Value:=M11.Text;
      Query3.Open;
      Query3.First;
      CB_BORD.Items.Clear;
      while not Query3.Eof do
      begin
        CB_BORD.Items.Add(Query3SZ_ALKOD.Value);
        Query3.Next;
      end;
      Query3.Close;
      if CB_BORD.Items.Count>0 then
        BORDNO:=True;
		 //	CB_BORD.Text	:= Query1.FieldByName('MB_BORDNO1').AsString;
		 //	E_BORD.Text	  := Query1.FieldByName('MB_BORDNO2').AsString;
		 //	E_BORD_PLUS.Text	  := Query1.FieldByName('MB_BORDNO3').AsString;



			// A vevő adatainak beolvasása
			Query_Run(Querykoz1,'SELECT * FROM VEVO WHERE V_KOD = '''+M11.Text+''' ',true);
			if QueryKoz1.RecordCount > 0 then begin
				M12.Text	:= QueryKoz1.FieldByName('V_NEV').AsString;
				M120.Text	:= QueryKoz1.FieldByName('V_FAX').AsString;
				M15.Text	:= QueryKoz1.FieldByName('V_IRSZ').AsString + ' '+QueryKoz1.FieldByName('V_VAROS').AsString + ', '+
                   QueryKoz1.FieldByName('V_CIM').AsString;
               kellpoz   := QueryKoz1.FieldByName('VE_PKELL').AsInteger>0;
        if BORDNO then
          kellpoz:=False;
        //CheckBox5.Visible:=kellpoz;
			end;
      CheckBox5.Visible:=kellpoz;
      CheckBox51.Visible:=BORDNO;
      M14.Visible:=not BORDNO;
      CB_BORD.Visible:=BORDNO;
      E_BORD.Visible:=BORDNO;
      E_BORD_PLUS.Visible:=BORDNO;

      // ne a mentett felelős kerüljön be
      // ComboBox1.ItemIndex:=ComboBox1.Items.IndexOf(Query1.FieldByName('MB_FENEV').AsString)   ;

			//M14.Text	:= Query1.FieldByName('MB_POZIC').AsString;
      M16.Text	:= Query1.FieldByName('MB_INTREF').AsString;
			M21.Text	:= Query1.FieldByName('MB_ALVAL').AsString;
			M22.Text	:= Query1.FieldByName('MB_ALNEV').AsString;
			CheckBox2.Checked	:= StrToIntDef(Query1.FieldByName('MB_KISCS').AsString, 0) > 0 ;
			CheckBox3.Checked	:= StrToIntDef(Query1.FieldByName('MB_VAM').AsString, 0) > 0 ;
			CheckBox4.Checked	:= StrToIntDef(Query1.FieldByName('MB_ADR').AsString, 0) > 0 ;
			CheckBox5.Checked	:= StrToIntDef(Query1.FieldByName('MB_NOPOZ').AsString, 0) > 0 ;
			CheckBox51.Checked	:= not (Query1.FieldByName('MB_BORDNO1').AsString <> EmptyStr ) ;
			CheckBox6.Checked	:= StrToIntDef(Query1.FieldByName('MB_EDIJ').AsString, 0) > 0 ;
			CheckBox7.Checked	:= StrToIntDef(Query1.FieldByName('MB_HUTOS').AsString, 0) > 0 ;
			CheckBox8.Checked	:= StrToIntDef(Query1.FieldByName('MB_FELFUG').AsString, 0) > 0 ;
			CheckBox9.Checked	:= StrToIntDef(Query1.FieldByName('MB_NUZPOT').AsString, 0) > 0 ;
			CheckBox10.Checked	:= StrToIntDef(Query1.FieldByName('MB_KOBOS').AsString, 0) > 0 ;
			CheckBox11.Checked	:= StrToIntDef(Query1.FieldByName('MB_NOCSOP').AsString, 0) > 0 ;
			// A vevő adatainak beolvasása
			M25.Text	:= '';
			M26.Text	:= '';
			M27.Text	:= '';
			if M21.Text <> '' then begin
        if EgyebDlg.v_evszam<'2013' then
				  Query_Run(Querykoz1,'SELECT * FROM ALVALLAL WHERE V_KOD = '''+M21.Text+''' ',true)
        else
				  Query_Run(Querykoz1,'SELECT * FROM VEVO WHERE V_KOD = '''+M21.Text+''' ',true);
				if QueryKoz1.RecordCount > 0 then begin
					M25.Text	:= QueryKoz1.FieldByName('V_IRSZ').AsString;
					M26.Text	:= QueryKoz1.FieldByName('V_VAROS').AsString;
					M27.Text	:= QueryKoz1.FieldByName('V_CIM').AsString;
				end;
			end;
			M31.Text	:= Query1.FieldByName('MB_RENSZ').AsString;
			M32.Text	:= Query1.FieldByName('MB_POTSZ').AsString;
       	M33.Text	:= Query1.FieldByName('MB_DOKOD').AsString;
           M34.Text	:= Query1.FieldByName('MB_SNEV1').AsString;
       	M35.Text	:= Query1.FieldByName('MB_DOKO2').AsString;
			M36.Text	:= Query1.FieldByName('MB_SNEV2').AsString;
       	M13.Text	:= Query1.FieldByName('MB_FUDIJ').AsString;
			ComboSet (CBGkat, Query1.FieldByName('MB_GKKAT').AsString, listagkat);
      GKKATEG_valtozott(Query1.FieldByName('MB_GKKAT').AsString);

      RegiRendszam:= Query1.FieldByName('MB_RENSZ').AsString;
      RegiPotszam:= Query1.FieldByName('MB_POTSZ').AsString;

			sorsz := listavnem.IndexOf(Query1.FieldByName('MB_FUNEM').AsString);
  			if sorsz < 0 then begin
				sorsz := 0;
			end;
  			CValuta.ItemIndex       := sorsz;
       	M23.Text	            := Query1.FieldByName('MB_ALDIJ').AsString;
			CValuta2.ItemIndex      := Max(0, listavnem.IndexOf(Query1.FieldByName('MB_ALNEM').AsString));
			CBStatusz.ItemIndex     := Max(0, listastatu.IndexOf(Query1.FieldByName('MB_STATU').AsString));
			M7.Text		            := Query1.FieldByName('MB_MEGJE').AsString;
			RadioButton1.Checked    := true;
			if Query1.FieldByName('MB_EXPOR').AsString = '0' then begin
				RadioButton2.Checked := true;
			end;
			sablon	:='G'; // Query1.FieldByName('MB_SABLO').AsString;
			if sablon = 'G' then begin
				// A sablon beállítás tiltása generált esetben
				CheckBox1.Enabled	:= false;
//				Bitbtn2.Enabled		:= false;
			end;
			if sablon = 'S' then begin
				CheckBox1.Visible	:= true;
				CheckBox1.Enabled	:= False;
				CheckBox1.Checked	:= true;
        Edit1.Visible:=True;
        Edit1.Enabled:=False;
        Edit1.Text:=Query_Select('SABLON','SB_MBKOD',ret_kod,'SB_MEGJE');
			end;
			if StrToIntDef(Query1.FieldByName('MB_SOSFL').AsString, 0) > 0 then begin
				CHB3.Checked		:= true;
				MaskEdit4.Text		:= Query1.FieldByName('MB_SOSKM').AsString;
			end;
			// A fuvartábla adatok beolvasása
			Query_Run(QueryFt, 'SELECT * FROM FUVARTABLA WHERE FT_FTKOD = '+IntToStr(StrToIntDef(Query1.FieldByName('MB_FUTID').AsString,0)) ,true);
			if QueryFt.RecordCount > 0 then begin
				MaskEdit5.Text	:= QueryFt.FieldByName('FT_LANID').AsString;
				MaskEdit7.Text	:= QueryFt.FieldByName('FT_KATEG').AsString;
				MaskEdit6.Text	:= QueryFt.FieldByName('FT_DESCR').AsString;
				MaskEdit8.Text	:= QueryFt.FieldByName('FT_FTKOD').AsString;
			end;
		end;
		// A felrakók/lerakók beolvasása
//		Felrakonyitas;
		CheckBox1Click(Self);
	end;
//	CheckBox1.Visible	:= false;
	BitBtn2.Visible		:= false;
	GombEllenor;
	PozicioIro;
	modosult 	:= True;
end;

procedure TMegbizasbeDlg.Sablontrlse1Click(Sender: TObject);
var
  mb: string;
  ind: integer;
begin
  if ComboBox2.ItemIndex<1 then exit;
  if (MessageBox(0, 'Valóban kéri a sablon törlését?', '', MB_ICONQUESTION or MB_YESNO or MB_DEFBUTTON2) in [idNo]) then
    exit;

  ind:=ComboBox2.ItemIndex;
  mb:= MBKOD[ind-1];
  if Query_Run(Query11,'delete from sablon where sb_mbkod='+mb) then
  begin
    ComboBox2.Items.Delete(ind);
    mb:= MBKOD[ind-1];
    MBKOD.Delete(ind-1);
    ComboBox2.ItemIndex:=0;
    ShowMessage('A sablon törlése kész!');
  end
  else
    ShowMessage('A sablon törlése nem sikerült!');

end;

procedure TMegbizasbeDlg.PopupMenu1Popup(Sender: TObject);
begin
  if not EgyebDlg.user_super then
    exit;
end;

procedure TMegbizasbeDlg.UpdatePozicio(mbkod, pozicio: string);
var
  jakod,sakod,vikod    : string;
  ujkod: integer;
begin
  jakod:=Query_Select('MEGBIZAS','MB_MBKOD',mbkod,'MB_JAKOD');
  vikod:=Query_Select('MEGBIZAS','MB_MBKOD',mbkod,'MB_VIKOD');
  if (jakod='')or(vikod='') then
    exit;
  ujkod:=Query_Select2('VISZONY','VI_JAKOD','VI_VIKOD',jakod,vikod,'VI_UJKOD');
  sakod:=Query_Select2('VISZONY','VI_JAKOD','VI_VIKOD',jakod,vikod,'VI_SAKOD');
  if (sakod<>'')and(sakod<>'0') then
    exit;

  if not Query_Update(Query2,'VISZONY',['VI_POZIC',''''+pozicio+''''],' where VI_VIKOD='+vikod) then
    exit;

  Query_Update(Query2,'SZFEJ',['SA_POZICIO',''''+pozicio+''''],' where SA_UJKOD='+IntToStr(ujkod)) ;

end;

procedure TMegbizasbeDlg.BitBtn102Click(Sender: TObject);
var
  mb: string;
  ind: integer;
begin
  if not ((  GetRightTag(571) <> RG_NORIGHT ) or EgyebDlg.user_super) then
  begin
    MessageBox(0, 'Nincs jogosultsága a művelethez!', '', MB_ICONWARNING or MB_OK);
    exit;
  end;
  if ComboBox2.ItemIndex<1 then exit;
  if (MessageBox(0, 'Valóban kéri a sablon törlését?', '', MB_ICONQUESTION or MB_YESNO or MB_DEFBUTTON2) in [idNo]) then
    exit;

  ind:=ComboBox2.ItemIndex;
  mb:= MBKOD[ind-1];
  if Query_Run(Query11,'delete from sablon where sb_mbkod='+mb) then
  begin
    ComboBox2.Items.Delete(ind);
    mb:= MBKOD[ind-1];
    MBKOD.Delete(ind-1);
    ComboBox2.ItemIndex:=0;
    ShowMessage('A sablon törlése kész!');
  end
  else
    ShowMessage('A sablon törlése nem sikerült!');

end;

procedure TMegbizasbeDlg.BitBtn101Click(Sender: TObject);
var
  mb, ma: string;
  sorsz, sorsz2: integer;
	ujkodszam	: integer;
	statusz		: integer;
  id,id2: string;
  sorsz3,tipus: string;
begin
 if ret_kod='' then begin   // új megbízás
  if (ComboBox2.ItemIndex<1)or(ComboBox2.Text='') then exit;
  if MessageBox(0, 'Mehet a megbízás létrehozása sablonról?', '', MB_ICONQUESTION or MB_YESNO or MB_DEFBUTTON2)=IDNO then
    exit;
  //ComboBox2.Visible:= ComboBox2.ItemIndex < 1;
  mb:= MBKOD[ComboBox2.ItemIndex-1];
  Tolto3('Új megbízás készítése sablonról - '+ComboBox2.Text,mb);
  ret_kod:='';
	if not Elkuldes then begin        // MEGBIZAS tárolása
		Exit;
	end;
  ma:=DateToStr(date);
  // tételek
  MEGSEGED2.Close;
  MEGSEGED2.Parameters[0].Value:=mb;
  MEGSEGED2.Open;
  MEGSEGED2.First;
  sorsz:=1;
  sorsz2:= MEGSEGED2MS_SORSZ.Value;
  while not MEGSEGED2.Eof do
  begin
    if sorsz2<>MEGSEGED2MS_SORSZ.Value then
    begin
      inc(sorsz);
      sorsz2:=sorsz;
    end;

    id2:=MEGSEGED2MS_ID2.AsString;
    if (id2<>'0')and(id2<>'') then  // van ID2 ; AF v. AL
    begin
      sorsz3:= Query_Select('MEGSEGED2','MS_ID',id2,'MS_SORSZ') ;
      tipus:= Query_Select('MEGSEGED2','MS_ID',id2,'MS_TIPUS') ;

    	Query_Run(Query11, 'SELECT MS_ID FROM MEGSEGED WHERE MS_MBKOD = '+ret_kod+' and ms_sorsz='+sorsz3+' and ms_tipus='+tipus);
	    id:=Query11.FieldByName('MS_ID').AsString ;
      id2:=id;
    end;
    if (id2='0')or(id2='') then
      id2:='Null';

		ujkodszam 	:= GetNextCode('MEGSEGED', 'MS_MSKOD', 1, 0);
		Query_Run ( Query11, 'INSERT INTO MEGSEGED (MS_MSKOD, MS_MBKOD, MS_SORSZ) VALUES ('
        + IntToStr(ujkodszam) + ',' + IntToStr(StrToIntDef(ret_kod,0)) + ',' + IntToStr(sorsz) +' )',true);
		Query_Update (Query11, 'MEGSEGED',
		[
		'MS_TIPUS', 	''''+MEGSEGED2MS_TIPUS.AsString+'''',
		'MS_FELNEV', 	''''+MEGSEGED2MS_FELNEV.Value+'''',
		'MS_FELIR', 	''''+MEGSEGED2MS_FELIR.Value+'''',
		'MS_FELTEL', 	''''+MEGSEGED2MS_FELTEL.Value+'''',
		'MS_FELCIM', 	''''+MEGSEGED2MS_FELCIM.Value+'''',
		'MS_FELSE1',	''''+MEGSEGED2MS_FELSE1.Value+'''',
		'MS_FELSE2',	''''+MEGSEGED2MS_FELSE2.Value+'''',
		'MS_FELSE3',	''''+MEGSEGED2MS_FELSE3.Value+'''',
		'MS_FELSE4',	''''+MEGSEGED2MS_FELSE4.Value+'''',
		'MS_FELSE5',	''''+MEGSEGED2MS_FELSE5.Value+'''',
		'MS_LOADID',	''''+MEGSEGED2MS_LOADID.Value+'''',
		'MS_FETED',		''''+''+'''',
		'MS_FETED2',	''''+''+'''',
		'MS_FETEI',		''''+''+'''',
		'MS_FETEI2',	''''+''+'''',
		'MS_FELDAT', 	''''+''+'''',
		'MS_FELDAT2', ''''+''+'''',
		'MS_FELIDO', 	''''+''+'''',
		'MS_FELIDO2', ''''+''+'''',
		'MS_FERKDAT', ''''+''+'''',
		'MS_FERKIDO', ''''+''+'''',
		'MS_FETDAT', 	''''+''+'''',
		'MS_FETIDO', 	''''+''+'''',
		'MS_TEIDO', 	''''+''+'''',
		'MS_FSULY', 	'0',
		'MS_FEPAL', 	'0',
		'MS_LERNEV', 	''''+MEGSEGED2MS_LERNEV.Value+'''',
		'MS_LELIR', 	''''+MEGSEGED2MS_LELIR.Value+'''',
		'MS_LERTEL', 	''''+MEGSEGED2MS_LERTEL.Value+'''',
		'MS_LERCIM', 	''''+MEGSEGED2MS_LERCIM.Value+'''',
		'MS_LERSE1',	''''+MEGSEGED2MS_LERSE1.Value+'''',
		'MS_LERSE2',	''''+MEGSEGED2MS_LERSE2.Value+'''',
		'MS_LERSE3',	''''+MEGSEGED2MS_LERSE3.Value+'''',
		'MS_LERSE4',	''''+MEGSEGED2MS_LERSE4.Value+'''',
		'MS_LERSE5',	''''+MEGSEGED2MS_LERSE5.Value+'''',
		'MS_LETED',		''''+''+'''',
		'MS_LETED2',	''''+''+'''',
		'MS_LETEI',		''''+''+'''',
		'MS_LETEI2',	''''+''+'''',
		'MS_TENNEV', 	''''+MEGSEGED2MS_TENNEV.Value+'''',
		'MS_TENOR',		''''+MEGSEGED2MS_TENOR.Value+'''',
		'MS_TENIR', 	''''+MEGSEGED2MS_TENIR.Value+'''',
		'MS_TENTEL', 	''''+MEGSEGED2MS_TENTEL.Value+'''',
		'MS_TENCIM', 	''''+MEGSEGED2MS_TENCIM.Value+'''',
		'MS_TENYL1',	''''+MEGSEGED2MS_TENYL1.Value+'''',
		'MS_TENYL2',	''''+MEGSEGED2MS_TENYL2.Value+'''',
		'MS_TENYL3',	''''+MEGSEGED2MS_TENYL3.Value+'''',
		'MS_TENYL4',	''''+MEGSEGED2MS_TENYL4.Value+'''',
		'MS_TENYL5',	''''+MEGSEGED2MS_TENYL5.Value+'''',

		'MS_TEFNEV', 	''''+MEGSEGED2MS_TEFNEV.Value+'''',
		'MS_TEFOR',		''''+MEGSEGED2MS_TEFOR.Value+'''',
		'MS_TEFIR', 	''''+MEGSEGED2MS_TEFIR.Value+'''',
		'MS_TEFTEL', 	''''+MEGSEGED2MS_TEFTEL.Value+'''',
		'MS_TEFCIM', 	''''+MEGSEGED2MS_TEFCIM.Value+'''',
		'MS_TEFL1',		''''+MEGSEGED2MS_TEFL1.Value+'''',
		'MS_TEFL2',		''''+MEGSEGED2MS_TEFL2.Value+'''',
		'MS_TEFL3',		''''+MEGSEGED2MS_TEFL3.Value+'''',
		'MS_TEFL4',		''''+MEGSEGED2MS_TEFL4.Value+'''',
		'MS_TEFL5',		''''+MEGSEGED2MS_TEFL5.Value+'''',

		'MS_LERDAT', 	''''+''+'''',
		'MS_LERDAT2', ''''+''+'''',
		'MS_LERIDO2', ''''+''+'''',
		'MS_LERIDO', 	''''+''+'''',
		'MS_LERKDAT', ''''+''+'''',
		'MS_LERKIDO', ''''+''+'''',
		'MS_LETDAT', 	''''+''+'''',
		'MS_LETIDO', 	''''+''+'''',
		'MS_FORSZ',		''''+MEGSEGED2MS_FORSZ.Value+'''',
		'MS_ORSZA',		''''+MEGSEGED2MS_ORSZA.Value+'''',
		'MS_EXPOR',		IntToStr(MEGSEGED2MS_EXPOR.Value),
		'MS_EXSTR', 	''''+MEGSEGED2MS_EXSTR.Value+'''',
		'MS_LSULY', 	'0',
		'MS_LEPAL', 	'0',
		'MS_DATUM',     ''''+ma+'''',
		'MS_IDOPT',     ''''+''+'''',
		'MS_TINEV', 	''''+MEGSEGED2MS_TINEV.Value+'''',
		'MS_AKTNEV', 	''''+MEGSEGED2MS_AKTNEV.Value+'''',
		'MS_AKTTEL', 	''''+MEGSEGED2MS_AKTTEL.Value+'''',
		'MS_AKTCIM', 	''''+MEGSEGED2MS_AKTCIM.Value+'''',
		'MS_AKTOR', 	''''+MEGSEGED2MS_AKTOR.Value+'''',
		'MS_AKTIR', 	''''+MEGSEGED2MS_AKTIR.Value+'''',
		'MS_REGIK',		'0',

    'MS_FPAFEL',  '0',
    'MS_FPALER',  '0',
		'MS_FPAMEGJ',	''''+''+'''',
		'MS_FPAIGAZT',BoolToStr01(False),
		'MS_FPAIGAZL',	''''+''+'''',
    'MS_LPAFEL',  '0',
    'MS_LPALER',  '0',
		'MS_LPAMEGJ',	''''+''+'''',
		'MS_LPAIGAZT',BoolToStr01(False),
		'MS_LPAIGAZL',	''''+''+'''',

		'MS_RENSZ', 	''''+MEGSEGED2MS_RENSZ.Value+'''',
		'MS_POTSZ', 	''''+MEGSEGED2MS_POTSZ.Value+'''',
		'MS_DOKOD', 	''''+MEGSEGED2MS_DOKOD.Value+'''',
		'MS_DOKO2', 	''''+MEGSEGED2MS_DOKO2.Value+'''',
		'MS_GKKAT', 	''''+MEGSEGED2MS_GKKAT.Value+'''',
		'MS_SNEV1', 	''''+MEGSEGED2MS_SNEV1.Value+'''',
		'MS_SNEV2', 	''''+MEGSEGED2MS_SNEV2.Value+'''',
		'MS_KIFON',		IntToStr(MEGSEGED2MS_KIFON.Value),
		//'MS_LDARU',		IntToStr(MEGSEGED2MS_LDARU.Value),
		//'MS_FDARU',		IntToStr(MEGSEGED2MS_FDARU.Value),
		'MS_DARU',		IntToStr(MEGSEGED2MS_DARU.Value),

		'MS_AZTIP', 	''''+MEGSEGED2MS_AZTIP.Value+'''',
		'MS_ID2'  ,		id2,

		'MS_CSPAL',		IntToStr(MEGSEGED2MS_CSPAL.Value),
		'MS_FAJKO', 	IntToStr(MEGSEGED2MS_FAJKO.Value),
		'MS_FAJTA',		''''+MEGSEGED2MS_FAJTA.Value+'''',
		'MS_FELARU', 	''''+MEGSEGED2MS_FELARU.Value+''''
		],
		' WHERE MS_MBKOD = '+ret_kod+' AND MS_MSKOD = '+IntToStr(ujkodszam) );

    MEGSEGED2.Next;
  end;
  MEGSEGED2.Close;
  ComboBox2.Enabled:=False;
	// A státuszok frissítése
	Query_Run(Query11, 'SELECT * FROM MEGSEGED WHERE MS_MBKOD = '+ret_kod);
	if Query11.RecordCount > 0 then begin
		while not Query11.Eof do begin
			statusz	:= GetFelrakoStatus(Query11.FieldByName('MS_ID').AsString);
			SetFelrakoStatus(Query11.FieldByName('MS_ID').AsString, statusz);
			Query11.Next;
		end;
	end;
	Felrakonyitas;
  GombEllenor;
 end else begin        // megbízás módosítása (már van MBKOD
    if not ((GetRightTag(571) <> RG_NORIGHT ) or EgyebDlg.user_super) then
    begin
      MessageBox(0, 'Nincs jogosultsága a művelethez!', '', MB_ICONWARNING or MB_OK);
      exit;
    end;
    if (copy( ComboBox2.Text,1,1)='*')or(Trim(ComboBox2.Text)='') then exit;
    if MessageBox(0, 'Kéri a sablon mentését?', '', MB_ICONQUESTION or MB_YESNO or MB_DEFBUTTON2)=IDNO then
      exit;
    mb:='';
    if ComboBox2.ItemIndex>-1 then
      mb:= MBKOD[ComboBox2.ItemIndex-1];
    if not SablonMentes(mb,ret_kod, ComboBox2.Text) then
      MessageBox(0, 'A sablon mentése nem sikerült!', '', MB_ICONWARNING or MB_OK)
    else
      MessageBox(0, 'A sablon elmentve!', '', MB_ICONWARNING or MB_OK);
 end;
end;
{ régi
procedure TMegbizasbeDlg.CValutaChange(Sender: TObject);
begin
   if (M13.Text<>'')and (NoticeKi('Megváltoztatja a valutanemet? ', NOT_QUESTION) = -1) then begin
       CValuta.ItemIndex   := Max(0, listavnem.IndexOf(Query1.FieldByName('MB_FUNEM').AsString));  // visszaállítás, ha nem változtat
       end
   else begin
       TeljesDijSzamitas;
       end;
end;
}

procedure TMegbizasbeDlg.CValutaChange(Sender: TObject);
var
  MB_FUNEM, SQLText, S, Ertek: string;
  Megvan, KellSzamolas: boolean;
begin
  Megvan:= False;
  KellSzamolas:= True;
    try
    MB_FUNEM:= Query1.FieldByName('MB_FUNEM').AsString; // az adatbázisban tárolt érték
    Megvan:= True;
  except
     SQLText:= trim(Query1.SQL.Text);
     if SQLText <> '' then begin
      S:= 'A lekérdezés adattartalma egészen váratlan. Kérlek értesítsd a fejlesztőt! SQL ='+ SQLText;
      NoticeKi(S);
      HibaKiiro(S);
      end;  // if
    end;  // try-except
  if Megvan then begin
    if (ret_kod<>'') and (CValuta.Text <> MB_FUNEM) then begin
        // if (jakod<>'') and ( Query_Select('VISZONY','VI_JAKOD',jakod,'VI_UJKOD')<>'') then begin
        if M1.Text <> '' then begin
          Ertek:= VedettMezoValtozas(IntToStr(listavnem.IndexOf(MB_FUNEM)), IntToStr(CValuta.ItemIndex), KellSzamolas);
          if Ertek <> IntToStr(CValuta.ItemIndex) then
            CValuta.ItemIndex:= StrToInt(Ertek);
          {if (VanHozzaIgaziSzamla(M1.Text) <> '') then begin
            NoticeKi('A megbízáshoz számla tartozik, ezért a fuvardíj devizaneme nem módosítható! Először törölje a megbízást a járatból.')  ;
            CValuta.ItemIndex:= Max(0, listavnem.IndexOf(MB_FUNEM));
            KellSzamolas:= False;
            end;}
          end;
        end;
     end;  // if Megvan
  if KellSzamolas then
    TeljesDijSzamitas;
end;

{ régi
procedure TMegbizasbeDlg.CVnemChange(Sender: TObject);
begin
	if (M130.Text<>'')and (NoticeKi('Megváltoztatja a valutanemet? ', NOT_QUESTION) = -1) then begin
      CVnem.ItemIndex   := Max(0, listavnem.IndexOf(Query1.FieldByName('MB_SENEM').AsString));   // visszaállítás, ha nem változtat
      end
   else begin
       TeljesDijSzamitas;
       end;
end;
}
procedure TMegbizasbeDlg.CVnemChange(Sender: TObject);
var
  MB_SENEM, SQLText, S, Ertek: string;
  Megvan, KellSzamolas: boolean;
begin
  KellSzamolas:= True;
  Megvan:= False;
  try
    MB_SENEM:= Query1.FieldByName('MB_SENEM').AsString; // az adatbázisban tárolt érték
    Megvan:= True;
  except
    SQLText:= trim(Query1.SQL.Text);
    if SQLText <> '' then begin
      S:= 'A lekérdezés adattartalma egészen váratlan. Kérlek értesítsd a fejlesztőt! SQL ='+ SQLText;
      NoticeKi(S);
      HibaKiiro(S);
      end;  // if
    end;  // try-except
  if Megvan then begin
    if (ret_kod<>'') and (CVnem.Text <> MB_SENEM) then begin
        // if (jakod<>'') and ( Query_Select('VISZONY','VI_JAKOD',jakod,'VI_UJKOD')<>'') then begin
        if M1.Text <> '' then begin
          Ertek:= VedettMezoValtozas(IntToStr(listavnem.IndexOf(MB_SENEM)), IntToStr(CVnem.ItemIndex), KellSzamolas);
          if Ertek <> IntToStr(CVnem.ItemIndex) then
            CVnem.ItemIndex:= StrToInt(Ertek);

          {if (VanHozzaIgaziSzamla(M1.Text) <> '') then begin
            NoticeKi('A megbízáshoz számla tartozik, ezért az egyéb díj devizaneme nem módosítható! Először törölje a megbízást a járatból.')  ;
            CVnem.ItemIndex:= Max(0, listavnem.IndexOf(MB_SENEM));
            KellSzamolas:= False;
            end;  // if
            }
          end;
      end;
    end; // if Megvan
  if KellSzamolas then
    TeljesDijSzamitas;
end;

{procedure TMegbizasbeDlg.CVnemExit(Sender: TObject);
var
    S, SQLText, MB_SENEM: string;
    Megvan: boolean;
begin
  MB_SENEM:= Query1.FieldByName('MB_SENEM').AsString; // az adatbázisban tárolt érték
  if (ret_kod<>'') and (CVnem.Text <> MB_SENEM) then begin
      if (jakod<>'') and ( Query_Select('VISZONY','VI_JAKOD',jakod,'VI_UJKOD')<>'') then begin
        NoticeKi('A megbízáshoz számla tartozik, ezért az egyéb díj devizaneme nem módosítható! Először törölje a megbízást a járatból.')  ;
        CVnem.ItemIndex:= Max(0, listavnem.IndexOf(MB_SENEM));
        end
      else begin
        TeljesDijSzamitas;
        end;  // else
      end
   else begin
      TeljesDijSzamitas;
      end;  // else
end;
}

procedure TMegbizasbeDlg.CValuta2Change(Sender: TObject);
begin
	if (M23.Text<>'')and (NoticeKi('Megváltoztatja a valutanemet? ', NOT_QUESTION) = -1) then begin
       CValuta2.ItemIndex   := Max(0, listavnem.IndexOf(Query1.FieldByName('MB_ALNEM').AsString));
   end;
end;

procedure TMegbizasbeDlg.BitBtn23Click(Sender: TObject);
begin
  EgyebDlg.Lejartszamlak(M11.Text,True,'50');
end;

procedure TMegbizasbeDlg.SendFeketeVevo;
var
   cimzett     : string;
begin
   // Feketelistás vevő került elküldésre
   Screen.Cursor   := crHourGlass;
   cimzett := EgyebDlg.Read_SZGrid('376','100');
   if cimzett <> '' then begin
       EgyebDlg.ellenlista.Clear;
       EgyebDlg.ellenlista.Add('Figyelem!');
       EgyebDlg.ellenlista.Add('');
       EgyebDlg.ellenlista.Add('A '+ret_kod+' kódú megbízáshoz feketelistás vevö kapcsolódik.');
       EgyebDlg.ellenlista.Add('Megbízó : '+M11.Text+', '+M12.Text);
       EgyebDlg.ellenlista.Add('Felelös : '+Combobox1.Text);
       EgyebDlg.ellenlista.Add('');
       EgyebDlg.ellenlista.Add('FUVAROS rendszer');
       if not SendJSEmail (cimzett, '', 'Feketelistás vevő',  EgyebDlg.ellenlista ) then begin
           HibaKiiro('Feketelistás üzenet elküldése nem sikerült! A megbízás kódja : '+ret_kod);
       end;
   end;
   Screen.Cursor   := crDefault;
end;

end.
