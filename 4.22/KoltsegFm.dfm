�
 TKOLTSEGFMDLG 0�  TPF0TKoltsegFmDlgKoltsegFmDlgTag� LeftoTop� Caption   Költségek karbantartásaClientHeightuClientWidthSColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameArial
Font.Style 
KeyPreview	OldCreateOrder	PositionpoScreenCenterOnCreate
FormCreate	OnKeyDownFormKeyDownOnResize
FormResizePixelsPerInch`
TextHeight TLabelLQ4Left� Top� WidthkHeightCaption   Érték összesenFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameArial
Font.Style 
ParentFont  TBitBtnBitBtn1TagLeft� TopHWidthkHeightHint(   Költségek importálása, feldolgozásaHelpContexts9Caption   KöltségimportFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameArial
Font.Style 	NumGlyphs
ParentFontParentShowHintShowHint	TabOrder TabStopVisibleOnClickBitBtn1Click  TBitBtnBitBtn2TagLeft� TopxWidthkHeightHint(   Költségek importálása, feldolgozásaHelpContexts9Caption   JáratFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameArial
Font.Style 	NumGlyphs
ParentFontParentShowHintShowHint	TabOrderTabStopOnClickBitBtn2Click  	TMaskEditM2TagLeftfTopWidthYHeightColorclAquaEnabled	MaxLengthTabOrderText      TBitBtnBitBtn5Left�TopWidthHeight
Glyph.Data
z  v  BMv      v   (                                    �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 33333333?�������        wwwwwwww���������7	��y���w�w���wwwwwwpwwwwwwww����������	��y��y�w�w�w�wwwwwwpwwwwwwww����������	��y��y�w�w�w�wwwwwwpwwwwwwww�����33�����y��y���w�w�        wwwwwwww��̈����www��www��������wwwwwwww	NumGlyphsTabOrderTabStopOnClickBitBtn5Click  	TMaskEditM3TagLeft
TopWidthYHeightColorclAquaEnabled	MaxLengthTabOrderText      TBitBtnBitBtn4LeftjTopWidthHeight
Glyph.Data
z  v  BMv      v   (                                    �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 33333333?�������        wwwwwwww���������7	��y���w�w���wwwwwwpwwwwwwww����������	��y��y�w�w�w�wwwwwwpwwwwwwww����������	��y��y�w�w�w�wwwwwwpwwwwwwww�����33�����y��y���w�w�        wwwwwwww��̈����www��www��������wwwwwwww	NumGlyphsTabOrderTabStopOnClickBitBtn4Click  TBitBtnBitBtn3TagLeftkTopAWidthjHeightHelpContexts9Caption1	NumGlyphsTabOrderTabStopVisible  TBitBtnBitBtn6TagLeftTopEWidthdHeightHelpContext2Caption2	NumGlyphsTabOrderVisible  TBitBtnBitBtn10Tag
Left�Top1Width#HeightHelpContexts9CaptionMind	NumGlyphsTabOrderTabStopOnClickBitBtn10Click  TBitBtnBitBtn7TagLeftTopuWidthdHeightHelpContext2Caption3	NumGlyphsTabOrder	Visible  TDBEditDBEdit1LeftTTop� WidthyHeight	DataFieldertek
DataSourceDSQ4Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameArial
Font.Style 
ParentFontReadOnly	TabOrder
  	TADOQueryQuery3TagConnectionString|Provider=SQLNCLI.1;Integrated Security=SSPI;Persist Security Info=False;Initial Catalog=DAT2007;Data Source=DEV32\SQLEXPRESS
CursorTypectStatic
Parameters SQL.StringsSELECT * FROM KOLTSEG         Left� Top0 TIntegerFieldIntegerField1DisplayLabel   KódDisplayWidth	FieldNameKS_KTKOD  TIntegerFieldIntegerField2	AlignmenttaCenterDisplayLabel   TípusDisplayWidth	FieldNameKS_TIPUS  TStringFieldStringField1DisplayLabel   Járat	FieldNameKS_JAKODSize  TStringFieldStringField2DisplayLabel   Dátum	FieldNameKS_DATUMSize  TFloatFieldFloatField1DisplayLabel   Összeg	FieldName	KS_OSSZEGDisplayFormat0.00  TStringFieldStringField3DisplayLabelV.nem	FieldName	KS_VALNEMSize  TFloatFieldFloatField2DisplayLabel	   Árfolyam	FieldName	KS_ARFOLYDisplayFormat0.00  	TBCDField	BCDField1TagDisplayLabel
   Bruttó FtDisplayWidth
	FieldNameKS_ERTEKDisplayFormat0.	Precision  TFloatFieldFloatField3DisplayLabel   Km/Üzemóra	FieldNameKS_KMORA  TStringFieldStringField4DisplayLabel	   Rendszám	FieldName	KS_RENDSZSize
  TIntegerFieldIntegerField3DisplayLabelT.DisplayWidth	FieldNameKS_TELEOriginFUVARALAP."koltseg.DB".KS_TELE  TIntegerFieldIntegerField4DisplayLabelTelepDisplayWidth	FieldNameKS_TETAN  TFloatFieldFloatField4TagDisplayLabel   Üz.a.menny.	FieldName	KS_UZMENYDisplayFormat0.00  TFloatFieldFloatField5DisplayLabel   Átlag	FieldNameKS_ATLAGDisplayFormat0.00  TStringFieldStringField5DisplayLabel   F.módDisplayWidth	FieldNameKS_FIMODOriginFUVARALAP."koltseg.DB".KS_FIMODSize  TStringFieldStringField6DisplayLabel   Leírás	FieldName	KS_LEIRASSizeP  TStringFieldStringField7DisplayLabel   Megjegyzés	FieldNameKS_MEGJSizeP   	TADOQueryQuery4Tag
ConnectionEgyebDlg.ADOConnectionAlap
CursorTypectStatic
Parameters SQL.Strings'Select sum(ks_ertek) ertek from koltseg Left� Top` 	TBCDFieldQuery4ertek	FieldNameertekReadOnly	DisplayFormat0, Ft	Precision    TDataSourceDSQ4DataSetQuery4Left� Top�    