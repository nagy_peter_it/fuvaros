unit PontKimut;

interface

uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, DBTables, DB, StdCtrls, Buttons, Mask, DBCtrls, ExtCtrls,DateUtils;

type
  TPontKimutDlg = class(TForm)
    Label1: TLabel;
    BitBtn4: TBitBtn;
    BitBtn6: TBitBtn;
    MaskEdit1: TMaskEdit;
    CheckBox1: TCheckBox;
    BitBtn10: TBitBtn;
    CheckBox2: TCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure BitBtn4Click(Sender: TObject);
    procedure BitBtn6Click(Sender: TObject);
    procedure MaskEdit1Click(Sender: TObject);
    procedure BitBtn10Click(Sender: TObject);
    procedure MaskEdit1Exit(Sender: TObject);
  private
  public
  end;

var
  PontKimutDlg: TPontKimutDlg;

implementation

uses
	PontList, Egyeb, Kozos;
{$R *.DFM}

procedure TPontKimutDlg.FormCreate(Sender: TObject);
begin
 	MaskEdit1.Text := copy(EgyebDlg.MaiDatum,1,8)+'01.';
end;

procedure TPontKimutDlg.BitBtn4Click(Sender: TObject);
begin

	{A lista �ssze�ll�t�sa a sz�r�si elemek alapj�n}
	if MaskEdit1.Text = '' then begin
  		MaskEdit1.Text := copy(EgyebDlg.MaiDatum,1,8)+'01.';
  	end;
  	if not Jodatum2(MaskEdit1) then begin
		NoticeKi('Rossz d�tum megad�sa!');
     	MaskEdit1.SetFocus;
     	Exit;
  	end;

  	{Csak eg�sz h�napot adhatunk meg}
   Screen.Cursor	:= crHourGlass;
  	Application.CreateForm(TPontListDlg, PontListDlg);
  	PontListDlg.Tolt(MaskEdit1.Text, CheckBox1.Checked, CheckBox2.Checked);
   Screen.Cursor	:= crDefault;
  	if PontListDlg.vanadat	then begin
     	PontListDlg.Rep.Preview;
  	end else begin
    	NoticeKi('Nincs a megadott felt�teleknek megfelel� t�tel!');
  	end;
  	PontListDlg.Destroy;
end;

procedure TPontKimutDlg.BitBtn6Click(Sender: TObject);
begin
	Close;
end;

procedure TPontKimutDlg.MaskEdit1Click(Sender: TObject);
begin
	with Sender as TMaskEdit do begin
  	SelectAll;
  end;
end;

procedure TPontKimutDlg.BitBtn10Click(Sender: TObject);
begin
	EgyebDlg.Calendarbe(MaskEdit1);
end;

procedure TPontKimutDlg.MaskEdit1Exit(Sender: TObject);
begin
	DatumExit(MaskEdit1, false);
end;

end.


