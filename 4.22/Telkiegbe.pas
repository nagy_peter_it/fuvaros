unit Telkiegbe;

interface

uses
	SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
	Forms, Dialogs, StdCtrls, Buttons, Mask, DB, DBTables, J_ALFORM, ADODB,
  Vcl.Grids, Vcl.ExtCtrls, Vcl.ComCtrls, Vcl.TabNotBk;

type
	TTelKiegbeDlg = class(TJ_AlformDlg)
	BitElkuld: TBitBtn;
	BitKilep: TBitBtn;
    Label3: TLabel;
    Label6: TLabel;
    M5: TMaskEdit;
    M6: TMaskEdit;
    BitBtn1: TBitBtn;
    Label1: TLabel;
    Label2: TLabel;
    M1: TMaskEdit;
	 Query1: TADOQuery;
	 Query2: TADOQuery;
	 BitBtn37: TBitBtn;
    meMegjegyzes: TMemo;
    cbTipus: TComboBox;
    Label10: TLabel;
    cbStatusz: TComboBox;
	 procedure BitKilepClick(Sender: TObject);
	 procedure FormCreate(Sender: TObject);
	 procedure BitElkuldClick(Sender: TObject);
	 procedure MaskEditClick(Sender: TObject);
	 procedure FormKeyDown(Sender: TObject; var Key: Word;
	   Shift: TShiftState);
	 procedure Tolto(cim, kod : string); override;
	 procedure BitBtn1Click(Sender: TObject);
	 procedure BitBtn37Click(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure M6Click(Sender: TObject);

	private
    lista_tipus, lista_statusz: TStringList;
    procedure Mentes;
	public
	end;

const
  SG_Elofizeteskod_oszlop = 5;
var
	TelKiegbeDlg: TTelKiegbeDlg;

implementation

uses
 Egyeb, J_SQL, Kozos,  J_VALASZTO;

{$R *.DFM}

procedure TTelKiegbeDlg.BitKilepClick(Sender: TObject);
begin
	ret_kod := '';
	Close;
end;

procedure TTelKiegbeDlg.FormCreate(Sender: TObject);
var
  i: integer;
begin
	EgyebDlg.SeTADOQueryDatabase(Query1);
	EgyebDlg.SeTADOQueryDatabase(Query2);
  lista_statusz	:= TStringList.Create;
  lista_tipus	:= TStringList.Create;
  SzotarTolt(CBStatusz, '144' , lista_statusz, false, false, true );
  SzotarTolt(CBTipus, '153' , lista_tipus, false, false, true );
	CBStatusz.ItemIndex  := 1;  // Akt�v
  // CBTipus.ItemIndex  := lista_tipus.IndexOf(LegutobbFelvittTelefonTipus);
	ret_kod:= '';
end;

procedure TTelKiegbeDlg.FormDestroy(Sender: TObject);
begin
  if lista_statusz <> nil then lista_statusz.Free;
  if lista_tipus <> nil then lista_tipus.Free;
end;

procedure TTelKiegbeDlg.Tolto(cim, kod : string);
var
  S: string;
begin
	SetMaskEdits([M1, M5, M6]);
	Caption 		:= cim;
	ret_kod  := kod;
	M1.Text		:= ret_kod;
	if ret_kod <> '' then begin
		if Query_Run (Query1, 'SELECT * FROM TELKIEG WHERE TI_ID = '+ret_kod ,true) then begin
      ComboSet (cbStatusz, Trim(Query1.FieldByName('TI_STATUSKOD').AsString), lista_statusz);
      ComboSet (cbTipus, Trim(Query1.FieldByName('TI_TIPUSKOD').AsString), lista_tipus);
			M5.Text	:= Query1.FieldByname('TI_DOKOD').AsString;
			meMegjegyzes.Text	:= Query1.FieldByname('TI_MEGJE').AsString;
			M6.Text		:= Query_Select('DOLGOZO', 'DO_KOD', M5.Text, 'DO_NAME');
  		end; // if
	end;
end;

procedure TTelKiegbeDlg.BitElkuldClick(Sender: TObject);
begin
  Mentes;
	Close;
end;

procedure TTelKiegbeDlg.Mentes;
var
  UjTIID, i: integer;
  DarabS: string;
begin

	if cbTipus.ItemIndex = -1 then begin
		NoticeKi('Kieg�sz�t� t�pus megad�sa k�telez�!');
		cbTipus.Setfocus;
		Exit;
	end;

	if ret_kod = '' then begin  // �j telefon
    UjTIID:= Query_Insert_Identity (Query1, 'TELKIEG',
      [
      'TI_STATUSKOD',  ''''+lista_statusz[cbStatusz.ItemIndex]+'''',
      'TI_TIPUSKOD', ''''+lista_tipus[cbTipus.ItemIndex]+'''',
      'TI_DOKOD', ''''+M5.Text+'''',
      'TI_MEGJE', ''''+meMegjegyzes.Text+''''
      ], 'TI_ID' );
     if UjTIID>=0 then begin  // ha nem volt sikeres, �gyis kap SQL hiba ablakot
      ret_kod:= IntToStr(UjTIID);
      M1.Text:= ret_kod;
      end;
     end
  else begin
     Query_Update (Query1, 'TELKIEG',
      [
      'TI_STATUSKOD',  ''''+lista_statusz[cbStatusz.ItemIndex]+'''',
      'TI_TIPUSKOD', ''''+lista_tipus[cbTipus.ItemIndex]+'''',
      'TI_DOKOD', ''''+M5.Text+'''',
      'TI_MEGJE', ''''+meMegjegyzes.Text+''''
      ], ' WHERE TI_ID='+ret_kod);
      end;  // else
end;

procedure TTelKiegbeDlg.M6Click(Sender: TObject);
begin
   EgyebDlg.DolgozoFotoMutato(M5.Text, M6);
end;

procedure TTelKiegbeDlg.MaskEditClick(Sender: TObject);
begin
	with Sender as TMaskEdit do begin
  	SelectAll;
  end;
end;

procedure TTelKiegbeDlg.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 // Nagyp 2016-08-03
 // EgyebDlg.FormReturn(Key);
end;

procedure TTelKiegbeDlg.BitBtn1Click(Sender: TObject);
begin
	DolgozoValaszto(M5, M6 );
  meMegjegyzes.SetFocus;
end;

procedure TTelKiegbeDlg.BitBtn37Click(Sender: TObject);
begin
	M5.Text	:= '';
	M6.Text	:= '';
end;


end.







