unit ForgList2;

interface

uses
  WinTypes, WinProcs, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, QuickRpt, QRExpr, Qrctrls, ExtCtrls, printers, DB, DBTables,
  Egyeb, Forgalom, J_SQL, Kozos, ADODB ;

type
  TKiForgList2Dlg = class(TForm)
    Rep: TQuickRep;
    QRBand3: TQRBand;
    QRLSzamla: TQRLabel;
    QRBand1: TQRBand;
    QRBand2: TQRBand;
    QRLabel25: TQRLabel;
    QRLabel56: TQRLabel;
    QRLabel57: TQRLabel;
    QRLabel75: TQRLabel;
    QRLabel63: TQRLabel;
    QRLabel6: TQRLabel;
    QRSysData1: TQRSysData;
    QRLabel7: TQRLabel;
    QRSysData4: TQRSysData;
    QRShape1: TQRShape;
	 QRLabel12: TQRLabel;
    QRLabel13: TQRLabel;
    QRLabel14: TQRLabel;
    QRLabel19: TQRLabel;
    QRLabel22: TQRLabel;
    QRLabel23: TQRLabel;
    QRShape2: TQRShape;
    QRGroup1: TQRGroup;
    QRBand4: TQRBand;
    QRLabel46: TQRLabel;
    QRLabel49: TQRLabel;
    Query1: TADOQuery;
    Query2: TADOQuery;
	 Query3: TADOQuery;
    Query4: TADOQuery;
    Query5: TADOQuery;
    QRLabel15: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel16: TQRLabel;
    QRLabel35: TQRLabel;
    QRLabel17: TQRLabel;
    QRLabel18: TQRLabel;
    QRLabel3: TQRLabel;
    QRLabel31: TQRLabel;
    QRLabel9: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel43: TQRLabel;
    QRLabel40: TQRLabel;
    QRLabel44: TQRLabel;
    QRLabel41: TQRLabel;
    QRLabel21: TQRLabel;
    QRLabel58: TQRLabel;
    QRLabel55: TQRLabel;
    QRSysData2: TQRSysData;
    QRLabel26: TQRLabel;
    QRLabel50: TQRLabel;
    QRLabel36: TQRLabel;
    QRLabel24: TQRLabel;
    QRLabel27: TQRLabel;
    QRLabel28: TQRLabel;
    QRLabel30: TQRLabel;
    QRLabel29: TQRLabel;
    QRLabel34: TQRLabel;
    QRLabel10: TQRLabel;
    QRLabel2: TQRLabel;
    QRLabel37: TQRLabel;
    QRLabel32: TQRLabel;
    QRLabel39: TQRLabel;
    QRLabel33: TQRLabel;
    QRLabel54: TQRLabel;
    QRLabel53: TQRLabel;
    QRLabel1: TQRLabel;
    QR01: TQRLabel;
    QR02: TQRLabel;
    QR04: TQRLabel;
    QR03: TQRLabel;
    QR05: TQRLabel;
    QR06: TQRLabel;
    QR08: TQRLabel;
    QR07: TQRLabel;
    QRLabel38: TQRLabel;
    QQ1: TQRLabel;
    QQ2: TQRLabel;
    QQ4: TQRLabel;
    QQ3: TQRLabel;
    QQ5: TQRLabel;
    QQ6: TQRLabel;
    QQ8: TQRLabel;
    QQ7: TQRLabel;
    QRLabel5: TQRLabel;
    procedure FormCreate(Sender: TObject);
    procedure QRBand3BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
    function	Tolt(sqlstr	: string; FDlg	: TForgalomDlg ) : boolean;
    procedure QRBand1BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
    procedure QRBand2BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
    procedure RepBeforePrint(Sender: TCustomQuickRep;
  var PrintReport: Boolean);
	 procedure QRBand4BeforePrint(Sender: TQRCustomBand;
	   var PrintBand: Boolean);
	 procedure QRGroup1BeforePrint(Sender: TQRCustomBand;
	   var PrintBand: Boolean);
	 procedure FormDestroy(Sender: TObject);
  private
		osnet		: double;
		osafa		: double;
		osneteur	: double;
		osafaeur	: double;
		osped		: double;
		renet		: double;
		reafa		: double;
		reneteur	: double;
		reafaeur	: double;
		reped   	: double;
		ForgDlg		: TForgalomDlg;
		spedkod		: string;
		kellcsoport	: boolean;
		osszesen    : double;
		lista2	  	: TStringList;
  public
	 reszletes		: boolean;
  end;

var
  KiForgList2Dlg: TKiForgList2Dlg;

implementation

{$R *.DFM}

procedure TKiForgList2Dlg.FormCreate(Sender: TObject);
begin
	EgyebDlg.SetADOQueryDatabase(Query1);
	EgyebDlg.SetADOQueryDatabase(Query2);
	EgyebDlg.SetADOQueryDatabase(Query3);
	EgyebDlg.SetADOQueryDatabase(Query4);
	EgyebDlg.SetADOQueryDatabase(Query5);
	osnet		:= 0;
	osafa		:= 0;
	osneteur	:= 0;
	osafaeur	:= 0;
	osped		:= 0;
	renet		:= 0;
	reafa		:= 0;
	reneteur	:= 0;
	reafaeur	:= 0;
	reped   	:= 0;
	EllenListTorol;
	kellcsoport	    := true;
   reszletes	    := true;
	Query_Run(Query3, 'SELECT * FROM JARAT ORDER BY JA_ALKOD');
 	spedkod 	:= EgyebDlg.Read_SZGrid('999', '560');
   if spedkod = '' then begin
       spedkod := ''' ''';
   end;
   while Pos('"', spedkod) > 0 do begin
   	spedkod := copy(spedkod, 1, Pos('"', spedkod) - 1) + '''' + copy(spedkod, Pos('"', spedkod) + 1, 255);
   end;
   Query_Run(Query4, 'SELECT SS_UJKOD, SS_DARAB * SS_EGYAR * (1 + SS_AFA /100 ) OSSZEG FROM SZSOR WHERE SS_TERKOD in ('+spedkod+') ');
	lista2	:= TStringLIst.Create;
end;

procedure TKiForgList2Dlg.QRBand3BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
   {A fejl�c adatainak ki�r�sa}
	QrLabel13.Caption := ForgDlg.ComboBox1.Text;
	QrLabel19.Caption := ForgDlg.MaskEdit1.Text +' - '+ForgDlg.MaskEdit2.Text;
	QrLabel23.Caption := ForgDlg.MaskEdit5.Text;
	QrLabel49.Caption := ForgDlg.MaskEdit3.Text;
end;

function TKiForgList2Dlg.Tolt(sqlstr	: string; FDlg	: TForgalomDlg ) : boolean;
begin
	Result 		:= false;
	ForgDlg		:= FDlg;
	{A kapott sql alapj�n v�grehajtja a lek�rdez�st}
	Query_Run(Query1, sqlstr, true);
	//  Query1.Sql.Savetofile(EgyebDlg.TempLIst + 'SQL.TXT');
	if Query1.RecordCount > 0 then begin
		Result 		:= true;
		osszesen    := 0;
		while not Query1.Eof do begin
			osszesen := osszesen + Query1.FieldByName('SA_NETTO').AsFloat;
			Query1.Next;
		end;
		Query1.First;
	end;

	// A csoportok meghat�roz�sa
	case FDlg.ComboBox1.ItemIndex of
	0:
		QrGroup1.Expression	:= 'Query1.SA_TEDAT';
	1:
		QrGroup1.Expression	:= 'Query1.SA_KIDAT';
	2:
		QrGroup1.Expression	:= 'Query1.SA_ESDAT';
	3:
		QrGroup1.Expression	:= 'Query1.SA_VEVONEV';
	4:
		begin
		QrGroup1.Expression	:= 'Query1.SA_OSSZEG';
		kellcsoport				:= false;
		end;
	5:
		QrGroup1.Expression	:= 'Query1.SA_JARAT';
	6:
		QrGroup1.Expression	:= 'Query1.SA_VEVONEV';
	7:
		begin
		QrGroup1.Expression	:= 'Query1.SA_KOD';
		kellcsoport				:= false;
		end;
	end;

end;

procedure TKiForgList2Dlg.QRBand1BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
	// A j�rat ellen�rz�tts�g�nek vizsg�lata
	QrLabel50.Caption	:= '';
	if  Query3.Locate('JA_ALKOD',Query1.FieldByname('SA_JARAT').AsString,[loCaseInsensitive, loPartialKey]) then begin
		if StrToIntDef(Query3.FieldByname('JA_FAZIS').AsString,0) < 1 then begin
			{M�g nem ellen�rz�tt j�rat}
			EllenListAppend('Nem ellen�rz�tt j�rat : ('+Query3.FieldByName('JA_KOD').AsString + ' ) ' +
				Query3.FieldByname('JA_ORSZ').AsString + '-' + Format('%5.5d',[StrToIntDef(Query3.FieldByname('JA_ALKOD').AsString,0)]), true);
		end;
	end else begin
		Lista2.Add(' -> ALKOD = '+Query1.FieldByname('SA_JARAT').AsString +
			'  ('+Query1.FieldByname('SA_KOD').AsString+'-'+Query1.FieldByname('SA_KIDAT').AsString+')');
		QrLabel50.Caption	:= 'NINCS J�RAT!';
	end;
	// QrLabel26.Caption := copy(Query1.FieldByname('SA_KOD').AsString,6,11);
  QrLabel26.Caption := Query1.FieldByname('SA_KOD').AsString;
	QrLabel24.Caption := copy(Query1.FieldByname('SA_TEDAT').AsString,3,10);
	QrLabel27.Caption := copy(Query1.FieldByname('SA_KIDAT').AsString,3,10);
	QrLabel28.Caption := copy(Query1.FieldByname('SA_ESDAT').AsString,3,10);
	QrLabel29.Caption := copy(Query1.FieldByname('SA_VEVONEV').AsString,1,15);
	QrLabel34.Caption := Query1.FieldByname('SA_RSZ').AsString;
	QrLabel36.Caption := Query1.FieldByname('SA_JARAT').AsString;
	QrLabel30.Caption := Query1.FieldByname('SA_MEGJ').AsString;
	ForgDlg.Label4.Caption	:= Query1.FieldByname('SA_KIDAT').AsString + ' - ' + Query1.FieldByname('SA_VEVONEV').AsString;
	ForgDlg.Label4.Update;
	QrLabel2.Caption  := Format('%9.0f', [Query1.FieldByName('SA_NEAFA').AsFloat+Query1.FieldByName('SA_NETTO').AsFloat - Query1.FieldByName('SA_FTSPED').AsFloat]);
	QrLabel10.Caption := Format('%9.0f', [Query1.FieldByName('SA_NEAFA').AsFloat+Query1.FieldByName('SA_NETTO').AsFloat]);
	QrLabel32.Caption := Format('%9.0f', [Query1.FieldByName('SA_FTSPED').AsFloat]);
	QrLabel37.Caption := Format('%9.0f', [Query1.FieldByName('SA_NETTO').AsFloat]);
	QrLabel39.Caption := Format('%9.0f', [Query1.FieldByName('SA_NEAFA').AsFloat]);
	QrLabel33.Caption := Format('%10.1f EUR', [Query1.FieldByName('SA_EURNET').AsFloat]);
	QrLabel53.Caption := Format('%10.1f EUR', [Query1.FieldByName('SA_EURAFA').AsFloat]);
	QrLabel54.Caption := Format('%10.1f EUR', [Query1.FieldByName('SA_EURNET').AsFloat+Query1.FieldByName('SA_EURAFA').AsFloat]);
	renet		:= renet    + Query1.FieldByName('SA_NETTO').AsFloat;
	reafa		:= reafa    + Query1.FieldByName('SA_NEAFA').AsFloat;
	reneteur 	:= reneteur + Query1.FieldByName('SA_EURNET').AsFloat;
	reafaeur	:= reafaeur + Query1.FieldByName('SA_EURAFA').AsFloat;
	reped		:= reped    + Query1.FieldByName('SA_FTSPED').AsFloat;
	PrintBand	:= reszletes;
end;

procedure TKiForgList2Dlg.QRBand2BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
	i : integer;
begin
	if lista2.Count > 0 then begin
		EllenListAppend('', false);
		EllenListAppend('  Hi�nyz� j�ratok : ', false);
		for i := 0 to lista2.Count - 1 do begin
			EllenListAppend(lista2[i], false);
		end;
		EllenListAppend('.', false);
	end;
	EllenListMutat('Probl�m�k', true);
	QQ1.Caption := Format('%9.0f',  [osnet+osafa-osped]);
	QQ2.Caption := Format('%9.0f',  [osnet+osafa]);
	QQ3.Caption := Format('%9.0f',  [osped]);
	QQ4.Caption := Format('%9.0f',  [osnet]);
	QQ5.Caption := Format('%9.0f',  [osneteur]);
	QQ6.Caption := Format('%9.0f',  [osafa]);
	QQ7.Caption := Format('%9.0f',  [osafaeur]);
	QQ8.Caption := Format('%9.0f',  [osneteur+osafaeur]);
end;

procedure TKiForgList2Dlg.RepBeforePrint(Sender: TCustomQuickRep;
  var PrintReport: Boolean);
begin
	osnet		:= 0;
	osafa		:= 0;
	osneteur	:= 0;
	osafaeur	:= 0;
	osped		:= 0;
	renet		:= 0;
	reafa		:= 0;
	reneteur	:= 0;
	reafaeur	:= 0;
	reped   	:= 0;
end;

procedure TKiForgList2Dlg.QRBand4BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
	case ForgDlg.ComboBox1.ItemIndex of
	0:
		QrLabel1.Caption := Query1.FieldByName('SA_TEDAT').AsString;
	1:
		QrLabel1.Caption := Query1.FieldByName('SA_KIDAT').AsString;
	2:
		QrLabel1.Caption := Query1.FieldByName('SA_ESDAT').AsString;
	3:
		QrLabel1.Caption := Query1.FieldByName('SA_VEVONEV').AsString;
	4:
		QrLabel1.Caption := Query1.FieldByName('SA_OSSZEG').AsString;
	5:
		QrLabel1.Caption := Query1.FieldByName('SA_JARAT').AsString;
	6:
		QrLabel1.Caption := Query1.FieldByName('SA_VEVONEV').AsString;
	end;
  QrLabel5.Caption :='';
	if osszesen = 0 then begin
		QrLabel1.Caption := QrLabel1.Caption + ' �sszesen :';
	end else begin
		// CSak a nett� sz�m�t az �sszesenekn�l
		//QrLabel1.Caption := QrLabel1.Caption + ' �sszesen ('+Format('%.2f',[renet*100/osszesen])+' %) :';
		QrLabel1.Caption := QrLabel1.Caption + ' �sszesen:';
		QrLabel5.Caption := Format('%.2f',[renet*100/osszesen])+' %';
	end;

	QR01.Caption := Format('%9.0f',  [renet+reafa-reped]);
	QR02.Caption := Format('%9.0f',  [renet+reafa]);
	QR03.Caption := Format('%9.0f',  [reped]);
	QR04.Caption := Format('%9.0f',  [renet]);
	QR05.Caption := Format('%9.0f',  [reneteur]);
	QR06.Caption := Format('%9.0f',  [reafa]);
	QR07.Caption := Format('%9.0f',  [reafaeur]);
	QR08.Caption := Format('%9.0f',  [reneteur+reafaeur]);

	osnet		:= osnet + renet;
	osafa		:= osafa + reafa;
	osneteur	:= osneteur + reneteur;
	osafaeur	:= osafaeur + reafaeur;
	osped		:= osped + reped;
	renet		:= 0;
	reafa		:= 0;
	reneteur	:= 0;
	reafaeur	:= 0;
	reped   	:= 0;
	PrintBand	:= kellcsoport;
end;

procedure TKiForgList2Dlg.QRGroup1BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
	PrintBand	:= false;
end;

procedure TKiForgList2Dlg.FormDestroy(Sender: TObject);
begin
	Lista2.Free;
end;

end.
