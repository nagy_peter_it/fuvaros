unit ShellFm;

interface

uses
	SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
	StdCtrls, Forms, ExtCtrls, Dialogs, J_FOFORMUJ;

type
	TShellFmDlg = class(TJ_FOFORMUJDLG)
	  procedure FormCreate(Sender: TObject);override;
	 procedure Adatlap(cim, kod : string);override;
	end;

var
	ShellFmDlg : TShellFmDlg;

implementation

uses
	Egyeb, J_SQL, Shellbe;

{$R *.DFM}

procedure TShellFmDlg.FormCreate(Sender: TObject);
begin
	Inherited FormCreate(Sender);
	FelTolto('�j SHELL k�rtya felvitele ', 'SHELL k�rtya adatok m�dos�t�sa ',
			'SHELL k�rty�k list�ja', 'SH_KSZAM', 'SELECT * FROM SHELL  LEFT OUTER JOIN DOLGOZO ON SH_TULAJ = DO_KOD ','SHELL', QUERY_KOZOS_TAG );
	nevmezo	:= 'SH_SHNEV';
	width	:= DLG_WIDTH;
	height	:= DLG_HEIGHT;
end;

procedure TShellFmDlg.Adatlap(cim, kod : string);
begin
	Application.CreateForm(TShellbeDlg, AlForm);
	Inherited Adatlap(cim, kod );
end;

end.

