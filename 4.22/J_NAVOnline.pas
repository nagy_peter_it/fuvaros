unit J_NAVOnline;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.Mask, Vcl.StdCtrls, Vcl.Buttons,
  Vcl.ExtCtrls, IdComponent, IdTCPConnection, IdTCPClient,
  IdHTTP, Xml.XMLIntf, Xml.XMLDoc, IdSSLOpenSSL, IdCoderMIME,
  IdHashSHA, IdAuthentication, FMX.Edit, DCPCrypt2, DCPrijndael, DCPsha1, Shellapi,
  IdBaseComponent, IniFiles, Vcl.ComCtrls, System.Generics.Collections,
  Vcl.FileCtrl, IDGlobal ;


  type FirmData = record
       taxid   : string;       // Ad�sz�m
       vatcode : string;       // �FA k�d -> NEM K�TELEZ� !!!
       countyc : string;       // Megye k�d -> NEM K�TELEZ� !!!
       name    : string;       // N�v
       country : string;       // Orsz�g k�d -> HU
       postal  : string;       // Ir�ny�t�sz�m
       city    : string;       // v�ros
       address : string;       // utca
       bank    : string;       // banksz�mla -> CSAK AZ ELAD�N�L !
  end;

  TSzamlaSor = class(TObject)
       lNumber             : integer;
       lproductCodeCategory: string;   // VTSZ; SZJ; KN; AHK; CSK; KT; EJ; OWN; OTHER
       lproductCodeValue   : string;
       lDescription        : string;
       lquantity           : double;
       lunitOfMeasure      : string;
       lunitPrice          : double;
       lNetAmount          : double;
       lvatPercentage      : double;
       lVatAmount          : double;
       lVatAmountHUF       : double;
       lGrossAmountNormal  : double;
  end;

  TOsszesen = class(TObject)
       ovatPercentage      : double;
       ovatRateNetAmount   : double;
       ovatRateVatAmount   : double;
       ovatRateVatAmountHUF: double;
       ovatRateGrossAmount : double;
  end;

type

   TNavSzamla = class(TComponent)
       public
      	constructor Create(AOwner: TComponent); override;
    	destructor 	Destroy; override;

       procedure   Osszesenkepzes;

       private
       supplier        : FirmData; // Elad�
       customer        : FirmData; // Vev�

       // SZ�mla fejl�c
       iNumber         : string;
       iCategory       : string;   // Sz�mla kateg�ria: NORMAL; SIMPLIFIED; AGGREGATE
       iIssueDate      : string;   // Kelt1
       iDeliveryDate   : string;   // Teles�t�s d�tuma
       icurrencyCode   : string;   // P�nznem
       ipaymentMethod  : string;   // Fizet�si m�d  : TRANSFER; CASH; CARD; VOUCHER; OTHER
       ipaymentDate    : string;   // Hat�rid�
       iAppearance     : string;   // Megjelen�si forma: PAPER; ELECTRONIC; EDI; UNKNOWN

       // Sor adatok
       iSorok          : TObjectList<TSzamlaSor>;
       iOsszesenek     : TObjectList<TOsszesen>;

       // V�g�sszesenek
       public
       iNetAmount      : double;
       iVatAmount      : double;
       iVatAmountHUF   : double;
       iGrossAmount    : double;

   end;

  TJNavOnline = class(TForm)
    MemoKap: TMemo;
    MemoAll: TMemo;
    IdHTTP1: TIdHTTP;
    Panel1: TPanel;
    Panel2: TPanel;
    NOMemo: TMemo;
    BitOK: TBitBtn;
    MSzam: TMaskEdit;
    Label1: TLabel;
    Timer1: TTimer;
    Panel3: TPanel;
    BitLezar: TBitBtn;
    Panel4: TPanel;
    Label2: TLabel;
    CBXml: TComboBox;
    BitStat: TBitBtn;
    BitInv: TBitBtn;
    FLB: TFileListBox;
    MTrid: TMaskEdit;
    Label3: TLabel;
    Label4: TLabel;
    MStat: TMaskEdit;

       function    GetErrorCode : integer;
       function    GetErrorStr  : string;
       function    IniNyit : boolean;
      	function    Tolt(inin, bdir, belo : string) : boolean;
       function    ReadElado : boolean;
       function    SetVevo(tax, name, country, postal, city, address, bank : string) : boolean;
       function    FejTolto(szamlaszam, categ, kiadas, teljesites, vanem, method, hatarido, fajta : string) : boolean;
       function    SorTolto(sorsz, vtszj, descr, megys : string; menny, egyar, netto, afasz, afa, afahuf, arfolyam : double) : boolean;
       procedure   Szamlakuldes;
       procedure   StatuszKuldes;
       procedure   SzamlaKiiro;

       procedure   GetResult(  noderes : IXMLNode; var ME1, ME2, ME3 : string);
       function    GetTimeStamp( td : TDateTime) : String;
       procedure   BuildTextNode( node0 : IXMLNode; name, str : string );
       procedure   CreateHeader2(no : IXMLNode; reqstr : string);
       function    SendXMLData( szoveg, urlstr : string ) : integer;
       function    DecryptString(AData, Akey: string) :string;
       function    GetResultSection(  noderes : IXMLNode; sec : string) : string;
       procedure   MemoElvalaszto(mem : TMemo; szoveg : string);
       procedure   SorszamIro;
       procedure   ListaTolto;
       function    GetSHA512Str(const str : Ansistring) : Ansistring;
       function    Encode64Str( str : string): String;
       function    GetGoogName(str : string) : string;
       procedure   GetVtsz(vtszstr : string; var vtcat, vtval : string);

       function    TokenRequest : string;
       function    SzamlaRequest : string;
       function    GetSzamlaStr(sza : TNAvSzamla) : string;
       function    GetSzamStr( sz : double) : string;
       function    GetDatumStr( str : string) : string;
       procedure   CreateInvoiceOperations(no : IXMLNode; invstr : string);
       function    StatusRequest( trcode : string ) : boolean;
       function    InvoiceRequest( invcode : string ) : boolean;

    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure BitOKClick(Sender: TObject);
    procedure XmlMentes( mnev : string);
    procedure BitLezarClick(Sender: TObject);
    procedure BitStatClick(Sender: TObject);
    procedure BitInvClick(Sender: TObject);
    procedure GombMutato(st : boolean);
    procedure CBXmlChange(Sender: TObject);

   private
       INIFile     : TMemInifile;
       ininame     : string;

       errorcode   : integer;
       errstr      : string;

       // Az ini-b�l olvasand� elemek
       elotag      : string;
       sorszam     : integer;
       url1        : string;
       url2        : string;
       url3        : string;
       url4        : string;
       url5        : string;
       username    : string;
       userpass    : string;
       usertax     : string;
       xmlkey      : string;
       replacekey  : string;
       sw_ID       : string;
       sw_name     : string;
       IdHTTP3     : TIdHTTP;
       resp_xml    : IXMLDocument;
       err_code    : integer;
       err_mess    : string;
       err_errmsg  : string;
       ered        : integer;

   public
       Szamla      : TNavSzamla;
       token       : string;
       validfrom   : string;
       validto     : string;
       invnum      : string;
       invstatus   : string;
       res1        : string;
       res2        : string;
       res3        : string;
       transactid  : string;
       err_list    : TStringLIst;
       war_list    : TStringLIst;
       runcommand  : string;
       ret_value   : string;
       base_dir    : string;   // Alap k�nyvt�r - ide mentj�k az xml-eket
       base_elo    : string;   // File n�v el�tag a ment�shez
       inv_storno  : string;
   end;

var
  JNavOnline: TJNavOnline;

implementation

{$R *.dfm}

uses
   synacrypt, Synacode, Kozos;


constructor TNavSzamla.Create(AOwner: TComponent);
begin
   // Alap�rt�kek be�ll�t�sa
   Inherited Create(AOwner);
   iSorok  := TObjectList<TSzamlaSor>.Create;
   isorok.OwnsObjects  := true;
   iOsszesenek  := TObjectList<TOsszesen>.Create;
   iOsszesenek.OwnsObjects  := true;
end;

destructor  TNavSzamla.Destroy;
begin
   // Megsz�ntetj�k az oszt�lyt
   iSorok.Free;
   iOsszesenek.Free;
end;

procedure TNavSzamla.Osszesenkepzes;
var
   ii  : integer;
   jj  : integer;
   os  : TOsszesen;
   voltafa : boolean;
begin
    // A sz�mla adataib�l l�trehozzuk az �sszesen elemeket
   iOsszesenek.Clear;
   for ii := 0 to iSorok.Count-1 do begin
       if iosszesenek.Count = 0 then begin
           // Ha m�g nincs �sszesen, azonnal l�trehozunk egyet
           os  := TOsszesen.Create;
           os.ovatPercentage       := iSorok[ii].lvatPercentage;
           os.ovatRateNetAmount    := iSorok[ii].lNetAmount;
           os.ovatRateVatAmount    := iSorok[ii].lVatAmount;
           os.ovatRateVatAmountHUF := iSorok[ii].lVatAmountHUF;
           os.ovatRateGrossAmount  := iSorok[ii].lGrossAmountNormal;
           iOsszesenek.Add(os);
       end else begin
           voltafa := false;
           for jj := 0 to iOsszesenek.Count - 1 do begin
               if iOsszesenek[jj].ovatPercentage  = iSorok[ii].lvatPercentage then begin
                   // Volt megfelel� �FA
                   voltafa := true;
                   iOsszesenek[jj].ovatRateNetAmount    := iOsszesenek[jj].ovatRateNetAmount+iSorok[ii].lNetAmount;
                   iOsszesenek[jj].ovatRateVatAmount    := iOsszesenek[jj].ovatRateVatAmount+iSorok[ii].lVatAmount;
                   iOsszesenek[jj].ovatRateVatAmountHUF := iOsszesenek[jj].ovatRateVatAmountHUF+iSorok[ii].lVatAmountHUF;
                   iOsszesenek[jj].ovatRateGrossAmount  := iOsszesenek[jj].ovatRateGrossAmount+iSorok[ii].lGrossAmountNormal;
               end;
           end;
           if not voltafa then begin
               // Felvissz�k az �j �F�-t
               os  := TOsszesen.Create;
               os.ovatPercentage       := iSorok[ii].lvatPercentage;
               os.ovatRateNetAmount    := iSorok[ii].lNetAmount;
               os.ovatRateVatAmount    := iSorok[ii].lVatAmount;
               os.ovatRateVatAmountHUF := iSorok[ii].lVatAmountHUF;
               os.ovatRateGrossAmount  := iSorok[ii].lGrossAmountNormal;
               iOsszesenek.Add(os);
           end;
       end;
   end;
   // Itt j�nnek a v�g�sszesen k�pz�sek
   iNetAmount      := 0;
   iVatAmount      := 0;
   iVatAmountHUF   := 0;
   iGrossAmount    := 0;
   for ii := 0 to iOsszesenek.Count-1 do begin
       iNetAmount      := iNetAmount + iOsszesenek[ii].ovatRateNetAmount;
       iVatAmount      := iVatAmount + iOsszesenek[ii].ovatRateVatAmount;
       iVatAmountHUF   := iVatAmountHUF + iOsszesenek[ii].ovatRateVatAmountHUF;
       iGrossAmount    := iGrossAmount + iOsszesenek[ii].ovatRateGrossAmount;
   end;

end;

// ============================================================================================================================== //
//  ITT J�NNEK A NAVONLINE FUNKCI�K !!!
// ============================================================================================================================== //


function    TJNavOnline.GetErrorCode : integer;
begin
   Result  := errorcode;
end;

function    TJNavOnline.GetErrorStr  : string;
begin
   Result  := errstr;
end;

function TJNavOnline.IniNyit : boolean;
begin
   Result  := false;
   if FileExists(ininame) then begin
       IniFile := TMemIniFile.Create( ininame, TEncoding.UTF8 );
       errorcode := 0;
       errstr  := '';
       Result  := true;
   end else begin
       errorcode := -1;
       errstr  := 'Nem l�tez� ini �llom�ny (' + ininame + ')!';
   end;
end;

function TJNavOnline.ReadElado : boolean;
var
   taxid   : string;
begin
   Result := false;
   if not IniNyit then begin
       Exit;
   end;
   taxid := StringReplace(IniFile.ReadString( 'SUPPLIER', 'TAXID', '' ), '-', '',[rfReplaceAll, rfIgnoreCase]);
   Szamla.supplier.taxid   := copy(taxid, 1, 8);
   usertax                 := Szamla.supplier.taxid;
   Szamla.supplier.vatcode := copy(taxid, 9, 1);
   Szamla.supplier.countyc := copy(taxid, 10, 2);
   Szamla.supplier.name    := IniFile.ReadString( 'SUPPLIER', 'NAME', '' );
   Szamla.supplier.country := IniFile.ReadString( 'SUPPLIER', 'COUNTRY', '' );
   Szamla.supplier.postal  := IniFile.ReadString( 'SUPPLIER', 'POSTALCODE', '' );
   Szamla.supplier.city    := IniFile.ReadString( 'SUPPLIER', 'CITY', '' );
   Szamla.supplier.address  := IniFile.ReadString( 'SUPPLIER', 'ADDRESS', '' );
   Szamla.supplier.bank    := IniFile.ReadString( 'SUPPLIER', 'BANK', '' );
   if ( (Trim(Szamla.supplier.taxid) = '') or (Trim(Szamla.supplier.vatcode) = '') or(Trim(Szamla.supplier.countyc) = '') or(Trim(Szamla.supplier.name) = '') or
       (Trim(Szamla.supplier.country) = '') or(Trim(Szamla.supplier.postal) = '') or(Trim(Szamla.supplier.city) = '') or(Trim(Szamla.supplier.address) = '') ) then begin
       errorcode := -4;
       errstr  := 'Hib�s elad� adatok : -'+taxid+'-'+Szamla.supplier.name+'-'+Szamla.supplier.country+'-'+Szamla.supplier.postal+'-'+Szamla.supplier.city+'-'+Szamla.supplier.address;
   end else begin
       Result  := true;
   end;
end;

function TJNavOnline.SetVevo(tax, name, country, postal, city, address, bank : string) : boolean;
var
   taxid   : string;
begin
   // A vev� asdatainak megad�sa
   Result  := false;
   errorcode   := 0;
   errstr      := '';
   taxid := StringReplace(tax, '-', '',[rfReplaceAll, rfIgnoreCase]);
   Szamla.customer.taxid   := copy(taxid, 1, 8);
   Szamla.customer.vatcode := copy(taxid, 9, 1);
   Szamla.customer.countyc := copy(taxid, 10, 2);
   Szamla.customer.name    := name;
   Szamla.customer.country := country;
   Szamla.customer.postal  := postal;
   Szamla.customer.city    := city;
   Szamla.customer.address  := address;
   Szamla.customer.bank    := bank;
   if ( Trim(Szamla.customer.taxid) = '') then begin
       errorcode := -5;
       errstr  := errstr + 'Hi�nyz� ad�sz�m; ';
   end;
   if ( Trim(Szamla.customer.vatcode) = '') then begin
       errorcode := -5;
       errstr  := errstr + 'Hi�nyz� �FA k�d (ad�sz�m 9. karakter); ';
   end;
   if ( Trim(Szamla.customer.countyc) = '') then begin
       errorcode := -5;
       errstr  := errstr + 'Hi�nyz� megyek�d (ad�sz�m 10.,11. karakter); ';
   end;
   if ( Trim(Szamla.customer.name) = '')then begin
       errorcode := -5;
       errstr  := errstr + 'Hi�nyz� n�v; ';
   end;
   if ( Trim(Szamla.customer.country) = '')then begin
       errorcode := -5;
       errstr  := errstr + 'Hi�nyz� orsz�gk�d; ';
   end;
   if ( Trim(Szamla.customer.postal) = '')then begin
       errorcode := -5;
       errstr  := errstr + 'Hi�nyz� ir�ny�t�sz�m; ';
   end;
   if ( Trim(Szamla.customer.city) = '')then begin
       errorcode := -5;
       errstr  := errstr + 'Hi�nyz� telep�l�s n�v; ';
   end;
   if ( Trim(Szamla.customer.address) = '')  then begin
       errorcode := -5;
       errstr  := 'Hi�nyz� c�m';
   end;
   if errorcode = 0 then begin
       Result  := true;
   end;
end;

function TJNavOnline.FejTolto(szamlaszam, categ, kiadas, teljesites, vanem, method, hatarido, fajta : string) : boolean;
begin
//   Result  := false;
   invnum                 := szamlaszam;
   Szamla.iNumber         := szamlaszam;
   Mszam.Text             := invnum;
   Szamla.iCategory       := categ;
   Szamla.iIssueDate      := kiadas;
   Szamla.iDeliveryDate   := teljesites;
   Szamla.icurrencyCode   := vanem;
   Szamla.ipaymentMethod  := method;
   Szamla.ipaymentDate    := hatarido;
   Szamla.iAppearance     := fajta;
   ListaTolto;
   Result  := true;
end;

procedure TJNavOnline.FormCreate(Sender: TObject);
begin
   Szamla      := TNavSzamla.Create(Self);
   err_list    := TStringList.Create;
   war_list    := TStringList.Create;
   runcommand  := '';
   inv_storno  := '';
end;

procedure TJNavOnline.FormDestroy(Sender: TObject);
begin
   // Megsz�ntet�s m�veletei
//   Szamla.Free;
   err_list.Free;
   war_list.Free;
end;

procedure TJNavOnline.FormShow(Sender: TObject);
begin
   // Ha be van �ll�tva parancs, megcsin�ljuk
   Timer1.Enabled  := true;
end;

function TJNavOnline.Tolt(inin, bdir, belo : string) : boolean;
var
   okgomb  : boolean;
   jelstr  : string;
begin
   // Alap�rt�kek be�ll�t�sa
   ininame     := inin;
   resp_xml    := NewXMLDocument;
   base_elo    := belo;
   base_dir    := IncludeTrailingPathDelimiter(bdir);
   ForceDirectories(base_dir);       // L�trehozzuk a bdir alk�nyvt�rat
   // Az ini �llom�ny megnyit�sa
   if IniNyit then begin
       elotag   := IniFile.ReadString( 'GENERAL', 'ELOTAG', 'ELO_' );
       sorszam  := StrToIntdef(IniFile.ReadString( 'GENERAL', 'SORSZAM', '1' ), 1);
       url1 	 := IniFile.ReadString( 'GENERAL', 'URL1', '' );
       url2 	 := IniFile.ReadString( 'GENERAL', 'URL2', '' );
       url3 	 := IniFile.ReadString( 'GENERAL', 'URL3', '' );
       url4 	 := IniFile.ReadString( 'GENERAL', 'URL4', '' );
       url5 	 := IniFile.ReadString( 'GENERAL', 'URL5', '' );
       username := IniFile.ReadString( 'GENERAL', 'USERNAME', '' );
       userpass := IniFile.ReadString( 'GENERAL', 'USERPSWD', '' );
       if userpass = '' then begin
           // A jelsz� beolvas�sa
           jelstr  := '';
           okgomb  := InputQuery('Jelsz�', 'A jelsz� sz�vege : ', jelstr);
           if okgomb then begin
               if jelstr <> '' then begin
                   try
                       IniFile.WriteString('GENERAL', 'USERPSWD',  EncodeString(jelstr));
                       userpass    := jelstr;
                   finally
                       IniFile.UpdateFile;
                   end;
               end;
           end else begin
               errorcode := -2;
               errstr  := 'Felhaszn�l�i jelsz� hiba!';
           end;
       end else begin
           userpass := DecodeString(userpass);
       end;
       xmlkey      := IniFile.ReadString( 'GENERAL', 'XMLKULCS', '' );
       replacekey  := IniFile.ReadString( 'GENERAL', 'CSEREKULCS', '' );
       sw_ID       := IniFile.ReadString( 'GENERAL', 'SW_ID', '' );
       sw_name     := IniFile.ReadString( 'GENERAL', 'SW_NAME', '' );
       Inifile.Free;
       if ((url1='')or(url2='')or(url3='')or(url4='')or(url5='')or(username='')or(userpass='')or(xmlkey='')or(replacekey='')or(sw_ID='')or(sw_name='')) then begin
           errorcode := -3;
           errstr  := 'Valamelyik ini mez� �res!';
       end;
   end;
   // A teszt/�les ter�let eld�nt�se
   Panel3.Caption  := 'NAV  �LES  TER�LET';
   if Pos('-test', url1) > 0 then begin
       Panel3.Caption  := 'NAV  TESZT  TER�LET';
   end;
end;

function TJNavOnline.SorTolto(sorsz, vtszj, descr, megys : string; menny, egyar, netto, afasz, afa, afahuf, arfolyam : double) : boolean;
var
   szasor  : TSzamlaSor;
   vtcat   : string;
   vtval   : string;
begin
//   Result  := false;
   szasor                      := TSzamlaSor.Create;
   szasor.lNumber              := StrToIntDef(sorsz, 0);
   GetVtsz(vtszj, vtcat, vtval );
   szasor.lproductCodeCategory := vtcat;
   szasor.lproductCodeValue    := vtval;
   szasor.lDescription         := descr;
   szasor.lquantity            := menny;
   if Trim(megys) = ''  then begin
       szasor.lunitOfMeasure       := '.';
   end else begin
       szasor.lunitOfMeasure       := megys;
   end;
   szasor.lunitPrice           := egyar;
   szasor.lNetAmount           := netto;
   szasor.lvatPercentage       := afasz;
   szasor.lVatAmount           := afa;
   szasor.lVatAmountHUF        := afahuf;
   szasor.lGrossAmountNormal   := szasor.lNetAmount + szasor.lVatAmount;
   Szamla.iSorok.Add(szasor);
   SZamla.Osszesenkepzes;
   Result  := true;
end;

function TJNavOnline.SendXMLData( szoveg, urlstr : string ) : integer;
var
  Req, Resp    : TStream;
  SSL1         : TIdSSLIOHandlerSocketOpenSSL;
  SL           : TStringList;
begin
   err_code   := 0;
   err_mess   := '';
   err_errmsg := '';
   Resp := TMemoryStream.Create;
   try
       Req := TMemoryStream.Create;
       try
           Req.WriteBuffer(Pointer(szoveg)^, Length(szoveg)*SizeOf(Char));
           Req.Position := 0;
           SSL1 := TIdSSLIOHandlerSocketOpenSSL.Create(nil);
           try
               SSL1.SSLOptions.Method := sslvSSLv23;
               IdHTTP1.IOHandler := SSL1;
               IdHTTP1.Request.ContentType := 'application/xml';
               IdHTTP1.Request.Accept := 'application/xml';
               IdHTTP1.Request.Charset := 'UTF-8';
               IdHTTP1.Post(urlstr, Req, Resp);
           except
                on E: EIdHTTPProtocolException do begin
                    err_code   := E.ErrorCode;
                    err_mess   := Utf8Decode(E.Message);
                    err_errmsg := Utf8Decode(E.ErrorMessage);
                    resp_xml.XML.Text  := Xml.XMLDoc.FormatXMLData(E.ErrorMessage);
                    resp_xml.Active   := true;
                    Memokap.Text       := resp_xml.XML.Text;
                end;
           end;
           SSl1.Free;
       finally
           Req.Free;
       end;
       Result  := IdHTTP1.ResponseCode;
       if IdHTTP1.ResponseCode <> 200 then begin
           Exit;
       end;

       SL := TStringList.Create;
       try
           Resp.Position := 0;
           SL.LoadFromStream(Resp);
           resp_xml.XML.Text  := Xml.XMLDoc.FormatXMLData(Utf8Decode(SL.Text));
           resp_xml.Active    := true;
           Memokap.Text       := resp_xml.XML.Text;
       finally
           SL.Free;
       end;
   finally
       Resp.Free;
   end;
end;

procedure TJNavOnline.CBXmlChange(Sender: TObject);
begin
   NoMemo.Clear;
   if CBXml.Text <> '' then begin
       if FileExists(base_dir+Copy(CBXml.Text, 1, Pos(' ',CBXml.Text)-1) ) then begin
           NoMemo.Lines.LoadFromFile(base_dir+Copy(CBXml.Text, 1, Pos(' ',CBXml.Text)-1));
       end;
   end;
end;

procedure TJNavOnline.CreateHeader2(no : IXMLNode; reqstr : string);
var
   n1      : IXMLNode;
   newid   : string;
   newstr  : string;
   ts      : string;
begin
   // A fejl�c adatok l�trehoz�sa
   Sorszamiro;
   newid   := elotag + IntToStr(sorszam);
   ts      := GetTimeStamp(now);
   n1      := no.AddChild('header');
   BuildTextNode( n1, 'requestId', newid) ;
   BuildTextNode( n1, 'timestamp', ts ) ;
   BuildTextNode( n1, 'requestVersion', '1.0') ;
   BuildTextNode( n1, 'headerVersion', '1.0') ;
   // A user adatok l�trehoz�sa
   newstr  := newid+copy(ts, 1, 4)+copy(ts, 6, 2)+copy(ts, 9, 2)+copy(ts, 12, 2)+copy(ts, 15, 2)+copy(ts, 18, 2)+xmlkey;
   if Trim(reqstr) <> '' then begin
       newstr  := newstr + reqstr;
   end;
   n1      := no.AddChild('user');
   BuildTextNode( n1, 'login', username) ;
   BuildTextNode( n1, 'passwordHash', GetSHA512Str(userpass)) ;
   BuildTextNode( n1, 'taxNumber', usertax) ;
   BuildTextNode( n1, 'requestSignature', GetSHA512Str(newstr)) ;
   // A szoftver adatok l�trehoz�sa
   n1      := no.AddChild('software');
   BuildTextNode( n1, 'softwareId', sw_ID) ;
   BuildTextNode( n1, 'softwareName', sw_name) ;
   BuildTextNode( n1, 'softwareOperation', 'LOCAL_SOFTWARE') ;
end;

procedure TJNavOnline.BitLezarClick(Sender: TObject);
begin
   // K�zzel z�rjuk le a NAV k�ld�st
   if NoticeKi('Figyelem! A k�zi lez�r�s azt jelenti, hogy a sz�ml�val kapcsolatos tov�bbi automatikusa funkci�k megsz�nnek! Folytatja?', 1) <> 0 then begin
       Exit;
   end;
   ret_value   := 'DONE (K�ZZEL !!!)';
   Close;
end;

procedure TJNavOnline.BitStatClick(Sender: TObject);
begin
   // Lek�rdezz�k a st�tuszt
   StatuszKuldes;
end;

procedure TJNavOnline.BitInvClick(Sender: TObject);
var
   eredm   : boolean;
begin
   // Sz�mla lek�rdez�s
   Screen.Cursor   := crHourGlass;
   NoMemo.Clear;
   NOMemo.Lines.Add('A sz�mla lek�rdez�s elindult ...');
   NoMemo.Update;
   MemoAll.Clear;
   GombMutato(false);
   eredm    := InvoiceRequest(invnum) ;
   NOMemo.Lines.Add('Sz�mla lek�rdez�s eredm�nye : '+res1+'; '+res2+'; '+res3+'; ');
   if not eredm then begin
       NOMemo.Lines.Add(GetErrorStr);
   end;
   // Az eredm�ny ment�se
   XmlMentes('INV');
   GombMutato(true);
   Screen.Cursor   := crDefault;
end;

procedure TJNavOnline.BitOKClick(Sender: TObject);
begin
   ret_value   := invstatus;
   Close;
end;

procedure TJNavOnline.BuildTextNode( node0 : IXMLNode; name, str : string );
var
   nnn : IXMLNode;
begin
   nnn      := node0.AddChild(name);
   nnn.Text := str;
end;

function TJNavOnline.GetTimeStamp( td : TDateTime) : String;
begin
    Result := FormatDateTime('YYYY-MM-DD', now)+'T'+FormatDateTime('hh:nn:ss.zzz', td)+'Z';
end;

function TJNavOnline.GetSHA512Str(const str : Ansistring) : Ansistring;
var
  SHA: TIdHashSHA512;
begin
   Result  := '';
   IdSSLOpenSSL.LoadOpenSSLLibrary;
   SHA:= TIdHashSHA512.Create;
// NoticeKi('33');
   try
      Result := UpperCase( SHA.HashStringAsHex(str) );
   finally
      SHA.Free;
   end;
// NoticeKi('34');
   IdSSLOpenSSL.UnLoadOpenSSLLibrary;
end;

// =======================================================================================
// Token k�r�s
// =======================================================================================

procedure TJNavOnline.Timer1Timer(Sender: TObject);
begin
   // Itt vannak z ind�t� funkci�k
   timer1.Enabled := false;
   MStat.Text             := invstatus;
   MTrid.Text             := transactid;
   GombMutato(true);
   if runcommand = 'KULD' then begin
       SzamlaKuldes;
   end;
   if runcommand = 'STATUS' then begin
       StatuszKuldes;
   end;
end;

function TJNavOnline.TokenRequest : string;
var
    xmloutput  : IXMLDocument;
    node1      : IXMLNode;
    ts         : string;
begin
   // A TokenExchangeRequest legener�l�sa
   Result  := '';
   xmloutput   := NewXMLDocument;
   xmloutput.Options := [doNodeAutoIndent];
   xmloutput.Active := true;
   xmloutput.Version   := '1.0';
   node1 := xmloutput.AddChild('TokenExchangeRequest');
   node1.SetAttribute('xmlns', 'http://schemas.nav.gov.hu/OSA/1.0/api');
   CreateHeader2(node1, '');
   xmloutput.XML.Text  := StringReplace(xmloutput.XML.Text, ' xmlns=""', '', [rfReplaceAll]);
   xmloutput.Active    := true;
   Memoelvalaszto(MemoAll, 'TOKEN K�R�S');
   MemoAll.Lines.Add(xmloutput.XML.Text);
   Memoelvalaszto(MemoAll, 'TOKEN V�LASZ');
   ered    := SendXMLData( xmloutput.XML.Text, url1 );
   MemoAll.Lines.Add(resp_xml.XML.Text);
   GetResult( resp_xml.DocumentElement, res1, res2, res3);
   if ered <> 200 then begin
       errorcode := -6;
       errstr  := 'Error code, message, error: '+IntToStr(err_code)+'; '+err_mess+'; '+err_errmsg;
   end else begin
       token       := GetResultSection( resp_xml.DocumentElement, 'encodedExchangeToken');
       validfrom   := GetResultSection( resp_xml.DocumentElement, 'tokenValidityFrom');
       validto     := GetResultSection( resp_xml.DocumentElement, 'tokenValidityTo');
        // Visszafejtj�k a tokent !!!
       token   := DecryptString(token, replacekey);
       Result  := token;
   end;
end;

function TJNavOnline.DecryptString(AData, Akey: string) :string;
var
    aesgen:TSynaAes;
    s  : string;
begin
   // AES decode
   aesgen:=TSynaAes.Create(Akey);
   s := DecodeBase64(Adata);
   Result := Aesgen.DecryptECB(copy(s, 1, 16));
   Result := Result + Aesgen.DecryptECB(copy(s, 17, 16));
   Result := Result + Aesgen.DecryptECB(copy(s, 33, 16));
   aesgen.Free;
end;

procedure TJNavOnline.GetResult(  noderes : IXMLNode; var ME1, ME2, ME3 : string);
var
   nodenew : IXMLNode;
begin
   nodenew   :=  noderes.ChildNodes['result'];
   ME1       :=  nodenew.ChildNodes['funcCode'].Text;
   ME2       :=  nodenew.ChildNodes['errorCode'].Text;
   ME3       :=  nodenew.ChildNodes['message'].Text;
end;

function TJNavOnline.GetResultSection(  noderes : IXMLNode; sec : string) : string;
var
   ii      : integer;
begin
   Result  := '';
   // Az eredm�nyek ki�r�sa az XML-b�l
   ii := 0;
   if noderes.ChildNodes.Count > 0 then begin
       while ii < noderes.ChildNodes.Count do begin
           if noderes.ChildNodes[ii].NodeName = sec then begin
               try
                   Result  := noderes.ChildNodes[ii].Text;
                   ii      := noderes.ChildNodes.Count;
               finally

               end;
           end;
           if ( ( Result = '' ) and ( noderes.ChildNodes[ii].ChildNodes.Count > 0) ) then begin
               Result  := GetResultSection(  noderes.ChildNodes[ii], sec );
           end;
           Inc(ii);
       end;
   end;
end;

procedure TJNavOnline.MemoElvalaszto(mem : TMemo; szoveg : string);
begin
   mem.Lines.Add('');
   mem.Lines.Add('===================================  '+szoveg+'  ===================================');
   mem.Lines.Add('');
end;

// =======================================================================================
// A sz�mla bek�ld�se
// =======================================================================================

function TJNavOnline.SzamlaRequest : string;
var
  xmloutput   : IXMLDocument;
  node1        : IXMLNode;
  instr        : string;
  crc32str     : string;
begin
   Result  := '';
    // A sz�mla legenr�l�sa a sz�mla oszt�ly alapj�n
   xmloutput := NewXMLDocument;
   xmloutput.Options := [doNodeAutoIndent];
   xmloutput.Active := true;
   xmloutput.Version   := '1.0';
   node1 := xmloutput.AddChild('ManageInvoiceRequest');
   node1.SetAttribute('xmlns', 'http://schemas.nav.gov.hu/OSA/1.0/api');
   instr := GetSzamlaStr(Szamla);
   instr := StringReplace(instr, sLinebreak, '', [rfReplaceAll]);
   crc32str    := Format('%U', [CRC32(instr)]);
   CreateHeader2(node1, crc32str);
   BuildTextNode( node1, 'exchangeToken', token ) ;
   CreateInvoiceOperations(node1, instr);
   xmloutput.XML.Text  := StringReplace(xmloutput.XML.Text, ' xmlns=""', '', [rfReplaceAll]);
   xmloutput.Active    := true;
   Memoelvalaszto(MemoAll, 'SZ�MLA K�R�S');
   MemoAll.Lines.Add(xmloutput.XML.Text);
   Memoelvalaszto(MemoAll, 'SZ�MLA V�LASZ');
   ered    := SendXMLData( xmloutput.XML.Text, url2 );
   MemoAll.Lines.Add(resp_xml.XML.Text);
   GetResult( resp_xml.DocumentElement, res1, res2, res3);
   if ered <> 200 then begin
       errorcode := -7;
       errstr  := 'Error code, message, error: '+IntToStr(err_code)+'; '+err_mess+'; '+err_errmsg;
   end else begin
       transactid  := GetResultSection(  resp_xml.DocumentElement, 'transactionId');
       Result      := transactid;
   end;
end;

function  TJNavOnline.GetSzamlaStr(sza : TNAvSzamla) : string;
var
   xmlszamla  : IXMLDocument;
   n1  : IXMLNode;
   n2  : IXMLNode;
   n3  : IXMLNode;
   n4  : IXMLNode;
   n5  : IXMLNode;
   n6  : IXMLNode;
   invstr  : string;
   sorsz   : integer;
   szasor  : TSzamlaSor;
   szaossz : TOsszesen;
begin
    // A sz�mla sz�veg�nek legener�l�sa
   Result := '';
   xmlszamla := TXMLDocument.Create(nil);
   xmlszamla.Active := true;
   xmlszamla.Version   := '1.0';
   n1:= xmlszamla.AddChild('Invoice');
   n1.SetAttribute('xmlns:xs', 'http://www.w3.org/2001/XMLSchema-instance');
   n1.SetAttribute('xmlns', 'http://schemas.nav.gov.hu/OSA/1.0/data');
   n1.SetAttribute('xs:schemaLocation', 'http://schemas.nav.gov.hu/OSA/1.0/data invoiceData.xsd');
   n2      := n1.AddChild('invoiceExchange');
   if inv_storno <> '' then begin
       // Ide j�n a storno fejl�c
       n3      := n2.AddChild('invoiceReference');
       BuildTextNode( n3, 'originalInvoiceNumber', inv_storno) ;
       BuildTextNode( n3, 'modificationIssueDate', GetDatumStr(sza.iIssueDate) ) ;
       BuildTextNode( n3, 'modificationTimestamp', GetTimeStamp(now)) ;
       BuildTextNode( n3, 'modifyWithoutMaster', 'false') ;
   end;

   n3      := n2.AddChild('invoiceHead');

   // Az elad� adatai
   n4      := n3.AddChild('supplierInfo');
   n5      := n4.AddChild('supplierTaxNumber');
   BuildTextNode( n5, 'taxpayerId', sza.supplier.taxid) ;
   BuildTextNode( n5, 'vatCode', sza.supplier.vatcode) ;
   BuildTextNode( n5, 'countyCode', sza.supplier.countyc ) ;
   BuildTextNode( n4, 'supplierName', sza.supplier.name ) ;
   n5      := n4.AddChild('supplierAddress');

   n6      := n5.AddChild('simpleAddress');
   // A c�m megad�sa
   BuildTextNode( n6, 'countryCode', sza.supplier.country) ;
   BuildTextNode( n6, 'postalCode', sza.supplier.postal ) ;
   BuildTextNode( n6, 'city', sza.supplier.city) ;
   BuildTextNode( n6, 'additionalAddressDetail', sza.supplier.address) ;
   if Trim(sza.supplier.bank) <> '' then begin
       BuildTextNode( n4, 'supplierBankAccountNumber', sza.supplier.bank ) ;
   end;

   // A vev� adatai
   n4      := n3.AddChild('customerInfo');
   n5      := n4.AddChild('customerTaxNumber');
   BuildTextNode( n5, 'taxpayerId', sza.customer.taxid ) ;
   BuildTextNode( n5, 'vatCode', sza.customer.vatcode ) ;
   BuildTextNode( n5, 'countyCode', sza.customer.countyc ) ;
   BuildTextNode( n4, 'customerName', sza.customer.name ) ;
   n5      := n4.AddChild('customerAddress');

   n6      := n5.AddChild('simpleAddress');
   // A c�m megad�sa
   BuildTextNode( n6, 'countryCode', sza.customer.country) ;
   BuildTextNode( n6, 'postalCode', sza.customer.postal ) ;
   BuildTextNode( n6, 'city', sza.customer.city) ;
   BuildTextNode( n6, 'additionalAddressDetail', sza.customer.address) ;

   // A sz�mla fejl�c adatai
   n4      := n3.AddChild('invoiceData');
   BuildTextNode( n4, 'invoiceNumber', sza.iNumber ) ;
   BuildTextNode( n4, 'invoiceCategory', sza.iCategory ) ;
   BuildTextNode( n4, 'invoiceIssueDate', GetDatumStr(sza.iIssueDate) ) ;
   BuildTextNode( n4, 'invoiceDeliveryDate', GetDatumStr(sza.iDeliveryDate) ) ;
   BuildTextNode( n4, 'currencyCode', sza.icurrencyCode ) ;
   BuildTextNode( n4, 'paymentMethod', sza.ipaymentMethod ) ;
   BuildTextNode( n4, 'paymentDate', GetDatumStr(sza.ipaymentDate) ) ;
   BuildTextNode( n4, 'invoiceAppearance', sza.iAppearance ) ;
   // A sz�mlasorok berak�sa
   n3      := n2.AddChild('invoiceLines');
   for sorsz := 0 to sza.iSorok.Count - 1 do begin
       szasor  := sza.iSorok[sorsz];
       n4      := n3.AddChild('line');
       BuildTextNode( n4, 'lineNumber', IntToStr(szasor.lNumber) ) ;
       if inv_storno <> '' then begin
           n5      := n4.AddChild('lineModificationReference');
//           BuildTextNode( n5, 'lineNumberReference', IntToStr(szasor.lNumber+sza.iSorok.Count) ) ;
           BuildTextNode( n5, 'lineNumberReference', IntToStr(szasor.lNumber) ) ;
           BuildTextNode( n5, 'lineOperation', 'MODIFY' ) ;
       end;
       n5      := n4.AddChild('productCodes');
       n6      := n5.AddChild('productCode');
       BuildTextNode( n6, 'productCodeCategory', szasor.lproductCodeCategory ) ;
       BuildTextNode( n6, 'productCodeValue', szasor.lproductCodeValue ) ;
       BuildTextNode( n4, 'lineDescription', szasor.lDescription ) ;
       BuildTextNode( n4, 'quantity', GetSzamStr(szasor.lquantity) ) ;
       BuildTextNode( n4, 'unitOfMeasure', szasor.lunitOfMeasure ) ;
       BuildTextNode( n4, 'unitPrice',  GetSzamStr(szasor.lunitPrice) ) ;
       n5      := n4.AddChild('lineAmountsNormal');
       BuildTextNode( n5, 'lineNetAmount', GetSzamStr(szasor.lNetAmount) ) ;
       n6      := n5.AddChild('lineVatRate');
       BuildTextNode( n6, 'vatPercentage', GetSzamStr(szasor.lvatPercentage) ) ;
       BuildTextNode( n5, 'lineVatAmount', GetSzamStr(szasor.lVatAmount) ) ;
       BuildTextNode( n5, 'lineGrossAmountNormal', GetSzamStr(szasor.lGrossAmountNormal) ) ;
   end;

   // Az �sszesen  berak�sa
   n3      := n2.AddChild('invoiceSummary');
   n4      := n3.AddChild('summaryNormal');
   for sorsz := 0 to sza.iOsszesenek.Count - 1 do begin
       szaossz  := sza.iOsszesenek[sorsz];
       n5      := n4.AddChild('summaryByVatRate');
       n6      := n5.AddChild('vatRate');
       BuildTextNode( n6, 'vatPercentage', GetSzamStr(szaossz.ovatPercentage) ) ;
       BuildTextNode( n5, 'vatRateNetAmount', GetSzamStr(szaossz.ovatRateNetAmount) ) ;
       BuildTextNode( n5, 'vatRateVatAmount', GetSzamStr(szaossz.ovatRateVatAmount) ) ;
       BuildTextNode( n5, 'vatRateVatAmountHUF', GetSzamStr(szaossz.ovatRateVatAmountHUF) ) ;
       BuildTextNode( n5, 'vatRateGrossAmount', GetSzamStr(szaossz.ovatRateGrossAmount) ) ;
   end;

   BuildTextNode( n4, 'invoiceNetAmount', GetSzamStr(sza.iNetAmount) ) ;
   BuildTextNode( n4, 'invoiceVatAmount', GetSzamStr(sza.iVatAmount) ) ;
   BuildTextNode( n4, 'invoiceVatAmountHUF', GetSzamStr(sza.iVatAmountHUF) ) ;
   BuildTextNode( n3, 'invoiceGrossAmount', GetSzamStr(sza.iGrossAmount) ) ;

   xmlszamla.XML.Text  := StringReplace(xmlszamla.XML.Text, ' xmlns=""', '', [rfReplaceAll]);
   xmlszamla.Active    := true;
   invstr  := Encode64Str( xmlszamla.XML.Text );
   xmlszamla.XML.Text  := Xml.XMLDoc.FormatXMLData(xmlszamla.XML.Text);
   xmlszamla.Active    := true;
   // Be�rjuk az olvashat� sz�mla tartalmat
   Memoelvalaszto(MemoAll, 'SZ�MLA TARTALOM');
   Szamlakiiro;
   // Be�rjuk a sz�mla XML -t
   Memoelvalaszto(MemoAll, 'SZ�MLA XML TARTALOM');
   MemoAll.Lines.Add(xmlszamla.XML.Text);
   Result  := invstr;
end;

function TJNavOnline.GetSzamStr( sz : double) : string;
begin
   // A sz�mot �talak�tjuk 2 tizedesjegyes .-os sz�vegg�
   Result := StringReplace(Format('%.2f', [sz]), ',', '.', [rfReplaceAll]);
end;

function TJNavOnline.GetDatumStr( str : string) : string;
begin
    // A d�tum �talak�t�sa
    Result := copy(str,1,4)+'-'+copy(str, 6,2)+'-'+copy(str, 9,2);
end;

procedure TJNavOnline.CreateInvoiceOperations(no : IXMLNode; invstr : string);
var
   n1  : IXMLNode;
   n2  : IXMLNode;
begin
   // A fejl�c adatok l�trehoz�sa
   n1      := no.AddChild('invoiceOperations');
   BuildTextNode( n1, 'technicalAnnulment', 'false') ;
   BuildTextNode( n1, 'compressedContent', 'false') ;
   // Itt j�nnek a sz�ml�k
   n2      := n1.AddChild('invoiceOperation');
   BuildTextNode( n2, 'index', '1') ;
   if inv_storno = '' then begin
       BuildTextNode( n2, 'operation', 'CREATE') ;
   end else begin
       BuildTextNode( n2, 'operation', 'STORNO') ;
   end;
   BuildTextNode( n2, 'invoice', invstr) ;
end;


function TJNavOnline.Encode64Str( str : string): String;
begin
    result := TIdEncoderMIME.EncodeString(str, IndyTextEncoding_UTF8);
//    result := TNetEncoding.Base64.Encode(str);
end;

// =======================================================================================
// St�tusz lek�rdez�s
// =======================================================================================

function TJNavOnline.StatusRequest( trcode : string ) : boolean;
var
    xmloutput  : IXMLDocument;
    node1      : IXMLNode;
    node2      : IXMLNode;
    ii         : integer;
    estr       : string;
begin
   // A st�tusz lek�rdez�s legener�l�sa
   Result  := false;
   xmloutput   := NewXMLDocument;
   xmloutput.Options := [doNodeAutoIndent];
   xmloutput.Active := true;
   xmloutput.Version   := '1.0';
   node1 := xmloutput.AddChild('QueryInvoiceStatusRequest');
   node1.SetAttribute('xmlns', 'http://schemas.nav.gov.hu/OSA/1.0/api');
   CreateHeader2(node1, '');
   BuildTextNode( node1, 'transactionId', trcode ) ;
   BuildTextNode( node1, 'returnOriginalRequest', 'true' ) ;
   xmloutput.XML.Text  := StringReplace(xmloutput.XML.Text, ' xmlns=""', '', [rfReplaceAll]);
   xmloutput.Active    := true;
   Memoelvalaszto(MemoAll, 'ST�TUSZ LEK�RDEZ�S');
   MemoAll.Lines.Add(xmloutput.XML.Text);
   Memoelvalaszto(MemoAll, 'ST�TUSZ V�LASZ');
   ered    := SendXMLData( xmloutput.XML.Text, url3 );
   MemoAll.Lines.Add(resp_xml.XML.Text);
   GetResult( resp_xml.DocumentElement, res1, res2, res3);
   if ered <> 200 then begin
       errorcode := -8;
       errstr  := 'Error code, message, error: '+IntToStr(err_code)+'; '+err_mess+'; '+err_errmsg;
   end else begin
       if Pos('DONE', invstatus) = 0 then begin
           invstatus   := GetResultSection( resp_xml.DocumentElement, 'invoiceStatus');
       end;
       // A hibalista kiszed�se
       err_list.Clear;
       node1   :=  resp_xml.DocumentElement.ChildNodes['processingResults'];
       node1   :=  node1.ChildNodes['processingResult'];
       for ii := 0 to node1.ChildNodes.Count-1 do begin
           node2   := node1.ChildNodes[ii];
           if node2.LocalName = 'technicalValidationMessages' then begin
               err_list.Add( node2.ChildNodes['validationResultCode'].Text+';'+node2.ChildNodes['validationErrorCode'].Text+';'+node2.ChildNodes['message'].Text);
           end;
           if node2.LocalName = 'schemaValidationMessages' then begin
               err_list.Add( node2.ChildNodes['validationResultCode'].Text+';'+node2.ChildNodes['validationErrorCode'].Text+';'+node2.ChildNodes['message'].Text);
           end;
           if node2.LocalName = 'businessValidationMessages' then begin
               err_list.Add( node2.ChildNodes['validationResultCode'].Text+';'+node2.ChildNodes['validationErrorCode'].Text+';'+node2.ChildNodes['message'].Text);
               err_list.Add( 'sor :'+ node2.ChildNodes['pointer'].ChildNodes['line'].Text + '; '+node2.ChildNodes['pointer'].ChildNodes['tag'].Text+'; '+node2.ChildNodes['pointer'].ChildNodes['tag'].Text);
           end;
       end;
       // A warning kiszed�se
       Result  := true;
   end;
end;

// =======================================================================================
// Sz�mla  �llapot kiolvas�s
// =======================================================================================

function TJNavOnline.InvoiceRequest( invcode : string ) : boolean;
var
    xmloutput  : IXMLDocument;
    node1      : IXMLNode;
    node2      : IXMLNode;
    ts         : string;
    newreqid   : string;
begin
   Result  := false;
   xmloutput   := NewXMLDocument;
   xmloutput.Options := [doNodeAutoIndent];
   xmloutput.Active := true;
   xmloutput.Version   := '1.0';
   node1 := xmloutput.AddChild('QueryInvoiceDataRequest');
   node1.SetAttribute('xmlns', 'http://schemas.nav.gov.hu/OSA/1.0/api');
   newreqid    := elotag + IntToStr(sorszam);
   CreateHeader2(node1, '');
   BuildTextNode( node1, 'page',  '1');
   node2   := node1.AddChild('invoiceQuery');
   BuildTextNode( node2, 'invoiceNumber',  invcode);
   BuildTextNode( node2, 'requestAllModification',  'false');
   xmloutput.XML.Text  := StringReplace(xmloutput.XML.Text, ' xmlns=""', '', [rfReplaceAll]);
   xmloutput.Active    := true;
   Memoelvalaszto(MemoAll, 'SZ�MLA LEK�RDEZ�S');
   MemoAll.Lines.Add(xmloutput.XML.Text);
   Memoelvalaszto(MemoAll, 'SZ�MLA LEK�RDEZ�S V�LASZ');
   ered    := SendXMLData( xmloutput.XML.Text, url4 );
   MemoAll.Lines.Add(resp_xml.XML.Text);
   GetResult( resp_xml.DocumentElement, res1, res2, res3);
   if ered <> 200 then begin
       errorcode := -9;
       errstr  := 'Error code, message, error: '+IntToStr(err_code)+'; '+err_mess+'; '+err_errmsg;
   end else begin
       Result  := true;
   end;
end;

procedure TJNavOnline.SorszamIro;
begin
   Inc(sorszam);
   if IniNyit then begin
       try
           IniFile.WriteString( 'GENERAL', 'SORSZAM', IntToStr(sorszam) );
       finally
           IniFile.UpdateFile;
           IniFile.Free;
       end;
   end;
end;

procedure TJNavOnline.Szamlakuldes;
var
   eredmeny : boolean;
begin
   GombMutato(false);
   // Bek�ldj�k a sz�ml�t a NAV -hoz
   try
       Screen.Cursor   := crHourGlass;
       NOMemo.Lines.Add('A sz�mla k�ld�s elindult ...');

       token   := TokenRequest;
//       Memo7.Lines.Add('Request ID : '+requestid);
       NOMemo.Lines.Add('A token k�r�s eredm�nye : '+res1+'; '+res2+'; '+res3+'; ');
       if token = '' then begin
           NOMemo.Lines.Add(GetErrorStr);
           Exit;
       end;
       // Sz�mla k�ld�s
       token   := SzamlaRequest;
       NOMemo.Lines.Add('Sz�mla k�ld�s eredm�ny : '+res1+'; '+res2+'; '+res3+'; ');
       if token = '' then begin
           NOMemo.Lines.Add(GetErrorStr);
           Exit;
       end;
       NOMemo.Lines.Add('Transact ID : '+token);
       // St�tusz lek�rdez�s
       eredmeny    := StatusRequest(token);
       NOMemo.Lines.Add('St�tusz lek�rdez�s eredm�ny : '+res1+'; '+res2+'; '+res3+'; ');
       if not eredmeny then begin
           NOMemo.Lines.Add(GetErrorStr);
           Exit;
       end;
       NOMemo.Lines.Add('Sz�mla st�tusza : '+invstatus);
       if err_list.Count > 0 then begin
           NOMemo.Lines.Add(err_list.Text);
       end;
(*
       proba   := 1;
       while ( ( invstatus <> 'DONE' ) and ( proba < 5) AND ( invstatus <> 'ABORTED' )  ) or ( not eredmeny) do begin
           NOMemo.Lines.Add('A k�ld�s�  ');
           Sleep(5000);
           eredmeny    := StatusRequest(token);
           NOMemo.Lines.Add('St�tusz k�ld�s eredm�ny : '+res1+'; '+res2+'; '+res3+'; ');
           NOMemo.Lines.Add('Sz�mla st�tusza : '+invstatus, false);
           if err_list.Count > 0 then begin
               NOMemo.Lines.Add(err_list.Text, false);
           end;
           Inc(proba);
       end;
*)
       if not eredmeny then begin
           NOMemo.Lines.Add(GetErrorStr);
           Exit;
       end;

       // Sz�mla lek�rdez�s
       eredmeny    := InvoiceRequest(invnum) ;
       NOMemo.Lines.Add('Sz�mla lek�rdez�s eredm�nye : '+res1+'; '+res2+'; '+res3+'; ');
       if not eredmeny then begin
           NOMemo.Lines.Add(GetErrorStr);
       end;

       // ---- �sszes k�ld�s�hez -----
       // ret_value   := invstatus;
       // Close;
       // ---------------------------

   finally
       // Az eredm�ny ment�se
       Screen.Cursor   := crDefault;
       XmlMentes('ALL');
       GombMutato(true);
   end;
end;

procedure TJNavOnline.XmlMentes( mnev : string);
var
   fnuj    : string;
   sorsz   : integer;
begin
   // A  MEMOALL xml ment�se
   sorsz   := 1;
   fnuj    := base_dir+base_elo+GetGoogName(invnum)+'_'+mnev;
   while FileExists(fnuj+'.xml') do begin
       fnuj    := base_dir+base_elo+GetGoogName(invnum)+'_'+mnev+'_'+Format('%3.3d', [sorsz]);
       Inc(sorsz);
   end;
   fnuj := fnuj+'.xml';
   MemoAll.Lines.SaveToFile(fnuj);
   ListaTolto;
end;

procedure TJNavOnline.StatuszKuldes;
var
   eredmeny : boolean;
begin
   NoMemo.Clear;
   NoMemo.Update;
   GombMutato(false);
   MemoAll.Clear;
   NOMemo.Lines.Add('A st�tusz lek�rdez�s elindult ...');
   // Bek�ldj�k a sz�ml�t a NAV -hoz
   try
       Screen.Cursor   := crHourGlass;
       // St�tusz lek�rdez�s
       eredmeny    := StatusRequest(transactid);
       NOMemo.Lines.Add('St�tusz k�ld�s eredm�ny : '+res1+'; '+res2+'; '+res3+'; ');
       if not eredmeny then begin
           NOMemo.Lines.Add(GetErrorStr);
           Exit;
       end;
       NOMemo.Lines.Add('Sz�mla st�tusza : '+invstatus);
       if err_list.Count > 0 then begin
           NOMemo.Lines.Add(err_list.Text);
       end;
(*
       proba   := 1;
       while ( ( invstatus <> 'DONE' ) and ( proba < 5) AND ( invstatus <> 'ABORTED' )  ) or ( not eredmeny) do begin
           NOMemo.Lines.Add('A k�ld�s�  ');
           Sleep(5000);
           eredmeny    := StatusRequest(token);
           NOMemo.Lines.Add('St�tusz k�ld�s eredm�ny : '+res1+'; '+res2+'; '+res3+'; ');
           NOMemo.Lines.Add('Sz�mla st�tusza : '+invstatus, false);
           if err_list.Count > 0 then begin
               NOMemo.Lines.Add(err_list.Text, false);
           end;
           Inc(proba);
       end;
*)
       if not eredmeny then begin
           NOMemo.Lines.Add(GetErrorStr);
           Exit;
       end;
   finally
       // Az eredm�ny ment�se
       Screen.Cursor   := crDefault;
       XmlMentes('STAT');
       GombMutato(true);
   end;
end;

procedure TJNavOnline.GombMutato(st : boolean);
begin
    // A gombok tilt�sa/enged�lyez�se
    BitLezar.Visible   := st;
    if Pos('DONE', invstatus) > 0 then begin
       BitLezar.visible    := false;
    end;
    BitOK.Visible      := st;
    BitInv.Visible     := st;
    BitStat.Visible    := st;
    Panel4.Visible    := st;
end;

procedure TJNavOnline.ListaTolto;
var
   ii  : integer;
   fdt : integer;
   fn3 : string;
begin
   // Felt�ltj�k az XML list�t
   FLB.Mask       := base_elo+GetGoogName(invnum)+'*.XML';
   FLB.Directory  := base_dir;
   FLB.Update;
   CBXml.Clear;
   CBXml.Items.Add('');
   for ii := 0 to FLB.Count-1 do begin
       fn3 := IncludeTrailingPathDelimiter(FLB.Directory)+FLB.Items[ii];
       fdt := FileAge(fn3);
       CBXml.Items.Add(FLB.Items[ii]+'  '+FormatDateTime('YYYY.MM.DD. hh:nn:ss', FileDateToDateTime(fdt)));
   end;
   CBXml.ItemIndex    := 0;
end;

function TJNavOnline.GetGoogName(str : string) : string;
var
   str2    : string;
begin
   str2    := StringReplace(str, '\','_',[rfReplaceAll, rfIgnoreCase]);
   str2    := StringReplace(str2, '/','_',[rfReplaceAll, rfIgnoreCase]);
   Result  := str2;
end;

procedure TJNavOnline.GetVtsz(vtszstr : string; var vtcat, vtval : string);
var
   ssz     : integer;
   vtsz    : string;
begin
   vtcat := '';
   vtval := '';
   vtsz  := vtszstr;
   if Trim(vtsz) = '' then begin
       vtcat := 'OTHER';
       vtval := '00';
       Exit;
   end;
   ssz := Pos('VTSZ', UpperCase(vtsz));
   if ssz > 0 then begin
       vtcat   := 'VTSZ';
       vtsz    := copy(vtsz, 1, ssz-1)+copy(vtsz, ssz+4, 99999);
   end;
   if vtcat = '' then begin
       ssz := Pos('SZJ', UpperCase(vtsz));
       if ssz > 0 then begin
           vtcat   := 'SZJ';
           vtsz    := copy(vtsz, 1, ssz-1)+copy(vtsz, ssz+3, 99999);
       end;
   end;
   if vtcat = '' then begin
       if Pos('.', vtsz) > 0 then begin
           vtcat := 'SZJ';
       end else begin
           vtcat := 'VTSZ';
       end;
   end;
   vtval := StringReplace(Trim(vtsz), '.', '', [rfReplaceAll]);
end;

procedure TJNavOnline.SzamlaKiiro;
var
   ii  : integer;
   so  : TSzamlaSor;
   soro: TOsszesen;
begin
   // A sz�mla ki�r�sa a MemoAll -ba
   MemoAll.Lines.Add('A sz�mla sz�ma : '+invnum);
   if inv_storno <> '' then begin
       MemoAll.Lines.Add('    A storn�zott sz�mla sz�ma : '+inv_storno);
   end;
   MemoAll.Lines.Add('Az elad� adatai : '+Szamla.supplier.name+';  '+Szamla.supplier.country+' '+Szamla.supplier.postal+' '+Szamla.supplier.City+'; '+Szamla.supplier.address+'; '+Szamla.supplier.vatcode);
   MemoAll.Lines.Add('A vev� adatai   : '+Szamla.customer.name+';  '+Szamla.customer.country+' '+Szamla.customer.postal+' '+Szamla.customer.City+'; '+Szamla.customer.address+'; '+Szamla.customer.vatcode);
   MemoAll.Lines.Add('Sz�mla teljes�t�s, kelt, hat�rid�, valutanem : '+Szamla.iDeliveryDate+'; '+Szamla.iIssueDate+'; '+Szamla.ipaymentDate+'; '+Szamla.icurrencyCode);
   MemoAll.Lines.Add('Sz�mla kateg�ria, fizet�si m�d, megjelen�si forma : '+Szamla.iCategory+'; '+Szamla.ipaymentMethod+'; '+Szamla.iAppearance);
   MemoAll.Lines.Add('Sz�mla sorok:');
   for ii := 0 to Szamla.iSorok.Count - 1 do begin
       so  := Szamla.iSorok[ii];
       MemoAll.Lines.Add('   '+IntToStr(so.lNumber)+' sor megnevez�s, kateg�ria:'+so.lDescription+'; '+so.lproductCodeCategory+'; '+so.lproductCodeValue);
       MemoAll.Lines.Add('      mennyis�g, egys�g�r :'+Format('%.f ', [so.lquantity])+so.lunitOfMeasure+Format('  ;  %.f; ', [so.lunitPrice]) );
       MemoAll.Lines.Add('      nett�, �FA %, �FA, brutt� :'+Format('%.f; ', [so.lNetAmount])+Format('%.f; ', [so.lvatPercentage])+Format('%.f; ', [so.lVatAmount])+Format('%.f; ', [so.lVatAmount+so.lNetAmount]) );
   end;
   MemoAll.Lines.Add('�sszesenek:');
   for ii := 0 to Szamla.iOsszesenek.Count - 1 do begin
       soro  := Szamla.iOsszesenek[ii];
       MemoAll.Lines.Add('      �FA %, nett�, �FA, �FA HUF, brutt� :'+Format('%.f; ', [soro.ovatPercentage])+Format('%.f; ', [soro.ovatRateNetAmount])+Format('%.f; ', [soro.ovatRateVatAmount])+Format('%.f; ', [soro.ovatRateVatAmountHUF])+Format('%.f; ', [soro.ovatRateGrossAmount]) );
   end;
   MemoAll.Lines.Add('---------------------------------------------------------------------------------------------');
   MemoAll.Lines.Add('V�g�sszesen nett�, �FA, �FA HUF, brutt� :'+Format('%.f; ', [Szamla.iNetAmount])+Format('%.f; ', [Szamla.iVatAmount])+Format('%.f; ', [Szamla.iVatAmountHUF])+Format('%.f; ', [Szamla.iGrossAmount]) );
   MemoAll.Lines.Add('');
end;


end.
