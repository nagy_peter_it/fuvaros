unit OSzamSorBe;

interface

uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, J_SQL, DB, DBTables, Grids, StdCtrls, Mask, Buttons,
  Egyeb, ADODB, ExtCtrls;

type
  TOSzamSorbeDlg = class(TForm)
    GroupBox1: TGroupBox;
    QFej: TADOQuery;
    QSor: TADOQuery;
    Panel1: TPanel;
    ButtonKuld: TBitBtn;
    ButtonKilep: TBitBtn;
    Panel2: TPanel;
    Label1: TLabel;
    MaskEdit1: TMaskEdit;
    SorGrid: TStringGrid;
    ComboBox1: TComboBox;
    ComboBox2: TComboBox;
    Edit1: TEdit;
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    Label2: TLabel;
    ebVEKOD: TEdit;
    procedure FormCreate(Sender: TObject);
    // procedure Tolto(kod : string; vevonev : string);
    procedure Tolto2(kod : string; vevonev, vevokod : string);
    procedure ButtonKuldClick(Sender: TObject);
    procedure ButtonKilepClick(Sender: TObject);
    procedure SorGridClick(Sender: TObject);
    procedure Kijelol;
    procedure SorGridDrawCell(Sender: TObject; Col, Row: Longint;
      Rect: TRect; State: TGridDrawState);
    procedure ComboBox1Change(Sender: TObject);
    procedure Edit1Change(Sender: TObject);
    procedure MaskEdit1Enter(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
  private
		ujkod	: string;
    fejsorrend, szuro: string;
 //   kod,kijel: array of string;
    skod,skijel: TStrings;
    kisorsz: integer;
   procedure SortStringGrid(var GenStrGrid: TStringGrid; ThatCol: Integer);
  public
		kilep	: boolean;
  end;

var
  OSzamSorbeDlg: TOSzamSorbeDlg;

implementation

uses
	Kozos;
{$R *.DFM}

procedure TOSzamSorbeDlg.FormCreate(Sender: TObject);
var
  i:integer;
begin
//  fejsorrend:='SA_UJKOD';
//  ComboBox1.ItemIndex:=0;
//  ComboBox1.OnChange(self);
  kisorsz:=0;
	EgyebDlg.SetADOQueryDatabase(QFej);
	EgyebDlg.SetADOQueryDatabase(QSor);
  skod:=TStringList.Create;
  skijel:=TStringList.Create;
	SorGrid.RowCount 		:= 2;
	SorGrid.Cells[0,0] 		:= 'Kijel�lt';
  //SorGrid.Canvas.Font.Color:=clBlue;
	SorGrid.Cells[1,0] 		:= 'K�d';
  //SorGrid.Canvas.Font.Color:=clBlack;
	SorGrid.Cells[2,0] 		:= 'J�rat';
	SorGrid.Cells[3,0] 		:= 'Telj.d�t.';
	SorGrid.Cells[4,0] 		:= 'Megjegyz�s';
	SorGrid.Cells[5,0] 		:= 'Nett�';
	SorGrid.Cells[6,0] 		:= '�FA';
	SorGrid.Cells[7,0] 		:= 'V.nem';
	SorGrid.Cells[8,0] 		:= 'V.nett�';
	SorGrid.Cells[9,0] 		:= 'V.�FA';
	SorGrid.Cells[10,0] 		:= 'R.';
  SorGrid.Cells[11,0] 		:= 'Poz�ci�';
  SorGrid.Cells[12,0] 		:= 'Ref.';
  SorGrid.Cells[13,0] 		:= 'Megb.k�d';
	SorGrid.ColWidths[0] 	:= 60;
	SorGrid.ColWidths[1] 	:= 55;
	SorGrid.ColWidths[2] 	:= 60;
	SorGrid.ColWidths[3] 	:= 90;
	SorGrid.ColWidths[4] 	:= 250;
	SorGrid.ColWidths[5] 	:= 80;
	SorGrid.ColWidths[6] 	:= 60;
	SorGrid.ColWidths[7] 	:= 50;
	SorGrid.ColWidths[8] 	:= 80;
	SorGrid.ColWidths[9] 	:= 60;
	SorGrid.ColWidths[10] 	:= 30;
	SorGrid.ColWidths[11] 	:= 180;
  SorGrid.ColWidths[12] 	:= 100;
  SorGrid.ColWidths[13] 	:= 68;
	SetMaskEdits([MaskEdit1]);

  ComboBox1.ItemIndex:=1;
  ComboBox1.OnChange(self);
end;

// �j Tolto2, vevokod alapj�n dolgozik
procedure TOSzamSorbeDlg.Tolto2(kod : string; vevonev, vevokod : string);
var
  OK: boolean;
  i:integer;
  jaratsz,edij, vikod, Netto, S: string;
begin
 	Screen.Cursor	:= crHourGlass;
  SorGrid.RowCount:=2;
	SorGrid.Cells[1,1] := '';
	ujkod	:= kod;

	if ujkod <> '' then begin
		{Bet�ltj�k a megfelel� elemeket}
		MaskEdit1.Text	:= vevonev;
    ebVEKOD.Text	:= vevokod;
    {if szuro='' then
    begin
		  OK:= Query_Run (QSor, 'SELECT isnull((SA_VALOSS / AR_EGYSEG * AR_ERTEK), 0) SZAMITOTT_NETTO,  SZFEJ.* '+
      ' FROM SZFEJ left outer join arfolyam on AR_VALNEM = SA_VALNEM and AR_DATUM = SA_ARFDAT '+
      ' WHERE SA_VEVOKOD = '''+vevokod +
			''' AND SA_FLAG = ''1'' AND ( ( SA_RESZE IS NULL ) OR ( SA_RESZE < 1 ) ) '+
			' AND SA_UJKOD NOT IN (SELECT S2_UJKOD FROM SZSOR2) ' +
      ' AND SA_UJKOD     IN (SELECT VI_UJKOD FROM VISZONY) '+
			' ORDER BY '+fejsorrend,true);

    end
    else
    begin
		  OK:= Query_Run (QSor, 'SELECT isnull((SA_VALOSS / AR_EGYSEG * AR_ERTEK), 0) SZAMITOTT_NETTO,  SZFEJ.* '+
      ' FROM SZFEJ left outer join arfolyam on AR_VALNEM = SA_VALNEM and AR_DATUM = SA_ARFDAT '+
      ' WHERE SA_VEVOKOD = '''+vevokod +
			''' AND SA_FLAG = ''1'' AND ( ( SA_RESZE IS NULL ) OR ( SA_RESZE < 1 ) ) '+
			' AND SA_UJKOD NOT IN (SELECT S2_UJKOD FROM SZSOR2) ' +
      ' AND SA_UJKOD     IN (SELECT VI_UJKOD FROM VISZONY) '+
      ' and '+fejsorrend+' LIKE '''+szuro+'%'+'''ORDER BY '+fejsorrend,true);

    end;  }

    S:= 'SELECT isnull((SA_VALOSS / AR_EGYSEG * AR_ERTEK), 0) SZAMITOTT_NETTO,  MB_MBKOD, MB_INTREF, SZFEJ.* '+
      ' FROM SZFEJ '+
      ' left join VISZONY on VI_UJKOD = SA_UJKOD '+
      ' left outer join arfolyam on AR_VALNEM = SA_VALNEM and AR_DATUM = SA_ARFDAT '+
      ' left outer join MEGBIZAS on MB_VIKOD = VI_VIKOD '+
      ' WHERE SA_VEVOKOD = '''+vevokod +
			''' AND SA_FLAG = ''1'' AND ( ( SA_RESZE IS NULL ) OR ( SA_RESZE < 1 ) ) '+
			' AND SA_UJKOD NOT IN (SELECT S2_UJKOD FROM SZSOR2) ' +
      ' and VI_UJKOD is not null ';
    if szuro <> '' then
      S:= S+' and '+fejsorrend+' LIKE '''+szuro+'%'+'''';
    S:= S+' ORDER BY '+fejsorrend;
    OK:= Query_Run (QSor, S);

    if OK then
    begin
			while not QSor.EOF do begin
				if SorGrid.Cells[1, 1] <> '' then begin
				  SorGrid.RowCount := SorGrid.RowCount + 1;
				end;
        i:=skod.IndexOf(QSor.FieldByName('SA_UJKOD').AsString);
 				SorGrid.Cells[0,SorGrid.RowCount-1] := '' ;
        if (i>-1)and(skijel[i]<>'') then
	  			SorGrid.Cells[0,SorGrid.RowCount-1] :=skijel[i] ;
//	  			SorGrid.Cells[0,SorGrid.RowCount-1] := '***';
				SorGrid.Cells[1,SorGrid.RowCount-1] := QSor.FieldByName('SA_UJKOD').AsString;
				SorGrid.Cells[2,SorGrid.RowCount-1] := QSor.FieldByName('SA_JARAT').AsString;
				SorGrid.Cells[3,SorGrid.RowCount-1] := QSor.FieldByName('SA_TEDAT').AsString;
				SorGrid.Cells[4,SorGrid.RowCount-1] := QSor.FieldByName('SA_MEGJ').AsString;
        if QSor.FieldByName('SA_OSSZEG').AsFloat>=0.01 then
          Netto := QSor.FieldByName('SA_OSSZEG').AsString
        else Netto := SzamString(QSor.FieldByName('SZAMITOTT_NETTO').AsInteger, 12, 0, False);
				SorGrid.Cells[5,SorGrid.RowCount-1] := Netto;
				SorGrid.Cells[6,SorGrid.RowCount-1] := QSor.FieldByName('SA_AFA').AsString;
				SorGrid.Cells[7,SorGrid.RowCount-1] := QSor.FieldByName('SA_VALNEM').AsString;
				SorGrid.Cells[8,SorGrid.RowCount-1] := QSor.FieldByName('SA_VALOSS').AsString;
				SorGrid.Cells[9,SorGrid.RowCount-1] := QSor.FieldByName('SA_VALAFA').AsString;
				SorGrid.Cells[11,SorGrid.RowCount-1] := QSor.FieldByName('SA_POZICIO').AsString;
        SorGrid.Cells[12,SorGrid.RowCount-1] := QSor.FieldByName('MB_INTREF').AsString;
        SorGrid.Cells[13,SorGrid.RowCount-1] := QSor.FieldByName('MB_MBKOD').AsString;

        jaratsz:=  Query_Select('VISZONY','VI_UJKOD',QSor.FieldByName('SA_UJKOD').AsString,'VI_JAKOD');
        vikod:=  Query_Select('VISZONY','VI_UJKOD',QSor.FieldByName('SA_UJKOD').AsString,'VI_VIKOD');
        edij:=Query_Select2('MEGBIZAS','MB_JAKOD','MB_VIKOD', jaratsz,vikod,'MB_EDIJ');
        //edij:=Query_Select('MEGBIZAS','MB_JAKOD',jaratsz,'MB_EDIJ');
        if edij='1' then
  				SorGrid.Cells[10,SorGrid.RowCount-1] :='X';
				QSor.Next;
			end;
		end;
	end;
	SorGrid.Refresh;
 	Screen.Cursor	:= crDefault;
  Try Edit1.SetFocus Except End;
end;

{
// R�gi Tolto, vevonev alapj�n dolgozik
procedure TOSzamSorbeDlg.Tolto(kod : string; vevonev : string);
var
  OK: boolean;
  i:integer;
  jaratsz,edij, vikod: string;
begin
  SorGrid.RowCount:=2;
	SorGrid.Cells[1,1] := '';
	ujkod	:= kod;

	if ujkod <> '' then begin
		// Bet�ltj�k a megfelel� elemeket
		MaskEdit1.Text	:= vevonev;
    if szuro='' then
    begin
		  OK:= Query_Run (QSor, 'SELECT * FROM SZFEJ WHERE SA_VEVONEV = '''+vevonev +
			''' AND SA_FLAG = ''1'' AND ( ( SA_RESZE IS NULL ) OR ( SA_RESZE < 1 ) ) '+
			' AND SA_UJKOD NOT IN (SELECT S2_UJKOD FROM SZSOR2) ' +
      ' AND SA_UJKOD     IN (SELECT VI_UJKOD FROM VISZONY) '+
			' ORDER BY '+fejsorrend,true);

    end
    else
    begin
		  OK:= Query_Run (QSor, 'SELECT * FROM SZFEJ WHERE SA_VEVONEV = '''+vevonev +
			''' AND SA_FLAG = ''1'' AND ( ( SA_RESZE IS NULL ) OR ( SA_RESZE < 1 ) ) '+
			' AND SA_UJKOD NOT IN (SELECT S2_UJKOD FROM SZSOR2) ' +
      ' AND SA_UJKOD     IN (SELECT VI_UJKOD FROM VISZONY) '+
      ' and '+fejsorrend+' LIKE '''+szuro+'%'+'''ORDER BY '+fejsorrend,true);

    end;

    if OK then
    begin
			while not QSor.EOF do begin
				if SorGrid.Cells[1, 1] <> '' then begin
				  SorGrid.RowCount := SorGrid.RowCount + 1;
				end;
        i:=skod.IndexOf(QSor.FieldByName('SA_UJKOD').AsString);
 				SorGrid.Cells[0,SorGrid.RowCount-1] := '' ;
        if (i>-1)and(skijel[i]<>'') then
	  			SorGrid.Cells[0,SorGrid.RowCount-1] :=skijel[i] ;
//	  			SorGrid.Cells[0,SorGrid.RowCount-1] := '***';
				SorGrid.Cells[1,SorGrid.RowCount-1] := QSor.FieldByName('SA_UJKOD').AsString;
				SorGrid.Cells[2,SorGrid.RowCount-1] := QSor.FieldByName('SA_JARAT').AsString;
				SorGrid.Cells[3,SorGrid.RowCount-1] := QSor.FieldByName('SA_TEDAT').AsString;
				SorGrid.Cells[4,SorGrid.RowCount-1] := QSor.FieldByName('SA_MEGJ').AsString;
				SorGrid.Cells[5,SorGrid.RowCount-1] := QSor.FieldByName('SA_OSSZEG').AsString;
				SorGrid.Cells[6,SorGrid.RowCount-1] := QSor.FieldByName('SA_AFA').AsString;
				SorGrid.Cells[7,SorGrid.RowCount-1] := QSor.FieldByName('SA_VALNEM').AsString;
				SorGrid.Cells[8,SorGrid.RowCount-1] := QSor.FieldByName('SA_VALOSS').AsString;
				SorGrid.Cells[9,SorGrid.RowCount-1] := QSor.FieldByName('SA_VALAFA').AsString;


        jaratsz:=  Query_Select('VISZONY','VI_UJKOD',QSor.FieldByName('SA_UJKOD').AsString,'VI_JAKOD');
        vikod:=  Query_Select('VISZONY','VI_UJKOD',QSor.FieldByName('SA_UJKOD').AsString,'VI_VIKOD');
        edij:=Query_Select2('MEGBIZAS','MB_JAKOD','MB_VIKOD', jaratsz,vikod,'MB_EDIJ');
        //edij:=Query_Select('MEGBIZAS','MB_JAKOD',jaratsz,'MB_EDIJ');
        if edij='1' then
  				SorGrid.Cells[10,SorGrid.RowCount-1] :='X';


				QSor.Next;
			end;
		end;
	end;
	SorGrid.Refresh;
  Try Edit1.SetFocus Except End;
end;

}

procedure TOSzamSorbeDlg.ButtonKuldClick(Sender: TObject);
var
	ii 		: integer;
	maxi	: integer;
begin
  if StrToInt( Label2.Caption)=0 then
  begin
  	kilep	:= false;
    close;
  end;
  // sorsz�m sorrendbe tenni
  SortStringGrid(sorGrid,0);

	{A maxim�lis sor�rt�k meghat�roz�sa}
	Query_Run(QFej, 'SELECT MAX(S2_S2SOR) MAXKOD FROM SZSOR2 WHERE S2_S2KOD = '+ujkod, true);
	if QFej.RecordCount < 1 then begin
		maxi	:= 1;
	end else begin
		maxi := QFej.Fieldbyname('MAXKOD').AsInteger + 1;
	end;
	for ii := 1 to SorGrid.RowCount - 1 do begin
//		if ( ( SorGrid.Cells[1,ii] <> '' ) AND ( SorGrid.Cells[0,ii] = '***' ) ) then begin
		if ( ( SorGrid.Cells[1,ii] <> '' ) AND ( Trim(SorGrid.Cells[0,ii]) <>'' ) ) then begin
			// Ellen�rizni kell, hogy a kijel�lt elem szerepel-e m�r m�sik �sszes�tett sz�ml�ban
			Query_Run(QFej, 'SELECT * FROM SZSOR2 WHERE S2_UJKOD = '+SorGrid.Cells[1,ii], true);
			if QFej.RecordCount > 0 then begin
				NoticeKi('A kiv�lasztott sz�mla '+ SorGrid.Cells[1,ii]
					+'m�r szerepel m�s �sszes�tett sz�ml�(k)ban!');
				Continue;
			end;
     Try                                 
			if not Query_Run(QSor, 'INSERT INTO SZSOR2 (S2_S2KOD, S2_S2SOR, S2_UJKOD ) '+
				'VALUES ('+ujkod+', '+IntToStr(maxi)+', '+SorGrid.Cells[1,ii]+' ) ', true) then Raise Exception.Create('');

			{A SA_RESZE be�ll�t�sa}
			if not Query_Run(QSor, 'UPDATE SZFEJ SET SA_RESZE = 1 WHERE SA_UJKOD = '+ SorGrid.Cells[1,ii], true) then Raise Exception.Create('');
			Inc(maxi);
     Except
  		NoticeKi('A(z) '+SorGrid.Cells[1,ii]+' k�d� sz�mla beilleszt�se nem siker�lt!');
    END;
    end;
  end;
	kilep	:= false;
	Close;
end;

procedure TOSzamSorbeDlg.ButtonKilepClick(Sender: TObject);
begin
	kilep	:= true;
	Close;
end;

procedure TOSzamSorbeDlg.SorGridClick(Sender: TObject);
begin
	Kijelol;
end;

procedure TOSzamSorbeDlg.Kijelol;
var
  kkod, ssorsz:string;
  i, db:integer;
begin
	kkod:= SorGrid.Cells[1, SorGrid.Row];
  i:=skod.IndexOf(kkod) ;
	if Trim(SorGrid.Cells[0, SorGrid.Row]) = '' then
  begin
      inc(kisorsz);
      ssorsz:='';
      if kisorsz<10 then ssorsz:='0';
      if kisorsz<100 then ssorsz:=ssorsz+ '0';
      ssorsz:=ssorsz+IntToStr(kisorsz);
  		SorGrid.Cells[0, SorGrid.Row] := ssorsz;
      skijel[i]:=ssorsz;
      if ComboBox1.ItemIndex=0 then
        ComboBox1.OnChange(self);
//  		SorGrid.Cells[0, SorGrid.Row] := '***';
//      skijel[i]:='*';
  end
  else
  begin
      if StrToInt(skijel[i]) = kisorsz then
        Dec(kisorsz);
		  SorGrid.Cells[0, SorGrid.Row] := '';
      skijel[i]:='';
  end;
  db:=0;
  for i:=1 to SorGrid.RowCount-1 do
  begin
    if SorGrid.Cells[0, i] <> ''   then
      inc(db);
  end;
  Label2.Caption:=IntToStr(db);
  SorGrid.Refresh;
  Try Edit1.SetFocus Except End;
end;

procedure TOSzamSorbeDlg.SorGridDrawCell(Sender: TObject; Col, Row: Longint;
  Rect: TRect; State: TGridDrawState);
begin
	if Row > 0 then begin
		SorGrid.Canvas.Font.Color	:= clBlack;
//     	if SorGrid.Cells[0, Row] = '***' then begin
     	if Trim(SorGrid.Cells[0, Row]) <>'' then begin
        	{A rekord ki van jel�lve}
        	SorGrid.Canvas.Brush.Color := clAqua;
     	end else begin
        	SorGrid.Canvas.Brush.Color := clWindow;
     	end;
     	SorGrid.Canvas.Rectangle(Rect.Left,Rect.Top, Rect.Right, Rect.Bottom);
     	SorGrid.Canvas.TextOut(Rect.Left+2,Rect.Top+2,SorGrid.Cells[Col, Row]);
  	end;
end;

procedure TOSzamSorbeDlg.ComboBox1Change(Sender: TObject);
begin
 if ComboBox1.itemindex=0 then
 begin
  SortStringGrid(sorGrid,0);
 end
 else
 begin
  fejsorrend:=ComboBox2.Items[ComboBox1.itemindex];
  Edit1.Text:='';
  szuro:=Edit1.Text;
  // Tolto(ujkod,MaskEdit1.Text);
  Tolto2(ujkod,MaskEdit1.Text, ebVEKOD.Text);
 end;
 Try Edit1.SetFocus Except End;
end;

procedure TOSzamSorbeDlg.Edit1Change(Sender: TObject);
begin
  if ComboBox1.ItemIndex=0 then
  begin
    Edit1.Text:='';
    exit;
  end;
  if ComboBox1.Text='' then
  begin
    szuro:='';
    Edit1.Text:=szuro;
    exit;
  end;
  szuro:=Edit1.Text;
  // Tolto(ujkod,MaskEdit1.Text);
  Tolto2(ujkod,MaskEdit1.Text, ebVEKOD.Text);
end;

procedure TOSzamSorbeDlg.MaskEdit1Enter(Sender: TObject);
begin
  Edit1.SetFocus;
end;

procedure TOSzamSorbeDlg.FormShow(Sender: TObject);
var
  i:integer;
begin
  QSor.Last;
//  SetLength(kod,QSor.RecordCount) ;
//  SetLength(kijel,QSor.RecordCount) ;
  QSor.First;
  while not QSor.Eof do begin
    skod.Add(QSor.fieldbyname('SA_UJKOD').AsString) ;
    skijel.Add('');
    QSor.Next;
  end;

end;

procedure TOSzamSorbeDlg.SortStringGrid(var GenStrGrid: TStringGrid;
  ThatCol: Integer);
const
  // Define the Separator
  TheSeparator = '@';
var
  CountItem, I, J, K, ThePosition: integer;
  MyList: TStringList;
  MyString, TempString: string;
begin
  // Give the number of rows in the StringGrid
  CountItem := GenStrGrid.RowCount;
  //Create the List
  MyList        := TStringList.Create;
  MyList.Sorted := False;
  try
    begin
      for I := 1 to (CountItem - 1) do
        MyList.Add(GenStrGrid.Rows[I].Strings[ThatCol] + TheSeparator +
          GenStrGrid.Rows[I].Text);
      //Sort the List
      Mylist.Sort;

      for K := 1 to Mylist.Count do
      begin
        //Take the String of the line (K ? 1)
        MyString := MyList.Strings[(K - 1)];
        //Find the position of the Separator in the String
        ThePosition := Pos(TheSeparator, MyString);
        TempString  := '';
        {Eliminate the Text of the column on which we have sorted the StringGrid}
        TempString := Copy(MyString, (ThePosition + 1), Length(MyString));
        MyList.Strings[(K - 1)] := '';
        MyList.Strings[(K - 1)] := TempString;
      end;

      // Refill the StringGrid
      for J := 1 to (CountItem - 1) do
        GenStrGrid.Rows[J].Text := MyList.Strings[(J - 1)];
    end;
  finally
    //Free the List
    MyList.Free;
  end;
end;

procedure TOSzamSorbeDlg.Button1Click(Sender: TObject);
begin
  SortStringGrid(sorGrid,0);
  
end;

procedure TOSzamSorbeDlg.Button2Click(Sender: TObject);
begin
  SortStringGrid(sorGrid,2);

end;

procedure TOSzamSorbeDlg.Button3Click(Sender: TObject);
begin
  SortStringGrid(sorGrid,3);

end;

end.
