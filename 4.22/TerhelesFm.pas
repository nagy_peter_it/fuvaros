unit TerhelesFm;

interface

uses
	SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
	StdCtrls, Forms, ExtCtrls, Dialogs, J_FOFORMUJ;

type
	TTerhelesFmDlg = class(TJ_FOFORMUJDLG)
	  procedure FormCreate(Sender: TObject);override;
	 procedure Adatlap(cim, kod : string);override;
	end;

var
	TerhelesFmDlg : TTerhelesFmDlg;

implementation

uses
	Egyeb, J_SQL, Terhelesbe;

{$R *.DFM}

procedure TTerhelesFmDlg.FormCreate(Sender: TObject);
begin
	Inherited FormCreate(Sender);
	AddCalcField('DOLGNEV', 'Dolgoz� neve', '1', '0', '25', '0', '25', '0');
	FelTolto('�j terhel�s/j�v��r�s felvitele ', 'Terhel�s/j�v��r�s adatok m�dos�t�sa ',
			'Terhel�sek/j�v��r�sok list�ja', 'TH_THKOD', 'Select * from TEHER ','TEHER', QUERY_ADAT_TAG );
	width 	:= DLG_WIDTH;
	height	:= DLG_HEIGHT;
end;

procedure TTerhelesFmDlg.Adatlap(cim, kod : string);
begin
	Application.CreateForm(TTerhelesbeDlg, AlForm);
	Inherited Adatlap(cim, kod );
end;

end.

