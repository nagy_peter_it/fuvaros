unit Potlekbe;

interface

uses
	SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
	Forms, Dialogs, StdCtrls, Buttons, Mask, DB, DBTables, J_ALFORM, ADODB,
  Grids;

type
	TPotlekbeDlg = class(TJ_AlformDlg)
	BitElkuld: TBitBtn;
	BitKilep: TBitBtn;
	Label12: TLabel;
    Query1: TADOQuery;
    SG1: TStringGrid;
    BitBtn30: TBitBtn;
    BitBtn32: TBitBtn;
	procedure BitKilepClick(Sender: TObject);
	procedure FormCreate(Sender: TObject);
	procedure Tolto(cim : string; teko : string);override;
	procedure BitElkuldClick(Sender: TObject);
    procedure BitBtn30Click(Sender: TObject);
    procedure BitBtn32Click(Sender: TObject);
	procedure Buttonki;
	private
	public
	end;

var
	PotlekbeDlg: TPotlekbeDlg;

implementation

uses
	Egyeb, J_SQL, Kozos;
{$R *.DFM}

procedure TPotlekbeDlg.BitKilepClick(Sender: TObject);
begin
	Close;
end;

procedure TPotlekbeDlg.FormCreate(Sender: TObject);
begin
 	EgyebDlg.SetADOQueryDatabase(Query1);
   SG1.Cells[0,0]	:= 'S�v alja';
   SG1.Cells[1,0]	:= 'S�v teteje';
   SG1.Cells[2,0]	:= '�rt�k %';
end;

procedure TPotlekbeDlg.Tolto(cim : string; teko:string);
var
	i : integer;
begin
	ret_kod 	:= teko;
	Caption 	:= cim;
   // Az �rt�kek bet�lt�se
   SG1.RowCount	:= 2;
   SG1.Rows[1].Clear;
   Query_Run(Query1, 'SELECT * FROM UZEMERT WHERE UA_VEKOD = '''+teko+''' ORDER BY UA_USAV1, UA_USAV2');
   if Query1.RecordCount > 0 then begin
   	SG1.RowCount	:= Query1.RecordCount + 1;
       i	:=1;
       while not Query1.Eof do begin
       	SG1.Cells[0,i]	:= Query1.FieldByName('UA_USAV1').AsString;
       	SG1.Cells[1,i]	:= Query1.FieldByName('UA_USAV2').AsString;
       	SG1.Cells[2,i]	:= Query1.FieldByName('UA_UPERT').AsString;
           Inc(i);
       	Query1.Next;
       end;
   end;
   ButtonKi;
end;

procedure TPotlekbeDlg.BitElkuldClick(Sender: TObject);
var
	i	: integer;
begin
	// SG1.Cells[Acol,Arow]	:= IntToStr(StrToIntDef(Value, 0));
	// A t�bl�zat �rt�keinek ellen�rz�se
   for i := 1 to SG1.RowCount - 1 do begin
   	if StringSzam(SG1.Cells[0,i]) > StringSzam(SG1.Cells[1,i]) then begin
			NoticeKi('Hib�s s�vhat�r megad�sa!');
           SG1.Row	:= i;
           Exit;
       end;
   end;
   // Rendezz�k nagys�g szerint a t�bl�zatot
   TablaRendezo(SG1, [0,1], [1,1], 1, true);
   for i := 1 to SG1.RowCount - 2 do begin
   	if StringSzam(SG1.Cells[1,i]) > StringSzam(SG1.Cells[0,i+1]) then begin
       	NoticeKi('�tk�z�s a s�vok k�z�tt!');
           SG1.Row	:= i;
           Exit;
       end;
   end;

	// A t�bl�zat �rt�keinek t�rol�sa
   Query_Run(Query1, 'DELETE UZEMERT WHERE UA_VEKOD = '''+ret_kod+''' ');
	for i:= 1 to SG1.RowCount - 1 do begin
   	if StringSzam(SG1.Cells[0,i]) > 0 then begin
           Query_Insert(Query1, 'UZEMERT',
               [
               'UA_VEKOD', '''' + ret_kod + '''',
               'UA_USAV1', SqlSzamString(StringSzam(SG1.Cells[0,i]),12,2),
               'UA_USAV2', SqlSzamString(StringSzam(SG1.Cells[1,i]),12,2),
               'UA_UPERT', SqlSzamString(StringSzam(SG1.Cells[2,i]),6,2)
               ]);
       end;
   end;
  	Close;
end;

procedure TPotlekbeDlg.BitBtn30Click(Sender: TObject);
begin
   SG1.RowCount	:= SG1.RowCount + 1;
   ButtonKi;
end;

procedure TPotlekbeDlg.BitBtn32Click(Sender: TObject);
begin
	if SG1.RowCount > 2 then begin
		GridTorles(SG1, SG1.Row);
   end else begin
		if SG1.Cells[0,1] <> '' then begin
       	SG1.Rows[1].Clear;
       end;
   end;
   ButtonKi;
end;

procedure TPotlekbeDlg.Buttonki;
begin
	BitBtn32.Enabled	:= true;
	if SG1.Cells[0,1] = '' then begin
   	BitBtn32.Enabled	:= false;
   end;
end;

end.
