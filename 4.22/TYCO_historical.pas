unit TYCO_historical;

interface

uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls, Egyeb,
  Forms, Dialogs, DBTables, DB, StdCtrls, Buttons, Mask, DBCtrls, Grids,
  Printers, Shellapi, ExtCtrls, ADODB, J_Excel, Lgauge, Math;

type

  TTYCO_HistoricalDlg = class(TForm)
	 Label1: TLabel;
    BitBtn4: TBitBtn;
    BitBtn6: TBitBtn;
    M1: TMaskEdit;
	 M2: TMaskEdit;
	 Label2: TLabel;
	 Label3: TLabel;
	 BitBtn10: TBitBtn;
	 BitBtn1: TBitBtn;
	 Query1: TADOQuery;
	 Query2: TADOQuery;
	 Query3: TADOQuery;
    M50: TMaskEdit;
    M5: TMaskEdit;
    BitBtn5: TBitBtn;
	 procedure FormCreate(Sender: TObject);
	 procedure BitBtn4Click(Sender: TObject);
	 procedure BitBtn6Click(Sender: TObject);
	 procedure M1Click(Sender: TObject);
	 procedure BitBtn10Click(Sender: TObject);
	 procedure BitBtn1Click(Sender: TObject);
	 procedure M1Exit(Sender: TObject);
	 function XLS_Generalas_historical : boolean;
 	 function XLS_Generalas_FTL : boolean;
 	 function XLS_Generalas_milkrun : boolean;
	 procedure BitBtn5Click(Sender: TObject);
  private
		kilepes		: boolean;
  public
		gr			: TStringGrid;
		voltkilepes	: boolean;
		kellshell	: boolean;
		kelluzenet	: boolean;
		kombinalt	: boolean;
    kelluzpotlek: boolean;
  end;

var
  TYCO_HistoricalDlg: TTYCO_HistoricalDlg;

implementation

uses
	j_sql, Kozos, ValVevo, J_DataModule, Kozos_Export,Kombi_Export;
{$R *.DFM}

procedure TTYCO_HistoricalDlg.FormCreate(Sender: TObject);
begin
	gr			:= nil;
	kilepes     := true;
	kellshell	:= true;
	voltkilepes	:= false;
	kelluzenet	:= true;
	kombinalt	:= false;
  kelluzpotlek:=True;
	EgyebDlg.SetADOQueryDatabase(Query1);
	EgyebDlg.SetADOQueryDatabase(Query2);
	EgyebDlg.SetADOQueryDatabase(Query3);
	// Kezdoertekek(M1, M2, M5, M50);
  M1.Text:='2015.06.01.';
  M2.Text:='2015.09.30.';
  M5.text:='<TYCO, TE partnerek>';
  M50.text:='0050,0272,0547,0765,0906,1384,1547,1680,1870,2404,2666';  //  A k�djaik
end;

procedure TTYCO_HistoricalDlg.BitBtn4Click(Sender: TObject);
begin
	if not DatumExit(M1, false) then begin
		Exit;
	end;
	if not DatumExit(M2, false) then begin
		Exit;
	end;
	if M50.Text = '' then begin
		NoticeKi('A megb�z� kiv�laszt�sa k�telez�!');
		// M5.SetFocus;
		Exit;
	end;
	// Az XLS �llom�ny gener�l�sa
	Screen.Cursor	:= crHourGlass;
	XLS_Generalas_historical;
  // XLS_Generalas_FTL;
  // XLS_Generalas_milkrun;
	Screen.Cursor	:= crDefault;
	if kombinalt then begin
		Close;
	end else begin
		NoticeKi('A riport gener�l�sa befejez�d�tt!');
	end;
end;

procedure TTYCO_HistoricalDlg.BitBtn6Click(Sender: TObject);
begin
	if not kilepes then begin
		kilepes	:= true;
	end else begin
		Close;
	end;
end;

procedure TTYCO_HistoricalDlg.M1Click(Sender: TObject);
begin
	with Sender as TMaskEdit do begin
		SelectAll;
	end;
end;

procedure TTYCO_HistoricalDlg.BitBtn10Click(Sender: TObject);
begin
	// EgyebDlg.Calendarbe(M1);
	// EgyebDlg.ElsoNapEllenor(M1, M2);
  EgyebDlg.CalendarBe_idoszak(M1, M2);
end;

procedure TTYCO_HistoricalDlg.BitBtn1Click(Sender: TObject);
begin
	EgyebDlg.Calendarbe(M2, True);
end;

procedure TTYCO_HistoricalDlg.M1Exit(Sender: TObject);
begin
	DatumExit(M1,false);
	// EgyebDlg.ElsoNapEllenor(M1, M2);
end;

function TTYCO_HistoricalDlg.XLS_Generalas_historical : boolean;
const
  Oszlopszam = 18;
var
	fn, S, ujkod, belf_alval, rendsz, gkkat, CONSIGNOR, CONSIGNEE, PICKUP_DATE: String;
	sorszam, i, datkul, orakul, recno, paletta, TotalPaletta, TotalWeight, TotalChgWeight, ACT_WEIGHT, CHG_WEIGHT, MBCount: integer;
	lanossz, suly, suly2, TotalCost: double;
  ORI_CITY, ORI_COUNTRY, ORI_ZIP, ORI_ZIP2, DEST_CITY, DEST_COUNTRY, DEST_ZIP, DEST_ZIP2: string;
	EX: TJExcel;
  kelluzpotl, alvall: boolean;
begin

  belf_alval	:= EgyebDlg.Read_SZGrid( '999', '748' );
//  belf_alval:='0607';

	EllenListTorol;
	result		:= true;
	fn		:= 'TYCO_HIST1_'+M1.Text+'_'+M2.Text;
	while Pos('.', fn) > 0 do begin
		fn		:= copy(fn, 1, Pos('.', fn) - 1 ) + copy(fn, Pos('.', fn) + 1, 999 );
	end;
	fn			:= EgyebDlg.DocumentPath + fn + '.XLS';;
	if FileExists(fn) then begin
		DeleteFile(PChar(fn));
	end;

  S:='select MS1.MS_FELNEV CONSIGNOR, '+ //  Consignor,
        'MS1.MS_FELTEL ORI_CITY, '+ //   Origin_city,
        'MS1.MS_FELIR ORI_ZIP, '+ // 	Origin_zipcode,
		    'SUBSTRING(MS1.MS_FELIR,1,2) ORI_ZIP2, '+ //  Origin_2digit_zipcode,
		    'MS1.MS_FORSZ ORI_COUNTRY, '+ //  Origin_country,
		    'MS2.MS_LERNEV CONSIGNEE, '+ //  Consignee,
		    'MS2.MS_LERTEL DEST_CITY, '+ //  Destination_city,
		    'MS2.MS_LELIR DEST_ZIP, '+ //  Destination_zipcode,
		    'SUBSTRING(MS2.MS_LELIR,1,2) DEST_ZIP2, '+ //  Destination_2digit_zipcode,
		    'MS2.MS_ORSZA DEST_COUNTRY, '+ //  Destination_country,
		    '(SELECT MIN(MS_FETDAT) FELDAT FROM MEGSEGED WHERE MS_MBKOD = MB_MBKOD) PICKUP_DATE, '+ //  Pickup_date,
		    '(SELECT SUM(MS_FSULY) SULY FROM MEGSEGED WHERE MS_MBKOD = MB_MBKOD AND MS_TIPUS = 0 AND MS_FAJKO = 0) ACT_WEIGHT, '+ //   Actual_weight,
		    '(SELECT SUM(case when MS_FEKOB>0 then MS_FEKOB else MS_FSULY end) FROM MEGSEGED WHERE MS_MBKOD = MB_MBKOD AND MS_TIPUS = 0 AND MS_FAJKO = 0) CHG_WEIGHT, '+ //  Chargeable_weight,
        'MB_VIKOD, MB_JAKOD, MB_ALVAL, MB_MBKOD, MB_FUDIJ, MS1.MS_MSKOD MSKOD1, MS2.MS_MSKOD MSKOD2 '+  // a k�lts�gsz�m�t�shoz
		    'FROM MEGBIZAS, MEGSEGED MS1, MEGSEGED MS2 '+
		    'WHERE MS1.MS_MSKOD=(select min(MS_MSKOD) FROM MEGSEGED WHERE MS_MBKOD = MB_MBKOD AND MS_TIPUS = 0) '+
        ' AND MS1.MS_TIPUS = 0 '+
        ' AND MS2.MS_MSKOD=(select max(MS_MSKOD) FROM MEGSEGED WHERE MS_MBKOD = MB_MBKOD AND MS_TIPUS = 1) '+
        ' AND MS2.MS_TIPUS = 1 '+
        ' AND MB_MBKOD IN ( SELECT MS_MBKOD FROM MEGSEGED WHERE MS_FETDAT >= '''+M1.Text+''' AND MS_FETDAT <= '''+M2.Text+''') '+
		    ' AND MB_VEKOD IN ('+M50.Text+') '+
		    'ORDER BY PICKUP_DATE, CONSIGNOR, CONSIGNEE';
  Query_Run(Query1, S);
	if Query1.RecordCount < 1 then begin
		if kelluzenet then begin
			NoticeKi('Nincs a felt�teleknek megfelel� elem!');
		end;
		result		:= false;
		Exit;
	end;
	BitBtn4.Hide;
	// Az XLS megnyit�sa
	EX 	:= TJexcel.Create(Self);
	if not EX.OpenXlsFile(fn) then begin
		if kelluzenet then begin
			NoticeKi('Nem siker�lt megnyitnom az Excel �llom�nyt!');
		end;
	end else begin
		EX.ColNumber	:= Oszlopszam;
		EX.InitRow;
		EX.SetActiveSheet(0);
		// EX.SetActiveSheet(1);
		sorszam			:= 1;

		EX.SetRowcell(1, 'TYCO Historical data extraction');
		EX.SaveRow(sorszam);
		Inc(sorszam);
		Ex.EmptyRow;
		EX.SetRowcell( 1, 'Consignor');
		EX.SetRowcell( 2, 'Origin city');
		EX.SetRowcell( 3, 'Origin zipcode');
    EX.SetRowcell( 4, 'Ori.2-digit zip');
		EX.SetRowcell( 5, 'Origin country ISO-code');
		EX.SetRowcell( 6, 'Consignee');
		EX.SetRowcell( 7, 'Destination city');
		EX.SetRowcell( 8, 'Destination zipcode');
    EX.SetRowcell( 9, 'Dest.2-digit zip');
		EX.SetRowcell(10, 'Destination country ISO-code');
		EX.SetRowcell(11, 'Pickup date (dd/mm/yyyy)');
		EX.SetRowcell(12, 'Calendar week');
		EX.SetRowcell(13, 'Weight unit of measure');
		EX.SetRowcell(14, 'Actual weight');
		EX.SetRowcell(15, 'Chargeable weight');
		EX.SetRowcell(16, 'Volume m3');
		EX.SetRowcell(17, 'Total costs excl. VAT excl. duty');

    EX.SetRowcell(18, 'Darab');
		EX.SaveRow(sorszam);
		Inc(sorszam);

		kilepes				:= false;
		Query1.DisableControls;
    Application.CreateForm(TFormGaugeDlg, FormGaugeDlg);
    FormGaugeDlg.GaugeNyit('Historical adatgy�jt�s folyamatban... ' , '', false);
    recno:=0;
    MBCount:=0;
    TotalCost:=0;
    TotalPaletta:=0;
    TotalWeight:=0;
    TotalChgWeight:=0;
		while ( ( not Query1.Eof ) and (not kilepes) ) do begin
			Application.ProcessMessages;
      FormGaugeDlg.Pozicio ( ( recno * 100 ) div Query1.RecordCount ) ;
      Inc(recno);
      Inc(MBCount);
      lanossz:= StringSzam(Query1.FieldByName('MB_FUDIJ').AsString);
			//Fuvard�j �s �zemanyagp�tl�k kisz�m�t�sa
			Query_Run(Query3, 'SELECT VI_UJKOD FROM VISZONY WHERE VI_VIKOD = '+ IntToStr(StrToIntDef(Query1.FieldByName('MB_VIKOD').AsString, 0)) +
				' AND VI_JAKOD = '''+Query1.FieldByName('MB_JAKOD').AsString+''' ');
			if Query3.RecordCount <> 1  then begin
				// Nins sz�mla
				EllenListAppend('A megb�z�shoz nem tartozik sz�mla! Megb�z�s k�d : '+Query1.FieldByName('MB_MBKOD').AsString);
			  end
      else begin
				ujkod	:= Query3.FieldByName('VI_UJKOD').AsString;
				// A sz�mla k�pz�se
				if StrToIntDef(ujkod, 0) = 0 then begin
					EllenListAppend('A megb�z�shoz nem tartozik sz�mla! Megb�z�s k�d : '+Query1.FieldByName('MB_MBKOD').AsString);
				  end
        else begin
					JSzamla	:= TJSzamla.Create(Self);
					JSzamla.FillSzamla(ujkod);
					// if kelluzpotlek and kelluzpotl and (JSzamla.uapotlekeur = 0)and(pos(JSzamla.szamlaszam ,Kombi_ExportDlg.kellaszamla)=0) then begin
          // 2017-02-13 Mintha ezt a "Kombi_ExportDlg.kellaszamla"-t nem t�lten�nk fel...
          if kelluzpotlek and kelluzpotl and (JSzamla.uapotlekeur = 0) then begin
						EllenListAppend('Az �zemanyag p�tl�k �sszege 0! Sz�mlasz�m : '+JSzamla.szamlaszam);
					  end
          else begin
						EX.SetRowcell(31, Format('%.2f', [JSZAMLA.uapotlekeur]));
						if ( Abs ( JSzamla.ossz_nettoeur - JSzamla.spedicioeur - JSzamla.uapotlekeur - lanossz ) > 0.00001 ) and (pos(Query1.FieldByName('MB_ALVAL').AsString,belf_alval)=0) then begin
              S:='A sz�mla nett� eur - sped�ci�s d�j <> megb�z�si fuvard�j! Sz�mla : '+JSzamla.szamlaszam+'  Megb�z�s k�d : '+Query1.FieldByName('MB_MBKOD').AsString;
              S:=S+' Nett� EUR='+ FormatFloat('0.00', JSzamla.ossz_nettoeur);
              S:=S+' Sped EUR='+ FormatFloat('0.00', JSzamla.spedicioeur);
              S:=S+' �za p�tl. EUR='+ FormatFloat('0.00', JSzamla.uapotlekeur);
              S:=S+' Szla='+ FormatFloat('0.00', lanossz);
							EllenListAppend(S);
						  end
            else begin
              lanossz:= JSzamla.ossz_nettoeur - JSzamla.spedicioeur - JSzamla.uapotlekeur;
							TotalCost:= TotalCost + lanossz;
						  end;
					  end;
          JSzamla.Destroy;
          end; // if StrToIntDef(ujkod
			  end; //Query3.RecordCoun

      // v�ltoz�ba mentj�k, mert a recordset l�pni fog
      CONSIGNOR:= trim(Query1.FieldByName('CONSIGNOR').AsString);
      ORI_CITY:= trim(Query1.FieldByName('ORI_CITY').AsString);
      ORI_ZIP:= trim(Query1.FieldByName('ORI_ZIP').AsString);
      ORI_ZIP2:= trim(Query1.FieldByName('ORI_ZIP2').AsString);
      ORI_COUNTRY:= trim(Query1.FieldByName('ORI_COUNTRY').AsString);
      CONSIGNEE:= trim(Query1.FieldByName('CONSIGNEE').AsString);
      DEST_CITY:= trim(Query1.FieldByName('DEST_CITY').AsString);
      DEST_ZIP:= trim(Query1.FieldByName('DEST_ZIP').AsString);
      DEST_ZIP2:= trim(Query1.FieldByName('DEST_ZIP2').AsString);
      DEST_COUNTRY:= trim(Query1.FieldByName('DEST_COUNTRY').AsString);
      PICKUP_DATE:= Query1.FieldByName('PICKUP_DATE').AsString;
      ACT_WEIGHT:= Query1.FieldByName('ACT_WEIGHT').AsInteger;
      CHG_WEIGHT:= Query1.FieldByName('CHG_WEIGHT').AsInteger;
      paletta	:= Floor(GetPaletta(Query1.FieldByName('MB_MBKOD').AsString));

      TotalPaletta:= TotalPaletta + paletta;
      TotalWeight:=TotalWeight + ACT_WEIGHT;
      TotalChgWeight:=TotalChgWeight + CHG_WEIGHT;

			Query1.Next;
      // ki�r�s, ha blokk v�ge van - azaz b�rmelyik elt�r az el�z�t�l
      if (trim(Query1.FieldByName('CONSIGNOR').AsString) <> CONSIGNOR)
       or (trim(Query1.FieldByName('CONSIGNEE').AsString) <> CONSIGNEE)
       or (trim(Query1.FieldByName('ORI_CITY').AsString) <> ORI_CITY)
       or (trim(Query1.FieldByName('ORI_ZIP').AsString) <> ORI_ZIP)
       or (trim(Query1.FieldByName('ORI_COUNTRY').AsString) <> ORI_COUNTRY)
       or (trim(Query1.FieldByName('DEST_CITY').AsString) <> DEST_CITY)
       or (trim(Query1.FieldByName('DEST_ZIP').AsString) <> DEST_ZIP)
       or (trim(Query1.FieldByName('DEST_COUNTRY').AsString) <> DEST_COUNTRY)
       or (trim(Query1.FieldByName('PICKUP_DATE').AsString) <> PICKUP_DATE)
       or Query1.EOF then begin
          EX.SetRowcell( 1, CONSIGNOR);
          EX.SetRowcell( 2, ORI_CITY);
          EX.SetRowcell( 3, ORI_ZIP);
          EX.SetRowcell( 4, ORI_ZIP2);
          EX.SetRowcell( 5, ORI_COUNTRY);
          EX.SetRowcell( 6, CONSIGNEE);
          EX.SetRowcell( 7, DEST_CITY);
          EX.SetRowcell( 8, DEST_ZIP);
          EX.SetRowcell( 9, DEST_ZIP2);
          EX.SetRowcell(10, DEST_COUNTRY);
          EX.SetRowcell(11, ''''+DatumAngolosra(PICKUP_DATE));
          EX.SetRowcell(12, IntToStr(CalendarWeek(PICKUP_DATE)));
          EX.SetRowcell(13, 'KG');
          EX.SetRowcell(14, TotalWeight);
          EX.SetRowcell(15, TotalChgWeight);

          EX.SetRowcell(16, TotalPaletta); // �gy ker�l bele sz�mk�nt
          EX.SetRowcell(17, TotalCost); // �gy ker�l bele sz�mk�nt
          // debug
          EX.SetRowcell(18, MBCount);

        // Minden rendben volt, lehet menteni a sort
   			EX.SaveRow(sorszam);
	 			Inc(sorszam);

        MBCount:=0;
        TotalCost:=0;
        TotalPaletta:=0;
        TotalWeight:=0;
        TotalChgWeight:=0;
        {CONSIGNOR:= trim(Query1.FieldByName('CONSIGNOR').AsString);
        ORI_CITY:= trim(Query1.FieldByName('ORI_CITY').AsString);
        ORI_ZIP:= trim(Query1.FieldByName('ORI_ZIP').AsString);
        ORI_ZIP2:= trim(Query1.FieldByName('ORI_ZIP2').AsString);
        ORI_COUNTRY:= trim(Query1.FieldByName('ORI_COUNTRY').AsString);
        DEST_CITY:= trim(Query1.FieldByName('DEST_CITY').AsString);
        DEST_ZIP:= trim(Query1.FieldByName('DEST_ZIP').AsString);
        DEST_ZIP2:= trim(Query1.FieldByName('DEST_ZIP2').AsString);
        DEST_COUNTRY:= trim(Query1.FieldByName('DEST_COUNTRY').AsString);
        PICKUP_DATE:= Query1.FieldByName('PICKUP_DATE').AsString;
        ACT_WEIGHT:= Query1.FieldByName('ACT_WEIGHT').AsInteger;
        CHG_WEIGHT:= Query1.FieldByName('CHG_WEIGHT').AsInteger;
        }
        end;  // if

		end; // while
    FormGaugeDlg.Destroy;
	end;
	EX.SaveFileAsXls(fn);
	EX.Free;
	voltkilepes	:= kilepes;
	if ( ( EgyebDlg.ellenlista.Count > 0 ) and kelluzenet ) then begin
		EllenListMutat('TYCO historikus adatok - Ellen�rz� lista', false);
	end;
	BitBtn4.Show;
	if kellshell then begin
		ShellExecute(Application.Handle,'open',PChar(fn), '', '', SW_SHOWNORMAL );
	end;
	kilepes			:= true;
end;

function TTYCO_HistoricalDlg.XLS_Generalas_milkrun : boolean;
const
  Oszlopszam = 28;
var
	fn, S, ujkod, belf_alval, rendsz, gkkat, PrevLANID: String;
	sorszam, i, datkul, orakul, recno, MBCount, FT_TRDAY, ActLANID: integer;
  TotalCost, Weeks: double;
  ORI_COUNTRY, ORI_ZIP, DEST_COUNTRY, DEST_ZIP, FT_GKKAT: string;
	lanossz, suly, paletta, suly2: double;
	EX: TJExcel;
  kelluzpotl, alvall: boolean;
begin

  belf_alval	:= EgyebDlg.Read_SZGrid( '999', '748' );

	EllenListTorol;
	result		:= true;
	fn		:= 'TYCO_HIST3_'+M1.Text+'_'+M2.Text;
	while Pos('.', fn) > 0 do begin
		fn		:= copy(fn, 1, Pos('.', fn) - 1 ) + copy(fn, Pos('.', fn) + 1, 999 );
	end;
	fn			:= EgyebDlg.DocumentPath + fn + '.XLS';
	if FileExists(fn) then begin
		DeleteFile(PChar(fn));
	end;

  S:='select '+
        'FT_LANID, '+
	      'substring(FT_KEZDO,1,2) ORI_COUNTRY, '+
	      'substring(FT_KEZDO,3,100) ORI_ZIP, '+
	      'substring(FT_VEGZO,1,2) DEST_COUNTRY,'+
	      'substring(FT_VEGZO,3,100) DEST_ZIP,'+
	      'FT_GKKAT, '+
	      'FT_TRDAY, '+
        'MB_VIKOD, MB_JAKOD, MB_ALVAL, MB_MBKOD, MB_FUDIJ '+
        'FROM MEGBIZAS '+
        '	  LEFT OUTER JOIN FUVARTABLA ON MB_FUTID = FT_FTKOD'+
        ' WHERE FT_TOL=''2015.05.01.'' '+
        'AND FT_LANID >= 1000 '+
        ' AND MB_MBKOD IN ( SELECT MS_MBKOD FROM MEGSEGED WHERE MS_FETDAT >= '''+M1.Text+''' AND MS_FETDAT <= '''+M2.Text+''') '+
		    ' AND MB_VEKOD IN ('+M50.Text+') '+
		    'ORDER BY MB_FUTID';
  Query_Run(Query1, S);
	if Query1.RecordCount < 1 then begin
		if kelluzenet then begin
			NoticeKi('Nincs a felt�teleknek megfelel� elem!');
		end;
		result		:= false;
		Exit;
	end;
	BitBtn4.Hide;
	// Az XLS megnyit�sa
	EX 	:= TJexcel.Create(Self);
	if not EX.OpenXlsFile(fn) then begin
		if kelluzenet then begin
			NoticeKi('Nem siker�lt megnyitnom az Excel �llom�nyt!');
		end;
	end else begin
		EX.ColNumber	:= Oszlopszam;
		EX.InitRow;
		EX.SetActiveSheet(0);
		// EX.SetActiveSheet(1);
		sorszam			:= 1;

		EX.SetRowcell(1, 'TYCO Historical data extraction - MILKRUNS');
		EX.SaveRow(sorszam);
		Inc(sorszam);
		Ex.EmptyRow;
		EX.SetRowcell( 1, 'Number of trucks per week');
		EX.SetRowcell( 2, 'NEW RFQ ID');
		EX.SetRowcell( 3, 'Taxation base');
    EX.SetRowcell( 4, 'Roundtrip rate');
		EX.SetRowcell( 5, 'Origin country code');
		EX.SetRowcell( 6, 'Location 1st pick up');
		EX.SetRowcell( 7, 'Location 2nd pick up');
		EX.SetRowcell( 8, 'Location 3rd pick up');
		EX.SetRowcell( 9, 'Location 4th pick up');
		EX.SetRowcell( 10, 'Destination country code');
		EX.SetRowcell( 11, 'Location 1st delivery');
		EX.SetRowcell( 12, 'Location 2nd delivery');
		EX.SetRowcell( 13, 'Location 3rd delivery');
		EX.SetRowcell( 14, 'Location 4th delivery');
		EX.SetRowcell( 15, 'Vehicle type');
		EX.SetRowcell( 16, 'Transit Time');
    EX.SetRowcell( 17, 'T.E.Fuel Index');
    EX.SetRowcell( 18, 'Export Declaration Fee');
    EX.SetRowcell( 19, 'Import Customs Clearance Fee');
    EX.SetRowcell( 20, 'Comments & specific requirements');
    EX.SetRowcell( 21, 'TE contact name');
    EX.SetRowcell( 22, 'TE contact E-Mail address');
    EX.SetRowcell( 23, 'CURRENCY');
    EX.SetRowcell( 24, 'Full unit rate in EURO');
    EX.SetRowcell( 25, 'Total cost per week (excl fuel)');
    EX.SetRowcell( 26, 'Weeks');
    EX.SetRowcell( 27, 'MBCount');
    EX.SetRowcell( 28, 'TotalCost');

		EX.SaveRow(sorszam);
		Inc(sorszam);

		kilepes				:= false;
		Query1.DisableControls;
    Application.CreateForm(TFormGaugeDlg, FormGaugeDlg);
    FormGaugeDlg.GaugeNyit('Milkrun adatgy�jt�s folyamatban... ' , '', false);
    recno:=0;
    MBCount:=0;
    TotalCost:=0;
    Weeks:= DatumKul(M2.Text, M1.Text) / 7.0;
		while ( ( not Query1.Eof ) and (not kilepes) ) do begin
			Application.ProcessMessages;
      FormGaugeDlg.Pozicio ( ( recno * 100 ) div Query1.RecordCount ) ;
      Inc(recno);
      Inc(MBCount);
      lanossz:= StringSzam(Query1.FieldByName('MB_FUDIJ').AsString);
			//Fuvard�j �s �zemanyagp�tl�k kisz�m�t�sa
			Query_Run(Query3, 'SELECT VI_UJKOD FROM VISZONY WHERE VI_VIKOD = '+ IntToStr(StrToIntDef(Query1.FieldByName('MB_VIKOD').AsString, 0)) +
				' AND VI_JAKOD = '''+Query1.FieldByName('MB_JAKOD').AsString+''' ');
			if Query3.RecordCount <> 1  then begin
				// Nins sz�mla
				EllenListAppend('A megb�z�shoz nem tartozik sz�mla! Megb�z�s k�d : '+Query1.FieldByName('MB_MBKOD').AsString);
			  end
      else begin
				ujkod	:= Query3.FieldByName('VI_UJKOD').AsString;
				// A sz�mla k�pz�se
				if StrToIntDef(ujkod, 0) = 0 then begin
					EllenListAppend('A megb�z�shoz nem tartozik sz�mla! Megb�z�s k�d : '+Query1.FieldByName('MB_MBKOD').AsString);
				  end
        else begin
					JSzamla	:= TJSzamla.Create(Self);
					JSzamla.FillSzamla(ujkod);
					if kelluzpotlek and kelluzpotl and (JSzamla.uapotlekeur = 0)and(pos(JSzamla.szamlaszam ,Kombi_ExportDlg.kellaszamla)=0) then begin
						EllenListAppend('Az �zemanyag p�tl�k �sszege 0! Sz�mlasz�m : '+JSzamla.szamlaszam);
					  end
          else begin
						EX.SetRowcell(31, Format('%.2f', [JSZAMLA.uapotlekeur]));
						if ( Abs ( JSzamla.ossz_nettoeur - JSzamla.spedicioeur - JSzamla.uapotlekeur - lanossz ) > 0.00001 ) and (pos(Query1.FieldByName('MB_ALVAL').AsString,belf_alval)=0) then begin
              S:='A sz�mla nett� eur - sped�ci�s d�j <> megb�z�si fuvard�j! Sz�mla : '+JSzamla.szamlaszam+'  Megb�z�s k�d : '+Query1.FieldByName('MB_MBKOD').AsString;
              S:=S+' Nett� EUR='+ FormatFloat('0.00', JSzamla.ossz_nettoeur);
              S:=S+' Sped EUR='+ FormatFloat('0.00', JSzamla.spedicioeur);
              S:=S+' �za p�tl. EUR='+ FormatFloat('0.00', JSzamla.uapotlekeur);
              S:=S+' Szla='+ FormatFloat('0.00', lanossz);
							EllenListAppend(S);
						  end
            else begin
              lanossz:= JSzamla.ossz_nettoeur - JSzamla.spedicioeur - JSzamla.uapotlekeur;
              TotalCost:= TotalCost + lanossz;
						  end;
					  end;
          JSzamla.Destroy;
          end; // if StrToIntDef(ujkod
			  end; //Query3.RecordCoun
      // v�ltoz�ba mentj�k, mert a recordset l�pni fog
      ActLANID:= Query1.FieldByName('FT_LANID').AsInteger;
      ORI_COUNTRY:= Query1.FieldByName('ORI_COUNTRY').AsString;
      ORI_ZIP:= Query1.FieldByName('ORI_ZIP').AsString;
      DEST_COUNTRY:= Query1.FieldByName('DEST_COUNTRY').AsString;
      DEST_ZIP:= Query1.FieldByName('DEST_ZIP').AsString;
      FT_GKKAT:= Query1.FieldByName('FT_GKKAT').AsString;
      FT_TRDAY:= Query1.FieldByName('FT_TRDAY').AsInteger;
			Query1.Next;
      // ki�r�s, ha blokk v�ge van
      if (Query1.FieldByName('FT_LANID').AsInteger <> ActLANID) or Query1.EOF then begin
          EX.SetRowcell( 1, MBCount / Weeks);
          EX.SetRowcell( 2, 'SP'+IntToStr(ActLANID-1000));
          EX.SetRowcell( 3, 'Full unit booking');
          EX.SetRowcell( 4, 'No');
          EX.SetRowcell( 5, ORI_COUNTRY);
          EX.SetRowcell( 6, ORI_ZIP);
          EX.SetRowcell( 7, '');
          EX.SetRowcell( 8, '');
          EX.SetRowcell( 9, '');
          EX.SetRowcell(10, DEST_COUNTRY);
          EX.SetRowcell(11, DEST_ZIP);
          EX.SetRowcell(12, '');
          EX.SetRowcell(13, '');
          EX.SetRowcell(14, '');
          EX.SetRowcell(15, FT_GKKAT);
          EX.SetRowcell(16, FT_TRDAY);
          EX.SetRowcell(17, 'N.A.');
          EX.SetRowcell(18, 'N.A.');
          EX.SetRowcell(19, 'N.A.');
          EX.SetRowcell(20, '');
          EX.SetRowcell(21, '');
          EX.SetRowcell(22, '');
          EX.SetRowcell(23, 'EUR');
          EX.SetRowcell(24, '');
          EX.SetRowcell(25, TotalCost / Weeks);
          // debug
          EX.SetRowcell(26, Weeks);
          EX.SetRowcell(27, MBCount);
          EX.SetRowcell(28, TotalCost);

          // Minden rendben volt, lehet menteni a sort
					EX.SaveRow(sorszam);
					Inc(sorszam);

          MBCount:=0;
          TotalCost:=0;
          ActLANID:= Query1.FieldByName('FT_LANID').AsInteger;
          end // if
		end; // while
    FormGaugeDlg.Destroy;
	end;
	EX.SaveFileAsXls(fn);
	EX.Free;
	voltkilepes	:= kilepes;
	if ( ( EgyebDlg.ellenlista.Count > 0 ) and kelluzenet ) then begin
		EllenListMutat('TYCO milkrun historikus adatok - Ellen�rz� lista', false);
	end;
	BitBtn4.Show;
	if kellshell then begin
		ShellExecute(Application.Handle,'open',PChar(fn), '', '', SW_SHOWNORMAL );
	end;
	kilepes			:= true;
end;

function TTYCO_HistoricalDlg.XLS_Generalas_FTL : boolean;
const
  Oszlopszam = 28;
var
	fn, S, ujkod, belf_alval, rendsz, gkkat, PrevLANID: String;
	sorszam, i, datkul, orakul, recno, MBCount, FT_TRDAY, ActLANID: integer;
  TotalCost, Weeks: double;
  ORI_CITY, ORI_COUNTRY, ORI_ZIP, DEST_CITY, DEST_COUNTRY, DEST_ZIP, FT_GKKAT, INOUT: string;
	lanossz, suly, paletta, suly2: double;
	EX: TJExcel;
  kelluzpotl, alvall: boolean;
begin

  belf_alval	:= EgyebDlg.Read_SZGrid( '999', '748' );

	EllenListTorol;
	result		:= true;
	fn		:= 'TYCO_HIST2_'+M1.Text+'_'+M2.Text;
	while Pos('.', fn) > 0 do begin
		fn		:= copy(fn, 1, Pos('.', fn) - 1 ) + copy(fn, Pos('.', fn) + 1, 999 );
	end;
	fn			:= EgyebDlg.DocumentPath + fn + '.XLS';
	if FileExists(fn) then begin
		DeleteFile(PChar(fn));
	end;

  S:='select '+
       ' MS1.MS_FELTEL ORI_CITY, '+
       ' SUBSTRING(MS1.MS_FELIR,1,2) ORI_ZIP, '+
       ' MS1.MS_FORSZ ORI_COUNTRY, '+
       ' MS2.MS_LERTEL DEST_CITY, '+
       ' SUBSTRING(MS2.MS_LELIR,1,2) DEST_ZIP, '+
       ' MS2.MS_ORSZA DEST_COUNTRY, '+
       ' case when MS2.MS_ORSZA = ''HU'' then ''I'' else ''O'' end INOUT, '+
       ' FT_TRDAY, '+
       ' MB_VIKOD, MB_JAKOD, MB_ALVAL, MB_MBKOD, MB_FUDIJ '+
    ' FROM MEGSEGED MS1, '+
	  ' MEGSEGED MS2, '+
	  ' MEGBIZAS '+
	  '   LEFT OUTER JOIN FUVARTABLA ON MB_FUTID = FT_FTKOD '+
	  '  WHERE '+
	  '  FT_TOL=''2015.05.01.'' '+
	  '  and FT_LANID < 1000  '+
	  '  AND MS1.MS_MSKOD=(select min(MS_MSKOD) FROM MEGSEGED WHERE MS_MBKOD = MB_MBKOD AND MS_TIPUS = 0) '+
	  '  AND MS1.MS_TIPUS = 0 '+
	  '  AND MS2.MS_MSKOD=(select max(MS_MSKOD) FROM MEGSEGED WHERE MS_MBKOD = MB_MBKOD AND MS_TIPUS = 1) '+
	  '  AND MS2.MS_TIPUS = 1 '+
    ' AND MB_MBKOD IN ( SELECT MS_MBKOD FROM MEGSEGED WHERE MS_FETDAT >= '''+M1.Text+''' AND MS_FETDAT <= '''+M2.Text+''') '+
    ' AND MB_VEKOD IN ('+M50.Text+') '+
    ' ORDER BY 1,2,3,4,5,6';

  Query_Run(Query1, S);
	if Query1.RecordCount < 1 then begin
		if kelluzenet then begin
			NoticeKi('Nincs a felt�teleknek megfelel� elem!');
		end;
		result		:= false;
		Exit;
	end;
	BitBtn4.Hide;
	// Az XLS megnyit�sa
	EX 	:= TJexcel.Create(Self);
	if not EX.OpenXlsFile(fn) then begin
		if kelluzenet then begin
			NoticeKi('Nem siker�lt megnyitnom az Excel �llom�nyt!');
		end;
	end else begin
		EX.ColNumber	:= Oszlopszam;
		EX.InitRow;
		EX.SetActiveSheet(0);
		// EX.SetActiveSheet(1);
		sorszam			:= 1;

		EX.SetRowcell(1, 'TYCO Historical data extraction - FTL');
		EX.SaveRow(sorszam);
		Inc(sorszam);
		Ex.EmptyRow;
		EX.SetRowcell( 1, 'Origin city name');
		EX.SetRowcell( 2, 'Origin 2-digit zip code');
		EX.SetRowcell( 3, 'Origin country code');
		EX.SetRowcell( 4, 'Destination  city name');
		EX.SetRowcell( 5, 'Destination  2-digit zip code');
		EX.SetRowcell( 6, 'Destination  country code');
    EX.SetRowcell( 7, 'Trade / Intercompany / Supplier');
		EX.SetRowcell( 8, 'Inbound / Outbound');
		EX.SetRowcell( 9, 'Transit time in working days');
		EX.SetRowcell( 10, 'Average nbr of FTL per week');
		EX.SetRowcell( 11, 'Special Requirements');
		EX.SetRowcell( 12, 'TE contact 1');
		EX.SetRowcell( 13, 'TE contact 2');
		EX.SetRowcell( 14, 'TE contact 3');
		EX.SetRowcell( 15, 'TE contact 4');
 		EX.SetRowcell( 16, 'Total cost per FTL in � (excl fuel)');
    EX.SetRowcell( 17, 'Customs clearance costs in � (if applicable)');
    EX.SetRowcell( 18, 'Total cost per week in � (excl fuel)');

    EX.SetRowcell( 19, 'Weeks');
    EX.SetRowcell( 20, 'MBCount');

		EX.SaveRow(sorszam);
		Inc(sorszam);

		kilepes				:= false;
		Query1.DisableControls;
    Application.CreateForm(TFormGaugeDlg, FormGaugeDlg);
    FormGaugeDlg.GaugeNyit('FTL adatgy�jt�s folyamatban... ' , '', false);
    recno:=0;
    MBCount:=0;
    TotalCost:=0;
    Weeks:= DatumKul(M2.Text, M1.Text) / 7.0;
		while ( ( not Query1.Eof ) and (not kilepes) ) do begin
			Application.ProcessMessages;
      FormGaugeDlg.Pozicio ( ( recno * 100 ) div Query1.RecordCount ) ;
      Inc(recno);
      Inc(MBCount);
      lanossz:= StringSzam(Query1.FieldByName('MB_FUDIJ').AsString);
			//Fuvard�j �s �zemanyagp�tl�k kisz�m�t�sa
			Query_Run(Query3, 'SELECT VI_UJKOD FROM VISZONY WHERE VI_VIKOD = '+ IntToStr(StrToIntDef(Query1.FieldByName('MB_VIKOD').AsString, 0)) +
				' AND VI_JAKOD = '''+Query1.FieldByName('MB_JAKOD').AsString+''' ');
			if Query3.RecordCount <> 1  then begin
				// Nins sz�mla
				EllenListAppend('A megb�z�shoz nem tartozik sz�mla! Megb�z�s k�d : '+Query1.FieldByName('MB_MBKOD').AsString);
			  end
      else begin
				ujkod	:= Query3.FieldByName('VI_UJKOD').AsString;
				// A sz�mla k�pz�se
				if StrToIntDef(ujkod, 0) = 0 then begin
					EllenListAppend('A megb�z�shoz nem tartozik sz�mla! Megb�z�s k�d : '+Query1.FieldByName('MB_MBKOD').AsString);
				  end
        else begin
					JSzamla	:= TJSzamla.Create(Self);
					JSzamla.FillSzamla(ujkod);
					if kelluzpotlek and kelluzpotl and (JSzamla.uapotlekeur = 0)and(pos(JSzamla.szamlaszam ,Kombi_ExportDlg.kellaszamla)=0) then begin
						EllenListAppend('Az �zemanyag p�tl�k �sszege 0! Sz�mlasz�m : '+JSzamla.szamlaszam);
					  end
          else begin
						EX.SetRowcell(31, Format('%.2f', [JSZAMLA.uapotlekeur]));
						if ( Abs ( JSzamla.ossz_nettoeur - JSzamla.spedicioeur - JSzamla.uapotlekeur - lanossz ) > 0.00001 ) and (pos(Query1.FieldByName('MB_ALVAL').AsString,belf_alval)=0) then begin
              S:='A sz�mla nett� eur - sped�ci�s d�j <> megb�z�si fuvard�j! Sz�mla : '+JSzamla.szamlaszam+'  Megb�z�s k�d : '+Query1.FieldByName('MB_MBKOD').AsString;
              S:=S+' Nett� EUR='+ FormatFloat('0.00', JSzamla.ossz_nettoeur);
              S:=S+' Sped EUR='+ FormatFloat('0.00', JSzamla.spedicioeur);
              S:=S+' �za p�tl. EUR='+ FormatFloat('0.00', JSzamla.uapotlekeur);
              S:=S+' Szla='+ FormatFloat('0.00', lanossz);
							EllenListAppend(S);
						  end
            else begin
              lanossz:= JSzamla.ossz_nettoeur - JSzamla.spedicioeur - JSzamla.uapotlekeur;
              TotalCost:= TotalCost + lanossz;
						  end;
					  end;
          JSzamla.Destroy;
          end; // if StrToIntDef(ujkod
			  end; //Query3.RecordCoun
      // v�ltoz�ba mentj�k, mert a recordset l�pni fog
      ORI_CITY:= trim(Query1.FieldByName('ORI_CITY').AsString);
      ORI_ZIP:= trim(Query1.FieldByName('ORI_ZIP').AsString);
      ORI_COUNTRY:= trim(Query1.FieldByName('ORI_COUNTRY').AsString);
      DEST_CITY:= trim(Query1.FieldByName('DEST_CITY').AsString);
      DEST_ZIP:= trim(Query1.FieldByName('DEST_ZIP').AsString);
      DEST_COUNTRY:= trim(Query1.FieldByName('DEST_COUNTRY').AsString);
      INOUT:= Query1.FieldByName('INOUT').AsString;
      FT_TRDAY:= Query1.FieldByName('FT_TRDAY').AsInteger;
			Query1.Next;
      // ki�r�s, ha blokk v�ge van - azaz b�rmelyik elt�r az el�z�t�l
      if (trim(Query1.FieldByName('ORI_CITY').AsString) <> ORI_CITY)
       or (trim(Query1.FieldByName('ORI_ZIP').AsString) <> ORI_ZIP)
       or (trim(Query1.FieldByName('ORI_COUNTRY').AsString) <> ORI_COUNTRY)
       or (trim(Query1.FieldByName('DEST_CITY').AsString) <> DEST_CITY)
       or (trim(Query1.FieldByName('DEST_ZIP').AsString) <> DEST_ZIP)
       or (trim(Query1.FieldByName('DEST_COUNTRY').AsString) <> DEST_COUNTRY)
       or Query1.EOF then begin
          EX.SetRowcell( 1, ORI_CITY);
          EX.SetRowcell( 2, ORI_ZIP);
          EX.SetRowcell( 3, ORI_COUNTRY);
          EX.SetRowcell( 4, DEST_CITY);
          EX.SetRowcell( 5, DEST_ZIP);
          EX.SetRowcell( 6, DEST_COUNTRY);
          EX.SetRowcell( 7, 'I');
          EX.SetRowcell( 8, INOUT);
          EX.SetRowcell( 9, FT_TRDAY);
          EX.SetRowcell(10, MBCount / Weeks);
          EX.SetRowcell(11, '');
          EX.SetRowcell(12, '');
          EX.SetRowcell(13, '');
          EX.SetRowcell(14, '');
          EX.SetRowcell(15, '');
          EX.SetRowcell(16, TotalCost);
          EX.SetRowcell(17, 'N.A.');
          EX.SetRowcell(18, TotalCost / Weeks);
          // debug
          EX.SetRowcell(19, Weeks);
          EX.SetRowcell(20, MBCount);

          // Minden rendben volt, lehet menteni a sort
					EX.SaveRow(sorszam);
					Inc(sorszam);

          MBCount:=0;
          TotalCost:=0;
          ORI_CITY:= trim(Query1.FieldByName('ORI_CITY').AsString);
          ORI_ZIP:= trim(Query1.FieldByName('ORI_ZIP').AsString);
          ORI_COUNTRY:= trim(Query1.FieldByName('ORI_COUNTRY').AsString);
          DEST_CITY:= trim(Query1.FieldByName('DEST_CITY').AsString);
          DEST_ZIP:= trim(Query1.FieldByName('DEST_ZIP').AsString);
          DEST_COUNTRY:= trim(Query1.FieldByName('DEST_COUNTRY').AsString);
          end // if
		end; // while
    FormGaugeDlg.Destroy;
	end;
	EX.SaveFileAsXls(fn);
	EX.Free;
	voltkilepes	:= kilepes;
	if ( ( EgyebDlg.ellenlista.Count > 0 ) and kelluzenet ) then begin
		EllenListMutat('TYCO FTL historikus adatok - Ellen�rz� lista', false);
	end;
	BitBtn4.Show;
	if kellshell then begin
		ShellExecute(Application.Handle,'open',PChar(fn), '', '', SW_SHOWNORMAL );
	end;
	kilepes			:= true;
end;



procedure TTYCO_HistoricalDlg.BitBtn5Click(Sender: TObject);
begin
	// T�bb vev� kiv�laszt�sa
	Application.CreateForm(TValvevoDlg, ValvevoDlg);
	ValvevoDlg.Tolt(M50.Text);
	ValvevoDlg.ShowModal;
	if ValvevoDlg.kodkilist.Count > 0 then begin
		M5.Text		:= ValvevoDlg.nevlista;
		M50.Text	:= ValvevoDlg.kodlista;
	end;
	ValvevoDlg.Destroy;
end;

{function TTYCO_HistoricalDlg.XLS_Generalas_historical : boolean;
const
  Oszlopszam = 17;
var
	fn, S, ujkod, belf_alval, rendsz, gkkat: String;
	sorszam, i, datkul, orakul, recno: integer;
	lanossz, suly, paletta, suly2, oW,oAB,oAC,oP: double;
	EX: TJExcel;
  kelluzpotl, alvall: boolean;
begin

  belf_alval	:= EgyebDlg.Read_SZGrid( '999', '748' );
//  belf_alval:='0607';

  oW:=0;
  oAB:=0;
  oAC:=0;
  oP:=0;
	EllenListTorol;
	result		:= true;
	fn		:= 'TYCO_HIST1_'+M1.Text+'_'+M2.Text;
	while Pos('.', fn) > 0 do begin
		fn		:= copy(fn, 1, Pos('.', fn) - 1 ) + copy(fn, Pos('.', fn) + 1, 999 );
	end;
	fn			:= EgyebDlg.Temppath + fn + '.XLS';;
	if FileExists(fn) then begin
		DeleteFile(PChar(fn));
	end;

  S:='select MS1.MS_FELNEV, '+ //  Consignor,
        'MS1.MS_FELTEL, '+ //   Origin_city,
        'MS1.MS_FELIR, '+ // 	Origin_zipcode,
		    'SUBSTRING(MS1.MS_FELIR,1,2), '+ //  Origin_2digit_zipcode,
		    'MS1.MS_FORSZ, '+ //  Origin_country,
		    'MS2.MS_LERNEV, '+ //  Consignee,
		    'MS2.MS_LERTEL, '+ //  Destination_city,
		    'MS2.MS_LELIR, '+ //  Destination_zipcode,
		    'SUBSTRING(MS2.MS_LELIR,1,2), '+ //  Destination_2digit_zipcode,
		    'MS2.MS_ORSZA, '+ //  Destination_country,
		    '(SELECT MIN(MS_FETDAT) FELDAT FROM MEGSEGED WHERE MS_MBKOD = MB_MBKOD), '+ //  Pickup_date,
        // ezt majd sz�moljuk
        ' -1, '+
		    // '(select datepart(wk, CONVERT(date, SUBSTRING(FELDAT,1,4)+SUBSTRING(FELDAT,6,2)+SUBSTRING(FELDAT,9,2), 112))), '+ //  Calendarweek,
		    '''KG'', '+ //  Weight_unit_of_measure,
		    '(SELECT SUM(MS_FSULY) SULY FROM MEGSEGED WHERE MS_MBKOD = MB_MBKOD AND MS_TIPUS = 0 AND MS_FAJKO = 0), '+ //   Actual_weight,
		    '(SELECT SUM(case when MS_FEKOB>0 then MS_FEKOB else MS_FSULY end) FROM MEGSEGED WHERE MS_MBKOD = MB_MBKOD AND MS_TIPUS = 0 AND MS_FAJKO = 0), '+ //  Chargeable_weight,
		    '-1, '+ //  Volume_m3,
		    '-1, '+ //  Total_Cost_excl_Fuel_excl_Customs_clearance
        'MB_VIKOD, MB_JAKOD, MB_ALVAL, MB_MBKOD, MB_FUDIJ, MS1.MS_MSKOD MSKOD1, MS2.MS_MSKOD MSKOD2 '+  // a k�lts�gsz�m�t�shoz
		    'FROM MEGBIZAS, MEGSEGED MS1, MEGSEGED MS2 '+
		    'WHERE MS1.MS_MSKOD=(select min(MS_MSKOD) FROM MEGSEGED WHERE MS_MBKOD = MB_MBKOD AND MS_TIPUS = 0) '+
        ' AND MS1.MS_TIPUS = 0 '+
        ' AND MS2.MS_MSKOD=(select max(MS_MSKOD) FROM MEGSEGED WHERE MS_MBKOD = MB_MBKOD AND MS_TIPUS = 1) '+
        ' AND MS2.MS_TIPUS = 1 '+
        ' AND MB_MBKOD IN ( SELECT MS_MBKOD FROM MEGSEGED WHERE MS_FETDAT >= '''+M1.Text+''' AND MS_FETDAT <= '''+M2.Text+''') '+
		    ' AND MB_VEKOD IN ('+M50.Text+') '+
		    'ORDER BY MB_DATUM, MB_MBKOD';
  Query_Run(Query1, S);
	if Query1.RecordCount < 1 then begin
		if kelluzenet then begin
			NoticeKi('Nincs a felt�teleknek megfelel� elem!');
		end;
		result		:= false;
		Exit;
	end;
	BitBtn4.Hide;
	// Az XLS megnyit�sa
	EX 	:= TJexcel.Create(Self);
	if not EX.OpenXlsFile(fn) then begin
		if kelluzenet then begin
			NoticeKi('Nem siker�lt megnyitnom az Excel �llom�nyt!');
		end;
	end else begin
		EX.ColNumber	:= Oszlopszam+3; // debug oszlopokkal
		EX.InitRow;
		EX.SetActiveSheet(0);
		// EX.SetActiveSheet(1);
		sorszam			:= 1;

		EX.SetRowcell(1, 'TYCO Historical data extraction');
		EX.SaveRow(sorszam);
		Inc(sorszam);
		Ex.EmptyRow;
		EX.SetRowcell( 1, 'Consignor');
		EX.SetRowcell( 2, 'Origin city');
		EX.SetRowcell( 3, 'Origin zipcode');
    EX.SetRowcell( 4, 'Ori.2-digit zip');
		EX.SetRowcell( 5, 'Origin country ISO-code');
		EX.SetRowcell( 6, 'Consignee');
		EX.SetRowcell( 7, 'Destination city');
		EX.SetRowcell( 8, 'Destination zipcode');
    EX.SetRowcell( 9, 'Dest.2-digit zip');
		EX.SetRowcell(10, 'Destination country ISO-code');
		EX.SetRowcell(11, 'Pickup date (dd/mm/yyyy)');
		EX.SetRowcell(12, 'Calendar week');
		EX.SetRowcell(13, 'Weight unit of measure');
		EX.SetRowcell(14, 'Actual weight');
		EX.SetRowcell(15, 'Chargeable weight');
		EX.SetRowcell(16, 'Volume m3');
		EX.SetRowcell(17, 'Total costs excl. VAT excl. duty');

    EX.SetRowcell(18, 'MBKOD');
    EX.SetRowcell(19, 'MSKOD1');
    EX.SetRowcell(20, 'MSKOD2');
		EX.SaveRow(sorszam);
		Inc(sorszam);

		kilepes				:= false;
		Query1.DisableControls;
    Application.CreateForm(TFormGaugeDlg, FormGaugeDlg);
    FormGaugeDlg.GaugeNyit('Historical adatgy�jt�s folyamatban... ' , '', false);
    recno:=0;
		while ( ( not Query1.Eof ) and (not kilepes) ) do begin
			Application.ProcessMessages;
      FormGaugeDlg.Pozicio ( ( recno * 100 ) div Query1.RecordCount ) ;
      Inc(recno);
      for i:=1 to Oszlopszam do
        case i of
          11: EX.SetRowcell( i, ''''+DatumAngolosra(Query1.Fields[i-1].AsString));  // hogy ne form�zza az Excel
          12: EX.SetRowcell( i, IntToStr(CalendarWeek(Query1.Fields[i-2].AsString))); // az el�z� d�tum oszlopb�l dolgozzon
          else
    			    EX.SetRowcell( i, Query1.Fields[i-1].AsString);
            end;  // case
      paletta	:= GetPaletta(Query1.FieldByName('MB_MBKOD').AsString);
      EX.SetRowcell(16, paletta); // �gy ker�l bele sz�mk�nt

      EX.SetRowcell(18, Query1.FieldByName('MB_MBKOD').AsString);
      EX.SetRowcell(19, Query1.FieldByName('MSKOD1').AsString);
      EX.SetRowcell(20, Query1.FieldByName('MSKOD2').AsString);

      lanossz:= StringSzam(Query1.FieldByName('MB_FUDIJ').AsString);
			//Fuvard�j �s �zemanyagp�tl�k kisz�m�t�sa
			Query_Run(Query3, 'SELECT VI_UJKOD FROM VISZONY WHERE VI_VIKOD = '+ IntToStr(StrToIntDef(Query1.FieldByName('MB_VIKOD').AsString, 0)) +
				' AND VI_JAKOD = '''+Query1.FieldByName('MB_JAKOD').AsString+''' ');
			if Query3.RecordCount <> 1  then begin
				// Nins sz�mla
				EllenListAppend('A megb�z�shoz nem tartozik sz�mla! Megb�z�s k�d : '+Query1.FieldByName('MB_MBKOD').AsString);
			  end
      else begin
				ujkod	:= Query3.FieldByName('VI_UJKOD').AsString;
				// A sz�mla k�pz�se
				if StrToIntDef(ujkod, 0) = 0 then begin
					EllenListAppend('A megb�z�shoz nem tartozik sz�mla! Megb�z�s k�d : '+Query1.FieldByName('MB_MBKOD').AsString);
				  end
        else begin
					JSzamla	:= TJSzamla.Create(Self);
					JSzamla.FillSzamla(ujkod);
					if kelluzpotlek and kelluzpotl and (JSzamla.uapotlekeur = 0)and(pos(JSzamla.szamlaszam ,Kombi_ExportDlg.kellaszamla)=0) then begin
						EllenListAppend('Az �zemanyag p�tl�k �sszege 0! Sz�mlasz�m : '+JSzamla.szamlaszam);
					  end
          else begin
						EX.SetRowcell(31, Format('%.2f', [JSZAMLA.uapotlekeur]));
						if ( Abs ( JSzamla.ossz_nettoeur - JSzamla.spedicioeur - JSzamla.uapotlekeur - lanossz ) > 0.00001 ) and (pos(Query1.FieldByName('MB_ALVAL').AsString,belf_alval)=0) then begin
              S:='A sz�mla nett� eur - sped�ci�s d�j <> megb�z�si fuvard�j! Sz�mla : '+JSzamla.szamlaszam+'  Megb�z�s k�d : '+Query1.FieldByName('MB_MBKOD').AsString;
              S:=S+' Nett� EUR='+ FormatFloat('0.00', JSzamla.ossz_nettoeur);
              S:=S+' Sped EUR='+ FormatFloat('0.00', JSzamla.spedicioeur);
              S:=S+' �za p�tl. EUR='+ FormatFloat('0.00', JSzamla.uapotlekeur);
              S:=S+' Szla='+ FormatFloat('0.00', lanossz);
							EllenListAppend(S);
						  end
            else begin
              lanossz:= JSzamla.ossz_nettoeur - JSzamla.spedicioeur - JSzamla.uapotlekeur;
							//EX.SetRowcell(28, Format('%.2f', [lanossz]));
							// EX.SetRowcell(17, Format('%.2f', [lanossz+JSZAMLA.uapotlekeur]));
              EX.SetRowcell(17, lanossz); // �gy ker�l bele sz�mk�nt
							// Minden rendben volt, lehet menteni a sort
							EX.SaveRow(sorszam);
							Inc(sorszam);
							SorLogolas(gr, Query1.FieldByName('MB_MBKOD').AsString, suly, lanossz+JSZAMLA.uapotlekeur, JSZAMLA.uapotlekeur, 1);
						  end;
					  end;
          JSzamla.Destroy;
          end; // if StrToIntDef(ujkod
			  end; //Query3.RecordCoun
			Query1.Next;
		end; // while
    FormGaugeDlg.Destroy;
	end;
	EX.SaveFileAsXls(fn);
	EX.Free;
	voltkilepes	:= kilepes;
	if ( ( EgyebDlg.ellenlista.Count > 0 ) and kelluzenet ) then begin
		EllenListMutat('TYCO historikus adatok - Ellen�rz� lista', false);
	end;
	BitBtn4.Show;
	if kellshell then begin
		ShellExecute(Application.Handle,'open',PChar(fn), '', '', SW_SHOWNORMAL );
	end;
	kilepes			:= true;
end;
}
end.
