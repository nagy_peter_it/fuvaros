object RESTAPIGridDlg: TRESTAPIGridDlg
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  ClientHeight = 622
  ClientWidth = 1092
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PopupMenu = PopupMenu1
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object SG: TStringGrid
    Left = 0
    Top = 33
    Width = 1092
    Height = 523
    Align = alClient
    ColCount = 2
    FixedCols = 0
    RowCount = 2
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goColSizing, goRowSelect]
    ParentFont = False
    TabOrder = 0
    OnDblClick = SGDblClick
    ExplicitTop = 41
    ExplicitWidth = 985
    ExplicitHeight = 515
    ColWidths = (
      84
      64)
  end
  object pnlMessage: TPanel
    Left = 0
    Top = 556
    Width = 1092
    Height = 25
    Align = alBottom
    BevelOuter = bvLowered
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    ExplicitWidth = 985
  end
  object GridPanel2: TGridPanel
    Left = 0
    Top = 581
    Width = 1092
    Height = 41
    Align = alBottom
    ColumnCollection = <
      item
        Value = 20.307855059686360000
      end
      item
        Value = 20.307855059686360000
      end
      item
        Value = 19.767512181557350000
      end
      item
        Value = 19.616818659090900000
      end
      item
        Value = 19.999959039979030000
      end>
    ControlCollection = <
      item
        Column = 1
        Control = Button2
        Row = 0
      end
      item
        Column = 0
        Control = Button4
        Row = 0
      end
      item
        Column = 2
        Control = btnKapcsolatok
        Row = 0
      end
      item
        Column = 3
        Control = btnArchive
        Row = 0
      end
      item
        Column = 4
        Control = btnFoto
        Row = 0
      end>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    RowCollection = <
      item
        Value = 100.000000000000000000
      end>
    TabOrder = 2
    ExplicitWidth = 985
    DesignSize = (
      1092
      41)
    object Button2: TButton
      Left = 252
      Top = 4
      Width = 161
      Height = 33
      Anchors = []
      Caption = 'Kijel'#246'lt elemet m'#243'dos'#237'tom'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      OnClick = Button2Click
      ExplicitLeft = 219
    end
    object Button4: TButton
      Left = 48
      Top = 4
      Width = 127
      Height = 33
      Anchors = []
      Caption = #218'j elemet r'#246'gz'#237'tek'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      OnClick = Button4Click
      ExplicitLeft = 37
    end
    object btnKapcsolatok: TButton
      Left = 491
      Top = 4
      Width = 118
      Height = 33
      Anchors = []
      Caption = 'Kapcsolatok'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
      OnClick = btnKapcsolatokClick
      ExplicitLeft = 437
    end
    object btnArchive: TButton
      Left = 681
      Top = 4
      Width = 167
      Height = 33
      Anchors = []
      Caption = 'Kijel'#246'lt elemet archiv'#225'lom'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 3
      OnClick = btnArchiveClick
      ExplicitLeft = 605
    end
    object btnFoto: TButton
      Left = 928
      Top = 4
      Width = 105
      Height = 33
      Anchors = []
      Caption = 'Fot'#243' felt'#246'lt'#233's'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 4
      OnClick = btnFotoClick
      ExplicitLeft = 832
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 1092
    Height = 33
    Align = alTop
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 3
    DesignSize = (
      1092
      33)
    object Label1: TLabel
      Left = 8
      Top = 7
      Width = 45
      Height = 16
      Caption = 'Sz'#369'r'#233's:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object Label5: TLabel
      Left = 184
      Top = 7
      Width = 67
      Height = 16
      Caption = 'Arch'#237'vak is:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object ebInkrementalis: TEdit
      Left = 59
      Top = 4
      Width = 105
      Height = 24
      HelpContext = 14703
      TabStop = False
      Anchors = [akTop, akRight]
      Color = 16770764
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 15
      ParentFont = False
      TabOrder = 0
      OnChange = ebInkrementalisChange
      OnClick = ebInkrementalisClick
    end
    object Panel2: TPanel
      Left = 497
      Top = 1
      Width = 589
      Height = 33
      TabOrder = 1
      object ebPhone: TEdit
        Left = 4
        Top = 5
        Width = 89
        Height = 24
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        Text = '36209588785'
      end
      object ebPassword: TEdit
        Left = 99
        Top = 5
        Width = 65
        Height = 24
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 1
        Text = '12345678'
      end
      object ebXuid: TEdit
        Left = 170
        Top = 4
        Width = 46
        Height = 24
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 2
        Text = '10000'
      end
      object Button7: TButton
        Left = 220
        Top = 5
        Width = 67
        Height = 25
        Caption = 'Set user'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 3
        OnClick = Button7Click
      end
      object Button1: TButton
        Left = 297
        Top = 4
        Width = 89
        Height = 25
        Caption = 'Balogh Ferenc'
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 4
        OnClick = Button1Click
      end
      object Button3: TButton
        Left = 392
        Top = 4
        Width = 121
        Height = 25
        Caption = 'Csop Process'
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 5
        OnClick = Button3Click
      end
      object pnlProgress: TPanel
        Left = 518
        Top = 4
        Width = 61
        Height = 25
        BevelOuter = bvLowered
        TabOrder = 6
      end
    end
    object chkArchivIs: TCheckBox
      Left = 258
      Top = 8
      Width = 16
      Height = 17
      TabOrder = 2
      OnClick = chkArchivIsClick
    end
    object pnlInfo: TPanel
      Left = 310
      Top = 3
      Width = 147
      Height = 25
      BevelOuter = bvLowered
      TabOrder = 3
    end
  end
  object PopupMenu1: TPopupMenu
    Left = 656
    Top = 112
    object UzenetTop1: TMenuItem
      Caption = 'UzenetTop'
      ShortCut = 121
      OnClick = UzenetTop1Click
    end
  end
  object ppmKapcsolatok: TPopupMenu
    Left = 656
    Top = 216
  end
  object DOLGFOTO: TADODataSet
    Connection = EgyebDlg.ADOConnectionKozos
    CommandText = 'select * from DOLGOZOFOTO'
    Parameters = <>
    Left = 760
    Top = 64
    object DOLGFOTODO_KOD: TStringField
      FieldName = 'DO_KOD'
      Size = 5
    end
    object DOLGFOTODO_FOTO: TBlobField
      FieldName = 'DO_FOTO'
    end
  end
end
