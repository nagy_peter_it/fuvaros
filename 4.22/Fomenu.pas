unit Fomenu;

interface

uses
	SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
	Forms, Dialogs, StdCtrls, Menus, Mask, DBCtrls, Buttons, ExtCtrls,
	ComCtrls, Shellapi, QuickRpt, Grids, DB, ADODB, DateUtils,
  IdBaseComponent, IdComponent, IdTCPConnection, IdTCPClient, IdHTTP,
  AppEvnts,ComObj, KozosTipusok, Clipbrd, RESTAPI_grid, RESTAPI_Kozos;

type
	TFomenuDlg = class(TForm)
	MainMenu1: TMainMenu;
	Menu11: TMenuItem;
    Technika: TMenuItem;
    Szotar: TMenuItem;
	Panel1: TPanel;
    SP1: TSpeedButton;
    Timer1: TTimer;
    Kilepes: TMenuItem;
    SP4: TSpeedButton;
    SP2: TSpeedButton;
    Jelszavak: TMenuItem;
    Felhasznalok: TMenuItem;
    N2: TMenuItem;
	 StatusBar1: TStatusBar;
    Jogok: TMenuItem;
	 N1: TMenuItem;
	 Sugo: TMenuItem;
	 Segitseg: TMenuItem;
	 Nevjegy: TMenuItem;
	 Termekek: TMenuItem;
	 SP5: TSpeedButton;
	 SP3: TSpeedButton;
    Dolgozok: TMenuItem;
	 Vevok: TMenuItem;
    Gepkocsik: TMenuItem;
	 Arfolyamok: TMenuItem;
	 Shellek: TMenuItem;
    Koltsegek: TMenuItem;
	 Jaratok: TMenuItem;
	 Telefonok: TMenuItem;
    Sulyadatok: TMenuItem;
	 Terhelesek: TMenuItem;
	 Szamlak: TMenuItem;
    Szamlazas: TMenuItem;
    Osszszamlazas: TMenuItem;
    Cegadatok: TMenuItem;
	 Kimutatasok: TMenuItem;
    Forgalmiki: TMenuItem;
    Menetlevel: TMenuItem;
    Koltsegki: TMenuItem;
	 Fizetesiki: TMenuItem;
    Napidij: TMenuItem;
	 Utnyilvantartas: TMenuItem;
	 Uzemanyagki: TMenuItem;
	 Koltsegosszes: TMenuItem;
    Hianyzojaratok: TMenuItem;
	 Hianyzomenetlevelek: TMenuItem;
    SP6: TSpeedButton;
	 SP7: TSpeedButton;
	 SP8: TSpeedButton;
	 SP9: TSpeedButton;
	 SP10: TSpeedButton;
    Adatellenorzes: TMenuItem;
    Ervenyessegek: TMenuItem;
	 Label1: TLabel;
    Panel2: TPanel;
    N3: TMenuItem;
	 VevoExport: TMenuItem;
    Zarokm: TMenuItem;
    Uzemanyagar: TMenuItem;
	 APEHuzemanyag: TMenuItem;
    zemanyagfogyasztsiadatok1: TMenuItem;
	 elepitankolsokgenerlsa1: TMenuItem;
    Gepcsere: TMenuItem;
    Panel3: TPanel;
	 Tankimport: TMenuItem;
    QueryKoz: TADOQuery;
	 Timer2: TTimer;
    Uzenetek_kezelese: TMenuItem;
    Megbizasok: TMenuItem;
	 Hutokimutatas: TMenuItem;
	 Felrakasok: TMenuItem;
    SP11: TSpeedButton;
	 Szotarfrissites: TMenuItem;
    NyitoExport: TMenuItem;
    KimutatasFelesleges: TMenuItem;
	 KimutatasLizing: TMenuItem;
    Feketelistasok: TMenuItem;
	 ForgAlvallal: TMenuItem;
    ExportEllenor: TMenuItem;
	 SpeedButton1: TSpeedButton;
    AdatbazisImport2: TMenuItem;
    CMRkarbantarts1: TMenuItem;
    AlGepkocsi: TMenuItem;
    Tetanar: TMenuItem;
	 N4: TMenuItem;
    XMLGeneralas: TMenuItem;
    LogExport: TMenuItem;
	 N5: TMenuItem;
    GepjarmuStat: TMenuItem;
    PotkocsiStat: TMenuItem;
    Fuvartabla: TMenuItem;
    HianyDoksik: TMenuItem;
    Dolgerveny: TMenuItem;
    Szmlaadatokmentse1: TMenuItem;
    SpeedButton2: TSpeedButton;
    ADR030Exportls1: TMenuItem;
	 Hianyzokm: TMenuItem;
    AdBluetankolas: TMenuItem;
    KPIkimutats1: TMenuItem;
    MSriport1: TMenuItem;
    SpeciRiportok: TMenuItem;
    Gkvezetido: TMenuItem;
    KombiExport: TMenuItem;
    DkvKartyak: TMenuItem;
    ApplicationEvents1: TApplicationEvents;
    Button1: TButton;
    AdatbazisImport: TMenuItem;
    SpeedButton3: TSpeedButton;
    GKStatFrissites: TMenuItem;
    QueryUj2: TADOQuery;
    QueryUj3: TADOQuery;
    Label4: TLabel;
    PKStatFrissites: TMenuItem;
    N6: TMenuItem;
    ADODataSet1: TADODataSet;
    ADODataSet1DO_JOKAT: TStringField;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    SZTETEL: TADOQuery;
    SZTETELSS_KOD: TStringField;
    SZTETELSS_SOR: TIntegerField;
    SZTETELSS_TERKOD: TStringField;
    SZTETELSS_TERNEV: TStringField;
    SZTETELSS_CIKKSZ: TStringField;
    SZTETELSS_ITJSZJ: TStringField;
    SZTETELSS_MEGY: TStringField;
    SZTETELSS_AFA: TBCDField;
    SZTETELSS_EGYAR: TBCDField;
    SZTETELSS_KEDV: TBCDField;
    SZTETELSS_VALNEM: TStringField;
    SZTETELSS_VALERT: TBCDField;
    SZTETELSS_VALARF: TBCDField;
    SZTETELSS_AFAKOD: TStringField;
    SZTETELSS_LEIRAS: TStringField;
    SZTETELSS_KULNEV: TStringField;
    SZTETELSS_KULLEI: TStringField;
    SZTETELSS_DARAB: TBCDField;
    SZTETELSS_KULME: TStringField;
    SZTETELSS_ARDAT: TStringField;
    SZTETELSS_ARFOL: TBCDField;
    SZTETELSS_UJKOD: TBCDField;
    SZTETELSS_NEMEU: TIntegerField;
    Label9: TLabel;
    Rendszergazda1: TMenuItem;
    telekexportlsaaknyvelshez1: TMenuItem;
    Exportlsokellenrzse1: TMenuItem;
    GKSOBELbers2: TMenuItem;
    SzmlkjraszmolsaEUR2: TMenuItem;
    Label2: TLabel;
    Label3: TLabel;
    SpeedButton4: TSpeedButton;
    Dolgcsop1: TMenuItem;
    DOLG: TADODataSet;
    DOLGCSOP: TADODataSet;
    DOLGCSOPDC_DOKOD: TStringField;
    DOLGCSOPDC_DONEV: TStringField;
    DOLGCSOPDC_DATTOL: TStringField;
    DOLGCSOPDC_CSKOD: TStringField;
    DOLGCSOPDC_CSNEV: TStringField;
    DOLGDO_KOD: TStringField;
    DOLGDO_CSKOD: TStringField;
    DOLGDO_CSNEV: TStringField;
    DOLGDO_CSDAT: TStringField;
    ankimportTeletankjavts1: TMenuItem;
    KOLTS: TADODataSet;
    GEPKOCSI: TADOQuery;
    KOLTSKS_KTKOD: TIntegerField;
    KOLTSKS_JAKOD: TStringField;
    KOLTSKS_DATUM: TStringField;
    KOLTSKS_KMORA: TBCDField;
    KOLTSKS_VALNEM: TStringField;
    KOLTSKS_ARFOLY: TBCDField;
    KOLTSKS_MEGJ: TStringField;
    KOLTSKS_RENDSZ: TStringField;
    KOLTSKS_LEIRAS: TStringField;
    KOLTSKS_UZMENY: TBCDField;
    KOLTSKS_OSSZEG: TBCDField;
    KOLTSKS_TELE: TIntegerField;
    KOLTSKS_SZKOD: TStringField;
    KOLTSKS_AFASZ: TBCDField;
    KOLTSKS_AFAKOD: TStringField;
    KOLTSKS_AFAERT: TBCDField;
    KOLTSKS_ERTEK: TBCDField;
    KOLTSKS_VIKOD: TStringField;
    KOLTSKS_ATLAG: TBCDField;
    KOLTSKS_FIMOD: TStringField;
    KOLTSKS_TETAN: TIntegerField;
    KOLTSKS_TIPUS: TIntegerField;
    KOLTSKS_THELY: TStringField;
    KOLTSKS_TELES: TStringField;
    KOLTSKS_JARAT: TStringField;
    KOLTSKS_SOKOD: TStringField;
    KOLTSKS_SONEV: TStringField;
    KOLTSKS_ORSZA: TStringField;
    KOLTSKS_IDO: TStringField;
    GEPKOCSIgk_tankl: TBCDField;
    KOLTSTANK_SZAZ: TFloatField;
    Gepkocsikor: TMenuItem;
    Httankolsktsg1: TMenuItem;
    ADODataSet6: TADODataSet;
    Kamera1: TMenuItem;
    ADODataSet7: TADODataSet;
    ADODataSet7GK_KOD: TStringField;
    ADODataSet7GK_RESZ: TStringField;
    ADODataSet7GK_TIPUS: TStringField;
    ADODataSet7GK_MEGJ: TStringField;
    ADODataSet7GK_NORMA: TBCDField;
    ADODataSet7GK_DIJ1: TBCDField;
    ADODataSet7GK_DIJ2: TBCDField;
    ADODataSet7GK_SULYF: TIntegerField;
    ADODataSet7GK_KLIMA: TIntegerField;
    ADODataSet7GK_SULYL: TBCDField;
    ADODataSet7GK_APETE: TBCDField;
    ADODataSet7GK_APENY: TBCDField;
    ADODataSet7GK_NORTE: TBCDField;
    ADODataSet7GK_NORNY: TBCDField;
    ADODataSet7GK_ALVAZ: TStringField;
    ADODataSet7GK_MOTOR: TStringField;
    ADODataSet7GK_EVJAR: TStringField;
    ADODataSet7GK_FORHE: TStringField;
    ADODataSet7GK_FEHER: TStringField;
    ADODataSet7GK_RAKSU: TBCDField;
    ADODataSet7GK_TERAK: TBCDField;
    ADODataSet7GK_RAME1: TBCDField;
    ADODataSet7GK_RAME2: TBCDField;
    ADODataSet7GK_RAME3: TBCDField;
    ADODataSet7GK_EUPAL: TBCDField;
    ADODataSet7GK_KULSO: TIntegerField;
    ADODataSet7GK_SULYA: TBCDField;
    ADODataSet7GK_OSULY: TBCDField;
    ADODataSet7GK_POTOS: TIntegerField;
    ADODataSet7GK_POKOD: TStringField;
    ADODataSet7GK_NEMZE: TIntegerField;
    ADODataSet7GK_GARKM: TBCDField;
    ADODataSet7GK_GARFI: TIntegerField;
    ADODataSet7GK_GARNO: TStringField;
    ADODataSet7GK_ARHIV: TIntegerField;
    ADODataSet7GK_GKKAT: TStringField;
    ADODataSet7GK_FAJTA: TStringField;
    ADODataSet7GK_SONEV: TStringField;
    ADODataSet7GK_HOZZA: TIntegerField;
    ADODataSet7GK_LIVAN: TIntegerField;
    ADODataSet7GK_LICEG: TStringField;
    ADODataSet7GK_LIDAT: TStringField;
    ADODataSet7GK_LIHON: TIntegerField;
    ADODataSet7GK_LISZE: TStringField;
    ADODataSet7GK_LIOSZ: TBCDField;
    ADODataSet7GK_LINEM: TStringField;
    ADODataSet7GK_LIMEG: TStringField;
    ADODataSet7GK_TANKL: TBCDField;
    ADODataSet7GK_TARTL: TBCDField;
    ADODataSet7GK_RESZR: TIntegerField;
    ADODataSet7GK_KIVON: TIntegerField;
    ADODataSet7GK_VETAR: TBCDField;
    ADODataSet7GK_VETVN: TStringField;
    ADODataSet7GK_EUROB: TIntegerField;
    ADODataSet7GK_SFORH: TStringField;
    ADODataSet7GK_SOBEL: TStringField;
    ADODataSet7GK_UT_DA: TStringField;
    ADODataSet7GK_UT_KM: TBCDField;
    ADODataSet6KS_KTKOD: TIntegerField;
    ADODataSet6KS_JAKOD: TStringField;
    ADODataSet6KS_DATUM: TStringField;
    ADODataSet6KS_KMORA: TBCDField;
    ADODataSet6KS_VALNEM: TStringField;
    ADODataSet6KS_ARFOLY: TBCDField;
    ADODataSet6KS_MEGJ: TStringField;
    ADODataSet6KS_RENDSZ: TStringField;
    ADODataSet6KS_LEIRAS: TStringField;
    ADODataSet6KS_UZMENY: TBCDField;
    ADODataSet6KS_OSSZEG: TBCDField;
    ADODataSet6KS_TELE: TIntegerField;
    ADODataSet6KS_SZKOD: TStringField;
    ADODataSet6KS_AFASZ: TBCDField;
    ADODataSet6KS_AFAKOD: TStringField;
    ADODataSet6KS_AFAERT: TBCDField;
    ADODataSet6KS_ERTEK: TBCDField;
    ADODataSet6KS_VIKOD: TStringField;
    ADODataSet6KS_ATLAG: TBCDField;
    ADODataSet6KS_FIMOD: TStringField;
    ADODataSet6KS_TETAN: TIntegerField;
    ADODataSet6KS_TIPUS: TIntegerField;
    ADODataSet6KS_THELY: TStringField;
    ADODataSet6KS_TELES: TStringField;
    ADODataSet6KS_JARAT: TStringField;
    ADODataSet6KS_SOKOD: TStringField;
    ADODataSet6KS_SONEV: TStringField;
    ADODataSet6KS_ORSZA: TStringField;
    ADODataSet6KS_IDO: TStringField;
    rkpteszt1: TMenuItem;
    OpenDialog1: TOpenDialog;
    Kszletkezels1: TMenuItem;
    Kszletkezels2: TMenuItem;
    rfolyamtlt1: TMenuItem;
    Napok1: TMenuItem;
    Szabadnapokmunkanapok1: TMenuItem;
    Alvllalkozvev1: TMenuItem;
    TVevo: TADODataSet;
    TVevoV_KOD: TStringField;
    TVevoV_NEV: TStringField;
    TVevoV_IRSZ: TStringField;
    TVevoV_VAROS: TStringField;
    TVevoV_CIM: TStringField;
    TVevoV_TEL: TStringField;
    TVevoV_FAX: TStringField;
    TVevoV_BANK: TStringField;
    TVevoV_ADO: TStringField;
    TVevoV_KEDV: TBCDField;
    TVevoV_UGYINT: TStringField;
    TVevoV_MEGJ: TStringField;
    TVevoV_LEJAR: TIntegerField;
    TVevoV_AFAKO: TStringField;
    TVevoV_NEMET: TIntegerField;
    TVevoV_TENAP: TIntegerField;
    TVevoV_ARNAP: TIntegerField;
    TVevoV_ORSZ: TStringField;
    TVevoVE_VBANK: TStringField;
    TVevoVE_EUADO: TStringField;
    TVevoV_UPTIP: TIntegerField;
    TVevoV_UPERT: TBCDField;
    TVevoVE_ARHIV: TIntegerField;
    TVevoVE_FELIS: TIntegerField;
    TVevoVE_EMAIL: TStringField;
    TVevoVE_POCIM: TStringField;
    TVevoVE_POVAR: TStringField;
    TVevoVE_PORSZ: TStringField;
    TVevoVE_PIRSZ: TStringField;
    TVevoVE_PFLAG: TIntegerField;
    TVevoVE_PNEV1: TStringField;
    TVevoVE_PNEV2: TStringField;
    TVevoVE_PNEV3: TStringField;
    TVevoVE_VEFEJ: TStringField;
    TVevoVE_XMLSZ: TIntegerField;
    TVevoVE_PKELL: TIntegerField;
    TVevoVE_LANID: TIntegerField;
    TVevoVE_FTELJ: TIntegerField;
    TVevoVE_TIMO: TStringField;
    TAlvallal: TADOQuery;
    TAlvallalV_KOD: TStringField;
    TAlvallalV_NEV: TStringField;
    TAlvallalV_IRSZ: TStringField;
    TAlvallalV_VAROS: TStringField;
    TAlvallalV_CIM: TStringField;
    TAlvallalV_TEL: TStringField;
    TAlvallalV_FAX: TStringField;
    TAlvallalV_BANK: TStringField;
    TAlvallalV_ADO: TStringField;
    TAlvallalV_KEDV: TBCDField;
    TAlvallalV_UGYINT: TStringField;
    TAlvallalV_MEGJ: TStringField;
    TAlvallalV_LEJAR: TIntegerField;
    TAlvallalV_AFAKO: TStringField;
    TAlvallalV_ORSZ: TStringField;
    TAlvallalV_EMAIL: TStringField;
    LabEV: TLabel;
    RendszSULYOK1: TMenuItem;
    SULYOK: TADODataSet;
    SULYOKSU_SUKOD: TStringField;
    SULYOKSU_RENDSZ: TStringField;
    SULYOKSU_KMORA: TBCDField;
    SULYOKSU_ERTEK: TBCDField;
    SULYOKSU_MEGJE: TStringField;
    SULYOKSU_KMOR2: TBCDField;
    SULYOKSU_JAKOD: TStringField;
    JARAT: TADOQuery;
    JARATja_rendsz: TStringField;
    N7: TMenuItem;
    GkKatKltsg1: TMenuItem;
    KOLTSEG: TADODataSet;
    KOLTSEGKS_KTKOD: TIntegerField;
    KOLTSEGKS_JAKOD: TStringField;
    KOLTSEGKS_DATUM: TStringField;
    KOLTSEGKS_KMORA: TBCDField;
    KOLTSEGKS_VALNEM: TStringField;
    KOLTSEGKS_ARFOLY: TBCDField;
    KOLTSEGKS_MEGJ: TStringField;
    KOLTSEGKS_RENDSZ: TStringField;
    KOLTSEGKS_LEIRAS: TStringField;
    KOLTSEGKS_UZMENY: TBCDField;
    KOLTSEGKS_OSSZEG: TBCDField;
    KOLTSEGKS_TELE: TIntegerField;
    KOLTSEGKS_SZKOD: TStringField;
    KOLTSEGKS_AFASZ: TBCDField;
    KOLTSEGKS_AFAKOD: TStringField;
    KOLTSEGKS_AFAERT: TBCDField;
    KOLTSEGKS_ERTEK: TBCDField;
    KOLTSEGKS_VIKOD: TStringField;
    KOLTSEGKS_ATLAG: TBCDField;
    KOLTSEGKS_FIMOD: TStringField;
    KOLTSEGKS_TETAN: TIntegerField;
    KOLTSEGKS_TIPUS: TIntegerField;
    KOLTSEGKS_THELY: TStringField;
    KOLTSEGKS_TELES: TStringField;
    KOLTSEGKS_JARAT: TStringField;
    KOLTSEGKS_SOKOD: TStringField;
    KOLTSEGKS_SONEV: TStringField;
    KOLTSEGKS_ORSZA: TStringField;
    KOLTSEGKS_IDO: TStringField;
    KOLTSEGKS_KTIP: TIntegerField;
    KOLTSEGKS_GKKAT: TStringField;
    Sajtkonfigbelltstrlse1: TMenuItem;
    Kltsgrtkszmols1: TMenuItem;
    KOLTSERTEK: TADODataSet;
    KOLTSERTEKKS_VALNEM: TStringField;
    KOLTSERTEKKS_ARFOLY: TBCDField;
    KOLTSERTEKKS_OSSZEG: TBCDField;
    KOLTSERTEKKS_ERTEK: TBCDField;
    KOLTSERTEKKS_DATUM: TStringField;
    Kltsgektrlsejrafeldolgozshoz1: TMenuItem;
    LOADID1: TMenuItem;
    Paletta1: TMenuItem;
    SpeedButton6: TSpeedButton;
    JARATMEGSEGED1: TMenuItem;
    MEGSEGED: TADODataSet;
    MEGSEGEDMS_FETDAT: TStringField;
    MEGSEGEDMS_FETIDO: TStringField;
    MEGSEGEDMS_LETDAT: TStringField;
    MEGSEGEDMS_LETIDO: TStringField;
    MEGSEGEDMS_RENSZ: TStringField;
    MEGSEGEDMS_JAKOD: TStringField;
    MEGSEGEDMS_JASZA: TStringField;
    JARAT2: TADOQuery;
    JARAT2JA_KOD: TStringField;
    JARAT2JA_JARAT: TStringField;
    JARAT2JA_JKEZD: TStringField;
    JARAT2JA_JVEGE: TStringField;
    JARAT2JA_RENDSZ: TStringField;
    JARAT2JA_SAJAT: TIntegerField;
    JARAT2JA_MEGJ: TStringField;
    JARAT2JA_MENSZ: TStringField;
    JARAT2JA_NYITKM: TBCDField;
    JARAT2JA_ZAROKM: TBCDField;
    JARAT2JA_BELKM: TBCDField;
    JARAT2JA_ALKOD: TBCDField;
    JARAT2JA_FAZIS: TBCDField;
    JARAT2JA_OSSZKM: TBCDField;
    JARAT2JA_ALVAL: TIntegerField;
    JARAT2JA_ORSZ: TStringField;
    JARAT2JA_SOFK1: TStringField;
    JARAT2JA_SOFK2: TStringField;
    JARAT2JA_SNEV1: TStringField;
    JARAT2JA_SNEV2: TStringField;
    JARAT2JA_JPOTK: TStringField;
    JARAT2JA_FAZST: TStringField;
    JARAT2JA_KEIDO: TStringField;
    JARAT2JA_VEIDO: TStringField;
    JARAT2JA_LFDAT: TStringField;
    JARAT2JA_SZAK1: TBCDField;
    JARAT2JA_SZAK2: TBCDField;
    JARAT2JA_URESK: TStringField;
    JARAT2JA_EUROS: TStringField;
    JARAT2JA_UBKM: TStringField;
    JARAT2JA_UKKM: TStringField;
    JARAT2JA_JAORA: TIntegerField;
    Label10: TLabel;
    MEGSEGEDMS_MBKOD: TIntegerField;
    MEGSEGEDMS_FAJKO: TIntegerField;
    Jratokvisszazrsa1: TMenuItem;
    ADOCommand1: TADOCommand;
    Query2: TADOQuery;
    Query33: TADOQuery;
    LoadIDmegtekintse1: TMenuItem;
    elepiteletankokjavtsa1: TMenuItem;
    TTANK: TADOQuery;
    TTANKks_rendsz: TStringField;
    TTANKks_kmora: TBCDField;
    TTANKCOLUMN1: TIntegerField;
    TTANK2: TADODataSet;
    TTANK2KS_KTKOD: TIntegerField;
    TTANK2KS_JAKOD: TStringField;
    TTANK2KS_DATUM: TStringField;
    TTANK2KS_KMORA: TBCDField;
    TTANK2KS_VALNEM: TStringField;
    TTANK2KS_ARFOLY: TBCDField;
    TTANK2KS_MEGJ: TStringField;
    TTANK2KS_RENDSZ: TStringField;
    TTANK2KS_LEIRAS: TStringField;
    TTANK2KS_UZMENY: TBCDField;
    TTANK2KS_OSSZEG: TBCDField;
    TTANK2KS_TELE: TIntegerField;
    TTANK2KS_SZKOD: TStringField;
    TTANK2KS_AFASZ: TBCDField;
    TTANK2KS_AFAKOD: TStringField;
    TTANK2KS_AFAERT: TBCDField;
    TTANK2KS_ERTEK: TBCDField;
    TTANK2KS_VIKOD: TStringField;
    TTANK2KS_ATLAG: TBCDField;
    TTANK2KS_FIMOD: TStringField;
    TTANK2KS_TETAN: TIntegerField;
    TTANK2KS_TIPUS: TIntegerField;
    TTANK2KS_THELY: TStringField;
    TTANK2KS_TELES: TStringField;
    TTANK2KS_JARAT: TStringField;
    TTANK2KS_SOKOD: TStringField;
    TTANK2KS_SONEV: TStringField;
    TTANK2KS_ORSZA: TStringField;
    TTANK2KS_IDO: TStringField;
    TTANK2KS_KTIP: TIntegerField;
    TTANK2KS_GKKAT: TStringField;
    MSJASZAjavtsa1: TMenuItem;
    ADOQuery2: TADOQuery;
    ADOQuery2ms_id: TAutoIncField;
    Query22: TADOQuery;
    Szmlaszmellenrzs1: TMenuItem;
    ADOSZFEJ: TADODataSet;
    ADOSZFEJsa_kod: TStringField;
    Jratellenrzs1: TMenuItem;
    Megbzsoklistja1: TMenuItem;
    Pontozknyilvntartsa1: TMenuItem;
    NavCKltsgtelepitank1: TMenuItem;
    NAVC_unit: TADOQuery;
    NAVC_unitunit_id: TAutoIncField;
    NAVC_unitunitlabel: TWideStringField;
    NAVC_unitunittype: TWideStringField;
    NAVC_unitdevicetype: TWordField;
    NAVC_unitgprsid: TStringField;
    NAVC_unitdatacallnumber: TStringField;
    NAVC_unitsmsnumber: TStringField;
    NAVC_unitinstallkm: TFloatField;
    NAVC_unitdriver_id: TIntegerField;
    NAVC_unittag: TStringField;
    NAVC_unitinstallationdate: TDateTimeField;
    NAVC_unitvehicletype: TWordField;
    NAVC_unitinactivetolerance: TIntegerField;
    NAVC_unitkirene: TWideStringField;
    NAVC_unitfueltype: TWordField;
    NAVC_unitconsumptionnorm: TFloatField;
    NAVC_GPRS: TADOQuery;
    NAVC_GPRSgprs_id: TAutoIncField;
    NAVC_GPRSdate_time: TDateTimeField;
    NAVC_GPRSvalid: TWordField;
    NAVC_GPRSlat: TFloatField;
    NAVC_GPRSlon: TFloatField;
    NAVC_GPRSangle: TSmallintField;
    NAVC_GPRSspeed: TSmallintField;
    NAVC_GPRSdigit: TWordField;
    NAVC_GPRSaltitude: TSmallintField;
    NAVC_GPRSsat: TWordField;
    NAVC_GPRSgeoinfo: TWordField;
    NAVC_IO: TADOQuery;
    NAVC_IOio_id: TAutoIncField;
    NAVC_IOrecordid: TIntegerField;
    NAVC_IOdevicetype: TWordField;
    NAVC_IOioid: TWordField;
    NAVC_IOiovalue: TFloatField;
    KTSG: TADODataSet;
    KTSGKS_KTKOD: TIntegerField;
    KTSGKS_JAKOD: TStringField;
    KTSGKS_DATUM: TStringField;
    KTSGKS_KMORA: TBCDField;
    KTSGKS_VALNEM: TStringField;
    KTSGKS_ARFOLY: TBCDField;
    KTSGKS_MEGJ: TStringField;
    KTSGKS_RENDSZ: TStringField;
    KTSGKS_LEIRAS: TStringField;
    KTSGKS_UZMENY: TBCDField;
    KTSGKS_OSSZEG: TBCDField;
    KTSGKS_TELE: TIntegerField;
    KTSGKS_SZKOD: TStringField;
    KTSGKS_AFASZ: TBCDField;
    KTSGKS_AFAKOD: TStringField;
    KTSGKS_AFAERT: TBCDField;
    KTSGKS_ERTEK: TBCDField;
    KTSGKS_VIKOD: TStringField;
    KTSGKS_ATLAG: TBCDField;
    KTSGKS_FIMOD: TStringField;
    KTSGKS_TETAN: TIntegerField;
    KTSGKS_TIPUS: TIntegerField;
    KTSGKS_THELY: TStringField;
    KTSGKS_TELES: TStringField;
    KTSGKS_JARAT: TStringField;
    KTSGKS_SOKOD: TStringField;
    KTSGKS_SONEV: TStringField;
    KTSGKS_ORSZA: TStringField;
    KTSGKS_IDO: TStringField;
    KTSGKS_KTIP: TIntegerField;
    KTSGKS_GKKAT: TStringField;
    KTSGKS_NC_KM: TBCDField;
    KTSGKS_NC_USZ: TBCDField;
    KTSGKS_NC_UME: TBCDField;
    KTSGKS_NC_DAT: TStringField;
    KTSGKS_NC_IDO: TStringField;
    ADODataSet8: TADODataSet;
    ADOC_NAVC: TADOConnection;
    resenfutottKM1: TMenuItem;
    JARAT3: TADOQuery;
    JARAT3JA_KOD: TStringField;
    JARAT3JA_JARAT: TStringField;
    JARAT3JA_JKEZD: TStringField;
    JARAT3JA_JVEGE: TStringField;
    JARAT3JA_RENDSZ: TStringField;
    JARAT3JA_SAJAT: TIntegerField;
    JARAT3JA_MEGJ: TStringField;
    JARAT3JA_MENSZ: TStringField;
    JARAT3JA_NYITKM: TBCDField;
    JARAT3JA_ZAROKM: TBCDField;
    JARAT3JA_BELKM: TBCDField;
    JARAT3JA_ALKOD: TBCDField;
    JARAT3JA_FAZIS: TBCDField;
    JARAT3JA_OSSZKM: TBCDField;
    JARAT3JA_ALVAL: TIntegerField;
    JARAT3JA_ORSZ: TStringField;
    JARAT3JA_SOFK1: TStringField;
    JARAT3JA_SOFK2: TStringField;
    JARAT3JA_SNEV1: TStringField;
    JARAT3JA_SNEV2: TStringField;
    JARAT3JA_JPOTK: TStringField;
    JARAT3JA_FAZST: TStringField;
    JARAT3JA_KEIDO: TStringField;
    JARAT3JA_VEIDO: TStringField;
    JARAT3JA_LFDAT: TStringField;
    JARAT3JA_SZAK1: TBCDField;
    JARAT3JA_SZAK2: TBCDField;
    JARAT3JA_URESK: TStringField;
    JARAT3JA_EUROS: TStringField;
    JARAT3JA_UBKM: TStringField;
    JARAT3JA_UKKM: TStringField;
    JARAT3JA_JAORA: TIntegerField;
    JARAT3JA_FAJKO: TIntegerField;
    Pontoknyilvntartsa1: TMenuItem;
    Pontoklistja1: TMenuItem;
    Jrattisztts1: TMenuItem;
    JARAT4: TADOQuery;
    JARAT4JA_KOD: TStringField;
    JARAT4JA_JARAT: TStringField;
    JARAT4JA_JKEZD: TStringField;
    JARAT4JA_JVEGE: TStringField;
    JARAT4JA_RENDSZ: TStringField;
    JARAT4JA_SAJAT: TIntegerField;
    JARAT4JA_MEGJ: TStringField;
    JARAT4JA_MENSZ: TStringField;
    JARAT4JA_NYITKM: TBCDField;
    JARAT4JA_ZAROKM: TBCDField;
    JARAT4JA_BELKM: TBCDField;
    JARAT4JA_ALKOD: TBCDField;
    JARAT4JA_FAZIS: TBCDField;
    JARAT4JA_OSSZKM: TBCDField;
    JARAT4JA_ALVAL: TIntegerField;
    JARAT4JA_ORSZ: TStringField;
    JARAT4JA_SOFK1: TStringField;
    JARAT4JA_SOFK2: TStringField;
    JARAT4JA_SNEV1: TStringField;
    JARAT4JA_SNEV2: TStringField;
    JARAT4JA_JPOTK: TStringField;
    JARAT4JA_FAZST: TStringField;
    JARAT4JA_KEIDO: TStringField;
    JARAT4JA_VEIDO: TStringField;
    JARAT4JA_LFDAT: TStringField;
    JARAT4JA_SZAK1: TBCDField;
    JARAT4JA_SZAK2: TBCDField;
    JARAT4JA_URESK: TStringField;
    JARAT4JA_EUROS: TStringField;
    JARAT4JA_UBKM: TStringField;
    JARAT4JA_UKKM: TStringField;
    JARAT4JA_JAORA: TIntegerField;
    JARAT4JA_FAJKO: TIntegerField;
    VISZONY: TADOQuery;
    VISZONYVI_JAKOD: TStringField;
    VISZONYVI_HONNAN: TStringField;
    VISZONYVI_HOVA: TStringField;
    VISZONYVI_VEKOD: TStringField;
    VISZONYVI_FDEXP: TBCDField;
    VISZONYVI_FDIMP: TBCDField;
    VISZONYVI_FDKOV: TBCDField;
    VISZONYVI_VALNEM: TStringField;
    VISZONYVI_ARFOLY: TBCDField;
    VISZONYVI_MEGJ: TStringField;
    VISZONYVI_VIKOD: TStringField;
    VISZONYVI_VENEV: TStringField;
    VISZONYVI_KULF: TIntegerField;
    VISZONYVI_TIPUS: TBCDField;
    VISZONYVI_BEDAT: TStringField;
    VISZONYVI_KIDAT: TStringField;
    VISZONYVI_SAKOD: TStringField;
    VISZONYVI_UJKOD: TIntegerField;
    VISZONYVI_POZIC: TStringField;
    VISZONYVI_EVIKOD: TStringField;
    VISZONYVI_JARAT: TStringField;
    VISZONYVI_ARFDAT: TStringField;
    Query222: TADOQuery;
    MEGSEGED2: TADOQuery;
    VISZONY2: TADOQuery;
    MEGSEGED2MS_MBKOD: TIntegerField;
    MEGSEGED2MS_SORSZ: TIntegerField;
    MEGSEGED2MS_FELNEV: TStringField;
    MEGSEGED2MS_FELTEL: TStringField;
    MEGSEGED2MS_FELCIM: TStringField;
    MEGSEGED2MS_FELDAT: TStringField;
    MEGSEGED2MS_FELIDO: TStringField;
    MEGSEGED2MS_FETDAT: TStringField;
    MEGSEGED2MS_FETIDO: TStringField;
    MEGSEGED2MS_LERNEV: TStringField;
    MEGSEGED2MS_LERTEL: TStringField;
    MEGSEGED2MS_LERCIM: TStringField;
    MEGSEGED2MS_LERDAT: TStringField;
    MEGSEGED2MS_LERIDO: TStringField;
    MEGSEGED2MS_LETDAT: TStringField;
    MEGSEGED2MS_LETIDO: TStringField;
    MEGSEGED2MS_FELARU: TStringField;
    MEGSEGED2MS_FSULY: TBCDField;
    MEGSEGED2MS_FEPAL: TBCDField;
    MEGSEGED2MS_LSULY: TBCDField;
    MEGSEGED2MS_LEPAL: TBCDField;
    MEGSEGED2MS_FELSE1: TStringField;
    MEGSEGED2MS_FELSE2: TStringField;
    MEGSEGED2MS_FELSE3: TStringField;
    MEGSEGED2MS_FELSE4: TStringField;
    MEGSEGED2MS_FELSE5: TStringField;
    MEGSEGED2MS_LERSE1: TStringField;
    MEGSEGED2MS_LERSE2: TStringField;
    MEGSEGED2MS_LERSE3: TStringField;
    MEGSEGED2MS_LERSE4: TStringField;
    MEGSEGED2MS_LERSE5: TStringField;
    MEGSEGED2MS_ORSZA: TStringField;
    MEGSEGED2MS_FORSZ: TStringField;
    MEGSEGED2MS_TIPUS: TIntegerField;
    MEGSEGED2MS_FELIR: TStringField;
    MEGSEGED2MS_LELIR: TStringField;
    MEGSEGED2MS_DATUM: TStringField;
    MEGSEGED2MS_IDOPT: TStringField;
    MEGSEGED2MS_MSKOD: TIntegerField;
    MEGSEGED2MS_TINEV: TStringField;
    MEGSEGED2MS_STATK: TStringField;
    MEGSEGED2MS_STATN: TStringField;
    MEGSEGED2MS_TEIDO: TStringField;
    MEGSEGED2MS_TENNEV: TStringField;
    MEGSEGED2MS_TENTEL: TStringField;
    MEGSEGED2MS_TENCIM: TStringField;
    MEGSEGED2MS_TENYL1: TStringField;
    MEGSEGED2MS_TENYL2: TStringField;
    MEGSEGED2MS_TENYL3: TStringField;
    MEGSEGED2MS_TENYL4: TStringField;
    MEGSEGED2MS_TENYL5: TStringField;
    MEGSEGED2MS_TENOR: TStringField;
    MEGSEGED2MS_TENIR: TStringField;
    MEGSEGED2MS_AKTNEV: TStringField;
    MEGSEGED2MS_AKTTEL: TStringField;
    MEGSEGED2MS_AKTCIM: TStringField;
    MEGSEGED2MS_AKTOR: TStringField;
    MEGSEGED2MS_AKTIR: TStringField;
    MEGSEGED2MS_EXSTR: TStringField;
    MEGSEGED2MS_EXPOR: TIntegerField;
    MEGSEGED2MS_TEFNEV: TStringField;
    MEGSEGED2MS_TEFTEL: TStringField;
    MEGSEGED2MS_TEFCIM: TStringField;
    MEGSEGED2MS_TEFL1: TStringField;
    MEGSEGED2MS_TEFL2: TStringField;
    MEGSEGED2MS_TEFL3: TStringField;
    MEGSEGED2MS_TEFL4: TStringField;
    MEGSEGED2MS_TEFL5: TStringField;
    MEGSEGED2MS_TEFOR: TStringField;
    MEGSEGED2MS_TEFIR: TStringField;
    MEGSEGED2MS_REGIK: TIntegerField;
    MEGSEGED2MS_FAJKO: TIntegerField;
    MEGSEGED2MS_FAJTA: TStringField;
    MEGSEGED2MS_RENSZ: TStringField;
    MEGSEGED2MS_POTSZ: TStringField;
    MEGSEGED2MS_DOKOD: TStringField;
    MEGSEGED2MS_DOKO2: TStringField;
    MEGSEGED2MS_GKKAT: TStringField;
    MEGSEGED2MS_SNEV1: TStringField;
    MEGSEGED2MS_SNEV2: TStringField;
    MEGSEGED2MS_KIFON: TIntegerField;
    MEGSEGED2MS_FETED: TStringField;
    MEGSEGED2MS_FETEI: TStringField;
    MEGSEGED2MS_LETED: TStringField;
    MEGSEGED2MS_LETEI: TStringField;
    MEGSEGED2MS_LDARU: TIntegerField;
    MEGSEGED2MS_FDARU: TIntegerField;
    MEGSEGED2MS_ID: TAutoIncField;
    MEGSEGED2MS_NORID: TIntegerField;
    MEGSEGED2MS_FERKDAT: TStringField;
    MEGSEGED2MS_LERKDAT: TStringField;
    MEGSEGED2MS_FERKIDO: TStringField;
    MEGSEGED2MS_LERKIDO: TStringField;
    MEGSEGED2MS_FETEI2: TStringField;
    MEGSEGED2MS_LETEI2: TStringField;
    MEGSEGED2MS_FELIDO2: TStringField;
    MEGSEGED2MS_LERIDO2: TStringField;
    MEGSEGED2MS_LOADID: TStringField;
    MEGSEGED2MS_M_MAIL: TDateTimeField;
    MEGSEGED2MS_S_MAIL: TDateTimeField;
    MEGSEGED2MS_FETED2: TStringField;
    MEGSEGED2MS_LETED2: TStringField;
    MEGSEGED2MS_FELDAT2: TStringField;
    MEGSEGED2MS_LERDAT2: TStringField;
    MEGSEGED2MS_FPAFEL: TIntegerField;
    MEGSEGED2MS_FPALER: TIntegerField;
    MEGSEGED2MS_FPAIGAZT: TIntegerField;
    MEGSEGED2MS_FPAIGAZL: TStringField;
    MEGSEGED2MS_FPAMEGJ: TStringField;
    MEGSEGED2MS_LPAFEL: TIntegerField;
    MEGSEGED2MS_LPALER: TIntegerField;
    MEGSEGED2MS_LPAIGAZT: TIntegerField;
    MEGSEGED2MS_LPAIGAZL: TStringField;
    MEGSEGED2MS_LPAMEGJ: TStringField;
    MEGSEGED2MS_ID2: TIntegerField;
    MEGSEGED2MS_CSPAL: TIntegerField;
    MEGSEGED2MS_JAKOD: TStringField;
    MEGSEGED2MS_JASZA: TStringField;
    MEGSEGED2MS_AZTIP: TStringField;
    MEGSEGED2MS_HFOK1: TStringField;
    MEGSEGED2MS_HFOK2: TStringField;
    MEGSEGED2MS_FCPAL: TBCDField;
    MEGSEGED2MS_LCPAL: TBCDField;
    MEGSEGED2MS_TAVKM: TBCDField;
    VISZONY2VI_JAKOD: TStringField;
    VISZONY2VI_HONNAN: TStringField;
    VISZONY2VI_HOVA: TStringField;
    VISZONY2VI_VEKOD: TStringField;
    VISZONY2VI_FDEXP: TBCDField;
    VISZONY2VI_FDIMP: TBCDField;
    VISZONY2VI_FDKOV: TBCDField;
    VISZONY2VI_VALNEM: TStringField;
    VISZONY2VI_ARFOLY: TBCDField;
    VISZONY2VI_MEGJ: TStringField;
    VISZONY2VI_VIKOD: TStringField;
    VISZONY2VI_VENEV: TStringField;
    VISZONY2VI_KULF: TIntegerField;
    VISZONY2VI_TIPUS: TBCDField;
    VISZONY2VI_BEDAT: TStringField;
    VISZONY2VI_KIDAT: TStringField;
    VISZONY2VI_SAKOD: TStringField;
    VISZONY2VI_UJKOD: TIntegerField;
    VISZONY2VI_POZIC: TStringField;
    VISZONY2VI_EVIKOD: TStringField;
    VISZONY2VI_JARAT: TStringField;
    VISZONY2VI_ARFDAT: TStringField;
    resenfutottkilomterek1: TMenuItem;
    Hinyzsofrklistja1: TMenuItem;
    reskmazrba1: TMenuItem;
    Szmlaszmmegbzsba1: TMenuItem;
    SpeedButton7: TSpeedButton;
    Havizemanyagkimutats1: TMenuItem;
    GKKATKltsg2: TMenuItem;
    DolgServantes1: TMenuItem;
    Lejrtszmlk1: TMenuItem;
    Lejrtszmlk2: TMenuItem;
    Panel4: TPanel;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    ListBoxLoadedStyles: TListBox;
    BtnApply: TButton;
    QueryAlap: TADOQuery;
    QueryAlap2: TADOQuery;
    Label11: TLabel;
    Kereskedelmizemanyag1: TMenuItem;
    RBexport: TMenuItem;
    Adatbzisellenrzse1: TMenuItem;
    Kltsgekimportlsa1: TMenuItem;
    Klsllomnyokmegnyitsa1: TMenuItem;
    Pontozsstatisztika1: TMenuItem;
    GoogleMatrixteszt1: TMenuItem;
    MEZOKakzsbl1: TMenuItem;
    Gyorshajtsstatisztika1: TMenuItem;
    Fizetsimorllista1: TMenuItem;
    PODBridgestone1: TMenuItem;
    Igazoltgyorshajtas: TMenuItem;
    MegbzsjraszmolsMegbizaskieg1: TMenuItem;
    MegbzsKiegegymegbzsra1: TMenuItem;
    SendOutlookMail1: TMenuItem;
    mmiFelszolitoLevelLista: TMenuItem;
    N8: TMenuItem;
    mmiFelszolitoLevelKuldes: TMenuItem;
    Prba1: TMenuItem;
    Rgebbiek1: TMenuItem;
    N9: TMenuItem;
    YCOHistrocal1: TMenuItem;
    Megbzsthosszdjakjraszmols1: TMenuItem;
    Megbzsjraszmols1centekmiatt1: TMenuItem;
    Csehtranzitok1: TMenuItem;
    Ellenrzsek1: TMenuItem;
    Pnzgyilistk1: TMenuItem;
    Tavolletidij: TMenuItem;
    elefonSIMelzmnyek1: TMenuItem;
    Palettavltozsok1: TMenuItem;
    Palettavltozsoklistja1: TMenuItem;
    Adatszolgltats1: TMenuItem;
    Adhatsgiellenrzsiadatszolgltats1: TMenuItem;
    zemanyagstatisztikaexport1: TMenuItem;
    MySMS1: TMenuItem;
    Szervizmunkalapok1: TMenuItem;
    Kapcsolatok1: TMenuItem;
    SMS1: TMenuItem;
    Klds1: TMenuItem;
    zenetek1: TMenuItem;
    TelElofizetesek: TMenuItem;
    N10: TMenuItem;
    Ritkbbanhasznltelemek1: TMenuItem;
    elefon1: TMenuItem;
    Krtyk1: TMenuItem;
    elefonszmlkbeolvassa1: TMenuItem;
    elefonszmlk1: TMenuItem;
    N12: TMenuItem;
    FAPexport1: TMenuItem;
    Rendkvliesemnyek1: TMenuItem;
    Esemnyrgztse1: TMenuItem;
    Esemnyekttekintse1: TMenuItem;
    Dolgoztabl1: TMenuItem;
    N13: TMenuItem;
    Proba2: TMenuItem;
    Rendkvliesemnyrgztse1: TMenuItem;
    Flottahelyzet1: TMenuItem;
    echnikaizenetkldse1: TMenuItem;
    gyintzs1: TMenuItem;
    Megrendelk1: TMenuItem;
    jkdkrse1: TMenuItem;
    Kdokttekintse1: TMenuItem;
    Encode1: TMenuItem;
    elefonkiegsztk1: TMenuItem;
    zemanyagktllapota1: TMenuItem;
    Fizetsijegyzkektovbbtsa1: TMenuItem;
    Osztrkrakodsokjelentse1: TMenuItem;
    VCARDexport1: TMenuItem;
    elefonszmlkttekints1: TMenuItem;
    elefonszmlattelek1: TMenuItem;
    elefonszmlk2: TMenuItem;
    N11: TMenuItem;
    elefonszmlastatisztikk1: TMenuItem;
    Szolgltatsszinten1: TMenuItem;
    Elfizetsszinten1: TMenuItem;
    Alkalmi1: TMenuItem;
    N14: TMenuItem;
    HECPOLLtankolsimport1: TMenuItem;
    SMSEagleQueuelength1: TMenuItem;
    elszmlaHINFOupdate1: TMenuItem;
    VCARDbelltsok1: TMenuItem;
    Egybkontaktok1: TMenuItem;
    elefonnvjegyzkexportbelltsok1: TMenuItem;
    elefonknyv1: TMenuItem;
    Napi1: TMenuItem;
    Importlerakaljrattal1: TMenuItem;
    Nagyautaljrattal1: TMenuItem;
    Kisautaljrattal1: TMenuItem;
    Htsaljrattal1: TMenuItem;
    Mindenmegbzsaljrattal1: TMenuItem;
    EKAER1: TMenuItem;
    MNBArfolyamteszt1: TMenuItem;
    CromeOnTop1: TMenuItem;
    mmiPotkocsiUzemorak: TMenuItem;
    mmiCockpitBeallitasok: TMenuItem;
    Kontaktok1: TMenuItem;
    Csoportok1: TMenuItem;
    N15: TMenuItem;
    FuvarosCockpitszinkronizls1: TMenuItem;
    Cgek1: TMenuItem;
    N16: TMenuItem;
	procedure SzotarClick(Sender: TObject);
	procedure FormClose(Sender: TObject; var Action: TCloseAction);
	procedure FormCreate(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure FelhasznalokClick(Sender: TObject);
    procedure JelszavakClick(Sender: TObject);
	 procedure FormResize(Sender: TObject);
	 procedure JogokClick(Sender: TObject);
	 procedure FormActivate(Sender: TObject);
	 procedure FormShow(Sender: TObject);
	 procedure KilepesClick(Sender: TObject);
	 function  Cimiro : string;
	 procedure NevjegyClick(Sender: TObject);
	 procedure SegitsegClick(Sender: TObject);
	 procedure DisplayMessage(var Msg: TMsg; var Handled: Boolean);
	 procedure TermekekClick(Sender: TObject);
    procedure FormCanResize(Sender: TObject; var NewWidth,
      NewHeight: Integer; var Resize: Boolean);
	 procedure DolgozokClick(Sender: TObject);
	 procedure VevokClick(Sender: TObject);
	// procedure AlvallakozokClick(Sender: TObject);
    procedure GepkocsikClick(Sender: TObject);
    procedure ArfolyamokClick(Sender: TObject);
    procedure ShellekClick(Sender: TObject);
    procedure KoltsegekClick(Sender: TObject);
    procedure JaratokClick(Sender: TObject);
    procedure TelefonokClick(Sender: TObject);
	 procedure SulyadatokClick(Sender: TObject);
    procedure TerhelesekClick(Sender: TObject);
	 procedure SzamlazasClick(Sender: TObject);
    procedure OsszszamlazasClick(Sender: TObject);
    procedure CegadatokClick(Sender: TObject);
	 procedure ForgalmikiClick(Sender: TObject);
	 procedure MenetlevelClick(Sender: TObject);
	 procedure KoltsegkiClick(Sender: TObject);
    procedure FizetesikiClick(Sender: TObject);
	 procedure NapidijClick(Sender: TObject);
    procedure UtnyilvantartasClick(Sender: TObject);
	 procedure UzemanyagkiClick(Sender: TObject);
	 procedure KoltsegosszesClick(Sender: TObject);
	 procedure HianyzojaratokClick(Sender: TObject);
    procedure HianyzomenetlevelekClick(Sender: TObject);
    procedure AdatellenorzesClick(Sender: TObject);
	 procedure ErvenyessegekClick(Sender: TObject);
	 procedure VevoExportClick(Sender: TObject);
    procedure ZarokmClick(Sender: TObject);
	 procedure UzemanyagarClick(Sender: TObject);
    procedure APEHuzemanyagClick(Sender: TObject);
	 procedure zemanyagfogyasztsiadatok1Click(Sender: TObject);
	 procedure elepitankolsokgenerlsa1Click(Sender: TObject);
	 procedure GepcsereClick(Sender: TObject);
	 procedure TankimportClick(Sender: TObject);
	 procedure Timer2Timer(Sender: TObject);
	 procedure Uzenetek_kezeleseClick(Sender: TObject);
	 procedure UzenetEllenorzes;
	 procedure MegbizasokClick(Sender: TObject);
	 procedure HutokimutatasClick(Sender: TObject);
	 procedure FelrakasokClick(Sender: TObject);
	 procedure FormPaint(Sender: TObject);
	 procedure FormSzinezo;
	 procedure SzotarfrissitesClick(Sender: TObject);
	 procedure NyitoExportClick(Sender: TObject);
	 procedure KimutatasFeleslegesClick(Sender: TObject);
    procedure KimutatasLizingClick(Sender: TObject);
	 procedure FeketelistasokClick(Sender: TObject);
	 procedure ForgAlvallalClick(Sender: TObject);
	 procedure ExportEllenorClick(Sender: TObject);
	 procedure AdatbazisImport2Click(Sender: TObject);
	 procedure CMRkarbantarts1Click(Sender: TObject);
    // procedure AlGepkocsiClick(Sender: TObject);
    procedure TetanarClick(Sender: TObject);
	 procedure XMLGeneralasClick(Sender: TObject);
	 procedure LogExportClick(Sender: TObject);
	 procedure GepjarmuStatClick(Sender: TObject);
	 procedure PotkocsiStatClick(Sender: TObject);
	 procedure FuvartablaClick(Sender: TObject);
    procedure HianyDoksikClick(Sender: TObject);
    procedure DolgervenyClick(Sender: TObject);
    procedure Szmlaadatokmentse1Click(Sender: TObject);
    procedure ADR030Exportls1Click(Sender: TObject);
    procedure HianyzokmClick(Sender: TObject);
    procedure AdBluetankolasClick(Sender: TObject);
    procedure KPIkimutats1Click(Sender: TObject);
    procedure MSriport1Click(Sender: TObject);
    procedure GkvezetidoClick(Sender: TObject);
	 procedure KombiExportClick(Sender: TObject);
	 procedure DkvKartyakClick(Sender: TObject);
    procedure ApplicationEvents1Message(var Msg: tagMSG;
      var Handled: Boolean);
    procedure Button1Click(Sender: TObject);
    procedure AdatbazisImportClick(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure GKStatFrissitesClick(Sender: TObject);
    procedure PKStatFrissitesClick(Sender: TObject);
    procedure telekexportlsaaknyvelshez1Click(Sender: TObject);
    procedure GKSOBELbers2Click(Sender: TObject);
    procedure SzmlkjraszmolsaEUR2Click(Sender: TObject);
    procedure FormClick(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure Dolgcsop1Click(Sender: TObject);
    procedure KOLTSCalcFields(DataSet: TDataSet);
    procedure ankimportTeletankjavts1Click(Sender: TObject);
    procedure GepkocsikorClick(Sender: TObject);
    procedure Httankolsktsg1Click(Sender: TObject);
    procedure Kamera1Click(Sender: TObject);
    procedure rkpteszt1Click(Sender: TObject);
    procedure Kszletkezels1Click(Sender: TObject);
    procedure Kszletkezels2Click(Sender: TObject);
    procedure rfolyamtlt1Click(Sender: TObject);
    procedure Napok1Click(Sender: TObject);
    procedure Szabadnapokmunkanapok1Click(Sender: TObject);
    procedure Alvllalkozvev1Click(Sender: TObject);
    procedure Panel1DblClick(Sender: TObject);
    procedure RendszSULYOK1Click(Sender: TObject);
    procedure GkKatKltsg1Click(Sender: TObject);
    procedure Sajtkonfigbelltstrlse1Click(Sender: TObject);
    procedure Kltsgrtkszmols1Click(Sender: TObject);
    procedure Kltsgektrlsejrafeldolgozshoz1Click(Sender: TObject);
    procedure LOADID1Click(Sender: TObject);
    procedure Paletta1Click(Sender: TObject);
    procedure SpeedButton6Click(Sender: TObject);
    procedure JARATMEGSEGED1Click(Sender: TObject);
    procedure Jratokvisszazrsa1Click(Sender: TObject);
    procedure LoadIDmegtekintse1Click(Sender: TObject);
    procedure elepiteletankokjavtsa1Click(Sender: TObject);
    procedure MSJASZAjavtsa1Click(Sender: TObject);
    procedure Szmlaszmellenrzs1Click(Sender: TObject);
    procedure kiv;
    procedure Jratellenrzs1Click(Sender: TObject);
    procedure Megbzsoklistja1Click(Sender: TObject);
    procedure Pontozknyilvntartsa1Click(Sender: TObject);
    procedure NavCKltsgtelepitank1Click(Sender: TObject);
    procedure resenfutottKM1Click(Sender: TObject);
    procedure Pontoknyilvntartsa1Click(Sender: TObject);
    procedure Pontoklistja1Click(Sender: TObject);
    procedure Jrattisztts1Click(Sender: TObject);
    procedure resenfutottkilomterek1Click(Sender: TObject);
    procedure Hinyzsofrklistja1Click(Sender: TObject);
    procedure reskmazrba1Click(Sender: TObject);
    procedure Szmlaszmmegbzsba1Click(Sender: TObject);
    procedure Havizemanyagkimutats1Click(Sender: TObject);
    procedure GKKATKltsg2Click(Sender: TObject);
    procedure DolgServantes1Click(Sender: TObject);
    procedure Lejrtszmlk1Click(Sender: TObject);
    procedure Lejrtszmlk2Click(Sender: TObject);
    procedure BtnApplyClick(Sender: TObject);
    procedure Kereskedelmizemanyag1Click(Sender: TObject);
    procedure RBexportClick(Sender: TObject);
    procedure Kltsgekimportlsa1Click(Sender: TObject);
    procedure Adatbzisellenrzse1Click(Sender: TObject);
    procedure PontozasCheck;
    procedure Klsllomnyokmegnyitsa1Click(Sender: TObject);
    procedure Pontozsstatisztika1Click(Sender: TObject);
    procedure GoogleMatrixteszt1Click(Sender: TObject);
    procedure MEZOKakzsbl1Click(Sender: TObject);
    procedure Gyorshajtsstatisztika1Click(Sender: TObject);
    procedure Fizetsimorllista1Click(Sender: TObject);
    procedure PODBridgestone1Click(Sender: TObject);
    procedure IgazoltgyorshajtasClick(Sender: TObject);
    procedure MegbzsjraszmolsMegbizaskieg1Click(Sender: TObject);
    procedure MegbzsKiegegymegbzsra1Click(Sender: TObject);
    procedure SendOutlookMail1Click(Sender: TObject);
    procedure mmiFelszolitoLevelKuldesClick(Sender: TObject);
    procedure mmiFelszolitoLevelListaClick(Sender: TObject);
    procedure Prba1Click(Sender: TObject);
    procedure YCOHistrocal1Click(Sender: TObject);
    procedure Megbzsthosszdjakjraszmols1Click(Sender: TObject);
    procedure Megbzsjraszmols1centekmiatt1Click(Sender: TObject);
    procedure Csehtranzitok1Click(Sender: TObject);
    procedure TavolletidijClick(Sender: TObject);
    procedure elefonSIMelzmnyek1Click(Sender: TObject);
    procedure Palettavltozsok1Click(Sender: TObject);
    procedure Palettavltozsoklistja1Click(Sender: TObject);
    procedure Adhatsgiellenrzsiadatszolgltats1Click(Sender: TObject);
    procedure zemanyagstatisztikaexport1Click(Sender: TObject);
    procedure MySMS1Click(Sender: TObject);
    procedure Szervizmunkalapok1Click(Sender: TObject);
    procedure Klds1Click(Sender: TObject);
    procedure zenetek1Click(Sender: TObject);
    procedure TelElofizetesekClick(Sender: TObject);
    procedure elefonszmlk1Click(Sender: TObject);
    procedure elefonszmlkbeolvassa1Click(Sender: TObject);
    procedure ShowTelefonSzamlak;
    procedure FAPexport1Click(Sender: TObject);
    procedure Esemnyekttekintse1Click(Sender: TObject);
    procedure Dolgoztabl1Click(Sender: TObject);
    procedure Esemnyrgztse1Click(Sender: TObject);
    procedure Proba2Click(Sender: TObject);
    procedure Rendkvliesemnyrgztse1Click(Sender: TObject);
    procedure Flottahelyzet1Click(Sender: TObject);
    procedure echnikaizenetkldse1Click(Sender: TObject);
    procedure jkdkrse1Click(Sender: TObject);
    procedure Kdokttekintse1Click(Sender: TObject);
    procedure Encode1Click(Sender: TObject);
    procedure elefonkiegsztk1Click(Sender: TObject);
    procedure zemanyagktllapota1Click(Sender: TObject);
    procedure Fizetsijegyzkektovbbtsa1Click(Sender: TObject);
    procedure Osztrkrakodsokjelentse1Click(Sender: TObject);
    procedure VCARDexport1Click(Sender: TObject);
    procedure elefonszmlkttekints1Click(Sender: TObject);
    procedure elefonszmlattelek1Click(Sender: TObject);
    procedure Szolgltatsszinten1Click(Sender: TObject);
    procedure Elfizetsszinten1Click(Sender: TObject);
    procedure HECPOLLtankolsimport1Click(Sender: TObject);
    procedure SMSEagleQueuelength1Click(Sender: TObject);
    procedure elszmlaHINFOupdate1Click(Sender: TObject);
    procedure elefonnvjegyzkexportbelltsok1Click(Sender: TObject);
    procedure Egybkontaktok1Click(Sender: TObject);
    procedure elefonknyv1Click(Sender: TObject);
    procedure EKAER_napi_riport(Melyik: TEKAERRiport);
    procedure Kisautaljrattal1Click(Sender: TObject);
    procedure Importlerakaljrattal1Click(Sender: TObject);
    procedure Nagyautaljrattal1Click(Sender: TObject);
    procedure Htsaljrattal1Click(Sender: TObject);
    procedure Mindenmegbzsaljrattal1Click(Sender: TObject);
    procedure AlGepkocsiClick(Sender: TObject);
    procedure MNBArfolyamteszt1Click(Sender: TObject);
    procedure Proba1;
    procedure CromeOnTop1Click(Sender: TObject);
    procedure mmiPotkocsiUzemorakClick(Sender: TObject);
    procedure Kontaktok1Click(Sender: TObject);
    procedure Csoportok1Click(Sender: TObject);
    procedure FuvarosCockpitszinkronizls1Click(Sender: TObject);
    procedure Cgek1Click(Sender: TObject);

	private
		felsumma	: double;
		mesz		: integer;
       kiellenor   : boolean;
    procedure LoadRegisteredStyles;
    procedure SetStyleByIndex(Index:Integer);
	public
    NELSOINFO:Boolean;
	 //	ALAPSTR			: string;
		function 	AppHelp(Command: Word; Data: Longint; var CallHelp: Boolean): Boolean;
		procedure 	DisplayHint(Sender: TObject);
    procedure Nevnap;
	end;


var
	FomenuDlg   : TFomenuDlg;
   FRestart    : boolean;
//  Maidatum:TDate;

implementation

uses
	Egyeb,    		Bazuj,			About,    		Kozos,			TermekFm,
	VevoUjfm, 		Arfolyamfm,		ShellFm,		TerhelesFm,		TelefonFm, TelElofizetFm,
	CegadatFm ,		KoltsegFm,		SulyokFm,		GepkocsiUjFm,	AlvallalUjFm,
	JaratFm,		SzamlaFm,		OSzamlaFm,      Zarokmfm,		JogForm,
	UzemanyagFm,	ApehUzem,		ArfolyamTolto,	Jelcsere, 		FelForm,
	CMRFm,			FuvartablaFm,	DkvKarFm,
	// Kimutat�sok
	Forgalom,		Napidij,		Menetlev,	 	Utnyil,			FelrakoFm,
	Uzema,			KoltNyil,		Ossznyil,	 	FizNyil,		ExportHason,
	J_Fogyaszt,		TelepGener,  	Gepcsere,		ForgAlval,
	Tankimport,		Adatell,		UzenetFm,		KiFolos,	 	KiLizing,
	MegbizasUjFm,	VevoExport,		Kihuto,			SzotarFrissito,	NyitoExport,
	FeketeList,		DolgozoUjFm,	GepStatFm,		Adatimport,		GkIdoKm,
	AlGepFm,		TetanarUjFm,   	XML_Export,		logExport,      PotStatFm,
	KiHianyDoksi,	KiDolgErv,		SzamlaExport,	ADR30_Export,	HianyKm,
	KiAdBlue,		KPI_Export,		MSR_Export,		Kombi_Export,   LGauge,
	// Adatell,  Jarcsere,
	J_SQL, J_DataModule, Kozos_Local
  , Info_, Terkep, Terkep2, NyitoKep, DB_Import, Tavollet, Windows, Nevnap,
  GkKorKm, Kamera, Main, {KoltsegImport, } StrUtils, Keszlet,
  KeszletFm, Napok, Math, KtsgDel, LoadIDjFm, PalettaFm,ViszonyBe,J_DataFunc,
  Jarat_ell, MegbizasFm, PontozoFm, UjaratFm, PontokFm,
  PontKimut, Ureskmnyil, UzemaHavi, LejartszamlaFm, POD_Export,
   Kikeres,        RB_Export,      KoltImpUjFm,    BazisEllen, UniStatFm,
   KulsoFile, PontStatFm, GyorshajtStatFm, GoogleDistance, Googlepoints,
   TypInfo, Idoszakbe, NVCKIVFm, SzamlaLevel,  TYCO_Historical, UniEllenor,
   NAVOutput, PalvaltFm, KiPaletta, UzenetSzerkeszto, TelSzamlaOlvasoPDF,
   System.IOUtils, Vcl.Styles, Vcl.Themes, Proba, Tavolletidij, UzemaExport,
  SMSKuld, UzenetSzalak, _proba_Cimsor, Fap_Export1, _proba_szamitas, DolgozoTablo,
  RendkivuliEsemenybe, FlottaHelyzet, MegrendeloKodkeres, Kutallapot, TelKiegFm,
  FizetesiJegyzekLevel, VCardRoutines, TelSzamlaFm, TelSzamlaTetelekFm, TankTranOlvaso,
  SMSEagle, EKontaktFm, SzamlaNezo, EDIFACT_Export, mnb, FuvarosDatum,
  UzenetTop, URLCache;

{$R *.DFM}

procedure TFomenuDlg.FormCreate(Sender: TObject);
begin
   // A st�lus v�laszt� panel eldug�sa
   Panel4.Visible  := false;
   EgyebDlg.SetMaidatum;
   //Maidatum:=Date;
   FormatSettings.DateSeparator    :=  '.';
   FormatSettings.ShortDateFormat  :=  'yyyy.MM.dd.';
   FormatSettings.TimeSeparator    :=  ':';
   FormatSettings.ShortTimeFormat  :=  'h:mm';
   FormatSettings.LongTimeFormat   :=  'h:mm:ss';
   FormatSettings.DecimalSeparator :=  ',';
   EgyebDlg.TIZEDESJEL             := FormatSettings.DecimalSeparator;
	Application.OnHint 			    := DisplayHint;
	Application.OnMessage		    := DisplayMessage;
	StatusBar1.Panels[0].Text 	    := '';
	Caption 					    := Cimiro;
	felsumma    				    := 0;
   kiellenor                       := true;
	EgyebDlg.SeTADOQueryDatabase(QueryKoz);
	EgyebDlg.SeTADOQueryDatabase(QueryAlap);
	EgyebDlg.SeTADOQueryDatabase(QueryAlap2);
	mesz := 0;
   Label11.Caption                 := 'Felhaszn�l� : '+EgyebDlg.user_name;

   Label2.Caption:=EgyebDlg.TESZTDB;
   Label3.Caption:=EgyebDlg.TESZTDAT;
   Label2.Visible:=(EgyebDlg.import_dat<>'') and (pos('TESZT',EgyebDlg.v_alap_db)>0);
   Label3.Visible:=(EgyebDlg.import_dat<>'') and (pos('TESZT',EgyebDlg.v_alap_db)>0);
//  LabEV.Visible:= (pos('TESZT',EgyebDlg.v_alap_db)=0);
//  But2012.Visible:= (pos('TESZT',EgyebDlg.v_alap_db)=0);
//  But2013.Visible:= (pos('TESZT',EgyebDlg.v_alap_db)=0);
   Application.ProcessMessages;

   LoadRegisteredStyles;
   ListBoxLoadedStyles.ItemIndex:=0;
   mmiFelszolitoLevelKuldes.Enabled:= EgyebDlg.LEJARTSZAMLA_LEVEL_ENABLED;
   mmiFelszolitoLevelLista.Enabled:= EgyebDlg.LEJARTSZAMLA_LEVEL_ENABLED;
   VevoExport.Enabled:= (EgyebDlg.KONYVELES_ATAD_FORMA = 'SERVANTES');
end;

procedure TFomenuDlg.DisplayMessage(var Msg: TMsg; var Handled: Boolean);
begin
(*
	if Msg.message = WM_KEYDOWN then begin
		if msg.wParam = 112 then begin
			{Az F1 lett megnyomva}
  //			StatusBar1.Panels[0].Text := 'Help';
//			NoticeKi('Ez egy help '+IntToStr(Screen.ActiveForm.Tag));
           // Ide j�het a teszt processz
		    Handled := false;
	    end;
	end;
	Inc(mesz);
	Caption	:= IntToStr(mesz);
*)
end;

function TFomenuDlg.AppHelp(Command: Word; Data: Longint; var CallHelp: Boolean): Boolean;
begin
	Result	:= true;
end;

procedure TFomenuDlg.DisplayHint(Sender: TObject);
begin
	StatusBar1.Panels[0].Text := ' ' + Application.Hint;
end;

procedure TFomenuDlg.Timer1Timer(Sender: TObject);
begin
	//StatusBar1.Panels[1].Text := EgyebDlg.MaiDatum+'   '+FormatDateTime('hh.nn',SysUtils.Time)+' ';
  //Maidatum:=date;
  EgyebDlg.SetMaidatum;
	StatusBar1.Panels[1].Text := EgyebDlg.MaiDatum+' ';
  if EgyebDlg.Maidatum<>DateToStr(date) then
  begin
    Timer1.Enabled:=False;
    Nevnap;
  end;
end;

procedure TFomenuDlg.FormClose(Sender: TObject; var Action: TCloseAction);
begin
   if ( ( not FRestart ) and (kiellenor) ) then begin
	    if NoticeKi('Val�ban kil�p a programb�l?', NOT_QUESTION) <> 0 then begin
    	    // Nem az 'Igen' lett megnyomva .
		    Action := caNone;
  	    end
      else begin
        URLCacheDlg.MemoryCachePersistAccessedKeys;
        EgyebDlg.CommitTrans_ALAPKOZOS;  // biztos ami biztos, ha esetleg tranzakci�ban lenne
        end;
   end;
end;

procedure TFomenuDlg.FormResize(Sender: TObject);
begin
//	StatusBar1.Panels[1].Width := Round(FomenuDlg.Width * 1 / 6);
	 StatusBar1.Panels[1].Width := 150;
	 StatusBar1.Panels[0].Width := ClientWidth - StatusBar1.Panels[1].Width;
   SpeedButton4.Left:=Panel1.Width-56;
   try       // program bez�r�skor Access Violation lehet
     if FInfo_ <> nil then
       FInfo_.Top:=FomenuDlg.Top+ FomenuDlg.Height-FInfo_.Height-FomenuDlg.StatusBar1.Height;
   except end;
end;

procedure TFomenuDlg.FormActivate(Sender: TObject);
var
	i, j, k	: integer;
  Teszt_vagy_dev: boolean;

  procedure MenuEngedelyez(AMenu: TMenuItem); // t�bbszint� rekurz�v
    var
      i, righttag: integer;
      tm: TMenuItem;
    begin
      for i:= 0 to AMenu.Count - 1 do begin
        tm := Amenu[i];
        if NO_MENU_SHOW then begin
          righttag:= GetRightTag(tm.Tag);
          tm.Enabled := (righttag  <> RG_NORIGHT );
          end
        else begin
          righttag:= GetRightTag(tm.Tag);
          tm.visible := (righttag <> RG_NORIGHT );
          end;  // else
        MenuEngedelyez(Amenu[i]);
        end;  // for
    end;


  {procedure MenuEngedelyez_OLD;
    begin
      for i:= 0 to Mainmenu1.Items.Count - 1 do begin
      tm := Mainmenu1.Items[i];
       if NO_MENU_SHOW then begin
        tm.Enabled := ( GetRightTag(tm.Tag) <> RG_NORIGHT );
       end else begin
        tm.visible := ( GetRightTag(tm.Tag) <> RG_NORIGHT );
       end;
        for j := 0 to tm.Count-1 do begin
          if ( tm.Items[j].Caption <> '-' ) then begin
          if NO_MENU_SHOW then begin
              tm.Items[j].Enabled := ( GetRightTag(tm.Items[j].Tag) <> RG_NORIGHT );
           end else begin
              tm.Items[j].Visible := ( GetRightTag(tm.Items[j].Tag) <> RG_NORIGHT );
           end;
            for k := 0 to tm.Items[j].Count-1 do begin
            if NO_MENU_SHOW then begin
                tm.Items[j].Items[k].Enabled := ( GetRightTag(tm.Items[j].Tag) <> RG_NORIGHT );
            end else begin
                tm.Items[j].Items[k].Visible := ( GetRightTag(tm.Items[j].Tag) <> RG_NORIGHT );
            end;
          end;
          end;
        end;
    end;
   }
begin
  EgyebDlg.GoogleMap('','','','',True);
  Label2.Caption:=EgyebDlg.TESZTDB;
  Label3.Caption:=EgyebDlg.TESZTDAT;
  if not EgyebDlg.user_super then begin
		// Ha nem rendszergazda, megvizsg�lhatjuk a jogosults�gokat
    MenuEngedelyez(MainMenu1.Items);
		Gepcsere.Visible	:= false;

	  // Megn�zz�k az ikonokat is
	  for i := 1 to ComponentCount - 1 do begin
		if (Components[i] is TSpeedButton ) then begin
			with ( Components[i] as TSpeedButton ) do begin
					if NO_MENU_SHOW then begin
				Enabled := ( GetRightTag(Tag) <> RG_NORIGHT );
			   end else begin
				Visible := ( GetRightTag(Tag) <> RG_NORIGHT );
			   end;
			end;
		 end;
	  end;
	end;

	// Az adatb�zis import enged�lyez�se vagy tilt�sa
  Teszt_vagy_dev:=(pos('TESZT',UpperCase(EgyebDlg.v_alap_db))>0) or (pos('DEV',UpperCase(EgyebDlg.v_alap_db))>0);
  Label2.Visible:=(EgyebDlg.import_dat<>'') and Teszt_vagy_dev;
  Label3.Visible:=(EgyebDlg.import_dat<>'') and Teszt_vagy_dev;
  AdatbazisImport.Visible:=(EgyebDlg.import_dat<>'') and Teszt_vagy_dev;
//  LabEV.Visible:= (pos('TESZT',EgyebDlg.v_alap_db)=0);
//  But2012.Visible:= (pos('TESZT',EgyebDlg.v_alap_db)=0);
//  But2013.Visible:= (pos('TESZT',EgyebDlg.v_alap_db)=0);

  Rendszergazda1.Visible:=EgyebDlg.user_super;
//  GKStatFrissites.Visible:=(EgyebDlg.user_super);
//  PKStatFrissites.Visible:=(EgyebDlg.user_super);
{	AdatbazisImport.Visible	:= false;
	if EgyebDlg.import_dat	<> '' then begin
		AdatbazisImport.Visible	:= true;
	end;
 }
	// A fuvart�bla ill. XML export enged�lyez�se v. tilt�sa
	if EgyebDlg.VANLANID < 1 then begin
		Fuvartabla.Visible		:= false;
		XMLGeneralas.Visible    := false;
	end;
   PontozasCheck;
end;

procedure TFomenuDlg.FormShow(Sender: TObject);
begin
   EgyebDlg.SetMaidatum;
   // ---- Kivettem innen 2016.10.03. NagyP
   // Application.CreateForm(TFInfo_, FInfo_);
   // --------------------------------------
   // A servantes adatb�zis ellen�rz�se
   // EgyebDlg.DolgServantes;
   NyitoKepDlg.Free;
	Timer1Timer(Sender);
	FormResize(Sender);
   Label1.Caption		:= EgyebDlg.Alcim;
	Timer2Timer(Sender);
   FormSzinezo;

   // A null�s j�ratid�k kisz�m�t�sa
   EgyebDlg.JaratIdotartam;

  /////////////////////////////////  N�vnap
  Nevnap;
 {
  Application.CreateForm(TFNevnap, FNevnap);
  Label7.Caption:= FNevnap.Nevnap_dat(date);
  Label8.Caption:= FNevnap.Nevnap_dat(date+1);
  if FNevnap.Nevnap(EgyebDlg.user_code) then
    FNevnap.ShowModal;

  FNevnap.Free;
  }
  if FInfo_ <> nil then begin
    FInfo_.PageControl1.ActivePageIndex:=0;
    FInfo_.Show;
    end;

  Application.CreateForm(TFTerkep2, FTerkep2);
  Application.CreateForm(TFTerkep, FTerkep);


  if FInfo_ <> nil then begin
    FInfo_.Visible:=True;
    FInfo_.SetFocus;
    end
  else begin
    Activate; // hogy megkapja a f�kuszt
    end;
end;

procedure TFomenuDlg.KilepesClick(Sender: TObject);
begin
	Close;
end;

function TFomenuDlg.Cimiro : string;
begin
//	Result := EgyebDlg.Focim + ' ( '+ SW_VERZIO + ' ) -> '+EgyebDlg.v_evszam;
	Result := EgyebDlg.Focim + ' ( '+ SW_VERZIO + ' )';
end;

procedure TFomenuDlg.SegitsegClick(Sender: TObject);
var
	fn	: string;
   pc1	: array[ 0..256] of char;
   pc2	: array[ 0..256] of char;
   i	: integer;
const
   pcProg	: array[ 0..11] of char = 'index.html'#0;
begin
	// Megnyitjuk a f� HTML oldalt
	for i := 0 to 256 do begin
		pc1[i]	:= #0;
		pc2[i]	:= #0;
	end;
	fn := EgyebDlg.HelpPath+'index.html';
	StrPLCopy(pc1, fn, 256);
	StrPLCopy(pc2, EgyebDlg.HelpPath, 256);
	if FileExists(fn) then begin
		ShellExecute(Application.Handle,'open',pc1,pcProg,pc2, SW_SHOWNORMAL );
   end;
end;

procedure TFomenuDlg.SendOutlookMail1Click(Sender: TObject);
var
   htmlbody: string;
begin
   // htmlbody := InputBox('Lev�l tartalom', 'Body:', '<b>Bold</b>nem bold');
   // SendOutlookHTMLMmail('nagyp69@index.hu', 'cimke', htmlbody, '');  // nincs mell�klet
end;

procedure TFomenuDlg.SetStyleByIndex(Index: Integer);
begin
  Try
   TStyleManager.SetStyle(ListBoxLoadedStyles.Items[Index]);
  Except

  End;
end;

procedure TFomenuDlg.UzenetEllenorzes;
begin
	if Query_Run(QueryKoz, 'SELECT * FROM UZENET WHERE UZ_STATU = 0 AND UZ_KINEK = '''+EgyebDlg.user_code+''' ' ) then begin
   	if QueryKoz.RecordCount > 0 then begin
           if NoticeKi('�nnek �j �zenete �rkezett. �tveszi? ', NOT_QUESTION) = 0 then begin
				Uzenetek_kezeleseClick(Self);
           end else begin
               // Eltessz�k talonba
               Query_Run(QueryKoz, 'UPDATE UZENET SET UZ_STATU = 2 WHERE UZ_UZKOD = '+IntToStr(StrToIntDef(QueryKoz.FieldByName('UZ_UZKOD').AsString, 0)));
           end;
       end;
   end;
end;

procedure TFomenuDlg.Szolgltatsszinten1Click(Sender: TObject);
var
   Fomezo, FoSQL, Fejlec: string;
   SzuromezoTomb, SzummamezoTomb: TStringArray;
begin
    Application.CreateForm(TUniStatFmDlg, UniStatFmDlg);
    // ----- param�terez�s ------- //
    // UniStatFmDlg.AdatlapOsztaly := 'TMunkalapReszletDlg';
    UniStatFmDlg.Caption:='Telefonsz�mla statisztika term�k/szolg�ltat�s szinten';
    UniStatFmDlg.Tag:=603;
    SzuromezoTomb:= TStringArray.Create('TELSZAM', '', '0',
                                        'IDOSZAK', '', '0',
                                        'TERMEK', '', '0');
    SzummamezoTomb:= TStringArray.Create('MU_NETTO');
    FoSQL:= 'select * from ( '+
            ' select convert(varchar, ROW_NUMBER() OVER(ORDER BY TELSZAM, IDOSZAK, TERMEK)) AS Sorsz�m, '+
            '* from TELSZAMLASTAT '+
            ') a where 1=1 ';  // a k�s�bbi sz�r�shez kell
    Fomezo:= 'Sorsz�m';
    Fejlec:= 'Telefonsz�mla statisztika term�k/szolg�ltat�s szinten';
    UniStatFmDlg.AdatTolt(Fomezo, FoSQL, Fejlec, SzuromezoTomb, SzummamezoTomb);
    // --------------------------- //
    Screen.Cursor	:= crDefault;
    UniStatFmDlg.ShowModal;
    UniStatFmDlg.Free;
end;

procedure TFomenuDlg.SzotarClick(Sender: TObject);
begin
	if Szotar.Visible then begin
     	Screen.Cursor	:= crHourGlass;
     	Application.CreateForm(TBazisUjDlg, BazisUjDlg);
     	Screen.Cursor	:= crDefault;
		BazisUjDlg.ShowModal;
		if BazisUjDlg.Voltmodositas then begin
			// A b�zissz�t�r adatainak �jraolvas�sa
			EgyebDlg.Fill_SZGrid;
			// Ellen�rizz�k a dolgoz�khoz tartoz� mez�k megl�t�t
			DolgozoMezoEllenor;
		end;
		BazisUjDlg.Free;
		EgyebDlg.felrakas_str	:= EgyebDlg.Read_SZGrid( '999', '712' );
		EgyebDlg.lerakas_str  	:= EgyebDlg.Read_SZGrid( '999', '713' );
	end;
end;

procedure TFomenuDlg.FelhasznalokClick(Sender: TObject);
begin
	if Felhasznalok.Visible then begin
     Screen.Cursor	:= crHourGlass;
     Application.CreateForm(TFelFormDlg, FelFormDlg);
     Screen.Cursor	:= crDefault;
     FelFormDlg.ShowModal;
     FelFormDlg.Free;
  end;
end;

procedure TFomenuDlg.JelszavakClick(Sender: TObject);
begin
  if EgyebDlg.user_code <> '00' then begin
		Screen.Cursor	:= crHourGlass;
		Application.CreateForm(TJelszoCsereDlg, JelszoCsereDlg);
		Screen.Cursor	:= crDefault;
		JelszoCsereDlg.ShowModal;
		JelszoCsereDlg.Free;
  end;
end;

procedure TFomenuDlg.jkdkrse1Click(Sender: TObject);
begin
  if jkdkrse1.Visible then begin
     Screen.Cursor	:= crHourGlass;
     Application.CreateForm(TMegrendeloKodkeresDlg, MegrendeloKodkeresDlg);
     Screen.Cursor	:= crDefault;
     MegrendeloKodkeresDlg.ShowModal;
     MegrendeloKodkeresDlg.Free;
  end;
end;

procedure TFomenuDlg.JogokClick(Sender: TObject);
begin
	if Jogok.Visible then begin
     Screen.Cursor	:= crHourGlass;
     Application.CreateForm(TJogFormDlg, JogFormDlg);
     Screen.Cursor	:= crDefault;
     JogFormDlg.ShowModal;
     JogFormDlg.Free;
  end;
end;

procedure TFomenuDlg.NevjegyClick(Sender: TObject);
begin
	Screen.Cursor	:= crHourGlass;
	Application.CreateForm(TAboutDlg, AboutDlg);
	Screen.Cursor	:= crDefault;
  	AboutDlg.ShowModal;
	AboutDlg.Free;
end;

procedure TFomenuDlg.TermekekClick(Sender: TObject);
begin
	if Termekek.Visible then begin
     Screen.Cursor	:= crHourGlass;
     Application.CreateForm(TTermekFmDlg, TermekFmDlg );
     Screen.Cursor	:= crDefault;
     if TermekFmDlg.vanrek then begin
        TermekFmDlg.ShowModal;
     end;
     TermekFmDlg.Free;
  end;
end;

procedure TFomenuDlg.FormCanResize(Sender: TObject; var NewWidth,
  NewHeight: Integer; var Resize: Boolean);
begin
	if ( ( NewWidth < 650 ) or ( NewHeight < 400 ) ) then begin
  	    Resize := false;
   end;
end;

procedure TFomenuDlg.DolgozokClick(Sender: TObject);
begin
	if Dolgozok.Visible then begin
	  Screen.Cursor	:= crHourGlass;
	  Application.CreateForm(TDolgozoUjFmDlg, DolgozoUjFmDlg);
	  Screen.Cursor	:= crDefault;
	  DolgozoUjFmDlg.ShowModal;
	  DolgozoUjFmDlg.Free;
  end;
end;

procedure TFomenuDlg.Dolgoztabl1Click(Sender: TObject);
begin
  Application.CreateForm(TDolgozoTabloDlg, DolgozoTabloDlg);
	DolgozoTabloDlg.ShowModal;
	DolgozoTabloDlg.Free;
end;

procedure TFomenuDlg.VevokClick(Sender: TObject);
begin
	if Vevok.Visible then begin
     Screen.Cursor	:= crHourGlass;
     Application.CreateForm(TVevoUjFmDlg, VevoUjFmDlg);
	  Screen.Cursor	:= crDefault;
	  VevoUjFmDlg.ShowModal;
     VevoUjFmDlg.Free;
  end;
end;

procedure TFomenuDlg.TavolletidijClick(Sender: TObject);
begin
	if Tavolletidij.Visible then begin
     Screen.Cursor	:= crHourGlass;
     Application.CreateForm(TTavolletidijDlg, TavolletidijDlg);
     Screen.Cursor	:= crDefault;
     TavolletidijDlg.ShowModal;
     TavolletidijDlg.Free;
     end;
end;

{
procedure TFomenuDlg.AlvallakozokClick(Sender: TObject);
begin
	if (Alvallakozok.Visible)and(EgyebDlg.v_evszam<'2013') then begin
	  Screen.Cursor	:= crHourGlass;
	  Application.CreateForm(TAlvallalUjFmDlg, AlvallalUjFmDlg);
	  Screen.Cursor	:= crDefault;
	  AlvallalUjFmDlg.ShowModal;
	  AlvallalUjFmDlg.Free;
  end;
end;
 }

procedure TFomenuDlg.GepkocsikClick(Sender: TObject);
begin
	if Gepkocsik.Visible then begin
	  Screen.Cursor	:= crHourGlass;
	  Application.CreateForm(TGepkocsiUjFmDlg, GepkocsiUjFmDlg);
	  Screen.Cursor	:= crDefault;
	  GepkocsiUjFmDlg.ShowModal;
	  GepkocsiUjFmDlg.Free;
  end;
end;

procedure TFomenuDlg.ArfolyamokClick(Sender: TObject);
begin
	if Arfolyamok.Visible then begin
     Screen.Cursor	:= crHourGlass;
     Application.CreateForm(TArfolyamFmDlg, ArfolyamFmDlg);
     Screen.Cursor	:= crDefault;
     ArfolyamFmDlg.ShowModal;
     ArfolyamFmDlg.Free;
  end;
end;

procedure TFomenuDlg.ShellekClick(Sender: TObject);
begin
	if Shellek.Visible then begin
	  Screen.Cursor	:= crHourGlass;
	  Application.CreateForm(TShellFmDlg, ShellFmDlg);
	  Screen.Cursor	:= crDefault;
	  ShellFmDlg.ShowModal;
	  ShellFmDlg.Free;
  end;
end;

procedure TFomenuDlg.KoltsegekClick(Sender: TObject);
begin
	if Koltsegek.Visible then begin
     Screen.Cursor	:= crHourGlass;
     Application.CreateForm(TKoltsegFmDlg, KoltsegFmDlg);
     Screen.Cursor	:= crDefault;
     KoltsegFmDlg.ShowModal;
     KoltsegFmDlg.Free;
  end;
end;

procedure TFomenuDlg.JaratokClick(Sender: TObject);
begin
	if Jaratok.Visible then begin
     Screen.Cursor	:= crHourGlass;
     Application.CreateForm(TJaratFormDlg, JaratFormDlg);
     Screen.Cursor	:= crDefault;
     JaratFormDlg.ShowModal;
     JaratFormDlg.Free;
     JaratFormDlg:=nil;
  end;
end;

procedure TFomenuDlg.TelefonokClick(Sender: TObject);
begin
	if Telefonok.Visible then begin
     Screen.Cursor	:= crHourGlass;
     Application.CreateForm(TTelefonFormDlg, TelefonFormDlg);
     Screen.Cursor	:= crDefault;
     TelefonFormDlg.ShowModal;
     TelefonFormDlg.Free;
  end;
end;

procedure TFomenuDlg.SulyadatokClick(Sender: TObject);
begin
	if Sulyadatok.Visible then begin
     Screen.Cursor	:= crHourGlass;
     Application.CreateForm(TSulyokFmDlg, SulyokFmDlg);
     Screen.Cursor	:= crDefault;
     SulyokFmDlg.ShowModal;
     SulyokFmDlg.Free;
  end;
end;

procedure TFomenuDlg.TerhelesekClick(Sender: TObject);
begin
	if Terhelesek.Visible then begin
     Screen.Cursor	:= crHourGlass;
     Application.CreateForm(TTerhelesFmDlg, TerhelesFmDlg);
     Screen.Cursor	:= crDefault;
     TerhelesFmDlg.ShowModal;
     TerhelesFmDlg.Free;
  end;
end;

procedure TFomenuDlg.SzamlazasClick(Sender: TObject);
begin
	if Szamlazas.Visible then begin
     Screen.Cursor	:= crHourGlass;
     Application.CreateForm(TSzamlaFormDlg, SzamlaFormDlg);
     Screen.Cursor	:= crDefault;
     SzamlaFormDlg.ShowModal;
     SzamlaFormDlg.Free;
  end;
end;

procedure TFomenuDlg.Szervizmunkalapok1Click(Sender: TObject);
var
   Fomezo, FoSQL, Fejlec: string;
   SzuromezoTomb, SzummamezoTomb: TStringArray;

begin
  if Szervizmunkalapok1.Visible then begin
      Application.CreateForm(TUniStatFmDlg, UniStatFmDlg);
      // ----- param�terez�s ------- //
      UniStatFmDlg.AdatlapOsztaly := 'TMunkalapReszletDlg';
      UniStatFmDlg.Caption:='Szerviz munkalap lista';
      UniStatFmDlg.Tag:=580;
      SzuromezoTomb:= TStringArray.Create('MU_MSZAM', 'SZERVIZMUNKALAP', '0',
                                          'MU_RENSZ', 'SZERVIZMUNKALAP', '0',
                                          'GK_GKKAT', 'GEPKOCSI', '0');
      SzummamezoTomb:= TStringArray.Create('MU_NETTO');
      FoSQL:='select MU_MSZAM, MU_RENSZ, GK_GKKAT, MU_DATUM, MU_IDO, MU_NETTO, MU_IGENY, MU_FENNM, MU_MEGJE from ( '+
          'SELECT MU_MSZAM, MU_RENSZ, GK_GKKAT, MU_DATUM, MU_IDO, convert(int,MU_NETTO) MU_NETTO, MU_IGENY, MU_FENNM, MU_MEGJE '+
          'from SZERVIZMUNKALAP '+
          '  LEFT OUTER JOIN GEPKOCSI ON MU_RENSZ=GK_RESZ '+
          ' ) a where 1=1 ';  // a k�s�bbi sz�r�shez kell
      Fomezo:= 'MU_MSZAM';
      Fejlec:= 'Szerviz munkalapok';
      UniStatFmDlg.AdatTolt(Fomezo, FoSQL, Fejlec, SzuromezoTomb, SzummamezoTomb);
      // --------------------------- //
      Screen.Cursor	:= crDefault;
      UniStatFmDlg.ShowModal;
      UniStatFmDlg.Free;
  end; // if visible
end;

procedure TFomenuDlg.OsszszamlazasClick(Sender: TObject);
begin
	if Osszszamlazas.Visible then begin
     Screen.Cursor	:= crHourGlass;
     Application.CreateForm(TOSzamlaFormDlg, OSzamlaFormDlg);
     Screen.Cursor	:= crDefault;
     OSzamlaFormDlg.ShowModal;
     OSzamlaFormDlg.Free;
  end;
end;

procedure TFomenuDlg.Osztrkrakodsokjelentse1Click(Sender: TObject);
const
    ThisRightTag = 601;
var
   Fomezo, FoSQL, Fejlec: string;
   SzuromezoTomb, SzummamezoTomb: TStringArray;
   RightTag: integer;
   LehetModositani: boolean;
begin
    Application.CreateForm(TUniStatFmDlg, UniStatFmDlg);
    // ----- param�terez�s ------- //
    UniStatFmDlg.AdatlapOsztaly := 'TOsztrakRakoDlg';
    UniStatFmDlg.Caption:='Osztr�k rakod�sok list�ja';
    UniStatFmDlg.Tag:=ThisRightTag;
    SzuromezoTomb:= TStringArray.Create('Megrendel�', '', '0',
                                        'Rak�_V�ros', '', '0',
                                        'Sof�r', '', '0',
                                        '�rkez�s', '', '0',
                                        'T�voz�s', '', '0',
                                        'Dokumentum', '', '0',
                                        'M�dos�t�', '', '0');
    SzummamezoTomb:= TStringArray.Create('Id�tartam');
    FoSQL:='select * from ( '+
        'select V_NEV Megrendel�, V_IRSZ Megr_Irsz, V_VAROS Megr_V�ros, V_CIM Megr_C�m, V_TEL Megr_Telefon, VE_EMAIL Megr_Email, '+
        ' DO_NAME Sof�r, '+
        ' AT_IRSZ Rak�_Irsz, AT_VAROS Rak�_V�ros, AT_CIM Rak�_C�m, AT_RAKDAT Rakod�s, AT_RAKIDO Rakod�s_id�, AT_ERKDAT �rkez�s, AT_ERKIDO �rkez�s_id�, '+
        ' AT_TAVDAT T�voz�s, AT_TAVIDO T�voz�s_id�, AT_IDOTART Id�tartam, isnull(JE_FENEV,'''') M�dos�t�, AT_LINK Dokumentum, AT_MSID '+
        ' from ATJELENT '+
        ' join VEVO on AT_VKOD = V_KOD '+
        ' join MEGSEGED on AT_MSID = MS_ID '+
        ' join DOLGOZO on MS_DOKOD = DO_KOD '+
        ' left outer join JELSZO on AT_ELLENOR = JE_FEKOD '+
        ' ) a where 1=1 ';  // a k�s�bbi sz�r�shez kell
    Fomezo:= 'AT_MSID';
    Fejlec:= 'Osztr�k rakod�sok list�ja';
    // UniStatFmDlg.StatOsztaly := '';  // a sor sz�nez�shez
    UniStatFmDlg.AdatTolt(Fomezo, FoSQL, Fejlec, SzuromezoTomb, SzummamezoTomb);
    RightTag:= GetRightTag(ThisRightTag);
    LehetModositani:= (RightTag = RG_MODIFY) or (RightTag = RG_ALLRIGHT) or EgyebDlg.user_super;
    UniStatFmDlg.ButtonMod.Enabled := LehetModositani;
    // UniStatFmDlg.ButtonUj.Enabled := LehetModositani;
    UniStatFmDlg.ButtonUj.Enabled := False;  // ne lehessen �jat felvinni
    // --------------------------- //
    Screen.Cursor	:= crDefault;
    UniStatFmDlg.ShowModal;
    UniStatFmDlg.Free;
end;

procedure TFomenuDlg.CegadatokClick(Sender: TObject);
begin
	if Cegadatok.Visible then begin
     Screen.Cursor	:= crHourGlass;
     Application.CreateForm(TCegadatFmDlg, CegadatFmDlg);
     Screen.Cursor	:= crDefault;
     CegadatFmDlg.ShowModal;
     CegadatFmDlg.Free;
  end;
end;

procedure TFomenuDlg.Cgek1Click(Sender: TObject);
begin
    Screen.Cursor	:= crHourGlass;
	  Application.CreateForm(TRESTAPIGridDlg, RESTAPIGridDlg);
    RESTAPIGridDlg.Load_domains;
	  Screen.Cursor	:= crDefault;
	  RESTAPIGridDlg.ShowModal;
	  RESTAPIGridDlg.Free;
end;

procedure TFomenuDlg.ForgalmikiClick(Sender: TObject);
begin
	if Forgalmiki.Visible then begin
	  Screen.Cursor	:= crHourGlass;
	  Application.CreateForm(TForgalomDlg, ForgalomDlg);
	  Screen.Cursor	:= crDefault;
	  ForgalomDlg.ShowModal;
	  ForgalomDlg.Free;
  end;
end;

procedure TFomenuDlg.MenetlevelClick(Sender: TObject);
begin
	if Menetlevel.Visible then begin
     Screen.Cursor	:= crHourGlass;
     Application.CreateForm(TMenetlevDlg, MenetlevDlg);
     Screen.Cursor	:= crDefault;
     MenetlevDlg.ShowModal;
     MenetlevDlg.Free;
  end;
end;

procedure TFomenuDlg.MEZOKakzsbl1Click(Sender: TObject);
begin
     Application.CreateForm(TProbaDlg, ProbaDlg);
     ProbaDlg.ShowModal;
     ProbaDlg.Free;
end;

procedure TFomenuDlg.Mindenmegbzsaljrattal1Click(Sender: TObject);
begin
  EKAER_napi_riport(mind_aljarattal);
end;

procedure TFomenuDlg.KoltsegkiClick(Sender: TObject);
begin
	if Koltsegki.Visible then begin
     Screen.Cursor	:= crHourGlass;
     Application.CreateForm(TKoltnyilDlg, KoltnyilDlg);
     Screen.Cursor	:= crDefault;
     KoltnyilDlg.ShowModal;
     KoltnyilDlg.Free;
  end;
end;

procedure TFomenuDlg.FizetesikiClick(Sender: TObject);
begin
	if Fizetesiki.Visible then begin
     Screen.Cursor	:= crHourGlass;
     Application.CreateForm(TFiznyilDlg, FiznyilDlg);
     Screen.Cursor	:= crDefault;
     FiznyilDlg.ShowModal;
     FiznyilDlg.Free;
  end;
end;

procedure TFomenuDlg.Fizetsijegyzkektovbbtsa1Click(Sender: TObject);
begin
 Application.CreateForm(TFizetesiJegyzekLevelDlg, FizetesiJegyzekLevelDlg);
 try
    FizetesiJegyzekLevelDlg.ShowModal;
 finally
   FizetesiJegyzekLevelDlg.Free;
   end;  // try-finally
end;

procedure TFomenuDlg.Fizetsimorllista1Click(Sender: TObject);
var
   Fomezo, FoSQL, Fejlec: string;
   SzuromezoTomb, SzummamezoTomb: TStringArray;
begin
  if Fizetsimorllista1.Visible then begin
    Application.CreateForm(TIdoszakbeDlg, IdoszakbeDlg);
    try
  	  IdoszakbeDlg.ShowModal;
    	if (IdoszakbeDlg.ret_datumtol <> '') and (IdoszakbeDlg.ret_datumig <> '') then begin
    	  Screen.Cursor	:= crHourGlass;
    	  Application.CreateForm(TUniStatFmDlg, UniStatFmDlg);
        // ----- param�terez�s ------- //
        UniStatFmDlg.Caption:='Fizet�si mor�l lista';
        UniStatFmDlg.Tag:=576;
        SzuromezoTomb:= TStringArray.Create('SA_VEVOKOD', 'FIZETESIMORAL', '0',
                                            'SA_VEVONEV', 'FIZETESIMORAL', '0');
        SzummamezoTomb:= TStringArray.Create('');
        FoSQL:='select sa_vevokod, sa_vevonev, atlagos_kiegyenlites, atlagos_keses from ( '+
            'select sa_vevokod, sa_vevonev, avg(sa_fnap) atlagos_kiegyenlites, '+
            ' avg(sa_fnap-sa_hnap) atlagos_keses '+
            ' from KIFIZETETTSZAMLA '+
            ' where sa_kidat between '''+IdoszakbeDlg.ret_datumtol+''' and '''+IdoszakbeDlg.ret_datumig+''''+
            ' group by sa_vevokod,sa_vevonev '+
            ' union '+
            ' select '''' sa_vevokod, ''__�sszes partner__'' sa_vevonev, avg(sa_fnap) atlagos_kiegyenlites, '+
            ' avg(sa_fnap-sa_hnap) atlagos_keses '+
            ' from KIFIZETETTSZAMLA '+
            ' where sa_kidat between '''+IdoszakbeDlg.ret_datumtol+''' and '''+IdoszakbeDlg.ret_datumig+''''+
            ' ) a where 1=1 ';  // a k�s�bbi sz�r�shez kell
        Fomezo:= 'SA_VEVOKOD';
        Fejlec:= 'Fizet�si mor�l';
        UniStatFmDlg.AdatTolt(Fomezo, FoSQL, Fejlec, SzuromezoTomb, SzummamezoTomb);
        // --------------------------- //
    	  Screen.Cursor	:= crDefault;
    	  UniStatFmDlg.ShowModal;
    	  UniStatFmDlg.Free;
        end;  // if j� a k�t d�tum
      finally
        if IdoszakbeDlg<>nil then IdoszakbeDlg.Release;
        end;  // try-finally
  end; // if visible
end;

procedure TFomenuDlg.Flottahelyzet1Click(Sender: TObject);
begin
  Application.CreateForm(TFlottaHelyzetDlg, FlottaHelyzetDlg);
  FlottaHelyzetDlg.Left:=(Screen.Width-FlottaHelyzetDlg.Width)  div 2;
  FlottaHelyzetDlg.Top:=(Screen.Height-FlottaHelyzetDlg.Height) div 2;
	FlottaHelyzetDlg.ShowModal;
  FlottaHelyzetDlg.Free;
end;

procedure TFomenuDlg.Nagyautaljrattal1Click(Sender: TObject);
begin
  EKAER_napi_riport(nagyauto_aljarattal);
end;

procedure TFomenuDlg.NapidijClick(Sender: TObject);
begin
	if Napidij.Visible then begin
     Screen.Cursor	:= crHourGlass;
     Application.CreateForm(TNapidijDlg, NapidijDlg);
     Screen.Cursor	:= crDefault;
     NapidijDlg.ShowModal;
     NapidijDlg.Free;
  end;
end;

procedure TFomenuDlg.UtnyilvantartasClick(Sender: TObject);
begin
	if Utnyilvantartas.Visible then begin
     Screen.Cursor	:= crHourGlass;
     Application.CreateForm(TUtnyilDlg, UtnyilDlg);
     Screen.Cursor	:= crDefault;
     UtnyilDlg.ShowModal;
     UtnyilDlg.Free;
  end;
end;

procedure TFomenuDlg.UzemanyagkiClick(Sender: TObject);
begin
	if Uzemanyagki.Visible then begin
     Screen.Cursor	:= crHourGlass;
     Application.CreateForm(TUzemaDlg, UzemaDlg);
     Screen.Cursor	:= crDefault;
     UzemaDlg.ShowModal;
     UzemaDlg.Free;
  end;
end;

procedure TFomenuDlg.KoltsegosszesClick(Sender: TObject);
begin
	if Koltsegosszes.Visible then begin
     Screen.Cursor	:= crHourGlass;
     Application.CreateForm(TOssznyilDlg, OssznyilDlg);
     Screen.Cursor	:= crDefault;
     OssznyilDlg.ShowModal;
     OssznyilDlg.Free;
  end;
end;

procedure TFomenuDlg.HianyzojaratokClick(Sender: TObject);
var
	fileazon 	: TextFile;
	filename		: string;
  maxim			: integer;
  aktkod		: integer;
  sorszam		: integer;
begin
	if Hianyzojaratok.Visible then begin
     {Hi�nyz� j�ratok list�ja}
     {$I-}
     filename		:= EgyebDlg.TempList + 'hianyjar.lst';
     AssignFile(fileazon, filename);
     Rewrite(fileazon);
     {$I+}
     if IOResult <> 0 then begin
        Exit;
     end;
     Screen.Cursor	:= crHourGlass;
     Query_Run(EgyebDlg.QueryAlap, 'SELECT * FROM JARAT ORDER BY JA_ALKOD',true);
     EgyebDlg.QueryAlap.First;
     aktkod	:= StrToIntDef(EgyebDlg.QueryAlap.Fieldbyname('JA_ALKOD').AsString,0);
     sorszam	:= 0;
     maxim		:= 0;
     while not EgyebDlg.QueryAlap.EOF do begin
        maxim	:= StrToIntDef(EgyebDlg.QueryAlap.Fieldbyname('JA_ALKOD').AsString,0);
        if aktkod < StrToIntDef(EgyebDlg.QueryAlap.Fieldbyname('JA_ALKOD').AsString,0) then begin
           {Nincs ilyen j�ratsz�m}
           Write(fileazon, Format('%3d, ',[aktkod]));
           Inc(sorszam);
           if sorszam mod 15 = 0 then begin
              Writeln(fileazon);
           end;
           Inc(aktkod);
        end else begin
           EgyebDlg.QueryAlap.Next;
           if aktkod = maxim then begin
              Inc(aktkod);
           end;
        end;
     end;
     EgyebDlg.QueryAlap.Close;
     CloseFile(fileazon);
     Screen.Cursor	:= crDefault;
     if sorszam = 0 then begin
        NoticeKi('Nincs hi�nyz� j�ratsz�m');
     end else begin
        EgyebDlg.Vanvonal	:= false;
        EgyebDlg.FileMutato(
           filename,
           'Hi�nyz� j�ratsz�mok list�z�sa',
           'Maxim�lis j�ratsz�m : '+IntToStr(maxim),
           ' ',
           'J�ratsz�mok',
           '--------------------------------------------------------------------------------------------------------------',
           0);
     end;
  end;
end;

procedure TFomenuDlg.HianyzomenetlevelekClick(Sender: TObject);
var
	fileazon 	: TextFile;
	filename		: string;
  maxim			: integer;
  aktkod		: integer;
  sorszam		: integer;
begin
	if Hianyzomenetlevelek.Visible then begin
     {Hi�nyz� menetlevelek list�ja}
     {$I-}
     filename		:= EgyebDlg.TempList + 'hianymen.lst';
     AssignFile(fileazon, filename);
     Rewrite(fileazon);
     {$I+}
     if IOResult <> 0 then begin
        Exit;
     end;
     Screen.Cursor	:= crHourGlass;
     Query_Run(EgyebDlg.QueryAlap, 'SELECT * FROM JARAT ORDER BY JA_MENSZ',true);
     EgyebDlg.QueryAlap.First;
     aktkod	:= StrToIntDef(EgyebDlg.QueryAlap.Fieldbyname('JA_MENSZ').AsString,0);
     sorszam	:= 0;
     maxim		:= 0;
     while not EgyebDlg.QueryAlap.EOF do begin
        maxim	:= StrToIntDef(EgyebDlg.QueryAlap.Fieldbyname('JA_MENSZ').AsString,0);
        if aktkod < StrToIntDef(EgyebDlg.QueryAlap.Fieldbyname('JA_MENSZ').AsString,0) then begin
           {Nincs ilyen j�ratsz�m}
           Write(fileazon, Format('%3d, ',[aktkod]));
           Inc(sorszam);
           if sorszam mod 15 = 0 then begin
              Writeln(fileazon);
           end;
           Inc(aktkod);
        end else begin
           EgyebDlg.QueryAlap.Next;
           if aktkod = maxim then begin
              Inc(aktkod);
           end;
        end;
     end;
     CloseFile(fileazon);
     Screen.Cursor	:= crDefault;
     if sorszam = 0 then begin
        NoticeKi('Nincs hi�nyz� menetlev�l');
     end else begin
        EgyebDlg.Vanvonal	:= false;
        EgyebDlg.FileMutato(
           filename,
           'Hi�nyz� menetlevelek list�z�sa',
           'Maxim�lis menetlev�lsz�m : '+IntToStr(maxim),
           ' ',
           'Menetlevelek',
           '--------------------------------------------------------------------------------------------------------------',
           0);
     end;
  end;
end;

procedure TFomenuDlg.AdatellenorzesClick(Sender: TObject);
begin
	if Adatellenorzes.Visible then begin
     // Adatellen�rz�sek v�grehajt�sa
     Application.CreateForm(TAdatellDlg, AdatellDlg);
     AdatellDlg.ShowModal;
     AdatellDlg.Free;
  end;
end;

procedure TFomenuDlg.Elfizetsszinten1Click(Sender: TObject);
var
   Fomezo, FoSQL, Fejlec: string;
   SzuromezoTomb, SzummamezoTomb: TStringArray;
begin
    Application.CreateForm(TUniStatFmDlg, UniStatFmDlg);
    // ----- param�terez�s ------- //
    // UniStatFmDlg.AdatlapOsztaly := 'TMunkalapReszletDlg';
    UniStatFmDlg.Caption:='Telefonsz�mla statisztika el�fizet�s szinten';
    UniStatFmDlg.Tag:=604;
    SzuromezoTomb:= TStringArray.Create('TELSZAM', '', '0',
                                        'IDOSZAK', '', '0');
    SzummamezoTomb:= TStringArray.Create('MU_NETTO');
 FoSQL:= 'select * from ( '+
            ' select convert(varchar, ROW_NUMBER() OVER(ORDER BY TELSZAM, IDOSZAK)) AS Sorsz�m, '+
            '* from TELSZAMLASTAT2 '+
            ') a where 1=1 ';  // a k�s�bbi sz�r�shez kell
    Fomezo:= 'Sorsz�m';
    Fejlec:= 'Telefonsz�mla statisztika term�k/szolg�ltat�s szinten';
    UniStatFmDlg.AdatTolt(Fomezo, FoSQL, Fejlec, SzuromezoTomb, SzummamezoTomb);
    // --------------------------- //
    Screen.Cursor	:= crDefault;
    UniStatFmDlg.ShowModal;
    UniStatFmDlg.Free;
end;

procedure TFomenuDlg.elszmlaHINFOupdate1Click(Sender: TObject);
var
   S, HINFO, TELSZAM: string;
   darab: integer;
   // ElofizetesInfo: TElofizetesInfo;
begin
   Screen.Cursor	:= crHourGlass;
   S:= 'select distinct TZ_TELSZAM from TELSZAMLASZAMOK where isnull(TZ_DOKODLIST, '''') = '''' and TZ_HINFO=''''  ';
   Query_Run(EgyebDlg.Querykoz2, S);
   darab:= 0;
   with EgyebDlg.Querykoz2 do begin
     while not EOF do begin
       TELSZAM:= FieldByName('TZ_TELSZAM').AsString;
       HINFO:= EgyebDlg.GetElofizetesInfo(TELSZAM).megjegyzes;
       if HINFO <> '' then begin
          Query_Update (QueryAlap2, 'TELSZAMLASZAMOK', [
           		'TZ_HINFO',    ''''+HINFO+''''
  		          ], ' WHERE TZ_TELSZAM = '''+TELSZAM+'''' );
          inc(darab);
          end;
       Next;
       end;  // while
      Close;
      end;  // with
   Screen.Cursor	:= crDefault;
   NoticeKi('HINFO update k�sz, �j HINFO: '+IntToStr(darab)+' db.');
end;

procedure TFomenuDlg.Encode1Click(Sender: TObject);
var
  S: string;
begin
   S:= InputBox('Bemenet', 'Bemenet: ', '');
   S:= EncodeString(S);
   Clipboard.Clear;
   Clipboard.AsText:= S;
end;

procedure TFomenuDlg.ErvenyessegekClick(Sender: TObject);
var
  elonap   	: integer;
  elokm		: integer;
  napstr		: string;
  kmstr		: string;
begin
	// A hat�r�rt�kek bek�r�se
   elonap	        := StrToIntDef(EgyebDlg.Read_SZGrid('999', '641'),0); {A napok sz�ma}
   elokm 	        := StrToIntDef(EgyebDlg.Read_SZGrid('999', '651'),0); {A km-ek sz�ma}
   napstr 	        := InputBox('Napok sz�ma', 'Az ellen�rz�tt napok sz�ma : ', intToStr(elonap));
   if FInfo_ = nil then  // l�trehozzuk ha m�g nem l�tezik
      Application.CreateForm(TFInfo_, FInfo_);
   FInfo_.elo_nap	:= StrToIntDef(napstr,0);
   kmstr 	        := InputBox('Kilom�ter', 'Az ellen�rz�tt km nagys�ga : ', intToStr(elokm));
   FInfo_.elo_km	:= StrToIntDef(kmstr,0);
	// A kezdeti �rv�nyess�gek ellen�rz�se
//  	EgyebDlg.ErvEllenor(elonap,elokm);
   FInfo_.PageControl1Change(Self);
   if not FInfo_.Showing then begin
       FInfo_.Show;
   end;
end;

procedure TFomenuDlg.Esemnyekttekintse1Click(Sender: TObject);
const
   ThisRightTag = 583;
var
   Fomezo, FoSQL, Fejlec: string;
   SzuromezoTomb, SzummamezoTomb: TStringArray;
   RightTag: integer;
   LehetModositani: boolean;
begin
  if Esemnyekttekintse1.Visible then begin
      Application.CreateForm(TUniStatFmDlg, UniStatFmDlg);
      // ----- param�terez�s ------- //
      UniStatFmDlg.AdatlapOsztaly := 'TRendkivuliEsemenybeDlg';
      UniStatFmDlg.Caption:='Rendk�v�li esem�nyek list�ja';
      UniStatFmDlg.Tag:=ThisRightTag;
      SzuromezoTomb:= TStringArray.Create('JE_FENEV', '', '0',
                                          'RK_RENDSZAM', '', '0',
                                          'RK_DATUM', '', '0',
                                          'STATUSZ', '', '0',
                                          'INTEZO', '', '0',
                                          'TERULET', '', '0');
      SzummamezoTomb:= TStringArray.Create('KOLTSEG');
      FoSQL:='select RK_ID, JE_FENEV, RK_RENDSZAM, TERULET, STATUSZ, RK_DATUM, RK_IDOPT, RK_JELENTI, RK_JELENTC, KOLTSEG, '+
          ' INTEZO, RK_ESEMENY, RK_STATUSZID from ( '+
          'select RK_ID, JE_FENEV, RK_RENDSZAM, sz1.SZ_MENEV TERULET, sz2.SZ_MENEV STATUSZ, RK_DATUM, RK_IDOPT, RK_JELENTI, RK_JELENTC, '+
          ' RK_ESEMENY, RK_STATUSZID, RK_KOLTSEG KOLTSEG, isnull(DO_NAME,'''') INTEZO '+
          ' from SZOTAR sz1, SZOTAR sz2, JELSZO, '+
          ' RENDKIVULI_ESEMENY left outer join DOLGOZO on RK_INTEZOKOD=DO_KOD'+
          ' where RK_FEKOD = JE_FEKOD and sz1.SZ_FOKOD=''148'' and RK_TERULETKOD=sz1.SZ_ALKOD '+
          ' and sz2.SZ_FOKOD=''379'' and RK_STATUSZID=sz2.SZ_ALKOD '+
          ' ) a where 1=1 ';  // a k�s�bbi sz�r�shez kell
      Fomezo:= 'RK_ID';
      Fejlec:= 'Rendk�v�li esem�nyek';
      UniStatFmDlg.StatOsztaly := 'Rendkivuli_esemenyek';  // a sor sz�nez�shez
      UniStatFmDlg.AdatTolt(Fomezo, FoSQL, Fejlec, SzuromezoTomb, SzummamezoTomb);
      RightTag:= GetRightTag(ThisRightTag);
      LehetModositani:= (RightTag = RG_MODIFY) or (RightTag = RG_ALLRIGHT) or EgyebDlg.user_super;
      UniStatFmDlg.ButtonMod.Enabled := LehetModositani;
      UniStatFmDlg.ButtonUj.Enabled := LehetModositani;
      // --------------------------- //
      Screen.Cursor	:= crDefault;
      UniStatFmDlg.ShowModal;
      UniStatFmDlg.Free;
  end; // if visible
end;

procedure TFomenuDlg.Esemnyrgztse1Click(Sender: TObject);
begin
if Esemnyrgztse1.Visible then begin
  Application.CreateForm(TRendkivuliEsemenybeDlg, RendkivuliEsemenybeDlg);
  RendkivuliEsemenybeDlg.ShowModal;
  RendkivuliEsemenybeDlg.Free;
  end; // if visible
end;

procedure TFomenuDlg.VCARDexport1Click(Sender: TObject);
var
	F1: TextFile;
  filename, S, Vezeteknev, Keresztnev, Telszam, Email: string;

begin
   filename  := EgyebDlg.TempList + 'fuvaros_dolgozok.vcf';
   AssignFile(F1, filename);
   Rewrite(F1);
   Screen.Cursor	:= crHourGlass;
   S:= 'select DO_NAME, isnull(DO_EMAIL,'''') EMAIL, isnull(TE_TELSZAM, '''') TE_TELSZAM '+
        ' from dolgozo left outer join '+
        ' (select TK_DOKOD, TE_TELSZAM from TELELOFIZETES, TELKESZULEK where TE_TKID=TK_ID '+
        '  union '+
        '  select TE_DOKOD, TE_TELSZAM from TELELOFIZETES) dotel on TK_DOKOD = DO_KOD '+
        ' where DO_ARHIV=0 '+
        ' order by DO_NAME  ';
   Query_Run(EgyebDlg.Querykoz2, S);
   with EgyebDlg.Querykoz2 do begin
     while not EOF do begin
       Vezeteknev:= EgyebDlg.SepFronttoken(' ', FieldByName('DO_NAME').AsString);
       Keresztnev:= EgyebDlg.SepRest(' ', FieldByName('DO_NAME').AsString);
       Telszam:= FieldByName('TE_TELSZAM').AsString;
       Email:= FieldByName('EMAIL').AsString;
       S:= GetVCARD(Vezeteknev, Keresztnev, '', Telszam, Email, '', '', '');
       Writeln(F1, S);
       Next;
       end;  // while
      Close;
      end;  // with
   CloseFile(F1);
   Screen.Cursor	:= crDefault;
   NoticeKi('VCARD export k�sz: '+filename);
end;

procedure TFomenuDlg.VevoExportClick(Sender: TObject);
begin
	// Adatok import�l�sa a k�nyvel�si rendszerbe
	if VevoExport.Visible then begin
     	Screen.Cursor	:= crHourGlass;
     	Application.CreateForm(TVevoExportDlg, VevoExportDlg);
     	Screen.Cursor	:= crDefault;
     	VevoExportDlg.ShowModal;
     	VevoExportDlg.Free;
  	end;
end;

procedure TFomenuDlg.ZarokmClick(Sender: TObject);
begin
	if Zarokm.Visible then begin
     Screen.Cursor	:= crHourGlass;
     Application.CreateForm(TZarokmFmDlg, ZarokmFmDlg);
     Screen.Cursor	:= crDefault;
     ZarokmFmDlg.ShowModal;
     ZarokmFmDlg.Free;
  end;
end;

procedure TFomenuDlg.UzemanyagarClick(Sender: TObject);
begin
	if Uzemanyagar.Visible then begin
     Screen.Cursor	:= crHourGlass;
     Application.CreateForm(TUzemanyagFmDlg, UzemanyagFmDlg);
     Screen.Cursor	:= crDefault;
     UzemanyagFmDlg.ShowModal;
     UzemanyagFmDlg.Free;
  end;
end;

procedure TFomenuDlg.APEHuzemanyagClick(Sender: TObject);
begin
	if APEHuzemanyag.Visible then begin
     Screen.Cursor	:= crHourGlass;
     Application.CreateForm(TApehUzemDlg, ApehUzemDlg);
     Screen.Cursor	:= crDefault;
     ApehUzemDlg.ShowModal;
     ApehUzemDlg.Free;
  end;
end;

procedure TFomenuDlg.zemanyagfogyasztsiadatok1Click(Sender: TObject);
begin
     Application.CreateForm(TFogyasztasDlg, FogyasztasDlg);
     FogyasztasDlg.ShowModal;
     FogyasztasDlg.Free;
end;

procedure TFomenuDlg.zemanyagktllapota1Click(Sender: TObject);
begin
     Application.CreateForm(TKutAllapotDlg, KutAllapotDlg);
     KutAllapotDlg.ShowModal;
     KutAllapotDlg.Release;
end;

procedure TFomenuDlg.zemanyagstatisztikaexport1Click(Sender: TObject);
begin
     Application.CreateForm(TUzemaExportDlg, UzemaExportDlg);
     UzemaExportDlg.ShowModal;
     UzemaExportDlg.Release;
end;

procedure TFomenuDlg.zenetek1Click(Sender: TObject);
begin
 {	if zenetek1.Visible then begin
     if UzenetSzalakDlg = nil then begin  // ha m�g nem l�tezik
       Screen.Cursor	:= crHourGlass;
       Application.CreateForm(TUzenetSzalakDlg, UzenetSzalakDlg);
       Screen.Cursor	:= crDefault;
       end;
     // UzenetSzalakDlg.ShowModal;
     // UzenetSzalakDlg.Free;
     UzenetSzalakDlg.Show;
  end;
  }
   if zenetek1.Visible then begin
       Screen.Cursor	:= crHourGlass;
       Application.CreateForm(TUzenetSzalakDlg, UzenetSzalakDlg);
       Screen.Cursor	:= crDefault;
       UzenetSzalakDlg.ShowModal;
       UzenetSzalakDlg.Free;
       end;

end;

procedure TFomenuDlg.TelElofizetesekClick(Sender: TObject);
begin
	if TelElofizetesek.Visible then begin
     Screen.Cursor	:= crHourGlass;
     Application.CreateForm(TTelElofizetFormDlg, TelElofizetFormDlg);
     Screen.Cursor	:= crDefault;
     TelElofizetFormDlg.ShowModal;
     TelElofizetFormDlg.Free;
  end;
end;

procedure TFomenuDlg.echnikaizenetkldse1Click(Sender: TObject);
begin
  if UzenetTopDlg <> nil then begin
     Screen.Cursor	:= crHourGlass;
     UzenetTopDlg.SetActivePage(UzenetTopDlg.ActivePage);  // friss�ti az aktu�lis lapot
     UzenetTopDlg.Show;
     Screen.Cursor	:= crDefault;
     end;
{ 	if echnikaizenetkldse1.Visible then begin
     Screen.Cursor	:= crHourGlass;
     // Application.CreateForm(TUzenetSzerkesztoDlg, UzenetSzerkesztoDlg);
     // UzenetSzerkesztoDlg.CsoportListaTolt;
     UzenetSzerkesztoDlg.Reset;
     UzenetSzerkesztoDlg.EagleSMSTipus:= technikai;
     Screen.Cursor	:= crDefault;
     UzenetSzerkesztoDlg.ShowModal;
     // UzenetSzerkesztoDlg.Free;
  end;
  }
end;

procedure TFomenuDlg.Egybkontaktok1Click(Sender: TObject);
begin
  Screen.Cursor	:= crHourGlass;
  Application.CreateForm(TEKontaklFmDlg, EKontaklFmDlg);
  Screen.Cursor	:= crDefault;
  EKontaklFmDlg.ShowModal;
  EKontaklFmDlg.Free;
end;

procedure TFomenuDlg.elefonkiegsztk1Click(Sender: TObject);
begin
  Screen.Cursor	:= crHourGlass;
  Application.CreateForm(TTelKiegFormDlg, TelKiegFormDlg);
  Screen.Cursor	:= crDefault;
  TelKiegFormDlg.ShowModal;
  TelKiegFormDlg.Free;
end;


procedure TFomenuDlg.elefonknyv1Click(Sender: TObject);
var
   Fomezo, FoSQL, Fejlec, EvSzama, HonapSzama, NapSzama, S: string;
   SzuromezoTomb, SzummamezoTomb: TStringArray;
begin
   EvSzama:= copy(EgyebDlg.MaiDatum, 1,4);
   HonapSzama:= copy(EgyebDlg.MaiDatum, 6,2);
   NapSzama:= copy(EgyebDlg.MaiDatum, 9,2);
   S:= ' select NEV+case when SZABI<>'''' then '' [''+SZABI+'']'' else '''' end N�v, BEOSZTAS Beoszt�s, BELSOMELLEK Bels�_mell�k, '+
    ' IRODAISZAM Irodai, MOBILSZAM Mobil from '+
    ' (select DO_NAME NEV, isnull(DO_EMAIL,'''') EMAIL, isnull(DO_IRODAITEL,'''') IRODAISZAM, isnull(DO_BELSOMELLEK,'''') BELSOMELLEK, '+
    ' isnull(TE_TELSZAM, '''') MOBILSZAM, DO_BEOSZTAS beosztas, '+
    ' case when isnull(TA_NAP'+NapSzama+', '''')='''' then '''' else ''szabads�gon'' end SZABI,'+
    '		 DOELS, RANK() OVER (PARTITION BY DO_NAME ORDER BY DOELS desc) TELSZAM_SORREND '+
    '         from dolgozo '+
    '         left outer join '+
    '         (select TK_DOKOD, TE_TELSZAM, isnull(TE_DOELS,0) DOELS from TELELOFIZETES, TELKESZULEK where TE_TKID=TK_ID '+
    '          union '+
    '          select TE_DOKOD, TE_TELSZAM, isnull(TE_DOELS,0) DOELS from TELELOFIZETES) dotel on TK_DOKOD = DO_KOD '+
    '         left outer join TAVOLLET on TA_DOKOD = DO_KOD and TA_EV='+EvSzama+' and TA_HONAP='+HonapSzama+' '+
    '         where DO_ARHIV=0 '+
    '       ) x1     '+
    ' where TELSZAM_SORREND = 1 '+  // lehet�leg az els�dleges sz�m ker�lj�n be
    ' union '+
    ' select EK_NEV, EK_BEOSZTAS, '''', EK_IRODAITEL, EK_MOBILTEL from EKontakt where isnull(EK_VALID,'''')=1 ';

    Application.CreateForm(TUniStatFmDlg, UniStatFmDlg);
    // ----- param�terez�s ------- //
    // UniStatFmDlg.AdatlapOsztaly := 'TMunkalapReszletDlg';
    UniStatFmDlg.Caption:='Telefonk�nyv';
    UniStatFmDlg.Tag:=608; // b�r mindenkinek el�rhet�, nem lehet 0, mert a t�bl�zathoz mentett param�terek "�sszevesznek"
    SzuromezoTomb:= TStringArray.Create('N�v', '', '0',
                                        'Beoszt�s', '', '1');
    // SzummamezoTomb:= TStringArray.Create('MU_NETTO');
    FoSQL:= 'select * from ( '+S+' ) a where 1=1 ';  // a k�s�bbi sz�r�shez kell
    Fomezo:= 'N�v';
    Fejlec:= 'Telefonk�nyv';
    UniStatFmDlg.AdatTolt(Fomezo, FoSQL, Fejlec, SzuromezoTomb, SzummamezoTomb);
    // --------------------------- //
    Screen.Cursor	:= crDefault;
    UniStatFmDlg.ShowModal;
    UniStatFmDlg.Free;
end;

procedure TFomenuDlg.elefonnvjegyzkexportbelltsok1Click(Sender: TObject);
var
   Fomezo, FoSQL, Fejlec, S, Ceg1, Ceg2, Ceg3: string;
   SzuromezoTomb, SzummamezoTomb: TStringArray;
begin
   S:= 'select sz1.SZ_MENEV CEG1, sz2.SZ_MENEV CEG2, sz3.SZ_MENEV CEG3 '+
       ' from szotar sz1, szotar sz2, szotar sz3 '+
       ' where sz1.SZ_FOKOD=''470'' and sz1.SZ_ALKOD=''CEG1'' '+
       '   and sz2.SZ_FOKOD=''470'' and sz2.SZ_ALKOD=''CEG2'' '+
       '   and sz3.SZ_FOKOD=''470'' and sz3.SZ_ALKOD=''CEG3'' ';
   Query_Run(EgyebDlg.QueryAlap, S, true);
   with EgyebDlg.QueryAlap do begin
      Ceg1:= FieldByName('CEG1').AsString;
      Ceg2:= FieldByName('CEG2').AsString;
      Ceg3:= FieldByName('CEG3').AsString;
      end;  // with

    Application.CreateForm(TUniStatFmDlg, UniStatFmDlg);
    // ----- param�terez�s ------- //
    // UniStatFmDlg.AdatlapOsztaly := 'TMunkalapReszletDlg';
    UniStatFmDlg.Caption:='VCARD / kontakt lista be�ll�t�sok';
    UniStatFmDlg.Tag:=606;
    UniStatFmDlg.AdatlapOsztaly:= 'TVCARDParamsDlg';
    SzuromezoTomb:= TStringArray.Create('N�v', '', '0',
                                        'Beoszt�s', '', '0',
                                        'T�bla', '', '0',
                                        Ceg1+'_Iroda', '', '0',
                                        Ceg1+'_Sof�r', '', '0',
                                        Ceg2+'_Iroda', '', '0',
                                        Ceg2+'_Sof�r', '', '0',
                                        Ceg3+'_Iroda', '', '0',
                                        Ceg3+'_Sof�r', '', '0');
    // SzummamezoTomb:= TStringArray.Create('MU_NETTO');
    SzummamezoTomb:= TStringArray.Create('');
    FoSQL:= 'select * from ( '+
            ' select VC_TABLA T�bla, VC_SZEMELYKOD K�d, case when VC_TABLA=''DOLGOZO'' then DO_NAME when VC_TABLA=''VEVO'' then V_NEV end N�v, VC_BEOSZTAS Beoszt�s, '+
            ' 	case when VC_LISTA1=1 then ''igen'' else ''nem'' end '+Ceg1+'_Iroda, '+
            ' 	case when VC_LISTA2=1 then ''igen'' else ''nem'' end '+Ceg1+'_Sof�r, '+
            ' 	case when VC_LISTA3=1 then ''igen'' else ''nem'' end '+Ceg2+'_Iroda, '+
            ' 	case when VC_LISTA4=1 then ''igen'' else ''nem'' end '+Ceg2+'_Sof�r, '+
            ' 	case when VC_LISTA5=1 then ''igen'' else ''nem'' end '+Ceg3+'_Iroda, '+
            ' 	case when VC_LISTA6=1 then ''igen'' else ''nem'' end '+Ceg3+'_Sof�r '+
            ' 	from VCARDPARAMS '+
            ' 	 left outer join DOLGOZO on DO_KOD = VC_SZEMELYKOD '+
            ' 	 left outer join VEVO on V_KOD = VC_SZEMELYKOD '+
            ') a where 1=1 ';  // a k�s�bbi sz�r�shez kell
    Fomezo:= 'N�v';
    Fejlec:= 'VCARD / kontakt lista be�ll�t�sok';
    UniStatFmDlg.AdatTolt(Fomezo, FoSQL, Fejlec, SzuromezoTomb, SzummamezoTomb);
    // --------------------------- //
    Screen.Cursor	:= crDefault;
    UniStatFmDlg.ShowModal;
    UniStatFmDlg.Free;
end;

{procedure TFomenuDlg.ShowTelefonKiegeszitok;
var
   Fomezo, FoSQL, Fejlec: string;
   SzuromezoTomb, SzummamezoTomb: TStringArray;
begin
    Application.CreateForm(TUniStatFmDlg, UniStatFmDlg);
    // ----- param�terez�s ------- //
    // UniStatFmDlg.AdatlapOsztaly := 'TMunkalapReszletDlg';
    UniStatFmDlg.Caption:='Telefon tartoz�kok';
    UniStatFmDlg.Tag:=174;
    SzuromezoTomb:= TStringArray.Create('DOLGOZO', '', '0',
                                        'TELEFONTIPUS', '', '0',
                                        'KIEGESZITO', '', '0');
    SzummamezoTomb:= TStringArray.Create('DARAB');
    FoSQL:='select SORSZAM, DOLGOZO, TELEFONTIPUS, IMEI, KIEGESZITO, DARAB from '+
        ' (select convert(varchar, ROW_NUMBER() OVER(ORDER BY DO_NAME, sz1.SZ_MENEV ASC)) AS SORSZAM, '+
        ' DO_NAME DOLGOZO, sz2.SZ_MENEV TELEFONTIPUS, TK_IMEI IMEI, sz1.SZ_MENEV KIEGESZITO, TI_DARAB DARAB from '+
        ' SZOTAR sz1 left outer join TELKIEG on sz1.SZ_ALKOD=TI_KIEGKOD and isnull(TI_DARAB,0) > 0 '+
        ' left join TELKESZULEK on TI_TKID = TK_ID '+
        ' left join SZOTAR sz2 on TK_TIPUSKOD = sz2.SZ_ALKOD and sz2.SZ_FOKOD = ''146'' '+
        ' left join DOLGOZO on TK_DOKOD = DO_KOD '+
        ' where sz1.SZ_FOKOD = ''153'' '+
        ' ) a where 1=1 ';  // a k�s�bbi sz�r�shez kell
    Fomezo:= 'SORSZAM';
    Fejlec:= 'Telefon tartoz�kok';
    UniStatFmDlg.AdatTolt(Fomezo, FoSQL, Fejlec, SzuromezoTomb, SzummamezoTomb);
    // --------------------------- //
    Screen.Cursor	:= crDefault;
    UniStatFmDlg.ShowModal;
    UniStatFmDlg.Free;
end;
}

procedure TFomenuDlg.elefonSIMelzmnyek1Click(Sender: TObject);
var
   Fomezo, FoSQL, Fejlec: string;
   SzuromezoTomb, SzummamezoTomb: TStringArray;
begin
  if elefonSIMelzmnyek1.Visible then begin
    Screen.Cursor	:= crHourGlass;
    Application.CreateForm(TUniStatFmDlg, UniStatFmDlg);
    // ----- param�terez�s ------- //
    UniStatFmDlg.Caption:='Telefon �s telefonsz�m el�zm�nyek';
    UniStatFmDlg.Tag:=172;
    UniStatFmDlg.StatOsztaly := 'TelefonElozmenyek';
    SzuromezoTomb:= TStringArray.Create('DO_NAME', '', '1',
                                        'ESZKOZ', '', '1',
                                        'TH_KATEG', '1', '1');
    SzummamezoTomb:= TStringArray.Create('');
   //  FoSQL:='select TH_MEDDIG, DO_NAME, TH_KATEG, ESZKOZ, TH_KOD, TH_ID from ( '+
    FoSQL:='select TH_ID, TH_MEDDIG, DO_NAME, TH_KATEG, ESZKOZ, TH_KOD, MEGJEGYZES from ( '+
        'select TH_ID, TH_MEDDIG, DO_NAME, TH_KOD, TH_KATEG, ''IMEI:''+TK_IMEI ESZKOZ, '+  // IMEI az egy�rtelm� azonos�t�
        'TK_MEGJE MEGJEGYZES '+
        'from telhist, telkeszulek, dolgozo, szotar sz '+
        'where DO_KOD = TH_DOKOD and TH_KATEG=''TELEFON'' and TH_KOD = TK_ID '+
        'and sz.SZ_ALKOD = TK_TIPUSKOD and sz.SZ_FOKOD=''146'' '+
        'union '+
        'select TH_ID, TH_MEDDIG, DO_NAME, TH_KOD, TH_KATEG, TE_TELSZAM, TE_MEGJE '+
        'from telhist, telelofizetes, dolgozo '+
        'where DO_KOD = TH_DOKOD and TH_KATEG=''SIM'' and TH_KOD = TE_ID '+
        ' ) a where 1=1 ';  // a k�s�bbi sz�r�shez kell
    Fomezo:= 'TH_ID';
    Fejlec:= 'Telefon �s telefonsz�m el�zm�nyek';
    UniStatFmDlg.AdatTolt(Fomezo, FoSQL, Fejlec, SzuromezoTomb, SzummamezoTomb);
    // --------------------------- //
    Screen.Cursor	:= crDefault;
    UniStatFmDlg.ShowModal;
    UniStatFmDlg.Free;
  end; // if visible
end;

procedure TFomenuDlg.elefonszmlattelek1Click(Sender: TObject);
begin
    Application.CreateForm(TTelSzamlaTetelekFmDlg, TelSzamlaTetelekFmDlg);
    TelSzamlaTetelekFmDlg.ShowModal;
    TelSzamlaTetelekFmDlg.Free;
end;

procedure TFomenuDlg.elefonszmlk1Click(Sender: TObject);
begin
  ShowTelefonSzamlak;
end;

procedure TFomenuDlg.ShowTelefonSzamlak;
var
   Fomezo, FoSQL, Fejlec: string;
   SzuromezoTomb, SzummamezoTomb: TStringArray;
begin
    Application.CreateForm(TTelSzamlaFmDlg, TelSzamlaFmDlg);
    TelSzamlaFmDlg.ShowModal;
    TelSzamlaFmDlg.Free;
end;

procedure TFomenuDlg.SMSEagleQueuelength1Click(Sender: TObject);
var
  i: integer;
  Lista: string;
begin
   // i:= QuerySMSEagleOutboxDarabszam(Lista);
   // NoticeKi('Outbox m�rete: '+IntToStr(i));
end;


procedure TFomenuDlg.elefonszmlkbeolvassa1Click(Sender: TObject);
begin
 	if elefonszmlkbeolvassa1.Visible then begin
     Screen.Cursor	:= crHourGlass;
     Application.CreateForm(TTelSzamlaOlvasoPDFDlg, TelSzamlaOlvasoPDFDlg);
     Screen.Cursor	:= crDefault;
     TelSzamlaOlvasoPDFDlg.ShowModal;
     TelSzamlaOlvasoPDFDlg.Free;
    //  ShowTelefonSzamlak;  // egyb�l meg is n�zheti
  end;
end;

procedure TFomenuDlg.elefonszmlkttekints1Click(Sender: TObject);
var
   Fomezo, FoSQL, Fejlec: string;
   SzuromezoTomb, SzummamezoTomb: TStringArray;
begin
    Application.CreateForm(TUniStatFmDlg, UniStatFmDlg);
    // ----- param�terez�s ------- //
    // UniStatFmDlg.AdatlapOsztaly := 'TMunkalapReszletDlg';
    UniStatFmDlg.StatOsztaly := 'TelefonSzamlak';
    UniStatFmDlg.Caption:='Telefonsz�ml�k �tteckint�s';
    UniStatFmDlg.Tag:=602;
    SzuromezoTomb:= TStringArray.Create('Id�szak_t�l', '', '0',
                                        'Id�szak_ig', '', '0',
                                        'Ki�ll�t�s', '', '0',
                                        'Teljes�t�s', '', '0',
                                        'Sz�mlacsoport', '', '0',
                                        'Fizet�si_hat�rid�', '', '0');
    SzummamezoTomb:= TStringArray.Create('T�vk�zl�si_szolg�ltat�sok', 'Teljes_�sszeg_nett�','Teljes_�sszeg_brutt�');
    FoSQL:='select * from '+
      '(select TS_ID, TS_IDOSZAK_TOL Id�szak_t�l, TS_IDOSZAK_IG Id�szak_ig, '+
      ' case when szami.TelefonszamlaIrodai(TS_ID)=1 then ''Iroda'' else ''Sof�r'' end Sz�mlacsoport, '+
      ' TS_KIALLDAT Ki�ll�t�s, TS_TELJDAT Teljes�t�s, '+
      ' TS_FIZHAT Fizet�si_hat�rid�, TS_SZAMLAINFO Sz�mla_info, TS_MEGJE Megjegyz�s, '+
      ' SUMTAVSZOLG T�vk�zl�si_szolg�ltat�sok, SUMNETTO Teljes_�sszeg_nett�, SUMTOTAL Teljes_�sszeg_brutt� '+
      ' from TELSZAMLAK '+
      ' join (select TT_TSID, SUM(case when TT_TAVKOZLESI=1 then TT_NETTOAR else 0 end) SUMTAVSZOLG, '+
      ' 			 SUM(TT_NETTOAR ) SUMNETTO, '+
      ' 			 SUM(TT_NETTOAR * (100+TT_AFAKULCS) / 100.0) SUMTOTAL '+
      ' 	  from TELSZAMLATETELEK group by TT_TSID) b on TT_TSID = TS_ID '+
      ' ) a where 1=1 ';  // a k�s�bbi sz�r�shez kell
    Fomezo:= 'TS_ID';
    Fejlec:= 'Telefonsz�ml�k �ttekint�s';
    UniStatFmDlg.AdatTolt(Fomezo, FoSQL, Fejlec, SzuromezoTomb, SzummamezoTomb);
    // --------------------------- //
    Screen.Cursor	:= crDefault;
    UniStatFmDlg.ShowModal;
    UniStatFmDlg.Free;
end;

procedure TFomenuDlg.elepitankolsokgenerlsa1Click(Sender: TObject);
begin
     Application.CreateForm(TTelepGenerDlg, TelepGenerDlg);
     TelepGenerDlg.ShowModal;
     TelepGenerDlg.Free;
end;

procedure TFomenuDlg.GepcsereClick(Sender: TObject);
begin
	if Gepcsere.Visible then begin
     Screen.Cursor	:= crHourGlass;
     Application.CreateForm(TGepcsereDlg, GepcsereDlg);
     Screen.Cursor	:= crDefault;
     GepcsereDlg.ShowModal;
     GepcsereDlg.Free;
  end;
end;

procedure TFomenuDlg.TankimportClick(Sender: TObject);
begin
  if EgyebDlg.v_evszam>'2012' then exit;
	if Tankimport.Visible then begin
     Screen.Cursor	:= crHourGlass;
     Application.CreateForm(TTankimportDlg, TankimportDlg);
     Screen.Cursor	:= crDefault;
	  TankimportDlg.ShowModal;
	  TankimportDlg.Free;
  end;
end;

procedure TFomenuDlg.Timer2Timer(Sender: TObject);
var
	i : integer;
begin
	Timer2.Enabled:= false;
  // teszt
  // if UzenetTopDlg<> nil then begin
  //    UzenetTopDlg.Label2.Caption:= FormatDateTime('HH:mm:ss', now);
  //    end;

  if EgyebDlg<> nil then begin
  	i:= EgyebDlg.log_level;
  	EgyebDlg.log_level	:= 0;
  	Query_Run(EgyebDlg.QueryKozos, 'UPDATE JELSZO SET JE_BEMAX = ' +SqlSzamString(now, 14, 4) +', JE_SWVER='''+SW_VERZIO+''' WHERE JE_FEKOD = ''' +EgyebDlg.user_code+ ''' ' , FALSE, FALSE);
  	EgyebDlg.log_level	:= i;
    if FInfo_ <> nil then begin
      FInfo_.QFelhasznalo.Close;
      FInfo_.QFelhasznalo.Open;
      end;
  	//UzenetEllenorzes;
    end; // if
	Timer2.Enabled:= true;
end;

procedure TFomenuDlg.Uzenetek_kezeleseClick(Sender: TObject);
begin
	if Uzenetek_kezelese.Visible then begin
   	Timer2.Enabled	:= false;
     	Screen.Cursor	:= crHourGlass;
     	Application.CreateForm(TUzenetFmDlg, UzenetFmDlg);
     	Screen.Cursor	:= crDefault;
     	UzenetFmDlg.ShowModal;
     	UzenetFmDlg.Free;
		Timer2.Enabled	:= true;
  	end;
end;

procedure TFomenuDlg.MegbizasokClick(Sender: TObject);
begin
	if Megbizasok.Visible then begin
     	Screen.Cursor	:= crHourGlass;
     	Application.CreateForm(TMegbizasUjFmDlg, MegbizasUjFmDlg);
     	Screen.Cursor	:= crDefault;
		MegbizasUjFmDlg.ShowModal;
	 	MegbizasUjFmDlg.Free;
    MegbizasUjFmDlg:=nil;
  end;
end;

procedure TFomenuDlg.HutokimutatasClick(Sender: TObject);
begin
	if Hutokimutatas.Visible then begin
		Screen.Cursor	:= crHourGlass;
		Application.CreateForm(TKihutoDlg, KihutoDlg);
		Screen.Cursor	:= crDefault;
		KihutoDlg.ShowModal;
		KihutoDlg.Free;
	end;
end;

procedure TFomenuDlg.IgazoltgyorshajtasClick(Sender: TObject);
begin
	if Igazoltgyorshajtas.Visible then begin
     Screen.Cursor	:= crHourGlass;
     Application.CreateForm(TNVCKIVFormDlg, NVCKIVFormDlg);
     Screen.Cursor	:= crDefault;
     NVCKIVFormDlg.ShowModal;
     NVCKIVFormDlg.Release;
  end;
end;

procedure TFomenuDlg.Importlerakaljrattal1Click(Sender: TObject);
begin
  EKAER_napi_riport(import_lerakoval);
end;

procedure TFomenuDlg.FelrakasokClick(Sender: TObject);
begin
	if Felrakasok.Visible then begin
     	Screen.Cursor	:= crHourGlass;
     	Application.CreateForm(TFelrakoFmDlg, FelrakoFmDlg);
     	Screen.Cursor	:= crDefault;
     	FelrakoFmDlg.ShowModal;
     	FelrakoFmDlg.Free;
  	end;
end;

procedure TFomenuDlg.mmiFelszolitoLevelKuldesClick(Sender: TObject);
begin
     Application.CreateForm(TSzamlaLevelDlg, SzamlaLevelDlg);
     try
        SzamlaLevelDlg.ShowModal;
     finally
       SzamlaLevelDlg.Free;
       end;  // try-finally
end;

procedure TFomenuDlg.mmiFelszolitoLevelListaClick(Sender: TObject);
var
   Fomezo, FoSQL, Fejlec: string;
   SzuromezoTomb, SzummamezoTomb: TStringArray;
begin
    Screen.Cursor	:= crHourGlass;
    Application.CreateForm(TUniStatFmDlg, UniStatFmDlg);
    // ----- param�terez�s ------- //
    UniStatFmDlg.Caption:='K�sedelmes kiegyenl�t�s figyelmeztet� levelek';
    UniStatFmDlg.Tag:=578;
    SzuromezoTomb:= TStringArray.Create('V_NEV', 'VEVO', '0',
                                        'SL_SZIGOR', 'SZAMLALEVEL', '0',
                                        'SL_CIMZETT', 'SZAMLALEVEL', '0',
                                        'SL_KULDVE', 'SZAMLALEVEL', '0',
                                        );
    SzummamezoTomb:= TStringArray.Create('');
    FoSQL:='select SL_VEVOKOD, V_NEV, SL_SAKOD, SL_KULDVE, SL_NYELV, SL_SZIGOR, SL_CIMZETT from '+
        '(select SL_VEVOKOD, V_NEV, SL_SAKOD, SL_KULDVE, SL_NYELV, SL_SZIGOR, SL_CIMZETT  '+
        ' from SZAMLALEVEL, VEVO '+
        ' where SL_VEVOKOD=V_KOD '+
         ' ) a where 1=1 ';  // a k�s�bbi sz�r�shez kell
    Fomezo:= 'SL_SAKOD';
    Fejlec:= 'K�sedelmes kiegyenl�t�s figyelmeztet� levelek';
    UniStatFmDlg.AdatTolt(Fomezo, FoSQL, Fejlec, SzuromezoTomb, SzummamezoTomb);
    // --------------------------- //
    Screen.Cursor	:= crDefault;
    UniStatFmDlg.ShowModal;
    UniStatFmDlg.Free;
end;

procedure TFomenuDlg.mmiPotkocsiUzemorakClick(Sender: TObject);
var
   Fomezo, FoSQL, Fejlec: string;
   SzuromezoTomb, SzummamezoTomb: TStringArray;
begin
    Application.CreateForm(TUniStatFmDlg, UniStatFmDlg);
    // ----- param�terez�s ------- //
    // UniStatFmDlg.AdatlapOsztaly := 'TMunkalapReszletDlg';
    UniStatFmDlg.Caption:='P�tkocsi �zem�ra kimutat�s';
    UniStatFmDlg.Tag:=610;
    UniStatFmDlg.StatFotabla:= 'JARPOTOS';  // a d�tum sz�r�shez. J_FOFORMUS.DATUMSZURO-ben van defini�lva hozz� adatmez�.
    SzuromezoTomb:= TStringArray.Create('JA_KOD', '', '0',
                                        'EV', '', '0',
                                        'JARATSZAM', '', '0',
                                        'JARAT', '', '0',
                                        'RENDSZAM', '', '0',
                                        'ORSZAG', '', '0');
    // SzummamezoTomb:= TStringArray.Create('UZEMORA1', 'UZEMORA2');
    SzummamezoTomb:= TStringArray.Create('');
    FoSQL:= 'select * from ( '+
            ' select JA_KOD, JA_EV EV, JA_ORSZ ORSZAG, convert(varchar, convert(int, JA_ALKOD)) JARATSZAM, '+
            ' JA_ORSZ+''-''+convert(varchar, convert(int, JA_ALKOD)) JARAT, '+
            ' JP_POREN RENDSZAM, JP_UZEM1 UZEMORA1, JP_UZEM2 UZEMORA2, JP_PORKM KEZDO_KM, JP_PORK2 ZARO_KM '+
            ' from JARPOTOS '+
            ' left join JARAT on JP_JAKOD = JA_KOD '+
            //  where JA_EV=2018 and JP_UZEM1>0
            ') a where 1=1 ';  // a k�s�bbi sz�r�shez kell
    Fomezo:= 'JA_KOD';
    Fejlec:= 'P�tkocsi �zem�ra kimutat�s';
    UniStatFmDlg.AdatTolt(Fomezo, FoSQL, Fejlec, SzuromezoTomb, SzummamezoTomb);
    // --------------------------- //
    Screen.Cursor	:= crDefault;
    UniStatFmDlg.ShowModal;
    UniStatFmDlg.Free;
end;

procedure TFomenuDlg.FormPaint(Sender: TObject);
begin
	Application.OnHint := DisplayHint;
end;

procedure TFomenuDlg.FormSzinezo;
var
	szin	: integer;
   ho		: integer;
  fMenuBrush : TBrush;
  MenuInfo : TMenuInfo;
const
	evszinek	: array [0..4] of integer =  ( clAqua, clLime, clSkyBlue, clMoneyGreen, clCream );
begin
	// Besz�nezi  a f�formot az �vnek megfelel� sz�nnel
   // ho					:= Length(evszinek);
   // szin			 	:= evszinek[StrToIntDef(EgyebDlg.v_evszam,0) MOD ho];
   // most hogy nincs �vv�lt�s, ez kiiktattam. NagyP 2016-01-04
   szin := clMoneyGreen;

   Color			 	:= szin;
   Panel1.Color	 	:= szin;
   Panel2.Color	 	:= szin;
   Panel3.Color	 	:= szin;
	Statusbar1.Color 	:= szin;
   FomenuDlg.Color  	:= szin;
  	fMenuBrush       	:= TBrush.Create;
  	fMenuBrush.Color 	:= szin;
  	MenuInfo.cbSize  	:= SizeOf(MenuInfo);
  	MenuInfo.hbrBack 	:= fMenuBrush.Handle;
  	MenuInfo.fMask   	:= MIM_BACKGROUND;
  	SetMenuInfo(MainMenu1.Handle, MenuInfo);
  	MenuInfo.dwStyle := 0;
  	DrawMenuBar(self.handle);
end;
procedure TFomenuDlg.SzotarfrissitesClick(Sender: TObject);
begin
	// A sz�t�r alapj�n �jragener�ljuk a t�rolt megnevez�seket
	if Szotarfrissites.Visible then begin
     	Screen.Cursor	:= crHourGlass;
     	Application.CreateForm(TSzotarFrissitoDlg, SzotarFrissitoDlg);
     	Screen.Cursor	:= crDefault;
     	SzotarFrissitoDlg.ShowModal;
     	SzotarFrissitoDlg.Free;
  	end;
end;

procedure TFomenuDlg.NyitoExportClick(Sender: TObject);
begin
	if NyitoExport.Visible then begin
     	Screen.Cursor	:= crHourGlass;
     	Application.CreateForm(TNyitoExportDlg, NyitoExportDlg);
     	Screen.Cursor	:= crDefault;
     	NyitoExportDlg.ShowModal;
     	NyitoExportDlg.Free;
  	end;
end;

procedure TFomenuDlg.KimutatasFeleslegesClick(Sender: TObject);
begin
	if KimutatasFelesleges.Visible then begin
		Screen.Cursor	:= crHourGlass;
		Application.CreateForm(TKifolosDlg, KifolosDlg);
		Screen.Cursor	:= crDefault;
		KifolosDlg.ShowModal;
		KifolosDlg.Free;
	end;
end;

procedure TFomenuDlg.KimutatasLizingClick(Sender: TObject);
begin
	if KimutatasLizing.Visible then begin
		Screen.Cursor	:= crHourGlass;
		Application.CreateForm(TKiLizingDlg, KiLizingDlg);
		Screen.Cursor	:= crDefault;
		KiLizingDlg.ShowModal;
		KiLizingDlg.Free;
	end;
end;

procedure TFomenuDlg.Kisautaljrattal1Click(Sender: TObject);
begin
   EKAER_napi_riport(kisauto_aljarattal);
end;

procedure TFomenuDlg.FAPexport1Click(Sender: TObject);
begin
	if FAPexport1.Visible then begin
		Application.CreateForm(TFap_Export1Dlg, Fap_Export1Dlg);
		Fap_Export1Dlg.ShowModal;
		Fap_Export1Dlg.Release;
	end;
end;

procedure TFomenuDlg.FeketelistasokClick(Sender: TObject);
begin
	if Feketelistasok.Visible then begin
		Application.CreateForm(TFeketelistDlg, FeketelistDlg);
		FeketelistDlg.Rep.Preview;
		FeketelistDlg.Free;
	end;
end;

procedure TFomenuDlg.ForgAlvallalClick(Sender: TObject);
begin
	if ForgAlvallal.Visible then begin
	  Screen.Cursor	:= crHourGlass;
	  Application.CreateForm(TForgAlvalDlg, ForgAlvalDlg);
	  Screen.Cursor	:= crDefault;
	  ForgAlvalDlg.ShowModal;
	  ForgAlvalDlg.Free;
  end;
end;

procedure TFomenuDlg.ExportEllenorClick(Sender: TObject);
begin
	if ExportEllenor.Visible then begin
	  Application.CreateForm(TExportHasonDlg, ExportHasonDlg);
	  ExportHasonDlg.ShowModal;
	  ExportHasonDlg.Free;
  end;
end;

procedure TFomenuDlg.AdatbazisImport2Click(Sender: TObject);
begin
  ShowMessage('Ideiglenesen LETILTVA!!!!!!');
{ KG 20111129
	// Az adatb�zis import�l�sa egy m�sikb�l (pl. �lesb�l tesztbe)
	Screen.Cursor	:= crHourGlass;
	Application.CreateForm(TAdatimportDlg, AdatimportDlg);
	Screen.Cursor	:= crDefault;
	AdatimportDlg.ShowModal;
	AdatimportDlg.Free;
  }
end;

procedure TFomenuDlg.CMRkarbantarts1Click(Sender: TObject);
begin
	if CMRkarbantarts1.Visible then begin
	  Screen.Cursor	:= crHourGlass;
	  Application.CreateForm(TCMRFmDlg, CMRFmDlg);
	  Screen.Cursor	:= crDefault;
	  CMRFmDlg.ShowModal;
	  CMRFmDlg.Free;
  end;
end;

procedure TFomenuDlg.CromeOnTop1Click(Sender: TObject);
begin
		Screen.Cursor	:= crHourGlass;
		Application.CreateForm(TUzenetTopDlg, UzenetTopDlg);
		Screen.Cursor	:= crDefault;
		UzenetTopDlg.Show;
		// UzenetTopDlg.Free;
end;

procedure TFomenuDlg.Csehtranzitok1Click(Sender: TObject);
var
   Fomezo, FoSQL, Fejlec: string;
   SzuromezoTomb, SzummamezoTomb: TStringArray;
begin
  if Fizetsimorllista1.Visible then begin
    Application.CreateForm(TIdoszakbeDlg, IdoszakbeDlg);
    IdoszakbeDlg.Mtol.Text:= DatumHozHonap (egyebDlg.MaiDatum, -5);  // 5 h�nappal kor�bban
    IdoszakbeDlg.Mig.Text:= egyebDlg.MaiDatum;
    try
  	  IdoszakbeDlg.ShowModal;
    	if (IdoszakbeDlg.ret_datumtol <> '') and (IdoszakbeDlg.ret_datumig <> '') then begin
    	  Screen.Cursor	:= crHourGlass;
    	  Application.CreateForm(TUniStatFmDlg, UniStatFmDlg);
        // ----- param�terez�s ------- //
        UniStatFmDlg.Caption:='Cseh tranzit - az id�szakban �t nem haladt g�pj�rm�vek list�ja';
        UniStatFmDlg.Tag:=579;
        SzuromezoTomb:= TStringArray.Create('GK_GKKAT', 'GEPKOCSI', '0',
                                            'GK_TIPUS', 'GEPKOCSI', '0');
        SzummamezoTomb:= TStringArray.Create('');
        FoSQL:='select GK_GKKAT, GK_RESZ, GK_TIPUS from ( '+
            'select GK_GKKAT, GK_RESZ, GK_TIPUS from GEPKOCSI '+
            'where GK_RESZ not in '+
            '	(SELECT JA_RENDSZ	FROM JARSOFOR, JARAT where JS_JAKOD=JA_KOD '+
            '    and JA_JKEZD<='''+IdoszakbeDlg.ret_datumig+''''+
            '	and JA_JVEGE>='''+IdoszakbeDlg.ret_datumtol+''''+
            '	and JS_CSEHTRAN>0) '+
            'AND GK_KULSO=0 and GK_ARHIV=0 and GK_POTOS=0 '+
            'AND GK_GKKAT not in (''GAZ-E'',''MG'',''SZGK.'') '+
            ' ) a where 1=1 ';  // a k�s�bbi sz�r�shez kell
        Fomezo:= 'GK_RESZ';
        // Fejlec:= 'Cseh tranzit n�lk�liek ('+IdoszakbeDlg.ret_datumtol+' - '+IdoszakbeDlg.ret_datumig+')';
        Fejlec:= 'Cseh tranzit n�lk�liek';
        UniStatFmDlg.AdatTolt(Fomezo, FoSQL, Fejlec, SzuromezoTomb, SzummamezoTomb);
        // --------------------------- //
    	  Screen.Cursor	:= crDefault;
    	  UniStatFmDlg.ShowModal;
    	  UniStatFmDlg.Free;
        end;  // if j� a k�t d�tum
      finally
        if IdoszakbeDlg<>nil then IdoszakbeDlg.Release;
        end;  // try-finally
  end; // if visible
end;

procedure TFomenuDlg.Csoportok1Click(Sender: TObject);
begin
    Screen.Cursor	:= crHourGlass;
	  Application.CreateForm(TRESTAPIGridDlg, RESTAPIGridDlg);
    RESTAPIGridDlg.Load_groups;
	  Screen.Cursor	:= crDefault;
	  RESTAPIGridDlg.ShowModal;
	  RESTAPIGridDlg.Free;
end;

{
procedure TFomenuDlg.AlGepkocsiClick(Sender: TObject);
begin
	if (Alvallakozok.Visible)and(EgyebDlg.v_evszam<'2013') then begin
	if Algepkocsi.Visible then begin
	  Screen.Cursor	:= crHourGlass;
	  Application.CreateForm(TAlGepFmDlg, AlGepFmDlg);
	  Screen.Cursor	:= crDefault;
	  AlGepFmDlg.ShowModal;
	  AlGepFmDlg.Free;
  end;
  end;
end;
 }

procedure TFomenuDlg.TetanarClick(Sender: TObject);
begin
	if Tetanar.Visible then begin
	  Screen.Cursor	:= crHourGlass;
	  Application.CreateForm(TTetanarUjFmDlg, TetanarUjFmDlg);
	  Screen.Cursor	:= crDefault;
	  TetanarUjFmDlg.ShowModal;
	  TetanarUjFmDlg.Free;
  end;
end;

procedure TFomenuDlg.XMLGeneralasClick(Sender: TObject);
begin
	  Screen.Cursor	:= crHourGlass;
	  Application.CreateForm(TXML_ExportDlg, XML_ExportDlg);
	  Screen.Cursor	:= crDefault;
	  XML_ExportDlg.ShowModal;
	  XML_ExportDlg.Free;
end;

procedure TFomenuDlg.YCOHistrocal1Click(Sender: TObject);
begin
	Application.CreateForm(TTYCO_HistoricalDlg, TYCO_HistoricalDlg);
	TYCO_HistoricalDlg.ShowModal;
	TYCO_HistoricalDlg.Free;
end;

procedure TFomenuDlg.LogExportClick(Sender: TObject);
begin
	// A LOG adatok kirak�sa �llom�nyba �s t�rl�se
  	Screen.Cursor	:= crHourGlass;
	Application.CreateForm(TLogExportDlg, LogExportDlg);
	Screen.Cursor	:= crDefault;
	LogExportDlg.ShowModal;
	LogExportDlg.Free;
end;

procedure TFomenuDlg.GepjarmuStatClick(Sender: TObject);
begin
	// G�pj�rm� statisztika
	Screen.Cursor	:= crHourGlass;
	Application.CreateForm(TGepStatFmDlg, GepStatFmDlg);
	Screen.Cursor	:= crDefault;
	GepStatFmDlg.ShowModal;
	GepStatFmDlg.Free;
end;

procedure TFomenuDlg.PotkocsiStatClick(Sender: TObject);
begin
	// P�tkocsi statisztika
	Screen.Cursor	:= crHourGlass;
	Application.CreateForm(TPotStatFmDlg, PotStatFmDlg);
	Screen.Cursor	:= crDefault;
	PotStatFmDlg.ShowModal;
	PotStatFmDlg.Free;
end;

procedure TFomenuDlg.Prba1Click(Sender: TObject);
var
   S: string;
begin
   // NoticeKi(IdezoBovito('Szundi, Kuka, Hapci'));
   // NoticeKi(FormatDateTime('YYYYMMDDHHMMSS', Now));

	// Application.CreateForm(TFlowPanelDlg, FlowPanelDlg);
	// FlowPanelDlg.ShowModal;
	// FlowPanelDlg.Free;

  // NoticeKi(JavaScriptTimestampToFuvaros(1463734837620));

  // NoticeKi(EgyebDlg.JSONArray_DuplikacioTorol('["+36209409098" , "+36209588494" , "+36209588494","+36209409098", "+36209409098", "+36209409111", "+36209588494"]'));
  // S:= RecordsetToJSON('select DO_KOD, DO_NAME, DO_EMAIL from dolgozo where do_kod in (770, 771)');
  // NoticeKi(S);

	// S:= InputBox('MailTo exception:','','');
	// ShellExecute(Application.Handle,'open', PChar(S), nil, nil, SW_SHOWNORMAL) ;

  // Application.CreateForm(TProba_szamitas, Proba_szamitas);
	// Proba_szamitas.ShowModal;
	// Proba_szamitas.Free;

  // Application.CreateForm(TFlottaHelyzetDlg, FlottaHelyzetDlg);
	// FlottaHelyzetDlg.ShowModal;
	// FlottaHelyzetDlg.Free;
  // S:= InputBox('Email:','','"Matyi A Heged�s" <matyi.a@ize.hu>');
  // NoticeKi(GetKopaszEmail(S));
  // NoticeKi(GetKozeliMertIdopont(1, '2018.01.12. 00:10:18'));

  // Proba1;
  // S:= InputBox('ShellExecute:','','');
  //  EgyebDlg.MyShellExecute(S);
  S:= Format('%6.6d',[95410]);
  NoticeKi(S);


end;

procedure TFomenuDlg.Proba1;
var
  S, TOVABBITERULET: string;
  cimzettlist, reszlista: TStringList;
  i: integer;
begin
  cimzettlist:= TStringList.Create;
  // ---- ez konkr�tan nem csin�l semmit ... ----//
  // cimzettlist.Sorted:= True;
  // cimzettlist.Duplicates:= dupIgnore;  // 2018.01.04.
  // ------------------------------------------- //
  reszlista:= TStringList.Create;
  try
     cimzettlist:= EgyebDlg.GetFelettesEmail('SZ');
     TOVABBITERULET:= 'SP.SZ';
     for i:=1 to EgyebDlg.SepListLength(',', TOVABBITERULET) do begin
          reszlista:= EgyebDlg.GetFelettesEmail(EgyebDlg.GetSepListItem(',', TOVABBITERULET, i));
          cimzettlist.AddStrings(reszlista);
          end;
     cimzettlist:= EgyediElemek(cimzettlist);
     NoticeKi(cimzettlist.CommaText);
   finally
      if cimzettlist <> nil then cimzettlist.Free;
      if reszlista <> nil then reszlista.Free;
      end;  // try-finally
end;

procedure TFomenuDlg.Proba2Click(Sender: TObject);
var
   S: string;
   Uid: TGuid;
   Res: HResult;
begin
   // S:= InputBox('Sz�veg', '','');
   // S:= EgyebDlg.SzovegFilter(S, 'sz�m_per_sz�m');
   // S:= TelenorIdoOsszead('22:40:59', '5:31:40');
   // Res := CreateGuid(Uid);
   // if (FilesAreEqual('K:\FUVARADO\LIST\DK_TRANS_GKV.VCF', '\\JS-BACKUPSERVER\COLLECTIONS\ADMIN\DK_TRANS_GKV.VCF')='') then
   //  NoticeKi('Egyforma');
   Application.CreateForm(TEDIFACT_ExportDlg, EDIFACT_ExportDlg);
   EDIFACT_ExportDlg.ShowModal;
   EDIFACT_ExportDlg.Free;
end;

procedure TFomenuDlg.FuvarosCockpitszinkronizls1Click(Sender: TObject);
begin
  RESTAPIKozosDlg.Show;
  RESTAPIKozosDlg.Fuvaros_backend_szinkron;
end;

procedure TFomenuDlg.FuvartablaClick(Sender: TObject);
begin
	// Fuvart�bla karbantart�sa
	Screen.Cursor	:= crHourGlass;
	Application.CreateForm(TFuvartablaFmDlg, FuvartablaFmDlg);
	Screen.Cursor	:= crDefault;
	FuvartablaFmDlg.ShowModal;
	FuvartablaFmDlg.Free;
end;

procedure TFomenuDlg.HianyDoksikClick(Sender: TObject);
begin
	if HianyDoksik.Visible then begin
		Screen.Cursor	:= crHourGlass;
		Application.CreateForm(TKiHianyDoksiDlg, KiHianyDoksiDlg);
		Screen.Cursor	:= crDefault;
		KiHianyDoksiDlg.ShowModal;
		KiHianyDoksiDlg.Free;
	end;
end;

procedure TFomenuDlg.DolgervenyClick(Sender: TObject);
begin
	if DolgErveny.Visible then begin
		Screen.Cursor	:= crHourGlass;
		Application.CreateForm(TKiDolgErvDlg, KiDolgErvDlg);
		Screen.Cursor	:= crDefault;
		KiDolgErvDlg.ShowModal;
		KiDolgErvDlg.Free;
	end;
end;

procedure TFomenuDlg.Szmlaadatokmentse1Click(Sender: TObject);
begin
	Application.CreateForm(TSzamlaExportDlg, SzamlaExportDlg);
	SzamlaExportDlg.ShowModal;
	SzamlaExportDlg.Free;
end;

procedure TFomenuDlg.ADR030Exportls1Click(Sender: TObject);
begin
	Application.CreateForm(TADR30_ExportDlg, ADR30_ExportDlg);
	ADR30_ExportDlg.ShowModal;
	ADR30_ExportDlg.Free;
end;

procedure TFomenuDlg.HianyzokmClick(Sender: TObject);
begin
	if Hianyzokm.Visible then begin
		Screen.Cursor	:= crHourGlass;
		Application.CreateForm(THianyKmDlg, HianyKmDlg);
		Screen.Cursor	:= crDefault;
		HianyKmDlg.ShowModal;
		HianyKmDlg.Free;
	end;
end;

procedure TFomenuDlg.AdBluetankolasClick(Sender: TObject);
begin
	if AdBluetankolas.Visible then begin
		Screen.Cursor	:= crHourGlass;
		Application.CreateForm(TKiAdBlueDlg, KiAdBlueDlg);
		Screen.Cursor	:= crDefault;
		KiAdBlueDlg.ShowModal;
		KiAdBlueDlg.Free;
	end;
end;

procedure TFomenuDlg.Adhatsgiellenrzsiadatszolgltats1Click(Sender: TObject);
begin
    Application.CreateForm(TNAVOutputDlg, NAVOutputDlg);
    Screen.Cursor    := crDefault;
    NAVOutputDlg.ShowModal;
    NAVOutputDlg.Free;
end;

procedure TFomenuDlg.KPIkimutats1Click(Sender: TObject);
begin
	Application.CreateForm(TKPI_ExportDlg, KPI_ExportDlg);
	KPI_ExportDlg.ShowModal;
	KPI_ExportDlg.Free;
end;

procedure TFomenuDlg.MSriport1Click(Sender: TObject);
begin
	Application.CreateForm(TMSR_ExportDlg, MSR_ExportDlg);
	MSR_ExportDlg.ShowModal;
	MSR_ExportDlg.Free;
end;

procedure TFomenuDlg.MySMS1Click(Sender: TObject);
begin
    Application.CreateForm(TUniEllenorDlg, UniEllenorDlg);
		UniEllenorDlg.ShowModal;
		UniEllenorDlg.Free;
end;

procedure TFomenuDlg.GkvezetidoClick(Sender: TObject);
begin
	if Gkvezetido.Visible then begin
		Screen.Cursor	:= crHourGlass;
		Application.CreateForm(TGkIdoKmDlg, GkIdoKmDlg);
		Screen.Cursor	:= crDefault;
		GkIdoKmDlg.ShowModal;
		GkIdoKmDlg.Free;
	end;
end;

procedure TFomenuDlg.GoogleMatrixteszt1Click(Sender: TObject);
var
   S: string;
   MBKOD, i, Mennyit: integer;
   UTINFO: TUtvonalInfo;
   OldCursor: TCursor;
begin
   Mennyit := StrToInt(InputBox('�jrasz�m�t�s', 'Megb�z�sok sz�ma', '500'));
   if NoticeKi('�jrasz�moljuk '+ IntToStr(Mennyit)+' megb�z�s �tvonal hossz�t. Folytatja?', NOT_QUESTION) <> 0 then begin
       Exit;  // ha nem "igen" a v�lasz
       end;
   OldCursor:= Screen.Cursor;  // elmentj�k
   Screen.Cursor	:= crHourGlass;
   FormGaugeDlg:= TFormGaugeDlg.Create(Self);
   FormGaugeDlg.GaugeNyit('�tvonal hossz sz�m�t�sa ...' , '', false);
   i:=0;
   EgyebDlg.SetADOQueryDatabase(QueryAlap);
   EgyebDlg.SetADOQueryDatabase(QueryAlap2);
   S:='SELECT TOP '+IntToStr(Mennyit)+' MB_MBKOD FROM MEGBIZAS WHERE ';
   S:=S+' ((MB_TAVMI IS NULL) or (MB_TAVMI='''') or (MB_TAVMI=''�thossz sz�m�t�s inakt�v!''))';

   // S:='SELECT MB_MBKOD FROM MEGBIZAS WHERE 1=1 ';
   // S:=S+' MB_TAVMI IS NULL ';
   // S:=S+' (MB_TAVMI IS NULL  or MB_TAVKM=0)';
   // --- eseti sz�r�s teszthez ----
   // S:=S+' AND MB_DATUM BETWEEN ''2015.04.06.'' and ''2015.04.12.'' ';
   // S:=S+' AND MB_TEDAT BETWEEN ''2015.04.06.'' and ''2015.04.12.'' ';
   // S:=S+' AND MB_MBKOD=43907 ';
   // ------------------------------
   S:=S+' ORDER BY 1 DESC';
   Query_Run(QueryAlap, S, true);

   with QueryAlap do begin
     while not Eof do begin
        MBKOD:= FieldByName('MB_MBKOD').AsInteger;
        UTINFO:= EgyebDlg.UtvonalSzamitas_megbizas (MBKOD);
      	Query_Update (QueryAlap2, 'MEGBIZAS', [
           		'MB_TAVKM',     FormatFloat('0',UTINFO.Kilometer),
           		'MB_TAVMI',     ''''+UTINFO.Leiras+''''
  		          ], ' WHERE MB_MBKOD = '+IntToStr(MBKOD) );

        MegbizasKieg(IntToStr(MBKOD));  // �jrasz�molja az EUR/km �s t�rsai �rt�keket
        Next;
        Inc(i);
        FormGaugeDlg.Pozicio (( i * 100 ) div RecordCount) ;
                       // element = eredm�nyhalmaz! pontok x pontok!!
        Sleep(5000);   //  "100 elements per 10 seconds, 2500 elements per 24 hour period."
        Application.ProcessMessages;
        end;  // while
     end;  // with
   FormGaugeDlg.Destroy;
   Screen.Cursor	:= OldCursor;
end;

procedure TFomenuDlg.Gyorshajtsstatisztika1Click(Sender: TObject);
begin
  if Gyorshajtsstatisztika1.Visible then begin
	  Screen.Cursor	:= crHourGlass;
	  Application.CreateForm(TGyorshajtStatFmDlg, GyorshajtStatFmDlg);
	  Screen.Cursor	:= crDefault;
	  GyorshajtStatFmDlg.ShowModal;
	  GyorshajtStatFmDlg.Free;
  end;
end;

procedure TFomenuDlg.KombiExportClick(Sender: TObject);
begin
	Application.CreateForm(TKombi_ExportDlg, Kombi_ExportDlg);
	Kombi_ExportDlg.ShowModal;
	Kombi_ExportDlg.Free;
end;

procedure TFomenuDlg.Kontaktok1Click(Sender: TObject);
begin
    Screen.Cursor	:= crHourGlass;
	  Application.CreateForm(TRESTAPIGridDlg, RESTAPIGridDlg);
    RESTAPIGridDlg.Load_users;
	  Screen.Cursor:= crDefault;
	  RESTAPIGridDlg.ShowModal;
	  RESTAPIGridDlg.Free;
end;

procedure TFomenuDlg.DkvKartyakClick(Sender: TObject);
begin
	if DkvKartyak.Visible then begin
	  Screen.Cursor	:= crHourGlass;
	  Application.CreateForm(TDkvKarFmDlg, DkvKarFmDlg);
	  Screen.Cursor	:= crDefault;
	  DkvKarFmDlg.ShowModal;
	  DkvKarFmDlg.Free;
  end;
end;

procedure TFomenuDlg.ApplicationEvents1Message(var Msg: tagMSG;
  var Handled: Boolean);
var i: integer;
begin
  if Msg.message=WM_MOUSEWHEEL then
  begin
   Msg.message:=WM_KEYDOWN;
   Msg.lParam:=0;
   i:=Msg.wParam;
   if i>0 then
    Msg.wParam:=VK_UP
   else
    Msg.wParam:=VK_DOWN;


   Handled:=False;
  end;
end;



procedure TFomenuDlg.Button1Click(Sender: TObject);
begin
  close;
end;

procedure TFomenuDlg.AdatbazisImportClick(Sender: TObject);
begin
	// Az adatb�zis import�l�sa egy m�sikb�l (pl. �lesb�l tesztbe)
	Screen.Cursor	:= crHourGlass;
	Application.CreateForm(TAdatimportDlg, AdatimportDlg);
	Screen.Cursor	:= crDefault;
	AdatimportDlg.ShowModal;
	AdatimportDlg.Free;
end;

procedure TFomenuDlg.Adatbzisellenrzse1Click(Sender: TObject);
begin
   Application.CreateForm(TBazisellenDlg, BazisellenDlg);
   BazisellenDlg.ShowModal;
   BazisellenDlg.Free;
end;

procedure TFomenuDlg.SpeedButton3Click(Sender: TObject);
begin
	if ( (GetRightTag(567) <> RG_NORIGHT	) or EgyebDlg.user_super) then begin
       Application.CreateForm(TFTavollet, FTavollet);
       if FTavollet.vanadat then begin
           FTavollet.ShowModal;
       end;
       FTavollet.Free;
   end else begin
       NoticeKi('Nincs jogosults�g!');
   end;
end;

procedure TFomenuDlg.GKStatFrissitesClick(Sender: TObject);
var
	rszlista	: TStringLIst;
	i			: integer;
	rsz			: string;
begin
 if MessageBox(0, 'Mehet a friss�t�s?', '', MB_ICONQUESTION or MB_YESNO or MB_DEFBUTTON2)=IDYES then
 begin
	EgyebDlg.SeTADOQueryDatabase(QueryUj2);
	// Ellen�rizz�k, hogy minden g�pkocsi benne van-e a list�ban
	rszlista	:= TStringList.Create;
	GetRszLista(rszlista, 0);
	Query_Run(QueryUj2, 'SELECT TG_RENSZ FROM T_GEPK ');
   Label4.Visible:=True;
	i := 0;
	while i < rszlista.Count do begin
		rsz	:= rszlista[i];
		if not QueryUj2.Locate('TG_RENSZ', rsz , []) then begin
			if Length(rsz) = 7 then begin
				if copy(rsz, 4, 3) <> '000' then begin
					// L�trehozzuk a g�pkocsit
					GepStatTolto(rsz);
				end;
			end;
		end;
		Inc(i);
	end;
	QueryUj2.First;
	while not QueryUj2.Eof do begin
    Label4.Caption:= QueryUj2.FieldByName('TG_RENSZ').AsString;
    Label4.Update;
		if rszlista.IndexOf( QueryUj2.FieldByName('TG_RENSZ').AsString ) < 0 then begin
			// T�r�lni kellene a g�pkocsit, mert m�r nem �l
			Query_Run(QueryUj3, 'DELETE FROM T_GEPK WHERE TG_RENSZ = '''+QueryUj2.FieldByName('TG_RENSZ').AsString+''' ');
		end
    else
    	GepStatTolto(QueryUj2.FieldByName('TG_RENSZ').AsString);

		QueryUj2.Next;
	end;
  Label4.Visible:=False;
 end;
end;

procedure TFomenuDlg.PKStatFrissitesClick(Sender: TObject);
var
	rszlista	: TStringLIst;
	i			: integer;
	rsz,sql	: string;
begin
 if MessageBox(0, 'Mehet a friss�t�s?', '', MB_ICONQUESTION or MB_YESNO or MB_DEFBUTTON2)=IDYES then
 begin
	EgyebDlg.SeTADOQueryDatabase(QueryUj2);
	// Ellen�rizz�k, hogy minden g�pkocsi benne van-e a list�ban
	rszlista	:= TStringList.Create;
	GetRszLista(rszlista, 1);
	Query_Run(QueryUj2, 'SELECT TP_POTSZ FROM T_POTK ');
  Label4.Visible:=True;
	i := 0;
	while i < rszlista.Count do begin
		rsz	:= rszlista[i];
		if not QueryUj2.Locate('TP_POTSZ', rsz , []) then begin
			if Length(rsz) = 7 then begin
				if copy(rsz, 4, 3) <> '000' then begin
					// L�trehozzuk a g�pkocsit
					PotStatTolto(rsz);
				end;
			end;
		end;
		Inc(i);
	end;
	QueryUj2.First;
	while not QueryUj2.Eof do begin
    Label4.Caption:= QueryUj2.FieldByName('TP_POTSZ').AsString;
    Label4.Update;
		if rszlista.IndexOf( QueryUj2.FieldByName('TP_POTSZ').AsString ) < 0 then begin
			// T�r�lni kellene a g�pkocsit, mert m�r nem �l
			sql:='DELETE FROM T_POTK WHERE TP_POTSZ = '''+QueryUj2.FieldByName('TP_POTSZ').AsString+''' ';
		 	Query_Run(QueryUj3,sql);
		end
    else
    	GepStatTolto(QueryUj2.FieldByName('TP_POTSZ').AsString);

		QueryUj2.Next;
	end;
  Label4.Visible:=False;
 end;
end;

procedure TFomenuDlg.PODBridgestone1Click(Sender: TObject);
begin
  Application.CreateForm(TPOD_ExportDlg, POD_ExportDlg);
	POD_ExportDlg.ShowModal;
	POD_ExportDlg.Release;
end;

procedure TFomenuDlg.telekexportlsaaknyvelshez1Click(Sender: TObject);
var
  jog:string;
begin
  ADODataSet1.Open;
  ADODataSet1.First;
  while not ADODataSet1.Eof do
  begin
    jog:='';
    if pos('A', ADODataSet1DO_JOKAT.Value)>0 then jog:=jog+'A,';
    if pos('B', ADODataSet1DO_JOKAT.Value)>0 then jog:=jog+'B,';
    if pos('C', ADODataSet1DO_JOKAT.Value)>0 then jog:=jog+'C,';
    if pos('D', ADODataSet1DO_JOKAT.Value)>0 then jog:=jog+'D,';
    if pos('E', ADODataSet1DO_JOKAT.Value)>0 then jog:=jog+'E,';
    if jog<>'' then
    begin
      ADODataSet1.Edit;
      ADODataSet1DO_JOKAT.Value:=jog;
      ADODataSet1.Post;
    end;
    ADODataSet1.Next;
  end;
  ADODataSet1.Close;

end;

procedure TFomenuDlg.GKSOBELbers2Click(Sender: TObject);
var
   sorsz   : integer;
begin
   if NoticeKi('Val�ban friss�ti a sof�r - g�pkocsi �sszerendel�seket?', NOT_QUESTION) <> 0 then begin
       Exit;
   end;
   Screen.Cursor   := crHourGlass;
   FormGaugeDlg    := TFormGaugeDlg.Create(Self);
   FormGaugeDlg.GaugeNyit('Sof�r adatok friss�t�se... ' , '', false);
   Query_Run(QueryKoz, 'SELECT GK_RESZ FROM GEPKOCSI');
   sorsz   := 0;
   while not QueryKoz.Eof do begin
       FormGaugeDlg.Pozicio ( ( sorsz * 100 ) div QueryKoz.RecordCount ) ;
       Inc(sorsz);
       SofornevKepzes( QueryKoz.FieldByName('GK_RESZ').AsString );
       QueryKoz.Next;
   end;
   Screen.Cursor   := crDefault;
   FormGaugeDlg.Free;
   NoticeKi('A friss�t�s megt�rt�nt!');
end;

procedure TFomenuDlg.SzmlkjraszmolsaEUR2Click(Sender: TObject);
var
   sorsz   : integer;
   vnem    : string;
   ossz    : double;
   afa     : double;
begin
   NoticeKi('M�velet t�r�lve!');
   Exit;
   if NoticeKi('Val�ban �jrasz�moltatja a sz�ml�kat?', NOT_QUESTION) <> 0 then begin
       Exit;
   end;
   Screen.Cursor   := crHourGlass;
   FormGaugeDlg    := TFormGaugeDlg.Create(Self);
   FormGaugeDlg.GaugeNyit('Sz�mla adatok friss�t�se... ' , '', false);
   Query_Run(QueryAlap, 'SELECT SA_UJKOD FROM SZFEJ');
   sorsz   := 0;
   while not QueryAlap.Eof do begin
       FormGaugeDlg.Pozicio ( ( sorsz * 100 ) div QueryAlap.RecordCount ) ;
       Inc(sorsz);

       vnem    := '';
       ossz    := 0;
       afa     := 0;
       Query_Run(QueryAlap2, 'SELECT SS_VALNEM, SS_VALERT, SS_DARAB, SS_AFA FROM SZSOR WHERE SS_UJKOD = '+ QueryAlap.FieldByName('SA_UJKOD').AsString );
       while not QueryAlap2.Eof do begin
           if QueryAlap2.FieldByName('SS_VALNEM').AsString <> '' then begin
               vnem    := QueryAlap2.FieldByName('SS_VALNEM').AsString;
           end;
           ossz    := ossz +   StringSzam( QueryAlap2.FieldByName('SS_DARAB').AsString ) * StringSzam( QueryAlap2.FieldByName('SS_VALERT').AsString );
           afa     := afa  +   StringSzam( QueryAlap2.FieldByName('SS_DARAB').AsString ) * StringSzam( QueryAlap2.FieldByName('SS_VALERT').AsString ) *
                               StringSzam( QueryAlap2.FieldByName('SS_AFA').AsString ) / 100;
           QueryAlap2.Next;
       end;

       Query_Update ( QueryAlap2, 'SZFEJ', [
           'SA_VALNEM', ''''+vnem+'''',
           'SA_VALOSS', SqlSzamString(ossz,12,2),
           'SA_VALAFA', SqlSzamString(afa,12,2)
       ] , 'WHERE SA_UJKOD = '+ QueryAlap.FieldByName('SA_UJKOD').AsString );

       QueryAlap.Next;
   end;
   Screen.Cursor   := crDefault;
   FormGaugeDlg.Free;
   NoticeKi('A friss�t�s megt�rt�nt!');
end;

procedure TFomenuDlg.Nevnap;
begin
  Application.CreateForm(TFNevnap, FNevnap);
  Label7.Caption:= FNevnap.Nevnap_dat(date);
  Label8.Caption:= FNevnap.Nevnap_dat(date+1);
  if FNevnap.Nevnap(EgyebDlg.user_code) then
    FNevnap.ShowModal;

  FNevnap.Free;
  Timer1.Enabled:=True;
end;

procedure TFomenuDlg.FormClick(Sender: TObject);
begin
  if FInfo_ <> nil then
     FInfo_.Visible:=True;
end;

procedure TFomenuDlg.SpeedButton4Click(Sender: TObject);
begin
  close;
end;

procedure TFomenuDlg.Dolgcsop1Click(Sender: TObject);
begin
  DOLGCSOP.Open;
  Dolgcsop.First;
  while not DOLGCSOP.Eof do
  begin
     DOLG.Close;
     DOLG.Parameters[0].Value:=DOLGCSOPDC_DOKOD.Value;
     dolg.Open;
     DOLG.First ;
     DOLG.Edit;
     DOLGDO_CSKOD.Value:=DOLGCSOPDC_CSKOD.Value;
     DOLGDO_CSNEV.Value:=DOLGCSOPDC_CSNEV.Value;
     DOLGDO_CSDAT.Value:=DOLGCSOPDC_DATTOL.Value;
     DOLG.Post;
     DOLGCSOP.Next;
  end;
  dolgcsop.close;
  DOLG.Close;
  ShowMessage('K�SZ!');
end;

procedure TFomenuDlg.KOLTSCalcFields(DataSet: TDataSet);
var
  tank, szaz:double;
begin
  GEPKOCSI.Close;
  GEPKOCSI.Parameters[0].Value:=KOLTSKS_RENDSZ.Value;
  GEPKOCSI.Open;
  GEPKOCSI.First;
  szaz:=100;
  if not GEPKOCSI.Eof then
  begin
     tank:=GEPKOCSIgk_tankl.Value;
     if tank<>0 then
       szaz:=KOLTSKS_UZMENY.Value/tank*100;
  end;
  KOLTSTANK_SZAZ.Value:=szaz;
end;

procedure TFomenuDlg.ankimportTeletankjavts1Click(Sender: TObject);
var
  HATAR,i:integer;
begin
	EllenListTorol;

  HATAR:= Trunc(StringSzam(EgyebDlg.Read_SZGrid('999','718')));
 // HATAR:=5;
  KOLTS.Close;
  KOLTS.Parameters[0].Value:='2012.01.01.';
  KOLTS.Open;
  KOLTS.First;
  i:=1;
  while not  KOLTS.Eof do
  begin
    if KOLTSTANK_SZAZ.Value<HATAR then
    begin
			EllenListAppend(IntToStr(i)+'  '+ KOLTSKS_RENDSZ.Value+' '+KOLTSKS_UZMENY.AsString+' '+KOLTSTANK_SZAZ.AsString);
     // KOLTS.Edit;
     // KOLTSKS_TELE.Value:=0;
     // KOLTS.Post;
     inc(i);
    end;
    KOLTS.Next;
  end;
  KOLTS.Close;
	if ( ( EgyebDlg.ellenlista.Count > 0 ) ) then begin
		EllenListMutat('A gener�l�s eredm�nye', false);
	end;

  ShowMessage('K�SZ');
end;

procedure TFomenuDlg.GepkocsikorClick(Sender: TObject);
begin
	if Gepkocsikor.Visible then begin
		Screen.Cursor	:= crHourGlass;
    Application.CreateForm(TGkKorKmDlg, GkKorKmDlg);
		Screen.Cursor	:= crDefault;
		GkKorKmDlg.ShowModal;
		GkKorKmDlg.Free;
	end;

end;

procedure TFomenuDlg.Htsaljrattal1Click(Sender: TObject);
begin
  EKAER_napi_riport(hutos_aljarattal);
end;

procedure TFomenuDlg.Httankolsktsg1Click(Sender: TObject);
begin
  ADODataSet6.close;
  ADODataSet6.Open;
  ADODataSet6.First;
  while not ADODataSet6.Eof do
  begin
    ADODataSet7.Close;
    ADODataSet7.Parameters[0].Value:=ADODataSet6KS_RENDSZ.Value;
    ADODataSet7.Open;
    ADODataSet7.First;
    if not ADODataSet7.Eof then
    begin
      if (ADODataSet7GK_UT_DA.Value<=ADODataSet6KS_DATUM.Value)or(ADODataSet7GK_UT_DA.IsNull) then
      begin
        ADODataSet7.Edit;
        ADODataSet7GK_UT_DA.Value:=ADODataSet6KS_DATUM.Value;
        ADODataSet7GK_UT_KM.Value:=ADODataSet6KS_KMORA.Value;
        ADODataSet7.Post;
      end;
    end;
    ADODataSet6.Next;
  end;
  ADODataSet6.Close;
  ShowMessage('K�sz!');
end;

procedure TFomenuDlg.Kamera1Click(Sender: TObject);
begin
  Application.CreateForm(TFKamera, FKamera);
  FKamera.Show;
//  FKamera.Free;
end;

procedure TFomenuDlg.Kdokttekintse1Click(Sender: TObject);
const
   ThisRightTag = 597;
var
   Fomezo, FoSQL, Fejlec: string;
   SzuromezoTomb, SzummamezoTomb: TStringArray;
   RightTag: integer;
   LehetModositani: boolean;
begin
  if Kdokttekintse1.Visible then begin
      Application.CreateForm(TUniStatFmDlg, UniStatFmDlg);
      // ----- param�terez�s ------- //
      UniStatFmDlg.AdatlapOsztaly := 'TMegrendeloKodMagyarazDlg';
      UniStatFmDlg.Caption:='Kiadott megrendel� k�dok list�ja';
      UniStatFmDlg.Tag:=ThisRightTag;
      SzuromezoTomb:= TStringArray.Create('MR_TERULET', '', '0',
                                          'MR_DATUM', '', '0',
                                          'JE_FENEV', '', '0',
                                          'MR_WEBSHOP', '', '0',
                                          'MR_NEMHASZNALT', '', '0');
      SzummamezoTomb:= TStringArray.Create('');
      FoSQL:='select MR_ID, MEGRENDELOKOD, MR_TERULET, MR_DATUM, MR_SORSZAM, JE_FENEV, MR_KIADI, MEGRENDELES_TARGYA, MR_WEBSHOP, '+
          ' MR_NEMHASZNALT, MR_MEGJE from ( '+
          'select MR_ID,  '+
        	' ''PO''+substring(MR_DATUM,3,2)+substring(MR_DATUM,6,2)+substring(MR_DATUM,9,2)+''_''+MR_TERULET+''_''+RIGHT(''000'' + CONVERT(varchar, MR_SORSZAM), 3 ) MEGRENDELOKOD, '+
	        ' MR_TERULET, MR_DATUM, MR_SORSZAM, JE_FENEV, MR_KIADI, MR_REF MEGRENDELES_TARGYA, MR_NEMHASZNALT, MR_MEGJE, MR_WEBSHOP '+
	        ' from MEGRENDELO, SZOTAR, JELSZO '+
	        ' where SZ_ALKOD=MR_TERULET and SZ_FOKOD = ''148'' '+
	        ' and MR_KINEK=JE_FEKOD '+
          ' ) a where 1=1 ';  // a k�s�bbi sz�r�shez kell
      Fomezo:= 'MR_ID';
      Fejlec:= 'Kiadott megrendel� k�dok list�ja';
      UniStatFmDlg.StatOsztaly := 'Megrendelo_kodok';  // a sor sz�nez�shez �s a PDF megjelen�t�shez
      UniStatFmDlg.AdatTolt(Fomezo, FoSQL, Fejlec, SzuromezoTomb, SzummamezoTomb);
      RightTag:= GetRightTag(ThisRightTag);
      LehetModositani:= (RightTag = RG_MODIFY) or (RightTag = RG_ALLRIGHT) or EgyebDlg.user_super;
      UniStatFmDlg.ButtonMod.Enabled := LehetModositani;
      UniStatFmDlg.ButtonUj.Enabled := LehetModositani;
      // --------------------------- //
      Screen.Cursor	:= crDefault;
      UniStatFmDlg.ShowModal;
      UniStatFmDlg.Free;
  end; // if visible
end;

procedure TFomenuDlg.rkpteszt1Click(Sender: TObject);
begin
  EgyebDlg.GoogleMap('','','','',True);
end;

{procedure TFomenuDlg.SHELLtankolsirak1Click(Sender: TObject);
var
  fn: string;
begin
	fn:= EgyebDlg.Read_SZGrid( '999', '724' );
   if fn='' then begin
       exit;
   end;
   if FileExists(fn) then begin
       ShellExecute(Handle, 'open', PChar(fn), nil, PChar(fn), SW_SHOWDEFAULT);
   end;
end;}


{procedure TFomenuDlg.Kategriavltsiignyek1Click(Sender: TObject);
var
  fn: string;
begin
	fn:= EgyebDlg.Read_SZGrid( '999', '726' );
   if fn='' then exit;
   if not FileExists(fn) then exit;
       ShellExecute(Handle, 'open', PChar(fn), nil, PChar(fn), SW_SHOWDEFAULT);
end;
}

procedure TFomenuDlg.Kereskedelmizemanyag1Click(Sender: TObject);
begin
   // Kereskedelmi �zemanyag list�ja
	if Kereskedelmizemanyag1.Visible then begin
		Screen.Cursor	:= crHourGlass;
		Application.CreateForm(TKiKeresDlg, KiKeresDlg);
		Screen.Cursor	:= crDefault;
		KiKeresDlg.ShowModal;
		KiKeresDlg.Free;
	end;
end;

{procedure TFomenuDlg.Munkavllalkpontozsa1Click(Sender: TObject);
var
  path, fn: string;
  ExcelApp: OleVariant;
begin
  if not EgyebDlg.user_super then exit;
	path:= EgyebDlg.Read_SZGrid( '999', '727' );
  if not DirectoryExists(path) then exit;
  OpenDialog1.InitialDir:=path;
  if not OpenDialog1.Execute then exit;
  Screen.Cursor:=crHourGlass;
  fn:=OpenDialog1.FileName;
  Try
    ExcelApp:=CreateOleObject('Excel.Application');
    ExcelApp.Workbooks.open(fn);
    ExcelApp.Visible:=True;
    Screen.Cursor:=crDefault;
    NoticeKi('A t�bl�zat megny�lt egy k�l�n Excel alkalmaz�s ablakban.');
  Except
  End;
  Screen.Cursor:=crDefault;
end;}

procedure TFomenuDlg.Kszletkezels1Click(Sender: TObject);
begin
{  Application.CreateForm(TFKeszlet, FKeszlet);
  FKeszlet.ShowModal;
  FKeszlet.Free;
 }
  Application.CreateForm(TKeszletFmDlg, KeszletFmDlg);
  KeszletFmDlg.ShowModal;
  KeszletFmDlg.Free;

end;

procedure TFomenuDlg.Kszletkezels2Click(Sender: TObject);
begin
	if ( (GetRightTag(569) <> RG_NORIGHT	) or EgyebDlg.user_super) then begin
    Application.CreateForm(TKeszletFmDlg, KeszletFmDlg);
    KeszletFmDlg.ShowModal;
    KeszletFmDlg.Free;
  end
  else begin
    NoticeKi('Nincs jogosults�g!');
  end;

end;

procedure TFomenuDlg.rfolyamtlt1Click(Sender: TObject);
begin
  Application.CreateForm(TArfolyamToltoDlg, ArfolyamToltoDlg);
  ArfolyamToltoDlg.ShowModal;
  ArfolyamToltoDlg.Free;

end;

procedure TFomenuDlg.Napok1Click(Sender: TObject);
begin
  Application.CreateForm(TFNapok, FNapok);
  FNapok.ShowModal;
  FNapok.Free;
end;

procedure TFomenuDlg.Szabadnapokmunkanapok1Click(Sender: TObject);
begin
  Application.CreateForm(TFNapok, FNapok);
  FNapok.ShowModal;
  FNapok.Free;
end;

procedure TFomenuDlg.AlGepkocsiClick(Sender: TObject);
begin
  Application.CreateForm(TAlGepFmDlg,AlGepFmDlg);
	AlGepFmDlg.ShowModal;
	AlGepFmDlg.Destroy;
end;

procedure TFomenuDlg.Alvllalkozvev1Click(Sender: TObject);
var
  kod:integer;
begin
  TVevo.Open;
  TAlvallal.Open;
  TAlvallal.First;
  kod:=4000;
  while not TAlvallal.Eof do
  begin
    TVevo.Append;
    TVevoV_KOD.Value:=IntToStr(kod);
    TVevoV_NEV.Value:=TAlvallalV_NEV.Value;
    TVevoV_IRSZ.Value:=TAlvallalV_IRSZ.Value;
    TVevoV_VAROS.Value:=TAlvallalV_VAROS.Value;
    TVevoV_CIM.Value:=TAlvallalV_CIM.Value;
    TVevoV_TEL.Value:=TAlvallalV_TEL.Value;
    TVevoV_FAX.Value:=TAlvallalV_FAX.Value;
    TVevoV_BANK.Value:=TAlvallalV_BANK.Value;
    TVevoV_ADO.Value:=TAlvallalV_ADO.Value;
    TVevoV_KEDV.Value:=TAlvallalV_KEDV.Value;
    TVevoV_UGYINT.Value:=TAlvallalV_UGYINT.Value;
    TVevoV_MEGJ.Value:=TAlvallalV_MEGJ.Value;
    TVevoV_LEJAR.Value:=TAlvallalV_LEJAR.Value;
    TVevoV_AFAKO.Value:=TAlvallalV_AFAKO.Value;
    TVevoV_ORSZ.Value:=TAlvallalV_ORSZ.Value;
    TVevoVE_EMAIL.Value:=TAlvallalV_EMAIL.Value;
    TVevo.Post;
    inc(kod);
    TAlvallal.Next;
  end;
  TVevo.Close;
  TAlvallal.Close;
  ShowMessage('K�sz!');
end;

procedure TFomenuDlg.Panel1DblClick(Sender: TObject);
begin
  ShowMessage(EgyebDlg.v_alap_db+#13+EgyebDlg.v_koz_db);
end;

procedure TFomenuDlg.BtnApplyClick(Sender: TObject);
begin
   if ListBoxLoadedStyles.ItemIndex>=0 then
   begin
     SetStyleByIndex(ListBoxLoadedStyles.ItemIndex);
     Application.CreateForm(TFInfo_, FInfo_);
   end;
end;

{procedure TFomenuDlg.Kresemnyektblzata1Click(Sender: TObject);
var
  fn: string;
begin
	fn:= EgyebDlg.Read_SZGrid( '999', '728' );
  if fn='' then exit;
  if not FileExists(fn) then exit;
  Try
    ShellExecute(Handle, 'open', PChar(fn), nil, PChar(fn), SW_SHOWDEFAULT);
  Except
    Screen.Cursor:=crDefault;
  End;
end;
}

{procedure TFomenuDlg.Alkatrszrendelsektblzata1Click(Sender: TObject);
var
  fn: string;
begin
	fn:= EgyebDlg.Read_SZGrid( '999', '729' );
  if fn='' then exit;
  if not FileExists(fn) then exit;
    ShellExecute(Handle, 'open', PChar(fn), nil, PChar(fn), SW_SHOWDEFAULT);
end;
}

procedure TFomenuDlg.RBexportClick(Sender: TObject);
begin
	Application.CreateForm(TRB_ExportDlg, RB_ExportDlg);
	RB_ExportDlg.ShowModal;
	RB_ExportDlg.Free;
end;

procedure TFomenuDlg.Rendkvliesemnyrgztse1Click(Sender: TObject);
begin
if Rendkvliesemnyrgztse1.Visible then begin
  Application.CreateForm(TRendkivuliEsemenybeDlg, RendkivuliEsemenybeDlg);
  RendkivuliEsemenybeDlg.ShowModal;
  RendkivuliEsemenybeDlg.Free;
  end; // if visible
end;

procedure TFomenuDlg.RendszSULYOK1Click(Sender: TObject);
begin
  if MessageBox(0, 'MEHET ?', '', MB_ICONWARNING or MB_YESNO or MB_DEFBUTTON2)=IDNO then
    exit;
    
  SULYOK.Open;
  SULYOK.First;
  while not SULYOK.Eof do
  begin
    JARAT.Close;
    JARAT.Parameters[0].Value:=SULYOKSU_JAKOD.Value;
    JARAT.Open;
    JARAT.First;
    if JARATja_rendsz.Value<>'' then
    begin
      SULYOK.Edit;
      SULYOKSU_RENDSZ.Value:=JARATja_rendsz.Value;
      SULYOK.Post;
    end;
    SULYOK.Next;
  end;
  SULYOK.Close;
  JARAT.Close;
  ShowMessage('K�sz!');
end;

{procedure TFomenuDlg.SHELLllomslista1Click(Sender: TObject);
var
  fn: string;
begin
	fn:= EgyebDlg.Read_SZGrid( '999', '750' );
  if fn='' then exit;
  if not FileExists(fn) then exit;
    ShellExecute(Handle, 'open', PChar(fn), nil, PChar(fn), SW_SHOWDEFAULT);
end;}

procedure TFomenuDlg.GkKatKltsg1Click(Sender: TObject);
begin
  KOLTSEG.Open;
  KOLTSEG.First;
  while not KOLTSEG.Eof do
  begin
    KOLTSEG.Edit;
    KOLTSEGKS_THELY.Value:='';
    KOLTSEG.Post;

    KOLTSEG.Next;
  end;
  KOLTSEG.Close;
  showmessage('K�sz!');
end;

procedure TFomenuDlg.Sajtkonfigbelltstrlse1Click(Sender: TObject);
begin
  if NoticeKi('K�ri a be�ll�t�sok t�rl�s�t?', NOT_QUESTION) = 0 then
  begin
    if FileExists(EgyebDlg.userini) then
    begin
      if DeleteFile(Pchar(EgyebDlg.userini)) then
        ShowMessage('K�sz. Ind�tsa �jra a programot!')
      else
        ShowMessage('A t�rl�s nem siker�lt!');
    end;
  end;
end;

procedure TFomenuDlg.Kltsgrtkszmols1Click(Sender: TObject);
var
  arf: double;
begin
  if MessageBox(0, 'Mehet?', '', MB_ICONQUESTION or MB_YESNO or MB_DEFBUTTON2)=IDNO then
    exit;
  KOLTSERTEK.Close;
  KOLTSERTEK.Open;
  KOLTSERTEK.First;
  while not KOLTSERTEK.Eof do
  begin
    if KOLTSERTEKKS_VALNEM.Value='HUF' then
      arf:=1        
    else
      arf:= EgyebDlg.ArfolyamErtek(KOLTSERTEKKS_VALNEM.Value,KOLTSERTEKKS_DATUM.Value,false);
    if arf>0 then
    begin
     Try
      KOLTSERTEK.Edit;
      KOLTSERTEKKS_ERTEK.Value:=RoundTo( KOLTSERTEKKS_OSSZEG.Value * arf , 0) ;
      KOLTSERTEKKS_ARFOLY.Value:=arf;
      KOLTSERTEK.Post;
      ShowMessage(KOLTSERTEKKS_DATUM.AsString);
     Except
     End;
    end;
    KOLTSERTEK.Next;
  end;
  KOLTSERTEK.Close;
  ShowMessage('K�sz!');
end;

procedure TFomenuDlg.Klds1Click(Sender: TObject);
begin
 	if Klds1.Visible then begin
     Screen.Cursor	:= crHourGlass;
     // Application.CreateForm(TUzenetSzerkesztoDlg, UzenetSzerkesztoDlg);
     // UzenetSzerkesztoDlg.CsoportListaTolt;
     UzenetSzerkesztoDlg.Reset;
     UzenetSzerkesztoDlg.EagleSMSTipus:= uzleti;
     Screen.Cursor:= crDefault;
     UzenetSzerkesztoDlg.ShowModal;
     // UzenetSzerkesztoDlg.Free;
  end;
end;

procedure TFomenuDlg.Klsllomnyokmegnyitsa1Click(Sender: TObject);
begin
  with TKulsoFileDlg.Create(Self) do begin
     ShowModal;
     Free;
     end; // with

end;

procedure TFomenuDlg.Kltsgekimportlsa1Click(Sender: TObject);
begin
   Screen.Cursor := crHourGlass;
   // A sz�r�si param�ter bell�t�sa -> dial�gus 595
   EgyebDlg.SetSzuroStr('595','KS_JAKOD = '''' ');
   Application.CreateForm(TKoltImpUjFmDlg, KoltImpUjFmDlg);
   Screen.Cursor := crDefault;
   KoltImpUjFmDlg.ShowModal;
   KoltImpUjFmDlg.Free;
end;

procedure TFomenuDlg.Kltsgektrlsejrafeldolgozshoz1Click(Sender: TObject);
begin
  Application.CreateForm(TFKtsgDel, FKtsgDel);
  FKtsgDel.ShowModal;
  FKtsgDel.Free;
end;

procedure TFomenuDlg.LOADID1Click(Sender: TObject);
begin
  Application.CreateForm(TLoadIDjFmDlg, LoadIDjFmDlg);
  LoadIDjFmDlg.ShowModal;
  LoadIDjFmDlg.Free;
end;

{procedure TFomenuDlg.Mszakiproblmk1Click(Sender: TObject);
var
  fn: string;
begin
	fn:= EgyebDlg.Read_SZGrid( '999', '751' );
  if fn='' then exit;
  if not FileExists(fn) then exit;
    ShellExecute(Handle, 'open', PChar(fn), nil, PChar(fn), SW_SHOWDEFAULT);
end;
 }

procedure TFomenuDlg.Paletta1Click(Sender: TObject);
begin
  Application.CreateForm(TPalettaFmDlg, PalettaFmDlg);
  PalettaFmDlg.ShowModal;
  PalettaFmDlg.Free;
end;

procedure TFomenuDlg.Palettavltozsok1Click(Sender: TObject);
begin
  Application.CreateForm(TPalvaltFmDlg, PalvaltFmDlg);
  PalvaltFmDlg.ShowModal;
  PalvaltFmDlg.Free;
end;

procedure TFomenuDlg.Palettavltozsoklistja1Click(Sender: TObject);
begin
   // Kimutat�s a paletta v�ltoz�sokr�l
   Application.CreateForm(TKiPalettaDlg, KiPalettaDlg);
   KiPalettaDlg.ShowModal;
   KiPalettaDlg.Free;
end;

procedure TFomenuDlg.SpeedButton6Click(Sender: TObject);
begin
	if ( (GetRightTag(570) <> RG_NORIGHT	) or EgyebDlg.user_super) then begin
    Application.CreateForm(TPalettaFmDlg, PalettaFmDlg);
    PalettaFmDlg.ShowModal;
    PalettaFmDlg.Free;
  end
  else begin
    NoticeKi('Nincs jogosults�g!');
  end;
end;

procedure TFomenuDlg.JARATMEGSEGED1Click(Sender: TObject);
var
  jaratszam: string;
  msmssorsz,mbdb: integer;
  mskod,mbkod,fojar,jakod,msfajta,msdb: string;
begin
  if (MessageBox(0, 'Mehet a feldolgoz�s?', '', MB_ICONQUESTION or MB_YESNO or MB_DEFBUTTON2) in [idNo]) then    exit;
	EgyebDLg.SetADOQueryDatabase(Query2);
	EgyebDLg.SetADOQueryDatabase(Query33);
  JARAT3.Close;
  JARAT3.Open;
  JARAT3.First;
  mbdb:=0;
  while not JARAT3.Eof do
  begin
    if JARAT3JA_FAZIS.Value=2 then  // STORNO
    begin
      JARAT3.Next;
      Continue;
    end;
      {  Query_Run(EgyebDlg.QueryAlap,'select distinct ms_mbkod,ms_sorsz from megseged '+
        'where MS_RENSZ ='''+JARAT3JA_RENDSZ.Value+''' and ((MS_LETDAT>='''+JARAT3JA_JKEZD.AsString+''' and MS_LETDAT<='''+JARAT3JA_JVEGE.AsString+''')or(MS_FETDAT>='''+JARAT3JA_JKEZD.AsString+''''+
        ' and MS_FETDAT<='''+JARAT3JA_JVEGE.AsString +''')) and MS_FAJKO>0 '+' and ((MS_JASZA='''+''')or(MS_JASZA is null))'+' order by MS_MBKOD,MS_SORSZ') ;
             }

        Query_Run(EgyebDlg.QueryAlap,'select distinct ms_mbkod,ms_sorsz,ms_fajta from megseged '+
        'where MS_RENSZ ='''+JARAT3JA_RENDSZ.Value+''' and ('+
        '((MS_LETDAT+MS_LETIDO)>='''+JARAT3JA_JKEZD.AsString+JARAT3JA_KEIDO.AsString+''' and (MS_LETDAT+MS_LETIDO)<='''+JARAT3JA_JVEGE.AsString+JARAT3JA_VEIDO.AsString+''')and'+
        '((MS_FETDAT+MS_FETIDO)>='''+JARAT3JA_JKEZD.AsString+JARAT3JA_KEIDO.AsString+''' and (MS_FETDAT+MS_FETIDO)<='''+JARAT3JA_JVEGE.AsString+JARAT3JA_VEIDO.AsString+'''))'+
        ' and MS_FAJKO>0 '+' and ((MS_JASZA='''+''')or(MS_JASZA is null))'+' order by MS_MBKOD,MS_SORSZ') ;

        msfajta:='';
        msdb:='0';
        if not EgyebDlg.QueryAlap.Eof then msdb:=IntToStr(EgyebDlg.QueryAlap.RecordCount);      // darab
        while not EgyebDlg.QueryAlap.Eof do
        begin
            if pos(EgyebDlg.QueryAlap.FieldByName('MS_FAJTA').AsString,msfajta)=0 then
              msfajta:=msfajta+ EgyebDlg.QueryAlap.FieldByName('MS_FAJTA').AsString+', ';

//          	jaratszam	:= JARAT3JA_ORSZ.Value + '-'+ JARAT3JA_ALKOD.AsString;
          	jaratszam	:= Getjaratszam(JARAT3.FieldByName('JA_KOD').AsString);
            mbkod:= EgyebDlg.QueryAlap.FieldByName('MS_MBKOD').AsString;
            msmssorsz:= EgyebDlg.QueryAlap.FieldByName('MS_SORSZ').AsInteger;
            mskod:= Query_Select2('MEGSEGED','MS_MBKOD','MS_SORSZ',mbkod,IntToStr(msmssorsz),'MS_MSKOD');
            Query_Run(Query2, 'select mb_jakod from megbizas where mb_mbkod='+mbkod );
            fojar:=Query2.fieldbyname('MB_JAKOD').AsString;
           // fojar:=Query_Select('MEGBIZAS','MB_KOD',mbkod,'MB_JAKOD' );
            jakod:= JARAT3JA_KOD.AsString;

          Try

            if not Query_Update( Query2, 'MEGSEGED', [
          			'MS_JAKOD', ''''+JARAT3JA_KOD.AsString+'''',
          			'MS_JASZA', ''''+jaratszam+''''
        	    	], ' WHERE MS_MBKOD = '+mbkod+' and MS_FAJKO>0'+' and MS_SORSZ='+IntToStr( msmssorsz)) then Raise Exception.Create('');    //


            if not Query_Update( Query2, 'JARAT', [
          			'JA_MSFAJ', ''''+msfajta+'''',
                'JA_MSDB',msdb
        	    	], ' WHERE JA_KOD = '+JARAT3JA_KOD.AsString) then Raise Exception.Create('');    //

          	// A megbizasok hozz�kapcsol�sa a j�rathoz
          	if not Query_Insert (Query33, 'JARATMEGSEGED',[
          				'JG_FOJAR', ''''+fojar+'''',
          				'JG_JAKOD', ''''+jakod+'''',
          				'JG_MBKOD', mbkod,
          				'JG_MSKOD', mskod
          				]) then Raise Exception.Create('');
            inc(mbdb);
          Except
          END;
          EgyebDlg.QueryAlap.Next;
        end;
    Label10.Caption:=JARAT3JA_JKEZD.AsString +'  '+IntToStr(mbdb);
    Label10.Update;
    Application.ProcessMessages;


    JARAT3.Next;
  end;
  JARAT3.Close;


{
  Application.CreateForm(TViszonybeDlg, ViszonybeDlg);
  fn:=ExtractFilePath(Application.ExeName)+'\error.log';
  if FileExists(fn) then
    DeleteFile(Pchar(fn));
  MEGSEGED.Open;
  MEGSEGED.First;
  rdb:=0;
  sdb:=0;
  tdb:=0;
  while not MEGSEGED.Eof do
  begin
    inc(sdb);
    FETDAT:=MEGSEGEDMS_FETDAT.Value;
    FETIDO:=MEGSEGEDMS_FETIDO.Value;
    LETDAT:=MEGSEGEDMS_LETDAT.Value;
    LETIDO:=MEGSEGEDMS_LETIDO.Value;
    FAJKO :=MEGSEGEDMS_FAJKO.AsString;

    MBKOD:=MEGSEGEDMS_MBKOD.AsString;

    if MEGSEGEDMS_FAJKO.Value=1 then
    begin
      FAJTA:='ER';
      LETDAT:=MEGSEGEDMS_FETDAT.Value;
      LETIDO:=MEGSEGEDMS_FETIDO.Value;
    end;
    if MEGSEGEDMS_FAJKO.Value=2 then
    begin
      FAJTA:='EH';
      FETDAT:=MEGSEGEDMS_LETDAT.Value;
      FETIDO:=MEGSEGEDMS_LETIDO.Value;
    end;
    if MEGSEGEDMS_FAJKO.Value=3 then
    begin
      FAJTA:='LE';
      FETDAT:=MEGSEGEDMS_LETDAT.Value;
      FETIDO:=MEGSEGEDMS_LETIDO.Value;
    end;
    JARAT2.Close;
    JARAT2.Parameters[0].Value:=MEGSEGEDMS_RENSZ.Value;
    JARAT2.Parameters[1].Value:=FETDAT+FETIDO;
    JARAT2.Parameters[2].Value:=FETDAT+FETIDO;
    JARAT2.Parameters[3].Value:=LETDAT+LETIDO;
    JARAT2.Parameters[4].Value:=LETDAT+LETIDO;
    JARAT2.Open;
    JARAT2.First;
    jakod:='';
    jasza:='';
    if not JARAT2.Eof then
    begin
      db:=JARAT2.RecordCount;
      if db=1 then
      begin
        jakod:=JARAT2JA_KOD.Value;
        jasza:=JARAT2JA_ORSZ.Value+'-'+JARAT2JA_ALKOD.AsString;
        JARAT2JA_RENDSZ.Value;
        JARAT2JA_JKEZD.Value;
        JARAT2JA_KEIDO.Value;
        JARAT2JA_JVEGE.Value;
        JARAT2JA_VEIDO.Value;

      	ViszonybeDlg.ToltMegbizas(MBKOD ,jakod);
        ViszonybeDlg.BitElkuld.OnClick(self);
      	if ViszonybeDlg.ret_kod <> '' then begin
      		// L�trej�tt az �j viszonylat, bejegyezz�k a megb�z�sba is
          MEGSEGED.Edit;
          MEGSEGEDMS_JAKOD.Value:=jakod;
          MEGSEGEDMS_JASZA.Value:=jasza;
          MEGSEGED.Post;
        end;

    		// A megbizasok hozz�kapcsol�sa a j�rathoz
		    Query_Run( Query2,  'SELECT COUNT(*) DB FROM JARATMEGBIZAS WHERE JM_JAKOD = '''+jakod+''' ');
    		sorsz	:= StrToIntDef(Query2.FieldByName('DB').AsString, 1) + 1;
		    Query_Run( Query2,  'SELECT DISTINCT MB_JAKOD, MS_MBKOD, MS_SORSZ FROM MEGSEGED, MEGBIZAS WHERE MS_MBKOD = MB_MBKOD AND MB_JAKOD <> '''' '+
    			' AND MB_MBKOD = '+MBKOD+' ORDER BY MB_JAKOD, MS_MBKOD, MS_SORSZ ', TRUE);
		    while not Query2.Eof do begin
			    Query_Insert (Query33, 'JARATMEGBIZAS',
				  [
  				'JM_JAKOD', ''''+Query2.FieldByName('MB_JAKOD').AsString+'''',
	  			'JM_MBKOD', Query2.FieldByName('MS_MBKOD').AsString,
		  		'JM_SORSZ', IntToStr(sorsz),
			  	'JM_ORIGS', Query2.FieldByName('MS_SORSZ').AsString
				  ]);
    			Inc(sorsz);
		    	Query2.Next;
		    end;
   //     Query_Run(Query2,'UPDATE JARAT set JA_FAJKO='+FAJKO+' where JA_KOD='+jakod);
      end
      else
      begin
        jakod:='T�bbJ';
        EgyebDlg.WriteLogFile(fn,'T�BB  '+MEGSEGEDMS_MBKOD.AsString+' '+FAJTA+' '+MEGSEGEDMS_RENSZ.Value+' '+MEGSEGEDMS_FETDAT.Value+MEGSEGEDMS_FETIDO.Value+'-'+MEGSEGEDMS_LETDAT.Value+MEGSEGEDMS_LETIDO.Value);
        inc(tdb);
      end;
 //     MEGSEGED.Edit;
  //    MEGSEGEDMS_JAKOD.Value:=jakod;
    //  MEGSEGEDMS_JASZA.Value:='';
  //    MEGSEGED.Post;
    end
    else
    begin
      EgyebDlg.WriteLogFile(fn,'HI�NY '+MEGSEGEDMS_MBKOD.AsString+' '+FAJTA+' '+MEGSEGEDMS_RENSZ.Value+' '+MEGSEGEDMS_FETDAT.Value+MEGSEGEDMS_FETIDO.Value+'-'+MEGSEGEDMS_LETDAT.Value+MEGSEGEDMS_LETIDO.Value);
      inc(rdb);
    end;
    Label10.Caption:=MEGSEGEDMS_FETDAT.AsString+'  '+IntToStr(sdb)+'  '+IntToStr(rdb)+'  '+IntToStr(tdb);
    Label10.Update;
    Application.ProcessMessages;
    MEGSEGED.Next;
  end;
  MEGSEGED.Close;
  JARAT2.Close;
 	ViszonybeDlg.Free;

  }
  ShowMessage('K�sz!');
end;

procedure TFomenuDlg.Jratokvisszazrsa1Click(Sender: TObject);
var
  S: string;
begin
  if MessageBox(0, 'Mehet?', '', MB_ICONQUESTION or MB_YESNO or MB_DEFBUTTON2)=IDNO then
    exit;

  Try
    // ADOCommand1.Execute;
    S:= 'update jarat set ja_fazis=1,ja_fazst=''Ellen�rz�tt'' where ja_fazis= -2';
    Command_Run(ADOCommand1, S, true);  // 20150218 NagyP

    ShowMessage('K�sz!');
  Except
    ShowMessage('NEM siker�lt!');
  End;
end;

procedure TFomenuDlg.LoadIDmegtekintse1Click(Sender: TObject);
begin
  Application.CreateForm(TLoadIDjFmDlg, LoadIDjFmDlg);
  LoadIDjFmDlg.ShowModal;
  LoadIDjFmDlg.Free;

end;

procedure TFomenuDlg.LoadRegisteredStyles;
Var
 Name  : string;
begin
    for Name in TStyleManager.StyleNames do
     if ListBoxLoadedStyles.Items.IndexOf(Name)=-1 then
      ListBoxLoadedStyles.Items.Add(Name);
end;

procedure TFomenuDlg.elepiteletankokjavtsa1Click(Sender: TObject);
begin
  if MessageBox(0, 'Mehet?', '', MB_ICONQUESTION or MB_YESNO or MB_DEFBUTTON2)=IDNO then
    exit;
  TTANK.Open;
  TTANK.First;
  while not TTANK.Eof do
  begin
    TTANK2.Close;
    TTANK2.Parameters[0].Value:=TTANKks_rendsz.Value;
    TTANK2.Parameters[1].Value:=TTANKks_kmora.Value;
    TTANK2.Open;
    TTANK2.First;
    TTANK2.Next;
    while not TTANK2.Eof do
    begin
      TTANK2.Edit;
      TTANK2KS_TELE.Value:=0;
      TTANK2.Post;
      TTANK2.Next;
    end;
    TTANK.Next;
  end;
  TTANK2.Close;
  TTANK.Close;
  ShowMessage('K�sz!');
end;

procedure TFomenuDlg.MNBArfolyamteszt1Click(Sender: TObject);
const
  FejlecSorokSzama = 2;   // az "ArfolyamJelentesSzoveg" fejl�c sorainak sz�ma
var
  XMLResult, ErrorString, HibaVoltS: string;
  MySOAP: MNBArfolyamServiceSoap;
  i: integer;
  hibavolt: boolean;
begin
  MySOAP:= GetMNBArfolyamServiceSoap(ErrorString, '', False, '', nil);
  XMLResult:= MySOAP.GetCurrentExchangeRates;
  NoticeKi(XMLResult);
end;

procedure TFomenuDlg.MSJASZAjavtsa1Click(Sender: TObject);
var
  db:integer;
begin
  if MessageBox(0, 'Mehet?', '', MB_ICONQUESTION or MB_YESNO or MB_DEFBUTTON2)=IDNO then
    exit;
  ADOQuery2.Open;
  db:=0;
  while not ADOQuery2.Eof do
  begin
    Query_Run(Query22,'UPDATE  megseged set ms_jasza='''+''''+',ms_jakod='''+''''+' where ms_id='+ADOQuery2ms_id.AsString);
    inc(db);
    ADOQuery2.Next;
  end;
  ADOQuery2.Close;

  ShowMessage('K�sz! '+IntToStr(db));
end;

procedure TFomenuDlg.Szmlaszmellenrzs1Click(Sender: TObject);
var
  hiba     : string;
  szam,k,v,i: integer;
begin
  if MessageBox(0, 'Mehet?', '', MB_ICONQUESTION or MB_YESNO or MB_DEFBUTTON2)=IDNO then
    exit;
  ADOSZFEJ.Open;
  ADOSZFEJ.Last;
  v:=ADOSZFEJsa_kod.AsInteger;
  ADOSZFEJ.First;
  szam:=ADOSZFEJsa_kod.AsInteger;
  k:=szam;
  for i:=k to v do
  begin
    if i=ADOSZFEJsa_kod.AsInteger then  //OK
    begin
      ADOSZFEJ.Next;
    end
    else
    begin
      hiba:=hiba+','+ADOSZFEJsa_kod.AsString;
    end;
  end;
  ADOSZFEJ.Close;
  if hiba='' then
    ShowMessage('K�SZ! OK.')
  else
    ShowMessage('Hi�nyz�k: '+hiba);
end;

procedure TFomenuDlg.kiv;
begin
    Raise Exception.Create('');

end;

procedure TFomenuDlg.Jratellenrzs1Click(Sender: TObject);
begin
  Application.CreateForm(TFJarat_ell, FJarat_ell);
  FJarat_ell.ShowModal;
  FJarat_ell.Free;
end;

procedure TFomenuDlg.Megbzsjraszmols1centekmiatt1Click(Sender: TObject);
var
   S, Mettol, Meddig: string;
   MBKOD, i: integer;
   OldCursor: TCursor;
begin
   if NoticeKi('�jrasz�moljuk a megb�z�sokat, ahol MB_EUDIJ �s MB_SZDIJ k�z�tt kicsi az elt�r�s. Folytatja?', NOT_QUESTION) <> 0 then begin
       Exit;  // ha nem "igen" a v�lasz
       end;
   OldCursor:= Screen.Cursor;  // elmentj�k
   Screen.Cursor	:= crHourGlass;
   FormGaugeDlg:= TFormGaugeDlg.Create(Self);
   FormGaugeDlg.GaugeNyit('Megb�z�sok �jrasz�m�t�sa ...' , '', false);
   i:=0;
   EgyebDlg.SetADOQueryDatabase(QueryAlap);
   S:='SELECT MB_MBKOD FROM MEGBIZAS WHERE ';
   S:=S+' abs(MB_EUDIJ - MB_SZDIJ) between 0.01 and 2 ';
   // S:=S+' AND MB_MBKOD in (41427) ';
   S:=S+' ORDER BY 1 DESC';
   Query_Run(QueryAlap, S, true);

   with QueryAlap do begin
     while not Eof do begin
        MBKOD:= FieldByName('MB_MBKOD').AsInteger;
        MegbizasKieg(IntToStr(MBKOD));  // �jrasz�molja az EUR/km �s t�rsai �rt�keket
        Next;
        Inc(i);
        FormGaugeDlg.Pozicio (( i * 100 ) div RecordCount) ;
        Application.ProcessMessages;
        end;  // while
     end;  // with
   FormGaugeDlg.Destroy;
   Screen.Cursor	:= OldCursor;
end;

procedure TFomenuDlg.MegbzsjraszmolsMegbizaskieg1Click(Sender: TObject);
var
   S, Mettol, Meddig: string;
   MBKOD, i: integer;
   OldCursor: TCursor;
begin
   Mettol := InputBox('�jrasz�m�t�s', 'D�tum -t�l', '2015.01.01.');
   Meddig := InputBox('�jrasz�m�t�s', 'D�tum -ig', EgyebDlg.MaiDatum);
   if NoticeKi('�jrasz�moljuk a megb�z�sokat '+Mettol+' �s '+ Meddig + ' k�z�tt. Folytatja?', NOT_QUESTION) <> 0 then begin
       Exit;  // ha nem "igen" a v�lasz
       end;
   OldCursor:= Screen.Cursor;  // elmentj�k
   Screen.Cursor	:= crHourGlass;
   FormGaugeDlg:= TFormGaugeDlg.Create(Self);
   FormGaugeDlg.GaugeNyit('Megb�z�sok �jrasz�m�t�sa ...' , '', false);
   i:=0;
   EgyebDlg.SetADOQueryDatabase(QueryAlap);
   S:='SELECT MB_MBKOD FROM MEGBIZAS WHERE ';
   S:=S+' MB_DATUM BETWEEN '''+Mettol+''' and '''+Meddig+''' ';
   // S:=S+' AND MB_MBKOD in (41427) ';
   S:=S+' ORDER BY 1 DESC';
   Query_Run(QueryAlap, S, true);

   with QueryAlap do begin
     while not Eof do begin
        MBKOD:= FieldByName('MB_MBKOD').AsInteger;
        MegbizasKieg(IntToStr(MBKOD));  // �jrasz�molja az EUR/km �s t�rsai �rt�keket
        Next;
        Inc(i);
        FormGaugeDlg.Pozicio (( i * 100 ) div RecordCount) ;
        Application.ProcessMessages;
        end;  // while
     end;  // with
   FormGaugeDlg.Destroy;
   Screen.Cursor	:= OldCursor;
end;

procedure TFomenuDlg.MegbzsKiegegymegbzsra1Click(Sender: TObject);
var
   S, MBKOD: string;
   OldCursor: TCursor;
begin
   S := InputBox('�jrasz�m�t�s', 'Megb�z�s k�d:', '');
   if NoticeKi('�jrasz�moljuk a megb�z�st?', NOT_QUESTION) <> 0 then begin
       Exit;  // ha nem "igen" a v�lasz
       end;
   OldCursor:= Screen.Cursor;  // elmentj�k
   Screen.Cursor	:= crHourGlass;
   while S <> '' do begin
     MBKOD:= EgyebDlg.SepFronttoken(',', S);
     S:= EgyebDlg.SepRest(',', S);
     MegbizasKieg(MBKOD);  // �jrasz�molja az EUR/km �s t�rsai �rt�keket
     end;
   Screen.Cursor	:= OldCursor;
end;

procedure TFomenuDlg.Megbzsoklistja1Click(Sender: TObject);
begin
  Application.CreateForm(TMegbizasFmDlg, MegbizasFmDlg);
  // a "Sz�r�sek" gombon aktiv�lhat� "behegesztett" sz�r�sek
  {
  MegbizasFmDlg.FixSzuresNev.Add('Elt�r� "EUR d�j" �s "Sz�mla EUR"');
  MegbizasFmDlg.FixSzuresSQL.Add('abs((MB_EUDIJ)-(MB_SZDIJ)) > 1');
  MegbizasFmDlg.FixSzuresNev.Add('"EUR d�j" = 0, "Sz�mla EUR" = 0');
  MegbizasFmDlg.FixSzuresSQL.Add('(MB_EUDIJ) = 0 and (MB_SZDIJ) = 0');
  }
  MegbizasFmDlg.FixSzuresNev.Add('Elt�r� "EUR d�j" �s "Sz�mla EUR" vagy mindkett� 0');
  MegbizasFmDlg.FixSzuresSQL.Add(fix_szures_blokk_eleje+'(abs((MB_EUDIJ)-(MB_SZDIJ)) >= 0.01 or ((MB_EUDIJ) = 0 and (MB_SZDIJ) = 0))'+fix_szures_blokk_vege);

  MegbizasFmDlg.InitSzuresek;
  // -----------------------------------
  MegbizasFmDlg.ShowModal;
  MegbizasFmDlg.Free;
end;

procedure TFomenuDlg.Megbzsthosszdjakjraszmols1Click(Sender: TObject);
var
   S, Mettol, Meddig: string;
   MBKOD, i: integer;
   OldCursor: TCursor;
begin
   Mettol := InputBox('�jrasz�m�t�s', 'D�tum -t�l', '2015.01.01.');
   Meddig := InputBox('�jrasz�m�t�s', 'D�tum -ig', EgyebDlg.MaiDatum);
   // if NoticeKi('�jrasz�moljuk az "�sszes megb�z�s" �s a TAVKM=0 megb�z�sokat '+Mettol+' �s '+ Meddig + ' k�z�tt. Folytatja?', NOT_QUESTION) <> 0 then begin
if NoticeKi('�jrasz�moljuk az "MB_NOCSOP = 1" megb�z�sokat '+Mettol+' �s '+ Meddig + ' k�z�tt. Folytatja?', NOT_QUESTION) <> 0 then begin
       Exit;  // ha nem "igen" a v�lasz
       end;
   OldCursor:= Screen.Cursor;  // elmentj�k
   Screen.Cursor	:= crHourGlass;
   FormGaugeDlg:= TFormGaugeDlg.Create(Self);
   FormGaugeDlg.GaugeNyit('Megb�z�sok �jrasz�m�t�sa ...' , '', false);
   i:=0;
   EgyebDlg.SetADOQueryDatabase(QueryAlap);
   S:='SELECT MB_MBKOD FROM MEGBIZAS WHERE ';
   S:=S+' MB_DATUM BETWEEN '''+Mettol+''' and '''+Meddig+''' ';
   // S:=S+' AND (MB_TAVMI is not null) and (ltrim(MB_TAVMI)<>'''') ';
   // S:=S+' AND ((MB_TAVMI like ''�sszes megb�z�s%'') or (MB_TAVKM=0)) ';
   S:=S+' AND (isnull(MB_NOCSOP, 0) = 1) ';
   S:=S+' ORDER BY 1 DESC';
   Query_Run(QueryAlap, S, true);

   with QueryAlap do begin
     while not Eof do begin
        MBKOD:= FieldByName('MB_MBKOD').AsInteger;
        MegbizasKieg_Utvonal_is(IntToStr(MBKOD));  // �jrasz�molja az �tvonal hosszt (csoportbesorol�ssal) + az EUR/km �s t�rsai �rt�keket
        Next;
        Inc(i);
        FormGaugeDlg.Pozicio (( i * 100 ) div RecordCount) ;
        Sleep(1000);   //  "100 elements per 10 seconds, 2500 elements per 24 hour period."
        Application.ProcessMessages;
        end;  // while
     end;  // with
   FormGaugeDlg.Destroy;
   Screen.Cursor	:= OldCursor;
end;

procedure TFomenuDlg.Pontozknyilvntartsa1Click(Sender: TObject);
begin
  Application.CreateForm(TPontozoFmDlg, PontozoFmDlg);
  PontozoFmDlg.ShowModal;
  PontozoFmDlg.Free;
end;

procedure TFomenuDlg.Pontozsstatisztika1Click(Sender: TObject);
begin
  if Pontozsstatisztika1.Visible then begin
	  Screen.Cursor	:= crHourGlass;
	  Application.CreateForm(TPontStatFmDlg, PontStatFmDlg);
	  Screen.Cursor	:= crDefault;
	  PontStatFmDlg.ShowModal;
	  PontStatFmDlg.Free;
  end;
end;

procedure TFomenuDlg.NavCKltsgtelepitank1Click(Sender: TObject);
var
  navc_id, navc_gprs_id, navc_datum, navc_ido, sql: string;
  navc_uza_lit,navc_uza_szint, navc_km, feszultseg: double;
  db,pdb, hdb :integer;
  ism, gyujtasbe: boolean;
begin
  if (MessageBox(0, 'Mehet a feldolgoz�s?', '', MB_ICONQUESTION or MB_YESNO or MB_DEFBUTTON2) in [ idNo]) then
    exit;

  db:=0;                        
  KTSG.Open;
  KTSG.First;
  while not KTSG.Eof do
  begin
    Try
      Label5.Caption:=KTSGKS_DATUM.Value;
      Label5.Update;
      navc_uza_lit:=0;
      navc_uza_szint:=0;
      navc_km:=0;
      navc_datum:='';
      navc_ido:='';
      navc_id:='';
      NAVC_unit.Close;
      NAVC_unit.Parameters[0].Value:=KTSGKS_RENDSZ.Value ;
      NAVC_unit.Open;
      NAVC_unit.First;
      if NAVC_unit.RecordCount=1 then
        navc_id:=NAVC_unitunit_id.AsString;
      NAVC_unit.Close;
      if navc_id='' then
      begin
        KTSG.Next;
        Continue;
      end;
      if navc_id<>'' then
      begin
        NAVC_GPRS.Close;
        NAVC_GPRS.SQL.Clear;
        //sql:='select TOP 1 * from gprs_'+navc_id+' where date_time >= CAST( '''+copy(KTSGKS_DATUM.AsString,1,10)+' '+KTSGKS_IDO.AsString+':00'''+' AS DATETIME) order by date_time' ;
        sql:='select * from gprs_'+navc_id+' where date_time >= CAST( '''+copy(KTSGKS_DATUM.AsString,1,10)+' '+KTSGKS_IDO.AsString+':00'''+' AS DATETIME) order by date_time' ;
        //NAVC_GPRS.SQL.Add('select TOP 1 * from gprs_'+navc_id+' where date_time >= CAST( '''+copy(KTSGKS_DATUM.AsString,1,10)+' '+KTSGKS_IDO.AsString+':00'''+' AS DATETIME) order by date_time' );
        NAVC_GPRS.SQL.Add('select * from gprs_'+navc_id+' where date_time >= CAST( '''+copy(KTSGKS_DATUM.AsString,1,10)+' '+KTSGKS_IDO.AsString+':00'''+' AS DATETIME) order by date_time' );
        NAVC_GPRS.Open;
        NAVC_GPRS.First;
        ism:=not NAVC_GPRS.Eof;
        pdb:=0;
       while ism do
       begin
        inc(pdb);
        navc_gprs_id:=NAVC_GPRSgprs_id.AsString;
        navc_datum:=copy( NAVC_GPRSdate_time.AsString,1,11);
        navc_ido:=Trim(copy( NAVC_GPRSdate_time.AsString,12,8));
        //NAVC_GPRS.Close;
        if navc_gprs_id='' then
        begin
          KTSG.Next;
          Continue;
        end;
        if navc_gprs_id<>'' then
        begin
          NAVC_IO.SQL.Clear;
          NAVC_IO.SQL.Add('select * from io_'+navc_id+' where recordid='+navc_gprs_id );
          NAVC_IO.Open;
          NAVC_IO.First;
          gyujtasbe:=False;
          while not NAVC_IO.Eof do
          begin
            if NAVC_IOioid.Value=1 then
              gyujtasbe:=NAVC_IOiovalue.Value=1;
            if NAVC_IOioid.Value=66 then
              feszultseg:=NAVC_IOiovalue.Value;
            if (NAVC_IOioid.Value=86)or(NAVC_IOioid.Value=145) then
              navc_uza_lit:=NAVC_IOiovalue.Value;
            if (NAVC_IOioid.Value=87)or(NAVC_IOioid.Value=147) then
              navc_uza_szint:=NAVC_IOiovalue.Value;
            if (NAVC_IOioid.Value=112)or(NAVC_IOioid.Value=150) then
              navc_km:=NAVC_IOiovalue.Value;

            NAVC_IO.Next
          end;
          NAVC_IO.Close;
          ism:=False;
          if (not gyujtasbe)or((feszultseg<27)and(feszultseg>20))or(navc_uza_szint>100) then
            ism:=true;
          if ((feszultseg<20)and(feszultseg>0))or(pdb>100) then
            ism:=False;
        end;
        NAVC_GPRS.Next;
        if NAVC_GPRS.Eof then
          ism:=False;
       end;
      end;
    Except
      KTSG.Next;
      Continue;
    End;
    // t�rol�s
    Try
    KTSG.Edit;
    KTSGKS_NC_KM.Value:=RoundTo(navc_km,0);
    KTSGKS_NC_USZ.Value:=RoundTo( navc_uza_szint,0);
    KTSGKS_NC_UME.Value:=RoundTo(navc_uza_lit,0);
    KTSGKS_NC_DAT.Value:=navc_datum;
    KTSGKS_NC_IDO.Value:=navc_ido;
    KTSG.Post;
    inc(db);
    Except
      inc(hdb);
      KTSG.Cancel;
    End;
    KTSG.Next;
  end;
  KTSG.Close;
  ShowMessage('K�sz  '+IntToStr(db)+' '+IntToStr(hdb));
end;

procedure TFomenuDlg.resenfutottKM1Click(Sender: TObject);
begin
  Application.CreateForm(TUjaratFormDlg, UjaratFormDlg);
  UjaratFormDlg.ShowModal;
  UjaratFormDlg.Free;
end;

procedure TFomenuDlg.Pontoknyilvntartsa1Click(Sender: TObject);
begin
    Application.CreateForm(TPontokFmDlg, PontokFmDlg);
	 Screen.Cursor	:= crDefault;
	 PontokFmDlg.ShowModal;
	 PontokFmDlg.Free;
end;

procedure TFomenuDlg.Pontoklistja1Click(Sender: TObject);
begin
    Application.CreateForm(TPontKimutDlg, PontKimutDlg);
		Screen.Cursor	:= crDefault;
		PontKimutDlg.ShowModal;
		PontKimutDlg.Free;
end;

procedure TFomenuDlg.Jrattisztts1Click(Sender: TObject);
var
  jakod    : string;
  db: integer;
  vankod,vankodnelkuli: boolean;
begin
  if (MessageBox(0, 'Mehet a feldolgoz�s?', '', MB_ICONQUESTION or MB_YESNO or MB_DEFBUTTON2) in [ idNo]) then
    exit;

  JARAT4.Close;
  JARAT4.Open;
  JARAT4.First;
  db:=0;
  while not JARAT4.Eof do
  begin
    jakod:=JARAT4JA_KOD.Value;
    VISZONY.Close;
    VISZONY.Parameters[0].Value:=jakod;
    VISZONY.Open;
    VISZONY.First;
    vankod:=False;
    vankodnelkuli:=False ;
    //db:=0;
    while not VISZONY.Eof do
    begin
      if (VISZONYVI_SAKOD.Value<>'')or(VISZONYVI_UJKOD.AsString<>'') then
      begin
         vankod:=True;
      end;
      if (VISZONYVI_SAKOD.Value='')and(VISZONYVI_UJKOD.AsString='') then
      begin
         vankodnelkuli:=True;
      end;
      //inc(db);
      VISZONY.Next;
    end;
    if vankod and vankodnelkuli then  // van sz�mla (norm�l), ez�rt kit�rl�m az UJKOD n�lk�li t�teleket
    begin
		  Query_Run(Query222, 'DELETE FROM VISZONY WHERE VI_JAKOD = '''+jakod+''' and vi_ujkod is null and vi_sakod is null',true) ;
      inc(db);
    end;
    JARAT4.Next;
  end;

  ShowMessage('K�sz!  '+inttostr(db));
end;

procedure TFomenuDlg.resenfutottkilomterek1Click(Sender: TObject);
begin
  Application.CreateForm(TUreskmnyilDlg, UreskmnyilDlg);
  UreskmnyilDlg.ShowModal;
  UreskmnyilDlg.Free;
end;

procedure TFomenuDlg.Hinyzsofrklistja1Click(Sender: TObject);
var
	fileazon 	: TextFile;
	filename		: string;
  sorszam		: integer;
begin
	if Hinyzsofrklistja1.Visible then begin
       {Hi�nyz� sof�r�k list�ja}
       {$I-}
       filename  := EgyebDlg.TempList + 'hianysofor.lst';
       AssignFile(fileazon, filename);
       Rewrite(fileazon);
       {$I+}
       if IOResult <> 0 then begin
           Exit;
       end;
       Screen.Cursor	:= crHourGlass;
       Query_Run(EgyebDlg.Querykoz2, 'SELECT GK_RESZ, GK_GKKAT, GK_TIPUS, GK_EVJAR FROM GEPKOCSI  '+
        //   ' WHERE GK_POTOS = 0 AND GK_KULSO = 0 AND GK_ARHIV = 0 '+
        //   ' WHERE GK_POTOS = 0 AND GK_KULSO = 0 AND GK_ARHIV = 0 '+' AND GK_KIVON=0 AND (GK_FORKI is NULL OR GK_FORKI='+''''')'+
           ' WHERE GK_POTOS <> 1 AND GK_KULSO = 0 AND GK_ARHIV = 0 '+' AND GK_KIVON=0 AND (GK_FORKI is NULL OR GK_FORKI='+''''')'+
           ' AND GK_GKKAT IN (''1,5'', ''1,5B'', ''1,5P'', ''15'', ''24'', ''24B'', ''24EU'', ''24M'', ''3,5'', ''3,5B'', ''4,5'', ''6'', ''6B'') '+
           ' AND GK_RESZ NOT IN (SELECT DO_RENDSZ FROM DOLGOZO WHERE  DO_ARHIV = 0) ORDER BY GK_GKKAT',true);
       sorszam	:= 1;
       while not EgyebDlg.Querykoz2.EOF do begin
           {Nincs ilyen j�ratsz�m}
           Writeln(fileazon, copy(IntToStr(sorszam)+URESSTRING, 1, 3)+'  '+
               copy(EgyebDlg.Querykoz2.FieldByName('GK_RESZ').AsString+URESSTRING, 1, 10)+'  '+
               copy(EgyebDlg.Querykoz2.FieldByName('GK_GKKAT').AsString+URESSTRING, 1, 5)+'  '+
               copy(EgyebDlg.Querykoz2.FieldByName('GK_TIPUS').AsString+URESSTRING, 1, 45)+'  '+
               copy(EgyebDlg.Querykoz2.FieldByName('GK_EVJAR').AsString+URESSTRING, 1, 4)+'  ');
           Inc(sorszam);
           EgyebDlg.Querykoz2.Next;
       end;
       EgyebDlg.Querykoz2.Close;
       CloseFile(fileazon);
       Screen.Cursor	:= crDefault;
       if sorszam = 1 then begin
           NoticeKi('Nincs hi�nyz� sof�r');
       end else begin
           EgyebDlg.Vanvonal	:= false;
           EgyebDlg.FileMutato(
           filename,
           'Hi�nyz� sof�r�k list�ja',
           ' ',
           ' ',
           'Ssz  Rendsz�m    Kat.   T�pus                                          �vj�rat',
           '------------------------------------------------------------------------------',
           0);
     end;
  end;

end;

procedure TFomenuDlg.reskmazrba1Click(Sender: TObject);
var
//  ujkodszam 	: integer;
  ureskm: string;
  kedat		: string;
  vedat		: string;
  rendsz,ret_kod: string;
begin
  if (MessageBox(0, 'Mehet a feldolgoz�s?', '', MB_ICONQUESTION or MB_YESNO or MB_DEFBUTTON2) in [ idNo]) then
    exit;
	EgyebDLg.SetADOQueryDatabase(Query2);
	EgyebDLg.SetADOQueryDatabase(Query33);

  Label4.Visible:=True;
  Query33.Close;
  Query33.SQL.Add('select * from zarokm where zk_ev1=2013 order by zk_datum') ;
  Query33.Open;
  while not Query33.Eof do
  begin
    kedat:=copy(Query33.FieldByName('ZK_DATUM').asstring,1,8)+'01.';
    vedat:=Query33.FieldByName('ZK_DATUM').asstring;
    rendsz:=Query33.FieldByName('ZK_RENSZ').asstring;
    ret_kod:=Query33.FieldByName('ZK_ZKKOD').asstring;
    Label4.Caption:=vedat;
    Label4.Update;

    Query_Run(Query2, 'SELECT sum(cast( ja_uresk as integer)) OSSZ_U_KM  FROM jarat WHERE ja_rendsz ='''+rendsz+''''+
    ' and JA_JKEZD >= '''+kedat+''' AND JA_JKEZD <= '''+vedat+'''  AND JA_FAZIS <> 2' );

    ureskm:=Query2.FieldByName('OSSZ_U_KM').AsString   ;

    Query_Update (Query2, 'ZAROKM',
    	[
     	'ZK_URESKM', SqlSzamString(StringSzam(ureskm), 12, 0)
     	], 	' WHERE ZK_ZKKOD = '+ret_kod);

    Query33.Next;
  end;
  Query33.Close;
  ShowMessage('K�sz!');
end;

procedure TFomenuDlg.Szmlaszmmegbzsba1Click(Sender: TObject);
begin
  if (MessageBox(0, 'Mehet a feldolgoz�s?', '', MB_ICONQUESTION or MB_YESNO or MB_DEFBUTTON2) in [ idNo]) then
    exit;
	EgyebDLg.SetADOQueryDatabase(Query2);
	EgyebDLg.SetADOQueryDatabase(Query33);

  Label4.Visible:=True;
  Query33.Close;
  Query33.SQL.Clear;
  Query33.SQL.Add('select MB_MBKOD, mb_vikod,vi_ujkod,sa_kod,sa_valoss,sa_valnem from szami.MEGBIZAS, szami.viszony, szami.SZFEJ '+
                  'where mb_sako2 is NULL and not mb_vikod is null and vi_vikod=mb_vikod and sa_ujkod=vi_ujkod order by mb_mbkod') ;
  Query33.Open;
  while not Query33.Eof do
  begin
    Label4.Caption:=Query33.fieldbyname('MB_MBKOD').AsString;
    Label4.Update;

    Query_Update (Query2, 'MEGBIZAS',
    	[
     	'MB_SAKO2',''''+Query33.fieldbyname('SA_KOD').AsString+'''',
     	'MB_SAVAL',''''+Query33.fieldbyname('SA_VALNEM').AsString+'''',
      'MB_SAOSS',SqlSzamString(StringSzam(Query33.fieldbyname('SA_VALOSS').AsString), 12, 0)
     	], 	' WHERE MB_VIKOD = '+Query33.fieldbyname('MB_VIKOD').AsString);


    Query33.Next;
  end;
  Query33.Close;
  ShowMessage('K�sz!');

end;

procedure TFomenuDlg.Havizemanyagkimutats1Click(Sender: TObject);
begin
  if Havizemanyagkimutats1.Visible then begin
      Screen.Cursor    := crHourGlass;
      Application.CreateForm(TUzemaHaviDlg, UzemaHaviDlg);
      Screen.Cursor    := crDefault;
      UzemaHaviDlg.ShowModal;
      UzemaHaviDlg.Free;
  end;
end;

procedure TFomenuDlg.HECPOLLtankolsimport1Click(Sender: TObject);
begin
     Application.CreateForm(TTankTranOlvasoDlg, TankTranOlvasoDlg);
     try
        TankTranOlvasoDlg.ShowModal;
     finally
        TankTranOlvasoDlg.Free;
        end;  // try-finally
end;

procedure TFomenuDlg.GKKATKltsg2Click(Sender: TObject);
begin
  if (MessageBox(0, 'Mehet a feldolgoz�s?', '', MB_ICONQUESTION or MB_YESNO or MB_DEFBUTTON2) in [ idNo]) then
    exit;
	EgyebDLg.SetADOQueryDatabase(Query2);
	EgyebDLg.SetADOQueryDatabase(Query33);
  Query2.SQL.Add('select ks_ktkod,ks_szkod from szami.koltseg where ks_gkkat is null order by ks_ktkod desc')  ;
  Query2.Open;
  Query2.First;
  Label4.Visible:=True;
  while not Query2.Eof do
  begin
    Label4.Caption:=Query2.fieldbyname('ks_ktkod').AsString;
    Label4.Update;
    Query_Run(Query33,'update koltseg set ks_szkod='''+''' where ks_ktkod='+Query2.fieldbyname('ks_ktkod').AsString);
    Query2.Next;
  end;
  Query2.Close;
  ShowMessage('K�sz!');
end;

procedure TFomenuDlg.DolgServantes1Click(Sender: TObject);
begin
 //  EgyebDlg.DolgServantes;
end;

procedure TFomenuDlg.Lejrtszmlk1Click(Sender: TObject);
begin
  EgyebDlg.Lejartszamlak('3514',True);
end;

procedure TFomenuDlg.Lejrtszmlk2Click(Sender: TObject);
begin
     Screen.Cursor	:= crHourGlass;
     Application.CreateForm(TLejartszamlaFmDlg, LejartszamlaFmDlg);
     Screen.Cursor	:= crDefault;
     LejartszamlaFmDlg.ShowModal;
     LejartszamlaFmDlg.Free;
end;


procedure TFomenuDlg.PontozasCheck;
var
   moddat      : string;
   folytat     : boolean;
begin
   // Ellen�rizz�k hogy mindenki pontoz�sra ker�lt-e.
   folytat := false;
   while not folytat do begin
       moddat := copy(EgyebDlg.MaiDatum, 1, 8)+'01.';
       moddat := DatumHoznap(moddat, -1, true);
       moddat := copy(moddat, 1, 8)+'01.';
      //  Query_Run (EgyebDlg.QueryKozos, 'SELECT DISTINCT DC_DOKOD, DO_NAME FROM PONTOZO, DOLGOZOCSOP, DOLGOZO '+
      //     ' WHERE DC_CSKOD = PO_ALKOD AND DO_KOD = DC_DOKOD AND DO_ARHIV = 0 AND PO_FEKOD = '''+EgyebDlg.user_code+''' '+
         Query_Run (EgyebDlg.QueryKozos, 'SELECT DISTINCT PO_DOKOD, DO_NAME FROM PONTOZO, DOLGOZO '+
           ' WHERE PO_DOKOD=DO_KOD AND DO_ARHIV = 0 AND PO_FEKOD = '''+EgyebDlg.user_code+''' '+
           ' AND PO_DOKOD NOT IN (SELECT PP_DOKOD FROM PONTOK WHERE PP_DATUM = '''+moddat+''' '+
           ' AND PP_FEKOD = '''+EgyebDlg.user_code+''' ) '+
           ' AND DO_BELEP<'''+moddat+''' '+    // csak az azel�tt bel�pett dolgoz�kat
           ' AND convert(int,DO_KOD) not in (select isnull(JE_DOKOD,0) from JELSZO where JE_FEKOD = '+EgyebDlg.user_code+')'+  // saj�t magunkat nem l�tjuk
           ' ORDER BY DO_NAME', true);
       if EgyebDlg.QueryKozos.RecordCount > 0 then begin
           if NoticeKi('�nnek m�g vannak pontoz�si hi�nyoss�gai, legel�sz�r ezeket kell p�tolnia!/Befejezi a pontoz�st? ', NOT_QUESTION) = 0 then begin
               Application.CreateForm(TPontokFmDlg, PontokFmDlg);
               PontokFmDlg.ShowModal;
               PontokFmDlg.Free;
           end else begin
               // Kil�p�nk a programb�l
               folytat     := true;
               kiellenor   := false;
               Close;
           end;
       end else begin
           folytat := true;
       end;
   end;
end;

procedure TFomenuDlg.EKAER_napi_riport(Melyik: TEKAERRiport);
const
   ExportImport1 = '	and ms1.MS_FAJKO = 0 and ms1.MS_EXSTR like ''%export%'' '+ //  -- van export
                ' and ms2.MS_FAJKO in (1,2,3)  ';  //  --  �s van el�rak�s, el�h�z�s, lerak�
   ExportImport2 =  ' and ms1.MS_FAJKO = 0 and ms1.MS_EXSTR like ''%import%'' '+
              'and ms2.MS_FAJKO in (3) ';  // �s van lerak�

var
   Fomezo, FoSQL, Fejlec, GKKAT_SQL, ExportImportSQL: string;
   SzuromezoTomb, SzummamezoTomb: TStringArray;
begin
    Application.CreateForm(TUniStatFmDlg, UniStatFmDlg);
    Screen.Cursor	:= crHourGlass;
    // ----- param�terez�s ------- //
    UniStatFmDlg.Caption:='EKAER napi riport';
    UniStatFmDlg.Tag:=609;
    UniStatFmDlg.pFontSize:= 7;
    UniStatFmDlg.pDisplaySzorzo:= 4.3;
    UniStatFmDlg.pFieldMargin:= 3;
    SzuromezoTomb:= TStringArray.Create('Rendsz�m', '', '1',
                                        'P�tkocsi', '', '1',
                                        'EKAER', '', '1',
                                        'CMR', '', '1',
                                        'Felrak�', '', '1',
                                        'Lerak�', '', '1',
                                        'GkVezet�', '', '1',);
    if Melyik = import_lerakoval then begin
        GKKAT_SQL:= '';
        ExportImportSQL:= ExportImport2;
        Fejlec:= 'EKAER napi riport - �sszes import lerak� alj�rattal';
        end;
    if Melyik = nagyauto_aljarattal then begin
        GKKAT_SQL:= ' and MB_GKKAT IN (''24'', ''24B'', ''24M'', ''4,5'', ''9'')';
        ExportImportSQL:= ExportImport1;
        Fejlec:= 'EKAER napi riport - nagyaut�s export alj�rattal';
        end;
    if Melyik = kisauto_aljarattal then begin
        GKKAT_SQL:= ' and MB_GKKAT IN (''1'', ''1,5'', ''1,5B'', ''1,5P'', ''15'', ''3,5'', ''3,5B'', ''6'', ''6B'')';
        ExportImportSQL:= ExportImport1;
        Fejlec:= 'EKAER napi riport - kisaut�s export alj�rattal';
        end;
    if Melyik = hutos_aljarattal then begin
        GKKAT_SQL:= ' and MB_GKKAT IN (''24EU'')';
        ExportImportSQL:= ExportImport1;
        Fejlec:= 'EKAER napi riport - h�t�s export alj�rattal';
        end;
    if Melyik = mind_aljarattal then begin
        GKKAT_SQL:= '';
        ExportImportSQL:= ExportImport1;
        Fejlec:= 'EKAER napi riport - �sszes megb�z�s alj�rattal';
        end;

    FoSQL:= 'select * from ( '+
            ' select case when isnull(MS_EKROK, '''')='''' then '''' else ''OK'' end EKAER, '+
            ' case when isnull(MS_CMROK, '''')='''' then '''' else ''OK'' end CMR, '+
            ' MB_MBKOD Megb�z�s, MB_JASZA J�rat, MB_GKKAT GkKat, '+
            ' MS_DATUM Rakod�s, MB_VENEV Vev�, MB_POZIC Poz�ci�, '+
            ' MS_RENSZ Rendsz�m, MB_POTSZ P�tkocsi, MS_SNEV1 GkVezet�, '+
           //  ' MB_ALNEV Alv�llalkoz�, '+
            ' MS_FELTEL+'',''+MS_FELNEV Felrak�, MS_LERTEL+'',''+MS_LERNEV Lerak� '+
            ' from MEGBIZAS '+
            '   inner join MEGSEGED on MS_MBKOD = MB_MBKOD '+
            ' where 1=1 '+
            GKKAT_SQL +
            ' and MB_MBKOD in '+
            ' 	(select distinct MB_MBKOD from MEGBIZAS '+
            ' 		inner join MEGSEGED ms1 on ms1.MS_MBKOD = MB_MBKOD '+
            ' 		inner join MEGSEGED ms2 on ms2.MS_MBKOD = MB_MBKOD '+
            ' 		where ((ms1.MS_FETDAT = '''+EgyebDlg.MaiDatum+''') or (ms2.MS_FETDAT = '''+EgyebDlg.MaiDatum+''')) '+
            ExportImportSQL+
            ' 	) '+
            ') a where 1=1 ';  // a k�s�bbi sz�r�shez kell
    Fomezo:= 'Megb�z�s';
    UniStatFmDlg.AdatTolt(Fomezo, FoSQL, Fejlec, SzuromezoTomb, SzummamezoTomb);
    // --------------------------- //
    Screen.Cursor	:= crDefault;
    UniStatFmDlg.ShowModal;
    UniStatFmDlg.Free;
end;

end.


