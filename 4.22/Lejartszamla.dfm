object FLejartszamla: TFLejartszamla
  Left = 708
  Top = 405
  BorderIcons = [biSystemMenu]
  Caption = 'Lej'#225'rt sz'#225'ml'#225'k'
  ClientHeight = 553
  ClientWidth = 557
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 16
  object Panel1: TPanel
    Left = 0
    Top = 512
    Width = 557
    Height = 41
    Align = alBottom
    TabOrder = 0
    DesignSize = (
      557
      41)
    object BitBtn2: TBitBtn
      Left = 458
      Top = 8
      Width = 100
      Height = 27
      HelpContext = 12005
      Anchors = [akTop, akRight]
      Cancel = True
      Caption = '&Kil'#233'p'#233's'
      Glyph.Data = {
        CE070000424DCE07000000000000360000002800000024000000120000000100
        1800000000009807000000000000000000000000000000000000008080008080
        0080800080800080800080800080800080800080800080800080800080800080
        8000808000808000808000808000808000808000808000808000808000808000
        8080008080008080008080008080008080008080008080008080008080008080
        0080800080800080800080800080800080808080808080800080800080800080
        8000808000808000808000808000808000808000808000808000808000808000
        8080008080008080008080FFFFFF008080008080008080008080008080008080
        0080800080800080800080800080800080800080800080800080800000FF0000
        800000808080800080800080800080800080800080800000FF80808000808000
        8080008080008080008080008080008080008080808080808080FFFFFF008080
        008080008080008080008080008080FFFFFF0080800080800080800080800080
        800080800080800000FF00008000008000008080808000808000808000808000
        00FF000080000080808080008080008080008080008080008080008080808080
        FFFFFF008080808080FFFFFF008080008080008080FFFFFF808080808080FFFF
        FF0080800080800080800080800080800080800000FF00008000008000008000
        00808080800080800000FF000080000080000080000080808080008080008080
        008080008080008080808080FFFFFF008080008080808080FFFFFF008080FFFF
        FF808080008080008080808080FFFFFF00808000808000808000808000808000
        80800000FF000080000080000080000080808080000080000080000080000080
        000080808080008080008080008080008080008080808080FFFFFF0080800080
        80008080808080FFFFFF808080008080008080008080008080808080FFFFFF00
        80800080800080800080800080800080800000FF000080000080000080000080
        0000800000800000800000808080800080800080800080800080800080800080
        80008080808080FFFFFF00808000808000808080808000808000808000808000
        8080FFFFFF808080008080008080008080008080008080008080008080008080
        0000FF0000800000800000800000800000800000808080800080800080800080
        80008080008080008080008080008080008080808080FFFFFF00808000808000
        8080008080008080008080FFFFFF808080008080008080008080008080008080
        0080800080800080800080800080800000800000800000800000800000808080
        8000808000808000808000808000808000808000808000808000808000808000
        8080808080FFFFFF008080008080008080008080008080808080008080008080
        0080800080800080800080800080800080800080800080800080800000FF0000
        8000008000008000008080808000808000808000808000808000808000808000
        8080008080008080008080008080008080808080FFFFFF008080008080008080
        8080800080800080800080800080800080800080800080800080800080800080
        800080800000FF00008000008000008000008000008080808000808000808000
        8080008080008080008080008080008080008080008080008080008080808080
        008080008080008080008080808080FFFFFF0080800080800080800080800080
        800080800080800080800080800000FF00008000008000008080808000008000
        0080000080808080008080008080008080008080008080008080008080008080
        008080008080808080008080008080008080008080008080808080FFFFFF0080
        800080800080800080800080800080800080800080800000FF00008000008000
        00808080800080800000FF000080000080000080808080008080008080008080
        008080008080008080008080008080808080008080008080008080808080FFFF
        FF008080008080808080FFFFFF00808000808000808000808000808000808000
        80800000FF0000800000808080800080800080800080800000FF000080000080
        000080808080008080008080008080008080008080008080808080FFFFFF0080
        80008080808080008080808080FFFFFF008080008080808080FFFFFF00808000
        80800080800080800080800080800080800000FF000080008080008080008080
        0080800080800000FF0000800000800000800080800080800080800080800080
        80008080808080FFFFFFFFFFFF808080008080008080008080808080FFFFFF00
        8080008080808080FFFFFF008080008080008080008080008080008080008080
        0080800080800080800080800080800080800080800000FF0000800000FF0080
        8000808000808000808000808000808000808080808080808000808000808000
        8080008080008080808080FFFFFFFFFFFFFFFFFF808080008080008080008080
        0080800080800080800080800080800080800080800080800080800080800080
        8000808000808000808000808000808000808000808000808000808000808000
        8080008080008080008080008080008080008080008080808080808080808080
        0080800080800080800080800080800080800080800080800080800080800080
        8000808000808000808000808000808000808000808000808000808000808000
        8080008080008080008080008080008080008080008080008080008080008080
        008080008080008080008080008080008080}
      NumGlyphs = 2
      TabOrder = 0
      OnClick = BitBtn2Click
    end
    object BitSzamla: TBitBtn
      Left = 8
      Top = 8
      Width = 100
      Height = 27
      Hint = 'Vagy dupla klikk a sz'#225'ml'#225'n'
      HelpContext = 14706
      Caption = 'Sz'#225'mla'
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        0400000000000001000000000000000000001000000010000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333000000
        000033333377777777773333330FFFFFFFF03FF3FF7FF33F3FF700300000FF0F
        00F077F777773F737737E00BFBFB0FFFFFF07773333F7F3333F7E0BFBF000FFF
        F0F077F3337773F3F737E0FBFBFBF0F00FF077F3333FF7F77F37E0BFBF00000B
        0FF077F3337777737337E0FBFBFBFBF0FFF077F33FFFFFF73337E0BF0000000F
        FFF077FF777777733FF7000BFB00B0FF00F07773FF77373377373330000B0FFF
        FFF03337777373333FF7333330B0FFFF00003333373733FF777733330B0FF00F
        0FF03333737F37737F373330B00FFFFF0F033337F77F33337F733309030FFFFF
        00333377737FFFFF773333303300000003333337337777777333}
      NumGlyphs = 2
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      TabStop = False
      OnClick = BitSzamlaClick
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 557
    Height = 132
    Align = alTop
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    object Label1: TLabel
      Left = 8
      Top = 40
      Width = 106
      Height = 16
      Alignment = taRightJustify
      Caption = 'Fizet'#233'si mor'#225'l :'
    end
    object Label2: TLabel
      Left = 184
      Top = 38
      Width = 9
      Height = 13
      Caption = '()'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label3: TLabel
      Left = 184
      Top = 51
      Width = 254
      Height = 13
      Caption = '(100% alatt k'#233'sedelmes fizet'#337', 100% felett el'#337're fizet'#337')'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label4: TLabel
      Left = 40
      Top = 72
      Width = 74
      Height = 16
      Alignment = taRightJustify
      Caption = 'H / F nap :'
    end
    object Label5: TLabel
      Left = 184
      Top = 72
      Width = 328
      Height = 16
      Caption = '('#193'tlagos fizet'#233'si hat'#225'rid'#337' / T'#233'nyleges kifizet'#233's)'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label6: TLabel
      Left = 7
      Top = 104
      Width = 107
      Height = 16
      Alignment = taRightJustify
      Caption = 'Lej'#225'rt tartoz'#225's :'
    end
    object Label7: TLabel
      Left = 216
      Top = 104
      Width = 14
      Height = 16
      Caption = 'Ft'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Edit1: TEdit
      Left = 8
      Top = 8
      Width = 49
      Height = 24
      ReadOnly = True
      TabOrder = 0
    end
    object Edit2: TEdit
      Left = 64
      Top = 8
      Width = 489
      Height = 24
      ReadOnly = True
      TabOrder = 1
    end
    object Edit3: TEdit
      Left = 120
      Top = 38
      Width = 57
      Height = 24
      ReadOnly = True
      TabOrder = 2
      Text = '100'
    end
    object Edit4: TEdit
      Left = 120
      Top = 70
      Width = 57
      Height = 24
      ReadOnly = True
      TabOrder = 3
      Text = '100'
    end
    object DBEdit1: TDBEdit
      Left = 120
      Top = 102
      Width = 89
      Height = 24
      DataField = 'osszeg'
      DataSource = DataSource3
      TabOrder = 4
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 132
    Width = 557
    Height = 380
    Align = alClient
    TabOrder = 2
    object DBGrid1: TDBGrid
      Left = 1
      Top = 1
      Width = 555
      Height = 378
      Align = alClient
      DataSource = DataSource1
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
      ReadOnly = True
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -13
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      OnDblClick = DBGrid1DblClick
      Columns = <
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'sa_kod'
          Title.Alignment = taCenter
          Title.Caption = 'Sz'#225'mlasz'#225'm'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -13
          Title.Font.Name = 'MS Sans Serif'
          Title.Font.Style = [fsBold]
          Width = 90
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'sa_kidat'
          Title.Alignment = taCenter
          Title.Caption = 'Ki'#225'll'#237't'#225's'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -13
          Title.Font.Name = 'MS Sans Serif'
          Title.Font.Style = [fsBold]
          Width = 100
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'sa_esdat'
          Title.Alignment = taCenter
          Title.Caption = 'Esed'#233'kess'#233'g'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -13
          Title.Font.Name = 'MS Sans Serif'
          Title.Font.Style = [fsBold]
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'sa_valoss'
          Title.Alignment = taRightJustify
          Title.Caption = 'Nett'#243' '#246'sszeg'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -13
          Title.Font.Name = 'MS Sans Serif'
          Title.Font.Style = [fsBold]
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'sa_valnem'
          Title.Alignment = taCenter
          Title.Caption = 'Val.nem'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -13
          Title.Font.Name = 'MS Sans Serif'
          Title.Font.Style = [fsBold]
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'sa_lnap'
          Title.Alignment = taRightJustify
          Title.Caption = 'H.nap'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -13
          Title.Font.Name = 'MS Sans Serif'
          Title.Font.Style = [fsBold]
          Width = 60
          Visible = True
        end>
    end
  end
  object Query1: TADOQuery
    Connection = EgyebDlg.ADOConnectionAlap
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'VKOD'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'select * from lejartszamla'
      'where sa_vevokod= :VKOD'
      'order by sa_lnap desc')
    Left = 488
    Top = 97
    object Query1sa_kod: TStringField
      FieldName = 'sa_kod'
      Size = 11
    end
    object Query1sa_vevokod: TStringField
      FieldName = 'sa_vevokod'
      Size = 4
    end
    object Query1sa_vevonev: TStringField
      FieldName = 'sa_vevonev'
      Size = 80
    end
    object Query1sa_kidat: TStringField
      FieldName = 'sa_kidat'
      Size = 11
    end
    object Query1sa_esdat: TStringField
      FieldName = 'sa_esdat'
      Size = 11
    end
    object Query1sa_valoss: TBCDField
      FieldName = 'sa_valoss'
      DisplayFormat = '0,'
      Precision = 14
    end
    object Query1sa_valafa: TBCDField
      FieldName = 'sa_valafa'
      Precision = 14
    end
    object Query1sa_valnem: TStringField
      FieldName = 'sa_valnem'
      Size = 3
    end
    object Query1sa_osszeg: TBCDField
      FieldName = 'sa_osszeg'
      Precision = 14
    end
    object Query1sa_afa: TBCDField
      FieldName = 'sa_afa'
      Precision = 14
    end
    object Query1sa_gyujto: TStringField
      FieldName = 'sa_gyujto'
      ReadOnly = True
      Size = 1
    end
    object Query1sa_lnap: TIntegerField
      FieldName = 'sa_lnap'
      ReadOnly = True
    end
    object Query1sa_ujkod: TBCDField
      FieldName = 'sa_ujkod'
      Precision = 14
    end
  end
  object DataSource1: TDataSource
    DataSet = Query1
    Left = 496
    Top = 161
  end
  object Query2: TADOQuery
    Connection = EgyebDlg.ADOConnectionAlap
    Parameters = <
      item
        Name = 'VKOD'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'select * from fizetesimoral'
      'where sa_vevokod= :VKOD'
      '')
    Left = 440
    Top = 97
    object Query2sa_vevokod: TStringField
      FieldName = 'sa_vevokod'
      Size = 4
    end
    object Query2sa_vevonev: TStringField
      FieldName = 'sa_vevonev'
      Size = 80
    end
    object Query2db: TIntegerField
      FieldName = 'db'
      ReadOnly = True
    end
    object Query2hnap: TIntegerField
      FieldName = 'hnap'
      ReadOnly = True
    end
    object Query2lnap: TBCDField
      FieldName = 'lnap'
      ReadOnly = True
      Precision = 32
      Size = 2
    end
    object Query2moral: TIntegerField
      FieldName = 'moral'
      ReadOnly = True
    end
  end
  object Query3: TADOQuery
    Connection = EgyebDlg.ADOConnectionAlap
    Parameters = <
      item
        Name = 'VKOD'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'select sum(sa_osszeg) osszeg from lejartszamla'
      'where sa_vevokod= :VKOD'
      '')
    Left = 392
    Top = 97
    object Query3osszeg: TBCDField
      FieldName = 'osszeg'
      ReadOnly = True
      DisplayFormat = '0,'
      Precision = 32
    end
  end
  object DataSource3: TDataSource
    DataSet = Query3
    Left = 432
    Top = 161
  end
end
