unit Forgalom;

interface

uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls, Egyeb,
  Forms, Dialogs, DBTables, DB, StdCtrls, Buttons, Mask, DBCtrls,
  Printers, ExtCtrls, ADODB,DateUtils;

type
  TForgalomDlg = class(TForm)
    Label1: TLabel;
    BitBtn4: TBitBtn;
    BitBtn6: TBitBtn;
    MaskEdit1: TMaskEdit;
    MaskEdit2: TMaskEdit;
    Label2: TLabel;
    Label3: TLabel;
    ComboBox1: TComboBox;
    CheckBox1: TCheckBox;
    RadioGroup1: TRadioGroup;
    CheckBox2: TCheckBox;
    MaskEdit3: TMaskEdit;
    CheckBox3: TCheckBox;
    MaskEdit4: TMaskEdit;
    BitBtn10: TBitBtn;
    BitBtn1: TBitBtn;
	 BitBtn2: TBitBtn;
    BitBtn3: TBitBtn;
    Label4: TLabel;
    M0: TMaskEdit;
    MaskEdit30: TMaskEdit;
    MaskEdit5: TMaskEdit;
	 BitBtn5: TBitBtn;
	 MaskEdit50: TMaskEdit;
	 CheckBox4: TCheckBox;
	 Query1: TADOQuery;
	 Query2: TADOQuery;
	 CheckBox5: TCheckBox;
	 procedure FormCreate(Sender: TObject);
	 procedure BitBtn4Click(Sender: TObject);
	 procedure BitBtn6Click(Sender: TObject);
	 procedure MaskEdit1Click(Sender: TObject);
	 procedure CheckBox1Click(Sender: TObject);
	 procedure CheckBox2Click(Sender: TObject);
	 procedure CheckBox3Click(Sender: TObject);
	 procedure BitBtn10Click(Sender: TObject);
	 procedure BitBtn1Click(Sender: TObject);
	 procedure MaskEdit1Exit(Sender: TObject);
	 procedure BitBtn2Click(Sender: TObject);
	 procedure BitBtn3Click(Sender: TObject);
	 procedure BitBtn5Click(Sender: TObject);
	 function  Nagybetus( instr : string) : string;
	 procedure CheckBox5Click(Sender: TObject);
  private
	  jaratkod		: string;
	  kilepes		: boolean;
     tablanev      : string;
  public
  end;

var
  ForgalomDlg: TForgalomDlg;

implementation

uses
	ForgList, ForgList2, j_valaszto, j_sql, Kozos, Valgepk, Valvevo,Valvevo2, J_DataModule;
{$R *.DFM}

procedure TForgalomDlg.FormCreate(Sender: TObject);
begin
	jaratkod	   	:= '';
	kilepes			:= true;
	MaskEdit1.Text := copy(EgyebDlg.MaiDatum,1,8)+'01';
	MaskEdit2.Text := EgyebDlg.MaiDatum;
	EgyebDlg.SetADOQueryDatabase(Query1);
	EgyebDlg.SetADOQueryDatabase(Query2);
	ComboBox1.ItemIndex	:=6;
	CheckBox1Click(Sender);
	CheckBox2Click(Sender);
	CheckBox3Click(Sender);
	Label4.Caption	:= '';
	Label4.Update;
	SetMaskEdits([MaskEdit3, MaskEdit5, MaskEdit4]);
   tablanev        := 'T_SZFEJ'+EgyebDlg.user_code;
end;

procedure TForgalomDlg.BitBtn4Click(Sender: TObject);
var
  sqlstr		: string;
  sqlstr2		: string;
  sorrend  	: integer;
  sorsz		: integer;
  regilevel    : integer;
begin
	if MaskEdit1.Text = '' then begin
		MaskEdit1.Text := '1990.01.01.';
	end;
	if MaskEdit2.Text = '' then begin
		MaskEdit2.Text := EgyebDlg.MaiDatum;;
	end;
	if not Jodatum2(MaskEdit1) then begin
		NoticeKi('Rossz d�tum megad�sa!');
		MaskEdit1.SetFocus;
		Exit;
	end;
	if not Jodatum2(MaskEdit2) then begin
		NoticeKi('Rossz d�tum megad�sa!');
		MaskEdit2.SetFocus;
		Exit;
	end;

	Screen.Cursor	:= crHourGlass;
	BitBtn4.Hide;
	// A "tablanev" t�bla t�rl�se
	Query_Run(Query1, 'DROP TABLE '+tablanev, false);
	Query_Run(Query1, 'CREATE TABLE '+tablanev+' ( SA_KOD VARCHAR(11), SA_KIDAT VARCHAR(11), SA_ESDAT VARCHAR(11), SA_TEDAT VARCHAR(20), '+
		' SA_VEVOKOD VARCHAR(5), SA_VEVONEV VARCHAR(80), SA_OSSZEG NUMERIC(14,4), SA_AFA NUMERIC(14,4), SA_FIMOD VARCHAR(20), SA_FLAG VARCHAR(1), SA_JARAT VARCHAR(80), SA_SORRE INTEGER, SA_TIPUS NUMERIC(14,4), '+
		' SA_UJKOD NUMERIC(14,4), SA_MEGJ VARCHAR(80), SA_RSZ VARCHAR(80), SA_NETTO NUMERIC(14,4), SA_NEAFA NUMERIC(14,4), '+
		' SA_EURNET NUMERIC(14,4), SA_EURAFA NUMERIC(14,4), SA_EUSPED NUMERIC(14,4), SA_FTSPED NUMERIC(14,4) ) ', false);
	try

	{Csak a v�gleges�tettek sz�m�tanak}
	sqlstr	:= ' WHERE ( SA_FLAG = ''0'' ) ';
	{Sz�r�s a vev�re}
	if CheckBox1.Checked then begin
		sqlstr	:= sqlstr + ' AND SA_VEVONEV IN ('+IdezoBovito(AposztrofCsere(MaskEdit5.Text))+') ';
		//sqlstr	:= sqlstr + ' AND SA_VEVOKOD IN ('+IdezoBovito(MaskEdit50.Text)+') ';
	end;
	{Sz�r�s a rendsz�mra}
	if CheckBox2.Checked then begin
		sqlstr	:= sqlstr + ' AND SA_RSZ IN ('+IdezoBovito(MaskEdit3.Text)+') ';
	end;
	{Sz�r�s a j�ratra}
	if CheckBox3.Checked then begin
		sqlstr	:= sqlstr + ' AND SA_JARAT LIKE  ''%'+jaratkod+'%'' ';
	end;
  // -------------------------------------------------------------------//
  // --------------- Teljes�t�s szerinti forgalom ----------------------//
  // -------------------------------------------------------------------//
	if CheckBox5.Checked then begin


		{Sz�r�s az id�szakra}
		sqlstr	:= sqlstr + ' AND ( SA_TEDAT >= '''+MaskEdit1.Text+
				''' ) AND ( SA_TEDAT <= '''+MaskEdit2.Text+''' ) ';

		// A rekordok berak�sa a "tablanev"-be
		Query_Run(Query1, 'SELECT * FROM SZFEJ ' + sqlstr);
		sorsz	:= 1;
		kilepes	:= false;
		Query1.DisableControls;
		regilevel			:= EgyebDlg.log_level;
		EgyebDlg.log_level	:= 0;
		while ( ( not Query1.Eof ) and (not kilepes) ) do begin
			Application.ProcessMessages;
			Label4.Caption	:= 'Sz�ml�k : '+Query1.FieldByName('SA_KOD').AsString+'  ('+IntToStr(sorsz)+' / '+IntToStr(Query1.RecordCount)+')';
			Label4.Update;
			Inc(sorsz);
			// A sz�mla adatainak �sszegy�jt�se
			JSzamla		:= TJSzamla.Create(Self);
			JSzamla.FillSzamla(Query1.FieldByName('SA_UJKOD').AsString);
			Query_Insert( Query2, tablanev,
			[
			'SA_KOD',		''''+Query1.FieldByName('SA_KOD').AsString+'''',
			'SA_KIDAT',		''''+Query1.FieldByName('SA_KIDAT').AsString+'''',
			'SA_ESDAT',		''''+Query1.FieldByName('SA_ESDAT').AsString+'''',
			'SA_TEDAT',		''''+Query1.FieldByName('SA_TEDAT').AsString+'''',
			'SA_VEVOKOD',  	''''+Query1.FieldByName('SA_VEVOKOD').AsString+'''',
			'SA_VEVONEV',  	''''+Nagybetus(AposztrofCsere(Query1.FieldByName('SA_VEVONEV').AsString))+'''',
			'SA_OSSZEG',   	SqlSzamString(StringSzam(Query1.FieldByName('SA_OSSZEG').AsString),12,2),
			'SA_AFA',		SqlSzamString(StringSzam(Query1.FieldByName('SA_AFA').AsString),12,2),
			'SA_NETTO',   	SqlSzamString(JSzamla.ossz_netto,12,2),
			'SA_NEAFA',   	SqlSzamString(JSzamla.ossz_afa,12,2),
			'SA_EURNET',   	SqlSzamString(JSzamla.ossz_nettoeur,12,2),
			'SA_EURAFA',   	SqlSzamString(JSzamla.ossz_afaeur,12,2),
			'SA_EUSPED',   	SqlSzamString(JSzamla.spedicioeur,12,2),
			'SA_FTSPED',   	SqlSzamString(JSzamla.spedicioft,12,2),
			'SA_FIMOD',		''''+Query1.FieldByName('SA_FIMOD').AsString+'''',
			'SA_FLAG',		''''+Query1.FieldByName('SA_FLAG').AsString+'''',
			'SA_JARAT',		''''+Query1.FieldByName('SA_JARAT').AsString+'''',
			'SA_RSZ',		''''+Query1.FieldByName('SA_RSZ').AsString+'''',
			'SA_TIPUS',		''''+Query1.FieldByName('SA_TIPUS').AsString+'''',
			'SA_UJKOD',		IntToStr(StrToIntDef(Query1.FieldByName('SA_UJKOD').AsString,0)),
			'SA_MEGJ',		''''+Query1.FieldByName('SA_MEGJ').AsString+''''
			]);
			Query1.Next;
			JSzamla.Destroy;
		end;
		EgyebDlg.log_level	:= regilevel;
		if kilepes then begin
			Label4.Caption	:= '';
			Label4.Update;
			Exit;
		end;
		kilepes	:= true;

		if ComboBox1.ItemIndex = 6 then begin
			// A sorrend be�ll��sa
			Query_Run(Query1, 'UPDATE '+tablanev+' SET SA_SORRE = 0 ');
			Query_Run(Query1, 'SELECT SA_VEVONEV, SUM(SA_OSSZEG) OSSZEG FROM '+tablanev+' GROUP BY SA_VEVONEV ORDER BY OSSZEG DESC ');
			sorrend := 1;
			while not Query1.Eof do begin
				// Query_Run(Query2, 'UPDATE '+tablanev+' SET SA_SORRE = '+IntToStr(sorrend)+' WHERE SA_VEVONEV = '''+Query1.FieldByName('SA_VEVONEV').AsString+''' ');
        Query_Update(Query2, tablanev,
            [
            'SA_SORRE', IntToStr(sorrend)
            ], ' WHERE SA_VEVONEV = '''+AposztrofCsere(Query1.FieldByName('SA_VEVONEV').AsString)+''' ');
				Inc(sorrend);
				Query1.Next;
			end;
			Query1.First;
		end;

		sqlstr	:= 'SELECT * FROM '+tablanev;
		{Sorrend meghat�roz�sa}
		case ComboBox1.ItemIndex of
		0:
			sqlstr	:= sqlstr + ' ORDER BY SA_TEDAT,SA_VEVONEV ';
		1:
			sqlstr	:= sqlstr + ' ORDER BY SA_KIDAT,SA_VEVONEV ';
		2:
			sqlstr	:= sqlstr + ' ORDER BY SA_ESDAT,SA_VEVONEV ';
		3:
			sqlstr	:= sqlstr + ' ORDER BY SA_VEVONEV ';
		4:
			sqlstr	:= sqlstr + ' ORDER BY SA_OSSZEG ';
		5:
			sqlstr	:= sqlstr + ' ORDER BY SA_JARAT ';
		6:
			sqlstr	:= sqlstr + ' ORDER BY SA_SORRE ';
		7:
			sqlstr	:= sqlstr + ' ORDER BY SA_KOD ';
		end;
		Application.CreateForm(TKiForgList2Dlg, KiForgList2Dlg);
		if KiForgList2Dlg.Tolt(sqlstr,ForgalomDlg) then begin
			KiForgList2Dlg.Reszletes	:= CheckBox4.Checked;
			KiForgList2Dlg.Rep.Preview;
		end else begin
			NoticeKi('Nincs a megadott felt�teleknek megfelel� t�tel!');
		end;
		KiForgList2Dlg.Destroy;
		Label4.Caption	:= '';
		Label4.Update;

	end
  // ------------------------------------------------------//
  // --------------- Norm�l forgalom ----------------------//
  // ------------------------------------------------------//

  else begin
		{Sz�r�s az id�szakra}
		case RadioGroup1.ItemIndex of
		0:
			sqlstr	:= sqlstr + ' AND ( SA_TEDAT >= '''+MaskEdit1.Text+
				''' ) AND ( SA_TEDAT <= '''+MaskEdit2.Text+''' ) ';
		1:
			sqlstr	:= sqlstr + ' AND ( SA_KIDAT >= '''+MaskEdit1.Text+
				''' ) AND ( SA_KIDAT <= '''+MaskEdit2.Text+''' ) ';
		else
			sqlstr	:= sqlstr + ' AND ( SA_ESDAT >= '''+MaskEdit1.Text+
				''' ) AND ( SA_ESDAT <= '''+MaskEdit2.Text+''' ) ';
		end;

		{Csak a v�gleges�tettek sz�m�tanak}
		sqlstr2	:= sqlstr;
		sqlstr	:= sqlstr + ' AND ( ( SA_RESZE = 0 ) OR ( SA_RESZE IS NULL ) )  ';

		// A rekordok berak�sa a "tablanev"-be
		Query_Run(Query1, 'SELECT * FROM SZFEJ ' + sqlstr);
		sorsz	:= 1;
		kilepes	:= false;
		Query1.DisableControls;
		regilevel			:= EgyebDlg.log_level;
		EgyebDlg.log_level	:= 0;
		while ( ( not Query1.Eof ) and (not kilepes) ) do begin
			Application.ProcessMessages;
			Label4.Caption	:= 'Sz�ml�k : '+Query1.FieldByName('SA_KOD').AsString+'  ('+IntToStr(sorsz)+' / '+IntToStr(Query1.RecordCount)+')';
			Label4.Update;
			Inc(sorsz);
			JSzamla		:= TJSzamla.Create(Self);
			JSzamla.FillSzamla(Query1.FieldByName('SA_UJKOD').AsString);
			Query_Insert( Query2, tablanev,
			[
			'SA_KOD',		''''+Query1.FieldByName('SA_KOD').AsString+'''',
			'SA_KIDAT',		''''+Query1.FieldByName('SA_KIDAT').AsString+'''',
			'SA_ESDAT',		''''+Query1.FieldByName('SA_ESDAT').AsString+'''',
			'SA_TEDAT',		''''+Query1.FieldByName('SA_TEDAT').AsString+'''',
			'SA_VEVOKOD',  	''''+Query1.FieldByName('SA_VEVOKOD').AsString+'''',
			'SA_VEVONEV',  	''''+Nagybetus(AposztrofCsere(Query1.FieldByName('SA_VEVONEV').AsString))+'''',
			'SA_OSSZEG',   	SqlSzamString(StringSzam(Query1.FieldByName('SA_OSSZEG').AsString),12,2),
			'SA_AFA',		SqlSzamString(StringSzam(Query1.FieldByName('SA_AFA').AsString),12,2),
			'SA_NETTO',   	SqlSzamString(JSzamla.ossz_netto,12,2),
			'SA_NEAFA',   	SqlSzamString(JSzamla.ossz_afa,12,2),
			'SA_EURNET',   	SqlSzamString(JSzamla.ossz_nettoeur,12,2),
			'SA_EURAFA',   	SqlSzamString(JSzamla.ossz_afaeur,12,2),
			'SA_EUSPED',   	SqlSzamString(JSzamla.spedicioeur,12,2),
			'SA_FTSPED',   	SqlSzamString(JSzamla.spedicioft,12,2),
			'SA_FIMOD',		''''+Query1.FieldByName('SA_FIMOD').AsString+'''',
			'SA_FLAG',		''''+Query1.FieldByName('SA_FLAG').AsString+'''',
			'SA_JARAT',		''''+Query1.FieldByName('SA_JARAT').AsString+'''',
			'SA_RSZ',		''''+Query1.FieldByName('SA_RSZ').AsString+'''',
			'SA_TIPUS',		''''+Query1.FieldByName('SA_TIPUS').AsString+'''',
			'SA_UJKOD',		IntToStr(StrToIntDef(Query1.FieldByName('SA_UJKOD').AsString,0)),
			'SA_MEGJ',		''''+Query1.FieldByName('SA_MEGJ').AsString+''''
			]);
			JSzamla.Destroy;
			Query1.Next;
		end;
		Query_Run(Query1, 'SELECT * FROM SZFEJ2 ' + sqlstr2);
		sorsz	:= 1;
		Query1.DisableControls;
		while ( ( not Query1.Eof ) and (not kilepes) ) do begin
			Application.ProcessMessages;
			Label4.Caption	:= '�sszes�tett sz�ml�k : '+Query1.FieldByName('SA_KOD').AsString+'  ('+IntToStr(sorsz)+' / '+IntToStr(Query1.RecordCount)+')';
			Label4.Update;
			Inc(sorsz);
			JSzamla		:= TJSzamla.Create(Self);
			JSzamla.FillOsszSzamla(Query1.FieldByName('SA_UJKOD').AsString);
			Query_Insert( Query2, tablanev,
			[
			// 'SA_KOD',		''''+'* '+Copy(Query1.FieldByName('SA_KOD').AsString,3,11)+'''',
      'SA_KOD',		''''+'* '+Query1.FieldByName('SA_KOD').AsString+'''',
			'SA_KIDAT',		''''+Query1.FieldByName('SA_KIDAT').AsString+'''',
			'SA_ESDAT',		''''+Query1.FieldByName('SA_ESDAT').AsString+'''',
			'SA_TEDAT',		''''+Query1.FieldByName('SA_TEDAT').AsString+'''',
			'SA_VEVOKOD',  	''''+Query1.FieldByName('SA_VEVOKOD').AsString+'''',
			'SA_VEVONEV',  	''''+Nagybetus(AposztrofCsere(Query1.FieldByName('SA_VEVONEV').AsString))+'''',
			'SA_OSSZEG',   	SqlSzamString(StringSzam(Query1.FieldByName('SA_OSSZEG').AsString),12,2),
			'SA_AFA',		SqlSzamString(StringSzam(Query1.FieldByName('SA_AFA').AsString),12,2),
			'SA_NETTO',   	SqlSzamString(JSzamla.ossz_netto,12,2),
			'SA_NEAFA',   	SqlSzamString(JSzamla.ossz_afa,12,2),
			'SA_EURNET',   	SqlSzamString(JSzamla.ossz_nettoeur,12,2),
			'SA_EURAFA',   	SqlSzamString(JSzamla.ossz_afaeur,12,2),
			'SA_EUSPED',   	SqlSzamString(JSzamla.spedicioeur,12,2),
			'SA_FTSPED',   	SqlSzamString(JSzamla.spedicioft,12,2),
			'SA_FIMOD',		''''+Query1.FieldByName('SA_FIMOD').AsString+'''',
			'SA_FLAG',		''''+Query1.FieldByName('SA_FLAG').AsString+'''',
			'SA_JARAT',		''''+Query1.FieldByName('SA_JARAT').AsString+'''',
			'SA_TIPUS',		''''+Query1.FieldByName('SA_TIPUS').AsString+'''',
			'SA_RSZ',		''''+Query1.FieldByName('SA_RSZ').AsString+'''',
			'SA_UJKOD',		IntToStr(StrToIntDef(Query1.FieldByName('SA_UJKOD').AsString,0)),
			'SA_MEGJ',		''''+Query1.FieldByName('SA_MEGJ').AsString+''''
			]);
			JSzamla.Destroy;
			Query1.Next;
		end;
		EgyebDlg.log_level	:= regilevel;
		if kilepes then begin
			Label4.Caption	:= '';
			Label4.Update;
			Exit;
		end;
		kilepes	:= true;
		if ComboBox1.ItemIndex = 6 then begin
			// A sorrend be�ll��sa
			Query_Run(Query1, 'UPDATE '+tablanev+' SET SA_SORRE = 0 ');
	 //		Query_Run(Query1, 'SELECT SA_VEVONEV, SUM(SA_AFA+SA_OSSZEG) OSSZEG FROM '+tablanev+' GROUP BY SA_VEVONEV ORDER BY OSSZEG DESC ');
			// Csak a nett� sz�m�tson
			Query_Run(Query1, 'SELECT SA_VEVONEV, SUM(SA_OSSZEG) OSSZEG FROM '+tablanev+' GROUP BY SA_VEVONEV ORDER BY OSSZEG DESC ');
			sorrend := 1;
			while not Query1.Eof do begin
				Query_Run(Query2, 'UPDATE '+tablanev+' SET SA_SORRE = '+IntToStr(sorrend)+' WHERE SA_VEVONEV = '''+AposztrofCsere(Query1.FieldByName('SA_VEVONEV').AsString)+''' ');
				Inc(sorrend);
				Query1.Next;
			end;
			Query1.First;
		end;

		sqlstr	:= 'SELECT * FROM '+tablanev;

		{Sorrend meghat�roz�sa}
		case ComboBox1.ItemIndex of
		0:
			sqlstr	:= sqlstr + ' ORDER BY SA_TEDAT,SA_VEVONEV ';
		1:
			sqlstr	:= sqlstr + ' ORDER BY SA_KIDAT,SA_VEVONEV ';
		2:
			sqlstr	:= sqlstr + ' ORDER BY SA_ESDAT,SA_VEVONEV ';
		3:
			sqlstr	:= sqlstr + ' ORDER BY SA_VEVONEV ';
		4:
			sqlstr	:= sqlstr + ' ORDER BY SA_OSSZEG ';
		5:
			sqlstr	:= sqlstr + ' ORDER BY SA_JARAT ';
		6:
			sqlstr	:= sqlstr + ' ORDER BY SA_SORRE ';
		7:
			sqlstr	:= sqlstr + ' ORDER BY SA_KOD ';
		end;
		Application.CreateForm(TKiForgListDlg, KiForgListDlg);
		if KiForgListDlg.Tolt(sqlstr,ForgalomDlg) then begin
			KiForgListDlg.Reszletes	:= CheckBox4.Checked;
			KiForgListDlg.Rep.Preview;
		end else begin
			NoticeKi('Nincs a megadott felt�teleknek megfelel� t�tel!');
		end;
		KiForgListDlg.Destroy;
		Label4.Caption	:= '';
		Label4.Update;
	end;
	finally
		Screen.Cursor	:= crDefault;
		kilepes	:= true;
		BitBtn4.Show;
		Query_Run(Query1, 'DROP TABLE '+tablanev, false);
	end;
end;

procedure TForgalomDlg.BitBtn6Click(Sender: TObject);
begin
	if not kilepes then begin
		kilepes	:= true;
	end else begin
		Close;
	end;
end;

procedure TForgalomDlg.MaskEdit1Click(Sender: TObject);
begin
	with Sender as TMaskEdit do begin
  	SelectAll;
  end;
end;

procedure TForgalomDlg.CheckBox1Click(Sender: TObject);
begin
	if CheckBox1.Checked then begin
     BitBtn5.Enabled 	:= true;
  end else begin
     MaskEdit5.Text		:= '';
     MaskEdit50.Text	:= '';
     BitBtn5.Enabled 	:= false;
  end;
end;

procedure TForgalomDlg.CheckBox2Click(Sender: TObject);
begin
	if CheckBox2.Checked then begin
	  BitBtn2.Enabled 	:= true;
  end else begin
     MaskEdit3.Text		:= '';
	  MaskEdit30.Text	:= '';
     BitBtn2.Enabled 	:= false;
  end;
end;

procedure TForgalomDlg.CheckBox3Click(Sender: TObject);
begin
	if CheckBox3.Checked then begin
	  BitBtn3.Enabled := true;
  end else begin
     jaratkod				:= '';
     MaskEdit4.Text		:= '';
	  BitBtn3.Enabled 	:= false;
  end;
end;

procedure TForgalomDlg.BitBtn10Click(Sender: TObject);
begin
	// EgyebDlg.Calendarbe(MaskEdit1);
  // EgyebDlg.ElsoNapEllenor(MaskEdit1, MaskEdit2);
  EgyebDlg.CalendarBe_idoszak(MaskEdit1, MaskEdit2);
end;

procedure TForgalomDlg.BitBtn1Click(Sender: TObject);
var
  dat:Tdate;
begin
	EgyebDlg.Calendarbe(MaskEdit2);
  dat:=StrToDate(MaskEdit2.Text);
  MaskEdit2.Text:=DateToStr(EndOfTheMonth(dat));
end;

procedure TForgalomDlg.MaskEdit1Exit(Sender: TObject);
begin
	with Sender As TMaskEdit do begin
		if ( ( Text <> '' ) and ( not Jodatum2( Sender as TMaskEdit ) ) ) then begin
			NoticeKi('Hib�s d�tum megad�sa!');
			Text := '';
			Setfocus;
        Exit;
     end;
  end;
  EgyebDlg.ElsoNapEllenor(MaskEdit1, MaskEdit2);
end;

procedure TForgalomDlg.BitBtn2Click(Sender: TObject);
var
	i : integer;
begin
	// T�bb g�pkocsi kiv�laszt�sa
	Application.CreateForm(TValgepkDlg, ValgepkDlg);
  ValgepkDlg.ARHIVNEM:=True;
	ValgepkDlg.Tolt(MaskEdit30.Text);
  	ValgepkDlg.ShowModal;
  	if ValgepkDlg.kodkilist.Count > 0 then begin
  		MaskEdit3.Text	:= '';
		MaskEdit30.Text	:= '';
     	for i := 0 to ValgepkDlg.kodkilist.Count - 1 do begin
       	MaskEdit3.Text	:= MaskEdit3.Text + ValgepkDlg.nevkilist[i]+',';
        	MaskEdit30.Text	:= MaskEdit30.Text + ValgepkDlg.kodkilist[i]+',';
     	end;
  	end;
  	ValgepkDlg.Destroy;
end;

procedure TForgalomDlg.BitBtn3Click(Sender: TObject);
begin
	JaratValaszto(M0, MaskEdit4);
//  jaratkod	:= M0.Text;
  jaratkod	:= Query_Select('JARAT', 'JA_KOD', M0.Text, 'JA_ALKOD');
end;

procedure TForgalomDlg.BitBtn5Click(Sender: TObject);
begin
	// T�bb vev� kiv�laszt�sa
{	Application.CreateForm(TValvevoDlg, ValvevoDlg);
	ValvevoDlg.Tolt(MaskEdit50.Text);
	ValvevoDlg.ShowModal;
	if ValvevoDlg.kodkilist.Count > 0 then begin
		MaskEdit5.Text		:= ValvevoDlg.nevlista+',';
		MaskEdit50.Text		:= ValvevoDlg.kodlista+',';
	end;
	ValvevoDlg.Destroy;
 }

	Application.CreateForm(TValvevo2Dlg, Valvevo2Dlg);
	case RadioGroup1.ItemIndex of
  		0: Valvevo2Dlg.DATTIP:='SA_TEDAT';
	  	1: Valvevo2Dlg.DATTIP:='SA_KIDAT';
      2: Valvevo2Dlg.DATTIP:='SA_ESDAT';
	end;
  Valvevo2Dlg.KDAT:=MaskEdit1.Text;
  Valvevo2Dlg.VDAT:=MaskEdit2.Text;
	Valvevo2Dlg.Tolt(MaskEdit50.Text);
	Valvevo2Dlg.ShowModal;
	if Valvevo2Dlg.kodkilist.Count > 0 then begin
		MaskEdit5.Text		:= Valvevo2Dlg.nevlista+',';
		MaskEdit50.Text		:= Valvevo2Dlg.kodlista+',';
	end;
	Valvevo2Dlg.Destroy;

end;

function TForgalomDlg.Nagybetus( instr : string) : string;
var
	tempstr	: string;
begin
	tempstr	:= UpperCase(instr);
	while Pos('�', tempstr) > 0 do begin
		tempstr	:= copy(tempstr, 1, Pos('�', tempstr)-1) + '�' + copy(tempstr,Pos('�', tempstr)+1, 999);
	end;
	while Pos('�', tempstr) > 0 do begin
		tempstr	:= copy(tempstr, 1, Pos('�', tempstr)-1) + '�' + copy(tempstr,Pos('�', tempstr)+1, 999);
	end;
	while Pos('�', tempstr) > 0 do begin
		tempstr	:= copy(tempstr, 1, Pos('�', tempstr)-1) + '�' + copy(tempstr,Pos('�', tempstr)+1, 999);
	end;
	while Pos('�', tempstr) > 0 do begin
		tempstr	:= copy(tempstr, 1, Pos('�', tempstr)-1) + '�' + copy(tempstr,Pos('�', tempstr)+1, 999);
	end;
	while Pos('�', tempstr) > 0 do begin
		tempstr	:= copy(tempstr, 1, Pos('�', tempstr)-1) + '�' + copy(tempstr,Pos('�', tempstr)+1, 999);
	end;
	while Pos('�', tempstr) > 0 do begin
		tempstr	:= copy(tempstr, 1, Pos('�', tempstr)-1) + '�' + copy(tempstr,Pos('�', tempstr)+1, 999);
	end;
	while Pos('�', tempstr) > 0 do begin
		tempstr	:= copy(tempstr, 1, Pos('�', tempstr)-1) + '�' + copy(tempstr,Pos('�', tempstr)+1, 999);
	end;
	while Pos('�', tempstr) > 0 do begin
		tempstr	:= copy(tempstr, 1, Pos('�', tempstr)-1) + '�' + copy(tempstr,Pos('�', tempstr)+1, 999);
	end;
	while Pos('�', tempstr) > 0 do begin
		tempstr	:= copy(tempstr, 1, Pos('�', tempstr)-1) + '�' + copy(tempstr,Pos('�', tempstr)+1, 999);
	end;
	Result	:= tempstr;
end;

procedure TForgalomDlg.CheckBox5Click(Sender: TObject);
begin
	if CheckBox5.Checked then begin
		RadioGroup1.ItemIndex	:= 0;
		RadioGroup1.Enabled		:= false;
	end else begin
		RadioGroup1.Enabled		:= true;
	end;
end;

end.




