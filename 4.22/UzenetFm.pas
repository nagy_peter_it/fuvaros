unit UzenetFm;

interface

uses
	SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
	StdCtrls, Forms, DBCtrls, DB, DBGrids, DBTables, Grids, ExtCtrls, Buttons,
	ShellApi, Dialogs, J_FOFORMUJ, ADODB;

type
	TUzenetFmDlg = class(TJ_FOFORMUJDLG)
    ButtonTekint: TBitBtn;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    BitBtn3: TBitBtn;
    CB1: TComboBox;
	  procedure FormCreate(Sender: TObject);override;
    procedure Adatlap(cim, kod : string);override;
	 procedure ButtonTekintClick(Sender: TObject);
	 procedure Mezoiro;override;
	 procedure DataSource1DataChange(Sender: TObject; Field: TField);override;
    procedure BitBtn1Click(Sender: TObject);
    procedure FormResize(Sender: TObject);override;
	end;

var
	UzenetFmDlg : TUzenetFmDlg;

implementation

uses
	Egyeb, J_SQL, Uzenetbe;

{$R *.DFM}

procedure TUzenetFmDlg.FormCreate(Sender: TObject);
begin
	Inherited FormCreate(Sender);
	FelTolto('�j �zenet felvitele ', '�zenet adatok m�dos�t�sa ',
  			'�zenetek list�ja', 'UZ_UZKOD', 'Select * from UZENET '+
				' WHERE UZ_KINEK = '''+EgyebDlg.user_code+''' OR ( UZ_KITOL =  '''+EgyebDlg.user_code+''' '+
				' AND UZ_KINEK = '''' )','UZENET', QUERY_KOZOS_TAG );
	width	:= DLG_WIDTH;
	height	:= DLG_HEIGHT;
	ButtonMod.Hide;
   ButtonTor.Hide;
   ButtonTekint.Parent	:= PanelBottom;
   BitBtn1.Parent		:= PanelBottom;
   BitBtn2.Parent		:= PanelBottom;
   BitBtn3.Parent		:= PanelBottom;
   BitBtn3.Visible		:= false;
   CB1.Parent			:= PanelBottom;
end;

procedure TUzenetFmDlg.Adatlap(cim, kod : string);
begin
	Application.CreateForm(TUzenetbeDlg, AlForm);
	Inherited Adatlap(cim, kod );
end;

procedure TUzenetFmDlg.ButtonTekintClick(Sender: TObject);
begin
	if not voltalt then begin
 		voltalt 	:= true;
 		Exit;
 	end;
	// Megtekint�s (m�dos�t�s)
  	Application.CreateForm(TUzenetbeDlg, UzenetbeDlg);
   UzenetbeDlg.Tolto('�zenet olvas�sa', Query1.FieldByName('UZ_UZKOD').AsString);
   UzenetbeDlg.ShowModal;
	if UzenetbeDlg.ret_kod <> '' then begin
   	// Friss�teni kell az adatsort
		ModLocate (Query1, FOMEZO, Query1.FieldByName(FOMEZO).AsString );
   end;
  	UzenetbeDlg.Destroy;
end;

procedure TUzenetFmDlg.Mezoiro;
begin
	Query1.FieldByname('STATUSZ').AsString := '�j �zenet';
	if Query1.FieldByName('UZ_STATU').AsString = '1' then begin
		Query1.FieldByname('STATUSZ').AsString := 'olvasott';
	end;
	Query1.FieldByname('UZTIP').AsString := 'elk�ld�tt';
	if Query1.FieldByName('UZ_TIPUS').AsString = '1' then begin
		Query1.FieldByname('UZTIP').AsString := '�rkezett';
	end;
	Query1.FieldByname('KULDO').AsString := Query_Select('JELSZO', 'JE_FEKOD', Query1.FieldByName('UZ_KITOL').AsString , 'JE_FENEV');
end;

procedure TUzenetFmDlg.BitBtn1Click(Sender: TObject);
begin
	// V�lasz a lev�lre
  	Application.CreateForm(TUzenetbeDlg, UzenetbeDlg);
   UzenetbeDlg.Tolto('V�lasz �zenet �r�sa', '');
   UzenetbeDlg.M40.Text	:= Query1.FieldByName('UZ_KITOL').AsString;
   UzenetbeDlg.ShowModal;
   if UzenetbeDlg.ret_kod <> '' then begin
   	// Friss�teni kell az adatsort
		ModLocate (Query1, FOMEZO, Query1.FieldByName(FOMEZO).AsString );
   end;
  	UzenetbeDlg.Destroy;
end;

procedure TUzenetFmDlg.DataSource1DataChange(Sender: TObject; Field: TField);
begin
	// Elk�ld�ttn�l nem l�tni a V�lasz gombot!!!!
   case StrToIntDef(Query1.FieldByName('UZ_TIPUS').AsString, 0) of
   	0 : // Elk�ld�tt lev�l
       begin
       	BitBtn1.Enabled	:= false;
       	BitBtn2.Enabled	:= false;
       end;
       1 : // �rkezett lev�l
       begin
       	BitBtn1.Enabled	:= true;
       	BitBtn2.Enabled	:= true;
       end;
   end;
end;

procedure TUzenetFmDlg.FormResize(Sender: TObject);
begin
	Inherited FormResize(Sender);
	CB1.Top		:= BitBtn3.Top + 5;
	CB1.Left	:= BitBtn3.Left + 5;
   CB1.Width   := BitBtn3.Width - 10;
end;

end.

