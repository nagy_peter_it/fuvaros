unit KiFolosList;

interface

uses
  WinTypes, WinProcs, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, QuickRpt, QRExpr, Qrctrls, ExtCtrls, printers, DB, DBTables, Grids, IniFiles,
  ADODB ;

type
  TKifolosListDlg = class(TForm)
    Rep: TQuickRep;
    QRBand3: TQRBand;
    QRLSzamla: TQRLabel;
    QRBand1: TQRBand;
    QRLabel6: TQRLabel;
    QRSysData1: TQRSysData;
    QRLabel7: TQRLabel;
    QRSysData4: TQRSysData;
    QRLabel1: TQRLabel;
    Q3: TQRLabel;
    Q2: TQRLabel;
    Q4: TQRLabel;
    QRLabel21: TQRLabel;
    Q1: TQRLabel;
	 ArfolyamGrid: TStringGrid;
    Osszesito: TStringGrid;
    QRLabel13: TQRLabel;
    QRLabel17: TQRLabel;
	 Query1: TADOQuery;
	 QRGroup1: TQRGroup;
    QRLabel4: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel15: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRShape1: TQRShape;
	 QRLabel9: TQRLabel;
	 Q6: TQRLabel;
	 QRLabel10: TQRLabel;
	 Q7: TQRLabel;
	 QRLabel11: TQRLabel;
	 QRLabel12: TQRLabel;
	 QRLabel14: TQRLabel;
	 QRLabel16: TQRLabel;
	 Query2: TADOQuery;
	 Query3: TADOQuery;
	 QRBand2: TQRBand;
	 QRLabel19: TQRLabel;
	 QRLabel22: TQRLabel;
	 QueryJarat: TADOQuery;
	 ChildBand1: TQRChildBand;
	 QRLabel18: TQRLabel;
	 QRLabel20: TQRLabel;
	 QRShape2: TQRShape;
	 QRBand4: TQRBand;
	 QRLabel23: TQRLabel;
	 QRLabel24: TQRLabel;
	 QRShape3: TQRShape;
	 QRShape4: TQRShape;
	 procedure FormCreate(Sender: TObject);
	 procedure	Tolt(dat1, dat2, rendsz	: string; reszletes : boolean );
	 procedure QRBand1BeforePrint(Sender: TQRCustomBand;
	   var PrintBand: Boolean);
	 procedure RepBeforePrint(Sender: TCustomQuickRep;
	   var PrintReport: Boolean);
	 procedure QRGroup1BeforePrint(Sender: TQRCustomBand;
	   var PrintBand: Boolean);
	 procedure QRBand2BeforePrint(Sender: TQRCustomBand;
	   var PrintBand: Boolean);
	 procedure ChildBand1BeforePrint(Sender: TQRCustomBand;
	   var PrintBand: Boolean);
    procedure QRBand4BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
  private
		sorszam			: integer;
		reszlet			: boolean;
		sqlstr			: string;
		dattol	  		: string;
		datig 			: string;
		tankliter		: double;
		tartliter		: double;
		osszfelesleg	: double;
		maradek			: double;
		kellkocsi		: string;
		vegossz			: double;
  public
	  vanadat			: boolean;
	  teljesfeles		: boolean;
  end;

var
  KifolosListDlg: TKifolosListDlg;

implementation

uses
	J_SQL, Egyeb, KOzos;

{$R *.DFM}

procedure TKifolosListDlg.FormCreate(Sender: TObject);
begin
	EgyebDlg.SetADOQueryDatabase(Query1);
	EgyebDlg.SetADOQueryDatabase(Query2);
	EgyebDlg.SetADOQueryDatabase(Query3);
	EgyebDlg.SetADOQueryDatabase(QueryJarat);
	teljesfeles	:= false;
end;

procedure	TKifolosListDlg.Tolt(dat1, dat2, rendsz	: string; reszletes : boolean );
var
	rsz	: string;
begin
	vanadat 	:= false;
	dattol		:= dat1;
	datig		:= dat2;
	reszlet		:= reszletes;
	QrLabel21.Caption	:= dat1 + ' - ' + dat2;
	if dat1 = '' then begin
		dattol	:= '1990.01.01.';
	end;
	if dat2 = '' then begin
		datig	:= EgyebDlg.MaiDatum;
	end;
	sqlstr	:= 'SELECT * FROM KOLTSEG WHERE KS_TIPUS = 0 AND KS_DATUM >= '''+dattol+''' '+
		' AND KS_DATUM <= '''+datig+''' AND KS_TETAN = 0 ';
	if rendsz <> '' then begin
		sqlstr	:= sqlstr + ' AND KS_RENDSZ = '''+rendsz+''' ';
	end;
	sqlstr	:= sqlstr + ' ORDER BY KS_RENDSZ, KS_KMORA DESC, KS_DATUM, KS_KTKOD DESC ';
	{A kapott sql alapj�n v�grehajtja a lek�rdez�st}
	Query_Run(Query1, sqlstr);
	vanadat := Query1.RecordCount > 0;
	Query_Run(QueryJarat, 'SELECT JA_KOD, JA_ORSZ, JA_ALKOD FROM JARAT ORDER BY JA_KOD');
	kellkocsi 	:= ';';
	rsz         := '';
	maradek		:= 0;
	while not Query1.Eof do begin
		if rsz	<> Query1.FieldByName('KS_RENDSZ').AsString then begin
			maradek		:= -10;
			tankliter	:= 0;
			tartliter	:= 0;
			rsz	:= Query1.FieldByName('KS_RENDSZ').AsString;
			Query_Run(Query2, 'SELECT GK_TIPUS, GK_TANKL, GK_TARTL FROM GEPKOCSI WHERE GK_RESZ = '''+Query1.FieldByName('KS_RENDSZ').AsString+''' ');
			if Query2.RecordCount > 0 then begin
				tankliter			:= StringSzam(Query2.FieldByName('GK_TANKL').AsString);
				tartliter			:= StringSzam(Query2.FieldByName('GK_TARTL').AsString);
			end;
		end;
		if ( ( Query1.FieldByName('KS_TELE').AsString = '1' ) and (UpperCase(Query1.FieldByName('KS_FIMOD').AsString) = 'TELEP' ) ) then begin
			// Inicializ�lni kell a sz�ml�l�t
			maradek	:= tankliter - StringSzam(Query1.FieldByName('KS_UZMENY').AsString) - tartliter;
		end else begin
			if UpperCase(Query1.FieldByName('KS_FIMOD').AsString) = 'TELEP' then begin
				maradek	:= maradek - StringSzam(Query1.FieldByName('KS_UZMENY').AsString);
			end else begin
				if maradek > 0 then begin
					maradek		:= maradek - StringSzam(Query1.FieldByName('KS_UZMENY').AsString);
					if maradek >= 0 then begin
						kellkocsi := kellkocsi + rsz +';';
					end else begin
						if not teljesfeles then begin
							kellkocsi := kellkocsi + rsz +';';
						end;
					end;
				end;
			end;
		end;
		Query1.Next;
	end;
	Query1.First;
end;

procedure TKifolosListDlg.QRBand1BeforePrint(Sender: TQRCustomBand;
	   var PrintBand: Boolean);
var
	felesleg    : double;
begin
	printband	:= false;
	if Pos(Query1.FieldByName('KS_RENDSZ').AsString, kellkocsi) < 1 then begin
		Exit;
	end;
	if ( ( Query1.FieldByName('KS_TELE').AsString = '1' ) and (UpperCase(Query1.FieldByName('KS_FIMOD').AsString) = 'TELEP' ) ) then begin
		// Inicializ�lni kell a sz�ml�l�t
		maradek	:= tankliter - StringSzam(Query1.FieldByName('KS_UZMENY').AsString) - tartliter;
	end else begin
		if UpperCase(Query1.FieldByName('KS_FIMOD').AsString) = 'TELEP' then begin
			maradek	:= maradek - StringSzam(Query1.FieldByName('KS_UZMENY').AsString);
		end else begin
			if maradek > 0 then begin
				felesleg    := maradek;
				if felesleg > StringSzam(Query1.FieldByName('KS_UZMENY').AsString) then begin
					felesleg := StringSzam(Query1.FieldByName('KS_UZMENY').AsString);
				end;
				maradek		:= maradek - StringSzam(Query1.FieldByName('KS_UZMENY').AsString);
				if maradek >= 0 then begin
					PrintBand	:= true;
					osszfelesleg	:= osszfelesleg + felesleg;
				end else begin
					if not teljesfeles then begin
						PrintBand	:= true;
						osszfelesleg	:= osszfelesleg + felesleg;
					end;
				end;
			end;
		end;
	end;
	if not reszlet then begin
		printband	:= false;
	end;
	if PrintBand then begin
		Q1.Caption 		:= IntToStr(sorszam);
		Inc(sorszam);
		Q2.Caption 		:= Query1.FieldByName('KS_DATUM').AsString;
		Q3.Caption 		:= Format('%.0f', [StringSzam(Query1.FieldByName('KS_KMORA').AsString)]);
		Q4.Caption		:= Format('%.2f', [StringSzam(Query1.FieldByName('KS_UZMENY').AsString)]);
		Q6.Caption		:= Format('%.2f', [felesleg]);
		Q7.Caption 		:= '';
		// Az orsz�g ki�r�sa
		if QueryJarat.Locate('JA_KOD', Query1.FieldByName('KS_JAKOD').AsString, []) then begin
			Q7.Caption 		:= GetJaratszamFromDb(QueryJarat);
		end;
	end;
end;

procedure TKifolosListDlg.RepBeforePrint(Sender: TCustomQuickRep;
	   var PrintReport: Boolean);
begin
	sorszam		:= 1;
	vegossz		:= 0;
end;

procedure TKifolosListDlg.QRGroup1BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
	// A rendsz�m be�r�sa
	if Pos(Query1.FieldByName('KS_RENDSZ').AsString, kellkocsi) < 1 then begin
		PrintBand			:= false;
		Exit;
	end;
	PrintBand			:= reszlet;
	osszfelesleg    	:= 0;
	QrLabel5.Caption	:= Query1.FieldByName('KS_RENDSZ').AsString;
	QrLabel12.Caption	:= '';
	QrLabel16.Caption	:= '';
	tankliter			:= 0;
	tartliter			:= 0;
	maradek				:= -10;
	Query_Run(Query2, 'SELECT GK_TIPUS, GK_TANKL, GK_TARTL FROM GEPKOCSI WHERE GK_RESZ = '''+Query1.FieldByName('KS_RENDSZ').AsString+''' ');
	if Query2.RecordCount > 0 then begin
		QrLabel5.Caption	:= Query1.FieldByName('KS_RENDSZ').AsString + ' ' + Query2.FieldByName('GK_TIPUS').AsString;
		tankliter			:= StringSzam(Query2.FieldByName('GK_TANKL').AsString);
		tartliter			:= StringSzam(Query2.FieldByName('GK_TARTL').AsString);
		QrLabel12.Caption	:= Format('%.1f', [tankliter])+' liter';
		QrLabel16.Caption	:= Format('%.1f', [tartliter])+' liter';
	end;
end;

procedure TKifolosListDlg.QRBand2BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
	if Pos(Query1.FieldByName('KS_RENDSZ').AsString, kellkocsi) < 1 then begin
		PrintBand			:= false;
		Exit;
	end;
	vegossz	:= vegossz + osszfelesleg;
	QrLabel19.Caption	:= Query1.FieldByName('KS_RENDSZ').AsString + ' ' + Query2.FieldByName('GK_TIPUS').AsString;
	QrLabel22.Caption	:= Format('%.2f', [osszfelesleg]);
	osszfelesleg		:= 0;
end;

procedure TKifolosListDlg.ChildBand1BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
	PrintBand	:= not reszlet;
end;

procedure TKifolosListDlg.QRBand4BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
	QrLabel24.Caption	:= Format('%.2f', [vegossz]);
end;

end.

