object Proba_szamitas: TProba_szamitas
  Left = 0
  Top = 0
  Caption = 'Proba_szamitas'
  ClientHeight = 544
  ClientWidth = 741
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 32
    Top = 27
    Width = 24
    Height = 16
    Caption = 'Lat1'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label2: TLabel
    Left = 32
    Top = 81
    Width = 24
    Height = 16
    Caption = 'Lat2'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label3: TLabel
    Left = 32
    Top = 53
    Width = 34
    Height = 16
    Caption = 'Long1'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label4: TLabel
    Left = 32
    Top = 108
    Width = 34
    Height = 16
    Caption = 'Long2'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label5: TLabel
    Left = 447
    Top = 69
    Width = 17
    Height = 16
    Caption = 'km'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Edit1: TEdit
    Left = 88
    Top = 24
    Width = 89
    Height = 24
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
  end
  object Edit2: TEdit
    Left = 88
    Top = 51
    Width = 89
    Height = 24
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
  end
  object Edit3: TEdit
    Left = 88
    Top = 78
    Width = 89
    Height = 24
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
  end
  object Edit4: TEdit
    Left = 88
    Top = 105
    Width = 89
    Height = 24
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 3
  end
  object Edit5: TEdit
    Left = 352
    Top = 66
    Width = 89
    Height = 24
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 4
  end
  object Button1: TButton
    Left = 256
    Top = 64
    Width = 75
    Height = 25
    Caption = 'Sz'#225'm'#237't'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 5
    OnClick = Button1Click
  end
  object Memo1: TMemo
    Left = 88
    Top = 184
    Width = 609
    Height = 209
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    Lines.Strings = (
      
        '{   "results" : [      {         "address_components" : [       ' +
        '     {               "long_name" : "Buderusstra'#258#378'e",            ' +
        '   "short_name" '
      
        ': "Buderusstra'#258#378'e",               "types" : [ "route" ]         ' +
        '   },            {               "long_name" : "Breidenbach",   ' +
        '            '
      
        '"short_name" : "Breidenbach",               "types" : [ "localit' +
        'y", "political" ]            },            {               "long' +
        '_name" : "Gie'#258#378'en",  '
      
        '             "short_name" : "GI",               "types" : [ "adm' +
        'inistrative_area_level_2", "political" ]            },          ' +
        '  {               '
      
        '"long_name" : "Hessen",               "short_name" : "HE",      ' +
        '         "types" : [ "administrative_area_level_1", "political" ' +
        ']            '
      
        '},            {               "long_name" : "Germany",          ' +
        '     "short_name" : "DE",               "types" : [ "country", "' +
        'political" ]            '
      
        '},            {               "long_name" : "35236",            ' +
        '   "short_name" : "35236",               "types" : [ "postal_cod' +
        'e" ]            }      '
      
        '   ],         "formatted_address" : "Buderusstra'#258#378'e, 35236 Breid' +
        'enbach, Germany",         "geometry" : {            "bounds" : {' +
        '      '
      
        '         "northeast" : {                  "lat" : 50.88704360000' +
        '001,                  "lng" : 8.4541336               },        ' +
        '       "southwest" : { '
      
        '                 "lat" : 50.8849283,                  "lng" : 8.' +
        '449442099999999               }            },            "locati' +
        'on" : {               "lat" : '
      
        '50.8858934,               "lng" : 8.451979999999999            }' +
        ',            "location_type" : "GEOMETRIC_CENTER",            '
      
        '"viewport" : {               "northeast" : {                  "l' +
        'at" : 50.8873349302915,                  "lng" : 8.4541336      ' +
        '         },               '
      
        '"southwest" : {                  "lat" : 50.8846369697085,      ' +
        '            "lng" : 8.449442099999999               }           ' +
        ' }         },         '
      
        '"partial_match" : true,         "place_id" : "ChIJVQZr3BJyvEcRc5' +
        '781rufOQ8",         "types" : [ "route" ]      }   ],   "status"' +
        ' : '
      '"OK"}')
    ParentFont = False
    TabOrder = 6
  end
  object Button2: TButton
    Left = 272
    Top = 399
    Width = 75
    Height = 25
    Caption = 'Sz'#225'm'#237't'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 7
    OnClick = Button2Click
  end
end
