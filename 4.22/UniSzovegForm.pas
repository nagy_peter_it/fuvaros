unit UniSzovegForm;

interface

uses
	SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
	Forms, Dialogs, StdCtrls, Buttons, Mask, DB, DBTables, J_ALFORM, ADODB;

type
	TUniSzovegFormDlg = class(TJ_AlformDlg)
	BitKilep: TBitBtn;
    lblSzovegelemNeve: TLabel;
    meSzoveg: TMemo;
    Query1: TADOQuery;
    BitBtn1: TBitBtn;
    Label2: TLabel;
    M1: TMaskEdit;
	  procedure BitKilepClick(Sender: TObject);
	  procedure BitElkuldClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
	private
	public
    UpdateSQLString: string;
	end;

var
	UniSzovegFormDlg: TUniSzovegFormDlg;

implementation

uses
 Egyeb, J_SQL, Kozos, Clipbrd;

{$R *.DFM}


procedure TUniSzovegFormDlg.BitKilepClick(Sender: TObject);
begin
	ret_kod := '';
	Close;
end;

procedure TUniSzovegFormDlg.FormCreate(Sender: TObject);
begin
	EgyebDlg.SeTADOQueryDatabase(Query1);
end;

procedure TUniSzovegFormDlg.BitElkuldClick(Sender: TObject);
var
  S: string;
begin
  S:= Format(UpdateSQLString, [AposztrofCsere(meSzoveg.Text)]);
  Query_Run(Query1, S);
  Close;
end;

end.




