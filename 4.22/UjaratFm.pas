unit UjaratFm;

interface

uses
	SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
	StdCtrls, Forms, ExtCtrls, Dialogs, J_FOFORMUJ, DB, ADODB, Buttons;

type
	TUjaratFormDlg = class(TJ_FOFORMUJDLG)
	 QueryJarat: TADOQuery;
    BitBtn1: TBitBtn;
    QueryAljarat: TADOQuery;
    Query3: TADOQuery;
	  procedure FormCreate(Sender: TObject);override;
	 procedure Adatlap(cim, kod : string);override;
	 procedure ButtonKuldClick(Sender: TObject);override;
	 procedure FormKeyDown(Sender: TObject; var Key: Word;
	   Shift: TShiftState);override;
	 function 	KeresChange(mezo, szoveg : string) : string; override;
	 procedure DataSource1DataChange(Sender: TObject; Field: TField);override;
	 procedure ButtonTorClick(Sender: TObject); override;
	 procedure BitBtn1Click(Sender: TObject);
	private
		vanellenor	: boolean;
		vanstorno	: boolean;
  public
		ret_orsz	: string;
		ret_alkod   : string;
	end;

var
	UjaratFormDlg : TUjaratFormDlg;

implementation

uses
	Egyeb, J_SQL, KOZOS, Kozos_Local;

{$R *.DFM}

procedure TUjaratFormDlg.FormCreate(Sender: TObject);
begin
	Inherited FormCreate(Sender);
	EgyebDLg.SetADOQueryDatabase(QueryAlJarat);
	Query_Run(QueryAlJarat, 'SELECT AJ_JAKOD, AJ_ALNEV FROM ALJARAT ORDER BY AJ_JAKOD ');
	AddSzuromezoRovid('JA_RENDSZ', 'JARAT');
	AddSzuromezoRovid('JA_ORSZ',   'JARAT');
	AddSzuromezoRovid('JA_SOFK1',  'JARAT');
	AddSzuromezoRovid('JA_SOFK2',  'JARAT');
	AddSzuromezoRovid('JA_SNEV1',  'JARAT');
	AddSzuromezoRovid('JA_SNEV2',  'JARAT');
	// AddSzuromezoRovid('JA_POTK',   'JARAT');
	AddSzuromezoRovid('JA_FAZST',  'JARAT');
	AddSzuromezoRovid('JA_FAZIS',  'JARAT');
	AddSzuromezoRovid('JA_JKEZD',  'JARAT');
	AddSzuromezoRovid('JA_FAJKO',  'JARAT');

	ButtonNez.Hide;
	ButtonMod.Hide;
	ButtonTor.Hide;
  ButtonUj.Hide;
  ButtonPrint.Hide;

  kellujures:=False;
	FelTolto('�j J�rat felvitele ', 'J�rat adatok m�dos�t�sa ',
			'J�ratok list�ja', 'JA_KOD', 'Select * from JARAT where ja_uresk>0','JARAT', QUERY_ADAT_TAG );
	width				:= DLG_WIDTH;
	height				:= DLG_HEIGHT;
	ret_orsz			:= '';
	ret_alkod   		:= '';
	BitBtn1.Parent		:= PanelBottom;
	EgyebDLg.SetADOQueryDatabase(QueryJarat);
	EgyebDLg.SetADOQueryDatabase(Query3);
	vanellenor	:= 	GetRightTag(562) <> RG_NORIGHT;
	vanstorno	:= 	GetRightTag(564) <> RG_NORIGHT;
end;

procedure TUjaratFormDlg.Adatlap(cim, kod : string);
begin
//	Application.CreateForm(TJaratbeDlg, AlForm);
	Inherited Adatlap(cim, kod );
end;

procedure TUjaratFormDlg.ButtonKuldClick(Sender: TObject);
begin
	Inherited ButtonKuldClick(Sender);
	ret_orsz	:= Query1.FieldByName('JA_ORSZ').AsString;
	ret_alkod   := Query1.FieldByName('JA_ALKOD').AsString;
end;

procedure TUjaratFormDlg.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
	Inherited FormKeyDown(Sender, Key, Shift);
end;

procedure TUjaratFormDlg.DataSource1DataChange(Sender: TObject; Field: TField);
begin
	// A nyom�gombok szab�lyoz�sa
	case Query1.FieldByName('JA_FAZIS').AsInteger  of
	  1 :
		begin
			{A sz�mla nem m�dos�that�}
			ButtonMod.Enabled 	:= false;
			ButtonMod.Hide;
			ButtonTor.Enabled 	:= false;
			ButtonTor.Hide;
			//BitBtn3.Enabled 	:= true;
			//BitBtn3.Show;
		end;
	  0, -1, -2 :
		begin
			{A sz�mla m�dos�that�}
			ButtonMod.Enabled 	:= true;
			ButtonMod.Show;
			ButtonTor.Enabled 	:= true;
			ButtonTor.Show;
			//BitBtn3.Enabled 	:= true;
			//BitBtn3.Show;
		end;
	  2 :	// Storn�zott sz�mla
		begin
			{A sz�mla nem m�dos�that�}
			ButtonMod.Enabled := false;
			ButtonMod.Hide;
			ButtonTor.Enabled := false;
			ButtonTor.Hide;
			//BitBtn3.Enabled := false;
			//BitBtn3.Hide;
		end;
	end;
	ShapeTorles.Visible	:= ButtonTor.Visible;
	// A jogosults�gok ellen�rz�se
	if not vanellenor then begin
	end;
	if not vanstorno then begin
		//BitBtn3.Enabled := false;
		//BitBtn3.Hide;
	end;
	// Alv�llalkoz�i j�rat eset�n lehet k�rni az elk�ld�tt pdf-eket
	if QueryAlJarat.Locate('AJ_JAKOD', Query1.FieldByName('JA_KOD').AsString, []) then begin
		if QueryAlJarat.FieldByName('AJ_ALNEV').AsString <> '' then begin
			// Alv�llalkoz�i j�ratr�l van sz�!!!
		end;
	end;
	inherited DataSource1DataChange(Sender, Field);
end;

function TUjaratFormDlg.KeresChange(mezo, szoveg : string) : string;
begin
	Result	:= szoveg;
	if UpperCase(mezo) = 'JA_KOD' then begin
		Result := Format('%6.6d',[StrToIntDef(szoveg,0)]);
	end;
end;

procedure TUjaratFormDlg.ButtonTorClick(Sender: TObject);
var
  jaratszam: string;
begin
	if not voltalt then begin
		voltalt 	:= true;
		Exit;
	end;
	if NoticeKi( TORLOTEXT, NOT_QUESTION ) = 0 then begin

    if Query_Select('KOLTSEG','KS_JAKOD',Query1.FieldByName(FOMEZO).AsString,'KS_JAKOD') <> '' then
    begin
      NoticeKi('FIGYELEM ! A j�rathoz k�lts�g tartozik, ez�rt nem t�r�lhet�!');
      exit;
    end;

    if Query_Select('VISZONY','VI_JAKOD',Query1.FieldByName(FOMEZO).AsString,'VI_JAKOD') <> '' then
    begin
      NoticeKi('FIGYELEM ! A j�rathoz viszony tartozik, ez�rt nem t�r�lhet�!');
      exit;
    end;

    jaratszam:=Trim(Query1.FieldByName('JA_ALKOD').AsString);
    jaratszam:=stringofchar('0',5-length(jaratszam))+jaratszam;
    jaratszam:=Query1.FieldByName('JA_ORSZ').AsString +'-'+ jaratszam;
    if (Query_Select('MEGBIZAS','MB_JAKOD',Query1.FieldByName(FOMEZO).AsString,'MB_JAKOD') <> '')and(Query_Select('MEGBIZAS','MB_JASZA',jaratszam,'MB_JASZA')<>'' ) then
//    if Query_Select2('MEGBIZAS','MB_JAKOD','MB_JASZA',Query1.FieldByName(FOMEZO).AsString, jaratszam,'MB_JAKOD') <>'' then
    begin
      NoticeKi('FIGYELEM ! A j�rathoz megb�z�s tartozik, ez�rt nem t�r�lhet�!');
      exit;
    end;
   Try
		if not Query_Run(Query2, 'DELETE from JARAT where '+FOMEZO+'  ='''+Query1.FieldByName(FOMEZO).AsString+''' ') then Raise Exception.Create('');
		if not Query_Run(Query2, 'DELETE from VISZONY where  VI_JAKOD ='''+Query1.FieldByName(FOMEZO).AsString+''' ') then Raise Exception.Create('');
		if not Query_Run(Query2, 'DELETE from KOLTSEG where  KS_JAKOD ='''+Query1.FieldByName(FOMEZO).AsString+''' ') then Raise Exception.Create('');
		if not Query_Run(Query2, 'DELETE from SULYOK where   SU_JAKOD ='''+Query1.FieldByName(FOMEZO).AsString+''' ') then Raise Exception.Create('');
		if not Query_Run(Query2, 'DELETE from JARPOTOS where JP_JAKOD ='''+Query1.FieldByName(FOMEZO).AsString+''' ') then Raise Exception.Create('');
		if not Query_Run(Query2, 'DELETE from JARSOFOR where JS_JAKOD ='''+Query1.FieldByName(FOMEZO).AsString+''' ') then Raise Exception.Create('');
		if not Query_Run(Query2, 'DELETE from ALJARAT  where AJ_JAKOD ='''+Query1.FieldByName(FOMEZO).AsString+''' ') then Raise Exception.Create('');
		if not Query_Update( Query2, 'MEGBIZAS', [
			'MB_JAKOD', ''''+''+'''',
			'MB_JASZA', ''''+''+'''',
			// 'MB_SZKOD', ''''+''+'''',
      'MB_SAKO2', ''''+''+'''',
			'MB_VIKOD', ''''+''+''''
		], ' WHERE MB_JAKOD = '''+Query1.FieldByName(FOMEZO).AsString+''' ') then Raise Exception.Create('') ;
//		], ' WHERE MB_JAKOD = '''+Query1.FieldByName(FOMEZO).AsString+''' AND MB_DATUM > '''+EgyebDlg.MaiDatum+''' ');
   Except
      NoticeKi('A t�rl�s nem siker�lt!');
   End;
		ModLocate (Query1, FOMEZO, Query1.FieldByName(FOMEZO).AsString );
	end;
end;

procedure TUjaratFormDlg.BitBtn1Click(Sender: TObject);
var
	str	: string;
begin
	if not voltalt then begin
		voltalt 	:= true;
		Exit;
	end;
	// A le�r�s �t�r�sa
	str := InputBox('Le�r�s cser�je', 'Az �j le�r�s : ', Query1.FieldByName('JA_JARAT').AsString);
	if str <> Query1.FieldByName('JA_JARAT').AsString then begin
		Query_Run(Query2, 'UPDATE JARAT SET JA_JARAT = '''+str+''' where '+FOMEZO+' ='''+Query1.FieldByName(FOMEZO).AsString+''' ');
		ModLocate (Query1, FOMEZO, Query1.FieldByName(FOMEZO).AsString );
	end;
end;

end.




