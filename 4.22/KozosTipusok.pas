unit KozosTipusok;

interface

uses system.SysUtils, System.Classes, Vcl.StdCtrls, Vcl.ExtCtrls;

type
  szamnev = array[0..9] of string[13];
  EMyWrongFormatException = class(Exception);
  TStringArray = array of string;
  TStringPar = record
     Elso: string;
     Masodik: string;
     end;
  TGeoCoordinates  = record  // f�ldrajzi koordin�ta fokokban
     Longitude: double;
     Latitude: double;
     end;
  TGeoCodeInfo = record
    Coordinates: TGeoCoordinates;
    FullJSON: string;
    end;  // record
  TEagleSMSTipus = (uzleti, technikai);
  TUserSelectMode = (becsukva, kinyitva);
  // TTruckpitIrodaiAlias = (kisautos, nagyautos, belfoldes, hutos);
  TMemoryStreamPointer = ^TMemoryStream;
  TMemoryStreamPointerArray = array of TMemoryStreamPointer;
  TMemoryStreamArray = array of TMemoryStream;
  TTermekInfo  = record
     TermekKod: string;
     MobilnetTermek: boolean;
     end;
  TEagleOutboxData = record
      darabszam: integer;
      lista: string;
      legregebbi: string;
      elerheto: boolean;
      end;
  TEKAERRiport = (import_lerakoval, nagyauto_aljarattal, kisauto_aljarattal, hutos_aljarattal, mind_aljarattal);
  TSorBeolvasoMod = (normal, standtrailer);

  TDoubleValid = record
      Value: double;
      IsValid: boolean;
      end;
  TIntegerValid = record
      Value: integer;
      IsValid: boolean;
      end;

  TMyLabel = class(TLabel)
  public
    message_id: string;
  end; // class

  TMyPanel = class(TPanel)
  public
    message_id: string;
  end; // class

  TUrlcacheItem = record
      cacheid: string;
      stream: TMemoryStream;
      url: string;
      isaccessed: boolean;  // if this item was used since app start
      end;
  TUrlcache = array of TUrlcacheItem;

  {TMyPanel = class(TPanel)
    procedure SetMessageID(const AMessageID: string);
  private
    FMessageID: string;
  published
    property message_id: string read FMessageID;
  end; // class
  }

  TTruckpitAliasInfo = record
     AliasXUID: string;
     AliasNev: string;
     AliasTelefon: string;
     AliasJelszo: string;
     Enabled: boolean;
     end;

  TTruckpitAliasInfoArray = array of TTruckpitAliasInfo;

implementation

{procedure TMyPanel.SetMessageID(const AMessageID: string);
begin
   FMessageID:= AMessageID;
end;
}


end.
