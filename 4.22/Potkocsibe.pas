unit PotkocsiBe;

interface

uses
	SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
	Forms, Dialogs, StdCtrls, Buttons, Mask, DB, DBTables, J_ALFORM, ADODB;

type
	TPotkocsibeDlg = class(TJ_AlformDlg)
	BitElkuld: TBitBtn;
	BitKilep: TBitBtn;
    Label3: TLabel;
    M2: TMaskEdit;
    BitBtn1: TBitBtn;
    Label6: TLabel;
    M3: TMaskEdit;
    Query1: TADOQuery;
    Label1: TLabel;
    M4: TMaskEdit;
    Label2: TLabel;
    M5: TMaskEdit;
    Label4: TLabel;
    M6: TMaskEdit;
    Query11: TADOQuery;
	procedure BitKilepClick(Sender: TObject);
	procedure FormCreate(Sender: TObject);
	procedure Tolt(cim : string; jaratkod, jpkod, kezdokm, vegzokm :string);
	procedure BitElkuldClick(Sender: TObject);
  	procedure MaskEditClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure BitBtn1Click(Sender: TObject);
    procedure M3Exit(Sender: TObject);
    procedure M3KeyPress(Sender: TObject; var Key: Char);
	 procedure KmHozzaadas(rendsz : string; Mkm1, Mkm2 : TMaskEdit );
    procedure M5Exit(Sender: TObject);
    procedure M6Exit(Sender: TObject);
    procedure M2Change(Sender: TObject);

	private
   	jarkod	: string;
       kezkm	: string;
       vegkm	: string;
	public
   	rendszam	: string;
   end;

var
	PotkocsibeDlg: TPotkocsibeDlg;

implementation

uses
	J_VALASZTO, Egyeb, J_SQL, Kozos, Kozos_Local,Jaratbe;
{$R *.DFM}

procedure TPotkocsibeDlg.BitKilepClick(Sender: TObject);
begin
	ret_kod 	:= '';
	Close;
end;

procedure TPotkocsibeDlg.FormCreate(Sender: TObject);
begin
  EgyebDlg.SetADOQueryDatabase(Query1);
  EgyebDlg.SetADOQueryDatabase(Query11);
	SetMaskEdits([M2]);
	ret_kod 	:= '';
end;

procedure TPotkocsibeDlg.Tolt(cim : string; jaratkod, jpkod, kezdokm, vegzokm :string);
begin
	jarkod		:= jaratkod;
	ret_kod  	:= jpkod;
	Caption  	:= cim;
	kezkm		:= kezdokm;
	vegkm		:= vegzokm;
	M2.Text  	:= '';
	M3.Text  	:= '';
	M4.Text  	:= '';
	if Trim(jpkod) <> '' then begin
   	Query_Run(Query1, 'SELECT * FROM JARPOTOS WHERE JP_JPKOD = '+jpkod, false);
       if Query1.RecordCount > 0 then begin
       	M2.Text	:= Query1.FieldByName('JP_POREN').AsString;
			M3.Text	:= Query1.FieldByName('JP_PORKM').AsString;
			M4.Text	:= Query1.FieldByName('JP_PORK2').AsString;
			M5.Text	:= Query1.FieldByName('JP_UZEM1').AsString;
			M6.Text	:= Query1.FieldByName('JP_UZEM2').AsString;
			// Nem lehet rendsz�mot v�ltoztatni
//			BitBtn1.Enabled := false;
       end;
   end;
end;

procedure TPotkocsibeDlg.BitElkuldClick(Sender: TObject);
var
  ujkodszam 	: integer;
begin
	 KmHozzaadas(rendszam, M3, M4 );
	 if M2.Text = '' then begin
	  	NoticeKi('Rendsz�m megad�sa k�telez�!');
      BitBtn1.SetFocus;
     	Exit;
   end;
   if StrToIntDef(M3.Text,-1) <= 0 then begin
	  	NoticeKi('�rv�nytelen kezd� km!');
       M3.SetFocus;
       Exit;
   end;
   if StrToIntDef(M4.Text,-1) <= 0 then begin
		NoticeKi('�rv�nytelen befejez� km!');
       M4.SetFocus;
       Exit;
   end;
   if StringSzam(M3.Text) > StringSzam(M4.Text) then begin
  		NoticeKi('Rossz km-ek megad�sa! A befejez�nek nagyobbnak kell lennie a kezd�n�l!');
	  	M4.Setfocus;
       Exit;
   end;
   if StringSzam(kezkm) > StringSzam(M3.Text) then begin
  		NoticeKi('Rossz kezd� km megad�sa! ( '+kezkm + '-'+vegkm+' )');
       M3.Setfocus;
       Exit;
   end;
   if StringSzam(M4.Text) > StringSzam(vegkm) then begin
	  	NoticeKi('Rossz befejez� km megad�sa! ( '+kezkm + '-'+vegkm+' )');
       M4.Setfocus;
       Exit;
   end;
   if EgyebDlg.HutosGk(M2.Text) and ((StringSzam(M5.Text)=0) or (StringSzam(M6.Text)=0)) then           // h�t�s
   begin
    NoticeKi('H�t�s p�tokn�l az �zem�r�t meg kell adni!');
    M5.SetFocus;
    Exit;
   end;
   if StringSzam(M5.Text) > StringSzam(M6.Text) then begin
  		NoticeKi('Rossz �ra�ll�s megad�sa! A befejez�nek nagyobbnak kell lennie a kezd�n�l!');
	  	M6.Setfocus;
       Exit;
   end;


	 if ret_kod = '' then begin
  		{�j rekord felvitele}
  		ujkodszam	:= GetNextCode('JARPOTOS', 'JP_JPKOD', 1, 0);
     	ret_kod		:= IntToStr(ujkodszam);
  		Query_Run ( Query11, 'INSERT INTO JARPOTOS (JP_JAKOD, JP_JPKOD) VALUES ('''+jarkod+''','+ret_kod+' )',true);
   end;
 	{A r�gi rekord m�dos�t�sa}
  	Query_Update (Query11, 'JARPOTOS',
    	[
    'JP_POREN', '''' + M2.Text + '''',
    'JP_PORKM', SqlSzamString(StringSzam(M3.Text), 12, 0),
		'JP_PORK2', SqlSzamString(StringSzam(M4.Text), 12, 0),
		'JP_UZEM1', SqlSzamString(StringSzam(M5.Text), 12, 2),
		'JP_UZEM2', SqlSzamString(StringSzam(M6.Text), 12, 2)
		],
    ' WHERE JP_JPKOD = '+ret_kod);
   if PotosUtkozes(jarkod) then begin
  		Query_Run ( Query11, 'DELETE FROM JARPOTOS WHERE JP_JPKOD = '+ ret_kod ,true);
       ret_kod	:= '';
		  Exit;
   end;
   Close;
end;

procedure TPotkocsibeDlg.MaskEditClick(Sender: TObject);
begin
	with Sender as TMaskEdit do begin
  		SelectAll;
  	end;
end;

procedure TPotkocsibeDlg.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
	EgyebDlg.FormReturn(Key);
end;

procedure TPotkocsibeDlg.BitBtn1Click(Sender: TObject);
var
	potk	: string;
begin
	GepkocsiValaszto(M2, GK_POTKOCSI);
   // Ha nem p�tkocsit v�lasztott, akkor hiba�zenet
   if M2.Text <> '' then begin
       potk := Query_Select('GEPKOCSI', 'GK_RESZ', M2.Text, 'GK_POTOS');
       if potk <> '1' then begin
			NoticeKi('Csak p�tkocsit lehet v�lasztani!');
           M2.Text	:= '';
           BitBtn1.SetFocus;
       end;
		if jarkod <> '' then begin
			Query_Run (Query11, 'SELECT * FROM JARPOTOS WHERE JP_JAKOD = '''+jarkod+''' '+' AND JP_POREN = '''+M2.Text+''' ',true);
			if Query11.RecordCount > 0 then begin
				if NoticeKi('L�tez� rendsz�m! Folytatja?', NOT_QUESTION) <> 0 then begin
					M2.Text	:= '';
					BitBtn1.SetFocus;
					Exit;
				end;
			end;
		end;
	end;
end;

procedure TPotkocsibeDlg.M3Exit(Sender: TObject);
begin
	SzamExit(Sender);
end;

procedure TPotkocsibeDlg.M3KeyPress(Sender: TObject; var Key: Char);
begin
	SzamKeyPress(Sender, Key);
end;

procedure TPotkocsibeDlg.KmHozzaadas(rendsz : string; Mkm1, Mkm2 : TMaskEdit );
var
	hozza	: double;
begin
   // A hozz�adott km. ellen�rz�se
   if rendsz <> '' then begin
   	hozza	:= StringSzam(Query_Select('GEPKOCSI', 'GK_RESZ', rendsz, 'GK_HOZZA'));
       if hozza > 0 then begin
       	if StringSzam(Mkm1.Text) <> 0 then begin
               if StringSzam(Mkm1.Text) < hozza then begin
               	Mkm1.Text	:= Format('%.0f', [hozza + StringSZam(Mkm1.Text)]);
               end;
           end;
       	if StringSzam(Mkm2.Text) <> 0 then begin
               if StringSzam(Mkm2.Text) < hozza then begin
               	Mkm2.Text	:= Format('%.0f', [hozza + StringSZam(Mkm2.Text)]);
               end;
           end;
       end;
   end;
end;

procedure TPotkocsibeDlg.M5Exit(Sender: TObject);
begin
	SzamExit(Sender);
	JaratbeDlg.KmHozzaadas(M2,M5 );

  if (M5.Text<>'') then begin
		 // Query_Run(Query11, 'SELECT * FROM JARPOTOS WHERE JP_POREN = '''+M2.Text+''' AND JP_JAKOD<>'''+Query1.FieldByName('JP_JAKOD').AsString+''' AND '+M5.Text+'> JP_UZEM1 AND '+M5.Text+'< JP_UZEM2');
     Query_Run(Query11, 'SELECT * FROM JARPOTOS WHERE JP_POREN = '''+M2.Text+''' AND JP_JAKOD<>'''+jarkod+''' AND '+M5.Text+'> JP_UZEM1 AND '+M5.Text+'< JP_UZEM2');

		 if Query11.RecordCount > 0 then begin
			if NoticeKi('Figyelem! A nyit� �ra�ll�s m�r szerepel egy m�sik j�rat intervallum�ban! J�ratk�d:'+Query11.FieldByname('JP_JAKOD').AsString+' �ra: '+
      Query11.FieldByname('JP_UZEM1').AsString+' - '+Query11.FieldByname('JP_UZEM2').AsString+' Folytatja?', NOT_QUESTION) <> 0 then begin
        M5.SetFocus;
			end;
		 end;
  end;
end;

procedure TPotkocsibeDlg.M6Exit(Sender: TObject);
begin
	SzamExit(Sender);
	JaratbeDlg.KmHozzaadas(M2,M6 );
  if  M6.Text<>'' then begin
		 // Query_Run(Query11, 'SELECT * FROM JARPOTOS WHERE JP_POREN = '''+M2.Text+''' AND JP_JAKOD<>'''+Query1.FieldByName('JP_JAKOD').AsString+''' AND '+M6.Text+'> JP_UZEM1 AND '+M6.Text+'< JP_UZEM2');
     Query_Run(Query11, 'SELECT * FROM JARPOTOS WHERE JP_POREN = '''+M2.Text+''' AND JP_JAKOD<>'''+jarkod+''' AND '+M6.Text+'> JP_UZEM1 AND '+M6.Text+'< JP_UZEM2');
		 if Query11.RecordCount > 0 then begin
			if NoticeKi('Figyelem! A z�r� �ra�ll�s m�r szerepel egy m�sik j�rat intervallum�ban! J�ratk�d:'+Query11.FieldByname('JP_JAKOD').AsString+' �ra: '+
      Query11.FieldByname('JP_UZEM1').AsString+' - '+Query11.FieldByname('JP_UZEM2').AsString+' Folytatja?', NOT_QUESTION) <> 0 then begin
        M6.SetFocus;
			end;
		 end;
  end;
end;

procedure TPotkocsibeDlg.M2Change(Sender: TObject);
var
  hutos: boolean;
begin
  hutos:=EgyebDlg.HutosGk(M2.Text);
  Label2.Enabled:=hutos;
  Label4.Enabled:=hutos;
  M5.Enabled:=hutos;
  M6.Enabled:=hutos;
end;

end.
