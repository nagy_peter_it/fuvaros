unit PontokFm;

interface

uses
	SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
	StdCtrls, Forms, DBCtrls, DB, DBGrids, DBTables, Grids, ExtCtrls, Buttons,
	ShellApi, Dialogs, ADODB, J_FOFORMUJ;

type
	TPontokFmDlg = class(TJ_FOFORMUJDLG)
    Query3: TADOQuery;
	  procedure FormCreate(Sender: TObject);override;
	 procedure Adatlap(cim, kod : string);override;
	 procedure DataSource1DataChange(Sender: TObject; Field: TField);override;
	end;

var
	PontokFmDlg : TPontokFmDlg;

implementation

uses
	Egyeb, J_SQL, Kozos, PontokBe;

{$R *.DFM}

procedure TPontokFmDlg.FormCreate(Sender: TObject);
begin
	EgyebDLg.SetADOQueryDatabase(Query3);
   if not Query_Run(Query3, 'SELECT * FROM PONTOK') then begin
       Query_Run(Query3, 'CREATE TABLE PONTOK (PP_PPKOD INTEGER, PP_DOKOD VARCHAR(5), PP_DONEV VARCHAR(45), PP_DATUM VARCHAR(11), PP_FEKOD VARCHAR(5), PP_ALKOD VARCHAR(5), PP_UPONT NUMERIC(12,2), PP_MEGJE VARCHAR(60) )');
   end;
	Inherited FormCreate(Sender);
	AddSzuromezoRovid('PP_DOKOD',  'PONTOK');
	AddSzuromezoRovid('PP_DONEV',  'PONTOK');
	AddSzuromezoRovid('PP_DATUM',  'PONTOK');
	AddSzuromezoRovid('PP_ALKOD',  'PONTOK');
	AddSzuromezo('SZ_MENEV', 'SELECT DISTINCT SZ_MENEV FROM SZOTAR WHERE SZ_FOKOD = ''400'' ');
   if EgyebDlg.user_super then begin
       alapstr := 'SELECT * FROM PONTOK, SZOTAR WHERE PP_ALKOD = SZ_ALKOD AND SZ_FOKOD = ''400'' ';
       alapstr := alapstr + 'AND convert(int,PP_DOKOD) not in (select JE_DOKOD from JELSZO where JE_FEKOD = '+EgyebDlg.user_code+')';  // saj�t magunkat nem l�tjuk
   end else begin
       alapstr := 'SELECT * FROM PONTOK, SZOTAR WHERE PP_ALKOD = SZ_ALKOD AND SZ_FOKOD = ''400'' AND PP_FEKOD = '''+EgyebDlg.user_code+''' ';
       alapstr := alapstr + 'AND convert(int,PP_DOKOD) not in (select JE_DOKOD from JELSZO where JE_FEKOD = '+EgyebDlg.user_code+')';  // saj�t magunkat nem l�tjuk
   end;

   FelTolto('�j pontoz�s felvitele ', 'Pontok m�dos�t�sa ',
			'Pontok list�ja', 'PP_PPKOD',
           alapstr,
           'PONTOK',
            QUERY_KOZOS_TAG);
	width	:= DLG_WIDTH;
	height	:= DLG_HEIGHT;
end;

procedure TPontokFmDlg.Adatlap(cim, kod : string);
begin
	Application.CreateForm(TPontokbeDlg, AlForm);
	Inherited Adatlap(cim, kod );
end;

procedure TPontokFmDlg.DataSource1DataChange(Sender: TObject; Field: TField);
begin
   if EgyebDlg.user_super then begin
       ButtonMod.Hide;
       ButtonTor.Hide;
       if EgyebDlg.user_code = Query1.FieldByName('PP_FEKOD').AsString then begin
           ButtonMod.Show;
           ButtonTor.Show;
       end;
   end;
end;

end.

