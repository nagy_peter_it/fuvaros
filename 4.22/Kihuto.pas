unit Kihuto;

interface

uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls, Egyeb,
  Forms, Dialogs, DBTables, DB, StdCtrls, Buttons, Mask, DBCtrls,
  ExtCtrls,DateUtils;

type
  TKihutoDlg = class(TForm)
    BitBtn4: TBitBtn;
    BitBtn6: TBitBtn;
    Label3: TLabel;
    M0: TMaskEdit;
    BitBtn1: TBitBtn;
    M1: TMaskEdit;
    Label1: TLabel;
    BitBtn2: TBitBtn;
    Label2: TLabel;
    M2: TMaskEdit;
    BitBtn3: TBitBtn;
    CB1: TCheckBox;
    BitBtn8: TBitBtn;
    M00: TMaskEdit;
    procedure FormCreate(Sender: TObject);
    procedure BitBtn4Click(Sender: TObject);
    procedure BitBtn6Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure M1Exit(Sender: TObject);
    procedure M2Exit(Sender: TObject);
    procedure BitBtn8Click(Sender: TObject);
  private
  public
  end;

var
  KihutoDlg: TKihutoDlg;

implementation

uses
	J_VALASZTO, KihutoList, Kozos;
{$R *.DFM}

procedure TKihutoDlg.FormCreate(Sender: TObject);
begin
 	SetMaskEdits([M0]);
 	M0.Text := '';
end;

procedure TKihutoDlg.BitBtn4Click(Sender: TObject);
begin
	 {A lista �ssze�ll�t�sa a sz�r�si elemek alapj�n}
   Screen.Cursor	:= crHourGlass;
   Application.CreateForm(TKihutoListDlg, KihutoListDlg);
   KihutoListDlg.Tolt(M1.Text, M2.Text, M0.Text, CB1.Checked);
   Screen.Cursor	:= crDefault;
   if KihutoListDlg.vanadat	then begin
     	KihutoListDlg.Rep.Preview;
   end else begin
	  	NoticeKi('Nincs a felt�teleknek megfelel� t�tel!');
   end;
   KihutoListDlg.Destroy;
end;

procedure TKihutoDlg.BitBtn6Click(Sender: TObject);
begin
	Close;
end;

procedure TKihutoDlg.BitBtn1Click(Sender: TObject);
begin
 //	GepkocsiValaszto(M0);
 //GepkocsiValaszto2(M0,EgyebDlg.GK_HUTOS);
	TobbGepkocsiValaszto( M0, M00, '',EgyebDlg.GK_HUTOS) ;
end;

procedure TKihutoDlg.BitBtn2Click(Sender: TObject);
begin
	// EgyebDlg.Calendarbe(M1);
  // EgyebDlg.ElsoNapEllenor(M1, M2);
  EgyebDlg.CalendarBe_idoszak(M1, M2);
end;

procedure TKihutoDlg.BitBtn3Click(Sender: TObject);
begin
	EgyebDlg.Calendarbe(M2);
  EgyebDlg.UtolsoNap(M2);
  // M2.Text:=DateToStr(EndOfTheMonth(StrToDate(M2.Text)));
end;

procedure TKihutoDlg.M1Exit(Sender: TObject);
begin
	if ( ( M1.Text <> '' ) and ( not Jodatum2(M1 ) ) ) then begin
		NoticeKi('Hib�s d�tum megad�sa!');
		M1.Text := '';
		M1.Setfocus;
	end;
  EgyebDlg.ElsoNapEllenor(M1, M2);
end;

procedure TKihutoDlg.M2Exit(Sender: TObject);
begin
	if ( ( M2.Text <> '' ) and ( not Jodatum2(M2 ) ) ) then begin
		NoticeKi('Hib�s d�tum megad�sa!');
		M2.Text := '';
		M2.Setfocus;
	end;
end;

procedure TKihutoDlg.BitBtn8Click(Sender: TObject);
begin
  M0.Text:='';
end;

end.


