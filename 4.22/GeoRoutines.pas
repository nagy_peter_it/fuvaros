unit GeoRoutines;

interface

uses KozosTipusok, Math, J_SQL, Sysutils;

  function GeoCoordinatesDistance(Point1, Point2: TGeoCoordinates): double;
  function GetGooglePlaceLink(Szelesseg, Hosszusag: double): string;
  function GetGoogleRouteLink(HonnanSzelesseg, HonnanHosszusag, HovaSzelesseg, HovaHosszusag: double): string;

implementation

function GeoCoordinatesDistance(Point1, Point2: TGeoCoordinates): double;
const
   R = 6371000; // a F�ld sugara m�terben
var
   Fi1, Fi2, DeltaFi, DeltaLambda, a, c: double;

   function sgn (a : real) : real;
    begin
      if a<0 then sgn:= -1
      else sgn:= 1;
    end;

    function atan2 (y, x : real) : real;
      begin
        if x>0 then atan2:= arctan (y/x)
        else if x<0 then atan2:= arctan (y/x) + pi
        else  atan2 := pi/2 * sgn (y);
      end;
begin
   Fi1:= DegToRad(Point1.Latitude);
   Fi2:= DegToRad(Point2.Latitude);
   DeltaFi:= DegToRad(Point2.Latitude - Point1.Latitude);
   DeltaLambda:= DegToRad(Point2.Longitude - Point1.Longitude);
   a:= sin(DeltaFi/2) * sin(DeltaFi/2) +
            cos(Fi1) * cos(Fi2) *
            sin(DeltaLambda/2) * sin(DeltaLambda/2);
   c:= 2 * atan2(sqrt(a), sqrt(1-a));
   Result:= R * c / 1000;  // km-ben

end;


function GetGooglePlaceLink(Szelesseg, Hosszusag: double): string;
var
  sz, h: string;
begin
  // http://www.google.com/maps/place/49.30166260000000,8.81875170000000
  sz:= trim(SqlSzamString(Szelesseg,16, 10));
  h:= trim(SqlSzamString(Hosszusag,16, 10));
  Result:= 'http://www.google.com/maps/place/'+sz+','+h;
end;

function GetGoogleRouteLink(HonnanSzelesseg, HonnanHosszusag, HovaSzelesseg, HovaHosszusag: double): string;
var
  sz1, sz2, h1, h2: string;
begin
  // http://www.google.com/maps/place/49.30166260000000,8.81875170000000
  sz1:= trim(SqlSzamString(HonnanSzelesseg,16, 10));
  h1:= trim(SqlSzamString(HonnanHosszusag,16, 10));
  sz2:= trim(SqlSzamString(HovaSzelesseg,16, 10));
  h2:= trim(SqlSzamString(HovaHosszusag,16, 10));

  Result:= 'http://maps.google.com/maps?saddr='+sz1+','+h1+'&daddr='+sz2+','+h2+'&t=m';
end;




end.
