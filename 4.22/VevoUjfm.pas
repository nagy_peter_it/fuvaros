unit VevoUjfm;

interface

uses
	SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
	StdCtrls, Forms, DBCtrls, DB, DBGrids, DBTables, Grids, ExtCtrls, Buttons,
	ShellApi, Dialogs, J_FOFORMUJ, ADODB, Mask, Menus, Variants;

type
	TVevoUjFmDlg = class(TJ_FOFORMUJDLG)
    BitBtn9: TBitBtn;
	 procedure FormCreate(Sender: TObject);override;
	 procedure Adatlap(cim, kod : string);override;
	 procedure ButtonKuldClick(Sender: TObject);override;
	 procedure ButtonTorClick(Sender: TObject);override;
    procedure BitBtn9Click(Sender: TObject);
	 private
	 public
	 	felista		: integer;
	end;

var
	VevoUjFmDlg : TVevoUjFmDlg;

implementation

uses
	Egyeb, J_SQL, Vevobe, Kozos;

{$R *.DFM}

procedure TVevoUjFmDlg.FormCreate(Sender: TObject);
begin
	Inherited FormCreate(Sender);
	AddSzuromezoRovid( 'VE_ARHIV', 'VEVO' );
	AddSzuromezoRovid( 'V_ORSZ',   'VEVO' );
	alapstr		:= 'Select * from VEVO';
	FelTolto('�j partner felvitele ', 'Partner adatok m�dos�t�sa ',
			'Partnerek list�ja', 'V_KOD', alapstr,'VEVO', QUERY_KOZOS_TAG );
	kulcsmezo			:= 'V_KOD';
	nevmezo				:= 'V_NEV';
	width				:= DLG_WIDTH;
	height				:= DLG_HEIGHT;

	BitBtn9.Parent		:= PanelBottom;
end;

procedure TVevoUjFmDlg.ButtonKuldClick(Sender: TObject);
begin
	Inherited ButtonKuldClick(Sender);
	ret_vnev	:= Query1.FieldByName('V_NEV').AsString;
	felista		:= StrToIntDef(Query1.FieldByName('VE_FELIS').AsString,0);
end;

procedure TVevoUjFmDlg.Adatlap(cim, kod : string);
begin
	Application.CreateForm(TVevobeDlg, AlForm);
	Inherited Adatlap(cim, kod );
end;

procedure TVevoUjFmDlg.ButtonTorClick(Sender: TObject);
var
	vekod   	: string;
   venev       : string;
   cimzett     : string;
begin
	if not voltalt then begin
		voltalt 	:= true;
		Exit;
	end;
	if ( (GetRightTag(595) = RG_NORIGHT) and not EgyebDlg.user_super ) then begin
		NoticeKi('Nincs jogosults�ga a t�rl�shez!');
       exit;
   end;
	vekod	:= Query1.FieldByName(FOMEZO).AsString;
   venev   := Query1.FieldByName('V_NEV').AsString;
	Query_Run(EgyebDlg.QueryAlap, 'SELECT mb_vekod FROM MEGBIZAS WHERE mb_vekod ='''+vekod+'''');
   if not EgyebDlg.QueryAlap.Eof then begin
       if NoticeKi('A vev�nek van megb�z�sa! Folytatja?', NOT_QUESTION) <> 0 then begin
          exit;
      end;
   end;

	if not EgyebDlg.RekordTorles(Query1, alapstr, GetOrderBy(false) ,Tag,
		'DELETE from '+FOTABLA+' where '+FOMEZO+'='''+Query1.FieldByName(FOMEZO).AsString+''' ') then begin
		Close;
	end;
	Query_Run(Query2, 'DELETE FROM VEVODIJ WHERE VD_VEKOD = '''+vekod+''' ');
	Label3.Caption	:= '('+IntToStr(Query1.RecordCount)+')';
	Label3.Update;
   // A t�rl�si �zenet elk�ld�se
   screen.Cursor   := crHourGlass;
   cimzett := EgyebDlg.Read_SZGrid('376','110');
   if cimzett <> '' then begin
       EgyebDlg.ellenlista.Clear;
       EgyebDlg.ellenlista.Add('Figyelem!');
       EgyebDlg.ellenlista.Add('');
       EgyebDlg.ellenlista.Add('Partner adatok t�rl�se. Azonos�t� : '+vekod+' '+venev);
       EgyebDlg.ellenlista.Add('Felhaszn�l� : '+EgyebDlg.user_code+' '+EgyebDlg.user_name);
       EgyebDlg.ellenlista.Add('M�velet id�pontja : '+FormatDateTime('YYYY.MM.DD hh:nn:ss', now));
       EgyebDlg.ellenlista.Add('');
       EgyebDlg.ellenlista.Add('FUVAROS rendszer');
       if not SendJSEmail (cimzett, '', 'Partner t�rl�s',  EgyebDlg.ellenlista ) then begin
           HibaKiiro('Partner t�rl�s �zenet elk�ld�se nem siker�lt! A partner k�dja : '+vekod);
       end;
   end;
   Screen.Cursor   := crDefault;
end;

procedure TVevoUjFmDlg.BitBtn9Click(Sender: TObject);
begin
  EgyebDlg.Lejartszamlak(Query1.FieldByName('V_KOD').AsString,True);
end;

end.


