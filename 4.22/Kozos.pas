﻿unit Kozos;

interface

uses
	SysUtils, Graphics, Mask, IniFiles, Forms, Classes, StdCtrls, Grids, DateUtils, Shellapi,
	Comobj, Variants , Calend1, Comctrls, WinTypes, FileList, printers, Controls,buttons, Notice, KozosTipusok,
   System.UITypes, Math ;

  function  	NoticeKi( szoveg : string; tipus : integer = 2 ) : integer;  	// Üzenet ill. kérdés megjelenítése
	procedure  ShowInfo(fnev, cim : string);                              // Információs állomány megjelenítése
  procedure ShowInfoString(S: string);
  // function ShowEditor(var S: string): boolean;
	function 	StringSzam(szamstr:string) : double;
	function 	StringEgesz(szamstr:string) : integer;
	function 	SzamString(szamstr: double; hossz, tizedes : integer; ezres : boolean = false) : string;
	function 	StrSzamStr(szamstr:string; hossz, tizedes : integer) : string;
	procedure 	SetMaskEdits(elemek : array of TMaskEdit; muvelet : integer = 0);
	procedure 	OpenIni;
	procedure 	CloseIni;
	function	ReadIni(const Section, Ident: string; Default: String = '' ) : string;
	function	ReadIniValue(const Section, Ident: string; Default: String = '' ) : string;
	procedure 	OpenLocalIni(inikod : integer );
	procedure 	CloseLocalIni;
	function	ReadLocalIni(const Section, Ident: string; Default: String = '' ) : string;
	function	ReadLocalIniSectionValues(const Section : string; list : TStringLIst) : boolean ;
	function	WriteLocalIni(const Section, Ident, Value: string) : boolean;
	function 	BoolToStr( bool : boolean ) : string;
	function 	BoolToStr01( bool : boolean ) : string;
	function 	StrToBool( str : string ) : boolean;
	function 	StrToBool01( str : string ) : boolean;
	function	WriteIni(const Section, Ident, Value: string) : boolean;
	function	ReadIniSectionValues(const Section : string; list : TStringLIst) : boolean ;
	procedure 	ComboSet (Combo : TComboBox; alkod : string; kodlist : TStringList; hanincs : integer = 0);
	procedure 	ComboSetSzoveg (Combo : TComboBox; szov : string);
	function 	IdoString(second : longint) : string;
	function   	SaveGridToFile(grid	: TStringGrid; fn : string) : boolean;
	procedure 	GridTorles(var grid:TStringGrid; sorszam : integer);
	procedure 	TablaRendezo( var griduj 		: TStringGrid;
								 oszlopok 	: array of integer;
								 tipusok   	: array of integer;	// 0 - string;  1 - szám
								 kezdo 		: integer;
								 emelkedo 	: boolean
										 );
	function 	Jodatum(var datum : string) : boolean;
	function 	Jodatum2(MEdit : TMaskEdit) : boolean;
	function 	Jodatum3(datum : string) : string;
	function 	Joido(var idopt : string) : boolean;
  function JoTelenorIdo(const IdoS: string; ures_lehet_e: boolean): boolean;
  function TelenorIdoOsszead(const Ido1S, Ido2S: string): string;
	function 	DatumKul( datum1, datum2 : string) : integer;
	function 	DatumHozNap(dat : string; napok : integer; kellhetvege : boolean) : string;
  function 	DatumHozHonap(dat : string; honapok : integer) : string;
  function 	DatumDiffHonap(dat1, dat2: string): integer;
	function 	IdoExit(Sender: TObject; kell : boolean = true) : boolean;
	procedure 	SzamExit(Sender: TObject);
	procedure 	SzamKeyPress(Sender: TObject; var Key: Char);
	function 	DatumExit(Sender: TObject; kell : boolean = true) : boolean;
	function 	Afaszam( afastr : string) : double;
	function 	BoolToIntStr( value : boolean) : string;
	procedure 	EllenListTorol;
	procedure 	EllenListAppend(szoveg : string; is_unique : boolean = false );
	procedure 	EllenListMutat(cim : string; sorted : boolean);
	procedure 	JaratEllenor( jaratkod : string);
	function 	IdezoBovito( alapstr : string ) : string;
	function	GetLastDay(datumstr : string) : string;
  function	GetFirstDay(datumstr : string) : string;
	procedure 	StringParse(forras : string; elvalaszto : string; var mezok : TStringList);
	function 	SendOutlookEmail(recipient, subject, body, attachment : string) : boolean;
	// function 	SendOutlookHTMLMmail(recipientlist, cclist: TStringList; subject, htmlbody, attachment : string; SendIt: boolean; EagleSMSTipus: TEagleSMSTipus) : boolean;
  function SendOutlookHTMLMmail(recipientlist, cclist, attachmentlist: TStringList; subject, htmlbody: string; SendIt: boolean; EagleSMSTipus: TEagleSMSTipus) : boolean;
	procedure SendDirectEmail(recipient, subject, body : string);
  procedure SendDirectEmailCC(recipient, cc, subject, body : string);
	function 	GetDateFromStr( str : string; strido : string = '' ) : TDateTime;
   function    GetNowString : string;
	function	VonalKonvert(vkod : string) : string;
	procedure 	GridExport (grid : TSTringGrid);
	procedure 	GridExportToFile (grid : TSTringGrid; fn : string);
	function	GetExportStr(ekod : integer) : string;
	function	GetExportKod(orsz1, orsz2 : string) : integer;
	procedure 	Minimalizal(Sender: TObject);
	function	GridToStr(gr : TStringGrid) : string;
	procedure	StrToGrid(str : string; gr : TStringGrid);
	function 	CopyFile (const FromName, ToName: string) : boolean;
	function 	Jochar(szoveg:string; betu:Char; egesz,tizedes : integer) : Char;
	function 	JoSzazalek(MaskEd : TMaskEdit) : Boolean;
  function JoSzam(szamstr: string; ures_lehet_e: boolean) : boolean;
  function JoFuvarosDatum(S: string; ures_lehet_e: boolean): boolean;
  function FuvarosDatumToSQLDate(S: string): string;
  function FuvarosTimestampToNemet(S: string): string;
  function JoTelefonszam(Formazott: String): string;
  function MindenfeleStringToszam(Formazott: String): double;
	function 	SzamText(szov : string) : string;
	function 	EzresSzoveg(szov : string) : string;
  function AngolSorszam (i: integer): string;
	function 	Kerekit(kereksz : double) : double;
	procedure 	FileMutato( filename, cim, st1, st2, st3, st4 : string; orient : integer);
	procedure 	FileMutato2( filename, cim : string; fejlec, lablec : TStringList; orient : integer);
	function 	DateTimeToNum( datstr, timestr : string) : double;
  function DateTimeStrToDateTime( datstr, timestr : string) : TDateTime;
	procedure 	DateTimeDifference( datstr1, timestr1, datstr2, timestr2 : string; var day, hour : integer);
   function   DateTimeDifferenceSec( datstr1, timestr1, datstr2, timestr2 : string ) : integer;
	function 	GetServerDate : TDateTime;
	function 	CalendarBe( edit : TMaskEdit ) : boolean;
	function 	EncodeString(forras : string) : string;
	function 	DecodeString(forras : string) : string;
	function	GetMuszakSzam(most : TDateTime) : integer;
	function	GetMuszakNev(muszam : integer) : string;
	procedure 	Kiegeszit (var str : string);
	function 	DateCorrection (str : string) : string;
	procedure 	ObjektumSorolo(szel : integer; gombok : array of TBitBtn);
	function	MaradekIdo(kezdes : TdateTime; eddigi, osszes : integer) : TDateTime;
	procedure   SendDirectEmail2(recipient, subject, body, filestr : string);
	function    CreateOutlookEmailAndOpen(recipient, subject, body, attachment : string) : boolean;
   procedure   SetHorEqual(holder : TControl; c_items : array of TControl; margin: integer = 0  );
   function    CheckAdoazonosito(cAdo: string): integer;
   function    SendJSEmail (cim, cccim, targy : string; sorok : TStrings) : boolean;
   function    Sorszamolo(fn : string) : integer;
   procedure   NoticeBezar;
   function    JoEgesz(Num: String): Boolean;
   function  IntToStr_default(Num: String; Default_value: integer): integer;
   procedure ListaElemTorol(var Lista: TStringList; Elem: string);
   function StringEncodeAsUTF8(str: string): string;
   function StringDecodeAsUTF8(str: ansistring): string;
   function GetElementIndex(const A: array of string; const E: string): integer;
   function FuvarosDatum_to_Kulcs(D: string): string;
   function KulcsDatum_to_Fuvaros(D: string): string;
   function JavaScriptTimestampToFuvaros(JSDate: Int64): string;
   function LoadStringFromFile(FN, LineSep: string): string;
   function LoadBinaryStringFromFile(const FN: string): string;
   function Felsorolashoz(Mihez, Mit, Szeparator: string; LehetAzonos: boolean): string;
   function StringtoHex(Data: ansistring): string;
   procedure Seplist2TStringList(S: string; Sep: string; Res: TStringList; ClearList: boolean = true);
   function GetLocalComputerName : string;
   function GetListOp(CL: string; N: integer; Sep: string): string;
   function Nevbol_extrainfo_levesz(N: string): string;
   function StringListToSepString(SL: TStringList; Sep: string; LehetAzonos: boolean): string;
   function GetKopaszEmail(TeljesEmail: string): string;
   function FormatMegrendeloKod(Datum, Terulet: string; Sorszam: integer): string;
   // procedure ClipboardMetafileToMemoryStream(var Stream: TMemoryStream; Logfn: string);
   function ClipboardMetafileToMemoryStream( Logfn: string): TMemoryStream;
   function KozosWriteLogFile(FName:string; Text:string):Boolean;
   function MemoryStreamToString(M: TMemoryStream): string;
   function Uppercase_HUN(S: string): string;
   function NetUseAddWithoutDriveMapping(RemoteName, UserName, Password: string): boolean;
   function MapNetworkDrive(const RemoteName, LocalDrive, UserName, Password: string): Boolean;
   function UnmapNetworkDrive(const LocalDrive: string): Boolean;
   function TestBit(I, B: integer): boolean;
   function CharSzamol(const ContentString: string; const KeresettChar: char): integer;
   procedure EKAERFigyelmeztetes(const UjRendszam, MBKOD: string);
   function FilesAreEqual(const File1, File2: TFileName): boolean;
   function FilesAreEqual_Info(const File1, File2: TFileName): string;
   function ListabanBenne(const Elem, Lista, Sep: string): boolean;
   function TruckpitUzenetKuldes(const ADOKOD, ADOKOD2, AUzenet, AMBKOD: string): boolean;
   function Tartalmaz(const Mi, Mit: string): boolean;

var
	Ini			: TIniFile  = nil;
	IniLocal	: TIniFile  = nil;
   NoticeDLg2  : TNoticeDlg;
{type
  szamnev = array[0..9] of string[13];
  TStringArray = array of string;
  TStringPar = record
     Elso: string;
     Masodik: string;
     end;
  TGeoCoordinates  = record  // földrajzi koordináta fokokban
     Longitude: double;
     Latitude: double;
     end;}
const

	// Konstansok a jogosultságokhoz
	RG_NORIGHT	= 1;
	RG_READONLY	= 2;
	RG_MODIFY	= 3;
	RG_ALLRIGHT	= 0;

	// Notice típusok
	NOT_QUESTION	= 1;
	NOT_MESSAGE		= 2;
	NOT_ERROR		= 3;
	NOT_DIALOG		= 4;

	// Konstansok a rekord törléshez
	TORLOCIM   		= 'Rekord törlése';
	TORLOTEXT		= 'Valóban törölni akarja a kijelölt rekordot?';

	TILTOTTSZIN		= clLime;
	FONTSZIN   		= clBlack;

	// Gépkocsi fajták választása
	GK_TRAKTOR		= 0;
	GK_POTKOCSI		= 1;
	GK_MINDENGK		= 2;

	// Grid mentéshez elválasztó karaktersorozatok
	SG_FIELD_STR	= '#@#';
	SG_GRROW_STR	= '##@@##';

	Szamok: szamnev 	= ('nulla','egy','kettő','három','négy','öt','hat','hét','nyolc','kilenc');
	Tizek: szamnev 		= ('nulla','tizen','huszon','harminc','negyven','ötven','hatvan','hetven','nyolcvan','kilencven');
	Ezrek: szamnev 		= ('','ezer','millió','milliárd','billió','billiárd','trillió','trilliárd','ezertrillió','ezertrilliárd');

  AngolHonapok: array[1..12] of string = ('January','February','March','April','May','June','July','August','September','October','November','December');

  DefaultSzamlaNyelv = 'GER';  // alapértelmezett érték új számla készítésénél, mindaddig amíg a partner default-ja nem írja felül.

  NINCS_ADAT = '-';  // rövidebbek lettek a mezők, nem fér el a '<nincs adat>';
  nem_szamolhato_cimke = '[Nem számolható]'; // '<nem számolható>'; // '* telitank! *'// '<nincs adat>';
  nem_szamolhato_cimke_kozepes = '[Nem sz.]';
  nem_szamolhato_cimke_rovid = '[-]';

  fix_szures_blokk_eleje = '/*begin*/';
  fix_szures_blokk_vege = '/*end*/';
  fix_szures_jelzo = 'Fix szűrés: ';

  Ismeretlen_Geocode_cim = 'Ismeretlen cím';

  const_CRLF = AnsiString(#13#10); // (chr(13)+chr(10)) as string;
  const_TAB = AnsiString(#9);  // chr(9);
  const_LF = AnsiString(#10); // chr(10);

  const_HTML_Ujsor = '<BR>';

  const_PDFMIME = 'application/pdf';
  const_JPGMIME = 'image/jpeg';

var
  LevelMellekletLista: TMemoryStreamPointerArray;  // TStringList;
  LevelMellekletNevek, LevelMellekletTipusok: TStringList;

implementation

uses
	Info, Egyeb, J_SQL, J_EXPORT, J_Email, Outlook2010, Clipbrd, UzenetTop;

function  NoticeKi( szoveg : string; tipus : integer ) : integer;
begin
  Result  := -1;
	NoticeDlg2	:= TNoticeDlg.Create(nil);
  try
  	NoticeDlg2.Init(szoveg, tipus);
    if tipus = NOT_DIALOG then begin
       NoticeDlg2.Show;
       end
    else begin
       NoticeDlg2.ShowModal;
       Result		:= NoticeDlg2.ret_code;
      end; // else
  finally
     if NoticeDlg2 <> nil then NoticeDlg2.Release;
     end;  // try-finally
end;

procedure NoticeBezar;
begin
    NoticeDlg2.Free;
end;

procedure ShowInfo(fnev, cim : string);
begin
	InfoDlg	:= TInfoDlg.Create(nil);
  with InfoDlg do begin
    Caption:= 'Információ';
    Memo1.ReadOnly:= True;
  	Tolt(fnev, cim);
 	  ShowModal;
	  Release;
    end;  // with
end;

procedure ShowInfoString(S: string);
begin
  with TInfoDlg.Create(nil) do begin
      Caption:= 'Információ';
      Memo1.ReadOnly:= True;
      Memo1.Clear;
      Memo1.Scrollbars:=ssVertical;
      Memo1.Lines.Add(S);
      ShowModal;
      Release;
      end;  // with
end;

{
function ShowEditor(var S: string): boolean;
begin
  with TInfoDlg.Create(nil) do begin
      Caption:= 'Szerkesztés';
      BitMegse.Visible:= True;
      Memo1.ReadOnly:= False;
      Memo1.Clear;
      Memo1.Scrollbars:=ssBoth;
      Memo1.Lines.Add(S);
      ShowModal;
      if ModalResult=mrOK then Result:= True
      else Result:= False;
      S:= memo1.Text;
      Release;
      end;  // with
end;
}



function StringSzam(szamstr:string) : double;
var
   sepjo   : string;
   sepro   : string;
   str     : string;
   pozic   : integer;
begin
 	Result	:= 0;
	if szamstr	= '' then begin
  		Exit;
  	end;
   str     := szamstr;
   sepjo   := SysUtils.FormatSettings.DecimalSeparator;
   if sepjo = '.' then begin
       sepro   := ',';
   end else begin
       sepro   := '.';
   end;
   // Ki kell cserélni a rossz szeparátorokat jóra
   pozic   := Pos(sepro, str);
   while pozic > 0 do begin
       str     := copy(str, 1, pozic-1)+sepjo+copy(str, pozic+1, 9999);
       pozic   := Pos(sepro, str);
   end;

 	try
 		Result 	:= StrToFloat(str);
 	except
    	Result := 0;
 	end;
end;

function 	StringEgesz(szamstr:string) : integer;
begin
  if trim(szamstr)	= '' then begin
      Result:= 0;
      end
  else begin
      try
        Result 	:= StrToInt(szamstr);
     	except
      	Result := 0;
 	      end;  // try-except
      end;
end;

function SzamString(szamstr: double; hossz, tizedes : integer; ezres : boolean = false) : string;
var
  locstr	: string;
begin
	if hossz < 1 then begin
		hossz := 1;
	end;
	if hossz > 20 then begin
   	hossz := 20;
   end;
   	if ezres then begin
  		locstr := FloatToStrF(szamstr, ffNumber, hossz, tizedes);
     end else begin
  		locstr := FloatToStrF(szamstr, ffFixed, hossz, tizedes);
     end;
  	if hossz < Pos('.',locstr) then begin
	  	SzamString := copy('********************',Length('********************')-hossz+1,hossz);
  	end else begin
		SzamString := copy( '                    ' + locstr,Length('                    ' + locstr)-hossz+1,hossz);
  	end;
end;

function StrSzamStr(szamstr:string; hossz, tizedes : integer) : string;
var
	ertek 	: double;
  	locstr	: string;
begin
 	Result	:= '';
   ertek := StringSzam(szamstr);
   if hossz > 20 then begin
   	hossz := 20;
   end;
  	locstr := FloatToStrF(ertek, ffFixed, hossz, tizedes);
  	if hossz < Pos('.',locstr) then begin
	  	Result := copy('********************',20-hossz+1,hossz);
  	end else begin
   	Result := copy('                    '+locstr,Length('                    '+locstr)-hossz+1,hossz);
  	end;
end;

{******************************************************************************
Függvény neve : SetMaskEdits

Funció        : A megadott MaskEdit elemek színét és engedélyezését állítja be.

Paraméterek   : elemek 	- MaskEdit tömb
                muvelet	- 	0 : tiltás
                             1 : engedélyezés

Visszaadott é.:
************************************************************************************ }
procedure 	SetMaskEdits(elemek : array of TMaskEdit; muvelet : integer = 0);
{ ********************************************************************************** }
var
	i : integer;
begin
	case muvelet of
	0: // letiltás (read only + színes háttér)
     	begin
			for i := Low(elemek) to High(elemek) do begin
				TMaskEdit(elemek[i]).Color	   	:= TILTOTTSZIN;
				TMaskEdit(elemek[i]).Enabled   	:= true;
				TMaskEdit(elemek[i]).ReadOnly	:= true;
				TMaskEdit(elemek[i]).Font.Color	:= FONTSZIN;
				TMaskEdit(elemek[i]).Font.Style := TMaskEdit(elemek[i]).Font.Style + [ fsBold ];
			end;
		end;
	1: // engedélyezés (enable + fehér háttér)
		begin
			for i := Low(elemek) to High(elemek) do begin
				TMaskEdit(elemek[i]).Color		:= clWindow;
				TMaskEdit(elemek[i]).Enabled	:= true;
				TMaskEdit(elemek[i]).ReadOnly	:= false;
				TMaskEdit(elemek[i]).Font.Style := TMaskEdit(elemek[i]).Font.Style - [ fsBold ];
			end;
		end;
end;
end;

function	ReadIni(const Section, Ident: string; Default: String = '') : string;
begin
 	Result	:= '';
  try
		Result 	:= Ini.ReadString( Section, Ident, Default );
  except
  end;
end;

function	ReadIniSectionValues(const Section : string; list : TStringLIst) : boolean ;
begin
	Result	:= false;
  try
		Ini.ReadSectionValues( Section, list );
     Result	:= true;
  except
  end;
end;

function	WriteIni(const Section, Ident, Value: string) : boolean;
begin
 	Result	:= false;
  try
		Ini.WriteString( Section, Ident, Value);
     Result 	:= true;
  except
  end;
end;

procedure OpenIni;
begin
 	Ini := TIniFile.Create( EgyebDlg.globalini );
end;

procedure CloseIni;
begin
	if Assigned(Ini) then begin
  	Ini.Free;
  end;
end;

function	ReadLocalIni(const Section, Ident: string; Default: String = '') : string;
begin
 	Result	:= '';
  	try
		Result 	:= IniLocal.ReadString( Section, Ident, Default );
  	except
  	end;
end;

function	ReadLocalIniSectionValues(const Section : string; list : TStringLIst) : boolean ;
begin
	Result	:= false;
  try
	  IniLocal.ReadSectionValues( Section, list );
     Result	:= true;
  except
  end;
end;

function	WriteLocalIni(const Section, Ident, Value: string) : boolean;
begin
 	Result	:= false;
   try
		IniLocal.WriteString( Section, Ident, Value);
       Result 	:= true;
   except
   end;
end;

procedure OpenLocalIni(inikod : integer );
begin
	if inikod = 0 then begin
		IniLocal := TIniFile.Create( EgyebDlg.localini );
	end else begin
		IniLocal := TIniFile.Create( EgyebDlg.userini );
	end;
end;

procedure CloseLocalIni;
begin
	if Assigned(IniLocal) then begin
  	Ini.Free;
  end;
end;

function BoolToStr( bool : boolean ) : string;
{*****************************************************************************}
begin
	Result	:= 'FALSE';
	if bool then begin
		Result	:= 'TRUE';
	end;
end;

function BoolToStr01( bool : boolean ) : string;
{*****************************************************************************}
begin
	Result	:= '0';
	if bool then begin
		Result	:= '1';
	end;
end;

function StrToBool( str : string ) : boolean;
begin
	Result	:= FALSE;
  	if UpperCase(str) = 'TRUE' then begin
		Result	:= TRUE;
  	end;
end;

function StrToBool01( str : string ) : boolean;
begin
	Result	:= FALSE;
  	if UpperCase(str) = '1' then begin
		Result	:= TRUE;
  	end;
end;

function ReadIniValue(const Section, Ident: string; Default: String = '' ) : string;
begin;
	OpenIni;
	Result	:= ReadIni(Section, Ident, Default);
	CloseIni;
end;

{****************************************************************************
Függvény neve : 	ComboSet
Funció        : 	A megadott lista és érték alapján beállítja a combolista
						elemét
Paraméterek   :  	Combo 	- a combobox
				   alkod 	- a keresendő szöveg
				   kodlist 	- a a kódok listája
Megjegyzés	  :
*****************************************************************************}
procedure ComboSet (Combo : TComboBox; alkod : string; kodlist : TStringList; hanincs : integer = 0);
{****************************************************************************}
var
	sorszam	: integer;
begin
	sorszam	:= kodlist.IndexOf(alkod);
	if sorszam < 0 then begin
		sorszam	:= hanincs;
	end;
	Combo.ItemIndex	:= sorszam;
end;

procedure 	ComboSetSzoveg (Combo : TComboBox; szov : string);
var
	sorszam	: integer;
begin
	sorszam	:= Combo.Items.IndexOf(szov);
	if sorszam < 0 then begin
		sorszam	:= 0;
	end;
	Combo.ItemIndex	:= sorszam;
end;


{A függvény formázza a másodpercekben megadott időt }
function IdoString(second : longint) : string;
{***************************************************}
var
	ora	: longint;
  perc	: longint;
  sec	: longint;
begin
	ora 			:= second div 3600;
  second 		:= second - ora * 3600;
  perc 			:= second div 60;
  sec 			:= second - perc * 60;
  if ora <= 999 then begin
		IdoString 	:= Format('%3dh%3d''%3d"',[ora,perc,sec]);
  end else begin
		IdoString 	:= Format('%5dh%2d''%2d"',[ora,perc,sec]);
  end;
end;

function		SaveGridToFile(grid	: TStringGrid; fn : string) : boolean;
var
	fila	: TextFile;
  i, j	: integer;
begin
	Result	:= false;
  {$I-}
  if FileExists(fn) then begin
  	AssignFile(fila, fn);
  	Append(fila);
  end else begin
  	AssignFile(fila, fn);
  	Rewrite(fila);
  end;
  {$I+}
  if IOResult <> 0 then begin
     Exit;
  end;
  // A táblázat tartalmának kiírása
  Writeln(fila);
  Writeln(fila);
 	for j := 0 to grid.RowCount -1 do begin
  	for i := 0 to grid.ColCount - 1 do begin
     	Write(fila, grid.Cells[i,j]+'; ');
     end;
     Writeln(fila);
  end;
 	CloseFile(fila);
  Result	:= true;
end;

{******************************************************************************
Függvény neve : GridTorles

Funció        : A megadott gridből kitörli a megadott sorszámú sort

Paraméterek   : 	grid 		- a módosítandó stringgrid
                 sorszam  - a törlendő sor száma

Visszaadott é.:
****************************************************************************** }
procedure 	GridTorles(var grid:TStringGrid; sorszam : integer);
{***************************************************************************** }
var
	i, j : integer;
begin
  	grid.Rows[sorszam].Clear;
	if sorszam > grid.RowCount - 1 then begin
  		Exit;
  	end;
	for i := sorszam to grid.RowCount - 2 do begin
  		for j := 0 to grid.ColCount-1 do begin
  			grid.Cells[j, i ] := grid.Cells[j, i+1 ];
     	end;
  	end;
//  	if grid.RowCount > 2 then begin
  		grid.RowCount := grid.RowCount - 1;
//  	end;
end;

{******************************************************************************
Függvény neve : TablaRendezo

Funció        : A megadott gridet rendezi az oszloptömbnek megfelelően

Paraméterek   : 	griduj		- a rendezendő stringgrid
                 oszlopok  	- a rendezési oszlopok száma
                 tipusok     - a rendezési oszlopok típusa
                 					 0 - string;  1 - szám
                 kezdo			- az első rendezendő sor száma
                 emelkedo		- növekvő vagy csökkenő a sorrend (true/false)

Visszaadott é.:	-
****************************************************************************** }
procedure TablaRendezo( var 	griduj 		: TStringGrid;
			  							oszlopok 	: array of integer;
										tipusok 		: array of integer;
                             kezdo 		: integer;
                             emelkedo 	: boolean
										 );
var
     i, j     : integer;
     tempstr  : string;
     sorszam	: integer;
     sorlist	: TStringList;
     grid2		: TStringGrid;
begin
	if kezdo < 0 then begin
  	    Exit;
   end;
	if Griduj.RowCount < (kezdo + 1) then begin
  	    Exit;
   end;
   sorlist 	:= TStringList.Create;
	for i := kezdo to Griduj.RowCount - 1 do begin
  	    tempstr := '';
  	    for j := 0 to High(oszlopok) do begin
     	    if tipusok[j] = 0 then begin
        	    // Az oszlop szöveget tartalmaz
     		    tempstr := tempstr + Griduj.Cells[oszlopok[j], i];
           end else begin
        	    // Az oszlop számot tartalmaz
     		    tempstr := tempstr + Format('%20.5f',[StringSzam(Griduj.Cells[oszlopok[j], i])]);
           end;
       end;
		sorlist.Add(tempstr+'^#^'+Format('%8.8d',[i]));
	end;
   sorlist.Sort;
   grid2		:= TStringGrid.Create(Application);
   grid2.Name := 'GRIDNEW';
   grid2.ColCount := Griduj.ColCount;
 	grid2.RowCount := Griduj.RowCount;
   for i := 0 to Griduj.RowCount - 1 do begin
  	    grid2.Rows[i].Assign(Griduj.Rows[i]);
	end;
   for i := 0 to sorlist.Count - 1 do begin
  	    tempstr := sorlist[i];
       sorszam := StrToIntDef(copy(tempstr,Pos('^#^',tempstr)+3,999),0);
       if emelkedo then begin
     	    // Rendezés növekvő sorrendben
  		    Griduj.Rows[kezdo + i].Assign(grid2.Rows[sorszam]);
       end else begin
     	    // Rendezés csökkenő sorrendben
  		    Griduj.Rows[kezdo + sorlist.Count - 1 - i].Assign(grid2.Rows[sorszam]);
       end;
   end;
   sorlist.Free;
   grid2.Free;
end;

// átalakítja helyes dátummá, ha lehet
function Jodatum(var datum : string) : boolean;
begin
	if Length(datum) = 5 then begin
		// A dátum hh.nn formátumú
		datum	:= EgyebDlg.EvSzam+'.'+copy(datum,1,2)+'.'+copy(datum,4,2)+'.';
	end;
	if Length(datum) = 6 then begin
		// A dátum ééhhnn formátumú
		if copy(datum,1,2) < '50' then begin
			datum := '20'+copy(datum,1,2)+'.'+copy(datum,3,2)+'.'+copy(datum,5,2)+'.';
		end else begin
			datum := '19'+copy(datum,1,2)+'.'+copy(datum,3,2)+'.'+copy(datum,5,2)+'.';
		end;
	end;
	if Length(datum) = 8 then begin
		// A dátum ééééhhnn formátumú
		datum := copy(datum,1,4)+'.'+copy(datum,5,2)+'.'+copy(datum,7,2)+'.';
	end;
	if Length(datum) >= 9 then begin
		// A dátum ééééxhhxnn formátumú
		datum := copy(datum,1,4)+'.'+copy(datum,6,2)+'.'+Format('%2.2d', [StrToIntDef(copy(datum,9,2),0)])+'.';
	end;
	Result	:= IsValidDate(StrToIntDef(copy(datum,1,4),0), StrToIntDef(copy(datum,6,2),0), StrToIntDef(copy(datum,9,2),0));
   if StrToIntDef(copy(datum,1,4),0) < 1900 then begin
       Result:=False;
   end;
end;

function Jodatum2(MEdit : TMaskEdit) : boolean;
var
  datum 	: string;
  ho			: integer;
  nap		: integer;
begin
	datum := MEdit.Text;

  // KG 20111012
  if LowerCase(datum)='ma' then
    datum:=DateToStr(date);

	if Length(datum) = 2 then
  begin
    if (MonthOf(date))>9 then
    	datum	:=IntToStr(YearOf(date))+'.'+IntToStr(MonthOf(date))+'.'+datum+'.'
    else
    	datum	:=IntToStr(YearOf(date))+'.0'+IntToStr(MonthOf(date))+'.'+datum+'.';
  end;
  /////////////////////////////
	if Pos('/',datum) > 0 then begin
		ho		:= StrToIntDef(copy(datum,1,Pos('/',datum)-1),0);
  		nap		:= StrToIntDef(copy(datum,Pos('/',datum)+1,20),0);
  		datum	:= format('%4.4d.%2.2d.%2.2d.',[StrToIntDef(copy(EgyebDlg.MaiDatum,1,4),0),ho,nap]);
  end;
	if Length(datum) = 5 then begin
  		// A dátum hh.nn formátumú
     	datum	:= EgyebDlg.EvSzam + '.' + datum;
  end;
	if Length(datum) = 4 then begin
  		// A dátum hhnn formátumú
     	datum	:= EgyebDlg.EvSzam + datum;
  end;
	if Length(datum) = 6 then begin
		if copy(datum,1,2) < '50' then begin
     		datum := '20'+copy(datum,1,2)+'.'+copy(datum,3,2)+'.'+copy(datum,5,2)+'.';
    end else begin
     		datum := '19'+copy(datum,1,2)+'.'+copy(datum,3,2)+'.'+copy(datum,5,2)+'.';
    end;
  end;
  if Length(datum) = 8 then begin
  		datum := copy(datum,1,4)+'.'+copy(datum,5,2)+'.'+copy(datum,7,2)+'.';
  end;
  if Length(datum) > 9 then begin
  		datum := copy(datum,1,4)+'.'+copy(datum,6,2)+'.'+copy(datum,9,2)+'.';
  end;
  Result	:= Jodatum(datum);
  if Result then begin
  		MEdit.Text	:= datum;
  end;
end;

function Jodatum3(datum : string) : string;
var
//  datum 	: string;
  ho			: integer;
  nap		: integer;
begin
	//datum := dat;

  // KG 20111012
  if LowerCase(datum)='ma' then
    datum:=DateToStr(date);

	if Length(datum) = 2 then
  	datum	:=IntToStr(YearOf(date))+'.'+IntToStr(MonthOf(date))+'.'+datum+'.';
  /////////////////////////////
	if Pos('/',datum) > 0 then begin
		ho		:= StrToIntDef(copy(datum,1,Pos('/',datum)-1),0);
  		nap		:= StrToIntDef(copy(datum,Pos('/',datum)+1,20),0);
  		datum	:= format('%4.4d.%2.2d.%2.2d.',[StrToIntDef(copy(EgyebDlg.MaiDatum,1,4),0),ho,nap]);
  	end;
	if Length(datum) = 5 then begin
  		// A dátum hh.nn formátumú
     	datum	:= EgyebDlg.EvSzam + '.' + datum;
  	end;
	if Length(datum) = 4 then begin
  		// A dátum hhnn formátumú
     	datum	:= EgyebDlg.EvSzam + datum;
  	end;
	if Length(datum) = 6 then begin
		if copy(datum,1,2) < '50' then begin
     		datum := '20'+copy(datum,1,2)+'.'+copy(datum,3,2)+'.'+copy(datum,5,2)+'.';
     	end else begin
     		datum := '19'+copy(datum,1,2)+'.'+copy(datum,3,2)+'.'+copy(datum,5,2)+'.';
     	end;
  	end;
  	if Length(datum) = 8 then begin
  		datum := copy(datum,1,4)+'.'+copy(datum,5,2)+'.'+copy(datum,7,2)+'.';
  	end;
  	if Length(datum) > 9 then begin
  		datum := copy(datum,1,4)+'.'+copy(datum,6,2)+'.'+copy(datum,9,2)+'.';
  	end;
  	if Jodatum(datum) then
      Result:=datum
    else
      Result:='';  
end;

function 	Joido(var idopt : string) : boolean;
var
   str : string;
begin
   if Pos(':', idopt) > 0 then begin
       str     := copy(idopt, Pos(':', idopt)+1, 999 );
       idopt   := copy(idopt, 1, Pos(':', idopt)-1 );
       if Pos(':', str) > 0 then begin
        	idopt   := Format ('%2.2d:%2.2d:%2.2d',[StrToIntDef(idopt,0), StrToIntDef(copy(str, 1, Pos(':', str)-1),0), StrToIntDef(copy(idopt,Pos(':', str)+1,999),0)]);
       end else begin
           idopt   := Format ('%2.2d:%2.2d',[StrToIntDef(idopt,0),StrToIntDef(str,0)]);	// Rövid forma -> óó:pp
       end;
   end else begin
       if Length(idopt) = 4 then begin
           // ÓÓPP alakú időpont
           idopt   := copy(idopt, 1, 2)+':'+copy(idopt, 3, 2);
       end;
       if Length(idopt) = 6 then begin
           // ÓÓPPSS alakú időpont
           idopt   := copy(idopt, 1, 2)+':'+copy(idopt, 3, 2)+':'+copy(idopt, 5, 2);
       end;

   end;
	Result := IsValidTime(StrToIntDef(copy(idopt,1,2),0), StrToIntDef(copy(idopt,4,2),0), StrToIntDef(copy(idopt,6,2),0), 0);
  	if Result then begin
     	if Length(idopt) < 7 then begin
        	idopt		:= Format ('%2.2d:%2.2d',[StrToIntDef(copy(idopt,1,2),0),StrToIntDef(copy(idopt,4,2),0)]);	// Rövid forma -> óó:pp
     	end else begin
        	idopt		:= Format ('%2.2d:%2.2d:%2.2d',[StrToIntDef(copy(idopt,1,2),0),StrToIntDef(copy(idopt,4,2),0), StrToIntDef(copy(idopt,6,2),0)]);
     	end;
  	end;
end;

{**************************************************************************************
Függvény neve : 	DatumKul
Funció        : 	Kiszámítja két dátum különbségét napokban
Paraméterek   :  	dat1		- a nagyobbik dátum
                  	dat2		- a kisebbik dátum
Visszaadott é.: 	a napok száma
						-1  		- hiba esetén
Megjegyzés	  :     AZ ELSŐ DÁTUM A NAGYOBB !!!
***************************************************************************************}
function DatumKul( datum1, datum2 : string) : integer;
{*********************************************************************}
begin
	Result := -1;
  	if ( ( Jodatum(datum1) ) and ( Jodatum(datum2) ) and (datum1 >= datum2) ) then begin
  		Result	:= 	DaysBetween(EncodeDate(StrToIntDef(copy(datum2,1,4),0), StrToIntDef(copy(datum2,6,2),0), StrToIntDef(copy(datum2,9,2),0)),
       						EncodeDate(StrToIntDef(copy(datum1,1,4),0), StrToIntDef(copy(datum1,6,2),0), StrToIntDef(copy(datum1,9,2),0)));
  	end;
end;

function IdoExit(Sender: TObject; kell : boolean = true) : boolean;
var
	idostr	: string;
	idostr2	: string;
	i		: integer;
begin
	Result := true;
	if Sender is TMaskEdit then begin
		idostr	:= TMaskEdit(Sender).Text;
		if not kell and ( idostr = '' ) then begin
			// Üresen is jó a mező
			Exit;
		end;
		if kell and ( idostr = '' ) then begin
			// Üresen nem jó a mező
			NoticeKi('Hibás időpont megadása !', NOT_ERROR);
			TMaskEdit(Sender).Setfocus;
			Result := false;
			Exit;
		end;
		idostr2	:= '';
		for i := 1 to Length(idostr) do begin
			if Pos(copy(idostr,i,1), '0123456789') > 0 then begin
				idostr2	:= idostr2 + copy(idostr,i,1);
			end;
		end;
		case Length(idostr2) of
			4 : idostr	:= copy(idostr2, 1, 2) + ':' + copy(idostr2, 3, 2);
			3 :
				begin
					if copy(idostr2,1,1) = '0' then begin
						idostr	:= copy(idostr2, 1, 2) + ':' + '0' +copy(idostr2, 3, 1);
					end else begin
						idostr	:= '0'+copy(idostr2, 1, 1) + ':' + copy(idostr2, 2, 2);
					end;
				end;
//       	2 : idostr	:= '0'+copy(idostr2, 1, 1) + ':' + '0' + copy(idostr2, 2, 1);
			2 : idostr	:= idostr2+':00';
		end;
		if ( ( TMaskEdit(Sender).Text <> '' ) and ( not Joido(idostr) ) ) then begin
			NoticeKi('Hibás időpont megadása !', NOT_ERROR);
			TMaskEdit(Sender).Setfocus;
			idostr	:= '';
			Result 	:= false;
		end;
		TMaskEdit(Sender).Text := idostr;
  end;
end;

function Afaszam( afastr : string) : double;
begin
  if pos(' ',afastr) > 0 then begin
		Result	:= stringszam(copy(afastr,1,Pos(' ',afastr)-1));
  end else begin
		Result	:= stringszam(afastr);
  end;
end;

procedure SzamExit(Sender: TObject);
begin
	with Sender as TMaskEdit do begin
		Text 	:= StrSzamStr( Text , MaxLength, Tag);
 	end;
end;

function DatumExit(Sender: TObject; kell : boolean = true) : boolean;
begin
	Result	:= false;
	with Sender as TMaskEdit do begin
  		if not kell and ( Text = '' ) then begin
     		// Üresen is jó a mező
			  Result	:= true;
     		Exit;
     	end;
		if not Jodatum2( Sender as TMaskEdit ) then begin
			NoticeKi('Hibás dátum megadása !');
			Text := '';
			Setfocus;
     	Exit;
    end;
//    if StrToInt( copy(Text,1,4))<>YearOf(Date) then
//			NoticeKi('A dátum nem idei. Ellenőrizze!');
  end;
	Result	:= true;
end;

function 	BoolToIntStr( value : boolean) : string;
begin
	Result	:= '0';
	if value then begin
		Result	:= '1';
  	end;
end;

procedure EllenListTorol;
begin
  EgyebDlg.ellenlista.Clear;
end;

procedure EllenListAppend(szoveg : string; is_unique : boolean = false);
begin
	if is_unique then begin
		if EgyebDlg.ellenlista.Indexof(szoveg) > - 1 then begin
			{Már van a listában, nem kell hozzáfűzni}
			Exit;
		end;
	end;
	EgyebDlg.ellenlista.Add(szoveg);
end;

procedure EllenListMutat(cim : string; sorted : boolean);
begin
  	if EgyebDlg.ellenlista.Count > 0 then begin
  		if sorted then begin
     		EgyebDlg.ellenlista.Sort;
     	end;
  		{Meg kell mutatni a listát}
     	EgyebDlg.ellenlista.SaveToFile(EgyebDlg.TempList+'ELLENLI.TXT');
     	Showinfo(EgyebDlg.TempList+'ELLENLI.TXT', cim);
  	end;
end;

procedure JaratEllenor( jaratkod : string );
var
	fazis	: integer;
begin
	if Query_Run(EgyebDlg.QueryAlap, 'SELECT JA_FAZIS, JA_ORSZ, JA_ALKOD FROM JARAT WHERE JA_KOD = '''+jaratkod+''' ', true) then begin
  	if EgyebDlg.QueryAlap.RecordCount > 0 then begin
        fazis	:= StrToIntDef(EgyebDlg.QueryAlap.FieldByname('JA_FAZIS').AsString,0);
        if fazis < 1 then begin
           {Még nem ellenőrzött járat}
           EllenListAppend('Nem ellenőrzött járat : ('+jaratkod + ' ) ' +
              EgyebDlg.QueryAlap.FieldByname('JA_ORSZ').AsString + '-' +
              Format('%5.5d',[StrToIntDef(EgyebDlg.QueryAlap.FieldByname('JA_ALKOD').AsString,0)]), true);
        end;
     end;
  end;
end;

function 	IdezoBovito( alapstr : string ) : string;
begin
  // Result	:= ''''+StringReplace(alapstr, ',', ''',''', [rfReplaceAll])+'zzzz''';  // mi ez a zzzz?
  Result	:= ''''+StringReplace(alapstr, ',', ''',''', [rfReplaceAll])+'''';
end;

function	GetLastDay(datumstr : string) : string;
begin
	Result	:= FormatDateTime('YYYY.MM.DD.', EndOfTheMonth(EncodeDate(StrToIntDef(copy(datumstr,1,4),0), StrToIntDef(copy(datumstr,6,2),0), StrToIntDef(copy(datumstr,9,2),0))));
end;

function	GetFirstDay(datumstr : string) : string;
begin
	Result	:= FormatDateTime('YYYY.MM.DD.', StartOfTheMonth(EncodeDate(StrToIntDef(copy(datumstr,1,4),0), StrToIntDef(copy(datumstr,6,2),0), StrToIntDef(copy(datumstr,9,2),0))));
end;

procedure 	StringParse(forras : string; elvalaszto : string; var mezok : TStringLIst);
var
	tempstr	: string;
begin
	mezok.Clear;
   tempstr	:= forras;
   while Pos(elvalaszto, tempstr) > 0 do begin
   	mezok.Add(copy(tempstr, 1, Pos(elvalaszto, tempstr) - 1));
       tempstr	:= copy(tempstr, Pos(elvalaszto, tempstr) + Length(elvalaszto) , 99999);
   end;
	if tempstr <> '' then begin
		mezok.Add(tempstr);
	end;
end;

procedure SzamKeyPress(Sender: TObject; var Key: Char);
begin
	with Sender as TMaskEdit do begin
		Key := Jochar(Text,Key,MaxLength,Tag);
	end;
end;

function SendOutlookEmail(recipient, subject, body, attachment : string) : boolean;
var
	Outlook: OLEVariant;
	MailItem: Variant;
begin
	try
		try
			Outlook:=GetActiveOleObject('Outlook.Application') ;
		except
			Outlook:=CreateOleObject('Outlook.Application') ;
		end;
		MailItem := Outlook.CreateItem(0) ;
		MailItem.Recipients.Add(recipient) ;
		MailItem.Subject := subject;
		MailItem.Body := body;
		if attachment <> '' then begin
			MailItem.Attachments.Add(attachment) ;
		end;
		MailItem.Send;
		Outlook := Unassigned;
		Result	:= true;
	except
		Result	:= false;
	end;
end;

function SendOutlookHTMLMmail(recipientlist, cclist, attachmentlist: TStringList; subject, htmlbody: string; SendIt: boolean; EagleSMSTipus: TEagleSMSTipus) : boolean;
var
	Outlook: OLEVariant;
	MailItem: Variant;
  i: integer;
  LocalSubject, Localhtmlbody, SMSCimzettek, LevelInfo: string;
  LocalRecipientlist, LocalCCList: TStringList;
  HolVagyok: string;

   procedure LocalSendMail(UseAttachments: boolean);
     var
      i: integer;
     begin
      HolVagyok:= '3.1';
      MailItem := Outlook.CreateItem(0) ;
      HolVagyok:= '3.2';
      for i:=0 to LocalRecipientlist.Count-1 do
         MailItem.Recipients.Add(LocalRecipientlist[i]) ;   // a Type default = 1 = "To"
      for i:=0 to LocalCCList.Count-1 do
         MailItem.Recipients.Add(LocalCCList[i]).Type:= 2;  // ettõl lesz cc, 3 = BCC
      MailItem.Subject := LocalSubject;
      MailItem.BodyFormat := olFormatHTML;
      MailItem.HTMLBody := Localhtmlbody;
      HolVagyok:= '3.3';
      if UseAttachments then begin
        for i:=0 to attachmentlist.Count-1 do
           MailItem.Attachments.Add(attachmentlist[i]);
        end; // if
      HolVagyok:= '3.4';
      if SendIt then MailItem.Send
      else MailItem.Display;
      HolVagyok:= '3.5';
     end;

begin
	try
		try
			Outlook:=GetActiveOleObject('Outlook.Application') ;
      // NoticeKi('SendOutlookHTMLMmail: Outlook OLE megnyitva');
		except
			Outlook:=CreateOleObject('Outlook.Application') ;
      // NoticeKi('SendOutlookHTMLMmail: Outlook OLE létrehozva');
		  end; // try-except
    if EgyebDlg.SMS_ROUTING_HASZNALATA then begin  // "új" mód: külön email és jssms.hu küldés
      // szétválasztjuk a címzett listákat
      try
        HolVagyok:= '1';
        LocalRecipientlist:= TStringList.Create;
        LocalCCList:= TStringList.Create;
        SMSCimzettek:= '';
        for i:=0 to recipientlist.Count-1 do
          if Pos(EgyebDlg.SMSEAGLE_DOMAIN, recipientlist[i])>0 then begin // ami SMS irányba megy
            LocalRecipientlist.Add(recipientlist[i]);
            SMSCimzettek:= Felsorolashoz(SMSCimzettek, recipientlist[i], ', ', False);
            end; // if
        for i:=0 to cclist.Count-1 do
          if Pos(EgyebDlg.SMSEAGLE_DOMAIN, cclist[i])>0 then begin // ami SMS irányba megy
            LocalCCList.Add(cclist[i]);
            SMSCimzettek:= Felsorolashoz(SMSCimzettek, cclist[i], ', ', False);
            end;  // if
        if (LocalRecipientlist.Count > 0) or (LocalCCList.Count > 0) then begin
          if EagleSMSTipus = uzleti then
            LocalSubject:= 'modemno=1';
          if EagleSMSTipus = technikai then
            LocalSubject:= 'modemno=2';
          Localhtmlbody:= subject+' '+htmlbody;
          LocalSendMail(False);
          end;  // if ha van címzett
        HolVagyok:= '1.1';
        LocalRecipientlist.Clear;
        LocalCCList.Clear;
        for i:=0 to recipientlist.Count-1 do
          if Pos(EgyebDlg.SMSEAGLE_DOMAIN, recipientlist[i])=0 then  // ami nem SMS irányba megy
            LocalRecipientlist.Add(recipientlist[i]);
        for i:=0 to cclist.Count-1 do
          if Pos(EgyebDlg.SMSEAGLE_DOMAIN, cclist[i])=0 then  // ami nem SMS irányba megy
            LocalCCList.Add(cclist[i]);
        HolVagyok:= '1.2';
        if (LocalRecipientlist.Count > 0) or (LocalCCList.Count > 0) then begin
          LocalSubject:= subject;
          // Localhtmlbody:= htmlbody;
          LevelInfo:= '';
          if Trim(SMSCimzettek) <> '' then
            LevelInfo:= '(SMS-ben megkapta: '+SMSCimzettek+') ';
          Localhtmlbody:= LevelInfo+htmlbody;
          HolVagyok:= '1.3';
          LocalSendMail(True);
          end;  // if ha van címzett
      finally
        if LocalRecipientlist <> nil then LocalRecipientlist.Free;
        if LocalCCList <> nil then LocalCCList.Free;
        end;  // try-finally
      end  // if "új" mód = SMS_ROUTING_HASZNALATA
    else begin // "régi" mód: nincs külön email és jssms.hu küldés
      HolVagyok:= '2';
      LocalSubject:= subject;
      Localhtmlbody:= htmlbody;
      LocalRecipientlist:= recipientlist;
      HolVagyok:= '2.1';
      LocalCCList:= TStringList.Create;
      try
        LocalSendMail(True);
      finally
        if LocalCCList <> nil then LocalCCList.Free;
        end;  // try-finally
      end; // else
		Outlook := Unassigned;
		Result	:= true;
	except
    on E: system.sysutils.exception do begin
      NoticeKi('SendOutlookHTMLMmail hiba ('+HolVagyok+'):'+ E.Message);
      HibaKiiro('SendOutlookHTMLMmail hiba ('+HolVagyok+'):'+ E.Message);
   		Result	:= false;
      end; // on E
  	end; // try-except
end;

{function SendOutlookHTMLMmail(recipientlist, cclist: TStringList; subject, htmlbody, attachment : string; SendIt: boolean) : boolean;
var
	Outlook: OLEVariant;
	MailItem: Variant;
  i: integer;
begin
	try
		try
			Outlook:=GetActiveOleObject('Outlook.Application') ;
		except
			Outlook:=CreateOleObject('Outlook.Application') ;
		end;
		MailItem := Outlook.CreateItem(0) ;
    for i:=0 to recipientlist.Count-1 do
       MailItem.Recipients.Add(recipientlist[i]) ;   // a Type default = 1 = "To"
    for i:=0 to cclist.Count-1 do
       MailItem.Recipients.Add(cclist[i]).Type:= 2;  // ettől lesz cc, 3 = BCC
		MailItem.Subject := subject;
    MailItem.BodyFormat := olFormatHTML;
		MailItem.HTMLBody := htmlbody;
		if attachment <> '' then begin
			MailItem.Attachments.Add(attachment) ;
		end;
    if SendIt then MailItem.Send
    else MailItem.Display;
		Outlook := Unassigned;
		Result	:= true;
	except
		Result	:= false;
	end;
end;
}

function CreateOutlookEmailAndOpen(recipient, subject, body, attachment : string) : boolean;
var
	Outlook		: OLEVariant;
	NmSpace, Folder: OleVariant;
	MailItem	: Variant;
begin
	// Email létrehozása és megnyitása
	Result	:= true;
	try
		try
			Outlook:=GetActiveOleObject('Outlook.Application') ;
		except
			Outlook:=CreateOleObject('Outlook.Application') ;
			NmSpace := Outlook.GetNamespace('MAPI');
			NmSpace.Logon(EmptyParam, EmptyParam, False, True);
			Folder := NmSpace.GetDefaultFolder(6);
			Folder.Display;
		end;
		MailItem := Outlook.CreateItem(0) ;
		if recipient <> '' then begin
			MailItem.Recipients.Add(recipient) ;
		end;
		MailItem.Subject := subject;
		MailItem.Body := body;
		if attachment <> '' then begin
			MailItem.Attachments.Add(attachment) ;
		end;
		MailItem.Display;
//		MailItem.Send;
		Outlook := Unassigned;
	except
		Result	:= false;
	end;
end;

procedure SendDirectEmail(recipient, subject, body : string);
var
	em_mail : string;
begin
  body:=StringReplace(body, '"', '', [rfReplaceAll]);  // 2016-03-23 - a shell hívást megakasztja
	em_mail := 'mailto:'+recipient+'?subject=' + subject + '&body=' + body ;
  // NoticeKi('Mérete: '+IntToStr(Length(em_mail))+' byte.');
	ShellExecute(Application.Handle,'open', PChar(em_mail), nil, nil, SW_SHOWNORMAL) ;
end;

procedure SendDirectEmailCC(recipient, cc, subject, body : string);
var
	em_mail : string;
begin
  body:=StringReplace(body, '"', '', [rfReplaceAll]);  // 2016-03-23 - a shell hívást megakasztja
	em_mail := 'mailto:'+recipient+'?cc='+cc+'&subject=' + subject + '&body=' + body ;
	ShellExecute(Application.Handle,'open', PChar(em_mail), nil, nil, SW_SHOWNORMAL) ;
end;

// "Standard mailto protocol does not support attachments"
procedure SendDirectEmail2(recipient, subject, body, filestr : string);
var
	em_mail : string;
begin
	em_mail := 'mailto:'+recipient+'?subject=' + subject + '&body=' + body + '&Attachment='''+filestr+'''' ;
//	em_mail := 'mailto:br.joe@freemail.hu?subject=tesztecske&body=ez a belseje&attachment=D:\Work\Mas\FUVAR_ADO\UJMEZOK.TXT' ;
 	em_mail := 'mailto:br.joe@freemail.hu?subject=tesztecske&body=ez a belseje&Attachment=''D:\\Work\\Mas\\FUVAR_ADO\\UJMEZOK.TXT''' ;
	ShellExecute(Application.Handle,'open', PChar(em_mail), nil, nil, SW_SHOWNORMAL) ;
end;


function GetDateFromStr( str : string; strido : string = '' ) : TDateTime;
var
	ora		: integer;
	perc    : integer;
begin
	try
		if Length(strido) = 4 then begin
			strido	:= '0' + strido;
		end;
		if strido <> '' then begin
			ora		:= StrToIntDef(copy(strido,1,2),0);
			perc	:= StrToIntDef(copy(strido,4,2),0);
			if ora = 24 then begin
				Result	:= EncodeDateTime(StrToIntDef(copy(str,1,4),0), StrToIntDef(copy(str,6,2),0), StrToIntDef(copy(str,9,2),0), 23, 59, 59, 999);
			end else begin
				Result	:= EncodeDateTime(StrToIntDef(copy(str,1,4),0), StrToIntDef(copy(str,6,2),0), StrToIntDef(copy(str,9,2),0), ora, perc, 0, 0);
			end;
		end else begin
			Result  := EncodeDate(StrToIntDef(copy(str,1,4),0), StrToIntDef(copy(str,6,2),0), StrToIntDef(copy(str,9,2),0));
		end;
  	except
  		Result	:= 0;
  	end;
end;

{******************************************************************************
Függvény neve : DatumHozNap

Funció        : A megadott dátumhoz hozzáadja a kívánt napokat. Ellenorzi, hogy
                a kapott dátum hétvége e. Ha a flag true, akkor  hétvége esetén
                a következo hétfot adja vissza a rutin.

Paraméterek   : dat - a kiinduló dátum
                napok - a hozzáadni kívánt napszám
                hetvege - flag a hétvége lekezelésére
                				igen  - a hétvégét átugorja
                          nem	- a szombatot, vasárnapot is beleszámolja

Visszaadott é.: az új dátum 11 hosszú karakteres formában
********************************************************************************************************** }
function DatumHozNap(dat : string; napok : integer; kellhetvege : boolean) : string;
{**********************************************************************************}
var
  ev, ho, nap : integer;
  tmpDate  : TDateTime;
begin
  DatumHozNap := '';
  if ( ( dat = '' ) or (Length(dat) < 10 ) ) then begin
     Exit;
  end;
  ev    := StrToIntDef(copy(dat,1,4),0);
  ho    := StrToIntDef(copy(dat,6,2),0);
  nap   := StrToIntDef(copy(dat,9,2),0);
  if ( ( ev = 0 ) or ( ho = 0 ) or ( nap = 0 ) ) then begin
     Exit;
  end;
  tmpDate := EncodeDate(ev, ho, nap) + napok;
  if not kellhetvege then begin
     if DayOfWeek(tmpDate) = 1 then begin      // vasárnap
        tmpDate := tmpDate + 1;
     end else begin
        if DayOfWeek(tmpDate) = 7 then begin   // szombat
           tmpDate := tmpDate + 2;
        end;
     end;
  end;
  DatumHozNap := FormatDateTime('yyyy.mm.dd.',tmpDate);
end;


function 	DatumHozHonap(dat : string; honapok : integer) : string;
var
  ev, ho, nap : integer;
  tmpDate  : TDateTime;
begin
  Result := '';
  if ( ( dat = '' ) or (Length(dat) < 10 ) ) then begin
     Exit;
  end;
  ev    := StrToIntDef(copy(dat,1,4),0);
  ho    := StrToIntDef(copy(dat,6,2),0);
  nap   := StrToIntDef(copy(dat,9,2),0);
  if ( ( ev = 0 ) or ( ho = 0 ) or ( nap = 0 ) ) then begin
     Exit;
     end;
  ho := ho + honapok;
  if ho > 0 then begin
    ev := ev + (ho div 12);
    ho := ho mod 12;
    end
  else begin
    ev := ev + (ho div 12) - 1;   // pl. 2015; -3 = 2014
    ho := 12 + (ho mod 12);       // ho = -3 --> 9
    end;
  tmpDate := EncodeDate(ev, ho, nap);
  Result := FormatDateTime('yyyy.mm.dd.',tmpDate);
end;

function 	DatumDiffHonap(dat1, dat2: string): integer;
var
  ev1, ho1, nap1: integer;
  ev2, ho2, nap2: integer;
  Res: integer;
begin
  if ( dat1 = '' ) or (Length(dat1) < 10 ) or ( dat2 = '' ) or (Length(dat2) < 10 ) then begin
     Exit;
  end;
  ev1    := StrToIntDef(copy(dat1,1,4),0);
  ho1    := StrToIntDef(copy(dat1,6,2),0);
  nap1   := StrToIntDef(copy(dat1,9,2),0);
  ev2    := StrToIntDef(copy(dat2,1,4),0);
  ho2    := StrToIntDef(copy(dat2,6,2),0);
  nap2   := StrToIntDef(copy(dat2,9,2),0);

  if (ev1 = 0) or (ho1 = 0) or (nap1 = 0) or (ev2 = 0) or (ho2 = 0) or (nap2 = 0) then begin
     Exit;
     end;

  // Res:= (12-ho1)+ho2+(ev2-ev1-1)*12;
  Res:= ho2-ho1+(ev2-ev1)*12;
  If nap1 > nap2 then Res:=Res - 1;  // nincs ki az egész hónap

  Result := Res;
end;


function	VonalKonvert(vkod : string) : string;
var
	vonalstr	: string;
	pozic		: integer;
	elo			: string;
begin
	vonalstr	:= vkod;
	if Length(vonalstr) = 7 then begin
		elo		:= copy(vonalstr,1,1);
		vonalstr	:= elo + elo + elo + elo + elo + elo + vonalstr;
	end;
	pozic		:= Pos('ö', vonalstr);
   while pozic > 0 do begin
		vonalstr	:= copy(vonalstr,1,pozic-1) + '0' + copy(vonalstr,pozic+1,999);
		pozic		:= Pos('ö', vonalstr);
   end;
	pozic		:= Pos('Ö', vonalstr);
   while pozic > 0 do begin
		vonalstr	:= copy(vonalstr,1,pozic-1) + '0' + copy(vonalstr,pozic+1,999);
		pozic		:= Pos('Ö', vonalstr);
   end;
	Result	:= vonalstr;
end;

procedure GridExport (grid : TSTringGrid);
begin
	// Tetszőleges grid kiexportálása valamibe
	Application.CreateForm(TExportDlg, ExportDlg);
	ExportDlg.ToltoGrid('GRID EXPORT',grid);
	ExportDlg.ShowModal;
	ExportDlg.Destroy;
end;

procedure GridExportToFile (grid : TSTringGrid; fn : string);
begin
	// Tetszőleges grid kiexportálása valamibe
	Application.CreateForm(TExportDlg, ExportDlg);
	ExportDlg.ToltoGrid('GRID EXPORT',grid);
	ExportDlg.SilentXls(fn);
	ExportDlg.Destroy;
end;


function	GetExportStr(ekod : integer) : string;
var
	str	: string;
begin
	case ekod of
		0 :	str	:= 'import';
		1 : str := 'export';
		2 : str := 'külföld';
		3 : str := 'belföld';
	else
			str := '';
	end;
	Result	:= str;
end;

function	GetExportKod(orsz1, orsz2 : string) : integer;
var
	ek	: integer;
begin
	if ( ( trim(orsz1) = 'HU' ) and ( trim(orsz2) = 'HU' ) ) then begin
		ek	:= 3;
	end else begin
		if ( trim(orsz1) = 'HU' ) then begin
			ek	:= 1;
		end else begin
			if ( trim(orsz2) = 'HU' ) then begin
				ek	:= 0;
			end else begin
				ek	:= 2;
			end;
		end;
	end;
	Result	:= ek;
end;

procedure Minimalizal(Sender: TObject);
begin
(*
	with Sender as Tform do begin
		if WindowState = wsMinimized then begin
			WindowState := wsNormal;
			Application.Minimize;
	  end;
  end;
*)
end;


function	GridToStr(gr : TStringGrid) : string;
var
	ro 	: integer;
	co	: integer;
begin
	Result	:= '';
	for ro := 0 to gr.RowCount - 1 do begin
		for co := 0 to gr.ColCount - 1 do begin
			Result	:= Result + gr.Cells[co, ro] + SG_FIELD_STR;
		end;
		Result	:= Result + SG_GRROW_STR;
	end;
	if Result	= SG_GRROW_STR then begin
		Result	:= '';
	end;
end;

procedure	StrToGrid(str : string; gr : TStringGrid);
var
	ro 		: integer;
	co		: integer;
	sor		: TStringList;
	mez		: TStringList;
begin
	mez	:= TStringList.Create;
	sor	:= TStringList.Create;
	gr.Rowcount	:= 1;
	gr.Colcount	:= 1;
	gr.Rows[0].Clear;
	StringParse(str,  SG_GRROW_STR, sor);
	if sor.Count > 0 then begin
		gr.RowCount := sor.Count;
		for ro	:= 0 to sor.Count - 1 do begin
			StringParse(sor[ro],  SG_FIELD_STR, mez);
			if mez.Count > 0 then begin
				if gr.ColCount < mez.Count then begin
					gr.ColCount	:= mez.Count;
				end;
				for co := 0 to gr.ColCount - 1 do begin
					gr.Cells[co, ro] := mez[co];
				end;
			end;
		end;
	end;
	mez.Free;
	sor.Free;
end;

function CopyFile (const FromName, ToName: string) : boolean;
var
	ff, tf: TFileStream;
begin
	try
		ff := TFileStream.Create (FromName, fmOpenRead);
		try
			tf := TFileStream.Create (ToName, fmOpenWrite or fmCreate);
			try
				tf.CopyFrom (ff, ff.Size);
			finally
				tf.Free;
			end;
		finally
			ff.Free;
		end;
		Result  := true;
	except
		Result  := false;
	end;
end;

function 	Jochar(szoveg:string; betu:Char; egesz,tizedes : integer) : Char;
begin
	Jochar := Chr(0);
	if ( ( ( betu >= '0' ) and ( betu <= '9' ) ) or ( betu = FormatSettings.DecimalSeparator )
		or ( betu < Chr(32) ) or ( betu = '-' ) or ( betu = '*' ) ) then begin
		Jochar := betu;
		if ( ( betu = FormatSettings.DecimalSeparator ) and ( pos(FormatSettings.DecimalSeparator,szoveg)>0  ) ) then
			Jochar := Chr(0);
	end;
end;

function JoSzazalek(MaskEd : TMaskEdit) : Boolean;
var
	kedv : double;
begin
	JoSzazalek := true;
	if MaskEd.Text <> '' then begin
	kedv := StringSzam(MaskEd.Text);
		if ( ( kedv < 0 ) or ( kedv > 100 ) ) then begin
			NoticeKi('Hibás % megadása !', NOT_ERROR);
			MaskEd.Text := '';
			MaskEd.SetFocus;
			JoSzazalek := false;
	  end else begin
		MaskEd.Text := SzamString(kedv,6,2);
		end;
	end;
end;

function SzamText(szov : string) : string;
var
	ss 		: string;
  resz		: string;
  kezdo 	: integer;
  minusz   : boolean;
begin
	ss := '';
	minusz := false;
	if copy(szov,1,1) = '-' then begin
		minusz	:= true;
		szov		:= copy(szov,2,255);
	end;
	while Length(szov) > 0 do begin
		kezdo := Length(szov) mod 3;
		if kezdo = 0 then begin
			kezdo := 3;
		end;
		resz := copy(szov,1,kezdo);
		szov := copy(szov,kezdo+1,Length(szov));
		if resz <> '000' then begin
			resz := EzresSzoveg(resz);
			if szov <> '' then begin
				resz := resz+String(Ezrek[(Length(szov)-1) div 3 + 1])+'-';
				if ((ss='') and (Length(szov)=3) and (resz='egyezer-')) then begin
					resz := 'egyezer';
				end;
			end;
			ss := ss + resz;
		end;
	end;
	if ss = '' then begin
		ss := 'nulla';
	end;
	if copy(ss,length(ss),1) = '-' then begin
		ss := copy(ss,1,length(ss)-1);
	end;
	if minusz then begin
		ss := 'mínusz '+ss;
	end;
	SzamText := ss;
end;

function EzresSzoveg(szov : string) : string;
var
	szam 	: double;
	szam2 : double;
  st 	: string;
  ezres	: integer;
  szazas, tizes, egyes : integer;
begin
  szam := Kerekit(StrToFloat(szov));
  szam2 := szam - Int(szam / 1000 ) * 1000 ;
  st := '';
  ezres := Trunc(szam2);
	szazas 	:= ezres div 100;
  tizes  	:= (ezres mod 100) div 10;
  egyes 	:= ezres mod 10;
  if szazas > 0 then begin
	st := String(Szamok[szazas])+'száz';
  end;
  if tizes > 0 then begin
	st := st + String(Tizek[tizes]);
  end;
  if egyes > 0 then begin
		st := st + String(Szamok[egyes]);
  end else begin
	if tizes = 1 then begin
		st := copy(st,1,Length(st)-5)+'tíz';
	  end;
	if tizes = 2 then begin
		st := copy(st,1,Length(st)-6)+'húsz';
	  end;
  end;
  EzresSzoveg := st;
end;

function AngolSorszam (i: integer): string;
var
  egyesek, tizesek: integer;
  utotag: string;
begin
  egyesek:= i mod 10;
  tizesek:= Floor(i / 10) mod 10;
  if tizesek = 1 then
       utotag:= 'th'
  else begin
     case egyesek of
       1: utotag:= 'st';
       2: utotag:= 'nd';
       3: utotag:= 'rd';
       else utotag:= 'th';
       end;  // case
     end;  // else
  Result:= IntToStr(i)+utotag;
end;

function 	Kerekit(kereksz : double) : double;
var
	ssz  : double;
begin
	if kereksz > 0 then begin
		ssz	:= Int( kereksz	+ 0.5);
	end else begin
		ssz	:= Int( kereksz	- 0.5);
	end;
	Kerekit := ssz;
end;

procedure 	FileMutato( filename, cim, st1, st2, st3, st4 : string; orient : integer);
var
	lista	: TStringlist;
begin
	lista := TStringList.Create;
	if st1 <> '' then begin
		lista.Add(st1);
	end;
	if st2 <> '' then begin
		lista.Add(st2);
	end;
	if st3 <> '' then begin
		lista.Add(st3);
	end;
	if st4 <> '' then begin
		lista.Add(st4);
	end;
	FileMutato2( filename, cim , lista, nil , orient );
	lista.Free;
end;

procedure 	FileMutato2( filename, cim : string; fejlec, lablec : TStringList; orient : integer);
begin
	Application.CreateForm(TFileListDlg, FileListDlg);
	FileListDlg.repfilename 	:= filename;
 	FileListDlg.focim			:= cim;
 	FileListDlg.fejlecek.Assign(fejlec);
  	if not Assigned(lablec) then begin
  		lablec	:= TStringList.Create;
  	end;
 	FileListDlg.lablecek.Assign(lablec);
	FileListDlg.cegnev					:= EgyebDlg.Read_SZGrid( '999', '101' );
	FileListDlg.Kezdofej				:= EgyebDlg.Kezdofej;
	FileListDlg.Rep.Page.Orientation 	:= poPortrait;
	FileListDlg.Vanvonal 				:= EgyebDlg.VanVonal;
	if orient <> 0 then begin
		FileListDlg.Rep.Page.Orientation := poLandScape;
	end;
	FileListDlg.Rep.Preview;
	FileListDlg.Destroy;
	EgyebDlg.Kezdofej	:= true;
	EgyebDlg.VanVonal	:= true;
end;

function DateTimeToNum( datstr, timestr : string) : double;
var
  ev, ho, nap 	: integer;
  ora, perc, sec : integer;
begin
   // Formátumok : EEEE.HH.NN. OO:PP:MM
	Result 	:= 0;
	ev 		:= StrToIntDef(copy(datstr,1,4),0);
	ho 		:= StrToIntDef(copy(datstr,6,2),0);
	nap    	:= StrToIntDef(copy(datstr,9,2),0);
	if ( ( ev = 0 ) or ( ho = 0 ) or (nap=0) ) then begin
		Exit;
	end;
	ora 	:= StrToIntDef(copy(timestr,1,2),0);
	perc 	:= StrToIntDef(copy(timestr,4,2),0);
	sec 	:= StrToIntDef(copy(timestr,7,2),0);
	try
		Result 	:= EncodeDate(ev, ho, nap) + EncodeTime(ora, perc, sec, 0);
	except
		Result 	:= 0;
	end;
end;

function DateTimeStrToDateTime( datstr, timestr : string) : TDateTime;
var
  ev, ho, nap 	: integer;
  ora, perc, sec : integer;
begin
   // Formátumok : EEEE.HH.NN. OO:PP:MM
	Result 	:= 0;
	ev 		:= StrToIntDef(copy(datstr,1,4),0);
	ho 		:= StrToIntDef(copy(datstr,6,2),0);
	nap    	:= StrToIntDef(copy(datstr,9,2),0);
	if ( ( ev = 0 ) or ( ho = 0 ) or (nap=0) ) then begin
		Exit;
	end;
	ora 	:= StrToIntDef(copy(timestr,1,2),0);
	perc 	:= StrToIntDef(copy(timestr,4,2),0);
	sec 	:= StrToIntDef(copy(timestr,7,2),0);
	try
		Result 	:= EncodeDate(ev, ho, nap) + EncodeTime(ora, perc, sec, 0);
	except
		Result 	:= 0;
	end;
end;

procedure DateTimeDifference( datstr1, timestr1, datstr2, timestr2 : string; var day, hour : integer);
var
	td1, td2	: TDateTime;
   kuld		: TDateTime;
begin
	day 		:= 0;
   hour		:= 0;
	td1 		:= DateTimeToNum(datstr1, timestr1);
	td2 		:= DateTimeToNum(datstr2, timestr2);
   if ( (td1 = 0) or (td2 = 0) ) then begin
	    Exit;
   end;
	kuld		:= td2 - td1;
   if ( kuld < 0 ) then begin
	    Exit;
   end;
   day		    := Round(Int(kuld));
   hour		:= Round(Int((Frac(kuld)+0.000001) * 24));	{Felfelé kerekíti az időt}
end;


function DateTimeDifferenceSec( datstr1, timestr1, datstr2, timestr2 : string ) : integer;
var
   sec    : integer;
   sec1   : integer;
   sec2   : integer;
begin
   Result  := -1;
   sec    := Datumkul(datstr1, datstr2);
   if sec < 0 then begin
       Exit;
   end;
   // Átszámoljuk a különbségeket
   sec     := sec * 3600 * 24;
   sec1    := StrToIntDef(copy(timestr1,1,2),0)*3600+StrToIntDef(copy(timestr1,4,2),0)*60+StrToIntDef(copy(timestr1,7,2),0);
   sec2    := StrToIntDef(copy(timestr2,1,2),0)*3600+StrToIntDef(copy(timestr2,4,2),0)*60+StrToIntDef(copy(timestr2,7,2),0);
   if sec = 0 then begin
       // Azonos nap
       Result := sec1 - sec2;
       if Result < 0 then begin
           Result := -1
       end;
   end else begin
       // Nem azonos a nap
       Result := sec - (3600*24-sec1) + (3600*24-sec2);
   end;
end;

function GetServerDate : TDateTime;
(*******************************************)
var
  fname 		: string;
  fhandle 		: Integer;
begin
	Result 	:= Now;
	fname 	:= EgyebDlg.GetTempFileName;
	fhandle 	:= FileCreate(fname);
	if fhandle > 0 then begin
	{ Az állomány dátumának lekérése}
	  Result := FileDateToDateTime(FileGetDate(fhandle));
	  FileClose(fhandle);
	DeleteFile(PWideChar(fname));
  end;
end;

function CalendarBe( edit : TMaskEdit ) : boolean;
{*************************************************}
var
	datestr	: string;
begin
  {Beolvassa a kiválasztott dátumot egy tömbből}
	Result	:= false;
	datestr := edit.Text;
	if datestr = '' then begin
		datestr := EgyebDlg.MaiDatum;
	end;
	Application.CreateForm(TCalendarDlg, CalendarDlg);
	CalendarDlg.Tolt(datestr);
	CalendarDlg.ShowModal;
	if CalendarDlg.ret_date <> '' then begin
		Result		:= true;
		edit.Text 	:= CalendarDlg.ret_date;
	end;
	CalendarDlg.Destroy;
end;

function 	EncodeString(forras : string) : string;
var
	res     : string;
	i       : integer;
	ch      : char;
	chkod   : integer;
begin
	Result  := '';
	res     := '';
	for i := 1 to Length(forras) do begin
		ch  := forras[i];
		chkod := ord(ch) - 5;
		res := res + chr(chkod);
	end;
	Result  := res;
end;

function 	DecodeString(forras : string) : string;
var
	res     : string;
	i       : integer;
	ch      : char;
	chkod   : integer;
begin
	Result  := '';
	res     := '';
	for i := 1 to Length(forras) do begin
		ch  := forras[i];
		chkod := ord(ch) + 5;
		res := res + chr(chkod);
	end;
	Result  := res;
end;

function	GetMuszakSzam(most : TDateTime) : integer;
begin
	if ( ( TimeOf(most) >= EncodeTime(6, 0, 0, 0) ) and ( TimeOf(most) < EncodeTime(14, 0, 0, 0) ) ) then begin
		Result	:= 0;
	end else begin
		if ( ( TimeOf(most) >= EncodeTime(14, 0, 0, 0) ) and ( TimeOf(most) < EncodeTime(22, 0, 0, 0)) ) then begin
			Result	:= 1;
		end else begin
			Result	:= 2;
		end;
	end;
end;

function	GetMuszakNev(muszam : integer) : string;
begin
	Result	:= '';
	case muszam of
		0 : Result := 'nappalos';
		1 : Result := 'délutános';
		2 : Result := 'éjszakás';
	end;
end;

procedure Kiegeszit (var str : string);
begin
	if ( str[Length(str)] <> '\' ) and  ( str[Length(str)] <> '/' ) then begin
		str	:= str + '\';
	end;
end;

function 	DateCorrection (str : string) : string;
var
	dat		: string;
	pozic	: integer;
begin
	dat		:= str;
	pozic	:= Pos(' ', dat);
	while pozic > 0 do begin
		dat		:= copy(dat, 1, pozic - 1) + copy(dat, pozic + 1, 99999);
		pozic	:= Pos(' ', dat);
	end;
	Result	:= dat;
end;

procedure ObjektumSorolo(szel : integer; gombok : array of TBitBtn);
var
	i 		: integer;
	db		: integer;
	oszel	: integer;
begin
	db		:= 0;
	oszel	:= 0;
	for i := Low(gombok) to High(gombok) do begin
		Inc(db);
		oszel	:= oszel +gombok[i].width;
	end;
	if oszel > 0 then begin
		oszel	:= ( szel - oszel ) div (db + 1);
		db		:= 0;
		for i := Low(gombok) to High(gombok) do begin
			if i > Low(gombok) then begin
				db	:= db + gombok[i-1].width;
			end;
			gombok[i].Left := (i+1) * oszel + db;
		end;
	end;
end;

function	MaradekIdo(kezdes : TdateTime; eddigi, osszes : integer) : TDateTime;
begin
	Result	:= 0;
	if ( ( eddigi <> 0 ) and (now > kezdes) ) then begin
		Result	:= osszes * (now - kezdes) / eddigi;
	end;
end;

{******************************************************************************
Eljárás neve  :  SetHorEqual
Leírás        :  Objektumok egyenletes elrendezése vízszintesen
Paraméterek   :  holder    - a szülő objektum
                 c_items   - az elrendezendő objektumok
                 margin    - a két oldalon hagyandó margó
*******************************************************************************}
procedure  SetHorEqual(holder : TControl; c_items : array of TControl; margin: integer = 0  );
var
   i       : integer;
   wi      : integer;
   gap     : integer;
begin
   // Vízszintesen elrendezi az objektumokat
   wi  := 0;
   for i := Low(c_items) to High(c_items) do begin
       wi := wi + c_items[i].width;
   end;
   gap := 0;
   if holder.Width > (wi + 2*margin) then begin
       gap := ( holder.Width - (wi + 2*margin) ) div (Length(c_items)+1);
   end;
   if gap > 0 then begin
       wi  := margin + gap;
       for i := Low(c_items) to High(c_items) do begin
           c_items[i].Left := wi;
           wi  := wi + gap + c_items[i].Width;
       end;
   end;
end;

{************************************************
* Adóazonosító jel ellenőrzése
* Visszatérési érték:
* -  0: Jó adóazonosító jel
* - -1: Rossz a kapott érték hossza (csak 10 karakter lehet)
* - -2: A kapott érték nem csak számjegyet tartalmaz
* - -3: A kapott érték nem 8-assal kezdődik
* - -4: A kapott értélk CDV hibás
* - -5: Az adóazonosító a mai napnál későbbi dátumot tartalmaz
* Környezet: Delphi 7
************************************************}
function CheckAdoazonosito(cAdo: string): integer;
var i: integer;
    nCDV: integer;
begin
  if length(cAdo)<>10 then
  begin
    Result := -1;
    exit;
  end;

  if not TryStrToInt(cAdo,i) then
  begin
    Result := -2;
    exit;
  end;

  if cAdo[1]<>'8' then
  begin
    Result := -3;
    exit;
  end;

  nCDV := 0;
  for i:=1 to 9 do
  begin
    nCDV := nCDV + i*StrToInt(cAdo[i]);
  end;

  if (nCDV mod 11)<>StrToInt(cAdo[10]) then
  begin
    Result := -4;
    exit;
  end;

  if StrToInt(Copy(cAdo,2,5))>(Trunc(Now)-StrToDate('1867.01.01')) then
  begin
    Result := -5;
    exit;
  end;

  Result := 0;

end;

function GetNowString : string;
begin
    Result := FormatDateTime('YYYY.MM.DD HH:NN:SS', now);
end;

function SendJSEmail (cim, cccim, targy : string; sorok : TStrings) : boolean;
var
   em      : TJEmail;
begin
   em := TJEmail.Create(nil);
   em.SetHostParams('sbs.jsspeed.local', 'fuvarosrendszer', 'Fuvar2014', 25);
   em.SetEmailParams('fuvarosrendszer@jsspeed.hu', cim, cccim, '', targy, sorok);
   em.SetSSLParams( 0 , false );
   Result := em.SendMessage;
   em.Destroy;
end;

{******************************************************************************
Függvény neve : Sorszamolo
Funció        : A megadott állomány sorainak számát adja vissza.
Paraméterek   : fn 	    - az állományneve
Visszaadott é.: integer    - a sorok száma vagy -1 ha hiba van
************************************************************************************ }
function Sorszamolo(fn : string) : integer;
var
   sz  : integer;
   sor : string;
   F   : Textfile;
begin
   Result  := -1;
   if not FileExists(fn) then begin
       Exit;
   end;
   {$I-}
   AssignFile(F, fn);
   Reset(F);
   {$I+}
   if IOResult <> 0 then begin
       Exit;
   end;
   sz  := 0;
   while not Eof(F) do begin
       Readln(F, sor);
       Inc(sz);
   end;
   CloseFile(F);
   Result  := sz;
end;

{******************************************************************************
Függvény neve : JoEgesz
Funció        : A megadott string csak számjegyeket tartalmaz-e
Paraméterek   : Num 	    - a string
Visszaadott é.: boolean    - TRUE ha csak számjegy, FALSE egyébként
************************************************************************************ }
function JoEgesz(Num: String): Boolean;
var
    X : Integer;
begin
  Num:=Trim(Num);
  if Length(Num)=0 then begin
      Result := False;
      end
  else begin
    Result := True;
    for X := 1 to Length(Num) do begin
     if Pos(copy(Num,X,1),'0123456789') = 0 then begin
         Result := False;
         Exit;
         end;  // if
     end;  // for
    end;  // else
end;

function JoSzam(szamstr: string; ures_lehet_e: boolean) : boolean;
var
   sepjo, seprossz, str: string;
   f: double;
begin
	if ures_lehet_e and (szamstr	= '') then begin
      Result	:= True;
 		  Exit;
  	  end;
  str:= szamstr;
  sepjo := SysUtils.FormatSettings.DecimalSeparator;
  if sepjo = '.' then seprossz   := ','
  else seprossz   := '.';
  str:= StringReplace(szamstr, seprossz, sepjo, [rfReplaceAll]);  // Kicseréljük a rossz szeparátorokat jóra
 	try
 		f:= StrToFloat(str);
    Result:= True;  // ha túljutott a konverzión hiba nélkül
 	except
    Result := False;
 	end;
end;

function JoFuvarosDatum(S: string; ures_lehet_e: boolean): boolean;
var
   S1, S2, S3, Sep1, Sep2, Sep3: string;
   Res: boolean;
begin
   if ures_lehet_e and (S = '') then  // Accept empty
      Res:=True
   else begin
     if (Length(S)<>11) then
        Res:=False
     else begin  // a hossza jó
        S1:=copy(S,1,4);
        Sep1:=copy(S,5,1);
        S2:=copy(S,6,2);
        Sep2:=copy(S,8,1);
        S3:=copy(S,9,2);
        Sep3:=copy(S,11,1);
        Res:=JoEgesz(S1) and JoEgesz(S2) and JoEgesz(S3)
           and (Sep1='.') and (Sep2='.') and (Sep3='.');
        end;
     end;  // else
   Result:=Res;
end;


function JoTelenorIdo(const IdoS: string; ures_lehet_e: boolean): boolean;
// 02:28:29
var
   S, S1, S2, S3, Sep1, Sep2, Sep3: string;
   Res: boolean;
begin
   S:= IdoS;
   if ures_lehet_e and (S = '') then  // Accept empty
      Res:=True
   else begin
     if (Length(S)=7) then
        S:= '0'+S;
     if (Length(S)<>8) then
        Res:=False
     else begin  // a hossza jó
        S1:=copy(S,1,2);
        Sep1:=copy(S,3,1);
        S2:=copy(S,4,2);
        Sep2:=copy(S,6,1);
        S3:=copy(S,7,2);
        Res:=JoEgesz(S1) and JoEgesz(S2) and JoEgesz(S3)
           and (StringSzam(S1)<=23) and (StringSzam(S2)<=60) and (StringSzam(S3)<=60)
           and (Sep1=':') and (Sep2=':');
        end;
     end;  // else
   Result:=Res;
end;

function TelenorIdoOsszead(const Ido1S, Ido2S: string): string;
// 02:28:29
var
   S1, S2, Hour1, Hour2, Min1, Min2, Sec1, Sec2: string;
   HH, MM, SS: integer;
   Res: string;
begin
   if not(JoTelenorIdo(Ido1S, False) and JoTelenorIdo(Ido1S, False)) then
      Res:=''
   else begin
     S1:= Ido1S;
     S2:= Ido2S;
     if (Length(S1)=7) then S1:= '0'+S1;
     if (Length(S2)=7) then S2:= '0'+S2;
     // szétszed
     Hour1:=copy(S1,1,2);
     Min1:=copy(S1,4,2);
     Sec1:=copy(S1,7,2);
     Hour2:=copy(S2,1,2);
     Min2:=copy(S2,4,2);
     Sec2:=copy(S2,7,2);
     // összead
     HH:= StrToInt(Hour1);
     MM:= StrToInt(Min1);
     SS:= StrToInt(Sec1)+StrToInt(Sec2);
     if SS>=60 then begin
        SS:= SS-60;
        MM:= MM+1;
        end;
     MM:= MM + StrToInt(Min2);
     if MM>=60 then begin
        MM:= MM-60;
        HH:= HH+1;
        end;
     HH:= HH+StrToInt(Hour2);
     Res:=FormatFloat('0#', HH)+':'+FormatFloat('0#', MM)+':'+FormatFloat('0#', SS);
     end;  // else
   Result:=Res;
end;


function FuvarosDatumToSQLDate(S: string): string;
//  MSSQL: az 'YYYY-MM-DD' nyelvfüggetlen formátum
begin
   if JoFuvarosDatum(S, True) then begin
      if S='' then
          Result:= ''
      else begin
          Result:= copy(S,1,4)+'-'+copy(S,6,2)+'-'+copy(S,9,2); // pl. 2017-02-15
          end;
      end
   else begin
      Raise SysUtils.Exception.Create('Hibás Fuvaros dátum formátum: '+S);
      end;
end;

function FuvarosTimestampToNemet(S: string): string;
//  bemenet: 2018.03.27. 22:45
//  kimenet: 27/03/2018 22:45
var
  D, T: string;
begin
  D:= copy(S,1,11);  // a dátum rész
  if length(S)=17 then
      T:= copy(S,13,5)  // az idő rész
  else T:= '';
  if JoFuvarosDatum(D, True) then begin
      if D='' then
          Result:= ''
      else begin
          Result:= copy(D,9,2)+'/'+copy(D,6,2)+'/'+copy(D,1,4) + ' '+T;
          end;
      end
   else begin
      Raise SysUtils.Exception.Create('Hibás Fuvaros dátum formátum: '+S);
      end;
end;

function IntToStr_default(Num: String; Default_value: integer): integer;
begin
  if JoEgesz(Num) then
     result:=StrToInt(Num)
  else result:=Default_value;
end;

{******************************************************************************
Függvény neve : JoTelefonszam
Funció        : A mindenféle tagolással megadott telefonszámokat szabvány formába (pl. +36202251121) hozza
Paraméterek   : Formazott - a formázott telefonszám
Visszaadott é.: szabvány telefonszám
************************************************************************************ }
function JoTelefonszam(Formazott: String): string;
var
    X, SHossz: Integer;
    S: string;
begin
  // először leveszünk mindent, ami nem '+' vagy számjegy
  S:= '';
  for X := 1 to Length(Formazott) do begin
     if Pos(copy(Formazott,X,1),'+0123456789') > 0 then begin
        S:=S+ copy(Formazott,X,1);
        end;  // if
     end;  // for
  // egy eleve jól formázott számhoz innen már nem nyúl
  SHossz:= Length(S);
  case SHossz of
     8, 9: begin      // pl. a formázott 1/555-2500, 22/512-650 vagy 20/484-9535
        S:= '+36'+S;
        end;
     10, 11: begin
        if (copy(S, 1, 2)='36') then begin     // pl. a formázott (36)(1)431 3534 vagy (36)74-412 979
          S:= '+'+S;
          end;
        if (copy(S, 1, 2)='06') then begin     // pl. a formázott 0696/449-795
          S:= '+36'+copy(S, 3, 999);  // --> +3620 ...
          end;
        end;
     end;  // case
  if (copy(S, 1, 2)='00') then begin     // pl. a formázott (00-36)-57-501-630
      S:= '+'+copy(S, 3, 999);  // --> +3657 ...
      end;
  Result := S;
end;

{******************************************************************************
Függvény neve : MindenfeleStringToszam
Funció        : A mindenféle tagolással megadott számokat float-tá alakítja
************************************************************************************ }
function MindenfeleStringToszam(Formazott: String): double;
var
    X, SHossz: Integer;
    S: string;
begin
  // először leveszünk mindent, ami nem '-', ',', '.' vagy számjegy
  S:= '';
  for X := 1 to Length(Formazott) do begin
     if Pos(copy(Formazott,X,1),'-,.0123456789') > 0 then begin
        S:=S+ copy(Formazott,X,1);
        end;  // if
     end;  // for
  Result := StringSzam(S);
end;


{******************************************************************************
Függvény neve : ListaElemTorol
Funció        : Töröl egy elemet a listából
Paraméterek   : A lista és a törlendő elem
 ************************************************************************************ }
procedure ListaElemTorol(var Lista: TStringList; Elem: string);
var
   i: integer;
begin
  i:= 0;
  while i<=Lista.Count - 1 do begin
    if Lista[i]=Elem then
      Lista.Delete(i)
    else Inc(i);
    end;
end;

function StringEncodeAsUTF8(str: string): string;
var
  TempStr: String;
begin
  str:=StringReplace(str, 'Á', '%C3%81', [rfReplaceAll]);
  str:=StringReplace(str, 'É', '%C3%89', [rfReplaceAll]);
  str:=StringReplace(str, 'Í', '%C3%8D', [rfReplaceAll]);
  str:=StringReplace(str, 'Ó', '%C3%93', [rfReplaceAll]);
  str:=StringReplace(str, 'Ö', '%C3%96', [rfReplaceAll]);
  str:=StringReplace(str, 'Ő', '%C3%90', [rfReplaceAll]);
  str:=StringReplace(str, 'Ú', '%C3%9A', [rfReplaceAll]);
  str:=StringReplace(str, 'Ü', '%C3%9C', [rfReplaceAll]);
  str:=StringReplace(str, 'Ű', '%C5%B0', [rfReplaceAll]);

  str:=StringReplace(str, 'á', '%C3%A1', [rfReplaceAll]);
  str:=StringReplace(str, 'é', '%C3%A9', [rfReplaceAll]);
  str:=StringReplace(str, 'í', '%C3%AD', [rfReplaceAll]);
  str:=StringReplace(str, 'ó', '%C3%B3', [rfReplaceAll]);
  str:=StringReplace(str, 'ö', '%C3%B6', [rfReplaceAll]);
  str:=StringReplace(str, 'ő', '%C5%91', [rfReplaceAll]);
  str:=StringReplace(str, 'ú', '%C3%BA', [rfReplaceAll]);
  str:=StringReplace(str, 'ü', '%C3%BC', [rfReplaceAll]);
  str:=StringReplace(str, 'ű', '%C5%B1', [rfReplaceAll]);

  Result:= str;
end;

function StringDecodeAsUTF8(str: ansistring): string;
var
  TempStr: String;
begin
  // használjuk ezt: UTF8Decode

  {str:=StringReplace(str, chr($C3)+chr($81), 'Á', [rfReplaceAll]);
  str:=StringReplace(str, chr($C3)+chr($89), 'É', [rfReplaceAll]);
  str:=StringReplace(str, chr($C3)+chr($8D), 'Í', [rfReplaceAll]);
  str:=StringReplace(str, chr($C3)+chr($93), 'Ó', [rfReplaceAll]);
  str:=StringReplace(str, chr($C3)+chr($96), 'Ö', [rfReplaceAll]);
  str:=StringReplace(str, chr($C3)+chr($90), 'Ő', [rfReplaceAll]);
  str:=StringReplace(str, chr($C3)+chr($9A), 'Ú', [rfReplaceAll]);
  str:=StringReplace(str, chr($C3)+chr($9C), 'Ü', [rfReplaceAll]);
  str:=StringReplace(str, chr($C3)+chr($B0), 'Ű', [rfReplaceAll]);

  str:=StringReplace(str, chr($C3)+chr($A1), 'á', [rfReplaceAll]);
  str:=StringReplace(str, chr($C3)+chr($A9), 'é', [rfReplaceAll]);
  str:=StringReplace(str, chr($C3)+chr($AD), 'í', [rfReplaceAll]);
  str:=StringReplace(str, chr($C3)+chr($B3), 'ó', [rfReplaceAll]);
  str:=StringReplace(str, chr($C3)+chr($B6), 'ö', [rfReplaceAll]);
  str:=StringReplace(str, chr($C5)+chr($91), 'ő', [rfReplaceAll]);
  str:=StringReplace(str, chr($C3)+chr($BA), 'ú', [rfReplaceAll]);
  str:=StringReplace(str, chr($C3)+chr($BC), 'ü', [rfReplaceAll]);
  str:=StringReplace(str, chr($C5)+chr($B1), 'ű', [rfReplaceAll]);
   }
  Result:= str;
end;

function StringtoHex(Data: ansistring): string;
var
  i, i2: Integer;
  s: string;
begin
  s:= '';
  for i := 1 to Length(Data) do begin
    s := s + IntToHex(Ord(Data[i]), 2) + ' ';
  end;
  Result := s;
end;

function GetElementIndex(const A: array of string; const E: string): integer;
const
  NotFound = -1;
var
  i: integer;
begin
  for i:= Low(A) to High(A) do
    if A[i] = E then
      Exit(i);
  Exit(NotFound);
end;

function FuvarosDatum_to_Kulcs(D: string): string;
// "2015.11.02." -> "2015.11.02"
begin
   Result:= copy(D,1,10);
end;

function KulcsDatum_to_Fuvaros(D: string): string;
// "2015.11.02" -> "2015.11.02."
begin
  if D <> '' then
     Result:= copy(D,1,10)+'.'
  else Result:= '';
end;



// MilliSecondsBetween(now, EncodeDateTime(1970,01,01,0,0,0,0));

function JavaScriptTimestampToFuvaros(JSDate: Int64): string;
// "1463730265797" -> "2015.11.02."   -- az 1970.01.01. óta eltelt ezredmásodpercek száma
const
  NapiEzredmasodpercek = 1000 * 60 * 60 * 24.0;
var
  DT, DLocalDateTime: TDateTime;
  PluszNapok, PluszMasodpercek: Word;
begin
  PluszNapok:= Floor(JSDate / NapiEzredmasodpercek);
  PluszMasodpercek := Floor((JSDate - ( PluszNapok * NapiEzredmasodpercek )) / 1000);
  DT := EncodeDateTime(1970, 1, 1, 0, 0, 0, 0); // 1970.01.01.
  DT := IncDay(DT, PluszNapok);
  DT := IncSecond(DT, PluszMasodpercek);
  DLocalDateTime := TTimeZone.Local.ToLocalTime(DT);
  Result:= FormatDateTime('YYYY.MM.DD. HH:MM:SS', DLocalDateTime);
end;

{function JavaScriptTimestampToFuvaros(JSDate: Int64): string;
// "1463730265797" -> "2015.11.02."   -- az 1970.01.01. óta eltelt ezredmásodpercek száma
const
  NapiEzredmasodpercek = 1000 * 60 * 60 * 24;
var
  DT: TDateTime;
  PluszNapok, PluszMasodpercek: Word;
begin
  PluszNapok:= Floor(JSDate / NapiEzredmasodpercek);
  PluszMasodpercek := Floor((JSDate - ( PluszNapok * NapiEzredmasodpercek )) / 1000);
  DT := EncodeDateTime(1970, 1, 1, 0, 0, 0, 0); // 1970.01.01.
  DT := IncDay(DT, PluszNapok);
  DT := IncSecond(DT, PluszMasodpercek);
  Result:= FormatDateTime('YYYY.MM.DD. HH:MM:SS', DT);
end;}

function LoadStringFromFile(FN, LineSep: string): string;
var
  F: textfile;
  S: string;
begin
 try
  AssignFile(F, FN);
  Reset(F);
  while not eof(F) do begin
    Readln(F, S);
    if Result = '' then
      Result:= S
    else Result:= Result + LineSep+S;
    end;
 finally
  CloseFile(F);
  end;  // try-finally
end;

function LoadBinaryStringFromFile(const FN: string): string;
var
  StringStream : TStringStream;
begin
  if not fileexists(FN) then begin
    result:='';
    end
  else begin
   StringStream:= TStringStream.Create;
    try
     StringStream.LoadFromFile(FN);
     Result:= StringStream.DataString;
    finally
      StringStream.Free;
      end;  // try-finally
    end;  // else
end;

function Felsorolashoz(Mihez, Mit, Szeparator: string; LehetAzonos: boolean): string;
var
  NincsMegBenne: boolean;
begin
   NincsMegBenne:= (Pos(Szeparator+Mit+Szeparator, Szeparator+Mihez+Szeparator) = 0);  // pl. ";a;" benne van a ";a;b;c;"-ben
   if (Trim(Mit)<>'') and (LehetAzonos or NincsMegBenne) then begin
     if Mihez <> '' then
       Result:= Mihez + Szeparator +  Mit
     else Result:= Mit;
     end
   else Result:= Mihez;
end;

function GetKopaszEmail(TeljesEmail: string): string;
// "Mlaka Tamás" <mlaka.tamas@js.hu>   -->  mlaka.tamas@js.hu
var
  S: string;
  P1, P2: integer;
begin
   S:= TeljesEmail;
   P1:= Pos('<', S);
   P2:= Pos('>', S, P1);
   try
     if (P1=0) or (P2=0) then raise EMyWrongFormatException.Create('');  // a bemenet nem a várt formájú
     S:= EgyebDlg.Seprest('<', S);
     S:= EgyebDlg.SepFrontToken('>', S);
     if Pos('@', S)=0 then raise EMyWrongFormatException.Create('');
     Result:= S;
   except
    on E: EMyWrongFormatException do begin
      Result:= TeljesEmail;
      end;  // on MyWrongFormatException
    end;  // try-except
end;

procedure Seplist2TStringList(S: string; Sep: string; Res: TStringList; ClearList: boolean = true);
var
    temp: string;

    function SepFrontToken(Sep, Miben: string): string;
    var  P:integer;
    begin
         P:=Pos(Sep, Miben);
         if P>0 then
            SepFrontToken:=Copy(Miben, 1, P-1)
         else
            SepFrontToken:=Miben;
    end;

    function SepRest(Sep, Miben: string): string;
    var  P:integer;
    begin
         P:=Pos(Sep, Miben);
         if P>0 then
            SepRest:=Copy(Miben, P+Length(Sep), Length(Miben)-Length(Sep)-P+1)
         else
            SepRest:='';
    end;
begin
  if ClearList then
    Res.Clear;
  while S<>'' do begin
      Res.Add(Trim(SepFrontToken(Sep,S)));
      S:=SepRest(Sep,S);
      end; // while
end;

function GetLocalComputerName : string;
    var c1    : dword;
    arrCh : array [0..MAX_PATH] of char;
begin
  c1 := MAX_PATH;
  GetComputerName(arrCh, c1);
  if c1 > 0 then
    result := arrCh
  else
    result := '';
end;

function GetListOp(CL: string; N: integer; Sep: string): string;
var T:string;
    i: integer;

    function SepFrontToken(Sep, Miben: string): string;
    var  P:integer;
    begin
         P:=Pos(Sep, Miben);
         if P>0 then
            SepFrontToken:=Copy(Miben, 1, P-1)
         else
            SepFrontToken:=Miben;
    end;
begin
     i:=0;
     T:=SepFrontToken(Sep, CL);
     while i<N do begin
           CL:=Copy(CL,Length(T)+Length(Sep)+1,Length(CL)-Length(T)-Length(Sep));
           T:=SepFrontToken(Sep, CL);
           i:=i+1;
           end;
     GetListOp:=T;
end;

// Van két olyan TE Connectivity cég, ahol ugyanaz a cégnév és a cím, de eltérő az adószám (ország)
// Őket a Kulcs számla átadás miatt meg kell valahogy különböztetnünk a névben vagy a címben,
// viszont az FAP exportban ugyanazzal a névvel-címmel kell szerepeljen. E célból a névbe szögletes
// zárójelek között extra információt teszünk, ami a Kulcs felé átmegy, az FAP exportba viszont nem kerül bele.

// nem szeretném túlbonyolítani a függvényt: csak egy zárójeles blokkot dolgoz fel, az viszont lehet középen is.
function Nevbol_extrainfo_levesz(N: string): string;
const
  ElejeJel = '[';
  VegeJel = ']';
var
  S, Res: string;
  P1, P2: integer;
  VegenVan: boolean;
begin
  P1:= Pos(ElejeJel, N);
  P2:= Pos(VegeJel, N);
  if P2=Length(N) then VegenVan:= True
  else VegenVan:= False;
  if (P1>0) and (P2>P1) then begin
    Res:= copy(N, 1, P1-1) + copy(N, P2+1, 9999999);
    if VegenVan then Res:=Trim(Res); // ha záró blokk-ként használjuk, elválaszthatjuk szóközzel
    Result:= Res;
    end
  else begin   // ha nincs benne értelmezhető blokk
    Result:= N;
    end;  // else
end;

function StringListToSepString(SL: TStringList; Sep: string; LehetAzonos: boolean): string;
 // Delimiter, DelimitedText megoldást nem tudtam QuoteChar nélkül működtetni
var
    i: integer;
    S: string;
begin
  S:='';
  for i:=0 to SL.Count-1 do begin
    S:=Felsorolashoz(S, SL[i], Sep, LehetAzonos);
    end;  // for
  Result:=S;
end;

function FormatMegrendeloKod(Datum, Terulet: string; Sorszam: integer): string;
begin
  Result:= 'PO'+copy(Datum,3,2)+copy(Datum,6,2)+copy(Datum,9,2)+'_'+Terulet+'_'+FormatFloat('000', Sorszam);
end;

// Clipboard --> TMetafile --> TMemoryStream
function ClipboardMetafileToMemoryStream( Logfn: string): TMemoryStream;
var
  MyMetafile: TMetaFile;
  Stream: TMemoryStream;
begin
  KozosWriteLogFile(Logfn, 'NapiJelentes MetafileToMS 01 '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
  Clipboard.Open;
  KozosWriteLogFile(Logfn, 'NapiJelentes MetafileToMS 02 '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
  if Clipboard.HasFormat(CF_METAFILEPICT) then begin   // If something is in the clipboard in the correct format
      MyMetafile:= TMetaFile.Create;
      Stream := TMemoryStream.Create;
      try
         KozosWriteLogFile(Logfn, 'NapiJelentes MetafileToMS 03 '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
         MyMetafile.LoadFromClipboardFormat(CF_METAFILEPICT, ClipBoard.GetAsHandle(CF_METAFILEPICT), 0);
         KozosWriteLogFile(Logfn, 'NapiJelentes MetafileToMS 04 '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
         MyMetafile.SaveToStream(Stream);
         Stream.Position:= 0;
         KozosWriteLogFile(Logfn, 'NapiJelentes MetafileToMS 05 '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
      finally
         if MyMetafile <> nil then MyMetafile.Free;
         Clipboard.Close;
         end;  // try-finally
      end;  // if
  Result:= Stream;
end;

{
// procedure ClipboardMetafileToMemoryStream(var Stream: TMemoryStream; Logfn: string);
function ClipboardMetafileToMemoryStream( Logfn: string): TMemoryStream;
var
  MemHandle: THandle;
  MemBlock: Pointer;
  ASize, Len, i: Integer;
  TempStr: String;
  Stream: TMemoryStream;
begin
  KozosWriteLogFile(Logfn, 'NapiJelentes MetafileToMS 01 '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
  Clipboard.Open;
  KozosWriteLogFile(Logfn, 'NapiJelentes MetafileToMS 02 '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
  Stream := TMemoryStream.Create;
  try
    if Clipboard.HasFormat(CF_METAFILEPICT) then begin   // If something is in the clipboard in the correct format
      KozosWriteLogFile(Logfn, 'NapiJelentes MetafileToMS 03 '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
      MemHandle := Clipboard.GetAsHandle(CF_METAFILEPICT);
      KozosWriteLogFile(Logfn, 'NapiJelentes MetafileToMS 04 '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
      if MemHandle <> 0 then begin
        ASize := GlobalSize(MemHandle);    // Detect size (number of bytes)
        KozosWriteLogFile(Logfn, 'NapiJelentes MetafileToMS 05 ASize='+IntToStr(ASize)+' '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
        MemBlock := GlobalLock(MemHandle);  // Lock the contents of the clipboard
        if MemBlock = nil then
            KozosWriteLogFile(Logfn, 'NapiJelentes: MemBlock is NIL!!! '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
        KozosWriteLogFile(Logfn, 'NapiJelentes MetafileToMS 06 '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
        try
          Stream.Write(MemBlock^, ASize);  // Copy the data into the stream.
          KozosWriteLogFile(Logfn, 'NapiJelentes MetafileToMS 07 '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
        finally
          GlobalUnlock(MemHandle);
          KozosWriteLogFile(Logfn, 'NapiJelentes MetafileToMS 08 '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
          end;  // try-finally
        end; // if MemHandle
      end;  // if Clipboard
  finally
    Clipboard.Close;
    end;  // try-finally
  Result:= Stream;
end;
}

function KozosWriteLogFile(FName, Text: string): Boolean;
var
  LogFile: TextFile;
begin
  // prepares log file
  AssignFile (LogFile, Fname);
  if FileExists (FName) then
    Append (LogFile) // open existing file
  else
    Rewrite (LogFile); // create a new one

  // write to the file and show error
  Writeln (LogFile,Text);

  // close the file
  CloseFile (LogFile);
  Result:=True;
end;

function MemoryStreamToString(M: TMemoryStream): string;
begin
  SetString(Result, PChar(M.Memory), M.Size div SizeOf(Char));
end;

function Uppercase_HUN(S: string): string;
var
  str: string;
begin
  str:= S;
  str:=StringReplace(str, 'é', 'É', [rfReplaceAll]);
  str:=StringReplace(str, 'á', 'Á', [rfReplaceAll]);
  str:=StringReplace(str, 'ö', 'Ö', [rfReplaceAll]);
  str:=StringReplace(str, 'ü', 'Ü', [rfReplaceAll]);
  str:=StringReplace(str, 'ő', 'Ő', [rfReplaceAll]);
  str:=StringReplace(str, 'ó', 'Ó', [rfReplaceAll]);
  str:=StringReplace(str, 'ű', 'Ű', [rfReplaceAll]);
  str:=StringReplace(str, 'í', 'Í', [rfReplaceAll]);
  str:=StringReplace(str, 'ú', 'Ú', [rfReplaceAll]);

{  str:=StringReplace(str, Chr(186), 'É', [rfReplaceAll]);
  str:=StringReplace(str, chr(222), 'Á', [rfReplaceAll]);
  str:=StringReplace(str, chr(192), 'Ö', [rfReplaceAll]);
  str:=StringReplace(str, chr(191), 'Ü', [rfReplaceAll]);
  str:=StringReplace(str, chr(219), 'Ő', [rfReplaceAll]);
  str:=StringReplace(str, chr(187), 'Ó', [rfReplaceAll]);
  str:=StringReplace(str, chr(220), 'Ű', [rfReplaceAll]);
  str:=StringReplace(str, chr(226), 'Í', [rfReplaceAll]);
  str:=StringReplace(str, chr(221), 'Ú', [rfReplaceAll]); }
  Result:= UpperCase(str);
end;

function MapNetworkDrive(const RemoteName, LocalDrive, UserName, Password: string): Boolean;
var
  NetRes: TNetResource;
  Res: DWord;
begin
  Result := True;
  FillChar(NetRes, SizeOf(TNetResource), 0);
  NetRes.dwType := RESOURCETYPE_DISK;
  NetRes.lpRemoteName := PChar(RemoteName);
  NetRes.lpLocalName := PChar(LocalDrive);
  Res := WNetAddConnection2(NetRes, PChar(Password), PChar(UserName), 0);
  if (Res = NO_ERROR) or (Res = 1219) then    // 1219: ERROR_SESSION_CREDENTIAL_CONFLICT = Multiple connections to a server or shared resource by the same user, using more than one user name, are not allowed."
    Result := True
  else Result := False;
  if not Result then
    SysErrorMessage(Res);
end;

function UnmapNetworkDrive(const LocalDrive: string): Boolean;
var
  Res: DWord;
begin
  Res := WNetCancelConnection2(PChar(LocalDrive), 0, True);
  Result := (Res = NO_ERROR);
end;

function NetUseAddWithoutDriveMapping(RemoteName, UserName, Password: string): boolean;
var
  netResource: TNetResource;
  dwResult, dwBufSize, dwFlags: DWORD;
  Res: DWORD;
begin
  dwFlags := CONNECT_REDIRECT;
  ZeroMemory(@netResource, sizeof(TNetResource));
  with netResource do begin
    dwType := RESOURCETYPE_DISK;
    lpLocalName := nil;
    lpRemoteName := PChar(RemoteName);
    lpProvider := nil;
  end;
  Res := WNetAddConnection2(netResource, PChar(Password), PChar(UserName), 0);
  netResource.lpRemoteName:= nil;
  if (Res = NO_ERROR) or (Res = 1219) then    // 1219: ERROR_SESSION_CREDENTIAL_CONFLICT = Multiple connections to a server or shared resource by the same user, using more than one user name, are not allowed."
    Result := True
  else Result := False;
end;

{function NetUseAdd(const LocalName, RemoteName, UserName, Password: string; var AccessName: string): DWord;
var
  netResource: TNetResource;
  dwResult, dwBufSize, dwFlags: DWORD;
  hRes: DWORD;
  buf: array[0..1024] of Char;
begin
  dwFlags := CONNECT_REDIRECT;
  ZeroMemory(@netResource, sizeof(TNetResource));
  with netResource do begin
    dwType := RESOURCETYPE_DISK;
    lpLocalName := PChar(LocalName);
    lpRemoteName := PChar(RemoteName);
    lpProvider := nil;
  end;
  dwBufSize := Sizeof(buf);
  result := WNetUseConnection(0, netResource, PChar(UserName), PChar(Password), dwFlags, buf, dwBufSize, dwResult);
  if hRes = NO_ERROR then
    AccessName := buf;
end;
}

function TestBit(I, B: integer): boolean;
var t: integer;
begin
  T:=(I and Floor(IntPower(2,B)));
  TestBit:=(T>0);
end;

// hányszor fordul elő "KeresettChar" a stringben
function CharSzamol(const ContentString: string; const KeresettChar: char): integer;
var
  C: Char;
begin
  result := 0;
  for C in ContentString do
    if C = KeresettChar then
      Inc(result);
end;

procedure EKAERFigyelmeztetes(const UjRendszam, MBKOD: string);
var
  S: string;
begin
  if EgyebDlg.EKAER_FIGYELMEZTETES_ENABLED then begin
    S:= 'Új rendszám ('+UjRendszam+'), kérlek győződj meg az EKAER-ben történő módosításról!';
    NoticeKi(S);
    Query_Log_Str('Megjelenítve (MBKOD='+MBKOD+'): '+S, 0, true);
    end;
end;

function ListabanBenne(const Elem, Lista, Sep: string): boolean;
begin
  Result:= (Pos(Sep+Elem+Sep, Sep+Lista+Sep) > 0 );  // pl. ",1,2,3,4," tartalmazza ",2," -t
end;

function FilesAreEqual_Info(const File1, File2: TFileName): string;
const
  // BlockSize = 65536;
  BlockSize = 1;
var
  fs1, fs2: TFileStream;
  L1, L2, Holtart: Integer;
  B1, B2: array[1..BlockSize] of Byte;
  Res: string;
  DiffFound: boolean;
begin
  Res := '';
  DiffFound:= False;
  Holtart:= 0;
  fs1 := TFileStream.Create(File1, fmOpenRead or fmShareDenyWrite);
  Res := '1';
  try
    fs2 := TFileStream.Create(File2, fmOpenRead or fmShareDenyWrite);
    Res := '2';
    try
      if fs1.Size = fs2.Size then begin
        Res := '3';
        while (fs1.Position < fs1.Size) and not DiffFound do begin
          Res := '4';
          L1 := fs1.Read(B1[1], BlockSize);
          L2 := fs2.Read(B2[1], BlockSize);
          Holtart:=Holtart + L1;
          Res := '5';
          if L1 <> L2 then begin
            Res := 'Eltérő blokkméret';
            DiffFound:= True;
            end
          else begin
            if not CompareMem(@B1[1], @B2[1], L1) then begin
              Res := 'Eltérő blokk tartalom ('+IntToStr(Holtart)+')';
              DiffFound:= True;
              end;  // if
            end;  // else
          end; // while
        if not DiffFound then Res := 'OK';
        end;
    finally
      fs2.Free;
    end;
  finally
    fs1.Free;
  end;
  if Res='OK' then Res:='';
  Result := Res;
end;

function FilesAreEqual(const File1, File2: TFileName): Boolean;
const
  BlockSize = 65536;
var
  fs1, fs2: TFileStream;
  L1, L2: Integer;
  B1, B2: array[1..BlockSize] of Byte;
begin
  Result := False;
  fs1 := TFileStream.Create(File1, fmOpenRead or fmShareDenyWrite);
  try
    fs2 := TFileStream.Create(File2, fmOpenRead or fmShareDenyWrite);
    try
      if fs1.Size = fs2.Size then
      begin
        while fs1.Position < fs1.Size do
        begin
          L1 := fs1.Read(B1[1], BlockSize);
          L2 := fs2.Read(B2[1], BlockSize);
          if L1 <> L2 then
          begin
            Exit;
          end;
          if not CompareMem(@B1[1], @B2[1], L1) then Exit;
        end;
        Result := True;
      end;
    finally
      fs2.Free;
    end;
  finally
    fs1.Free;
  end;
end;

function TruckpitUzenetKuldes(const ADOKOD, ADOKOD2, AUzenet, AMBKOD: string): boolean;
var
  DONEV1, DONEV2, ArchS, S: string;
begin
	if (ADOKOD = '') then begin
		Exit;
	end;
  S:= 'select DO_NAME, DO_ARHIV from DOLGOZO where DO_KOD='''+ADOKOD+'''';
  Query_run(EgyebDlg.QueryAl3, S);
  DONEV1:= EgyebDlg.QueryAl3.FieldByName('DO_NAME').AsString;
  if (EgyebDlg.QueryAl3.FieldByName('DO_ARHIV').AsString <> '0') then begin
    ArchS:= Felsorolashoz(ArchS, DONEV1, ', ', False);
    end;  // if
  S:= 'select DO_NAME, DO_ARHIV from DOLGOZO where DO_KOD='''+ADOKOD2+'''';
  Query_run(EgyebDlg.QueryAl3, S);
  DONEV2:= EgyebDlg.QueryAl3.FieldByName('DO_NAME').AsString;
  if (EgyebDlg.QueryAl3.FieldByName('DO_ARHIV').AsString <> '0') then begin
    ArchS:= Felsorolashoz(ArchS, DONEV2, ', ', False);
    end;  // if
  if ArchS <> '' then begin
      NoticeKi('Archív dolgozónak nem küldhető üzenet! ('+ArchS+')');
      Result:= False;
      exit;
      end;
  UzenetTopDlg.UzenetElokeszit(ADOKOD, ADOKOD2, DONEV1, DONEV2, AUzenet, AMBKOD);
  // UzenetTopDlg.Show;
end;

function Tartalmaz(const Mi, Mit: string): boolean;
begin
   Result:= (Pos(Mit, Mi) > 0);

end;

end.
