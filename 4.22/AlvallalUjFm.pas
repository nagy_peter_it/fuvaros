unit AlvallalUjfm;

interface

uses
	SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
	StdCtrls, Forms, DBCtrls, DB, DBGrids, DBTables, Grids, ExtCtrls, Buttons,
	ShellApi, Dialogs, J_FOFORMUJ, ADODB, Mask, Menus, Variants;

type
	TAlvallalUjFmDlg = class(TJ_FOFORMUJDLG)
	 procedure FormCreate(Sender: TObject);override;
	 procedure Adatlap(cim, kod : string);override;
    procedure ButtonKuldClick(Sender: TObject);override;
	 procedure ButtonTorClick(Sender: TObject);override;
	 private
	 public
	end;

var
	AlvallalUjFmDlg : TAlvallalUjFmDlg;

implementation

uses
	Egyeb, J_SQL, Alvallalbe, Kozos;

{$R *.DFM}

procedure TAlvallalUjFmDlg.FormCreate(Sender: TObject);
begin
	Inherited FormCreate(Sender);
	AddSzuromezoRovid( 'TT_TIPUS', 'TETANAR' );
	alapstr		:= 'Select * from ALVALLAL ';
	FelTolto('Új alvállalkozó felvitele ', 'Alvállalkozói adatok módosítása ',
			'Alvállalkozók listája', 'V_KOD', alapstr,'ALVALLAL', QUERY_KOZOS_TAG );
	kulcsmezo			:= 'V_KOD';
	nevmezo				:= 'V_NEV';
	width				:= DLG_WIDTH;
	height				:= DLG_HEIGHT;
end;

procedure TAlvallalUjFmDlg.ButtonKuldClick(Sender: TObject);
begin
	Inherited ButtonKuldClick(Sender);
   ret_vnev	:= Query1.FieldByName('V_NEV').AsString;
end;

procedure TAlvallalUjFmDlg.Adatlap(cim, kod : string);
begin
	Application.CreateForm(TAlvallalbeDlg, AlForm);
	Inherited Adatlap(cim, kod );
end;

procedure TAlvallalUjFmDlg.ButtonTorClick(Sender: TObject);
var
	vekod	: string;
begin
	if not voltalt then begin
		voltalt 	:= true;
		Exit;
	end;
	if not EgyebDlg.user_super then begin
		NoticeKi('Csak Rendszergazdai jogosultsággal lehet törölni!');
    exit;
  end;
	vekod	:= Query1.FieldByName(FOMEZO).AsString;
	if not EgyebDlg.RekordTorles(Query1, alapstr, GetOrderBy(false) ,Tag,
		'DELETE from '+FOTABLA+' where '+FOMEZO+'='''+Query1.FieldByName(FOMEZO).AsString+''' ') then begin
		Close;
	end;
	Query_Run(Query2, 'DELETE FROM ALGEPKOCSI WHERE AG_AVKOD = '''+vekod+''' ');
	Label3.Caption	:= '('+IntToStr(Query1.RecordCount)+')';
	Label3.Update;
end;

end.


