unit _proba_szamitas;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, KozosTipusok, GeoRoutines, Vcl.StdCtrls;

type
  TProba_szamitas = class(TForm)
    Edit1: TEdit;
    Edit2: TEdit;
    Edit3: TEdit;
    Edit4: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Edit5: TEdit;
    Button1: TButton;
    Label5: TLabel;
    Memo1: TMemo;
    Button2: TButton;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Proba_szamitas: TProba_szamitas;

implementation

{$R *.dfm}

uses GoogleGeoCode;

procedure TProba_szamitas.Button1Click(Sender: TObject);
var
  Point1, Point2: TGeoCoordinates;
  Dist: double;
begin
  Point1.Latitude:= StrToFloat(Edit1.Text);
  Point1.Longitude:= StrToFloat(Edit2.Text);
  Point2.Latitude:= StrToFloat(Edit3.Text);
  Point2.Longitude:= StrToFloat(Edit4.Text);
  Dist:= GeoCoordinatesDistance(Point1, Point2);
  Edit5.Text:= FormatFloat('0.000', Dist);
end;

procedure TProba_szamitas.Button2Click(Sender: TObject);
var
  GoogleGeoCode: TGoogleGeoCode;
begin
  GoogleGeoCode:= TGoogleGeoCode.Create(nil);
  GoogleGeoCode.ProcessJSON(Memo1.Text, True, 'TESZT');

end;

end.
