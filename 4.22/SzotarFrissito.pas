unit SzotarFrissito;

interface

uses
	SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
	Forms, Dialogs, StdCtrls, Buttons, DB, DBTables, ADODB;

type
	TSzotarFrissitoDlg = class(TForm)
	BitBtn1: TBitBtn;
	BitBtn2: TBitBtn;
	Label2: TLabel;
    Query1: TADOQuery;
    Label1: TLabel;
    Query2: TADOQuery;
	procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
	private
	end;

var
	SzotarFrissitoDlg: TSzotarFrissitoDlg;

implementation

uses
	Egyeb, J_SQL, Kozos;

{$R *.DFM}

procedure TSzotarFrissitoDlg.BitBtn2Click(Sender: TObject);
begin
	Close;
end;

procedure TSzotarFrissitoDlg.BitBtn1Click(Sender: TObject);
var
	sqlstr	: string;
begin
	Screen.Cursor	:= crHourGlass;
	Label2.Caption	:= 'A felrak�sok friss�t�se';
	Label2.Update;
	sqlstr	:= 'UPDATE MEGSEGED SET MS_TINEV = ( SELECT SZ_MENEV FROM '+GetTableFullName(EgyebDlg.v_koz_db, 'SZOTAR')+' WHERE SZ_FOKOD = ''999'' AND SZ_ALKOD = ''712'' ) WHERE MS_TIPUS = 0';
	Query_Run(Query1, sqlstr);
	Label2.Caption	:= 'A lerak�sok friss�t�se';
	Label2.Update;
	sqlstr	:= 'UPDATE MEGSEGED SET MS_TINEV = ( SELECT SZ_MENEV FROM '+GetTableFullName(EgyebDlg.v_koz_db, 'SZOTAR')+' WHERE SZ_FOKOD = ''999'' AND SZ_ALKOD = ''713'' ) WHERE MS_TIPUS = 1';
	Query_Run(Query1, sqlstr);
	Label2.Caption	:= 'A felrak�si st�tuszok friss�t�se';
	Label2.Update;
	sqlstr	:= 'UPDATE MEGSEGED SET MS_STATN = ( SELECT SZ_MENEV FROM '+GetTableFullName(EgyebDlg.v_koz_db, 'SZOTAR')+' WHERE SZ_FOKOD = ''960'' AND SZ_ALKOD = MEGSEGED.MS_STATK ) ';
	Query_Run(Query1, sqlstr);
	// A belf�ld/k�lf�ld/... be�r�sa a MEGSEGED.MS_EXSTR -be
	Label2.Caption	:= 'A belf�ld/k�lf�ld/... oszlop friss�t�se';
	Label2.Update;
	sqlstr	:= 'UPDATE MEGSEGED SET MS_EXSTR = '''+GetExportStr(3)+''' WHERE MS_FORSZ =  ''HU'' AND MS_ORSZA =  ''HU'' ';
	Query_Run(Query1, sqlstr);
	sqlstr	:= 'UPDATE MEGSEGED SET MS_EXSTR = '''+GetExportStr(1)+''' WHERE MS_FORSZ =  ''HU'' AND MS_ORSZA <> ''HU'' ';
	Query_Run(Query1, sqlstr);
	sqlstr	:= 'UPDATE MEGSEGED SET MS_EXSTR = '''+GetExportStr(0)+''' WHERE MS_FORSZ <> ''HU'' AND MS_ORSZA =  ''HU'' ';
	Query_Run(Query1, sqlstr);
	sqlstr	:= 'UPDATE MEGSEGED SET MS_EXSTR = '''+GetExportStr(2)+''' WHERE MS_FORSZ <> ''HU'' AND MS_ORSZA <> ''HU'' ';
	Query_Run(Query1, sqlstr);
	sqlstr	:= 'UPDATE MEGSEGED SET MS_EXPOR = 3 WHERE MS_FORSZ =  ''HU'' AND MS_ORSZA =  ''HU'' ';
	Query_Run(Query1, sqlstr);
	sqlstr	:= 'UPDATE MEGSEGED SET MS_EXPOR = 1 WHERE MS_FORSZ =  ''HU'' AND MS_ORSZA <> ''HU'' ';
	Query_Run(Query1, sqlstr);
	sqlstr	:= 'UPDATE MEGSEGED SET MS_EXPOR = 0 WHERE MS_FORSZ <> ''HU'' AND MS_ORSZA =  ''HU'' ';
	Query_Run(Query1, sqlstr);
	sqlstr	:= 'UPDATE MEGSEGED SET MS_EXPOR = 2 WHERE MS_FORSZ <> ''HU'' AND MS_ORSZA <> ''HU'' ';
	Query_Run(Query1, sqlstr);
	Screen.Cursor	:= crDefault;
	Close;
end;

procedure TSzotarFrissitoDlg.FormCreate(Sender: TObject);
begin
	EgyebDlg.SetADOQueryDatabase(Query1);
	EgyebDlg.SetADOQueryDatabase(Query2);
	Label2.Caption	:= '';
	Label2.Update;
end;

end.
