unit SzamlaNezo;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ADODB;

type
  TSzamlaNezoDlg = class(TForm)
	 QE1: TADOQuery;
	 QE2: TADOQuery;
	 procedure FormCreate(Sender: TObject);
	 function SzamlaNyomtatas(ujkod, nyflag : integer) : boolean;
	 function OsszesitettNyomtatas(ujkod, nyflag : integer) : boolean;
	 function SzamlaSorozat(ujkod : integer) : boolean;
	 function OsszesitettSorozat(ujkod : integer) : boolean;
	 procedure SzamlaKezelo(vujkod, euros, tipus : integer; FN: string = '');
	 procedure OsszesitettSzamlaKezelo(vujkod, tipus : integer; FN: string = '');
   function SzamlaExport(const szamlakod: string; FileNev: string) : boolean;
  private
	 { Private declarations }
  public
	 { Public declarations }
  end;

var
  SzamlaNezoDlg: TSzamlaNezoDlg;

implementation

uses 	Egyeb, Szamla_JS_HUF, J_SQL, KOZOS, Szamla_DK_HUF, Szamla_DK_EUR,
		OSzamla_JS, Kozos_Local, OSZamla_New, Szamla_New, SzamlaDK_NEW,
       OSZamlaDK_New, OSzamla_LE, Kozos_Export;

{$R *.dfm}

procedure TSzamlaNezoDlg.FormCreate(Sender: TObject);
begin
	EgyebDlg.SeTADOQueryDatabase(QE1);
	EgyebDlg.SeTADOQueryDatabase(QE2);

  	Query_Run(EgyebDlg.QueryKozos, 'SELECT * FROM SZOTAR WHERE SZ_FOKOD = ''210'' AND SZ_ALKOD = ''HUN51'' ', FALSE);
	if EgyebDlg.QueryKozos.RecordCount < 1 then begin
       Query_Run(EgyebDlg.QueryKozos, 'INSERT INTO SZOTAR ( SZ_ALKOD, SZ_EGYEB1, SZ_ERVNY, SZ_FOKOD, SZ_KODHO, SZ_LEIRO, SZ_MENEV, SZ_NEVHO ) VALUES ( ''ENG51'', 0, 1, ''210'', 0, ''Eredeti -> ENG'', ''Original'', 0)');
       Query_Run(EgyebDlg.QueryKozos, 'INSERT INTO SZOTAR ( SZ_ALKOD, SZ_EGYEB1, SZ_ERVNY, SZ_FOKOD, SZ_KODHO, SZ_LEIRO, SZ_MENEV, SZ_NEVHO ) VALUES ( ''GER51'', 0, 1, ''210'', 0, ''Eredeti -> GER'', ''Original'', 0)');
       Query_Run(EgyebDlg.QueryKozos, 'INSERT INTO SZOTAR ( SZ_ALKOD, SZ_EGYEB1, SZ_ERVNY, SZ_FOKOD, SZ_KODHO, SZ_LEIRO, SZ_MENEV, SZ_NEVHO ) VALUES ( ''ENG52'', 0, 1, ''210'', 0, ''M�solat -> ENG'', ''Copy'', 0)');
       Query_Run(EgyebDlg.QueryKozos, 'INSERT INTO SZOTAR ( SZ_ALKOD, SZ_EGYEB1, SZ_ERVNY, SZ_FOKOD, SZ_KODHO, SZ_LEIRO, SZ_MENEV, SZ_NEVHO ) VALUES ( ''GER52'', 0, 1, ''210'', 0, ''M�solat -> GER'', ''Kopie'', 0)');
       Query_Run(EgyebDlg.QueryKozos, 'INSERT INTO SZOTAR ( SZ_ALKOD, SZ_EGYEB1, SZ_ERVNY, SZ_FOKOD, SZ_KODHO, SZ_LEIRO, SZ_MENEV, SZ_NEVHO ) VALUES ( ''HUN52'', 0, 1, ''210'', 0, ''M�solat -> HUN'', ''M�solat'', 0)');
       Query_Run(EgyebDlg.QueryKozos, 'INSERT INTO SZOTAR ( SZ_ALKOD, SZ_EGYEB1, SZ_ERVNY, SZ_FOKOD, SZ_KODHO, SZ_LEIRO, SZ_MENEV, SZ_NEVHO ) VALUES ( ''HUN51'', 0, 1, ''210'', 0, ''Eredeti -> HUN'', ''Eredeti'', 0)');
       // A b�zissz�t�r adatainak �jraolvas�sa
		EgyebDlg.Fill_SZGrid;
   end;

end;


function TSzamlaNezoDlg.SzamlaNyomtatas(ujkod, nyflag : integer) : boolean;
var
	reginyomtat	: boolean;
	pld			: integer;
begin
	Result	:= false;
	// Sima sz�mla nyomtat�sa
	if not Query_Run(QE1, 'SELECT SA_PELDANY, SA_UJKOD, SA_SAEUR FROM SZFEJ WHERE SA_UJKOD = '+IntToStr(ujkod),true) then begin
		Exit;
	end;
	if QE1.RecordCount < 1 then begin
		Exit;
	end;
	pld:= StrToIntDef(QE1.FieldByName('SA_PELDANY').AsString,0);
	reginyomtat				:= EgyebDlg.Nyomtathato;
	EgyebDlg.Nyomtathato	:= true;
	EgyebDlg.VoltNyomtatas	:= false;
	EgyebDlg.Szamlanyom		:= true;
	if nyflag < 1 then begin
		EgyebDlg.Nyomtathato	:= false;
	end;
	SzamlaKezelo(ujkod, StrToIntDef(QE1.FieldByName('SA_SAEUR').AsString, 0), 0);
	if EgyebDlg.VoltNyomtatas then begin
		// A p�ld�nysz�m n�vel�se
		Query_Run(QE1, 'UPDATE SZFEJ SET SA_PELDANY = '+IntToStr(pld+1)+' WHERE SA_UJKOD = '+IntToStr(ujkod));
	end;
	EgyebDlg.Szamlanyom		:= false;
	EgyebDlg.Nyomtathato	:= reginyomtat;
	Result					:= true;
end;

function TSzamlaNezoDlg.SzamlaExport(const szamlakod: string; FileNev: string) : boolean;
var
 	reginyomtat, gyujtoszamla	: boolean;
  ujkod: integer;
begin
	Result:= false;
  reginyomtat				:= EgyebDlg.Nyomtathato;
  gyujtoszamla:= true;
	Query_Run(QE1, 'SELECT SA_UJKOD, SA_SAEUR FROM SZFEJ2 WHERE SA_KOD = '+szamlakod,true);
	if QE1.RecordCount = 0 then begin  // nem gy�jt�sz�ml�r�l van sz� --> SZFEJ lek�rdez�s
    Query_Run(QE1, 'SELECT SA_UJKOD, SA_SAEUR FROM SZFEJ WHERE SA_KOD = '+szamlakod,true);
    gyujtoszamla:= false;
	  end;  // if
	if QE1.RecordCount < 1 then begin
		Exit;  // nem tal�ltunk ilyen  sz�ml�t
	  end;
	EgyebDlg.Nyomtathato	:= true;
	EgyebDlg.VoltNyomtatas	:= false;
	EgyebDlg.Szamlanyom		:= true;
  if gyujtoszamla then
    OsszesitettSzamlaKezelo(QE1.FieldByName('SA_UJKOD').AsInteger, 2, FileNev)
  else
  	SzamlaKezelo(QE1.FieldByName('SA_UJKOD').AsInteger, StrToIntDef(QE1.FieldByName('SA_SAEUR').AsString, 0), 2, FileNev);
	EgyebDlg.Szamlanyom		:= false;
	EgyebDlg.Nyomtathato	:= reginyomtat;
	Result:= true;
end;

function TSzamlaNezoDlg.SzamlaSorozat(ujkod : integer) : boolean;
var
	pld, pluszpld			: integer;
  vekod: string;
begin
	Result	:= false;
	// Sima sz�mla nyomtat�sa
	if not Query_Run(QE1, 'SELECT SA_PELDANY, SA_UJKOD, SA_SAEUR FROM SZFEJ WHERE SA_UJKOD = '+IntToStr(ujkod),true) then begin
		Exit;
	end;
	if QE1.RecordCount < 1 then begin
		Exit;
	end;
  vekod:=Query_Select('SZFEJ','SA_UJKOD',IntToStr(ujkod),'SA_VEVOKOD');
  pluszpld:=StrToIntDef(Query_Select('VEVO','V_KOD',vekod,'VE_SZPLD'),0);

	pld	:= StrToIntDef(QE1.FieldByName('SA_PELDANY').AsString,0);
	if pld >= EgyebDlg.Peldanyszam + pluszpld then begin
		Exit;
	end;
	while pld < EgyebDlg.Peldanyszam + pluszpld do begin
		SzamlaKezelo(ujkod, StrToIntDef(QE1.FieldByName('SA_SAEUR').AsString, 0), 1);
		// A p�ld�nysz�m n�vel�se
		Query_Run(QE2, 'UPDATE SZFEJ SET SA_PELDANY = '+IntToStr(pld+1)+' WHERE SA_UJKOD = '+IntToStr(ujkod));
		Inc(pld);
	end;
	Result 	:= true;
end;

function TSzamlaNezoDlg.OsszesitettNyomtatas(ujkod, nyflag : integer) : boolean;
var
	reginyomtat	: boolean;
	pld			: integer;
begin
	Result	:= false;
	// �sszes�tett sz�mla nyomtat�sa
	if not Query_Run(QE1, 'SELECT SA_PELDANY, SA_UJKOD FROM SZFEJ2 WHERE SA_UJKOD = '+IntToStr(ujkod),true) then begin
		Exit;
	end;
	if QE1.RecordCount < 1 then begin
		Exit;
	end;
	pld						:= StrToIntDef(QE1.FieldByName('SA_PELDANY').AsString,0);
	reginyomtat				:= EgyebDlg.Nyomtathato;
	EgyebDlg.Nyomtathato	:= true;
	EgyebDlg.VoltNyomtatas	:= false;
	EgyebDlg.Szamlanyom		:= true;
	if nyflag < 1 then begin
		EgyebDlg.Nyomtathato	:= false;
	end;
	OsszesitettSzamlaKezelo(ujkod, 0);
	if EgyebDlg.VoltNyomtatas then begin
		// A p�ld�nysz�m n�vel�se
		Query_Run(QE1, 'UPDATE SZFEJ2 SET SA_PELDANY = '+IntToStr(pld+1)+' WHERE SA_UJKOD = '+IntToStr(ujkod));
	end;
	EgyebDlg.Szamlanyom		:= false;
	EgyebDlg.Nyomtathato	:= reginyomtat;
	Result					:= true;
end;

function TSzamlaNezoDlg.OsszesitettSorozat(ujkod : integer) : boolean;
var
	pld			: integer;
begin
	Result	:= false;
	// Sima sz�mla nyomtat�sa
	if not Query_Run(QE1, 'SELECT SA_PELDANY, SA_UJKOD FROM SZFEJ2 WHERE SA_UJKOD = '+IntToStr(ujkod),true) then begin
		Exit;
	end;
	if QE1.RecordCount < 1 then begin
		Exit;
	end;
	pld	:= StrToIntDef(QE1.FieldByName('SA_PELDANY').AsString,0);
	if pld >= EgyebDlg.Peldanyszam then begin
		Exit;
	end;
	EgyebDlg.Szamlanyom		:= true;
	while pld < EgyebDlg.Peldanyszam do begin
//		OsszesitettSzamlaKezelo(ujkod, 1);
		OsszesitettSzamlaKezelo(ujkod, 0);
		// A p�ld�nysz�m n�vel�se
		Query_Run(QE2, 'UPDATE SZFEJ2 SET SA_PELDANY = '+IntToStr(pld+1)+' WHERE SA_UJKOD = '+IntToStr(ujkod));
		Inc(pld);
	end;
	EgyebDlg.Szamlanyom		:= false;
	Result 	:= true;
end;

procedure TSzamlaNezoDlg.SzamlaKezelo(vujkod, euros, tipus : integer; FN: string = '');
begin
	// case EgyebDlg.v_formatum of
  case EgyebDlg.v_szamlaformatum of
		SZAMLA_FORMA_JS :	// J&S-es sz�ml�k = 0
		begin
	        Query_Run(QE1, 'SELECT * FROM SZFEJ WHERE SA_UJKOD = '+IntToStr(vujkod),true);
           if  QE1.FieldByName('SA_KIDAT').AsString < '2015.02.01.'  then begin
				Application.CreateForm(TSzamla_JS_HUFForm, Szamla_JS_HUFForm);
				if Szamla_JS_HUFForm.Tolt(IntToStr(vujkod)) then begin
          if tipus = 0 then begin
						Szamla_JS_HUFForm.Rep.Preview;
            end;  // if
					if tipus = 1 then begin
						Szamla_JS_HUFForm.Rep.Print;
  					end;  // if
					if tipus = 2 then begin
            QuickReportExport(Szamla_JS_HUFForm.Rep, FN);
  					end;  // if
				end;
				Szamla_JS_HUFForm.Destroy;
           end else begin
				Application.CreateForm(TSzamla_JSDlg, Szamla_JSDlg);
				if Szamla_JSDlg.Tolt(IntToStr(vujkod)) then begin
          // ------ az oldalsz�moz�s miatt kell ------
          Szamla_JsDlg.Rep.Prepare;
          Szamla_JsDlg.FTotalPages := Szamla_JsDlg.Rep.QRPRinter.PageCount;
          Szamla_JsDlg.Rep.QRPrinter.Free;
          Szamla_JsDlg.Rep.QRPrinter := nil;
          // -----------------------------------------
					if tipus = 0 then begin
						Szamla_JSDlg.Rep.Preview;
            end;  // if
					if tipus = 1 then begin
						Szamla_JSDlg.Rep.Print;
  					end;  // if
					if tipus = 2 then begin
            QuickReportExport(Szamla_JSDlg.Rep, FN);
  					end;  // if
				end;
				Szamla_JSDlg.Destroy;
           end;
		 //	end;
		end;

		SZAMLA_FORMA_DK :	// DK-s sz�ml�k  = 3
		begin
	        Query_Run(QE1, 'SELECT * FROM SZFEJ WHERE SA_UJKOD = '+IntToStr(vujkod),true);
          //  if  QE1.FieldByName('SA_KIDAT').AsString < '2015.09.01.'  then begin
          if  QE1.FieldByName('SA_KIDAT').AsString < EgyebDlg.UJ_DK_SZAMLA_TOL then begin
               if euros > 0 then begin
                   // Ilyenkor az eur�s sz�ml�t nyomtatjuk
                   Application.CreateForm(TSZAMLA_DK_EURForm, SZAMLA_DK_EURForm);
                   if SZAMLA_DK_EURForm.Tolt(IntToStr(vujkod)) then begin
                       if tipus = 0 then begin
                           SZAMLA_DK_EURForm.Rep.Preview;
                       end else begin
                           SZAMLA_DK_EURForm.Rep.Print;
                       end;
                   end;
                   SZAMLA_DK_EURForm.Destroy;
               end else begin
                   Application.CreateForm(TSZAMLA_DK_HUFForm, Szamla_DK_HUFForm);
                   if SZAMLA_DK_HUFForm.Tolt(IntToStr(vujkod)) then begin
                      if tipus = 0 then begin
                        SZAMLA_DK_HUFForm.Rep.Preview;
                        end;  // if
                      if tipus = 1 then begin
                        SZAMLA_DK_HUFForm.Rep.Print;
                        end;  // if
                      if tipus = 2 then begin
                        QuickReportExport(SZAMLA_DK_HUFForm.Rep, FN);
                        end;  // if
                   end;
                   SZAMLA_DK_HUFForm.Destroy;
               end;
           end else begin
               // Itt a DK �j sz�ml�ja
				Application.CreateForm(TSzamla_DKDlg, Szamla_DKDlg);
				if Szamla_DKDlg.Tolt(IntToStr(vujkod)) then begin
          // ------ az oldalsz�moz�s miatt kell ------
          Szamla_DKDlg.Rep.Prepare;
          Szamla_DKDlg.FTotalPages := Szamla_DKDlg.Rep.QRPRinter.PageCount;
          Szamla_DKDlg.Rep.QRPrinter.Free;
          Szamla_DKDlg.Rep.QRPrinter := nil;
          // -----------------------------------------
          if tipus = 0 then begin
            Szamla_DKDlg.Rep.Preview;
            end;  // if
          if tipus = 1 then begin
            Szamla_DKDlg.Rep.Print;
            end;  // if
          if tipus = 2 then begin
            QuickReportExport(Szamla_DKDlg.Rep, FN);
            end;  // if
				end;
				Szamla_DKDlg.Destroy;
           end;
		end;
		else begin
			NoticeKi('Hi�nyz� sz�mlaform�tum : '+IntToStr(EgyebDlg.v_szamlaformatum));
		end;
	end;
end;

procedure TSzamlaNezoDlg.OsszesitettSzamlaKezelo(vujkod, tipus : integer; FN: string = '');
begin
	// case EgyebDlg.v_formatum of
	case EgyebDlg.v_szamlaformatum of
		SZAMLA_FORMA_JS :	// J&S-es sz�ml�k = 0
		begin
	        Query_Run(QE1, 'SELECT * FROM SZFEJ2 WHERE SA_UJKOD = '+IntToStr(vujkod),true);
           if  QE1.FieldByName('SA_KIDAT').AsString < '2015.02.01.'  then begin
               Application.CreateForm(TOSZamla_JSDlg, OSZamla_JSDlg);
               if OSZamla_JSDlg.Tolt(IntToStr(vujkod)) then begin
                   if tipus = 0 then begin
                    OSZamla_JSDlg.Rep.Preview;
                    end;  // if
                  if tipus = 1 then begin
                    OSZamla_JSDlg.Rep.Print;
                    end;  // if
                  if tipus = 2 then begin
                    QuickReportExport(OSZamla_JSDlg.Rep, FN);
                    end;  // if
               end;
               OSZamla_JSDlg.Destroy;
           end else begin
               Application.CreateForm(TOSZamlaDlg, OSZamlaDlg);
               if OSZamlaDlg.Tolt(IntToStr(vujkod)) then begin
                   // ------ az oldalsz�moz�s miatt kell ------
                    OSZamlaDlg.Rep.Prepare;
                    OSZamlaDlg.FTotalPages := OSZamlaDlg.Rep.QRPRinter.PageCount;
                    OSZamlaDlg.Rep.QRPrinter.Free;
                    OSZamlaDlg.Rep.QRPrinter := nil;
                    // -----------------------------------------
                   if tipus = 0 then begin
                    OSZamlaDlg.Rep.Preview;
                    end;  // if
                  if tipus = 1 then begin
                    OSZamlaDlg.Rep.Print;
                    end;  // if
                  if tipus = 2 then begin
                    QuickReportExport(OSZamlaDlg.Rep, FN);
                    end;  // if
               end;
               OSZamlaDlg.Destroy;
           end;
		end;
		SZAMLA_FORMA_DK :	// DK sz�ml�k = 3
		begin
	        Query_Run(QE1, 'SELECT * FROM SZFEJ2 WHERE SA_UJKOD = '+IntToStr(vujkod),true);
           if  QE1.FieldByName('SA_KIDAT').AsString < '2015.05.01.'  then begin
               Application.CreateForm(TOSZamla_LEDlg, OSZamla_LEDlg);
               if OSZamla_LEDlg.Tolt(IntToStr(vujkod)) then begin
                  if tipus = 0 then begin
                    OSZamla_LEDlg.Rep.Preview;
                    end;  // if
                  if tipus = 1 then begin
                    OSZamla_LEDlg.Rep.Print;
                    end;  // if
                  if tipus = 2 then begin
                    QuickReportExport(OSZamla_LEDlg.Rep, FN);
                    end;  // if
               end;
               OSZamla_LEDlg.Destroy;
           end else begin
               // Itt az �j sz�mla
               Application.CreateForm(TOSZamlaDKDlg, OSZamlaDKDlg);
               if OSZamlaDKDlg.Tolt(IntToStr(vujkod)) then begin
                   // ------ az oldalsz�moz�s miatt kell ------
                   OSZamlaDKDlg.Rep.Prepare;
                   OSZamlaDKDlg.FTotalPages := OSZamlaDKDlg.Rep.QRPRinter.PageCount;
                   OSZamlaDKDlg.Rep.QRPrinter.Free;
                   OSZamlaDKDlg.Rep.QRPrinter := nil;
                   // -----------------------------------------
                  if tipus = 0 then begin
                    OSZamlaDKDlg.Rep.Preview;
                    end;  // if
                  if tipus = 1 then begin
                    OSZamlaDKDlg.Rep.Print;
                    end;  // if
                  if tipus = 2 then begin
                    QuickReportExport(OSZamlaDKDlg.Rep, FN);
                    end;  // if
               end;
               OSZamlaDKDlg.Destroy;
           end;
		end;
		else begin
			NoticeKi('Hi�nyz� sz�mlaform�tum : '+IntToStr(EgyebDlg.v_szamlaformatum));
		end;
	end;
end;

end.
