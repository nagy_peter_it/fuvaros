unit KiLizingList;

interface

uses
  WinTypes, WinProcs, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, QuickRpt, QRExpr, Qrctrls, ExtCtrls, printers, DB, DBTables, Grids, IniFiles,
  ADODB, ClipBrd, VclTee.TeeGDIPlus, VCLTee.Chart, VCLTee.TeEngine,
  VCLTee.Series, VCLTee.TeeProcs;

type
  TKilizingListDlg = class(TForm)
    Rep: TQuickRep;
    QRBand3: TQRBand;
    QRLSzamla: TQRLabel;
    QRBand1: TQRBand;
    QRBand2: TQRBand;
    QRLabel38: TQRLabel;
    QRLabel6: TQRLabel;
    QRSysData1: TQRSysData;
    QRLabel7: TQRLabel;
    QRSysData4: TQRSysData;
    QRLabel1: TQRLabel;
    Q3: TQRLabel;
    Q2: TQRLabel;
    Q5: TQRLabel;
    Q4: TQRLabel;
    QRLabel21: TQRLabel;
    Q1: TQRLabel;
	 ArfolyamGrid: TStringGrid;
    Osszesito: TStringGrid;
	 QRLabel13: TQRLabel;
	 QRLabel17: TQRLabel;
    Query1: TADOQuery;
    QRGroup1: TQRGroup;
    QRBand5: TQRBand;
    O1: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel5: TQRLabel;
	 QRLabel15: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRLabel18: TQRLabel;
    QRShape1: TQRShape;
    O6: TQRLabel;
    QV6: TQRLabel;
	 QRShape2: TQRShape;
    Query2: TADOQuery;
    Q0: TQRLabel;
	 ChildBand1: TQRChildBand;
	 SG1: TStringGrid;
	 QRImage1: TQRImage;
	 QRImage2: TQRImage;
    CH1: TChart;
    Series1: TBarSeries;
    CH2: TChart;
    BarSeries1: TBarSeries;
    QRLabel9: TQRLabel;
    Q31: TQRLabel;
	 procedure FormCreate(Sender: TObject);
	 procedure	Tolt(dat1, dat2, rendsz, vnem	: string; reszletes : boolean; sorrend : integer );
	 procedure QRBand1BeforePrint(Sender: TQRCustomBand;
	   var PrintBand: Boolean);
	 procedure QRBand2BeforePrint(Sender: TQRCustomBand;
	   var PrintBand: Boolean);
	 procedure RepBeforePrint(Sender: TCustomQuickRep;
	   var PrintReport: Boolean);
	 procedure QRGroup1BeforePrint(Sender: TQRCustomBand;
	   var PrintBand: Boolean);
	 procedure QRBand5BeforePrint(Sender: TQRCustomBand;
	   var PrintBand: Boolean);
	 procedure ChildBand1BeforePrint(Sender: TQRCustomBand;
	   var PrintBand: Boolean);
    procedure RepAfterPreview(Sender: TObject);
  private
		sorszam			: integer;
		reszlet			: boolean;
		osszertek		: double;
		vegertek		: double;
		tname			: string;
		sorrendezes		: integer;
		valnem			: string;
  public
	  vanadat			: boolean;
	  kellgrafikon		: boolean;
  end;

var
  KilizingListDlg: TKilizingListDlg;

implementation

uses
	J_SQL, Egyeb, KOzos, Kozos_Local;

{$R *.DFM}

procedure TKilizingListDlg.FormCreate(Sender: TObject);
begin
	EgyebDlg.SetADOQueryDatabase(Query1);
	EgyebDlg.SetADOQueryDatabase(Query2);
	sorszam			:= 0;
	kellgrafikon    := false;
	sorrendezes		:= 0;
end;

procedure	TKilizingListDlg.Tolt(dat1, dat2, rendsz, vnem	: string; reszletes : boolean; sorrend : integer );
var
	sqlstr		: string;
	dattol	  	: string;
	datig 		: string;
	ertek		: double;
	licegnev	: string;
	elotag		: string;
	cegkod		: string;
	aktho		: integer;
	str			: string;
	datstr		: string;
  lnap:string;
begin
	vanadat 	:= false;
	reszlet		:= reszletes;
	valnem		:= vnem;
	QrLabel21.Caption	:= dat1 + ' - ' + dat2;
	if dat1 = '' then begin
		dat1	:= '1990.01.01.';
	end;
	if dat2 = '' then begin
		dat2	:= EgyebDlg.MaiDatum;
	end;
	dattol 	:= copy(dat1,1,4)+copy(dat1,6,2);
	datig 	:= copy(dat2,1,4)+copy(dat2,6,2);


	// A t�bla l�trehoz�sa
	tname		:= 'T_LIZ'+EgyebDlg.user_code;
	Query_Run(Query1, 'DROP TABLE '+tname, FALSE);
	sqlstr:='CREATE TABLE '+tname+' (LI_RENSZ VARCHAR(10),  LI_ERTEK NUMERIC(12), LI_LICEG VARCHAR(50), '+
		'LI_ELTAG VARCHAR(2), LI_ACDAT VARCHAR(11), LI_MEGJE VARCHAR(80),LI_DATUM_ VARCHAR(11 ),LI_DATUM VARCHAR(11 ) )' ;
//  Clipboard.Clear;
//  Clipboard.AsText:=sqlstr;  
	Query_Run(Query1,sqlstr);
	// A t�bla felt�lt�se -> a befizetett l�zing adatok berak�sa
	sqlstr	:= 'SELECT * FROM LIZING WHERE LZ_BEDAT >= '''+dat1+''' AND LZ_BEDAT <= '''+dat2+''' ';
	if rendsz <> '' then begin
		sqlstr	:= sqlstr + ' AND LZ_RENSZ = '''+rendsz+''' ';
	end;
	Query_Run(Query1, sqlstr );
	while not Query1.Eof do begin
		cegkod		:= Query_Select('GEPKOCSI', 'GK_RESZ', Query1.FieldByName('LZ_RENSZ').AsString, 'GK_LICEG');
		licegnev	:= EgyebDlg.Read_SZGrid( '380', cegkod );
		elotag		:= '';
		ertek		:= GetValueInOtherCur(StringSzam(Query1.FieldByName('LZ_ERTEK').AsString), Query1.FieldByName('LZ_BEDAT').AsString,
			Query1.FieldByName('LZ_VANEM').AsString, vnem);
		if ertek = 0 then begin
			elotag	:= '*';
			ertek		:= GetValueInOtherCur(StringSzam(Query1.FieldByName('LZ_ERTEK').AsString), EgyebDlg.MaiDatum,
						   Query1.FieldByName('LZ_VANEM').AsString, vnem);
		end;
		if ertek = 0 then begin
			elotag	:= '**';
		end;
		Query_Insert( Query2, tname,
		[
		'LI_RENSZ', ''''+Query1.FieldByName('LZ_RENSZ').AsString+'''',
		'LI_DATUM', ''''+copy(Query1.FieldByName('LZ_BEDAT').AsString,1,4)+copy(Query1.FieldByName('LZ_BEDAT').AsString,6,2)+'''',
		'LI_DATUM_', ''''+Query1.FieldByName('LZ_DATUM').AsString+'''',
//		'LI_DATUM', ''''+Query1.FieldByName('LZ_DATUM').AsString+'''',
		'LI_ERTEK', SqlSzamString(ertek, 12,2),
		'LI_LICEG', ''''+licegnev+'''',
		'LI_ELTAG', ''''+elotag+'''',
		'LI_ACDAT', ''''+Query1.FieldByName('LZ_BEDAT').AsString+'''',
		'LI_MEGJE', ''''+Query1.FieldByName('LZ_MEGJE').AsString+''''
		]);
		Query1.Next;
	end;
	// A g�pkocsik havi k�telez�j�nek felt�lt�se
	sqlstr	:= 'SELECT * FROM GEPKOCSI left outer join SZOTAR on SZ_FOKOD=''240'' AND GK_LIPER = SZ_ALKOD WHERE GK_LIVAN = 1 ';
	if rendsz <> '' then begin
		sqlstr	:= sqlstr + ' AND GK_RESZ = '''+rendsz+''' ';
	end;
	Query_Run(Query1, sqlstr );
	if Query1.RecordCount > 0 then begin
		while not Query1.Eof do begin
			// V�gigmegy�nk a h�napokon �s ha nincs befizet�s, akkor berakjuk a sz�ks�ges �sszeget
			aktho := StrToIntDef(dattol, 0);
			while aktho <= StrToIntDef(datig, 0) do begin
				str	:= IntToStr(aktho);
				Query_Run(Query2, 'SELECT * FROM '+tname+' WHERE LI_DATUM = '''+str+''' '+
					' AND LI_RENSZ = '''+Query1.FieldByName('GK_RESZ').AsString+''' ');
				if Query2.RecordCount < 1 then begin
          lnap:=copy( Query1.FieldByName('GK_LIDAT').AsString,8,3);
					// Az utols� befizet�s d�tum�nak meghat�roz�sa
					datstr	:= GetLastLizingDate( Query1.FieldByName('GK_LIDAT').AsString, Query1.FieldByName('GK_LIHON').AsString, Query1.FieldByName('GK_LIPER').AsInteger);
					str		:= copy(Query1.FieldByName('GK_LIDAT').AsString,1,4)+ copy(Query1.FieldByName('GK_LIDAT').AsString,6,2);
					if ( ( datstr >= IntToStr(aktho) ) and ( str <= IntToStr(aktho)) ) then begin
						// Kell a bejegyz�s mert m�r elkezd�d�tt a lizing id�szak �s m�g nem j�rt le
						cegkod		:= Query_Select('GEPKOCSI', 'GK_RESZ', Query1.FieldByName('GK_RESZ').AsString, 'GK_LICEG');
						licegnev	:= EgyebDlg.Read_SZGrid( '380', cegkod );
						elotag		:= '';
						ertek		:= GetValueInOtherCur(StringSzam(Query1.FieldByName('GK_LIOSZ').AsString), EgyebDlg.MaiDatum, Query1.FieldByName('GK_LINEM').AsString, vnem);
						if ertek = 0 then begin
							elotag	:= '**';
						end;
						Query_Insert( Query2, tname,
						[
						'LI_RENSZ', ''''+Query1.FieldByName('GK_RESZ').AsString+'''',
						'LI_DATUM', ''''+IntToStr(aktho)+'''',
						'LI_ERTEK', SqlSzamString(ertek, 12,2),
						'LI_LICEG', ''''+licegnev+'''',
						'LI_ELTAG', ''''+elotag+'''',
						'LI_ACDAT', ''''+IntToStr(aktho)+lnap+'''',
						'LI_MEGJE', ''''+Query1.FieldByName('GK_LIMEG').AsString+''''
						]);
					end;
				end;
				Inc(aktho);
				if aktho mod 100 = 13 then begin
					aktho := aktho + 88;
				end;
			end;
			Query1.Next;
		end;
	end;

	// A t�nyleges SQL �sszerak�sa
	sorrendezes		:= sorrend;
	sqlstr			:= 'SELECT * FROM '+tname;
	case sorrend of
		0 : // Rendsz�m 	+ d�tum
		begin
			sqlstr	:= sqlstr + ' ORDER BY LI_RENSZ, LI_ACDAT';
			QrGroup1.Expression	:= 'Query1.LI_RENSZ';
			QrLabel4.Caption	:= 'G�pj�rm� : ';
		end;
		1 : // D�tum 		+ rendsz�m
		begin
			sqlstr	:= sqlstr + ' ORDER BY LI_DATUM, LI_RENSZ';
			QrGroup1.Expression	:= 'Query1.LI_DATUM';
			QrLabel4.Caption	:= 'H�nap : ';
		end;
		2 : // L�zing c�g  	+ rendsz�m
		begin
			sqlstr	:= sqlstr + ' ORDER BY LI_LICEG, LI_RENSZ, LI_DATUM';
			QrGroup1.Expression	:= 'Query1.LI_LICEG';
			QrLabel4.Caption	:= 'L�zing c�g : ';
		end;
	end;

	{A kapott sql alapj�n v�grehajtja a lek�rdez�st}
	Query_Run(Query1, sqlstr);
	vanadat := Query1.RecordCount > 0;
	if not reszlet then begin
		// Az �sszesen sorokat kisebbre venni
//		QRBand5.Height := O1.Top + O1.Height + 5;
//		QRShape2.Enabled 	:= false;
	end;
end;

procedure TKilizingListDlg.QRBand1BeforePrint(Sender: TQRCustomBand;
	   var PrintBand: Boolean);
begin
	Q1.Caption 	:= IntToStr(sorszam);
	Inc(sorszam);
	Q0.Caption 	:= Query1.FieldByName('LI_ELTAG').AsString;
	Q2.Caption 	:= Query1.FieldByName('LI_RENSZ').AsString;
	Q3.Caption 	:= Query1.FieldByName('LI_ACDAT').AsString;
//  if Query1.FieldByName('LI_DATUM_').IsNull then
//  	Q31.Caption 	:= Query1.FieldByName('LI_DATUM').AsString
//  else
  	Q31.Caption 	:= Query1.FieldByName('LI_DATUM_').AsString;
	Q4.Caption 	:= Format('%.2f', [StringSzam(Query1.FieldByName('LI_ERTEK').AsString)]);
	Q5.Caption 	:= Query1.FieldByName('LI_MEGJE').AsString;
	osszertek	:= osszertek + StringSzam(Q4.Caption);
	PrintBand	:= reszlet;
end;

procedure TKilizingListDlg.QRBand2BeforePrint(Sender: TQRCustomBand;
	   var PrintBand: Boolean);
begin
	// A v�g�sszesenek ki�r�sa
	QV6.Caption 	:= Format('%.2f', [vegertek]);
end;

procedure TKilizingListDlg.RepBeforePrint(Sender: TCustomQuickRep;
	   var PrintReport: Boolean);
begin
	vegertek	:= 0;
	sorszam		:= 1;
	osszertek	:= 0;
	SG1.RowCount	:= 1;
	SG1.Rows[0].Clear;
end;

procedure TKilizingListDlg.QRGroup1BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
	// A rendsz�m be�r�sa
	case sorrendezes of
		0 : // Rendsz�m 	+ d�tum
		begin
			QrLabel5.Caption	:= Query1.FieldByName('LI_RENSZ').AsString + ' ' +
				Query_Select('GEPKOCSI', 'GK_RESZ', Query1.FieldByName('LI_RENSZ').AsString, 'GK_TIPUS');
			O1.Caption	:= Query1.FieldByName('LI_RENSZ').AsString + ' �sszes :';
		end;
		1 : // D�tum 		+ rendsz�m
		begin
			QrLabel5.Caption	:= Query1.FieldByName('LI_DATUM').AsString;
			O1.Caption			:= Query1.FieldByName('LI_DATUM').AsString + ' �sszes :';
		end;
		2 : // L�zing c�g  	+ rendsz�m
		begin
			QrLabel5.Caption	:= Query1.FieldByName('LI_LICEG').AsString;
			O1.Caption			:= Query1.FieldByName('LI_LICEG').AsString + ' �sszes :';
		end;
	end;
	PrintBand			:= reszlet;
end;

procedure TKilizingListDlg.QRBand5BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
	if SG1.Cells[0,0] <> '' then begin
		SG1.RowCount	:= SG1.RowCount + 1;
	end;
	case sorrendezes of
		0 : // Rendsz�m 	+ d�tum
			SG1.Cells[0,SG1.RowCount - 1]	:= Query1.FieldByName('LI_RENSZ').AsString;
		1 : // D�tum 		+ rendsz�m
			SG1.Cells[0,SG1.RowCount - 1]	:= Query1.FieldByName('LI_DATUM').AsString;
		2 : // L�zing c�g  	+ rendsz�m
			SG1.Cells[0,SG1.RowCount - 1]	:= Query1.FieldByName('LI_LICEG').AsString;
	end;
	SG1.Cells[1,SG1.RowCount - 1]	:= Format('%.2f', [osszertek]);
	O6.Caption 	:= Format('%.2f', [osszertek]);
	vegertek	:= vegertek + osszertek;
	osszertek	:= 0;
	sorszam		:= 1;
end;

procedure TKilizingListDlg.ChildBand1BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
	i 			: integer;
	oszlopszam	: integer;
	tempfn		: string;
begin
	Printband	:= kellgrafikon;
	if kellgrafikon then begin
		// A grafikon �ssze�ll�t�sa
		CH1.Width	:= QrImage1.Width;
		CH1.Height	:= QrImage1.Height;
		CH1.Title.Text.Clear;
		CH1.Title.Text.Add('Kimutat�s a l�zingekr�l');
		CH1.Series[0].Clear;
		oszlopszam	:= SG1.RowCount - 1;
		if oszlopszam > 40 then begin
			oszlopszam	:= oszlopszam div 2;
		end;
		for i := 0 to oszlopszam do begin
			CH1.Series[0].AddXY(i, StringSzam(SG1.Cells[1,i]), SG1.Cells[0,i], clTeeColor);
//			CH1.Series[0].XLabel[i]		:= SG1.Cells[0,i];
			case sorrendezes of
				0 : // Rendsz�m 	+ d�tum
				begin
					CH1.BottomAxis.Title.Caption	:= 'Rendsz�mok';
					CH1.BottomAxis.LabelsAngle		:= 90;
				end;
				1 : // D�tum 		+ rendsz�m
				begin
					CH1.BottomAxis.Title.Caption	:= 'H�napok';
					CH1.BottomAxis.LabelsAngle		:= 0;
				end;
				2 : // L�zing c�g  	+ rendsz�m
				begin
					CH1.BottomAxis.Title.Caption	:= 'L�zingc�gek';
					CH1.BottomAxis.LabelsAngle		:= 0;
				end;
			end;
		end;
		CH1.LeftAxis.Title.Caption	:= valnem;
		tempfn	:=  EgyebDlg.GetTempFileName('.BMP');
		CH1.SaveToBitmapFile(tempfn);
//		CH1.CopyToClipboardBitmap;
//		QrImage1.Picture.Bitmap.LoadFromClipboardFormat(cf_BitMap,ClipBoard.GetAsHandle(cf_Bitmap), 0);
		QrImage1.Picture.Bitmap.LoadFromFile(tempfn);
		DeleteFile(tempfn);

		if ( SG1.RowCount - 1 ) > oszlopszam then begin
			// A 2. grafikon �ssze�ll�t�sa
			CH2.Width	:= QrImage2.Width;
			CH2.Height	:= QrImage2.Height;
			CH2.Title.Text.Clear;
			CH2.Title.Text.Add('Kimutat�s a l�zingekr�l');
			CH2.Series[0].Clear;
			for i := oszlopszam + 1 to SG1.RowCount - 1 do begin
				CH2.Series[0].AddXY(i, StringSzam(SG1.Cells[1,i]), SG1.Cells[0,i], clTeeColor);
	 //			CH2.Series[0].XLabel[i]		:= SG1.Cells[0,i];
				case sorrendezes of
					0 : // Rendsz�m 	+ d�tum
					begin
						CH2.BottomAxis.Title.Caption	:= 'Rendsz�mok';
						CH2.BottomAxis.LabelsAngle		:= 90;
					end;
					1 : // D�tum 		+ rendsz�m
					begin
						CH2.BottomAxis.Title.Caption	:= 'H�napok';
						CH2.BottomAxis.LabelsAngle		:= 0;
					end;
					2 : // L�zing c�g  	+ rendsz�m
					begin
						CH2.BottomAxis.Title.Caption	:= 'L�zingc�gek';
						CH2.BottomAxis.LabelsAngle		:= 0;
					end;
				end;
			end;
			CH2.LeftAxis.Title.Caption	:= valnem;
//			CH2.CopyToClipboardBitmap;
//			QrImage2.Picture.Bitmap.LoadFromClipboardFormat(cf_BitMap,ClipBoard.GetAsHandle(cf_Bitmap), 0);
			tempfn	:=  EgyebDlg.GetTempFileName('.BMP');
			CH2.SaveToBitmapFile(tempfn);
			QrImage2.Picture.Bitmap.LoadFromFile(tempfn);
			DeleteFile(tempfn);
		end;
	end;
end;

procedure TKilizingListDlg.RepAfterPreview(Sender: TObject);
begin
	Query_Run(Query1, 'DROP TABLE '+'T_LIZ'+EgyebDlg.user_code, FALSE);
end;

end.
