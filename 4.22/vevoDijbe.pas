unit VevoDijBe;

interface

uses
	SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
	Forms, Dialogs, StdCtrls, Buttons, Mask, DB, DBTables, ADODB;
	
type
	TVevodijbeDlg = class(TForm)
	BitElkuld: TBitBtn;
	BitKilep: TBitBtn;
	Label1: TLabel;
	Label2: TLabel;
	Label3: TLabel;
    M1: TMaskEdit;
    M2: TMaskEdit;
    M3: TMaskEdit;
	Label9: TLabel;
	ButValVevo: TBitBtn;
    Label10: TLabel;
    M5: TMaskEdit;
    M4: TMaskEdit;
    Label12: TLabel;
    CVnem: TComboBox;
    Query1: TADOQuery;
	procedure BitKilepClick(Sender: TObject);
	procedure BitElkuldClick(Sender: TObject);
  procedure MaskEditClick(Sender: TObject);
  procedure MaskEditKeyPress(Sender: TObject; var Key: Char);
  procedure Tolt(vekod, sorsz : string);
  procedure ButValVevoClick(Sender: TObject);
  procedure FormCreate(Sender: TObject);
  procedure FormDestroy(Sender: TObject);
    procedure M5Exit(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
	private
     	vnemlist   	: TStringList;
       vevokod		: string;
	public
   	ret_sorsz	: string;
end;

var
	VevodijbeDlg: TVevodijbeDlg;

implementation

uses
	Egyeb , TermekFm, J_SQL, Kozos;
{$R *.DFM}

procedure TVevodijbeDlg.FormCreate(Sender: TObject);
var
  sorsz	: integer;
begin
	EgyebDlg.SetADOQueryDatabase(Query1);
	{A valutanem t�bla felt�lt�se a k�dokkal}
  	vnemlist := TStringList.Create;
	SzotarTolt(CVnem, '110' , vnemlist, false, false );
  	CVnem.Items.Clear;
  	for sorsz := 0 to vnemlist.Count - 1 do begin
  		CVnem.Items.Add(vnemlist[sorsz]);
  	end;
end;

procedure TVevodijbeDlg.BitKilepClick(Sender: TObject);
begin
	ret_sorsz	:= '';
	Close;
end;

procedure TVevodijbeDlg.BitElkuldClick(Sender: TObject);
begin
	if vevokod = '' then begin
		NoticeKi('�rv�nytelen vev�k�d!');
       Exit;
   end;
   if M2.Text = '' then begin
   	NoticeKi('A term�k kiv�laszt�sa k�telez�!');
       Exit;
   end;
	if ret_sorsz = '' then begin
   	// �j rekord l�trehoz�sa
       Query_Run(Query1, 'SELECT MAX(VD_SORSZ) MAXSOR FROM VEVODIJ WHERE VD_VEKOD = '''+vevokod+''' ');
		ret_sorsz	:= IntToStr(StrToIntDef(Query1.FieldByName('MAXSOR').AsString,0)+1);
  		Query_Insert( Query1, 'VEVODIJ', [
    		'VD_VEKOD', ''''+vevokod+'''',
           'VD_SORSZ', ret_sorsz,
           'VD_TEKOD', ''''+M2.Text+'''',
           'VD_EGYAR', SqlSzamString(StringSzam(M5.Text), 12,2 ),
           'VD_VANEM', ''''+CVNem.Text+''''
           ] );

   end else begin
		// R�gebbi rekord m�dosit�sa t�rt�nik
  		Query_Update( Query1, 'VEVODIJ', [
           'VD_TEKOD', ''''+M2.Text+'''',
           'VD_EGYAR', SqlSzamString(StringSzam(M5.Text), 12,2 ),
           'VD_VANEM', ''''+CVNem.Text+''''
           ], ' WHERE VD_VEKOD = '''+vevokod+''' AND VD_SORSZ = '+ret_sorsz );
   end;
   EgyebDlg.Valutanem	:= CVNem.Text;
	Close;
end;

procedure TVevodijbeDlg.MaskEditClick(Sender: TObject);
begin
	with Sender as TMaskEdit do begin
  		SelectAll;
  	end;
end;

procedure TVevodijbeDlg.MaskEditKeyPress(Sender: TObject; var Key: Char);
begin
  	with Sender as TMaskEdit do begin
  		Key := EgyebDlg.Jochar(Text,Key,12,2);
  	end;
end;

procedure TVevodijbeDlg.Tolt(vekod, sorsz : string);
begin
	vevokod 	:= vekod;
   ret_sorsz	:= sorsz;
   M1.Text		:= sorsz;
   ComboSet (CVNem, EgyebDlg.Valutanem, vnemlist);
   if sorsz <> '' then begin
       Query_Run(Query1, 'SELECT * FROM VEVODIJ WHERE VD_VEKOD = '''+vevokod+''' AND VD_SORSZ = '+sorsz );
       if Query1.RecordCount > 0 then begin
       	M2.Text		:= Query1.FieldByName('VD_TEKOD').AsString;
           M3.Text		:= Query_Select ('TERMEK', 'T_TERKOD', Query1.FieldByName('VD_TEKOD').AsString , 'T_TERNEV');
           M4.Text		:= Query_Select ('TERMEK', 'T_TERKOD', Query1.FieldByName('VD_TEKOD').AsString , 'T_LEIRAS');
           M5.Text		:= Query1.FieldByName('VD_EGYAR').AsString;
           ComboSet (CVNem, Query1.FieldByName('VD_VANEM').AsString, vnemlist);
       end;
   end;
end;

procedure TVevodijbeDlg.ButValVevoClick(Sender: TObject);
begin
	Application.CreateForm(TTermekFmDlg, TermekFmDlg);
	TermekFmDlg.valaszt := true;
	TermekFmDlg.ShowModal;
	if TermekFmDlg.ret_vkod <> '' then begin
   	if Query_Run(Query1, 'SELECT * FROM TERMEK WHERE T_TERKOD = '''+TermekFmDlg.ret_vkod+''' ') then begin
      		M2.Text 	:= Query1.FieldByName('T_TERKOD').AsString;
      		M3.Text 	:= Query1.FieldByName('T_TERNEV').AsString;
      		M4.Text 	:= Query1.FieldByName('T_LEIRAS').AsString;
           M5.SetFocus;
   	end;
	end;
	TermekFmDlg.Destroy;
end;

procedure TVevodijbeDlg.FormDestroy(Sender: TObject);
begin
	vnemlist.Free;
end;

procedure TVevodijbeDlg.M5Exit(Sender: TObject);
begin
 	M5.Text := StrSzamStr( M5.Text, 12, 2);
end;

procedure TVevodijbeDlg.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
	EgyebDlg.FormReturn(Key);
end;

end.
