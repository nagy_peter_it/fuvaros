unit Kialval2;

interface

uses
  WinTypes, WinProcs, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, QuickRpt, QRExpr, Qrctrls, ExtCtrls, printers, DB, DBTables,
  Egyeb, Forgalom, J_SQL, Kozos, Kozos_Local, ADODB ,StrUtils;

type
  TKiAlval2Dlg = class(TForm)
    Rep: TQuickRep;
    QRBand3: TQRBand;
    QRLSzamla: TQRLabel;
    QRBand1: TQRBand;
    QRBand2: TQRBand;
    QRLabel15: TQRLabel;
    QRLabel16: TQRLabel;
    QRLabel25: TQRLabel;
    QRLabel38: TQRLabel;
    QRLabel56: TQRLabel;
    QRLabel57: TQRLabel;
    QRLabel63: TQRLabel;
    QRLabel6: TQRLabel;
    QRSysData1: TQRSysData;
	 QRLabel7: TQRLabel;
    QRSysData4: TQRSysData;
    QRShape1: TQRShape;
	 QRLabel8: TQRLabel;
	 QRLabel2: TQRLabel;
	 QV1: TQRLabel;
	 QRLabel14: TQRLabel;
    QRLabel19: TQRLabel;
    QRLabel22: TQRLabel;
    QRLabel23: TQRLabel;
    QRLabel24: TQRLabel;
    QRLabel26: TQRLabel;
    QRLabel27: TQRLabel;
    QRLabel28: TQRLabel;
    QRGroup1: TQRGroup;
    QRBand4: TQRBand;
    QRLabel1: TQRLabel;
    QR1: TQRLabel;
    QR2: TQRLabel;
    Query1: TADOQuery;
    QR3: TQRLabel;
    QV2: TQRLabel;
	 QRShape2: TQRShape;
    QRLabel4: TQRLabel;
    QV3: TQRLabel;
	 Query2: TADOQuery;
    QRLabel3: TQRLabel;
    QRLabel11: TQRLabel;
    QRLabel12: TQRLabel;
    QR5: TQRLabel;
    QRLabel29: TQRLabel;
    QV5: TQRLabel;
    QRLabel13: TQRLabel;
    QRLabel18: TQRLabel;
    QRLabel17: TQRLabel;
    QRLabel10: TQRLabel;
    QRLabel20: TQRLabel;
    QRLabel9: TQRLabel;
    QRLabel43: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel34: TQRLabel;
    QRLabel31: TQRLabel;
    procedure FormCreate(Sender: TObject);
	 function	Tolt(dat1, dat2, alval : string ) : boolean;
	 procedure QRBand1BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
    procedure QRBand2BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
	 procedure RepBeforePrint(Sender: TCustomQuickRep;
  var PrintReport: Boolean);
	 procedure QRBand4BeforePrint(Sender: TQRCustomBand;
	   var PrintBand: Boolean);
	 procedure QRGroup1BeforePrint(Sender: TQRCustomBand;
	   var PrintBand: Boolean);
    procedure QRBand3BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure FormDestroy(Sender: TObject);
  private
	 ossz1			: double;
	 ossz2			: double;
	 ossz3			: double;
	 ossz4			: double;
	 ossz5			: double;
	 ossz6			: double;
	 reszossz1		: double;
	 reszossz2		: double;  // ebben a HUF �sszeget szumm�zzuk, az elv�rt nyeres�g vizsg�lathoz
	 reszossz21		: double;  // ebben az alj�ratok k�lts�g�t szumm�zzuk
	 reszossz3   	: double;
	 reszossz4	    : double;
	 reszossz5	    : double;
	 reszossz6	    : double;
	 recszam		: integer;
	 jaratlista		: TStringList;
  public
	 reszletes		: boolean;
  end;

var
  KiAlval2Dlg: TKiAlval2Dlg;

implementation
uses ForgAlval, FogyasztSzamolo;

{$R *.DFM}

procedure TKiAlval2Dlg.FormCreate(Sender: TObject);
begin
	EgyebDlg.SetADOQueryDatabase(Query1);
	EgyebDlg.SetADOQueryDatabase(Query2);
	ossz1			:= 0;
	ossz2			:= 0;
	ossz3			:= 0;
	ossz4			:= 0;
	ossz5			:= 0;
	ossz6			:= 0;
	reszossz1	    := 0;
	reszossz2	    := 0;
	reszossz21    := 0;
	reszossz3	    := 0;
	reszossz4	    := 0;
	reszossz5	    := 0;
	reszossz6	    := 0;
	EllenListTorol;
	reszletes	    := true;
	jaratlista		:= TStringList.Create;
  Application.CreateForm(TFogyasztSzamoloDlg, FogyasztSzamoloDlg);
end;

function TKiAlval2Dlg.Tolt(dat1, dat2, alval : string ) : boolean;
var
  sqlstr	: string;
  d1		: string;
  d2		: string;
begin
	{A fejl�c adatainak ki�r�sa}
	QrLabel19.Caption := dat1 +' - '+dat2;
	if alval = '' then begin
		QrLabel23.Caption := 'Minden alv�llalkoz�';
	end else begin
		QrLabel23.Caption := alval;
	end;
	d1	:= dat1;
	d2	:= dat2;
	if d2 = '' then begin
		d2 := EgyebDlg.MaiDatum;;
	end;

	{Sz�r�s az id�szakra}
	sqlstr	:=	'SELECT * FROM JARAT LEFT OUTER JOIN VISZONY ON VI_JAKOD = JA_KOD, ALJARAT WHERE JA_FAZIS<>''2'' AND AJ_JAKOD = JA_KOD AND AJ_ALNEV <> '''' '+
		' AND ( JA_JKEZD >= '''+d1+ ''' ) AND ( JA_JKEZD <= '''+d2+''' ) ';
	if alval  <> '' then begin
		sqlstr	:= sqlstr + ' AND VI_VENEV = '''+alval+''' ';
	end;
	sqlstr	:= sqlstr + ' ORDER BY VI_VENEV, JA_JKEZD ';
	Query_Run(Query1, sqlstr, true);
	QrGroup1.Expression	:= 'Query1.VI_VENEV';
	Result	:= ( Query1.RecordCount > 0 );
end;

procedure TKiAlval2Dlg.FormDestroy(Sender: TObject);
begin
  if FogyasztSzamoloDlg<> nil then FogyasztSzamoloDlg.Release;
end;

procedure TKiAlval2Dlg.QRBand1BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
	eurossz		: double;
	hufossz		: double;
	szamossz	: double;
	szameur		: double;
	ertek		: double;
	eurarf, nyEUR, nyHUF, aljaratossz: double;
	ardat		: string;
  ossz,oszaz: integer;
  szaz:double;
  csatoltkoltseg: boolean;
  AljaratKoltseg: TDoubleValidAdat;
  res: TDoubleValidAdat;
begin
  ossz:=StrToIntDef(ForgAlvalDlg.Edit1.Text,0);
  oszaz:=StrToIntDef(ForgAlvalDlg.Edit2.Text,0);
	PrintBand	:= true;
	if jaratlista.IndexOf(Query1.FieldByName('JA_KOD').AsString) > -1 then begin
		PrintBand	:= false;
		Exit;
	end;
	jaratlista.Add(Query1.FieldByName('JA_KOD').AsString);
	QrLabel3.Caption	:= IntToStr(recszam)+'.';
	Inc(recszam);

  csatoltkoltseg:= (Query_Select('JARATKOLTSEG','JK_CSJAR',Query1.FieldByName('JA_KOD').AsString,'JK_JAKOD')<>'');
  if csatoltkoltseg then QRLabel3.Caption:='*'+QRLabel3.Caption;

	QrLabel26.Caption	:= GetJaratszam(Query1.FieldByName('JA_KOD').AsString);
	QrLabel24.Caption	:= Query1.FieldByName('JA_JKEZD').AsString;
	QrLabel12.Caption	:= Query1.FieldByName('AJ_ALREN').AsString;
	// A j�rat ellen�rz�tts�g�nek vizsg�lata
	if StrToIntDef(Query1.FieldByname('JA_FAZIS').AsString,0) < 1 then begin
		EllenListAppend('Nem ellen�rz�tt j�rat : ('+Query1.FieldByName('JA_KOD').AsString + ' ) ' + QrLabel26.Caption, false);
	end;
   hufossz:=EgyebDlg.JaratkoltsegHUF(Query1.FieldByName('JA_KOD').AsString);
   eurossz:=EgyebDlg.JaratkoltsegEUR(Query1.FieldByName('JA_KOD').AsString);

  res:= FogyasztSzamoloDlg.Jarat_Egyeb_km(Query1.FieldByName('JA_KOD').AsString);
  if res.IsValid then begin  // �rv�nyes fogyaszt�si adatok
      aljaratossz:= res.EURValue;  // EUR-ban!
      end // if
  else begin
      aljaratossz:=0;
      end;


//	eurossz				:= GetValueInOtherCur(StringSzam(Query1.FieldByName('AJ_FUDIJ').AsString),
//						   Query1.FieldByName('JA_JKEZD').AsString, Query1.FieldByName('AJ_FUVAL').AsString, 'EUR');
	QrLabel27.Caption 	:= Format('%.2f', [eurossz]);
//	hufossz				:= GetValueInOtherCur(StringSzam(Query1.FieldByName('AJ_FUDIJ').AsString),
//						   Query1.FieldByName('JA_JKEZD').AsString, Query1.FieldByName('AJ_FUVAL').AsString, 'HUF');

	// QrLabel28.Caption   := Format('%.0f', [hufossz]);
    QrLabel28.Caption 	:= Format('%.2f', [aljaratossz]);

	if Query1.FieldByName('AJ_FUVAL').AsString <> 'EUR' then begin
		QrLabel24.Caption := QrLabel24.Caption + ' *';
	end;
	// A sz�mla�sszegek meghat�roz�sa
	szamossz			:= 0;
	szameur	:= 0;
//	Query_Run(Query2, 'SELECT * FROM SZSOR,SZFEJ WHERE ss_kod=sa_kod and sa_flag='''+'0'''+' and SS_KOD<>'''+''' and SS_UJKOD IN (SELECT VI_UJKOD FROM VISZONY, JARAT WHERE JA_KOD = VI_JAKOD AND JA_KOD = '''+Query1.FieldByName('JA_KOD').AsString+''')');
	Query_Run(Query2, 'SELECT * FROM SZSOR WHERE SS_KOD<>'''+''' and SS_UJKOD IN (SELECT VI_UJKOD FROM VISZONY, JARAT WHERE JA_KOD = VI_JAKOD AND JA_KOD = '''+Query1.FieldByName('JA_KOD').AsString+''')');
//	Query_Run(Query2, 'SELECT * FROM SZSOR WHERE SS_UJKOD = '+IntToStr(StrToIntDef(Query1.FieldByName('VI_UJKOD').AsString,0)) );
	while not Query2.Eof do begin
		ertek		:= Kerekit(StringSZam(Query2.FieldByName('SS_EGYAR').AsString) * StringSzam(Query2.FieldByName('SS_DARAB').AsString));
		szamossz    := szamossz + ertek;
		eurarf		:= 0;
		if Query2.FieldByName('SS_VALARF').AsFloat <> 0 then begin
			eurarf	:= StringSzam(Query2.FieldByName('SS_VALARF').AsString);
		end;
		if Query2.FieldByName('SS_VALNEM').AsString = 'EUR' then begin
			if eurarf > 0 then begin
				szameur :=  szameur + StringSzam(Format('%.2f',[ertek / eurarf]));
			end;
		end else begin
			{Mindenk�ppen ki�rjuk a valut�s �rt�ket a HUF -b�l kiindulva}
			eurarf   	:= StringSzam(Query2.FieldByName('SS_ARFOL').AsString);
			if eurarf > 1 then begin
				szameur :=  szameur + StringSzam(Format('%.2f',[ertek / eurarf]));
			end else begin
				ardat		:= Query2.FieldByName('SS_ARDAT').AsString;
				if ardat = '' then begin
				   ardat	:= Query1.FieldByName('JA_JKEZD').AsString;
				end;
				eurarf := EgyebDlg.ArfolyamErtek( 'EUR', ardat, true);
				if eurarf > 0 then begin
					szameur :=  szameur + StringSzam(Format('%.2f',[ertek / eurarf]));
				end;
			end;
		end;
		Query2.Next;
	end;

  szaz:=0;
  if szamossz<>0 then
  szaz:= (szamossz-hufossz) / szamossz * 100;
//  if (ForgAlvalDlg.CheckBox4.Checked)and(ForgAlvalDlg.CheckBox1.Checked)and(szamossz-hufossz > ossz) then
  if (ForgAlvalDlg.CheckBox4.Checked)and
     (((ForgAlvalDlg.CheckBox1.Checked)and(ForgAlvalDlg.CheckBox2.Checked)and(szamossz-hufossz  > ossz)) or
      ((ForgAlvalDlg.CheckBox1.Checked)and(ForgAlvalDlg.CheckBox3.Checked)and(szaz > oszaz))) then
  begin
    PrintBand:=False;
    exit;
  end;

	// QrLabel21.Caption 	:= Format('%.0f', [szamossz]);
	QrLabel2.Caption 	:= Format('%.2f', [szameur]);
  // nyEUR:=szameur-eurossz;
  nyEUR:=szameur-eurossz-aljaratossz; // az alj�ratok d�j�t is levonjuk
  if eurossz=0 then nyEUR:=0;
  // nyHUF:=szamossz-hufossz;
  // if hufossz=0 then nyHUF:=0;
	QrLabel29.Caption 	:= Format('%.2f', [nyEUR]);
//	QrLabel30.Caption 	:= Format('%.0f', [nyHUF]);
//	QrLabel29.Caption 	:= Format('%.2f', [szameur-eurossz]);
//	QrLabel30.Caption 	:= Format('%.0f', [szamossz-hufossz]);
	reszossz1			:= reszossz1 + eurossz;
	reszossz2			:= reszossz2 + hufossz;
  reszossz21		:= reszossz21 + aljaratossz;
	reszossz3			:= reszossz3 + szameur;
	reszossz4			:= reszossz4 + szamossz;
	reszossz5			:= reszossz5 + nyEUR;
	// reszossz6			:= reszossz6 + nyHUF;
	PrintBand			:= reszletes;
end;

procedure TKiAlval2Dlg.QRBand2BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
	EllenListMutat('Nem ellen�rz�tt j�ratok', true);
	QV1.Caption 	:= Format('%.2f', [ossz1]);
	QV2.Caption 	:= Format('%.0f', [ossz2]);
	QV3.Caption 	:= Format('%.2f', [ossz3]);
	// QV4.Caption 	:= Format('%.0f', [ossz4]);
	QV5.Caption 	:= Format('%.2f', [ossz5]);
	// QV6.Caption 	:= Format('%.0f', [ossz6]);
end;

procedure TKiAlval2Dlg.RepBeforePrint(Sender: TCustomQuickRep;
  var PrintReport: Boolean);
begin
	ossz1			:= 0;
	ossz2			:= 0;
	ossz3			:= 0;
	ossz4			:= 0;
	ossz5			:= 0;
	ossz6			:= 0;
	reszossz1	    := 0;
	reszossz2	    := 0;
  reszossz21    := 0;
	reszossz3	    := 0;
	reszossz4	    := 0;
	reszossz5	    := 0;
	reszossz6	    := 0;
	jaratlista.Clear;
end;

procedure TKiAlval2Dlg.QRBand4BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
  ossz,oszaz:integer;
  szaz:double;
begin
  ossz:=StrToIntDef(ForgAlvalDlg.Edit1.Text,0);
  oszaz:=StrToIntDef(ForgAlvalDlg.Edit2.Text,0);
	if Query1.FieldByName('VI_VENEV').AsString = '' then begin
		QrLabel1.Caption  	:= 'Sz�mla n�lk�li megb�z�sok �sszesen : ';
	end else begin
		QrLabel1.Caption  	:= Query1.FieldByName('VI_VENEV').AsString+' �sszesen : ';
	end;
	QR1.Caption 		:= Format('%.2f', [reszossz1]);
  QR2.Caption 		:= Format('%.2f', [reszossz21]);
	QR3.Caption 		:= Format('%.2f', [reszossz3]);
	// QR4.Caption 		:= Format('%.0f', [reszossz4]);
	QR5.Caption 		:= Format('%.2f', [reszossz5]);
	// QR6.Caption 		:= Format('%.0f', [reszossz6]);

  szaz:=0;
  if reszossz4<>0 then
  szaz:= (reszossz4-reszossz2) / reszossz4 * 100;
//  if (not ForgAlvalDlg.CheckBox4.Checked)and(ForgAlvalDlg.CheckBox1.Checked)and(reszossz4-reszossz2 > ossz) then
  if (not ForgAlvalDlg.CheckBox4.Checked)and
     (((ForgAlvalDlg.CheckBox1.Checked)and(ForgAlvalDlg.CheckBox2.Checked)and(reszossz4-reszossz2 > ossz)) or
      ((ForgAlvalDlg.CheckBox1.Checked)and(ForgAlvalDlg.CheckBox3.Checked)and(szaz > oszaz))) then
  begin
  	reszossz1	:= 0;
	  reszossz2	:= 0;
 	  reszossz21:= 0;
  	reszossz3	:= 0;
	  reszossz4	:= 0;
	  reszossz5	:= 0;
	  reszossz6	:= 0;
    PrintBand:=False;
    exit;
  end;

	ossz1		:= ossz1 + reszossz1;
	ossz2		:= ossz2 + reszossz21;
	ossz3		:= ossz3 + reszossz3;
	ossz4		:= ossz4 + reszossz4;
	ossz5		:= ossz5 + reszossz5;
	ossz6		:= ossz6 + reszossz6;
	reszossz1	:= 0;
	reszossz2	:= 0;
	reszossz21:= 0;
	reszossz3	:= 0;
	reszossz4	:= 0;
	reszossz5	:= 0;
	reszossz6	:= 0;
end;

procedure TKiAlval2Dlg.QRGroup1BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
	if Query1.FieldByName('VI_VENEV').AsString = '' then begin
		QrLabel4.Caption	:= 'Sz�mla n�lk�li megb�z�sok';
	end else begin
		QrLabel4.Caption	:= Query1.FieldByName('VI_VENEV').AsString;
	end;
	recszam				:= 1;
	PrintBand			:= reszletes;
end;

procedure TKiAlval2Dlg.QRBand3BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  QRLabel18.Enabled:=ForgAlvalDlg.CheckBox1.Checked;
  QRLabel18.Caption:='Sz�r�s: Nyeres�g < '+IfThen(ForgAlvalDlg.CheckBox2.Checked,Trim(ForgAlvalDlg.Edit1.Text)+' Ft',Trim(ForgAlvalDlg.Edit2.Text)+' %');

end;

end.
