unit Valszuro;

interface

uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, DBTables, DB, StdCtrls, Buttons, Mask, DBCtrls,
  Printers, ExtCtrls,  ADODB , J_FOFORMUJ, Menus, CheckLst, IniFiles;

type
  TValszuroDlg = class(TForm)
	 Memo1: TMemo;
	 Query1: TADOQuery;
	 Panel2: TPanel;
	 Panel1: TPanel;
	 BitKilep: TBitBtn;
    PopupMenu1: TPopupMenu;
    OszlopCsokkent: TMenuItem;
    OszlopNovel: TMenuItem;
    CLB1: TCheckListBox;
    EgyoszlopFugg: TMenuItem;
    EgyoszlopViz: TMenuItem;
    N1: TMenuItem;
    AdminSetting: TMenuItem;
    N2: TMenuItem;
    Kijells1: TMenuItem;
    Kijellsmegszntetse1: TMenuItem;
    BitElkuld: TBitBtn;
    Panel3: TPanel;
    Edit1: TEdit;
    Timer1: TTimer;
    BitBtn5: TBitBtn;
	 procedure BitBtn6Click(Sender: TObject);
	 procedure BitElkuldClick(Sender: TObject);
	 procedure BitKilepClick(Sender: TObject);
	 procedure FormShow(Sender: TObject);
	 procedure FormDestroy(Sender: TObject);
	 procedure FormCreate(Sender: TObject);
	 procedure Tolt(mezonev, sqlstr : string; tagnr : integer);
   procedure Tolt2(mezonev: string; elemlista: TStringList; tagnr : integer);
	 procedure CLB1Click(Sender: TObject);
	 procedure FormResize(Sender: TObject);
    procedure OszlopCsokkentClick(Sender: TObject);
    procedure OszlopNovelClick(Sender: TObject);
    procedure FormCanResize(Sender: TObject; var NewWidth,
      NewHeight: Integer; var Resize: Boolean);
    procedure EgyoszlopFuggClick(Sender: TObject);
    procedure EgyoszlopVizClick(Sender: TObject);
	 procedure FillProperties(mezo_nev : string; dial_nr : integer);
	 procedure MenuBeallito;
    procedure AdminSettingClick(Sender: TObject);
    procedure Kijells1Click(Sender: TObject);
	 procedure Jelolgetes(beki : boolean);
    procedure Kijellsmegszntetse1Click(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Timer1Timer(Sender: TObject);
    procedure Edit1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure BitBtn5Click(Sender: TObject);
	 private
		foselected		: boolean;
       dialnr			: integer;
       mezonev			: string;
       dialpos			: TRect;
       ekeres: string;
  public
		kodkilist   	: TStringList;
		voltenter		: boolean;
		mezo_sorsz		: integer;
		kelluresszures 	: boolean;
       dial_left		: integer;
       dial_top		: integer;
  end;

var
  ValszuroDlg: TValszuroDlg;

implementation

uses
	Egyeb, Kozos, J_SQL;
{$R *.DFM}

procedure TValszuroDlg.BitBtn6Click(Sender: TObject);
begin
	Close;
end;

procedure TValszuroDlg.BitElkuldClick(Sender: TObject);
var
	i 				: integer;
	mindenjelolt	: boolean;
begin
	// Itt �ll�tjuk �ssze a visszaadand� stringeket
	kodkilist.Clear;
	voltenter		:= true;
	// mindenjelolt	:= CLB1.Checked[1];  // az 1. elem az "�res"!
  mindenjelolt	:= CLB1.Checked[0];
	if CLB1.Checked[1] = true then begin
		kodkilist.Add('');
	end;
	for i := 2 to CLB1.Items.Count - 1 do begin
		if CLB1.Checked[i] = true then begin
			kodkilist.Add(CLB1.Items[i]);
		end else begin
			mindenjelolt	:= false;
		end;
	end;
	if mindenjelolt then begin
		// Ha mindegyik ki van jel�lve, akkor az olyan, mintha egyiket sem jel�lt�k volna ki!!!
		kodkilist.Clear;
	end;
//	fodial.VoltSzuroEnter(mezo_sorsz);
	Close;
end;

procedure TValszuroDlg.BitKilepClick(Sender: TObject);
begin
	kodkilist.Clear;
	voltenter	:= false;
	Close;
end;

procedure TValszuroDlg.FillProperties(mezo_nev : string; dial_nr : integer);
begin
	mezonev	:= mezo_nev;
   dialnr	:= dial_nr;
end;

procedure TValszuroDlg.Tolt(mezonev, sqlstr : string; tagnr : integer);
var
	i 	 : integer;
begin
	Query1.Close;
	Query1.Tag		:= tagnr;
	EgyebDlg.SetADOQueryDatabase(Query1);
	Query_Run(Query1, sqlstr);
	CLB1.Clear;
	CLB1.Items.Add('minden �rt�k');
	CLB1.Items.Add('�res �rt�k');
	while not Query1.Eof do begin
		if Query1.FieldByName(mezonev).AsString <> '' then begin
			CLB1.Items.Add(Query1.FieldByName(mezonev).AsString);
		end;
		Query1.Next;
	end;
(*
	for i := 0 to kodkilist.Count - 1 do begin
		if  kodkilist[i] = '' then begin
			CLB1.Checked[1] := true;
		end;
	end;
*)
	// Az �res �rt�k mindig legyen bekapcsolva alapb�l
	CLB1.Checked[1] 	:= true;
	CLB1.ItemEnabled[1]	:= kelluresszures;
	for i := 2 to CLB1.Items.Count - 1 do begin
		if  kodkilist.IndexOf(CLB1.Items[i]) > -1 then begin
			CLB1.Checked[i] := true;
		end;
	end;
	// A kezdo param�terek be�ll�t�sa
	CLB1.Columns	:= 0;
end;

// �j st�lus: a t�bl�zatb�l vett elemlist�t dobja fel
procedure TValszuroDlg.Tolt2(mezonev: string; elemlista: TStringList; tagnr : integer);
var
	i 	 : integer;
begin
	CLB1.Clear;
	CLB1.Items.Add('minden �rt�k');
	CLB1.Items.Add('�res �rt�k');
  for i:=0 to elemlista.Count-1 do begin
		if elemlista[i] <> '' then begin
			CLB1.Items.Add(elemlista[i]);
	  	end; // if
   	end; // for

	// Az �res �rt�k mindig legyen bekapcsolva alapb�l
	CLB1.Checked[1] 	:= true;
	CLB1.ItemEnabled[1]	:= kelluresszures;
	for i := 2 to CLB1.Items.Count - 1 do begin
		if  kodkilist.IndexOf(CLB1.Items[i]) > -1 then begin
			CLB1.Checked[i] := true;
		end;
	end;
	// A kezdo param�terek be�ll�t�sa
	CLB1.Columns	:= 0;
end;

procedure TValszuroDlg.FormShow(Sender: TObject);
var
	str	: string;
   li	: TStringList;
begin
	//CLB1.SetFocus;
  Edit1.SetFocus;
	if CLB1.Items.Count > 10 then begin
		ValszuroDlg.Height := 400;
	end;
	FormResize(Sender);
	// A bal felso sarok mindig a fejl�cben legyen
  	Self.Top				:= dial_top;
  	Self.Left				:= dial_left;
   // Beolvassuk az �rt�keket
   str := EgyebDlg.GetFieldStr(IntToStr(dialnr)+'#'+EgyebDlg.user_code+'#'+mezonev);
   if str > '' then begin
     	li	:= TStringList.Create;
     	StringParse(str, '#', li);
     	CLB1.Columns			:= StrToIntDef(li[0], 0);
     	Self.Width				:= StrToIntDef(li[3], 0);
     	Self.Height				:= StrToIntDef(li[4], 0);
       AdminSetting.Checked	:= StrToBool01(li[5]);
       if AdminSetting.Checked then begin
       	// Beolvassuk az adminisztr�tori be�ll�t�sokat
           str := EgyebDlg.GetFieldStr(IntToStr(dialnr)+'#'+'000'+'#'+mezonev);
           if str > '' then begin
               li	:= TStringList.Create;
               StringParse(str, '#', li);
               CLB1.Columns			:= StrToIntDef(li[0], 0);
               Self.Width				:= StrToIntDef(li[3], 0);
               Self.Height				:= StrToIntDef(li[4], 0);
           end;
       end;
       li.Free;
   end;
	MenuBeallito;
end;

procedure TValszuroDlg.FormDestroy(Sender: TObject);
begin
	// Ki�rjuk a be�ll�tott �rt�ket az ini-be, ha volt m�dos�t�s
	EgyebDlg.SetFieldStr(	IntToStr(dialnr)+'#'+EgyebDlg.user_code+'#'+mezonev,
   						IntToStr(CLB1.Columns)+'#'+IntToStr(Self.Top)+'#'+IntToStr(Self.Left)+'#'+IntToStr(Self.Width)+'#'+IntToStr(Self.Height)
                           +'#'+BoolToStr01(AdminSetting.Checked));
	kodkilist.Free;
end;

procedure TValszuroDlg.FormCreate(Sender: TObject);
begin
	kodkilist			:= TStringList.Create;
	foselected			:= false;
	kelluresszures  	:= true;
  //CLB1.MultiSelect	:= true;
end;

procedure TValszuroDlg.CLB1Click(Sender: TObject);
var
	i : integer;
begin
	if ( ( not foselected ) and  ( CLB1.Checked[0] = true ) ) then begin
		foselected	:= true;
		// Bejel�lj�k az �sszes elemet
		for i := 1 to CLB1.Items.Count - 1 do begin
			CLB1.Checked[i] := true;
		end;
	end;
	if ( ( foselected ) and  ( CLB1.Checked[0] = false ) ) then begin
		foselected	:= false;
		// Megsz�ntet�nk minden bejel�l�st
		for i := 1 to CLB1.Items.Count - 1 do begin
			CLB1.Checked[i] := false;
		end;
	end;
  Edit1.Text:='';
  Edit1.SetFocus;
end;

procedure TValszuroDlg.FormResize(Sender: TObject);
begin
	BitElkuld.Top		:= 0;
	BitElkuld.Left		:= 0;
	BitElkuld.Height	:= Panel1.Height;
	BitElkuld.Width		:= Panel1.Width div 2;
	BitKilep.Top		:= 0;
	BitKilep.Left		:= BitElkuld.Width;
	BitKilep.Height		:= Panel1.Height;
	BitKilep.Width		:= Panel1.Width - BitElkuld.Width;
end;

procedure TValszuroDlg.OszlopCsokkentClick(Sender: TObject);
begin
	if CLB1.Columns > 1 then begin
		CLB1.Columns   	:= CLB1.Columns - 1;
   end;
	MenuBeallito;
end;

procedure TValszuroDlg.OszlopNovelClick(Sender: TObject);
begin
	if CLB1.Columns = 0 then begin
   	// Egybol 2 oszlopot csin�lunk
   	CLB1.Columns := 1;
   end;
	CLB1.Columns		:= CLB1.Columns + 1;
	MenuBeallito;
end;

procedure TValszuroDlg.FormCanResize(Sender: TObject; var NewWidth,
  NewHeight: Integer; var Resize: Boolean);
begin
	if NewWidth < 200 then begin
   	Resize	:= false;
   end;
   MenuBeallito;
end;

procedure TValszuroDlg.EgyoszlopFuggClick(Sender: TObject);
begin
	CLB1.Columns	:= 0;
	MenuBeallito;
end;

procedure TValszuroDlg.EgyoszlopVizClick(Sender: TObject);
begin
	CLB1.Columns	:= 1;
	MenuBeallito;
end;

procedure TValszuroDlg.MenuBeallito;
var
	sosz	: integer;
   str		: string;
begin
	OszlopCsokkent.Enabled	:= (CLB1.Columns > 1);
   // Ellenorizz�k, hogy lehet-e t�bb oszlop
  	OszlopNovel.Enabled	:= true;
   sosz	:= CLB1.ClientHeight div CLB1.ItemHeight;
   if ( CLB1.Columns * sosz ) >= CLB1.Items.Count then begin
   	OszlopNovel.Enabled	:= false;
   end;
   // Ellenorizz�k, hogy van-e adminisztr�tori be�ll�t�s
   AdminSetting.Enabled	:= true;
   N2.Enabled				:= true;
   str := EgyebDlg.GetFieldStr(IntToStr(dialnr)+'#'+'000'+'#'+mezonev);
	if str = '' then begin
   	AdminSetting.Enabled	:= false;
   	N2.Enabled				:= false;
   end;
end;

procedure TValszuroDlg.AdminSettingClick(Sender: TObject);
var
	str	: string;
   li	: TStringList;
begin
	// �tvessz�k az adminisztr�tori be�ll�t�sokat
   str := EgyebDlg.GetFieldStr(IntToStr(dialnr)+'#'+'000'+'#'+mezonev);
   if str > '' then begin
     	li	:= TStringList.Create;
     	StringParse(str, '#', li);
     	CLB1.Columns			:= StrToIntDef(li[0], 0);
     	Self.Width				:= StrToIntDef(li[3], 0);
     	Self.Height				:= StrToIntDef(li[4], 0);
       li.Free;
   end;
	MenuBeallito;
end;

procedure TValszuroDlg.Kijells1Click(Sender: TObject);
begin
	//  Kijel�lj�k a kiv�lasztott elemeket
   Jelolgetes(true);
end;

procedure TValszuroDlg.Jelolgetes(beki : boolean);
var
	i : integer;
begin
	//  Kijel�lj�k a kiv�lasztott elemeket
 	for i := 0 to CLB1.Items.Count -1 do begin
   	if CLB1.Selected[i] then begin
       	CLB1.Checked[i]	:= beki;
       end;
   end;
end;

procedure TValszuroDlg.Kijellsmegszntetse1Click(Sender: TObject);
begin
   Jelolgetes(false);
end;

procedure TValszuroDlg.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
	Inifn			: TIniFile;
begin
	if ssAlt in Shift then begin
     	if key = VK_F3 then begin
			if NoticeKi('Val�ban el k�v�nja menteni a be�ll�t�sokat alap�rtelmezettk�nt? ', NOT_QUESTION) = 0 then begin
           	// Adminisztr�tori ment�s
				EgyebDlg.SetFieldStr(	IntToStr(dialnr)+'#'+'000'+'#'+mezonev,
					IntToStr(CLB1.Columns)+'#'+IntToStr(Self.Top)+'#'+IntToStr(Self.Left)+'#'+IntToStr(Self.Width)+'#'+IntToStr(Self.Height) );
				// Be�ll�tjuk az �rt�keket az INI-ben
               IniFn 	:= TIniFile.Create( EgyebDlg.globalini );
				IniFn.WriteString( 'FIELDS', IntToStr(dialnr)+'#'+'000'+'#'+mezonev,
					IntToStr(CLB1.Columns)+'#'+IntToStr(Self.Top)+'#'+IntToStr(Self.Left)+'#'+IntToStr(Self.Width)+'#'+IntToStr(Self.Height) );
               IniFn.Free;
       	end;
           MenuBeallito;
     	end;
    	Exit;
 	end;
end;

procedure TValszuroDlg.Timer1Timer(Sender: TObject);
var
  S : Array[0..255] of Char;
begin
  Timer1.Enabled:=False;
  if Edit1.Text='' then exit;
  StrPCopy(S, Edit1.Text);
//  with CLB1 do
  CLB1.ItemIndex :=CLB1.Perform(LB_SELECTSTRING, 0, LongInt(@S));
  if CLB1.ItemIndex> -1 then
  begin
    //Edit1.Color:=clWindow;
    ekeres:=Edit1.Text;
  end
  else
  begin        // Nem tal�lt
    //Edit1.Color:=clRed;
    //Edit1.Update;
    //Application.ProcessMessages;
    Edit1.Text:=ekeres;
    Edit1.SelStart:=length(Edit1.Text);
    Edit1.SelLength:=0;
    SendMessage(Edit1.Handle,EM_SETSEL,100,100);
    Timer1.OnTimer(self);
    Beep(400,500);
  end;

  //Edit1.Text:='';
end;

procedure TValszuroDlg.Edit1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  Timer1.Enabled:=False;
  Timer1.Enabled:=True;
end;

procedure TValszuroDlg.BitBtn5Click(Sender: TObject);
begin
  Edit1.Text:='';
end;

end.


