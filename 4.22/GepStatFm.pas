unit GepStatFm;

interface

uses
	SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
	StdCtrls, Forms, DBCtrls, DB, DBGrids, DBTables, Grids, ExtCtrls, Buttons,
	ShellApi, Dialogs, J_FOFORMUJ, ADODB, Mask, Menus, Variants, Clipbrd;

type
	TGepStatFmDlg = class(TJ_FOFORMUJDLG)
	 QueryUj1: TADOQuery;
	 QueryUj2: TADOQuery;
	 BitBtn7: TBitBtn;
	 BitBtn8: TBitBtn;
	 QueryUj3: TADOQuery;
    Timer1: TTimer;
    QueryUj4: TADOQuery;
    BitBtn10: TBitBtn;
	  procedure FormCreate(Sender: TObject);override;
	 procedure Adatlap(cim, kod : string);override;
	 procedure DBGrid2DrawColumnCell(Sender: TObject; const Rect: TRect;
	   DataCol: Integer; Column: TColumn; State: TGridDrawState);override;
    procedure BitBtn8Click(Sender: TObject);
    procedure BitBtn7Click(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure BitBtn10Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
	 private
	 public
		ret_mskod	: string;
	end;

var
	GepStatFmDlg : TGepStatFmDlg;

implementation

uses
	Egyeb, J_SQL, MegbizasBe, J_VALASZTO, Viszonybe, Kozos, Segedbe2, 
	Idobe, GKErvenyesseg;

{$R *.DFM}
const
sh : string[16] = '0123456789ABCDEF';


procedure TGepStatFmDlg.FormCreate(Sender: TObject);
var
	rszlista	: TStringLIst;
	i			: integer;
	rsz			: string;
begin
//  Application.CreateForm(TFGKErvenyesseg, FGKErvenyesseg);
	EgyebDlg.SeTADOQueryDatabase(QueryUj1);
	EgyebDlg.SeTADOQueryDatabase(QueryUj2);
	EgyebDlg.SeTADOQueryDatabase(QueryUj3);
	EgyebDlg.SeTADOQueryDatabase(QueryUj4);
	// Ellen�rizz�k, hogy minden g�pkocsi benne van-e a list�ban
	rszlista	:= TStringList.Create;
	GetRszLista(rszlista, 0);
	Query_Run(QueryUj2, 'SELECT TG_RENSZ FROM T_GEPK ');
	i := 0;
	while i < rszlista.Count do begin
		rsz	:= rszlista[i];
		if not QueryUj2.Locate('TG_RENSZ', rsz , []) then begin
			if Length(rsz) = 7 then begin
				if copy(rsz, 4, 3) <> '000' then begin
					// L�trehozzuk a g�pkocsit
					GepStatTolto(rsz);
				end;
			end;
		end;
		Inc(i);
	end;
	QueryUj2.First;
	while not QueryUj2.Eof do begin
		if rszlista.IndexOf( QueryUj2.FieldByName('TG_RENSZ').AsString ) < 0 then begin
			// T�r�lni kellene a g�pkocsit, mert m�r nem �l
			Query_Run(QueryUj3, 'DELETE FROM T_GEPK WHERE TG_RENSZ = '''+QueryUj2.FieldByName('TG_RENSZ').AsString+''' ');
		end;
		QueryUj2.Next;
	end;
	ret_mskod	:= '';
	Inherited FormCreate(Sender);
	kellujures	:= false;
	AddSzuromezoRovid('TG_TINEV', 'T_GEPK');
	AddSzuromezoRovid('TG_RENSZ', 'T_GEPK');
	AddSzuromezoRovid('TG_GKKAT', 'T_GEPK');
	AddSzuromezoRovid('TG_TORSZ', 'T_GEPK');
	AddSzuromezoRovid('TG_TENYO', 'T_GEPK');
	AddSzuromezoRovid('TG_POTSZ', 'T_GEPK');
	AddSzuromezoRovid('TG_SONEV', 'T_GEPK');
	alapstr		:= 'SELECT * FROM T_GEPK ';
	FelTolto('�j gk statisztika felvitele ', 'G�pkocsi statisztika adatok m�dos�t�sa ',
			'G�pkocsi statisztika list�ja', 'TG_RENSZ', alapstr, 'T_GEPK', QUERY_ADAT_TAG );
	ButtonUj.Hide;
	ButtonMod.Hide;
	ButtonTor.Hide;
  ButtonKuld.Hide;
	BitBtn7.Parent		:= PanelBottom;
	BitBtn8.Parent		:= PanelBottom;
 	BitBtn10.Parent		:= PanelBottom;
	BitBtn10.Visible 	:= EgyebDlg.NavC_Kapcs_OK;
	width				:= DLG_WIDTH;
	height				:= DLG_HEIGHT;
	Timer1Timer(Sender);
	rszlista.Destroy;
end;

procedure TGepStatFmDlg.Adatlap(cim, kod : string);
begin
//	Application.CreateForm(TMegbizasbeDlg, AlForm);
//	Inherited Adatlap(cim, kod );
//   if kod <> '' then begin
//   	ModLocate (Query1, 'MS_MSKOD', oldmezo1 );
//   end;
end;

procedure TGepStatFmDlg.DBGrid2DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
var
	szin	: integer;
begin
//	oldszin := DBGrid2.Canvas.Brush.Color;
	// A felrak�s/lerak�s sz�n�nek meghat�roz�sa
	if Query1.FieldByName('TG_TEIDO').AsString <> '' then begin
		szin		:= clYellow;
	end else begin
		szin		:= clWindow;
	end;
	DBGrid2.Canvas.Brush.Color		:= szin;
	DBGrid2.DefaultDrawDataCell(Rect, Column.Field, State);
	inherited DBGrid2DrawColumnCell(Sender, Rect, DataCol, Column, State);
//  	DBGrid2.Canvas.Brush.Color		:= oldszin;
end;

procedure TGepStatFmDlg.BitBtn8Click(Sender: TObject);
var
	rendsz	: string;
begin
	// Ellen�rizz�k, hogy �res soron futunk-4
	Timer1.Enabled	:= false;
	if STrToIntDef(Query1.FieldByName('TG_MSKOD').AsString, -1) <= 1 then begin
		Timer1.Enabled	:= true;
		Exit;
	end;
	// Ellen�rizz�k, hogy ki dolgozik m�g ezen a megb�z�son
	Query_Run(QueryUj3, 'SELECT * FROM MEGSEGED WHERE MS_MSKOD = '+Query1.FieldByName('TG_MSKOD').AsString);
	if QueryUj3.RecordCount = 0 then begin
		Timer1.Enabled	:= true;
		Exit;
	end;
	Query_Run(QueryUj2, 'SELECT * FROM MEGBIZAS WHERE MB_MBKOD = '+QueryUj3.FieldByName('MS_MBKOD').AsString);
	if QueryUj2.FieldByName('MB_AFKOD').AsString <> '' then begin
		if QueryUj2.FieldByName('MB_AFKOD').AsString <> EgyebDlg.user_code then begin
			NoticeKi('A megb�z�st �ppen m�s m�dos�tja!');
			Timer1.Enabled	:= true;
			Exit;
		end;
	 end;
	// A kijel�lt felrak� megjelenit�se
	rendsz	:= Query1.FieldByName('TG_RENSZ').AsString;
	Query_Update(QueryUj2, 'MEGBIZAS',
		[
		'MB_AFKOD', ''''+EgyebDlg.user_code+'''',
		'MB_AFNEV', ''''+EgyebDlg.user_name+'''',
		'MB_AFLAG', IntToStr(StrToINtDef(FormatDateTime('nnss', now),0))
		], ' WHERE MB_MBKOD = '+QueryUj3.FieldByName('MS_MBKOD').AsString );
	Application.CreateForm(TMegbizasbeDlg,MegbizasbeDlg);
	MegbizasbeDlg.Tolto('Megb�z�s m�dos�t�sa', QueryUj3.FieldByName('MS_MBKOD').AsString);
	MegbizasbeDlg.ShowModal;
	if MegbizasbeDlg.ret_kod  <> '' then begin
		GepStatTolto(rendsz);
		ModLocate (Query1, 'TG_RENSZ', rendsz );
	end;
	MegbizasbeDlg.Destroy;
	Query_Update(QueryUj3, 'MEGBIZAS',
		[
		'MB_AFKOD', ''''+''+'''',
		'MB_AFNEV', ''''+''+'''',
		'MB_AFLAG', IntToStr(StrToINtDef(FormatDateTime('nnss', now),0))
		], ' WHERE MB_AFKOD = '''+EgyebDlg.user_code+'''' );
	Timer1.Enabled	:= true;
end;

procedure TGepStatFmDlg.BitBtn7Click(Sender: TObject);
var
	rendsz	: string;
begin
	// Ellen�rizz�k, hogy �res soron futunk-4
	Timer1.Enabled	:= false;
	if STrToIntDef(Query1.FieldByName('TG_MSKOD').AsString, -1) <= 1 then begin
		Timer1.Enabled	:= true;
		Exit;
	end;
	// Ellen�rizz�k, hogy ki dolgozik m�g ezen a megb�z�son
	Query_Run(QueryUj3, 'SELECT * FROM MEGSEGED WHERE MS_MSKOD = '+Query1.FieldByName('TG_MSKOD').AsString);
	if QueryUj3.RecordCount = 0 then begin
		Timer1.Enabled	:= true;
		Exit;
	end;
	Query_Run(QueryUj2, 'SELECT * FROM MEGBIZAS WHERE MB_MBKOD = '+QueryUj3.FieldByName('MS_MBKOD').AsString);
	if QueryUj2.FieldByName('MB_AFKOD').AsString <> '' then begin
		if QueryUj2.FieldByName('MB_AFKOD').AsString <> EgyebDlg.user_code then begin
			NoticeKi('A megb�z�st �ppen m�s m�dos�tja!');
			Timer1.Enabled	:= true;
			Exit;
		end;
	 end;
	// A kijel�lt felrak� megjelenit�se
	rendsz	:= Query1.FieldByName('TG_RENSZ').AsString;
	Query_Update(QueryUj2, 'MEGBIZAS',
		[
		'MB_AFKOD', ''''+EgyebDlg.user_code+'''',
		'MB_AFNEV', ''''+EgyebDlg.user_name+'''',
		'MB_AFLAG', IntToStr(StrToINtDef(FormatDateTime('nnss', now),0))
		], ' WHERE MB_MBKOD = '+QueryUj3.FieldByName('MS_MBKOD').AsString );
	Application.CreateForm(TSegedbe2Dlg,Segedbe2Dlg);
	Segedbe2Dlg.tolt('Felrak�s/lerak�s m�dos�t�sa', QueryUj3.FieldByName('MS_MBKOD').AsString,
		QueryUj3.FieldByname('MS_SORSZ').AsString, QueryUj3.FieldByName('MS_MSKOD').AsString);
	Segedbe2Dlg.ShowModal;
	if Segedbe2Dlg.voltenter then begin
		ModLocate (Query1, 'TG_RENSZ', rendsz );
	end;
	Segedbe2Dlg.Destroy;
	Query_Update(QueryUj3, 'MEGBIZAS',
		[
		'MB_AFKOD', ''''+''+'''',
		'MB_AFNEV', ''''+''+'''',
		'MB_AFLAG', IntToStr(StrToINtDef(FormatDateTime('nnss', now),0))
		], ' WHERE MB_AFKOD = '''+EgyebDlg.user_code+'''' );
	Timer1.Enabled	:= true;
end;

function konvert(x : word;alap : integer) : string;
begin
    Result := '';
    if not(alap in [2..16]) then exit;
    if x=0 then exit;
    if (x div alap)<>0 then Result := konvert(x div alap,alap) + sh[(x mod alap)+1]
                       else Result := sh[x+1]
end;

procedure TGepStatFmDlg.Timer1Timer(Sender: TObject);
var
	str		: string;
  eszk,gypoz :integer;
begin

	// Percenk�nt ellen�rizz�k a poz�ci�t
	Timer1.Enabled	:= false;
	Query_Run(QueryUj4, 'SELECT * FROM POZICIOK ORDER BY PO_RENSZ ');
	Query_Run(QueryUj3, 'SELECT * FROM T_GEPK ');
	while not QueryUj3.Eof do begin
		if QueryUj4.Locate('PO_RENSZ', QueryUj3.FieldByName('TG_RENSZ').AsString , []) then begin
			if (	(QueryUj4.FieldByName('PO_GEKOD').AsString <> QueryUj3.FieldByName('TG_LOCAT').AsString) or
					(QueryUj4.FieldByName('PO_DATUM').AsString <> QueryUj3.FieldByName('TG_LODAT').AsString) or
					(QueryUj4.FieldByName('PO_DIGIT').AsString <> QueryUj3.FieldByName('TG_DIGIT').AsString) ) then begin
			 //	str		:= QueryUj4.FieldByName('PO_DIGIT').AsString;
        str:= konvert(QueryUj4.FieldByName('PO_DIGIT2').Value,2);
        str:=stringofchar('0',8-length(str))+str;

        eszk:=QueryUj4.FieldByName('PO_ETIPUS').Value;
        if eszk=0 then
          gypoz:=1
        else
          gypoz:=5;

        if copy(str, gypoz, 1) = '0' then
			  	str	:= 'GYUJT�S BE'
        else
			  	str	:= 'GYUJT�S KI';
        if QueryUj4.FieldByName('PO_VALID').Value=0 then
			  	str	:= 'HIB�S ADAT';

{
				if copy(str, 5, 1) = '0' then begin
					str	:= 'GYUJT�S BE';
				end else begin
					str	:= 'GYUJT�S KI';
				end;        }
				if not Query_Update( QueryUj2 , 'T_GEPK',
				[
				'TG_LOCAT', ''''+copy(StringReplace(QueryUj4.FieldByName('PO_GEKOD').AsString,'''','',[rfReplaceAll	]),1,60)+'''',
				'TG_DIGIT', ''''+str+'''',
				'TG_SEBES', IntToStr(StrToIntdef(QueryUj4.FieldByName('PO_SEBES').AsString,0)),
				'TG_LODAT', ''''+QueryUj4.FieldByName('PO_DATUM').AsString+''''
				], ' WHERE TG_RENSZ = '''+QueryUj3.FieldByName('TG_RENSZ').AsString+''' ' ) then
        begin
        //  Clipboard.Clear;
        //  Clipboard.AsText:=QueryUj2.SQL.Text;
        end;
			end;
		end
    else
    begin
			if (QueryUj3.FieldByName('TG_LODAT').AsString <>'') then
      begin
				if not Query_Update( QueryUj2 , 'T_GEPK',
				[
				'TG_LOCAT','''''',
				'TG_DIGIT','''''',
				'TG_SEBES','''''',
				'TG_LODAT',''''''
				], ' WHERE TG_RENSZ = '''+QueryUj3.FieldByName('TG_RENSZ').AsString+''' ' ) then
        begin
        //  Clipboard.Clear;
       //   Clipboard.AsText:=QueryUj3.SQL.Text;
        end;
      end;
    end;
		QueryUj3.Next;
	end;
	Timer1.Enabled	:= true;

end;

procedure TGepStatFmDlg.FormShow(Sender: TObject);
begin
	// Vegy�k ki a rossz rendsz�mokat
	Query_Run(QueryUj3, 'SELECT TG_RENSZ FROM T_GEPK, MEGSEGED WHERE TG_MSKOD = MS_MSKOD AND TG_RENSZ <> MS_RENSZ' );
	if QueryUj3.RecordCount > 0 then begin
		Screen.Cursor	:= crHourGlass;
		while not QueryUj3.Eof do begin
			GepStatTolto(QueryUj3.FieldByName('TG_RENSZ').AsString);
			QueryUj3.Next;
		end;
		Query1.Close;
		Query1.Open;
		Screen.Cursor	:= crDefault;
	end;
end;

procedure TGepStatFmDlg.BitBtn10Click(Sender: TObject);
begin
 if Query1.FieldByName('TG_RENSZ').AsString<>'' then
  EgyebDlg.GoogleMap(Query1.FieldByName('TG_RENSZ').AsString,'','','',False)     ;



end;

procedure TGepStatFmDlg.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
//  FGKErvenyesseg.Free;
end;

end.








