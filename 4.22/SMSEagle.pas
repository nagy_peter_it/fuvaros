unit SMSEagle;

interface

uses
	SysUtils, Graphics, Mask, IniFiles, Forms, Classes, StdCtrls, Grids, DateUtils, Shellapi,
	Comobj, Variants , Comctrls, WinTypes,  printers, Controls,buttons, Notice, KozosTipusok,
  System.UITypes, Math, Kozos;

  function QuerySMSEagleOutbox: TEagleOutboxData;
  function GetLegregebbi(const http_output: string): string;

implementation

uses
    Egyeb, HTTPRutinok;

function QuerySMSEagleOutbox: TEagleOutboxData;
const
  CRLF = chr(13)+chr(10);
  LF = chr(10);
  TAB = chr(9);
  Minta_NotFound = 'Host not found';   // 'Hiba a HTTP lek�rdez�sben: Socket Error # 11001'#$D#$A'Host not found.'
  Minta_Empty = 'No data to display';
var
   // str, json_output: string;
   UTF8Url, http_output, Cim: string;
   str, resultstring: string;
begin
   str:='http://'+EgyebDlg.SMSEAGLE_IP+'/index.php/http_api/read_sms?';
   str:=str+'login='+EgyebDlg.SMSEAGLE_USER+'&pass='+EgyebDlg.SMSEAGLE_PASSWORD;
   str:=str+'&folder=outbox';
   // str:=str+'&folder=inbox';  // teszthez...
   http_output:= HttpGet(str);
   // NoticeKi(http_output);

   if Pos(Minta_NotFound, http_output) > 0  then
     Result.elerheto:= False  // nem el�rhet� az eszk�z
   else Result.elerheto:= True;

   if Pos(Minta_Empty, http_output) > 0  then begin
     Result.darabszam:= 0;
     Result.lista:= '';
     Result.legregebbi:='';
     end
   // else if copy(http_output, 1, length(FejlecMinta)) = FejlecMinta then begin   // nem l�ttunk m�g Outbox fejl�cet...
   else begin
      Result.lista:= http_output;
      Result.darabszam:= CharSzamol(http_output, LF) -1;  // minden sor v�g�n #0A, a fejl�cn�l is
      if Result.darabszam >= 1 then begin
         Result.legregebbi:= GetLegregebbi(http_output);
         end;
      end;
end;

function GetLegregebbi(const http_output: string): string;
// Bemenet form�tum:
// UpdatedInDB;InsertIntoDB;SendingDateTime;SendBefore;SendAfter;Text;DestinationNumber;Coding;UDH;Class;TextDecoded;ID;MultiPart;RelativeValidity;SenderID;SendingTimeOut;DeliveryReport;CreatorID;Retries;Priority
// "2017-07-13 16:32:48";"2017-07-13 16:30:58";"2017-07-13 16:30:58";"23:59:59";"00:00:00";"";"+36209588515";"Default_No_Compression";"";"-1";"Sziasztok! Nincs ??gyelet! Ma ?�s holnap 16:30-ig vagyunk az irod??ban!";"35372";"f";"-1";"smseagle1";"2017-07-13 16:33:48";"default";"admin";"0";""
// "2017-07-13 16:30:59";"2017-07-13 16:30:59";"2017-07-13 16:30:58";"23:59:59";"00:00:00";"";"+36209588525";"Default_No_Compression";"";"-1";"Sziasztok! Nincs ??gyelet! Ma ?�s holnap 16:30-ig vagyunk az irod??ban!";"35373";"f";"-1";"smseagle1";"2017-07-13 16:30:59";"default";"admin";"0";""
const
   MaxDatum = '2999-01-01 00:00:00';  // enn�l kisebb lesz
var
   S, Sor, DatumElem, LegregebbiDatum: string;
begin
   LegregebbiDatum:= MaxDatum;
   S:= http_output;
   while S <> '' do begin
     Sor:= EgyebDlg.SepFronttoken(const_LF, S);
     S:= EgyebDlg.SepRest(const_LF, S);
     DatumElem:= EgyebDlg.GetSepListItem(';', Sor, 2);
     DatumElem:= StringReplace( DatumElem,'"','',[rfReplaceAll]) ; //  2017-07-13 16:30:59  (vagy 'InsertIntoDB')
     if JoEgesz(copy(DatumElem,1,4)) then
       if DatumElem < LegregebbiDatum then
          LegregebbiDatum:= DatumElem;
     end;  // while
   try
     LegregebbiDatum:= StringReplace( LegregebbiDatum,'-','.',[rfReplaceAll]) ;  // 2017.07.13 16:30:59
     LegregebbiDatum:= copy(LegregebbiDatum, 1, 10)+'. '+ copy(LegregebbiDatum, 12, 5);
   except
     LegregebbiDatum:= '';
     end;  // try-except
   if LegregebbiDatum <> MaxDatum then
     Result:= LegregebbiDatum
   else Result:= '';
end;

{function QuerySMSEagleOutboxVarakozik: boolean;
const
  CRLF = chr(13)+chr(10);
  TAB = chr(9);
  LF = chr(10);
var
   // str, json_output: string;
   UTF8Url, http_output, Cim: string;
   str, resultstring: string;
begin
   // http://192.168.1.235/index.php/http_api/get_queue_length?login=admin&pass=password

   // str:='http://'+EgyebDlg.SMSEAGLE_IP+'/index.php/http_api/get_queue_length?'; // ez valami�rt nem m�k�dik...
   // str:='http://'+EgyebDlg.SMSEAGLE_IP+'/index.php/http_api/get_inbox_length?';
   str:='http://'+EgyebDlg.SMSEAGLE_IP+'/index.php/http_api/read_sms?';
   str:=str+'login='+EgyebDlg.SMSEAGLE_USER+'&pass='+EgyebDlg.SMSEAGLE_PASSWORD;
   str:=str+'&folder=outbox';
   http_output:= HttpGet(str);
   // NoticeKi(http_output);
   if http_output <> 'No data to display' then
     Result:= True
   else Result:= False;
end;}

end.
