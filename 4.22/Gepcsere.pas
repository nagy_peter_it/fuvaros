unit GEPCSERE;

interface

uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls, Egyeb,
  Forms, Dialogs, DBTables, DB, StdCtrls, Buttons, Mask, DBCtrls,
  Printers, ExtCtrls;

type
  TGepcsereDlg = class(TForm)
    BitBtn6: TBitBtn;
    Label3: TLabel;
    Label4: TLabel;
    M1: TMaskEdit;
    BitBtn5: TBitBtn;
    M01: TMaskEdit;
    OkButton: TBitBtn;
    procedure BitBtn6Click(Sender: TObject);
    procedure BitBtn5Click(Sender: TObject);
    procedure OkButtonClick(Sender: TObject);
  private
  public
  end;

var
  GepcsereDlg: TGepcsereDlg;

implementation

uses
	J_Valaszto, J_SQL, Kozos, Windows;
{$R *.DFM}

procedure TGepcsereDlg.BitBtn6Click(Sender: TObject);
begin
	Close;
end;

procedure TGepcsereDlg.BitBtn5Click(Sender: TObject);
begin
	GepkocsiValaszto(M1);
end;

procedure TGepcsereDlg.OkButtonClick(Sender: TObject);
begin
	if M1.Text = '' then begin
	NoticeKi('A r�gi rendsz�m mez� �res!');
     M1.SetFocus;
     Exit;
  end;

	if M01.Text = '' then begin
	NoticeKi('Az �j rendsz�m mez� �res!');
     M01.SetFocus;
     Exit;
  end;

  if NoticeKi('Val�ban kicser�li a megadott rendsz�mot? ', NOT_QUESTION) <> 0 then begin
  	Exit;
  end;

 Try

  // A rendsz�mcsere elv�gz�se
  if not Query_Run (EgyebDlg.QueryAlap, 'UPDATE JARAT SET JA_RENDSZ = '''+M01.Text+''' '+
	' WHERE JA_RENDSZ = '''+M1.Text+''' ',true) then Raise Exception.Create('');
  if not Query_Run (EgyebDlg.QueryAlap, 'UPDATE MEGBIZAS SET MB_RENSZ = '''+M01.Text+''' '+
	' WHERE MB_RENSZ = '''+M1.Text+''' ',true) then  Raise Exception.Create('');
  if not Query_Run (EgyebDlg.QueryAlap, 'UPDATE MEGSEGED SET MS_RENSZ = '''+M01.Text+''' '+
	' WHERE MS_RENSZ = '''+M1.Text+''' ',true) then  Raise Exception.Create('');
  if not Query_Run (EgyebDlg.QueryAlap, 'UPDATE JARPOTOS SET JP_POREN = '''+M01.Text+''' '+
  	' WHERE JP_POREN = '''+M1.Text+''' ',true) then Raise Exception.Create('');
  if not Query_Run (EgyebDlg.QueryAlap, 'UPDATE KOLTSEG SET KS_RENDSZ = '''+M01.Text+''' '+
  	' WHERE KS_RENDSZ = '''+M1.Text+''' ',true) then Raise Exception.Create('');
  if not Query_Run (EgyebDlg.QueryAlap, 'UPDATE SULYOK SET SU_RENDSZ = '''+M01.Text+''' '+
  	' WHERE SU_RENDSZ = '''+M1.Text+''' ',true) then Raise Exception.Create('');
  if not Query_Run (EgyebDlg.QueryAlap, 'UPDATE T_KOLT SET KS_RENDSZ = '''+M01.Text+''' '+
  	' WHERE KS_RENDSZ = '''+M1.Text+''' ',true) then Raise Exception.Create('');
  if not Query_Run (EgyebDlg.QueryAlap, 'UPDATE ZAROKM SET ZK_RENSZ = '''+M01.Text+''' '+
  	' WHERE ZK_RENSZ = '''+M1.Text+''' ',true) then  Raise Exception.Create('');
  if not Query_Run (EgyebDlg.QueryKozos, 'UPDATE DOLGOZO SET DO_RENDSZ = '''+M01.Text+''' '+
  	' WHERE DO_RENDSZ = '''+M1.Text+''' ',true) then Raise Exception.Create('');
  if not Query_Run (EgyebDlg.QueryKozos, 'UPDATE GARANCIA SET GA_RENSZ = '''+M01.Text+''' '+
  	' WHERE GA_RENSZ = '''+M1.Text+''' ',true) then  Raise Exception.Create('');
  if not Query_Run (EgyebDlg.QueryKozos, 'UPDATE GEPKOCSI SET GK_RESZ = '''+M01.Text+''' '+
  	' WHERE GK_RESZ = '''+M1.Text+''' ',true) then   Raise Exception.Create('');
  if not Query_Run (EgyebDlg.QueryKozos, 'UPDATE LIZING SET LZ_RENSZ = '''+M01.Text+''' '+
	' WHERE LZ_RENSZ = '''+M1.Text+''' ',true) then    Raise Exception.Create('');

  NoticeKi('A rendsz�m cser�je megt�rt�nt.');
 Except
  NoticeKi('Hiba t�rt�nt az adatok t�rol�s�n�l!');
 End;
end;

end.


