unit TankImport;

interface

uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, DBTables, DB, StdCtrls, Buttons, Mask, DBCtrls,
  ExtCtrls, ADODB, FileCtrl, StrUtils, Grids,clipbrd;

type
  TTankimportDlg = class(TForm)
    BitBtn6: TBitBtn;
    Label4: TLabel;
    M1: TMaskEdit;
    BitBtn5: TBitBtn;
    BtnExport: TBitBtn;
    Query1: TADOQuery;
    Query2: TADOQuery;
    Label1: TLabel;
    OpenDialog1: TOpenDialog;
    SG1: TStringGrid;
	 BitBtn1: TBitBtn;
    procedure FormCreate(Sender: TObject);
    procedure BitBtn6Click(Sender: TObject);
    procedure BitBtn5Click(Sender: TObject);
    procedure BtnExportClick(Sender: TObject);
    procedure Feldolgozas;
	 procedure BitBtn1Click(Sender: TObject);
  private
	  kilepes		: boolean;
  public
  end;

var
  TankimportDlg: TTankimportDlg;

implementation

uses
	Egyeb, Kozos, J_SQL;
{$R *.DFM}

procedure TTankimportDlg.FormCreate(Sender: TObject);
begin
 	M1.Text 		:= '';
	EgyebDlg.SetADOQueryDatabase(Query1);
  	EgyebDlg.SetADOQueryDatabase(Query2);
   Label1.Caption	:= '';
   Label1.Update;
end;

procedure TTankimportDlg.BitBtn6Click(Sender: TObject);
begin
	if not kilepes then begin
		kilepes:= true;
		BitBtn1.Show;
	  end
  else begin
		Close;
	  end;
end;

procedure TTankimportDlg.BitBtn5Click(Sender: TObject);
begin
	// Az �llom�ny kiv�laszt�sa
	OpenDialog1.Filter		:= 'Sz�veges �llom�nyok (*.txt)|*.TXT';
	OpenDialog1.DefaultExt  := 'TXT';
	OpenDialog1.InitialDir	:= EgyebDlg.DocumentPath;
	if OpenDialog1.Execute then begin
		M1.Text := OpenDialog1.FileName;
	end;
end;

procedure TTankimportDlg.BtnExportClick(Sender: TObject);
begin
	Screen.Cursor	:= crHourGlass;
	BtnExport.Hide;
	BitBtn1.Hide;
	Feldolgozas;
	Screen.Cursor	:= crDefault;
	Close;
end;

procedure TTankimportDlg.Feldolgozas;
var
	fname   	: string;
	F			: TextFile;
	sor			: string;
	mezolista	: TStringList;
	i			: integer;
	datum		: string;
	idopont		: string;
	rendszam	: string;
	kmora		: integer;
	menny		: string;
	egyar		: string;
	jarat		: string;
	jaratszam	: string;
	eredm		: string;
	ujkodszam	: integer;
	folytat		: boolean;
	megj_str	: string;
	tipus		: integer;
	jolista		: TStringList;
	tkezd		: TDateTime;
	tvege		: TDateTime;
	timport		: TDateTime;
  jaratkod, ktgnev  :string;
begin
	kilepes	:= false;
	if M1.Text	= '' then begin
		NoticeKi('A bemeneti �lom�ny nem lehet �res!');
		M1.SetFocus;
		Exit;
	end;
	if not FileExists(M1.Text) then begin
		NoticeKi('Nem l�tez� �llom�ny!');
		M1.SetFocus;
		Exit;
	end;
	fname		:= m1.Text;
	megj_str	:= copy('Tankol�si import : ' + ExtractFileName(fname), 1, 80);
	// Az adatok import�l�s�nak megkezd�se
	EllenlistTorol;
	EllenListAppend('Tankol�si adatok feldolgoz�sa:', false);
	EllenListAppend('******************************', false);
	EllenListAppend('', false);
	EllenListAppend('�llom�ny : '+fname, false);
	EllenListAppend('D�tum    : '+EgyebDlg.MaiDatum , false);
	EllenListAppend('', false);
	EllenListAppend('A hib�s sorok : ', false);
	mezolista	:= TStringList.Create;
	jolista		:= TStringList.Create;
	AssignFile(F, fname);
	Reset(F);
	i	:= 0;
//	voltszam		:= StrToIntDef(EgyebDlg.Read_SZGrid( '888', '110' ),0);
	SG1.RowCount	:= 1;
	SG1.Rows[0].Clear;
	while ( ( not Eof(F) ) and (not kilepes) ) do begin
		Application.ProcessMessages;
		ReadLn(F, sor);
		Label1.Caption	:= IntToStr(i)+'. sor: '+sor;
		Label1.Update;
		StringParse(sor, ';', mezolista);
		SG1.RowCount  	:= i+1;
		SG1.Cells[0,i] 	:= IntToStr(StrToIntDef(mezolista[2],0));   // Sorsz�m
		SG1.Cells[1,i] 	:= '';                              		// J�rat
		SG1.Cells[2,i] 	:= mezolista[0];                    		// D�tum
		SG1.Cells[3,i] 	:= Trim(mezolista[3]);                     	// Rendsz�m
		if Length(Trim(mezolista[3])) = 6 then begin
			SG1.Cells[3,i]	:= copy(Trim(mezolista[3]), 1, 3)+'-'+copy(Trim(mezolista[3]), 4, 3);
		end;
		SG1.Cells[4,i] 	:= IntToStr(StrToIntDef(mezolista[4],0));   // Km �ra
		SG1.Cells[5,i] 	:= mezolista[8];                    		// Mennyis�g
		SG1.Cells[6,i] 	:= mezolista[9];                    		// Egys�g�r
		SG1.Cells[7,i] 	:= mezolista[7];                    		// T�pus : Diesel; Adblue
		SG1.Cells[8,i] 	:= sor;                                     // A teljes sor
		SG1.Cells[9,i] 	:= mezolista[1];                    		// Id�pont
		SG1.Cells[10,i] 	:= '1';                    		// Teletank
		Inc(i);
	end;
	mezolista.Free;
	CloseFile(f);
	if kilepes then begin
		Exit;
	end;
	Label1.Caption	:= '';
	Label1.Update;
	// Export�ljuk ki a t�bl�zatot
//	GridExportToFile (SG1, EgyebDlg.DocumentPath + 'tablazat1.xls');
	// Rendezz�k a t�bl�zatot rsz, kmora, datum-ra
	TablaRendezo( SG1, [2,3,4, 7], [0,0,1,0], 0, true );
//	GridExportToFile (SG1, EgyebDlg.DocumentPath + 'tablazat2.xls');
	// �sszeadjuk az egyform�kat
	for i := 0 to SG1.RowCount - 2 do begin
		if ( ( SG1.Cells[2,i] = SG1.Cells[2,i+1] ) and ( SG1.Cells[3,i] = SG1.Cells[3,i+1] )  and
			 ( SG1.Cells[4,i] = SG1.Cells[4,i+1] )  and ( SG1.Cells[7,i] = SG1.Cells[7,i+1] ) ) then begin
			// �sszeadjuk
			SG1.Cells[5,i+1]	:= Format('%.2f', [StringSzam(SG1.Cells[5,i]) + StringSzam(SG1.Cells[5,i+1]) ]);
			SG1.Cells[8,i+1]	:= SG1.Cells[8,i+1] + Format(' +(%.2f) ', [StringSzam(SG1.Cells[5,i])]);
			// Ki�r�tj�k az aktu�lisat
			SG1.Rows[i].Clear;
		end;
	end;
	// Rendezz�k sorsz�m szerint
	TablaRendezo( SG1, [0], [1], 0, true );
//	GridExportToFile (SG1, EgyebDlg.DocumentPath + 'tablazat3.xls');
	i := 0;
	while ( ( i <= SG1.RowCount - 1 ) and (not kilepes) ) do begin
		Application.ProcessMessages;
		Label1.Caption	:= IntToStr(i)+' / '+IntToStr(SG1.RowCount - 1)+'  sor feldolgoz�sa ';
		Label1.Update;
		// A sor feldolgoz�sa
		if  SG1.Cells[0,i] <> '' then begin	// A sor nem �res
			eredm		:= 'Feldolgozatlan sor!!!';
			datum		:= SG1.Cells[2,i];
			idopont		:= SG1.Cells[9,i];
//			sorszam		:= StrToIntDef(SG1.Cells[0,i],0);
			rendszam	:= Trim(SG1.Cells[3,i]);
			kmora		:= StrToIntDef(SG1.Cells[4,i],0);
			menny		:= SG1.Cells[5,i];
			egyar		:= SG1.Cells[6,i];
			// AZ egys�g�rat a telepi �zemanyag �rakb�l kell venni!
			tipus	:= -1;
			if UpperCase(SG1.Cells[7,i]) = 'ADBLUE' then begin
				egyar		:= Format('%.2f',[GetTelepiAr(datum,1)]);
				tipus		:= 2;
        ktgnev:='AD Blue';
			end;
			if UpperCase(SG1.Cells[7,i]) = 'DIESEL' then begin
				egyar		:= Format('%.2f',[GetTelepiAr(datum,0)]);
				tipus		:= 0;
        ktgnev:='�zemanyag k�lts�g';
			end;
			folytat		:= true;
			// A sor �rv�nyess�g�nek meg�llap�t�sa
			jarat	:= '00';
      jaratkod:='';
			if Jodatum(datum) then begin
				if rendszam <> '' then begin
					// A km �ra korrig�l�sa a hozz�adott km -el
					kmora	:= kmora + StrToIntDef(Query_Select('GEPKOCSI', 'GK_RESZ', rendszam, 'GK_HOZZA'), 0);
					// A j�rat meghat�roz�sa
					Query_Run(Query2, 'SELECT * FROM JARAT WHERE JA_RENDSZ = '''+rendszam+''' '+
						' AND JA_JKEZD <= '''+datum+''' AND JA_JVEGE >= '''+datum+''' ');
					jarat	:= '';
					while not Query2.Eof do begin
						tkezd	:= GetDateFromStr(Query2.FieldByName('JA_JKEZD').AsString, Query2.FieldByName('JA_KEIDO').AsString);
						tvege	:= GetDateFromStr(Query2.FieldByName('JA_JVEGE').AsString, Query2.FieldByName('JA_VEIDO').AsString);
						timport	:= GetDateFromStr(datum, idopont);


(*
						EllenListAppend('     A d�tumok : '+FormatDateTime('yyyy.mm.dd hh:nn:ss.zzz',tkezd)+' -> '+
							FormatDateTime('yyyy.mm.dd hh:nn:ss.zzz',timport)+ ' -> '+
							FormatDateTime('yyyy.mm.dd hh:nn:ss.zzz',tvege)+
							'  A j�rat : '+GetJaratszamFromDb(Query2), false);
*)
						if ((tkezd <= timport) and (tvege >= timport) ) then begin
							// Hasonl�tsuk �ssze a kmora �ll�st is, hiba�zenet, ha elt�r�s van!!!
							if ( ( StrToIntDef(Query2.FieldByName('JA_NYITKM').AsString, 0) > kmora ) or
								 ( StrToIntDef(Query2.FieldByName('JA_ZAROKM').AsString, 0) < kmora ) ) then begin
								eredm := '�tk�z�s a d�tum �s a km k�z�tt a '+GetJaratszamFromDb(Query2)+' j�rattal!'	;
								folytat	:= false;
							end;
							jarat		:= Query2.FieldByName('JA_KOD').AsString;
							jaratszam	:= GetJaratszamFromDb(Query2);
  			      jaratkod:= Query2.FieldByName('JA_ORSZ').AsString+'-'+Format('%5.5d',[Query2.FieldByName('JA_ALKOD').AsInteger]);
							Query2.Last;
						end;
						Query2.Next;
					end;
				end else begin
					eredm 	:= 'Hi�nyz� rendsz�m!';
					folytat	:= false;
				end;
			end else begin
				eredm 	:= 'Hib�s d�tum megad�sa : '+datum;
				folytat	:= false;
			end;
			// Ellen�rizz�k a j�ratot
			if jarat = '' then begin
				eredm 	:= 'Hi�nyz� j�rat : '+rendszam + ' - ' + datum + ' ' + idopont + '('+IntToStr(kmora)+')';
				folytat	:= false;
			end;
			// Ellen�rizz�k az id�pontot
			if not Joido(idopont) then begin
				eredm 	:= 'Hib�s id�pont : '+idopont;
				folytat	:= false;
			end;
			// Ellen�rizz�k a rendsz�mot
			Query_Run(Query1, 'SELECT * FROM GEPKOCSI WHERE GK_RESZ = '''+rendszam+''' ');
			if Query1.RecordCount < 1 then begin
				eredm 	:= 'Hib�s rendsz�m : '+rendszam;
				folytat	:= false;
			end;
			// Ellen�rizz�k a t�pust
			if tipus < 0 then begin
				eredm 	:= '�rv�nytelen t�pus : '+SG1.Cells[7,i];
				folytat	:= false;
			end;
			// Ellen�rizz�k a mennyis�get
			if StringSzam(SG1.Cells[5,i]) = 0 then begin
				eredm 	:= '�rv�nytelen mennyis�g : '+SG1.Cells[5,i];
				folytat	:= false;
			end;
			if folytat then begin
				// Ellen�rizz�k a k�lts�g megl�t�t
				Query_Run(Query2, 'SELECT * FROM KOLTSEG WHERE KS_RENDSZ = '''+rendszam+''' '+
					' AND KS_DATUM  = '''+datum+''' '+
					' AND KS_KMORA  = '+IntToStr(kmora)+
//					' AND KS_UZMENY = '+SqlSzamString(StringSzam(menny),12,2)+
					' AND KS_TIPUS  = '+IntToStr(tipus) );
				if Query2.RecordCount > 0 then begin
					eredm := 'M�r l�tez� k�lts�g rekord!'
				end else begin
					ujkodszam	:= GetNextCode('KOLTSEG', 'KS_KTKOD', 1, 0);
					if not Query_Insert (Query2, 'KOLTSEG',
						[
						'KS_KTKOD',     IntToStr(ujkodszam),
						'KS_JAKOD', 	''''+jarat+'''',
						'KS_DATUM', 	''''+ datum +'''',
						'KS_IDO', 	''''+ idopont +'''',
						'KS_RENDSZ', 	''''+ rendszam +'''',
						'KS_KMORA', 	IntToStr(kmora),
						'KS_UZMENY', 	SqlSzamString(StringSzam(menny),12,2),
					//	'KS_LEIRAS', 	''''+ '�zemanyag k�lts�g' +'''',
						'KS_LEIRAS', 	''''+ ktgnev +'''',
						'KS_VALNEM', 	''''+'HUF'+'''',
						'KS_ARFOLY',	'1',
						'KS_ORSZA',	    '''HU''',
						'KS_OSSZEG',	SqlSzamString(StringSzam(menny)*StringSzam(egyar),12,2),
						'KS_ERTEK',		SqlSzamString(StringSzam(menny)*StringSzam(egyar),12,2),
						'KS_AFASZ',		'0',
						'KS_AFAERT',	'0',
						'KS_AFAKOD',	''''+'103'+'''',
						'KS_FIMOD',		''''+'Telep'+'''',
						'KS_ATLAG',		'0',
						'KS_THELY',     ''''+''+'''',
						'KS_TELE',		'1',
						'KS_TELES',		''''+' N '+'''',
						'KS_TETAN',		'0',
						'KS_TIPUS', 	IntToStr(tipus),
						'KS_MEGJ',		''''+megj_str +'''',
						'KS_JARAT',		''''+jaratkod+''''
						] ) then begin
            //Clipboard.Clear;
            //Clipboard.AsText:=Query2.SQL.Text;
						eredm 	:= 'Nem siker�lt a rekord l�trehoz�sa!';
					end else begin
//						eredm := '�j k�lts�g rekord j�tt l�tre!'
						jolista.Add(SG1.Cells[0,i]+'. '+SG1.Cells[8,i]+'  -> '+jaratszam);
						eredm	:= '';
					end;
				end;
			end;
			if eredm <> '' then begin
				EllenListAppend(SG1.Cells[0,i]+'. '+SG1.Cells[8,i], false);
				EllenListAppend('     '+eredm, false);
			end;
		end;
		Inc(i);
	end;
	Label1.Caption	:= '';
	Label1.Update;
	// A ber�gz�tett sorok felvitele a list�ba
	EllenListAppend('', false);
	if jolista.Count = 0 then begin
		EllenListAppend('FIGYELEM! NINCSENEK FELDOLGOZOTT SOROK!!! ', false);
	end else begin
		EllenListAppend('A feldolgozott sorok : ', false);
		for i := 0 to jolista.Count - 1 do begin
			EllenListAppend('     '+jolista[i], false);
		end;
	end;
	jolista.Free;
	EllenlistMutat('Tankol�si lista import�l�s�nak eredm�nye ', false);
	kilepes			:= true;
end;

procedure TTankimportDlg.BitBtn1Click(Sender: TObject);
var
	megj_str	: string;
begin
	if M1.Text	= '' then begin
		NoticeKi('A bemeneti �lom�ny nem lehet �res!');
		M1.SetFocus;
		Exit;
	end;
	if NoticeKi('Val�ban t�r�lni akarja az �llom�nyb�l beolvasott k�lts�geket?', NOT_QUESTION) <> 0 then begin
		Exit;
	end;
	if NoticeKi('Figyelem! Az �llom�nyhoz tartoz� k�lts�gek t�rl�sre ker�lnek! Folytatja?', NOT_QUESTION) <> 0 then begin
		Exit;
	end;
	// T�r�lj�k le az adatokat
	megj_str	:= copy('Tankol�si import : ' + ExtractFileName(M1.Text), 1, 80);
	Query_Run(Query2, 'SELECT * FROM KOLTSEG WHERE KS_MEGJ = '''+megj_str+''' ');
	if Query2.RecordCount < 1 then begin
		NoticeKi('Nincs megfelel� k�lts�g rekord!');
		Exit;
	end else begin
		if NoticeKi('Figyelem! '+IntToStr(Query2.RecordCount)+' db rekord t�rl�sre ker�l! Folytatja?', NOT_QUESTION) <> 0 then begin
			Exit;
		end;
		Screen.Cursor	:= crHourGlass;
		BitBtn1.Hide;
		BtnExport.Hide;
		BitBtn6.Hide;
		Query_Run(Query2, 'DELETE FROM KOLTSEG WHERE KS_MEGJ = '''+megj_str+''' ');
		BitBtn1.Show;
		BtnExport.Show;
		BitBtn6.Show;
		Screen.Cursor	:= crDefault;
		NoticeKi('A t�rl�s megt�rt�nt!');
	end;
	kilepes	:= true;
end;

end.




