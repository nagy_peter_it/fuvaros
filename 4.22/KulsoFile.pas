unit KulsoFile;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Buttons;

type
  TKulsoFileDlg = class(TForm)
    cbKulsoFile: TComboBox;
    Label1: TLabel;
    OKBtn: TBitBtn;
    CancelBtn: TBitBtn;
    OpenDialog1: TOpenDialog;
    procedure FormCreate(Sender: TObject);
    procedure OKBtnClick(Sender: TObject);
    procedure ComboTolto;
    procedure Megnyit(i_FOKOD, i_ALKOD: string; i_DIALG: integer);
  private
    list_FOKOD, list_ALKOD: TStringList;
    list_DIALG: TStringList;
  public
    { Public declarations }
  end;

var
  KulsoFileDlg: TKulsoFileDlg;

implementation

uses Egyeb, Kozos, ShellAPI;

{$R *.dfm}

procedure TKulsoFileDlg.FormCreate(Sender: TObject);
begin
    list_FOKOD:= TStringList.Create;
    list_ALKOD:= TStringList.Create;
    list_DIALG:= TStringList.Create;
    ComboTolto;
end;


procedure TKulsoFileDlg.ComboTolto;
var
	i: integer;
  s: string;
begin
  cbKulsoFile.Clear;
  list_FOKOD.Clear;
  list_ALKOD.Clear;
  list_DIALG.Clear;
  with EgyebDLg.QueryKozos do begin
     SQL.Clear;
     S:='select FM_LABEL, FM_FOKOD, FM_ALKOD, FM_DIALG from FILEMUTAT order by 1';
     SQL.Add(s);
     Open;
     while not eof do begin
         cbKulsoFile.Items.Add(Fields[0].AsString);
         list_FOKOD.Add(Fields[1].AsString);
         list_ALKOD.Add(Fields[2].AsString);
         list_DIALG.Add(Fields[3].AsString);
         Next;
         end;  // while
     end;  // with
end;


procedure TKulsoFileDlg.OKBtnClick(Sender: TObject);
var
   i, DIALG: integer;
   FOKOD, ALKOD: string;
begin
   i:=cbKulsoFile.ItemIndex;
   FOKOD:=list_FOKOD[i];
   ALKOD:=list_ALKOD[i];
   DIALG:=StrToInt(list_DIALG[i]);
   Megnyit(FOKOD, ALKOD, DIALG);
end;
procedure TKulsoFileDlg.Megnyit(i_FOKOD, i_ALKOD: string; i_DIALG: integer);
var
  path, fn: string;

begin
  // jogosults�g ellen�rz�s: az eg�sz k�nytr�rra vonatkoz� megtekint�shez er�s jog kell
  if (i_DIALG=1) and (not(EgyebDlg.user_super)) then begin
      NoticeKi('A megtekint�shez "Teljes" jog sz�ks�ges!', NOT_ERROR);
      exit;  // procedure
      end;  // if
  if i_DIALG=1 then begin // dial�gus kell
    path:= EgyebDlg.Read_SZGrid(i_FOKOD, i_ALKOD);
    if not DirectoryExists(path) then begin
      NoticeKi('A megadott k�nyvt�r ('+path+') nem l�tezik!', NOT_ERROR);
      exit;  // procedure
      end;
    OpenDialog1.InitialDir:=path;
    if OpenDialog1.Execute then begin
      fn:=OpenDialog1.FileName;
      end;  // if
    end // if dial�gus
  else begin  // nem kell dial�gus
    fn:= EgyebDlg.Read_SZGrid(i_FOKOD, i_ALKOD);
    end;  // else
  if (fn='') or (not FileExists(fn)) then begin
    NoticeKi('Nem l�tez� �llom�ny: '+fn+'!', NOT_ERROR);
    exit;
    end;  // if
  ShellExecute(Handle, 'open', PChar(fn), nil, PChar(fn), SW_SHOWMAXIMIZED);  // SW_SHOWMAXIMIZED  // SW_SHOWDEFAULT
end;

end.


