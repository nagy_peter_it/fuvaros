unit J_OUTLOOK;

interface

uses
	Classes, ComObj, Variants, SysUtils, Forms;

type

  	TOutlookItem = class(TObject)
  		Item_Object	:OleVariant;
   	Item_Type	:string;
  	end;

   TEmailItem = class(TObject)
   	Orig_item		: OleVariant;
   	To_Address		: string;
       CC_Address		: string;
       BCC_Address		: string;
   	Subject			: string;
       Body			: string;
       Sender			: string;
       Size			: integer;
       Create_time		: TDateTime;
       Receive_time	: TDateTime;
       Attach_files	: TStringList;
       public
		constructor		Create;
		destructor		Destroy;override;
       function		Send : boolean;
       function 		Move( DestFldr: TOutLookItem) : boolean;
       function		FillEmail( oleemail : Variant) : boolean;
       function		SaveAttachment(atnr : integer; fname : string) : integer;
   end;

  	TJOUTLOOK = class(TComponent)
       function 	Connect 	: boolean;
       procedure   Disconnect;
       function 	GetFolders( root : OleVariant; var folder_list : TList ) : integer;
		function 	GetEmails( folder : OleVariant; var email_list : TList ) : integer;
       function	GetRootFolder : OleVariant;
       function 	GetEmailsCount( folder : OleVariant ) : integer;
       procedure 	FreeList( flist : TList );
  	private
   	Out_Application		: OleVariant;
       Out_NameSpace		: OleVariant;
       IsConnected			: boolean;
   public
  	end;

var
   Joutlook	: TJOUTLOOK;

implementation

function TJOUTLOOK.Connect 	: boolean;
begin
   try
      	Out_Application		:= UnAssigned;
       try
           Out_Application	:= GetActiveOleObject('Outlook.Application');
       except
           Out_Application	:= CreateOleObject('Outlook.Application');
       end;
      	Out_NameSpace   	:= Out_Application.getNameSpace( 'MAPI' );
      	IsConnected     	:= true;
   except
      	Out_NameSpace   	:= Unassigned;
      	IsConnected     	:= false;
      	Out_Application		:= UnAssigned;
   end;
   Result					:= IsConnected;
end;

procedure TJOUTLOOK.Disconnect();
begin
   Out_NameSpace.LogOff;
	Out_Application			:= UnAssigned;
end;

function TJOUTLOOK.GetFolders( root : OleVariant; var folder_list : TList ) : integer;
var
   oi 		: TOutlookItem;
   i   	: integer;
   fcount  : integer;
begin
//   FreeList(folder_list);
	fcount := 0;
	try
        folder_list.Clear;
        fcount	:= root.count;
        try
            for i := 1 to fcount do begin
               oi := TOutlookItem.Create;
               oi.Item_Object	:= root.item[i];
               oi.Item_Type	:= 'Folder';
               folder_list.Add(oi);
            end;
        except
//   		Application.MessageBox('K�nyvt�rkeres� hiba !', 'Hiba',0);
        end;
   except
//   	Application.MessageBox('Darabsz�mos hiba', 'Hiba',0);
   end;
   Result	:= fcount;
end;

function	TJOUTLOOK.GetRootFolder : OleVariant;
begin
	Result	:= Out_NameSpace.Folders;
end;

procedure 	TJOUTLOOK.FreeList( flist : TList );
var
	i : integer;
begin
   if flist.Count > 0 then begin
   	for i := 0 to flist.Count - 1 do begin
       	try
   			TOutlookItem(flist.Items[i]).Free;
           except
           end;
       end;
   end;
end;

function TJOUTLOOK.GetEmails( folder : OleVariant; var email_list : TList ) : integer;
var
   oi 		: TOutlookItem;
   i   	: integer;
   Efolder	: OleVariant;
begin
//   FreeList(folder_list);
   email_list.Clear;
   Efolder:=Out_NameSpace.GetFolderFromId(folder.EntryID,folder.StoreID);
   if EFolder.Items.count > 0 then begin
       for i:=1 to EFolder.Items.count do begin
           oi 				:= TOutlookItem.Create;
           oi.Item_Object	:= EFolder.items[i];
           oi.Item_Type	:= 'File';
           email_list.Add(oi);
       end;
   end;
   Result	:= EFolder.Items.count;
end;

function TJOUTLOOK.GetEmailsCount( folder : OleVariant ) : integer;
var
   Efolder	: OleVariant;
begin
   Efolder	:=	Out_NameSpace.GetFolderFromId(folder.EntryID,folder.StoreID);
   result 	:= 	EFolder.Items.count;
end;

function	TEmailItem.Send : boolean;
begin
	Result	:= true;
end;

function	TEmailItem.FillEmail( oleemail : Variant) : boolean;
var
	i : integer;
begin
   Result	:= true;
	try
   	Orig_Item		:= oleemail;
       Subject			:= oleemail.Subject;
       To_Address		:= '';
       CC_Address		:= oleemail.CC;
       BCC_Address		:= oleemail.BCC;
       Body			:= oleemail.body;
       Sender			:= oleemail.SenderName;
       Size			:= oleemail.size;
       Create_time		:= oleemail.CreationTime;
       Receive_time	:= oleemail.ReceivedTime;
       Attach_files.Clear;
       if oleemail.Attachments.Count > 0 then begin
           for i := 1 to oleemail.Attachments.Count do begin
               Attach_files.Add(oleemail.Attachments(i).filename);
           end;
       end;
   except
   	Result	:= false;
   end;
end;

constructor	TEmailItem.Create;
begin
	Attach_files	:= TStringList.Create;
end;

destructor		TEmailItem.Destroy;
begin
	Attach_files.Free;
end;

function  TEmailItem.SaveAttachment(atnr : integer; fname : string) : integer;
begin
	Result	:= -1;
	if atnr > Attach_files.Count then begin
       Exit;
   end;
   if FileExists(fname) then begin
   	Result	:= -2;
       Exit;
   end;
   try
   	Orig_item.Attachments.Item(atnr).SaveAsFile(fname);
   except
   	Result	:= -3;
       Exit;
   end;
   Result	:= 0;
end;

function TEmailItem.Move( DestFldr: TOutLookItem) : boolean;
begin
    Orig_Item.Move(DestFldr.Item_Object);
    Result	:= true;
end;

end.
