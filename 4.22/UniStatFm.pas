unit UniStatFm;

interface

uses
	SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
	StdCtrls, Forms, DBCtrls, DB, DBGrids, DBTables, Grids, ExtCtrls, Buttons,
	ShellApi, Dialogs, J_FOFORMUJ, ADODB, Mask, Menus, Variants, Kozos, KozosTipusok ;


type
	TUniStatFmDlg = class(TJ_FOFORMUJDLG)
    PopupMenu1: TPopupMenu;
    miAzonositoVagolapra: TMenuItem;
    miMegjegyzesSzerkesztese: TMenuItem;
    miMegrendeloMegtekintes: TMenuItem;
    procedure AdatTolt(Fomezo, FoSQL, Fejlec: string; SzuromezoTomb, SzummaMezoTomb: TStringArray;
              LehetNew: boolean = False; LehetMod: boolean = False);
    procedure Adatlap(cim, kod : string);override;
    procedure DBGrid2DrawColumnCell(Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);override;
    procedure miAzonositoVagolapraClick(Sender: TObject);
    procedure miMegjegyzesSzerkeszteseClick(Sender: TObject);
    procedure miMegrendeloMegtekintesClick(Sender: TObject);
	 private
    procedure HideAllSubmenu(PM: TPopupMenu);
	 public
      AdatlapOsztaly: string;
      StatOsztaly: string;
      StatFotabla: string;
	end;

var
	UniStatFmDlg : TUniStatFmDlg;

implementation

uses
	Egyeb, J_SQL, MunkalapReszlet, J_ALFORM, Clipbrd, RendkivuliEsemenybe, MegrendeloKodMagyaraz, OsztrakRako,
  UniSzovegForm, VCARDParams;

{$R *.DFM}


procedure TUniStatFmDlg.AdatTolt(Fomezo, FoSQL, Fejlec: string; SzuromezoTomb, SzummaMezoTomb: TStringArray;
              LehetNew: boolean = False; LehetMod: boolean = False);
var
  S: string;
  i: integer;
begin
	kellujures	:= false;

  i:= 0;
 	while i <= High(SzuromezoTomb) do begin
    AddSzuromezoRovid( SzuromezoTomb[i], SzuromezoTomb[i+1], SzuromezoTomb[i+2]);
    i := i+3;
 	  end;  // while
  i:= 0;
 	while i <= High(SzummamezoTomb) do begin
    AddSzummamezo(SzummamezoTomb[i]);
    i := i+1;
 	  end;  // while

  // FELVISZ, MODOSIT, LISTCIM, FOMEZO, ALAPSTR, FOTABLA
  alapstr:= FoSQL;
	// FelTolto('', '', Fejlec, Fomezo, alapstr, '__', QUERY_ADAT_TAG );
  if StatFotabla='' then StatFotabla:='__';
  FelTolto('', '', Fejlec, Fomezo, alapstr, StatFotabla, QUERY_ADAT_TAG );
	kulcsmezo			:= Fomezo;
	nevmezo				:= Fomezo;
	width				:= DLG_WIDTH;
	height				:= DLG_HEIGHT;
  ButtonTor.Enabled :=False;
  if LehetNew then ButtonUj.Enabled :=True
  else ButtonUj.Enabled :=False;
  if LehetMod then ButtonMod.Enabled :=True
  else ButtonMod.Enabled :=False;
  if AdatlapOsztaly <> '' then begin
    ButtonNez.Visible :=True;
    ButtonNez.Enabled :=True;
    end
  else begin
    ButtonNez.Enabled :=False;
    end;
  if StatOsztaly = 'TelefonElozmenyek' then begin
     DBGrid2.PopupMenu	:= PopupMenu1;
     HideAllSubmenu(PopupMenu1);
     miAzonositoVagolapra.Visible:= True;
     end;
  if StatOsztaly = 'TelefonSzamlak' then begin
     DBGrid2.PopupMenu	:= PopupMenu1;
     HideAllSubmenu(PopupMenu1);
     miMegjegyzesSzerkesztese.Visible:= True;
     end;
  if StatOsztaly = 'Megrendelo_kodok' then begin
     DBGrid2.PopupMenu	:= PopupMenu1;
     HideAllSubmenu(PopupMenu1);
     miMegrendeloMegtekintes.Visible:= True;
     end;
end;

procedure TUniStatFmDlg.HideAllSubmenu(PM: TPopupMenu);
var
  i: integer;
begin
  for i:=0 to PM.Items.Count-1 do
    PM.Items[i].Visible:= False;
end;

procedure TUniStatFmDlg.miMegjegyzesSzerkeszteseClick(Sender: TObject);
var
    TS_ID, Szoveg: string;
begin
   TS_ID:= Query1.FieldByName('TS_ID').AsString;
    Szoveg:= Query1.FieldByName('Megjegyz�s').AsString;
		Application.CreateForm(TUniSzovegFormDlg, UniSzovegFormDlg);
    with UniSzovegFormDlg do begin
      Caption:='Megjegyz�s m�dos�t�sa';
      lblSzovegelemNeve.Caption:='Megjegyz�s:';
      meSzoveg.Text:= Szoveg;
      M1.Text:= TS_ID;
      UpdateSQLString:= 'update TELSZAMLAK set TS_MEGJE=''%s'' where TS_ID='+TS_ID;
      ShowModal;
      Release;
      end; // with
    SQL_ToltoFo(GetOrderBy(true));   // t�bl�zat friss�t�s
    ModLocate (Query1, FOMEZO, TS_ID);
end;

procedure TUniStatFmDlg.miMegrendeloMegtekintesClick(Sender: TObject);
var
    MEGRENDELOKOD, FileNev: string;
begin
   MEGRENDELOKOD:= Query1.FieldByName('MEGRENDELOKOD').AsString;
   FileNev:= EgyebDlg.Read_SZGrid('999', '760') +'\'+MEGRENDELOKOD+'.pdf';
   if not FileExists(FileNev) then begin
   		 NoticeKi('Az �llom�ny nem l�tezik! ('+FileNev+')');
    	 Exit;
       end;
   EgyebDlg.MyShellExecute (FileNev);
end;

procedure TUniStatFmDlg.miAzonositoVagolapraClick(Sender: TObject);
var
    ESZKOZ, THKATEG, AZONOSITO: string;
begin
   ESZKOZ:= Query1.FieldByName('ESZKOZ').AsString;
   THKATEG:= Query1.FieldByName('TH_KATEG').AsString;
   if THKATEG='TELEFON' then begin
      AZONOSITO:= EgyebDlg.SepRest('IMEI:', ESZKOZ);  // az IMEI
      end;
   if THKATEG='SIM' then begin
      AZONOSITO:= ESZKOZ;  //  a telefonsz�m
      end;
   Clipboard.Clear;
	 Clipboard.AsText:= AZONOSITO;
end;

procedure TUniStatFmDlg.Adatlap(cim, kod : string);
var
  MehetAzAlform, MehetAzAdatlap: boolean;
  Tulaj: string;
begin
  MehetAzAlform:= True;
  if AdatlapOsztaly = 'TMegrendeloKodMagyarazDlg' then begin
    Tulaj:= Query_select('MEGRENDELO', 'MR_ID', kod, 'MR_KINEK');
    if Tulaj <> EgyebDlg.user_code then begin
        MehetAzAlform:= False;
        NoticeKi ('Csak a saj�t megrendel�k m�dos�that�k!');
        end;  // if
    end;  // if
  if MehetAzAlform then begin
     MehetAzAdatlap:= True;
    if AdatlapOsztaly = 'TMunkalapReszletDlg' then
      Application.CreateForm(TMunkalapReszletDlg, AlForm);
    if AdatlapOsztaly = 'TRendkivuliEsemenybeDlg' then
      Application.CreateForm(TRendkivuliEsemenybeDlg, AlForm);
    if AdatlapOsztaly = 'TMegrendeloKodMagyarazDlg' then
      Application.CreateForm(TMegrendeloKodMagyarazDlg, AlForm);
    if AdatlapOsztaly = 'TOsztrakRakoDlg' then
      Application.CreateForm(TOsztrakRakoDlg, AlForm);
    if AdatlapOsztaly = 'TVCARDParamsDlg' then begin
      Application.CreateForm(TVCARDParamsDlg, VCARDParamsDlg);
    	VCARDParamsDlg.VCardTolto(Query1.FieldByName('T�bla').AsString, Query1.FieldByName('K�d').AsString, Query1.FieldByName('N�v').AsString);
    	VCARDParamsDlg.ShowModal;
    	VCARDParamsDlg.Release;
      ModLocate (Query1, FOMEZO, Query1.FieldByName('N�v').AsString);
	    DBGrid2.Refresh;
      MehetAzAdatlap:= False;
      end;
    if MehetAzAdatlap then
      Inherited Adatlap(cim, kod );
    end;  // if
end;

procedure TUniStatFmDlg.DBGrid2DrawColumnCell(Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
var
   statuszid: integer;
begin
   if StatOsztaly = 'Rendkivuli_esemenyek' then begin
     DBGrid2.Canvas.Brush.Color:= clWindow;  // alap�rtelmezett h�tt�rsz�n
     statuszid:= Query1.FieldByName('RK_STATUSZID').AsInteger;
     if statuszid = 2 then begin
        DBGrid2.Canvas.Brush.Color:= EgyebDlg.GetFuvarosSzin('1');  // s�t�t s�rg�ra a lez�rtakat
        end;
     DBGrid2.DefaultDrawDataCell(Rect, Column.Field, State);
     end;  // Rendkivuli_esemenyek

	inherited DBGrid2DrawColumnCell(Sender, Rect, DataCol, Column, State);
end;




end.


