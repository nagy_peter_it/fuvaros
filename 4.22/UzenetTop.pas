unit UzenetTop;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls, Data.DBXJSON, ChatControl,
  {Gradient,} acPNG, Vcl.Buttons, System.Win.ScktComp, cefvcl, ceflib, RESTAPI_Kozos, Kozos, KozosTipusok,
  Vcl.Menus, Vcl.Mask, Vcl.Imaging.jpeg;

const
 WM_REENABLE = WM_USER + $100;
 NumberOfPages = 3;
 NumberOfSwitches = 2;
 BelsoSzep = '^';

 colorSargas = $0035AEF1;   // $0018AFF2;
 colorKekes = $00B29943;
 colorSotetSzurke = $00C1BFBD;
 colorAlapSzurke = $00C1BFBD;
 colorVonalKek = $00CCBD8B;
 colorChatSzurke = $00ACAC9A;
 colorVilagosSzurke = $00F2F2F3;
 colorNevsorSzeparator = colorSotetSzurke;
 Switch_ActiveColor = colorSargas; // clHotLight;
 Switch_NeutralColor = clWhite; //  clBtnFace;
 KeresAlapStr = 'Keres�s';
 cUserPanelHeight = 31;

// type
  // TGroupIDList = TDictionary<string,string>;
type
  TContactRecord = record
    which: string;
    id: string;
    name: string;
    end;
  TContactArray = array of TContactRecord;

type
  TUzenetTopDlg = class(TForm)
    pnlUzenetMaster: TPanel;
    pnlUzenetPage1: TPanel;
    pnlUzenetPage2: TPanel;
    pnlUzenetPage3: TPanel;
    pnlWriteMessage: TPanel;
    memoMessage: TMemo;
    pnlSzalKeres: TPanel;
    ebSzalbanKeres: TEdit;
    Panel2: TPanel;
    pnlSwitch1Elem1: TPanel;
    pnlSwitch1Elem2: TPanel;
    Panel3: TPanel;
    pnlSwitch2Elem1: TPanel;
    ScrollBox1: TScrollBox;
    ScrollBox2: TScrollBox;
    Panel1: TPanel;
    Label1: TLabel;
    Panel15: TPanel;
    Panel16: TPanel;
    lblActThread: TLabel;
    Panel17: TPanel;
    Panel18: TPanel;
    Label23: TLabel;
    Panel19: TPanel;
    pnlSwitch2Elem2: TPanel;
    Button1: TButton;
    lbSorrendbe: TListBox;
    imgPont: TImage;
    Button2: TButton;
    Panel21: TPanel;
    Image5: TImage;
    ebKontaktKeres: TEdit;
    Panel4: TPanel;
    ebSzalKeres: TEdit;
    Image4: TImage;
    btnSzalbanKeres: TBitBtn;
    Button3: TButton;
    Chromium1: TChromium;
    Timer1: TTimer;
    ebCCNevsor: TEdit;
    Panel5: TPanel;
    Image3: TImage;
    Label2: TLabel;
    Image2: TImage;
    M33: TMaskEdit;
    M34: TMaskEdit;
    PopupMenu1: TPopupMenu;
    mmiSzalTorol: TMenuItem;
    pnlActUser: TPanel;
    lbUzenetekUserData: TListBox;
    CheckBox1: TCheckBox;
    Label3: TLabel;
    imgGenericAvatar: TImage;
    procedure FormCreate(Sender: TObject);
    procedure Panel15Click(Sender: TObject);
    procedure Panel17Click(Sender: TObject);
    procedure Panel19Click(Sender: TObject);
    procedure Panel4Click(Sender: TObject);
    procedure pnlSwitch1Elem1Click(Sender: TObject);
    procedure pnlSwitch1Elem2Click(Sender: TObject);
    procedure pnlSwitch2Elem1Click(Sender: TObject);
    procedure pnlSwitch2Elem2Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure KontaktPanelClick(Sender: TObject);
    procedure KontaktLabelClick(Sender: TObject);
    // procedure KontaktGradientClick(Sender: TObject);
    procedure ThreadsPanelClick(Sender: TObject);
    procedure ThreadsLabelClick(Sender: TObject);
    // procedure ThreadsGradientClick(Sender: TObject);
    procedure Image2Click(Sender: TObject);
    procedure KeresClick(Sender: TObject);
    procedure ebKontaktKeresChange(Sender: TObject);
    procedure memoMessageKeyPress(Sender: TObject; var Key: Char);
    procedure Button2Click(Sender: TObject);
    procedure btnSzalbanKeresClick(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Image3Click(Sender: TObject);
    procedure MyExecuteJavaScript(const JSString: string);
    procedure Chromium1ConsoleMessage(Sender: TObject;
      const browser: ICefBrowser; const message, source: ustring; line: Integer;
      out Result: Boolean);
    procedure FormDeactivate(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormDestroy(Sender: TObject);
    procedure btnPrevMessagesClick(Sender: TObject);
    procedure ebSzalKeresChange(Sender: TObject);
    procedure Label2Click(Sender: TObject);
    procedure mmiSzalTorolClick(Sender: TObject);
    procedure cbUzenetekUserChange(Sender: TObject);
    // procedure ChatClick;
    procedure ClearUserSelect;
    procedure AddUserSelect(const AUserName: string; const ATag: integer; const AEnabled: boolean);
    procedure SetUserSelect(const ATag: integer; const ADoLogin: Boolean);
    procedure ShowAllUserSelect;
    procedure UserPanelClick(Sender: TObject);
    procedure UserCheckClick(Sender: TObject);
    procedure LoginTruckpitUser(const ATag: integer);
    function GetTagFromXuid(const Axuid: string): integer;
    procedure SetUserEnabled(const ATag: integer; const AEnabled: boolean);
  private
    PageArray: array [1..NumberOfPages] of TPanel;
    SwitchArray: array [1..NumberOfSwitches , 1..2] of TPanel;
    ContactIDArray, ThreadsContactIDArray: TContactArray;
    ActiveThread: TContactRecord;
    KontaktTopPosition, ThreadsTopPosition: integer;
    ActThreadMinMessageID: string;
    UserSelectMode: TUserSelectMode;
    TruckpitAliasInfoArray: TTruckpitAliasInfoArray;
    // GroupIDList: TGroupIDList;
    // LastestContactsJSON, LatestThreadsJSON: string;
    procedure WMEnable(var Message: TWMEnable); message WM_ENABLE;
    procedure WMReenable(var Message: TMessage); message WM_REENABLE;
    procedure SetActiveSwitch(const SwitchNumber, ActiveNumber: integer);
    function GetActiveSwitch(const SwitchNumber: integer): integer;
    function TaskBarHeight: integer;
    // function GetListOp(CL: string; N: integer; Sep: string): string;
    procedure AddKontaktPanel(const ANeve, ACeg: string; const KellLine: boolean; const ASorszam: integer;
        const AvatarUrl, AvatarCacheID: string);
    procedure AddThreadPanel(const ANeve, AUzenet, AUzenetMikor: string; const AVanOlvasatlan: boolean;
    const ASorszam: integer; const AUtolsoUzenetID, AvatarUrl, AvatarCacheID: string);
    procedure ShowThread(const Honnan: string; const ASorszam: integer);
    procedure MyCreatePanelLabel(Elem: TPanel; i_Left, i_Top, i_Width, i_Height: integer; i_Caption: string; i_IsBold: boolean;
            i_Color, i_BackColor: TColor; i_Size, i_Tag: integer; ClickMode, MessageID: string; AAlignment: TAlignment = taLeftJustify);
    procedure MyCreatePanelPanel(Elem: TPanel; i_Left, i_Top, i_Width, i_Height: integer; i_Caption: string; i_IsBold: boolean;
            i_Color, i_BackColor: TColor);
    procedure MyCreatePanelMemo(Elem: TPanel; i_Left, i_Top, i_Width, i_Height: integer; i_Text: string; i_IsBold: boolean;
            i_Color, i_BackColor: TColor; i_Size, i_Tag: integer; ClickMode: string);
    procedure MyCreatePanelHorLine(Elem: TPanel; i_Left, i_Top, i_Width: integer; i_BackColor: TColor);
    procedure MyCreatePanelImage1(Elem: TPanel; i_Left, i_Top, i_Width, i_Height: integer);
    procedure MyCreatePanelAvatar(Elem: TPanel; i_Left, i_Top, i_Width, i_Height: integer; const ImageURL, ImageCacheID: String);
    // procedure MyCreatePanelGradient(Elem: TPanel; i_GradientStyle: TGradientStyle; i_Size, i_Tag: integer; ClickMode: string);
    procedure SetOlvasva(const AMessageID: string);
    procedure SendAttachment;
    procedure MySocketEvent(const SocketText: string);
    procedure PinToTop(AMyAlwaysOnTop: boolean);
    procedure WMNCLBUTTONDOWN(var msg: TMessage); message WM_NCLBUTTONDOWN;
    procedure SendEagleSMS(const ActiveThread: TContactRecord; const aText: string);
    procedure MyScrollVertUp(Sender: TObject);
  public
    Actxuid: string;
    Userxuid: string;
    ActOverkill: boolean;  // k�ldhet-e a user s�rg�s "�breszt�" �zenetet
    ActivePage: integer;
    Lathato: boolean;
    MyAlwaysOnTop: boolean;
    MegbizasKod: string;
    procedure RefreshContacts(const ASendRequest: boolean);
    procedure RefreshThread(const ContactRecord: TContactRecord; const KeresMinta: string; const IfKorabbiak: boolean);
    procedure RefreshActThread;
    procedure RefreshThreads(const ASendRequest: boolean);
    procedure UzenetKuld(const aText, aAttachment,  AThumb, aMimeType: string);
    procedure CCKuld(const aText: string);
    procedure SetActivePage(const PageNumber: integer);
    procedure UzenetElokeszit(const AXUID, AXUID2, AName, AName2, ASzoveg, AMBKOD: string);
    procedure ChangeToXUID(const Axuid: string);
    procedure SetAlias;
    { Public declarations }
  end;
const
    WebSocketHTMLStr: AnsiString =
    '<html> '+CRLF+
    '<head> '+CRLF+
    '<title>Socket.IO for development</title> '+CRLF+
    '</head> '+CRLF+
    // '<body > '+
    '<body id="signal"> '+CRLF+
    //'   <b>HTML legal�bb megvan</b> '+CRLF+
    // '   <p id="kiir"></p> '+CRLF+
    //'   <ul id="messages"></ul> '+CRLF+
    // '    <form action=""> '+CRLF+
    // '      <input id="m" autocomplete="off" /><button>Send</button> '+CRLF+
    // '  </form> '+CRLF+
    // '  <div id="mycanvas" style="width:100%; height:100%"></div> '+
    '</body> '+CRLF+
    // '<meta name="viewport" content="initial-scale=1.0, user-scalable=yes" /> '+
    ' <script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.1.1/socket.io.slim.js"></script> '+CRLF+
    '  <script> '+CRLF+
    // '     var socket = io(''wss://soforbackend.herokuapp.com/'', {query: ''token=something''}); '+CRLF+
    '     var socket = io(''wss://truckpit.novel.hu/'', {query: ''token=something''}); '+CRLF+
    // '     var socket = io(''wss://soforbackend.herokuapp.com/msgbus'', {query: ''token=something''}); '+CRLF+
    //'     var messages = document.getElementById("messages"); '+CRLF+
    // '     var kiir = document.getElem�entById("kiir"); '+CRLF+
    //'       socket.on(''connect'', function (socket) { '+CRLF+
    // '     document.getElementById(''signal'').classList.remove(''disconnected'') '+CRLF+
    // '      document.getElementById(''signal'').classList.add(''connected'') '+CRLF+
    // '       kiir.innerHTML += '' Server UP'';  '+CRLF+
    //'       messages.innerHTML += ''<li>Server UP</li>'';  '+CRLF+
    //'       console.log(''Connected''); '+CRLF+
    // '       mydelphiclass.Value = ''Connected''; '+
    // '       alert(mydelphiclass.Value); '+
    // '     }); '+CRLF+
    '     socket.on(''refresh'', function (data) { '+CRLF+
    //'       kiir.innerHTML += '' Refresh'';  '+CRLF+
    // '       console.log(''Refresh van!''); '+CRLF+
    // '       console.log(''Groups: '' + data[''groups''] + '' Users: '' + data[''users''] ); '+CRLF+
    '       console.log(data[''groups''] + ''|'' + data[''users''] ); '+CRLF+
    //'       messages.innerHTML += "<li>(refresh) Groups: " + data[''groups''] + " Users: " + data[''users''] + "</li>" ; '+CRLF+
    '     }); '+CRLF+
    //'     socket.on(''join'', function (data) { '+CRLF+
    //'       console.log(''joined id: '' + data); '+CRLF+
    // '       kiir.innerHTML += '' Join'';  '+CRLF+
    // '       mydelphiclass.Value = ''(join) id: '' + data; '+
    // '       alert(mydelphiclass.Value); '+
    //'       messages.innerHTML += ''<li>join id: ''+ data + ''</li>''; '+CRLF+
    //'     }); '+CRLF+
    //'     socket.on(''disconnect'', function (socket) { '+CRLF+
    //'       console.log(''Server DOWN''); '+CRLF+
    // '       mydelphiclass.Value = ''Server DOWN''; '+
    // '       alert(mydelphiclass.Value); '+
    //'       messages.innerHTML += ''<li>Server DOWN</li>''; '+CRLF+
    //'     }); '+CRLF+
    ' </script> '+CRLF+
    '</html> ';

 var
  UzenetTopDlg: TUzenetTopDlg;
  ChatControl1: TChatControl;

implementation

uses Egyeb, J_SQL, NotificationTop, J_VALASZTO;

{$R *.dfm}

procedure TUzenetTopDlg.WMEnable(var Message: TWMEnable);
begin
 if not Message.Enabled then
 begin
   PostMessage(Self.Handle, WM_REENABLE, 1, 0);
 end;
end;

procedure TUzenetTopDlg.PinToTop(AMyAlwaysOnTop: boolean);
begin
  if AMyAlwaysOnTop then begin
    SetWindowPos(Handle, HWND_TOPMOST, 0, 0, 0, 0, SWP_NoMove or SWP_NoSize or SWP_NOACTIVATE);
    end
  else begin
    SetWindowPos(Handle, HWND_NOTOPMOST, 0, 0, 0, 0, SWP_NoMove or SWP_NoSize or SWP_NOACTIVATE);
    end;
end;

procedure TUzenetTopDlg.WMReenable(var Message: TMessage);
begin
  EnableWindow(Self.Handle, True);
end;

procedure TUzenetTopDlg.WMNCLBUTTONDOWN(var msg: TMessage);
begin
  if msg.wParam = HTCAPTION then begin
    MyAlwaysOnTop:= not MyAlwaysOnTop;
    // if MyAlwaysOnTop then Caption := '�zenetek (mindig fel�l)'
    // else Caption := '�zenetek';
    if MyAlwaysOnTop then BorderStyle:= bsSingle  // display form icon
    else BorderStyle:= bsDialog;  // no form icon
    PinToTop(MyAlwaysOnTop);
    end;
    // Message.Result := 0; {to ignore the message}
  inherited;
end;

procedure TUzenetTopDlg.FormDeactivate(Sender: TObject);
begin
   {if MyAlwaysOnTop then begin
      PinToTop(MyAlwaysOnTop);
      end
   else begin}
   Lathato:= False;
   //   end;
end;

procedure TUzenetTopDlg.FormDestroy(Sender: TObject);
begin
   // if GroupIDList<> nil then GroupIDList.Free;
end;

procedure TUzenetTopDlg.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = VK_F4) and (ssAlt in Shift) then
      Key:=0;  // disable Alt-F4 form close
end;

procedure TUzenetTopDlg.Timer1Timer(Sender: TObject);
begin
   // if (not Lathato) and MyAlwaysOnTop then begin
   if MyAlwaysOnTop then begin
      PinToTop(True);
      Lathato:= True;
      end;
end;

procedure TUzenetTopDlg.Button1Click(Sender: TObject);
begin
   RefreshContacts(True);
end;

procedure TUzenetTopDlg.Button2Click(Sender: TObject);
begin
   RefreshThreads(True);
end;

procedure TUzenetTopDlg.Button3Click(Sender: TObject);
begin
  RefreshThread(ActiveThread, '', False);
end;

procedure TUzenetTopDlg.cbUzenetekUserChange(Sender: TObject);
var
  ErrorText, InfoString: string;
  i, ResponseCode: integer;
begin
{  i:= cbUzenetekUser.ItemIndex;
  InfoString:= lbUzenetekUserData.Items[i];
  RESTAPIKozosDlg.InitTokenRequest(GetListOp(InfoString, 0, BelsoSzep), GetListOp(InfoString, 2, BelsoSzep));
  if RESTAPIKozosDlg.GetNewToken(ResponseCode, ErrorText)  then begin
     UzenetTopDlg.Actxuid:= GetListOp(InfoString, 1, BelsoSzep);
     end
   else begin
     MessageDlg('Hiba a bejelentkez�s sor�n! '+ErrorText, mtError,[mbOK],0);
     end;
  SetActivePage(ActivePage);  // b�rhol is van, friss�ti
  }
end;

procedure TUzenetTopDlg.Chromium1ConsoleMessage(Sender: TObject;
  const browser: ICefBrowser; const message, source: ustring; line: Integer;
  out Result: Boolean);
begin
   MySocketEvent(message);
   // Memo1.Lines.Add(message);
   Result:= True;
end;

procedure TUzenetTopDlg.ebKontaktKeresChange(Sender: TObject);
begin
  RefreshContacts(False);
end;

procedure TUzenetTopDlg.ebSzalKeresChange(Sender: TObject);
begin
   RefreshThreads(False);
end;

procedure TUzenetTopDlg.KeresClick(Sender: TObject);
begin
    (Sender as TEdit).SelectAll;
end;

procedure TUzenetTopDlg.FormActivate(Sender: TObject);
begin
   Lathato:= True;
end;

procedure TUzenetTopDlg.ClearUserSelect;
begin
  while pnlActUser.ControlCount > 0 do pnlActUser.Controls[0].Free;  // az �sszes al-panel t�rl�se
end;

procedure TUzenetTopDlg.AddUserSelect(const AUserName: string; const ATag: integer; const AEnabled: boolean);
var
   MyPanel: TPanel;
begin
     MyPanel:= TPanel.Create(pnlActUser);
     with MyPanel do begin
        Parent:= pnlActUser;
        AutoSize:= False;  // nehogy egym�sra l�gjanak
        Caption:= AUserName;
        // BevelInner:= bvNone;
        // BevelOuter:= bvLowered;
        Left:= 0;
        Top:= 0; // majd berendezz�k
        Width:= 305;
        Height:= cUserPanelHeight;
        ParentColor:= False;
        ParentBackground:= False;
        Font.Size:= 10;
        Font.Style := [fsBold];
        Tag:= ATag;
        OnClick:= UserPanelClick;
        if AEnabled then Color:= colorSargas
        else Color:= colorAlapSzurke;
        Visible:= False;  // alapb�l ne l�tsszon
        end;
     if ATag >= 1 then begin  // a saj�t nevet nem lehet letiltani, csak az aliasokat
       with TCheckBox.Create(MyPanel) do begin
          Parent:= MyPanel;
          Caption:= '';
          Left:=10;
          Top:=8;
          Width:= 14;
          Tag:= ATag;
          Checked:= AEnabled;
          OnClick:= UserCheckClick;
          end; // with
       end;  // if
     //  pnlActUser.Height:= pnlActUser.Height + cUserPanelHeight;
end;

procedure TUzenetTopDlg.SetUserEnabled(const ATag: integer; const AEnabled: boolean);
var
  i: integer;
  MyPanel: TPanel;
  DBValue, InfoString, AliasXuid, S: string;
begin
 for i:= 0 to pnlActUser.ControlCount-1 do begin
    MyPanel:= (pnlActUser.Controls[i] as TPanel);
    if MyPanel.Tag = ATag then begin
        InfoString:= lbUzenetekUserData.Items[i];
        AliasXuid:= GetListOp(InfoString, 1, BelsoSzep);
        if AEnabled then begin
            MyPanel.Color:= colorSargas;
            DBValue:= '1';
            end
        else begin
            MyPanel.Color:= colorAlapSzurke;
            DBValue:= '0';
            end; // else
        S:= 'update TRUCKPIT_ALIAS set TA_ENABLED = '+DBValue+
            ' where TA_FEKOD = '''+ EgyebDlg.user_dokod +''''+
            ' and TA_XUID = '''+ AliasXuid + '''';
        Query_run(EgyebDlg.Query3, S);
        end; // if
    end;  // for
end;

procedure TUzenetTopDlg.ShowAllUserSelect;
var
  i: integer;
  MyPanel: TPanel;
begin
   pnlActUser.Height:= pnlActUser.ControlCount * (cUserPanelHeight+2) + 2; // bevel
   for i:= 0 to pnlActUser.ControlCount-1 do begin
      MyPanel:= (pnlActUser.Controls[i] as TPanel);
      MyPanel.Top:= i*(cUserPanelHeight+2);
      MyPanel.Visible:= True;
      end; // for
end;

procedure TUzenetTopDlg.SetUserSelect(const ATag: integer; const ADoLogin: Boolean);
var
  i: integer;
  MyPanel: TPanel;
begin
   for i:= 0 to pnlActUser.ControlCount-1 do begin
      MyPanel:= (pnlActUser.Controls[i] as TPanel);
      if MyPanel.Tag = ATag then begin
          MyPanel.Visible:= True;
          MyPanel.Top:= 0; // ez legyen fel�l
          end // if
      else begin
          MyPanel.Visible:= False;
          end; // else
      end; // for
   pnlActUser.Height:= cUserPanelHeight + 2;  // bevel
   if ADoLogin then begin
      LoginTruckpitUser(ATag);
      end; // if
end;

function TUzenetTopDlg.GetTagFromXuid(const Axuid: string): integer;
var
  InfoString: string;
  i: integer;
begin
  for i:= 0 to lbUzenetekUserData.Items.Count-1 do begin
    InfoString:= lbUzenetekUserData.Items[i];
    if (GetListOp(InfoString, 1, BelsoSzep) = Axuid) then begin
      Result := i;
      Break;
      end;
    end; // for
end;

procedure TUzenetTopDlg.LoginTruckpitUser(const ATag: integer);
var
  ErrorText, InfoString: string;
  i, ResponseCode: integer;
begin
  InfoString:= lbUzenetekUserData.Items[ATag];
  RESTAPIKozosDlg.InitTokenRequest(GetListOp(InfoString, 0, BelsoSzep), GetListOp(InfoString, 2, BelsoSzep));
  if RESTAPIKozosDlg.GetNewToken(ResponseCode, ErrorText)  then begin
     UzenetTopDlg.Actxuid:= GetListOp(InfoString, 1, BelsoSzep);
     end
   else begin
     MessageDlg('Hiba a bejelentkez�s sor�n! '+ErrorText, mtError,[mbOK],0);
     end;
  // SetActivePage(ActivePage);  // b�rhol is van, friss�ti
  SetActivePage(1);  // v�lt�skor mindig az 1. lapon kezdjen
 end;

procedure TUzenetTopDlg.ChangeToXUID(const Axuid: string);
var
  ActTag: integer;
begin
  if (Axuid <> ActXUID) then begin
     ActTag:= GetTagFromXuid(Axuid);
     SetUserSelect(ActTag, True);
     end; // if
end;

procedure TUzenetTopDlg.UserPanelClick(Sender: TObject);
var
   MyTag: integer;
begin
   if UserSelectMode = becsukva then begin
     ShowAllUserSelect;
     UserSelectMode:= kinyitva;
     end
   else begin  // kinyitva
     if ((Sender as TPanel).Color = colorSargas) then begin  // csak ha enged�lyezett panel... bocs a megold�s�rt
       MyTag:= (Sender as TPanel).Tag;
       SetUserSelect(MyTag, True);
       UserSelectMode:= becsukva;
       end;
     end;
end;

procedure TUzenetTopDlg.UserCheckClick(Sender: TObject);
var
   MyTag: integer;
begin
   MyTag:= (Sender as TCheckbox).Tag;
   SetUserEnabled(MyTag, (Sender as TCheckbox).Checked);
end;


procedure TUzenetTopDlg.FormCreate(Sender: TObject);
var
  TResp, i: integer;
  TErr, TelSzam, CockpitJelszo, S: string;
begin
   // RESTAPIKozosDlg.InitTokenRequest('36209588785', '12345678');
   // TelSzam:= EgyebDlg.GetMobilAppTelszam;
   // CockpitJelszo:= EgyebDlg.GetMobilAppJelszo;

   RESTAPIKozosDlg.InitTokenRequest(EgyebDlg.MobilAppTelszam, EgyebDlg.MobilAppJelszo);
   if RESTAPIKozosDlg.GetNewToken(TResp, TErr) then begin
     // Actxuid:= '10000';
     Actxuid:= EgyebDlg.user_dokod;
     UserXUID:= Actxuid;  // ha k�s�bb �tjelentkezik, az akkor is megmarad
     end
   else begin
     MessageDlg('Hiba a bejelentkez�s sor�n! '+TErr, mtError,[mbOK],0);
     end;
   // -------- alias bel�p�sek --------- //
   SetAlias;
   // ---------------------------------- //
   PageArray[1]:= pnlUzenetPage1;
   PageArray[2]:= pnlUzenetPage2;
   PageArray[3]:= pnlUzenetPage3;

   SwitchArray[1,1]:= pnlSwitch1Elem1;
   SwitchArray[1,2]:= pnlSwitch1Elem2;
   SwitchArray[2,1]:= pnlSwitch2Elem1;
   SwitchArray[2,2]:= pnlSwitch2Elem2;

   // Actxuid:= '3'; // vagy 1111 // majd a Fuvarosban h�v�skor megadjuk
   // Actxuid:= '2'; // majd a Fuvarosban h�v�skor megadjuk
   // Actxuid:= '10000'; // majd a Fuvarosban h�v�skor megadjuk

   ChatControl1:= TChatControl.Create(pnlUzenetPage2);
   ChatControl1.Parent:= pnlUzenetPage2;
   ChatControl1.Align:=alClient;
   ChatControl1.BackColor:= pnlUzenetPage2.Color;
   ChatControl1.SetFontSize(10);
   ChatControl1.SetInfoFontSize(7);
   ChatControl1.SetSenderFontSize(7);
   ChatControl1.Color1:= colorChatSzurke;
   ChatControl1.Color2:= colorSargas;
   ChatControl1.FontColor1:= clWhite;
   ChatControl1.FontColor2:= clWhite;

   ChatControl1.OnScrollVertUp := MyScrollVertUp;
   ChatControl1.OnRequestRefresh:= RefreshActThread;
   // ChatControl1.OnScrollHorz := MyScrollHorz;
   // debughoz kellett volna  (ChatControl1 as TCustomControl).OnClick:= ChatClick;

   // ebSzalKeres.Text:= KeresAlapStr;
   // ebSzalbanKeres.Text:= KeresAlapStr;
   // ebKontaktKeres.Text:= KeresAlapStr;

   // GroupIDList:= TGroupIDList.Create;
   SetActivePage(1);
   Chromium1.Browser.MainFrame.LoadString(WebSocketHTMLStr, 'source://html');
   Lathato:= True;  // fel�l van a form
   EnableMenuItem( GetSystemMenu( handle, False ),SC_CLOSE, MF_BYCOMMAND or MF_GRAYED );  // system menu / "X" disable
   MyAlwaysOnTop:= True;
   PinToTop(MyAlwaysOnTop);
end;

procedure TUzenetTopDlg.SetAlias;
var
  S: string;
  i: integer;
begin
  TruckpitAliasInfoArray:= EgyebDlg.GetTruckpitAliasList(EgyebDlg.user_dokod);
  if (Length(TruckpitAliasInfoArray) = 0) then begin  // nincs alias
      pnlActUser.Visible:= False;
      end
   else begin
      pnlActUser.Visible:= True;
      // cbUzenetekUser.Items.Clear;
      ClearUserSelect;
      lbUzenetekUserData.Clear;
      // cbUzenetekUser.Items.Add(EgyebDlg.user_name);
      AddUserSelect(EgyebDlg.user_name, 0, True);
      S:= EgyebDlg.MobilAppTelszam + BelsoSzep +
          EgyebDlg.user_dokod + BelsoSzep +
          EgyebDlg.MobilAppJelszo;
      lbUzenetekUserData.Items.Add(S);
      for i:= 0 to Length(TruckpitAliasInfoArray)-1 do begin
        // cbUzenetekUser.Items.Add(TruckpitAliasInfoArray[i].AliasNev);
        AddUserSelect(TruckpitAliasInfoArray[i].AliasNev, i+1, TruckpitAliasInfoArray[i].Enabled);
        S:= TruckpitAliasInfoArray[i].AliasTelefon + BelsoSzep +
            TruckpitAliasInfoArray[i].AliasXUID + BelsoSzep +
            TruckpitAliasInfoArray[i].AliasJelszo;
        lbUzenetekUserData.Items.Add(S);
        end;  // for
      SetUserSelect(0, False);  // nem kell bejelentkezni
      end;  // else
end;

procedure TUzenetTopDlg.MyScrollVertUp(Sender: TObject);
begin
   if (ChatControl1.VertScrollBar.Position = 0) and (ebSzalbanKeres.text = '') then
       RefreshThread(ActiveThread, '', True);
end;

{procedure TUzenetTopDlg.MyScrollHorz(Sender: TObject);
begin

end;
}

procedure TUzenetTopDlg.MyExecuteJavaScript(const JSString: string);
begin
   Chromium1.Browser.MainFrame.ExecuteJavaScript(JSString,'about:blank', 0);
end;

procedure TUzenetTopDlg.MySocketEvent(const SocketText: string);
// Groups:  Users: 401,10000   lek�pezve: |401,10000
// Groups: DEV Users: 10003,10000,2,10004  lek�pezve: DEV|10003,10000,2,10004
// Att�l f�gg�en hogy melyik lapot l�tom:
//   [�zenet sz�lak] ha az �rintett csoportnak tagja vagyok (fel vagyok sorolva a felhaszn�l�kban), akkor friss�tem
//   [�zenet sz�l] ha �pp az �rintett csoport van megjelen�tve, akkor friss�tem
//                 ha benne vagyok egy sz�lban, de m�sik sz�lon j�n �zenet?
var
  CsoportLista, EgyeniLista, S: string;
  NotifXUID, AliasXUID: string;  // az �rtes�tend� XUID
  i: integer;
begin
  // teszt
  // ebSzalbanKeres.Text:= SocketText;
  CsoportLista:= RESTAPIKozosDlg.GetListOp(SocketText, 0, '|');
  EgyeniLista:= RESTAPIKozosDlg.GetListOp(SocketText, 1, '|');
  // Query_Log_Str('Socket: csop='+CsoportLista+', user='+EgyeniLista, 0, false, false);
  NotifXUID:= '';
  // -------- single version ------
  if RESTAPIKozosDlg.ListabanBenne(UserXUID, EgyeniLista, ',') then begin  // �rintve vagyok
      NotifXUID:= UserXUID;
      end;
  // --------- multi version ------
  for i:= 0 to Length(TruckpitAliasInfoArray)-1 do begin
      AliasXUID:= TruckpitAliasInfoArray[i].AliasXUID;
      if RESTAPIKozosDlg.ListabanBenne(AliasXUID, EgyeniLista, ',') then begin  // �rintve vagyok
        NotifXUID:= AliasXUID;
        break;
        end;
      end;
  // ------------------------------
  if NotifXUID <> '' then begin

   // ezzel az a baj, hogy nem j�, ha pl. �pp �zenetet �r, �s �tkapcsol neki egy m�sik felhaszn�l�ra
   // ink�bb csak sz�ljon, �s majd � benyitja.
   { if UzenetTopDlg.Lathato then begin
      ChangeToXUID(NotifXUID);
      if ActivePage=1 then begin
         RefreshThreads(True);
         end;
      if ActivePage=2 then begin
        if (ActiveThread.which = 'group') and RESTAPIKozosDlg.ListabanBenne(ActiveThread.id, CsoportLista, ',')
        or (ActiveThread.which = 'user') and RESTAPIKozosDlg.ListabanBenne(ActiveThread.id, EgyeniLista, ',') then begin
          RefreshThread(ActiveThread, ebSzalbanKeres.text, False);
          end;
        end;
       if ActivePage=3 then begin
         // ha esetleg el�rhet�s�gi inform�ci� friss�l?
         // RefreshContacts(True);
         end;
       end  // l�tszik a form
     else begin  // nem l�tszik a form: notification
        if NotificationTopDlg = nil then begin
          Application.CreateForm(TNotificationTopDlg, NotificationTopDlg);
          end;
        NotificationTopDlg.XUID:= NotifXUID;
        NotificationTopDlg.Show;
        end; }

    if NotificationTopDlg = nil then begin
          Application.CreateForm(TNotificationTopDlg, NotificationTopDlg);
          end;
    NotificationTopDlg.XUID:= NotifXUID;
    if UzenetTopDlg.Lathato then begin
       NotificationTopDlg.Top:= UzenetTopDlg.Top - NotificationTopDlg.Height - 5;
       end
    else begin
       NotificationTopDlg.Top:= (Screen.Height - NotificationTopDlg.Height); //  - TaskBarHeight;
       end;
    NotificationTopDlg.Show;

    end;
end;


{procedure TUzenetTopDlg.ChatClick(Sender: TObject);
begin
  MessageDlg('Chat clicked.', mtInformation,[mbOK],0);
end;
}

procedure TUzenetTopDlg.FormShow(Sender: TObject);
begin
  Left := (Screen.Width - Width);
  Top := (Screen.Height - Height) - TaskBarHeight;

  if not done then begin
     // I want to have the application show its window, then start loading the webpage.
     PostMessage(Handle, WM_ONSHOWCALLBACK, 0, 0);
     done := true;
  end;
  while Chromium1.Browser.IsLoading do Application.ProcessMessages;
end;

function TUzenetTopDlg.TaskBarHeight: integer;
  var
    hTB: HWND; // taskbar handle
    TBRect: TRect; // taskbar rectangle
  begin
    hTB:= FindWindow('Shell_TrayWnd', '');
    if hTB = 0 then
      Result := 0
    else begin
      GetWindowRect(hTB, TBRect);
      Result := TBRect.Bottom - TBRect.Top;
    end;
  end;

procedure TUzenetTopDlg.KontaktLabelClick(Sender: TObject);
begin
   ShowThread('kontakt', (Sender as TLabel).Tag);
end;

procedure TUzenetTopDlg.KontaktPanelClick(Sender: TObject);
begin
   ShowThread('kontakt', (Sender as TPanel).Tag);
end;

procedure TUzenetTopDlg.Label2Click(Sender: TObject);
var
  DOKOD, Lista: string;
begin
	TobbDolgozoValaszto2(M33, M34, True); // arch�v dolgoz�k nem kellenek
  if M33.text<> '' then begin
    ebCCNevsor.Text:= 'CC: '+M34.text;
    ebCCNevsor.Visible:= True;
    end
  else begin
    ebCCNevsor.Visible:= False;
    end;
end;

{procedure TUzenetTopDlg.KontaktGradientClick(Sender: TObject);
begin
   ShowThread('kontakt', (Sender as TGradient).Tag);
end;}

procedure TUzenetTopDlg.ThreadsLabelClick(Sender: TObject);
begin
   ShowThread('threads', (Sender as TLabel).Tag);
end;

procedure TUzenetTopDlg.ThreadsPanelClick(Sender: TObject);
begin
   ShowThread('threads', (Sender as TPanel).Tag);
end;


{procedure TUzenetTopDlg.ThreadsGradientClick(Sender: TObject);
begin
   ShowThread('threads', (Sender as TGradient).Tag);
end;}

procedure TUzenetTopDlg.memoMessageKeyPress(Sender: TObject; var Key: Char);
begin
   if ord(Key) = VK_RETURN then begin
      UzenetKuld(memoMessage.Text, '', '', '');
      Key:= chr(0);  // elnyelj�k az Entert
      end;
end;

procedure TUzenetTopDlg.mmiSzalTorolClick(Sender: TObject);
var
   Res, MessageID: string;
begin
  MessageID:='';
  if PopupMenu1.PopupComponent is TMyPanel then begin
      MessageID:= (PopupMenu1.PopupComponent as TMyPanel).message_id;
      end;
  if PopupMenu1.PopupComponent is TMyLabel then begin
      MessageID:= (PopupMenu1.PopupComponent as TMyLabel).message_id;
      end;
  if (MessageID <> '') then begin
      if NoticeKi('Biztosan t�r�lni szeretn� ezt a besz�lget�st?', NOT_QUESTION) = 0 then begin
         Res:= RESTAPIKozosDlg.DeleteThread(MessageID);
         if Res = '' then begin
            RefreshThreads(True);
            end
         else begin
            NoticeKi('Az �zenet t�rl�s nem siker�lt (' + Res + ')', NOT_QUESTION);
            end;
         end; // if
      end;  // if
end;

procedure TUzenetTopDlg.ShowThread(const Honnan: string; const ASorszam: integer);
begin
  if Honnan = 'kontakt' then begin
    if (ContactIDArray[ASorszam-1].which = 'user') and (ContactIDArray[ASorszam-1].id = ActXuid) then begin
      NoticeKi('Nem adhatja hozz� saj�t mag�t a besz�lget�shez.');
      Exit;
      end
    else begin
      ActiveThread:= ContactIDArray[ASorszam-1];
      end; // else
    end;
  if Honnan = 'threads' then
     ActiveThread:= ThreadsContactIDArray[ASorszam-1];
  SetActivePage(2);  // friss�t is
end;

procedure TUzenetTopDlg.Panel15Click(Sender: TObject);
begin
   SetActivePage(3);
end;

procedure TUzenetTopDlg.Panel17Click(Sender: TObject);
begin
   SetActivePage(1);
end;

procedure TUzenetTopDlg.Panel19Click(Sender: TObject);
begin
   SetActivePage(1);
end;

procedure TUzenetTopDlg.Panel4Click(Sender: TObject);
begin
   SetActivePage(2);
end;


procedure TUzenetTopDlg.pnlSwitch1Elem1Click(Sender: TObject);
begin
  SetActiveSwitch(1,1);
  RefreshThreads(False);
end;

procedure TUzenetTopDlg.pnlSwitch1Elem2Click(Sender: TObject);
begin
  SetActiveSwitch(1,2);
  RefreshThreads(False);
end;

procedure TUzenetTopDlg.pnlSwitch2Elem1Click(Sender: TObject);
begin
  SetActiveSwitch(2,1);
  RefreshContacts(False);
end;

procedure TUzenetTopDlg.pnlSwitch2Elem2Click(Sender: TObject);
begin
  SetActiveSwitch(2,2);
  RefreshContacts(False);
end;

procedure TUzenetTopDlg.SetActivePage(const PageNumber: integer);
//const
  // FormBorderWidth = 18;   // 323 - 305
  // FormBorderWidth = 0;   // nincs border
var
  i, FormBorderWidth: integer;
begin
  FormBorderWidth:= Width - ClientWidth;
  Megbizaskod:= ''; // t�r�lni
  // el�bb az inform�ci� friss�t�s
  if PageNumber=1 then
     RefreshThreads(True);
  if PageNumber=2 then begin
    ebSzalbanKeres.Text:= '';
    RefreshThread(ActiveThread, '', False);
    end;
   if PageNumber=3 then begin
     RefreshContacts(True);
     end;
   // oldal v�lt�s
   for i:=1 to NumberOfPages do begin
     PageArray[i].Visible:= (i=PageNumber);
     end;
   ActivePage:= PageNumber;
   Width:= PageArray[PageNumber].Width + FormBorderWidth;
   Constraints.MinWidth:= Width;
   Constraints.MaxWidth:= Width; // set exact size
   if ActivePage=2 then begin
      // memoMessage.Lines.Clear;
      // invisible control???
      // memoMessage.SetFocus;
      end;
end;

procedure TUzenetTopDlg.SetActiveSwitch(const SwitchNumber, ActiveNumber: integer);
var
  i: integer;
begin
   for i:=1 to 2 do begin
     if (i=ActiveNumber) then begin
       SwitchArray[SwitchNumber, i].Color:= Switch_ActiveColor;
       SwitchArray[SwitchNumber, i].Font.Color:= clWhite;
       end
     else begin
       SwitchArray[SwitchNumber, i].Color:= Switch_NeutralColor;
       SwitchArray[SwitchNumber, i].Font.Color:= clBlack;
       end;  // else
     // debug ... SwitchArray[SwitchNumber, i].Caption:= IntToStr(SwitchNumber)+' / '+IntToStr(i);
     // SwitchArray[SwitchNumber, i].Refresh;
     end;  // for
end;

function TUzenetTopDlg.GetActiveSwitch(const SwitchNumber: integer): integer;
var
  i: integer;
begin
   for i:=1 to 2 do begin
     if SwitchArray[SwitchNumber, i].Color = Switch_ActiveColor then begin
       Result:= i;
       Break;
       end;
     end;  // for
end;

procedure TUzenetTopDlg.btnPrevMessagesClick(Sender: TObject);
begin
   RefreshThread(ActiveThread, '', True);
end;

procedure TUzenetTopDlg.btnSzalbanKeresClick(Sender: TObject);
begin
   if Tartalmaz(ebSzalbanKeres.text, ' ') then begin
      NoticeKi('Nem lehet benne sz�k�z!');
      Exit;
      end;
   RefreshThread(ActiveThread, ebSzalbanKeres.text, False);
end;

procedure TUzenetTopDlg.RefreshActThread; // ezt h�vhatja a ChatControl, hogy �nmag�t friss�tse
begin
   RefreshThread(ActiveThread, '', False);
end;

procedure TUzenetTopDlg.RefreshThread(const ContactRecord: TContactRecord; const KeresMinta: string; const IfKorabbiak: boolean);
const
   ChatsResourceGroup = 'messages/group/%s';  // 'messages/group/:xgid'
   ChatsResourceUser = 'messages/private/%s';  // 'messages/private/:peer_xuid'
   ChatsResourceKeres = 'messages/search';
   ChatsResourcePrev = 'messages/%s/prev';   // /messages/:msg_id/prev
   BelsoSzep = '^';
   constUzenetStatusTorolt = 'T';
   constUzenetStatusNormal = 'N';
var
  i, Sorszam: integer;
  jsValasz, jsListaElem, jsElemKuldo, jsElemAttachment: TJSONValue;
  jsMessageLista: TJSONArray;
  jsMessage: TJSONObject;
  MessageSide: TUser;
  ResourceS, KuldoNeve, KuldoXID, UzenetSzoveg, Elkuldve, Res, S, ResponseJSON: string;
  MessageID, UtolsoMessageID, AttachmentTipus, AttachmentURL, ThumbURL: string;
  ElemTimeStamp, ActMessageID, UzenetStatusz: string;
  JSONResult: TJSONResult;
  ChatControlLatestTop: integer;
begin
   // -------- elemek t�rl�se ------------ //
   ChatControl1.Clear;
   // -------- lek�rdez�s ------------------ //
   ActiveThread:= ContactRecord;
   lblActThread.Caption:= ActiveThread.name;
   // nem kell, mert a "Threads" lek�rdez�s m�r beleteszi
   {if (ContactRecord.which= 'user') then
      if (RESTAPIKozosDlg.UserStatusList[ContactRecord.id] = 'P') then
         lblActThread.Caption:= lblActThread.Caption + '(pihen�)';
         }
   Screen.Cursor:= crHourGlass;
   if KeresMinta = '' then begin
     if IfKorabbiak then begin
       ResourceS:= Format(ChatsResourcePrev, [ActThreadMinMessageID]);
       ChatControlLatestTop:= ChatControl1.GetContentHeight;
       end
     else begin
       if ContactRecord.which= 'group' then ResourceS:= Format(ChatsResourceGroup, [ContactRecord.id]);
       if ContactRecord.which= 'user' then ResourceS:= Format(ChatsResourceUser, [ContactRecord.id]);
       ChatControlLatestTop:= 0;  // nem kell g�rgetni friss�t�s ut�n
       end;
     JSONResult:= RESTAPIKozosDlg.ExecRequest(ResourceS, 'get', RESTAPIKozosDlg.EmptyParamsList);
     ActThreadMinMessageID:= '';
     end
   else begin
     ResourceS:= ChatsResourceKeres;
     RESTAPIKozosDlg.RESTAPIParamsList.Clear;
     RESTAPIKozosDlg.RESTAPIParamsList.Add('q', KeresMinta);
     RESTAPIKozosDlg.RESTAPIParamsList.Add('msg_id', ActThreadMinMessageID); // tetsz�leges msg_id a sz�lon bel�l

     JSONResult:= RESTAPIKozosDlg.ExecRequest(ResourceS, 'get', RESTAPIKozosDlg.RESTAPIParamsList);
     ActThreadMinMessageID:= '';
     ChatControlLatestTop:= 0;  // nem kell g�rgetni friss�t�s ut�n
     end;
   if JSONResult.ErrorString <> '' then begin
        S:='Hiba a lek�rdez�s sor�n! K�d: '+ JSONResult.ErrorString;
        MessageDlg( S, mtError,[mbOK],0);
        // NoticeKi(S);
        Screen.Cursor:= crDefault;
        Exit; // function
        end;
   jsMessageLista:= (JSONResult.JSONContent as TJSONArray);
   // ----------- sorrendez�s -------------- //
   if not IfKorabbiak then
       lbSorrendbe.Items.Clear;  // ha kor�bbi �zeneteket k�r�nk le, akkor nem t�rl�m a m�r lek�rteket
   for jsListaElem in jsMessageLista do begin
     // !!!! megn�zni: ha m�r nem m�s a response szerkezete, akkor �sszevonni ezt a k�t �gat
     if ContactRecord.which= 'group' then begin  // m�s a response szerkezet
        ElemTimeStamp:= ((jsListaElem as TJSONObject).Get('created_at').JsonValue as TJSONString).Value;
        jsMessage:= (jsListaElem as TJSONObject).Get('Message').JsonValue as TJSONObject;
        if (jsMessage as TJSONObject).Get('text').JsonValue is TJSONNull then
           UzenetSzoveg:= ''
        else UzenetSzoveg:= ((jsMessage as TJSONObject).Get('text').JsonValue as TJSONString).Value;
        if (jsMessage as TJSONObject).Get('deleted').JsonValue is TJSONTrue then
            UzenetStatusz:= constUzenetStatusTorolt
        else UzenetStatusz:= constUzenetStatusNormal;
        jsElemKuldo:= (jsMessage as TJSONObject).Get('sender').JsonValue as TJSONObject;
        MessageID:= ((jsMessage as TJSONObject).Get('id').JsonValue as TJSONString).Value;
        KuldoNeve:= ((jsElemKuldo as TJSONObject).Get('name').JsonValue as TJSONString).Value;
        KuldoXID:= ((jsElemKuldo as TJSONObject).Get('xuid').JsonValue as TJSONString).Value;
        jsElemAttachment:= (jsMessage as TJSONObject).Get('Attachment').JsonValue;
        if not(jsElemAttachment is TJSONNull) then begin
          AttachmentTipus:= ((jsElemAttachment as TJSONObject).Get('resource_type').JsonValue as TJSONString).Value;
          AttachmentURL:= ((jsElemAttachment as TJSONObject).Get('resource_url').JsonValue as TJSONString).Value;
          ThumbURL:= ((jsElemAttachment as TJSONObject).Get('thumbnail').JsonValue as TJSONString).Value;
          end  // if
        else begin
          AttachmentTipus:= '';
          AttachmentURL:= '';
          ThumbURL:= '';
          end; // else
        end;
     if ContactRecord.which= 'user' then begin  // m�s a response szerkezet
        ElemTimeStamp:= ((jsListaElem as TJSONObject).Get('created_at').JsonValue as TJSONString).Value;
        jsMessage:= (jsListaElem as TJSONObject).Get('Message').JsonValue as TJSONObject;
        if (jsMessage as TJSONObject).Get('text').JsonValue is TJSONNull then
           UzenetSzoveg:= ''
        else UzenetSzoveg:= ((jsMessage as TJSONObject).Get('text').JsonValue as TJSONString).Value;
        MessageID:= ((jsMessage as TJSONObject).Get('id').JsonValue as TJSONString).Value;
        if (jsMessage as TJSONObject).Get('deleted').JsonValue is TJSONTrue then
            UzenetStatusz:= constUzenetStatusTorolt
        else UzenetStatusz:= constUzenetStatusNormal;
        jsElemKuldo:= (jsMessage as TJSONObject).Get('sender').JsonValue as TJSONObject;
        KuldoNeve:= ((jsElemKuldo as TJSONObject).Get('name').JsonValue as TJSONString).Value;
        KuldoXID:= ((jsElemKuldo as TJSONObject).Get('xuid').JsonValue as TJSONString).Value;
        jsElemAttachment:= (jsMessage as TJSONObject).Get('Attachment').JsonValue;
        if not(jsElemAttachment is TJSONNull) then begin
          AttachmentTipus:= ((jsElemAttachment as TJSONObject).Get('resource_type').JsonValue as TJSONString).Value;
          AttachmentURL:= ((jsElemAttachment as TJSONObject).Get('resource_url').JsonValue as TJSONString).Value;
          ThumbURL:= ((jsElemAttachment as TJSONObject).Get('thumbnail').JsonValue as TJSONString).Value;
          end  // if
        else begin
          AttachmentTipus:= '';
          AttachmentURL:= '';
          ThumbURL:= '';
          end; // else
        end;
      lbSorrendbe.Items.Add(ElemTimeStamp+BelsoSzep+UzenetSzoveg+BelsoSzep+KuldoNeve+BelsoSzep+KuldoXID+BelsoSzep+MessageID+
        BelsoSzep+UzenetStatusz+BelsoSzep+AttachmentTipus+BelsoSzep+ThumbURL+BelsoSzep+AttachmentURL);
      end;
   // ----------- megjelen�t�s -------------- //
   for i:=0 to lbSorrendbe.Items.Count-1 do begin  // �gy lesznek n�vekv� sorrendben a panelen
      S:= lbSorrendbe.Items[i];
      if i = 0 then
        ActThreadMinMessageID:= RESTAPIKozosDlg.GetListOp(S, 4, BelsoSzep);
      Elkuldve:= RESTAPIKozosDlg.GetListOp(S, 0, BelsoSzep);
      Elkuldve:= RESTAPIKozosDlg.RESTTimeStampFormat(Elkuldve);
      UzenetSzoveg:= RESTAPIKozosDlg.GetListOp(S, 1, BelsoSzep);
      KuldoNeve:= RESTAPIKozosDlg.GetListOp(S, 2, BelsoSzep);
      KuldoXID:= RESTAPIKozosDlg.GetListOp(S, 3, BelsoSzep);
      ActMessageID:= RESTAPIKozosDlg.GetListOp(S, 4, BelsoSzep);
      UzenetStatusz:= RESTAPIKozosDlg.GetListOp(S, 5, BelsoSzep);
      AttachmentTipus:= RESTAPIKozosDlg.GetListOp(S, 6, BelsoSzep);
      ThumbURL:= RESTAPIKozosDlg.GetListOp(S, 7, BelsoSzep);
      AttachmentURL:= RESTAPIKozosDlg.GetListOp(S, 8, BelsoSzep);
      if i = lbSorrendbe.Items.Count-1 then
        UtolsoMessageID:= RESTAPIKozosDlg.GetListOp(S, 4, BelsoSzep);
      if KuldoXID = Actxuid then begin
          MessageSide:=User2
          end
      else begin
          MessageSide:=User1;  // bal oldal a k�ld�s, jobb a v�lasz (a felhaszn�l� szemsz�g�b�l)
          ChatControl1.Say(MessageSide, 'sender:'+'0'+BelsoSzep+KuldoNeve);  // csak a bal oldalon kell k�ld�
          end;
      if UzenetStatusz = constUzenetStatusTorolt then
        ChatControl1.Say(MessageSide, 'deleted:'+ActMessageID+BelsoSzep+'** T�r�lt �zenet **'); // t�r�lt �zenetnek nincs sz�vege
      if (UzenetStatusz = constUzenetStatusNormal) and (UzenetSzoveg <> '') then
        // ChatControl1.Say(MessageSide, UzenetSzoveg);
        ChatControl1.Say(MessageSide, 'text:'+ActMessageID+BelsoSzep+UzenetSzoveg);
      if (AttachmentTipus <> '') and (UzenetStatusz = constUzenetStatusNormal) then
        ChatControl1.Say(MessageSide, 'attachment:'+ActMessageID+BelsoSzep+AttachmentTipus+BelsoSzep+ThumbUrl+BelsoSzep+AttachmentURL);
      ChatControl1.Say(MessageSide, 'timestamp:'+'0'+BelsoSzep+Elkuldve);
      ChatControl1.Say(MessageSide, 'timestamp:'+'0'+BelsoSzep+'      '); // csak egy kis sornyi sz�net az �zenetek k�z�tt
      end;  // for
   ChatControl1.ShowChat;
   if ChatControlLatestTop>0 then
     ChatControl1.ScrollTo(ChatControlLatestTop);
   // ---------- set "read" flag --------
   SetOlvasva(UtolsoMessageID);
   Screen.Cursor:= crDefault;
end;

procedure TUzenetTopDlg.SetOlvasva(const AMessageID: string);
const
   APIResource = 'messages/read_so_far/%s';  // 'messages/read_so_far/:msgid'
var
  i, Sorszam: integer;
  jsValasz, jsListaElem: TJSONValue;
  jsEgyediLista, jsCsoportLista: TJSONArray;
  Neve, Fajta, Kod, Res, S, ResponseJSON, ElemNeve, ElemID, ActKezdobetu, PrevKezdobetu: string;
  KeresMinta: string;
  KellElvalaszto, SzuresOK, SzuresKell: boolean;
  JSONResult: TJSONResult;
begin
   if AMessageID <>'' then begin
     Screen.Cursor:= crHourGlass;
     JSONResult:= RESTAPIKozosDlg.ExecRequest(Format(APIResource, [AMessageID]), 'post', RESTAPIKozosDlg.EmptyParamsList);
     if JSONResult.ErrorString <> '' then begin
          S:='Hiba a k�r�s sor�n! K�d: '+ JSONResult.ErrorString;
          MessageDlg( S, mtError,[mbOK],0);
          // NoticeKi(S);
          Screen.Cursor:= crDefault;
          Exit; // function
          end;
      Screen.Cursor:= crDefault;
      end;  // if
end;


procedure TUzenetTopDlg.RefreshContacts(const ASendRequest: boolean);
const
   ContactsResource = 'users/contacts';  // 'users/contacts'
var
  i, Sorszam: integer;
  jsValasz, jsListaElem, jsCeg: TJSONValue;
  jsEgyediLista, jsCsoportLista: TJSONArray;
  Neve, Ceg, Fajta, Kod, Res, S, ResponseJSON, ElemNeve, ElemID, ActKezdobetu, PrevKezdobetu: string;
  KeresMinta, ElemStatus, EnVagyok, AvatarUrl, AvatarCacheID: string;
  KellElvalaszto, SzuresOK, SzuresKell, KategoriaOK, VanEgyediLista, VanGroupLista: boolean;
  JSONResult: TJSONResult;
begin
   Screen.Cursor:= crHourGlass;
   // Writelog_NoThread('DEBUG.TXT', 'RefreshContacts START', True);
   // -------- elemek t�rl�se ------------ //
   while ScrollBox2.ControlCount > 0 do ScrollBox2.Controls[0].Free;  // az �sszes al-panel t�rl�se
   SetLength(ContactIDArray, 0);
   // ---------- lek�rdez�s ------------------ //
   if ASendRequest then begin
       lbSorrendbe.Items.Clear; // csak itt t�r�lj�k
       JSONResult:= RESTAPIKozosDlg.ExecRequest(Format(ContactsResource, []), 'get', RESTAPIKozosDlg.EmptyParamsList);
       if JSONResult.ErrorString <> '' then begin
            S:='Hiba a lek�rdez�s sor�n! K�d: '+ JSONResult.ErrorString;
            MessageDlg( S, mtError,[mbOK],0);
            // NoticeKi(S);
            Screen.Cursor:= crDefault;
            Exit; // function
            end;
       jsValasz:= JSONResult.JSONContent;
       if not ((jsValasz as TJSONObject).Get('contacts').JsonValue is TJSONNull) then begin
         jsEgyediLista:= (jsValasz as TJSONObject).Get('contacts').JsonValue as TJSONArray;
         VanEgyediLista:= True;
         end
       else VanEgyediLista:= False;
       if not ((jsValasz as TJSONObject).Get('groups').JsonValue is TJSONNull) then begin
         jsCsoportLista:= (jsValasz as TJSONObject).Get('groups').JsonValue as TJSONArray;
         VanGroupLista:= True;
         end
       else VanGroupLista:= False;
       // GroupIDList.Clear;
       // ----------- sorrendez�s -------------- //
       if VanEgyediLista then begin
         for jsListaElem in jsEgyediLista do begin
              ElemNeve:= ((jsListaElem as TJSONObject).Get('name').JsonValue as TJSONString).Value;
              ElemID:= ((jsListaElem as TJSONObject).Get('xuid').JsonValue as TJSONString).Value;
              ElemStatus:= ((jsListaElem as TJSONObject).Get('status').JsonValue as TJSONString).Value;
              if ElemID = ActXuid then
                 EnVagyok:= ' (�n)'
              else EnVagyok:= '';
              AvatarUrl:= ((jsListaElem as TJSONObject).Get('gravatar').JsonValue as TJSONString).Value;
              AvatarCacheID:= 'img:uava:'+ElemID;
              if (jsListaElem as TJSONObject).Get('Domain').JsonValue is TJSONNull then
                Ceg:= ''
              else begin
                jsCeg:= (jsListaElem as TJSONObject).Get('Domain').JsonValue as TJSONObject;
                Ceg:= ((jsCeg as TJSONObject).Get('name').JsonValue as TJSONString).Value;
                end;
              lbSorrendbe.Items.Add(ElemNeve+EnVagyok+'|user|'+ElemID+'|'+ElemStatus+'|'+AvatarUrl+'|'+AvatarCacheID+'|'+Ceg);
              end; // for
         end; // if
       if VanGroupLista then begin
         for jsListaElem in jsCsoportLista do begin
            ElemNeve:= ((jsListaElem as TJSONObject).Get('name').JsonValue as TJSONString).Value;
            ElemID:= ((jsListaElem as TJSONObject).Get('xgid').JsonValue as TJSONString).Value;
            AvatarUrl:= ((jsListaElem as TJSONObject).Get('avatar_url').JsonValue as TJSONString).Value;
            AvatarCacheID:= 'img:gava:'+ElemID;
            Ceg:= '';
            lbSorrendbe.Items.Add(ElemNeve+'|group|'+ElemID+'|'+'A'+'|'+AvatarUrl+'|'+AvatarCacheID+'|'+Ceg);  // group is always active
            // GroupIDList.Add(ElemID, ...);
            end; // for
         end;  // if
       end; // if ASendRequest
   // ---------------- megjelen�t�s --------------- //
   PrevKezdobetu:= '';
   if (ebKontaktKeres.Text = KeresAlapStr) or (ebKontaktKeres.Text = '') then begin
       KeresMinta := '';
       SzuresKell:= False;
       end
   else begin
       KeresMinta := LowerCase(ebKontaktKeres.Text);
       SzuresKell:= True;
       end;
   // for i:=lbSorrendbe.Items.Count-1 downto 0 do begin  // �gy lesznek n�vekv� sorrendben a panelen
   KontaktTopPosition:= 0;
   for i:=0 to lbSorrendbe.Items.Count-1 do begin  // most m�r �gy lesznek n�vekv� sorrendben a panelen
     S:= lbSorrendbe.Items[i];
     Neve:= RESTAPIKozosDlg.GetListOp(S, 0, '|');
     Fajta:= RESTAPIKozosDlg.GetListOp(S, 1, '|');
     Kod:= RESTAPIKozosDlg.GetListOp(S, 2, '|');
     ElemStatus:= RESTAPIKozosDlg.GetListOp(S, 3, '|');
     AvatarUrl:= RESTAPIKozosDlg.GetListOp(S, 4, '|');
     AvatarCacheID:= RESTAPIKozosDlg.GetListOp(S, 5, '|');
     Ceg:= RESTAPIKozosDlg.GetListOp(S, 6, '|');
     if ElemStatus = 'P' then Neve:= Neve+ ' (pihen�)';
     KategoriaOK:= ((GetActiveSwitch(2) = 1) and (Fajta='user')) or (Fajta='group'); // egy�ni sz�lak: csak ha az "�sszes" van kiv�lasztva
     SzuresOK:= not(SzuresKell) or EgyebDlg.Mintailleszt(KeresMinta, Neve);
     if KategoriaOK and SzuresOK then begin
         SetLength(ContactIDArray, Length(ContactIDArray)+1);
         Sorszam:= Length(ContactIDArray); //
         ContactIDArray[Sorszam-1].which:= Fajta;
         ContactIDArray[Sorszam-1].id:= Kod;
         ContactIDArray[Sorszam-1].name:= Neve;
         // Writelog_NoThread('DEBUG.TXT', 'New contact: '+ Neve, True);
         ActKezdobetu:= RESTAPIKozosDlg.GetMagyarKezdobetu(Neve);
         if ActKezdobetu <> PrevKezdobetu then begin
            AddKontaktPanel(ActKezdobetu, '', False, -1, '', '');
            PrevKezdobetu:= ActKezdobetu;
            KellElvalaszto:= False;
            end;
         AddKontaktPanel(Neve, Ceg, KellElvalaszto, Sorszam, AvatarUrl, AvatarCacheID);
         KellElvalaszto:= True;
        end;
     end;  // for
     // AddKontaktPanel(PrevKezdobetu, KellElvalaszto, -1); // ez lesz legfel�l, az els� elem bet�j�vel
    // Writelog_NoThread('DEBUG.TXT', 'RefreshContacts END', True);
    Screen.Cursor:= crDefault;
end;

{procedure TUzenetTopDlg.MyCreatePanelGradient(Elem: TPanel; i_GradientStyle: TGradientStyle; i_Size, i_Tag: integer; ClickMode: string);
  begin
   with TGradient.Create(Elem) do begin
      Parent:= Elem;
      Align:= alClient;
      Style:= i_GradientStyle;
      ColorBegin:= clWhite;
      ColorEnd:= clGradientInactiveCaption;
      if i_Tag>=0 then begin
        Tag:= i_Tag;
        if ClickMode = 'kontakt' then OnClick:= KontaktGradientClick;
        if ClickMode = 'threads' then OnClick:= ThreadsGradientClick;
        end;  // if
      end; // with
   end;
 }

procedure TUzenetTopDlg.MyCreatePanelLabel(Elem: TPanel; i_Left, i_Top, i_Width, i_Height: integer; i_Caption: string;
      i_IsBold: boolean; i_Color, i_BackColor: TColor; i_Size, i_Tag: integer; ClickMode, MessageID: string; AAlignment: TAlignment = taLeftJustify);
  begin
   with TMyLabel.Create(Elem) do begin
      Parent:= Elem;
      AutoSize:= False;  // nehogy egym�sra l�gjanak
      Transparent:= True;  // a panel l�tsszon alatta
      Wordwrap:= True;
      // Caption:= i_Caption;
      Caption:= StringReplace(i_Caption, '&','&&', [rfReplaceAll]) ;
      Left:= i_Left;
      Top:= i_Top;
      Width:= i_Width;
      Height:= i_Height;
      Color:= i_BackColor;
      Font.Size:= i_Size;
      Alignment:= AAlignment;
      if i_IsBold then Font.Style:= [fsBold]; // alap�rtelmezett a "norm�l"
      Font.Color:= i_Color;
      message_id:= MessageID;
      if i_Tag>=0 then begin
        Tag:= i_Tag;
        if ClickMode = 'kontakt' then OnClick:= KontaktLabelClick;
        if ClickMode = 'threads' then begin
           OnClick:= ThreadsLabelClick;
           Popupmenu:= PopupMenu1;
           end;
        end;
      end; // with
   end;

procedure TUzenetTopDlg.MyCreatePanelMemo(Elem: TPanel; i_Left, i_Top, i_Width, i_Height: integer; i_Text: string;
        i_IsBold: boolean; i_Color, i_BackColor: TColor; i_Size, i_Tag: integer; ClickMode: string);
  begin
   with TMemo.Create(Elem) do begin
      Parent:= Elem;
      Lines.Add(i_Text);
      Left:= i_Left;
      Top:= i_Top;
      Width:= i_Width;
      Height:= i_Height;
      Color:= i_BackColor;
      Font.Size:= i_Size;
      if i_IsBold then Font.Style:= [fsBold]; // alap�rtelmezett a "norm�l"
      Font.Color:= i_Color;
      if i_Tag>=0 then begin
        Tag:= i_Tag;
        if ClickMode = 'kontakt' then OnClick:= KontaktLabelClick;
        if ClickMode = 'threads' then OnClick:= ThreadsLabelClick;
        end;
      end; // with
   end;

procedure TUzenetTopDlg.MyCreatePanelPanel(Elem: TPanel; i_Left, i_Top, i_Width, i_Height: integer; i_Caption: string;
      i_IsBold: boolean; i_Color, i_BackColor: TColor);
  var
    MyPanel: TPanel;
  begin
   MyPanel:= TPanel.Create(Elem);
   with MyPanel do begin
      Parent:= Elem;
      AutoSize:= False;  // nehogy egym�sra l�gjanak
      Caption:='';
      BevelInner:= bvNone;
      BevelOuter:= bvLowered;
      Left:= i_Left;
      Top:= i_Top;
      Width:= i_Width;
      Height:= i_Height+2;
      Color:= i_BackColor;
      end;
   if i_Caption <> '' then begin
     with TLabel.Create(MyPanel) do begin
        Parent:= MyPanel;
        Align:= alClient;
        // AutoSize:= False;  // nehogy egym�sra l�gjanak
        Caption:= i_Caption;
        Transparent:= True;
        Left:=0;
        Top:=0;
        Font.Size:=10;
        if i_IsBold then Font.Style:= [fsBold]; // alap�rtelmezett a "norm�l"
        Font.Color:= i_Color;
        end; // with
     end; // if
   end;

procedure TUzenetTopDlg.MyCreatePanelHorLine(Elem: TPanel; i_Left, i_Top, i_Width: integer; i_BackColor: TColor);
  var
    MyShape: TShape;
  begin
   MyShape:= TShape.Create(Elem);
   with MyShape do begin
      Parent:= Elem;
      Left:= i_Left;
      Top:= i_Top;
      Width:= i_Width;
      Height:= 1;
      Pen.Color:= i_BackColor;
      // Refresh;
      end;
   end;

{procedure TUzenetTopDlg.MyCreatePanelHorLine(Elem: TPanel; i_Left, i_Top, i_Width: integer; i_BackColor: TColor);
  var
    MyPanel: TPanel;
  begin
   MyPanel:= TPanel.Create(Elem);
   with MyPanel do begin
      Parent:= Elem;
      // AutoSize:= False;  // nehogy egym�sra l�gjanak
      Caption:='';
      // BevelEdges:= [];
      //BevelInner:= bvNone;
      //BevelOuter:= bvNone;
      //BorderStyle:= bsNone;
      Left:= i_Left;
      Top:= i_Top;
      Width:= i_Width;
      Height:= 8;
      Color:= i_BackColor;
      Refresh;
      end;
   end;}

procedure TUzenetTopDlg.MyCreatePanelImage1(Elem: TPanel; i_Left, i_Top, i_Width, i_Height: integer);
begin
 with TImage.Create(Elem) do begin
    Parent:= Elem;
    Autosize:= False;
    Stretch:= False;
    Transparent:= True;
    Left:= i_Left;
    Top:= i_Top;
    Width:= i_Width;
    Height:= i_Height;
    Picture:= imgPont.Picture;
    // Tag:= i_Tag;
    // OnClick:= KontaktImageClick;
    end; // with
 end;

procedure TUzenetTopDlg.MyCreatePanelAvatar(Elem: TPanel; i_Left, i_Top, i_Width, i_Height: integer; const ImageURL, ImageCacheID: String);
var
  S: string;
  newImage: TImage;
  KellGeneric: boolean;
begin
 newImage:=  TImage.Create(Elem);
 KellGeneric:= True;
 if (ImageURL <> '') then begin
   S:= RESTAPIKozosDlg.GetURLPicture(newImage, ImageURL, 'image/jpg', ImageCacheID);
   if (S = '') then begin // sikeres volt a bet�lt�s
      KellGeneric:= False;
      end; // if
   end; // if
 if KellGeneric then begin
   newImage.Picture:= imgGenericAvatar.Picture;
   end;
 with newImage do begin
    Parent:= Elem;
    Autosize:= False;
    Stretch:= True;
    Transparent:= True;
    Left:= i_Left;
    Top:= i_Top;
    Width:= i_Width;
    Height:= i_Height;
    end; // with
 end;

procedure TUzenetTopDlg.AddKontaktPanel(const ANeve, ACeg: string; const KellLine: boolean; const ASorszam: integer;
        const AvatarUrl, AvatarCacheID: string);
var
  Elem, GaugePanel: TPanel;
  MBInfo1, MBInfo2, S: string;
  MBKod1, MBKod2: integer;
  MyImage, MyWarningImage: TImage;
begin
   Elem:= TPanel.Create(Scrollbox2);
   // Writelog_NoThread('DEBUG.TXT', 'AddKontaktPanel 1 '+ ANeve, True);
   with Elem do begin
      Parent:= Scrollbox2;
      if ASorszam >= 0 then begin
        if KellLine then
            Height:= 50 // 35
        else Height:= 45;  // 27;
        Tag:= ASorszam;
        // Color:= colorVilagosSzurke;
        OnClick:= KontaktPanelClick;
        end  // if
      else begin
        Height:=20;
        // Color:= colorNevsorSzeparator;
        // Color:= clGreen;  // hogy l�tsszon
        end; // else
      // Align:= alTop;
      // Writelog_NoThread('DEBUG.TXT', 'AddKontaktPanel 2 '+ ANeve, True);
      Top:= KontaktTopPosition;
      KontaktTopPosition:= Top+Height+3;
      Width:= Scrollbox2.Width - 22;  // - border - scrollbar
      Color:= colorVilagosSzurke;
      BevelInner:= bvNone;
      BevelOuter:= bvNone;
      bevelKind:= bkNone;
      BevelEdges:= [];
      BorderStyle:= bsNone;
      Caption:='';
      end; // with Elem
   // Writelog_NoThread('DEBUG.TXT', 'AddKontaktPanel 3 '+ ANeve, True);
   // MyCreatePanelGradient(Elem, gsReflectedV, 10, ASorszam);
   if ASorszam >= 0 then begin  // val�di n�vsor elem
     if KellLine then begin
        MyCreatePanelHorLine(Elem, 16, 2, 285, colorVonalKek); // "1" magas panel
        end;
     MyCreatePanelLabel(Elem, 60, Elem.Height - 38, 240, 16, ANeve, True, clWindowText, colorAlapSzurke, 10, ASorszam, 'kontakt', '');
     MyCreatePanelLabel(Elem, 60, Elem.Height - 18, 240, 16, ACeg, False, clWindowText, colorAlapSzurke, 8, ASorszam, 'kontakt', '');
    //  Writelog_NoThread('DEBUG.TXT', 'AddKontaktPanel 4a '+ ANeve, True);
     MyCreatePanelAvatar(Elem, 5, Elem.Height - 42, 40, 40, AvatarUrl, AvatarCacheID);
     end // if ASorszam
   else begin // kezd�bet� fejsor
     with TShape.Create(Elem) do begin
      Parent:= Elem;
      Brush.Color:= colorNevsorSzeparator;
      Align:= alClient;
      end; // with
    // Writelog_NoThread('DEBUG.TXT', 'AddKontaktPanel 4b '+ ANeve, True);
    MyCreatePanelLabel(Elem, 16, 1, 268, 16, ANeve, True, clWindowText, colorVonalKek, 10, -1, 'kontakt', '');
    end; // else
    // Writelog_NoThread('DEBUG.TXT', 'AddKontaktPanel 5 '+ ANeve, True);
end;

procedure TUzenetTopDlg.AddThreadPanel(const ANeve, AUzenet, AUzenetMikor: string; const AVanOlvasatlan: boolean;
    const ASorszam: integer; const AUtolsoUzenetID, AvatarUrl, AvatarCacheID: string);
var
  GaugePanel: TPanel;
  Elem: TMyPanel;
  MBInfo1, MBInfo2, S: string;
  MBKod1, MBKod2: integer;
  MyImage, MyWarningImage: TImage;

begin
   // Elem:= TPanel.Create(Scrollbox1);
   Elem:= TMyPanel.Create(Scrollbox1);
   // Writelog_NoThread('DEBUG.TXT', 'AddThreadPanel 1 '+ ANeve, True);
   with Elem do begin
      Parent:= Scrollbox1;
      Height:=59;
      // Align:= alTop;
      Top:= ThreadsTopPosition;
      Color:= colorVilagosSzurke;
      ThreadsTopPosition:= Top+Height+3;
      Width:= Scrollbox1.Width-22;  // - border - scrollbar
      Caption:='';
      Tag:= ASorszam;
      message_id:= AUtolsoUzenetID;
      BevelEdges:= [];
      BevelInner:= bvNone;
      BevelOuter:= bvNone;
      bevelKind:= bkNone;
      BorderStyle:= bsNone;
      OnClick:= ThreadsPanelClick;
      Popupmenu:= PopupMenu1;
      end; // with
   // Writelog_NoThread('DEBUG.TXT', 'AddThreadPanel 2 '+ ANeve, True);
   // MyCreatePanelGradient(Elem, gsReflectedV, 10, ASorszam);
   MyCreatePanelLabel(Elem, 62, 3, 135, 16, ANeve, True, clWindowText, colorAlapSzurke, 10, ASorszam, 'threads', AUtolsoUzenetID);
   // MyCreatePanelMemo(Elem, 24, 3, 265, 33, AUzenet, False, clWindowText, 10, ASorszam);
   MyCreatePanelLabel(Elem, 62, 19, 200, 33, AUzenet, False, clWindowText, colorAlapSzurke, 10, ASorszam, 'threads', AUtolsoUzenetID);
   MyCreatePanelLabel(Elem, 200, 3, 81, 16, AUzenetMikor, False, clWindowText, colorAlapSzurke, 10, ASorszam, 'threads', AUtolsoUzenetID, taRightJustify);
   MyCreatePanelHorLine(Elem, 24, 55, 271, colorVonalKek);
   // Writelog_NoThread('DEBUG.TXT', 'AddThreadPanel 3 '+ ANeve, True);
   if AVanOlvasatlan then begin
      MyCreatePanelImage1(Elem, 50, 6, 8, 8);
      end;
   // Writelog_NoThread('DEBUG.TXT', 'AddThreadPanel 4 '+ ANeve, True);
   MyCreatePanelAvatar(Elem, 5, 5, 40, 40, AvatarUrl, AvatarCacheID);
   // Writelog_NoThread('DEBUG.TXT', 'AddThreadPanel 5 '+ ANeve, True);
end;

procedure TUzenetTopDlg.RefreshThreads(const ASendRequest: boolean);
const
   ContactsResource = 'messages';
   BelsoSzep = '^';
var
  i, Sorszam: integer;
  jsValasz, jsListaElem: TJSONValue;
  jsEgyediLista, jsCsoportLista: TJSONArray;
  Neve, ElemFajta, Kod, Res, S, ResponseJSON, KuldoNeve, ElemID, D, SenderXuid, PartnerObject, KeresMinta: string;
  AvatarUrl, AvatarCacheID: string;
  ElemUtolsoUzenet, ElemUtolsoUzenetID, ElemUtolsoUzenetMikor, ElemTimestamp, ElemOlvasva, ElemStatus, PartnerNeve: string;
  ElemVanOlvasatlan, SzuresOK, SzuresKell, KategoriaOK: boolean;
  JSONResult: TJSONResult;
begin
   Screen.Cursor:= crHourGlass;
   RestApiKozosDlg.RefreshAllUserStatus;
   // -------- elemek t�rl�se ------------ //
   while ScrollBox1.ControlCount > 0 do ScrollBox1.Controls[0].Free;  // az �sszes al-panel t�rl�se
   SetLength(ThreadsContactIDArray, 0);
   // ------------ lek�rdez�s ------------ //
   if ASendRequest then begin
       lbSorrendbe.Items.Clear;  // csak itt t�r�lj�k
       JSONResult:= RESTAPIKozosDlg.ExecRequest(Format(ContactsResource, []), 'get', RESTAPIKozosDlg.EmptyParamsList);
       if JSONResult.ErrorString <> '' then begin
            S:='Hiba a lek�rdez�s sor�n! K�d: '+ JSONResult.ErrorString;
            MessageDlg( S, mtError,[mbOK],0);
            // NoticeKi(S);
            Screen.Cursor:= crDefault;
            Exit; // function
            end;
       jsValasz:= JSONResult.JSONContent;

       jsEgyediLista:= (jsValasz as TJSONObject).Get('user_messages').JsonValue as TJSONArray;
       jsCsoportLista:= (jsValasz as TJSONObject).Get('group_messages').JsonValue as TJSONArray;
       // ----------- sorrendez�s -------------- //
       for jsListaElem in jsCsoportLista do begin
          ElemTimestamp:= ((jsListaElem as TJSONObject).Get('Message.created_at').JsonValue as TJSONString).Value;
          KuldoNeve:= ((jsListaElem as TJSONObject).Get('Group.name').JsonValue as TJSONString).Value; // ide a csoport ker�l

          if (jsListaElem as TJSONObject).Get('Message.text').JsonValue is TJSONNull then ElemUtolsoUzenet:= ''
          else ElemUtolsoUzenet:= ((jsListaElem as TJSONObject).Get('Message.text').JsonValue as TJSONString).Value;

          if (jsListaElem as TJSONObject).Get('Message.id').JsonValue is TJSONNull then ElemUtolsoUzenetID:= ''
          else ElemUtolsoUzenetID:= ((jsListaElem as TJSONObject).Get('Message.id').JsonValue as TJSONString).Value;

          ElemID:= ((jsListaElem as TJSONObject).Get('Group.xgid').JsonValue as TJSONString).Value;
          if (jsListaElem as TJSONObject).Get('UserQ.read_at').JsonValue is TJSONNull then
            ElemOlvasva:= 'false'
          else ElemOlvasva:= 'true';
          AvatarUrl:= ((jsListaElem as TJSONObject).Get('Group.avatar_url').JsonValue as TJSONString).Value;
          AvatarCacheID:= 'img:gava:'+ElemID;
          lbSorrendbe.Items.Add(ElemTimestamp+BelsoSzep+KuldoNeve+BelsoSzep+'group'+BelsoSzep+ElemID+BelsoSzep+ElemUtolsoUzenet+
             BelsoSzep+ElemOlvasva+BelsoSzep+ElemUtolsoUzenetID+BelsoSzep+AvatarUrl+BelsoSzep+AvatarCacheID);
          end;
       for jsListaElem in jsEgyediLista do begin
            ElemTimestamp:= ((jsListaElem as TJSONObject).Get('Message.created_at').JsonValue as TJSONString).Value;
            SenderXuid:= ((jsListaElem as TJSONObject).Get('Sender.xuid').JsonValue as TJSONString).Value;
            if SenderXuid <> Actxuid then
              PartnerObject:= 'Sender'
            else PartnerObject:= 'Recipient';
            PartnerNeve:= ((jsListaElem as TJSONObject).Get(PartnerObject+'.name').JsonValue as TJSONString).Value;
            ElemStatus:= ((jsListaElem as TJSONObject).Get(PartnerObject+'.status').JsonValue as TJSONString).Value;
            if ElemStatus = 'P' then PartnerNeve:= PartnerNeve+ ' (pihen�)';

            if (jsListaElem as TJSONObject).Get('Message.text').JsonValue is TJSONNull then ElemUtolsoUzenet:= ''
            else ElemUtolsoUzenet:= ((jsListaElem as TJSONObject).Get('Message.text').JsonValue as TJSONString).Value;

            if (jsListaElem as TJSONObject).Get('Message.id').JsonValue is TJSONNull then ElemUtolsoUzenetID:= ''
            else ElemUtolsoUzenetID:= ((jsListaElem as TJSONObject).Get('Message.id').JsonValue as TJSONString).Value;

            ElemID:= ((jsListaElem as TJSONObject).Get(PartnerObject+'.xuid').JsonValue as TJSONString).Value;
            if (SenderXuid <> Actxuid) and ((jsListaElem as TJSONObject).Get('UserQ.read_at').JsonValue is TJSONNull) then
              ElemOlvasva:= 'false'
            else ElemOlvasva:= 'true';

            AvatarUrl:= ((jsListaElem as TJSONObject).Get(PartnerObject+'.gravatar').JsonValue as TJSONString).Value;
            AvatarCacheID:= 'img:uava:'+ElemID;
            if ElemID <> ActXuid then begin // saj�t magammal ne fecsegjek
              lbSorrendbe.Items.Add(ElemTimestamp+BelsoSzep+PartnerNeve+BelsoSzep+'user'+BelsoSzep+ElemID+BelsoSzep+ElemUtolsoUzenet+
                  BelsoSzep+ElemOlvasva+BelsoSzep+ElemUtolsoUzenetID+BelsoSzep+AvatarUrl+BelsoSzep+AvatarCacheID);
              end;  // if
            end;
       end;  // if ASendRequest
   // ---------------- megjelen�t�s --------------- //
   ThreadsTopPosition:= 0;
   if (ebSzalKeres.Text = KeresAlapStr) or (ebSzalKeres.Text = '') then begin
       KeresMinta := '';
       SzuresKell:= False;
       end
   else begin
       KeresMinta := LowerCase(ebSzalKeres.Text);
       SzuresKell:= True;
       end;
   for i:=lbSorrendbe.Items.Count-1 downto 0 do begin  // �gy lesznek cs�kken� sorrendben a panelen
      S:= lbSorrendbe.Items[i];
      ElemTimestamp:= RESTAPIKozosDlg.RESTTimeStampFormat(RESTAPIKozosDlg.GetListOp(S, 0, BelsoSzep)) + ' >';
      KuldoNeve:= RESTAPIKozosDlg.GetListOp(S, 1, BelsoSzep);
      ElemFajta:= RESTAPIKozosDlg.GetListOp(S, 2, BelsoSzep);
      ElemID:= RESTAPIKozosDlg.GetListOp(S, 3, BelsoSzep);
      ElemUtolsoUzenet:= RESTAPIKozosDlg.GetListOp(S, 4, BelsoSzep);
      ElemOlvasva:= RESTAPIKozosDlg.GetListOp(S, 5, BelsoSzep);
      ElemUtolsoUzenetID:= RESTAPIKozosDlg.GetListOp(S, 6, BelsoSzep);
      AvatarUrl:= RESTAPIKozosDlg.GetListOp(S, 7, BelsoSzep);
      AvatarCacheID:= RESTAPIKozosDlg.GetListOp(S, 8, BelsoSzep);
      ElemVanOlvasatlan:= (ElemOlvasva = 'false');
      KategoriaOK:= ((GetActiveSwitch(1) = 1) and (ElemFajta='user')) or (ElemFajta='group'); // egy�ni sz�lak: csak ha az "�sszes" van kiv�lasztva
      SzuresOK:= not(SzuresKell) or EgyebDlg.Mintailleszt(KeresMinta, KuldoNeve+'^'+ElemUtolsoUzenet);  // ha vagy a n�vben, vagy az �zenetben szerepel
      if KategoriaOK and SzuresOK then begin
          SetLength(ThreadsContactIDArray, Length(ThreadsContactIDArray)+1);
          Sorszam:= Length(ThreadsContactIDArray); //
          ThreadsContactIDArray[Sorszam-1].which:= ElemFajta;
          ThreadsContactIDArray[Sorszam-1].id:= ElemID;
          ThreadsContactIDArray[Sorszam-1].name:= KuldoNeve;
          AddThreadPanel(KuldoNeve, ElemUtolsoUzenet, ElemTimestamp, ElemVanOlvasatlan, Sorszam, ElemUtolsoUzenetID, AvatarUrl, AvatarCacheID);
          end;  // id SzuresOK
      end;  // for
   Screen.Cursor:= crDefault;
end;

procedure TUzenetTopDlg.Image2Click(Sender: TObject);
begin
   UzenetKuld(memoMessage.Text, '', '', '');
end;

procedure TUzenetTopDlg.Image3Click(Sender: TObject);
begin
   SendAttachment;
end;

procedure TUzenetTopDlg.SendAttachment;
var
  aOpenDialog : TOpenDialog;
  attachbase64, thumbbase64, attachmime: string;
begin
 aOpenDialog:=TopenDialog.Create(self);
 with aOpenDialog do begin
 		// Filter:='K�p (*.JPG)|*.JPG|K�p (*.PNG)|*.PNG|K�p (*.BMP)|*.BMP|Vide� (*.MPG)|*.MPG|Vide� (*.MP4)|*.MP4|';
    Filter:='K�p (*.JPG)|*.JPG|K�p (*.PNG)|*.PNG|K�p (*.BMP)|*.BMP|Vide� (*.MPG)|*.MPG|Vide� (*.MP4)|*.MP4|'+
            'PDF|*.PDF|Excel|*.XLS;*.XLSX|Word|*.DOC;*.DOCX|';
    Options:=[ofFileMustExist];
		if Execute then begin
        attachmime:= RESTAPIKozosDlg.GetAttachmentMime(Filename);
				RESTAPIKozosDlg.ConvertFileToBase64(Filename, attachmime, attachbase64, thumbbase64);
        // RESTAPIKozosDlg.String2File(thumbbase64, 'D:\temp\thumb-base64.txt');
        MegbizasKod:='';  // ez biztos nem megb�z�shoz megy
        UzenetKuld(memoMessage.Text, attachbase64, thumbbase64, attachmime);
 		  	end;  // if
    end; // with
end;

procedure TUzenetTopDlg.UzenetKuld(const aText, aAttachment,  AThumb, aMimeType: string);
  const
   SendGroupMessage = 'messages/group/%s';  // 'messages/group/:xgid
   SendPrivateMessage = 'messages/private/%s';  // 'messages/private/:peer_xuid'
var
  i: integer;
  BodyKey, BodyValue, S, Res, ResourceS: string;
  JSONResult: TJSONResult;
  LegyenSurgos: boolean;
begin
  if ActiveThread.which= 'group' then ResourceS:= Format(SendGroupMessage, [ActiveThread.id]);
  if ActiveThread.which= 'user' then ResourceS:= Format(SendPrivateMessage, [ActiveThread.id]);
  RESTAPIKozosDlg.RESTAPIParamsList.Clear;
  BodyKey:= 'text';
  BodyValue:= aText;
  RESTAPIKozosDlg.RESTAPIParamsList.Add(BodyKey, BodyValue);
  LegyenSurgos:= False;
  if ActiveThread.which= 'user' then begin
    RESTAPIKozosDlg.RefreshAllUserStatus;  // friss adatok
    // csak nehogy �gy maradjon
    LegyenSurgos:= RESTAPIKozosDlg.GetLegyenSurgos(ActiveThread.id);
    // LegyenSurgos := False;
    end;
  if LegyenSurgos then RESTAPIKozosDlg.RESTAPIParamsList.Add('overkill', 'true')
  else RESTAPIKozosDlg.RESTAPIParamsList.Add('overkill', 'false');
  if (aAttachment<>'') and (aMimeType<>'') then begin
      RESTAPIKozosDlg.RESTAPIParamsList.Add('attachment_type', aMimeType);
      // RESTAPIKozosDlg.RESTAPIParamsList.Add('attachment', aAttachment);
      // RESTAPIKozosDlg.RESTAPIParamsList.Add('thumbnail', AThumb);
      RESTAPIKozosDlg.RESTAPIParamsList.Add('attachment', '"'+aAttachment+'"');
      RESTAPIKozosDlg.RESTAPIParamsList.Add('thumbnail', '"'+AThumb+'"');
      end;
  if EgyebDlg.TRUCKPIT_SMS_PARHUZAMOSAN then begin
     SendEagleSMS(ActiveThread, aText);
     end;
  JSONResult:= RESTAPIKozosDlg.ExecRequest(ResourceS, 'post', RESTAPIKozosDlg.RESTAPIParamsList);
  if JSONResult.ErrorString = '' then begin
    CCKuld(aText);
    if Megbizaskod <> '' then begin
      EgyebDlg.StoreMegbizasLevel(Megbizaskod, ActiveThread.Name, aText);
      Megbizaskod:= '';
      end;  // if
    memoMessage.Lines.Clear;
    ebCCNevsor.Visible:= False;
    M33.Text:= '';
    M34.Text:= '';
    ebSzalbanKeres.Text:= '';
    RefreshThread(ActiveThread, '', False);
    end
  else begin
    S:='Hiba a k�ld�s sor�n! K�d: '+ JSONResult.ErrorString;
    MessageDlg( S, mtError,[mbOK],0);
    // NoticeKi(S);
    end;  // if
end;


// A CC-ben k�ld�ttet nem k�ld�m el m�g SMS-ben is, mert az csak a p�rhuzamos m�k�d�shez kell, �s az irodai dolgoz�k m�r megkapj�k Truckpit �zenetben.
  {if EgyebDlg.TRUCKPIT_SMS_PARHUZAMOSAN then begin
     SendEagleSMS(ActiveThread, aText);
     end;}

procedure TUzenetTopDlg.CCKuld(const aText: string);
  const
   // SendGroupMessage = 'messages/group/%s';  // 'messages/group/:xgid
   SendPrivateMessage = 'messages/private/%s';  // 'messages/private/:peer_xuid'
var
  i: integer;
  BodyKey, BodyValue, S, Res, ResourceS, Lista, DOKOD: string;
  JSONResult: TJSONResult;
  LegyenSurgos: boolean;
begin
  if M33.Text <> '' then begin
    Lista:= M33.Text; // DOKOD lista
    while Lista <> '' do begin
        RESTAPIKozosDlg.RESTAPIParamsList.Clear;
        BodyKey:= 'text';
        BodyValue:= 'Elk�ldve '+ActiveThread.Name+' r�sz�re: "'+aText+'"';
        RESTAPIKozosDlg.RESTAPIParamsList.Add(BodyKey, BodyValue);
        DOKOD:= Trim(EgyebDlg.SepFrontToken(',', Lista));
        LegyenSurgos:= RESTAPIKozosDlg.GetLegyenSurgos(DOKOD);
        if LegyenSurgos then RESTAPIKozosDlg.RESTAPIParamsList.Add('overkill', 'true')
        else RESTAPIKozosDlg.RESTAPIParamsList.Add('overkill', 'false');

        Lista:= Trim(EgyebDlg.SepRest(',', Lista));
        ResourceS:= Format(SendPrivateMessage, [DOKOD]);
        JSONResult:= RESTAPIKozosDlg.ExecRequest(ResourceS, 'post', RESTAPIKozosDlg.RESTAPIParamsList);
        if JSONResult.ErrorString <> '' then begin
          S:='Hiba a k�ld�s sor�n! K�d: '+ JSONResult.ErrorString;
          NoticeKi(S);
          end;  // if
        end; // while
    end; // if
end;


procedure TUzenetTopDlg.SendEagleSMS(const ActiveThread: TContactRecord; const aText: string);
var
  CimzettekSL, UresSL: TStringList;
  CsopList, Dokod: string;
  LLen, i: integer;
  Res: TStringResult;
  procedure AddDokod(const ADokod: string);
    var
      EagleContact: string;
    begin
      EagleContact:= EgyebDlg.GetDolgozoSMSEagleCimzett(ADokod, True);
      CimzettekSL.Add(EagleContact);
    end;

begin
  CimzettekSL:=TStringList.Create;
  UresSL:=TStringList.Create;
  try
    if ActiveThread.which = 'user' then begin
        AddDokod(ActiveThread.id);
        end;
    if ActiveThread.which = 'group' then begin
        Res:= RESTAPIKozosDlg.GetCsoportTagok(ActiveThread.id);
        if not(Res.Success) then begin
            NoticeKi('FIGYELEM! Csoport tagok lek�rdez�se sikertelen! SMS k�ld�s nem t�rt�nt.');
            Exit;
            end;
        CsopList:=Res.ResultString;
        LLen:= EgyebDlg.SepListLength(',', CsopList);
        for i:= 1 to LLen do begin
          Dokod:= EgyebDlg.GetSepListItem(',', CsopList, i);  // first item: 1
          AddDokod(Dokod);
          end;  // for
        end;
    if CimzettekSL.Count>0 then
       SendOutlookHTMLMmail(CimzettekSL, UresSL, UresSL, '', aText, True, uzleti);
  finally
     if CimzettekSL <> nil then CimzettekSL.Free;
     if UresSL <> nil then UresSL.Free;
     end; // try-finally
end;

procedure TUzenetTopDlg.UzenetElokeszit(const AXUID, AXUID2, AName, AName2, ASzoveg, AMBKOD: string);
// felhoz egy egy�ni sz�lat az illet�vel, �s bele�rja a sz�veget, ut�na csak k�ldeni kell.
var
   MyContactRecord: TContactRecord;
    which: string;
    id: string;
    name: string;
begin
  MyContactRecord.which:= 'user';
  MyContactRecord.id:= AXUID;
  MyContactRecord.name:= AName;
  ActiveThread:= MyContactRecord;
  SetActivePage(2);  // friss�t is
  memoMessage.Text:= ASzoveg;
  MegbizasKod:= AMBKOD;
  if AXUID2 <> '' then begin  // CC is van
    M33.text:= AXUID2;
    ebCCNevsor.Text:= 'CC: '+AName2;
    ebCCNevsor.Visible:= True;
    end;
  UzenetTopDlg.Show;
  memoMessage.SetFocus;
end;

end.
