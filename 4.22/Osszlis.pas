unit osszlis;

interface

uses
  WinTypes, WinProcs, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, QuickRpt, QRExpr, Qrctrls, ExtCtrls, printers, DB, DBTables, Egyeb, Forgalom, J_SQL,
  Grids, Kozos, KozosTipusok, ADODB, Ossznyil, Kozos_Local, J_Excel, Clipbrd;

type
  E_res= array[1..5] of string;

  TOsszlisDlg = class(TForm)
    Rep: TQuickRep;
    QRBand3: TQRBand;
    QRLSzamla: TQRLabel;
    QRBand1: TQRBand;
    QRLabel16: TQRLabel;
    QRLabel17: TQRLabel;
    QRLabel6: TQRLabel;
    QRSysData1: TQRSysData;
    QRLabel7: TQRLabel;
    QRSysData4: TQRSysData;
    QRLabel1: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel3: TQRLabel;
    QRShape3: TQRShape;
    QRShape5: TQRShape;
	 QRShape8: TQRShape;
    QRLabel4: TQRLabel;
	 QRLabel12: TQRLabel;
    QRLabel13: TQRLabel;
    QRLabel14: TQRLabel;
	 QRLabel24: TQRLabel;
    QRLabel2: TQRLabel;
    QRLabel5: TQRLabel;
    QRBand2: TQRBand;
    QRLabel21: TQRLabel;
    QRShape15: TQRShape;
    QL3: TQRLabel;
	 QS2: TQRLabel;
    QS3: TQRLabel;
    QS4: TQRLabel;
    SG1: TStringGrid;
    QRShape1: TQRShape;
    QRLabel9: TQRLabel;
    QS30: TQRLabel;
    QRLabel10: TQRLabel;
    QL2: TQRLabel;
    QRLabel15: TQRLabel;
    QL1: TQRLabel;
    QSA1: TQRShape;
    Query1: TADOQuery;
    Query2: TADOQuery;
	 Query3: TADOQuery;
    Query4: TADOQuery;
    Query5: TADOQuery;
    QRLabel11: TQRLabel;
	 QL4: TQRLabel;
    QL5: TQRLabel;
    QRLabel20: TQRLabel;
    QL6: TQRLabel;
    QRLabel23: TQRLabel;
    QL7: TQRLabel;
	 QRLabel26: TQRLabel;
    QL8: TQRLabel;
    QRLabel30: TQRLabel;
    QRLabel18: TQRLabel;
    QL9: TQRLabel;
    QL10: TQRLabel;
    QL11: TQRLabel;
    QRLabel22: TQRLabel;
	 ChildBand1: TQRChildBand;
    QRLabel28: TQRLabel;
    QKI6: TQRLabel;
    QRLabel31: TQRLabel;
    QKI1: TQRLabel;
    QRLabel33: TQRLabel;
    QKI5: TQRLabel;
    QRLabel35: TQRLabel;
    QKI2: TQRLabel;
    QKI4: TQRLabel;
    QKI3: TQRLabel;
	 QRLabel38: TQRLabel;
	 QRLabel39: TQRLabel;
	 QKI7: TQRLabel;
	 QKI8: TQRLabel;
	 Query6: TADOQuery;
	 QSofor1: TQRLabel;
	 QSofor2: TQRLabel;
    QRLabel27: TQRLabel;
    QS1A: TQRLabel;
    QS1B: TQRLabel;
    QS2B: TQRLabel;
    QS2A: TQRLabel;
    Q_Szotar: TADOQuery;
    Query1_: TADOQuery;
    QueryAlap: TADOQuery;
    Queryal2: TADOQuery;
    SGOsszes: TStringGrid;
    QRLabel29: TQRLabel;
    QRLabel32: TQRLabel;
    QL20: TQRLabel;
    QRLabel34: TQRLabel;
    QRLabel36: TQRLabel;
	 procedure FormCreate(Sender: TObject);
   procedure	Tolt(jarat, rendsz, dat1, dat2, vevokod, viszonysql: string; csakSajatAuto: boolean);
   procedure	ExportToExcel(ExportFN: string);
	 procedure QRBand1BeforePrint(Sender: TQRCustomBand;
	   var PrintBand: Boolean);
	 procedure RepBeforePrint(Sender: TCustomQuickRep;
	   var PrintReport: Boolean);
	 procedure RepNeedData(Sender: TObject; var MoreData: Boolean);
	 procedure FormDestroy(Sender: TObject);
	 procedure ChildBand1BeforePrint(Sender: TQRCustomBand;
	   var PrintBand: Boolean);
	private
	rekszam				: integer;
	  selkm1			: double;
	  selkm2			: double;
	  sextra1			: double;
	  sedij1			: double;
	  sedij2			: double;
	  jaratsz			: string;
	  ertekek			: array [1..5] of double;
	  osszeur			: double;
	  kezdodatum		: string;
	  minimumkt			: double;
	  maximumkt			: double;
	okolts		: double;
	okolts2		: double;
	eurokolts		: double;
	eurokolts2		: double;
	oeurkolts	: double;
	oeurkolts2: double;
	oeurbevet : double;
	ofuvar		: double;
	oertek		: double;
	arfoly		: double;
  op_km, p_ktg: integer;
  p_jasza: string;
  ervenytelen_fogyasztas_grandtotal: boolean;

    function ktsg_nemkell(nev: string): boolean;
    function Jarat_Egyeb_km(jakod: string): E_res;
	  function Jarat_Egyeb_ktg(jarat : string ): TIntegerValid;
    procedure	ExportJarat(EX: TJExcel; var SG1sorszam: integer; var Excelsorszam: integer);
  public
     vanadat			: boolean;
     kateg				: string;
     erintett_jaratok: integer;
     ossz,ossz2,eurossz,eurossz2				: double;
	  labi				: TLabel;
  end;

var
  OsszlisDlg: TOsszlisDlg;

implementation

uses
	J_Fogyaszt, StrUtils;

{$R *.DFM}

procedure TOsszlisDlg.FormCreate(Sender: TObject);
begin
	EgyebDlg.SetADOQueryDatabase(Query1);
	EgyebDlg.SetADOQueryDatabase(Query1_);
	EgyebDlg.SetADOQueryDatabase(Query2);
	EgyebDlg.SetADOQueryDatabase(Query3);
	EgyebDlg.SetADOQueryDatabase(Query4);
	EgyebDlg.SetADOQueryDatabase(Query5);
	EgyebDlg.SetADOQueryDatabase(Query6);
	EgyebDlg.SetADOQueryDatabase(QueryAlap);
	EgyebDlg.SetADOQueryDatabase(Queryal2);
	rekszam	:= 1;
  kateg:= '';
	Application.CreateForm(TFogyasztasDlg, FogyasztasDlg);
  op_km:=0;
end;

procedure	TOsszlisDlg.ExportToExcel(ExportFN: string);
var
  EX: TJExcel;
  Excelsorszam, SG1sorszam, i, j: integer;
  S: string;
begin
  if FileExists(ExportFN) then
		  DeleteFile(PChar(ExportFN));
  EX:= TJexcel.Create(Self);
	if not EX.OpenXlsFile(ExportFN) then begin
 		NoticeKi('Nem siker�lt megnyitnom az Excel �llom�nyt!');
    Exit;  // procedure
		end;

  EX.ColNumber	:= 25;
  EX.InitRow;
  EX.SetActiveSheet(0);
  Excelsorszam:= 1;
  // EX.SetRowcell(1, 'J�rat k�lts�gek');
  // EX.SaveRow(Excelsorszam);
  // Inc(Excelsorszam);
  // fejl�c
	Ex.EmptyRow;
	EX.SetRowcell( 1, 'J�rat');
	EX.SetRowcell( 2, 'Indul�s id�pont');
	EX.SetRowcell( 3, '�rkez�s id�pont');
 	EX.SetRowcell( 4, 'Id�tartam (nap)');
	EX.SetRowcell( 5, 'J�rat km');
	EX.SetRowcell( 6, 'Alj�rat km');
 	EX.SetRowcell( 7, 'Alj�rat id� (nap)');
	EX.SetRowcell( 8, 'G�pkocsi');
	EX.SetRowcell( 9, 'Gk. kateg�ria');
	EX.SetRowcell(10, 'P�tkocsi');
	EX.SetRowcell(11, 'P�t kateg�ria');
	EX.SetRowcell(12, 'Sof�r1');
	EX.SetRowcell(13, 'Ft/km');
	EX.SetRowcell(14, 'K/T');
	EX.SetRowcell(15, 'Bev�tel Ft/km');
	EX.SetRowcell(16, 'Bev�tel EUR/km');
	EX.SetRowcell(17, 'K�lts�g Ft/km');
	EX.SetRowcell(18, 'K�lts�g EUR/km');
	EX.SetRowcell(19, 'Fogy. �tlag l/100km');
	EX.SetRowcell(20, 'Megtakar�t�s l/100km');
	EX.SetRowcell(21, 'Nyeres�g Ft/km');
	EX.SetRowcell(22, 'Nyeres�g EUR/km');
	EX.SetRowcell(23, 'Sz�mla viszonylat');
	EX.SetRowcell(24, 'Sz�ml�zott d�j EUR');
	EX.SetRowcell(25, 'Vev�');
	EX.SetRowcell(26, 'Sz�mlasz�m');
  EX.SaveRow(Excelsorszam);
	Inc(Excelsorszam);
  // mi van a StringGrid-ekben???
  {S:='';
  for i := 1 to SGOsszes.RowCount do begin
    S:=S+const_CRLF;
    for j := 1 to SGOsszes.ColCount do
      S:= S+';'+SGOsszes.Cells[j-1, i-1];
    end;
  S:=S+'-----------------------'+const_CRLF;
  for i := 1 to SG1.RowCount do begin
    S:=S+const_CRLF;
    for j := 1 to SG1.ColCount do
      S:= S+';'+SG1.Cells[j-1, i-1];
    end;
  ClipBoard.AsText := S;}
  SG1sorszam:= 0;
  while SG1sorszam < SG1.RowCount do begin
    ExportJarat(EX, SG1sorszam, Excelsorszam);
    end;
  EX.SaveFileAsXls(ExportFN);
	EX.Free;
end;

procedure	TOsszlisDlg.ExportJarat(EX: TJExcel; var SG1sorszam: integer; var Excelsorszam: integer);
var
	jakod, Mettol, Meddig, Date1, Time1, Date2, Time2, gkkat: string;
	rsz			: integer;
	osszkm, bevetel, koltseg, nyereseg, szaz, Napok: double;
  p_km: integer;
  kellujsor: boolean;
begin
  while (SG1.Cells[1,SG1sorszam] <> '--+') and (SG1sorszam < SG1.RowCount) do begin
      inc(SG1sorszam);  // a k�vetkez� fejl�c sorig p�rgetj�k
      end;
	jakod			:= SG1.Cells[4,SG1sorszam];
	rsz				:= SGOsszes.Cols[0].IndexOf(jakod);
	// osszkm			:= 100000000;
	// if StringSzam(SGOsszes.Cells[4, rsz]) > 0 then begin
  osszkm	:= StringSzam(SGOsszes.Cells[4, rsz]);
    // NagyP 2017-02-24
    // if OssznyilDlg.M1.Text<>'' then
  	//	osszkm	:= FogyasztasDlg.GetOsszKm;

  p_km:= StrToIntdef(SGOsszes.Cells[19, rsz], 0); // az elmentett �rt�ket fogjuk haszn�lni
	//osszkm	:= FogyasztasDlg.GetOsszKm;
  Ex.EmptyRow;
	if rsz = -1 then begin
		EX.SetRowcell(1, 'Hi�nyz� j�ratsz�m : '+jakod);
    EX.SetRowcell(8, '');  // rendsz�m
    EX.SetRowcell(9, '');  // gk. kateg
	end else begin
		gkkat			:= EgyebDlg.GkTipus(SGOsszes.Cells[2, rsz]);
		EX.SetRowcell(1, SGOsszes.Cells[1, rsz]); // j�ratsz�m
    EX.SetRowcell(8, SGOsszes.Cells[2, rsz]);  // rendsz�m
    EX.SetRowcell(9, gkkat);  // gk. kateg
		gkkat			:= EgyebDlg.GkTipus(SGOsszes.Cells[3, rsz]);     // p�t
    EX.SetRowcell(10, SGOsszes.Cells[3, rsz]);  // p�t rendsz�m
    EX.SetRowcell(11, gkkat);  // gk. kateg
		if SGOsszes.Cells[11, rsz] <> '' then begin
			// Az els� sof�r bet�lt�se
			Query_Run(Query5, 'SELECT SUM(JS_SZAKM) OKM FROM JARSOFOR WHERE JS_JAKOD = '''+jakod+''' AND JS_SOFOR = '''+SGOsszes.Cells[11, rsz]+''' ');
			szaz	:= StringSzam(Query5.FieldByName('OKM').AsString) * 100/ osszkm ;
			EX.SetRowcell(12, Query_Select ('DOLGOZO', 'DO_KOD', SGOsszes.Cells[11, rsz] , 'DO_NAME')); // Sof�r1
      EX.SetRowcell(14, Format('%5.1f%%',[szaz]) );  // K/T
			if StringSzam(SGOsszes.Cells[15, rsz]) > 0 then begin
				EX.SetRowcell(13, Format('%6.2f',[StringSzam(SGOsszes.Cells[13, rsz])/StringSzam(SGOsszes.Cells[15, rsz])]));
			end else begin
				EX.SetRowcell(13, '0');
			  end;
		  end;  // if Cells[11
    // a 2. sof�rt most nem tessz�k bele
		{if SGOsszes.Cells[12, rsz] <> '' then begin
			// A m�sodik sof�r bet�lt�se
			Query_Run(Query5, 'SELECT SUM(JS_SZAKM) OKM FROM JARSOFOR WHERE JS_JAKOD = '''+jasz+''' AND JS_SOFOR = '''+SGOsszes.Cells[12, rsz]+''' ');
			szaz	:= StringSzam(Query5.FieldByName('OKM').AsString) * 100/ osszkm ;
			QSofor2.Caption	:= Query_Select ('DOLGOZO', 'DO_KOD', SGOsszes.Cells[12, rsz] , 'DO_NAME');
			QS2B.Caption	:= Format('(K/T: %5.1f%%)',[szaz]);
			if StringSzam(SGOsszes.Cells[16, rsz]) > 0 then begin
				QS2A.Caption	:= Format('(%6.2f Ft/km)',[StringSzam(SGOsszes.Cells[14, rsz])/StringSzam(SGOsszes.Cells[16, rsz])]);
			  end
      else begin
				QS2A.Caption	:= '  0.00 Ft/km)';
  			end;
   		end;}
    if OssznyilDlg.CheckBox2.Checked then begin
  		// Bev�tel
	  	EX.SetRowcell(15, Format('%.0f',   [ (StringSzam(SGOsszes.Cells[5, rsz])+StringSzam(SGOsszes.Cells[7, rsz])-StringSzam(SGOsszes.Cells[17, rsz])) / (osszkm+p_km) ]));
		  EX.SetRowcell(16, Format('%.3f',  [ (StringSzam(SGOsszes.Cells[6, rsz])+StringSzam(SGOsszes.Cells[8, rsz])-StringSzam(SGOsszes.Cells[18, rsz])) / (osszkm+p_km) ]));
  		// K�lts�g
  		EX.SetRowcell(17, Format('%.0f',   [ -1 * StringSzam(SGOsszes.Cells[17, rsz]) / (osszkm+p_km) ]));
  		EX.SetRowcell(18, Format('%.3f', [ -1 * StringSzam(SGOsszes.Cells[18, rsz]) / (osszkm+p_km) ]));
      end
    else begin
  		// Bev�tel
	  	EX.SetRowcell(15, Format('%.0f',   [ StringSzam(SGOsszes.Cells[5, rsz]) / (osszkm+p_km) ]));
		  EX.SetRowcell(16, Format('%.3f',  [ StringSzam(SGOsszes.Cells[6, rsz]) / (osszkm+p_km) ]));
  		// K�lts�g
	  	EX.SetRowcell(17, Format('%.0f',   [ -1 * StringSzam(SGOsszes.Cells[7, rsz]) / (osszkm+p_km) ]));
  		EX.SetRowcell(18, Format('%.3f', [ -1 * StringSzam(SGOsszes.Cells[8, rsz]) / (osszkm+p_km) ]));
      end;
		bevetel			:= StringSzam(SGOsszes.Cells[6, rsz]) / osszkm;
		koltseg			:= -1 * StringSzam(SGOsszes.Cells[7, rsz]) / osszkm;

    if SGOsszes.Cells[20, rsz] <> '1' then begin  // �rv�nyes a fogyaszt�s adat
    	EX.SetRowcell(19, Format('%.2f', [ StringSzam(SGOsszes.Cells[9, rsz])]));  // �tlag
		  EX.SetRowcell(20, Format('%.2f', [ StringSzam(SGOsszes.Cells[10, rsz])*100 / osszkm ]));		// Megtakar�t�s
      end
    else begin
      EX.SetRowcell(19, nem_szamolhato_cimke);
      EX.SetRowcell(20, nem_szamolhato_cimke);
      end;
		// ---- Nyeres�g ----
		EX.SetRowcell(21, Format('%.0f',   [ (StringSzam(SGOsszes.Cells[5, rsz]) + StringSzam(SGOsszes.Cells[7, rsz])) / (osszkm+p_km) ]));
		EX.SetRowcell(22, Format('%.3f', [ (StringSzam(SGOsszes.Cells[6, rsz]) + StringSzam(SGOsszes.Cells[8, rsz])) / (osszkm+p_km) ]));
    // id�szak, kilom�terek
    Mettol:= SG1.Cells[6,SG1sorszam];
    Date1:=copy(Mettol,1,11);
    Time1:=copy(Mettol,13,5)+':00';  // nincs benne m�sodperc
    Meddig:= SG1.Cells[7,SG1sorszam];
    Date2:=copy(Meddig,1,11);
    Time2:=copy(Meddig,13,5)+':00';  // nincs benne m�sodperc
    Napok:= DateTimeDifferenceSec(Date2, Time2, Date1, Time1)/(3600*24);  // el�l a k�s�bbi d�tum! tizedes t�rttel
    EX.SetRowcell(2, Mettol);
    EX.SetRowcell(3, Meddig);
    EX.SetRowcell(4, Format('%.2f', [Napok]));
    // EX.SetRowcell(5, SG1.Cells[8,SG1sorszam]);
    EX.SetRowcell(5, Format('%.0f', [osszkm]));  // elvileg ugyanaz
    EX.SetRowcell(6, SG1.Cells[9,SG1sorszam]);
    EX.SetRowcell(7, SG1.Cells[10,SG1sorszam]);
    // --- megb�z�sok ----
    kellujsor:= False;
    inc(SG1sorszam);  // eddig a fejsoron �lltunk
    while (SG1.Cells[1,SG1sorszam] <> '--+') and (SG1sorszam < SG1.RowCount) do begin // az ide tartoz� sorok, a k�vetkez� fejl�cig
      if SG1.Cells[1,SG1sorszam] = 'Fuvard�j' then begin
        if kellujsor then begin
          EX.SaveRow(Excelsorszam);
	        Inc(Excelsorszam);
          Ex.EmptyRow;
          EX.SetRowcell(1, SGOsszes.Cells[1, rsz]); // a j�ratsz�m szerepeljen minden sorban
          end;
        EX.SetRowcell(23, SG1.Cells[6,SG1sorszam]); // Sz�mla viszonylat
        EX.SetRowcell(24, SG1.Cells[7,SG1sorszam]); // Sz�ml�zott d�j EUR
        EX.SetRowcell(25, SG1.Cells[8,SG1sorszam]); // Vev�
        EX.SetRowcell(26, SG1.Cells[9,SG1sorszam]); // Sz�mlasz�m
        kellujsor:= True;
        end;
      inc(SG1sorszam);
      end;  // while
    end;  // else
  EX.SaveRow(Excelsorszam);
	Inc(Excelsorszam);
end;


procedure	TOsszlisDlg.Tolt(jarat, rendsz, dat1, dat2, vevokod, viszonysql: string; csakSajatAuto: boolean);
var
	ertek    	: double;
	sqlstr,S : string;
	sof1		: string;
	szossz		: double;
	sorszam		: integer;
	jojarat		: string;
	kmsz	 	: string;

	euralap		: double;
	eurertek 	: double;
	val_oert 	: double;
	eurarf		: double;
	ardat	 	: string;
	ertek2		: double;
	vevonev		: string;
	szamlaszam	: string;
	str			: string;
	feldij		: double;
	felra1		: double;
//	fogyi		: string;
	voltsofor	: string;
	i			: integer;
	uzkolt		: double;
	tipus		: integer;
	osszkm		: double;
	uzmegtak	: double;
	ksatlag		: double;
	rekordszam	: integer;
	osszam		: integer;
	jaratatlag, F: double;
  kjel, S1, S2, S3, Elotag, KMFigyelmezteto: string;
  p_km: integer;  // ne legyen glob�lis, ha egyszer �rt�ket csak lok�lisan kap.
  res: E_res;
  NULLA: integer;
  szam, TenyFogyasztas, Megtakaritas, p_ido: double;
  p_ktg_ervenyes, Ervenytelen_osszkilometer: boolean;
begin
	selkm1		:= 0;
	selkm2		:= 0;
	sedij1		:= 0;
	sedij2		:= 0;
	sorszam		:= 0;
	oertek		:= 0;
	okolts		:= 0;
	okolts2		:= 0;
	eurokolts		:= 0;
	eurokolts2		:= 0;
  oeurkolts := 0;
  oeurkolts2:= 0;
  oeurbevet := 0;
	ofuvar		:= 0;
  ossz:=0;
  ossz2:=0;
  eurossz:=0;
  eurossz2:=0;
	vanadat 	:= false;
  ervenytelen_fogyasztas_grandtotal:= false;
	feldij		:= StringSzam(EgyebDlg.Read_SZGrid('CEG', '301'));
	minimumkt	:= StringSzam(EgyebDlg.Read_SZGrid('999', '742'));
	maximumkt	:= StringSzam(EgyebDlg.Read_SZGrid('999', '743'));
	for i := 1 to 5 do begin
		ertekek[i]	:= 0;
	end;
	{A fejl�c kit�lt�se}
	SG1.RowCount 	:= 0;

	if dat1 = '' then begin
		dat1	:= '2000.01.01.';
	end;
	if dat2 = '' then begin
		dat2	:= EgyebDlg.MaiDatum;
	end;
	kezdodatum	:= dat1;
  Ervenytelen_osszkilometer:= False;
	{El�sz�r lesz�rj�k a j�ratokat a megadott felt�telek alapj�n}
	sqlstr	:= 'SELECT * FROM JARAT WHERE JA_JKEZD >= '''+dat1+''' AND JA_JKEZD <= '''+dat2+''' '+
      ' /* AND JA_ZAROKM > 0 */ ';
	if jarat <> '' then begin
		sqlstr	:= sqlstr + ' AND JA_KOD IN ('+jarat+') ';
	end else begin
		sqlstr	:= sqlstr + ' AND JA_FAZIS <> 2 ';	// Storn�zott j�ratok nem kellenek
    sqlstr	:= sqlstr + ' AND JA_FAJKO in (0,1) ';	// Rezsi j�ratok nem kellenek

	end;

	if rendsz <> '' then begin
//     sqlstr	:= sqlstr + ' AND JA_RENDSZ = '''+rendsz+''' ';
		sqlstr	:= sqlstr + ' AND JA_RENDSZ IN ( ';
		str		:= rendsz;
		while Pos(',',str) > 0 do begin
			sqlstr	:= sqlstr + '''' + copy(str,1,Pos(',',str)-1) + ''',';
			str		:= copy(str, Pos(',',str)+1,999);
		end;
		if str <> '' then begin
			sqlstr	:= sqlstr + '''' + str + ''',';
		end;
		sqlstr	:= copy(sqlstr, 1, Length(sqlstr)-1) +' ) ';
	end;

  if kateg <> '' then begin
		sqlstr := sqlstr + ' and JA_RENDSZ IN (select GK_RESZ from gepkocsi where GK_GKKAT IN ( '+kateg+' )) ';
	end;

  // 2017-08-29: csak azok a j�ratok, amelyekhez kapcsolt aut� a j�rat sor�n a mi�nk volt (a DK-hoz �tadott aut�k t�vesen voltak itt)
  if csakSajatAuto then
    sqlstr	:= sqlstr + ' and JA_RENDSZ in (select GK_RESZ from GEPKOCSI where (GK_FORKI='''' or GK_FORKI>JA_JVEGE)) ';

  // vev�, viszonylat sz�r�s 2017-07-28
  if vevokod<> '' then begin
     sqlstr	:= sqlstr + 'and JA_KOD in (select VI_JAKOD from viszony where VI_VEKOD = '+vevokod+ ' '+viszonysql+' )'
     end;

//	sqlstr	:= sqlstr + ' ORDER BY JA_ALKOD ';
	sqlstr	:= sqlstr + ' ORDER BY JA_NYITKM ';
	if Query_Run (Query1,sqlstr ,true) then begin
		Query1.DisableControls;
    // Query1.RecordCount;  ??? NP
    erintett_jaratok:= Query1.RecordCount;
		rekordszam			:= 1;
		SGOsszes.RowCount	:= 1;
		SGOsszes.Rows[0].Clear;
		while ( ( not Query1.EOF ) and (not EgyebDlg.kilepes) ) do begin
			Application.ProcessMessages;
			jaratsz			:= Query1.FieldByName('JA_KOD').AsString;
			if Labi <> nil then begin
        Labi.Caption	:= IntToStr(rekordszam)+' / '+IntToStr(Query1.RecordCount);
  			Labi.Update;
        end;
			Inc(rekordszam);
			// A j�rat berak�sa az �sszes�tett t�bl�zatba
			osszam	:= SGOsszes.Cols[0].IndexOf(jaratsz);
			if osszam = -1 then begin
				if SGOsszes.Cells[0,0] = '' then begin
					osszam	:= 0;
				end else begin
					osszam				:= SGOsszes.RowCount;
					SGOsszes.RowCount	:= SGOsszes.RowCount + 1;
				end;
			end else begin
				// M�r l�tez� j�rat!!!
				NoticeKi('L�tez� j�rat!');
			end;
			SGOsszes.Cells[0,osszam] := jaratsz;
			SGOsszes.Cells[1,osszam] := GetJaratszamFromDb(Query1);
			SGOsszes.Cells[2,osszam] := Query1.FieldByName('JA_RENDSZ').AsString;
			//SGOsszes.Cells[3,osszam] := '';
			SGOsszes.Cells[3,osszam] := Query1.FieldByName('JA_JPOTK').AsString;
//			Application.MainForm.StatusBar1.Panels[0].Text	:= jaratsz;
      SGOsszes.Cells[20, osszam]:='0';  // "ervenytelen_fogyasztas_auto", alap�rtelmezett a "nem"

      p_km:=0;
      p_ktg:=0;
      p_jasza:='';
      p_ido:= 0;
      if OssznyilDlg.CheckBox3.Checked then begin
        res:= Jarat_Egyeb_km(jaratsz)   ;
        p_km:= StrToIntdef( res[1], 0)   ;
        SGOsszes.Cells[19,osszam] := IntToStr(p_km);  // elmentj�k a "BeforePrint" esem�nykezel�h�z
        op_km:=op_km+p_km;
        p_ktg:=StrToIntdef( res[2], 0);
        p_ktg_ervenyes:= (res[4]='1');
        p_jasza:=res[3];
        p_ido:=StringSzam(res[5]);
        end     // if
      else begin
        SGOsszes.Cells[19,osszam] := '';
        end;  // else

			jojarat	:= GetJaratszamFromDb(Query1);
			if (jarat <> '') and (Pos(',', jarat) = 0) then begin
				QRLabel5.Caption	:= jojarat;
        if p_jasza<>'' then
          QRLabel5.Caption:=jojarat+' ('+p_jasza+')';
				QRLabel16.Caption	:= Query1.FieldByname('JA_RENDSZ').AsString +'('+EgyebDlg.GkTipus(Query1.FieldByname('JA_RENDSZ').AsString)+') ; '+
          Query1.FieldByname('JA_JPOTK').AsString+'('+EgyebDlg.GkTipus(Query1.FieldByname('JA_JPOTK').AsString)+')'         ;
			end else begin
				QRLabel5.Caption	:= 'T�bb j�rat';
				QRLabel16.Caption	:= '';
			end;
			vanadat 	:= true;

     // Jarat_Egyeb_ktg(jaratsz);

			// A j�rathoz tartoz� fogyaszt�si adatok kisz�m�t�sa
			FogyasztasDlg.ClearAllList;
			FogyasztasDlg.JaratBetoltes(Query1.FieldByname('JA_KOD').AsString);
			FogyasztasDlg.CreateTankLista;
			FogyasztasDlg.CreateAdBlueLista;
			FogyasztasDlg.Szakaszolo;

      SGOsszes.Cells[21, osszam]:='0';
      if FogyasztasDlg.GetOsszKm < 0 then begin
        SGOsszes.Cells[21, osszam]:='1';  // "�rvenytelen km �rt�k a j�ratban", alap�rtelmezett a "nem"
        Ervenytelen_osszkilometer:= True;
        end;

			// A j�rat fejl�c ki�r�sa
			SG1.RowCount  					:= SG1.RowCount + 1;
			SG1.Cells[0,SG1.RowCount - 1]	:= '';
			SG1.Cells[1,SG1.RowCount - 1]	:= '--+';
//			SG1.Cells[2,SG1.RowCount - 1]	:= jojarat + ' j�rat :   Id�szak : '+
      if (Query1.FieldByName('JA_ZAROKM').AsInteger - Query1.FieldByName('JA_NYITKM').AsInteger) < 0 then
          KMFigyelmezteto:= 'HIB�S KM �RT�KEK!'
      else KMFigyelmezteto:='';
			SG1.Cells[2,SG1.RowCount - 1]	:= 'Id�szak : '+
				Query1.FieldByName('JA_JKEZD').AsString + ' ' + Query1.FieldByName('JA_KEIDO').AsString + ' - ' +
				Query1.FieldByName('JA_JVEGE').AsString + ' ' + Query1.FieldByName('JA_VEIDO').AsString +
				' Gk.: '+ Query1.FieldByName('JA_RENDSZ').AsString +
				Format(' -> %.0f km - %.0f km = %.0f km %s',[StringSzam(Query1.FieldByName('JA_NYITKM').AsString), StringSzam(Query1.FieldByName('JA_ZAROKM').AsString),
					StringSzam(Query1.FieldByName('JA_ZAROKM').AsString) - StringSzam(Query1.FieldByName('JA_NYITKM').AsString), KMFigyelmezteto]);
      // ---- k�l�n mez�kbe is betessz�k az Excel exporthoz ----
      SG1.Cells[6, SG1.RowCount - 1]	:= Query1.FieldByName('JA_JKEZD').AsString + ' ' + Query1.FieldByName('JA_KEIDO').AsString;
      SG1.Cells[7, SG1.RowCount - 1]	:= Query1.FieldByName('JA_JVEGE').AsString + ' ' + Query1.FieldByName('JA_VEIDO').AsString;
      SG1.Cells[8, SG1.RowCount - 1]	:= Format('%.0f', [StringSzam(Query1.FieldByName('JA_ZAROKM').AsString) - StringSzam(Query1.FieldByName('JA_NYITKM').AsString)]);
      // -------------------------------------------------------
      if p_km>0 then  // van kapcsol�d� j�rat km
      begin
        SG1.Cells[2,SG1.RowCount - 1]	:=SG1.Cells[2,SG1.RowCount - 1]+' (+ '+IntToStr(p_km)+'km)';
        SG1.Cells[9, SG1.RowCount - 1]	:= IntToStr(p_km);
        SG1.Cells[10, SG1.RowCount - 1]	:= SzamString(p_ido, 5, 2, False); // alj�rat id�tartam
        end;
			SG1.Cells[3,SG1.RowCount - 1]	:= '0';
			SG1.Cells[4,SG1.RowCount - 1]	:= Query1.FieldByName('JA_KOD').AsString;

			QRLabel17.Caption	:= dat1 + ' - ' + dat2;
			if jarat <> '' then begin
				// Egyetlen j�rat eset�n a d�tumhoz az � adatait �rjuk ki
				QRLabel17.Caption	:= 	Query1.FieldByName('JA_JKEZD').AsString + ' ' + Query1.FieldByName('JA_KEIDO').AsString + '  -  ' +
										Query1.FieldByName('JA_JVEGE').AsString + ' ' + Query1.FieldByName('JA_VEIDO').AsString;
				kezdodatum			:= 	Query1.FieldByName('JA_JKEZD').AsString;
			end;

			{A dolgoz�i adatok bet�tele}
			voltsofor	:= '';
			Query_Run(Query5, 'SELECT * FROM JARSOFOR WHERE JS_JAKOD = '''+jaratsz+''' ORDER BY JS_SORSZ ');
			if Query5.RecordCount > 0 then begin
				while not Query5.Eof do begin
					{% km teljes�t�se}
					if  Round(Query1.FieldByname('JA_OSSZKM').AsFloat ) = 0  then begin
						kmsz 		:= '';
					end else begin
						kmsz 		:= Format('  K/T: %5.1f%%',[Round(Query5.FieldByname('JS_SZAKM').AsFloat)*100 /
										Round(Query1.FieldByname('JA_OSSZKM').AsFloat )]);
					end;
					sof1		:= Query_Select('DOLGOZO', 'DO_KOD', Query5.FieldByname('JS_SOFOR').AsString , 'DO_NAME');
					if sof1 <> '' then begin
						// Betessz�k a sof�r fogyaszt�si adatokat k�l�n sorba
//						fogyi	:= '';
						uzkolt 	:= 0;
						if Pos (Query5.FieldByname('JS_SOFOR').AsString, voltsofor) < 1 then begin
							voltsofor	:= voltsofor + Query5.FieldByname('JS_SOFOR').AsString + ';';
							if  Round(Query1.FieldByname('JA_OSSZKM').AsFloat ) > 0  then begin
								SG1.RowCount 				   	:= SG1.RowCount + 1;
								SG1.Cells[0,SG1.RowCount - 1]  	:= IntToStr(sorszam);
								SG1.Cells[1,SG1.RowCount - 1]  	:= 'Fogyaszt�s';
								uzmegtak                        := FogyasztasDlg.GetSoforMegtakaritas(Query5.FieldByname('JS_SOFOR').AsString);
								SG1.Cells[2,SG1.RowCount - 1]  	:= sof1+'('+Format(' Terv: %.2f l - T�ny: %.2f l = Megtak.: %.2f l   �a. k�lts�g : %.2f Ft)',
									[FogyasztasDlg.GetSoforTerv(Query5.FieldByname('JS_SOFOR').AsString), FogyasztasDlg.GetSoforTeny(Query5.FieldByname('JS_SOFOR').AsString),
									 uzmegtak, FogyasztasDlg.GetSoforKoltseg(Query5.FieldByname('JS_SOFOR').AsString)]);
								// Az �zemanyag megtakar�t�s k�lts�g�nek kisz�m�t�sa �s megjelen�t�se
								if uzmegtak <> 0 then begin
									uzkolt	 				   	:= -1 * uzmegtak * GetApehUzemanyag(Query1.FieldByName('JA_JKEZD').AsString, 1);
								end;
								SG1.Cells[3,SG1.RowCount - 1]  	:= SzamString(uzkolt, 12, 0);;
								SG1.Cells[4,SG1.RowCount - 1]  	:= Query1.FieldByName('JA_KOD').AsString;
								Inc(sorszam);
								ossz  		:= ossz + uzkolt;
								ossz2  		:= ossz2+ uzkolt;
                eurertek:=GetValueInOtherCur(uzkolt,Query1.FieldByName('JA_JKEZD').AsString,'HUF','EUR' );
								eurossz  		:= eurossz + eurertek;
								eurossz2  	:= eurossz2+ eurertek;
							end;
						end;
						if QRLabel16.Caption <> '' then begin
							if Pos (sof1, QRLabel16.Caption ) < 1 then begin
								QRLabel16.Caption	:= QRLabel16.Caption + '; ' + sof1;
							end;
						end;
						selkm1	:= StringSzam(Query5.FieldByname('JS_SZAKM').AsString);
						sedij1	:= StringSzam(Query5.FieldByname('JS_JADIJ').AsString);
						sextra1	:= StringSzam(Query5.FieldByname('JS_EXDIJ').AsString);
						felra1  := StringSzam(Query5.FieldByname('JS_FELRA').AsString);
						SG1.RowCount 						:= SG1.RowCount + 1;
						SG1.Cells[0,SG1.RowCount - 1]		:= IntToStr(sorszam);
						SG1.Cells[1,SG1.RowCount - 1]		:= 'Fizet�s';
						if sextra1 <> 0 then begin
							SG1.Cells[2,SG1.RowCount - 1]	:= sof1+'('+Format('%.0f km*%.2f Ft+%.1f Ft+%.1f db*%.0f Ft)',[selkm1, sedij1, sextra1, felra1, feldij])+kmsz;
						end else begin
							SG1.Cells[2,SG1.RowCount - 1]	:= sof1+'('+Format('%.0f km*%.2f Ft+%.1f db*%.0f Ft)',[selkm1, sedij1, felra1, feldij])+kmsz;
						end;
						SG1.Cells[3,SG1.RowCount - 1]		:= SzamString( -1 * selkm1 * sedij1 - sextra1- felra1*feldij, 12, 0);
						SG1.Cells[4,SG1.RowCount - 1]		:= Query1.FieldByName('JA_KOD').AsString;
						szam  								:=  - selkm1 * sedij1 - sextra1 - felra1*feldij;
						ossz  								:= ossz + szam;
						ossz2  								:= ossz2+ szam;
            eurertek:=GetValueInOtherCur(szam,Query1.FieldByName('JA_JKEZD').AsString,'HUF','EUR' );
						eurossz  		:= eurossz + eurertek;
						eurossz2  	:= eurossz2+ eurertek;
						// A sof�rlista felt�lt�se ha m�g nincs rajta
						if SGOsszes.Cells[11,osszam] = '' then begin
							SGOsszes.Cells[11,osszam] := Query5.FieldByname('JS_SOFOR').AsString;	// Az els� sof�r bet�lt�se
							SGOsszes.Cells[13,osszam] := Format('%.2f', [-1 * uzkolt + selkm1 * sedij1 + sextra1 + felra1 * feldij]);	// Az els� bev�teleinek
							SGOsszes.Cells[15,osszam] := Format('%.2f', [FogyasztasDlg.GetSoforKm(Query5.FieldByname('JS_SOFOR').AsString)]);	// A sof�rh�z tartoz� km
						end else begin
							if SGOsszes.Cells[11,osszam] <> Query5.FieldByname('JS_SOFOR').AsString then begin
								if SGOsszes.Cells[12,osszam] = '' then begin
									SGOsszes.Cells[12,osszam] := Query5.FieldByname('JS_SOFOR').AsString;	// A m�sodik sof�r bet�lt�se
									SGOsszes.Cells[14,osszam] := Format('%.2f', [-1 * uzkolt + selkm1 * sedij1 + sextra1 + felra1 * feldij]);	// Az els� bev�teleinek
									SGOsszes.Cells[16,osszam] := Format('%.2f', [FogyasztasDlg.GetSoforKm(Query5.FieldByname('JS_SOFOR').AsString)]);	// A sof�rh�z tartoz� km
								end;
							end;
						end;
						Inc(sorszam);
					end;
					Query5.Next;  // SELECT * FROM JARSOFOR
				end;
			end;


		 {Bet�ltj�k a k�lts�geket a Query1-be}
		 if Query_Run (Query3,'SELECT * FROM KOLTSEG WHERE KS_JAKOD = '''+jaratsz+''' '+
			' AND KS_TETAN = 0 '+
			' ORDER BY KS_RENDSZ DESC, KS_DATUM' ,true) then begin
			while not Query3.EOF do begin
				{K�lts�g a gridbe}
				SG1.RowCount 				 	:= SG1.RowCount + 1;
				SG1.Cells[0,SG1.RowCount - 1]	:= IntToStr(sorszam);
				Inc(sorszam);
				SG1.Cells[1,SG1.RowCount - 1]	:= Query3.FieldByname('KS_DATUM').AsString;
				SG1.Cells[3,SG1.RowCount - 1]	:= '0';
				tipus 	:= StrToIntdef(Query3.FieldByName('KS_TIPUS').AsString,0);
				case tipus of
					0: // �zemanyag k�lts�gr�l van sz�!
					begin
						{�zemanyag k�lts�gr�l van sz�}
						SG1.Cells[2,SG1.RowCount - 1]	:=Query3.FieldByname('KS_ORSZA').AsString+' '+ '�zemanyag ktg. '+Query3.FieldByname('KS_RENDSZ').AsString+
							' ('+Query3.FieldByname('KS_KMORA').AsString+' km; '+
							Format('%.1f',[Query3.FieldByname('KS_UZMENY').AsFloat])+' l = '+
							Format('%.2f',[Query3.FieldByname('KS_OSSZEG').AsFloat])+' '+Query3.FieldByname('KS_VALNEM').AsString+
							' * '+Format('%.4f',[Query3.FieldByname('KS_ARFOLY').AsFloat])+' )';
					end;
					1: // Egy�b k�lts�g
					begin
            kjel:='';
						if Query3.FieldByname('KS_LEIRAS').AsString = '' then begin
							SG1.Cells[2,SG1.RowCount - 1]	:=kjel+Query3.FieldByname('KS_ORSZA').AsString+' '+ 'Egy�b k�lts�g';
						end else begin
              if OssznyilDlg.CheckBox2.Checked and ktsg_nemkell(Query3.FieldByname('KS_LEIRAS').AsString) then kjel:='* ';
							SG1.Cells[2,SG1.RowCount - 1]	:=kjel+Query3.FieldByname('KS_ORSZA').AsString+' '+ Query3.FieldByname('KS_LEIRAS').AsString;
						end;
					end;
					2: // AD Blue
					begin
						SG1.Cells[2,SG1.RowCount - 1]	:=Query3.FieldByname('KS_ORSZA').AsString+' '+ 'AD Blue ktg. '+Query3.FieldByname('KS_RENDSZ').AsString+
							' ('+Query3.FieldByname('KS_KMORA').AsString+' km; '+
							Format('%.1f',[Query3.FieldByname('KS_UZMENY').AsFloat])+' l = '+
							Format('%.2f',[Query3.FieldByname('KS_OSSZEG').AsFloat])+' '+Query3.FieldByname('KS_VALNEM').AsString+
							' * '+Format('%.4f',[Query3.FieldByname('KS_ARFOLY').AsFloat])+' )';
					end;
					3: // H�t� tankol�s
					begin
						SG1.Cells[2,SG1.RowCount - 1]	:=Query3.FieldByname('KS_ORSZA').AsString+' '+ 'H�t� tankol�s  '+Query3.FieldByname('KS_RENDSZ').AsString+
							' ('+Query3.FieldByname('KS_KMORA').AsString+' km; '+
							Format('%.1f',[Query3.FieldByname('KS_UZMENY').AsFloat])+' l = '+
							Format('%.2f',[Query3.FieldByname('KS_OSSZEG').AsFloat])+' '+Query3.FieldByname('KS_VALNEM').AsString+
							' * '+Format('%.4f',[Query3.FieldByname('KS_ARFOLY').AsFloat])+' )';
					end;
				end;

				{Ellen�rizz�k, hogy a k�lts�g viszonyhoz van-e kapcsolva. Ha igen, a viszonylatot is �rjuk ki}
				if Query3.FieldByname('KS_VIKOD').AsString <> '' then begin
					if Query_Run (Query2,'SELECT * FROM VISZONY WHERE VI_VIKOD = '''+ Query3.FieldByname('KS_VIKOD').AsString +''' ' ,true) then begin
						SG1.Cells[2,SG1.RowCount - 1]	:= SG1.Cells[2,SG1.RowCount - 1]	+ ' (' +
						Query2.FieldByname('VI_HONNAN').AsString+'-'+Query2.FieldByname('VI_HOVA').AsString + ')';
					end;
				end;
				{Nett� �rt�ket sz�molunk}
//        if Query3.FieldByname('KS_LEIRAS').AsString='Komp' then
//          date
//        else
  			ertek	:= -1 * Query3.FieldByname('KS_OSSZEG').AsFloat * Query3.FieldByname('KS_ARFOLY').AsFloat + Query3.FieldByname('KS_AFAERT').AsFloat;
        if  Query3.FieldByname('KS_VALNEM').AsString = 'EUR' then
          eurertek:= -1 * Query3.FieldByname('KS_OSSZEG').AsFloat
        else
          eurertek:= -1 * GetValueInOtherCur(Query3.Fieldbyname('KS_OSSZEG').Value,Query3.FieldByName('KS_DATUM').AsString,Query3.Fieldbyname('KS_VALNEM').Value,'EUR' );

				SG1.Cells[4,SG1.RowCount - 1]	:= Query1.FieldByName('JA_KOD').AsString;
				ksatlag	:= GetKoltsegAtlag(Query3.FieldByName('KS_KTKOD').AsString);
{				Query_Update (Query5, 'KOLTSEG',
					[
					'KS_ATLAG',		SqlSzamString(ksatlag,10,2)
					], ' WHERE KS_KTKOD = '+Query3.FieldByName('KS_KTKOD').AsString );
  }
				SG1.Cells[5,SG1.RowCount - 1]	:= SzamString( ksatlag, 10, 2);
				if tipus = 2 then begin   // AD Blue
//					SG1.Cells[5,SG1.RowCount - 1]	:= Format('%.2f',[FogyasztasDlg.GetBlueAtlag(StrToIntDef(Query3.FieldByname('KS_KMORA').AsString,0))]);
					SG1.Cells[5,SG1.RowCount - 1]	:= Format('%.2f',[GetAdBlueKoltsegAtlag(Query3.FieldByName('KS_KTKOD').AsString)]);
				end;
				if ( ( tipus <> 0 ) and ( tipus <> 3 ) and ( tipus <> 2 ) ) then begin		// Az �zemanyag k�lts�get + Ad blue -t + h�t� tankol�st nem vonjuk le, mert azt ut�na sz�moljuk
					SG1.Cells[3,SG1.RowCount - 1]	:= SzamString( ertek, 12, 0);
					ossz	:= ossz + ertek;
					eurossz	:= eurossz + eurertek;

          if  copy(SG1.Cells[2,SG1.RowCount - 1],1,1)<>'*' then  // nem minden k�lts�get adunk hozz�
          begin
  					ossz2	:= ossz2 + ertek;
  					eurossz2	:= eurossz2 + eurertek;
          end;
				end;
				Query3.Next;   // SELECT * FROM KOLTSEG
			end;
		 end;

		{A g�pkocsi �zemanyag k�lts�g�nek berak�sa}
		// Ha nincs ilyen adat, ezt ki kellene hagyni!!!
		SG1.RowCount 						:= SG1.RowCount + 1;
		SG1.Cells[0,SG1.RowCount - 1]		:= IntToStr(sorszam);
    TenyFogyasztas:= FogyasztasDlg.GetTenyFogyasztas;
    Megtakaritas:= FogyasztasDlg.GetMegtakaritas;
    if FogyasztasDlg.ErvenyesTenyFogyasztas then begin
        Elotag:= '';
        S1:= Format('  %.2f', [TenyFogyasztas]);
        S2:= Format( '%.2f', [TenyFogyasztas * 100/FogyasztasDlg.GetOsszKm]);
        S3:= SzamString( -1 * FogyasztasDlg.GetKoltseg, 12, 0);
        end
    else begin
        ervenytelen_fogyasztas_grandtotal:= true;  // innent�l az �sszes�t�n se legyen fogyaszt�s �rt�k
        SGOsszes.Cells[20, osszam]:='1';  // a csoport �sszesen is �rv�nytelen
        Elotag:= '* ';
        S1:= nem_szamolhato_cimke;
        S2:= nem_szamolhato_cimke_rovid;
        S3:= nem_szamolhato_cimke_rovid;
        end;
    SG1.Cells[1,SG1.RowCount - 1]		:= Elotag+'�.a k�lts�g';
		SG1.Cells[2,SG1.RowCount - 1]		:= 'Sz�m�tott �zemanyag k�lts�g  ('+Query1.FieldByname('JA_RENDSZ').AsString+
       		Format('  %.0f km  ',  [FogyasztasDlg.GetOsszKm])+ S1 + ' liter )';
		SG1.Cells[3,SG1.RowCount - 1]		:= S3;
		SG1.Cells[4,SG1.RowCount - 1]		:= Query1.FieldByName('JA_KOD').AsString;
		if FogyasztasDlg.GetOsszKm <> 0 then begin
			SG1.Cells[5,SG1.RowCount - 1]		:= S2;
		end else begin
			SG1.Cells[5,SG1.RowCount - 1]		:= '0.00';
		end;
		szam  								:= FogyasztasDlg.GetKoltseg;
		ossz  								:= ossz - szam;
		ossz2  								:= ossz2- szam;
    eurertek:=GetValueInOtherCur(szam,Query1.FieldByName('JA_JKEZD').AsString,'HUF','EUR' );
		eurossz  		:= eurossz - eurertek;
		eurossz2  	:= eurossz2- eurertek;
		Inc(sorszam);

		{A g�pkocsi Ad-blue k�lts�g�nek berak�sa -> Ha nincs ilyen adat, ezt ki kell hagyni!!!}
		Query_Run (Query5, 'SELECT * FROM KOLTSEG WHERE KS_RENDSZ = '''+Query1.FieldByname('JA_RENDSZ').AsString+''' AND KS_TIPUS = 2 ' );
		if Query5.RecordCount > 0 then begin
			SG1.RowCount 						:= SG1.RowCount + 1;
			SG1.Cells[0,SG1.RowCount - 1]		:= IntToStr(sorszam);
			SG1.Cells[1,SG1.RowCount - 1]		:= 'Ad-blue k�lts�g';
			SG1.Cells[2,SG1.RowCount - 1]		:= 'Sz�m�tott Ad-blue k�lts�g  ('+Query1.FieldByname('JA_RENDSZ').AsString+
				Format('  %.0f km  ', [FogyasztasDlg.GetOsszKm])+Format('  %.2f liter ) ', [FogyasztasDlg.GetBlueFogyasztas]);
			SG1.Cells[3,SG1.RowCount - 1]		:= SzamString( -1 * FogyasztasDlg.GetBlueKoltseg, 12, 0);
			SG1.Cells[4,SG1.RowCount - 1]		:= Query1.FieldByName('JA_KOD').AsString;
			if FogyasztasDlg.GetOsszKm <> 0 then begin
				SG1.Cells[5,SG1.RowCount - 1]		:= Format( '%.2f', [FogyasztasDlg.GetBlueFogyasztas * 100/FogyasztasDlg.GetOsszKm]);
			end else begin
				SG1.Cells[5,SG1.RowCount - 1]		:= '0.00';
			end;
			ossz  								:= ossz - FogyasztasDlg.GetBlueKoltseg;
			ossz2  								:= ossz2- FogyasztasDlg.GetBlueKoltseg;
      eurertek:=GetValueInOtherCur(FogyasztasDlg.GetBlueKoltseg,Query1.FieldByName('JA_JKEZD').AsString,'HUF','EUR' );
	  	eurossz  		:= eurossz - eurertek;
		  eurossz2  	:= eurossz2- eurertek;
			Inc(sorszam);
		end;

		// A h�t� tankol�sok berak�sa
		Query_Run (Query2,'SELECT * FROM JARPOTOS WHERE JP_JAKOD = '''+jaratsz+''' ',true);
		while not Query2.EOF do begin
			if HutoPotkocsi(Query2.FieldByname('JP_POREN').AsString) then begin
				SG1.RowCount := SG1.RowCount + 1;
				SG1.Cells[0,SG1.RowCount - 1]	:= IntToStr(sorszam);
				Inc(sorszam);
				SG1.Cells[1,SG1.RowCount - 1]	:= 'H�t� tank.';
				szossz	:= GetHutoOsszeg(Query2.FieldByname('JP_POREN').AsString, Query2.FieldByname('JP_UZEM1').AsString, Query2.FieldByname('JP_UZEM2').AsString);
				SG1.Cells[2,SG1.RowCount - 1]		:= 'Sz�m�tott h�t� ktg ('+Query2.FieldByname('JP_POREN').AsString+
					Format(' %.2f-%.2f = %.0f Ft ) ', [StringSzam(Query2.FieldByName('JP_UZEM1').AsString), StringSzam(Query2.FieldByName('JP_UZEM2').AsString), -1*szossz]);
				SG1.Cells[3,SG1.RowCount - 1]		:= SzamString( -1 * szossz, 12, 0);
				SG1.Cells[4,SG1.RowCount - 1]		:= Query1.FieldByName('JA_KOD').AsString;
				ossz  								:= ossz - szossz;
				ossz2  								:= ossz2- szossz;
        eurertek:=GetValueInOtherCur(szossz,Query1.FieldByName('JA_JKEZD').AsString,'HUF','EUR' );
	    	eurossz  		:= eurossz - eurertek;
		    eurossz2  	:= eurossz2- eurertek;
			end;
			Query2.Next;
		end;
		Query2.Close;

    // kapcsol�d� j�rat(ok) k�lts�ge
    if p_jasza<>'' then
    begin
   	 	SG1.RowCount 						:= SG1.RowCount + 1;
      SG1.Cells[0,SG1.RowCount - 1]		:= IntToStr(sorszam);
  		SG1.Cells[2,SG1.RowCount - 1]		:= 'Alj�ratok k�lts�gei'+' ('+p_jasza+')';
      if p_ktg_ervenyes then begin
        SG1.Cells[1,SG1.RowCount - 1]		:= 'Alj�rat ktg.';
    	  SG1.Cells[3,SG1.RowCount - 1]		:= SzamString( p_ktg, 12, 0)
        end
      else begin
        SG1.Cells[1,SG1.RowCount - 1]		:= '* Alj�rat ktg.';
        SG1.Cells[3,SG1.RowCount - 1]		:= nem_szamolhato_cimke_kozepes;
        end;
  		//SG1.Cells[4,SG1.RowCount - 1]		:= '44'; //Query1.FieldByName('JA_KOD').AsString;
  		//SG1.Cells[5,SG1.RowCount - 1]		:= '55'; //SzamString( p_ktg, 12, 0);
  		ossz  								:= ossz + p_ktg;
	  	ossz2  								:= ossz2+ p_ktg;
      eurertek:=GetValueInOtherCur(p_ktg,Query1.FieldByName('JA_JKEZD').AsString,'HUF','EUR' );
	    eurossz  		:= eurossz + eurertek;
		  eurossz2  	:= eurossz2+ eurertek;
		  Inc(sorszam);
    end;


		 {K�lts�g �sszesen bet�lt�se}
		 SG1.RowCount 					:= SG1.RowCount + 1;
		 SG1.Cells[0,SG1.RowCount - 1]	:= '';
		 //SG1.Cells[1,SG1.RowCount - 1]	:= '---';
		 SG1.Cells[1,SG1.RowCount - 1]	:= '';
		 SG1.Cells[2,SG1.RowCount - 1]	:= 'K�lts�gek �sszesen';
		 SG1.Cells[3,SG1.RowCount - 1]	:= SzamString( ossz, 12, 0);
		 SG1.Cells[4,SG1.RowCount - 1]	:= Query1.FieldByName('JA_KOD').AsString;
     //if OssznyilDlg.CheckBox2.Checked then        // Bizonyos k�lts�geket NE vegyen figyelembe
     //   ossz:=ossz2;
		 okolts	:= okolts + ossz;
		 okolts2:= okolts2+ ossz2;
		 eurokolts	:= eurokolts + eurossz;
		 eurokolts2 := eurokolts2+ eurossz2;

		 {A fuvard�jak bet�lt�se}
		 ertek 			:= 0;
		 eurertek		:= 0;
		 vevonev  		:= '';
		 szamlaszam  	:= '';
		 if Query_Run (Query2,'SELECT * FROM VISZONY WHERE VI_JAKOD = '''+jaratsz+''' ',true) then begin
			while not Query2.EOF do begin
			   SG1.RowCount := SG1.RowCount + 1;
			   SG1.Cells[0,SG1.RowCount - 1]	:= IntToStr(sorszam);
			   Inc(sorszam);
			   SG1.Cells[1,SG1.RowCount - 1]	:= 'Fuvard�j';
			   SG1.Cells[2,SG1.RowCount - 1]	:= Query2.FieldByname('VI_HONNAN').AsString+'-'+
					Query2.FieldByname('VI_HOVA').AsString;
         // 6. oszlop: csak a viszonylat
         SG1.Cells[6,SG1.RowCount - 1]	:= Query2.FieldByname('VI_HONNAN').AsString+'-'+
  					Query2.FieldByname('VI_HOVA').AsString;
			   szossz	:= Query2.FieldByname('VI_FDEXP').AsFloat * Query2.FieldByname('VI_ARFOLY').AsFloat;
			   {Megkeress�k a sz�ml�t a viszonyhoz}
			   if Query2.FieldByname('VI_SAKOD').AsString <> '' then begin
				  {Van hozz� sz�mla}
				  if Query_Run (Query3,'SELECT * FROM SZFEJ WHERE SA_UJKOD = '+
					 Query2.FieldByname('VI_UJKOD').AsString,true) then begin
					 if Query3.RecordCount > 0 then begin
						vevonev		:= Query3.FieldByName('SA_VEVONEV').AsString;
						szamlaszam	:= Query3.FieldByName('SA_KOD').AsString;
						szossz		:= Query3.FieldByname('SA_OSSZEG').AsFloat;
						ardat		:= Query3.FieldByName('SA_TEDAT').AsString;
						euralap 		:= 0;
						{A valut�s �rt�k meghat�roz�sa}
						if Query_Run (Query3,'SELECT * FROM SZSOR WHERE SS_UJKOD = '+
						   Query2.FieldByname('VI_UJKOD').AsString,true) then begin
               szossz:=0;
						   while not Query3.EOF do begin

							  if Query3.FieldByName('SS_EGYAR').AsFloat <> 0 then begin
								 ertek2	:= Query3.FieldByName('SS_EGYAR').AsFloat * Query3.FieldByName('SS_DARAB').AsFloat;
                 szossz:=szossz+ertek2;
								 if ( ( Query3.FieldByName('SS_VALERT').AsFloat <> 0 ) AND
									(Query3.FieldByName('SS_VALNEM').AsString = 'EUR' ) )then begin
									eurarf	:= Query3.FieldByName('SS_VALARF').AsFloat;
								 end else begin
									{Mindenk�ppen ki�rjuk a valut�s �rt�ket}
									if Query3.FieldByName('SS_ARDAT').AsString <> '' then begin
									   ardat		:= Query3.FieldByName('SS_ARDAT').AsString;
									end;
									arfoly   := Query3.FieldByName('SS_ARFOL').AsFloat;
									if arfoly > 1 then begin
									   eurarf	:= arfoly;
									end else begin
									   eurarf := EgyebDlg.ArfolyamErtek( 'EUR', ardat, true);
									end;
								 end;
								 {Az eur�s �rt�kek hozz�rendel�se az �faalapokhoz}
								 if eurarf = 0 then begin
									eurarf := 10000000;
								 end;
								 euralap	:= euralap  + ertek2/eurarf;
							  end;
							  Query3.Next;
						   end;
						end;
						val_oert 	:= euralap;
						eurertek	:= eurertek + euralap;
						if val_oert <> 0 then begin
							{A sor ki�r�sa + vev�n�v + sz�mlasz�m}
						   SG1.Cells[2,SG1.RowCount - 1]	:= SG1.Cells[2,SG1.RowCount - 1]	+ Format('  ( %.1f EUR )',[val_oert])+
								   ' '+vevonev+'  ('+szamlaszam+')';
               // 7. oszlop: csak a sz�mla�sszeg
						   SG1.Cells[7,SG1.RowCount - 1]:= Format('%.1f',[val_oert]);
               // 8. oszlop: vev� neve
						   SG1.Cells[8,SG1.RowCount - 1]:= vevonev;
               // 9. oszlop: sz�mlasz�m
						   SG1.Cells[9,SG1.RowCount - 1]:= szamlaszam;
 							 osszeur	:= osszeur + val_oert;
						end;
					 end;
				  end;
				  SG1.Cells[3,SG1.RowCount - 1]	:= SzamString( szossz, 12, 0);
				end else begin
				  // Nincs hozz� m�g sz�mla
				  SG1.Cells[2,SG1.RowCount - 1]	:= SG1.Cells[2,SG1.RowCount - 1] + ' ( Hi�nyz� sz�mla : '+
          					Format( '%.0f',[szossz]) + ' )';
          // 9. oszlop: sz�mlasz�m
 			    SG1.Cells[9,SG1.RowCount - 1]:= SG1.Cells[9,SG1.RowCount - 1] + ' ( Hi�nyz� sz�mla : '+
          					Format( '%.0f',[szossz]) + ' )';
				  SG1.Cells[3,SG1.RowCount - 1]	:= '0';
				  szossz	:= 0;
			   end;
			   SG1.Cells[4,SG1.RowCount - 1]	:= Query1.FieldByName('JA_KOD').AsString;
			   ertek	:= ertek +  szossz;
			   Query2.Next;
			end;
			Query2.Close;
		 end;
		 {Az �ssz bev�tel ki�r�sa}
		 SG1.RowCount 				   	:= SG1.RowCount + 1;
		 SG1.Cells[0,SG1.RowCount - 1]	:= '';
		 SG1.Cells[1,SG1.RowCount - 1]	:= '---';
		 {Ide j�n a fuvard�j / �sz km h�nyados}
		 SG1.Cells[2,SG1.RowCount - 1]	:= 'Fuvard�jak �sszesen  / �ssz km : '+
			Format('%.0f',[Query1.FieldByName('JA_OSSZKM').AsFloat+p_km]);
		 if Query1.FieldByName('JA_OSSZKM').AsFloat = 0 then begin
			SG1.Cells[2,SG1.RowCount - 1]	:= SG1.Cells[2,SG1.RowCount - 1]	+
			   ' / ';
		 end else begin
			SG1.Cells[2,SG1.RowCount - 1]	:= SG1.Cells[2,SG1.RowCount - 1]	+
				' -> FD/OKm = '+ Format('%.2f',[ertek / (Query1.FieldByName('JA_OSSZKM').AsFloat+p_km)]) +
			   ' / ';
		 end;
		 SG1.Cells[3,SG1.RowCount - 1]	:= SzamString( ertek, 12, 0);
		 SG1.Cells[4,SG1.RowCount - 1]	:= Query1.FieldByName('JA_KOD').AsString;
		 ofuvar	:= ofuvar + ertek;
		 jaratatlag						:= EgyebDlg.JaratAtlag(jaratsz);
		 // *****************************************************************
		 SG1.RowCount 				 	:= SG1.RowCount + 1;
		 SG1.Cells[0,SG1.RowCount - 1]	:= '';
		 SG1.Cells[1,SG1.RowCount - 1]	:= '---';
     if FogyasztasDlg.ErvenyesTenyFogyasztas then begin
        S1:= Format('%.1f liter',[jaratatlag]);
        S2:= Format('%.1f liter ',[TenyFogyasztas]);
        end
     else begin
        S1:= nem_szamolhato_cimke;
        S1:= nem_szamolhato_cimke;
        end;
		 SG1.Cells[2,SG1.RowCount - 1]	:= jojarat + ' j�rat egyenleg:    �tlag : '+S1+ '   Teljes fogyaszt�s : '+ S2;
		 SG1.Cells[3,SG1.RowCount - 1]	:= SzamString( ertek+ossz, 12, 0);	// Nyeres�g!!! (fuvard�j - k�lts�g)
		 SG1.Cells[4,SG1.RowCount - 1]	:= Query1.FieldByName('JA_KOD').AsString;
			// A j�rat 2. sora
		 SG1.RowCount 				 	:= SG1.RowCount + 1;
		 SG1.Cells[0,SG1.RowCount - 1]	:= '';
		 SG1.Cells[1,SG1.RowCount - 1]	:= '---';
		 osszkm							:= FogyasztasDlg.GetOsszKm;
		 if osszkm = 0 then begin
				SG1.Cells[2,SG1.RowCount - 1]	:= copy('                    ', 1, Length(jojarat)) +
					' Megtakar�t�s : 0.00 liter -> 0.00 liter/100km    �tlags�ly : 0.00 t';
			end else begin
        // if Megtakaritas > 0 then  // lehet pozit�v �s negat�v is!
        if FogyasztasDlg.ErvenyesTenyFogyasztas then
           S1:= Format('%.2f liter -> %.2f  liter/100km',[Megtakaritas, Megtakaritas * 100 / osszkm])
        else
           S1:= nem_szamolhato_cimke;
				SG1.Cells[2,SG1.RowCount - 1]	:= copy('                    ', 1, Length(jojarat)) + ' Megtakar�t�s : '+ S1 +
					Format(' �tlags�ly : %.2f t',[FogyasztasDlg.GetSulyAtlag]);
			  end;
		 SG1.Cells[3,SG1.RowCount - 1]	:= '0';
		 SG1.Cells[4,SG1.RowCount - 1]	:= Query1.FieldByName('JA_KOD').AsString;

			ertekek[1]		:= ertekek[1] + FogyasztasDlg.GetOsszKm + p_km;
			ertekek[2]		:= ertekek[2] + TenyFogyasztas;
			ertekek[3]		:= ertekek[3] + FogyasztasDlg.GetTervFogyasztas;
			ertekek[4]		:= ertekek[4] + Megtakaritas;
			ertekek[5]		:= ertekek[5] + FogyasztasDlg.GetSulyAtlag * FogyasztasDlg.GetOsszKm ;
      // S:= 'OERTEK= '+FormatFloat('0', oertek)+', ';
			oertek			:= oertek + ertek + ossz;    // ertek: a sz�ml�zott �sszeg   ossz: k�lts�gek �sszege  --> oertek: szumma nyeres�g
      // S:=S+ 'ertek='+FormatFloat('0', ertek);
      // S:=S+ 'ossz='+FormatFloat('0', ossz);
      // S:=S+ '--> oertek='+FormatFloat('0', oertek);
      // Query_Log_Str(S, 0, true);

			// Az �sszes�t�sek berak�sa a j�rat sorba
			//SGOsszes.Cells[4,osszam] := Format('%0f', [FogyasztasDlg.GetOsszKm+p_km]);
			SGOsszes.Cells[4,osszam] := Format('%0f', [FogyasztasDlg.GetOsszKm]);
			SGOsszes.Cells[5,osszam] := Format('%0f', [ertek]);
			SGOsszes.Cells[6,osszam] := Format('%2f', [eurertek]);
			SGOsszes.Cells[7,osszam] := Format('%0f', [ossz]); 					// K�lts�g Ft
			SGOsszes.Cells[17,osszam]:= Format('%0f', [ossz2]); 					// K�lts�g Ft
			SGOsszes.Cells[8,osszam] := '0.00';
			// Az EUR k�lts�g kisz�m�t�sa a j�rat indul� d�tum alapj�n
			arfoly   := EgyebDlg.ArfolyamErtek( 'EUR', Query1.FieldByName('JA_JKEZD').AsString, true);
			if arfoly > 0 then begin
				//SGOsszes.Cells[ 8,osszam] := Format('%2f', [ossz  / arfoly]);						// K�lts�g EUR
				//SGOsszes.Cells[18,osszam] := Format('%2f', [ossz2 / arfoly]);						// K�lts�g EUR
				SGOsszes.Cells[ 8,osszam] := Format('%2f', [eurossz ]);						// K�lts�g EUR
				SGOsszes.Cells[18,osszam] := Format('%2f', [eurossz2]);						// K�lts�g EUR
			end;
			SGOsszes.Cells[9,osszam] := Format('%2f', [jaratatlag]);	  			// �tlag fogyaszt�s
			SGOsszes.Cells[10,osszam]:= Format('%2f', [Megtakaritas]);	// Megtakar�t�s
      oeurkolts :=oeurkolts + StringSzam(SGOsszes.Cells[ 8, osszam]);
      oeurkolts2:=oeurkolts2+ StringSzam(SGOsszes.Cells[18, osszam]);
      oeurbevet :=oeurbevet + StringSzam(SGOsszes.Cells[ 6, osszam]);
			ossz			:= 0;
			ossz2			:= 0;
			eurossz			:= 0;
			eurossz2			:= 0;
			Query1.Next;  // SELECT * FROM JARAT
		end;
		Query1.EnableControls;
	end;
	// A grid ki�r�sa
//	SaveGridToFile(SG1, 'TESZT001.TXT' );
	{A nyeres�g megjelen�t�se}
	QL1.Caption			:= SzamString( okolts, 12, 0);
	QL2.Caption			:= SzamString( ofuvar, 12, 0);
	QL3.Caption			:= SzamString( oertek, 12, 0);

	if Ervenytelen_osszkilometer then
        QL4.Caption	:= nem_szamolhato_cimke_kozepes
  		else QL4.Caption			:= Format('%.0f km', [ ertekek[1]]);
//  if p_km<>0 then
//    QL4.Caption:=QL4.Caption+'  (+ '+IntToStr(p_km)+' km)';
  if ervenytelen_fogyasztas_grandtotal then
    QL5.Caption:= nem_szamolhato_cimke
  else
  	QL5.Caption:= Format('%.0f l', [ ertekek[2]]);
	QL6.Caption			:= Format('%.0f l', [ ertekek[3]]);
	if ertekek[1] = 0 then begin
		QL7.Caption		:= '0.00 l -> 0.00 l/100km';
	end else begin
    if ervenytelen_fogyasztas_grandtotal then
      QL7.Caption:= nem_szamolhato_cimke
    else
  		QL7.Caption		:= Format(' %.2f l  ->  %.2f  l/100km',[ertekek[4], ertekek[4] *100 / (ertekek[1]-op_km)])
	end;
	if ertekek[1] = 0 then begin
		QL8.Caption		:= '0 t';
		QL9.Caption		:= '0 l';
		QL10.Caption	:= '0.00';
		QL11.Caption	:= '0.00';
		QL20.Caption	:= '0.00';
	end else begin
		QL8.Caption		:= Format('%.2f', [ ertekek[5] / ertekek[1]])+ ' t';
    if ervenytelen_fogyasztas_grandtotal then
      QL9.Caption:= nem_szamolhato_cimke_kozepes
    else
	  	QL9.Caption		:= Format('%.2f', [ ertekek[2] * 100 / (ertekek[1]-op_km)])+ ' l';
		//QL10.Caption	:= Format('%.0f Ft/km (%.3f EUR/km) ', [ ofuvar / (ertekek[1]), osszeur / (ertekek[1]) ]);

    QRLabel29.Enabled:=OssznyilDlg.CheckBox2.Checked;
    QRLabel32.Enabled:=OssznyilDlg.CheckBox2.Checked;
    if OssznyilDlg.CheckBox2.Checked then begin
      if Ervenytelen_osszkilometer then
        QL11.Caption	:= nem_szamolhato_cimke_kozepes
  		else QL11.Caption	:= Format('%.0f Ft/km', [ -1 * okolts2 / (ertekek[1])]);
      QRLabel29.Caption:= '*('+SzamString( okolts2, 12, 0)+')';
      QRLabel32.Caption:= '*('+SzamString( ofuvar+okolts-okolts2, 12, 0)+')';
	  	arfoly   := EgyebDlg.ArfolyamErtek( 'EUR', kezdodatum, true);
		  if arfoly > 0 then begin
			  if Ervenytelen_osszkilometer then
            QL11.Caption	:= nem_szamolhato_cimke_kozepes
    		else QL11.Caption	:= Format('%.0f Ft/km (%.3f EUR/km)', [ -1 * okolts2 / (ertekek[1]), -1 * okolts2 / (ertekek[1]) / arfoly ]);
			  //QL11.Caption	:= Format('%.0f Ft/km (%.3f EUR/km)', [ -1 * okolts2 / (ertekek[1]), -1 * eurokolts2 / (ertekek[1]) ]);
		  end;
  		//QL10.Caption	:= Format('%.0f Ft/km (%.3f EUR/km) ', [ (ofuvar-okolts+okolts2) / (ertekek[1]), (oeurbevet-oeurkolts+oeurkolts2) / (ertekek[1]) ]);
  		if Ervenytelen_osszkilometer then
        QL10.Caption	:= nem_szamolhato_cimke_kozepes
  		else QL10.Caption	:= Format('%.0f Ft/km (%.3f EUR/km) ', [ (ofuvar+okolts-okolts2) / (ertekek[1]), (oeurbevet+oeurkolts-oeurkolts2) / (ertekek[1]) ]);
  		if Ervenytelen_osszkilometer then
        QL20.Caption	:= nem_szamolhato_cimke_kozepes
  		else QL20.Caption	:= Format('%.0f Ft/km (%.3f EUR/km) ', [ (oertek) / (ertekek[1]), (osszeur+oeurkolts) / (ertekek[1]) ]);
      // S:= 'OERTEK='+FormatFloat('0', oertek);
      // S:=S+ 'ertekek[1]='+FormatFloat('0', ertekek[1]);
      // Query_Log_Str(S, 0, true);
    end
    else
    begin
  		if Ervenytelen_osszkilometer then
        QL11.Caption	:= nem_szamolhato_cimke_kozepes
  		else QL11.Caption	:= Format('%.0f Ft/km', [ -1 * okolts / (ertekek[1])]);
	  	arfoly   := EgyebDlg.ArfolyamErtek( 'EUR', kezdodatum, true);
		  if arfoly > 0 then begin
			  if Ervenytelen_osszkilometer then
          QL11.Caption	:= nem_szamolhato_cimke_kozepes
  		  else QL11.Caption	:= Format('%.0f Ft/km (%.3f EUR/km)', [ -1 * okolts / (ertekek[1]), -1 * okolts / (ertekek[1]) / arfoly ]);
		  end;
  		if Ervenytelen_osszkilometer then
        QL10.Caption	:= nem_szamolhato_cimke_kozepes
  		else QL10.Caption	:= Format('%.0f Ft/km (%.3f EUR/km) ', [ ofuvar / (ertekek[1]), osszeur / (ertekek[1]) ]);
  		if Ervenytelen_osszkilometer then
        QL20.Caption	:= nem_szamolhato_cimke_kozepes
  		else QL20.Caption	:= Format('%.0f Ft/km (%.3f EUR/km) ', [ (oertek) / (ertekek[1]), (osszeur-oeurkolts) / (ertekek[1]) ]);
    end;
	end;
	//QRLabel34.Caption	:= Format('( %.2f EUR)', [ oeurkolts]);
	QRLabel34.Caption	:= Format('( %.2f EUR)', [ eurokolts]);
	QRLabel22.Caption	:= Format('( %.2f EUR)', [ osszeur]);
	//QRLabel36.Caption	:= Format('( %.2f EUR)', [ osszeur+oeurkolts]);
	QRLabel36.Caption	:= Format('( %.2f EUR)', [ osszeur+eurokolts]);

end;

procedure TOsszlisDlg.QRBand1BeforePrint(Sender: TQRCustomBand;
	   var PrintBand: Boolean);
var
  S: string;
begin
	QS3.Width			:= QRLabel12.Width;
	if ( ( Pos( 'Fuvar', SG1.Cells[1,rekszam] ) > 0 ) or ( Pos( 'Fizet�s', SG1.Cells[1,rekszam] ) > 0 )
		or ( Pos( 'Fogyaszt', SG1.Cells[1,rekszam] ) > 0 ) ) then begin
		QS3.Width		:= QRLabel12.Width + QrShape1.Width;
	end;
	QRBand1.Color		:= clWhite;
	QSA1.Enabled		:= true;
	QS3.Color			:= clWhite;
	QS3.Width			:= QrLabel12.Width;
	QS3.Left			:= QSA1.Left + 4;
	QS2.Caption			:= SG1.Cells[1,rekszam];
	QS3.Caption			:= SG1.Cells[2,rekszam];
	QS30.Caption 		:= SG1.Cells[5,rekszam];
  S:= Trim(SG1.Cells[3,rekszam]);
  if JoEgesz(copy(S,1,1)) then
   	QS4.Caption 		:= SzamString(StringSzam(S), 12, 0)  // form�zunk, ha sz�m
  else QS4.Caption 		:= S;
	ChildBand1.Enabled	:= ( SG1.Cells[1,rekszam] = '--+' );		// J�ratc�m ut�n megjelenik az �sszes�t�s
	if SG1.Cells[1,rekszam] = '--+' then begin
		{�sszesen sor}
		QS2.Caption			:= '';
		QS4.Caption			:= '';
		QS30.Caption		:= '';
		QSA1.Enabled		:= false;
		QRBand1.Color		:= clAqua;
		QS3.Width			:= QRBand1.Width - 20;
		QS3.Left			:= 10;
		QS3.Color			:= clAqua;
	end else begin
		if SG1.Cells[1,rekszam] = '---' then begin
			{�sszesen sor}
			QS2.Caption			:= '';
			QS4.Caption			:= '';
			QS30.Caption		:= '';
			QS3.Width			:= QrLabel12.Width + QrLabel9.Width + QrLabel24.Width;
			QSA1.Enabled		:= false;
		end;
	 end;
	 Inc(rekszam);
end;

procedure TOsszlisDlg.RepBeforePrint(Sender: TCustomQuickRep;
	   var PrintReport: Boolean);
begin
	rekszam		:= 1;
	osszeur		:= 0;
end;

procedure TOsszlisDlg.RepNeedData(Sender: TObject; var MoreData: Boolean);
begin
	MoreData	:= true;
	if rekszam >= SG1.RowCount then begin
		MoreData	:= false;
	end;
end;

procedure TOsszlisDlg.FormDestroy(Sender: TObject);
begin
	FogyasztasDlg.Destroy;
end;

procedure TOsszlisDlg.ChildBand1BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
	jasz		: string;
	rsz			: integer;
	osszkm		: double;
	gkkat		: string;
	ertekszin	: integer;
	bevetel		: double;
	koltseg		: double;
	nyereseg	: double;
	szaz		: double;
  p_km: integer;
begin
	jasz			:= SG1.Cells[4,rekszam-1];
	rsz				:= SGOsszes.Cols[0].IndexOf(jasz);
	QKI1.Caption	:= '';
	QKI2.Caption	:= '';
	QKI3.Caption	:= '';
	QKI4.Caption	:= '';
	QKI5.Caption	:= '';
	QKI6.Caption	:= '';
	QKI7.Caption	:= '';
	QKI8.Caption	:= '';
	osszkm			:= 100000000;
	if StringSzam(SGOsszes.Cells[4, rsz]) > 0 then begin
		osszkm	:= StringSzam(SGOsszes.Cells[4, rsz]);
    if OssznyilDlg.M1.Text<>'' then
  		osszkm	:= FogyasztasDlg.GetOsszKm;
	end;
  p_km:= StrToIntdef(SGOsszes.Cells[19, rsz], 0); // az elmentett �rt�ket fogjuk haszn�lni
	//osszkm	:= FogyasztasDlg.GetOsszKm;
	if rsz = -1 then begin
		QRLabel28.Caption := 'Hi�nyz� j�ratsz�m : '+jasz;
		QRLabel27.Caption := '';
	end else begin
		//gkkat			:= Query_Select('GEPKOCSI', 'GK_RESZ', SGOsszes.Cells[2, rsz], 'GK_GKKAT');
		gkkat			:= EgyebDlg.GkTipus(SGOsszes.Cells[2, rsz]);
		QRLabel28.Caption := SGOsszes.Cells[1, rsz];
		QRLabel27.Caption := SGOsszes.Cells[2, rsz]+' ('+gkkat+')';
		gkkat			:= EgyebDlg.GkTipus(SGOsszes.Cells[3, rsz]);     // p�t
		QRLabel27.Caption := QRLabel27.Caption+';'+SGOsszes.Cells[3, rsz]+' ('+gkkat+')';
		// V�gigmegy�nk a sof�r�k�n �s kisz�moljuk a %-kokat
		QSofor1.Caption	:= '';
		QSofor2.Caption	:= '';
		QS1A.Caption	:= '';
		QS1B.Caption	:= '';
		QS2A.Caption	:= '';
		QS2B.Caption	:= '';
		ertekszin		:= clAqua;
		if SGOsszes.Cells[11, rsz] <> '' then begin
			// Az els� sof�r bet�lt�se
			Query_Run(Query5, 'SELECT SUM(JS_SZAKM) OKM FROM JARSOFOR WHERE JS_JAKOD = '''+jasz+''' AND JS_SOFOR = '''+SGOsszes.Cells[11, rsz]+''' ');
			szaz	:= StringSzam(Query5.FieldByName('OKM').AsString) * 100/ osszkm ;
			QSofor1.Caption	:= Query_Select ('DOLGOZO', 'DO_KOD', SGOsszes.Cells[11, rsz] , 'DO_NAME');
			QS1B.Caption	:= Format('(K/T: %5.1f%%)',[szaz]);
			if StringSzam(SGOsszes.Cells[15, rsz]) > 0 then begin
				QS1A.Caption	:= Format('(%6.2f Ft/km)',[StringSzam(SGOsszes.Cells[13, rsz])/StringSzam(SGOsszes.Cells[15, rsz])]);
			end else begin
				QS1A.Caption	:= '  0.00 Ft/km)';
			end;
			// Az K/T% elem sz�nez�se
			if szaz > maximumkt then begin
				ertekszin	:= clLime;
			end;
			if szaz < minimumkt then begin
				ertekszin	:= clRed;
			end;
			QS1B.Color	:= ertekszin;
		end;
		ertekszin		:= clAqua;
		if SGOsszes.Cells[12, rsz] <> '' then begin
			// A m�sodik sof�r bet�lt�se
			Query_Run(Query5, 'SELECT SUM(JS_SZAKM) OKM FROM JARSOFOR WHERE JS_JAKOD = '''+jasz+''' AND JS_SOFOR = '''+SGOsszes.Cells[12, rsz]+''' ');
			szaz	:= StringSzam(Query5.FieldByName('OKM').AsString) * 100/ osszkm ;
			QSofor2.Caption	:= Query_Select ('DOLGOZO', 'DO_KOD', SGOsszes.Cells[12, rsz] , 'DO_NAME');
			QS2B.Caption	:= Format('(K/T: %5.1f%%)',[szaz]);
			if StringSzam(SGOsszes.Cells[16, rsz]) > 0 then begin
				QS2A.Caption	:= Format('(%6.2f Ft/km)',[StringSzam(SGOsszes.Cells[14, rsz])/StringSzam(SGOsszes.Cells[16, rsz])]);
			end else begin
				QS2A.Caption	:= '  0.00 Ft/km)';
			end;
			// Az elem sz�nez�se
			if szaz > maximumkt then begin
				ertekszin	:= clLime;
			end;
			if szaz < minimumkt then begin
				ertekszin	:= clRed;
			end;
			QS2B.Color	:= ertekszin;
		end;
    if SGOsszes.Cells[21, rsz]='1' then begin
         QKI1.Caption	:= nem_szamolhato_cimke_kozepes;
         QKI2.Caption	:= nem_szamolhato_cimke_kozepes;
         QKI3.Caption	:= nem_szamolhato_cimke_kozepes;
         QKI4.Caption	:= nem_szamolhato_cimke_kozepes;
         end
    else begin
      if OssznyilDlg.CheckBox2.Checked then begin
          QKI1.Caption	:= Format('%.0f Ft/km',   [ (StringSzam(SGOsszes.Cells[5, rsz])+StringSzam(SGOsszes.Cells[7, rsz])-StringSzam(SGOsszes.Cells[17, rsz])) / (osszkm+p_km) ]);
          QKI2.Caption	:= Format('%.3f EUR/km',  [ (StringSzam(SGOsszes.Cells[6, rsz])+StringSzam(SGOsszes.Cells[8, rsz])-StringSzam(SGOsszes.Cells[18, rsz])) / (osszkm+p_km) ]);
          // K�lts�g
          QKI3.Caption	:= Format('%.0f Ft/km',   [ -1 * StringSzam(SGOsszes.Cells[17, rsz]) / (osszkm+p_km) ]);
          QKI4.Caption	:= Format('%.3f EUR/km*', [ -1 * StringSzam(SGOsszes.Cells[18, rsz]) / (osszkm+p_km) ]);
          end
      else begin
          // Bev�tel
          QKI1.Caption	:= Format('%.0f Ft/km',   [ StringSzam(SGOsszes.Cells[5, rsz]) / (osszkm+p_km) ]);
          QKI2.Caption	:= Format('%.3f EUR/km',  [ StringSzam(SGOsszes.Cells[6, rsz]) / (osszkm+p_km) ]);
          // K�lts�g
          QKI3.Caption	:= Format('%.0f Ft/km',   [ -1 * StringSzam(SGOsszes.Cells[7, rsz]) / (osszkm+p_km) ]);
          QKI4.Caption	:= Format('%.3f EUR/km*', [ -1 * StringSzam(SGOsszes.Cells[8, rsz]) / (osszkm+p_km) ]);
          end;
      end;  // �rv�nyes a km
		bevetel			:= StringSzam(SGOsszes.Cells[6, rsz]) / osszkm;
		koltseg			:= -1 * StringSzam(SGOsszes.Cells[7, rsz]) / osszkm;
//    oeurkolts :=oeurkolts + StringSzam(SGOsszes.Cells[ 8, rsz]);
//    oeurkolts2:=oeurkolts2+ StringSzam(SGOsszes.Cells[18, rsz]);
//    oeurbevet :=oeurbevet + StringSzam(SGOsszes.Cells[ 6, rsz]);
    if (SGOsszes.Cells[20, rsz] <> '1') and (SGOsszes.Cells[21, rsz] <> '1') then begin  // �rv�nyes a fogyaszt�s �s a km adat
    	QKI5.Caption := Format('%.2f l/100km', [ StringSzam(SGOsszes.Cells[9, rsz])]);  // �tlag
		  QKI6.Caption := Format('%.2f l/100km', [ StringSzam(SGOsszes.Cells[10, rsz])*100 / osszkm ]);		// Megtakar�t�s
      end
    else begin
      QKI5.Caption := nem_szamolhato_cimke;
      QKI6.Caption := nem_szamolhato_cimke;
      end;
		// Nyeres�g
    if SGOsszes.Cells[21, rsz]='1' then begin
         QKI7.Caption	:= nem_szamolhato_cimke_kozepes;
         QKI8.Caption	:= nem_szamolhato_cimke_kozepes;
         end
    else begin
    		QKI7.Caption	:= Format('%.0f Ft/km',   [ (StringSzam(SGOsszes.Cells[5, rsz]) + StringSzam(SGOsszes.Cells[7, rsz])) / (osszkm+p_km) ]);
    		QKI8.Caption	:= Format('%.3f EUR/km*', [ (StringSzam(SGOsszes.Cells[6, rsz]) + StringSzam(SGOsszes.Cells[8, rsz])) / (osszkm+p_km) ]);
        end;  // else
		nyereseg		:= (StringSzam(SGOsszes.Cells[5, rsz]) + StringSzam(SGOsszes.Cells[7, rsz])) / osszkm;

		// Az �rt�kek sz�nez�se
		Query_Run(Query6, 'SELECT * FROM KATINTER WHERE KI_GKKAT = '''+gkkat+''' ');
		if Query6.RecordCount > 0 then begin
			ertekszin	:= clAqua;
			// Bev�tel
			if StringSzam(Query6.FieldByName('KI_EUTOL').AsString) > bevetel then begin
				ertekszin	:= clRed;       // A bev�teln�l a nagyobb a j�, a kisebb a rossz.
			end;
			if StringSzam(Query6.FieldByName('KI_EURIG').AsString) <> 0 then begin
				if bevetel > StringSzam(Query6.FieldByName('KI_EURIG').AsString) then begin
					ertekszin	:= clLime;
				end;
			end;
			QKI1.Color	:= ertekszin;
			QKI2.Color	:= ertekszin;

      if SGOsszes.Cells[20, rsz] <> '1' then begin  // ha �rv�nyes a fogyaszt�s adat
          ertekszin	:= clAqua;
          if StringSzam(Query6.FieldByName('KI_ATTOL').AsString) > StringSzam(SGOsszes.Cells[9, rsz]) then begin  // �TLAG
            ertekszin	:= clLime;
          end;
          if StringSzam(Query6.FieldByName('KI_ATLIG').AsString) <> 0 then begin
            if StringSzam(SGOsszes.Cells[9, rsz]) > StringSzam(Query6.FieldByName('KI_ATLIG').AsString) then begin
              ertekszin	:= clRed;
            end;
          end;
          QKI5.Color	:= ertekszin;

          ertekszin	:= clAqua;
          if StringSzam(Query6.FieldByName('KI_MTTOL').AsString) > ( StringSzam(SGOsszes.Cells[10, rsz])*100 / osszkm )  		// MEGTAKAR�T�S
          then begin
            ertekszin	:= clRed;
          end;
          if StringSzam(Query6.FieldByName('KI_MTAIG').AsString) <> 0 then begin
            if ( StringSzam(SGOsszes.Cells[10, rsz])*100 / osszkm ) > StringSzam(Query6.FieldByName('KI_MTAIG').AsString) then begin
              ertekszin	:= clLime;
            end;
          end;
          QKI6.Color	:= ertekszin;
          end;

			ertekszin	:= clAqua;
			// K�LTS�G
			if StringSzam(Query6.FieldByName('KI_KTTOL').AsString) > koltseg then begin
				ertekszin	:= clLime;
			end;
			if StringSzam(Query6.FieldByName('KI_KTGIG').AsString) <> 0 then begin
				if koltseg > StringSzam(Query6.FieldByName('KI_KTGIG').AsString) then begin
					ertekszin	:= clRed;
				end;
			end;
			QKI3.Color	:= ertekszin;
			QKI4.Color	:= ertekszin;

			ertekszin	:= clAqua;
			// NYERES�G
			if StringSzam(Query6.FieldByName('KI_NYTOL').AsString) > nyereseg then begin
				ertekszin	:= clRed;
			end;
			if StringSzam(Query6.FieldByName('KI_NYEIG').AsString) <> 0 then begin
				if nyereseg > StringSzam(Query6.FieldByName('KI_NYEIG').AsString) then begin
					ertekszin	:= clLime;
				end;
			end;
			QKI7.Color	:= ertekszin;
			QKI8.Color	:= ertekszin;



			ertekszin	:= clAqua;
			// Sof�r Ft/km 1
			if QSofor1.Caption <> '' then begin
				if StringSzam(Query6.FieldByName('KI_FTKM1').AsString) > ( StringSzam(SGOsszes.Cells[13, rsz]) / osszkm )
				then begin
					ertekszin	:= clRed;
				end;
				if StringSzam(Query6.FieldByName('KI_FTKM2').AsString) <> 0 then begin
					if ( StringSzam(SGOsszes.Cells[13, rsz]) / osszkm ) > StringSzam(Query6.FieldByName('KI_FTKM2').AsString) then begin
						ertekszin	:= clLime;
					end;
				end;
				QS1A.Color	:= ertekszin;
			end;

			ertekszin	:= clAqua;
			// Sof�r Ft/km 2
			if QSofor2.Caption <> '' then begin
				if StringSzam(Query6.FieldByName('KI_FTKM1').AsString) > ( StringSzam(SGOsszes.Cells[14, rsz]) / osszkm )
				then begin
					ertekszin	:= clRed;
				end;
				if StringSzam(Query6.FieldByName('KI_FTKM2').AsString) <> 0 then begin
					if ( StringSzam(SGOsszes.Cells[14, rsz]) / osszkm ) > StringSzam(Query6.FieldByName('KI_FTKM2').AsString) then begin
						ertekszin	:= clLime;
					end;
				end;
				QS2A.Color	:= ertekszin;
			end;
		end;
	end;
end;

function TOsszlisDlg.ktsg_nemkell(nev: string): boolean;
begin
  Result:=False;
  Q_Szotar.Close;
  Q_Szotar.Parameters[0].Value:=nev;
  Q_Szotar.Open;
  Q_Szotar.First;
  if not Q_Szotar.Eof then
     Result:=True;
  Q_Szotar.Close;   
end;


function	TOsszlisDlg.Jarat_Egyeb_ktg(jarat : string ): TIntegerValid;
var
	ertek    	: double;
	sqlstr		: string;
	sof1		: string;
	szossz		: double;
	jojarat		: string;
	feldij		: double;
	felra1		: double;

//	fogyi		: string;
	voltsofor	: string;
	i			: integer;
	uzkolt		: double;
	tipus		: integer;
	uzmegtak	: double;
   jaratsz     : string;
  ertekek			: array [1..5] of double;
	loc_okolts		: double;
	// ofuvar		: double;
	// oertek		: double;
  ossz,ossz2, TenyFogyasztas: double;
begin
  Result.IsValid:=True;  // am�g nem �rv�nytelen�tj�k
	selkm1		:= 0;
	selkm2		:= 0;
	sedij1		:= 0;
	sedij2		:= 0;
	// oertek		:= 0;  ez t�ves 2016-02-15 NagyP
	loc_okolts		:= 0;
	// ofuvar		:= 0;  ez t�ves 2016-02-15 NagyP
  ossz      := 0;
  ossz2     := 0;
	vanadat 	:= false;
	feldij		:= StringSzam(EgyebDlg.Read_SZGrid('CEG', '301'));
	minimumkt	:= StringSzam(EgyebDlg.Read_SZGrid('999', '742'));
	maximumkt	:= StringSzam(EgyebDlg.Read_SZGrid('999', '743'));
	for i := 1 to 5 do begin
		ertekek[i]	:= 0;
	end;
	{El�sz�r lesz�rj�k a j�ratokat a megadott felt�telek alapj�n}
	sqlstr	:= 'SELECT * FROM JARAT WHERE JA_KOD = '''+jarat+''' ';
	if Query_Run (Query1_,sqlstr ,true) then begin
		while ( ( not Query1_.EOF ) and (not EgyebDlg.kilepes) ) do begin
			jaratsz			:= Query1_.FieldByName('JA_KOD').AsString;
			jojarat	:= GetJaratszamFromDb(Query1_);
			vanadat 	:= true;
			// A j�rathoz tartoz� fogyaszt�si adatok kisz�m�t�sa

			FogyasztasDlg.ClearAllList;
			FogyasztasDlg.JaratBetoltes(jarat);
			FogyasztasDlg.CreateTankLista;
			FogyasztasDlg.CreateAdBlueLista;
			FogyasztasDlg.Szakaszolo;

			{A dolgoz�i adatok bet�tele}
			voltsofor	:= '';
			Query_Run(Query5, 'SELECT * FROM JARSOFOR WHERE JS_JAKOD = '''+jaratsz+''' ORDER BY JS_SORSZ ');
			if Query5.RecordCount > 0 then begin
				while not Query5.Eof do begin
					sof1		:= Query_Select('DOLGOZO', 'DO_KOD', Query5.FieldByname('JS_SOFOR').AsString , 'DO_NAME');
					if sof1 <> '' then begin
						// Betessz�k a sof�r fogyaszt�si adatokat k�l�n sorba
						uzkolt 	:= 0;
						if Pos (Query5.FieldByname('JS_SOFOR').AsString, voltsofor) < 1 then begin
							voltsofor	:= voltsofor + Query5.FieldByname('JS_SOFOR').AsString + ';';
							if  Round(Query1_.FieldByname('JA_OSSZKM').AsFloat ) > 0  then begin
								uzmegtak                        := FogyasztasDlg.GetSoforMegtakaritas(Query5.FieldByname('JS_SOFOR').AsString);
								// Az �zemanyag megtakar�t�s k�lts�g�nek kisz�m�t�sa �s megjelen�t�se
								if uzmegtak <> 0 then begin
									uzkolt	 				   	:= -1 * uzmegtak * GetApehUzemanyag(Query1_.FieldByName('JA_JKEZD').AsString, 1);
								end;
								ossz  		:= ossz + uzkolt;
								ossz2  		:= ossz2+ uzkolt;
							end;
						end;
						selkm1	:= StringSzam(Query5.FieldByname('JS_SZAKM').AsString);
						sedij1	:= StringSzam(Query5.FieldByname('JS_JADIJ').AsString);
						sextra1	:= StringSzam(Query5.FieldByname('JS_EXDIJ').AsString);
						felra1  := StringSzam(Query5.FieldByname('JS_FELRA').AsString);
						ossz  								:= ossz - selkm1 * sedij1 - sextra1 - felra1*feldij;
						ossz2  								:= ossz2- selkm1 * sedij1 - sextra1 - felra1*feldij;
					end;
					Query5.Next;
				end;
			end;


		 {Bet�ltj�k a k�lts�geket a Query1_-be}
		 if Query_Run (Query3,'SELECT * FROM KOLTSEG WHERE KS_JAKOD = '''+jaratsz+''' '+
			' AND KS_TETAN = 0 '+
			' ORDER BY KS_RENDSZ DESC, KS_DATUM' ,true) then begin
			while not Query3.EOF do begin
				tipus 	:= StrToIntdef(Query3.FieldByName('KS_TIPUS').AsString,0);
				{Nett� �rt�ket sz�molunk}
  			ertek	:= -1 * Query3.FieldByname('KS_OSSZEG').AsFloat * Query3.FieldByname('KS_ARFOLY').AsFloat + Query3.FieldByname('KS_AFAERT').AsFloat;
				if ( ( tipus <> 0 ) and ( tipus <> 3 ) and ( tipus <> 2 ) ) then begin		// Az �zemanyag k�lts�get + Ad blue -t + h�t� tankol�st nem vonjuk le, mert azt ut�na sz�moljuk
					ossz	:= ossz + ertek;
 					ossz2	:= ossz2 + ertek;
				end;
				Query3.Next;
			end;
		 end;

		{A g�pkocsi �zemanyag k�lts�g�nek berak�sa}
		// Ha nincs ilyen adat, ezt ki kellene hagyni!!!
		ossz  								:= ossz - FogyasztasDlg.GetKoltseg;
		ossz2  								:= ossz2- FogyasztasDlg.GetKoltseg;

		{A g�pkocsi Ad-blue k�lts�g�nek berak�sa -> Ha nincs ilyen adat, ezt ki kell hagyni!!!}
		Query_Run (Query5, 'SELECT * FROM KOLTSEG WHERE KS_RENDSZ = '''+Query1_.FieldByname('JA_RENDSZ').AsString+''' AND KS_TIPUS = 2 ' );
		if Query5.RecordCount > 0 then begin
			ossz  								:= ossz - FogyasztasDlg.GetBlueKoltseg;
			ossz2  								:= ossz2- FogyasztasDlg.GetBlueKoltseg;
		end;

		// A h�t� tankol�sok berak�sa
		Query_Run (Query2,'SELECT * FROM JARPOTOS WHERE JP_JAKOD = '''+jaratsz+''' ',true);
		while not Query2.EOF do begin
			if HutoPotkocsi(Query2.FieldByname('JP_POREN').AsString) then begin
				szossz	:= GetHutoOsszeg(Query2.FieldByname('JP_POREN').AsString, Query2.FieldByname('JP_UZEM1').AsString, Query2.FieldByname('JP_UZEM2').AsString);
				ossz  								:= ossz - szossz;
				ossz2  								:= ossz2- szossz;
			end;
			Query2.Next;
		end;
		Query2.Close;

		 {K�lts�g �sszesen bet�lt�se}
//     if OssznyilDlg.CheckBox2.Checked then
//        ossz:=ossz2;
		 loc_okolts	:= loc_okolts + ossz;

		 // ofuvar	:= ofuvar + ertek;

     // ??? Az alj�ratok fogyaszt�s�t nem kell hozz�adni a tot�l sorhoz!
     TenyFogyasztas:= FogyasztasDlg.GetTenyFogyasztas;
     if not(FogyasztasDlg.ErvenyesTenyFogyasztas) then begin // �rv�nytelen �rt�k, pl. -1 = nincs minden szakaszhoz z�r� telitank
          ervenytelen_fogyasztas_grandtotal:= true;
          Result.IsValid:=False;
          end;
			ertekek[1]		:= ertekek[1] + FogyasztasDlg.GetOsszKm;
			ertekek[2]		:= ertekek[2] + TenyFogyasztas;
			ertekek[3]		:= ertekek[3] + FogyasztasDlg.GetTervFogyasztas;
			ertekek[4]		:= ertekek[4] + FogyasztasDlg.GetMegtakaritas;
			ertekek[5]		:= ertekek[5] + FogyasztasDlg.GetSulyAtlag * FogyasztasDlg.GetOsszKm ;

			// oertek			:= oertek + ertek + ossz;
			// Az EUR k�lts�g kisz�m�t�sa a j�rat indul� d�tum alapj�n
			//ossz			:= 0;
			Query1_.Next;
		end;
		Query1_.EnableControls;
	end;
  Result.Value:=Trunc(loc_okolts);
end;

function TOsszlisDlg.Jarat_Egyeb_km(jakod: string): E_res;
var
  mbdb, okm, oktg: integer;
  osszido, jaratido: double;
  ejakod,ejasza,ejaev, Date1, Date2, Time1, Time2: string;
  jaratok,fajta, S: string;
  res: E_res;
  KtgRecord: TIntegerValid;
begin
  //mbkod:=StrToIntDef( Query_Select('JARATMEGBIZAS','JM_JAKOD',jakod,'JM_MBKOD'),-1);
  res[1]:='0';
  res[2]:='0';
  res[3]:='';
  res[4]:='1'; // �rv�nyes fogyaszt�si adatok
  res[5]:='0';

  Result:=res;
  S:='SELECT DISTINCT ms_jakod,ms_jasza,ms_fajta, substring(MS_DATUM,1,4) ev FROM MEGSEGED, MEGBIZAS where ms_mbkod=mb_mbkod and mb_jakod='+jakod +' and ms_fajko<>0';
  S:=S+' union ';
  S:=S+' select DISTINCT ja_kod, JA_ORSZ+''-''+convert(varchar,convert(int, ja_alkod)), ''alj�rat'', substring(JA_JKEZD,1,4) from JARAT WHERE JA_FOJAR='+jakod;

//	if Query_Run(QueryAlap, 'SELECT DISTINCT ms_jakod,ms_jasza,ms_fajta FROM MEGSEGED where ms_mbkod='+IntToStr(mbkod)+' and ms_fajko<>0') then
	if Query_Run(QueryAlap, S) then
  begin
    okm:=0;
    oktg:=0;
    osszido:=0;
    jaratok:='';
    while not QueryAlap.EOF do    // MEGSEGED
    begin
      fajta:=QueryAlap.Fieldbyname('MS_FAJTA').AsString ;
      ejakod:=QueryAlap.Fieldbyname('MS_JAKOD').AsString ;
      ejasza:=QueryAlap.Fieldbyname('MS_JASZA').AsString ;
      ejaev:=QueryAlap.Fieldbyname('ev').AsString ;
      if ejakod<>'' then begin
        if fajta= 'alj�rat' then begin
          mbdb:= 1;
          end
        else begin
          Query_Run(Queryal2, 'select DISTINCT mb_jasza from MEGSEGED, megbizas where ms_mbkod=mb_mbkod and ms_jasza='''+ejasza+''' '+
                ' and substring(MS_DATUM,1,4)='''+ejaev+'''');
          mbdb:= Queryal2.RecordCount;
          end;
      	Query_Run(Queryal2, 'SELECT JA_OSSZKM, JA_JKEZD, JA_KEIDO, JA_JVEGE, JA_VEIDO FROM JARAT where ja_kod='''+ejakod+'''');
        Date1:= Queryal2.FieldByName('JA_JKEZD').AsString;
        Time1:= Queryal2.FieldByName('JA_KEIDO').AsString+':00';  // nincs benne m�sodperc
        Date2:= Queryal2.FieldByName('JA_JVEGE').AsString;
        Time2:= Queryal2.FieldByName('JA_VEIDO').AsString+':00';  // nincs benne m�sodperc
        jaratido:= DateTimeDifferenceSec(Date2, Time2, Date1, Time1)/(3600*24);  // el�l a k�s�bbi d�tum! tizedes t�rttel
        KtgRecord:= Jarat_Egyeb_ktg(ejakod);
        if mbdb>0 then begin
          okm:= okm+ Trunc(QueryAl2.Fieldbyname('JA_OSSZKM').AsInteger / mbdb);
          oktg:= oktg+ Trunc(KtgRecord.Value / mbdb);
          osszido:= osszido+ jaratido / mbdb;
          if not(KtgRecord.IsValid) then
              res[4]:='0'; // ha egy is invalid, akkor az eg�sz eredm�ny az
          jaratok:=jaratok+ejasza+IfThen(mbdb>1,'/'+IntToStr(mbdb))+' '+fajta+',' ;
          end
        else begin
          okm:= okm+ Trunc(QueryAl2.Fieldbyname('JA_OSSZKM').AsInteger);
          oktg:= oktg+ Trunc(KtgRecord.Value);
          osszido:= osszido+ jaratido;
          if not(KtgRecord.IsValid) then
              res[4]:='0';  // ha egy is invalid, akkor az eg�sz eredm�ny az
          jaratok:=jaratok+ejasza+' '+fajta+',' ;
        end;
      end;
      QueryAlap.Next;
    end;
  end;
  res[1]:=IntToStr( okm);
  res[2]:=IntToStr( oktg);
  res[3]:=copy(jaratok,1,length(jaratok)-1);
  res[5]:=FormatFloat('0.00', osszido);
  Result:=res;

end;

end.


