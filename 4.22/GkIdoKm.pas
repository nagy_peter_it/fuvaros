unit GkIdoKm;

interface

uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls, 
  Forms, Dialogs, DBTables, DB, StdCtrls, Buttons, Mask, DBCtrls,
  ExtCtrls, ADODB;

type
  TGkIdoKmDlg = class(TForm)
    BitBtn4: TBitBtn;
    BitBtn6: TBitBtn;
    CheckBox1: TCheckBox;
    QueryA1: TADOQuery;
    QueryKoz: TADOQuery;
    ComboBox1: TComboBox;
    Label1: TLabel;
    procedure BitBtn4Click(Sender: TObject);
    procedure BitBtn6Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure CheckBox1Click(Sender: TObject);
  private
  public
  end;

var
  GkIdoKmDlg: TGkIdoKmDlg;

implementation

uses
	GkIdoKmList, Kozos, Kozos_Local, J_SQL,DateUtils, Egyeb;
{$R *.DFM}

procedure TGkIdoKmDlg.BitBtn4Click(Sender: TObject);
var
  bedat, akdat: TDate;
  honap: integer;
  sdat: string;
begin
	// A dolgoz�i kateg�ri�k �jrasz�mol�sa
	Screen.Cursor	:= crHourGlass;
	Query_Run(QueryA1, 'SELECT * FROM DOLGOZO ');
	while not QueryA1.Eof do begin
    sdat:=Trim(QueryA1.Fieldbyname('DO_BELEP').AsString);
    akdat:=date;
    honap:=0;
		if 	sdat <> '' then begin
      bedat:=StrToDate(QueryA1.Fieldbyname('DO_BELEP').AsString);
      honap:=MonthsBetween(bedat,akdat)
		end;
		Query_Run(QueryKoz, 'UPDATE DOLGOZO SET DO_MBKAT = '+IntToStr(honap)+' WHERE DO_KOD = '''+QueryA1.FieldByName('DO_KOD').AsString+''' ', FALSE );
		QueryA1.Next;
	end;
  QueryA1.Close;

	{A lista �ssze�ll�t�sa a sz�r�si elemek alapj�n}
	Application.CreateForm(TGkIdoKmlisDlg, GkIdoKmlisDlg);
 // GkIdoKmlisDlg.Caption:='Dolgoz�k ideje kateg�ri�nk�nt';
	GkIdoKmlisDlg.Tolt(CheckBox1.Checked, (CheckBox1.Checked and (ComboBox1.ItemIndex=0)));
	Screen.Cursor	:= crDefault;
	if GkIdoKmlisDlg.vanadat	then begin
		GkIdoKmlisDlg.Rep.Preview;
	end else begin
		NoticeKi('Nincs a megadott felt�teleknek megfelel� t�tel!');
	end;
	GkIdoKmlisDlg.Destroy;
end;

procedure TGkIdoKmDlg.BitBtn6Click(Sender: TObject);
begin
	Close;
end;

procedure TGkIdoKmDlg.FormCreate(Sender: TObject);
begin
{	QueryA1.Close;
	QueryA1.Tag	:= QUERY_ADAT_TAG;
	EgyebDlg.SeTADOQueryDatabase(QueryA1);
 }
end;

procedure TGkIdoKmDlg.CheckBox1Click(Sender: TObject);
begin
  Label1.Visible:=CheckBox1.Checked;
  ComboBox1.Visible:=CheckBox1.Checked;
end;

end.


