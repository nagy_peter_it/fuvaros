object FizlisDlg: TFizlisDlg
  Left = 371
  Top = 201
  Width = 1313
  Height = 577
  HorzScrollBar.Range = 1200
  VertScrollBar.Range = 2000
  Caption = 'FizlisDlg'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clBlack
  Font.Height = -13
  Font.Name = 'Courier New'
  Font.Style = []
  OldCreateOrder = True
  Scaled = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 16
  object Rep: TQuickRep
    Left = 8
    Top = 0
    Width = 1270
    Height = 1796
    BeforePrint = RepBeforePrint
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -17
    Font.Name = 'Arial'
    Font.Style = []
    Functions.Strings = (
      'PAGENUMBER'
      'COLUMNNUMBER'
      'REPORTTITLE')
    Functions.DATA = (
      '0'
      '0'
      #39#39)
    OnNeedData = RepNeedData
    Options = [FirstPageHeader, LastPageFooter]
    Page.Columns = 1
    Page.Orientation = poPortrait
    Page.PaperSize = A4
    Page.Continuous = False
    Page.Values = (
      100.000000000000000000
      2970.000000000000000000
      100.000000000000000000
      2100.000000000000000000
      100.000000000000000000
      100.000000000000000000
      0.000000000000000000)
    PrinterSettings.Copies = 1
    PrinterSettings.OutputBin = Auto
    PrinterSettings.Duplex = False
    PrinterSettings.FirstPage = 0
    PrinterSettings.LastPage = 0
    PrinterSettings.UseStandardprinter = False
    PrinterSettings.UseCustomBinCode = False
    PrinterSettings.CustomBinCode = 0
    PrinterSettings.ExtendedDuplex = 0
    PrinterSettings.UseCustomPaperCode = False
    PrinterSettings.CustomPaperCode = 0
    PrinterSettings.PrintMetaFile = False
    PrinterSettings.MemoryLimit = 1000000
    PrinterSettings.PrintQuality = 0
    PrinterSettings.Collate = 0
    PrinterSettings.ColorOption = 0
    PrintIfEmpty = True
    ReportTitle = 'Fizet'#233'si lista'
    ShowProgress = False
    SnapToGrid = True
    Units = MM
    Zoom = 160
    PrevFormStyle = fsNormal
    PreviewInitialState = wsNormal
    PrevInitialZoom = qrZoomToFit
    PreviewDefaultSaveType = stQRP
    PreviewLeft = 0
    PreviewTop = 0
    object QRBand3: TQRBand
      Left = 60
      Top = 60
      Width = 1149
      Height = 166
      AlignToBottom = False
      Color = clWhite
      TransparentBand = False
      ForceNewColumn = False
      ForceNewPage = False
      Size.Values = (
        274.505208333333300000
        1900.039062500000000000)
      PreCaluculateBandHeight = False
      KeepOnOnePage = False
      BandType = rbPageHeader
      object QRShape8: TQRShape
        Left = 1024
        Top = 128
        Width = 94
        Height = 38
        Size.Values = (
          62.177083333333330000
          1693.333333333333000000
          211.666666666666700000
          156.104166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Brush.Color = clSilver
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape7: TQRShape
        Left = 834
        Top = 128
        Width = 54
        Height = 38
        Size.Values = (
          62.537878787878790000
          1379.441287878788000000
          211.666666666666700000
          88.996212121212120000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Brush.Color = clSilver
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape6: TQRShape
        Left = 763
        Top = 128
        Width = 72
        Height = 38
        Size.Values = (
          62.537878787878790000
          1261.581439393939000000
          211.666666666666700000
          119.062500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Brush.Color = clSilver
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape5: TQRShape
        Left = 324
        Top = 128
        Width = 378
        Height = 38
        Size.Values = (
          62.537878787878790000
          536.382575757575800000
          211.666666666666700000
          625.378787878787900000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Brush.Color = clSilver
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape4: TQRShape
        Left = 135
        Top = 128
        Width = 190
        Height = 38
        Size.Values = (
          62.537878787878790000
          223.693181818181800000
          211.666666666666700000
          313.892045454545500000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Brush.Color = clSilver
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape3: TQRShape
        Left = 47
        Top = 128
        Width = 89
        Height = 38
        Size.Values = (
          62.537878787878790000
          78.172348484848480000
          211.666666666666700000
          146.723484848484800000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Brush.Color = clSilver
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape1: TQRShape
        Left = 4
        Top = 128
        Width = 44
        Height = 38
        Size.Values = (
          62.177083333333330000
          6.614583333333333000
          211.666666666666700000
          72.760416666666670000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Brush.Color = clSilver
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRLSzamla: TQRLabel
        Left = 4
        Top = 9
        Width = 1126
        Height = 43
        Size.Values = (
          70.555555555555560000
          7.055555555555555000
          14.111111111111110000
          1862.666666666667000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = 'Havi lista'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -23
        Font.Name = 'Arial'
        Font.Style = [fsBold, fsItalic]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 17
      end
      object QRLabel15: TQRLabel
        Left = 7
        Top = 134
        Width = 31
        Height = 26
        Size.Values = (
          42.994791666666670000
          11.575520833333330000
          221.588541666666700000
          51.263020833333330000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = 'Ssz.'
        Color = clSilver
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 8
      end
      object QRLabel16: TQRLabel
        Left = 333
        Top = 69
        Width = 384
        Height = 23
        Size.Values = (
          38.805555555555560000
          550.333333333333300000
          114.652777777777800000
          635.000000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'QRLabel16'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 8
      end
      object QRLabel17: TQRLabel
        Left = 333
        Top = 98
        Width = 384
        Height = 23
        Size.Values = (
          38.805555555555560000
          550.333333333333300000
          162.277777777777800000
          635.000000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'QRLabel17'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 8
      end
      object QRLabel6: TQRLabel
        Left = 979
        Top = 9
        Width = 64
        Height = 26
        Size.Values = (
          42.333333333333330000
          1619.250000000000000000
          14.111111111111110000
          105.833333333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'D'#225'tum :'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 9
      end
      object QRSysData1: TQRSysData
        Left = 1052
        Top = 9
        Width = 91
        Height = 26
        Size.Values = (
          42.333333333333330000
          1739.194444444444000000
          14.111111111111110000
          149.930555555555500000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Color = clWhite
        Data = qrsDate
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Text = ''
        Transparent = True
        ExportAs = exptText
        FontSize = 9
      end
      object QRLabel7: TQRLabel
        Left = 979
        Top = 35
        Width = 64
        Height = 26
        Size.Values = (
          42.333333333333330000
          1619.250000000000000000
          58.208333333333320000
          105.833333333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Lap   :'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 9
      end
      object QRSysData4: TQRSysData
        Left = 1052
        Top = 35
        Width = 91
        Height = 26
        Size.Values = (
          42.333333333333330000
          1739.194444444444000000
          58.208333333333320000
          149.930555555555500000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Color = clWhite
        Data = qrsPageNumber
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Text = ''
        Transparent = True
        ExportAs = exptText
        FontSize = 9
      end
      object QRLabel8: TQRLabel
        Left = 256
        Top = 69
        Width = 64
        Height = 23
        Size.Values = (
          38.805555555555560000
          423.333333333333300000
          114.652777777777800000
          105.833333333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Sof'#337'r : '
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 8
      end
      object QRLabel3: TQRLabel
        Left = 256
        Top = 98
        Width = 64
        Height = 23
        Size.Values = (
          38.805555555555560000
          423.333333333333300000
          162.277777777777800000
          105.833333333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Id'#337'szak :'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 8
      end
      object QRLabel4: TQRLabel
        Left = 51
        Top = 135
        Width = 83
        Height = 25
        Size.Values = (
          41.892361111111120000
          83.784722222222230000
          222.690972222222300000
          136.701388888888900000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = 'Kelt'
        Color = clSilver
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 8
      end
      object QRLabel9: TQRLabel
        Left = 226
        Top = 134
        Width = 94
        Height = 26
        Size.Values = (
          42.333333333333340000
          374.385416666666700000
          222.250000000000000000
          154.781250000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = 'Rendsz.'
        Color = clSilver
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 8
      end
      object QRLabel12: TQRLabel
        Left = 330
        Top = 134
        Width = 272
        Height = 26
        Size.Values = (
          42.333333333333340000
          546.364583333333400000
          222.250000000000000000
          449.791666666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Viszonylat'
        Color = clSilver
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 8
      end
      object QRLabel22: TQRLabel
        Left = 835
        Top = 135
        Width = 51
        Height = 25
        Size.Values = (
          41.341145833333330000
          1380.794270833333000000
          223.242187500000000000
          84.335937500000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Ft/km'
        Color = clSilver
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 8
      end
      object QRLabel24: TQRLabel
        Left = 1022
        Top = 135
        Width = 96
        Height = 22
        Size.Values = (
          35.718750000000000000
          1689.364583333333000000
          223.572916666666700000
          158.750000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = #214'sszeg (Ft)'
        Color = clSilver
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 8
      end
      object QRLabel10: TQRLabel
        Left = 142
        Top = 134
        Width = 81
        Height = 26
        Size.Values = (
          42.333333333333340000
          234.156250000000000000
          222.250000000000000000
          133.614583333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'J'#225'ratsz'#225'm'
        Color = clSilver
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 8
      end
      object QRLabel11: TQRLabel
        Left = 772
        Top = 135
        Width = 53
        Height = 25
        Size.Values = (
          41.341145833333330000
          1276.614583333333000000
          223.242187500000000000
          87.643229166666670000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Elsz.km'
        Color = clSilver
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 8
      end
      object QRShape17: TQRShape
        Left = 954
        Top = 128
        Width = 71
        Height = 38
        Size.Values = (
          62.177083333333330000
          1576.916666666667000000
          211.666666666666700000
          117.739583333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Brush.Color = clSilver
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRLabel1: TQRLabel
        Left = 959
        Top = 135
        Width = 64
        Height = 25
        Size.Values = (
          41.341145833333330000
          1585.846354166667000000
          223.242187500000000000
          105.833333333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = 'Extra d.'
        Color = clSilver
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 8
      end
      object QRShape21: TQRShape
        Left = 887
        Top = 128
        Width = 67
        Height = 38
        Margins.Left = 1
        Size.Values = (
          62.177083333333330000
          1467.114583333333000000
          211.666666666666700000
          111.125000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Brush.Color = clSilver
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRLabel13: TQRLabel
        Left = 887
        Top = 135
        Width = 66
        Height = 25
        Margins.Left = 1
        Margins.Top = 1
        Margins.Right = 1
        Margins.Bottom = 1
        Size.Values = (
          41.010416666666670000
          1467.114583333333000000
          223.572916666666700000
          108.479166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Felrak'#243
        Color = clSilver
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 8
      end
      object QRShape22: TQRShape
        Left = 702
        Top = 128
        Width = 62
        Height = 38
        Size.Values = (
          62.177083333333330000
          1160.197916666667000000
          211.666666666666700000
          103.187500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Brush.Color = clSilver
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRLabel2: TQRLabel
        Left = 704
        Top = 134
        Width = 57
        Height = 26
        Size.Values = (
          42.994791666666670000
          1164.166666666667000000
          221.588541666666700000
          94.257812500000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = 'Cseh tr'
        Color = clSilver
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 8
      end
    end
    object QRBand1: TQRBand
      Left = 60
      Top = 226
      Width = 1149
      Height = 26
      AlignToBottom = False
      BeforePrint = QRBand1BeforePrint
      Color = clWhite
      TransparentBand = False
      ForceNewColumn = False
      ForceNewPage = False
      Size.Values = (
        42.994791666666670000
        1900.039062500000000000)
      PreCaluculateBandHeight = False
      KeepOnOnePage = False
      BandType = rbDetail
      object QRShape27: TQRShape
        Left = 701
        Top = 0
        Width = 74
        Height = 26
        Size.Values = (
          43.656250000000000000
          1158.875000000000000000
          0.000000000000000000
          121.708333333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape10: TQRShape
        Left = 136
        Top = 0
        Width = 190
        Height = 26
        Size.Values = (
          42.333333333333330000
          224.895833333333300000
          0.000000000000000000
          313.531250000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape14: TQRShape
        Left = 1023
        Top = 0
        Width = 95
        Height = 26
        Size.Values = (
          43.656250000000000000
          1692.010416666667000000
          0.000000000000000000
          157.427083333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape13: TQRShape
        Left = 835
        Top = 0
        Width = 54
        Height = 26
        Size.Values = (
          43.656250000000000000
          1381.125000000000000000
          0.000000000000000000
          88.635416666666670000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape12: TQRShape
        Left = 764
        Top = 0
        Width = 71
        Height = 26
        Size.Values = (
          43.656250000000000000
          1263.385416666667000000
          0.000000000000000000
          117.739583333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape11: TQRShape
        Left = 325
        Top = 0
        Width = 377
        Height = 26
        Size.Values = (
          42.333333333333330000
          537.104166666666700000
          0.000000000000000000
          623.093750000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape9: TQRShape
        Left = 48
        Top = 0
        Width = 89
        Height = 26
        Size.Values = (
          42.333333333333330000
          79.375000000000000000
          0.000000000000000000
          146.843750000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape2: TQRShape
        Left = 5
        Top = 0
        Width = 44
        Height = 26
        Size.Values = (
          42.333333333333330000
          7.937500000000000000
          0.000000000000000000
          72.760416666666670000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object SL8: TQRLabel
        Left = 1037
        Top = 3
        Width = 76
        Height = 20
        Size.Values = (
          33.072916666666670000
          1714.830729166667000000
          4.960937500000000000
          125.677083333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = '5480044'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object SL6: TQRLabel
        Left = 767
        Top = 3
        Width = 63
        Height = 20
        Size.Values = (
          33.072916666666670000
          1268.346354166667000000
          4.960937500000000000
          104.179687500000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = '999999'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object SL1: TQRLabel
        Left = 8
        Top = 3
        Width = 34
        Height = 20
        Size.Values = (
          33.072916666666670000
          13.229166666666670000
          4.960937500000000000
          56.223958333333330000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = '1234'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object SL2: TQRLabel
        Left = 51
        Top = 3
        Width = 83
        Height = 20
        Size.Values = (
          33.072916666666670000
          83.784722222222230000
          4.409722222222222000
          136.701388888888900000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = '2222.22.22'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object SL3: TQRLabel
        Left = 140
        Top = 3
        Width = 85
        Height = 20
        Size.Values = (
          33.072916666666670000
          231.510416666666700000
          4.409722222222222000
          141.111111111111100000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = '123-12345'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object SL4: TQRLabel
        Left = 227
        Top = 3
        Width = 93
        Height = 20
        Size.Values = (
          33.072916666666670000
          374.826388888888800000
          4.409722222222222000
          154.340277777777800000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = '1234'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object SL5: TQRLabel
        Left = 330
        Top = 2
        Width = 364
        Height = 20
        Size.Values = (
          33.072916666666670000
          545.041666666666800000
          3.968750000000000000
          601.927083333333400000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = '1234'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object QRShape18: TQRShape
        Left = 954
        Top = 0
        Width = 70
        Height = 26
        Size.Values = (
          43.656250000000000000
          1576.916666666667000000
          0.000000000000000000
          116.416666666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object SL7A: TQRLabel
        Left = 960
        Top = 3
        Width = 60
        Height = 20
        Size.Values = (
          33.072916666666670000
          1587.500000000000000000
          4.960937500000000000
          99.218750000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = '574000'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object QRShape26: TQRShape
        Left = 886
        Top = 0
        Width = 69
        Height = 26
        Size.Values = (
          43.656250000000000000
          1465.791666666667000000
          0.000000000000000000
          113.770833333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object SL7b: TQRLabel
        Left = 888
        Top = 3
        Width = 61
        Height = 20
        Size.Values = (
          33.072916666666670000
          1468.437500000000000000
          4.960937500000000000
          100.872395833333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = '50000'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object SL5A: TQRLabel
        Left = 706
        Top = 3
        Width = 57
        Height = 20
        Size.Values = (
          33.072916666666670000
          1166.812500000000000000
          5.291666666666667000
          93.927083333333330000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = '999'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object SL7: TQRLabel
        Left = 836
        Top = 3
        Width = 46
        Height = 20
        Size.Values = (
          33.072916666666670000
          1382.447916666667000000
          5.291666666666667000
          75.406250000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = '556,2'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
    end
    object QRBand2: TQRBand
      Left = 60
      Top = 252
      Width = 1149
      Height = 107
      AlignToBottom = False
      BeforePrint = QRBand2BeforePrint
      Color = clWhite
      TransparentBand = False
      ForceNewColumn = False
      ForceNewPage = False
      Size.Values = (
        176.940104166666700000
        1900.039062500000000000)
      PreCaluculateBandHeight = False
      KeepOnOnePage = False
      BandType = rbSummary
      object QRShape23: TQRShape
        Left = 701
        Top = -1
        Width = 64
        Height = 29
        Size.Values = (
          47.625000000000000000
          1158.875000000000000000
          -1.322916666666667000
          105.833333333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Brush.Color = clSilver
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape25: TQRShape
        Left = 886
        Top = 0
        Width = 69
        Height = 28
        Size.Values = (
          46.302083333333330000
          1465.791666666667000000
          0.000000000000000000
          113.770833333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Brush.Color = clSilver
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape19: TQRShape
        Left = 954
        Top = 0
        Width = 70
        Height = 28
        Size.Values = (
          46.302083333333330000
          1577.578125000000000000
          0.000000000000000000
          115.755208333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Brush.Color = clSilver
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape16: TQRShape
        Left = 764
        Top = 0
        Width = 73
        Height = 28
        Size.Values = (
          46.302083333333330000
          1263.385416666667000000
          0.000000000000000000
          120.385416666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Brush.Color = clSilver
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRLabel21: TQRLabel
        Left = 595
        Top = 2
        Width = 98
        Height = 22
        Size.Values = (
          37.041666666666670000
          984.249999999999900000
          3.968750000000000000
          162.718750000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = #214'sszesen :'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 8
      end
      object QRShape15: TQRShape
        Left = 1023
        Top = 0
        Width = 95
        Height = 28
        Size.Values = (
          46.302083333333330000
          1692.010416666667000000
          0.000000000000000000
          157.427083333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Brush.Color = clSilver
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QL3: TQRLabel
        Left = 1027
        Top = 4
        Width = 89
        Height = 20
        Size.Values = (
          33.072916666666670000
          1698.625000000000000000
          6.614583333333333000
          146.843750000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = '123456'
        Color = clSilver
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object QL4: TQRLabel
        Left = 767
        Top = 4
        Width = 66
        Height = 20
        Size.Values = (
          33.072916666666670000
          1268.346354166667000000
          6.614583333333333000
          109.140625000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = '999999'
        Color = clSilver
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object QL2A: TQRLabel
        Left = 962
        Top = 4
        Width = 58
        Height = 20
        Size.Values = (
          33.072916666666670000
          1590.145833333333000000
          6.614583333333333000
          95.250000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = '123456'
        Color = clSilver
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object QRShape20: TQRShape
        Left = 836
        Top = 0
        Width = 51
        Height = 28
        Size.Values = (
          46.302083333333330000
          1382.447916666667000000
          0.000000000000000000
          84.666666666666670000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Brush.Color = clSilver
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QL5: TQRLabel
        Left = 838
        Top = 4
        Width = 46
        Height = 20
        Size.Values = (
          33.072916666666670000
          1386.416666666667000000
          6.614583333333333000
          75.406250000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = '475,4'
        Color = clSilver
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object QL2B: TQRLabel
        Left = 889
        Top = 4
        Width = 61
        Height = 20
        Size.Values = (
          33.072916666666670000
          1469.760416666667000000
          6.614583333333333000
          100.541666666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = '123456'
        Color = clSilver
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object QL5A: TQRLabel
        Left = 706
        Top = 4
        Width = 57
        Height = 20
        Size.Values = (
          33.072916666666670000
          1167.473958333333000000
          6.614583333333333000
          94.257812500000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = '999'
        Color = clSilver
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
    end
  end
  object SG1: TStringGrid
    Left = 360
    Top = 344
    Width = 593
    Height = 129
    ColCount = 11
    FixedCols = 0
    FixedRows = 0
    TabOrder = 0
  end
  object Query1: TADOQuery
    Tag = 1
    Parameters = <>
    Left = 26
    Top = 40
  end
  object Query2: TADOQuery
    Tag = 1
    Parameters = <>
    Left = 58
    Top = 40
  end
  object Query3: TADOQuery
    Tag = 1
    Parameters = <>
    Left = 90
    Top = 40
  end
  object Query4: TADOQuery
    Parameters = <>
    Left = 122
    Top = 40
  end
  object Query5: TADOQuery
    Tag = 1
    Parameters = <>
    Left = 154
    Top = 40
  end
  object QJarat: TADOQuery
    Tag = 1
    Parameters = <>
    Left = 210
    Top = 40
  end
  object QViszony: TADOQuery
    Tag = 1
    Parameters = <>
    Left = 58
    Top = 88
  end
end
