unit UniLista;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls;

type
  TUniListaDlg = class(TForm)
    Panel1: TPanel;
    ListBox1: TListBox;
    Panel2: TPanel;
    Button2: TButton;
    DisplayLabel: TLabel;
    procedure Button1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ListBox1DblClick(Sender: TObject);
  private
    procedure Valaszt;
  public
    ret_string: string;
  end;

var
  UniListaDlg: TUniListaDlg;

implementation

{$R *.dfm}

procedure TUniListaDlg.Valaszt;
begin
  if ListBox1.ItemIndex <> -1 then begin
      ret_string:= ListBox1.Items[ListBox1.ItemIndex];
      ModalResult:= mrOK;
      end
  else begin
      ret_string:= '';
      end;
end;

procedure TUniListaDlg.Button1Click(Sender: TObject);
begin
  Valaszt;
end;

procedure TUniListaDlg.FormCreate(Sender: TObject);
begin
  ret_string:= '';
end;

procedure TUniListaDlg.ListBox1DblClick(Sender: TObject);
begin
  Valaszt;
end;

end.
