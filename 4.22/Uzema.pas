unit Uzema;

interface

uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls, Forms, Dialogs,
  DBTables, DB, StdCtrls, Buttons, Mask, DBCtrls, ExtCtrls, J_SQL,
  ADODB;

type
  TUzemaDlg = class(TForm)
    BitBtn4: TBitBtn;
    BitBtn6: TBitBtn;
    Label3: TLabel;
    M4: TMaskEdit;
    BitBtn1: TBitBtn;
    Label1: TLabel;
    ListKezd: TComboBox;
    ListVege: TComboBox;
    Label2: TLabel;
    CheckBox1: TCheckBox;
    Query1: TADOQuery;
    QueryAl2: TADOQuery;
    RB2: TLabel;
    M1: TMaskEdit;
    BitBtn5: TBitBtn;
    RB1: TRadioButton;
    RadioButton2: TRadioButton;
    M0: TMaskEdit;
    RadioButton1: TRadioButton;
    RadioButton3: TRadioButton;
    RadioButton4: TRadioButton;
    procedure FormCreate(Sender: TObject);
    procedure BitBtn4Click(Sender: TObject);
    procedure BitBtn6Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn5Click(Sender: TObject);
	 procedure RB1Click(Sender: TObject);
	 procedure Tolt(rsz : string);
	 procedure Adatbeolvaso;
  private
  public
		vanadat	: boolean;
  end;

var
  UzemaDlg: TUzemaDlg;

implementation

uses
	J_VALASZTO, Uzemalis, Egyeb, Kozos, Kozos_Local;
{$R *.DFM}

procedure TUzemaDlg.FormCreate(Sender: TObject);
begin
 	M4.Text := '';
  	EgyebDlg.SetADOQueryDatabase(Query1);
  	EgyebDlg.SetADOQueryDatabase(QueryAl2);
   RB1Click(Sender);
end;

procedure TUzemaDlg.BitBtn4Click(Sender: TObject);
var
  apnorm ,telep	: string;
begin
  	if ListKezd.Items[ListKezd.ItemIndex] >= ListVege.Items[ListVege.ItemIndex] then begin
		NoticeKi('A befejez� d�tumnak nagyobbnak kell lennie!');
     	Exit;
  	end;

  	apnorm	:= '0';
  	if CheckBox1.Checked then begin
  		apnorm	:= '1';
	end;
  if RadioButton1.Checked then telep:='0';
  if RadioButton3.Checked then telep:='1';
  if RadioButton4.Checked then telep:='2';

	Screen.Cursor	:= crHourGlass;
	Application.CreateForm(TUzemalisDlg, UzemalisDlg);
	UzemalisDlg.Tolt(M4.Text, ListKezd.Items[ListKezd.ItemIndex], ListVege.Items[ListVege.ItemIndex], apnorm,telep);
	if UzemalisDlg.vanadat	then begin
		Screen.Cursor	:= crDefault;
		EgyebDlg.kelllistagomb	:= false;
		EgyebDlg.ListaGomb	   	:= '';
		EgyebDlg.kelllistagomb2	:= false;
		EgyebDlg.ListaGomb2	   	:= '';
		EgyebDlg.kelllistagomb3	:= false;
		EgyebDlg.ListaGomb3	   	:= '';
		UzemalisDlg.RepUzem.Preview;
	end else begin
		Screen.Cursor	:= crDefault;
		NoticeKi('Nincs a megadott felt�teleknek megfelel� t�tel!');
  	end;
  	UzemalisDlg.Destroy;
end;

procedure TUzemaDlg.BitBtn6Click(Sender: TObject);
begin
	Close;
end;

procedure TUzemaDlg.BitBtn1Click(Sender: TObject);
begin
	M4.Text := '';
	GepkocsiValaszto(M4, GK_TRAKTOR);
	if M4.Text <> '' then begin
		Adatbeolvaso;
   end;
end;

procedure TUzemaDlg.BitBtn5Click(Sender: TObject);
begin
	DolgozoValaszto(M0, M1);
end;

procedure TUzemaDlg.RB1Click(Sender: TObject);
begin
//	if RB1.Checked then begin
	if True then begin
   	BitBtn1.Enabled	:= true;
   	BitBtn5.Enabled	:= false;
       M1.Text			:= '';
   end else begin
   	BitBtn5.Enabled	:= true;
   	BitBtn1.Enabled	:= false;
       M4.Text			:= '';
   end;
end;

procedure TUzemaDlg.Tolt(rsz : string);
begin
	M4.Text		:= rsz;
	BitBtn1.Enabled	:= false;
	Adatbeolvaso;
end;

procedure TUzemaDlg.Adatbeolvaso;
var
	listdarab	: integer;
	sqlstr		: string;
begin
	{A listaboxok felt�lt�se a teljes tankol�sokkal}
	vanadat	:= true;
	ListKezd.Clear;
	ListVege.Clear;

  // alktu�lis �v
	sqlstr	:= 'SELECT KS_DATUM, KS_KMORA FROM KOLTSEG '+
			' WHERE KS_RENDSZ = '''+M4.Text+''' AND KS_TELE > 0 '+
		 //	' AND KS_TETAN = 0 '+  NP 2015-12-29
			' AND KS_TIPUS = 0 ' +
			' ORDER BY KS_DATUM ';
	Query_Run(Query1, sqlstr, true);
	while not Query1.EOF DO BEGIN
		  ListKezd.Items.Add(Query1.FieldByname('KS_DATUM').AsString + '  ( '+ Query1.FieldByname('KS_KMORA').AsString + ' km )');
  		ListVege.Items.Add(Query1.FieldByname('KS_DATUM').AsString + '  ( '+ Query1.FieldByname('KS_KMORA').AsString + ' km )');
  		Query1.Next;
	end;
	Query1.Close;

	listdarab 	:= ListKezd.Items.Count;
	if listdarab > 0 then begin
		ListKezd.ItemIndex := listdarab - 1;
		ListVege.ItemIndex := listdarab - 1;
		if listdarab > 1 then begin
			ListKezd.ItemIndex := listdarab - 2;
		end;
    Application.ProcessMessages;
	end else begin
		NoticeKi('Nincs megfelel� adat!');
		vanadat	:= false;
	end;
end;

end.


