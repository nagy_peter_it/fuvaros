unit PalettaFm;

interface

uses
	SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
	StdCtrls, Forms, DBCtrls, DB, DBGrids, DBTables, Grids, ExtCtrls, Buttons,
	ShellApi, Dialogs, J_FOFORMUJ, ADODB, Mask, Menus, Variants;

type
  TPalettaFmDlg = class(TJ_FOFORMUJDLG)
    BitBtn9: TBitBtn;
    procedure FormCreate(Sender: TObject);

	 procedure DBGrid2DrawColumnCell(Sender: TObject; const Rect: TRect;
	   DataCol: Integer; Column: TColumn; State: TGridDrawState);override;
   procedure DBGrid2DblClick(Sender: TObject);
   procedure Adatlap(cim, kod : string);override;
 	 procedure ButtonModClick(Sender: TObject);override;
	 procedure ButtonTorClick(Sender: TObject); override;
   procedure BitBtn9Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  csakmegjegyzes: boolean;
  end;

var
  PalettaFmDlg: TPalettaFmDlg;

implementation

uses 	Egyeb, J_SQL, Kozos, Palettabe,Segedbe2;

{$R *.dfm}

procedure TPalettaFmDlg.Adatlap(cim, kod: string);
begin

  if (kod<>'')and(Query1.FieldByName('PA_MTIP').AsInteger=1) then   // Megb�z�s
  begin
    csakmegjegyzes:=True ;
    //NoticeKi('Ezt a t�telt csak a Megb�z�sban lehet m�dos�tani!');
    //exit;
  end;

  Application.CreateForm(TPalettabeDlg, AlForm);
	Inherited Adatlap(cim, kod );

	ModLocate (Query1, FOMEZO, Query1.FieldByName(FOMEZO).AsString );
end;

procedure TPalettaFmDlg.ButtonModClick(Sender: TObject);
begin
{  inherited;
	if not voltalt then begin
 		voltalt 	:= true;
 		Exit;
 	end;
  }
 // PalettabeDlg.csakmegjegyzes:=csakmegjegyzes;
	if ButtonMod.Enabled then begin
  		Adatlap(MODOSIT,Query1.FieldByName(FOMEZO).AsString);
  	end;
	ModLocate (Query1, FOMEZO, Query1.FieldByName(FOMEZO).AsString );
  csakmegjegyzes:=False ;

end;

procedure TPalettaFmDlg.ButtonTorClick(Sender: TObject);
var
  mindtorol: boolean;
  ID1,ID2: string;
begin
  ID1:=Query1.FieldByName('PA_ID' ).AsString ;
  ID2:=Query1.FieldByName('PA_ID2').AsString ;
  if  ID2 = '0' then  // NEM �tad�s-�tv�tel
    inherited
  else
  begin
    if NoticeKi('Val�ban t�r�lni akarja a kijel�lt t�telt?',1)= 0 then
    begin
      mindtorol:= NoticeKi('A mozg�st�tel p�rj�t is t�rli?',1)= 0 ;
  		Query_Run(Query2, 'DELETE FROM PALETTA WHERE PA_ID = '''+ID1+'''');
      if mindtorol then
    		Query_Run(Query2, 'DELETE FROM PALETTA WHERE PA_ID = '''+ID2+'''')
      else
    		Query_Run(Query2, 'UPDATE PALETTA SET PA_ID2=0 WHERE PA_ID = '''+ID2+'''');

    	ModLocate (Query1, 'PA_ID', Query1.FieldByName('PA_ID').AsString );
    end;
  end;
end;

procedure TPalettaFmDlg.DBGrid2DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin

  if Query1.FieldByName('PA_OK').AsInteger=0 then   // M�g nem �rv�nyes
    	DBGrid2.Canvas.Font.Color		:= clRed ;

	DBGrid2.DefaultDrawDataCell(Rect, Column.Field, State);

	inherited DBGrid2DrawColumnCell(Sender, Rect, DataCol, Column, State);
//  inherited;

end;

procedure TPalettaFmDlg.FormCreate(Sender: TObject);
begin
	Inherited FormCreate(Sender);

	AddSzuromezoRovid('PA_VENEV',  'PALETTA','1');
	AddSzuromezoRovid('PA_FAJTA',  'PALETTA');
	AddSzuromezoRovid('PA_TINEV',  'PALETTA');
	AddSzuromezoRovid('PA_SNEV1',  'PALETTA','1');
	AddSzuromezoRovid('PA_RENSZ',  'PALETTA','1');
	AddSzuromezoRovid('PA_POTSZ',  'PALETTA','1');
 	AddSzuromezoRovid('PA_IGAZOLT',  'PALETTA');
	AddSzuromezoRovid('PA_MNEM',  'PALETTA');
	AddSzuromezoRovid('PA_MTIP',  'PALETTA');
	AddSzuromezoRovid('PA_OK',  'PALETTA');
	AddSzuromezoRovid('PA_FELNEV',  'PALETTA','1');
	AddSzuromezoRovid('PA_LERNEV',  'PALETTA','1');
	AddSzuromezoRovid('PA_OK',  'PALETTA');
	AddSzuromezoRovid('PA_GKKAT',  'PALETTA');
	AddSzuromezoRovid('PA_POKAT',  'PALETTA');

	BitBtn9.Parent		:= PanelBottom;
  
//	alapstr		:= 'select *  from paletta p, pal_vevo_keszlet v,pal_sofor_keszlet s,pal_rendszam_keszlet r, pal_potkocsi_keszlet o '+
//  ' where v.pv_vekod=p.pa_vekod and s.ps_skod1=p.pa_skod1 and r.pr_rensz=p.pa_rensz and o.pp_potsz=p.pa_potsz ';

	alapstr		:= 'select *  from paletta p  '+
  'left join pal_felrako_keszlet  f on f.pf_felnev=p.pa_felnev '+
  'left join pal_lerako_keszlet   l on l.pl_lernev=p.pa_lernev '+
  'left join pal_vevo_keszlet  v    on v.pv_vekod =p.pa_vekod  '+
  'left join pal_sofor_keszlet s    on s.ps_skod1 =p.pa_skod1  '+
  'left join pal_rendszam_keszlet r on r.pr_rensz =p.pa_rensz  '+
  'left join pal_potkocsi_keszlet o on o.pp_potsz =p.pa_potsz  ';

	FelTolto('', ' ','', 'PA_ID', alapstr, 'paletta', QUERY_ADAT_TAG );
	kulcsmezo			:= 'PA_ID';
	width				  := DLG_WIDTH;
	height				:= DLG_HEIGHT;

end;

procedure TPalettaFmDlg.BitBtn9Click(Sender: TObject);
begin
 if  Query1.FieldByName('PA_MBKOD').AsString <> '' then
 begin
	Application.CreateForm(TSegedbe2Dlg,Segedbe2Dlg);
  Segedbe2Dlg.kellpoz:=False;
	Segedbe2Dlg.tolt('Felrak�s/lerak�s m�dos�t�sa',Query1.FieldByName('PA_MBKOD').AsString, Query1.FieldByName('PA_SORSZ').AsString, '0');
  Screen.Cursor:=crDefault;
  Application.ProcessMessages;

	Segedbe2Dlg.ShowModal;
	Segedbe2Dlg.Destroy;

	ModLocate (Query1, FOMEZO, Query1.FieldByName(FOMEZO).AsString );
 end;
end;

procedure TPalettaFmDlg.DBGrid2DblClick(Sender: TObject);
begin
  if Query1.FieldByName('PA_MBKOD').AsString <> '' then
    BitBtn9.OnClick(self)
  else
    inherited    ;

end;

end.
