unit Info;

interface

uses WinTypes, WinProcs, Classes, Graphics, Forms, Controls, Buttons,
	StdCtrls, ExtCtrls, Sysutils;

type
	TInfoDlg = class(TForm)
	Panel1: TPanel;
    Memo1: TMemo;
    GridPanel2: TGridPanel;
    BitBtn1: TBitBtn;
	procedure OKBtnClick(Sender: TObject);
 	procedure Tolt(fn, cimstr : string);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormCreate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
	private
   	modosult		: boolean;
  		fnev			: string;
	public
	end;

var
	InfoDlg: TInfoDlg;

implementation
uses
	Egyeb;

{$R *.DFM}

procedure TInfoDlg.OKBtnClick(Sender: TObject);
begin
	if (fnev<>'') and modosult then begin
   	// A sz�veg elment�se az �llom�nyba
		Memo1.Lines.SaveToFile(fnev);
    end;  // if
	Close;
end;

procedure TInfoDlg.Tolt(fn, cimstr : string);
begin
  Caption := cimstr;
  fnev 	  := fn;
  if ExtractFilePath(fnev) = '' then begin
  	fnev 	  := EgyebDlg.TempPath + fn;
  end;
  if not FileExists(fnev) then begin
     Memo1.Clear;
     Memo1.Alignment := taCenter;
     Memo1.Lines.Add('');
     Memo1.Lines.Add('');
     Memo1.Lines.Add(fnev+' �llom�ny nem l�tezik!!!');
  end else begin
		Memo1.Lines.LoadFromFile(fnev);
  end;
end;

procedure TInfoDlg.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
	if ssCtrl in Shift then begin
   	if Chr(Key) in ['M','m'] then begin
   	//if Key = Key_M then begin
      	modosult			:= true;
         Memo1.ReadOnly	:= false;
         Key				:= 0;
		end;
   end;
end;

procedure TInfoDlg.BitBtn1Click(Sender: TObject);
begin
	if modosult then begin
   	// A sz�veg elment�se az �llom�nyba
		Memo1.Lines.SaveToFile(fnev);
   end;
	Close;
end;

procedure TInfoDlg.FormCreate(Sender: TObject);
begin
	modosult	:= false;
end;

procedure TInfoDlg.FormResize(Sender: TObject);
begin
 // 	OkBtn.Left := ( Panel2.Width - OKBtn.Width ) div 2;
end;

procedure TInfoDlg.FormShow(Sender: TObject);
begin
    SetWindowPos(
    Self.Handle,
    HWND_TOPMOST,
    0,
    0,
    0,
    0,
    SWP_NOACTIVATE or SWP_NOMOVE or SWP_NOSIZE);

end;

end.
