unit JaratSegedDat;

interface

uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, DBTables, DB, StdCtrls, Buttons, Mask, DBCtrls, ExtCtrls,
  ADODB,DateUtils;

type
  TJaratSegedDatDlg = class(TForm)
    BitBtn6: TBitBtn;
    Label1: TLabel;
    MD1: TMaskEdit;
    Label2: TLabel;
    MD2: TMaskEdit;
    BitBtn2: TBitBtn;
    BitBtn3: TBitBtn;
    BitElkuld: TBitBtn;
    procedure FormCreate(Sender: TObject);
    procedure BitBtn6Click(Sender: TObject);
    procedure MD1Exit(Sender: TObject);
    procedure MD2Exit(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure BitElkuldClick(Sender: TObject);
  private
  public
  end;

var
  JaratSegedDatDlg: TJaratSegedDatDlg;

implementation

uses
	Egyeb, Kozos;
{$R *.DFM}

procedure TJaratSegedDatDlg.FormCreate(Sender: TObject);
begin
  MD2.Text		:= DatumHozNap(copy(EgyebDlg.MaiDatum,1,8)+'01.', -1, true);
  MD1.Text		:= copy(MD2.Text,1,8)+'01.';
end;

procedure TJaratSegedDatDlg.BitBtn6Click(Sender: TObject);
begin
   MD2.Text		:= '';
   MD1.Text		:= '';
	Close;
end;

procedure TJaratSegedDatDlg.MD1Exit(Sender: TObject);
begin
	if ( ( MD1.Text <> '' ) and ( not Jodatum2(MD1 ) ) ) then begin
		NoticeKi('Hib�s d�tum megad�sa!');
		MD1.Text := '';
		MD1.Setfocus;
	end;
  EgyebDlg.ElsoNapEllenor(MD1, MD2);
end;

procedure TJaratSegedDatDlg.MD2Exit(Sender: TObject);
begin
	if ( ( MD2.Text <> '' ) and ( not Jodatum2(MD2 ) ) ) then begin
		NoticeKi('Hib�s d�tum megad�sa!');
		MD2.Text := '';
		MD2.Setfocus;
	end;
end;

procedure TJaratSegedDatDlg.BitBtn2Click(Sender: TObject);
begin
	// EgyebDlg.Calendarbe(MD1);
  // EgyebDlg.ElsoNapEllenor(MD1, MD2);
  EgyebDlg.CalendarBe_idoszak(MD1, MD2);
end;

procedure TJaratSegedDatDlg.BitBtn3Click(Sender: TObject);
begin
	EgyebDlg.Calendarbe(MD2);
end;

procedure TJaratSegedDatDlg.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
	EgyebDlg.FormReturn(Key);
end;

procedure TJaratSegedDatDlg.BitElkuldClick(Sender: TObject);
begin
   Close;
end;

end.


