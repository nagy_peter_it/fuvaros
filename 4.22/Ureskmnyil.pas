unit Ureskmnyil;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Mask, Buttons, DB, ADODB,DateUtils, CheckLst;

type
  TUreskmnyilDlg = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    BitBtn4: TBitBtn;
    BitBtn6: TBitBtn;
    MD1: TMaskEdit;
    MD2: TMaskEdit;
    BitBtn2: TBitBtn;
    BitBtn3: TBitBtn;
    CheckBox1: TCheckBox;
    Query1: TADOQuery;
    Label5: TLabel;
    Query2: TADOQuery;
    Query2gk_gkkat: TStringField;
    Query2OSSZ_KM: TBCDField;
    Query2OSSZ_U_KM: TIntegerField;
    Query2OSSZ_UB_KM: TIntegerField;
    Query2OSSZ_UK_KM: TIntegerField;
    CL1: TCheckListBox;
    CBGkat: TComboBox;
    Label7: TLabel;
    Query2ja_rendsz: TStringField;
    procedure BitBtn6Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
  	procedure Ellenorzes;
    procedure FormCreate(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure BitBtn4Click(Sender: TObject);
    procedure CL1Click(Sender: TObject);

  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  UreskmnyilDlg: TUreskmnyilDlg;
  listagkat		: TStringList;
  kategstr		: string;
  mind: boolean;

implementation

uses Egyeb,Kozos,J_SQL, Ureskmlis;

{$R *.dfm}

procedure TUreskmnyilDlg.BitBtn6Click(Sender: TObject);
begin
		Close;
end;

procedure TUreskmnyilDlg.BitBtn2Click(Sender: TObject);
begin
	// EgyebDlg.Calendarbe(MD1);
  // EgyebDlg.ElsoNapEllenor(MD1, MD2);
  EgyebDlg.CalendarBe_idoszak(MD1, MD2);
  Ellenorzes;

end;

procedure TUreskmnyilDlg.Ellenorzes;
var
	jarat	: string;
  dat1	: string;
  dat2	: string;
	rendsz	: string;
  str,vegstr		: string;
begin
	if (MD1.Text='') or (MD2.Text='') then begin
		NoticeKi('Rossz d�tum megad�sa!');
		MD1.SetFocus;
		EgyebDlg.kilepes	:= true;
		Exit;
	end;


  {Megn�zz�k, hogy a sz�r�si felt�telekben szerepl� valamennyi j�rat ellen�rz�tt-e}
  //jarat		:= M0.Text;
  dat1		:= MD1.Text;
	dat2		:= MD2.Text;
	if dat1 = '' then begin
  		dat1	:= '2000.01.01.';
  end;
  if dat2 = '' then begin
  		dat2	:= EgyebDlg.MaiDatum;
  end;
  if not Jodatum(dat1) then begin
     	Exit;
  end;
  if not Jodatum(dat2) then begin
		Exit;
  end;
  vegstr	:= ' WHERE JA_JKEZD >= '''+dat1+''' AND JA_JKEZD <= '''+dat2+''' ';
  //if jarat <> '' then begin
	 //	vegstr	:= vegstr + ' AND JA_KOD = '''+jarat+''' ';
	//end else begin
 	vegstr	:= vegstr + ' AND JA_FAZIS <> 2 ';	// Storn�zott j�ratok nem kellenek
	//end;
	Query_Run(Query1, 'SELECT JA_KOD FROM JARAT ' + vegstr + ' AND JA_FAZIS <> ''1'' ' );
  Label5.Hide;
  if Query1.RecordCount < 1 then begin
		Query_Run(Query1, 'SELECT JA_KOD FROM JARAT ' + vegstr + ' AND JA_FAZIS = ''1'' ' );
		if Query1.RecordCount > 0 then begin
       	Label5.Show;
    end;
	end;
  Label5.Update;

end;

procedure TUreskmnyilDlg.FormCreate(Sender: TObject);
var
  i: integer;
begin
	EgyebDlg.SetADOQueryDatabase(Query1);
	EgyebDlg.SetADOQueryDatabase(Query2);

	listagkat	:= TStringList.Create;
	SzotarTolt(CBGkat, '340' , listagkat, false, true );
   CL1.Clear;
   for i := 0 to listagkat.Count - 1 do begin
   	CBGkat.Items[i]	:= listagkat[i];
       CL1.Items.Add(listagkat[i]);
   end;
   CBGkat.Items[0]		:= 'Minden kat.';
   CL1.Items[0]		:= 'Minden kat.';
	CL1.ItemIndex		:= 0;
	CBGkat.ItemIndex	:= 0;
end;

procedure TUreskmnyilDlg.BitBtn3Click(Sender: TObject);
begin
	EgyebDlg.Calendarbe(MD2);
  EgyebDlg.UtolsoNap(MD2);
  // MD2.Text:=DateToStr(EndOfTheMonth(StrToDate(MD2.Text)));
  Ellenorzes;

end;

procedure TUreskmnyilDlg.BitBtn4Click(Sender: TObject);
var
  kedat		: string;
  vedat		: string;
  folyt		: boolean;
  kategstr		: string;
  i			: integer;
  str			: string;
 // Jaratuj		: TJaratbeDlg;
begin
	{A lista �ssze�ll�t�sa a sz�r�si elemek alapj�n}

	EgyebDlg.kilepes	:= false;
	Ellenorzes;
	if (MD1.Text='') or (MD2.Text='') then begin
	 //	NoticeKi('Rossz d�tum megad�sa!');
		MD1.SetFocus;
		EgyebDlg.kilepes	:= true;
		Exit;
	end;

	kedat				:= MD1.Text;
	vedat				:= MD2.Text;
	if ( ( kedat <> '' ) and (vedat = '') ) then begin
		vedat		:= kedat;
		MD2.Text    := MD1.Text;
	end;
	if kedat = '' then begin
		kedat			:= '2000.01.01.';
	end;
	if vedat = '' then begin
		vedat			:= EgyebDlg.MaiDatum;
	end;

	if not Jodatum(kedat) then begin
		NoticeKi('Rossz d�tum megad�sa!');
		MD1.SetFocus;
		EgyebDlg.kilepes	:= true;
		Exit;
	end;
	if not Jodatum(vedat) then begin
		NoticeKi('Rossz d�tum megad�sa!');
		MD2.SetFocus;
		EgyebDlg.kilepes	:= true;
		Exit;
	end;
  kategstr	:= '';
	for i := 0 to CL1.Items.Count - 1 do begin
			if CL1.Checked[i] then begin
				kategstr	:= kategstr + ''''+ listagkat[i] + ''', ';
			end;
	end;
	if kategstr = '' then begin
			NoticeKi('Gk. kateg�ri�t kell v�lasztani!');
      CL1.SetFocus;
      exit;
  END;

  if True then //CheckBox1.Checked then  // r�szletes
  begin

    str:='select gk_gkkat,ja_rendsz,sum(ja_osszkm) OSSZ_KM,sum(cast( ja_uresk as integer)) OSSZ_U_KM ,'+
    'sum(cast( ja_ubkm as integer)) OSSZ_UB_KM,sum(cast( ja_ukkm as integer)) OSSZ_UK_KM '+
    'from jarat, GEPKOCSI where gk_resz=ja_rendsz '+' AND GK_GKKAT IN ( '+kategstr+'''XXX'''+' ) '+
    'and JA_JKEZD >= '''+kedat+''' AND JA_JKEZD <= '''+vedat+'''  AND JA_FAZIS <> 2 and GK_ARHIV=0 '+
    'group by gk_gkkat ,ja_rendsz order by gk_gkkat ,ja_rendsz';
  end
  else
  begin
    str:='select gk_gkkat,sum(ja_osszkm) OSSZ_KM,sum(cast( ja_uresk as integer)) OSSZ_U_KM ,'+
    'sum(cast( ja_ubkm as integer)) OSSZ_UB_KM,sum(cast( ja_ukkm as integer)) OSSZ_UK_KM '+
    'from jarat, GEPKOCSI where gk_resz=ja_rendsz '+' AND GK_GKKAT IN ( '+kategstr+'''XXX'''+' ) '+
    'and JA_JKEZD >= '''+kedat+''' AND JA_JKEZD <= '''+vedat+'''  AND JA_FAZIS <> 2 '+
    'group by gk_gkkat  order by gk_gkkat ';
  end;
 { Query2.Close;
  Query2.SQL.Clear;
  Query2.SQL.Add(str);
  Query2.Open;     }

  Application.CreateForm(TUreskmlisDlg, UreskmlisDlg);

	Query_Run(UreskmlisDlg.Query2,str,true);
	//Query_Run(Query2,str,true);
  if UreskmlisDlg.Query2.Eof then
  begin
			NoticeKi('Nincs a megadott felt�teleknek megfelel� t�tel!');
      UreskmlisDlg.Destroy;
      exit;
  end;

  UreskmlisDlg.Rep.Preview;
  UreskmlisDlg.Destroy;
end;

procedure TUreskmnyilDlg.CL1Click(Sender: TObject);
var
	i : integer;
begin
	// Ellen�rzi, hogy az els� kiv�lasztott-e
	if CL1.Checked[0] and not mind then
  begin
    mind:=True;
		for i := 1 to CL1.Items.Count - 1 do begin
			CL1.Checked[i]	:= true;
		end;
	end;
	if (not CL1.Checked[0])and mind then
  begin
    mind:=False;
		for i := 1 to CL1.Items.Count - 1 do begin
			CL1.Checked[i]	:= False;
		end;
	end;

end;

end.
