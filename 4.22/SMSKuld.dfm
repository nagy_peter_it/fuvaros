object SMSKuldDlg: TSMSKuldDlg
  Left = 0
  Top = 0
  Caption = 'MySMS k'#252'ld'#233's'
  ClientHeight = 648
  ClientWidth = 1028
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 20
    Top = 21
    Width = 78
    Height = 16
    Caption = 'Telefonsz'#225'm:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label2: TLabel
    Left = 20
    Top = 59
    Width = 81
    Height = 16
    Caption = 'SMS sz'#246'vege:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label4: TLabel
    Left = 36
    Top = 117
    Width = 55
    Height = 16
    Caption = 'Request :'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label3: TLabel
    Left = 36
    Top = 332
    Width = 55
    Height = 16
    Caption = 'Request :'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label5: TLabel
    Left = 35
    Top = 274
    Width = 44
    Height = 16
    Caption = 'Token :'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label6: TLabel
    Left = 36
    Top = 306
    Width = 63
    Height = 16
    Caption = 'Reference:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object ebSzoveg: TEdit
    Left = 107
    Top = 55
    Width = 449
    Height = 24
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    Text = 'Odafel'#233' men'#337' '#252'zenet'
  end
  object ebSzam: TEdit
    Left = 104
    Top = 18
    Width = 185
    Height = 24
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    Text = '"+36209588785"'
  end
  object Button1: TButton
    Left = 336
    Top = 16
    Width = 113
    Height = 25
    Caption = 'K'#252'ld'#233's'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    OnClick = Button1Click
  end
  object ebResult: TEdit
    Left = 96
    Top = 146
    Width = 920
    Height = 24
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 3
  end
  object ebResult2: TEdit
    Left = 96
    Top = 176
    Width = 920
    Height = 24
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 4
  end
  object Edit2: TEdit
    Left = 96
    Top = 114
    Width = 924
    Height = 24
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 5
  end
  object Panel1: TPanel
    Left = 24
    Top = 224
    Width = 992
    Height = 1
    BevelKind = bkFlat
    BorderStyle = bsSingle
    Caption = 'Panel1'
    TabOrder = 6
  end
  object ebCustomRequest: TEdit
    Left = 99
    Top = 329
    Width = 924
    Height = 24
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 7
    Text = 
      '{"address" : "+36209588785", "offset" : 0, "limit" : 999, "authT' +
      'oken" : "", "apiKey" : "zDVf5m0vqwu1cs0H_4ju6Q"}'
  end
  object Button2: TButton
    Left = 99
    Top = 359
    Width = 113
    Height = 25
    Caption = 'K'#252'ld'#233's'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 8
    OnClick = Button2Click
  end
  object ebAuthToken: TEdit
    Left = 100
    Top = 271
    Width = 920
    Height = 24
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 9
  end
  object Button3: TButton
    Left = 96
    Top = 240
    Width = 113
    Height = 25
    Caption = 'Get Token'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 10
    OnClick = Button3Click
  end
  object ebResult3: TEdit
    Left = 99
    Top = 390
    Width = 920
    Height = 24
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 11
  end
  object ebRef: TEdit
    Left = 100
    Top = 301
    Width = 924
    Height = 24
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 12
    Text = '/json/remote/sms/sent'
  end
  object Edit1: TEdit
    Left = 100
    Top = 462
    Width = 920
    Height = 24
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 13
    Text = '"message":"Odafel'#258#169' men'#313#8216' '#258#317'zenet","incoming":'
  end
  object Edit3: TEdit
    Left = 100
    Top = 526
    Width = 920
    Height = 24
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 14
  end
  object Button4: TButton
    Left = 120
    Top = 495
    Width = 75
    Height = 25
    Caption = 'UTF8Decode'
    TabOrder = 15
    OnClick = Button4Click
  end
end
