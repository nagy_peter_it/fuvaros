unit KiAdBlue;

interface

uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls, Egyeb,
  Forms, Dialogs, DBTables, DB, StdCtrls, Buttons, Mask, DBCtrls,
  ExtCtrls,DateUtils;

type
  TKiAdBlueDlg = class(TForm)
    BitBtn4: TBitBtn;
    BitBtn6: TBitBtn;
    Label3: TLabel;
    M0: TMaskEdit;
    BitBtn1: TBitBtn;
    M1: TMaskEdit;
    Label1: TLabel;
    BitBtn2: TBitBtn;
    Label2: TLabel;
    M2: TMaskEdit;
    BitBtn3: TBitBtn;
    CB1: TCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure BitBtn4Click(Sender: TObject);
    procedure BitBtn6Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
	 procedure M1Exit(Sender: TObject);
    procedure M2Exit(Sender: TObject);
  private
  public
  end;

var
  KiAdBlueDlg: TKiAdBlueDlg;

implementation

uses
	J_VALASZTO, KiAdBlueList, Kozos;
{$R *.DFM}

procedure TKiAdBlueDlg.FormCreate(Sender: TObject);
begin
	M0.Text := '';
end;

procedure TKiAdBlueDlg.BitBtn4Click(Sender: TObject);
begin
	{A lista �ssze�ll�t�sa a sz�r�si elemek alapj�n}
	Screen.Cursor	:= crHourGlass;
	Application.CreateForm(TKiAdBlueListDlg, KiAdBlueListDlg);
	KiAdBlueListDlg.Tolt(M1.Text, M2.Text, M0.Text, CB1.Checked);
	Screen.Cursor	:= crDefault;
	if KiAdBlueListDlg.vanadat	then begin
		KiAdBlueListDlg.Rep.Preview;
  	end else begin
		NoticeKi('Nincs a felt�teleknek megfelel� t�tel!');
  	end;
	KiAdBlueListDlg.Destroy;
end;

procedure TKiAdBlueDlg.BitBtn6Click(Sender: TObject);
begin
	Close;
end;

procedure TKiAdBlueDlg.BitBtn1Click(Sender: TObject);
begin
	GepkocsiValaszto(M0, GK_TRAKTOR);
end;

procedure TKiAdBlueDlg.BitBtn2Click(Sender: TObject);
begin
	// EgyebDlg.Calendarbe(M1);
  // EgyebDlg.ElsoNapEllenor(M1, M2);
  EgyebDlg.CalendarBe_idoszak(M1, M2);
end;

procedure TKiAdBlueDlg.BitBtn3Click(Sender: TObject);
begin
	EgyebDlg.Calendarbe(M2);
  EgyebDlg.UtolsoNap(M2);
  // M2.Text:=DateToStr(EndOfTheMonth(StrToDate(M2.Text)));
end;

procedure TKiAdBlueDlg.M1Exit(Sender: TObject);
begin
	if ( ( M1.Text <> '' ) and ( not Jodatum2(M1 ) ) ) then begin
		NoticeKi('Hib�s d�tum megad�sa!');
		M1.Text := '';
		M1.Setfocus;
	end;
  EgyebDlg.ElsoNapEllenor(M1, M2);
end;

procedure TKiAdBlueDlg.M2Exit(Sender: TObject);
begin
	if ( ( M2.Text <> '' ) and ( not Jodatum2(M2 ) ) ) then begin
		NoticeKi('Hib�s d�tum megad�sa!');
		M2.Text := '';
		M2.Setfocus;
	end;
end;

end.


