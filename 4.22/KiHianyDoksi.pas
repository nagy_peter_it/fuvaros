unit KiHianyDoksi;

interface

uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls, Egyeb,
  Forms, Dialogs, DBTables, DB, StdCtrls, Buttons, Mask, DBCtrls,
  ExtCtrls, ADODB, QuickRpt;

type
  TKiHianyDoksiDlg = class(TForm)
	 BitBtn4: TBitBtn;
	 BitBtn6: TBitBtn;
	 CB1: TCheckBox;
	 CB2: TCheckBox;
	 CB3: TCheckBox;
	 RG1: TRadioGroup;
	 procedure FormCreate(Sender: TObject);
	 procedure BitBtn4Click(Sender: TObject);
	 procedure BitBtn6Click(Sender: TObject);
	 procedure CB3Click(Sender: TObject);
  private
  public
  end;

var
  KiHianyDoksiDlg: TKiHianyDoksiDlg;

implementation

uses
	HianyDokList, Kozos;
{$R *.DFM}

procedure TKiHianyDoksiDlg.FormCreate(Sender: TObject);
begin
	CB3Click(Sender);
end;

procedure TKiHianyDoksiDlg.BitBtn4Click(Sender: TObject);
begin
	if ((not CB1.Checked) and (not CB2.Checked) and (not CB3.Checked) ) then begin
		NoticeKi('Legal�bb egy kateg�ri�t v�lasztani kell!');
		Exit;
	end;
	{A lista �ssze�ll�t�sa a sz�r�si elemek alapj�n}
	Screen.Cursor	:= crHourGlass;
	Application.CreateForm(THianyDoklistDlg, HianyDoklistDlg);
   HianyDoklistDlg.Tolto(CB1.Checked, CB2.Checked, CB3.Checked, RG1.ItemIndex);
	Screen.Cursor	:= crDefault;
	HianyDoklistDlg.Rep.Preview;
	HianyDoklistDlg.Destroy;
end;

procedure TKiHianyDoksiDlg.BitBtn6Click(Sender: TObject);
begin
	Close;
end;

procedure TKiHianyDoksiDlg.CB3Click(Sender: TObject);
begin
	if CB3.Checked then begin
		RG1.Enabled	:= true;
	end else begin
		RG1.Enabled	:= false;
	end;
end;

end.


