unit JARLIS;

interface

uses
  WinTypes, WinProcs, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, QuickRpt, QRExpr, Qrctrls, ExtCtrls, printers, DB, DBTables, Egyeb, Forgalom, J_SQL,
  Grids, ADODB ;

type
  TJarlisDlg = class(TForm)
    Rep: TQuickRep;
    QRBand3: TQRBand;
    QRLSzamla: TQRLabel;
    QRBand1: TQRBand;
    QRLabel16: TQRLabel;
    QRLabel6: TQRLabel;
    QRSysData1: TQRSysData;
    QRLabel7: TQRLabel;
    QRSysData4: TQRSysData;
    QRLabel1: TQRLabel;
    QRLabel8: TQRLabel;
    QRShape3: TQRShape;
    QRShape5: TQRShape;
    QRShape8: TQRShape;
    QRLabel4: TQRLabel;
    QRLabel12: TQRLabel;
    QRLabel13: TQRLabel;
    QRLabel24: TQRLabel;
    QRLabel2: TQRLabel;
    QRLabel5: TQRLabel;
    QS2: TQRLabel;
    QS3: TQRLabel;
    QS4: TQRLabel;
    SG1: TStringGrid;
    QSA1: TQRShape;
    QRShape1: TQRShape;
    QRLabel9: TQRLabel;
    QS30: TQRLabel;
    Query1: TADOQuery;
    Query2: TADOQuery;
    Query3: TADOQuery;
    QRShape2: TQRShape;
    QRLabel3: TQRLabel;
    QS31: TQRLabel;
    procedure FormCreate(Sender: TObject);
    procedure	Tolt(jarat : string );
    procedure QRBand1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure RepBeforePrint(Sender: TCustomQuickRep;
      var PrintReport: Boolean);
    procedure RepNeedData(Sender: TObject; var MoreData: Boolean);
	private
  	sorszam			: integer;
     selkm1			: double;
     selkm2			: double;
     sextra1			: double;
     sedij1			: double;
     sedij2			: double;
     jaratsz			: string;
  public
     vanadat			: boolean;
     ossz				: double;
  end;

var
  JarlisDlg: TJarlisDlg;

implementation

{$R *.DFM}
uses
	Kozos;

procedure TJarlisDlg.FormCreate(Sender: TObject);
begin
	EgyebDlg.SetADOQueryDatabase(Query1);
	EgyebDlg.SetADOQueryDatabase(Query2);
	EgyebDlg.SetADOQueryDatabase(Query3);
  sorszam					:= 0;
end;

procedure	TJarlisDlg.Tolt(jarat : string );
var
  	ertek    			: double;
  	sof1		  		: string;
  	sorszam				: integer;
  	jojarat				: string;
  	kmsz				: string;
  	felra1				: double;
  	feldij				: double;
   tipus				: integer;
begin
  	selkm1			:= 0;
  	selkm2			:= 0;
  	sedij1			:= 0;
  	sedij2			:= 0;
  	sorszam			:= 0;
	vanadat 		:= false;
 	SG1.RowCount 	:= 0;
  	feldij			:=  StringSzam(EgyebDlg.Read_SZGrid('CEG', '301'));

  	{El�sz�r lesz�rj�k a j�ratokat a megadott felt�telek alapj�n}
  	if Query_Run (Query1,'SELECT * FROM JARAT WHERE JA_KOD = '''+jarat+''' ' ,true) then begin
  		jaratsz	:= Query1.FieldByName('JA_KOD').AsString;
//       jojarat	:= copy(Query1.FieldByName('JA_ORSZ').AsString+URESSTRING,1,3) + '-' +Format('%5.5d',[Query1.FieldByName('JA_ALKOD').AsInteger]);
       jojarat := GetJaratszam(Query1.FieldByName('JA_KOD').AsString);
       if jarat <> '' then begin
			QRLabel5.Caption	:= jojarat;
       end else begin
			QRLabel5.Caption	:= 'T�bb j�rat';
       end;
		vanadat 			:= true;
		QRLabel16.Caption	:= Query1.FieldByname('JA_RENDSZ').AsString;

       {A dolgoz�i adatok bet�tele}
		Query_Run(Query2, 'SELECT * FROM JARSOFOR WHERE JS_JAKOD = '''+jaratsz+''' ORDER BY JS_SORSZ ');
//   	if Query2.RecordCount > 0 then begin
		while not Query2.Eof do begin
           {% km teljes�t�se}
           if  Round(Query1.FieldByname('JA_OSSZKM').AsFloat ) = 0  then begin
               kmsz 		:= '';
           end else begin
               kmsz 		:= Format('  K/T: %5.1f%%',[Round(Query2.FieldByname('JS_SZAKM').AsFloat)*100 /
                               Round(Query1.FieldByname('JA_OSSZKM').AsFloat )]);
           end;
           sof1		:= Query_Select('DOLGOZO', 'DO_KOD', Query2.FieldByname('JS_SOFOR').AsString , 'DO_NAME');
  			if sof1 <> '' then begin
				QRLabel16.Caption	:= QRLabel16.Caption + '; ' + sof1;
               selkm1	:= StringSzam(Query2.FieldByname('JS_SZAKM').AsString);
               sedij1	:= StringSzam(Query2.FieldByname('JS_JADIJ').AsString);
               sextra1	:= StringSzam(Query2.FieldByname('JS_EXDIJ').AsString);
               felra1  := StringSzam(Query2.FieldByname('JS_FELRA').AsString);
               SG1.RowCount 					:= SG1.RowCount + 1;
               SG1.Cells[0,SG1.RowCount - 1]	:= IntToStr(sorszam);
               SG1.Cells[1,SG1.RowCount - 1]	:= 'Fizet�s';
               if sextra1 <> 0 then begin
                   SG1.Cells[2,SG1.RowCount - 1]	:= sof1+'('+Format('%.1f km*%.1f Ft+%.1f Ft+%.1f db*%.0f Ft)',[selkm1, sedij1, sextra1, felra1, feldij])+kmsz;
               end else begin
                   SG1.Cells[2,SG1.RowCount - 1]	:= sof1+'('+Format('%.1f km*%.1f Ft+%.1f db*%.0f Ft)',[selkm1, sedij1, felra1, feldij])+kmsz;
               end;
               SG1.Cells[3,SG1.RowCount - 1]	:= SzamString( -1 * selkm1 * sedij1 - sextra1- felra1*feldij, 12, 0);
               SG1.Cells[4,SG1.RowCount - 1]	:= Query1.FieldByName('JA_KOD').AsString;
               ossz							:= ossz - selkm1 * sedij1 - sextra1 - felra1*feldij;
               Inc(sorszam);
     		end;
			Query2.Next;
       end;

		{Bet�ltj�k a k�lts�geket a Query3-be}
  		if Query_Run (Query3,'SELECT * FROM KOLTSEG WHERE KS_JAKOD = '''+jaratsz+''' '+
  			' ORDER BY KS_RENDSZ DESC, KS_DATUM' ,true) then begin
     		while not Query3.EOF do begin
     			{K�lts�g a gridbe}
  				SG1.RowCount 						:= SG1.RowCount + 1;
  				SG1.Cells[0,SG1.RowCount - 1]	:= IntToStr(sorszam);
           	Inc(sorszam);
  				SG1.Cells[1,SG1.RowCount - 1]	:= Query3.FieldByname('KS_DATUM').AsString;

               tipus			:= StrToIntdef(Query3.FieldByName('KS_TIPUS').AsString,0);
               case tipus of
                   0: // �zemanyag k�lts�gr�l van sz�!
                   begin
                       {�zemanyag k�lts�gr�l van sz�}
                       SG1.Cells[2,SG1.RowCount - 1]	:= '�zemanyag ktg. '+Query3.FieldByname('KS_RENDSZ').AsString+
                           ' ('+Format('%.1f',[Query3.FieldByname('KS_UZMENY').AsFloat])+' l = '+
                           Format('%.2f',[Query3.FieldByname('KS_OSSZEG').AsFloat])+' '+Query3.FieldByname('KS_VALNEM').AsString+
                           ' * '+Format('%.4f',[Query3.FieldByname('KS_ARFOLY').AsFloat])+' )';
                   end;
                   1: // Egy�b k�lts�g
                   begin
						SG1.Cells[2,SG1.RowCount - 1]	:= Query3.FieldByname('KS_LEIRAS').AsString;
                   end;
                   2: // AD Blue
                   begin
                       SG1.Cells[2,SG1.RowCount - 1]	:= 'AD Blue ktg. '+Query3.FieldByname('KS_RENDSZ').AsString+
                           ' ('+Format('%.1f',[Query3.FieldByname('KS_UZMENY').AsFloat])+' l = '+
                           Format('%.2f',[Query3.FieldByname('KS_OSSZEG').AsFloat])+' '+Query3.FieldByname('KS_VALNEM').AsString+
                           ' * '+Format('%.4f',[Query3.FieldByname('KS_ARFOLY').AsFloat])+' )';
                   end;
                   3: // H�t� tankol�s
                   begin
                       SG1.Cells[2,SG1.RowCount - 1]	:= 'H�t� tankol�s  '+Query3.FieldByname('KS_RENDSZ').AsString+
                           ' ('+Format('%.1f',[Query3.FieldByname('KS_UZMENY').AsFloat])+' l = '+
                           Format('%.2f',[Query3.FieldByname('KS_OSSZEG').AsFloat])+' '+Query3.FieldByname('KS_VALNEM').AsString+
                           ' * '+Format('%.4f',[Query3.FieldByname('KS_ARFOLY').AsFloat])+' )';
                   end;
               end;

           	{Ellen�rizz�k, hogy a k�lts�g viszonyhoz van-e kapcsolva. Ha igen, a viszonylatot is �rjuk ki}
               if Query3.FieldByname('KS_VIKOD').AsString <> '' then begin
  					if Query_Run (Query2,'SELECT * FROM VISZONY WHERE VI_VIKOD = '+ Query3.FieldByname('KS_VIKOD').AsString ,true) then begin
           	   		SG1.Cells[2,SG1.RowCount - 1]	:= SG1.Cells[2,SG1.RowCount - 1]	+ ' (' +
           	      	Query2.FieldByname('VI_HONNAN').AsString+'-'+Query2.FieldByname('VI_HOVA').AsString + ')';
                   end;
           	end;
				ertek	:= -1 * Query3.FieldByname('KS_OSSZEG').AsFloat * Query3.FieldByname('KS_ARFOLY').AsFloat;
  				SG1.Cells[3,SG1.RowCount - 1]	:= SzamString( ertek, 12, 0);
				SG1.Cells[4,SG1.RowCount - 1]	:= Query1.FieldByName('JA_KOD').AsString;
				SG1.Cells[5,SG1.RowCount - 1]	:= Query3.FieldByName('KS_ATLAG').AsString;
				SG1.Cells[6,SG1.RowCount - 1]	:= Query3.FieldByName('KS_ORSZA').AsString;
  				ossz	:= ossz + ertek;
     			Query3.Next;
     		end;
  		end;

  		{K�lts�g �sszesen bet�lt�se}
		SG1.RowCount 						:= SG1.RowCount + 1;
		SG1.Cells[0,SG1.RowCount - 1]	:= '';
		SG1.Cells[1,SG1.RowCount - 1]	:= '---';
		SG1.Cells[2,SG1.RowCount - 1]	:= 'K�lts�gek �sszesen';
		SG1.Cells[3,SG1.RowCount - 1]	:= SzamString( ossz, 12, 0);
		SG1.Cells[4,SG1.RowCount - 1]	:= Query1.FieldByName('JA_KOD').AsString;
	end;
{
 		Query1.Next;
  end;
}
end;

procedure TJarlisDlg.QRBand1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
begin
	QS2.Caption	:= SG1.Cells[1,sorszam];
	QS3.Caption	:= SG1.Cells[2,sorszam];
	QS31.Caption	:= SG1.Cells[6,sorszam];
 	QS30.Caption := SG1.Cells[5,sorszam];
	QS4.Caption := SzamString(StringSzam(SG1.Cells[3,sorszam]), 12, 0);
// 	QSA1.Visible	:= false;
 	QSA1.Height	:= 0;
  if SG1.Cells[1,sorszam] = '---' then begin
  	{�sszesen sor}
		QS2.Caption		:= '';
 		QSA1.Height		:= 19;
//  	QSA1.Visible	:= true;
  end;
 	Inc(sorszam);
end;

procedure TJarlisDlg.RepBeforePrint(Sender: TCustomQuickRep;
      var PrintReport: Boolean);
begin
  sorszam	:= 0;
end;

procedure TJarlisDlg.RepNeedData(Sender: TObject; var MoreData: Boolean);
begin
 	MoreData	:= true;
  if sorszam >= SG1.RowCount then begin
  	MoreData	:= false;
  end;
end;

end.
