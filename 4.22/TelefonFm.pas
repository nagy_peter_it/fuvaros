unit TelefonFm;

interface

uses
	SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
	StdCtrls, Forms, ExtCtrls, Dialogs, J_FOFORMUJ, Vcl.Menus;

type
	TTelefonFormDlg = class(TJ_FOFORMUJDLG)
    PopupMenu1: TPopupMenu;
    KeszulekCsere: TMenuItem;
    UGRAS1: TMenuItem;
	  procedure FormCreate(Sender: TObject);override;
	 procedure Adatlap(cim, kod : string);override;
	 procedure ButtonModClick(Sender: TObject);override;
    procedure KeszulekCsereClick(Sender: TObject);
    procedure UGRAS1Click(Sender: TObject);
	end;

var
	TelefonFormDlg : TTelefonFormDlg;

implementation

uses
	Egyeb, Telefonbe2, J_SQL, TelefonCsere, Clipbrd, Kozos;

{$R *.DFM}

procedure TTelefonFormDlg.FormCreate(Sender: TObject);
var
  S: string;
begin
	Inherited FormCreate(Sender);
  S:= 'select TK_ID, sz2.SZ_MENEV TEL_TIPUS, sz.SZ_MENEV TEL_STATUSZ, ISNULL(DO_NAME, '''') DO_NAME, '+
   //  ' TK_IMEI, TK_TECH, TK_APPLEID, TK_APPLEJELSZO, TK_KIADDAT, TK_MEGJE, TK_STATUSKOD, ISNULL(TE_TELSZAM,'''') TE_TELSZAM, '+
    ' TK_IMEI, TK_TECH, TK_KIADDAT, TK_MEGJE, TK_STATUSKOD, ISNULL(TE_TELSZAM,'''') TE_TELSZAM, '+
    ' case when TK_KIADHATO = 1 then ''Igen'' else ''Nem'' end TK_KIADHATO, '+
    ' TK_MDM '+
    ' from SZOTAR sz, SZOTAR sz2, TELKESZULEK k '+
    ' left outer join DOLGOZO ON TK_DOKOD = DO_KOD '+
    ' left outer join TELELOFIZETES ON TK_ID = TE_TKID '+
    ' where sz.SZ_ALKOD = TK_STATUSKOD and sz.SZ_FOKOD=144 '+
    ' and sz2.SZ_ALKOD = TK_TIPUSKOD and sz2.SZ_FOKOD=146 ';
    // ' and TK_STATUSKOD = 1 ';
  S:= 'select * from ('+S+ ') a where 1=1';  // a sz�r�sekhez be kell "csomagolni"

	AddSzuromezoRovid('TEL_TIPUS',  '(select distinct SZ_MENEV TEL_TIPUS from SZOTAR where SZ_FOKOD=146) a');
	AddSzuromezoRovid('TEL_STATUSZ', '(select distinct SZ_MENEV TEL_STATUSZ from SZOTAR where SZ_FOKOD=144) a');
  AddSzuromezoRovid('DO_NAME',  '', '1');
  AddSzuromezoRovid('TE_TELSZAM', '', '1');
  AddSzuromezoRovid('TK_MDM', '', '1');
  AddSzuromezoRovid('TK_TECH',  'TELKESZULEK');
	AddSzuromezo('DO_NAME', 'SELECT DISTINCT DO_NAME FROM TELKESZULEK LEFT OUTER JOIN DOLGOZO ON TK_DOKOD = DO_KOD');
  AddSzuromezoRovid('TK_KIADHATO', '');
	FelTolto('�j telefonk�sz�l�k felvitele ', 'Telefonk�sz�l�k adatok m�dos�t�sa ',
			'Telefonk�sz�l�kek list�ja', 'TK_ID', S, 'TELKESZULEK', QUERY_KOZOS_TAG );
	nevmezo	:= 'TK_ID';
	width	:= DLG_WIDTH;
	height	:= DLG_HEIGHT;
  // uresszures:= True;
  ButtonTor.Enabled :=False;
  DBGrid2.PopupMenu	:= PopupMenu1;
end;

procedure TTelefonFormDlg.Adatlap(cim, kod : string);
begin
	Application.CreateForm(TTelefonbe2Dlg, AlForm);
	Inherited Adatlap(cim, kod );
end;

procedure TTelefonFormDlg.ButtonModClick(Sender: TObject);
var
	tulaj	: string;
begin
	if not voltalt then begin
 		voltalt 	:= true;
 		Exit;
 	end;
	if ButtonMod.Enabled then begin
  		Adatlap(MODOSIT,Query1.FieldByName(FOMEZO).AsString);
      end;
end;

procedure TTelefonFormDlg.KeszulekCsereClick(Sender: TObject);
begin
   Application.CreateForm(TTelefonCsereDlg,TelefonCsereDlg);
   TelefonCsereDlg.RegiTelKod:=Query1.FieldByName('TK_ID').AsInteger;
   TelefonCsereDlg.meRegiIMEI.Text:=Query1.FieldByName('TK_IMEI').AsString;
   // if NoticeKi('Hozzuk a r�gi Apple ID-t? ', NOT_QUESTION) = 0 then begin
   //   TelefonCsereDlg.meAppleID.Text:=Query1.FieldByName('TK_APPLEID').AsString;
   //   TelefonCsereDlg.meAppleJelszo.Text:=Query1.FieldByName('TK_APPLEJELSZO').AsString;
   //   end;  // if
   Clipboard.Clear;
	 Clipboard.AsText:= Query1.FieldByName('TK_IMEI').AsString;
   TelefonCsereDlg.ShowModal;
   if TelefonCsereDlg.ret_kod <> '' then begin
     ModLocate (Query1, FOMEZO, TelefonCsereDlg.ret_kod );
     DBGrid2.Refresh;
     end;
end;

procedure TTelefonFormDlg.UGRAS1Click(Sender: TObject);
var
  pt : tPoint;
begin
  // pt := ScreenToClient(Mouse.CursorPos);
  // EgyebDlg.ppmUgras.Popup(pt.X, pt.Y);
end;

{procedure TTelefonFormDlg.FormBezaras();
begin
    ButtonKuld.OnClick;
end;
 }
end.

