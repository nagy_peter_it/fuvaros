unit Arfolybe;

interface

uses
	SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
	Forms, Dialogs, StdCtrls, Buttons, Mask, DB, DBTables, J_ALFORM, ADODB;

type
	TArfolybeDlg = class(TJ_AlformDlg)
	BitElkuld: TBitBtn;
	BitKilep: TBitBtn;
	Label2: TLabel;
	Label7: TLabel;
	MaskEdit2: TMaskEdit;
	MaskEdit7: TMaskEdit;
	Label8: TLabel;
	Label9: TLabel;
	Label12: TLabel;
  MaskEdit10: TMaskEdit;
    AFACombo: TComboBox;
    BitBtn7: TBitBtn;
    Query1: TADOQuery;
    Label1: TLabel;
    MaskEdit1: TMaskEdit;
	procedure BitKilepClick(Sender: TObject);
	procedure FormCreate(Sender: TObject);
	procedure Tolto(cim : string; teko : string);override;
	procedure BitElkuldClick(Sender: TObject);
	procedure FormDestroy(Sender: TObject);
  procedure MaskEditClick(Sender: TObject);
  procedure MaskEditExit(Sender: TObject);
	procedure MaskEditKeyPress(Sender: TObject; var Key: Char);
    procedure Modosit(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit10Exit(Sender: TObject);
    procedure MaskEdit2Exit(Sender: TObject);
    procedure MaskEdit2KeyPress(Sender: TObject; var Key: Char);
    procedure BitBtn7Click(Sender: TObject);
	private
     modosult 	: boolean;
	public
		afalist		: TStringList;
     ret_arfoly	: string;
	end;

var
	ArfolybeDlg: TArfolybeDlg;

implementation

uses
	Egyeb, J_SQL, Kozos;
{$R *.DFM}

procedure TArfolybeDlg.BitKilepClick(Sender: TObject);
begin
	if modosult then begin
  		if NoticeKi('M�dosultak az adatok . Ki akar l�pni ment�s n�lk�l?', NOT_QUESTION ) = 0 then begin
			ret_kod := '';
			Close;
     	end;
  	end else begin
		ret_kod := '';
		Close;
  	end;
end;

procedure TArfolybeDlg.FormCreate(Sender: TObject);
begin
 	EgyebDlg.SetADOQueryDatabase(Query1);
	ret_kod 	:= '';
  	modosult 	:= false;
  	afalist 	:= TStringList.Create;
	SzotarTolt(AFACombo, '110' , afalist, false );
  	AFACombo.ItemIndex := 0;
end;

procedure TArfolybeDlg.Tolto(cim : string; teko:string);
begin
	ret_kod 	:= teko;
	Caption := cim;
	MaskEdit2.Text := '1';
	MaskEdit7.Text := '';
	MaskEdit10.Text := EgyebDlg.MaiDatum;
	ComboSet (AfaCombo, 'EUR', afalist);
	if ret_kod <> '' then begin		{M�dos�t�s}
		if Query_Run (Query1, 'SELECT * FROM ARFOLYAM WHERE AR_KOD = '+teko,true) then begin
			MaskEdit2.Text 	:= Query1.FieldByName('AR_EGYSEG').AsString;
			MaskEdit7.Text 	:= SzamString(Query1.FieldByName('AR_ERTEK').AsFloat,12,4);
			MaskEdit10.Text := Query1.FieldByName('AR_DATUM').AsString;
			MaskEdit1.Text 	:= Query1.FieldByName('AR_MEGJE').AsString;
			ComboSet (AfaCombo, Query1.FieldByName('AR_VALNEM').AsString, afalist);
		end;
	end;
  	modosult 	:= false;
end;

procedure TArfolybeDlg.BitElkuldClick(Sender: TObject);
var
  ujkodszam 	: integer;
  S: string;
begin
	if not Jodatum2(MaskEdit10 ) then begin
		NoticeKi('Hib�s d�tum megad�sa!');
		MaskEdit10.Text := '';
		MaskEdit10.Setfocus;
		Exit;
	end;
	if ret_kod = '' then begin   // �j rekord felvitele
		ujkodszam	:= GetNextCode('ARFOLYAM', 'AR_KOD', 1, 5);
		ret_kod	:= Format('%5.5d',[ujkodszam]);
    S:='INSERT INTO ARFOLYAM (AR_KOD, AR_EGYSEG, AR_VALNEM, AR_ERTEK, AR_DATUM, AR_MEGJE) VALUES (';
    S:= S+ ret_kod+', '+MaskEdit2.Text+', '+ ''''+afalist[AfaCombo.ItemIndex]+''', '+SqlSzamString(StringSzam(MaskEdit7.Text),12,4)+', ';
    S:= S+ ''''+MaskEdit10.Text+''', '+ ''''+MaskEdit1.Text+''''+ ' )';
		Query_Run ( Query1, S,true);
	  end
  else begin  // megl�v� rekord m�dos�t�sa
 	  Query_Update (Query1, 'ARFOLYAM',
		  [
  		'AR_EGYSEG', MaskEdit2.Text,
  		'AR_VALNEM', ''''+afalist[AfaCombo.ItemIndex]+'''',
  		'AR_ERTEK', SqlSzamString(StringSzam(MaskEdit7.Text),12,4),
  		'AR_DATUM', ''''+MaskEdit10.Text+'''',
  		'AR_MEGJE', ''''+MaskEdit1.Text+''''
  	  ], ' WHERE AR_KOD = '+ret_kod);
    end;  // else
	Query1.Close;
	ret_arfoly	:= MaskEdit7.Text;
	Close;
end;

procedure TArfolybeDlg.FormDestroy(Sender: TObject);
begin
  afalist.Free;
end;

procedure TArfolybeDlg.MaskEditClick(Sender: TObject);
begin
	with Sender as TMaskEdit do begin
	SelectAll;
  end;
end;

procedure TArfolybeDlg.MaskEditExit(Sender: TObject);
begin
	with Sender as TMaskEdit do begin
		Text := StrSzamStr(Text,12,4);
	end;
end;

procedure TArfolybeDlg.MaskEditKeyPress(Sender: TObject; var Key: Char);
begin
  with Sender as TMaskEdit do begin
		Key := EgyebDlg.Jochar(Text,Key,12,4);
  end;
end;

procedure TArfolybeDlg.Modosit(Sender: TObject);
begin
	modosult := true;
end;

procedure TArfolybeDlg.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
	EgyebDlg.FormReturn(Key);
end;

procedure TArfolybeDlg.MaskEdit10Exit(Sender: TObject);
begin
	if ( ( MaskEdit10.Text <> '' ) and ( not Jodatum2(MaskEdit10 ) ) ) then begin
		NoticeKi('Hib�s d�tum megad�sa!');
		MaskEdit10.Text := '';
		MaskEdit10.Setfocus;
	end;
end;

procedure TArfolybeDlg.MaskEdit2Exit(Sender: TObject);
begin
	with Sender as TMaskEdit do begin
  		Text := StrSzamStr(Text,5,0);
  	end;
end;

procedure TArfolybeDlg.MaskEdit2KeyPress(Sender: TObject; var Key: Char);
begin
  	with Sender as TMaskEdit do begin
  		Key := EgyebDlg.Jochar(Text,Key,5,0);
  	end;
end;

procedure TArfolybeDlg.BitBtn7Click(Sender: TObject);
begin
	EgyebDlg.Calendarbe(MaskEdit10);
end;

end.
