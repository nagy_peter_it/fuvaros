unit PalvaltFm;

interface

uses
	SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
	StdCtrls, Forms, ExtCtrls, Dialogs, J_FOFORMUJ, Buttons, Jaratbe;

type
	TPalvaltFmDlg = class(TJ_FOFORMUJDLG)
	  procedure FormCreate(Sender: TObject);override;
    procedure Adatlap(cim, kod : string);override;
	end;

var
	PalvaltFmDlg : TPalvaltFmDlg;

implementation

uses
	Egyeb, J_SQL, PalValtbe;

{$R *.DFM}

procedure TPalvaltFmDlg.FormCreate(Sender: TObject);
begin
	Inherited FormCreate(Sender);
	AddSzuromezoRovid('PV_HEKO1', 'PALVALT');
	AddSzuromezoRovid('PV_HEKO2', 'PALVALT');
	AddSzuromezoRovid('PV_HETI1', 'PALVALT');
	AddSzuromezoRovid('PV_HETI2', 'PALVALT');
	AddSzuromezoRovid('PV_TIPUS', 'PALVALT');
	FelTolto('�j paletta v�ltoz�s felvitele ', 'Paletta v�ltoz�sok m�dos�t�sa ',
			'Paletta v�ltoz�sok list�ja', 'PV_PVKOD', 'SELECT * FROM PALVALT ', 'PALVALT', QUERY_KOZOS_TAG );
	width				:= DLG_WIDTH;
	height				:= DLG_HEIGHT;
end;

procedure TPalvaltFmDlg.Adatlap(cim, kod : string);
begin
	Application.CreateForm(TPalValtbeDlg, AlForm);
	Inherited Adatlap(cim, kod );
end;

end.

