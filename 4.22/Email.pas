unit Email;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Kozos, KozosTipusok, IdBaseComponent, IdComponent, IdTCPConnection,
  IdTCPClient, IdAttachmentFile, IdAttachmentMemory,
  IdMessageClient, IdText, IdSMTP, IdMessage, IdExplicitTLSClientServerBase, IdSMTPBase;

type
  TEmailDlg = class(TForm)
    NMSMTP1: TIdSMTP;
    procedure FormCreate(Sender: TObject);
  private
    function WriteLogFile(FName:string; Text:string):Boolean;
  public
    procedure Mail(IdMess :TIdMessage; cim, masolat, targy: string; torzs: TStringList; kellfelado: boolean; FeladoNeve: string);
    function SendMail(IdMess :TIdMessage) : string;
    procedure SendIrodaiLevel(Rendszam, Szoveg, LogFN: string);
    procedure SendSimaIrodaiLevel(const Cimzett, Targy, Szoveg, logfn: string);
    function SendLevel(cimzettlista: TStringList; masolat, targy, szoveg: string; kellfelado: boolean; feladoneve: string;
                          MellekletLista: TMemoryStreamPointerArray; MellekletNevek, MellekletTipusok: TStringList;
                          MellekletStilus: string; ContentType: string = 'text/plain'): string;

  end;

var
  EmailDlg: TEmailDlg;

implementation

{$R *.dfm}

uses Egyeb, J_SQL;

procedure TEmailDlg.FormCreate(Sender: TObject);
begin
   NMSMTP1.Host:= EgyebDlg.SMTP_HOST;
end;

procedure TEmailDlg.Mail(IdMess: TIdMessage; cim, masolat, targy: string;
  torzs: TStringList; kellfelado: boolean; FeladoNeve: string);
var
  i: integer;
  sor: string;
begin
     IdMess.MessageParts.Clear;
     if kellfelado then
     begin
       // IdMess.From.Address:='info@jsspeed.hu';     // felad� c�me
       IdMess.From.Address:='info@js.hu';     // felad� c�me
       IdMess.From.Name:=FeladoNeve  // 'Megb�z�s ellen�rz�';
     end;
     IdMess.Recipients.EMailAddresses:=cim;   // c�mzett
     IdMess.CCList.EMailAddresses:=masolat;
     IdMess.Subject:=targy;

     {for i:=0 to torzs.Count-1 do begin
       sor:=torzs[i];
       IdMess.Body.Add(sor);
     end;
     }
     IdMess.Body:= torzs;
end;

function TEmailDlg.SendMail(IdMess: TIdMessage): string;
var
  PCName: string;
begin
  Result:='';
  if IdMess.Recipients.EMailAddresses='' then
    exit;
  // PCName:= GetLocalComputerName;
  // if PCName <> 'ts1' then begin  // 2018.08.06. anonymous connector-ok letiltva
   NMSMTP1.Username:= 'Szolgaltatasgazda@jsspeed.local';  // 'JSSPEED\Szolgaltatasgazda';
   NMSMTP1.Password:= 'Servicer_2018';
  //  end;
  NMSMTP1.Connect;
  Try
     try
        if NMSMTP1.Connected then begin
         NMSMTP1.Send(IdMess);
         Result:='';
         // WriteLogFile(logfile,'  '+IdMess.Recipients.EMailAddresses+';'+IdMess.CCList.EMailAddresses);
         end;
      except
         on E: Exception do begin
            // WriteLogFile(logfile,'"SendMail" hiba: '+E.Message+' C�mzettek:'+IdMess.Recipients.EMailAddresses);
            Result:= '"SendMail" hiba: '+E.Message+' C�mzettek:'+IdMess.Recipients.EMailAddresses
            end;  // on E
         end;  // try-except
     Finally
        if NMSMTP1.Connected then
         NMSMTP1.Disconnect;
       End;  // try-finally
end;


function TEmailDlg.SendLevel(cimzettlista: TStringList; masolat, targy, szoveg: string; kellfelado: boolean; feladoneve: string;
                          MellekletLista: TMemoryStreamPointerArray; MellekletNevek, MellekletTipusok: TStringList;
                          MellekletStilus: string; ContentType: string = 'text/plain'): string;
const
  HTMLKezdet = '<div dir="ltr">';
  HTMLVegzet = '</div>';
var
  S, s_init, FN, LogFN, VoltHiba: string;
  IdMessage1: TIdMessage;
  torzs, LocalRecipientlist: TStringList;
  kiem_k, kiem_v: string;
  LocalSubject, Localbody, LocalCimzett, LocalMasolat: string;
  i: integer;

  function LocalSendMail (UseAttachments: boolean): string;
     var
        i: integer;
        MyStream: TMemoryStream;
        MyStreamPointer: TMemoryStreamPointer;
        // MyIdStream: TIdStream;
        idAttachement: TIdAttachmentMemory;
        MyS: string;
     begin
        VoltHiba:= '';
        torzs.Clear;
        if ContentType = 'text/html' then
          torzs.Add(HTMLKezdet + Localbody + HTMLVegzet)
        else torzs.Add(Localbody);

        // ----- ment�s file-ba -------- //
        LogFN:= ExtractFilePath(Application.ExeName);
        LogFN:= LogFN+'LOG\OutMail'+FormatDateTime('yyyymmdd_HHmmsszzz', Now)+'.txt';
        S:= 'T�rgy: '+LocalSubject+', C�mzett: ['+ LocalCimzett + '] '+torzs.Text;
        WriteLogFile(LogFN, S);
        // ----------------------------- //
        // try
          Mail(IdMessage1, LocalCimzett, LocalMasolat, LocalSubject, torzs, kellfelado, feladoneve);
          if UseAttachments and (Length(MellekletLista)>0) then begin
              if MellekletStilus = 'multipart/related' then begin  // be�gyazzuk a tartalmat
                S:= '';
                for i:=0 to Length(MellekletLista)-1 do begin
                  // S:= S+ ' <img src="cid:'+MellekletNevek[i]+'"> <br><br>';
                  // S:= S+ '<div> <img src="cid:'+MellekletNevek[i]+' "width="400" height="400"> <br><br></div>';
                  // S:= S+ '<div> <img src="cid:'+IntToStr(i)+' "width="400" height="400"> <br>'+MellekletNevek[i]+'<br><br></div>';
                  if MellekletTipusok[i] = const_JPGMIME then
                      S:= S+ '<div> <img src="cid:ATTACH'+IntToStr(i)+'" height="300"> <br>'+MellekletNevek[i]+'<br><br></div>';
                  if MellekletTipusok[i] = const_PDFMIME then
                      S:= S+ '<div> <embed src="cid:ATTACH'+IntToStr(i)+'" height="300" type='''+const_PDFMIME+'''> <br>Mell�klet: '+MellekletNevek[i]+'<br><br></div>';
                  end;  // for
                with TIdText.Create(IdMessage1.MessageParts, nil) do begin
                  Body.Text := HTMLKezdet+ Localbody +'<br><br>'+ S + HTMLVegzet;  // fel�l�rja a "torzs"-ben betett �rt�ket
                  ContentType := 'text/html';
                  end; // with
                end;  // if

              // IdMessage1.ContentType:= 'multipart/related';  // a be�gyazott mell�kletek miatt
              IdMessage1.ContentType:= MellekletStilus;
              for i:=0 to Length(MellekletLista)-1 do begin
               idAttachement:= TIdAttachmentMemory.Create(IdMessage1.MessageParts);
               // MyStreamPointer:= MellekletLista[i];
               // MyStream:= MyStreamPointer^;
               // debug...
               // MyStream.SaveToFile('D:\NagyP\Temp\x-2.jpg');  // debug
               // MyStream.SaveToFile('d:\NagyP\Temp\X\Attach'+IntToStr(i)+'.wmf');
               // (idAttachement.DataStream as TMemoryStream).LoadFromStream(MyStream);
               try
                // MellekletLista[i]^.SaveToFile('D:\NagyP\Temp\y'+IntToStr(i)+'_'+FormatDateTime('HHMMSSZZZ', now)+'.jpg');  // debug
                (idAttachement.DataStream as TMemoryStream).LoadFromStream(MellekletLista[i]^);
               except
                VoltHiba:= 'LoadFromStream error, i='+ IntToStr(i);
                end; // try - except
               IdAttachement.ContentID := 'ATTACH'+IntToStr(i);
               IdAttachement.ContentType := MellekletTipusok[i];
               IdAttachement.FileName := MellekletNevek[i];
               end;  // for
            end;  // if
          if (VoltHiba ='') then begin
              Result:= SendMail(IdMessage1);
              end
          else begin
              Result:= VoltHiba;
              end;
       { except
          on E: exception do begin
             Result:= E.Message;
             end;
          end;  // try-except
          }
     end;

begin
  s_init:= '';

  Result:= '';
  IdMessage1:=TIdMessage.Create(Self);
  IdMessage1.ContentType:=ContentType;
  IdMessage1.CharSet:= 'iso-8859-2';  // 'ISO-8859-2'; // 'windows-1250'; // 'UTF-8';

  IdMessage1.ContentTransferEncoding:= 'quoted-printable';
  torzs:= TStringList.Create;
  try
    if EgyebDlg.SMS_ROUTING_HASZNALATA then begin  // "�j" m�d: k�l�n email �s jssms.hu k�ld�s
        LocalRecipientlist:= TStringList.Create;
        try
          for i:=0 to cimzettlista.Count-1 do
              if Pos(EgyebDlg.SMSEAGLE_DOMAIN, cimzettlista[i])=0 then  // ami nem SMS ir�nyba megy
                LocalRecipientlist.Add(cimzettlista[i]);
          if LocalRecipientlist.Count > 0 then begin
             LocalSubject:= targy;
             Localbody:= szoveg;
             LocalCimzett:= StringListToSepString(LocalRecipientlist, ',', false);
             LocalMasolat:= masolat;
             Result:= LocalSendMail (True);
             end;  // ha van c�mzett
          LocalRecipientlist.Clear;
          for i:=0 to cimzettlista.Count-1 do
            if Pos(EgyebDlg.SMSEAGLE_DOMAIN, cimzettlista[i])>0 then  // ami SMS ir�nyba megy
              LocalRecipientlist.Add(cimzettlista[i]);
          if LocalRecipientlist.Count > 0 then begin
            LocalSubject:= 'modemno=2';  // innen csak technikai �zenetet k�ld�nk
            Localbody:= targy+' '+szoveg;
            LocalCimzett:= StringListToSepString(LocalRecipientlist, ',', false);
            LocalMasolat:= '';  // ne kapj�k meg m�g egyszer
            Result:= LocalSendMail (False);
            end;  // ha van c�mzett
        finally
          if LocalRecipientlist <> nil then LocalRecipientlist.Free;
          end;  // try-finally
        end  // if "�j" m�d
   else begin  // "r�gi m�di" - nincs SMS routing
      Localbody:= szoveg;
      LocalCimzett:= StringListToSepString(cimzettlista, ',', false);
      LocalSubject:= targy;
      Result:= LocalSendMail (True);
      end; // else
  finally
     if IdMessage1 <> nil then IdMessage1.Free;
     if torzs <> nil then torzs.Free;
     end;  // finally
end;

{ 2017-02-17-ig, Attachment el�tt
function TEmailDlg.SendLevel(cimzettlista: TStringList; masolat, targy, szoveg: string; kellfelado: boolean; feladoneve: string): string;
var
  S, s_init: string;
  IdMessage1: TIdMessage;
  torzs, LocalRecipientlist: TStringList;
  kiem_k, kiem_v: string;
  LocalSubject, Localbody, LocalCimzett, LocalMasolat: string;
  i: integer;

  procedure LocalSendMail;
     begin
        torzs.Clear;
        torzs.Add(Localbody);
        try
          Mail(IdMessage1, LocalCimzett, LocalMasolat, LocalSubject, torzs, kellfelado, feladoneve);
          if SendMail(IdMessage1) then Result:=''
          else Result:= 'Hiba a k�ld�s sor�n';
        except
          on E: exception do begin
             Result:= E.Message;
             end;
          end;  // try-except
     end;

begin
  s_init:= '';

  Result:= '';
  IdMessage1:=TIdMessage.Create(Self);
  IdMessage1.ContentType:='text/plain';
  IdMessage1.CharSet:= 'iso-8859-2';  // 'ISO-8859-2'; // 'windows-1250'; // 'UTF-8';

  IdMessage1.ContentTransferEncoding:= 'quoted-printable';
  torzs  :=TStringList.Create;
  try
    if EgyebDlg.SMS_ROUTING_HASZNALATA then begin  // "�j" m�d: k�l�n email �s jssms.hu k�ld�s
        LocalRecipientlist:= TStringList.Create;
        try
          for i:=0 to cimzettlista.Count-1 do
              if Pos(EgyebDlg.SMSEAGLE_DOMAIN, cimzettlista[i])=0 then  // ami nem SMS ir�nyba megy
                LocalRecipientlist.Add(cimzettlista[i]);
          if LocalRecipientlist.Count > 0 then begin
             LocalSubject:= targy;
             Localbody:= szoveg;
             LocalCimzett:= StringListToSepString(LocalRecipientlist, ',', false);
             LocalMasolat:= masolat;
             LocalSendMail;
             end;  // ha van c�mzett
          LocalRecipientlist.Clear;
          for i:=0 to cimzettlista.Count-1 do
            if Pos(EgyebDlg.SMSEAGLE_DOMAIN, cimzettlista[i])>0 then  // ami SMS ir�nyba megy
              LocalRecipientlist.Add(cimzettlista[i]);
          if LocalRecipientlist.Count > 0 then begin
            LocalSubject:= 'modemno=2';  // innen csak technikai �zenetet k�ld�nk
            Localbody:= targy+' '+szoveg;
            LocalCimzett:= StringListToSepString(LocalRecipientlist, ',', false);
            LocalMasolat:= '';  // ne kapj�k meg m�g egyszer
            LocalSendMail;
            end;  // ha van c�mzett
        finally
          if LocalRecipientlist <> nil then LocalRecipientlist.Free;
          end;  // try-finally
        end  // if "�j" m�d
   else begin  // "r�gi m�di" - nincs SMS routing
      Localbody:= szoveg;
      LocalCimzett:= StringListToSepString(cimzettlista, ',', false);
      LocalSubject:= targy;
      LocalSendMail;
      end; // else
  finally
     if IdMessage1 <> nil then IdMessage1.Free;
     if torzs <> nil then torzs.Free;
     end;  // finally
end;
}

{ 2016-12-14 ig, az SMSRouting el�tt
function TForm1.SendLevel(cimzett, masolat, targy, szoveg: string; kellfelado: boolean; feladoneve: string): string;
var
  S, s_init: string;
  IdMessage1: TIdMessage;
  torzs: TStringList;
  kiem_k, kiem_v: string;
begin
  // s_init:='<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-2">';
  // s_init:='<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">';
  s_init:= '';
  // kiem_k:='<strong>';
  // kiem_v:='</strong>';
  Result:= '';
  IdMessage1:=TIdMessage.Create(Self);
  IdMessage1.ContentType:='text/plain';
  IdMessage1.CharSet:= 'iso-8859-2';  // 'ISO-8859-2'; // 'windows-1250'; // 'UTF-8';
  // Content-Language: hu-HU
  IdMessage1.ContentTransferEncoding:= 'quoted-printable';
  torzs  :=TStringList.Create;
  try
    try
      // torzs.Add(s_init);
      // torzs.Add(kiem_k+ ' FIGYELEM! '+kiem_v);
      torzs.Add(szoveg);
      // torzs.Add(StringEncodeAsUTF8(szoveg));
      Mail(IdMessage1, cimzett, masolat, targy, torzs, kellfelado, feladoneve);
      if SendMail(IdMessage1) then Result:=''
      else Result:= 'Hiba a k�ld�s sor�n';
    except
      on E: exception do begin
         Result:= E.Message;
         end;
      end;  // try-except
  finally
     if IdMessage1 <> nil then IdMessage1.Free;
     if torzs <> nil then torzs.Free;
     end;  // finally
end;
}

{ Ez j� volt a GMAIL-ben
function TForm1.SendLevel(cimzett, masolat, targy, szoveg: string; kellfelado: boolean; feladoneve: string): string;
var
  S, s_init: string;
  IdMessage1: TIdMessage;
  torzs: TStringList;
  kiem_k, kiem_v: string;
begin
  // s_init:='<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-2">';
  // s_init:='<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">';
  s_init:= '';
  // kiem_k:='<strong>';
  // kiem_v:='</strong>';
  Result:= '';
  IdMessage1:=TIdMessage.Create(Self);
  IdMessage1.ContentType:='text/html';
  IdMessage1.CharSet:= 'UTF-8';  // 'ISO-8859-2'; // 'windows-1250'; // 'UTF-8';
  torzs  :=TStringList.Create;
  try
    try
      // torzs.Add(s_init);
      // torzs.Add(kiem_k+ ' FIGYELEM! '+kiem_v);
      torzs.Add(szoveg);
      // torzs.Add(StringEncodeAsUTF8(szoveg));
      Mail(IdMessage1, cimzett, masolat, targy, torzs, kellfelado, feladoneve);
      if SendMail(IdMessage1) then Result:=''
      else Result:= 'Hiba a k�ld�s sor�n';
    except
      on E: exception do begin
         Result:= E.Message;
         end;
      end;  // try-except
  finally
     if IdMessage1 <> nil then IdMessage1.Free;
     if torzs <> nil then torzs.Free;
     end;  // finally
end;
}

procedure TEmailDlg.SendSimaIrodaiLevel(const Cimzett, Targy, Szoveg, logfn: string);
var
   HibaHely: string;
   IdMessage1: TIdMessage;
   torzs: TStringList;
   Res: string;
begin
  IdMessage1:=TIdMessage.Create(Self);
  torzs:=TStringList.Create;
  try
   try
    HibaHely:='1';
    IdMessage1.ContentType:='text/html';
    IdMessage1.CharSet:= 'UTF-8';
    HibaHely:='2';
    torzs.Add(Szoveg);
    HibaHely:='3';
    Mail(IdMessage1, Cimzett, '', Targy, torzs, True,'Fuvaros �zenetk�zpont');
    // torzs.Add('** Kinek: '+IrodaiCC);
    //Mail(IdMessage1, 'nagy.peter@js.hu', '', 'Szerviz: '+rendszam+' jav�t�sa elk�sz�lt', torzs, True,'Fuvaros �zenetk�zpont');
    HibaHely:='4';
    Res:= SendMail(IdMessage1);
    if Res = '' then
      if logfn<>'' then WriteLogFile(logfn,'Irodai lev�l kik�ldve: ' + Cimzett)
    else
      if logfn<>'' then WriteLogFile(logfn,'!!! Irodai lev�l k�ld�s sikertelen ('+Res+'): ' + Cimzett);
   except
      on E: Exception do begin
          if logfn<>'' then WriteLogFile(logfn,'Hiba az irodai lev�l k�ld�se sor�n ('+HibaHely+'): '+E.Message);
          end;  // on E
     end;  // try-except
  finally
      if IdMessage1 <> nil then IdMessage1.Free;
      if torzs <> nil then torzs.Free;
      end;  // try-finally
end;




procedure TEmailDlg.SendIrodaiLevel(Rendszam, Szoveg, LogFN: string);
var
   IrodaiCC, GKKateg, HibaHely: string;
   IdMessage1: TIdMessage;
   torzs: TStringList;
begin
   GKKateg:= Query_Select('GEPKOCSI', 'GK_RESZ', Rendszam, 'GK_GKKAT');
   // IrodaiCC:= Trim(Szotar_olvaso('378',GKKateg));
   IrodaiCC:= Trim(EgyebDlg.Read_SZGrid('378',GKKateg));
   if IrodaiCC <> '' then begin
      SendSimaIrodaiLevel(IrodaiCC, 'Szerviz: '+rendszam+' jav�t�sa elk�sz�lt', Szoveg, LogFN);
      end;  // if
end;

function TEmailDlg.WriteLogFile(FName, Text: string): Boolean;
var
  LogFile: TextFile;
begin
  // prepares log file
  AssignFile (LogFile, Fname);
  if FileExists (FName) then
    Append (LogFile) // open existing file
  else
    Rewrite (LogFile); // create a new one

  // write to the file and show error
  Writeln (LogFile,Text);

  // close the file
  CloseFile (LogFile);
  Result:=True;
end;

end.
