﻿unit J_Email;
{******************************************************************************
   Levél küldése.
*******************************************************************************}

interface

uses
   IdMessage, IdSMTPBase, IdSMTP, IdAttachment, IdAttachmentFile, Classes, SysUtils, Variants,  Vcl.ExtCtrls,
   IdExplicitTLSClientServerBase, IdSSLOpenSSL;

type

   TJEmail = Class (TComponent)
       public
      	constructor Create(AOwner: TComponent); override;
    	destructor 	Destroy; override;
       procedure   SetHostParams(hn, us, pw : string; hp : integer = 25);
       procedure   SetEmailParams(fromst, tou, cc, bcc, sub : string; bod : TStrings );
       procedure   SetSSLParams(sslpo : integer; usess : boolean );
       function    GetResult : string;
       function    SendMessage : boolean;
       function    AddFile(filename : string) : boolean;
       function    RemoveFile(filename : string) : boolean;
       private
           host            : string;
           port            : integer;
           pswd            : string;
           user            : string;
           fromstr         : string;
           Reclist         : string;
           CCList          : string;
           BCCList         : string;
           Subject         : string;
           body            : TStrings;
           resultstr       : string;
           usessl          : boolean;
           sslport         : integer;
           filelist        : TStringList;
   end;

implementation

{******************************************************************************
Eljárás neve  :  Create
Leírás        :  Objektum létrehozása
Paraméterek   :  AOwner - szülő objektum neve
*******************************************************************************}
constructor TJEmail.Create(AOwner: TComponent);
begin
   Inherited Create(AOwner);
   host            := '';
   port            := 0;
   pswd            := '';
   user            := '';
   fromstr         := '';
   Reclist         := '';
   CCList          := '';
   BCCList         := '';
   Subject         := '';
   body            := nil;
   usessl          := false;
   sslport         := 0;
   resultstr       := '';
   filelist        := TStringList.Create;
end;

{******************************************************************************
Eljárás neve  :  Destroy
Leírás        :  Objektum megszüntetése
Paraméterek   :  -
*******************************************************************************}
destructor TJEmail.Destroy;
begin
	// Itt jönnek a megszüntetések
   filelist.Destroy;
  	inherited Destroy;
end;

procedure   TJEmail.SetHostParams(hn, us, pw : string; hp : integer = 25);
begin
   // A szerverparaméterek beállítása
   host            := hn;
   port            := hp;
   pswd            := pw;
   user            := us;
end;

procedure   TJEmail.SetEmailParams(fromst, tou, cc, bcc, sub : string; bod : TStrings );
begin
   // A levél attribútumok beállítása
   fromstr         := fromst;
   Reclist         := tou;
   CCList          := cc;
   BCCList         := bcc;
   Subject         := sub;
   body            := bod;
end;

function TJEmail.GetResult : string;
begin
    result := resultstr;
end;

procedure TJEmail.SetSSLParams(sslpo : integer; usess : boolean );
begin
   sslport := sslpo;
   usessl  := usess;
end;

function   TJEmail.SendMessage : boolean;
var
   idSMTP1     : TIdSMTP;
   IdMessage1  : TIdMessage;
   IdSSLIOHandlerSocketOpenSSL1    : TIdSSLIOHandlerSocketOpenSSL;
//   Attachment  : TIdAttachment;
   i           : integer;
begin
   // A levél elküldése
   Result      := false;
   resultstr   := '';
   // SSL IoHendler létrehozása
   if usessl then begin
       try
           IdSSLIOHandlerSocketOpenSSL1                := TIdSSLIOHandlerSocketOpenSSL.Create(Self);
           IdSSLIOHandlerSocketOpenSSL1.Destination    := host + ':'+IntToStr(sslport);
           IdSSLIOHandlerSocketOpenSSL1.Host           := host;
           IdSSLIOHandlerSocketOpenSSL1.Port           := sslport;
       except
           on E: Exception do begin
               resultstr := 'SSLHandler error : '+E.Message;
               Exit;
           end;
       end;
   end;

   // Kapcsolódás a szerverhez
   try
       IdSMTP1 := TIdSMTP.Create(Self);
       IdSMTP1.Disconnect(false);
       IdSMTP1.Host        := host;
       IdSMTP1.Port        := port;
       if user <> '' then begin
           IdSMTP1.Username    := user;
       end;
       if pswd <> '' then begin
           IdSMTP1.Password    := pswd;
       end;
       if usessl then begin
           idSMTP1.IOHandler   := IdSSLIOHandlerSocketOpenSSL1;
           idSMTP1.UseTLS      := utUseRequireTLS;
       end;
       IdSMTP1.Connect;
   except
       on E: Exception do begin
		    resultstr := 'Host error : '+E.Message;
           Exit;
       end;
   end;

   // A levél tényleges elküldése
   try
       IdMessage1  := TIdMessage.Create(Self);
//       IdMessage1.CharSet                    := 'Central Europe';
       IdMessage1.From.Address                 := fromstr;
       IdMessage1.Recipients.EMailAddresses    := reclist;
       IdMessage1.CCList.EMailAddresses        := cclist;
       IdMessage1.BCCList.EMailAddresses       := bcclist;
       IdMessage1.Subject                      := subject;
       IdMessage1.Body                         := body;
       // A csatolmányok hozzáfűzése
       if filelist.Count > 0 then begin
           for i := 0 to filelist.Count - 1 do begin
               TIdAttachmentFile.Create( IdMessage1.MessageParts, filelist[i]);
//               Attachment  := TIdAttachment.Create(IdMessage1.MessageParts);
//               Attachment.FileName := filelist[i];
           end;
       end;
       IdSMTP1.Send(IdMessage1);
   except
       on E: Exception do begin
		    resultstr := 'Email sending error : ' + E.Message;
           IdSMTP1.Disconnect;
           Exit;
       end;
   end;
   IdSMTP1.Disconnect;
   Result  := true;
end;

function TJEmail.AddFile(filename : string) : boolean;
begin
    // Új csatolmány hozzáadása
    Result := false;
    if not FileExists(filename) then begin
        Exit;
    end;
    filelist.Add(filename);
    Result := true;
end;

function TJEmail.RemoveFile(filename : string) : boolean;
var
   pozic   : integer ;
begin
    // attachment megszüntetése
    Result := false;
    pozic  := filelist.IndexOf(filename);
    if pozic > -1 then begin
       filelist.Delete(pozic);
       Result  := true;
    end;
end;


end.
