unit Menetlev;

interface

uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls, Egyeb,
  Forms, Dialogs, DBTables, DB, StdCtrls, Buttons, Mask, DBCtrls,DateUtils,
  ExtCtrls;

type
  TMenetlevDlg = class(TForm)
    BitBtn4: TBitBtn;
    BitBtn6: TBitBtn;
    Label3: TLabel;
    M0: TMaskEdit;
    BitBtn1: TBitBtn;
    M1: TMaskEdit;
    Label1: TLabel;
    BitBtn2: TBitBtn;
    Label2: TLabel;
    M2: TMaskEdit;
    BitBtn3: TBitBtn;
    procedure FormCreate(Sender: TObject);
    procedure BitBtn4Click(Sender: TObject);
    procedure BitBtn6Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure M1Exit(Sender: TObject);
    procedure M2Exit(Sender: TObject);
  private
  public
  end;

var
  MenetlevDlg: TMenetlevDlg;

implementation

uses
	J_VALASZTO, Menetlis, Kozos, Kozos_Local;
{$R *.DFM}

procedure TMenetlevDlg.FormCreate(Sender: TObject);
begin
 	M0.Text := '';
end;

procedure TMenetlevDlg.BitBtn4Click(Sender: TObject);
begin
	{A lista �ssze�ll�t�sa a sz�r�si elemek alapj�n}
   Screen.Cursor	:= crHourGlass;
  	Application.CreateForm(TMenetlisDlg, MenetlisDlg);
  	MenetlisDlg.Tolt(M0.Text, M1.Text, M2.Text);
   Screen.Cursor	:= crDefault;
  	if MenetlisDlg.vanadat	then begin
     	MenetlisDlg.Rep.Preview;
  	end else begin
		NoticeKi('Nincs a megadott felt�teleknek megfelel� t�tel!');
  	end;
  	MenetlisDlg.Destroy;
end;

procedure TMenetlevDlg.BitBtn6Click(Sender: TObject);
begin
	Close;
end;

procedure TMenetlevDlg.BitBtn1Click(Sender: TObject);
begin
	GepkocsiValaszto(M0, GK_TRAKTOR);
end;

procedure TMenetlevDlg.BitBtn2Click(Sender: TObject);
begin
	// EgyebDlg.Calendarbe(M1);
  // EgyebDlg.ElsoNapEllenor(M1, M2);
  EgyebDlg.CalendarBe_idoszak(M1, M2);
end;

procedure TMenetlevDlg.BitBtn3Click(Sender: TObject);
begin
	EgyebDlg.Calendarbe(M2);
  EgyebDlg.UtolsoNap(M2);
  // M2.Text:=DateToStr(EndOfTheMonth(StrToDate(M2.Text)));
end;

procedure TMenetlevDlg.M1Exit(Sender: TObject);
begin
	if ( ( M1.Text <> '' ) and ( not Jodatum2(M1 ) ) ) then begin
		NoticeKi('Hib�s d�tum megad�sa!');
		M1.Text := '';
		M1.Setfocus;
	end;
  EgyebDlg.ElsoNapEllenor(M1, M2);
end;

procedure TMenetlevDlg.M2Exit(Sender: TObject);
begin
	if ( ( M2.Text <> '' ) and ( not Jodatum2(M2 ) ) ) then begin
		NoticeKi('Hib�s d�tum megad�sa!');
		M2.Text := '';
		M2.Setfocus;
	end;
end;

end.


