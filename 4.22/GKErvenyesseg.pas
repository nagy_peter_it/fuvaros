unit GKErvenyesseg;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, Buttons, DB, ADODB;

type
  TFGKErvenyesseg = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    Memo1: TMemo;
    BitKilep: TBitBtn;
    Query1: TADOQuery;
    Label1: TLabel;
    Query1ga_datum: TStringField;
    Query1sz_menev: TStringField;
    procedure BitKilepClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    function Ervenyesseg(rendszam:string; nap,pnap:integer):boolean;
  end;

var
  FGKErvenyesseg: TFGKErvenyesseg;

implementation

uses Egyeb,J_SQL;

{$R *.dfm}

{ TFGKErvenyesseg }

function TFGKErvenyesseg.Ervenyesseg(rendszam: string; nap,pnap:integer): boolean;
var
  hiba:integer;
begin
  if rendszam=EmptyStr then
  begin
    Result:=False;
    exit;
  end;
  if Query_Select('GEPKOCSI','GK_RESZ',rendszam,'GK_ARHIV') = '1' then
  begin
    Result:=False;
    exit;
  end;

  if nap<0 then nap:=0;
  Label1.Caption:=rendszam;
  hiba:=0;
  Memo1.Lines.Clear;
  Memo1.Lines.Add('Ellen�rz�si id�szak: '+DateToStr(date)+' - '+DateToStr(date+nap)+' + '+IntToStr(pnap)+'nap');
 // Memo1.Lines.Add('Ma  : '+DateToStr(date));
 // Memo1.Lines.Add('V�ge: '+DateToStr(date+nap));
  Memo1.Lines.Add('-----------------------------------------------------------------');
  Memo1.Lines.Add('');
  Query1.Close;
  Query1.Parameters[0].Value:=rendszam;
  Query1.Open;
  Query1.First;
  while not Query1.Eof do
  begin
    if date >= Query1GA_DATUM.AsDateTime then
    begin
      Memo1.Lines.Add(Query1GA_DATUM.Value+' !!! '+Query1sz_menev.Value);
      inc(hiba);
    end
    else
    begin
      if (date+nap+pnap) >= Query1GA_DATUM.AsDateTime then
      begin
        Memo1.Lines.Add(Query1GA_DATUM.Value+'     '+Query1sz_menev.Value);
        inc(hiba);
      end;
    end;
    Query1.Next;
  end;
  Query1.Close;
  Result:= hiba<>0;

end;

procedure TFGKErvenyesseg.BitKilepClick(Sender: TObject);
begin
  close;
end;

end.
