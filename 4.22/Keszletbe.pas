unit Keszletbe;

interface

uses
	SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
	Forms, Dialogs, StdCtrls, Buttons, Mask, DB, DBTables, J_ALFORM, ADODB,
  ExtCtrls;

type
	TKeszletbeDlg = class(TJ_AlformDlg)
	Label2: TLabel;
	Label7: TLabel;
    M3: TMaskEdit;
    M2: TMaskEdit;
	Label8: TLabel;
	Label9: TLabel;
	Label12: TLabel;
    M1: TMaskEdit;
    CB1: TComboBox;
    BitBtn7: TBitBtn;
    Query1: TADOQuery;
    Label1: TLabel;
    M5: TMaskEdit;
    Label3: TLabel;
    CB2: TComboBox;
    Label4: TLabel;
    CB3: TComboBox;
    Label5: TLabel;
    M4: TMaskEdit;
    Label6: TLabel;
    Panel1: TPanel;
    BitElkuld: TBitBtn;
    BitKilep: TBitBtn;
    Label10: TLabel;
    BitBtn1: TBitBtn;
    TTermek: TADOQuery;
    TTermekT_TERKOD: TStringField;
    TTermekT_TERNEV: TStringField;
    TTermekT_LEIRAS: TStringField;
    TTermekT_CIKKSZAM: TStringField;
    TTermekT_ITJSZJ: TStringField;
    TTermekT_AFA: TStringField;
    TTermekT_EGYAR: TBCDField;
    TTermekT_MEGJ: TStringField;
    TTermekT_ME: TStringField;
    TTermekT_KKEZ: TIntegerField;
    TTermekT_KESZLET: TBCDField;
    TSZOTAR: TADOQuery;
    TSZOTARSZ_FOKOD: TStringField;
    TSZOTARSZ_ALKOD: TStringField;
    TSZOTARSZ_MENEV: TStringField;
    TSZOTARSZ_LEIRO: TStringField;
    TSZOTARSZ_KODHO: TBCDField;
    TSZOTARSZ_NEVHO: TBCDField;
    TSZOTARSZ_ERVNY: TIntegerField;
    Edit1: TEdit;
    Edit2: TEdit;
    SQL: TADOCommand;
    CB5: TComboBox;
    Label11: TLabel;
    Query2: TADOQuery;
    Query2K_RAKKOD: TStringField;
    Query2K_RAKNEV: TStringField;
    Query2K_TKOD: TStringField;
    Query2K_TNEV: TStringField;
    Query2K_TME: TStringField;
    Query2K_KESZLET: TBCDField;
	procedure BitKilepClick(Sender: TObject);
	procedure FormCreate(Sender: TObject);
	procedure Tolto(cim : string; teko : string);override;
	procedure BitElkuldClick(Sender: TObject);
  procedure MaskEditClick(Sender: TObject);
  procedure MaskEditExit(Sender: TObject);
	procedure MaskEditKeyPress(Sender: TObject; var Key: Char);
    procedure Modosit(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure M1Exit(Sender: TObject);
    procedure BitBtn7Click(Sender: TObject);
    procedure CB1Change(Sender: TObject);
    procedure CB3Change(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure CB2Change(Sender: TObject);
    procedure M2Click(Sender: TObject);
	private
     modosult 	: boolean;
    procedure keszletki;

	public
		afalist		: TStringList;
     ret_arfoly	: string;
	end;

var
	KeszletbeDlg: TKeszletbeDlg;
  _rkod, _rnev, _mkod, _mnev: TStringList;

implementation

uses
	Egyeb, J_SQL, Kozos, StrUtils,J_VALASZTO;
{$R *.DFM}


procedure TKeszletbeDlg.BitKilepClick(Sender: TObject);
begin
	if modosult then begin
  		if NoticeKi('M�dosultak az adatok . Ki akar l�pni ment�s n�lk�l?', NOT_QUESTION ) = 0 then begin
			ret_kod := '';
			Close;
     	end;
  	end else begin
		ret_kod := '';
		Close;
  	end;
end;

procedure TKeszletbeDlg.FormCreate(Sender: TObject);
begin
// 	EgyebDlg.SetADOQueryDatabase(Query1);
	ret_kod 	:= '';
  modosult 	:= false;
  //afalist 	:= TStringList.Create;
  ///////////////////////
  TSZOTAR.Close;
  TSZOTAR.Parameters[0].Value:='140';  //rakt�rak
  TSZOTAR.Open;
  TSZOTAR.last;
  TSZOTAR.First;
  _rkod:=TStringList.Create;
  _rnev:=TStringList.Create;
  while not TSZOTAR.Eof do
  begin
    _rkod.Add(TSZOTARSZ_ALKOD.Value);
    _rnev.add(TSZOTARSZ_MENEV.Value);
    CB1.Items.Add(TSZOTARSZ_MENEV.Value) ;
    TSZOTAR.Next;
  end;
  if CB1.Items.Count=1 then
  begin
    CB1.ItemIndex:=0;
    CB1.OnChange(self);
  end;
  /////////////////
  TSZOTAR.Close;
  TSZOTAR.Parameters[0].Value:='141';  //mozg�snem
  TSZOTAR.Open;
  TSZOTAR.last;
  _mkod:=TStringList.Create;
  _mnev:=TStringList.Create;
  TSZOTAR.First;
  while not TSZOTAR.Eof do
  begin
    _mkod.Add(TSZOTARSZ_ALKOD.Value);
    _mnev.Add(TSZOTARSZ_MENEV.Value);
    CB3.Items.Add(TSZOTARSZ_MENEV.Value);
    TSZOTAR.Next;
  end;
  CB3.ItemIndex:=0;
  CB3.OnChange(self);
  TSZOTAR.Close;
  ////////////////
  TTermek.Close;
  TTermek.Open;
  TTermek.First;
  while not TTermek.Eof do
  begin
    CB2.Items.add(TTermekT_TERNEV.Value);
    TTermek.Next;
  end;
  TTermek.Close;
  if CB2.Items.Count=1 then
  begin
    CB2.ItemIndex:=0;
    CB2.OnChange(self);
  end;
  ////////////////
  modosult:=False;
end;

procedure TKeszletbeDlg.Tolto(cim : string; teko:string);
begin
	ret_kod 	:= teko;
	Caption := cim;
 //	M1.Text := EgyebDlg.MaiDatum;
	if ret_kod <> '' then begin		{M�dos�t�s}
		if Query_Run (Query1, 'SELECT * FROM KMOZGAS WHERE KM_ID = '+teko,true) then begin
			M1.Text 	:= Query1.FieldByName('KM_DATUM').AsString;
			M2.Text 	:= Query1.FieldByName('KM_GKRESZ').AsString;
 			M3.Text 	:= IntToStr( Query1.FieldByName('KM_KMORA').AsInteger);
			M4.Text 	:= SzamString(Query1.FieldByName('KM_MENNY').AsFloat,12,2);
			M5.Text 	:= Query1.FieldByName('KM_MEGJ').AsString;
			Label10.Caption 	:= Query1.FieldByName('KM_KIBE').AsString;
			Label6.Caption 	:= Query1.FieldByName('KM_ME').AsString;
			Edit1.Text 	:= Query1.FieldByName('KM_RAKKOD').AsString;
			Edit2.Text 	:= Query1.FieldByName('KM_TKOD').AsString;
      ComboSetSzoveg(CB1,Query1.FieldByName('KM_RAKNEV').AsString);
      ComboSetSzoveg(CB2,Query1.FieldByName('KM_TNEV').AsString);
      ComboSetSzoveg(CB3,Query1.FieldByName('KM_MNEM').AsString);
 //			CB1.Text 	:= Query1.FieldByName('KM_RAKNEV').AsString;
 //			CB2.Text 	:= Query1.FieldByName('KM_TNEV').AsString;
 //			CB3.Text 	:= Query1.FieldByName('KM_MNEM').AsString;
      keszletki;
		end;
	end;
  CB1.Enabled:=ret_kod='';
  CB2.Enabled:=ret_kod='';
  CB3.Enabled:=ret_kod='';
  modosult 	:= false;
end;

procedure TKeszletbeDlg.BitElkuldClick(Sender: TObject);
var
  ujkodszam 	: integer;
  gkkat,gkeurob, elojel: string;
  S: string;
begin
	if not Jodatum2(M1 ) then begin
		NoticeKi('Hib�s d�tum megad�sa!');
		M1.Text := '';
		M1.Setfocus;
		Exit;
	end;
	if CB1.Text='' then begin
		NoticeKi('A rakt�rat meg kell adni!');
		CB1.Setfocus;
    CB1.DroppedDown:=True;
		Exit;
	end;
	if CB2.Text='' then begin
		NoticeKi('A term�ket meg kell adni!');
		CB2.Setfocus;
    CB2.DroppedDown:=True;
		Exit;
	end;
	if (CB3.Text='Kiv�t') and  (M2.Text='') then begin
		NoticeKi('A rendsz�mot meg kell adni!');
		M2.Setfocus;
		Exit;
	end;
	if M4.Text='' then begin
		NoticeKi('A mennyis�get meg kell adni!');
		M4.Setfocus;
		Exit;
	end;
  //////////////
  gkkat:='';
  gkeurob:='';
  if M2.Text<>'' then
  begin
    gkkat:=Query_Select('GEPKOCSI','GK_RESZ',M2.Text,'GK_GKKAT');
    gkeurob:=Query_Select('GEPKOCSI','GK_RESZ',M2.Text,'GK_EUROB');
  end;
  elojel:='1*';
  if Label10.Caption='KI' then elojel:='-1*';
	if ret_kod = '' then begin
		//�j rekord felvitele

		//SQL.CommandText:='INSERT INTO KMOZGAS (KM_DATUM,KM_RAKKOD,KM_RAKNEV,KM_TKOD,KM_TNEV,KM_GKRESZ,KM_GKKAT,KM_GKEUROB,KM_KMORA,KM_MENNY,KM_EMENNY,KM_ME,KM_MEGJ,KM_MNEM,KM_KIBE,KM_MODDAT,KM_USER) VALUES (' +
    // ''''+M1.Text+''','''+Edit1.Text+''','''+CB1.text+''','''+Edit2.Text+''','''+CB2.Text+''','''+M2.Text+''','''+gkkat+''','''+gkeurob+''','+IntToStr(StrToIntDef(M3.Text,0))+','+
    // Trim(SqlSzamString(StringSzam(M4.Text), 10, 2))+','+elojel+Trim(SqlSzamString(StringSzam(M4.Text), 10, 2))+','''+Label6.Caption+''','''+M5.Text+''','''+CB3.Text+''','''+Label10.Caption+''',getdate(),'''+EgyebDlg.user_name+'''' +' )';
    // SQL.Execute;
    S:= 'INSERT INTO KMOZGAS (KM_DATUM,KM_RAKKOD,KM_RAKNEV,KM_TKOD,KM_TNEV,KM_GKRESZ,KM_GKKAT,KM_GKEUROB,KM_KMORA,KM_MENNY,KM_EMENNY,KM_ME,KM_MEGJ,KM_MNEM,KM_KIBE,KM_MODDAT,KM_USER) VALUES (' +
    ''''+M1.Text+''','''+Edit1.Text+''','''+CB1.text+''','''+Edit2.Text+''','''+CB2.Text+''','''+M2.Text+''','''+gkkat+''','''+gkeurob+''','+IntToStr(StrToIntDef(M3.Text,0))+','+
    Trim(SqlSzamString(StringSzam(M4.Text), 10, 2))+','+elojel+Trim(SqlSzamString(StringSzam(M4.Text), 10, 2))+','''+Label6.Caption+''','''+M5.Text+''','''+CB3.Text+''','''+Label10.Caption+''',getdate(),'''+EgyebDlg.user_name+'''' +' )';
    Command_Run(SQL, S, true);

//    modosult:=False;
//    close;

  end
  else
  begin
    // SQL.CommandText:='UPDATE KMOZGAS set '+
    S:='UPDATE KMOZGAS set '+
    'KM_DATUM='''+M1.Text+''','+
    'KM_RAKKOD='''+Edit1.Text+''','+
    'KM_RAKNEV='''+CB1.Text+''','+
    'KM_TKOD='''+Edit2.Text+''','+
    'KM_TNEV='''+CB2.Text+''','+
    'KM_GKRESZ='''+M2.Text+''','+
    'KM_GKKAT='''+gkkat+''','+
    'KM_GKEUROB='''+gkeurob+''','+
    'KM_KMORA='+M3.Text+','+
    'KM_MENNY='+SqlSzamString(StringSzam(M4.Text), 10, 2)+','+
    'KM_EMENNY='+elojel+SqlSzamString(StringSzam(M4.Text), 10, 2)+','+
    'KM_ME='''+Label6.Caption+''','+
    'KM_MEGJ='''+M5.Text+''','+
    'KM_MNEM='''+CB3.Text+''','+
    'KM_KIBE='''+Label10.caption+''','+
    'KM_MODDAT=getdate()'+','+
    'KM_USER='''+EgyebDlg.user_name+''''+
    ' WHERE KM_ID = '+ret_kod;
    // SQL.Execute;
    Command_Run(SQL, S, true);
//    modosult:=False;
  //  close;
  end;
    modosult:=False;
	Query1.Close;
	Close;     
end;

procedure TKeszletbeDlg.MaskEditClick(Sender: TObject);
begin
	with Sender as TMaskEdit do begin
	SelectAll;
  end;
end;

procedure TKeszletbeDlg.MaskEditExit(Sender: TObject);
begin
	with Sender as TMaskEdit do begin
		Text := StrSzamStr(Text,12,2);
	end;
end;

procedure TKeszletbeDlg.MaskEditKeyPress(Sender: TObject; var Key: Char);
begin
  with Sender as TMaskEdit do begin
		Key := EgyebDlg.Jochar(Text,Key,12,2);
  end;
end;

procedure TKeszletbeDlg.Modosit(Sender: TObject);
begin
	modosult := true;
end;

procedure TKeszletbeDlg.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
	EgyebDlg.FormReturn(Key);
end;

procedure TKeszletbeDlg.M1Exit(Sender: TObject);
begin
	if ( ( M1.Text <> '' ) and ( not Jodatum2(M1 ) ) ) then begin
		NoticeKi('Hib�s d�tum megad�sa!');
		M1.Text := '';
		M1.Setfocus;
	end;
end;

procedure TKeszletbeDlg.BitBtn7Click(Sender: TObject);
begin
	EgyebDlg.Calendarbe(M1);
end;

procedure TKeszletbeDlg.CB1Change(Sender: TObject);
begin
	modosult := true;
  Edit1.Text:=_rkod[ _rnev.IndexOf(CB1.Text) ]  ;

end;

procedure TKeszletbeDlg.CB3Change(Sender: TObject);
begin
	modosult := true;
  if (StrToInt( (copy(_mkod[ _mnev.IndexOf(CB3.Text)],1,1)) ) mod 2)<>0 then
    Label10.Caption:='KI'
  else
    Label10.Caption:='BE'  ;

end;

procedure TKeszletbeDlg.BitBtn1Click(Sender: TObject);
begin
	GepkocsiValaszto(M2, GK_TRAKTOR);

end;

procedure TKeszletbeDlg.CB2Change(Sender: TObject);
begin
	modosult := true;
  Edit2.Text:= Query_Select('TERMEK','T_TERNEV',CB2.Text,'T_TERKOD') ;
  Label6.Caption:= Query_Select('TERMEK','T_TERNEV',CB2.Text,'T_ME') ;
  keszletki;
end;

procedure TKeszletbeDlg.keszletki;
var
  sql: string;
begin
  CB5.Items.Clear;
  Query2.Close;
  Query2.SQL.Clear;
  SQL:='select * from keszlet where (K_TKOD='''+Edit2.Text+''') and (K_RAKKOD='''+''')'       ;
  Query2.SQL.Add(sql)       ;
  Query2.Open  ;
  Query2.First;
  CB5.Items.Add('�sszesen:  '+FloatToStr(Query2K_KESZLET.Value)+' '+Query2K_TME.Value);
  ////////////
  Query2.Close;
  Query2.SQL.Clear;
  SQL:='select * from keszlet where (K_TKOD='''+Edit2.Text+''') and (K_RAKKOD<>'''+''')'       ;
  Query2.SQL.Add(sql)       ;
  Query2.Open  ;
  Query2.First;
  while not Query2.Eof do
  begin
    CB5.Items.Add(Query2K_RAKNEV.Value+':  '+FloatToStr(Query2K_KESZLET.Value)+' '+Query2K_TME.Value);
    Query2.Next;
  end;
  Query2.Close;
  CB5.ItemIndex:=0;
end;

procedure TKeszletbeDlg.M2Click(Sender: TObject);
begin
  BitBtn1.OnClick(self);
end;

end.
