{******************************************************************************}
{                                                                              }
{ CapDemoC                                                                     }
{                                                                              }
{ The contents of this file are subject to the Mozilla Public License Version  }
{ 1.0 (the "License"); you may not use this file except in compliance with the }
{ License. You may obtain a copy of the License at http://www.mozilla.org/MPL/ }
{                                                                              }
{ Software distributed under the License is distributed on an "AS IS" basis,   }
{ WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for }
{ the specific language governing rights and limitations under the License.    }
{                                                                              }
{ The Original Code is Main.pas and CapDemoC.dpr                               }
{                                                                              }
{ The Initial Developer of the Original Code is Peter J. Haas. Portions        }
{ created by Peter J. Haas are Copyright (C) 2000 Peter J. Haas. All Rights    }
{ Reserved.                                                                    }
{                                                                              }
{ Contributor(s):                                                              }
{                                                                              }
{                                                                              }
{ Contact:     (if possible you write in german language)                      }
{   EMail:     PeterJHaas@t-online.de                                          }
{   HomePage:  http://home.t-online.de/home/PeterJHaas/delphi.htm              }
{                                                                              }
{                                                                              }
{ Limitations:                                                                 }
{   Delphi 3 to 5                                                              }
{                                                                              }
{                                                                              }
{ History:                                                                     }
{   2000-12-07                                                                 }
{     - First public version                                                   }
{   2001-06-14                                                                 }
{     - video driver dialogs                                                   }
{     - status and error report                                                }
{     - check the real video format after SetVideoFormat                       }
{     - support for RGB 32, RGB 24, RGB 16, YUY2, UYVY, BTYUV (Y41P), Y8       }
{                                                                              }
{                                                                              }
{******************************************************************************}

{$ALIGN ON, $BOOLEVAL OFF, $LONGSTRINGS ON, $IOCHECKS ON, $WRITEABLECONST OFF}
{$OVERFLOWCHECKS OFF, $RANGECHECKS OFF, $TYPEDADDRESS ON, $MINENUMSIZE 1}
unit Main;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, VfW, ComCtrls, Menus, YUVConverts;

type
  TMainForm = class(TForm)
    Panel1: TPanel;
    BtnSequenceNoFile: TButton;
    BtnGrabFrameNoStop: TButton;
    SBtnFreeze: TSpeedButton;
    BtnSave: TButton;
    BtnCopy: TButton;
    SaveDialog1: TSaveDialog;
    LblTime: TLabel;
    PaintBoxFrame: TPaintBox;
    CBxFlip: TCheckBox;
    Label1: TLabel;
    EditFrameRate: TEdit;
    Label2: TLabel;
    UDFrameRate: TUpDown;
    Bevel1: TBevel;
    Label3: TLabel;
    GroupBox1: TGroupBox;
    RBtnPreview: TRadioButton;
    RBtnOverlay: TRadioButton;
    EditPreviewRate: TEdit;
    UDPreviewRate: TUpDown;
    LblPreviewRate: TLabel;
    MainMenu1: TMainMenu;
    MPntFile: TMenuItem;
    MPntExit: TMenuItem;
    MPntDialog: TMenuItem;
    MPntDlgVideoFormat: TMenuItem;
    MPntDlgVideoSource: TMenuItem;
    MPntDlgVideoDisplay: TMenuItem;
    MPntDlgCompression: TMenuItem;
    StatusBar1: TStatusBar;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    LblVideoFormatWidth: TLabel;
    LblVideoFormatHeight: TLabel;
    LblVideoFormatBitsPerPixel: TLabel;
    LblVideoFormatCompression: TLabel;
    Bevel2: TBevel;
    Label8: TLabel;
    Bevel3: TBevel;
    Label9: TLabel;
    Label10: TLabel;
    LblCapWinSize: TLabel;
    Label12: TLabel;
    LblCapWinScroll: TLabel;
    LblCapWinPreview: TLabel;
    LblCapWinOverlay: TLabel;
    LblCapWinScale: TLabel;
    Label11: TLabel;
    LblCapWinFrameNumber: TLabel;
    LblCapWinFrameDropped: TLabel;
    Label13: TLabel;
    BtnSequence: TButton;
    procedure FormDestroy(Sender: TObject);
    procedure SBtnFreezeClick(Sender: TObject);
    procedure BtnCopyClick(Sender: TObject);
    procedure BtnSaveClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormCreate(Sender: TObject);
    procedure BtnSequenceNoFileClick(Sender: TObject);
    procedure BtnGrabFrameNoStopClick(Sender: TObject);
    procedure UDPreviewRateClick(Sender: TObject; Button: TUDBtnType);
    procedure RBtnPreviewClick(Sender: TObject);
    procedure RBtnOverlayClick(Sender: TObject);
    procedure MPntDlgVideoDisplayClick(Sender: TObject);
    procedure MPntDialogClick(Sender: TObject);
    procedure MPntDlgVideoFormatClick(Sender: TObject);
    procedure MPntDlgVideoSourceClick(Sender: TObject);
    procedure MPntDlgCompressionClick(Sender: TObject);
    procedure MPntExitClick(Sender: TObject);
    procedure BtnSequenceClick(Sender: TObject);
  private
    { Private-Deklarationen }
    FCapWnd        : HWnd;             // AVICapture window handle
    FDriverCaps    : TCAPDRIVERCAPS;   // Driver caps
    FCapStatus     : TCAPSTATUS;       // Capture status

    FCaptureBIHSize : Integer;            // Size of capture bmp format info
    FCaptureBIH     : PBitmapInfoHeader;  // capture bmp format info
    FStreamBIHSize  : Integer;            // Size of stream bmp format info
    FStreamBIH      : PBitmapInfoHeader;  // stream bmp format info
    FFrameStream    : TMemoryStream;      // stream for read a frame
    FPixelPointer   : PByte;              // Pointer to pixel data in FFrameStream
    FVideoCodec     : TVideoCodec;
    FFrameBitmap    : TBitmap;            // bitmap contain the last frame

    procedure WMMove(var Message: TWMMove); message WM_MOVE;
    // Status Callbacks
    procedure StatusCallback(nID: Integer; lpsz: PChar);
    procedure ErrorCallback(nID: Integer; lpsz: PChar);
    // Callback for single frame and capture
    procedure FrameCallback(hCapWnd: HWND; lpVHDR: PVideoHdr);
    procedure VideoStreamCallback(hCapWnd: HWND; lpVHDR: PVideoHdr);
    procedure DisplayVideo(AEnable: Boolean);
    procedure GetDriverCaps;
    procedure GetCaptureStatus;
    procedure VideoFormatChanged;
    procedure StartSequence(ToFile: Boolean);
  public
    { Public-Deklarationen }
  end;

var
  MainForm: TMainForm;

const
  CaptureWidth  = 384;  // half size PAL
  CaptureHeight = 288;
  CaptureColorDepth = 24;

implementation

{$R *.DFM}

function WidthBytes(I: LongInt): LongInt;
begin
  Result := ((I + 31) div 32) * 4;
end;

procedure CreateBmpHeader(var bmih: TBitmapInfoHeader;
                          AWidth, AHeight, ABitsPerPixel: Integer);
begin
  with bmih do begin
    biSize := SizeOf(TBitmapInfoHeader);
    biWidth := AWidth;
    biHeight := AHeight;
    biPlanes := 1;
    biBitCount := ABitsPerPixel;
    biCompression := BI_RGB;
    biSizeImage := WidthBytes(biWidth * biBitCount) * biHeight * biPlanes;
    biXPelsPerMeter := 0;
    biYPelsPerMeter := 0;
    biClrUsed := 0;
    biClrImportant := 0;
  end;
end;

function ErrorCallback(hCapWnd: HWnd; nID: Integer; lpsz: PChar): DWord; stdcall; far;
var
  OwnerForm: TMainForm;
begin
  OwnerForm := TMainForm(capGetUserData(hCapWnd));
  if (OwnerForm <> Nil) and (OwnerForm is TMainForm) then
    OwnerForm.ErrorCallback(nID, lpsz);
  Result := 1;
end;

function StatusCallback(hCapWnd: HWnd; nID: Integer; lpsz: PChar): DWord; stdcall; far;
var
  OwnerForm: TMainForm;
begin
  OwnerForm := TMainForm(capGetUserData(hCapWnd));
  if (OwnerForm <> Nil) and (OwnerForm is TMainForm) then
    OwnerForm.StatusCallback(nID, lpsz);
  Result := 1;
end;

function FrameCallback(hCapWnd: HWnd; lpVHDR: PVideoHdr): DWord; stdcall; far;
var
  OwnerForm: TMainForm;
begin
  OwnerForm := TMainForm(capGetUserData(hCapWnd));
  if (OwnerForm <> Nil) and (OwnerForm is TMainForm) then
    OwnerForm.FrameCallBack(hCapWnd, lpVHDR);
  Result := 1;
end;

function VideoStreamCallback(hCapWnd: HWND; lpVHDR: PVideoHdr): UINT; stdcall; far;
var
  OwnerForm: TMainForm;
begin
  OwnerForm := TMainForm(capGetUserData(hCapWnd));
  if (OwnerForm <> Nil) and (OwnerForm is TMainForm) then
    OwnerForm.VideoStreamCallBack(hCapWnd, lpVHDR);
  Result := 1;
end;

{    TCAPVIDEOCALLBACK               = function(hWnd: HWND; lpVHdr: PVIDEOHDR): DWORD; stdcall;
    TCAPWAVECALLBACK                = function(hWnd: HWND; lpWHdr: PWAVEHDR): DWORD; stdcall;
    TCAPCONTROLCALLBACK             = function(hWnd: HWND; nState: int): DWORD; stdcall;}

procedure SetCallbacks(ACapWnd: HWnd);
begin
  if ACapWnd = 0 then Exit;
  capSetCallbackOnError(ACapWnd, @ErrorCallback);
  capSetCallbackOnStatus(ACapWnd, @StatusCallback);
//  capSetCallbackOnYield(ACapWnd, @);
  capSetCallbackOnFrame(ACapWnd, @FrameCallback);
  capSetCallbackOnVideoStream(ACapWnd, @VideoStreamCallback);
//  capSetCallbackOnWaveStream(ACapWnd, @);
//  capSetCallbackOnCapControl(ACapWnd, @);
end;

procedure ClearCallbacks(ACapWnd: HWnd);
begin
  if ACapWnd = 0 then Exit;
  capSetCallbackOnError(ACapWnd, Nil);
  capSetCallbackOnStatus(ACapWnd, Nil);
  capSetCallbackOnYield(ACapWnd, Nil);
  capSetCallbackOnFrame(ACapWnd, Nil);
  capSetCallbackOnVideoStream(ACapWnd, Nil);
  capSetCallbackOnWaveStream(ACapWnd, Nil);
  capSetCallbackOnCapControl(ACapWnd, Nil);
end;

procedure TMainForm.GetCaptureStatus;
begin
  if FCapWnd = 0 then Exit;
  capGetStatus(FCapWnd, @FCapStatus, SizeOf(FCapStatus));

  with FCapStatus do begin
    LblCapWinSize.Caption := Format('%d x %d', [uiImageWidth, uiImageHeight]);
    LblCapWinScroll.Caption := Format('%d, %d', [ptScroll.X, ptScroll.Y]);

    LblCapWinScale.Enabled := fScale;
    LblCapWinPreview.Enabled := fLiveWindow;
    LblCapWinOverlay.Enabled := fOverlayWindow;

    LblCapWinFrameNumber.Caption := IntToStr(dwCurrentVideoFrame);
    LblCapWinFrameNumber.Refresh;
    LblCapWinFrameDropped.Caption := IntToStr(dwCurrentVideoFramesDropped);
    LblCapWinFrameDropped.Refresh;

    // fUsingDefaultPalette         // Using default driver palette?
    // fAudioHardware               // Audio hardware present?
    // fCapFileExists               // Does capture file exist?
    // dwCurrentWaveSamples         // # of wave samples cap'td
    // dwCurrentTimeElapsedMS       // Elapsed capture duration
    // hPalCurrent                  // Current palette in use
    // fCapturingNow                // Capture in progress?
    // dwReturn                     // Error value after any operation
    // wNumVideoAllocated           // Actual number of video buffers
    // wNumAudioAllocated           // Actual number of audio buffers
  end;
end;

procedure TMainForm.VideoFormatChanged;
const
  BITexts : array[BI_RGB..BI_BITFIELDS] of String =
    ('RGB', 'RLE8', 'RLE4', 'BITFIELDS');
var
  s : String;
  BmpFileHeader : TBitmapFileHeader;
begin
  if FCapWnd = 0 then Exit;

  // read the really capture format
  FCaptureBIHSize := capGetVideoFormatSize(FCapWnd);
  GetMem(FCaptureBIH, FCaptureBIHSize);
  capGetVideoFormat(FCapWnd, FCaptureBIH, FCaptureBIHSize);

  FVideoCodec := BICompressionToVideoCodec(FCaptureBIH^.biCompression);

  case FVideoCodec of
    vcRGB : begin  // RGB format
      // Copy header
      if FStreamBIHSize <> FCaptureBIHSize then
      begin
        FStreamBIHSize := FCaptureBIHSize;
        ReAllocMem(FStreamBIH, FStreamBIHSize);
      end;
      Move(FCaptureBIH^, FStreamBIH^, FStreamBIHSize);
    end;
    vcYUY2..High(TVideoCodec) : begin
      // 32 Bit!
      CreateBmpHeader(FStreamBIH^, FCaptureBIH^.biWidth, FCaptureBIH^.biHeight, 32);
    end;
  end;

  // prepare Stream for Frame
  FFrameStream.Size := SizeOf(BmpFileHeader) + FStreamBIHSize +
    Integer(FStreamBIH^.biSizeImage);
  // Fill the Header
  FillChar(BmpFileHeader, SizeOf(BmpFileHeader), 0);
  with BmpFileHeader do begin
    bfType := $4D42;  // 'BM'
    bfSize := FFrameStream.Size;
    bfOffBits := SizeOf(BmpFileHeader) + FStreamBIHSize;
  end;
  FFrameStream.Position := 0;
  FFrameStream.WriteBuffer(BmpFileHeader, SizeOf(BmpFileHeader));
  FFrameStream.WriteBuffer(FStreamBIH^, FStreamBIHSize);
  FPixelPointer := FFrameStream.Memory;
  Inc(FPixelPointer, BmpFileHeader.bfOffBits);

  // Display
  LblVideoFormatWidth.Caption := Format('%d ', [FCaptureBIH^.biWidth]);
  LblVideoFormatHeight.Caption := Format('%d ', [FCaptureBIH^.biHeight]);
  LblVideoFormatBitsPerPixel.Caption := Format('%d ', [FCaptureBIH^.biBitCount]);
  if FCaptureBIH^.biCompression in [BI_RGB..BI_BITFIELDS] then
    s := BITexts[FCaptureBIH^.biCompression]
  else
    SetString(s, PChar(@FCaptureBIH^.biCompression), 4);
  LblVideoFormatCompression.Caption := s + ' ';
end;

procedure TMainForm.FormCreate(Sender: TObject);
begin
  FFrameStream := TMemoryStream.Create;

  FCaptureBIHSize := 0;
  FCaptureBIH := Nil;

  // bitmap info header for frame grab
  FStreamBIHSize := SizeOf(TBitmapInfoHeader);
  GetMem(FStreamBIH, FStreamBIHSize);
  // create capture window
  FCapWnd := capCreateCaptureWindow('Capture Window', WS_CHILD or WS_VISIBLE,
                                    0, 0, CaptureWidth, CaptureHeight,
                                    Panel1.Handle, 0);
  if FCapWnd <> 0 then
  begin
    // the second parameter is the driver index
    if capDriverConnect(FCapWnd, 0) then
    begin
      GetDriverCaps;

      // create header for half size pal with true color
      CreateBmpHeader(FStreamBIH^, CaptureWidth, CaptureHeight, CaptureColorDepth);
      capSetVideoFormat(FCapWnd, FStreamBIH, FStreamBIHSize);
      VideoFormatChanged;

      DisplayVideo(True);

      // set callback
      if capSetUserData(FCapWnd, LongInt(Self)) then
        SetCallbacks(FCapWnd);
    end
    else
    begin  // no connect to driver
      DestroyWindow(FCapWnd);
      FCapWnd := 0;
    end;
  end;
  // create frame bitmap
  FFrameBitmap := TBitmap.Create;
  GetCaptureStatus;
end;

procedure TMainForm.FormDestroy(Sender: TObject);
begin
  if FCapWnd <> 0 then begin
    capCaptureStop(FCapWnd);

    ClearCallbacks(FCapWnd);
    capDriverDisconnect(FCapWnd);
    DestroyWindow(FCapWnd);
    FCapWnd := 0;
  end;
  FPixelPointer := Nil;
  FFrameStream.Free;
  FFrameBitmap.Free;
  if (FStreamBIHSize > 0) and (FStreamBIH <> Nil) then
    FreeMem(FStreamBIH, FStreamBIHSize);
  if (FCaptureBIHSize > 0) and (FCaptureBIH <> Nil) then
    FreeMem(FCaptureBIH, FCaptureBIHSize);
end;

procedure TMainForm.WMMove(var Message: TWMMove);
begin
  inherited;
  // after move of form
  if FCapWnd = 0 then Exit;
  ShowWindow(FCapWnd, SW_HIDE);
  ShowWindow(FCapWnd, SW_SHOW);
end;

procedure TMainForm.DisplayVideo(AEnable: Boolean);
begin
  if FCapWnd = 0 then Exit;
  if AEnable then
  begin
    if RBtnOverlay.Checked then
    begin
      capPreviewRate(FCapWnd, 0);
      capOverlay(FCapWnd, True);
    end
    else
    begin
      capPreviewRate(FCapWnd, UDPreviewRate.Position);
      capPreview(FCapWnd, True);
    end;
  end
  else
  begin
    capPreview(FCapWnd, False);
    capOverlay(FCapWnd, False);
  end;
  GetCaptureStatus;
end;

procedure TMainForm.GetDriverCaps;
begin
  if FCapWnd = 0 then Exit;
  capDriverGetCaps(FCapWnd, @FDriverCaps, SizeOf(FDriverCaps));
  if Not FDriverCaps.fHasOverlay then
    RBtnPreview.Checked := True;
  RBtnOverlay.Enabled := FDriverCaps.fHasOverlay;
end;

procedure TMainForm.StatusCallback(nID: Integer; lpsz: PChar);
begin
  StatusBar1.Panels[0].Text := Format('%d %s', [nID,lpsz]);
end;

procedure TMainForm.ErrorCallback(nID: Integer; lpsz: PChar);
begin
  StatusBar1.Panels[1].Text := Format('%d %s', [nID,lpsz]);
end;

procedure TMainForm.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  case Key of
    vk_Space : begin
      with SBtnFreeze do Down := Not Down;
      SBtnFreezeClick(Nil);
      Key := 0;
    end;
  end;
end;

procedure TMainForm.SBtnFreezeClick(Sender: TObject);
begin
  if FCapWnd = 0 then Exit;
  if SBtnFreeze.Down then
    capGrabFrame(FCapWnd)
  else
    DisplayVideo(True);
  GetCaptureStatus;
end;

procedure TMainForm.BtnGrabFrameNoStopClick(Sender: TObject);
begin
  if FCapWnd = 0 then Exit;
  capGrabFrameNoStop(FCapWnd);
  GetCaptureStatus;
end;

procedure TMainForm.StartSequence(ToFile: Boolean);
var
  CaptureParms : TCaptureParms;
begin
  if FCapWnd = 0 then Exit;
  capCaptureGetSetup(FCapWnd, @CaptureParms, SizeOf(CaptureParms));
  with CaptureParms do
  begin
    // frame rate
    dwRequestMicroSecPerFrame := 1000000 div UDFrameRate.Position;  // *Micro* seconds!
    // background capture
    fYield := False;
    // abort by mouse click
    fAbortLeftMouse := True;
    fAbortRightMouse := True;
    // abort after 30 seconds
    fLimitEnabled := True;
    wTimeLimit := 30;
    // abort with space key
    vKeyAbort := VK_ESCAPE;
  end;
  capCaptureSetSetup(FCapWnd, @CaptureParms, SizeOf(CaptureParms));
  if ToFile then
    capCaptureSequence(FCapWnd)
  else
    capCaptureSequenceNoFile(FCapWnd);
  GetCaptureStatus;
end;

procedure TMainForm.BtnSequenceNoFileClick(Sender: TObject);
begin
  StartSequence(False);
end;

procedure TMainForm.BtnSequenceClick(Sender: TObject);
begin
  StartSequence(True);
end;

procedure TMainForm.BtnCopyClick(Sender: TObject);
begin
  if FCapWnd = 0 then Exit;
  capEditCopy(FCapWnd);
  GetCaptureStatus;
end;

procedure TMainForm.BtnSaveClick(Sender: TObject);
begin
  if FCapWnd = 0 then Exit;
  if SaveDialog1.Execute then
//    capFileSaveDIB(FCapWnd, PChar(SaveDialog1.Filename));
  GetCaptureStatus;
end;

procedure TMainForm.FrameCallback(hCapWnd: HWND; lpVHDR: PVideoHdr);
begin
  if hCapWnd = FCapWnd then
  begin
    LblTime.Caption := 'Single Frame';
    if Assigned(FPixelPointer) then
    begin
      case FVideoCodec of
        vcUnknown : Exit;
        vcRGB : Move(lpVHDR^.lpData^, FPixelPointer^, FCaptureBIH^.biSizeImage);
      else
        if Not ConvertCodecToRGB(FVideoCodec, lpVHDR^.lpData, FPixelPointer, FCaptureBIH^.biWidth, FCaptureBIH^.biHeight) then Exit;
      end;
      Exit;
      FFrameStream.Position := 0;
      FFrameBitmap.LoadFromStream(FFrameStream);
      PaintBoxFrame.Canvas.Draw(0, 0, FFrameBitmap);
    end;
  end;
  GetCaptureStatus;
end;

function MillisecondsToStr(Value: DWord): String;
var
  hour, min, sec, msec: Integer;
begin
  msec := Value mod 1000;
  Value := Value div 1000;
  sec := Value mod 60;
  Value := Value div 60;
  min := Value mod 60;
  hour := Value div 60;
  Result := Format('%.2d:%.2d:%.2d.%.3d', [hour, min, sec, msec]);
end;

procedure TMainForm.VideoStreamCallback(hCapWnd: HWND; lpVHDR: PVideoHdr);
begin
  if hCapWnd = FCapWnd then
  begin
    LblTime.Caption := MillisecondsToStr(lpVHDR^.dwTimeCaptured);
    LblTime.Refresh;
    if Assigned(FPixelPointer) then
    begin
      case FVideoCodec of
        vcUnknown : Exit;
        vcRGB : Move(lpVHDR^.lpData^, FPixelPointer^, FCaptureBIH^.biSizeImage);
      else
        if Not ConvertCodecToRGB(FVideoCodec, lpVHDR^.lpData, FPixelPointer, FCaptureBIH^.biWidth, FCaptureBIH^.biHeight) then Exit;
      end;
      FFrameStream.Position := 0;
      FFrameBitmap.LoadFromStream(FFrameStream);
      if CBxFlip.Checked then
      begin
        PaintBoxFrame.Canvas.CopyMode := SrcCopy;
        PaintBoxFrame.Canvas.CopyRect(Rect(0, 0, CaptureWidth, CaptureHeight),
          FFrameBitmap.Canvas, Rect(0, CaptureHeight, CaptureWidth, 0));
      end
      else
        PaintBoxFrame.Canvas.Draw(0, 0, FFrameBitmap);
    end;
  end;
  GetCaptureStatus;
end;

procedure TMainForm.UDPreviewRateClick(Sender: TObject;
  Button: TUDBtnType);
begin
  if FCapWnd = 0 then Exit;
  capPreviewRate(FCapWnd, UDPreviewRate.Position);
  GetCaptureStatus;
end;

procedure TMainForm.RBtnPreviewClick(Sender: TObject);
var
  fl : Boolean;
begin
  fl := RBtnPreview.Checked;
  if fl and (FCapWnd <> 0) then
    capPreviewRate(FCapWnd, UDPreviewRate.Position);
  EditPreviewRate.Enabled := fl;
  UDPreviewRate  .Enabled := fl;
  LblPreviewRate .Enabled := fl;
  if fl then DisplayVideo(Not SBtnFreeze.Down);
end;

procedure TMainForm.RBtnOverlayClick(Sender: TObject);
begin
  if RBtnOverlay.Checked then
    DisplayVideo(Not SBtnFreeze.Down);
end;

procedure TMainForm.MPntDialogClick(Sender: TObject);
begin
  MPntDlgVideoSource.Enabled := FDriverCaps.fHasDlgVideoSource;
  MPntDlgVideoFormat.Enabled := FDriverCaps.fHasDlgVideoFormat;
  MPntDlgVideoDisplay.Enabled := FDriverCaps.fHasDlgVideoDisplay;
end;

procedure TMainForm.MPntDlgVideoFormatClick(Sender: TObject);
begin
  if FCapWnd = 0 then Exit;
  capDlgVideoFormat(FCapWnd);
  VideoFormatChanged;
  GetCaptureStatus;
end;

procedure TMainForm.MPntDlgVideoSourceClick(Sender: TObject);
begin
  if FCapWnd = 0 then Exit;
  capDlgVideoSource(FCapWnd);
  GetCaptureStatus;
end;

procedure TMainForm.MPntDlgVideoDisplayClick(Sender: TObject);
begin
  if FCapWnd = 0 then Exit;
  capDlgVideoDisplay(FCapWnd);
  GetCaptureStatus;
end;

procedure TMainForm.MPntDlgCompressionClick(Sender: TObject);
begin
  if FCapWnd = 0 then Exit;
  capDlgVideoCompression(FCapWnd);
  GetCaptureStatus;
end;

procedure TMainForm.MPntExitClick(Sender: TObject);
begin
  Close;
end;

end.
