unit ChatControl;

interface

uses
  Windows, Messages, SysUtils, Classes, Controls, Menus, Forms, Graphics, Jpeg, Dialogs,
  StdCtrls, Vcl.ExtCtrls, Clipbrd, RESTAPI_Kozos, KozosTipusok;

type

  TMyImage = class(TImage)
  public
    thumburl: string;
    fullurl: string;
    mimetype: string;
    message_id: string
  end; // class

  TMyButton = class(TButton)
  public
    thumburl: string;
    fullurl: string;
    mimetype: string;
  end; // class

  TUser = (User1 = 0, User2 = 1);

  // TChatControl = class(TCustomControl)
  TChatControl = class(TScrollBox)
   // procedure WMHScroll(var Message: TWMHScroll); message WM_HSCROLL;
   procedure WMVScroll(var Message: TWMVScroll); message WM_VSCROLL;
  private
    FColor1, FColor2, FBackColor, FDeletedBackColor: TColor;
    FFontColor1, FFontColor2: TColor;
    FStrings: TStringList;
    // FScrollPos: integer;
    // FOldScrollPos: integer;
    // FBottomPos: integer;
    // FBoxTops: array of integer;
    // FInvalidateCache: boolean;
    FFontSize, FInfoFontSize, FSenderFontSize: integer;
    // FThumbArray: array of TMyImage;  // attachment thumbnail images
    // FThumbArray: array of TMyButton;  // attachment thumbnail images
    FUniThumbImage: TJPEGImage;
    // procedure StringsChanged(Sender: TObject);
    FContentHeight: integer;
    FOnScrollVertUp: TNotifyEvent;
    // FOnScrollVertDown: TNotifyEvent;
    // FOnScrollHorz: TNotifyEvent;

    ppmMessageClick: TPopupMenu;

    procedure SetColor1(Color1: TColor);
    procedure SetColor2(Color2: TColor);
    procedure SetFontColor1(AColor: TColor);
    procedure SetFontColor2(AColor: TColor);
    procedure SetBackColor(Color: TColor);
    procedure SetDeletedBackColor(Color: TColor);
    procedure SetStringList(Strings: TStringList);
    // procedure ScrollPosUpdated;
    // procedure InvalidateCache;
    // procedure FreeImages;
    function GetListOp(CL: string; N: integer; Sep: string): string;
    procedure ThumbClick(Sender: TObject);
    procedure LabelClick(Sender: TObject);
    procedure pmiUzenetTorlesClick(Sender: TObject);
  protected
  public
    OnRequestRefresh: procedure of object;
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    property OnScrollVertUp: TNotifyEvent read FOnScrollVertUp Write FonScrollVertUp;
    // property OnScrollHorz:TNotifyEvent read FOnScrollHorz Write FonScrollHorz;
    // procedure OnDblClick; override;

    function Say(const User: TUser; const S: String): Integer;
    procedure ShowChat;
    // procedure ScrollToBottom;
    procedure Clear;
    procedure SetFontSize(ASize: integer);
    procedure SetSenderFontSize(ASize: integer);
    procedure SetInfoFontSize(ASize: integer);
    function GetContentHeight: integer;
    procedure ScrollTo(const APos: integer);
    procedure ppmMessageClickPopup(Sender: TObject);
  published
    property Align;
    property Anchors;
    property Cursor;
    property Font;
    property Color1: TColor read FColor1 write SetColor1 default clSkyBlue;
    property Color2: TColor read FColor2 write SetColor2 default clMoneyGreen;
    property FontColor1: TColor read FFontColor1 write SetFontColor1 default clBlack;
    property FontColor2: TColor read FFontColor2 write SetFontColor2 default clBlack;
    property BackColor: TColor read FBackColor write SetBackColor default clWhite;
    property DeletedBackColor: TColor read FDeletedBackColor write SetDeletedBackColor default clRed;
    property Strings: TStringList read FStrings write SetStringList;
    // property UniThumbImage: TPNGGraphic read FUniThumbImage write SetUniThumbImage;
    property TabOrder;
    property TabStop;
  end;

procedure Register;

var
    RefreshCallbackProc: TRefreshCallbackProc;

implementation

uses Math, Shellapi, Kozos, URLCache;

procedure Register;
begin
  RegisterComponents('Rejbrand 2009', [TChatControl]);
end;

{ TChatControl }


constructor TChatControl.Create(AOwner: TComponent);
var
  Stream: TMemoryStream;
begin
  inherited;

  DoubleBuffered := true;

  FStrings := TStringList.Create;
  FColor1 :=  $00ACAC9A; //clSkyBlue;
  FColor2 := $00B29943; // clMoneyGreen;

  // FOldScrollPos := MaxInt;
  ppmMessageClick:= TPopupMenu.Create(Self);
  ppmMessageClick.OnPopup:= ppmMessageClickPopup;
  ppmMessageClick.Items.Add(TMenuItem.Create(ppmMessageClick));
  with ppmMessageClick.Items[ppmMessageClick.Items.Count-1] do begin
      Caption:= 'T�rl�m az �zenetet';
      OnClick:= pmiUzenetTorlesClick;
      end;  // with



  Stream := TMemoryStream.Create;
  FUniThumbImage:= TJPEGImage.Create;
  try
    Stream.LoadFromFile('Thumb_uni.jpg');
    Stream.Position := 0;

    FUniThumbImage.LoadFromStream(Stream);
  finally
    Stream.Free;
    end; // try-finally

end;

destructor TChatControl.Destroy;
begin
  FStrings.Free;
  if ppmMessageClick <> nil then ppmMessageClick.Free;
  inherited;
end;

{procedure TChatControl.WMHScroll(var Message: TWMHScroll);
begin
   inherited;
   if Assigned(FOnScrollHorz) then  FOnScrollHorz(Self);
end;}

procedure TChatControl.WMVScroll(var Message: TWMVScroll);
begin
   inherited;
   if (Message.ScrollCode = SB_LINEUP)
    or (Message.ScrollCode = SB_PAGEUP)
    or (Message.ScrollCode = SB_TOP) then begin
     if Assigned(FOnScrollVertUp) then FOnScrollVertUp(Self);
     end;  // if
end;

procedure TChatControl.ppmMessageClickPopup(Sender: TObject);
var
  S: string;
begin
{  if (Sender as TPopupMenu).PopupComponent is TMyImage then begin
      S:= ((Sender as TPopupMenu).PopupComponent as TMyImage).message_id;
      end;
  if (Sender as TPopupMenu).PopupComponent is TMyLabel then begin
      S:= ((Sender as TPopupMenu).PopupComponent as TMyLabel).message_id;
      end;
 }
 // NoticeKi(S);
end;

procedure TChatControl.pmiUzenetTorlesClick(Sender: TObject);
var
   Res, MessageID: string;
begin

  MessageID:='';
  if ppmMessageClick.PopupComponent is TMyImage then begin
      MessageID:= (ppmMessageClick.PopupComponent as TMyImage).message_id;
      end;
  if ppmMessageClick.PopupComponent is TMyLabel then begin
      MessageID:= (ppmMessageClick.PopupComponent as TMyLabel).message_id;
      end;
  if (MessageID <> '') then begin
      if NoticeKi('Biztosan t�r�lni szeretn� ezt az �zenetet?', NOT_QUESTION) = 0 then
         Res:= RESTAPIKozosDlg.DeleteMessage(MessageID);
         if Res = '' then begin
            if Assigned(OnRequestRefresh) then
                OnRequestRefresh;  // event fires
            end
         else begin
            NoticeKi('Az �zenet t�rl�s nem siker�lt (' + Res + ')', NOT_QUESTION);
            end;
      end;  // if
end;


procedure TChatControl.SetFontSize(ASize: integer);
begin
  FFontSize:= ASize;
end;

procedure TChatControl.SetSenderFontSize(ASize: integer);
begin
  FSenderFontSize:= ASize;
end;

procedure TChatControl.SetInfoFontSize(ASize: integer);
begin
  FInfoFontSize:= ASize;
end;

function TChatControl.GetContentHeight: integer;
begin
   Result:= FContentHeight;
end;

procedure TChatControl.Clear;
begin
  FStrings.Clear;
  ShowChat;
end;

procedure TChatControl.ShowChat;
const
  Aligns: array[TUser] of integer = (DT_LEFT, DT_LEFT);   // NagyP: both aligns to the left
  InfoColor = clBtnFace;  // clYellow
  TimestampPattern = 'timestamp:';
  SenderColor = clBtnFace;
  SenderPattern = 'sender:';
  AttachmentPattern = 'attachment:';
  TextPattern = 'text:';
  DeletedPattern = 'deleted:';
  BelsoSzep = '^';
  ShapeBorderWidth = 6;
var
  Colors: array[TUser] of TColor;
var
  User: TUser;
  i, y, MaxWidth, RectWidth, DynamicGap: integer;
  ItemWidth, ItemHeight, ItemLeft, ItemTop, LatestRightTextLeft, MyClientWidth, RightMarginPos: integer;
  ActFontSize: integer;
  ActBackColor, ActBrushColor: TColor;
  ActLabel: TMyLabel;
  ActThumb: TMyImage;
  ActShape: TShape;
  Ratio: Double;
  MyDrawStyle, StringToPrint, Res: string;
begin
  Colors[User1] := FColor1;
  Colors[User2] := FColor2;
  // -------- elemek t�rl�se ------------ //
  while Self.ControlCount > 0 do Self.Controls[0].Free;  // az �sszes al-panel t�rl�se
  // FreeImages;
  // ------------------------------------ //
  y := 10;
  MyClientWidth:= ClientWidth;
  MaxWidth := floor(MyClientWidth * 0.8);  // NagyP: wider text display
  RightMarginPos:= MyClientWidth - 25;  // scrollbar-nak hely
  LatestRightTextLeft:= MyClientWidth - 10 - MaxWidth;
  for i := 0 to FStrings.Count - 1 do
  begin
    StringToPrint:= FStrings[i];
    MyDrawStyle:='text';  // az�rt egy default legyen
    if copy(StringToPrint, 1, length(TextPattern)) = TextPattern then begin
      StringToPrint:= copy(StringToPrint, length(TextPattern)+1, 999999);
      MyDrawStyle:= 'text';
      end;
    if copy(StringToPrint, 1, length(TimestampPattern)) = TimestampPattern then begin
      StringToPrint:= copy(StringToPrint, length(TimestampPattern)+1, 999999);
      MyDrawStyle:= 'timestamp';
      end;
    if copy(StringToPrint, 1, length(SenderPattern)) = SenderPattern then begin
      StringToPrint:= copy(StringToPrint, length(SenderPattern)+1, 999999);
      MyDrawStyle:= 'sender';
      end;
    if copy(StringToPrint, 1, length(AttachmentPattern)) = AttachmentPattern then begin
      StringToPrint:= copy(StringToPrint, length(AttachmentPattern)+1, 999999);
      MyDrawStyle:= 'attachment';
      end;
    if copy(StringToPrint, 1, length(DeletedPattern)) = DeletedPattern then begin
      StringToPrint:= copy(StringToPrint, length(DeletedPattern)+1, 999999);
      MyDrawStyle:= 'deleted';
      end;

    User := TUser(FStrings.Objects[i]);

    if MyDrawStyle='timestamp' then begin
      ActFontSize:= FInfoFontSize;
      ActBrushColor:= InfoColor;
      ActBackColor:= FBackColor;
      DynamicGap:= 5
      end;
    if MyDrawStyle='sender' then begin
      ActFontSize:= FSenderFontSize;
      ActBrushColor:= SenderColor;
      ActBackColor:= FBackColor;
      DynamicGap:= 5
      end;
    if MyDrawStyle='text' then begin
      ActFontSize:= FFontSize;
      ActBrushColor:= Colors[User];
      ActBackColor:= Colors[User];
      DynamicGap:= 0;
      end;
    if MyDrawStyle='deleted' then begin
      ActFontSize:= FFontSize;
      ActBrushColor:= FDeletedBackColor;
      ActBackColor:= FDeletedBackColor;
      DynamicGap:= 0;
      end;
    if MyDrawStyle='attachment' then begin
      DynamicGap:= 0;
      end;
    y:= y - DynamicGap;
    // ----------------- //
    ItemTop:= y;
    if User = User1 then ItemLeft:= 10
    else ItemLeft:= RightMarginPos - 25;  // ItemLeft:= LatestRightTextLeft;  // jobbra igaz�tjuk a d�tumot is

    if (MyDrawStyle <> 'attachment') then begin  // a sz�veges elemek
      // ActLabel:= TLabel.Create(Owner);
      ActLabel:= TMyLabel.Create(Owner);  // TLabel + message_id
      ActLabel.Parent:= Self;
      ActLabel.Left:= ItemLeft;
      ActLabel.Top:= ItemTop;
      ActLabel.Constraints.MaxWidth:= MaxWidth;
      ActLabel.Autosize:= True;
      ActLabel.Transparent:= True;
      ActLabel.Font.Size:= ActFontSize;
      ActLabel.message_id:= GetListOp(StringToPrint, 0, BelsoSzep);
      ActLabel.Caption:= GetListOp(StringToPrint, 1, BelsoSzep);
      ActLabel.Wordwrap:= True;
      if (MyDrawStyle='timestamp') and (User = User2) then begin
        ActLabel.Left:= RightMarginPos - ActLabel.Width;
        end;
      if (MyDrawStyle = 'text') or (MyDrawStyle = 'deleted') then begin
        if User = User1 then begin
            ActLabel.Font.Color:= FFontColor1;
            end
        else begin
            ActLabel.Font.Color:= FFontColor2;
            ActLabel.Left:= RightMarginPos - ActLabel.Width;  // jobbra igaz�tjuk, �s hagyunk helyet a f�gg�leges cs�szk�nak
            LatestRightTextLeft:= ActLabel.Left;
            end;  // else
        end;  // if
      if (MyDrawStyle = 'text') then begin  // csak a sz�veges bubor�khoz
        ActLabel.OnClick:= LabelClick;
        if (User = User2) then
          ActLabel.Popupmenu:= ppmMessageClick;
        end;

      if (MyDrawStyle = 'text') or (MyDrawStyle = 'deleted') then begin  // a sz�veges elemek
        ActShape:= TShape.Create(Owner);
        ActShape.Parent:= Self;
        with ActShape do begin
          Left:= ActLabel.Left-ShapeBorderWidth;
          Top:= ActLabel.Top-ShapeBorderWidth;
          Width:= ActLabel.Width + 2*ShapeBorderWidth;
          Height:= ActLabel.Height + 2*ShapeBorderWidth;
          Shape:= stRoundRect;
          Brush.Color:= ActBackColor;
          end; // with
        ActLabel.BringToFront;  // bring label to front
        end;  // if
      y := ActLabel.top + ActLabel.Height + 12 - DynamicGap;
      // FBottomPos := y + FScrollPos;
      end // text elemek ki�r�sa
    else begin  // attachment
      ActThumb:= TMyImage.Create(Owner);
      ActThumb.Parent:= Self;
      with ActThumb do begin
        Left:= ItemLeft;
        Top:= ItemTop;
        thumburl:= GetListOp(StringToPrint, 2, BelsoSzep);
        if thumburl = '' then
           (ActThumb as TImage).Picture.Graphic := FUniThumbImage
        else begin
           Res:= RESTAPIKozosDlg.GetURLPicture((ActThumb as TImage), thumburl, 'image/jpg', URLCacheDlg.GetCacheIDFromURL(thumburl));
           end;
        if (ActThumb.Width > cThumbMaxSize) or (ActThumb.Height > cThumbMaxSize) then begin  // m�retezz�k a k�pet
          Ratio:=ActThumb.Width / ActThumb.Height;
          ActThumb.Autosize:= False;
          ActThumb.Stretch:= True;
          if Ratio>1 then begin
            ActThumb.Height:= Round(cThumbMaxSize/Ratio);
            ActThumb.Width:=cThumbMaxSize;
            end
          else begin
            ActThumb.Width:=Round(cThumbMaxSize*Ratio);
            ActThumb.Height:=cThumbMaxSize;
            end;  // else
          end;
        if (User = User2) then begin
            ActThumb.Left:= RightMarginPos - ActThumb.Width;
            end;
        mimetype:= GetListOp(StringToPrint, 1, BelsoSzep);
        fullurl:= GetListOp(StringToPrint, 3, BelsoSzep);
        message_id:= GetListOp(StringToPrint, 0, BelsoSzep);
        ItemHeight:= ActThumb.Height;  // a tartalomhoz beh�zott 'autosize' TImage alapj�n
        OnClick:= ThumbClick;
        if (User = User2) then
          Popupmenu:= ppmMessageClick;
        end;
      y := ItemTop + ItemHeight + 10 - DynamicGap;
      // FBottomPos := y + FScrollPos;
      end;  // attachment
   FContentHeight:= y;
  end;
 VertScrollBar.Position:= VertScrollBar.Range; // scroll down to the end
end;


procedure TChatControl.ScrollTo(const APos: integer);
begin
   VertScrollBar.Position:= VertScrollBar.Range - APos;
end;

function TChatControl.Say(const User: TUser; const S: String): Integer;
begin
  result := FStrings.AddObject(S, TObject(User));
end;

procedure TChatControl.SetColor1(Color1: TColor);
begin
  if FColor1 <> Color1 then FColor1 := Color1;
end;

procedure TChatControl.SetColor2(Color2: TColor);
begin
  if FColor2 <> Color2 then FColor2 := Color2;
end;

procedure TChatControl.SetFontColor1(AColor: TColor);
begin
  if FFontColor1 <> AColor then FFontColor1 := AColor;
end;

procedure TChatControl.SetFontColor2(AColor: TColor);
begin
  if FFontColor2 <> AColor then FFontColor2 := AColor;
end;

procedure TChatControl.SetBackColor(Color: TColor);
begin
  if FBackColor <> Color then
  begin
    FBackColor := Color;
  end;
end;

procedure TChatControl.SetDeletedBackColor(Color: TColor);
begin
  if FDeletedBackColor <> Color then
  begin
    FDeletedBackColor := Color;
  end;
end;

procedure TChatControl.SetStringList(Strings: TStringList);
begin
  FStrings.Assign(Strings);
end;

procedure TChatControl.ThumbClick(Sender: TObject);
var
  FullURL, MimeType, TempFN: string;
begin
  FullURL:= (Sender as TMyImage).fullurl;
  MimeType:= (Sender as TMyImage).mimetype;
	// ShellExecute(Application.Handle,'open', PChar(FullURL), nil, nil, SW_SHOWNORMAL) ;

  TempFN:= RESTAPIKozosDlg.SaveURLFile(FullURL, MimeType);
  ShellExecute(Application.Handle,'open', PChar(TempFN), nil, nil, SW_SHOWNORMAL) ;

  // Clipboard.Clear;
  // Clipboard.AsText:= FullURL;
  // MessageDlg('Hivatkoz�s a v�g�lapra m�solva.', mtInformation,[mbOK],0);
end;

procedure TChatControl.LabelClick(Sender: TObject);
var
  S: string;
begin
  S:= (Sender as TLabel).Caption;
  // MessageDlg(S, mtInformation,[mbOK],0);
  Clipboard.Clear;
  Clipboard.AsText:= S;
  MessageDlg('Sz�veg a v�g�lapra m�solva.', mtInformation,[mbOK],0);
end;

function TChatControl.GetListOp(CL: string; N: integer; Sep: string): string;
var T:string;
    i: integer;

    function SepFrontToken(Sep, Miben: string): string;
    var  P:integer;
    begin
         P:=Pos(Sep, Miben);
         if P>0 then
            SepFrontToken:=Copy(Miben, 1, P-1)
         else
            SepFrontToken:=Miben;
    end;
begin
     i:=0;
     T:=SepFrontToken(Sep, CL);
     while i<N do begin
           CL:=Copy(CL,Length(T)+Length(Sep)+1,Length(CL)-Length(T)-Length(Sep));
           T:=SepFrontToken(Sep, CL);
           i:=i+1;
           end;
     GetListOp:=T;
end;

end.