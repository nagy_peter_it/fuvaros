object FInfo_: TFInfo_
  Left = 534
  Top = 333
  Caption = 'Inform'#225'ci'#243'k'
  ClientHeight = 447
  ClientWidth = 900
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poMainFormCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnDeactivate = FormDeactivate
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 0
    Top = 0
    Width = 900
    Height = 447
    ActivePage = TabSheet4
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 0
    OnChange = PageControl1Change
    object TabSheet1: TTabSheet
      Caption = 'Felhaszn'#225'l'#243'k'
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object DBGrid2: TDBGrid
        Left = 0
        Top = 0
        Width = 892
        Height = 416
        Align = alClient
        DataSource = DSFelhasznalo
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -13
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = [fsBold]
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'Verzi'#243't'#246'rt'#233'net'
      ImageIndex = 1
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Memo1: TMemo
        Left = 0
        Top = 41
        Width = 892
        Height = 375
        Hint = 'Dupla klikkre v'#225'lt'#225's norm'#225'l '#233's maxim'#225'lis ablakm'#233'ret k'#246'z'#246'tt.'
        Align = alClient
        Color = clInfoBk
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ScrollBars = ssBoth
        ShowHint = True
        TabOrder = 0
        WordWrap = False
        OnDblClick = Memo1DblClick
      end
      object Panel2: TPanel
        Left = 0
        Top = 0
        Width = 892
        Height = 41
        Align = alTop
        BevelInner = bvLowered
        BevelOuter = bvLowered
        ParentBackground = False
        TabOrder = 1
        object Label5: TLabel
          Left = 16
          Top = 14
          Width = 94
          Height = 16
          Caption = 'V'#225'ltoz'#225'slista:'
        end
        object cbValtozaslist: TComboBox
          Left = 123
          Top = 11
          Width = 399
          Height = 24
          Style = csDropDownList
          Sorted = True
          TabOrder = 0
        end
        object Button2: TButton
          Left = 536
          Top = 11
          Width = 145
          Height = 24
          Caption = 'Megnyit'#225's'
          TabOrder = 1
          OnClick = Button2Click
        end
      end
    end
    object TabSheet3: TTabSheet
      Caption = 'G'#233'pkocsi ell.'
      ImageIndex = 2
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Memo2: TMemo
        Left = 0
        Top = 0
        Width = 892
        Height = 416
        Hint = 'Dupla klikkre v'#225'lt'#225's norm'#225'l '#233's maxim'#225'lis ablakm'#233'ret k'#246'z'#246'tt.'
        Align = alClient
        Color = clInfoBk
        Lines.Strings = (
          'Fejleszt'#233's alatt.')
        ParentShowHint = False
        ReadOnly = True
        ScrollBars = ssBoth
        ShowHint = True
        TabOrder = 0
        WordWrap = False
        OnDblClick = Memo1DblClick
      end
    end
    object TabSheet4: TTabSheet
      Caption = 'Dolgoz'#243' ell.'
      ImageIndex = 3
      object Memo3: TMemo
        Left = 0
        Top = 0
        Width = 892
        Height = 416
        Hint = 'Dupla klikkre v'#225'lt'#225's norm'#225'l '#233's maxim'#225'lis ablakm'#233'ret k'#246'z'#246'tt.'
        Align = alClient
        Color = clInfoBk
        Lines.Strings = (
          'Fejleszt'#233's alatt.')
        ParentShowHint = False
        ReadOnly = True
        ScrollBars = ssBoth
        ShowHint = True
        TabOrder = 0
        WordWrap = False
        OnDblClick = Memo1DblClick
      end
    end
    object TabSheet5: TTabSheet
      Caption = 'Vev'#337' ell.'
      ImageIndex = 4
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Memo4: TMemo
        Left = 0
        Top = 0
        Width = 892
        Height = 416
        Hint = 'Dupla klikkre v'#225'lt'#225's norm'#225'l '#233's maxim'#225'lis ablakm'#233'ret k'#246'z'#246'tt.'
        Align = alClient
        Color = clInfoBk
        Lines.Strings = (
          'Fejleszt'#233's alatt.')
        ParentShowHint = False
        ReadOnly = True
        ScrollBars = ssBoth
        ShowHint = True
        TabOrder = 0
        WordWrap = False
        OnDblClick = Memo1DblClick
      end
    end
    object TabSheet6: TTabSheet
      Caption = 'Z'#225'r'#243' km ell.'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ImageIndex = 5
      ParentFont = False
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Memo5: TMemo
        Left = 0
        Top = 0
        Width = 892
        Height = 416
        Hint = 'Dupla klikkre v'#225'lt'#225's norm'#225'l '#233's maxim'#225'lis ablakm'#233'ret k'#246'z'#246'tt.'
        Align = alClient
        Color = clInfoBk
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Courier New'
        Font.Style = [fsBold]
        Lines.Strings = (
          'Fejleszt'#233's alatt.')
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ScrollBars = ssBoth
        ShowHint = True
        TabOrder = 0
        WordWrap = False
        OnDblClick = Memo1DblClick
      end
    end
    object TabSheet7: TTabSheet
      Caption = 'Poz'#237'ci'#243'sz'#225'm ell.'
      ImageIndex = 6
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Memo6: TMemo
        Left = 0
        Top = 0
        Width = 892
        Height = 416
        Hint = 'Dupla klikkre v'#225'lt'#225's norm'#225'l '#233's maxim'#225'lis ablakm'#233'ret k'#246'z'#246'tt.'
        Align = alClient
        Color = clInfoBk
        Lines.Strings = (
          'Fejleszt'#233's alatt.')
        ParentShowHint = False
        ReadOnly = True
        ScrollBars = ssBoth
        ShowHint = True
        TabOrder = 0
        WordWrap = False
        OnDblClick = Memo1DblClick
      end
    end
    object TabSheet8: TTabSheet
      Caption = 'T'#225'voll'#233't'
      ImageIndex = 7
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object DBGrid1: TDBGrid
        Left = 0
        Top = 0
        Width = 892
        Height = 357
        Align = alClient
        DataSource = DSDolgozo
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        Options = [dgTitles, dgIndicator, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -13
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = [fsBold]
        OnDrawColumnCell = DBGrid1DrawColumnCell
        Columns = <
          item
            Expanded = False
            FieldName = 'TA_DONEV'
            Title.Alignment = taCenter
            Title.Caption = 'Dolgoz'#243'n'#233'v'
            Width = 200
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'TA_NAP01'
            Title.Alignment = taCenter
            Width = 20
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'TA_NAP02'
            Title.Alignment = taCenter
            Width = 20
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'TA_NAP03'
            Title.Alignment = taCenter
            Width = 20
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'TA_NAP04'
            Title.Alignment = taCenter
            Width = 20
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'TA_NAP05'
            Title.Alignment = taCenter
            Width = 20
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'TA_NAP06'
            Title.Alignment = taCenter
            Width = 20
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'TA_NAP07'
            Title.Alignment = taCenter
            Width = 20
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'TA_NAP08'
            Title.Alignment = taCenter
            Width = 20
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'TA_NAP09'
            Title.Alignment = taCenter
            Width = 20
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'TA_NAP10'
            Title.Alignment = taCenter
            Width = 20
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'TA_NAP11'
            Title.Alignment = taCenter
            Width = 20
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'TA_NAP12'
            Title.Alignment = taCenter
            Width = 20
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'TA_NAP13'
            Title.Alignment = taCenter
            Width = 20
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'TA_NAP14'
            Title.Alignment = taCenter
            Width = 20
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'TA_NAP15'
            Title.Alignment = taCenter
            Width = 20
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'TA_NAP16'
            Title.Alignment = taCenter
            Width = 20
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'TA_NAP17'
            Title.Alignment = taCenter
            Width = 20
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'TA_NAP18'
            Title.Alignment = taCenter
            Width = 20
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'TA_NAP19'
            Title.Alignment = taCenter
            Width = 20
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'TA_NAP20'
            Title.Alignment = taCenter
            Width = 20
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'TA_NAP21'
            Title.Alignment = taCenter
            Width = 20
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'TA_NAP22'
            Title.Alignment = taCenter
            Width = 20
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'TA_NAP23'
            Title.Alignment = taCenter
            Width = 20
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'TA_NAP24'
            Title.Alignment = taCenter
            Width = 20
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'TA_NAP25'
            Title.Alignment = taCenter
            Width = 20
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'TA_NAP26'
            Title.Alignment = taCenter
            Width = 20
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'TA_NAP27'
            Title.Alignment = taCenter
            Width = 20
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'TA_NAP28'
            Title.Alignment = taCenter
            Width = 20
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'TA_NAP29'
            Title.Alignment = taCenter
            Width = 20
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'TA_NAP30'
            Title.Alignment = taCenter
            Width = 20
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'TA_NAP31'
            Title.Alignment = taCenter
            Width = 20
            Visible = True
          end>
      end
      object Panel1: TPanel
        Left = 0
        Top = 357
        Width = 892
        Height = 59
        Align = alBottom
        TabOrder = 1
        object Label1: TLabel
          Left = 8
          Top = 10
          Width = 59
          Height = 16
          Caption = 'Bel'#233'p'#233's'
        end
        object Label2: TLabel
          Left = 8
          Top = 34
          Width = 76
          Height = 16
          Caption = 'Dolg.csop.'
        end
        object Label3: TLabel
          Left = 360
          Top = 10
          Width = 133
          Height = 16
          Caption = #214'sszes szabads'#225'g'
          Visible = False
        end
        object Label4: TLabel
          Left = 360
          Top = 34
          Width = 120
          Height = 16
          Caption = 'Kivett szabads'#225'g'
          Visible = False
        end
        object Image2: TImage
          Left = 267
          Top = 2
          Width = 50
          Height = 56
          Hint = 'Klikkre nagy'#237't'#225's'
          Center = True
          ParentShowHint = False
          Proportional = True
          ShowHint = True
          OnClick = Image2Click
        end
        object DBEdit1: TDBEdit
          Left = 90
          Top = 8
          Width = 80
          Height = 24
          DataField = 'DO_BELEP'
          DataSource = DSDolgozo
          ReadOnly = True
          TabOrder = 0
          OnEnter = DBEdit1Enter
        end
        object DBEdit2: TDBEdit
          Left = 90
          Top = 32
          Width = 170
          Height = 24
          DataField = 'DO_CSNEV'
          DataSource = DSDolgozo
          ReadOnly = True
          TabOrder = 1
          OnEnter = DBEdit1Enter
        end
        object DBEdit3: TDBEdit
          Left = 180
          Top = 8
          Width = 80
          Height = 24
          DataField = 'DO_RENDSZ'
          DataSource = DSDolgozo
          ReadOnly = True
          TabOrder = 2
          OnEnter = DBEdit1Enter
        end
        object DBEdit4: TDBEdit
          Left = 496
          Top = 8
          Width = 49
          Height = 24
          DataField = 'DO_SZABI'
          DataSource = DSDolgozo
          ReadOnly = True
          TabOrder = 3
          Visible = False
          OnEnter = DBEdit1Enter
        end
        object DBEdit5: TDBEdit
          Left = 496
          Top = 32
          Width = 49
          Height = 24
          DataField = 'DO_KSZAB'
          DataSource = DSDolgozo
          ReadOnly = True
          TabOrder = 4
          Visible = False
          OnEnter = DBEdit1Enter
        end
        object ComboBox1: TComboBox
          Left = 744
          Top = 8
          Width = 145
          Height = 24
          Style = csDropDownList
          DropDownCount = 12
          TabOrder = 5
          OnChange = ComboBox1Change
          Items.Strings = (
            'Janu'#225'r'
            'Febru'#225'r'
            'M'#225'rcius'
            #193'prilis'
            'M'#225'jus'
            'J'#250'nius'
            'J'#250'lius'
            'Augusztus'
            'Szeptember'
            'Okt'#243'ber'
            'November'
            'December')
        end
        object ComboBox2: TComboBox
          Left = 744
          Top = 32
          Width = 145
          Height = 24
          Style = csDropDownList
          DropDownCount = 20
          TabOrder = 6
          OnChange = ComboBox1Change
          Items.Strings = (
            'Janu'#225'r'
            'Febru'#225'r'
            'M'#225'rcius'
            #193'prilis'
            'M'#225'jus'
            'J'#250'nius'
            'J'#250'lius'
            'Augusztus'
            'Szeptember'
            'Okt'#243'ber'
            'November'
            'December')
        end
        object CheckBox1: TCheckBox
          Left = 640
          Top = 32
          Width = 49
          Height = 17
          Caption = 'Ma'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 7
          OnClick = CheckBox1Click
        end
        object meEv: TMaskEdit
          Left = 692
          Top = 8
          Width = 46
          Height = 24
          TabOrder = 8
          Text = '2017'
        end
      end
      object Button1: TButton
        Left = 248
        Top = 216
        Width = 75
        Height = 25
        Caption = 'Button1'
        TabOrder = 2
        Visible = False
        OnClick = Button1Click
      end
    end
    object TabSheet9: TTabSheet
      Caption = 'Szerviz'
      ImageIndex = 8
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Memo8: TMemo
        Left = 0
        Top = 0
        Width = 892
        Height = 416
        Hint = 'Dupla klikkre v'#225'lt'#225's norm'#225'l '#233's maxim'#225'lis ablakm'#233'ret k'#246'z'#246'tt.'
        Align = alClient
        Color = clInfoBk
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Courier New'
        Font.Style = [fsBold]
        Lines.Strings = (
          'Fejleszt'#233's alatt.')
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ScrollBars = ssBoth
        ShowHint = True
        TabOrder = 0
        WordWrap = False
        OnDblClick = Memo1DblClick
      end
    end
  end
  object TDolgozo: TADODataSet
    Connection = EgyebDlg.ADOConnectionKozos
    CursorType = ctStatic
    CommandText = 'select * from DOLGOZO'#13#10
    Parameters = <>
    Left = 120
    Top = 128
  end
  object DSDolgozo: TDataSource
    DataSet = TTavollet
    Left = 192
    Top = 72
  end
  object TTavollet: TADODataSet
    Tag = 1
    Connection = EgyebDlg.ADOConnectionAlap
    CursorType = ctStatic
    AfterScroll = TTavolletAfterScroll
    CommandText = 'select * from TAVOLLET'
    Parameters = <>
    Left = 176
    Top = 128
    object TTavolletTA_DOKOD: TStringField
      FieldName = 'TA_DOKOD'
      Size = 5
    end
    object TTavolletTA_DONEV: TStringField
      FieldName = 'TA_DONEV'
      Size = 45
    end
    object TTavolletTA_HONAP: TStringField
      FieldName = 'TA_HONAP'
      FixedChar = True
      Size = 2
    end
    object TTavolletTA_NAP01: TStringField
      FieldName = 'TA_NAP01'
      Size = 10
    end
    object TTavolletTA_NAP02: TStringField
      FieldName = 'TA_NAP02'
      Size = 10
    end
    object TTavolletTA_NAP03: TStringField
      FieldName = 'TA_NAP03'
      Size = 10
    end
    object TTavolletTA_NAP04: TStringField
      FieldName = 'TA_NAP04'
      Size = 10
    end
    object TTavolletTA_NAP05: TStringField
      FieldName = 'TA_NAP05'
      Size = 10
    end
    object TTavolletTA_NAP06: TStringField
      FieldName = 'TA_NAP06'
      Size = 10
    end
    object TTavolletTA_NAP07: TStringField
      FieldName = 'TA_NAP07'
      Size = 10
    end
    object TTavolletTA_NAP08: TStringField
      FieldName = 'TA_NAP08'
      Size = 10
    end
    object TTavolletTA_NAP09: TStringField
      FieldName = 'TA_NAP09'
      Size = 10
    end
    object TTavolletTA_NAP10: TStringField
      FieldName = 'TA_NAP10'
      Size = 10
    end
    object TTavolletTA_NAP11: TStringField
      FieldName = 'TA_NAP11'
      Size = 10
    end
    object TTavolletTA_NAP12: TStringField
      FieldName = 'TA_NAP12'
      Size = 10
    end
    object TTavolletTA_NAP13: TStringField
      FieldName = 'TA_NAP13'
      Size = 10
    end
    object TTavolletTA_NAP14: TStringField
      FieldName = 'TA_NAP14'
      Size = 10
    end
    object TTavolletTA_NAP15: TStringField
      FieldName = 'TA_NAP15'
      Size = 10
    end
    object TTavolletTA_NAP16: TStringField
      FieldName = 'TA_NAP16'
      Size = 10
    end
    object TTavolletTA_NAP17: TStringField
      FieldName = 'TA_NAP17'
      Size = 10
    end
    object TTavolletTA_NAP18: TStringField
      FieldName = 'TA_NAP18'
      Size = 10
    end
    object TTavolletTA_NAP19: TStringField
      FieldName = 'TA_NAP19'
      Size = 10
    end
    object TTavolletTA_NAP20: TStringField
      FieldName = 'TA_NAP20'
      Size = 10
    end
    object TTavolletTA_NAP21: TStringField
      FieldName = 'TA_NAP21'
      Size = 10
    end
    object TTavolletTA_NAP22: TStringField
      FieldName = 'TA_NAP22'
      Size = 10
    end
    object TTavolletTA_NAP23: TStringField
      FieldName = 'TA_NAP23'
      Size = 10
    end
    object TTavolletTA_NAP24: TStringField
      FieldName = 'TA_NAP24'
      Size = 10
    end
    object TTavolletTA_NAP25: TStringField
      FieldName = 'TA_NAP25'
      Size = 10
    end
    object TTavolletTA_NAP26: TStringField
      FieldName = 'TA_NAP26'
      Size = 10
    end
    object TTavolletTA_NAP27: TStringField
      FieldName = 'TA_NAP27'
      Size = 10
    end
    object TTavolletTA_NAP28: TStringField
      FieldName = 'TA_NAP28'
      Size = 10
    end
    object TTavolletTA_NAP29: TStringField
      FieldName = 'TA_NAP29'
      Size = 10
    end
    object TTavolletTA_NAP30: TStringField
      FieldName = 'TA_NAP30'
      Size = 10
    end
    object TTavolletTA_NAP31: TStringField
      FieldName = 'TA_NAP31'
      Size = 10
    end
    object TTavolletTA_AKTIV: TIntegerField
      FieldName = 'TA_AKTIV'
    end
    object TTavolletTA_O_X: TIntegerField
      FieldName = 'TA_O_X'
    end
    object TTavolletTA_O_SZ: TIntegerField
      FieldName = 'TA_O_SZ'
    end
    object TTavolletTA_O_B: TIntegerField
      FieldName = 'TA_O_B'
    end
    object TTavolletTA_O_EI: TIntegerField
      FieldName = 'TA_O_EI'
    end
    object TTavolletTA_O_EL: TIntegerField
      FieldName = 'TA_O_EL'
    end
    object TTavolletTA_BERKOD: TStringField
      FieldName = 'TA_BERKOD'
      Size = 8
    end
    object TTavolletTA_CSKOD: TStringField
      FieldName = 'TA_CSKOD'
      Size = 3
    end
    object TTavolletTA_CSNEV: TStringField
      FieldName = 'TA_CSNEV'
      Size = 40
    end
    object TTavolletDO_NAME: TStringField
      FieldKind = fkLookup
      FieldName = 'DO_NAME'
      LookupDataSet = TDolgozo
      LookupKeyFields = 'DO_KOD'
      LookupResultField = 'DO_NAME'
      KeyFields = 'TA_DOKOD'
      Lookup = True
    end
    object TTavolletDO_BELEP: TStringField
      FieldKind = fkLookup
      FieldName = 'DO_BELEP'
      LookupDataSet = TDolgozo
      LookupKeyFields = 'DO_KOD'
      LookupResultField = 'DO_BELEP'
      KeyFields = 'TA_DOKOD'
      Lookup = True
    end
    object TTavolletDO_RENDSZ: TStringField
      FieldKind = fkLookup
      FieldName = 'DO_RENDSZ'
      LookupDataSet = TDolgozo
      LookupKeyFields = 'DO_KOD'
      LookupResultField = 'DO_RENDSZ'
      KeyFields = 'TA_DOKOD'
      Lookup = True
    end
    object TTavolletDO_CSNEV: TStringField
      FieldKind = fkLookup
      FieldName = 'DO_CSNEV'
      LookupDataSet = TDolgozo
      LookupKeyFields = 'DO_KOD'
      LookupResultField = 'DO_CSNEV'
      KeyFields = 'TA_DOKOD'
      Lookup = True
    end
    object TTavolletDO_SZABI: TIntegerField
      FieldKind = fkLookup
      FieldName = 'DO_SZABI'
      LookupDataSet = TDolgozo
      LookupKeyFields = 'DO_KOD'
      LookupResultField = 'DO_SZABI'
      KeyFields = 'TA_DOKOD'
      Lookup = True
    end
    object TTavolletDO_KSZAB: TIntegerField
      FieldKind = fkLookup
      FieldName = 'DO_KSZAB'
      LookupDataSet = TDolgozo
      LookupKeyFields = 'DO_KOD'
      LookupResultField = 'DO_KSZAB'
      KeyFields = 'TA_DOKOD'
      Lookup = True
    end
  end
  object TDFOTO: TADOQuery
    Connection = EgyebDlg.ADOConnectionKozos
    Parameters = <
      item
        Name = 'ID'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 5
        Value = Null
      end>
    SQL.Strings = (
      'select * from dolgozofoto'
      'where  (do_kod= :ID)')
    Left = 324
    Top = 159
    object TDFOTODO_KOD: TStringField
      FieldName = 'DO_KOD'
      Size = 5
    end
    object TDFOTODO_FOTO: TBlobField
      FieldName = 'DO_FOTO'
    end
  end
  object TNapok: TADOQuery
    Connection = EgyebDlg.ADOConnectionKozos
    Parameters = <>
    SQL.Strings = (
      'select * from napok'
      'order by na_datum')
    Left = 368
    Top = 150
    object TNapokNA_ORSZA: TStringField
      FieldName = 'NA_ORSZA'
      Size = 3
    end
    object TNapokNA_DATUM: TDateTimeField
      FieldName = 'NA_DATUM'
    end
    object TNapokNA_UNNEP: TIntegerField
      FieldName = 'NA_UNNEP'
    end
    object TNapokNA_MEGJE: TStringField
      FieldName = 'NA_MEGJE'
      Size = 100
    end
    object TNapokNA_EV: TIntegerField
      FieldName = 'NA_EV'
    end
    object TNapokNA_HO: TIntegerField
      FieldName = 'NA_HO'
    end
  end
  object Csoportok: TADODataSet
    Tag = 1
    Connection = EgyebDlg.ADOConnectionAlap
    CursorType = ctStatic
    CommandText = 'select DISTINCT TA_CSNEV from TAVOLLET order by TA_CSNEV '
    Parameters = <>
    Left = 264
    Top = 72
    object CsoportokTA_CSNEV: TStringField
      FieldName = 'TA_CSNEV'
      Size = 40
    end
  end
  object DSFelhasznalo: TDataSource
    DataSet = QFelhasznalo
    Left = 44
    Top = 67
  end
  object QFelhasznalo: TADOQuery
    Connection = EgyebDlg.ADOConnectionKozos
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      
        'SELECT JE_FEKOD, JE_FENEV, JE_BEDAT, JE_BEIDO, JE_SWVER FROM JEL' +
        'SZO')
    Left = 44
    Top = 119
    object QFelhasznaloJE_FEKOD: TStringField
      FieldName = 'JE_FEKOD'
      Size = 5
    end
    object QFelhasznaloJE_FENEV: TStringField
      DisplayLabel = 'N'#233'v'
      DisplayWidth = 40
      FieldName = 'JE_FENEV'
      Size = 80
    end
    object QFelhasznaloJE_BEDAT: TStringField
      DisplayLabel = 'Bel'#233'p'#233's d'#225'.'
      FieldName = 'JE_BEDAT'
      Size = 11
    end
    object QFelhasznaloJE_BEIDO: TStringField
      DisplayLabel = 'Ideje'
      FieldName = 'JE_BEIDO'
      Size = 5
    end
    object QFelhasznaloJE_SWVER: TStringField
      DisplayLabel = 'Ver.'
      FieldName = 'JE_SWVER'
      Size = 10
    end
  end
end
