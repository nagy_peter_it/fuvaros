unit MegbizasUjFm;

interface

uses
	SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
	StdCtrls, Forms, DBCtrls, DB, DBGrids, DBTables, Grids, ExtCtrls, Buttons, DateUtils,
   ShellApi, Dialogs, J_FOFORMUJ, ADODB, Mask, Menus, Variants, ImgList, KozosTipusok;
type
	TMegbizasUjFmDlg = class(TJ_FOFORMUJDLG)
    BitBtn1: TBitBtn;
	 BitBtn2: TBitBtn;
    jaratkod: TMaskEdit;
    jaratszam: TMaskEdit;
    BitBtn3: TBitBtn;
    M2: TMaskEdit;
    BitBtn5: TBitBtn;
    BitBtn6: TBitBtn;
    BitBtn7: TBitBtn;
    BitBtn8: TBitBtn;
    PopupMenu1: TPopupMenu;
	 DatumAtiras: TMenuItem;
    BitBtn9: TBitBtn;
    ColorDialog2: TColorDialog;
    Szinbeallitas: TMenuItem;
    Timer1: TTimer;
    QueryUj3: TADOQuery;
    M3: TMaskEdit;
    BitBtn4: TBitBtn;
	 BitBtn10: TBitBtn;
    ImageList1: TImageList;
    BitBtn11: TBitBtn;
    BitBtn12: TBitBtn;
    BitBtn13: TBitBtn;
    CheckBox1: TCheckBox;
    QueryAtcsat: TADOQuery;
    CheckBox2: TCheckBox;
    CheckBox3: TCheckBox;
    Nemnormljratltrehozs1: TMenuItem;
    Query01: TADOQuery;
    Query02: TADOQuery;
    SMSkldssofrnek1: TMenuItem;
    SMSkplds1: TMenuItem;
    N1: TMenuItem;
    BitBtn14: TBitBtn;
    N2: TMenuItem;
    pmiCMROK: TMenuItem;
    pmiEKROK: TMenuItem;
    Mercedespozcijelents1: TMenuItem;
	  procedure FormCreate(Sender: TObject);override;
	 procedure Adatlap(cim, kod : string);override;
	 procedure ButtonKuldClick(Sender: TObject);override;
	 procedure DataSource1DataChange(Sender: TObject; Field: TField);override;
	 procedure Mezoiro;override;
	 procedure BitBtn1Click(Sender: TObject);
	 procedure BitBtn2Click(Sender: TObject);
	 procedure ButtonTorClick(Sender: TObject); override;
	 procedure FormResize(Sender: TObject);override;
	 procedure BitBtn5Click(Sender: TObject);
	 procedure DBGrid2DrawColumnCell(Sender: TObject; const Rect: TRect;
	   DataCol: Integer; Column: TColumn; State: TGridDrawState);override;
	 procedure BitBtn6Click(Sender: TObject);
	 procedure BitBtn7Click(Sender: TObject);
	 procedure BitBtn8Click(Sender: TObject);
	 procedure DatumAtirasClick(Sender: TObject);
	 procedure BitBtn9Click(Sender: TObject);
	 procedure SzinbeallitasClick(Sender: TObject);
	 procedure Timer1Timer(Sender: TObject);
	 procedure StatuszBeiras(mbkod : string);
	 procedure StatuszTorles;
	 procedure SummaTolto;
    function  AutoTolto(d1, d2 : string) : boolean;
	 procedure BitBtn4Click(Sender: TObject);
	 procedure BitBtn10Click(Sender: TObject);
	 procedure BitBtn11Click(Sender: TObject);
	 procedure BitBtn12Click(Sender: TObject);
	 function  KeresChange(mezo, szoveg : string) : string; override;
    procedure BitBtn13Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure CheckBox1Click(Sender: TObject);
    procedure Atcsatolas(esakod,usakod: string);
    procedure BitBtn8MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Nemnormljratltrehozs1Click(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);override;
    procedure AutoToltoVisszavon;
    procedure Elohuzas;
    procedure Fellerakas(tipus : integer);
    procedure SMSkldssofrnek1Click(Sender: TObject);
    procedure SMSKuldes(mbkod, soforkod1, soforkod2: string);
   // procedure UresSMSKuldes;
    procedure SMSkplds1Click(Sender: TObject);
    procedure BitBtn14Click(Sender: TObject);
    procedure pmiCMROKClick(Sender: TObject);
    procedure PopupMenu1Popup(Sender: TObject);
    procedure pmiEKROKClick(Sender: TObject);
    procedure Mercedespozcijelents1Click(Sender: TObject);
	 private
		statusszin	: array [1..10, 1..2] of string;
		checksumma	: integer;
		elotag		: string;
       szur140     :string;
	 public
		ret_mskod	: string;
    ret_msid	: string;
		ret_mbkod	: string;
		ret_mssorsz	: string;
    ret_rendsz: string;
    alap        : boolean;
	end;

const
  STATU_kizaras = ' AND (MB_STATU <> 300) ';  // A "T�r�lt" st�tusz� megb�z�sokat nem akarjuk l�tni itt
var
	MegbizasUjFmDlg : TMegbizasUjFmDlg;

implementation

uses
	Egyeb, J_SQL, MegbizasBe, J_VALASZTO, Viszonybe, Kozos, Segedbe2, Idobe,
	GepStatFm, Jaratbe, RendVal, CsatLIstFm, GKErvenyesseg,Szamlabe, clipbrd,
   MegJaratBe, KOzos_Local, JaratSegedDat, UzenetSzerkeszto, FlottaHelyzet;

{$R *.DFM}

procedure TMegbizasUjFmDlg.FormCreate(Sender: TObject);
var
	i           : integer;
   szur140     : string;
begin
//  Application.CreateForm(TFGKErvenyesseg, FGKErvenyesseg);
	ret_mskod	:= '';
	ret_msid	:= '';
	ret_mbkod	:= '';
   ret_mssorsz := '';
	Inherited FormCreate(Sender);
	EgyebDlg.SeTADOQueryDatabase(QueryUj3);
	EgyebDlg.SeTADOQueryDatabase(Query01);
	EgyebDlg.SeTADOQueryDatabase(Query02);
	EgyebDlg.SeTADOQueryDatabase(QueryAtcsat);
	kellujures	:= false;

	AddSzuromezoRovid('MS_TIPUS',  'MEGSEGED');
	AddSzuromezoRovid('MS_TINEV',  'MEGSEGED');
	AddSzuromezoRovid('MS_STATN',  'MEGSEGED');
	AddSzuromezoRovid('MS_EXSTR',  'MEGSEGED');
	AddSzuromezoRovid('MS_FORSZ',  'MEGSEGED');
	AddSzuromezoRovid('MS_ORSZA',  'MEGSEGED');
	AddSzuromezoRovid('MS_FAJTA',  'MEGSEGED');
	AddSzuromezoRovid('MS_RENSZ',  'MEGSEGED','1');
	AddSzuromezoRovid('MS_POTSZ',  'MEGSEGED','1');
	AddSzuromezoRovid('MS_GKKAT',  'MEGSEGED');
	AddSzuromezoRovid('MS_FELNEV', 'MEGSEGED');
	AddSzuromezoRovid('MS_FELTEL', 'MEGSEGED');
	AddSzuromezoRovid('MS_LERNEV', 'MEGSEGED');
	AddSzuromezoRovid('MS_LERTEL', 'MEGSEGED');
	AddSzuromezoRovid('MS_KIFON',  'MEGSEGED' );
	AddSzuromezoRovid('MS_SNEV1',  '' );
	AddSzuromezoRovid('MS_SNEV2',  '' );

	AddSzuromezoRovid('MB_VEKOD',  'MEGBIZAS');
	AddSzuromezoRovid('MB_VENEV',  'MEGBIZAS');
	AddSzuromezoRovid('MB_RENSZ',  'MEGBIZAS','1');
	AddSzuromezoRovid('MB_EXSTR',  'MEGBIZAS');
	AddSzuromezoRovid('MB_EXPOR',  'MEGBIZAS');
	AddSzuromezoRovid('MB_GKKAT',  'MEGBIZAS');
	AddSzuromezoRovid('MB_POTSZ',  'MEGBIZAS','1');
	AddSzuromezoRovid('MB_ALNEV',  'MEGBIZAS');
	AddSzuromezoRovid('MB_HUTOS',  'MEGBIZAS');
	AddSzuromezoRovid('MB_ALV',    'MEGBIZAS');
	AddSzuromezoRovid('MB_FENEV',  'MEGBIZAS');
	AddSzuromezoRovid('MB_EDIJ' ,  'MEGBIZAS');
	AddSzuromezoRovid('MB_STATU',  'MEGBIZAS');
  AddSzuromezoRovid('MB_INTREF',  '');
	AddSzuromezoRovid('MS_CMROK',  '','1');
	AddSzuromezoRovid('MS_EKROK', '','1');
  AddSzuromezoRovid('MB_NOCSOP', '', '1');

	// Bel�p�skor t�r�lj�k ki a keres�s tartalm�t
//	SzuresUrites;

	M2.Text		:= EgyebDlg.MaiDatum;
	M3.Text		:= EgyebDlg.MaiDatum;
//alapstr		:= 'SELECT * FROM MEGSEGED, MEGBIZAS WHERE MS_MBKOD = MB_MBKOD AND MS_DATUM = '''+M2.Text+''' ';

  if EgyebDlg.stralap then
  	ALAPSTR		:= 'SELECT * FROM MEGSEGED, MEGBIZAS WHERE MS_MBKOD = MB_MBKOD '+ STATU_kizaras + szur140
  else
  	ALAPSTR		:= 'SELECT * FROM MEGSEGED, MEGBIZAS WHERE MS_MBKOD = MB_MBKOD AND MS_DATUM = '''+M2.Text+''' '+ STATU_kizaras + szur140;

  EgyebDlg.stralap:=False;
	FelTolto('�j megb�z�s felvitele ', 'Megb�z�s adatok m�dos�t�sa ',
			'Megb�z�sok list�ja', 'MB_MBKOD', alapstr, 'MEGBIZAS', QUERY_ADAT_TAG );
   multiselect         := true;
	kulcsmezo			:= 'MS_MSKOD';
	width				:= DLG_WIDTH;
	height				:= DLG_HEIGHT;
	BitBtn1.Parent		:= PanelBottom;
	BitBtn2.Parent		:= PanelBottom;
	BitBtn3.Parent		:= PanelBottom;
	M2.Parent			:= PanelBottom;
	M3.Parent			:= PanelBottom;
	BitBtn5.Parent		:= PanelBottom;
	BitBtn4.Parent		:= PanelBottom;
	BitBtn6.Parent		:= PanelBottom;
	BitBtn7.Parent		:= PanelBottom;
	BitBtn8.Parent		:= PanelBottom;
	BitBtn9.Parent		:= PanelBottom;
	BitBtn10.Parent		:= PanelBottom;
	BitBtn11.Parent		:= PanelBottom;
	BitBtn12.Parent		:= PanelBottom;
	BitBtn13.Parent		:= PanelBottom;
  BitBtn14.Parent		:= PanelBottom;
	CheckBox1.Parent 	:= PanelBottom;
	CheckBox2.Parent 	:= PanelBottom;
	CheckBox3.Parent 	:= PanelBottom;
	BitBtn13.Visible 	:= EgyebDlg.NavC_Kapcs_OK;
	DBGrid2.PopupMenu	:= PopupMenu1;
  BitBtn11.Visible:=EgyebDlg.user_super;
	// A st�tussz�nek felt�lt�se
	for i := 1 to 10 do begin
		statusszin[i, 1] := '';
		statusszin[i, 2] := IntToStr(clAqua);
	end;
	Query_Run(EgyebDlg.QueryKozos, 'SELECT SZ_ALKOD, SZ_KODHO FROM SZOTAR WHERE SZ_FOKOD = ''960'' ' );
	i := 1;
	while ( ( i <= 10 ) and ( not EgyebDlg.QueryKozos.Eof ) ) do begin
		statusszin[i, 1] := EgyebDlg.QueryKozos.FieldByName('SZ_ALKOD').AsString;
		statusszin[i, 2] := EgyebDlg.QueryKozos.FieldByName('SZ_KODHO').AsString;
		Inc(i);
		EgyebDlg.QueryKozos.Next;
	end;
	SummaTolto;
  if EgyebDlg.UTSZAKASZ_MEGJELENITES_HASZNALATA then
      BitBtn14.Visible:= True
  else BitBtn14.Visible:= False;
//	uresszures	:= false;
	elotag	   	:= EgyebDlg.Read_SZGrid('CEG', '310');
end;

procedure TMegbizasUjFmDlg.ButtonKuldClick(Sender: TObject);
begin
	Inherited ButtonKuldClick(Sender);
   ret_mskod	:= Query1.FieldByName('MS_MSKOD').AsString;
   ret_msid	:= Query1.FieldByName('MS_ID').AsString;
   ret_mbkod	:= Query1.FieldByName('MS_MBKOD').AsString;
   ret_mssorsz:= Query1.FieldByName('MS_SORSZ').AsString;
   ret_rendsz := Query1.FieldByName('MB_RENSZ').AsString;
end;

procedure TMegbizasUjFmDlg.Adatlap(cim, kod : string);
var
	oldmezo1, Modosito: string;
begin
	if kod <> '' then begin
		if Query1.FieldByName('MB_AFKOD').AsString <> '' then begin
			// Ellen�rizz�k, hogy ki dolgozik m�g ezen a megb�z�son
			if Query1.FieldByName('MB_AFKOD').AsString <> EgyebDlg.user_code then begin
        Modosito:=Query_Select('JELSZO', 'JE_FEKOD', Query1.FieldByName('MB_AFKOD').AsString, 'JE_FENEV');
				NoticeKi('A megb�z�st �ppen m�s m�dos�tja! ('+Modosito+')');
				Exit;
			end;
		end;
	end;
	oldmezo1	:= Query1.FieldByName('MS_MSKOD').AsString;
///	StatuszBeiras(kod);
	Application.CreateForm(TMegbizasbeDlg, AlForm);
	Inherited Adatlap(cim, kod );
///	StatuszTorles;
	SummaTolto;
	if kod <> '' then begin
		ModLocate (Query1, 'MS_MSKOD', oldmezo1 );
	end;
  // --- 2018.01.29. Kivettem: A Megbizasbe.Elkuld-ben is benne van.
	// GepStatTolto(Query1.FieldByName('MS_RENSZ').AsString);
	// PotStatTolto(Query1.FieldByName('MS_POTSZ').AsString);
  // ------------------------------------------
     // MegbizasKieg(Query1.FieldByName('MB_MBKOD').AsString); �tker�lt a Megbizasbe / "Elk�ld"-be
end;

procedure TMegbizasUjFmDlg.Mercedespozcijelents1Click(Sender: TObject);
var
  mbkodlist: TStringList;
  i: integer;
  mbkod, Res, Uzenet: string;
begin
   if NoticeKi('Indulhat a poz�ci� jelent�s a kiv�lasztott megb�z�sokr�l?', NOT_QUESTION) <> 0 then begin
      Exit;
      end;
   // V1: egy db megb�z�s, clipboard-ra
   // Clipboard.Clear;
   // Clipboard.AsText:= EgyebDlg.MerciPozicioJelentesHTML (Query1.FieldByName('MB_MBKOD').AsInteger);
   // NoticeKi('Adat a v�g�lapon.');

   mbkodlist:= TStringList.Create;
   mbkodlist.Sorted:= True;
   mbkodlist.Duplicates:= dupIgnore;
   try
     if SelectList.Count < 1 then begin   // Csak az aktu�lis rekord legyen kiv�lasztva
          mbkodlist.Add(Query1.FieldByName('MS_MBKOD').AsString);
          end  // if
     else begin
         for i := 0 to SelectList.Count - 1 do begin
            mbkod:= Query_select('MEGSEGED', 'MS_MSKOD', SelectList[i], 'MS_MBKOD');
            mbkodlist.Add(mbkod);
            end; // for
         end;  // else
     EgyebDlg.MerciPozicioJelentesKuld( mbkodlist, EgyebDlg.user_dokod, Uzenet);  // aki gener�lta, az kapja!
   finally
      if mbkodlist <> nil then mbkodlist.free;
      end;  // try-finally
   // A kijel�l�sek megsz�ntet�se
   SelectList.Clear;
   DBGrid2.Refresh;
end;

procedure TMegbizasUjFmDlg.Mezoiro;
begin
	Query1.FieldByname('STATUSZ').AsString 	:= EgyebDlg.Read_SZGrid('950', Query1.FieldByName('MB_STATU').AsString);
end;

procedure TMegbizasUjFmDlg.DataSource1DataChange(Sender: TObject; Field: TField);
begin
	// A gombok megjelen�t�s�nek vez�rl�se
	oldrecord	:= Query1.FieldByName('MS_MSKOD').AsString;
	DBGrid2.Refresh;
	//if Query1.FieldByName('MB_JAKOD').AsString = '' then begin
	if (Query1.FieldByName('MS_JAKOD').AsString <> '')or(Query1.FieldByName('MB_JAKOD').AsString <> '') then begin
    // if EgyebDlg.user_super then
    if EgyebDlg.user_super or (GetRightTag(598, False) <> RG_NORIGHT)then   // .. vagy megb�z�s admin
		  BitBtn2.Show;
  end;
	if (Query1.FieldByName('MS_JAKOD').AsString = '')and(Query1.FieldByName('MB_JAKOD').AsString = '') then begin
		BitBtn1.Show;
		BitBtn2.Hide;
		BitBtn8.Hide;
	end else begin
		BitBtn1.Hide;
		BitBtn8.Show;
	end;
end;

procedure TMegbizasUjFmDlg.BitBtn1Click(Sender: TObject);
var
	szurosor	: integer;
	sorsz		: integer;
  MBDatum, JAKezd,JAVege, GKKAT:string;
  msfajko: string;
begin
	if not voltalt then begin
		voltalt  := true;
		Exit;
	end;
	// Hozz�rendelj�k a rekordot a j�rathoz
	//if Query1.FieldByName('MB_JAKOD').AsString <> '' then begin
	if Query1.FieldByName('MS_JAKOD').AsString <> '' then begin
		NoticeKi('A kiv�lasztott megb�z�s m�r j�rathoz tartozik!');
		Exit;
	end;
	// Megjelen�tj�k a rendsz�mhoz tartoz� j�ratokat, amib�l ki kell v�lasztani a megfelel�t
	//if Query1.FieldByName('MB_RENSZ').AsString = '' then begin
	if Query1.FieldByName('MS_RENSZ').AsString = '' then begin
		NoticeKi('Nincs megadva rendsz�m a megb�z�shoz!');
		Exit;
	end;
	if Query1.FieldByName('MB_VEKOD').AsString = '' then begin
		NoticeKi('Nincs megadva megb�z� a megb�z�shoz!');
		Exit;
	end;

	// A j�rat sz�r�s be�ll�t�sa
	EgyebDlg.SzuroGrid.RowCount 			:= EgyebDlg.SzuroGrid.RowCount + 1;
	szurosor								:= EgyebDlg.SzuroGrid.RowCount - 1 ;
	EgyebDlg.SzuroGrid.Cells[0,szurosor] 	:= '190';
	//EgyebDlg.SzuroGrid.Cells[1,szurosor] 	:= 'JA_RENDSZ = '''+Query1.FieldByName('MS_RENSZ').AsString+''' ';

  msfajko:=Query1.FieldByName('MS_FAJKO').AsString   ;
  if msfajko<>'0' then msfajko:='1';
  if CheckBox3.Checked then
  	EgyebDlg.SzuroGrid.Cells[1,szurosor] 	:= 'JA_RENDSZ = '''+Query1.FieldByName('MB_RENSZ').AsString+''' and JA_JKEZD<='''+ Query1.FieldByName('MS_DATUM').AsString+
    ''' and JA_JVEGE>='''+ Query1.FieldByName('MS_DATUM').AsString+'''  and JA_FAJKO='+msfajko
//    ''' and JA_JVEGE>='''+ Query1.FieldByName('MS_DATUM').AsString+'''  and JA_FAJKO='+Query1.FieldByName('MS_FAJKO').AsString
//  	EgyebDlg.SzuroGrid.Cells[1,szurosor] 	:= 'JA_RENDSZ = '''+Query1.FieldByName('MB_RENSZ').AsString+''' and JA_JKEZD<='''+ Query1.FieldByName('MB_DATUM').AsString+
//    ''' and JA_JVEGE>='''+ Query1.FieldByName('MB_DATUM').AsString+'''  and JA_FAJKO='+Query1.FieldByName('MS_FAJKO').AsString
  else
  	EgyebDlg.SzuroGrid.Cells[1,szurosor] 	:= 'JA_RENDSZ = '''+Query1.FieldByName('MS_RENSZ').AsString+''' and JA_FAJKO='+msfajko;
//  	EgyebDlg.SzuroGrid.Cells[1,szurosor] 	:= 'JA_RENDSZ = '''+Query1.FieldByName('MS_RENSZ').AsString+''' and JA_FAJKO='+Query1.FieldByName('MS_FAJKO').AsString;

	EgyebDlg.megbizaskod					:= Query1.FieldByName('MB_MBKOD').AsString;

	JaratValaszto(jaratkod, jaratszam);

	EgyebDlg.megbizaskod					:= '';
	// A sz�r�s t�rl�se
	GridTorles(EgyebDlg.SzuroGrid, szurosor);
	if jaratkod.Text = '' then begin
		Exit;
	end;
	// Ellen�rizz�k a j�rat ellen�rz�tts�g�t
	if EllenorzottJarat(jaratkod.Text) > 0 then begin
		Exit;
	end;

  // D�tum ellen�rz�se
  MBDatum:= Query1.FieldByName('MS_DATUM').AsString;
  JAKezd:= Query_Select('JARAT','JA_KOD',jaratkod.Text,'JA_JKEZD');
  JAVege:= Query_Select('JARAT','JA_KOD',jaratkod.Text,'JA_JVEGE');
  if not ((JAKezd<=MBDatum)and(JAVege>=MBDatum)) then
  begin
    if NoticeKi('A megb�z�st d�tuma nem esik bele a j�rat intervallum�ba! Folytatja a m�veletet?  '+JAKezd+'-'+JAVege,1)= -1 then
      exit;
  end;

	// L�trehozzuk a viszony-t �s hozz�rendelj�k a j�rathoz
	Application.CreateForm(TViszonybeDlg, ViszonybeDlg);
	ViszonybeDlg.ToltMegbizas(Query1.FieldByName('MB_MBKOD').AsString ,jaratkod.Text);
	ViszonybeDlg.ShowModal;

  Try
	 if ViszonybeDlg.ret_kod <> '' then begin
		// L�trej�tt az �j viszonylat, bejegyezz�k a megb�z�sba is
		if not Query_Update( Query2, 'MEGBIZAS', [
			'MB_JAKOD', ''''+jaratkod.Text+'''',
			'MB_JASZA', ''''+jaratszam.Text+'''',
//           'MB_STATU', ''''+'130'+'''',
			'MB_VIKOD', ''''+ViszonybeDlg.ret_kod+''''
		], ' WHERE MB_MBKOD = '+Query1.FieldByName('MB_MBKOD').AsString) then Raise Exception.Create('');

		if not Query_Update( Query2, 'MEGSEGED', [
			'MS_JAKOD', ''''+jaratkod.Text+'''',
			'MS_JASZA', ''''+jaratszam.Text+''''
		], ' WHERE MS_MBKOD = '+Query1.FieldByName('MB_MBKOD').AsString+' and MS_FAJKO=0') then Raise Exception.Create('');

		MegbizasSzamlaTolto(Query1.FieldByName('MB_MBKOD').AsString);

		// A megbizasok hozz�kapcsol�sa a j�rathoz
		Query_Run( Query2,  'SELECT COUNT(*) DB FROM JARATMEGBIZAS WHERE JM_JAKOD = '''+jaratkod.Text+''' ');
		sorsz	:= StrToIntDef(Query2.FieldByName('DB').AsString, 1) + 1;
		Query_Run( Query2,  'SELECT DISTINCT MB_JAKOD, MS_MBKOD, MS_SORSZ FROM MEGSEGED, MEGBIZAS WHERE MS_MBKOD = MB_MBKOD AND MB_JAKOD <> '''' '+
			' AND MB_MBKOD = '+Query1.FieldByName('MB_MBKOD').AsString+' ORDER BY MB_JAKOD, MS_MBKOD, MS_SORSZ ', TRUE);
		while not Query2.Eof do begin
			if not Query_Insert (QueryUj3, 'JARATMEGBIZAS',
				[
				'JM_JAKOD', ''''+Query2.FieldByName('MB_JAKOD').AsString+'''',
				'JM_MBKOD', Query2.FieldByName('MS_MBKOD').AsString,
				'JM_SORSZ', IntToStr(sorsz),
				'JM_ORIGS', Query2.FieldByName('MS_SORSZ').AsString
				]) then Raise Exception.Create('');
			Inc(sorsz);
			Query2.Next;
		end;
		if not FuvardijTolto(jaratkod.Text) then begin
		 //	NoticeKi('A fuvard�j be�ll�t�sa nem siker�lt!');
		end;
	 end;
  Except
  		NoticeKi('A j�ratkapcsolat l�trehoz�sa nem siker�lt!');
	    ViszonybeDlg.ret_kod:='';
  END;
	//ViszonybeDlg.Destroy;

  // J�rat
	if ViszonybeDlg.ret_kod <> '' then begin
		Application.CreateForm(TJaratbeDlg, JaratbeDlg);
		JaratbeDlg.Tolto('J�rat m�dos�t�s',Query2.FieldByName('MB_JAKOD').AsString);

    if Query1.FieldByName('MB_ALV').AsInteger=1 then // Alv�llalkoz�
    begin
      GKKAT:=JaratbeDlg.MaskEdit9.Text;
      if GKKAT='' then
        GKKAT:= Query_Select('ALGEPKOCSI','AG_RENSZ',Query1.FieldByName('MB_RENSZ').AsString,'AG_GKKAT' );
      JaratbeDlg.GKKATEG:=GKKAT;
    end;
    if EgyebDlg.VANLANID>0 then
    begin
      Query_Run(Query2, 'SELECT FT_GKKAT FROM FUVARTABLA WHERE FT_FTKOD = '+Query1.FieldByName('MB_FUTID').AsString);
      JaratbeDlg.GKKATEG:= Query2.FieldByName('FT_GKKAT').AsString;
    end;

    EgyebDlg.AUTOSZAMLA:=True;
    // [2017-05-09 NagyP] Teljesen kikapcsolom itt az automatikus sz�mla k�sz�t�st, mert a megb�z�si d�j m�dos�t�sa eset�n nem
    // tudunk "ut�nany�lni", el�g ha a viszonylatban �t kell jav�tani a d�jat
    // [2017-05-18 NagyP] Vissza az eg�sz. Kell az automatikus sz�mla, de fuvard�j m�dos�t�skor t�r�lni fogja a sz�ml�t, a
    // viszonylatban pedig m�dos�tja
    // EgyebDlg.AUTOSZAMLA:=False;

    JaratbeDlg.TabbedNotebook1.PageIndex:=1;
		JaratbeDlg.ShowModal;
		JaratbeDlg.Destroy;
    JaratbeDlg:=nil;
  end;
	if ViszonybeDlg <> nil then ViszonybeDlg.Release;


	ModLocate (Query1, 'MS_MSKOD', Query1.FieldByName('MS_MSKOD').AsString );
	SummaTolto;
end;

procedure TMegbizasUjFmDlg.BitBtn2Click(Sender: TObject);
var
	sorsz		: integer;
  ujkod		: string;
begin
	if not voltalt then begin
		voltalt 	:= true;
		Exit;
	end;
//	if Query1.FieldByName('MB_JAKOD').AsString = '' then begin

	if (Query1.FieldByName('MS_JAKOD').AsString = '')and(Query1.FieldByName('MB_JAKOD').AsString = '') then begin
		NoticeKi('A kiv�lasztott megb�z�s m�g nem kapcsol�dik j�rathoz!');
		Exit;
	end;
	// Ellen�rizz�k a j�rat ellen�rz�tts�g�t
	//if EllenorzottJarat(Query1.FieldByName('MB_JAKOD').AsString) > 0 then begin
	if EllenorzottJarat(Query1.FieldByName('MS_JAKOD').AsString) > 0 then begin
		Exit;
	end;
	// Ellen�rizz�k a viszonylat sz�ml�zotts�g�t
	Query_Run(Query2, 'SELECT VI_UJKOD FROM VISZONY WHERE VI_VIKOD = '''+Query1.FieldByName('MB_VIKOD').AsString+''' ');
  ujkod		:= Query2.FieldByName('VI_UJKOD').AsString;
	if StrToIntDef(ujkod,0) > 0 then begin
       Query_Run(Query2, 'SELECT VI_JAKOD, MB_JAKOD FROM MEGBIZAS, VISZONY, JARAT WHERE VI_VIKOD = MB_VIKOD AND JA_KOD = MB_JAKOD '+
       	'AND MB_DATUM LIKE '''+EgyebDlg.Evszam+'%'' AND MB_MBKOD = '+Query1.FieldByName('MB_MBKOD').AsString);
       Query_Run(Query2, 'SELECT SA_FLAG, isnull(SA_KOD,'''') SA_KOD, SA_RESZE FROM SZFEJ WHERE SA_UJKOD = '+ujkod);
       if Query2.FieldByName('SA_KOD').AsString <> '' then begin  // ez lehet v�gleges�tett, vagy sztorn�zott vagy sztorn� sz�mla
				    NoticeKi('A viszonylathoz m�r tartozik v�gleges�tett sz�mla, ez�rt nem t�r�lhet�!');
            Exit;
           end; // if
       if Query2.FieldByName('SA_RESZE').AsInteger = 1 then begin  // r�sz-sz�mla
				    NoticeKi('A viszonylathoz tartoz� sz�mla gy�jt�sz�mla r�sze, ez�rt nem t�r�lhet�!');
				    Exit;
       end;  // if

      // ide m�r csak akkor jutunk, ha a sz�mla nem v�gleges�tett

      if NoticeKi('Val�ban t�r�lni k�v�nja a kijel�lt rekord j�rat kapcsolat�t?', NOT_QUESTION) <> 0 then begin
          Exit;
          end;

      // A viszonylat t�rl�se �s a MEGBIZAS rekord friss�t�se
      Query_Run(Query2, 'DELETE FROM VISZONY WHERE VI_VIKOD = '''+Query1.FieldByName('MB_VIKOD').AsString+''' ');
      // sz�mla t�rl�se
      Query_Run(Query2, 'DELETE FROM SZFEJ WHERE SA_UJKOD = '+ujkod);
      Query_Run(Query2, 'DELETE FROM SZSOR WHERE SS_UJKOD = '+ujkod);

      Query_Update( Query2, 'MEGBIZAS', [
        'MB_JAKOD', ''''+''+'''',
        'MB_JASZA', ''''+''+'''',
        // 'MB_SZKOD', ''''+''+'''',
        'MB_SAKO2', ''''+''+'''',
        'MB_VIKOD', ''''+''+''''
      ], ' WHERE MB_MBKOD = '+Query1.FieldByName('MB_MBKOD').AsString);
      Query_Update( Query2, 'MEGSEGED', [
        'MS_JAKOD', ''''+''+'''',
        'MS_JASZA', ''''+''+''''
      ], ' WHERE MS_MBKOD = '+Query1.FieldByName('MB_MBKOD').AsString);
      // A j�rat megb�z�sok t�rl�se
      Query_Run(Query2, 'DELETE FROM JARATMEGBIZAS WHERE JM_MBKOD = '''+Query1.FieldByName('MB_MBKOD').AsString+''' ');
      // �tsorsz�mozzuk a megl�v�ket
      sorsz	:= 1;
      Query_Run( Query2,  'SELECT * FROM JARATMEGBIZAS WHERE JM_JAKOD = '''+Query1.FieldByName('MB_JAKOD').AsString+''' ORDER BY JM_SORSZ ', TRUE);
      while not Query2.Eof do begin
        Query_Update (QueryUj3, 'JARATMEGBIZAS',
          [
          'JM_SORSZ', IntToStr(sorsz)
          ], ' WHERE JM_JAKOD = '''+Query1.FieldByName('MB_JAKOD').AsString+''' '+
             ' AND JM_MBKOD = '+Query2.FieldByName('JM_MBKOD').AsString +
             ' AND JM_ORIGS = '+Query2.FieldByName('JM_ORIGS').AsString );
        Inc(sorsz);
        Query2.Next;
        end; // while
      if not FuvardijTolto(Query1.FieldByName('MB_JAKOD').AsString) then begin
        NoticeKi('A fuvard�j be�ll�t�sa nem siker�lt!');
        end;
      // A t�bla friss�t�se
      ModLocate (Query1, 'MS_MSKOD', Query1.FieldByName('MS_MSKOD').AsString );
      SummaTolto;
      end; // if
end;

{procedure TMegbizasUjFmDlg.BitBtn2Click(Sender: TObject);
var
	sorsz		: integer;
   ujkod		: string;
   torolheto   : boolean;
begin
	if not voltalt then begin
		voltalt 	:= true;
		Exit;
	end;
//	if Query1.FieldByName('MB_JAKOD').AsString = '' then begin

	if (Query1.FieldByName('MS_JAKOD').AsString = '')and(Query1.FieldByName('MB_JAKOD').AsString = '') then begin
		NoticeKi('A kiv�lasztott megb�z�s m�g nem kapcsol�dik j�rathoz!');
		Exit;
	end;
	// Ellen�rizz�k a j�rat ellen�rz�tts�g�t
	//if EllenorzottJarat(Query1.FieldByName('MB_JAKOD').AsString) > 0 then begin
	if EllenorzottJarat(Query1.FieldByName('MS_JAKOD').AsString) > 0 then begin
		Exit;
	end;
	// Ellen�rizz�k a viszonylat sz�ml�zotts�g�t
	Query_Run(Query2, 'SELECT VI_UJKOD FROM VISZONY WHERE VI_VIKOD = '''+Query1.FieldByName('MB_VIKOD').AsString+''' ');
   ujkod		:= Query2.FieldByName('VI_UJKOD').AsString;
   torolheto	:= true;
	if StrToIntDef(ujkod,0) > 0 then begin
      //  Query_Run(Query2, 'SELECT VI_JAKOD, MB_JAKOD FROM MEGBIZAS, VISZONY, JARAT WHERE VI_VIKOD = MB_VIKOD AND JA_KOD = MB_JAKOD AND VI_JAKOD <> MB_JAKOD '+
      // csak akkor sz�lt, ha m�sik j�rathoz kapcsol�dott
       Query_Run(Query2, 'SELECT VI_JAKOD, MB_JAKOD FROM MEGBIZAS, VISZONY, JARAT WHERE VI_VIKOD = MB_VIKOD AND JA_KOD = MB_JAKOD '+
       	'AND MB_DATUM LIKE '''+EgyebDlg.Evszam+'%'' AND MB_MBKOD = '+Query1.FieldByName('MB_MBKOD').AsString);
       if Query2.FieldByName('VI_JAKOD').AsString = Query2.FieldByName('MB_JAKOD').AsString then begin
           // Ha nem egyezik a k�t k�d, akkor lehet t�r�lni, k�l�nben nem biztos ...
           Query_Run(Query2, 'SELECT * FROM SZFEJ WHERE SA_UJKOD = '+ujkod);
           if Query2.FieldByName('SA_FLAG').AsString = '1' then begin
           	// Csak a v�gleges�tett sz�ml�kat nem lehet t�r�lni, a storn�t lehet
				    NoticeKi('A viszonylathoz m�r tartozik sz�mla, ez�rt nem t�r�lhet�!');
				    Exit;
           end;
           if Query2.FieldByName('SA_FLAG').AsString > '1' then begin
           	// Stornos sz�ml�hoz tartoz� viszonyt nem lehet t�r�lni
           	torolheto	:= false;
           end;
       end;
	end;
	if NoticeKi('Val�ban t�r�lni k�v�nja a kijel�lt rekord j�rat kapcsolat�t?', NOT_QUESTION) <> 0 then begin
		Exit;
	end;
	// A viszonylat t�rl�se �s a MEGBIZAS rekord friss�t�se
   if torolheto then begin
		Query_Run(Query2, 'DELETE FROM VISZONY WHERE VI_VIKOD = '''+Query1.FieldByName('MB_VIKOD').AsString+''' ');
   end;
	Query_Update( Query2, 'MEGBIZAS', [
		'MB_JAKOD', ''''+''+'''',
		'MB_JASZA', ''''+''+'''',
		// 'MB_SZKOD', ''''+''+'''',
    'MB_SAKO2', ''''+''+'''',
		'MB_VIKOD', ''''+''+''''
	], ' WHERE MB_MBKOD = '+Query1.FieldByName('MB_MBKOD').AsString);
	Query_Update( Query2, 'MEGSEGED', [
		'MS_JAKOD', ''''+''+'''',
		'MS_JASZA', ''''+''+''''
	], ' WHERE MS_MBKOD = '+Query1.FieldByName('MB_MBKOD').AsString);
	// A j�rat megb�z�sok t�rl�se
	Query_Run(Query2, 'DELETE FROM JARATMEGBIZAS WHERE JM_MBKOD = '''+Query1.FieldByName('MB_MBKOD').AsString+''' ');
	// �tsorsz�mozzuk a megl�v�ket
	sorsz	:= 1;
	Query_Run( Query2,  'SELECT * FROM JARATMEGBIZAS WHERE JM_JAKOD = '''+Query1.FieldByName('MB_JAKOD').AsString+''' ORDER BY JM_SORSZ ', TRUE);
	while not Query2.Eof do begin
		Query_Update (QueryUj3, 'JARATMEGBIZAS',
			[
			'JM_SORSZ', IntToStr(sorsz)
			], ' WHERE JM_JAKOD = '''+Query1.FieldByName('MB_JAKOD').AsString+''' '+
			   ' AND JM_MBKOD = '+Query2.FieldByName('JM_MBKOD').AsString +
			   ' AND JM_ORIGS = '+Query2.FieldByName('JM_ORIGS').AsString );
		Inc(sorsz);
		Query2.Next;
	end;
	if not FuvardijTolto(Query1.FieldByName('MB_JAKOD').AsString) then begin
		NoticeKi('A fuvard�j be�ll�t�sa nem siker�lt!');
	end;
	// A t�bla friss�t�se
	ModLocate (Query1, 'MS_MSKOD', Query1.FieldByName('MS_MSKOD').AsString );
	SummaTolto;
end;
}

procedure TMegbizasUjFmDlg.ButtonTorClick(Sender: TObject);
var
	rszlist	: TStringList;
	potlist	: TStringList;
	i		: integer;
  VanJoga: boolean;
begin
	if not voltalt then begin
		voltalt 	:= true;
		Exit;
	end;
	// if not EgyebDlg.user_super then begin
  VanJoga:= EgyebDlg.user_super or (GetRightTag(598) <> RG_NORIGHT);
  if not VanJoga then begin
		NoticeKi('A m�velethez nincs elegend� jogosults�ga!');
    exit;
  end;
	rszlist	:= TStringList.Create;
	potlist	:= TStringList.Create;
	// Ellen�rizz�k a viszonylat sz�ml�zotts�g�t
	Query_Run(Query2, 'SELECT VI_UJKOD FROM VISZONY WHERE VI_VIKOD = '''+Query1.FieldByName('MB_VIKOD').AsString+''' ');
	if StrToIntDef(Query2.FieldByName('VI_UJKOD').AsString,0) > 0 then begin
		NoticeKi('A viszonylathoz m�r tartozik sz�mla, ez�rt nem t�r�lhet�!');
		Exit;
	end;
	if StrToIntDef(Query1.FieldByName('MB_XMLSZ').AsString, 0) > 0 then begin
		if NoticeKi('FIGYELEM!!! A megb�z�s m�r export�l�sra ker�lt XML-be. Folytatja a t�rl�st?', NOT_QUESTION) <> 0 then begin
			Exit;
		end;
	end;
   if NoticeKi( 'Figyelem! Csak a kijel�lt elemet k�v�nja t�r�lni (nem az eg�sz megb�z�st)? ', NOT_QUESTION ) = 0 then begin
       Query_Run(Query2, 'SELECT MS_RENSZ, MS_POTSZ FROM MEGSEGED WHERE MS_MBKOD = '''+Query1.FieldByName('MS_MBKOD').AsString+''' '+
           ' AND MS_SORSZ = '+Query1.FieldByName('MS_SORSZ').AsString);
       while not Query2.Eof do begin
           if rszlist.IndexOf(Query2.FieldByName('MS_RENSZ').AsString) < 0 then begin
               rszlist.Add(Query2.FieldByName('MS_RENSZ').AsString);
           end;
           if potlist.IndexOf(Query2.FieldByName('MS_POTSZ').AsString) < 0 then begin
               potlist.Add(Query2.FieldByName('MS_POTSZ').AsString);
           end;
           Query2.Next;
       end;
       Query_Run(Query2, 'DELETE FROM MEGSEGED WHERE MS_MBKOD = '''+Query1.FieldByName('MS_MBKOD').AsString+''' '+
           ' AND MS_SORSZ = '+Query1.FieldByName('MS_SORSZ').AsString);
       // Ellen�rizz�k az �ress�get
       Query_Run(Query2, 'SELECT COUNT(*) DB FROM MEGSEGED WHERE MS_MBKOD = '''+Query1.FieldByName('MS_MBKOD').AsString+''' ');
       if StrToIntDef(Query2.FieldByName('DB').AsString, 0) < 1 then begin
           // Nincs t�bb elem , t�r�lni kell a teljes megb�z�st
		    Query_Run(Query2, 'DELETE FROM MEGBIZAS WHERE MB_MBKOD = '''+Query1.FieldByName('MS_MBKOD').AsString+''' ');
		    Query_Run(Query2, 'DELETE FROM MEGLEVEL WHERE ML_MBKOD = '''+Query1.FieldByName('MS_MBKOD').AsString+''' ');
		    Query_Run(Query2, 'DELETE FROM SABLON   WHERE SB_MBKOD = '''+Query1.FieldByName('MS_MBKOD').AsString+''' ');
       end;
       MegbizasKieg(Query1.FieldByName('MB_MBKOD').AsString);
       ModLocate(Query1, 'MS_MBKOD', Query1.FieldByName('MS_MBKOD').AsString );
       SummaTolto;
       if rszlist.Count > 0 then begin
           for i := 0 to rszlist.Count - 1 do begin
               GepStatTolto(rszlist[i]);
           end;
       end;
       if potlist.Count > 0 then begin
           for i := 0 to potlist.Count - 1 do begin
               PotStatTolto(potlist[i]);
           end;
       end;
       rszlist.Free;
       potlist.Free;
       Exit;
   end;
	if NoticeKi( 'Figyelem! Val�ban a teljes megb�z�st t�r�lni k�v�nja? ', NOT_QUESTION ) = 0 then begin
		Query_Run(Query2, 'SELECT MS_RENSZ, MS_POTSZ FROM MEGSEGED WHERE MS_MBKOD = '''+Query1.FieldByName('MS_MBKOD').AsString+''' ');
		while not Query2.Eof do begin
			if rszlist.IndexOf(Query2.FieldByName('MS_RENSZ').AsString) < 0 then begin
				rszlist.Add(Query2.FieldByName('MS_RENSZ').AsString);
			end;
			if potlist.IndexOf(Query2.FieldByName('MS_POTSZ').AsString) < 0 then begin
				potlist.Add(Query2.FieldByName('MS_POTSZ').AsString);
			end;
			Query2.Next;
		end;
		// A megb�z�s g�pkocsi + p�tkocsi hozz�rak�sa
		if rszlist.IndexOf(Query1.FieldByName('MB_RENSZ').AsString) < 0 then begin
			rszlist.Add(Query1.FieldByName('MB_RENSZ').AsString);
		end;
		if potlist.IndexOf(Query1.FieldByName('MB_POTSZ').AsString) < 0 then begin
			potlist.Add(Query1.FieldByName('MB_POTSZ').AsString);
		end;
		Query_Run(Query2, 'DELETE FROM MEGSEGED WHERE MS_MBKOD = '''+Query1.FieldByName('MS_MBKOD').AsString+''' ');
		Query_Run(Query2, 'DELETE FROM MEGBIZAS WHERE MB_MBKOD = '''+Query1.FieldByName('MS_MBKOD').AsString+''' ');
		Query_Run(Query2, 'DELETE FROM MEGLEVEL WHERE ML_MBKOD = '''+Query1.FieldByName('MS_MBKOD').AsString+''' ');
		Query_Run(Query2, 'DELETE FROM SABLON   WHERE SB_MBKOD = '''+Query1.FieldByName('MS_MBKOD').AsString+''' ');
    EgyebDlg.Megbizas_valtozas (Query1.FieldByName('MS_MBKOD').AsInteger, 0, mbTOROL);  // a r�szmegb�z�s kapcsolatokat rendezi
		ModLocate(Query1, 'MS_MSKOD', Query1.FieldByName('MS_MSKOD').AsString );
       MegbizasKieg(Query1.FieldByName('MB_MBKOD').AsString);
		SummaTolto;
		if rszlist.Count > 0 then begin
			for i := 0 to rszlist.Count - 1 do begin
				GepStatTolto(rszlist[i]);
			end;
		end;
		if potlist.Count > 0 then begin
			for i := 0 to potlist.Count - 1 do begin
				PotStatTolto(potlist[i]);
			end;
		end;
	end;
	rszlist.Free;
	potlist.Free;
end;

procedure TMegbizasUjFmDlg.FormResize(Sender: TObject);
begin
	Inherited FormResize(Sender);
	M2.Top			:= BitBtn3.Top+2;
	M2.Left			:= BitBtn3.Left;
	BitBtn5.Top		:= BitBtn3.Top+2;
	BitBtn5.Left	:= BitBtn3.Left + M2.Width;
	BitBtn5.Width	:= BitBtn5.Height;

	M3.Top			:= BitBtn6.Top+2;
	M3.Left			:= BitBtn5.Left + BitBtn5.Width + 10;
	BitBtn4.Top		:= M3.Top;
	BitBtn4.Left	:= M3.Left + M3.Width;
	BitBtn4.Width	:= BitBtn4.Height;

	BitBtn10.Top	:= BitBtn6.Top;
	BitBtn10.Left	:= BitBtn4.Left + BitBtn4.Width + 10;
	BitBtn10.Width	:= 40;

	CheckBox1.Top	:= BitBtn10.Top+0;            // 7
	CheckBox1.Left	:= BitBtn10.Left + BitBtn10.Width + 10;

	CheckBox2.Top	:= BitBtn10.Top+17;
	CheckBox2.Left	:= BitBtn10.Left + BitBtn10.Width + 10;

//	CheckBox3.Top	:= BitBtn1.Top+0;            // 7
//	CheckBox3.Left	:= BitBtn1.Left + BitBtn1.Width + 5;
	CheckBox3.Top	:= BitBtn1.Top+BitBtn1.Height+1;            // 7
	CheckBox3.Left	:= BitBtn1.Left ;

	// Az �tcsatol�sok �s list�z�sok kezel�se
//	BitBtn11.Top	:= BitBtn6.Top;
//	BitBtn11.Left	:= BitBtn10.Left + BitBtn10.Width + 20;
	BitBtn11.Width	:= 90;
	BitBtn12.Top	:= BitBtn6.Top;
	BitBtn12.Left	:= BitBtn11.Left + BitBtn11.Width + 2;
	BitBtn12.Width	:= 40;

end;

procedure TMegbizasUjFmDlg.BitBtn5Click(Sender: TObject);
var
   d1, d2  : string;
begin
  // d1      := M2.Text;
	// CalendarRead(d1, d2);
  // M2.Text := d1;
	// M3.Text	:= d1;

  EgyebDlg.CalendarBe_idoszak(M2, M3);
  d1:= M2.Text;
  d2:= M3.Text;

	alapstr	:= 'SELECT * FROM MEGSEGED, MEGBIZAS WHERE MS_MBKOD = MB_MBKOD AND MS_DATUM = '''+M2.Text+''' '+ STATU_kizaras + szur140;
  if d2 <> '' then begin
	    M3.Text	:= d2;
	    alapstr	:= 'SELECT * FROM MEGSEGED, MEGBIZAS WHERE MS_MBKOD = MB_MBKOD AND MS_DATUM >= '''+M2.Text+''' '+
		    'AND MS_DATUM <= '''+M3.Text+''' '+ STATU_kizaras + szur140;
  end;
	SQL_ToltoFo(GetOrderBy(true));
end;

procedure TMegbizasUjFmDlg.DBGrid2DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);

  // vil�gos s�rga h�tt�r: ha  MS_STATK (felrak�si st�tusz) = 130 �s MS_TIPUS = 0 (felrak�s) �s MS_LETIDO �res

var
	szin	: integer;
//	oldszin	: integer;
   stkod	: string;
   MB_STATU: string;
   i		: integer;
	Icon	: TBitmap;
  tido, aido : TdateTime;
  nemteljesult, megerkezett : Boolean;
begin
//	oldszin := DBGrid2.Canvas.Brush.Color;
	// A felrak�s/lerak�s sz�n�nek meghat�roz�sa
   szin	:= EgyebDlg.row_color;
	if oldrecord = Query1.FieldByName('MS_MSKOD').AsString then begin
//     	szin		:= EgyebDlg.row_color;
	end else begin
		szin	:= clWindow;
	end;
	stkod	:= '';
	try
		stkod	:= Query1.FieldByName('MS_STATK').AsString;
    MB_STATU := Query1.FieldByName('MB_STATU').AsString;
	except
	end;
	if stkod <> '' then begin
		i := 1;
		while i <= 10 do begin
			if statusszin[i, 1] = stkod then begin
				szin	:= StrToIntDef(statusszin[i, 2], clAqua);
				i		:= 10;
			end;
           Inc(i);
		end;
   end;
	// Ha felrakott exportunk van �s ez lerak�s �s nincs lerak�si d�tum, akkor a sz�n = szabad export = 110
  if ( ( stkod = '130' ) and (Query1.FieldByName('MS_TIPUS').AsString = '1' ) and (Query1.FieldByName('MS_LETIDO').AsString = '' ) ) then begin
		stkod := '110';
		i := 1;
		while i <= 10 do begin
			if statusszin[i, 1] = stkod then begin
				szin	:= StrToIntDef(statusszin[i, 2], clAqua);
				i		:= 10;
			end;
			Inc(i);
		end;
	end;
	if ( ( stkod = '230' ) and (Query1.FieldByName('MS_TIPUS').AsString = '1' ) and (Query1.FieldByName('MS_LETIDO').AsString = '' ) ) then begin
		stkod := '210';
		i := 1;
		while i <= 10 do begin
			if statusszin[i, 1] = stkod then begin
				szin	:= StrToIntDef(statusszin[i, 2], clAqua);
				i		:= 10;
			end;
			Inc(i);
		end;
	end;
	if SelectList.IndexOf(Query1.FieldByName(KULCSMEZO).AsString) > -1 then begin
		// A sor ki van jel�lve
		szin		:= clSilver;
	end;
	if oldrecord = Query1.FieldByName('MS_MSKOD').AsString then begin
//     	szin		:= EgyebDlg.row_color;
	end;

  /// Ha van �rkez�si id�, akkor z�ld lesz a bet�
  megerkezett:=False;
  if ((Query1.FieldByName('MS_FETIDO').AsString='')and(Query1.FieldByName('MS_FERKIDO').AsString<>'')and(Query1.FieldByName('MS_TIPUS').AsString='0'))or
     ((Query1.FieldByName('MS_LETIDO').AsString='')and(Query1.FieldByName('MS_LERKIDO').AsString<>'')and(Query1.FieldByName('MS_TIPUS').AsString='1')) then  megerkezett:=True;

  ///////////////////////       ha nem t�rt�nt meg id�ben a fel-lerak�s, akkor piros lesz a bet�sz�n
  nemteljesult:=False;
  if  (Query1.FieldByName('MS_FETIDO').AsString='')and(Query1.FieldByName('MS_FERKIDO').AsString='')and(Query1.FieldByName('MS_TIPUS').AsString='0') then     // nincs kit�ltve a t�ny id�
  begin
    //ido:= Query1.FieldByName('MS_FELIDO').AsString;
    if Query1.FieldByName('MS_FELIDO2').AsString<>'' then   // -ig ki van t�ltve
      if Query1.FieldByName('MS_FELDAT2').AsString <> '' then
        tido:= StrToDateTimeDef(Query1.FieldByName('MS_FELDAT2').AsString+' '+Query1.FieldByName('MS_FELIDO2').AsString+':00',now)
      else
        tido:= StrToDateTimeDef(Query1.FieldByName('MS_FELDAT').AsString+' '+Query1.FieldByName('MS_FELIDO2').AsString+':00',now)
    else
      tido:= StrToDateTimeDef(Query1.FieldByName('MS_FELDAT').AsString+' '+Query1.FieldByName('MS_FELIDO').AsString+':00',now) ;
  {
        if  Query1.FieldByName('MS_FELIDO2').AsString < Query1.FieldByName('MS_FELIDO').AsString   then   // 20:00 - 06:00
          ido:= '23:59'
        else
          ido:= Query1.FieldByName('MS_FELIDO2').AsString;
    tido:= StrToDateTimeDef(Query1.FieldByName('MS_FELDAT').AsString+' '+ido+':00',now);
        }
    aido:= Now;
    nemteljesult:= nemteljesult OR (aido > tido) ;
  end;
  if  (Query1.FieldByName('MS_LETIDO').AsString='')and(Query1.FieldByName('MS_LERKIDO').AsString='')and(Query1.FieldByName('MS_TIPUS').AsString='1') then     // nincs kit�ltve a t�ny id�
  begin
    if Query1.FieldByName('MS_LERIDO2').AsString<>'' then   // -ig ki van t�ltve
      if Query1.FieldByName('MS_LERDAT2').AsString <> '' then
        tido:= StrToDateTimeDef(Query1.FieldByName('MS_LERDAT2').AsString+' '+Query1.FieldByName('MS_LERIDO2').AsString+':00',now)
      else
        tido:= StrToDateTimeDef(Query1.FieldByName('MS_LERDAT').AsString+' '+Query1.FieldByName('MS_LERIDO2').AsString+':00',now)
    else
      tido:= StrToDateTimeDef(Query1.FieldByName('MS_LERDAT').AsString+' '+Query1.FieldByName('MS_LERIDO').AsString+':00',now) ;

{

    ido:= Query1.FieldByName('MS_LERIDO').AsString;
    if Query1.FieldByName('MS_LERIDO2').AsString<>'' then    // -ig
      if  Query1.FieldByName('MS_LERIDO2').AsString < Query1.FieldByName('MS_LERIDO').AsString   then   // 20:00 - 06:00
        ido:= '23:59'
      else
        ido:= Query1.FieldByName('MS_LERIDO2').AsString;
    tido:= StrToDateTimeDef(Query1.FieldByName('MS_LERDAT').AsString+' '+ido+':00',now);    }
    
    aido:= Now;
    nemteljesult:= nemteljesult OR (aido > tido) ;
  end;

  if MB_STATU = '300' then begin   // Meghi�sult/t�r�lt megb�z�s
     // szin:= clGray;
     // DBGrid2.Canvas.Font.Style := [fsItalic];
     DBGrid2.Canvas.Font.Style := [fsStrikeOut];
     end
  else begin
     DBGrid2.Canvas.Font.Style := [];
     end;

  if nemteljesult then
    	DBGrid2.Canvas.Font.Color		:= clRed
  else
    if megerkezett then
    	DBGrid2.Canvas.Font.Color		:= clGreen
    else
    	DBGrid2.Canvas.Font.Color		:= clBlack;
  //////////////////////

	DBGrid2.Canvas.Brush.Color		:= szin;

	DBGrid2.DefaultDrawDataCell(Rect, Column.Field, State);
	alapszin						:= szin;
	inherited DBGrid2DrawColumnCell(Sender, Rect, DataCol, Column, State);
	// Ikon kirajzol�sa a fontoss�ghoz!!!
	if Column.FieldName = 'MS_KIFON' then begin
		Icon:=TBitmap.Create;
		DBGrid2.Canvas.FillRect(Rect);
		if Query1.FieldByName('MS_KIFON').AsString = '1' then begin
			ImageList1.GetBitmap(0,Icon);
			DBGrid2.Canvas.Draw(round((Rect.Left+Rect.Right-Icon.Width) DIV 2), Rect.Top + ((Rect.Bottom-Rect.Top-Icon.Height) DIV 2),Icon);
		end;
	end;

	if Column.FieldName = 'MS_DARU' then begin
		Icon:=TBitmap.Create;
		DBGrid2.Canvas.FillRect(Rect);
		if Query1.FieldByName('MS_DARU').AsString = '1' then begin
			ImageList1.GetBitmap(0,Icon);
			DBGrid2.Canvas.Draw(round((Rect.Left+Rect.Right-Icon.Width) DIV 2), Rect.Top + ((Rect.Bottom-Rect.Top-Icon.Height) DIV 2),Icon);
		end;
	end;
  // ----- 2018-09-07
  if Column.FieldName = 'MB_ADR' then begin
		Icon:=TBitmap.Create;
		DBGrid2.Canvas.FillRect(Rect);
		if Query1.FieldByName('MB_ADR').AsString = '1' then begin
			ImageList1.GetBitmap(1,Icon); // Icon = TBitmap
      //---- ez az eg�sz sajnos nem m�k�dik m�g, nem lett �tl�tsz� a h�tt�r -----
      // Icon.Transparent := true;
      // Icon.TransparentColor := clWhite;
      // Icon.Canvas.Brush.Color := clWhite;
      // Icon.TransparentMode:= tmAuto;
      // Icon.alphaformat:= afPremultiplied;
      // ---------------------------------------------
			DBGrid2.Canvas.Draw(round((Rect.Left+Rect.Right-Icon.Width) DIV 2), Rect.Top + ((Rect.Bottom-Rect.Top-Icon.Height) DIV 2),Icon);
      // vagy ez: max opacity?? nem, itt az eg�sz bmp lesz �ttetsz�
      // DBGrid2.Canvas.Draw(round((Rect.Left+Rect.Right-Icon.Width) DIV 2), Rect.Top + ((Rect.Bottom-Rect.Top-Icon.Height) DIV 2),Icon, 0);
		end;
	end;

  {
	// KG 20111123 Ikon kirajzol�sa a DARU sz�ks�ges!!!
	if Column.FieldName = 'MS_LDARU' then begin
		Icon:=TBitmap.Create;
		DBGrid2.Canvas.FillRect(Rect);
		if Query1.FieldByName('MS_LDARU').AsString = '1' then begin
			ImageList1.GetBitmap(0,Icon);
			DBGrid2.Canvas.Draw(round((Rect.Left+Rect.Right-Icon.Width) DIV 2), Rect.Top + ((Rect.Bottom-Rect.Top-Icon.Height) DIV 2),Icon);
		end;
	end;
	// KG 20111123 Ikon kirajzol�sa a DARU sz�ks�ges!!!
	if Column.FieldName = 'MS_FDARU' then begin
		Icon:=TBitmap.Create;
		DBGrid2.Canvas.FillRect(Rect);
		if Query1.FieldByName('MS_FDARU').AsString = '1' then begin
			ImageList1.GetBitmap(0,Icon);
			DBGrid2.Canvas.Draw(round((Rect.Left+Rect.Right-Icon.Width) DIV 2), Rect.Top + ((Rect.Bottom-Rect.Top-Icon.Height) DIV 2),Icon);
		end;
	end;
  }
//  	DBGrid2.Canvas.Brush.Color		:= oldszin;
end;

procedure TMegbizasUjFmDlg.BitBtn6Click(Sender: TObject);
begin
(*
	// Let�r�lj�k a felt�teleket
	alapstr	:= 'SELECT * FROM MEGSEGED, MEGBIZAS WHERE MS_MBKOD = MB_MBKOD ';
	SQL_ToltoFo(GetOrderBy(true));
*)
end;

procedure TMegbizasUjFmDlg.BitBtn7Click(Sender: TObject);
var
  kod, Modosito: string;
begin
	// Ellen�rizz�k, hogy ki dolgozik m�g ezen a megb�z�son
	 if Query1.FieldByName('MB_AFKOD').AsString <> '' then begin
		if Query1.FieldByName('MB_AFKOD').AsString <> EgyebDlg.user_code then begin
      Modosito:=Query_Select('JELSZO', 'JE_FEKOD', Query1.FieldByName('MB_AFKOD').AsString, 'JE_FENEV');
  		NoticeKi('A megb�z�st �ppen m�s m�dos�tja! ('+Modosito+')');
			Exit;
		end;
	 end;
	// A kijel�lt felrak� megjelenit�se
///	StatuszBeiras(Query1.FieldByName('MB_MBKOD').AsString);
  Screen.Cursor:=crHourGlass;
  Application.ProcessMessages;
  EgyebDlg.MBujtetel:=False;
	Application.CreateForm(TSegedbe2Dlg,Segedbe2Dlg);
	Segedbe2Dlg.MegForm		:= Self;
	Segedbe2Dlg.kellelore	:= true;
	Segedbe2Dlg.tolt('Felrak�s/lerak�s m�dos�t�sa', Query1.FieldByName('MB_MBKOD').AsString,
		Query1.FieldByname('MS_SORSZ').AsString, Query1.FieldByName('MS_MSKOD').AsString);
  Screen.Cursor:=crDefault;
  Application.ProcessMessages;
	Segedbe2Dlg.ShowModal;
///	StatuszTorles;
	SummaTolto;
	if Segedbe2Dlg.voltenter then begin
    kod:=Query1.FieldByName('MS_MSKOD').AsString;
  	//SQL_ToltoFo(GetOrderBy(true));
		ModLocate (Query1, 'MS_MSKOD', kod );
	end;
	Segedbe2Dlg.Destroy;
   MegbizasKieg(Query1.FieldByName('MB_MBKOD').AsString);
end;

procedure TMegbizasUjFmDlg.BitBtn8Click(Sender: TObject);
var
  jakod: string;
begin
  if EgyebDlg.IsFormOpen('JaratbeDlg')    then
  begin
		NoticeKi('A j�rat ablak m�r meg van nyitva!');
    Exit;
  end;
	// J�rat megjelen�t�se
	jakod:= Query1.FieldByName('MB_JAKOD').AsString ;
	if (jakod='') and (Query1.FieldByName('MS_JAKOD').AsString<>'') then
  	jakod:= Query1.FieldByName('MS_JAKOD').AsString ;

	if jakod <> '' then begin
  	if Query1.FieldByName('MS_JAKOD').AsString <> '' then
      jakod:= Query1.FieldByName('MS_JAKOD').AsString ;

		Query_Run (QueryUj3, 'SELECT * FROM JARAT WHERE JA_KOD = '''+jakod+''' ',true);
		if QueryUj3.RecordCount < 1 then begin
			// NoticeKi('El�z� �vi j�rat!');
      NoticeKi('A j�rat nem tal�lhat�! J�ratk�d: '+jakod);
			Exit;
		end;
		Application.CreateForm(TJaratbeDlg, JaratbeDlg);
    JaratbeDlg.megtekint:= (QueryUj3.FieldByName('JA_FAZIS').AsInteger > 0);
		JaratbeDlg.Tolto('J�rat m�dos�t�s',jakod);
		JaratbeDlg.ShowModal;
		JaratbeDlg.Destroy;

  	ModLocate (Query1, 'MS_MSKOD', Query1.FieldByName('MS_MSKOD').AsString );
	  SummaTolto;

	end;
end;

procedure TMegbizasUjFmDlg.DatumAtirasClick(Sender: TObject);
const
  UseLogThread = False;
var
	oldmezo1	: string;
  statusz		: integer;
  msid0:integer;
  TraceString, ExpImp: string;
begin
  // try
   TraceString:='1';
	 oldmezo1	:= Query1.FieldByName('MS_MSKOD').AsString;
	 Application.CreateForm(TIdobeDlg,IdobeDlg);
   TraceString:=TraceString+';2';
   if Query1.FieldByName('MS_TIPUS').AsString = '0' then
   begin        // fel
     IdobeDlg.M5.Text	:= Query1.FieldByName('MS_FETDAT').AsString;
     IdobeDlg.M6.Text	:= Query1.FieldByName('MS_FETIDO').AsString;
     IdobeDlg.M51.Text	:= Query1.FieldByName('MS_FERKDAT').AsString;
     IdobeDlg.M61.Text	:= Query1.FieldByName('MS_FERKIDO').AsString;
     IdobeDlg.M7.Text	:= Query1.FieldByName('MS_FEPAL').AsString;
     IdobeDlg.M8.Text	:= Query1.FieldByName('MS_FSULY').AsString;
     IdobeDlg.Label3.Caption:='Felrak�';
   end
   else
   begin      //le
    IdobeDlg.M5.Text	:= Query1.FieldByName('MS_LETDAT').AsString;
    IdobeDlg.M6.Text	:= Query1.FieldByName('MS_LETIDO').AsString;
    IdobeDlg.M51.Text	:= Query1.FieldByName('MS_LERKDAT').AsString;
    IdobeDlg.M61.Text	:= Query1.FieldByName('MS_LERKIDO').AsString;
    IdobeDlg.Label3.Caption:='Lerak�';
    if  (Query1.FieldByName('MS_LEPAL').AsString<>'0')or(Query1.FieldByName('MS_LSULY').AsString<>'0')   then
    begin
     IdobeDlg.M7.Text	:= Query1.FieldByName('MS_LEPAL').AsString;
     IdobeDlg.M8.Text	:= Query1.FieldByName('MS_LSULY').AsString;
    end
    else
    begin
     IdobeDlg.M7.Text	:= Query1.FieldByName('MS_FEPAL').AsString;
     IdobeDlg.M8.Text	:= Query1.FieldByName('MS_FSULY').AsString;
    end ;
   end;
   if IdobeDlg.M5.Text='' then
   begin
     IdobeDlg.M5.Text	:= Query1.FieldByName('MS_DATUM').AsString;
     IdobeDlg.M6.Text	:= Query1.FieldByName('MS_IDOPT').AsString;
   end;
   TraceString:=TraceString+';3';
	 IdobeDlg.ShowModal;
   Query_Log_Str('===== MEGBIZAS JOBBCLICK MODI kezd', 0, false, UseLogThread);
   TraceString:=TraceString+';4';
   Query_Log_Str('OK@Megb�z�s modi, Trace:'+TraceString, 0, false, true);

   if (IdobeDlg.ret_datum <> '')and(IdobeDlg.ret_idopt <> '') then begin
    	  // Ellen�rizz�k a t�nyd�tumokat
		    if ( IdobeDlg.ret_datum ) > FormatDateTime('yyyy.mm.dd.', IncMinute(now, 5)) then begin
    			NoticeKi('�rv�nytelen t�nyd�tum, id�pont!');
          IdobeDlg.ret_datum:='';
          IdobeDlg.ret_idopt:='';
		    end;
   end;
   if (IdobeDlg.ret_datum1 <> '')and(IdobeDlg.ret_idopt1 <> '') then begin
    	  // Ellen�rizz�k a �rkez�si d.
		    if ( IdobeDlg.ret_datum1 ) > FormatDateTime('yyyy.mm.dd.', IncMinute(now, 5)) then begin
    			NoticeKi('�rv�nytelen �rkez�si d�tum, id�pont!');
          IdobeDlg.ret_datum1:='';
          IdobeDlg.ret_idopt1:='';
		    end;
   end;
   if IdobeDlg.ret_idopt <> '' then begin
    	  // Ellen�rizz�k a t�nyd�tumokat
		    if ( IdobeDlg.ret_datum + ' ' + IdobeDlg.ret_idopt ) > FormatDateTime('yyyy.mm.dd. HH:NN', IncMinute(now, 5)) then begin
    			NoticeKi('�rv�nytelen t�nyd�tum, id�pont!');
          IdobeDlg.ret_datum:='';
          IdobeDlg.ret_idopt:='';
		    end;
   end;
   if IdobeDlg.ret_idopt1 <> '' then begin
    	  // Ellen�rizz�k a t�nyd�tumokat
		    if ( IdobeDlg.ret_datum1 + ' ' + IdobeDlg.ret_idopt1 ) > FormatDateTime('yyyy.mm.dd. HH:NN', IncMinute(now, 5)) then begin
    			NoticeKi('�rv�nytelen �rkez�si d�tum, id�pont!');
          IdobeDlg.ret_datum1:='';
          IdobeDlg.ret_idopt1:='';
		    end;
   end;
   Screen.Cursor:= crHourGlass;
   TraceString:=TraceString+';5';
   Query_Log_Str('OK@Megb�z�s modi, Trace:'+TraceString, 0, false, UseLogThread);
   if (IdobeDlg.ret_datum <> '')and(Query1.FieldByName('MS_TIPUS').AsString = '0') then                  // Felrak�s
   begin
//     if (Query1.FieldByName('MS_LETDAT').AsString+Query1.FieldByName('MS_LETIDO').AsString <>'')and
     if (Query1.FieldByName('MS_LETDAT').AsString<>'') and (Query1.FieldByName('MS_LETIDO').AsString <>'')and
        (Query1.FieldByName('MS_LETDAT').AsString+Query1.FieldByName('MS_LETIDO').AsString < IdobeDlg.ret_datum+IdobeDlg.ret_idopt )  then
     begin
    		NoticeKi('A lerak�s d�tuma nem lehet kisebb mint a felrak�s d�tuma!');
        IdobeDlg.ret_datum:='';
        IdobeDlg.ret_idopt:='';
     end;
   end;
   if (IdobeDlg.ret_datum <> '')and(Query1.FieldByName('MS_TIPUS').AsString = '1') then                  // LErak�s
   begin
     if (Query1.FieldByName('MS_FETDAT').AsString+Query1.FieldByName('MS_FETIDO').AsString <>'')and(IdobeDlg.ret_idopt<>'')and
        (Query1.FieldByName('MS_FETDAT').AsString+Query1.FieldByName('MS_FETIDO').AsString > IdobeDlg.ret_datum+IdobeDlg.ret_idopt )  then
     begin
    		NoticeKi('A lerak�s d�tuma nem lehet kisebb mint a felrak�s d�tuma!');
        IdobeDlg.ret_datum:='';
        IdobeDlg.ret_idopt:='';
     end;
   end;
   TraceString:=TraceString+';5b';
   Query_Log_Str('OK@Megb�z�s modi, Trace:'+TraceString, 0, false, UseLogThread);
   if IdobeDlg.ret_datum <> '' then begin
       TraceString:=TraceString+';6a';
       Query_Log_Str('OK@Megb�z�s modi, Trace:'+TraceString, 0, false, UseLogThread);
       SetMegbizasModositoUser(Query1.FieldByName('MS_MBKOD').AsString);
       TraceString:=TraceString+';6b';
       Query_Log_Str('OK@Megb�z�s modi, Trace:'+TraceString, 0, false, UseLogThread);
       if Query1.FieldByName('MS_TIPUS').AsString = '0' then begin      //Fel
       	// Felrak�sr�l van sz�
        msid0:= Query1.FieldByName('MS_ID').value;
        Query_Update (Query2, 'MEGSEGED',       // Azonos felrak� t�telekbe is be�rja
               [
               'MS_FETDAT', 	''''+IdobeDlg.ret_datum+'''',
               'MS_FETIDO', 	''''+IdobeDlg.ret_idopt+'''',
               'MS_FERKDAT', 	''''+IdobeDlg.ret_datum1+'''',
               'MS_FERKIDO', 	''''+IdobeDlg.ret_idopt1+'''',
               'MS_FSULY', 	SqlSzamString(StringSzam(IdobeDlg.ret_suly),8,2),
               'MS_FEPAL', 	SqlSzamString(StringSzam(IdobeDlg.ret_paletta),8,2)
				],
				' WHERE (MS_MBKOD = '+Query1.FieldByName('MS_MBKOD').AsString+' AND MS_SORSZ = '+Query1.FieldByName('MS_SORSZ').AsString+')or('+
    		'  MS_AZTIP='''+'AF'''+' AND MS_ID<>'+IntToStr(msid0)+' AND MS_MBKOD = '+Query1.FieldByName('MS_MBKOD').AsString+' AND MS_ID2 = '+IntToStr(msid0)+')' );

        if IdobeDlg.ret_idopt<>'' then
			  Query_Update (Query2, 'MEGSEGED',
				[
				'MS_DATUM',     ''''+IdobeDlg.ret_datum+'''',
				'MS_IDOPT',     ''''+IdobeDlg.ret_idopt+'''',
				'MS_TEIDO',     ''''+IdobeDlg.ret_idopt+''''
				],
				' WHERE MS_MBKOD = '+Query1.FieldByName('MS_MBKOD').AsString+
				' AND MS_SORSZ = '+Query1.FieldByName('MS_SORSZ').AsString+' AND MS_TIPUS = 0' );   //FEl
        // ----- 2017-02-09: ha egy felrak�-egy lerak�, akkor a lerak�shoz is be�rja a s�lyt �s a paletta sz�mot
        ExpImp:= Query1.FieldByName('MS_EXPOR').AsString; // 0: import, 1: export, 2: k�lf�ld, 3: belf�ld
        TraceString:=TraceString+';6c';
        Query_Log_Str('OK@Megb�z�s modi, Trace:'+TraceString, 0, false, UseLogThread);
        // Query_run(Query2, 'select distinct COUNT(MS_ID), MAX(MS_SORSZ) FROM MEGSEGED WHERE MS_MBKOD = '+Query1.FieldByName('MS_MBKOD').AsString);
        Query_run(Query2, 'select distinct COUNT(MS_ID) FROM MEGSEGED WHERE MS_MBKOD = '+Query1.FieldByName('MS_MBKOD').AsString+' and MS_EXPOR='+ExpImp);
        if (Query2.Fields[0].AsInteger=2) then begin  // pontosan k�t ugyanolyan MEGSEGED sor van = egy ir�nyban egy felrak-lerak
            Query_Update (Query2, 'MEGSEGED',
               [
               'MS_LSULY', 	SqlSzamString(StringSzam(IdobeDlg.ret_suly),8,2),
               'MS_LEPAL', 	SqlSzamString(StringSzam(IdobeDlg.ret_paletta),8,2)
        				], ' WHERE MS_MBKOD = '+Query1.FieldByName('MS_MBKOD').AsString);
            end;  // if
        TraceString:=TraceString+';6d';
        Query_Log_Str('OK@Megb�z�s modi, Trace:'+TraceString, 0, false, UseLogThread);

		   end else begin
       	// Lerak�sr�l van sz�
        TraceString:=TraceString+';6e';
        Query_Log_Str('OK@Megb�z�s modi, Trace:'+TraceString, 0, false, UseLogThread);

        msid0:= Query1.FieldByName('MS_ID').value;
        Query_Update (Query2, 'MEGSEGED',
               [
               'MS_LETDAT', 	''''+IdobeDlg.ret_datum+'''',
               'MS_LETIDO', 	''''+IdobeDlg.ret_idopt+'''',
               'MS_LERKDAT', 	''''+IdobeDlg.ret_datum1+'''',
               'MS_LERKIDO', 	''''+IdobeDlg.ret_idopt1+'''',
               // 'MS_LSULY', 	IdobeDlg.ret_suly,
               // 'MS_LEPAL', 	IdobeDlg.ret_paletta
               'MS_LSULY', 	SqlSzamString(StringSzam(IdobeDlg.ret_suly),8,2),
               'MS_LEPAL', 	SqlSzamString(StringSzam(IdobeDlg.ret_paletta),8,2)
               ],
               ' WHERE (MS_MBKOD = '+Query1.FieldByName('MS_MBKOD').AsString+' AND MS_SORSZ = '+Query1.FieldByName('MS_SORSZ').AsString+')or('+
            	 '  MS_AZTIP='''+'AL'''+' AND MS_ID<>'+IntToStr(msid0)+' AND MS_MBKOD = '+Query1.FieldByName('MS_MBKOD').AsString+' AND MS_ID2 = '+IntToStr(msid0)+')' );
                                  // Azonos lerak� t�telekbe is be�rja

        if IdobeDlg.ret_idopt<>'' then
          Query_Update (Query2, 'MEGSEGED',
               [
               'MS_DATUM',     ''''+IdobeDlg.ret_datum+'''',
               'MS_IDOPT',     ''''+IdobeDlg.ret_idopt+'''',
               'MS_TEIDO',     ''''+IdobeDlg.ret_idopt+''''
               ],
               ' WHERE MS_MBKOD = '+Query1.FieldByName('MS_MBKOD').AsString+
               ' AND MS_SORSZ = '+Query1.FieldByName('MS_SORSZ').AsString+' AND MS_TIPUS = 1' );
        TraceString:=TraceString+';6f';
        Query_Log_Str('OK@Megb�z�s modi, Trace:'+TraceString, 0, false, UseLogThread);
       end;
       TraceString:=TraceString+';6.5';
       Query_Log_Str('OK@Megb�z�s modi, Trace:'+TraceString, 0, false, UseLogThread);

		// A st�tusz be�ll�t�sa az �rintett elemekn�l
       Query_Run(Query2, 'SELECT * FROM MEGSEGED WHERE MS_MBKOD = '+Query1.FieldByName('MS_MBKOD').AsString+
			' AND MS_SORSZ = '+Query1.FieldByName('MS_SORSZ').AsString);
		   if Query2.RecordCount > 0 then begin
           while not Query2.Eof do begin
               statusz	:= GetFelrakoStatus(Query2.FieldByName('MS_ID').AsString);
               SetFelrakoStatus(Query2.FieldByName('MS_ID').AsString, statusz);
               Query2.Next;
			     end;
		   end;


    TraceString:=TraceString+';7';
		// �jrasz�moljuk a g�pkocsi statisztik�kat
    // SzabadMemoriaNaploz('GepStatTolto el�tt');
    Query_Log_Str('GepStatTolto el�tt, Trace:'+TraceString, 0, false, UseLogThread);
		GepStatTolto(Query1.FieldByName('MS_RENSZ').AsString);
    TraceString:=TraceString+';8';
    // SzabadMemoriaNaploz('GepStatTolto ut�n');
    Query_Log_Str('GepStatTolto ut�n, Trace:'+TraceString, 0, false, UseLogThread);
		PotStatTolto(Query1.FieldByName('MS_POTSZ').AsString);
    // SzabadMemoriaNaploz('PotStatTolto ut�n');
//		alapstr	:= 'SELECT * FROM MEGSEGED, MEGBIZAS WHERE MS_MBKOD = MB_MBKOD AND MS_DATUM = '''+M2.Text+''' ';
		alapstr	:= 'SELECT * FROM MEGSEGED, MEGBIZAS WHERE MS_MBKOD = MB_MBKOD AND MS_DATUM >= '''+M2.Text+''' '+
			'AND MS_DATUM <= '''+M3.Text+''' '+ STATU_kizaras + szur140;
    TraceString:=TraceString+';9';
    Query_Log_Str('PotStatTolto ut�n, Trace:'+TraceString, 0, false, UseLogThread);
		SQL_ToltoFo(GetOrderBy(true));
    TraceString:=TraceString+';10';
    Query_Log_Str('SQL_ToltoFo ut�n, Trace:'+TraceString, 0, false, UseLogThread);
		ModLocate (Query1, 'MS_MSKOD', oldmezo1 );
    TraceString:=TraceString+';11';
    Query_Log_Str('OK@Megb�z�s modi, Trace:'+TraceString, 0, false, UseLogThread);
		SummaTolto;
    TraceString:=TraceString+';12';
    // elvileg itt is kellene, de f�lek hogy nagyon lelass�tja
    // UTINFO:= EgyebDlg.Megbizas_valtozas (MBKOD, EUDIJ, mbMODOSIT);
    Query_Log_Str('OK@Megb�z�s modi, Trace:'+TraceString, 0, false, UseLogThread);
	 end;
   // Query_Log_Str('OK@Megb�z�s modi, Trace:'+TraceString, 0, true);
	 // IdobeDlg.Destroy;
   IdobeDlg.Release;
   Query_Log_Str('===== MEGBIZAS JOBBCLICK MODI vegez', 0, false, UseLogThread);
   Screen.Cursor:= crDefault;
 {  except
     on E: exception do begin
          Hibakiiro(E.Message+' @Megb�z�s modi, Trace:'+TraceString, 0, true);
        end; // on E
     end;  // try-except
     }
end;

procedure TMegbizasUjFmDlg.BitBtn9Click(Sender: TObject);
begin
	// Megjelen�tj�k a g�pkocsi formot
	Screen.Cursor	:= crHourGlass;
	Application.CreateForm(TGepStatFmDlg, GepStatFmDlg);
	Screen.Cursor	:= crDefault;
	GepStatFmDlg.ShowModal;
	GepStatFmDlg.Destroy;
end;

procedure TMegbizasUjFmDlg.SzinbeallitasClick(Sender: TObject);
var
	stkod	: string;
	i		: integer;
begin
	// A kijel�lt sor st�tusz sz�n�nek be�ll�t�sa
	if ColorDialog2.Execute then begin
		stkod	:= '';
		try
			stkod	:= Query1.FieldByName('MS_STATK').AsString;
		except
		end;
		if stkod <> '' then begin
			Query_Run(EgyebDlg.QueryKozos, 'UPDATE SZOTAR SET SZ_KODHO = '+IntToStr(ColorDialog2.Color)+
				' WHERE SZ_FOKOD = ''960'' AND SZ_ALKOD = '''+stkod+''' ' );
			// A t�bl�zat friss�t�se
			Query_Run(EgyebDlg.QueryKozos, 'SELECT SZ_ALKOD, SZ_KODHO FROM SZOTAR WHERE SZ_FOKOD = ''960'' ' );
			i := 1;
			while ( ( i <= 10 ) and ( not EgyebDlg.QueryKozos.Eof ) ) do begin
				statusszin[i, 1] := EgyebDlg.QueryKozos.FieldByName('SZ_ALKOD').AsString;
				statusszin[i, 2] := EgyebDlg.QueryKozos.FieldByName('SZ_KODHO').AsString;
				Inc(i);
				EgyebDlg.QueryKozos.Next;
			end;
			ModLocate (Query1, 'MS_MSKOD', Query1.FieldByName('MS_MSKOD').AsString );
			SummaTolto;
		end;
	end;
end;

procedure TMegbizasUjFmDlg.Timer1Timer(Sender: TObject);
begin
	// Ellen�rizz�k, hogy t�rt�nt-e v�ltoz�s az adatb�zisban, �s ha igen, friss�t�nk
(*
	Timer1.Enabled	:= false;
	Query_Run(QueryUj3, 'SELECT SUM(MB_AFLAG) OSSZEG FROM MEGBIZAS ');
	if checksumma <> StrToIntDef(QueryUj3.FieldByName('OSSZEG').AsString,1) then begin
		checksumma	:= StrToIntDef(QueryUj3.FieldByName('OSSZEG').AsString,1);
		ModLocate (Query1, 'MS_MSKOD', Query1.FieldByName('MS_MSKOD').AsString );
	end;
	Timer1.Enabled	:= true;
*)
end;

procedure TMegbizasUjFmDlg.SMSkldssofrnek1Click(Sender: TObject);
var
  mbkod, soforkod1, soforkod2: string;
begin
  // mbkod:= Query1.FieldByName('MB_MBKOD').AsString;
  soforkod1:= Query1.FieldByName('MB_DOKOD').AsString;
  soforkod2:= Query1.FieldByName('MB_DOKO2').AsString;
  // SMSKuldes(mbkod, soforkod1, soforkod2);
  TruckpitUzenetKuldes(soforkod1, soforkod2, '', '');
end;


procedure TMegbizasUjFmDlg.SMSkplds1Click(Sender: TObject);
begin
  // UresSMSKuldes;
end;

procedure TMegbizasUjFmDlg.SMSKuldes(mbkod, soforkod1, soforkod2: string);
var
  kuldott_szoveg, kuldott_cimzettek, emailkod: string;
begin
	if (mbkod='') or ((soforkod1 = '') and (soforkod2 = '')) then begin
		Exit;
	end;
  TruckpitUzenetKuldes(soforkod1, soforkod2, '', mbkod); // � t�rolja is a megb�z�shoz, ha van k�d
end;

{procedure TMegbizasUjFmDlg.SMSKuldes(mbkod, soforkod1, soforkod2: string);
var
  kuldott_szoveg, kuldott_cimzettek, emailkod: string;
begin
	if (mbkod='') or ((soforkod1 = '') and (soforkod2 = '')) then begin
		Exit;
	end;
  // UzenetSzerkesztoDlg:= TUzenetSzerkesztoDlg.Create(nil);
  UzenetSzerkesztoDlg.Reset;
  //try
    if soforkod1	<> '' then begin
      UzenetSzerkesztoDlg.AddDolgozoCimzett(soforkod1, cimzett, False);
      end;
    if soforkod2	<> '' then begin
      UzenetSzerkesztoDlg.AddDolgozoCimzett(soforkod2, cimzett, False);
      end;
    UzenetSzerkesztoDlg.SetSzoveg('');
    UzenetSzerkesztoDlg.EagleSMSTipus:= uzleti;
    UzenetSzerkesztoDlg.ShowModal;
    if UzenetSzerkesztoDlg.VoltKuldes then begin
         kuldott_szoveg:= UzenetSzerkesztoDlg.KuldottSzoveg;
         kuldott_cimzettek:= UzenetSzerkesztoDlg.KuldottCimzettek;
         emailkod := IntToStr(GetNextCode('MEGLEVEL', 'ML_MLKOD', 1, 0));
         Query_Insert (EgyebDlg.Query1, 'MEGLEVEL',
           [
           'ML_MLKOD', emailkod,
           'ML_MBKOD', mbkod,
           'ML_FEKOD', ''''+EgyebDlg.user_code+'''',
           'ML_FENEV', ''''+EgyebDlg.user_name+'''',
           'ML_DATUM', ''''+EgyebDlg.MaiDatum+'''',
           'ML_IDOPT', ''''+FormatDateTime('hh:nn',now)+'''',
           'ML_EMCIM', ''''+copy(kuldott_cimzettek,1,128)+'''',
           'ML_SZOV1', ''''+copy(kuldott_szoveg,1,512)+'''',
           'ML_SZOV2', ''''+copy(kuldott_szoveg,513,512)+''''
          ] );
          end; // if
    // finally
    //   if UzenetSzerkesztoDlg <> nil then UzenetSzerkesztoDlg.Release;
    //   end;  // try-finally
end;
}

{procedure TMegbizasUjFmDlg.UresSMSKuldes;
var
  kuldott_szoveg, kuldott_cimzettek, emailkod: string;
begin
  // UzenetSzerkesztoDlg:= TUzenetSzerkesztoDlg.Create(nil);
  UzenetSzerkesztoDlg.Reset;
  // Query_Log_Str('CsoportListaTolt?', 0, false, false);
  UzenetSzerkesztoDlg.CsoportListaTolt;
  // Query_Log_Str('CsoportListaTolt OK', 0, false, false);
  // try
    UzenetSzerkesztoDlg.SetSzoveg('');
    UzenetSzerkesztoDlg.EagleSMSTipus:= uzleti;
    UzenetSzerkesztoDlg.ShowModal;
  // finally
  //  if UzenetSzerkesztoDlg <> nil then UzenetSzerkesztoDlg.Release;
  //  end;  // try-finally
end;
 }

procedure TMegbizasUjFmDlg.StatuszBeiras(mbkod : string);
begin
	if mbkod = '' then begin
		Exit;
	end;
	Query_Update(QueryUj3, 'MEGBIZAS',
		[
		'MB_AFKOD', ''''+EgyebDlg.user_code+'''',
		'MB_AFNEV', ''''+EgyebDlg.user_name+'''',
		'MB_AFLAG', IntToStr(StrToINtDef(FormatDateTime('nnss', now),0))
		], ' WHERE MB_MBKOD = '+mbkod );
end;

procedure TMegbizasUjFmDlg.StatuszTorles;
begin
	Query_Update(QueryUj3, 'MEGBIZAS',
		[
		'MB_AFKOD', ''''+''+'''',
		'MB_AFNEV', ''''+''+'''',
		'MB_AFLAG', IntToStr(StrToINtDef(FormatDateTime('nnss', now),0))
		], ' WHERE MB_AFKOD = '''+EgyebDlg.user_code+'''' );
end;

procedure TMegbizasUjFmDlg.SummaTolto;
begin
	// Felt�ltj�k a checksumm�t
  // 2017-12-19  nem haszn�ljuk, kiveszem
	{Timer1.Enabled	:= false;
	Query_Run(QueryUj3, 'SELECT SUM(MB_AFLAG) OSSZEG FROM MEGBIZAS ');
	checksumma	    := StrToIntDef(QueryUj3.FieldByName('OSSZEG').AsString,1);
	Timer1.Enabled	:= true;
  }
end;

procedure TMegbizasUjFmDlg.BitBtn4Click(Sender: TObject);
begin
	EgyebDlg.Calendarbe(M3, True);
	// Az intervallum megad�sa
	alapstr	:= 'SELECT * FROM MEGSEGED, MEGBIZAS WHERE MS_MBKOD = MB_MBKOD AND MS_DATUM >= '''+M2.Text+''' '+
		'AND MS_DATUM <= '''+M3.Text+''' '+ STATU_kizaras + szur140;
	SQL_ToltoFo(GetOrderBy(true));
end;

procedure TMegbizasUjFmDlg.BitBtn10Click(Sender: TObject);
begin
	// Let�r�lj�k a felt�teleket
	alapstr	:= 'SELECT * FROM MEGSEGED, MEGBIZAS WHERE MS_MBKOD = MB_MBKOD '+ STATU_kizaras + szur140;
	SQL_ToltoFo(GetOrderBy(true));
end;

procedure TMegbizasUjFmDlg.BitBtn11Click(Sender: TObject);
var
	szurosor	: integer;
	ujkod		: string;
	vikod		: string;
begin
	// Az �tcsatol�s megval�s�t�sa
	if not voltalt then begin
		voltalt 	:= true;
		Exit;
	end;
	// Hozz�rendelj�k a rekordot a j�rathoz
	if Query1.FieldByName('MB_JAKOD').AsString = '' then begin
		NoticeKi('A kiv�lasztott megb�z�shoz m�g nem tartozik j�rat!');
		Exit;
	end;
	// Megjelen�tj�k a rendsz�mhoz tartoz� j�ratokat, amib�l ki kell v�lasztani a megfelel�t
	if Query1.FieldByName('MB_RENSZ').AsString = '' then begin
		NoticeKi('Nincs megadva rendsz�m a megb�z�shoz!');
		Exit;
	end;
	if Query1.FieldByName('MB_VEKOD').AsString = '' then begin
		NoticeKi('Nincs megadva megb�z� a megb�z�shoz!');
		Exit;
	end;
	// A j�rat sz�r�s be�ll�t�sa
	EgyebDlg.SzuroGrid.RowCount 			:= EgyebDlg.SzuroGrid.RowCount + 1;
	szurosor								:= EgyebDlg.SzuroGrid.RowCount - 1 ;
	EgyebDlg.SzuroGrid.Cells[0,szurosor] 	:= '190';
	EgyebDlg.SzuroGrid.Cells[1,szurosor] 	:= 'JA_RENDSZ = '''+Query1.FieldByName('MB_RENSZ').AsString+''' ';
	EgyebDlg.megbizaskod					:= Query1.FieldByName('MB_MBKOD').AsString;
	JaratValaszto(jaratkod, jaratszam);
	EgyebDlg.megbizaskod					:= '';
	// A sz�r�s t�rl�se
	GridTorles(EgyebDlg.SzuroGrid, szurosor);
	if jaratkod.Text = '' then begin
		Exit;
	end;
	// Ellen�rizz�k a j�rat ellen�rz�tts�g�t
	if EllenorzottJarat(jaratkod.Text) > 0 then begin
		Exit;
	end;
	// Ellen�rizz�k a j�rathoz tartoz� viszonyokat
	Query_Run(Query2, 'SELECT * FROM VISZONY WHERE VI_JAKOD = '''+jaratkod.Text+''' ');
	if Query2.RecordCount < 1 then begin
		NoticeKi('A j�rathoz m�g nem kapcsol�dnak viszonylatok!');
		Exit;
	end;
	// A megfelel� viszonylat kiv�laszt�sa
	vikod	:= '';
	if Query2.RecordCount > 1 then begin
		Application.CreateForm(TRendvalDlg,RendvalDlg);
		RendvalDlg.Tolt('A megb�z�s kiv�laszt�sa',jaratkod.Text);
		if RendvalDlg.vanrek then begin
			RendvalDlg.ShowModal;
			if RendvalDlg.ret_vikod <> '' then begin
				{A megb�z�s hozz�rendel�se a k�lts�ghez}
				vikod	:= RendvalDlg.ret_vikod;
			end;
		end;
		RendvalDlg.Destroy;
	end else begin
		vikod	:= Query2.FieldByName('VI_VIKOD').AsString;
	end;
	if vikod = '' then begin
		Exit;
	end;
	// Az �tkapcsol�s v�grehajt�sa
	ujkod	:= IntToStr(GetNextCode('MBCSATOL', 'MC_MCKOD', 1, 0));
	if not Query_Insert(Query2, 'MBCSATOL',
		[
		'MC_MCKOD', 	ujkod,
		'MC_MBKOD',     Query1.FieldByName('MB_MBKOD').AsString,
		'MC_JAKOD',		''''+Query1.FieldByName('MB_JAKOD').AsString+'''',
		'MC_VIKOD',		Query1.FieldByName('MB_VIKOD').AsString,
		'MC_DATUM',     ''''+FormatDateTime('YYY.MM.DD.', now)+'''',
		'MC_IDOPT',     ''''+FormatDateTime('hh:nn:ss', now)+''''
		]) then begin
		NoticeKi('A csatol�si rekord besz�r�sa nem siker�lt!');
		Exit;
	end;
   // A JARATMEGBIZAS m�dos�t�sa
	Query_Update( Query2, 'JARATMEGBIZAS', [
		'JM_JAKOD', ''''+jaratkod.Text+''''
		], ' WHERE JM_MBKOD = '+IntToStr(StrToIntDef(Query1.FieldByName('MB_MBKOD').AsString, 0))+
   	' AND JM_JAKOD = '''+Query1.FieldByName('MB_JAKOD').AsString+'''');

	Query_Update( Query2, 'MEGBIZAS', [
		'MB_JAKOD', ''''+jaratkod.Text+'''',
		'MB_JASZA', ''''+jaratszam.Text+'''',
		'MB_VIKOD', IntToStr(StrToIntDef(vikod, 0))
	], ' WHERE MB_MBKOD = '+IntToStr(StrToIntDef(Query1.FieldByName('MB_MBKOD').AsString, 0)));
	MegbizasSzamlaTolto(IntToStr(StrToIntDef(Query1.FieldByName('MB_MBKOD').AsString, 0)));
	ModLocate (Query1, 'MS_MSKOD', Query1.FieldByName('MS_MSKOD').AsString );
end;

procedure TMegbizasUjFmDlg.BitBtn12Click(Sender: TObject);
begin
	// Az �tcsatolt elemek list�z�sa
	if not voltalt then begin
		voltalt 	:= true;
		Exit;
	end;
	// Hozz�rendelj�k a rekordot a j�rathoz
	if Query1.FieldByName('MB_JAKOD').AsString = '' then begin
		NoticeKi('A kiv�lasztott megb�z�shoz m�g nem tartozik j�rat!');
		Exit;
	end;
	// Ellen�rizz�k a j�rathoz tartoz� �tcsatol�sokat
	Query_Run(Query2, 'SELECT * FROM MBCSATOL WHERE MC_MBKOD = '''+Query1.FieldByName('MB_MBKOD').AsString+''' ');
	if Query2.RecordCount < 1 then begin
		NoticeKi('A megb�z�shoz m�g nem tartoznak �tcsatol�sok!');
		Exit;
	end;
	// Az �tcsatol�sok megjelen�t�se
	Application.CreateForm(TCsatListFmDlg,CsatListFmDlg);
	CsatListFmDlg.Tolt('�tcsatol�sok list�ja',Query1.FieldByName('MB_MBKOD').AsString);
	CsatListFmDlg.ShowModal;
	CsatListFmDlg.Destroy;
end;

function TMegbizasUjFmDlg.KeresChange(mezo, szoveg : string) : string;
begin
	Result	:= szoveg;
	// if UpperCase(mezo) = 'MB_SZKOD' then begin
  if UpperCase(mezo) = 'MB_SAKO2' then begin
		Result := elotag + Format('%5.5d',[StrToIntDef(szoveg,0)]);
	end;
end;

procedure TMegbizasUjFmDlg.BitBtn13Click(Sender: TObject);
var
  rendsz,rajt,cel,rc:string;
begin
  rc:='0';
  if Query1.FieldByName('MS_FETIDO').AsString <>'' then rc:='1';
  if Query1.FieldByName('MS_LETIDO').AsString <>'' then rc:='2';

  rajt:=Query1.FieldByName('MS_FORSZ').AsString;
  rajt:=rajt+', '+Query1.FieldByName('MS_FELIR').AsString;
  rajt:=rajt+', '+Query1.FieldByName('MS_FELTEL').AsString;
  rajt:=rajt+', '+Query1.FieldByName('MS_FELCIM').AsString;


  cel:=Query1.FieldByName('MS_ORSZA').AsString;
  cel:=cel+', '+Query1.FieldByName('MS_LELIR').AsString;
  cel:=cel+', '+Query1.FieldByName('MS_LERTEL').AsString;
  cel:=cel+', '+Query1.FieldByName('MS_LERCIM').AsString;

  rendsz:=Query1.FieldByName('MB_RENSZ').AsString;
  //if (rendsz<>'') then
   EgyebDlg.GoogleMap(rendsz,rajt,cel,rc,False)     ;

 {
  cel:=Query1.FieldByName('MS_ORSZA').AsString;
  cel:=cel+', '+Query1.FieldByName('MS_LELIR').AsString;
  cel:=cel+', '+Query1.FieldByName('MS_LERTEL').AsString;
  cel:=cel+', '+Query1.FieldByName('MS_LERCIM').AsString;
 if Query1.FieldByName('MB_RENSZ').AsString<>'' then
  EgyebDlg.GoogleMap(Query1.FieldByName('MB_RENSZ').AsString,'',cel,'',False)     ;
  }
end;

procedure TMegbizasUjFmDlg.BitBtn14Click(Sender: TObject);
begin
  inherited;
  Application.CreateForm(TFlottaHelyzetDlg, FlottaHelyzetDlg);
  FlottaHelyzetDlg.Left:=(Screen.Width-FlottaHelyzetDlg.Width)  div 2;
  FlottaHelyzetDlg.Top:=(Screen.Height-FlottaHelyzetDlg.Height) div 2;
  // FlottaHelyzetDlg.Left:= Screen.DesktopWidth - FlottaHelyzetDlg.Width;
  // FlottaHelyzetDlg.Top:= 1;
  // if Width> FlottaHelyzetDlg.Left then
  //    Width:= FlottaHelyzetDlg.Left; // �sszeh�zzuk az alap ablakot
	FlottaHelyzetDlg.ShowModal;
  FlottaHelyzetDlg.Free;
end;

procedure TMegbizasUjFmDlg.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
//   FGKErvenyesseg.Free;
end;

procedure TMegbizasUjFmDlg.CheckBox1Click(Sender: TObject);
begin
  szur140:='';
  if CheckBox1.Checked then
  begin
    szur140:=' and MS_TEIDO='''+'''';
//    szur140:=' and MS_STATK<>'''+'140''';
    ALAPSTR:=ALAPSTR+szur140;
  end
  else
    ALAPSTR:=StringReplace(ALAPSTR,' and MS_TEIDO='''+'''','',[rfReplaceAll]);
//    ALAPSTR:=StringReplace(ALAPSTR,' and MS_STATK<>'''+'140''','',[rfReplaceAll]);

	SQL_ToltoFo(GetOrderBy(true));
end;

procedure TMegbizasUjFmDlg.Atcsatolas(esakod, usakod: string);
var
  vikod, evikod: string;
begin
  Query_Run( QueryAtcsat,  'SELECT * from SZFEJ where SA_KOD='''+esakod+'''');
  QueryAtcsat.First;
  while not QueryAtcsat.Eof do
  begin
    vikod:= Query_Select2('VISZONY','VI_SAKOD','VI_JAKOD',usakod,QueryAtcsat.fieldbyname('VI_JAKOD').asstring,'VI_VIKOD') ;
    evikod:=Query_Select2('VISZONY','VI_SAKOD','VI_JAKOD',usakod,QueryAtcsat.fieldbyname('VI_JAKOD').asstring,'VI_EVIKOD') ;
		Query_Run(EgyebDlg.QueryKozos, 'UPDATE MEGBIZAS SET MB_VIKOD = '''+vikod+''' WHERE MB_VIKOD='''+evikod+'''');

    QueryAtcsat.Next;
  end;
  QueryAtcsat.Close;
end;

procedure TMegbizasUjFmDlg.BitBtn8MouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
 if Button=mbRight then
 begin
	// J�rat megjelen�t�se
	if Query1.FieldByName('MS_JAKOD').AsString <> '' then begin
		Query_Run (QueryUj3, 'SELECT * FROM JARAT WHERE JA_KOD = '''+Query1.FieldByName('MS_JAKOD').AsString+''' ',true);
		if QueryUj3.RecordCount < 1 then begin
			NoticeKi('El�z� �vi j�rat!');
			Exit;
		end;
		Application.CreateForm(TJaratbeDlg, JaratbeDlg);
    JaratbeDlg.megtekint:= (QueryUj3.FieldByName('JA_FAZIS').AsInteger > 0);
		JaratbeDlg.Tolto('J�rat m�dos�t�s',Query1.FieldByName('MS_JAKOD').AsString);
		JaratbeDlg.ShowModal;
		JaratbeDlg.Destroy;
    JaratbeDlg:=nil;

  	ModLocate (Query1, 'MS_MSKOD', Query1.FieldByName('MS_MSKOD').AsString );
	  SummaTolto;
	end;
 end;
end;

procedure TMegbizasUjFmDlg.Nemnormljratltrehozs1Click(Sender: TObject);
var
   kodok, msid   : string;
   i       : integer;
begin
   // L�trehozzuk a nem norm�l j�ratot
   kodok   := '';
   if SelectList.Count < 1 then begin
       // Csak az aktu�lis rekord legyen kiv�lasztva
        kodok := Query1.FieldByName('MS_MSKOD').AsString;
       // kodok := Query1.FieldByName('MS_ID').AsString;
   end else begin
       for i := 0 to SelectList.Count - 1 do begin
          // nem jutottam d�l�re, hogy legyen-e az MS_ID a kulcsmez�, vagy kider�tsem hogy hogyan lesznek a duplik�lt MS_MSKOD �rt�kek.
          // ez most egy �thidal� megold�s.
          // msid:= Query_select('MEGSEGED', 'MS_MSKOD', SelectList[i], 'MS_ID');
          kodok := kodok + SelectList[i]+',';
          // kodok := kodok + SelectList[i]+',';
          end;
       kodok := copy(kodok, 1, Length(kodok)-1);
   end;
	Application.CreateForm(TMegjaratbeDlg, MegjaratbeDlg);
	MegjaratbeDlg.Tolto('�j nem norm�l j�rat gener�l�sa' ,kodok);
	MegjaratbeDlg.ShowModal;
   if MegjaratbeDlg.ret_kod <> '' then begin
       // Friss�t�s
	    // ModLocate (Query1, 'MS_ID', Query1.FieldByName('MS_ID').AsString );
      ModLocate (Query1, 'MS_MSKOD', Query1.FieldByName('MS_MSKOD').AsString );
   end;
   MegjaratbeDlg.Destroy;
   // A kijel�l�sek megsz�ntet�se
   SelectList.Clear;
   DBGrid2.Refresh;
end;

procedure TMegbizasUjFmDlg.pmiCMROKClick(Sender: TObject);
var
	msid, S: string;
begin
  if pmiCMROK.Checked then begin
    NoticeKi('M�r kor�bban megt�rt�nt!');
    Exit;
    end;
  msid:= Query1.FieldByName('MS_ID').AsString;
  S:= 'update megseged set MS_CMROK='''+FormatDateTime('yyyy.mm.dd. HH:MM', now)+''' where MS_ID='+msid;
	Query_Run(EgyebDlg.QueryKozos, S);
  ModLocate (Query1, 'MS_MSKOD', Query1.FieldByName('MS_MSKOD').AsString );
	SummaTolto;
end;

procedure TMegbizasUjFmDlg.pmiEKROKClick(Sender: TObject);
var
	msid, S: string;
begin
  if pmiEKROK.Checked then begin
    NoticeKi('M�r kor�bban megt�rt�nt!');
    Exit;
    end;
  msid:= Query1.FieldByName('MS_ID').AsString;
  S:= 'update megseged set MS_EKROK='''+FormatDateTime('yyyy.mm.dd. HH:MM', now)+''' where MS_ID='+msid;
	Query_Run(EgyebDlg.QueryKozos, S);
  ModLocate (Query1, 'MS_MSKOD', Query1.FieldByName('MS_MSKOD').AsString );
	SummaTolto;
end;

// loop-ban h�vjuk!
function TMegbizasUjFmDlg.AutoTolto(d1, d2 : string) : boolean;
begin
   result:= true;
	Query_Run(QueryUj3, 'SELECT * FROM MEGSEGED WHERE MS_FAJKO IN (1,2,3) AND ( MS_JAKOD = '''' OR MS_JAKOD IS NULL) AND MS_DATUM > '''+d1+ ''' '+
       ' AND ( ( MS_DATUM < '''+d2+''' ) OR ( '''+d2+''' = '''' ) ) ORDER BY MS_RENSZ, MS_FAJKO, MS_MBKOD ');
   if QueryUj3.RecordCount > 0 then begin
       caption := '�rintett rekordok : '+IntToStr(QueryUj3.RecordCount);
       case StrToIntDef(QueryUj3.FieldByName('MS_FAJKO').AsString, 0) of
           1: Fellerakas(1);
           2: Elohuzas;
           3: Fellerakas(3);
       end;
   end else begin
       Result  := false;
   end;
end;

procedure TMegbizasUjFmDlg.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
   oldcaption  : string;
   dd1, dd2    : string;
begin
   if key = VK_F5 then begin
       oldcaption  := caption;
       // Az id�szak beolvas�sa
       Application.CreateForm(TJaratSegedDatDlg, JaratSegedDatDlg);
       dd1 := '';
       dd2 := '';
       JaratSegedDatDlg.ShowModal;
       if ( ( JaratSegedDatDlg.MD1.Text <> '' ) or (JaratSegedDatDlg.MD2.Text <> '' ) ) then begin
           dd1 := JaratSegedDatDlg.MD1.Text;
           dd2 := JaratSegedDatDlg.MD2.Text;
       end;
       JaratSegedDatDlg.Destroy;
       if ( ( dd1 <> '' ) or (dd2 <> '' ) ) then begin
           while AutoTolto(dd1, dd2) do begin  // am�g van mit csin�lnia
               Application.ProcessMessages;
           end;
       end;
       caption := oldcaption;
   end;
   if key = VK_F6 then begin
       if NoticeKi('Val�ban vissza akarja vonni az automatikus t�lt�s eredm�ny�t?', NOT_QUESTION) = 0 then begin
           AutoToltoVisszavon;
       end;
   end;
   // J_FOFORMUJ-ba ker�lt.
   // if key = VK_F9 then begin
   //   UresSMSKuldes;
   // end;
   inherited FormKeyDown(Sender,Key,Shift);    
end;

procedure TMegbizasUjFmDlg.AutoToltoVisszavon;
begin
   // Visszavonjuk az autotolto -vel v�grehajtott m�veleteket
   Query_Run( QueryUj3, 'UPDATE MEGSEGED SET MS_JAKOD = '''', MS_JASZA= '''' WHERE MS_MSKOD IN (SELECT JG_MSKOD FROM JARATMEGSEGED WHERE JG_JAKOD IN (SELECT JA_KOD FROM JARAT WHERE JA_ALVAL = -3))');
   Query_Run( QueryUj3, 'DELETE FROM JARATMEGSEGED WHERE JG_JAKOD IN (SELECT JA_KOD FROM JARAT WHERE JA_ALVAL = -3)');
   Query_Run( QueryUj3, 'DELETE FROM JARPOTOS WHERE JP_JAKOD IN (SELECT JA_KOD FROM JARAT WHERE JA_ALVAL = -3)');
   Query_Run( QueryUj3, 'DELETE FROM JARSOFOR WHERE JS_JAKOD IN (SELECT JA_KOD FROM JARAT WHERE JA_ALVAL = -3)');
   Query_Run( QueryUj3, 'DELETE FROM JARAT WHERE JA_ALVAL = -3');
end;

procedure TMegbizasUjFmDlg.Elohuzas;
var
   rsz     : string;
   potsz   : string;
   sof     : string;
   sof2    : string;
   jakod   : string;
   alkod   : string;
   jadat1  : string;
   jaido1  : string;
   jadat2  : string;
   jaido2  : string;
   voltjarat   : boolean;
   jaratszam   : string;
   tipusstr    : string;
   kmdij       : string;
begin
   // El�h�z�s lekezel�se -> k�l�n j�rat l�trehoz�sa
   rsz     := QueryUj3.FieldByName('MS_RENSZ').AsString;
   sof     := QueryUj3.FieldByName('MS_DOKOD').AsString;
   jakod   := '';
   jadat1  := '';
   jaido1  := '';
   jadat2  := '';
   jaido2  := '';
   // A d�tumok, id�pontok meghat�roz�sa
   jadat1  := QueryUj3.FieldByName('MS_FETDAT').AsString;
   jaido1  := QueryUj3.FieldByName('MS_FETIDO').AsString;
   jadat2  := QueryUj3.FieldByName('MS_LETDAT').AsString;
   jaido2  := QueryUj3.FieldByName('MS_LETIDO').AsString;

   // L�tre kell hozni az �j j�ratot
   jakod	    := Format('%6.6d',[GetNextStrCode('JARAT', 'JA_KOD', 0, 6)]);
   alkod	    := Format('%5.5d',[ GetJaratAlkod(copy(jadat1, 1, 4))]);
   jaratszam   := QueryUj3.FieldByName('MS_TEFOR').AsString+'-'+ alkod;
   {Az �j j�rat l�trehoz�sa}
   Query_Run ( Query02, 'INSERT INTO JARAT (JA_KOD, JA_ALVAL, JA_ALKOD, JA_RENDSZ, JA_FAZIS, JA_FAZST, JA_FAJKO, JA_ORSZ) VALUES ('''+jakod+''', -3, '''+alkod+''', '+
       ''''+rsz+''', 0, '''+GetJaratFazis(0)+''', 1, '''+QueryUj3.FieldByName('MS_TEFOR').AsString+''' )',true);
   tipusstr    := 'el�h�z�s';
   Query_Update ( Query02, 'JARAT' , [
           'JA_JKEZD',		''''+jadat1+'''',
           'JA_KEIDO',		''''+jaido1+'''',
           'JA_JVEGE',		''''+jadat2+'''',
           'JA_VEIDO',		''''+jaido2+'''',
           'JA_MSFAJ',     ''''+tipusstr+''''
           ], ' WHERE JA_KOD = '''+jakod+''' ' );

   kmdij	:= Query_select('GEPKOCSI', 'GK_RESZ', rsz, 'GK_DIJ1');
   // A sof�r berak�sa
   if sof <> '' then begin
       Query_Run(Query02, 'SELECT * FROM JARSOFOR WHERE JS_SOFOR ='''+sof+''' AND JS_JAKOD = '''+jakod+''' ');
       if Query02.RecordCount < 1 then begin
           Query_Insert(  Query02, 'JARSOFOR' ,
               [
               'JS_JAKOD',     ''''+jakod+'''',
               'JS_SOFOR',     ''''+sof+'''',
               'JS_JADIJ',     SqlSzamString(StringSzam(kmdij),8,2),
               'JS_SORSZ',     '1'
               ]);
       end;
   end;

   if QueryUj3.FieldByName('MS_POTSZ').AsString <> '' then begin
       potsz   := QueryUj3.FieldByName('MS_POTSZ').AsString;
       // A p�tkocsi berak�sa
       Query_Run(Query02, 'SELECT * FROM JARPOTOS WHERE JP_POREN ='''+potsz+''' AND JP_JAKOD = '''+jakod+''' ');
       if Query02.RecordCount < 1 then begin
           Query_Insert(  Query02, 'JARPOTOS' ,
               [
               'JP_JAKOD',     ''''+jakod+'''',
               'JP_POREN',     ''''+potsz+'''',
               'JP_JPKOD',     IntToStr(GetNextCode('JARPOTOS', 'JP_JPKOD', 1, 0))
               ]);
       end;
   end;
   if QueryUj3.FieldByName('MS_DOKO2').AsString <> '' then begin
       sof2   := QueryUj3.FieldByName('MS_DOKO2').AsString;
       // A 2. sof�r berak�sa
       Query_Run(Query02, 'SELECT * FROM JARSOFOR WHERE JS_SOFOR ='''+sof2+''' AND JS_JAKOD = '''+jakod+''' ');
       if Query02.RecordCount < 1 then begin
           Query_Insert(  Query02, 'JARSOFOR' ,
               [
               'JS_JAKOD',     ''''+jakod+'''',
               'JS_SOFOR',     ''''+sof2+'''',
               'JS_JADIJ',     SqlSzamString(StringSzam(kmdij),8,2),
               'JS_SORSZ',     '1'
               ]);
       end;
   end;

   Query_Run(Query01, 'SELECT MS_MSKOD FROM MEGSEGED WHERE MS_MBKOD = '+QueryUj3.FieldByName('MS_MBKOD').AsString+ ' AND MS_SORSZ = '+QueryUj3.FieldByName('MS_SORSZ').AsString);    // a bar�tait is update-elj�k
   while not Query01.Eof do begin
      // A megsegedek hozz�kapcsol�sa a j�rathoz
      Query_Update( Query02, 'MEGSEGED', [
       'MS_JAKOD', ''''+jakod+'''',
       'MS_JASZA', ''''+jaratszam+''''
       ], ' WHERE MS_MSKOD = '+Query01.FieldByName('MS_MSKOD').AsString);    //

     // A JARATMEGSEGED t�bla t�lt�se
     Query_Run ( Query01, 'INSERT INTO JARATMEGSEGED ( JG_JAKOD, JG_MBKOD, JG_MSKOD, JG_FOJAR ) VALUES ( '+
       ''''+ jakod + ''', '+QueryUj3.FieldByName('MS_MBKOD').AsString +','+ Query01.FieldByName('MS_MSKOD').AsString +','''+ Query_Select('MEGBIZAS', 'MB_MBKOD', QueryUj3.FieldByName('MS_MBKOD').AsString, 'MB_JAKOD')+''' )');
     Query01.Next;
     end;  // while

end;

procedure TMegbizasUjFmDlg.Fellerakas(tipus : integer);
var
   nev     : string;
   tel     : string;
   cim     : string;
   dat     : string;
   ido     : string;
   rsz     : string;
   potsz   : string;
   sof     : string;
   sof2    : string;
   jakod   : string;
   alkod   : string;
   jadat1  : string;
   jaido1  : string;
   jadat2  : string;
   jaido2  : string;
   voltjarat   : boolean;
   jaratszam   : string;
   tipusstr    : string;
   kmdij       : string;
   S: string;
begin
   // Lerak�s lekezel�se
   rsz     := QueryUj3.FieldByName('MS_RENSZ').AsString;
   sof     := QueryUj3.FieldByName('MS_DOKOD').AsString;
   // Lerak�sr�l van sz�
   if tipus = 1 then begin
       // El�rak�sr�l van sz�
       nev     := 'MS_TEFNEV';
       tel     := 'MS_TEFTEL';
       cim     := 'MS_TEFCIM';
       dat     := 'MS_FETDAT';
       ido     := 'MS_FETIDO';
   end else begin
       nev     := 'MS_TENNEV';
       tel     := 'MS_TENTEL';
       cim     := 'MS_TENCIM';
       dat     := 'MS_LETDAT';
       ido     := 'MS_LETIDO';
   end;
   // El�sz�r ellen�rizz�k, hogy van-e ilyen m�s k�sz j�rattal
   Query_Run(Query01, 'SELECT * FROM MEGSEGED WHERE MS_JAKOD <> '''' AND MS_RENSZ = '''+rsz+''' AND MS_FAJKO = '+IntToStr(tipus)+
       ' AND MS_DOKOD = '''+sof+''' AND '+nev+' = '''+QueryUj3.FieldByName(nev).AsString+''' AND '+tel+' = '''+QueryUj3.FieldByName(tel).AsString+''' '+
       ' AND '+cim+' = '''+QueryUj3.FieldByName(cim).AsString+''' AND '+dat+' = '''+QueryUj3.FieldByName(dat).AsString+''' '+
       ' AND '+ido+' = '''+QueryUj3.FieldByName(ido).AsString+''' ');
   jakod   := '';
   jadat1  := '';
   jaido1  := '';
   jadat2  := '';
   jaido2  := '';
   voltjarat   := false;
   if Query01.RecordCount > 0 then begin
       // Tal�ltunk egy j�ratot, ehhez kell hozz�kapcsolni a mostanit is (a d�tumokkal nem kell foglalkozni, mert a mostaniakban van -> id�pontok ???)
       voltjarat   := true;
       jakod       := Query01.FieldByName('MS_JAKOD').AsString;
       NoticeKi('L�tez� j�rat! jakod = '+jakod);
       Query_Run(Query02, 'SELECT * FROM JARAT WHERE JA_KOD = '''+jakod+''' ');
       if Query02.RecordCount <> 1 then begin
           NoticeKi('Hi�nyz� j�rat : jakod = '+jakod);
       end else begin
           jaratszam   := Query02.FieldByName('JA_ORSZ').AsString + '-'+ Query02.FieldByName('JA_ALKOD').AsString;
           jadat1      := Query02.FieldByName('JA_JKEZD').AsString;
           jaido1      := Query02.FieldByName('JA_KEIDO').AsString;
           jadat2      := Query02.FieldByName('JA_JVEGE').AsString;
           jaido2      := Query02.FieldByName('JA_VEIDO').AsString;
       end;
   end;

   // Megkeress�k az �sszes ilyen param�terrel rendelkez� megseged rekordot -> ezek mennek egy j�ratba
   Query_Run(Query01, 'SELECT * FROM MEGSEGED WHERE MS_FAJKO = '+IntToStr(tipus)+' AND ( MS_JAKOD = '''' OR MS_JAKOD IS NULL ) AND MS_RENSZ = '''+rsz+''' '+
       ' AND MS_DOKOD = '''+sof+''' AND '+nev+' = '''+QueryUj3.FieldByName(nev).AsString+''' AND '+tel+' = '''+QueryUj3.FieldByName(tel).AsString+''' '+
       ' AND '+cim+' = '''+QueryUj3.FieldByName(cim).AsString+''' AND '+dat+' = '''+QueryUj3.FieldByName(dat).AsString+''' '+
       ' AND '+ido+' = '''+QueryUj3.FieldByName(ido).AsString+''' ');

   // A d�tumok, id�pontok meghat�roz�sa
   Query01.First;
   jadat1  := Query01.FieldByName('MS_FETDAT').AsString;
   jaido1  := Query01.FieldByName('MS_FETIDO').AsString;
   jadat2  := Query01.FieldByName('MS_LETDAT').AsString;
   jaido2  := Query01.FieldByName('MS_LETIDO').AsString;
   while not Query01.Eof do begin
       if jadat1 + ' ' + jaido1 > Query01.FieldByName('MS_FETDAT').AsString + ' ' + Query01.FieldByName('MS_FETIDO').AsString then begin
           // A kezd� d�tum + id�pont p�rost cs�kkenteni kell
           jadat1  := Query01.FieldByName('MS_FETDAT').AsString;
           jaido1  := Query01.FieldByName('MS_FETIDO').AsString;
       end;
       if jadat2 + ' ' + jaido2 < Query01.FieldByName('MS_LETDAT').AsString + ' ' + Query01.FieldByName('MS_LETIDO').AsString then begin
           // A v�gs� d�tum + id�pont p�rost n�velni kell
           jadat2  := Query01.FieldByName('MS_LETDAT').AsString;
           jaido2  := Query01.FieldByName('MS_LETIDO').AsString;
       end;
       Query01.Next;
   end;

   // A j�rat lekezel�se
   if not voltjarat then begin
       // L�tre kell hozni az �j j�ratot
       jakod	    := Format('%6.6d',[GetNextStrCode('JARAT', 'JA_KOD', 0, 6)]);
       alkod	    := Format('%5.5d',[ GetJaratAlkod(copy(jadat1, 1, 4))]);
       if StrToIntDef(alkod, 0) < 1851 then begin
           alkod := '01851';
       end;
       jaratszam   := 'HU-'+ alkod;
       {Az �j j�rat l�trehoz�sa}
       Query_Run ( Query02, 'INSERT INTO JARAT (JA_KOD, JA_ALVAL, JA_ALKOD, JA_RENDSZ, JA_FAZIS, JA_FAZST, JA_FAJKO, JA_ORSZ, JA_EUROS) VALUES ('''+jakod+''', -3, '''+alkod+''', '+
           ''''+rsz+''', 0, '''+GetJaratFazis(0)+''', 1, ''HU'', 2 )',true);
   end;
   // Friss�teni kell a r�gebbi j�ratot
   tipusstr    := 'el�rak�s';
   if tipus = 3 then begin
      tipusstr    := 'lerak�';
   end;
   Query_Update ( Query02, 'JARAT' , [
           'JA_JKEZD',		''''+jadat1+'''',
           'JA_KEIDO',		''''+jaido1+'''',
           'JA_JVEGE',		''''+jadat2+'''',
           'JA_VEIDO',		''''+jaido2+'''',
           'JA_MSFAJ',     ''''+tipusstr+''''
           ], ' WHERE JA_KOD = '''+jakod+''' ' );

   kmdij	:= Query_select('GEPKOCSI', 'GK_RESZ', rsz, 'GK_DIJ1');
   // A sof�r berak�sa
   if sof <> '' then begin
       Query_Run(Query02, 'SELECT * FROM JARSOFOR WHERE JS_SOFOR ='''+sof+''' AND JS_JAKOD = '''+jakod+''' ');
       if Query02.RecordCount < 1 then begin
           Query_Insert(  Query02, 'JARSOFOR' ,
               [
               'JS_JAKOD',     ''''+jakod+'''',
               'JS_SOFOR',     ''''+sof+'''',
               'JS_JADIJ',     SqlSzamString(StringSzam(kmdij),8,2),
               'JS_SORSZ',     '1'
               ]);
       end;
   end;

   // P�tkocsik �s 2. sof�r�k berak�sa
   Query01.First;
   while not Query01.Eof do begin
       if Query01.FieldByName('MS_POTSZ').AsString <> '' then begin
           potsz   := Query01.FieldByName('MS_POTSZ').AsString;
           // A p�tkocsi berak�sa
           Query_Run(Query02, 'SELECT * FROM JARPOTOS WHERE JP_POREN ='''+potsz+''' AND JP_JAKOD = '''+jakod+''' ');
           if Query02.RecordCount < 1 then begin
               Query_Insert(  Query02, 'JARPOTOS' ,
                   [
                   'JP_JAKOD',     ''''+jakod+'''',
                   'JP_POREN',     ''''+potsz+'''',
                   'JP_JPKOD',     IntToStr(GetNextCode('JARPOTOS', 'JP_JPKOD', 1, 0))
                   ]);
           end;
       end;
       if Query01.FieldByName('MS_DOKO2').AsString <> '' then begin
           sof2   := Query01.FieldByName('MS_DOKO2').AsString;
           // A 2. sof�r berak�sa
           Query_Run(Query02, 'SELECT * FROM JARSOFOR WHERE JS_SOFOR ='''+sof2+''' AND JS_JAKOD = '''+jakod+''' ');
           if Query02.RecordCount < 1 then begin
               Query_Insert(  Query02, 'JARSOFOR' ,
                   [
                   'JS_JAKOD',     ''''+jakod+'''',
                   'JS_SOFOR',     ''''+sof2+'''',
                   'JS_JADIJ',     SqlSzamString(StringSzam(kmdij),8,2),
                   // 'JS_SORSZ',     '1'
                   'JS_SORSZ',     '2'
                   ]);
           end;
       end;

       // A megsegedek hozz�kapcsol�sa a j�rathoz
       Query_Update( Query02, 'MEGSEGED', [
           'MS_JAKOD', ''''+jakod+'''',
           'MS_JASZA', ''''+jaratszam+''''
           ], ' WHERE MS_MSKOD = '+Query01.FieldByName('MS_MSKOD').AsString);    //

       // A JARATMEGSEGED t�bla t�lt�se, ha m�g nem szerepel benne ez a kapcsolat
       S:= 'SELECT * FROM JARATMEGSEGED WHERE JG_JAKOD ='''+jakod+''' AND JG_MBKOD = '+Query01.FieldByName('MS_MBKOD').AsString;
       S:= S+ ' AND JG_MSKOD = '+Query01.FieldByName('MS_MSKOD').AsString;
       Query_Run(Query02, S);
       if Query02.RecordCount < 1 then begin
           Query_Run ( Query1, 'INSERT INTO JARATMEGSEGED ( JG_JAKOD, JG_MBKOD, JG_MSKOD, JG_FOJAR ) VALUES ( '+
             ''''+ jakod + ''', '+Query01.FieldByName('MS_MBKOD').AsString +','+ Query01.FieldByName('MS_MSKOD').AsString +','''+ Query_Select('MEGBIZAS', 'MB_MBKOD', Query01.FieldByName('MS_MBKOD').AsString, 'MB_JAKOD')+''' )');
           end;
       Query01.Next;
   end;
end;

procedure TMegbizasUjFmDlg.PopupMenu1Popup(Sender: TObject);
begin
  if Query1.FieldByName('MS_CMROK').AsString <> '' then
    pmiCMROK.Checked:= True
  else pmiCMROK.Checked:= False;
  if Query1.FieldByName('MS_EKROK').AsString <> '' then
    pmiEKROK.Checked:= True
  else pmiEKROK.Checked:= False;
end;

end.

