unit POD_Export;

interface

uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls, Egyeb,
  Forms, Dialogs, DBTables, DB, StdCtrls, Buttons, Mask, DBCtrls, Grids,
  Printers, ExtCtrls, ADODB, ComObj, Shellapi;

type
  TPOD_ExportDlg = class(TForm)
    Label1: TLabel;
    BitBtn4: TBitBtn;
    BitBtn6: TBitBtn;
    M1: TMaskEdit;
    Label3: TLabel;
    BitBtn10: TBitBtn;
    Query1: TADOQuery;
    pnlMegbizok: TPanel;
    BtnSend: TBitBtn;
    M2: TMaskEdit;
    procedure FormCreate(Sender: TObject);
    procedure BitBtn4Click(Sender: TObject);
    procedure M1Click(Sender: TObject);
    procedure BitBtn6Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn10Click(Sender: TObject);
    procedure M1Exit(Sender: TObject);
    function XLS_Generalas: boolean;
    procedure BtnSendClick(Sender: TObject);
  private
    voltgeneralas, voltkuldes: boolean;
    PODFileName: string;
  public
    gr: TStringGrid;
  end;

var
  POD_ExportDlg: TPOD_ExportDlg;

implementation

uses
  j_sql, Kozos, Kozos_Export, J_Datamodule, Windows, J_Excel;
{$R *.DFM}

procedure TPOD_ExportDlg.FormCreate(Sender: TObject);
begin
  EgyebDlg.SetADOQueryDatabase(Query1);
  m2.Text:= DatumHozNap(copy(EgyebDlg.MaiDatum, 1, 8)+'01.', -1, true); // e h�nap elseje - 1 nap = az el�z� h�nap utols� napja
	m1.Text:= copy(M2.Text, 1, 8)+'01.';  // az el�z� h�nap els� napja
  pnlMegbizok.Caption:= EgyebDlg.Read_SZGrid('372','100');
  voltgeneralas:= false;
  voltkuldes:= false;
end;

procedure TPOD_ExportDlg.BitBtn4Click(Sender: TObject);
begin
  if not DatumExit(M1, false) then begin
    Exit;
    end;
  if not DatumExit(M2, false) then begin
    Exit;
    end;
  // Az XLS �llom�ny gener�l�sa
  Screen.Cursor := crHourGlass;
  if XLS_Generalas then begin
     voltgeneralas:= True;
     NoticeKi('A POD jelent�s gener�l�sa befejez�d�tt!');
     end;
  Screen.Cursor := crDefault;
end;

procedure TPOD_ExportDlg.BitBtn6Click(Sender: TObject);
begin
  if voltgeneralas and not (voltkuldes) then begin
     if NoticeKi('A jelent�s nincs elk�ldve. Ki akar l�pni k�ld�s n�lk�l?', NOT_QUESTION ) = 0 then begin
  			Close;
        end;
      end
  else begin
    Close;  // csak �gy
    end;  // else
end;

procedure TPOD_ExportDlg.BtnSendClick(Sender: TObject);
var
   mail_to, subject, body: string;
begin
  mail_to:=EgyebDlg.Read_SZGrid('372','102'); //
   // p�lda: POD [0000012345] March 2015
  subject:='POD ['+EgyebDlg.Read_SZGrid('372','101')+']';  // CARRIER_ID
  subject:=subject+' '+AngolHonapok[StrToInt(copy(M1.Text,6,2))];  // pl. April
  subject:=subject+' '+copy(M1.Text,1,4);  // �v
  body:= ''; // 'Dear Bridgestone,'+Chr(13)+Chr(10)+Chr(13)+Chr(10)+'Please find POD file attached.';

  CreateOutlookEmailAndOpen(mail_to, subject, body, PODFileName);
  voltkuldes:=True;  // azt nem tudjuk, hogy a felhaszn�l� t�nyleg elk�ldte-e
end;

procedure TPOD_ExportDlg.M1Click(Sender: TObject);
begin
  with Sender as TMaskEdit do
  begin
    SelectAll;
  end;
end;

procedure TPOD_ExportDlg.BitBtn10Click(Sender: TObject);
begin
  // EgyebDlg.Calendarbe(M1);
  // EgyebDlg.ElsoNapEllenor(M1, M2);
  EgyebDlg.CalendarBe_idoszak(M1, M2);
end;

procedure TPOD_ExportDlg.BitBtn1Click(Sender: TObject);
begin
  EgyebDlg.Calendarbe(M2);
end;

procedure TPOD_ExportDlg.M1Exit(Sender: TObject);
begin
  DatumExit(M1, false);
  EgyebDlg.ElsoNapEllenor(M1, M2);
end;

function TPOD_ExportDlg.XLS_Generalas : boolean;
const
   templatefn='POD_TEMPLATE.xls';  // az �res template
   oszlopok = 16;
var
	ActRow, ActCol: integer;
	EX: TJExcel;
  S, KezdoDatum: string;
begin
	result:= true;
  // fn minta: POD_0000012345_March_2015.xls
  PODFileName:=EgyebDlg.DocumentPath+'LIST\';
	PODFileName:=PODFileName+'POD_'+EgyebDlg.Read_SZGrid('372','101');  // CARRIER_ID
  PODFileName:=PODFileName+'_'+AngolHonapok[StrToInt(copy(M1.Text,6,2))];  // pl. April
  PODFileName:=PODFileName+'_'+copy(M1.Text,1,4);  // �v
  PODFileName:=PODFileName+'.xls';

	if FileExists(PODFileName) then begin
		DeleteFile(PChar(PODFileName));
	  end;
	// EllenListTorol;

  KezdoDatum:=DatumHozNap(M1.Text, -30, True);  // egy h�napot visszal�p�nk
  S:='select distinct '+
      ''''+EgyebDlg.Read_SZGrid('372','101')+''' as CARRIER_ID, '+
		  'MB_POZIC as SHIPMENT_ID, '+
		  'case when (ms_le.MS_LERKDAT+ms_le.MS_LERKIDO <= ms_le.MS_LERDAT+ms_le.MS_LERIDO) then ''Y'' else ''N'' end as DELIVERY_ONTIME, '+
		  'case when (ms_le.MS_LERKDAT+ms_le.MS_LERKIDO <= ms_le.MS_LERDAT+ms_le.MS_LERIDO) then '''' else ''6'' end as DELIVERY_NOTONTIME_REASON, '+
      'case when (ms_le.MS_LEPALHI>0) then ''N'' else ''Y'' end as DELIVERY_COMPLETE,  '+
      'case when (ms_le.MS_LEPALHI>0) then substring(ms_le.MS_HIOKA,1,1) else '''' end as DELIVERY_INCOMPLETE_REASON,  '+
      '''TYRE'' as MEASUREMENT, '+
		  'case when (ms_le.MS_LEPALHI>0) then ms_le.MS_LEPALHI  else '''' end as UNITS_MEASUREMENT_INCOMPLETE, '+
	   	'ms_le.MS_LEPAL - ms_le.MS_LEPALHI as ACTUAL_UNITS_MEASUREMENT, '+
		  'ms_fel.MS_FETDAT as ACTUAL_LOAD_DATE, '+
		  'ms_le.MS_LETDAT as DELIVERY_DATE, '+
		  'ms_fel.MS_FERKIDO as LOAD_ARR_TIME,  '+
		  ' '''' as LOAD_TIME, '+
		  'ms_le.MS_LERKIDO as UNLOAD_ARR_TIME, '+
		  ' '''' as UNLOAD_TIME, '+
		  'ms_le.MS_LETIDO as UNLOAD_DPT_TIME  '+
      ' from MEGBIZAS, MEGSEGED ms_fel, MEGSEGED ms_le, '+
  	  '(select MS_MBKOD MINIMAX_MBKOD, min(ISNULL(MS_FETDAT+MS_FETIDO,''9999999999'')) MINFEL, max(ISNULL(MS_LETDAT+MS_LETIDO,'''')) MAXLE '+
		  '    from MEGSEGED, MEGBIZAS where MB_MBKOD = MS_MBKOD and MB_VEKOD in ('+pnlMegbizok.Caption+')'+
      '    and MB_DATUM between '''+KezdoDatum+''' and '''+M2.Text+'''' +
		  '    group by MS_MBKOD) minimax '+
		  'where MB_MBKOD = ms_fel.MS_MBKOD and MB_MBKOD = ms_le.MS_MBKOD '+
		  'and  ms_fel.MS_ID <> ms_le.MS_ID '+
		  'and minimax.MINIMAX_MBKOD = MB_MBKOD '+
		  'and ms_fel.MS_FETDAT+ms_fel.MS_FETIDO = minimax.MINFEL '+
		  'and ms_fel.MS_TIPUS=0 '+
		  'and ms_le.MS_LETDAT+ms_le.MS_LETIDO = minimax.MAXLE '+
		  'and ms_le.MS_TIPUS=1  '+
      'and ms_fel.MS_FETDAT between '''+M1.Text+''' and '''+M2.Text+'''' +
      'order by ms_fel.MS_FETDAT';  // ACTUAL_LOAD_DATE
	Query_Run(Query1, S);
	if Query1.RecordCount < 1 then begin
			NoticeKi('Nincs a felt�teleknek megfelel� elem!');
		  Exit;
    	end;  // if
	BitBtn4.Hide;
	// Az XLS megnyit�sa
	EX:= TJexcel.Create(Self);
  try
   try
  	if not EX.OpenXlsFile(EgyebDlg.ExePath+'INI\'+templatefn) then begin
			NoticeKi('Nem siker�lt megnyitnom az Excel template-et! ('+EgyebDlg.TempPath+'INI\'+templatefn+')');
  		end
   	else begin
    	EX.ColNumber	:= oszlopok;
      // EX.SetActiveSheet(2); // az "Instructions" az 1. sheet, nek�nk a "POD template" kell, ez a 2.
      EX.SetActiveSheet(1); //  2016-02-05: a "POD template" sheet lett az 1.
      ActRow:=2;
      while not Query1.Eof do begin
         EX.InitRow;
         for ActCol:= 1 to oszlopok do
            EX.SetRowcell(ActCol, Query1.Fields[ActCol-1].AsString);
         EX.SaveRow(ActRow);
         Inc(ActRow);
         Query1.Next;
         end;  // while
      EX.SaveFileAsXls(PODFileName);
      voltgeneralas:= True;
      end; // else
    except
      on E: exception do begin
         NoticeKi('Hiba a POD riport gener�l�s�n�l: '+E.Message);
         Hibakiiro('Hiba a POD riport gener�l�s�n�l: '+E.Message);
         end;  // on E
      end;  // try-except
    finally
  	  EX.Free;
      end; // try-finally

	BitBtn4.Show;
	ShellExecute(Application.Handle,'open',PChar(PODFileName), '', '', SW_SHOWNORMAL );
end;

end.
