unit MSR_Export;

interface

uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls, Egyeb,
  Forms, Dialogs, DBTables, DB, StdCtrls, Buttons, Mask, DBCtrls, Grids,
  Printers, Shellapi, ExtCtrls, ADODB, J_Excel;

type

  TMSR_ExportDlg = class(TForm)
	 Label1: TLabel;
    BitBtn4: TBitBtn;
    BitBtn6: TBitBtn;
    M1: TMaskEdit;
	 M2: TMaskEdit;
	 Label2: TLabel;
	 Label3: TLabel;
	 BitBtn10: TBitBtn;
	 BitBtn1: TBitBtn;
	 Query1: TADOQuery;
    Label4: TLabel;
	 Query2: TADOQuery;
	 Query3: TADOQuery;
    M50: TMaskEdit;
    M5: TMaskEdit;
    BitBtn5: TBitBtn;
	 Label6: TLabel;
	 M4: TMaskEdit;
    CheckBox1: TCheckBox;
	 procedure FormCreate(Sender: TObject);
	 procedure BitBtn4Click(Sender: TObject);
	 procedure BitBtn6Click(Sender: TObject);
	 procedure M1Click(Sender: TObject);
	 procedure BitBtn10Click(Sender: TObject);
	 procedure BitBtn1Click(Sender: TObject);
	 procedure M1Exit(Sender: TObject);
	 function XLS_Generalas : boolean;
	 procedure BitBtn5Click(Sender: TObject);
    procedure CheckBox1Click(Sender: TObject);
  private
		kilepes		: boolean;
  public
		gr			: TStringGrid;
		voltkilepes	: boolean;
		kellshell	: boolean;
		kelluzenet	: boolean;
		kombinalt	: boolean;
    kelluzpotlek: boolean;
  end;

var
  MSR_ExportDlg: TMSR_ExportDlg;

implementation

uses
	j_sql, Kozos, ValVevo, J_DataModule, Kozos_Export,Kombi_Export;
{$R *.DFM}

procedure TMSR_ExportDlg.FormCreate(Sender: TObject);
begin
	gr			:= nil;
	M4.Text		:= 'Izs� L�szl�';
	kilepes     := true;
	kellshell	:= true;
	voltkilepes	:= false;
	kelluzenet	:= true;
	kombinalt	:= false;
  kelluzpotlek:=True;
	EgyebDlg.SetADOQueryDatabase(Query1);
	EgyebDlg.SetADOQueryDatabase(Query2);
	EgyebDlg.SetADOQueryDatabase(Query3);
	Kezdoertekek(M1, M2, M5, M50);
end;

procedure TMSR_ExportDlg.BitBtn4Click(Sender: TObject);
begin
	if not DatumExit(M1, false) then begin
		Exit;
	end;
	if not DatumExit(M2, false) then begin
		Exit;
	end;
	if M50.Text = '' then begin
		NoticeKi('A megb�z� kiv�laszt�sa k�telez�!');
		M5.SetFocus;
		Exit;
	end;
	// Az XLS �llom�ny gener�l�sa
	Screen.Cursor	:= crHourGlass;
	XLS_Generalas;
	Screen.Cursor	:= crDefault;
	if kombinalt then begin
		Close;
	end else begin
		NoticeKi('A riport gener�l�sa befejez�d�tt!');
	end;
end;

procedure TMSR_ExportDlg.BitBtn6Click(Sender: TObject);
begin
	if not kilepes then begin
		kilepes	:= true;
	end else begin
		Close;
	end;
end;

procedure TMSR_ExportDlg.M1Click(Sender: TObject);
begin
	with Sender as TMaskEdit do begin
		SelectAll;
	end;
end;

procedure TMSR_ExportDlg.BitBtn10Click(Sender: TObject);
begin
	// EgyebDlg.Calendarbe(M1);
	// EgyebDlg.ElsoNapEllenor(M1, M2);
  EgyebDlg.CalendarBe_idoszak(M1, M2);
end;

procedure TMSR_ExportDlg.BitBtn1Click(Sender: TObject);
begin
	EgyebDlg.Calendarbe(M2, True);
end;

procedure TMSR_ExportDlg.M1Exit(Sender: TObject);
begin
	DatumExit(M1,false);
	EgyebDlg.ElsoNapEllenor(M1, M2);
end;

function TMSR_ExportDlg.XLS_Generalas : boolean;
var
	fn			: String;
	sorszam,i		: integer;
	ujkod		: string;
	lanossz		: double;
	EX	  		: TJExcel;
	datkul		: integer;
	orakul		: integer;
	suly, paletta		: double;
   suly2       : double;
  belf_alval:string;
  oW,oAB,oAC,oP: double;
  kelluzpotl, alvall: boolean;
  rendsz, gkkat: string;
begin

  belf_alval	:= EgyebDlg.Read_SZGrid( '999', '748' );
//  belf_alval:='0607';

  oW:=0;
  oAB:=0;
  oAC:=0;
  oP:=0;
	EllenListTorol;
	result		:= true;
	fn		:= 'MGR_'+M1.Text+'_'+M2.Text;
	while Pos('.', fn) > 0 do begin
		fn		:= copy(fn, 1, Pos('.', fn) - 1 ) + copy(fn, Pos('.', fn) + 1, 999 );
	end;
	fn			:= EgyebDlg.DocumentPath + fn + '.XLS';;
	if FileExists(fn) then begin
		DeleteFile(PChar(fn));
	end;

	Query_Run(Query1, 'SELECT MB_MBKOD, MB_RENSZ, MB_POZIC, MB_FUTID, MB_FUDIJ, MB_VIKOD, MB_JAKOD, MB_DATUM,MB_KISCS,MB_ALVAL,MB_NUZPOT,MB_ALV FROM MEGBIZAS '+
			' WHERE MB_MBKOD IN ( SELECT MS_MBKOD FROM MEGSEGED WHERE MS_LETDAT >= '''+M1.Text+''' AND MS_LETDAT <= '''+M2.Text+''') '+
			' AND MB_VEKOD IN ('+M50.Text+') '+
			' ORDER BY MB_DATUM, MB_MBKOD ' );
	if Query1.RecordCount < 1 then begin
		if kelluzenet then begin
			NoticeKi('Nincs a felt�teleknek megfelel� elem!');
		end;
		result		:= false;
		Exit;
	end;
	BitBtn4.Hide;
	// Az XLS megnyit�sa
	EX 	:= TJexcel.Create(Self);
	if not EX.OpenXlsFile(fn) then begin
		if kelluzenet then begin
			NoticeKi('Nem siker�lt megnyitnom az Excel �llom�nyt!');
		end;
	end else begin
		EX.ColNumber	:= 36;
		EX.InitRow;
		EX.SetActiveSheet(0);

		EX.SetActiveSheet(1);
		sorszam			:= 1;

		EX.SetRowcell(1, 'Monthly Global Carrier Shipment Report');
		EX.SaveRow(sorszam);
		Inc(sorszam);
		EX.SetRowcell(1, 'Document version: 3');
		EX.SaveRow(sorszam);
		Inc(sorszam);
		Inc(sorszam);

		Ex.EmptyRow;
		EX.SetRowcell(1, 'Carrier:');
		EX.SetRowcell(2, 'J&S Speed Kft.');
		EX.SaveRow(sorszam);
		Inc(sorszam);
		EX.SetRowcell(1, 'Contact:');
		EX.SetRowcell(2, M4.Text);
		EX.SaveRow(sorszam);
		Inc(sorszam);
		EX.SetRowcell(1, 'Delivery period from-to');
		EX.SetRowcell(2, M1.Text + ' - ' + M2.Text);
		EX.SaveRow(sorszam);
		Inc(sorszam);
		Inc(sorszam);

		Ex.EmptyRow;
		EX.SetRowcell( 1, 'Shipment details');
		EX.SetRowcell( 4, 'Origin details');
		EX.SetRowcell(10, 'Destination details');
		EX.SetRowcell(16, 'Shipment characteristics');
		EX.SetRowcell(18, 'Shipment dates in calendar days');
		EX.SetRowcell(22, 'Weight and volume');
		EX.SetRowcell(28, 'Costs');
		EX.SaveRow(sorszam);
		EX.MergeCell(sorszam,  1, sorszam,  3);
		EX.MergeCell(sorszam,  4, sorszam,  9);
		EX.MergeCell(sorszam, 10, sorszam, 15);
		EX.MergeCell(sorszam, 16, sorszam, 17);
		EX.MergeCell(sorszam, 18, sorszam, 21);
		EX.MergeCell(sorszam, 22, sorszam, 27);
		EX.MergeCell(sorszam, 28, sorszam, 29);
		Inc(sorszam);

		Ex.EmptyRow;
		EX.SetRowcell( 1, 'Forwarder');
		EX.SetRowcell( 2, 'Shipment number (AWB, BOL, etc.)');
		EX.SetRowcell( 3, 'TE Shipment reference number');
		EX.SetRowcell( 4, 'Consignor');
		EX.SetRowcell( 5, 'Origin city');
		EX.SetRowcell( 6, 'Origin zipcode');
		EX.SetRowcell( 7, 'Origin country ISO-code');
		EX.SetRowcell( 8, 'City of origin air- or seaport or road-hub');
		EX.SetRowcell( 9, 'Country ISO-code of origin air- or seaport or road-hub');
		EX.SetRowcell(10, 'Consignee');
		EX.SetRowcell(11, 'Destination city');
		EX.SetRowcell(12, 'Destination zipcode');
		EX.SetRowcell(13, 'Destination country ISO-code');
		EX.SetRowcell(14, 'City of dest. air- or seaport or road-hub');
		EX.SetRowcell(15, 'Country ISO-code of dest. air- or seaport or road-hub');
		EX.SetRowcell(16, 'Mode of Transport');
		EX.SetRowcell(17, 'Service');
		EX.SetRowcell(18, 'Pickup or receipt date (dd/mm/yyyy)');
		EX.SetRowcell(19, 'Delivery date (dd/mm/yyyy)');
		EX.SetRowcell(20, 'Fiscal month');
		EX.SetRowcell(21, 'Actual transit time');
		EX.SetRowcell(22, 'Agreed transit time');
		EX.SetRowcell(23, 'Palette');
		EX.SetRowcell(24, 'Weight unit of measure');
		EX.SetRowcell(25, 'Actual weight');
		EX.SetRowcell(26, 'Chargeable weight');
		EX.SetRowcell(27, 'Volume unit of measure');
		EX.SetRowcell(28, 'Volume');
		EX.SetRowcell(29, 'Currency');
		EX.SetRowcell(30, 'Total costs excl. VAT excl. duty');
		EX.SetRowcell(31, 'Fuel costs');
	 	EX.SetRowcell(32, 'Vehicle type');
		EX.SaveRow(sorszam);
		Inc(sorszam);

		kilepes				:= false;
		Query1.DisableControls;
		while ( ( not Query1.Eof ) and (not kilepes) ) do begin
		 Query_Run(Query2, 'SELECT MS_MBKOD FROM MEGSEGED WHERE MS_MBKOD = '+Query1.FieldByName('MB_MBKOD').AsString+' AND MS_FETDAT <'''+ M1.Text+''' ');
     if Query2.RecordCount=0 then
     begin
			Application.ProcessMessages;
      rendsz:=Query1.FieldByName('MB_RENSZ').AsString ;
      alvall:=Query1.FieldByName('MB_ALV').AsInteger=1 ;
      kelluzpotl:= StrToIntDef( Query1.FieldByName('MB_NUZPOT').AsString,0) = 0 ;
			Label4.Caption	:= 'D�tum : '+Query1.FieldByName('MB_DATUM').AsString + ' - megb�z�s k�d : ' +Query1.FieldByName('MB_MBKOD').AsString;
			Label4.Update;
			lanossz				:= StringSzam(Query1.FieldByName('MB_FUDIJ').AsString);
			EX.EmptyRow;
			EX.SetRowcell(1, 'J&S Speed Kft.');
			if Query1.FieldByName('MB_POZIC').AsString<>EmptyStr then
  			EX.SetRowcell(3, Query1.FieldByName('MB_POZIC').AsString)
      else
  			EX.SetRowcell(3, 'NON SAP');

			// Megkeress�k az els� felrak�st
			Query_Run(Query2, 'SELECT MS_FELNEV, MS_FELTEL, MS_FELIR, MS_FORSZ FROM MEGSEGED WHERE MS_MBKOD = '+Query1.FieldByName('MB_MBKOD').AsString+' AND MS_TIPUS = 0 ORDER BY MS_MSKOD ');
			EX.SetRowcell( 4, Query2.FieldByName('MS_FELNEV').AsString);
			EX.SetRowcell( 5, Query2.FieldByName('MS_FELTEL').AsString);
			EX.SetRowcell( 6, Query2.FieldByName('MS_FELIR').AsString);
			EX.SetRowcell( 7, Query2.FieldByName('MS_FORSZ').AsString);


			EX.SetRowcell( 16, 'ROAD');

      if StrToIntDef(Query1.FieldByName('MB_KISCS').AsString, 0) > 0 then
  			EX.SetRowcell( 17, 'EXPRESS')
      else
  			EX.SetRowcell( 17, 'standard');

			// Megkeress�k az utols� lerak�st
			Query_Run(Query2, 'SELECT MS_LERNEV, MS_LERTEL, MS_LELIR, MS_ORSZA FROM MEGSEGED WHERE MS_MBKOD = '+Query1.FieldByName('MB_MBKOD').AsString+' AND MS_TIPUS = 1 ORDER BY MS_MSKOD ');
			Query2.Last;
			EX.SetRowcell(10, Query2.FieldByName('MS_LERNEV').AsString);
			EX.SetRowcell(11, Query2.FieldByName('MS_LERTEL').AsString);
			EX.SetRowcell(12, Query2.FieldByName('MS_LELIR').AsString);
			EX.SetRowcell(13, Query2.FieldByName('MS_ORSZA').AsString);

			// A d�tumok meghat�roz�sa
			Query_Run(Query2, 'SELECT MIN(MS_FETDAT) FELDAT, MAX(MS_LETDAT) LERDAT FROM MEGSEGED WHERE MS_MBKOD = '+Query1.FieldByName('MB_MBKOD').AsString );
			EX.SetRowcell(18, Query2.FieldByName('FELDAT').AsString);
			EX.SetRowcell(19, Query2.FieldByName('LERDAT').AsString);
			DateTimeDifference( Query2.FieldByName('FELDAT').AsString, '', Query2.FieldByName('LERDAT').AsString, '' , datkul, orakul);
			EX.SetRowcell(21, IntToStr(datkul));

      // GK kat.
      gkkat:='';
			// Query_Run(Query2, 'SELECT FT_KATEG FROM FUVARTABLA WHERE FT_FTKOD = '+IntToStr(StrToIntDef(Query1.FieldByName('MB_FUTID').AsString, 0)));
      Query_Run(Query2, 'SELECT FT_GKKAT FROM FUVARTABLA WHERE FT_FTKOD = '+IntToStr(StrToIntDef(Query1.FieldByName('MB_FUTID').AsString, 0)));

      { 20150723ig
      if not Query2.Eof then
  			gkkat:=Query2.FieldByName('FT_GKKAT').AsString
      else
        gkkat:= Query_Select('GEPKOCSI','GK_RESZ',rendsz,'GK_GKKAT');
      if alvall then
        gkkat:= Query_Select('ALGEPKOCSI','AG_RENSZ',rendsz,'AG_GKKAT');
        }

      if not Query2.Eof then begin
  			gkkat:=Query2.FieldByName('FT_GKKAT').AsString;
        end
      else begin  // ha a fuvart�bl�ban nem tal�ljuk
        if not alvall then
            gkkat:= Query_Select('GEPKOCSI','GK_RESZ',rendsz,'GK_GKKAT')
        else
            gkkat:= Query_Select('ALGEPKOCSI','AG_RENSZ',rendsz,'AG_GKKAT');
        end;  // else

  		EX.SetRowcell(32, gkkat);

			// Az enged�lyezett napok beolvas�sa a LAN ID alapj�n
			Query_Run(Query2, 'SELECT FT_TRDAY, FT_FTEUR FROM FUVARTABLA WHERE FT_FTKOD = '+IntToStr(StrToIntDef(Query1.FieldByName('MB_FUTID').AsString, 0)));
			EX.SetRowcell(22, Query2.FieldByName('FT_TRDAY').AsString);
			EX.SetRowcell(24, 'KG');
			suly	    := GetSuly(Query1.FieldByName('MB_MBKOD').AsString);
			paletta	    := GetPaletta(Query1.FieldByName('MB_MBKOD').AsString);
			EX.SetRowcell(25, Format('%f', [suly]));
			suly2	    := GetKobSuly(Query1.FieldByName('MB_MBKOD').AsString);
           if suly2 = 0 then begin
			    EX.SetRowcell(26, Format('%f', [suly]));        // Nincs k�b�s s�ly
           end else begin
			    EX.SetRowcell(26, Format('%f', [suly2]));        // K�b�s s�ly
           end;
			EX.SetRowcell(23, Format('%f', [paletta]));
    //  oW:=oW+suly;
			EX.SetRowcell(29, 'EUR');

			//Fuvard�j �s �zemanyagp�tl�k kisz�m�t�sa
			Query_Run(Query3, 'SELECT VI_UJKOD FROM VISZONY WHERE VI_VIKOD = '+ IntToStr(StrToIntDef(Query1.FieldByName('MB_VIKOD').AsString, 0)) +
				' AND VI_JAKOD = '''+Query1.FieldByName('MB_JAKOD').AsString+''' ');
			if Query3.RecordCount <> 1  then begin
				// Nins sz�mla
				EllenListAppend('A megb�z�shoz nem tartozik sz�mla! Megb�z�s k�d : '+Query1.FieldByName('MB_MBKOD').AsString);
			end else begin
				ujkod	:= Query3.FieldByName('VI_UJKOD').AsString;
				// A sz�mla k�pz�se
				if StrToIntDef(ujkod, 0) = 0 then begin
					EllenListAppend('A megb�z�shoz nem tartozik sz�mla! Megb�z�s k�d : '+Query1.FieldByName('MB_MBKOD').AsString);
				end else begin
					JSzamla	:= TJSzamla.Create(Self);
					JSzamla.FillSzamla(ujkod);
          if (Query1.FieldByName('MB_MBKOD').AsString = '56121') then
            NoticeKi('56121');
					// if kelluzpotlek and kelluzpotl and (JSzamla.uapotlekeur = 0)and(pos(JSzamla.szamlaszam ,Kombi_ExportDlg.kellaszamla)=0) then begin
          // 2017-02-13 Mintha ezt a "Kombi_ExportDlg.kellaszamla"-t nem t�lten�nk fel...
          if kelluzpotlek and kelluzpotl and (JSzamla.uapotlekeur = 0) then begin
						EllenListAppend('Az �zemanyag p�tl�k �sszege 0! Sz�mlasz�m : '+JSzamla.szamlaszam);
					end else begin
						EX.SetRowcell(31, Format('%.2f', [JSZAMLA.uapotlekeur]));
            //oAC:=oAC+ JSZAMLA.uapotlekeur;
            //if lanossz>20000 then
            //  date;
//						if ( Abs ( JSzamla.ossz_nettoeur - JSzamla.spedicioeur - JSzamla.uapotlekeur - lanossz ) > 0.00001 )and(pos(Query1.FieldByName('MB_ALVAL').AsString,Kombi_ExportDlg.belf_alval)=0) then begin
						if ( Abs ( JSzamla.ossz_nettoeur - JSzamla.spedicioeur - JSzamla.uapotlekeur - lanossz ) > 0.00001 )and(pos(Query1.FieldByName('MB_ALVAL').AsString,belf_alval)=0) then begin
							EllenListAppend('A sz�mla nett� eur - sped�ci�s d�j <> megb�z�si fuvard�j! Sz�mla : '+JSzamla.szamlaszam+'  Megb�z�s k�d : '+Query1.FieldByName('MB_MBKOD').AsString);
						end else begin
              lanossz:= JSzamla.ossz_nettoeur - JSzamla.spedicioeur - JSzamla.uapotlekeur;
							//EX.SetRowcell(28, Format('%.2f', [lanossz]));
							EX.SetRowcell(30, Format('%.2f', [lanossz+JSZAMLA.uapotlekeur]));
              oW:=oW+suly;
              oP:=oP+paletta;
              oAC:=oAC+ JSZAMLA.uapotlekeur;
              oAB:=oAB+ JSZAMLA.uapotlekeur+lanossz;
							// Minden rendben volt, lehet menteni a sort
							EX.SaveRow(sorszam);
							Inc(sorszam);
							SorLogolas(gr, Query1.FieldByName('MB_MBKOD').AsString, suly, lanossz+JSZAMLA.uapotlekeur, JSZAMLA.uapotlekeur, 1);
						end;
					end;
					JSzamla.Destroy;
				end;
			end;
     end;
			Query1.Next;
		end;
	end;
  for i:=1 to 22+1 do
  	EX.SetRowcell(i,'');
  for i:=25+2 to 27+2 do
  	EX.SetRowcell(i,'');
	EX.SetRowcell(25, Format('%f', [oW]));
	EX.SetRowcell(26, Format('%f', [oW]));
	EX.SetRowcell(27, Format('%.2f', [oAB]));
	EX.SetRowcell(31, Format('%.2f', [oAC]));
	EX.SetRowcell(23, Format('%f', [oP]));

	EX.SaveRow(sorszam+2);
  ///////////////////////////////
	EX.SaveFileAsXls(fn);
	EX.Free;
	voltkilepes	:= kilepes;
	if ( ( EgyebDlg.ellenlista.Count > 0 ) and kelluzenet ) then begin
		EllenListMutat('A monthly shipment riport eredm�nye', false);
	end;
	BitBtn4.Show;
	if kellshell then begin
		ShellExecute(Application.Handle,'open',PChar(fn), '', '', SW_SHOWNORMAL );
	end;
	Label4.Caption	:= '';
	Label4.Update;
	kilepes			:= true;
end;

procedure TMSR_ExportDlg.BitBtn5Click(Sender: TObject);
begin
	// T�bb vev� kiv�laszt�sa
	Application.CreateForm(TValvevoDlg, ValvevoDlg);
	ValvevoDlg.Tolt(M50.Text);
	ValvevoDlg.ShowModal;
	if ValvevoDlg.kodkilist.Count > 0 then begin
		M5.Text		:= ValvevoDlg.nevlista;
		M50.Text	:= ValvevoDlg.kodlista;
	end;
	ValvevoDlg.Destroy;
end;

procedure TMSR_ExportDlg.CheckBox1Click(Sender: TObject);
begin
  kelluzpotlek:=CheckBox1.Checked;
end;

end.
