unit Terkep;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OleCtrls, SHDocVw, ExtCtrls,MSHTML, StdCtrls, DB, ADODB, Mask,
  DBCtrls, WinInet, cefvcl, ceflib;

const
   WM_ONSHOWCALLBACK = WM_USER + 565;
  
type
  TFTerkep = class(TForm)
    Panel1: TPanel;
    Button1: TButton;
    Query1: TADOQuery;
    Query1PO_RENSZ: TStringField;
    Query1PO_GEKOD: TStringField;
    Query1PO_DATUM: TStringField;
    Query1PO_SEBES: TIntegerField;
    Query1PO_DIGIT: TStringField;
    Query1PO_SZELE: TFloatField;
    Query1PO_HOSZA: TFloatField;
    Timer1: TTimer;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    DataSource1: TDataSource;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    Button2: TButton;
    Query1PO_GYUJTAS: TStringField;
    DBEdit4: TDBEdit;
    CheckBox1: TCheckBox;
    DBText1: TDBText;
    Timer2: TTimer;
    Label4: TLabel;
    CheckBoxTraffic: TCheckBox;
    Query1PO_DAT: TDateTimeField;
    Query1PO_IRANYSZOG: TSmallintField;
    Query1PO_UDAT: TDateTimeField;
    Panel2: TPanel;
    Label5: TLabel;
    Edit1: TEdit;
    Button3: TButton;
    Edit2: TEdit;
    Button4: TButton;
    Button5: TButton;
    Label7: TLabel;
    Button6: TButton;
    Button7: TButton;
    CheckBox2: TCheckBox;
    Panel3: TPanel;
    Panel4: TPanel;
    Query1PO_DIGIT2: TWordField;
    Query1PO_VALID: TWordField;
    Query1PO_ETIPUS: TWordField;
    Chromium1: TChromium;
    pnlStatus: TPanel;
    procedure FormCreate(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Query1CalcFields(DataSet: TDataSet);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure CheckBox1Click(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    // procedure WebBrowser1DocumentComplete(Sender: TObject;
    //  const pDisp: IDispatch; var URL: OleVariant);
    procedure Timer2Timer(Sender: TObject);
    procedure CheckBoxTrafficClick(Sender: TObject);
    procedure DBEdit1Enter(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure Button6Click(Sender: TObject);
    procedure Button7Click(Sender: TObject);
    procedure CheckBox2Click(Sender: TObject);
    procedure Panel3Click(Sender: TObject);
    procedure Panel4Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Chromium1ConsoleMessage(Sender: TObject;
      const browser: ICefBrowser; const message, source: ustring; line: Integer;
      out Result: Boolean);
  private
    { Private declarations }
    // HTMLWindow2: IHTMLWindow2;
    function GetRouteLink(Start, Destination: string): string;
    procedure UtvonalMutat(link: string);
    procedure SetHTML;
    procedure MyExecuteJavaScript(const JSString: string);
  public
    { Public declarations }
    Rendszam,fel_le:string;
    TESZT: Boolean;
  end;

var
  FTerkep: TFTerkep;
  TShow:Boolean;
  //Rendszam:string;
  nx,ny:Double;
  enx,eny:Double;
  sx,sy:string;
  pontdb, mp:integer;
  OK, vanrajt, done:Boolean;

implementation

uses
   ActiveX, Egyeb, DateUtils, Terkep2, Kozos, ShellAPI;


{$R *.dfm}

const
sh : string[16] = '0123456789ABCDEF';

var
HTMLStr: AnsiString;

function konvert(x : word;alap : integer) : string;
begin
    Result := '';
    if not(alap in [2..16]) then exit;
    if x=0 then exit;
    if (x div alap)<>0 then Result := konvert(x div alap,alap) + sh[(x mod alap)+1]
                       else Result := sh[x+1]
end;


procedure TFTerkep.SetHTML;
begin
HTMLStr:=
'<html> '+
'<head> '+
'<meta name="viewport" content="initial-scale=1.0, user-scalable=yes" /> '+
// '<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script> '+
'<script type="text/javascript" src="http://maps.google.com/maps/api/js?key='+EgyebDlg.MATRIXAPIKEY+'&sensor=true"></script> '+
'<script type="text/javascript"> '+
''+
''+
'  var geocoder; '+
'  var map;  '+
'  var trafficLayer;'+
'  var bikeLayer;'+
'  var markersArray = [];'+
''+
''+
'  function initialize() { '+
'    geocoder = new google.maps.Geocoder();'+
'    var latlng = new google.maps.LatLng(47.6570014953613,18.343879699707); '+
'    var myOptions = { '+
'      zoom: 13, '+
'      center: latlng, '+
'      mapTypeId: google.maps.MapTypeId.ROADMAP, '+
'      scaleControl: true '+
'    }; '+
'    map = new google.maps.Map(document.getElementById("map_canvas"), myOptions); '+
'    trafficLayer = new google.maps.TrafficLayer();'+
'    bikeLayer = new google.maps.BicyclingLayer();'+
'    map.set("streetViewControl", false);'+
'  } '+
''+
''+
'  function codeAddress(address,Msg) { '+
'    if (geocoder) {'+
'      geocoder.geocode( { address: address}, function(results, status) { '+
'        if (status == google.maps.GeocoderStatus.OK) {'+
'          map.setCenter(results[0].geometry.location);'+
'          PutMarker(results[0].geometry.location.lat(), results[0].geometry.location.lng(), Msg);'+
//'          PutMarker(results[0].geometry.location.lat(), results[0].geometry.location.lng(), results[0].geometry.location.lat()+","+results[0].geometry.location.lng());'+
'        } else {'+
'          alert("Geok�d meghat�roz�s sikertelen: " + status);'+
'        }'+
'      });'+
'    }'+
'  }'+
''+
''+
'  function GotoLatLng(Lat, Lang, Msg) { '+
'   var latlng = new google.maps.LatLng(Lat,Lang);'+
'   map.setCenter(latlng);'+
'   PutMarker(Lat, Lang, Msg);'+
// '   PutMarker(Lat, Lang, Lat+","+Lang);'+
'  }'+
''+
''+
'function ClearMarkers() {  '+
'  if (markersArray) {        '+
'    for (i in markersArray) {  '+
'      markersArray[i].setMap(null); '+
'    } '+
'  } '+
'}  '+
''+
'  function PutMarker(Lat, Lang, Msg) { '+
'   var latlng = new google.maps.LatLng(Lat,Lang);'+
'   var image = ''marker0.png'';'+
'   var marker = new google.maps.Marker({'+
'      position: latlng, '+
'      map: map,'+
//'      icon: image,'+
'      title: Msg+""'+
'  });'+
' markersArray.push(marker); '+
//' index= (markersArray.length % 10);'+
//' index= (markersArray.length);'+
//' if (index==0) { index=10 } '+
//' icon = "<a href="http://www.google.com/mapfiles/kml/paddle/&quot;+index+&quot;- lv.png">http://www.google.com/mapfiles/kml/paddle/"+index+"-lv.png</a>"; '+
//' icon = "e:\marker"+ index+".png"; '+
//' marker.setIcon(icon); '+
'  }'+
''+
''+
'  function TrafficOn()   { trafficLayer.setMap(map); }'+
''+
'  function TrafficOff()  { trafficLayer.setMap(null); }'+
''+''+
'  function BicyclingOn() { bikeLayer.setMap(map); }'+
''+
'  function BicyclingOff(){ bikeLayer.setMap(null);}'+
''+
'  function StreetViewOn() { map.set("streetViewControl", true); }'+
''+
'  function StreetViewOff() { map.set("streetViewControl", false); }'+
''+
'  google.maps.event.addDomListener(window, "load", initialize); '+
''+
''+'</script> '+
'</head> '+
'<body onload="initialize()"> '+
'  <div id="map_canvas" style="width:100%; height:100%"></div> '+
'</body> '+
'</html> ';
end;

procedure TFTerkep.FormCreate(Sender: TObject);
var
  aStream     : TMemoryStream;
  sx,sy:string;
begin
    pontdb:=0;
    SetHTML;
    Chromium1.Browser.MainFrame.LoadString(HTMLStr, 'source://html');
  {
    WebBrowser1.Navigate('about:blank');

    while WebBrowser1.ReadyState < READYSTATE_INTERACTIVE do Application.ProcessMessages;

    if Assigned(WebBrowser1.Document) then
    begin
      aStream := TMemoryStream.Create;
      try
         aStream.WriteBuffer(Pointer(HTMLStr)^, Length(HTMLStr));
         //aStream.Write(HTMLStr[1], Length(HTMLStr));
         aStream.Seek(0, soFromBeginning);
         (WebBrowser1.Document as IPersistStreamInit).Load(TStreamAdapter.Create(aStream));
      finally
         aStream.Free;
      end;
      HTMLWindow2 := (WebBrowser1.Document as IHTMLDocument2).parentWindow;
    end;

  Try
      sx:='47.6568565368652';
      sy:='18.3449516296387';
      HTMLWindow2.execScript(Format('GotoLatLng(%s,%s,%s)',[sx,sy,'Teszt']), 'JavaScript');
      while HTMLWindow2.document.readyState<> 'complete' do
        Application.ProcessMessages;

  Except
  End;
  }
end;

procedure TFTerkep.MyExecuteJavaScript(const JSString: string);
begin
   Chromium1.Browser.MainFrame.ExecuteJavaScript(JSString,'about:blank', 0);
end;

procedure TFTerkep.Button1Click(Sender: TObject);
var
  sdat,ido:string;
begin
 Panel3.Color:=clBtnFace;
 Panel4.Color:=clBtnFace;
 Panel1.Visible:=True;
 if Rendszam<>'' then
 begin
  if (CheckBox1.Checked) then
  begin
    Timer1.Enabled:=False;
    Timer1.Enabled:=True;
    mp:=0;
  end;
  FTerkep.Query1.Close;
  FTerkep.Query1.Open;
  if FTerkep.Query1.Locate('PO_RENSZ',Rendszam,[loCaseInsensitive	]) then
  begin
    nx:=Query1PO_SZELE.AsFloat;
    ny:=Query1PO_HOSZA.AsFloat;
    sx:=StringReplace(FloatToStr(nx),',','.',[rfReplaceAll]);
    sy:=StringReplace(FloatToStr(ny),',','.',[rfReplaceAll]);
    DBEdit3.Hint:=sx+' ; '+sy;
    if not vanrajt then
        Edit2.Text:=sx+', '+sy;
    OK:=True;
  end
  else
  begin
    nx:=0;
    ny:=0;
    Button4.Enabled:=False;
    Button5.Enabled:=False;  
   // Panel1.Visible:=False;
    Timer1.Enabled:=False;
    Timer2.Enabled:=False;
   // ShowMessage('Nincs adat!');
    OK:=False;
    //exit;
  end;
  if (nx=0) or(ny=0) then
  begin
  //  PostMessage(self.handle, WM_CLOSE, 0, 0);
  end
  else
   if (nx<>enx) or (ny<>eny) then
   begin
     Try
      inc(pontdb);
      ido:=IntToStr( HourOf(Query1PO_DAT.Value))+':'+IntToStr( MinuteOf(Query1PO_DAT.Value))+' ��:pp';
//    sdat:=IntToStr(pontdb)+'. ('+ido+')'+' ('+Query1PO_SEBES.AsString+' km/h)';
      sdat:=IntToStr(pontdb)+'. ('+ido+')'+' ('+Query1PO_SEBES.AsString+' km/h)';
      sdat:='"'+sdat+'"';
      MyExecuteJavaScript(Format('GotoLatLng(%s,%s,%s)',[sx,sy,sdat]));
      enx:=nx;
      eny:=ny;

   //   L_Irany2.Angle:=360-Query1PO_IRANYSZOG.Value     ;
     Except
      ShowMessage('Nem siker�lt a szolg�ltat�st el�rni!');
     End;
   end;
 end
 else
 begin
  Try
      sx:='47.6568565368652';
      sy:='18.3449516296387';
     // HTMLWindow2.execScript(Format('GotoLatLng(%s,%s,%s)',[sx,sy,'Teszt']), 'JavaScript');
  Except
  End;
 end;
end;

procedure TFTerkep.FormActivate(Sender: TObject);
var
  dwConnectionTypes: DWORD;
  vangkcs:Boolean;
begin
  dwConnectionTypes := INTERNET_CONNECTION_MODEM + INTERNET_CONNECTION_LAN +
    INTERNET_CONNECTION_PROXY;
  if (not InternetGetConnectedState(@dwConnectionTypes, 0))and(Rendszam<>'') then
     ShowMessage('Val�sz�n�leg nincs Internet kapcsolat!')
  else
  begin
   if not TESZT then
   begin
    FTerkep.Query1.Close;
    FTerkep.Query1.Open;
    vangkcs:= (fel_le<'2')and(Rendszam<>'')and(FTerkep.Query1.Locate('PO_RENSZ',Rendszam,[loCaseInsensitive	]));
    CheckBox1.Checked:=vangkcs;
    CheckBox1.OnClick(self);
    Panel1.Visible:=vangkcs;
    Panel1.Visible:=vangkcs;
    FTerkep2.Panel1.Visible:=vangkcs;
    Button4.Enabled:=vangkcs;
    Button5.Enabled:=vangkcs;
   // L_Irany.Visible:=vangkcs;
    Button4.Enabled:=(fel_le='0')and(vangkcs);      // felrak�hoz
    Button5.Enabled:=(fel_le='1')and(vangkcs);      // lerak�hoz
    Application.ProcessMessages;
    if not vangkcs then
    begin
      Sleep(2000);
      Panel3.OnClick(self);
//      Button6.OnClick(self);
    end;
    if CheckBox1.Checked then
     Button1.OnClick(self)
    else
    begin
      OK:=True;
    //  Button3.OnClick(self);
    end;
   end;
//    if not OK then
//    begin
//      PostMessage(self.handle, WM_CLOSE, 0, 0);
//    end;
  end;
  vanrajt:=Edit2.Text<>'';
end;

procedure TFTerkep.Button2Click(Sender: TObject);
begin
  close;
end;

procedure TFTerkep.Query1CalcFields(DataSet: TDataSet);
var
  digit:string;
  eszk, gypoz:integer;
begin
 // HexToBin(IntToHex(Query1PO_DIGIT2.Value,8));
  digit:= konvert(Query1PO_DIGIT2.Value,2);
  digit:=stringofchar('0',8-length(digit))+digit;
  eszk:=Query1PO_ETIPUS.Value;
  if eszk=0 then
      gypoz:=1
    else
      gypoz:=5;
//  if copy(Query1PO_DIGIT.Value, gypoz, 1) = '0' then
  if copy(digit, gypoz, 1) = '0' then
				Query1PO_GYUJTAS.Value	:= 'GYUJT�S BE'
  else
				Query1PO_GYUJTAS.Value	:= 'GYUJT�S KI';
  if Query1PO_VALID.Value=0 then
				Query1PO_GYUJTAS.Value	:= 'HIB�S ADAT';

end;

procedure TFTerkep.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Panel3.Color:=clBtnFace;
  Panel4.Color:=clBtnFace;
  Timer1.Enabled:=False;
  Timer2.Enabled:=False;
  CheckBox1.Checked:=False;
  enx:=0;
  eny:=0;
  pontdb:=0;
  MyExecuteJavaScript('ClearMarkers()');
end;

procedure TFTerkep.CheckBox1Click(Sender: TObject);
begin
  Timer1.Enabled:=CheckBox1.Checked;
  Timer2.Enabled:=CheckBox1.Checked;
  Label4.Visible:=CheckBox1.Checked;
  Chromium1.SetFocus;
end;

procedure TFTerkep.Timer1Timer(Sender: TObject);
begin
  mp:=0;
  Button1.OnClick(self);
end;

{
procedure TFTerkep.WebBrowser1DocumentComplete(Sender: TObject;
  const pDisp: IDispatch; var URL: OleVariant);
begin
  while WebBrowser1.ReadyState <> READYSTATE_COMPLETE do
    Application.ProcessMessages;
  // This is what turns the scrollbars off!
  WebBrowser1.OleObject.document.body.style.overflowX := 'hidden';
  WebBrowser1.OleObject.document.body.style.overflowY := 'hidden';
  WebBrowser1.OleObject.document.body.style.borderstyle := '';
//  WebBrowser1.OleObject.document.border.size := 0;
end;}

procedure TFTerkep.Timer2Timer(Sender: TObject);
begin
   Label4.Caption:=FloatToStr(Timer1.Interval/1000-mp);
   inc(mp);
end;

procedure TFTerkep.CheckBoxTrafficClick(Sender: TObject);
begin
    if CheckBoxTraffic.Checked then
     MyExecuteJavaScript('TrafficOn()')
    else
     MyExecuteJavaScript('TrafficOff()');
  Chromium1.SetFocus;
end;

procedure TFTerkep.Chromium1ConsoleMessage(Sender: TObject;
  const browser: ICefBrowser; const message, source: ustring; line: Integer;
  out Result: Boolean);
begin
   pnlStatus.Caption:=message;
   pnlStatus.Refresh;
   Result:= True;
end;

procedure TFTerkep.DBEdit1Enter(Sender: TObject);
begin
  Chromium1.SetFocus;
end;

procedure TFTerkep.UtvonalMutat(link: string);
begin
  if EgyebDlg.UTVONAL_KULSO_BONGESZOVEL then begin
     ShellExecute(0, nil, PChar(String(link)), nil, nil, SW_SHOWNORMAL);
     end
  else begin  // --- bels� b�ng�sz�vel
    // FTerkep2.Webbrowser1.Navigate(link);
    // FTerkep2.Chromium1.Browser.MainFrame.LoadString(link, 'source://html');
    // FTerkep2.Chromium1.Browser.MainFrame.LoadString('', link);
    FTerkep2.Chromium1.Browser.MainFrame.LoadURL(link);
    FTerkep2.ShowModal;
    end;
end;

procedure TFTerkep.Button3Click(Sender: TObject);
var
   link:string;
begin
  //  link:='http://maps.google.com/maps?saddr='+sx+','+sy+'+to:'+Edit1.Text+'&via=1&t=m';
  // link:='http://maps.google.com/maps?saddr='+Edit2.Text+'+to:'+Edit1.Text+'&via=1&t=m';
  FTerkep2.Label6.Caption:=Button3.Caption;
  link:= GetRouteLink(Edit2.Text, Edit1.Text);

  UtvonalMutat(link);
  Application.ProcessMessages;

end;

procedure TFTerkep.Button4Click(Sender: TObject);
var
   link:string;
begin
  FTerkep2.Label6.Caption:=Button4.Caption;
  link:= GetRouteLink(sx+','+sy, Edit2.Text);
  // link:='http://maps.google.com/maps?saddr='+sx+','+sy+'+to:'+Edit2.Text+'&via=1&t=m';
  //  link:='http://maps.google.com/maps?saddr='+Edit2.Text+'+to:'+Edit1.Text+'&via=1&t=m';
  UtvonalMutat(link);
  // FTerkep2.Webbrowser1.Navigate(link);
  // FTerkep2.ShowModal;
  Application.ProcessMessages;

end;

procedure TFTerkep.Button5Click(Sender: TObject);
var
   link:string;
begin
  FTerkep2.Label6.Caption:=Button5.Caption;
  link:= GetRouteLink(sx+','+sy, Edit1.Text);
  // link:='http://maps.google.com/maps?saddr='+sx+','+sy+'+to:'+Edit1.Text+'&via=1&t=m';
  //  link:='http://maps.google.com/maps?saddr='+Edit2.Text+'+to:'+Edit1.Text+'&via=1&t=m';
  UtvonalMutat(link);
  // FTerkep2.Webbrowser1.Navigate(link);
  // FTerkep2.ShowModal;
  Application.ProcessMessages;
end;

function TFTerkep.GetRouteLink(Start, Destination: string): string;
var
  S: string;
begin
  S:= 'http://maps.google.com/maps?saddr='+StringEncodeAsUTF8(Start)+'&daddr='+StringEncodeAsUTF8(Destination)+'&t=m';
  if Trim(EgyebDlg.MATRIXAPIKEY)<>'' then begin
     S:=S+'&key='+EgyebDlg.MATRIXAPIKEY;  // az .INI-b�l, API key for jsspeedmatrix@gmail.com
     end;
  Result:= S;
end;

procedure TFTerkep.Button6Click(Sender: TObject);
var
   address,sdat    : string;
begin
   if Edit2.Text='' then exit;
   CheckBox1.Checked:=False;
   address := Edit2.Text;
   sdat:='"'+'Felrak�: '+Edit2.Text+'"';
   MyExecuteJavaScript(Format('codeAddress(%s,%s)',[QuotedStr(address),sdat]));
end;

procedure TFTerkep.Button7Click(Sender: TObject);
var
   address ,sdat   : string;
begin
   if Edit1.Text='' then exit;
   CheckBox1.Checked:=False;
   address := Edit1.Text;
   sdat:='"'+'Lerak�: '+Edit1.Text+'"';
   Panel3.Color:=clBtnFace;
   MyExecuteJavaScript(Format('codeAddress(%s,%s)',[QuotedStr(address),sdat]));
end;

procedure TFTerkep.CheckBox2Click(Sender: TObject);
begin
    if CheckBox2.Checked then
     MyExecuteJavaScript('TrafficOn()')
    else
     MyExecuteJavaScript('TrafficOff()');
  Chromium1.SetFocus;
end;

procedure TFTerkep.Panel3Click(Sender: TObject);
var
   address,sdat    : string;
begin
   if Edit2.Text='' then exit;
   CheckBox1.Checked:=False;
   address := Edit2.Text;
   sdat:='"'+'Felrak�: '+Edit2.Text+'"';
   enx:=0; eny:=0; pontdb:=0;
   Panel3.Color:=clLime;
   Panel4.Color:=clBtnFace;
   MyExecuteJavaScript('ClearMarkers()');
   MyExecuteJavaScript(Format('codeAddress(%s,%s)',[QuotedStr(address),sdat]));

end;

procedure TFTerkep.Panel4Click(Sender: TObject);
var
   address ,sdat   : string;
begin
   if Edit1.Text='' then exit;
   CheckBox1.Checked:=False;
   address := Edit1.Text;
   sdat:='"'+'Lerak�: '+Edit1.Text+'"';
   enx:=0; eny:=0; pontdb:=0;
   Panel3.Color:=clBtnFace;
   Panel4.Color:=clLime;
   MyExecuteJavaScript('ClearMarkers()');
   MyExecuteJavaScript(Format('codeAddress(%s,%s)',[QuotedStr(address),sdat]));

end;

procedure TFTerkep.FormShow(Sender: TObject);
begin
   if not done then begin
      // I want to have the application show its window, then start loading the webpage.
      PostMessage(Handle, WM_ONSHOWCALLBACK, 0, 0);

      done := true;
   end;
   while Chromium1.Browser.IsLoading do Application.ProcessMessages;

end;

end.
