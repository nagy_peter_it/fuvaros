unit CsatListFm;

interface

uses
	SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
	Forms, Dialogs, StdCtrls, Mask, Buttons, Egyeb, DB, DBTables, J_SQL,
  Grids, DBGrids, ADODB, ExtCtrls;

type
	TCsatListFmDlg = class(TForm)
	 DBGrid1: TDBGrid;
	 DataSource1: TDataSource;
    Panel1: TPanel;
    BitKilep: TBitBtn;
    Query1: TADOQuery;
    Query1VI_JAKOD: TStringField;
    Query1VI_VIKOD: TStringField;
    Query1VI_HONNAN: TStringField;
    Query1VI_HOVA: TStringField;
    Query1VI_VEKOD: TStringField;
    Query1VI_VENEV: TStringField;
    Query1VI_SAKOD: TStringField;
    Query1VI_UJKOD: TIntegerField;
    Query1VI_VALNEM: TStringField;
    Query1VI_ARFOLY: TBCDField;
    Query1VI_FDEXP: TBCDField;
    Query1VI_FDIMP: TBCDField;
    Query1VI_FDKOV: TBCDField;
	 Query1VI_BEDAT: TStringField;
	 Query1VI_KIDAT: TStringField;
	 Query1VI_MEGJ: TStringField;
	 Query1VI_KULF: TIntegerField;
	 Query1VI_TIPUS: TBCDField;
    Query1MC_DATUM: TStringField;
    Query1MC_IDOPT: TStringField;
    Query1JA_RENDSZ: TStringField;
    Query1JA_ALKOD: TBCDField;
    Query1JA_ORSZ: TStringField;
	procedure Tolt(cim : string; terko : string);
	procedure FormCreate(Sender: TObject);
	procedure BitKilepClick(Sender: TObject);
	private
	public
	end;

var
	CsatListFmDlg: TCsatListFmDlg;

implementation

{$R *.DFM}

procedure TCsatListFmDlg.Tolt(cim : string; terko : string);
begin
	Caption := cim;
	Query_Run (Query1, 'SELECT * FROM MBCSATOL, VISZONY, JARAT WHERE MC_VIKOD = VI_VIKOD AND JA_KOD = VI_JAKOD AND MC_MBKOD = '+terko+
		' ORDER BY MC_DATUM, MC_IDOPT ',true);
end;

procedure TCsatListFmDlg.FormCreate(Sender: TObject);
begin
	EgyebDlg.SetADOQueryDatabase(Query1);
end;

procedure TCsatListFmDlg.BitKilepClick(Sender: TObject);
begin
	Close;
end;

end.
