// ************************************************************************ //
// The types declared in this file were generated from data read from the
// WSDL File described below:
// WSDL     : P:\SRC\FUVAR_ADO\4.20\WSDL\Fleetboard_PerformanceAnalysisService.wsdl
//  >Import : P:\SRC\FUVAR_ADO\4.20\WSDL\Fleetboard_PerformanceAnalysisService.wsdl>0
// Encoding : UTF-8
// Version  : 1.0
// (2017.09.29. 15:03:39 - - $Rev: 56641 $)
// ************************************************************************ //

unit Fleetboard_PerformanceAnalysisService;

interface

uses InvokeRegistry, SOAPHTTPClient, Types, XSBuiltIns;

const
  IS_OPTN = $0001;
  IS_UNBD = $0002;
  IS_NLBL = $0004;
  IS_ATTR = $0010;
  IS_REF  = $0080;


type

  // ************************************************************************ //
  // The following types, referred to in the WSDL document are not being represented
  // in this file. They are either aliases[@] of other types represented or were referred
  // to but never[!] declared in the document. The types from the latter category
  // typically map to predefined/known XML or Embarcadero types; however, they could also 
  // indicate incorrect WSDL documents that failed to declare or import a schema type.
  // ************************************************************************ //
  // !:boolean         - "http://www.w3.org/2001/XMLSchema"[Gbl]
  // !:integer         - "http://www.w3.org/2001/XMLSchema"[Gbl]
  // !:long            - "http://www.w3.org/2001/XMLSchema"[Gbl]
  // !:int             - "http://www.w3.org/2001/XMLSchema"[Gbl]
  // !:short           - "http://www.w3.org/2001/XMLSchema"[Gbl]
  // !:double          - "http://www.w3.org/2001/XMLSchema"[Gbl]
  // !:string          - "http://www.w3.org/2001/XMLSchema"[Gbl]

  ToursSummary         = class;                 { "http://www.fleetboard.com/data"[Cplx] }
  ToursSummary2        = class;                 { "http://www.fleetboard.com/data"[Cplx] }
  kickDown             = class;                 { "http://www.fleetboard.com/data"[GblCplx] }
  ecoRoll              = class;                 { "http://www.fleetboard.com/data"[GblCplx] }
  cruiseControlModes   = class;                 { "http://www.fleetboard.com/data"[GblCplx] }
  assistanceSystems    = class;                 { "http://www.fleetboard.com/data"[GblCplx] }
  drivingPrograms      = class;                 { "http://www.fleetboard.com/data"[GblCplx] }
  PowerTakeOff         = class;                 { "http://www.fleetboard.com/data"[Cplx] }
  PowerTakeOff2        = class;                 { "http://www.fleetboard.com/data"[Cplx] }
  cruiseCtrlOff        = class;                 { "http://www.fleetboard.com/data"[GblCplx] }
  cruiseCtrlWithoutPPC = class;                 { "http://www.fleetboard.com/data"[GblCplx] }
  cruiseCtrlWithPPC    = class;                 { "http://www.fleetboard.com/data"[GblCplx] }
  VehicleStateTimeValuesType = class;           { "http://www.fleetboard.com/data"[GblCplx] }
  limiter              = class;                 { "http://www.fleetboard.com/data"[GblCplx] }
  Cells                = class;                 { "http://www.fleetboard.com/data"[Cplx] }
  MinMaxWeightType     = class;                 { "http://www.fleetboard.com/data"[GblCplx] }
  ConsumptionValuesType = class;                { "http://www.fleetboard.com/data"[GblCplx] }
  Consumption          = class;                 { "http://www.fleetboard.com/data"[Cplx] }
  Consumption2         = class;                 { "http://www.fleetboard.com/data"[Cplx] }
  Consumption3         = class;                 { "http://www.fleetboard.com/data"[Cplx] }
  Consumption4         = class;                 { "http://www.fleetboard.com/data"[Cplx] }
  Consumption5         = class;                 { "http://www.fleetboard.com/data"[Cplx] }
  ppc                  = class;                 { "http://www.fleetboard.com/data"[GblCplx] }
  AvgValues            = class;                 { "http://www.fleetboard.com/data"[Cplx] }
  AvgValues2           = class;                 { "http://www.fleetboard.com/data"[Cplx] }
  AvgValues3           = class;                 { "http://www.fleetboard.com/data"[Cplx] }
  AvgValues4           = class;                 { "http://www.fleetboard.com/data"[Cplx] }
  AvgValues5           = class;                 { "http://www.fleetboard.com/data"[Cplx] }
  MinMaxSpeedType      = class;                 { "http://www.fleetboard.com/data"[GblCplx] }
  MinMaxConsumptionType = class;                { "http://www.fleetboard.com/data"[GblCplx] }
  Grades               = class;                 { "http://www.fleetboard.com/data"[Cplx] }
  Grades2              = class;                 { "http://www.fleetboard.com/data"[Cplx] }
  Grades3              = class;                 { "http://www.fleetboard.com/data"[Cplx] }
  Grades4              = class;                 { "http://www.fleetboard.com/data"[Cplx] }
  Grades5              = class;                 { "http://www.fleetboard.com/data"[Cplx] }
  Grades6              = class;                 { "http://www.fleetboard.com/data"[Cplx] }
  Grades7              = class;                 { "http://www.fleetboard.com/data"[Cplx] }
  MinMaxGradeType      = class;                 { "http://www.fleetboard.com/data"[GblCplx] }
  TimeRangeType        = class;                 { "http://www.fleetboard.com/data"[GblCplx] }
  Values               = class;                 { "http://www.fleetboard.com/data"[Cplx] }
  engineChartType      = class;                 { "http://www.fleetboard.com/data"[GblCplx] }
  VehicleGroup         = class;                 { "http://www.fleetboard.com/data"[GblCplx] }
  DriverGroup          = class;                 { "http://www.fleetboard.com/data"[GblCplx] }
  TPDate               = class;                 { "http://www.fleetboard.com/data"[GblCplx] }
  X                    = class;                 { "http://www.fleetboard.com/data"[Cplx] }
  Rows                 = class;                 { "http://www.fleetboard.com/data"[Cplx] }
  Y1                   = class;                 { "http://www.fleetboard.com/data"[Cplx] }
  Y2                   = class;                 { "http://www.fleetboard.com/data"[Cplx] }
  Period               = class;                 { "http://www.fleetboard.com/data"[Cplx] }
  Period2              = class;                 { "http://www.fleetboard.com/data"[Cplx] }
  Period3              = class;                 { "http://www.fleetboard.com/data"[Cplx] }
  Period4              = class;                 { "http://www.fleetboard.com/data"[Cplx] }
  Period5              = class;                 { "http://www.fleetboard.com/data"[Cplx] }
  Period6              = class;                 { "http://www.fleetboard.com/data"[Cplx] }
  Period7              = class;                 { "http://www.fleetboard.com/data"[Cplx] }
  PeriodBreakDown      = class;                 { "http://www.fleetboard.com/data"[Cplx] }
  PeriodBreakDown2     = class;                 { "http://www.fleetboard.com/data"[Cplx] }
  barChartType         = class;                 { "http://www.fleetboard.com/data"[GblCplx] }
  ChartsType           = class;                 { "http://www.fleetboard.com/data"[GblCplx] }
  Period3Type          = class;                 { "http://www.fleetboard.com/data"[GblCplx] }
  PredefinedPeriod     = class;                 { "http://www.fleetboard.com/data"[Cplx] }
  PredefinedPeriod2    = class;                 { "http://www.fleetboard.com/data"[Cplx] }
  PredefinedPeriod3    = class;                 { "http://www.fleetboard.com/data"[Cplx] }
  VehicleDataSet       = class;                 { "http://www.fleetboard.com/data"[Cplx] }
  VehicleDataSet2      = class;                 { "http://www.fleetboard.com/data"[Cplx] }
  PowerTakeOff3        = class;                 { "http://www.fleetboard.com/data"[Cplx] }
  Consumption6         = class;                 { "http://www.fleetboard.com/data"[Cplx] }
  PeriodBreakDown3     = class;                 { "http://www.fleetboard.com/data"[Cplx] }
  ToursSummary3        = class;                 { "http://www.fleetboard.com/data"[Cplx] }
  AvgValues6           = class;                 { "http://www.fleetboard.com/data"[Cplx] }
  PredefinedPeriod4    = class;                 { "http://www.fleetboard.com/data"[Cplx] }
  PeriodBreakDown4     = class;                 { "http://www.fleetboard.com/data"[Cplx] }
  Grades8              = class;                 { "http://www.fleetboard.com/data"[Cplx] }
  Period8              = class;                 { "http://www.fleetboard.com/data"[Cplx] }
  PredefinedPeriod5    = class;                 { "http://www.fleetboard.com/data"[Cplx] }
  UniversalPerformanceAnalysisVehicleOverviewReport = class;   { "http://www.fleetboard.com/data"[GblCplx] }
  PeriodBreakDown5     = class;                 { "http://www.fleetboard.com/data"[Cplx] }
  ToursSummary4        = class;                 { "http://www.fleetboard.com/data"[Cplx] }
  PredefinedPeriod6    = class;                 { "http://www.fleetboard.com/data"[Cplx] }
  GetUniversalPerformanceAnalysisVehicleOverviewRequestType = class;   { "http://www.fleetboard.com/data"[GblCplx] }
  GetUniversalPerformanceAnalysisVehicleOverviewRequest = class;   { "http://www.fleetboard.com/data"[GblElm] }
  PeriodBreakDown6     = class;                 { "http://www.fleetboard.com/data"[Cplx] }
  ToursSummary5        = class;                 { "http://www.fleetboard.com/data"[Cplx] }
  ToursSummary6        = class;                 { "http://www.fleetboard.com/data"[Cplx] }
  MBPerformanceAnalysisVehicleOverviewReport = class;   { "http://www.fleetboard.com/data"[Cplx] }
  GetUniversalPerformanceAnalysisDriverOverviewResponseType = class;   { "http://www.fleetboard.com/data"[GblCplx] }
  GetUniversalPerformanceAnalysisDriverOverviewResponse = class;   { "http://www.fleetboard.com/data"[GblElm] }
  GetUniversalPerformanceAnalysisVehicleOverviewResponseType = class;   { "http://www.fleetboard.com/data"[GblCplx] }
  GetUniversalPerformanceAnalysisVehicleOverviewResponse = class;   { "http://www.fleetboard.com/data"[GblElm] }
  GetMBPerformanceAnalysisVehicleResponseType = class;   { "http://www.fleetboard.com/data"[GblCplx] }
  GetMBPerformanceAnalysisVehicleResponse = class;   { "http://www.fleetboard.com/data"[GblElm] }
  GetUniversalPerformanceAnalysisVehicleResponseType = class;   { "http://www.fleetboard.com/data"[GblCplx] }
  GetUniversalPerformanceAnalysisVehicleResponse = class;   { "http://www.fleetboard.com/data"[GblElm] }
  GetMBPerformanceAnalysisDriverResponseType = class;   { "http://www.fleetboard.com/data"[GblCplx] }
  GetMBPerformanceAnalysisDriverResponse = class;   { "http://www.fleetboard.com/data"[GblElm] }
  GetUniversalPerformanceAnalysisDriverResponseType = class;   { "http://www.fleetboard.com/data"[GblCplx] }
  GetUniversalPerformanceAnalysisDriverResponse = class;   { "http://www.fleetboard.com/data"[GblElm] }
  AvgValues7           = class;                 { "http://www.fleetboard.com/data"[Cplx] }
  Grades9              = class;                 { "http://www.fleetboard.com/data"[Cplx] }
  Period9              = class;                 { "http://www.fleetboard.com/data"[Cplx] }
  GetMBPerformanceAnalysisVehicleOverviewResponseType = class;   { "http://www.fleetboard.com/data"[GblCplx] }
  GetMBPerformanceAnalysisVehicleOverviewResponse = class;   { "http://www.fleetboard.com/data"[GblElm] }
  Analysis             = class;                 { "http://www.fleetboard.com/data"[Cplx] }
  Analysis2            = class;                 { "http://www.fleetboard.com/data"[Cplx] }
  Analysis3            = class;                 { "http://www.fleetboard.com/data"[Cplx] }
  Analysis4            = class;                 { "http://www.fleetboard.com/data"[Cplx] }
  Analysis5            = class;                 { "http://www.fleetboard.com/data"[Cplx] }
  Analysis6            = class;                 { "http://www.fleetboard.com/data"[Cplx] }
  GetMBPerformanceAnalysisVehicleOverviewRequestType = class;   { "http://www.fleetboard.com/data"[GblCplx] }
  GetMBPerformanceAnalysisVehicleOverviewRequest = class;   { "http://www.fleetboard.com/data"[GblElm] }
  PredefinedPeriod7    = class;                 { "http://www.fleetboard.com/data"[Cplx] }
  PeriodBreakDown7     = class;                 { "http://www.fleetboard.com/data"[Cplx] }
  ToursSummary7        = class;                 { "http://www.fleetboard.com/data"[Cplx] }
  AvgValues8           = class;                 { "http://www.fleetboard.com/data"[Cplx] }
  Grades10             = class;                 { "http://www.fleetboard.com/data"[Cplx] }
  Period10             = class;                 { "http://www.fleetboard.com/data"[Cplx] }
  Analysis7            = class;                 { "http://www.fleetboard.com/data"[Cplx] }
  GetUniversalPerformanceAnalysisVehicleRequestType = class;   { "http://www.fleetboard.com/data"[GblCplx] }
  GetUniversalPerformanceAnalysisVehicleRequest = class;   { "http://www.fleetboard.com/data"[GblElm] }
  GetMBPerformanceAnalysisVehicleRequestType = class;   { "http://www.fleetboard.com/data"[GblCplx] }
  GetMBPerformanceAnalysisVehicleRequest = class;   { "http://www.fleetboard.com/data"[GblElm] }
  ToursSummary8        = class;                 { "http://www.fleetboard.com/data"[Cplx] }
  Analysis8            = class;                 { "http://www.fleetboard.com/data"[Cplx] }
  MBPerformanceAnalysisDriverOverviewReport = class;   { "http://www.fleetboard.com/data"[Cplx] }
  GetMBPerformanceAnalysisDriverOverviewRequestType = class;   { "http://www.fleetboard.com/data"[GblCplx] }
  GetMBPerformanceAnalysisDriverOverviewRequest = class;   { "http://www.fleetboard.com/data"[GblElm] }
  UniversalPerformanceAnalysisDriverOverviewReport = class;   { "http://www.fleetboard.com/data"[GblCplx] }
  GetMBPerformanceAnalysisDriverRequestType = class;   { "http://www.fleetboard.com/data"[GblCplx] }
  GetMBPerformanceAnalysisDriverRequest = class;   { "http://www.fleetboard.com/data"[GblElm] }
  GetUniversalPerformanceAnalysisDriverOverviewRequestType = class;   { "http://www.fleetboard.com/data"[GblCplx] }
  GetUniversalPerformanceAnalysisDriverOverviewRequest = class;   { "http://www.fleetboard.com/data"[GblElm] }
  MBPerformanceAnalysisVehicleReport = class;   { "http://www.fleetboard.com/data"[Cplx] }
  UniversalPerformanceAnalysisVehicleReport = class;   { "http://www.fleetboard.com/data"[Cplx] }
  GetUniversalPerformanceAnalysisDriverRequestType = class;   { "http://www.fleetboard.com/data"[GblCplx] }
  GetUniversalPerformanceAnalysisDriverRequest = class;   { "http://www.fleetboard.com/data"[GblElm] }
  MBPerformanceAnalysisDriverReport = class;    { "http://www.fleetboard.com/data"[Cplx] }
  UniversalPerformanceAnalysisDriverReport = class;   { "http://www.fleetboard.com/data"[Cplx] }
  GetMBPerformanceAnalysisDriverOverviewResponseType = class;   { "http://www.fleetboard.com/data"[GblCplx] }
  GetMBPerformanceAnalysisDriverOverviewResponse = class;   { "http://www.fleetboard.com/data"[GblElm] }
  PredefinedPeriod8    = class;                 { "http://www.fleetboard.com/data"[Cplx] }
  PeriodBreakDown8     = class;                 { "http://www.fleetboard.com/data"[Cplx] }

  {$SCOPEDENUMS ON}
  { "http://www.fleetboard.com/data"[Smpl] }
  ContainsVehicleDataSetList2 = (TRUE, FALSE);

  { "http://www.fleetboard.com/data"[Smpl] }
  Model = (ONLY_RECENT_MESSAGES, TODAY, THIS_WEEK, THIS_MONTH, YESTERDAY, LAST_WEEK, LAST_MONTH, PERIOD);

  { "http://www.fleetboard.com/data"[GblSmpl] }
  period1Type = (
      YEAR, 
      FIRST_HALF, 
      SECOND_HALF, 
      FIRST_QUARTER, 
      SECOND_QUARTER, 
      THIRD_QUARTER, 
      FOURTH_QUARTER, 
      JANUARY, 
      FEBRUARY, 
      MARCH, 
      APRIL, 
      MAY, 
      JUNE, 
      JULY, 
      AUGUST, 
      SEPTEMBER, 
      OCTOBER, 
      NOVEMBER, 
      DECEMBER
  );

  { "http://www.fleetboard.com/data"[GblSmpl] }
  period2Type = (ANNUAL, BIANNUAL, QUARTERLY, MONTHLY, WEEKLY);

  { "http://www.fleetboard.com/data"[GblSmpl] }
  calendarWeekType = (
      _1, 
      _2, 
      _3, 
      _4, 
      _5, 
      _6, 
      _7, 
      _8, 
      _9, 
      _10, 
      _11, 
      _12, 
      _13, 
      _14, 
      _15, 
      _16, 
      _17, 
      _18, 
      _19, 
      _20, 
      _21, 
      _22, 
      _23, 
      _24, 
      _25, 
      _26, 
      _27, 
      _28, 
      _29, 
      _30, 
      _31, 
      _32, 
      _33, 
      _34, 
      _35, 
      _36, 
      _37, 
      _38, 
      _39, 
      _40, 
      _41, 
      _42, 
      _43, 
      _44, 
      _45, 
      _46, 
      _47, 
      _48, 
      _49, 
      _50, 
      _51, 
      _52, 
      _53
  );

  { "http://www.fleetboard.com/data"[GblSmpl] }
  analysisModelType = (ToursSummary, PeriodBreakDown, PredefinedPeriods);

  { "http://www.fleetboard.com/data"[GblSmpl] }
  telematicGroupIDType = (MB, AM);

  {$SCOPEDENUMS OFF}



  // ************************************************************************ //
  // XML       : ToursSummary, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  ToursSummary = class(TRemotable)
  private
    FTimeRange: TimeRangeType;
  public
    destructor Destroy; override;
  published
    property TimeRange: TimeRangeType  read FTimeRange write FTimeRange;
  end;



  // ************************************************************************ //
  // XML       : ToursSummary, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  ToursSummary2 = class(TRemotable)
  private
    FTimeRange: TimeRangeType;
  public
    destructor Destroy; override;
  published
    property TimeRange: TimeRangeType  read FTimeRange write FTimeRange;
  end;

  Array_Of_MBPerformanceAnalysisVehicleOverviewReport = array of MBPerformanceAnalysisVehicleOverviewReport;   { "http://www.fleetboard.com/data"[Ubnd] }
  Array_Of_Cells = array of Cells;              { "http://www.fleetboard.com/data"[Ubnd] }
  Array_Of_MBPerformanceAnalysisDriverOverviewReport = array of MBPerformanceAnalysisDriverOverviewReport;   { "http://www.fleetboard.com/data"[Ubnd] }
  Array_Of_MBPerformanceAnalysisVehicleReport = array of MBPerformanceAnalysisVehicleReport;   { "http://www.fleetboard.com/data"[Ubnd] }
  Array_Of_barChartType = array of barChartType;   { "http://www.fleetboard.com/data"[GblUbnd] }
  Array_Of_engineChartType = array of engineChartType;   { "http://www.fleetboard.com/data"[GblUbnd] }
  Array_Of_Rows = array of Rows;                { "http://www.fleetboard.com/data"[Ubnd] }
  Array_Of_Values = array of Values;            { "http://www.fleetboard.com/data"[Ubnd] }
  Array_Of_VehicleDataSet = array of VehicleDataSet;   { "http://www.fleetboard.com/data"[Ubnd] }
  Array_Of_UniversalPerformanceAnalysisVehicleReport = array of UniversalPerformanceAnalysisVehicleReport;   { "http://www.fleetboard.com/data"[Ubnd] }
  Array_Of_VehicleDataSet2 = array of VehicleDataSet2;   { "http://www.fleetboard.com/data"[Ubnd] }
  Array_Of_UniversalPerformanceAnalysisDriverReport = array of UniversalPerformanceAnalysisDriverReport;   { "http://www.fleetboard.com/data"[Ubnd] }
  Array_Of_DriverGroup = array of DriverGroup;   { "http://www.fleetboard.com/data"[GblUbnd] }
  Array_Of_UniversalPerformanceAnalysisDriverOverviewReport = array of UniversalPerformanceAnalysisDriverOverviewReport;   { "http://www.fleetboard.com/data"[GblUbnd] }
  Array_Of_UniversalPerformanceAnalysisVehicleOverviewReport = array of UniversalPerformanceAnalysisVehicleOverviewReport;   { "http://www.fleetboard.com/data"[GblUbnd] }
  Array_Of_MBPerformanceAnalysisDriverReport = array of MBPerformanceAnalysisDriverReport;   { "http://www.fleetboard.com/data"[Ubnd] }
  Array_Of_VehicleGroup = array of VehicleGroup;   { "http://www.fleetboard.com/data"[GblUbnd] }
  ContainsVehicleDataSetList =  type ContainsVehicleDataSetList2;      { "http://www.fleetboard.com/data"[GblElm] }
  distanceType    =  type Int64;      { "http://www.fleetboard.com/data"[GblSmpl] }


  // ************************************************************************ //
  // XML       : kickDown, global, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  kickDown = class(TRemotable)
  private
    FKickDownUse: Int64;
  published
    property KickDownUse: Int64  read FKickDownUse write FKickDownUse;
  end;



  // ************************************************************************ //
  // XML       : ecoRoll, global, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  ecoRoll = class(TRemotable)
  private
    FEcoRollOn: distanceType;
    FEcoRollActive: distanceType;
  published
    property EcoRollOn:     distanceType  read FEcoRollOn write FEcoRollOn;
    property EcoRollActive: distanceType  read FEcoRollActive write FEcoRollActive;
  end;



  // ************************************************************************ //
  // XML       : cruiseControlModes, global, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  cruiseControlModes = class(TRemotable)
  private
    FCruiseCtrlWithPPC: cruiseCtrlWithPPC;
    FCruiseCtrlWithPPC_Specified: boolean;
    FCruiseCtrlWithoutPPC: cruiseCtrlWithoutPPC;
    FCruiseCtrlWithoutPPC_Specified: boolean;
    FCruiseCtrlOff: cruiseCtrlOff;
    FCruiseCtrlOff_Specified: boolean;
    FPPC: ppc;
    FPPC_Specified: boolean;
    FLimiter: limiter;
    FLimiter_Specified: boolean;
    procedure SetCruiseCtrlWithPPC(Index: Integer; const AcruiseCtrlWithPPC: cruiseCtrlWithPPC);
    function  CruiseCtrlWithPPC_Specified(Index: Integer): boolean;
    procedure SetCruiseCtrlWithoutPPC(Index: Integer; const AcruiseCtrlWithoutPPC: cruiseCtrlWithoutPPC);
    function  CruiseCtrlWithoutPPC_Specified(Index: Integer): boolean;
    procedure SetCruiseCtrlOff(Index: Integer; const AcruiseCtrlOff: cruiseCtrlOff);
    function  CruiseCtrlOff_Specified(Index: Integer): boolean;
    procedure SetPPC(Index: Integer; const Appc: ppc);
    function  PPC_Specified(Index: Integer): boolean;
    procedure SetLimiter(Index: Integer; const Alimiter: limiter);
    function  Limiter_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property CruiseCtrlWithPPC:    cruiseCtrlWithPPC     Index (IS_OPTN) read FCruiseCtrlWithPPC write SetCruiseCtrlWithPPC stored CruiseCtrlWithPPC_Specified;
    property CruiseCtrlWithoutPPC: cruiseCtrlWithoutPPC  Index (IS_OPTN) read FCruiseCtrlWithoutPPC write SetCruiseCtrlWithoutPPC stored CruiseCtrlWithoutPPC_Specified;
    property CruiseCtrlOff:        cruiseCtrlOff         Index (IS_OPTN) read FCruiseCtrlOff write SetCruiseCtrlOff stored CruiseCtrlOff_Specified;
    property PPC:                  ppc                   Index (IS_OPTN) read FPPC write SetPPC stored PPC_Specified;
    property Limiter:              limiter               Index (IS_OPTN) read FLimiter write SetLimiter stored Limiter_Specified;
  end;



  // ************************************************************************ //
  // XML       : assistanceSystems, global, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  assistanceSystems = class(TRemotable)
  private
    FKickDown: kickDown;
    FKickDown_Specified: boolean;
    FEcoRoll: ecoRoll;
    FEcoRoll_Specified: boolean;
    FDrivingPrograms: drivingPrograms;
    FDrivingPrograms_Specified: boolean;
    FCruiseControlModes: cruiseControlModes;
    FCruiseControlModes_Specified: boolean;
    procedure SetKickDown(Index: Integer; const AkickDown: kickDown);
    function  KickDown_Specified(Index: Integer): boolean;
    procedure SetEcoRoll(Index: Integer; const AecoRoll: ecoRoll);
    function  EcoRoll_Specified(Index: Integer): boolean;
    procedure SetDrivingPrograms(Index: Integer; const AdrivingPrograms: drivingPrograms);
    function  DrivingPrograms_Specified(Index: Integer): boolean;
    procedure SetCruiseControlModes(Index: Integer; const AcruiseControlModes: cruiseControlModes);
    function  CruiseControlModes_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property KickDown:           kickDown            Index (IS_OPTN) read FKickDown write SetKickDown stored KickDown_Specified;
    property EcoRoll:            ecoRoll             Index (IS_OPTN) read FEcoRoll write SetEcoRoll stored EcoRoll_Specified;
    property DrivingPrograms:    drivingPrograms     Index (IS_OPTN) read FDrivingPrograms write SetDrivingPrograms stored DrivingPrograms_Specified;
    property CruiseControlModes: cruiseControlModes  Index (IS_OPTN) read FCruiseControlModes write SetCruiseControlModes stored CruiseControlModes_Specified;
  end;



  // ************************************************************************ //
  // XML       : drivingPrograms, global, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  drivingPrograms = class(TRemotable)
  private
    FAutomaticDrivingProgramId: SmallInt;
    FStandardDrivingProgram: distanceType;
    FAutomaticDrivingProgram: distanceType;
    FManualShiftingMode: distanceType;
  published
    property AutomaticDrivingProgramId: SmallInt      read FAutomaticDrivingProgramId write FAutomaticDrivingProgramId;
    property StandardDrivingProgram:    distanceType  read FStandardDrivingProgram write FStandardDrivingProgram;
    property AutomaticDrivingProgram:   distanceType  read FAutomaticDrivingProgram write FAutomaticDrivingProgram;
    property ManualShiftingMode:        distanceType  read FManualShiftingMode write FManualShiftingMode;
  end;

  versionType     =  type Int64;      { "http://www.fleetboard.com/data"[GblSmpl] }
  offsetType      =  type Int64;      { "http://www.fleetboard.com/data"[GblSmpl] }
  limitType       =  type Int64;      { "http://www.fleetboard.com/data"[GblSmpl] }
  timeType        =  type Int64;      { "http://www.fleetboard.com/data"[GblSmpl] }


  // ************************************************************************ //
  // XML       : PowerTakeOff, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  PowerTakeOff = class(TRemotable)
  private
    FPto1: timeType;
    FPto2: timeType;
    FPto3: timeType;
  published
    property Pto1: timeType  read FPto1 write FPto1;
    property Pto2: timeType  read FPto2 write FPto2;
    property Pto3: timeType  read FPto3 write FPto3;
  end;



  // ************************************************************************ //
  // XML       : PowerTakeOff, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  PowerTakeOff2 = class(TRemotable)
  private
    FPto1: timeType;
    FPto1_Specified: boolean;
    FPto2: timeType;
    FPto2_Specified: boolean;
    FPto3: timeType;
    FPto3_Specified: boolean;
    procedure SetPto1(Index: Integer; const AtimeType: timeType);
    function  Pto1_Specified(Index: Integer): boolean;
    procedure SetPto2(Index: Integer; const AtimeType: timeType);
    function  Pto2_Specified(Index: Integer): boolean;
    procedure SetPto3(Index: Integer; const AtimeType: timeType);
    function  Pto3_Specified(Index: Integer): boolean;
  published
    property Pto1: timeType  Index (IS_OPTN) read FPto1 write SetPto1 stored Pto1_Specified;
    property Pto2: timeType  Index (IS_OPTN) read FPto2 write SetPto2 stored Pto2_Specified;
    property Pto3: timeType  Index (IS_OPTN) read FPto3 write SetPto3 stored Pto3_Specified;
  end;



  // ************************************************************************ //
  // XML       : cruiseCtrlOff, global, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  cruiseCtrlOff = class(TRemotable)
  private
    FDistance: distanceType;
  published
    property Distance: distanceType  read FDistance write FDistance;
  end;



  // ************************************************************************ //
  // XML       : cruiseCtrlWithoutPPC, global, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  cruiseCtrlWithoutPPC = class(TRemotable)
  private
    FDistance: distanceType;
  published
    property Distance: distanceType  read FDistance write FDistance;
  end;



  // ************************************************************************ //
  // XML       : cruiseCtrlWithPPC, global, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  cruiseCtrlWithPPC = class(TRemotable)
  private
    FDistance: distanceType;
  published
    property Distance: distanceType  read FDistance write FDistance;
  end;



  // ************************************************************************ //
  // XML       : VehicleStateTimeValuesType, global, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  VehicleStateTimeValuesType = class(TRemotable)
  private
    FMovingTime: timeType;
    FMovingTime_Specified: boolean;
    FIdleTimeEngineOn: timeType;
    FIdleTimeEngineOn_Specified: boolean;
    procedure SetMovingTime(Index: Integer; const AtimeType: timeType);
    function  MovingTime_Specified(Index: Integer): boolean;
    procedure SetIdleTimeEngineOn(Index: Integer; const AtimeType: timeType);
    function  IdleTimeEngineOn_Specified(Index: Integer): boolean;
  published
    property MovingTime:       timeType  Index (IS_OPTN) read FMovingTime write SetMovingTime stored MovingTime_Specified;
    property IdleTimeEngineOn: timeType  Index (IS_OPTN) read FIdleTimeEngineOn write SetIdleTimeEngineOn stored IdleTimeEngineOn_Specified;
  end;



  // ************************************************************************ //
  // XML       : limiter, global, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  limiter = class(TRemotable)
  private
    FDistance: distanceType;
  published
    property Distance: distanceType  read FDistance write FDistance;
  end;



  // ************************************************************************ //
  // XML       : Cells, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  Cells = class(TRemotable)
  private
    Fvalue: Double;
    FecoRating: Integer;
    FnumberOfRevolutions: Integer;
    FnumberOfRevolutions_Specified: boolean;
    procedure SetnumberOfRevolutions(Index: Integer; const AInteger: Integer);
    function  numberOfRevolutions_Specified(Index: Integer): boolean;
  published
    property value:               Double   Index (IS_ATTR) read Fvalue write Fvalue;
    property ecoRating:           Integer  Index (IS_ATTR) read FecoRating write FecoRating;
    property numberOfRevolutions: Integer  Index (IS_ATTR or IS_OPTN) read FnumberOfRevolutions write SetnumberOfRevolutions stored numberOfRevolutions_Specified;
  end;

  ratioType       =  type Double;      { "http://www.fleetboard.com/data"[GblSmpl] }
  weightType      =  type Int64;      { "http://www.fleetboard.com/data"[GblSmpl] }


  // ************************************************************************ //
  // XML       : MinMaxWeightType, global, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  MinMaxWeightType = class(TRemotable)
  private
    FMin: weightType;
    FMax: weightType;
  published
    property Min: weightType  read FMin write FMin;
    property Max: weightType  read FMax write FMax;
  end;

  consumptionType =  type Double;      { "http://www.fleetboard.com/data"[GblSmpl] }


  // ************************************************************************ //
  // XML       : ConsumptionValuesType, global, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  ConsumptionValuesType = class(TRemotable)
  private
    FDriveConsumption: consumptionType;
    FDriveConsumption_Specified: boolean;
    FTotalConsumption: consumptionType;
    FTotalConsumption_Specified: boolean;
    FIdleConsumption: consumptionType;
    FIdleConsumption_Specified: boolean;
    FAdBlueConsumption: consumptionType;
    FAdBlueConsumption_Specified: boolean;
    FIdleConsumptionPTOActive: consumptionType;
    FIdleConsumptionPTOActive_Specified: boolean;
    FIdleConsumptionPTOInactive: consumptionType;
    FIdleConsumptionPTOInactive_Specified: boolean;
    procedure SetDriveConsumption(Index: Integer; const AconsumptionType: consumptionType);
    function  DriveConsumption_Specified(Index: Integer): boolean;
    procedure SetTotalConsumption(Index: Integer; const AconsumptionType: consumptionType);
    function  TotalConsumption_Specified(Index: Integer): boolean;
    procedure SetIdleConsumption(Index: Integer; const AconsumptionType: consumptionType);
    function  IdleConsumption_Specified(Index: Integer): boolean;
    procedure SetAdBlueConsumption(Index: Integer; const AconsumptionType: consumptionType);
    function  AdBlueConsumption_Specified(Index: Integer): boolean;
    procedure SetIdleConsumptionPTOActive(Index: Integer; const AconsumptionType: consumptionType);
    function  IdleConsumptionPTOActive_Specified(Index: Integer): boolean;
    procedure SetIdleConsumptionPTOInactive(Index: Integer; const AconsumptionType: consumptionType);
    function  IdleConsumptionPTOInactive_Specified(Index: Integer): boolean;
  published
    property DriveConsumption:           consumptionType  Index (IS_OPTN) read FDriveConsumption write SetDriveConsumption stored DriveConsumption_Specified;
    property TotalConsumption:           consumptionType  Index (IS_OPTN) read FTotalConsumption write SetTotalConsumption stored TotalConsumption_Specified;
    property IdleConsumption:            consumptionType  Index (IS_OPTN) read FIdleConsumption write SetIdleConsumption stored IdleConsumption_Specified;
    property AdBlueConsumption:          consumptionType  Index (IS_OPTN) read FAdBlueConsumption write SetAdBlueConsumption stored AdBlueConsumption_Specified;
    property IdleConsumptionPTOActive:   consumptionType  Index (IS_OPTN) read FIdleConsumptionPTOActive write SetIdleConsumptionPTOActive stored IdleConsumptionPTOActive_Specified;
    property IdleConsumptionPTOInactive: consumptionType  Index (IS_OPTN) read FIdleConsumptionPTOInactive write SetIdleConsumptionPTOInactive stored IdleConsumptionPTOInactive_Specified;
  end;



  // ************************************************************************ //
  // XML       : Consumption, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  Consumption = class(TRemotable)
  private
    FDriveConsumption: consumptionType;
    FDriveConsumption_Specified: boolean;
    FTotalConsumption: consumptionType;
    FTotalConsumption_Specified: boolean;
    FStandConsumption: consumptionType;
    FStandConsumption_Specified: boolean;
    FPTOConsumption: consumptionType;
    FPTOConsumption_Specified: boolean;
    procedure SetDriveConsumption(Index: Integer; const AconsumptionType: consumptionType);
    function  DriveConsumption_Specified(Index: Integer): boolean;
    procedure SetTotalConsumption(Index: Integer; const AconsumptionType: consumptionType);
    function  TotalConsumption_Specified(Index: Integer): boolean;
    procedure SetStandConsumption(Index: Integer; const AconsumptionType: consumptionType);
    function  StandConsumption_Specified(Index: Integer): boolean;
    procedure SetPTOConsumption(Index: Integer; const AconsumptionType: consumptionType);
    function  PTOConsumption_Specified(Index: Integer): boolean;
  published
    property DriveConsumption: consumptionType  Index (IS_OPTN) read FDriveConsumption write SetDriveConsumption stored DriveConsumption_Specified;
    property TotalConsumption: consumptionType  Index (IS_OPTN) read FTotalConsumption write SetTotalConsumption stored TotalConsumption_Specified;
    property StandConsumption: consumptionType  Index (IS_OPTN) read FStandConsumption write SetStandConsumption stored StandConsumption_Specified;
    property PTOConsumption:   consumptionType  Index (IS_OPTN) read FPTOConsumption write SetPTOConsumption stored PTOConsumption_Specified;
  end;



  // ************************************************************************ //
  // XML       : Consumption, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  Consumption2 = class(TRemotable)
  private
    FDriveConsumption: consumptionType;
    FDriveConsumption_Specified: boolean;
    FTotalConsumption: consumptionType;
    FTotalConsumption_Specified: boolean;
    FIdleConsumption: consumptionType;
    FIdleConsumption_Specified: boolean;
    FAdBlueConsumption: consumptionType;
    FAdBlueConsumption_Specified: boolean;
    FIdleConsumptionPTOActive: consumptionType;
    FIdleConsumptionPTOActive_Specified: boolean;
    FIdleConsumptionPTOInactive: consumptionType;
    FIdleConsumptionPTOInactive_Specified: boolean;
    procedure SetDriveConsumption(Index: Integer; const AconsumptionType: consumptionType);
    function  DriveConsumption_Specified(Index: Integer): boolean;
    procedure SetTotalConsumption(Index: Integer; const AconsumptionType: consumptionType);
    function  TotalConsumption_Specified(Index: Integer): boolean;
    procedure SetIdleConsumption(Index: Integer; const AconsumptionType: consumptionType);
    function  IdleConsumption_Specified(Index: Integer): boolean;
    procedure SetAdBlueConsumption(Index: Integer; const AconsumptionType: consumptionType);
    function  AdBlueConsumption_Specified(Index: Integer): boolean;
    procedure SetIdleConsumptionPTOActive(Index: Integer; const AconsumptionType: consumptionType);
    function  IdleConsumptionPTOActive_Specified(Index: Integer): boolean;
    procedure SetIdleConsumptionPTOInactive(Index: Integer; const AconsumptionType: consumptionType);
    function  IdleConsumptionPTOInactive_Specified(Index: Integer): boolean;
  published
    property DriveConsumption:           consumptionType  Index (IS_OPTN) read FDriveConsumption write SetDriveConsumption stored DriveConsumption_Specified;
    property TotalConsumption:           consumptionType  Index (IS_OPTN) read FTotalConsumption write SetTotalConsumption stored TotalConsumption_Specified;
    property IdleConsumption:            consumptionType  Index (IS_OPTN) read FIdleConsumption write SetIdleConsumption stored IdleConsumption_Specified;
    property AdBlueConsumption:          consumptionType  Index (IS_OPTN) read FAdBlueConsumption write SetAdBlueConsumption stored AdBlueConsumption_Specified;
    property IdleConsumptionPTOActive:   consumptionType  Index (IS_OPTN) read FIdleConsumptionPTOActive write SetIdleConsumptionPTOActive stored IdleConsumptionPTOActive_Specified;
    property IdleConsumptionPTOInactive: consumptionType  Index (IS_OPTN) read FIdleConsumptionPTOInactive write SetIdleConsumptionPTOInactive stored IdleConsumptionPTOInactive_Specified;
  end;



  // ************************************************************************ //
  // XML       : Consumption, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  Consumption3 = class(TRemotable)
  private
    FDriveConsumption: consumptionType;
    FDriveConsumption_Specified: boolean;
    FTotalConsumption: consumptionType;
    FTotalConsumption_Specified: boolean;
    FIdleConsumption: consumptionType;
    FIdleConsumption_Specified: boolean;
    FAdBlueConsumption: consumptionType;
    FAdBlueConsumption_Specified: boolean;
    FIdleConsumptionPTOActive: consumptionType;
    FIdleConsumptionPTOActive_Specified: boolean;
    FIdleConsumptionPTOInactive: consumptionType;
    FIdleConsumptionPTOInactive_Specified: boolean;
    procedure SetDriveConsumption(Index: Integer; const AconsumptionType: consumptionType);
    function  DriveConsumption_Specified(Index: Integer): boolean;
    procedure SetTotalConsumption(Index: Integer; const AconsumptionType: consumptionType);
    function  TotalConsumption_Specified(Index: Integer): boolean;
    procedure SetIdleConsumption(Index: Integer; const AconsumptionType: consumptionType);
    function  IdleConsumption_Specified(Index: Integer): boolean;
    procedure SetAdBlueConsumption(Index: Integer; const AconsumptionType: consumptionType);
    function  AdBlueConsumption_Specified(Index: Integer): boolean;
    procedure SetIdleConsumptionPTOActive(Index: Integer; const AconsumptionType: consumptionType);
    function  IdleConsumptionPTOActive_Specified(Index: Integer): boolean;
    procedure SetIdleConsumptionPTOInactive(Index: Integer; const AconsumptionType: consumptionType);
    function  IdleConsumptionPTOInactive_Specified(Index: Integer): boolean;
  published
    property DriveConsumption:           consumptionType  Index (IS_OPTN) read FDriveConsumption write SetDriveConsumption stored DriveConsumption_Specified;
    property TotalConsumption:           consumptionType  Index (IS_OPTN) read FTotalConsumption write SetTotalConsumption stored TotalConsumption_Specified;
    property IdleConsumption:            consumptionType  Index (IS_OPTN) read FIdleConsumption write SetIdleConsumption stored IdleConsumption_Specified;
    property AdBlueConsumption:          consumptionType  Index (IS_OPTN) read FAdBlueConsumption write SetAdBlueConsumption stored AdBlueConsumption_Specified;
    property IdleConsumptionPTOActive:   consumptionType  Index (IS_OPTN) read FIdleConsumptionPTOActive write SetIdleConsumptionPTOActive stored IdleConsumptionPTOActive_Specified;
    property IdleConsumptionPTOInactive: consumptionType  Index (IS_OPTN) read FIdleConsumptionPTOInactive write SetIdleConsumptionPTOInactive stored IdleConsumptionPTOInactive_Specified;
  end;



  // ************************************************************************ //
  // XML       : Consumption, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  Consumption4 = class(TRemotable)
  private
    FDriveConsumption: consumptionType;
    FDriveConsumption_Specified: boolean;
    FTotalConsumption: consumptionType;
    FTotalConsumption_Specified: boolean;
    FStandConsumption: consumptionType;
    FStandConsumption_Specified: boolean;
    FPTOConsumption: consumptionType;
    FPTOConsumption_Specified: boolean;
    procedure SetDriveConsumption(Index: Integer; const AconsumptionType: consumptionType);
    function  DriveConsumption_Specified(Index: Integer): boolean;
    procedure SetTotalConsumption(Index: Integer; const AconsumptionType: consumptionType);
    function  TotalConsumption_Specified(Index: Integer): boolean;
    procedure SetStandConsumption(Index: Integer; const AconsumptionType: consumptionType);
    function  StandConsumption_Specified(Index: Integer): boolean;
    procedure SetPTOConsumption(Index: Integer; const AconsumptionType: consumptionType);
    function  PTOConsumption_Specified(Index: Integer): boolean;
  published
    property DriveConsumption: consumptionType  Index (IS_OPTN) read FDriveConsumption write SetDriveConsumption stored DriveConsumption_Specified;
    property TotalConsumption: consumptionType  Index (IS_OPTN) read FTotalConsumption write SetTotalConsumption stored TotalConsumption_Specified;
    property StandConsumption: consumptionType  Index (IS_OPTN) read FStandConsumption write SetStandConsumption stored StandConsumption_Specified;
    property PTOConsumption:   consumptionType  Index (IS_OPTN) read FPTOConsumption write SetPTOConsumption stored PTOConsumption_Specified;
  end;



  // ************************************************************************ //
  // XML       : Consumption, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  Consumption5 = class(TRemotable)
  private
    FDriveConsumption: consumptionType;
    FDriveConsumption_Specified: boolean;
    FTotalConsumption: consumptionType;
    FTotalConsumption_Specified: boolean;
    FStandConsumption: consumptionType;
    FStandConsumption_Specified: boolean;
    FPTOConsumption: consumptionType;
    FPTOConsumption_Specified: boolean;
    procedure SetDriveConsumption(Index: Integer; const AconsumptionType: consumptionType);
    function  DriveConsumption_Specified(Index: Integer): boolean;
    procedure SetTotalConsumption(Index: Integer; const AconsumptionType: consumptionType);
    function  TotalConsumption_Specified(Index: Integer): boolean;
    procedure SetStandConsumption(Index: Integer; const AconsumptionType: consumptionType);
    function  StandConsumption_Specified(Index: Integer): boolean;
    procedure SetPTOConsumption(Index: Integer; const AconsumptionType: consumptionType);
    function  PTOConsumption_Specified(Index: Integer): boolean;
  published
    property DriveConsumption: consumptionType  Index (IS_OPTN) read FDriveConsumption write SetDriveConsumption stored DriveConsumption_Specified;
    property TotalConsumption: consumptionType  Index (IS_OPTN) read FTotalConsumption write SetTotalConsumption stored TotalConsumption_Specified;
    property StandConsumption: consumptionType  Index (IS_OPTN) read FStandConsumption write SetStandConsumption stored StandConsumption_Specified;
    property PTOConsumption:   consumptionType  Index (IS_OPTN) read FPTOConsumption write SetPTOConsumption stored PTOConsumption_Specified;
  end;

  speedType       =  type Double;      { "http://www.fleetboard.com/data"[GblSmpl] }


  // ************************************************************************ //
  // XML       : ppc, global, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  ppc = class(TRemotable)
  private
    FLowerHysteresisBoundaryMin: speedType;
    FLowerHysteresisBoundaryAvg: speedType;
    FLowerHysteresisBoundaryMax: speedType;
    FUpperHysteresisBoundaryMin: speedType;
    FUpperHysteresisBoundaryAvg: speedType;
    FUpperHysteresisBoundaryMax: speedType;
  published
    property LowerHysteresisBoundaryMin: speedType  read FLowerHysteresisBoundaryMin write FLowerHysteresisBoundaryMin;
    property LowerHysteresisBoundaryAvg: speedType  read FLowerHysteresisBoundaryAvg write FLowerHysteresisBoundaryAvg;
    property LowerHysteresisBoundaryMax: speedType  read FLowerHysteresisBoundaryMax write FLowerHysteresisBoundaryMax;
    property UpperHysteresisBoundaryMin: speedType  read FUpperHysteresisBoundaryMin write FUpperHysteresisBoundaryMin;
    property UpperHysteresisBoundaryAvg: speedType  read FUpperHysteresisBoundaryAvg write FUpperHysteresisBoundaryAvg;
    property UpperHysteresisBoundaryMax: speedType  read FUpperHysteresisBoundaryMax write FUpperHysteresisBoundaryMax;
  end;



  // ************************************************************************ //
  // XML       : AvgValues, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  AvgValues = class(TRemotable)
  private
    FSpeed: speedType;
    FSpeed_Specified: boolean;
    FOverallConsumption: consumptionType;
    FOverallConsumption_Specified: boolean;
    FDriveConsumption: consumptionType;
    FDriveConsumption_Specified: boolean;
    procedure SetSpeed(Index: Integer; const AspeedType: speedType);
    function  Speed_Specified(Index: Integer): boolean;
    procedure SetOverallConsumption(Index: Integer; const AconsumptionType: consumptionType);
    function  OverallConsumption_Specified(Index: Integer): boolean;
    procedure SetDriveConsumption(Index: Integer; const AconsumptionType: consumptionType);
    function  DriveConsumption_Specified(Index: Integer): boolean;
  published
    property Speed:              speedType        Index (IS_OPTN) read FSpeed write SetSpeed stored Speed_Specified;
    property OverallConsumption: consumptionType  Index (IS_OPTN) read FOverallConsumption write SetOverallConsumption stored OverallConsumption_Specified;
    property DriveConsumption:   consumptionType  Index (IS_OPTN) read FDriveConsumption write SetDriveConsumption stored DriveConsumption_Specified;
  end;



  // ************************************************************************ //
  // XML       : AvgValues, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  AvgValues2 = class(TRemotable)
  private
    FWeight: weightType;
    FWeight_Specified: boolean;
    FSpeed: speedType;
    FSpeed_Specified: boolean;
    FOverallConsumption: consumptionType;
    FOverallConsumption_Specified: boolean;
    FDriveConsumption: consumptionType;
    FDriveConsumption_Specified: boolean;
    FAdBlueConsumption: consumptionType;
    FAdBlueConsumption_Specified: boolean;
    FAdBlueToFuelRatioConsumption: ratioType;
    FAdBlueToFuelRatioConsumption_Specified: boolean;
    procedure SetWeight(Index: Integer; const AweightType: weightType);
    function  Weight_Specified(Index: Integer): boolean;
    procedure SetSpeed(Index: Integer; const AspeedType: speedType);
    function  Speed_Specified(Index: Integer): boolean;
    procedure SetOverallConsumption(Index: Integer; const AconsumptionType: consumptionType);
    function  OverallConsumption_Specified(Index: Integer): boolean;
    procedure SetDriveConsumption(Index: Integer; const AconsumptionType: consumptionType);
    function  DriveConsumption_Specified(Index: Integer): boolean;
    procedure SetAdBlueConsumption(Index: Integer; const AconsumptionType: consumptionType);
    function  AdBlueConsumption_Specified(Index: Integer): boolean;
    procedure SetAdBlueToFuelRatioConsumption(Index: Integer; const AratioType: ratioType);
    function  AdBlueToFuelRatioConsumption_Specified(Index: Integer): boolean;
  published
    property Weight:                       weightType       Index (IS_OPTN) read FWeight write SetWeight stored Weight_Specified;
    property Speed:                        speedType        Index (IS_OPTN) read FSpeed write SetSpeed stored Speed_Specified;
    property OverallConsumption:           consumptionType  Index (IS_OPTN) read FOverallConsumption write SetOverallConsumption stored OverallConsumption_Specified;
    property DriveConsumption:             consumptionType  Index (IS_OPTN) read FDriveConsumption write SetDriveConsumption stored DriveConsumption_Specified;
    property AdBlueConsumption:            consumptionType  Index (IS_OPTN) read FAdBlueConsumption write SetAdBlueConsumption stored AdBlueConsumption_Specified;
    property AdBlueToFuelRatioConsumption: ratioType        Index (IS_OPTN) read FAdBlueToFuelRatioConsumption write SetAdBlueToFuelRatioConsumption stored AdBlueToFuelRatioConsumption_Specified;
  end;



  // ************************************************************************ //
  // XML       : AvgValues, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  AvgValues3 = class(TRemotable)
  private
    FWeight: weightType;
    FWeight_Specified: boolean;
    FSpeed: speedType;
    FSpeed_Specified: boolean;
    FOverallConsumption: consumptionType;
    FOverallConsumption_Specified: boolean;
    FDriveConsumption: consumptionType;
    FDriveConsumption_Specified: boolean;
    FAdBlueConsumption: consumptionType;
    FAdBlueConsumption_Specified: boolean;
    FAdBlueToFuelRatioConsumption: ratioType;
    FAdBlueToFuelRatioConsumption_Specified: boolean;
    procedure SetWeight(Index: Integer; const AweightType: weightType);
    function  Weight_Specified(Index: Integer): boolean;
    procedure SetSpeed(Index: Integer; const AspeedType: speedType);
    function  Speed_Specified(Index: Integer): boolean;
    procedure SetOverallConsumption(Index: Integer; const AconsumptionType: consumptionType);
    function  OverallConsumption_Specified(Index: Integer): boolean;
    procedure SetDriveConsumption(Index: Integer; const AconsumptionType: consumptionType);
    function  DriveConsumption_Specified(Index: Integer): boolean;
    procedure SetAdBlueConsumption(Index: Integer; const AconsumptionType: consumptionType);
    function  AdBlueConsumption_Specified(Index: Integer): boolean;
    procedure SetAdBlueToFuelRatioConsumption(Index: Integer; const AratioType: ratioType);
    function  AdBlueToFuelRatioConsumption_Specified(Index: Integer): boolean;
  published
    property Weight:                       weightType       Index (IS_OPTN) read FWeight write SetWeight stored Weight_Specified;
    property Speed:                        speedType        Index (IS_OPTN) read FSpeed write SetSpeed stored Speed_Specified;
    property OverallConsumption:           consumptionType  Index (IS_OPTN) read FOverallConsumption write SetOverallConsumption stored OverallConsumption_Specified;
    property DriveConsumption:             consumptionType  Index (IS_OPTN) read FDriveConsumption write SetDriveConsumption stored DriveConsumption_Specified;
    property AdBlueConsumption:            consumptionType  Index (IS_OPTN) read FAdBlueConsumption write SetAdBlueConsumption stored AdBlueConsumption_Specified;
    property AdBlueToFuelRatioConsumption: ratioType        Index (IS_OPTN) read FAdBlueToFuelRatioConsumption write SetAdBlueToFuelRatioConsumption stored AdBlueToFuelRatioConsumption_Specified;
  end;



  // ************************************************************************ //
  // XML       : AvgValues, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  AvgValues4 = class(TRemotable)
  private
    FSpeed: speedType;
    FSpeed_Specified: boolean;
    FOverallConsumption: consumptionType;
    FOverallConsumption_Specified: boolean;
    FDriveConsumption: consumptionType;
    FDriveConsumption_Specified: boolean;
    procedure SetSpeed(Index: Integer; const AspeedType: speedType);
    function  Speed_Specified(Index: Integer): boolean;
    procedure SetOverallConsumption(Index: Integer; const AconsumptionType: consumptionType);
    function  OverallConsumption_Specified(Index: Integer): boolean;
    procedure SetDriveConsumption(Index: Integer; const AconsumptionType: consumptionType);
    function  DriveConsumption_Specified(Index: Integer): boolean;
  published
    property Speed:              speedType        Index (IS_OPTN) read FSpeed write SetSpeed stored Speed_Specified;
    property OverallConsumption: consumptionType  Index (IS_OPTN) read FOverallConsumption write SetOverallConsumption stored OverallConsumption_Specified;
    property DriveConsumption:   consumptionType  Index (IS_OPTN) read FDriveConsumption write SetDriveConsumption stored DriveConsumption_Specified;
  end;



  // ************************************************************************ //
  // XML       : AvgValues, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  AvgValues5 = class(TRemotable)
  private
    FSpeed: speedType;
    FSpeed_Specified: boolean;
    FOverallConsumption: consumptionType;
    FOverallConsumption_Specified: boolean;
    FDriveConsumption: consumptionType;
    FDriveConsumption_Specified: boolean;
    procedure SetSpeed(Index: Integer; const AspeedType: speedType);
    function  Speed_Specified(Index: Integer): boolean;
    procedure SetOverallConsumption(Index: Integer; const AconsumptionType: consumptionType);
    function  OverallConsumption_Specified(Index: Integer): boolean;
    procedure SetDriveConsumption(Index: Integer; const AconsumptionType: consumptionType);
    function  DriveConsumption_Specified(Index: Integer): boolean;
  published
    property Speed:              speedType        Index (IS_OPTN) read FSpeed write SetSpeed stored Speed_Specified;
    property OverallConsumption: consumptionType  Index (IS_OPTN) read FOverallConsumption write SetOverallConsumption stored OverallConsumption_Specified;
    property DriveConsumption:   consumptionType  Index (IS_OPTN) read FDriveConsumption write SetDriveConsumption stored DriveConsumption_Specified;
  end;



  // ************************************************************************ //
  // XML       : MinMaxSpeedType, global, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  MinMaxSpeedType = class(TRemotable)
  private
    FMin: speedType;
    FMax: speedType;
  published
    property Min: speedType  read FMin write FMin;
    property Max: speedType  read FMax write FMax;
  end;



  // ************************************************************************ //
  // XML       : MinMaxConsumptionType, global, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  MinMaxConsumptionType = class(TRemotable)
  private
    FMin: consumptionType;
    FMax: consumptionType;
  published
    property Min: consumptionType  read FMin write FMin;
    property Max: consumptionType  read FMax write FMax;
  end;

  gradeType       =  type Double;      { "http://www.fleetboard.com/data"[GblSmpl] }


  // ************************************************************************ //
  // XML       : Grades, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  Grades = class(TRemotable)
  private
    FDrivingStyle: gradeType;
    FDrivingStyle_Specified: boolean;
    FEconomicDrivingGrading: gradeType;
    FEconomicDrivingGrading_Specified: boolean;
    FOverRevNumberGrading: gradeType;
    FOverRevNumberGrading_Specified: boolean;
    FOverRevTimeGrading: gradeType;
    FOverRevTimeGrading_Specified: boolean;
    FOverSpeedTimeGrading: gradeType;
    FOverSpeedTimeGrading_Specified: boolean;
    FHarshBrakeGrading: gradeType;
    FHarshBrakeGrading_Specified: boolean;
    FIdlingTimeGrading: gradeType;
    FIdlingTimeGrading_Specified: boolean;
    procedure SetDrivingStyle(Index: Integer; const AgradeType: gradeType);
    function  DrivingStyle_Specified(Index: Integer): boolean;
    procedure SetEconomicDrivingGrading(Index: Integer; const AgradeType: gradeType);
    function  EconomicDrivingGrading_Specified(Index: Integer): boolean;
    procedure SetOverRevNumberGrading(Index: Integer; const AgradeType: gradeType);
    function  OverRevNumberGrading_Specified(Index: Integer): boolean;
    procedure SetOverRevTimeGrading(Index: Integer; const AgradeType: gradeType);
    function  OverRevTimeGrading_Specified(Index: Integer): boolean;
    procedure SetOverSpeedTimeGrading(Index: Integer; const AgradeType: gradeType);
    function  OverSpeedTimeGrading_Specified(Index: Integer): boolean;
    procedure SetHarshBrakeGrading(Index: Integer; const AgradeType: gradeType);
    function  HarshBrakeGrading_Specified(Index: Integer): boolean;
    procedure SetIdlingTimeGrading(Index: Integer; const AgradeType: gradeType);
    function  IdlingTimeGrading_Specified(Index: Integer): boolean;
  published
    property DrivingStyle:           gradeType  Index (IS_OPTN) read FDrivingStyle write SetDrivingStyle stored DrivingStyle_Specified;
    property EconomicDrivingGrading: gradeType  Index (IS_OPTN) read FEconomicDrivingGrading write SetEconomicDrivingGrading stored EconomicDrivingGrading_Specified;
    property OverRevNumberGrading:   gradeType  Index (IS_OPTN) read FOverRevNumberGrading write SetOverRevNumberGrading stored OverRevNumberGrading_Specified;
    property OverRevTimeGrading:     gradeType  Index (IS_OPTN) read FOverRevTimeGrading write SetOverRevTimeGrading stored OverRevTimeGrading_Specified;
    property OverSpeedTimeGrading:   gradeType  Index (IS_OPTN) read FOverSpeedTimeGrading write SetOverSpeedTimeGrading stored OverSpeedTimeGrading_Specified;
    property HarshBrakeGrading:      gradeType  Index (IS_OPTN) read FHarshBrakeGrading write SetHarshBrakeGrading stored HarshBrakeGrading_Specified;
    property IdlingTimeGrading:      gradeType  Index (IS_OPTN) read FIdlingTimeGrading write SetIdlingTimeGrading stored IdlingTimeGrading_Specified;
  end;



  // ************************************************************************ //
  // XML       : Grades, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  Grades2 = class(TRemotable)
  private
    FDrivingStyle: gradeType;
    FDrivingStyle_Specified: boolean;
    FEconomicDrivingGrading: gradeType;
    FEconomicDrivingGrading_Specified: boolean;
    FOverRevNumberGrading: gradeType;
    FOverRevNumberGrading_Specified: boolean;
    FOverRevTimeGrading: gradeType;
    FOverRevTimeGrading_Specified: boolean;
    FOverSpeedTimeGrading: gradeType;
    FOverSpeedTimeGrading_Specified: boolean;
    FHarshBrakeGrading: gradeType;
    FHarshBrakeGrading_Specified: boolean;
    FIdlingTimeGrading: gradeType;
    FIdlingTimeGrading_Specified: boolean;
    procedure SetDrivingStyle(Index: Integer; const AgradeType: gradeType);
    function  DrivingStyle_Specified(Index: Integer): boolean;
    procedure SetEconomicDrivingGrading(Index: Integer; const AgradeType: gradeType);
    function  EconomicDrivingGrading_Specified(Index: Integer): boolean;
    procedure SetOverRevNumberGrading(Index: Integer; const AgradeType: gradeType);
    function  OverRevNumberGrading_Specified(Index: Integer): boolean;
    procedure SetOverRevTimeGrading(Index: Integer; const AgradeType: gradeType);
    function  OverRevTimeGrading_Specified(Index: Integer): boolean;
    procedure SetOverSpeedTimeGrading(Index: Integer; const AgradeType: gradeType);
    function  OverSpeedTimeGrading_Specified(Index: Integer): boolean;
    procedure SetHarshBrakeGrading(Index: Integer; const AgradeType: gradeType);
    function  HarshBrakeGrading_Specified(Index: Integer): boolean;
    procedure SetIdlingTimeGrading(Index: Integer; const AgradeType: gradeType);
    function  IdlingTimeGrading_Specified(Index: Integer): boolean;
  published
    property DrivingStyle:           gradeType  Index (IS_OPTN) read FDrivingStyle write SetDrivingStyle stored DrivingStyle_Specified;
    property EconomicDrivingGrading: gradeType  Index (IS_OPTN) read FEconomicDrivingGrading write SetEconomicDrivingGrading stored EconomicDrivingGrading_Specified;
    property OverRevNumberGrading:   gradeType  Index (IS_OPTN) read FOverRevNumberGrading write SetOverRevNumberGrading stored OverRevNumberGrading_Specified;
    property OverRevTimeGrading:     gradeType  Index (IS_OPTN) read FOverRevTimeGrading write SetOverRevTimeGrading stored OverRevTimeGrading_Specified;
    property OverSpeedTimeGrading:   gradeType  Index (IS_OPTN) read FOverSpeedTimeGrading write SetOverSpeedTimeGrading stored OverSpeedTimeGrading_Specified;
    property HarshBrakeGrading:      gradeType  Index (IS_OPTN) read FHarshBrakeGrading write SetHarshBrakeGrading stored HarshBrakeGrading_Specified;
    property IdlingTimeGrading:      gradeType  Index (IS_OPTN) read FIdlingTimeGrading write SetIdlingTimeGrading stored IdlingTimeGrading_Specified;
  end;



  // ************************************************************************ //
  // XML       : Grades, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  Grades3 = class(TRemotable)
  private
    FDrivingStyle: gradeType;
    FDrivingStyle_Specified: boolean;
    FOverrevNumberGrading: gradeType;
    FOverrevNumberGrading_Specified: boolean;
    FOverrevTimeGrading: gradeType;
    FOverrevTimeGrading_Specified: boolean;
    FOverspeedTimeGrading: gradeType;
    FOverspeedTimeGrading_Specified: boolean;
    FEconomicDrivingGrading: gradeType;
    FEconomicDrivingGrading_Specified: boolean;
    FIdlingTimeGrading: gradeType;
    FIdlingTimeGrading_Specified: boolean;
    FHarshBrakeGrading: gradeType;
    FHarshBrakeGrading_Specified: boolean;
    procedure SetDrivingStyle(Index: Integer; const AgradeType: gradeType);
    function  DrivingStyle_Specified(Index: Integer): boolean;
    procedure SetOverrevNumberGrading(Index: Integer; const AgradeType: gradeType);
    function  OverrevNumberGrading_Specified(Index: Integer): boolean;
    procedure SetOverrevTimeGrading(Index: Integer; const AgradeType: gradeType);
    function  OverrevTimeGrading_Specified(Index: Integer): boolean;
    procedure SetOverspeedTimeGrading(Index: Integer; const AgradeType: gradeType);
    function  OverspeedTimeGrading_Specified(Index: Integer): boolean;
    procedure SetEconomicDrivingGrading(Index: Integer; const AgradeType: gradeType);
    function  EconomicDrivingGrading_Specified(Index: Integer): boolean;
    procedure SetIdlingTimeGrading(Index: Integer; const AgradeType: gradeType);
    function  IdlingTimeGrading_Specified(Index: Integer): boolean;
    procedure SetHarshBrakeGrading(Index: Integer; const AgradeType: gradeType);
    function  HarshBrakeGrading_Specified(Index: Integer): boolean;
  published
    property DrivingStyle:           gradeType  Index (IS_OPTN) read FDrivingStyle write SetDrivingStyle stored DrivingStyle_Specified;
    property OverrevNumberGrading:   gradeType  Index (IS_OPTN) read FOverrevNumberGrading write SetOverrevNumberGrading stored OverrevNumberGrading_Specified;
    property OverrevTimeGrading:     gradeType  Index (IS_OPTN) read FOverrevTimeGrading write SetOverrevTimeGrading stored OverrevTimeGrading_Specified;
    property OverspeedTimeGrading:   gradeType  Index (IS_OPTN) read FOverspeedTimeGrading write SetOverspeedTimeGrading stored OverspeedTimeGrading_Specified;
    property EconomicDrivingGrading: gradeType  Index (IS_OPTN) read FEconomicDrivingGrading write SetEconomicDrivingGrading stored EconomicDrivingGrading_Specified;
    property IdlingTimeGrading:      gradeType  Index (IS_OPTN) read FIdlingTimeGrading write SetIdlingTimeGrading stored IdlingTimeGrading_Specified;
    property HarshBrakeGrading:      gradeType  Index (IS_OPTN) read FHarshBrakeGrading write SetHarshBrakeGrading stored HarshBrakeGrading_Specified;
  end;



  // ************************************************************************ //
  // XML       : Grades, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  Grades4 = class(TRemotable)
  private
    FDrivingStyle: gradeType;
    FDrivingStyle_Specified: boolean;
    FDrivingStyleCons: gradeType;
    FDrivingStyleCons_Specified: boolean;
    FDrivingStyleConsPreventive: gradeType;
    FDrivingStyleConsPreventive_Specified: boolean;
    FDrivingStyleConsEngineOperationMN: gradeType;
    FDrivingStyleConsEngineOperationMN_Specified: boolean;
    FDrivingStyleConsPedalMovements: gradeType;
    FDrivingStyleConsPedalMovements_Specified: boolean;
    FDrivingStyleConsUniformSpeed: gradeType;
    FDrivingStyleConsUniformSpeed_Specified: boolean;
    FDrivingStyleConsStops: gradeType;
    FDrivingStyleConsStops_Specified: boolean;
    FDrivingStyleBrake: gradeType;
    FDrivingStyleBrake_Specified: boolean;
    FDrivingStyleBrakePreventive: gradeType;
    FDrivingStyleBrakePreventive_Specified: boolean;
    FDrivingStyleBrakeDeceleration: gradeType;
    FDrivingStyleBrakeDeceleration_Specified: boolean;
    FDegreeOfDifficulty: gradeType;
    FDegreeOfDifficulty_Specified: boolean;
    FDegreeOfDifficultyAverageSlope: gradeType;
    FDegreeOfDifficultyAverageSlope_Specified: boolean;
    FDegreeOfDifficultyWeight: gradeType;
    FDegreeOfDifficultyWeight_Specified: boolean;
    FDegreeOfDifficultyStops: gradeType;
    FDegreeOfDifficultyStops_Specified: boolean;
    FTorqueClassification: gradeType;
    FTorqueClassification_Specified: boolean;
    procedure SetDrivingStyle(Index: Integer; const AgradeType: gradeType);
    function  DrivingStyle_Specified(Index: Integer): boolean;
    procedure SetDrivingStyleCons(Index: Integer; const AgradeType: gradeType);
    function  DrivingStyleCons_Specified(Index: Integer): boolean;
    procedure SetDrivingStyleConsPreventive(Index: Integer; const AgradeType: gradeType);
    function  DrivingStyleConsPreventive_Specified(Index: Integer): boolean;
    procedure SetDrivingStyleConsEngineOperationMN(Index: Integer; const AgradeType: gradeType);
    function  DrivingStyleConsEngineOperationMN_Specified(Index: Integer): boolean;
    procedure SetDrivingStyleConsPedalMovements(Index: Integer; const AgradeType: gradeType);
    function  DrivingStyleConsPedalMovements_Specified(Index: Integer): boolean;
    procedure SetDrivingStyleConsUniformSpeed(Index: Integer; const AgradeType: gradeType);
    function  DrivingStyleConsUniformSpeed_Specified(Index: Integer): boolean;
    procedure SetDrivingStyleConsStops(Index: Integer; const AgradeType: gradeType);
    function  DrivingStyleConsStops_Specified(Index: Integer): boolean;
    procedure SetDrivingStyleBrake(Index: Integer; const AgradeType: gradeType);
    function  DrivingStyleBrake_Specified(Index: Integer): boolean;
    procedure SetDrivingStyleBrakePreventive(Index: Integer; const AgradeType: gradeType);
    function  DrivingStyleBrakePreventive_Specified(Index: Integer): boolean;
    procedure SetDrivingStyleBrakeDeceleration(Index: Integer; const AgradeType: gradeType);
    function  DrivingStyleBrakeDeceleration_Specified(Index: Integer): boolean;
    procedure SetDegreeOfDifficulty(Index: Integer; const AgradeType: gradeType);
    function  DegreeOfDifficulty_Specified(Index: Integer): boolean;
    procedure SetDegreeOfDifficultyAverageSlope(Index: Integer; const AgradeType: gradeType);
    function  DegreeOfDifficultyAverageSlope_Specified(Index: Integer): boolean;
    procedure SetDegreeOfDifficultyWeight(Index: Integer; const AgradeType: gradeType);
    function  DegreeOfDifficultyWeight_Specified(Index: Integer): boolean;
    procedure SetDegreeOfDifficultyStops(Index: Integer; const AgradeType: gradeType);
    function  DegreeOfDifficultyStops_Specified(Index: Integer): boolean;
    procedure SetTorqueClassification(Index: Integer; const AgradeType: gradeType);
    function  TorqueClassification_Specified(Index: Integer): boolean;
  published
    property DrivingStyle:                      gradeType  Index (IS_OPTN) read FDrivingStyle write SetDrivingStyle stored DrivingStyle_Specified;
    property DrivingStyleCons:                  gradeType  Index (IS_OPTN) read FDrivingStyleCons write SetDrivingStyleCons stored DrivingStyleCons_Specified;
    property DrivingStyleConsPreventive:        gradeType  Index (IS_OPTN) read FDrivingStyleConsPreventive write SetDrivingStyleConsPreventive stored DrivingStyleConsPreventive_Specified;
    property DrivingStyleConsEngineOperationMN: gradeType  Index (IS_OPTN) read FDrivingStyleConsEngineOperationMN write SetDrivingStyleConsEngineOperationMN stored DrivingStyleConsEngineOperationMN_Specified;
    property DrivingStyleConsPedalMovements:    gradeType  Index (IS_OPTN) read FDrivingStyleConsPedalMovements write SetDrivingStyleConsPedalMovements stored DrivingStyleConsPedalMovements_Specified;
    property DrivingStyleConsUniformSpeed:      gradeType  Index (IS_OPTN) read FDrivingStyleConsUniformSpeed write SetDrivingStyleConsUniformSpeed stored DrivingStyleConsUniformSpeed_Specified;
    property DrivingStyleConsStops:             gradeType  Index (IS_OPTN) read FDrivingStyleConsStops write SetDrivingStyleConsStops stored DrivingStyleConsStops_Specified;
    property DrivingStyleBrake:                 gradeType  Index (IS_OPTN) read FDrivingStyleBrake write SetDrivingStyleBrake stored DrivingStyleBrake_Specified;
    property DrivingStyleBrakePreventive:       gradeType  Index (IS_OPTN) read FDrivingStyleBrakePreventive write SetDrivingStyleBrakePreventive stored DrivingStyleBrakePreventive_Specified;
    property DrivingStyleBrakeDeceleration:     gradeType  Index (IS_OPTN) read FDrivingStyleBrakeDeceleration write SetDrivingStyleBrakeDeceleration stored DrivingStyleBrakeDeceleration_Specified;
    property DegreeOfDifficulty:                gradeType  Index (IS_OPTN) read FDegreeOfDifficulty write SetDegreeOfDifficulty stored DegreeOfDifficulty_Specified;
    property DegreeOfDifficultyAverageSlope:    gradeType  Index (IS_OPTN) read FDegreeOfDifficultyAverageSlope write SetDegreeOfDifficultyAverageSlope stored DegreeOfDifficultyAverageSlope_Specified;
    property DegreeOfDifficultyWeight:          gradeType  Index (IS_OPTN) read FDegreeOfDifficultyWeight write SetDegreeOfDifficultyWeight stored DegreeOfDifficultyWeight_Specified;
    property DegreeOfDifficultyStops:           gradeType  Index (IS_OPTN) read FDegreeOfDifficultyStops write SetDegreeOfDifficultyStops stored DegreeOfDifficultyStops_Specified;
    property TorqueClassification:              gradeType  Index (IS_OPTN) read FTorqueClassification write SetTorqueClassification stored TorqueClassification_Specified;
  end;



  // ************************************************************************ //
  // XML       : Grades, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  Grades5 = class(TRemotable)
  private
    FDrivingStyle: gradeType;
    FDrivingStyle_Specified: boolean;
    FDrivingStyleCons: gradeType;
    FDrivingStyleCons_Specified: boolean;
    FDrivingStyleConsPreventive: gradeType;
    FDrivingStyleConsPreventive_Specified: boolean;
    FDrivingStyleConsEngineOperationMN: gradeType;
    FDrivingStyleConsEngineOperationMN_Specified: boolean;
    FDrivingStyleConsPedalMovements: gradeType;
    FDrivingStyleConsPedalMovements_Specified: boolean;
    FDrivingStyleConsUniformSpeed: gradeType;
    FDrivingStyleConsUniformSpeed_Specified: boolean;
    FDrivingStyleConsStops: gradeType;
    FDrivingStyleConsStops_Specified: boolean;
    FDrivingStyleBrake: gradeType;
    FDrivingStyleBrake_Specified: boolean;
    FDrivingStyleBrakePreventive: gradeType;
    FDrivingStyleBrakePreventive_Specified: boolean;
    FDrivingStyleBrakeDeceleration: gradeType;
    FDrivingStyleBrakeDeceleration_Specified: boolean;
    FDegreeOfDifficulty: gradeType;
    FDegreeOfDifficulty_Specified: boolean;
    FDegreeOfDifficultyAverageSlope: gradeType;
    FDegreeOfDifficultyAverageSlope_Specified: boolean;
    FDegreeOfDifficultyWeight: gradeType;
    FDegreeOfDifficultyWeight_Specified: boolean;
    FDegreeOfDifficultyStops: gradeType;
    FDegreeOfDifficultyStops_Specified: boolean;
    FTorqueClassification: gradeType;
    FTorqueClassification_Specified: boolean;
    procedure SetDrivingStyle(Index: Integer; const AgradeType: gradeType);
    function  DrivingStyle_Specified(Index: Integer): boolean;
    procedure SetDrivingStyleCons(Index: Integer; const AgradeType: gradeType);
    function  DrivingStyleCons_Specified(Index: Integer): boolean;
    procedure SetDrivingStyleConsPreventive(Index: Integer; const AgradeType: gradeType);
    function  DrivingStyleConsPreventive_Specified(Index: Integer): boolean;
    procedure SetDrivingStyleConsEngineOperationMN(Index: Integer; const AgradeType: gradeType);
    function  DrivingStyleConsEngineOperationMN_Specified(Index: Integer): boolean;
    procedure SetDrivingStyleConsPedalMovements(Index: Integer; const AgradeType: gradeType);
    function  DrivingStyleConsPedalMovements_Specified(Index: Integer): boolean;
    procedure SetDrivingStyleConsUniformSpeed(Index: Integer; const AgradeType: gradeType);
    function  DrivingStyleConsUniformSpeed_Specified(Index: Integer): boolean;
    procedure SetDrivingStyleConsStops(Index: Integer; const AgradeType: gradeType);
    function  DrivingStyleConsStops_Specified(Index: Integer): boolean;
    procedure SetDrivingStyleBrake(Index: Integer; const AgradeType: gradeType);
    function  DrivingStyleBrake_Specified(Index: Integer): boolean;
    procedure SetDrivingStyleBrakePreventive(Index: Integer; const AgradeType: gradeType);
    function  DrivingStyleBrakePreventive_Specified(Index: Integer): boolean;
    procedure SetDrivingStyleBrakeDeceleration(Index: Integer; const AgradeType: gradeType);
    function  DrivingStyleBrakeDeceleration_Specified(Index: Integer): boolean;
    procedure SetDegreeOfDifficulty(Index: Integer; const AgradeType: gradeType);
    function  DegreeOfDifficulty_Specified(Index: Integer): boolean;
    procedure SetDegreeOfDifficultyAverageSlope(Index: Integer; const AgradeType: gradeType);
    function  DegreeOfDifficultyAverageSlope_Specified(Index: Integer): boolean;
    procedure SetDegreeOfDifficultyWeight(Index: Integer; const AgradeType: gradeType);
    function  DegreeOfDifficultyWeight_Specified(Index: Integer): boolean;
    procedure SetDegreeOfDifficultyStops(Index: Integer; const AgradeType: gradeType);
    function  DegreeOfDifficultyStops_Specified(Index: Integer): boolean;
    procedure SetTorqueClassification(Index: Integer; const AgradeType: gradeType);
    function  TorqueClassification_Specified(Index: Integer): boolean;
  published
    property DrivingStyle:                      gradeType  Index (IS_OPTN) read FDrivingStyle write SetDrivingStyle stored DrivingStyle_Specified;
    property DrivingStyleCons:                  gradeType  Index (IS_OPTN) read FDrivingStyleCons write SetDrivingStyleCons stored DrivingStyleCons_Specified;
    property DrivingStyleConsPreventive:        gradeType  Index (IS_OPTN) read FDrivingStyleConsPreventive write SetDrivingStyleConsPreventive stored DrivingStyleConsPreventive_Specified;
    property DrivingStyleConsEngineOperationMN: gradeType  Index (IS_OPTN) read FDrivingStyleConsEngineOperationMN write SetDrivingStyleConsEngineOperationMN stored DrivingStyleConsEngineOperationMN_Specified;
    property DrivingStyleConsPedalMovements:    gradeType  Index (IS_OPTN) read FDrivingStyleConsPedalMovements write SetDrivingStyleConsPedalMovements stored DrivingStyleConsPedalMovements_Specified;
    property DrivingStyleConsUniformSpeed:      gradeType  Index (IS_OPTN) read FDrivingStyleConsUniformSpeed write SetDrivingStyleConsUniformSpeed stored DrivingStyleConsUniformSpeed_Specified;
    property DrivingStyleConsStops:             gradeType  Index (IS_OPTN) read FDrivingStyleConsStops write SetDrivingStyleConsStops stored DrivingStyleConsStops_Specified;
    property DrivingStyleBrake:                 gradeType  Index (IS_OPTN) read FDrivingStyleBrake write SetDrivingStyleBrake stored DrivingStyleBrake_Specified;
    property DrivingStyleBrakePreventive:       gradeType  Index (IS_OPTN) read FDrivingStyleBrakePreventive write SetDrivingStyleBrakePreventive stored DrivingStyleBrakePreventive_Specified;
    property DrivingStyleBrakeDeceleration:     gradeType  Index (IS_OPTN) read FDrivingStyleBrakeDeceleration write SetDrivingStyleBrakeDeceleration stored DrivingStyleBrakeDeceleration_Specified;
    property DegreeOfDifficulty:                gradeType  Index (IS_OPTN) read FDegreeOfDifficulty write SetDegreeOfDifficulty stored DegreeOfDifficulty_Specified;
    property DegreeOfDifficultyAverageSlope:    gradeType  Index (IS_OPTN) read FDegreeOfDifficultyAverageSlope write SetDegreeOfDifficultyAverageSlope stored DegreeOfDifficultyAverageSlope_Specified;
    property DegreeOfDifficultyWeight:          gradeType  Index (IS_OPTN) read FDegreeOfDifficultyWeight write SetDegreeOfDifficultyWeight stored DegreeOfDifficultyWeight_Specified;
    property DegreeOfDifficultyStops:           gradeType  Index (IS_OPTN) read FDegreeOfDifficultyStops write SetDegreeOfDifficultyStops stored DegreeOfDifficultyStops_Specified;
    property TorqueClassification:              gradeType  Index (IS_OPTN) read FTorqueClassification write SetTorqueClassification stored TorqueClassification_Specified;
  end;



  // ************************************************************************ //
  // XML       : Grades, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  Grades6 = class(TRemotable)
  private
    FDrivingStyle: gradeType;
    FDrivingStyle_Specified: boolean;
    FOverrevNumberGrading: gradeType;
    FOverrevNumberGrading_Specified: boolean;
    FOverrevTimeGrading: gradeType;
    FOverrevTimeGrading_Specified: boolean;
    FHarshBrakeGrading: gradeType;
    FHarshBrakeGrading_Specified: boolean;
    FOverspeedTimeGrading: gradeType;
    FOverspeedTimeGrading_Specified: boolean;
    FEconomicDrivingGrading: gradeType;
    FEconomicDrivingGrading_Specified: boolean;
    FIdlingTimeGrading: gradeType;
    FIdlingTimeGrading_Specified: boolean;
    procedure SetDrivingStyle(Index: Integer; const AgradeType: gradeType);
    function  DrivingStyle_Specified(Index: Integer): boolean;
    procedure SetOverrevNumberGrading(Index: Integer; const AgradeType: gradeType);
    function  OverrevNumberGrading_Specified(Index: Integer): boolean;
    procedure SetOverrevTimeGrading(Index: Integer; const AgradeType: gradeType);
    function  OverrevTimeGrading_Specified(Index: Integer): boolean;
    procedure SetHarshBrakeGrading(Index: Integer; const AgradeType: gradeType);
    function  HarshBrakeGrading_Specified(Index: Integer): boolean;
    procedure SetOverspeedTimeGrading(Index: Integer; const AgradeType: gradeType);
    function  OverspeedTimeGrading_Specified(Index: Integer): boolean;
    procedure SetEconomicDrivingGrading(Index: Integer; const AgradeType: gradeType);
    function  EconomicDrivingGrading_Specified(Index: Integer): boolean;
    procedure SetIdlingTimeGrading(Index: Integer; const AgradeType: gradeType);
    function  IdlingTimeGrading_Specified(Index: Integer): boolean;
  published
    property DrivingStyle:           gradeType  Index (IS_OPTN) read FDrivingStyle write SetDrivingStyle stored DrivingStyle_Specified;
    property OverrevNumberGrading:   gradeType  Index (IS_OPTN) read FOverrevNumberGrading write SetOverrevNumberGrading stored OverrevNumberGrading_Specified;
    property OverrevTimeGrading:     gradeType  Index (IS_OPTN) read FOverrevTimeGrading write SetOverrevTimeGrading stored OverrevTimeGrading_Specified;
    property HarshBrakeGrading:      gradeType  Index (IS_OPTN) read FHarshBrakeGrading write SetHarshBrakeGrading stored HarshBrakeGrading_Specified;
    property OverspeedTimeGrading:   gradeType  Index (IS_OPTN) read FOverspeedTimeGrading write SetOverspeedTimeGrading stored OverspeedTimeGrading_Specified;
    property EconomicDrivingGrading: gradeType  Index (IS_OPTN) read FEconomicDrivingGrading write SetEconomicDrivingGrading stored EconomicDrivingGrading_Specified;
    property IdlingTimeGrading:      gradeType  Index (IS_OPTN) read FIdlingTimeGrading write SetIdlingTimeGrading stored IdlingTimeGrading_Specified;
  end;



  // ************************************************************************ //
  // XML       : Grades, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  Grades7 = class(TRemotable)
  private
    FDrivingStyle: gradeType;
    FDrivingStyle_Specified: boolean;
    FOverrevNumberGrading: gradeType;
    FOverrevNumberGrading_Specified: boolean;
    FOverrevTimeGrading: gradeType;
    FOverrevTimeGrading_Specified: boolean;
    FHarshBrakeGrading: gradeType;
    FHarshBrakeGrading_Specified: boolean;
    FOverspeedTimeGrading: gradeType;
    FOverspeedTimeGrading_Specified: boolean;
    FEconomicDrivingGrading: gradeType;
    FEconomicDrivingGrading_Specified: boolean;
    FIdlingTimeGrading: gradeType;
    FIdlingTimeGrading_Specified: boolean;
    procedure SetDrivingStyle(Index: Integer; const AgradeType: gradeType);
    function  DrivingStyle_Specified(Index: Integer): boolean;
    procedure SetOverrevNumberGrading(Index: Integer; const AgradeType: gradeType);
    function  OverrevNumberGrading_Specified(Index: Integer): boolean;
    procedure SetOverrevTimeGrading(Index: Integer; const AgradeType: gradeType);
    function  OverrevTimeGrading_Specified(Index: Integer): boolean;
    procedure SetHarshBrakeGrading(Index: Integer; const AgradeType: gradeType);
    function  HarshBrakeGrading_Specified(Index: Integer): boolean;
    procedure SetOverspeedTimeGrading(Index: Integer; const AgradeType: gradeType);
    function  OverspeedTimeGrading_Specified(Index: Integer): boolean;
    procedure SetEconomicDrivingGrading(Index: Integer; const AgradeType: gradeType);
    function  EconomicDrivingGrading_Specified(Index: Integer): boolean;
    procedure SetIdlingTimeGrading(Index: Integer; const AgradeType: gradeType);
    function  IdlingTimeGrading_Specified(Index: Integer): boolean;
  published
    property DrivingStyle:           gradeType  Index (IS_OPTN) read FDrivingStyle write SetDrivingStyle stored DrivingStyle_Specified;
    property OverrevNumberGrading:   gradeType  Index (IS_OPTN) read FOverrevNumberGrading write SetOverrevNumberGrading stored OverrevNumberGrading_Specified;
    property OverrevTimeGrading:     gradeType  Index (IS_OPTN) read FOverrevTimeGrading write SetOverrevTimeGrading stored OverrevTimeGrading_Specified;
    property HarshBrakeGrading:      gradeType  Index (IS_OPTN) read FHarshBrakeGrading write SetHarshBrakeGrading stored HarshBrakeGrading_Specified;
    property OverspeedTimeGrading:   gradeType  Index (IS_OPTN) read FOverspeedTimeGrading write SetOverspeedTimeGrading stored OverspeedTimeGrading_Specified;
    property EconomicDrivingGrading: gradeType  Index (IS_OPTN) read FEconomicDrivingGrading write SetEconomicDrivingGrading stored EconomicDrivingGrading_Specified;
    property IdlingTimeGrading:      gradeType  Index (IS_OPTN) read FIdlingTimeGrading write SetIdlingTimeGrading stored IdlingTimeGrading_Specified;
  end;



  // ************************************************************************ //
  // XML       : MinMaxGradeType, global, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  MinMaxGradeType = class(TRemotable)
  private
    FMin: gradeType;
    FMax: gradeType;
  published
    property Min: gradeType  read FMin write FMin;
    property Max: gradeType  read FMax write FMax;
  end;



  // ************************************************************************ //
  // XML       : TimeRangeType, global, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  TimeRangeType = class(TRemotable)
  private
    FModel: Model;
    FModel_Specified: boolean;
    FPeriod: TPDate;
    FPeriod_Specified: boolean;
    procedure SetModel(Index: Integer; const AModel: Model);
    function  Model_Specified(Index: Integer): boolean;
    procedure SetPeriod(Index: Integer; const ATPDate: TPDate);
    function  Period_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property Model:  Model   Index (IS_OPTN) read FModel write SetModel stored Model_Specified;
    property Period: TPDate  Index (IS_OPTN) read FPeriod write SetPeriod stored Period_Specified;
  end;

  vehicleGroupIDType =  type Int64;      { "http://www.fleetboard.com/data"[GblSmpl] }


  // ************************************************************************ //
  // XML       : Values, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  Values = class(TRemotable)
  private
    FX: X;
    FY1: Y1;
    FY2: Y2;
    FY2_Specified: boolean;
    procedure SetY2(Index: Integer; const AY2: Y2);
    function  Y2_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property X:  X   read FX write FX;
    property Y1: Y1  read FY1 write FY1;
    property Y2: Y2  Index (IS_OPTN) read FY2 write SetY2 stored Y2_Specified;
  end;

  driverGroupIDType =  type Int64;      { "http://www.fleetboard.com/data"[GblSmpl] }


  // ************************************************************************ //
  // XML       : engineChartType, global, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  engineChartType = class(TRemotable)
  private
    FChartName: string;
    FRows: Array_Of_Rows;
  public
    destructor Destroy; override;
  published
    property ChartName: string         read FChartName write FChartName;
    property Rows:      Array_Of_Rows  Index (IS_UNBD) read FRows write FRows;
  end;



  // ************************************************************************ //
  // XML       : VehicleGroup, global, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  VehicleGroup = class(TRemotable)
  private
    FVehicleGroupId: vehicleGroupIDType;
    FVehicleGroupName: string;
  published
    property VehicleGroupId:   vehicleGroupIDType  read FVehicleGroupId write FVehicleGroupId;
    property VehicleGroupName: string              read FVehicleGroupName write FVehicleGroupName;
  end;



  // ************************************************************************ //
  // XML       : DriverGroup, global, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  DriverGroup = class(TRemotable)
  private
    FDriverGroupId: driverGroupIDType;
    FDriverGroupName: string;
  published
    property DriverGroupId:   driverGroupIDType  read FDriverGroupId write FDriverGroupId;
    property DriverGroupName: string             read FDriverGroupName write FDriverGroupName;
  end;

  yearType        =  type string;      { "http://www.fleetboard.com/data"[GblSmpl] }
  timestampType   =  type string;      { "http://www.fleetboard.com/data"[GblSmpl] }


  // ************************************************************************ //
  // XML       : TPDate, global, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  TPDate = class(TRemotable)
  private
    FBegin_: timestampType;
    FBegin__Specified: boolean;
    FEnd_: timestampType;
    FEnd__Specified: boolean;
    procedure SetBegin_(Index: Integer; const AtimestampType: timestampType);
    function  Begin__Specified(Index: Integer): boolean;
    procedure SetEnd_(Index: Integer; const AtimestampType: timestampType);
    function  End__Specified(Index: Integer): boolean;
  published
    property Begin_: timestampType  Index (IS_OPTN) read FBegin_ write SetBegin_ stored Begin__Specified;
    property End_:   timestampType  Index (IS_OPTN) read FEnd_ write SetEnd_ stored End__Specified;
  end;

  sessionidType   =  type string;      { "http://www.fleetboard.com/data"[GblSmpl] }
  unitType        =  type string;      { "http://www.fleetboard.com/data"[GblSmpl] }


  // ************************************************************************ //
  // XML       : X, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  X = class(TRemotable)
  private
    Fvalue: Double;
    Funit_: unitType;
    Funit__Specified: boolean;
    procedure Setunit_(Index: Integer; const AunitType: unitType);
    function  unit__Specified(Index: Integer): boolean;
  published
    property value: Double    Index (IS_ATTR) read Fvalue write Fvalue;
    property unit_: unitType  Index (IS_ATTR or IS_OPTN) read Funit_ write Setunit_ stored unit__Specified;
  end;



  // ************************************************************************ //
  // XML       : Rows, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  Rows = class(TRemotable)
  private
    Fname_: string;
    Fname__Specified: boolean;
    Fvalue: Double;
    Funit_: unitType;
    Funit__Specified: boolean;
    FCells: Array_Of_Cells;
    procedure Setname_(Index: Integer; const Astring: string);
    function  name__Specified(Index: Integer): boolean;
    procedure Setunit_(Index: Integer; const AunitType: unitType);
    function  unit__Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property name_: string          Index (IS_ATTR or IS_OPTN) read Fname_ write Setname_ stored name__Specified;
    property value: Double          Index (IS_ATTR) read Fvalue write Fvalue;
    property unit_: unitType        Index (IS_ATTR or IS_OPTN) read Funit_ write Setunit_ stored unit__Specified;
    property Cells: Array_Of_Cells  Index (IS_UNBD) read FCells write FCells;
  end;



  // ************************************************************************ //
  // XML       : Y1, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  Y1 = class(TRemotable)
  private
    Fvalue: Double;
    Funit_: unitType;
    Funit__Specified: boolean;
    procedure Setunit_(Index: Integer; const AunitType: unitType);
    function  unit__Specified(Index: Integer): boolean;
  published
    property value: Double    Index (IS_ATTR) read Fvalue write Fvalue;
    property unit_: unitType  Index (IS_ATTR or IS_OPTN) read Funit_ write Setunit_ stored unit__Specified;
  end;



  // ************************************************************************ //
  // XML       : Y2, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  Y2 = class(TRemotable)
  private
    Fvalue: Double;
    Funit_: unitType;
    Funit__Specified: boolean;
    procedure Setunit_(Index: Integer; const AunitType: unitType);
    function  unit__Specified(Index: Integer): boolean;
  published
    property value: Double    Index (IS_ATTR) read Fvalue write Fvalue;
    property unit_: unitType  Index (IS_ATTR or IS_OPTN) read Funit_ write Setunit_ stored unit__Specified;
  end;



  // ************************************************************************ //
  // XML       : Period, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  Period = class(TRemotable)
  private
    FBegin_: timestampType;
    FBegin__Specified: boolean;
    FEnd_: timestampType;
    FEnd__Specified: boolean;
    FPeriod: Period3Type;
    FPeriod_Specified: boolean;
    procedure SetBegin_(Index: Integer; const AtimestampType: timestampType);
    function  Begin__Specified(Index: Integer): boolean;
    procedure SetEnd_(Index: Integer; const AtimestampType: timestampType);
    function  End__Specified(Index: Integer): boolean;
    procedure SetPeriod(Index: Integer; const APeriod3Type: Period3Type);
    function  Period_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property Begin_: timestampType  Index (IS_OPTN) read FBegin_ write SetBegin_ stored Begin__Specified;
    property End_:   timestampType  Index (IS_OPTN) read FEnd_ write SetEnd_ stored End__Specified;
    property Period: Period3Type    Index (IS_OPTN) read FPeriod write SetPeriod stored Period_Specified;
  end;



  // ************************************************************************ //
  // XML       : Period, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  Period2 = class(TRemotable)
  private
    FBegin_: timestampType;
    FBegin__Specified: boolean;
    FEnd_: timestampType;
    FEnd__Specified: boolean;
    FPeriod: Period3Type;
    FPeriod_Specified: boolean;
    procedure SetBegin_(Index: Integer; const AtimestampType: timestampType);
    function  Begin__Specified(Index: Integer): boolean;
    procedure SetEnd_(Index: Integer; const AtimestampType: timestampType);
    function  End__Specified(Index: Integer): boolean;
    procedure SetPeriod(Index: Integer; const APeriod3Type: Period3Type);
    function  Period_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property Begin_: timestampType  Index (IS_OPTN) read FBegin_ write SetBegin_ stored Begin__Specified;
    property End_:   timestampType  Index (IS_OPTN) read FEnd_ write SetEnd_ stored End__Specified;
    property Period: Period3Type    Index (IS_OPTN) read FPeriod write SetPeriod stored Period_Specified;
  end;

  Array_Of_string = array of string;            { "http://www.w3.org/2001/XMLSchema"[GblUbnd] }


  // ************************************************************************ //
  // XML       : Period, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  Period3 = class(TRemotable)
  private
    FBegin_: timestampType;
    FBegin__Specified: boolean;
    FEnd_: timestampType;
    FEnd__Specified: boolean;
    FPeriod: Period3Type;
    FPeriod_Specified: boolean;
    procedure SetBegin_(Index: Integer; const AtimestampType: timestampType);
    function  Begin__Specified(Index: Integer): boolean;
    procedure SetEnd_(Index: Integer; const AtimestampType: timestampType);
    function  End__Specified(Index: Integer): boolean;
    procedure SetPeriod(Index: Integer; const APeriod3Type: Period3Type);
    function  Period_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property Begin_: timestampType  Index (IS_OPTN) read FBegin_ write SetBegin_ stored Begin__Specified;
    property End_:   timestampType  Index (IS_OPTN) read FEnd_ write SetEnd_ stored End__Specified;
    property Period: Period3Type    Index (IS_OPTN) read FPeriod write SetPeriod stored Period_Specified;
  end;



  // ************************************************************************ //
  // XML       : Period, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  Period4 = class(TRemotable)
  private
    FBegin_: timestampType;
    FBegin__Specified: boolean;
    FEnd_: timestampType;
    FEnd__Specified: boolean;
    FPeriod: Period3Type;
    FPeriod_Specified: boolean;
    procedure SetBegin_(Index: Integer; const AtimestampType: timestampType);
    function  Begin__Specified(Index: Integer): boolean;
    procedure SetEnd_(Index: Integer; const AtimestampType: timestampType);
    function  End__Specified(Index: Integer): boolean;
    procedure SetPeriod(Index: Integer; const APeriod3Type: Period3Type);
    function  Period_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property Begin_: timestampType  Index (IS_OPTN) read FBegin_ write SetBegin_ stored Begin__Specified;
    property End_:   timestampType  Index (IS_OPTN) read FEnd_ write SetEnd_ stored End__Specified;
    property Period: Period3Type    Index (IS_OPTN) read FPeriod write SetPeriod stored Period_Specified;
  end;



  // ************************************************************************ //
  // XML       : Period, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  Period5 = class(TRemotable)
  private
    FBegin_: timestampType;
    FBegin__Specified: boolean;
    FEnd_: timestampType;
    FEnd__Specified: boolean;
    FPeriod: Period3Type;
    FPeriod_Specified: boolean;
    procedure SetBegin_(Index: Integer; const AtimestampType: timestampType);
    function  Begin__Specified(Index: Integer): boolean;
    procedure SetEnd_(Index: Integer; const AtimestampType: timestampType);
    function  End__Specified(Index: Integer): boolean;
    procedure SetPeriod(Index: Integer; const APeriod3Type: Period3Type);
    function  Period_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property Begin_: timestampType  Index (IS_OPTN) read FBegin_ write SetBegin_ stored Begin__Specified;
    property End_:   timestampType  Index (IS_OPTN) read FEnd_ write SetEnd_ stored End__Specified;
    property Period: Period3Type    Index (IS_OPTN) read FPeriod write SetPeriod stored Period_Specified;
  end;



  // ************************************************************************ //
  // XML       : Period, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  Period6 = class(TRemotable)
  private
    FBegin_: timestampType;
    FBegin__Specified: boolean;
    FEnd_: timestampType;
    FEnd__Specified: boolean;
    FPeriod: Period3Type;
    FPeriod_Specified: boolean;
    procedure SetBegin_(Index: Integer; const AtimestampType: timestampType);
    function  Begin__Specified(Index: Integer): boolean;
    procedure SetEnd_(Index: Integer; const AtimestampType: timestampType);
    function  End__Specified(Index: Integer): boolean;
    procedure SetPeriod(Index: Integer; const APeriod3Type: Period3Type);
    function  Period_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property Begin_: timestampType  Index (IS_OPTN) read FBegin_ write SetBegin_ stored Begin__Specified;
    property End_:   timestampType  Index (IS_OPTN) read FEnd_ write SetEnd_ stored End__Specified;
    property Period: Period3Type    Index (IS_OPTN) read FPeriod write SetPeriod stored Period_Specified;
  end;



  // ************************************************************************ //
  // XML       : Period, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  Period7 = class(TRemotable)
  private
    FBegin_: timestampType;
    FBegin__Specified: boolean;
    FEnd_: timestampType;
    FEnd__Specified: boolean;
    FPeriod: Period3Type;
    FPeriod_Specified: boolean;
    procedure SetBegin_(Index: Integer; const AtimestampType: timestampType);
    function  Begin__Specified(Index: Integer): boolean;
    procedure SetEnd_(Index: Integer; const AtimestampType: timestampType);
    function  End__Specified(Index: Integer): boolean;
    procedure SetPeriod(Index: Integer; const APeriod3Type: Period3Type);
    function  Period_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property Begin_: timestampType  Index (IS_OPTN) read FBegin_ write SetBegin_ stored Begin__Specified;
    property End_:   timestampType  Index (IS_OPTN) read FEnd_ write SetEnd_ stored End__Specified;
    property Period: Period3Type    Index (IS_OPTN) read FPeriod write SetPeriod stored Period_Specified;
  end;



  // ************************************************************************ //
  // XML       : PeriodBreakDown, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  PeriodBreakDown = class(TRemotable)
  private
    FStartYear: yearType;
    FEndYear: yearType;
    FInterval: period2Type;
  published
    property StartYear: yearType     read FStartYear write FStartYear;
    property EndYear:   yearType     read FEndYear write FEndYear;
    property Interval:  period2Type  read FInterval write FInterval;
  end;



  // ************************************************************************ //
  // XML       : PeriodBreakDown, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  PeriodBreakDown2 = class(TRemotable)
  private
    FStartYear: yearType;
    FEndYear: yearType;
    FInterval: period2Type;
  published
    property StartYear: yearType     read FStartYear write FStartYear;
    property EndYear:   yearType     read FEndYear write FEndYear;
    property Interval:  period2Type  read FInterval write FInterval;
  end;



  // ************************************************************************ //
  // XML       : barChartType, global, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  barChartType = class(TRemotable)
  private
    FChartName: string;
    FValues: Array_Of_Values;
  public
    destructor Destroy; override;
  published
    property ChartName: string           read FChartName write FChartName;
    property Values:    Array_Of_Values  Index (IS_UNBD) read FValues write FValues;
  end;



  // ************************************************************************ //
  // XML       : ChartsType, global, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  ChartsType = class(TRemotable)
  private
    FBarCharts: Array_Of_barChartType;
    FBarCharts_Specified: boolean;
    FEngineOperationCharts: Array_Of_engineChartType;
    FEngineOperationCharts_Specified: boolean;
    procedure SetBarCharts(Index: Integer; const AArray_Of_barChartType: Array_Of_barChartType);
    function  BarCharts_Specified(Index: Integer): boolean;
    procedure SetEngineOperationCharts(Index: Integer; const AArray_Of_engineChartType: Array_Of_engineChartType);
    function  EngineOperationCharts_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property BarCharts:             Array_Of_barChartType     Index (IS_OPTN or IS_UNBD) read FBarCharts write SetBarCharts stored BarCharts_Specified;
    property EngineOperationCharts: Array_Of_engineChartType  Index (IS_OPTN or IS_UNBD) read FEngineOperationCharts write SetEngineOperationCharts stored EngineOperationCharts_Specified;
  end;



  // ************************************************************************ //
  // XML       : Period3Type, global, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  Period3Type = class(TRemotable)
  private
    FTimeRange: TimeRangeType;
    FTimeRange_Specified: boolean;
    FStartYear: yearType;
    FStartYear_Specified: boolean;
    FEndYear: yearType;
    FEndYear_Specified: boolean;
    FInterval: period2Type;
    FInterval_Specified: boolean;
    FYear: string;
    FYear_Specified: boolean;
    FPeriod: period1Type;
    FPeriod_Specified: boolean;
    FCalendarWeek: calendarWeekType;
    FCalendarWeek_Specified: boolean;
    procedure SetTimeRange(Index: Integer; const ATimeRangeType: TimeRangeType);
    function  TimeRange_Specified(Index: Integer): boolean;
    procedure SetStartYear(Index: Integer; const AyearType: yearType);
    function  StartYear_Specified(Index: Integer): boolean;
    procedure SetEndYear(Index: Integer; const AyearType: yearType);
    function  EndYear_Specified(Index: Integer): boolean;
    procedure SetInterval(Index: Integer; const Aperiod2Type: period2Type);
    function  Interval_Specified(Index: Integer): boolean;
    procedure SetYear(Index: Integer; const Astring: string);
    function  Year_Specified(Index: Integer): boolean;
    procedure SetPeriod(Index: Integer; const Aperiod1Type: period1Type);
    function  Period_Specified(Index: Integer): boolean;
    procedure SetCalendarWeek(Index: Integer; const AcalendarWeekType: calendarWeekType);
    function  CalendarWeek_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property TimeRange:    TimeRangeType     Index (IS_OPTN) read FTimeRange write SetTimeRange stored TimeRange_Specified;
    property StartYear:    yearType          Index (IS_OPTN) read FStartYear write SetStartYear stored StartYear_Specified;
    property EndYear:      yearType          Index (IS_OPTN) read FEndYear write SetEndYear stored EndYear_Specified;
    property Interval:     period2Type       Index (IS_OPTN) read FInterval write SetInterval stored Interval_Specified;
    property Year:         string            Index (IS_OPTN) read FYear write SetYear stored Year_Specified;
    property Period:       period1Type       Index (IS_OPTN) read FPeriod write SetPeriod stored Period_Specified;
    property CalendarWeek: calendarWeekType  Index (IS_OPTN) read FCalendarWeek write SetCalendarWeek stored CalendarWeek_Specified;
  end;



  // ************************************************************************ //
  // XML       : PredefinedPeriod, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  PredefinedPeriod = class(TRemotable)
  private
    FYear: yearType;
    FYear_Specified: boolean;
    FInterval: period1Type;
    FInterval_Specified: boolean;
    FCalendarWeek: calendarWeekType;
    FCalendarWeek_Specified: boolean;
    procedure SetYear(Index: Integer; const AyearType: yearType);
    function  Year_Specified(Index: Integer): boolean;
    procedure SetInterval(Index: Integer; const Aperiod1Type: period1Type);
    function  Interval_Specified(Index: Integer): boolean;
    procedure SetCalendarWeek(Index: Integer; const AcalendarWeekType: calendarWeekType);
    function  CalendarWeek_Specified(Index: Integer): boolean;
  published
    property Year:         yearType          Index (IS_OPTN) read FYear write SetYear stored Year_Specified;
    property Interval:     period1Type       Index (IS_OPTN) read FInterval write SetInterval stored Interval_Specified;
    property CalendarWeek: calendarWeekType  Index (IS_OPTN) read FCalendarWeek write SetCalendarWeek stored CalendarWeek_Specified;
  end;



  // ************************************************************************ //
  // XML       : PredefinedPeriod, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  PredefinedPeriod2 = class(TRemotable)
  private
    FYear: yearType;
    FYear_Specified: boolean;
    FInterval: period1Type;
    FInterval_Specified: boolean;
    FCalendarWeek: calendarWeekType;
    FCalendarWeek_Specified: boolean;
    procedure SetYear(Index: Integer; const AyearType: yearType);
    function  Year_Specified(Index: Integer): boolean;
    procedure SetInterval(Index: Integer; const Aperiod1Type: period1Type);
    function  Interval_Specified(Index: Integer): boolean;
    procedure SetCalendarWeek(Index: Integer; const AcalendarWeekType: calendarWeekType);
    function  CalendarWeek_Specified(Index: Integer): boolean;
  published
    property Year:         yearType          Index (IS_OPTN) read FYear write SetYear stored Year_Specified;
    property Interval:     period1Type       Index (IS_OPTN) read FInterval write SetInterval stored Interval_Specified;
    property CalendarWeek: calendarWeekType  Index (IS_OPTN) read FCalendarWeek write SetCalendarWeek stored CalendarWeek_Specified;
  end;



  // ************************************************************************ //
  // XML       : PredefinedPeriod, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  PredefinedPeriod3 = class(TRemotable)
  private
    FYear: yearType;
    FYear_Specified: boolean;
    FInterval: period1Type;
    FInterval_Specified: boolean;
    FCalendarWeek: calendarWeekType;
    FCalendarWeek_Specified: boolean;
    procedure SetYear(Index: Integer; const AyearType: yearType);
    function  Year_Specified(Index: Integer): boolean;
    procedure SetInterval(Index: Integer; const Aperiod1Type: period1Type);
    function  Interval_Specified(Index: Integer): boolean;
    procedure SetCalendarWeek(Index: Integer; const AcalendarWeekType: calendarWeekType);
    function  CalendarWeek_Specified(Index: Integer): boolean;
  published
    property Year:         yearType          Index (IS_OPTN) read FYear write SetYear stored Year_Specified;
    property Interval:     period1Type       Index (IS_OPTN) read FInterval write SetInterval stored Interval_Specified;
    property CalendarWeek: calendarWeekType  Index (IS_OPTN) read FCalendarWeek write SetCalendarWeek stored CalendarWeek_Specified;
  end;

  vehicleidType   =  type Int64;      { "http://www.fleetboard.com/data"[GblSmpl] }
  Array_Of_vehicleidType = array of vehicleidType;   { "http://www.fleetboard.com/data"[GblUbnd] }


  // ************************************************************************ //
  // XML       : VehicleDataSet, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  VehicleDataSet = class(TRemotable)
  private
    FID: Int64;
    FVehicleID: vehicleidType;
    FPeriod: Period5;
    FGrades: Grades5;
    FAvgValues: AvgValues3;
    FConsumption: Consumption2;
    FConsumption_Specified: boolean;
    FPowerTakeOff: PowerTakeOff;
    FPowerTakeOff_Specified: boolean;
    FBrakingDistance: distanceType;
    FBrakingDistance_Specified: boolean;
    FNrOfStops: Int64;
    FNrOfStops_Specified: boolean;
    FHandBrakeUsageCount: Int64;
    FHandBrakeUsageCount_Specified: boolean;
    FMovingTime: Int64;
    FMovingTime_Specified: boolean;
    FStopTimeEngineOn: timeType;
    FStopTimeEngineOn_Specified: boolean;
    FStopTimeEngineOff: timeType;
    FStopTimeEngineOff_Specified: boolean;
    FSpeedOver85PerTotalDistance: speedType;
    FSpeedOver85PerTotalDistance_Specified: boolean;
    FRetarderBrakingDistance: distanceType;
    FRetarderBrakingDistance_Specified: boolean;
    FTotalCoastingDistance: distanceType;
    FTotalCoastingDistance_Specified: boolean;
    FTotalDistance: distanceType;
    FTotalDistance_Specified: boolean;
    FCruiseControlDistance: distanceType;
    FCruiseControlDistance_Specified: boolean;
    FAssistanceSystems: assistanceSystems;
    FAssistanceSystems_Specified: boolean;
    FCharts: ChartsType;
    FCharts_Specified: boolean;
    procedure SetConsumption(Index: Integer; const AConsumption2: Consumption2);
    function  Consumption_Specified(Index: Integer): boolean;
    procedure SetPowerTakeOff(Index: Integer; const APowerTakeOff: PowerTakeOff);
    function  PowerTakeOff_Specified(Index: Integer): boolean;
    procedure SetBrakingDistance(Index: Integer; const AdistanceType: distanceType);
    function  BrakingDistance_Specified(Index: Integer): boolean;
    procedure SetNrOfStops(Index: Integer; const AInt64: Int64);
    function  NrOfStops_Specified(Index: Integer): boolean;
    procedure SetHandBrakeUsageCount(Index: Integer; const AInt64: Int64);
    function  HandBrakeUsageCount_Specified(Index: Integer): boolean;
    procedure SetMovingTime(Index: Integer; const AInt64: Int64);
    function  MovingTime_Specified(Index: Integer): boolean;
    procedure SetStopTimeEngineOn(Index: Integer; const AtimeType: timeType);
    function  StopTimeEngineOn_Specified(Index: Integer): boolean;
    procedure SetStopTimeEngineOff(Index: Integer; const AtimeType: timeType);
    function  StopTimeEngineOff_Specified(Index: Integer): boolean;
    procedure SetSpeedOver85PerTotalDistance(Index: Integer; const AspeedType: speedType);
    function  SpeedOver85PerTotalDistance_Specified(Index: Integer): boolean;
    procedure SetRetarderBrakingDistance(Index: Integer; const AdistanceType: distanceType);
    function  RetarderBrakingDistance_Specified(Index: Integer): boolean;
    procedure SetTotalCoastingDistance(Index: Integer; const AdistanceType: distanceType);
    function  TotalCoastingDistance_Specified(Index: Integer): boolean;
    procedure SetTotalDistance(Index: Integer; const AdistanceType: distanceType);
    function  TotalDistance_Specified(Index: Integer): boolean;
    procedure SetCruiseControlDistance(Index: Integer; const AdistanceType: distanceType);
    function  CruiseControlDistance_Specified(Index: Integer): boolean;
    procedure SetAssistanceSystems(Index: Integer; const AassistanceSystems: assistanceSystems);
    function  AssistanceSystems_Specified(Index: Integer): boolean;
    procedure SetCharts(Index: Integer; const AChartsType: ChartsType);
    function  Charts_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property ID:                          Int64              read FID write FID;
    property VehicleID:                   vehicleidType      read FVehicleID write FVehicleID;
    property Period:                      Period5            Index (IS_NLBL) read FPeriod write FPeriod;
    property Grades:                      Grades5            Index (IS_NLBL) read FGrades write FGrades;
    property AvgValues:                   AvgValues3         Index (IS_NLBL) read FAvgValues write FAvgValues;
    property Consumption:                 Consumption2       Index (IS_OPTN) read FConsumption write SetConsumption stored Consumption_Specified;
    property PowerTakeOff:                PowerTakeOff       Index (IS_OPTN) read FPowerTakeOff write SetPowerTakeOff stored PowerTakeOff_Specified;
    property BrakingDistance:             distanceType       Index (IS_OPTN) read FBrakingDistance write SetBrakingDistance stored BrakingDistance_Specified;
    property NrOfStops:                   Int64              Index (IS_OPTN) read FNrOfStops write SetNrOfStops stored NrOfStops_Specified;
    property HandBrakeUsageCount:         Int64              Index (IS_OPTN) read FHandBrakeUsageCount write SetHandBrakeUsageCount stored HandBrakeUsageCount_Specified;
    property MovingTime:                  Int64              Index (IS_OPTN) read FMovingTime write SetMovingTime stored MovingTime_Specified;
    property StopTimeEngineOn:            timeType           Index (IS_OPTN) read FStopTimeEngineOn write SetStopTimeEngineOn stored StopTimeEngineOn_Specified;
    property StopTimeEngineOff:           timeType           Index (IS_OPTN) read FStopTimeEngineOff write SetStopTimeEngineOff stored StopTimeEngineOff_Specified;
    property SpeedOver85PerTotalDistance: speedType          Index (IS_OPTN) read FSpeedOver85PerTotalDistance write SetSpeedOver85PerTotalDistance stored SpeedOver85PerTotalDistance_Specified;
    property RetarderBrakingDistance:     distanceType       Index (IS_OPTN) read FRetarderBrakingDistance write SetRetarderBrakingDistance stored RetarderBrakingDistance_Specified;
    property TotalCoastingDistance:       distanceType       Index (IS_OPTN) read FTotalCoastingDistance write SetTotalCoastingDistance stored TotalCoastingDistance_Specified;
    property TotalDistance:               distanceType       Index (IS_OPTN) read FTotalDistance write SetTotalDistance stored TotalDistance_Specified;
    property CruiseControlDistance:       distanceType       Index (IS_OPTN) read FCruiseControlDistance write SetCruiseControlDistance stored CruiseControlDistance_Specified;
    property AssistanceSystems:           assistanceSystems  Index (IS_OPTN) read FAssistanceSystems write SetAssistanceSystems stored AssistanceSystems_Specified;
    property Charts:                      ChartsType         Index (IS_OPTN) read FCharts write SetCharts stored Charts_Specified;
  end;



  // ************************************************************************ //
  // XML       : VehicleDataSet, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  VehicleDataSet2 = class(TRemotable)
  private
    FID: Int64;
    FVehicleID: vehicleidType;
    FPeriod: Period7;
    FGrades: Grades7;
    FAvgValues: AvgValues5;
    FConsumption: Consumption5;
    FTotalDistance: distanceType;
    FTotalDistance_Specified: boolean;
    FMovingTime: Int64;
    FMovingTime_Specified: boolean;
    FIdlingTimeEngineOn: timeType;
    FIdlingTimeEngineOn_Specified: boolean;
    FIdlingTimeEngineOff: timeType;
    FIdlingTimeEngineOff_Specified: boolean;
    FPTOTime: timeType;
    FPTOTime_Specified: boolean;
    FOverspeedTime: timeType;
    FOverspeedTime_Specified: boolean;
    FOverrevTime: timeType;
    FOverrevTime_Specified: boolean;
    FOverrevNumber: Int64;
    FOverrevNumber_Specified: boolean;
    FGreenBandRate: SmallInt;
    FGreenBandRate_Specified: boolean;
    FAccelerationNumber: Int64;
    FAccelerationNumber_Specified: boolean;
    FNumberOfStops: Int64;
    FNumberOfStops_Specified: boolean;
    FPTONumber: Int64;
    FPTONumber_Specified: boolean;
    FHarshBrakeNumber: Int64;
    FHarshBrakeNumber_Specified: boolean;
    FCharts: ChartsType;
    FCharts_Specified: boolean;
    procedure SetTotalDistance(Index: Integer; const AdistanceType: distanceType);
    function  TotalDistance_Specified(Index: Integer): boolean;
    procedure SetMovingTime(Index: Integer; const AInt64: Int64);
    function  MovingTime_Specified(Index: Integer): boolean;
    procedure SetIdlingTimeEngineOn(Index: Integer; const AtimeType: timeType);
    function  IdlingTimeEngineOn_Specified(Index: Integer): boolean;
    procedure SetIdlingTimeEngineOff(Index: Integer; const AtimeType: timeType);
    function  IdlingTimeEngineOff_Specified(Index: Integer): boolean;
    procedure SetPTOTime(Index: Integer; const AtimeType: timeType);
    function  PTOTime_Specified(Index: Integer): boolean;
    procedure SetOverspeedTime(Index: Integer; const AtimeType: timeType);
    function  OverspeedTime_Specified(Index: Integer): boolean;
    procedure SetOverrevTime(Index: Integer; const AtimeType: timeType);
    function  OverrevTime_Specified(Index: Integer): boolean;
    procedure SetOverrevNumber(Index: Integer; const AInt64: Int64);
    function  OverrevNumber_Specified(Index: Integer): boolean;
    procedure SetGreenBandRate(Index: Integer; const ASmallInt: SmallInt);
    function  GreenBandRate_Specified(Index: Integer): boolean;
    procedure SetAccelerationNumber(Index: Integer; const AInt64: Int64);
    function  AccelerationNumber_Specified(Index: Integer): boolean;
    procedure SetNumberOfStops(Index: Integer; const AInt64: Int64);
    function  NumberOfStops_Specified(Index: Integer): boolean;
    procedure SetPTONumber(Index: Integer; const AInt64: Int64);
    function  PTONumber_Specified(Index: Integer): boolean;
    procedure SetHarshBrakeNumber(Index: Integer; const AInt64: Int64);
    function  HarshBrakeNumber_Specified(Index: Integer): boolean;
    procedure SetCharts(Index: Integer; const AChartsType: ChartsType);
    function  Charts_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property ID:                  Int64          read FID write FID;
    property VehicleID:           vehicleidType  read FVehicleID write FVehicleID;
    property Period:              Period7        Index (IS_NLBL) read FPeriod write FPeriod;
    property Grades:              Grades7        Index (IS_NLBL) read FGrades write FGrades;
    property AvgValues:           AvgValues5     Index (IS_NLBL) read FAvgValues write FAvgValues;
    property Consumption:         Consumption5   Index (IS_NLBL) read FConsumption write FConsumption;
    property TotalDistance:       distanceType   Index (IS_OPTN) read FTotalDistance write SetTotalDistance stored TotalDistance_Specified;
    property MovingTime:          Int64          Index (IS_OPTN) read FMovingTime write SetMovingTime stored MovingTime_Specified;
    property IdlingTimeEngineOn:  timeType       Index (IS_OPTN) read FIdlingTimeEngineOn write SetIdlingTimeEngineOn stored IdlingTimeEngineOn_Specified;
    property IdlingTimeEngineOff: timeType       Index (IS_OPTN) read FIdlingTimeEngineOff write SetIdlingTimeEngineOff stored IdlingTimeEngineOff_Specified;
    property PTOTime:             timeType       Index (IS_OPTN) read FPTOTime write SetPTOTime stored PTOTime_Specified;
    property OverspeedTime:       timeType       Index (IS_OPTN) read FOverspeedTime write SetOverspeedTime stored OverspeedTime_Specified;
    property OverrevTime:         timeType       Index (IS_OPTN) read FOverrevTime write SetOverrevTime stored OverrevTime_Specified;
    property OverrevNumber:       Int64          Index (IS_OPTN) read FOverrevNumber write SetOverrevNumber stored OverrevNumber_Specified;
    property GreenBandRate:       SmallInt       Index (IS_OPTN) read FGreenBandRate write SetGreenBandRate stored GreenBandRate_Specified;
    property AccelerationNumber:  Int64          Index (IS_OPTN) read FAccelerationNumber write SetAccelerationNumber stored AccelerationNumber_Specified;
    property NumberOfStops:       Int64          Index (IS_OPTN) read FNumberOfStops write SetNumberOfStops stored NumberOfStops_Specified;
    property PTONumber:           Int64          Index (IS_OPTN) read FPTONumber write SetPTONumber stored PTONumber_Specified;
    property HarshBrakeNumber:    Int64          Index (IS_OPTN) read FHarshBrakeNumber write SetHarshBrakeNumber stored HarshBrakeNumber_Specified;
    property Charts:              ChartsType     Index (IS_OPTN) read FCharts write SetCharts stored Charts_Specified;
  end;



  // ************************************************************************ //
  // XML       : PowerTakeOff, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  PowerTakeOff3 = class(TRemotable)
  private
    FPto1: timeType;
    FPto1_Specified: boolean;
    FPto2: timeType;
    FPto2_Specified: boolean;
    FPto3: timeType;
    FPto3_Specified: boolean;
    procedure SetPto1(Index: Integer; const AtimeType: timeType);
    function  Pto1_Specified(Index: Integer): boolean;
    procedure SetPto2(Index: Integer; const AtimeType: timeType);
    function  Pto2_Specified(Index: Integer): boolean;
    procedure SetPto3(Index: Integer; const AtimeType: timeType);
    function  Pto3_Specified(Index: Integer): boolean;
  published
    property Pto1: timeType  Index (IS_OPTN) read FPto1 write SetPto1 stored Pto1_Specified;
    property Pto2: timeType  Index (IS_OPTN) read FPto2 write SetPto2 stored Pto2_Specified;
    property Pto3: timeType  Index (IS_OPTN) read FPto3 write SetPto3 stored Pto3_Specified;
  end;



  // ************************************************************************ //
  // XML       : Consumption, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  Consumption6 = class(TRemotable)
  private
    FDriveConsumption: consumptionType;
    FDriveConsumption_Specified: boolean;
    FTotalConsumption: consumptionType;
    FTotalConsumption_Specified: boolean;
    FIdleConsumption: consumptionType;
    FIdleConsumption_Specified: boolean;
    FAdBlueConsumption: consumptionType;
    FAdBlueConsumption_Specified: boolean;
    FIdleConsumptionPTOActive: consumptionType;
    FIdleConsumptionPTOActive_Specified: boolean;
    FIdleConsumptionPTOInactive: consumptionType;
    FIdleConsumptionPTOInactive_Specified: boolean;
    procedure SetDriveConsumption(Index: Integer; const AconsumptionType: consumptionType);
    function  DriveConsumption_Specified(Index: Integer): boolean;
    procedure SetTotalConsumption(Index: Integer; const AconsumptionType: consumptionType);
    function  TotalConsumption_Specified(Index: Integer): boolean;
    procedure SetIdleConsumption(Index: Integer; const AconsumptionType: consumptionType);
    function  IdleConsumption_Specified(Index: Integer): boolean;
    procedure SetAdBlueConsumption(Index: Integer; const AconsumptionType: consumptionType);
    function  AdBlueConsumption_Specified(Index: Integer): boolean;
    procedure SetIdleConsumptionPTOActive(Index: Integer; const AconsumptionType: consumptionType);
    function  IdleConsumptionPTOActive_Specified(Index: Integer): boolean;
    procedure SetIdleConsumptionPTOInactive(Index: Integer; const AconsumptionType: consumptionType);
    function  IdleConsumptionPTOInactive_Specified(Index: Integer): boolean;
  published
    property DriveConsumption:           consumptionType  Index (IS_OPTN) read FDriveConsumption write SetDriveConsumption stored DriveConsumption_Specified;
    property TotalConsumption:           consumptionType  Index (IS_OPTN) read FTotalConsumption write SetTotalConsumption stored TotalConsumption_Specified;
    property IdleConsumption:            consumptionType  Index (IS_OPTN) read FIdleConsumption write SetIdleConsumption stored IdleConsumption_Specified;
    property AdBlueConsumption:          consumptionType  Index (IS_OPTN) read FAdBlueConsumption write SetAdBlueConsumption stored AdBlueConsumption_Specified;
    property IdleConsumptionPTOActive:   consumptionType  Index (IS_OPTN) read FIdleConsumptionPTOActive write SetIdleConsumptionPTOActive stored IdleConsumptionPTOActive_Specified;
    property IdleConsumptionPTOInactive: consumptionType  Index (IS_OPTN) read FIdleConsumptionPTOInactive write SetIdleConsumptionPTOInactive stored IdleConsumptionPTOInactive_Specified;
  end;



  // ************************************************************************ //
  // XML       : PeriodBreakDown, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  PeriodBreakDown3 = class(TRemotable)
  private
    FStartYear: yearType;
    FEndYear: yearType;
    FInterval: period2Type;
  published
    property StartYear: yearType     read FStartYear write FStartYear;
    property EndYear:   yearType     read FEndYear write FEndYear;
    property Interval:  period2Type  read FInterval write FInterval;
  end;



  // ************************************************************************ //
  // XML       : ToursSummary, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  ToursSummary3 = class(TRemotable)
  private
    FTimeRange: TimeRangeType;
  public
    destructor Destroy; override;
  published
    property TimeRange: TimeRangeType  read FTimeRange write FTimeRange;
  end;



  // ************************************************************************ //
  // XML       : AvgValues, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  AvgValues6 = class(TRemotable)
  private
    FWeight: weightType;
    FWeight_Specified: boolean;
    FSpeed: speedType;
    FSpeed_Specified: boolean;
    FOverallConsumption: consumptionType;
    FOverallConsumption_Specified: boolean;
    FDriveConsumption: consumptionType;
    FDriveConsumption_Specified: boolean;
    FAdBlueConsumption: consumptionType;
    FAdBlueConsumption_Specified: boolean;
    FAdBlueToFuelRatioConsumption: consumptionType;
    FAdBlueToFuelRatioConsumption_Specified: boolean;
    procedure SetWeight(Index: Integer; const AweightType: weightType);
    function  Weight_Specified(Index: Integer): boolean;
    procedure SetSpeed(Index: Integer; const AspeedType: speedType);
    function  Speed_Specified(Index: Integer): boolean;
    procedure SetOverallConsumption(Index: Integer; const AconsumptionType: consumptionType);
    function  OverallConsumption_Specified(Index: Integer): boolean;
    procedure SetDriveConsumption(Index: Integer; const AconsumptionType: consumptionType);
    function  DriveConsumption_Specified(Index: Integer): boolean;
    procedure SetAdBlueConsumption(Index: Integer; const AconsumptionType: consumptionType);
    function  AdBlueConsumption_Specified(Index: Integer): boolean;
    procedure SetAdBlueToFuelRatioConsumption(Index: Integer; const AconsumptionType: consumptionType);
    function  AdBlueToFuelRatioConsumption_Specified(Index: Integer): boolean;
  published
    property Weight:                       weightType       Index (IS_OPTN) read FWeight write SetWeight stored Weight_Specified;
    property Speed:                        speedType        Index (IS_OPTN) read FSpeed write SetSpeed stored Speed_Specified;
    property OverallConsumption:           consumptionType  Index (IS_OPTN) read FOverallConsumption write SetOverallConsumption stored OverallConsumption_Specified;
    property DriveConsumption:             consumptionType  Index (IS_OPTN) read FDriveConsumption write SetDriveConsumption stored DriveConsumption_Specified;
    property AdBlueConsumption:            consumptionType  Index (IS_OPTN) read FAdBlueConsumption write SetAdBlueConsumption stored AdBlueConsumption_Specified;
    property AdBlueToFuelRatioConsumption: consumptionType  Index (IS_OPTN) read FAdBlueToFuelRatioConsumption write SetAdBlueToFuelRatioConsumption stored AdBlueToFuelRatioConsumption_Specified;
  end;



  // ************************************************************************ //
  // XML       : PredefinedPeriod, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  PredefinedPeriod4 = class(TRemotable)
  private
    FYear: yearType;
    FInterval: period1Type;
    FInterval_Specified: boolean;
    FCalendarWeek: calendarWeekType;
    FCalendarWeek_Specified: boolean;
    procedure SetInterval(Index: Integer; const Aperiod1Type: period1Type);
    function  Interval_Specified(Index: Integer): boolean;
    procedure SetCalendarWeek(Index: Integer; const AcalendarWeekType: calendarWeekType);
    function  CalendarWeek_Specified(Index: Integer): boolean;
  published
    property Year:         yearType          read FYear write FYear;
    property Interval:     period1Type       Index (IS_OPTN) read FInterval write SetInterval stored Interval_Specified;
    property CalendarWeek: calendarWeekType  Index (IS_OPTN) read FCalendarWeek write SetCalendarWeek stored CalendarWeek_Specified;
  end;



  // ************************************************************************ //
  // XML       : PeriodBreakDown, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  PeriodBreakDown4 = class(TRemotable)
  private
    FStartYear: yearType;
    FEndYear: yearType;
    FInterval: period2Type;
  published
    property StartYear: yearType     read FStartYear write FStartYear;
    property EndYear:   yearType     read FEndYear write FEndYear;
    property Interval:  period2Type  read FInterval write FInterval;
  end;



  // ************************************************************************ //
  // XML       : Grades, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  Grades8 = class(TRemotable)
  private
    FDrivingStyle: gradeType;
    FDrivingStyle_Specified: boolean;
    FDrivingStyleCons: gradeType;
    FDrivingStyleCons_Specified: boolean;
    FDrivingStyleConsPreventive: gradeType;
    FDrivingStyleConsPreventive_Specified: boolean;
    FDrivingStyleConsEngineOperationMN: gradeType;
    FDrivingStyleConsEngineOperationMN_Specified: boolean;
    FDrivingStyleConsPedalMovements: gradeType;
    FDrivingStyleConsPedalMovements_Specified: boolean;
    FDrivingStyleConsUniformSpeed: gradeType;
    FDrivingStyleConsUniformSpeed_Specified: boolean;
    FDrivingStyleConsStops: gradeType;
    FDrivingStyleConsStops_Specified: boolean;
    FDrivingStyleBrake: gradeType;
    FDrivingStyleBrake_Specified: boolean;
    FDrivingStyleBrakePreventive: gradeType;
    FDrivingStyleBrakePreventive_Specified: boolean;
    FDrivingStyleBrakeDeceleration: gradeType;
    FDrivingStyleBrakeDeceleration_Specified: boolean;
    FDegreeOfDifficulty: gradeType;
    FDegreeOfDifficulty_Specified: boolean;
    FDegreeOfDifficultyAverageSlope: gradeType;
    FDegreeOfDifficultyAverageSlope_Specified: boolean;
    FDegreeOfDifficultyWeight: gradeType;
    FDegreeOfDifficultyWeight_Specified: boolean;
    FDegreeOfDifficultyStops: gradeType;
    FDegreeOfDifficultyStops_Specified: boolean;
    FTorqueClassification: gradeType;
    FTorqueClassification_Specified: boolean;
    procedure SetDrivingStyle(Index: Integer; const AgradeType: gradeType);
    function  DrivingStyle_Specified(Index: Integer): boolean;
    procedure SetDrivingStyleCons(Index: Integer; const AgradeType: gradeType);
    function  DrivingStyleCons_Specified(Index: Integer): boolean;
    procedure SetDrivingStyleConsPreventive(Index: Integer; const AgradeType: gradeType);
    function  DrivingStyleConsPreventive_Specified(Index: Integer): boolean;
    procedure SetDrivingStyleConsEngineOperationMN(Index: Integer; const AgradeType: gradeType);
    function  DrivingStyleConsEngineOperationMN_Specified(Index: Integer): boolean;
    procedure SetDrivingStyleConsPedalMovements(Index: Integer; const AgradeType: gradeType);
    function  DrivingStyleConsPedalMovements_Specified(Index: Integer): boolean;
    procedure SetDrivingStyleConsUniformSpeed(Index: Integer; const AgradeType: gradeType);
    function  DrivingStyleConsUniformSpeed_Specified(Index: Integer): boolean;
    procedure SetDrivingStyleConsStops(Index: Integer; const AgradeType: gradeType);
    function  DrivingStyleConsStops_Specified(Index: Integer): boolean;
    procedure SetDrivingStyleBrake(Index: Integer; const AgradeType: gradeType);
    function  DrivingStyleBrake_Specified(Index: Integer): boolean;
    procedure SetDrivingStyleBrakePreventive(Index: Integer; const AgradeType: gradeType);
    function  DrivingStyleBrakePreventive_Specified(Index: Integer): boolean;
    procedure SetDrivingStyleBrakeDeceleration(Index: Integer; const AgradeType: gradeType);
    function  DrivingStyleBrakeDeceleration_Specified(Index: Integer): boolean;
    procedure SetDegreeOfDifficulty(Index: Integer; const AgradeType: gradeType);
    function  DegreeOfDifficulty_Specified(Index: Integer): boolean;
    procedure SetDegreeOfDifficultyAverageSlope(Index: Integer; const AgradeType: gradeType);
    function  DegreeOfDifficultyAverageSlope_Specified(Index: Integer): boolean;
    procedure SetDegreeOfDifficultyWeight(Index: Integer; const AgradeType: gradeType);
    function  DegreeOfDifficultyWeight_Specified(Index: Integer): boolean;
    procedure SetDegreeOfDifficultyStops(Index: Integer; const AgradeType: gradeType);
    function  DegreeOfDifficultyStops_Specified(Index: Integer): boolean;
    procedure SetTorqueClassification(Index: Integer; const AgradeType: gradeType);
    function  TorqueClassification_Specified(Index: Integer): boolean;
  published
    property DrivingStyle:                      gradeType  Index (IS_OPTN) read FDrivingStyle write SetDrivingStyle stored DrivingStyle_Specified;
    property DrivingStyleCons:                  gradeType  Index (IS_OPTN) read FDrivingStyleCons write SetDrivingStyleCons stored DrivingStyleCons_Specified;
    property DrivingStyleConsPreventive:        gradeType  Index (IS_OPTN) read FDrivingStyleConsPreventive write SetDrivingStyleConsPreventive stored DrivingStyleConsPreventive_Specified;
    property DrivingStyleConsEngineOperationMN: gradeType  Index (IS_OPTN) read FDrivingStyleConsEngineOperationMN write SetDrivingStyleConsEngineOperationMN stored DrivingStyleConsEngineOperationMN_Specified;
    property DrivingStyleConsPedalMovements:    gradeType  Index (IS_OPTN) read FDrivingStyleConsPedalMovements write SetDrivingStyleConsPedalMovements stored DrivingStyleConsPedalMovements_Specified;
    property DrivingStyleConsUniformSpeed:      gradeType  Index (IS_OPTN) read FDrivingStyleConsUniformSpeed write SetDrivingStyleConsUniformSpeed stored DrivingStyleConsUniformSpeed_Specified;
    property DrivingStyleConsStops:             gradeType  Index (IS_OPTN) read FDrivingStyleConsStops write SetDrivingStyleConsStops stored DrivingStyleConsStops_Specified;
    property DrivingStyleBrake:                 gradeType  Index (IS_OPTN) read FDrivingStyleBrake write SetDrivingStyleBrake stored DrivingStyleBrake_Specified;
    property DrivingStyleBrakePreventive:       gradeType  Index (IS_OPTN) read FDrivingStyleBrakePreventive write SetDrivingStyleBrakePreventive stored DrivingStyleBrakePreventive_Specified;
    property DrivingStyleBrakeDeceleration:     gradeType  Index (IS_OPTN) read FDrivingStyleBrakeDeceleration write SetDrivingStyleBrakeDeceleration stored DrivingStyleBrakeDeceleration_Specified;
    property DegreeOfDifficulty:                gradeType  Index (IS_OPTN) read FDegreeOfDifficulty write SetDegreeOfDifficulty stored DegreeOfDifficulty_Specified;
    property DegreeOfDifficultyAverageSlope:    gradeType  Index (IS_OPTN) read FDegreeOfDifficultyAverageSlope write SetDegreeOfDifficultyAverageSlope stored DegreeOfDifficultyAverageSlope_Specified;
    property DegreeOfDifficultyWeight:          gradeType  Index (IS_OPTN) read FDegreeOfDifficultyWeight write SetDegreeOfDifficultyWeight stored DegreeOfDifficultyWeight_Specified;
    property DegreeOfDifficultyStops:           gradeType  Index (IS_OPTN) read FDegreeOfDifficultyStops write SetDegreeOfDifficultyStops stored DegreeOfDifficultyStops_Specified;
    property TorqueClassification:              gradeType  Index (IS_OPTN) read FTorqueClassification write SetTorqueClassification stored TorqueClassification_Specified;
  end;



  // ************************************************************************ //
  // XML       : Period, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  Period8 = class(TRemotable)
  private
    FBegin_: timestampType;
    FBegin__Specified: boolean;
    FEnd_: timestampType;
    FEnd__Specified: boolean;
    FPeriod: Period3Type;
    FPeriod_Specified: boolean;
    procedure SetBegin_(Index: Integer; const AtimestampType: timestampType);
    function  Begin__Specified(Index: Integer): boolean;
    procedure SetEnd_(Index: Integer; const AtimestampType: timestampType);
    function  End__Specified(Index: Integer): boolean;
    procedure SetPeriod(Index: Integer; const APeriod3Type: Period3Type);
    function  Period_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property Begin_: timestampType  Index (IS_OPTN) read FBegin_ write SetBegin_ stored Begin__Specified;
    property End_:   timestampType  Index (IS_OPTN) read FEnd_ write SetEnd_ stored End__Specified;
    property Period: Period3Type    Index (IS_OPTN) read FPeriod write SetPeriod stored Period_Specified;
  end;



  // ************************************************************************ //
  // XML       : PredefinedPeriod, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  PredefinedPeriod5 = class(TRemotable)
  private
    FYear: yearType;
    FYear_Specified: boolean;
    FInterval: period1Type;
    FInterval_Specified: boolean;
    FCalendarWeek: calendarWeekType;
    FCalendarWeek_Specified: boolean;
    procedure SetYear(Index: Integer; const AyearType: yearType);
    function  Year_Specified(Index: Integer): boolean;
    procedure SetInterval(Index: Integer; const Aperiod1Type: period1Type);
    function  Interval_Specified(Index: Integer): boolean;
    procedure SetCalendarWeek(Index: Integer; const AcalendarWeekType: calendarWeekType);
    function  CalendarWeek_Specified(Index: Integer): boolean;
  published
    property Year:         yearType          Index (IS_OPTN) read FYear write SetYear stored Year_Specified;
    property Interval:     period1Type       Index (IS_OPTN) read FInterval write SetInterval stored Interval_Specified;
    property CalendarWeek: calendarWeekType  Index (IS_OPTN) read FCalendarWeek write SetCalendarWeek stored CalendarWeek_Specified;
  end;



  // ************************************************************************ //
  // XML       : UniversalPerformanceAnalysisVehicleOverviewReport, global, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  UniversalPerformanceAnalysisVehicleOverviewReport = class(TRemotable)
  private
    FElementId: Int64;
    FElementId_Specified: boolean;
    FVehicleId: vehicleidType;
    FVehicleId_Specified: boolean;
    FVehicleGroups: Array_Of_VehicleGroup;
    FVehicleGroups_Specified: boolean;
    FVehicleName: string;
    FVehicleName_Specified: boolean;
    FPeriod: Period;
    FGrades: Grades;
    FGrades_Specified: boolean;
    FAverageTotalConsumption: consumptionType;
    FAverageTotalConsumption_Specified: boolean;
    FTotalDistance: distanceType;
    FTotalDistance_Specified: boolean;
    FNumberOfVehicles: Integer;
    FNumberOfVehicles_Specified: boolean;
    procedure SetElementId(Index: Integer; const AInt64: Int64);
    function  ElementId_Specified(Index: Integer): boolean;
    procedure SetVehicleId(Index: Integer; const AvehicleidType: vehicleidType);
    function  VehicleId_Specified(Index: Integer): boolean;
    procedure SetVehicleGroups(Index: Integer; const AArray_Of_VehicleGroup: Array_Of_VehicleGroup);
    function  VehicleGroups_Specified(Index: Integer): boolean;
    procedure SetVehicleName(Index: Integer; const Astring: string);
    function  VehicleName_Specified(Index: Integer): boolean;
    procedure SetGrades(Index: Integer; const AGrades: Grades);
    function  Grades_Specified(Index: Integer): boolean;
    procedure SetAverageTotalConsumption(Index: Integer; const AconsumptionType: consumptionType);
    function  AverageTotalConsumption_Specified(Index: Integer): boolean;
    procedure SetTotalDistance(Index: Integer; const AdistanceType: distanceType);
    function  TotalDistance_Specified(Index: Integer): boolean;
    procedure SetNumberOfVehicles(Index: Integer; const AInteger: Integer);
    function  NumberOfVehicles_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property ElementId:               Int64                  Index (IS_OPTN) read FElementId write SetElementId stored ElementId_Specified;
    property VehicleId:               vehicleidType          Index (IS_OPTN) read FVehicleId write SetVehicleId stored VehicleId_Specified;
    property VehicleGroups:           Array_Of_VehicleGroup  Index (IS_OPTN or IS_UNBD) read FVehicleGroups write SetVehicleGroups stored VehicleGroups_Specified;
    property VehicleName:             string                 Index (IS_OPTN) read FVehicleName write SetVehicleName stored VehicleName_Specified;
    property Period:                  Period                 read FPeriod write FPeriod;
    property Grades:                  Grades                 Index (IS_OPTN) read FGrades write SetGrades stored Grades_Specified;
    property AverageTotalConsumption: consumptionType        Index (IS_OPTN) read FAverageTotalConsumption write SetAverageTotalConsumption stored AverageTotalConsumption_Specified;
    property TotalDistance:           distanceType           Index (IS_OPTN) read FTotalDistance write SetTotalDistance stored TotalDistance_Specified;
    property NumberOfVehicles:        Integer                Index (IS_OPTN) read FNumberOfVehicles write SetNumberOfVehicles stored NumberOfVehicles_Specified;
  end;



  // ************************************************************************ //
  // XML       : PeriodBreakDown, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  PeriodBreakDown5 = class(TRemotable)
  private
    FStartYear: yearType;
    FEndYear: yearType;
    FInterval: period2Type;
  published
    property StartYear: yearType     read FStartYear write FStartYear;
    property EndYear:   yearType     read FEndYear write FEndYear;
    property Interval:  period2Type  read FInterval write FInterval;
  end;



  // ************************************************************************ //
  // XML       : ToursSummary, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  ToursSummary4 = class(TRemotable)
  private
    FTimeRange: TimeRangeType;
  public
    destructor Destroy; override;
  published
    property TimeRange: TimeRangeType  read FTimeRange write FTimeRange;
  end;



  // ************************************************************************ //
  // XML       : PredefinedPeriod, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  PredefinedPeriod6 = class(TRemotable)
  private
    FYear: yearType;
    FYear_Specified: boolean;
    FInterval: period1Type;
    FInterval_Specified: boolean;
    FCalendarWeek: calendarWeekType;
    FCalendarWeek_Specified: boolean;
    procedure SetYear(Index: Integer; const AyearType: yearType);
    function  Year_Specified(Index: Integer): boolean;
    procedure SetInterval(Index: Integer; const Aperiod1Type: period1Type);
    function  Interval_Specified(Index: Integer): boolean;
    procedure SetCalendarWeek(Index: Integer; const AcalendarWeekType: calendarWeekType);
    function  CalendarWeek_Specified(Index: Integer): boolean;
  published
    property Year:         yearType          Index (IS_OPTN) read FYear write SetYear stored Year_Specified;
    property Interval:     period1Type       Index (IS_OPTN) read FInterval write SetInterval stored Interval_Specified;
    property CalendarWeek: calendarWeekType  Index (IS_OPTN) read FCalendarWeek write SetCalendarWeek stored CalendarWeek_Specified;
  end;



  // ************************************************************************ //
  // XML       : GetUniversalPerformanceAnalysisVehicleOverviewRequestType, global, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  GetUniversalPerformanceAnalysisVehicleOverviewRequestType = class(TRemotable)
  private
    Fsessionid: sessionidType;
    Fsessionid_Specified: boolean;
    Flimit: limitType;
    Flimit_Specified: boolean;
    Foffset: offsetType;
    Foffset_Specified: boolean;
    Fversion: versionType;
    Fversion_Specified: boolean;
    FVehicleId: vehicleidType;
    FVehicleId_Specified: boolean;
    FVehicleGroupId: vehicleGroupIDType;
    FVehicleGroupId_Specified: boolean;
    FAnalysis: Analysis;
    FDrivingStyle: MinMaxGradeType;
    FDrivingStyle_Specified: boolean;
    FIdleTimeGrading: MinMaxGradeType;
    FIdleTimeGrading_Specified: boolean;
    FOverrevNumberGrading: MinMaxGradeType;
    FOverrevNumberGrading_Specified: boolean;
    FOverrevTimeGrading: MinMaxGradeType;
    FOverrevTimeGrading_Specified: boolean;
    FEcoDrivingGrading: MinMaxGradeType;
    FEcoDrivingGrading_Specified: boolean;
    FHarshBrakeGrading: MinMaxGradeType;
    FHarshBrakeGrading_Specified: boolean;
    FOverspeedTimeGrading: MinMaxGradeType;
    FOverspeedTimeGrading_Specified: boolean;
    FAvgTotalConsumption: MinMaxConsumptionType;
    FAvgTotalConsumption_Specified: boolean;
    procedure Setsessionid(Index: Integer; const AsessionidType: sessionidType);
    function  sessionid_Specified(Index: Integer): boolean;
    procedure Setlimit(Index: Integer; const AlimitType: limitType);
    function  limit_Specified(Index: Integer): boolean;
    procedure Setoffset(Index: Integer; const AoffsetType: offsetType);
    function  offset_Specified(Index: Integer): boolean;
    procedure Setversion(Index: Integer; const AversionType: versionType);
    function  version_Specified(Index: Integer): boolean;
    procedure SetVehicleId(Index: Integer; const AvehicleidType: vehicleidType);
    function  VehicleId_Specified(Index: Integer): boolean;
    procedure SetVehicleGroupId(Index: Integer; const AvehicleGroupIDType: vehicleGroupIDType);
    function  VehicleGroupId_Specified(Index: Integer): boolean;
    procedure SetDrivingStyle(Index: Integer; const AMinMaxGradeType: MinMaxGradeType);
    function  DrivingStyle_Specified(Index: Integer): boolean;
    procedure SetIdleTimeGrading(Index: Integer; const AMinMaxGradeType: MinMaxGradeType);
    function  IdleTimeGrading_Specified(Index: Integer): boolean;
    procedure SetOverrevNumberGrading(Index: Integer; const AMinMaxGradeType: MinMaxGradeType);
    function  OverrevNumberGrading_Specified(Index: Integer): boolean;
    procedure SetOverrevTimeGrading(Index: Integer; const AMinMaxGradeType: MinMaxGradeType);
    function  OverrevTimeGrading_Specified(Index: Integer): boolean;
    procedure SetEcoDrivingGrading(Index: Integer; const AMinMaxGradeType: MinMaxGradeType);
    function  EcoDrivingGrading_Specified(Index: Integer): boolean;
    procedure SetHarshBrakeGrading(Index: Integer; const AMinMaxGradeType: MinMaxGradeType);
    function  HarshBrakeGrading_Specified(Index: Integer): boolean;
    procedure SetOverspeedTimeGrading(Index: Integer; const AMinMaxGradeType: MinMaxGradeType);
    function  OverspeedTimeGrading_Specified(Index: Integer): boolean;
    procedure SetAvgTotalConsumption(Index: Integer; const AMinMaxConsumptionType: MinMaxConsumptionType);
    function  AvgTotalConsumption_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property sessionid:            sessionidType          Index (IS_ATTR or IS_OPTN) read Fsessionid write Setsessionid stored sessionid_Specified;
    property limit:                limitType              Index (IS_ATTR or IS_OPTN) read Flimit write Setlimit stored limit_Specified;
    property offset:               offsetType             Index (IS_ATTR or IS_OPTN) read Foffset write Setoffset stored offset_Specified;
    property version:              versionType            Index (IS_ATTR or IS_OPTN) read Fversion write Setversion stored version_Specified;
    property VehicleId:            vehicleidType          Index (IS_OPTN) read FVehicleId write SetVehicleId stored VehicleId_Specified;
    property VehicleGroupId:       vehicleGroupIDType     Index (IS_OPTN) read FVehicleGroupId write SetVehicleGroupId stored VehicleGroupId_Specified;
    property Analysis:             Analysis               read FAnalysis write FAnalysis;
    property DrivingStyle:         MinMaxGradeType        Index (IS_OPTN) read FDrivingStyle write SetDrivingStyle stored DrivingStyle_Specified;
    property IdleTimeGrading:      MinMaxGradeType        Index (IS_OPTN) read FIdleTimeGrading write SetIdleTimeGrading stored IdleTimeGrading_Specified;
    property OverrevNumberGrading: MinMaxGradeType        Index (IS_OPTN) read FOverrevNumberGrading write SetOverrevNumberGrading stored OverrevNumberGrading_Specified;
    property OverrevTimeGrading:   MinMaxGradeType        Index (IS_OPTN) read FOverrevTimeGrading write SetOverrevTimeGrading stored OverrevTimeGrading_Specified;
    property EcoDrivingGrading:    MinMaxGradeType        Index (IS_OPTN) read FEcoDrivingGrading write SetEcoDrivingGrading stored EcoDrivingGrading_Specified;
    property HarshBrakeGrading:    MinMaxGradeType        Index (IS_OPTN) read FHarshBrakeGrading write SetHarshBrakeGrading stored HarshBrakeGrading_Specified;
    property OverspeedTimeGrading: MinMaxGradeType        Index (IS_OPTN) read FOverspeedTimeGrading write SetOverspeedTimeGrading stored OverspeedTimeGrading_Specified;
    property AvgTotalConsumption:  MinMaxConsumptionType  Index (IS_OPTN) read FAvgTotalConsumption write SetAvgTotalConsumption stored AvgTotalConsumption_Specified;
  end;



  // ************************************************************************ //
  // XML       : GetUniversalPerformanceAnalysisVehicleOverviewRequest, global, <element>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  GetUniversalPerformanceAnalysisVehicleOverviewRequest = class(GetUniversalPerformanceAnalysisVehicleOverviewRequestType)
  private
  published
  end;



  // ************************************************************************ //
  // XML       : PeriodBreakDown, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  PeriodBreakDown6 = class(TRemotable)
  private
    FStartYear: yearType;
    FEndYear: yearType;
    FInterval: period2Type;
  published
    property StartYear: yearType     read FStartYear write FStartYear;
    property EndYear:   yearType     read FEndYear write FEndYear;
    property Interval:  period2Type  read FInterval write FInterval;
  end;



  // ************************************************************************ //
  // XML       : ToursSummary, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  ToursSummary5 = class(TRemotable)
  private
    FTimeRange: TimeRangeType;
  public
    destructor Destroy; override;
  published
    property TimeRange: TimeRangeType  read FTimeRange write FTimeRange;
  end;



  // ************************************************************************ //
  // XML       : ToursSummary, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  ToursSummary6 = class(TRemotable)
  private
    FTimeRange: TimeRangeType;
    FTimeRange_Specified: boolean;
    procedure SetTimeRange(Index: Integer; const ATimeRangeType: TimeRangeType);
    function  TimeRange_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property TimeRange: TimeRangeType  Index (IS_OPTN) read FTimeRange write SetTimeRange stored TimeRange_Specified;
  end;



  // ************************************************************************ //
  // XML       : MBPerformanceAnalysisVehicleOverviewReport, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  MBPerformanceAnalysisVehicleOverviewReport = class(TRemotable)
  private
    FElementId: Int64;
    FVehicleID: vehicleidType;
    FPeriod: Period9;
    FGrades: Grades9;
    FAvgValues: AvgValues7;
    FConsumption: ConsumptionValuesType;
    FConsumption_Specified: boolean;
    FTotalDistance: distanceType;
    FTotalDistance_Specified: boolean;
    FVehicleStateTimes: VehicleStateTimeValuesType;
    FVehicleStateTimes_Specified: boolean;
    FNumberOfVehicles: Integer;
    FNumberOfVehicles_Specified: boolean;
    procedure SetConsumption(Index: Integer; const AConsumptionValuesType: ConsumptionValuesType);
    function  Consumption_Specified(Index: Integer): boolean;
    procedure SetTotalDistance(Index: Integer; const AdistanceType: distanceType);
    function  TotalDistance_Specified(Index: Integer): boolean;
    procedure SetVehicleStateTimes(Index: Integer; const AVehicleStateTimeValuesType: VehicleStateTimeValuesType);
    function  VehicleStateTimes_Specified(Index: Integer): boolean;
    procedure SetNumberOfVehicles(Index: Integer; const AInteger: Integer);
    function  NumberOfVehicles_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property ElementId:         Int64                       read FElementId write FElementId;
    property VehicleID:         vehicleidType               read FVehicleID write FVehicleID;
    property Period:            Period9                     Index (IS_NLBL) read FPeriod write FPeriod;
    property Grades:            Grades9                     Index (IS_NLBL) read FGrades write FGrades;
    property AvgValues:         AvgValues7                  Index (IS_NLBL) read FAvgValues write FAvgValues;
    property Consumption:       ConsumptionValuesType       Index (IS_OPTN) read FConsumption write SetConsumption stored Consumption_Specified;
    property TotalDistance:     distanceType                Index (IS_OPTN) read FTotalDistance write SetTotalDistance stored TotalDistance_Specified;
    property VehicleStateTimes: VehicleStateTimeValuesType  Index (IS_OPTN) read FVehicleStateTimes write SetVehicleStateTimes stored VehicleStateTimes_Specified;
    property NumberOfVehicles:  Integer                     Index (IS_OPTN) read FNumberOfVehicles write SetNumberOfVehicles stored NumberOfVehicles_Specified;
  end;

  responseSizeType =  type Int64;      { "http://www.fleetboard.com/data"[GblSmpl] }
  resultSizeType  =  type Int64;      { "http://www.fleetboard.com/data"[GblSmpl] }


  // ************************************************************************ //
  // XML       : GetUniversalPerformanceAnalysisDriverOverviewResponseType, global, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  GetUniversalPerformanceAnalysisDriverOverviewResponseType = class(TRemotable)
  private
    Flimit: limitType;
    Flimit_Specified: boolean;
    Foffset: offsetType;
    Foffset_Specified: boolean;
    FresultSize: resultSizeType;
    FresultSize_Specified: boolean;
    FresponseSize: responseSizeType;
    FresponseSize_Specified: boolean;
    FUniversalPerformanceAnalysisDriverOverviewReport: Array_Of_UniversalPerformanceAnalysisDriverOverviewReport;
    FUniversalPerformanceAnalysisDriverOverviewReport_Specified: boolean;
    procedure Setlimit(Index: Integer; const AlimitType: limitType);
    function  limit_Specified(Index: Integer): boolean;
    procedure Setoffset(Index: Integer; const AoffsetType: offsetType);
    function  offset_Specified(Index: Integer): boolean;
    procedure SetresultSize(Index: Integer; const AresultSizeType: resultSizeType);
    function  resultSize_Specified(Index: Integer): boolean;
    procedure SetresponseSize(Index: Integer; const AresponseSizeType: responseSizeType);
    function  responseSize_Specified(Index: Integer): boolean;
    procedure SetUniversalPerformanceAnalysisDriverOverviewReport(Index: Integer; const AArray_Of_UniversalPerformanceAnalysisDriverOverviewReport: Array_Of_UniversalPerformanceAnalysisDriverOverviewReport);
    function  UniversalPerformanceAnalysisDriverOverviewReport_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property limit:                                            limitType                                                  Index (IS_ATTR or IS_OPTN) read Flimit write Setlimit stored limit_Specified;
    property offset:                                           offsetType                                                 Index (IS_ATTR or IS_OPTN) read Foffset write Setoffset stored offset_Specified;
    property resultSize:                                       resultSizeType                                             Index (IS_ATTR or IS_OPTN) read FresultSize write SetresultSize stored resultSize_Specified;
    property responseSize:                                     responseSizeType                                           Index (IS_ATTR or IS_OPTN) read FresponseSize write SetresponseSize stored responseSize_Specified;
    property UniversalPerformanceAnalysisDriverOverviewReport: Array_Of_UniversalPerformanceAnalysisDriverOverviewReport  Index (IS_OPTN or IS_UNBD) read FUniversalPerformanceAnalysisDriverOverviewReport write SetUniversalPerformanceAnalysisDriverOverviewReport stored UniversalPerformanceAnalysisDriverOverviewReport_Specified;
  end;



  // ************************************************************************ //
  // XML       : GetUniversalPerformanceAnalysisDriverOverviewResponse, global, <element>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  GetUniversalPerformanceAnalysisDriverOverviewResponse = class(GetUniversalPerformanceAnalysisDriverOverviewResponseType)
  private
  published
  end;



  // ************************************************************************ //
  // XML       : GetUniversalPerformanceAnalysisVehicleOverviewResponseType, global, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  GetUniversalPerformanceAnalysisVehicleOverviewResponseType = class(TRemotable)
  private
    Flimit: limitType;
    Flimit_Specified: boolean;
    Foffset: offsetType;
    Foffset_Specified: boolean;
    FresultSize: resultSizeType;
    FresultSize_Specified: boolean;
    FresponseSize: responseSizeType;
    FresponseSize_Specified: boolean;
    FUniversalPerformanceAnalysisVehicleOverviewReport: Array_Of_UniversalPerformanceAnalysisVehicleOverviewReport;
    FUniversalPerformanceAnalysisVehicleOverviewReport_Specified: boolean;
    procedure Setlimit(Index: Integer; const AlimitType: limitType);
    function  limit_Specified(Index: Integer): boolean;
    procedure Setoffset(Index: Integer; const AoffsetType: offsetType);
    function  offset_Specified(Index: Integer): boolean;
    procedure SetresultSize(Index: Integer; const AresultSizeType: resultSizeType);
    function  resultSize_Specified(Index: Integer): boolean;
    procedure SetresponseSize(Index: Integer; const AresponseSizeType: responseSizeType);
    function  responseSize_Specified(Index: Integer): boolean;
    procedure SetUniversalPerformanceAnalysisVehicleOverviewReport(Index: Integer; const AArray_Of_UniversalPerformanceAnalysisVehicleOverviewReport: Array_Of_UniversalPerformanceAnalysisVehicleOverviewReport);
    function  UniversalPerformanceAnalysisVehicleOverviewReport_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property limit:                                             limitType                                                   Index (IS_ATTR or IS_OPTN) read Flimit write Setlimit stored limit_Specified;
    property offset:                                            offsetType                                                  Index (IS_ATTR or IS_OPTN) read Foffset write Setoffset stored offset_Specified;
    property resultSize:                                        resultSizeType                                              Index (IS_ATTR or IS_OPTN) read FresultSize write SetresultSize stored resultSize_Specified;
    property responseSize:                                      responseSizeType                                            Index (IS_ATTR or IS_OPTN) read FresponseSize write SetresponseSize stored responseSize_Specified;
    property UniversalPerformanceAnalysisVehicleOverviewReport: Array_Of_UniversalPerformanceAnalysisVehicleOverviewReport  Index (IS_OPTN or IS_UNBD) read FUniversalPerformanceAnalysisVehicleOverviewReport write SetUniversalPerformanceAnalysisVehicleOverviewReport stored UniversalPerformanceAnalysisVehicleOverviewReport_Specified;
  end;



  // ************************************************************************ //
  // XML       : GetUniversalPerformanceAnalysisVehicleOverviewResponse, global, <element>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  GetUniversalPerformanceAnalysisVehicleOverviewResponse = class(GetUniversalPerformanceAnalysisVehicleOverviewResponseType)
  private
  published
  end;



  // ************************************************************************ //
  // XML       : GetMBPerformanceAnalysisVehicleResponseType, global, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  GetMBPerformanceAnalysisVehicleResponseType = class(TRemotable)
  private
    Flimit: limitType;
    Flimit_Specified: boolean;
    Foffset: offsetType;
    Foffset_Specified: boolean;
    FresultSize: resultSizeType;
    FresultSize_Specified: boolean;
    FresponseSize: responseSizeType;
    FresponseSize_Specified: boolean;
    Fversion: versionType;
    Fversion_Specified: boolean;
    FMBPerformanceAnalysisVehicleReport: Array_Of_MBPerformanceAnalysisVehicleReport;
    FMBPerformanceAnalysisVehicleReport_Specified: boolean;
    procedure Setlimit(Index: Integer; const AlimitType: limitType);
    function  limit_Specified(Index: Integer): boolean;
    procedure Setoffset(Index: Integer; const AoffsetType: offsetType);
    function  offset_Specified(Index: Integer): boolean;
    procedure SetresultSize(Index: Integer; const AresultSizeType: resultSizeType);
    function  resultSize_Specified(Index: Integer): boolean;
    procedure SetresponseSize(Index: Integer; const AresponseSizeType: responseSizeType);
    function  responseSize_Specified(Index: Integer): boolean;
    procedure Setversion(Index: Integer; const AversionType: versionType);
    function  version_Specified(Index: Integer): boolean;
    procedure SetMBPerformanceAnalysisVehicleReport(Index: Integer; const AArray_Of_MBPerformanceAnalysisVehicleReport: Array_Of_MBPerformanceAnalysisVehicleReport);
    function  MBPerformanceAnalysisVehicleReport_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property limit:                              limitType                                    Index (IS_ATTR or IS_OPTN) read Flimit write Setlimit stored limit_Specified;
    property offset:                             offsetType                                   Index (IS_ATTR or IS_OPTN) read Foffset write Setoffset stored offset_Specified;
    property resultSize:                         resultSizeType                               Index (IS_ATTR or IS_OPTN) read FresultSize write SetresultSize stored resultSize_Specified;
    property responseSize:                       responseSizeType                             Index (IS_ATTR or IS_OPTN) read FresponseSize write SetresponseSize stored responseSize_Specified;
    property version:                            versionType                                  Index (IS_ATTR or IS_OPTN) read Fversion write Setversion stored version_Specified;
    property MBPerformanceAnalysisVehicleReport: Array_Of_MBPerformanceAnalysisVehicleReport  Index (IS_OPTN or IS_UNBD) read FMBPerformanceAnalysisVehicleReport write SetMBPerformanceAnalysisVehicleReport stored MBPerformanceAnalysisVehicleReport_Specified;
  end;



  // ************************************************************************ //
  // XML       : GetMBPerformanceAnalysisVehicleResponse, global, <element>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  GetMBPerformanceAnalysisVehicleResponse = class(GetMBPerformanceAnalysisVehicleResponseType)
  private
  published
  end;



  // ************************************************************************ //
  // XML       : GetUniversalPerformanceAnalysisVehicleResponseType, global, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  GetUniversalPerformanceAnalysisVehicleResponseType = class(TRemotable)
  private
    Flimit: limitType;
    Flimit_Specified: boolean;
    Foffset: offsetType;
    Foffset_Specified: boolean;
    FresultSize: resultSizeType;
    FresultSize_Specified: boolean;
    FresponseSize: responseSizeType;
    FresponseSize_Specified: boolean;
    FUniversalPerformanceAnalysisVehicleReport: Array_Of_UniversalPerformanceAnalysisVehicleReport;
    FUniversalPerformanceAnalysisVehicleReport_Specified: boolean;
    procedure Setlimit(Index: Integer; const AlimitType: limitType);
    function  limit_Specified(Index: Integer): boolean;
    procedure Setoffset(Index: Integer; const AoffsetType: offsetType);
    function  offset_Specified(Index: Integer): boolean;
    procedure SetresultSize(Index: Integer; const AresultSizeType: resultSizeType);
    function  resultSize_Specified(Index: Integer): boolean;
    procedure SetresponseSize(Index: Integer; const AresponseSizeType: responseSizeType);
    function  responseSize_Specified(Index: Integer): boolean;
    procedure SetUniversalPerformanceAnalysisVehicleReport(Index: Integer; const AArray_Of_UniversalPerformanceAnalysisVehicleReport: Array_Of_UniversalPerformanceAnalysisVehicleReport);
    function  UniversalPerformanceAnalysisVehicleReport_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property limit:                                     limitType                                           Index (IS_ATTR or IS_OPTN) read Flimit write Setlimit stored limit_Specified;
    property offset:                                    offsetType                                          Index (IS_ATTR or IS_OPTN) read Foffset write Setoffset stored offset_Specified;
    property resultSize:                                resultSizeType                                      Index (IS_ATTR or IS_OPTN) read FresultSize write SetresultSize stored resultSize_Specified;
    property responseSize:                              responseSizeType                                    Index (IS_ATTR or IS_OPTN) read FresponseSize write SetresponseSize stored responseSize_Specified;
    property UniversalPerformanceAnalysisVehicleReport: Array_Of_UniversalPerformanceAnalysisVehicleReport  Index (IS_OPTN or IS_UNBD) read FUniversalPerformanceAnalysisVehicleReport write SetUniversalPerformanceAnalysisVehicleReport stored UniversalPerformanceAnalysisVehicleReport_Specified;
  end;



  // ************************************************************************ //
  // XML       : GetUniversalPerformanceAnalysisVehicleResponse, global, <element>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  GetUniversalPerformanceAnalysisVehicleResponse = class(GetUniversalPerformanceAnalysisVehicleResponseType)
  private
  published
  end;



  // ************************************************************************ //
  // XML       : GetMBPerformanceAnalysisDriverResponseType, global, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  GetMBPerformanceAnalysisDriverResponseType = class(TRemotable)
  private
    Flimit: limitType;
    Flimit_Specified: boolean;
    Foffset: offsetType;
    Foffset_Specified: boolean;
    FresultSize: resultSizeType;
    FresultSize_Specified: boolean;
    FresponseSize: responseSizeType;
    FresponseSize_Specified: boolean;
    Fversion: versionType;
    Fversion_Specified: boolean;
    FMBPerformanceAnalysisDriverReport: Array_Of_MBPerformanceAnalysisDriverReport;
    FMBPerformanceAnalysisDriverReport_Specified: boolean;
    procedure Setlimit(Index: Integer; const AlimitType: limitType);
    function  limit_Specified(Index: Integer): boolean;
    procedure Setoffset(Index: Integer; const AoffsetType: offsetType);
    function  offset_Specified(Index: Integer): boolean;
    procedure SetresultSize(Index: Integer; const AresultSizeType: resultSizeType);
    function  resultSize_Specified(Index: Integer): boolean;
    procedure SetresponseSize(Index: Integer; const AresponseSizeType: responseSizeType);
    function  responseSize_Specified(Index: Integer): boolean;
    procedure Setversion(Index: Integer; const AversionType: versionType);
    function  version_Specified(Index: Integer): boolean;
    procedure SetMBPerformanceAnalysisDriverReport(Index: Integer; const AArray_Of_MBPerformanceAnalysisDriverReport: Array_Of_MBPerformanceAnalysisDriverReport);
    function  MBPerformanceAnalysisDriverReport_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property limit:                             limitType                                   Index (IS_ATTR or IS_OPTN) read Flimit write Setlimit stored limit_Specified;
    property offset:                            offsetType                                  Index (IS_ATTR or IS_OPTN) read Foffset write Setoffset stored offset_Specified;
    property resultSize:                        resultSizeType                              Index (IS_ATTR or IS_OPTN) read FresultSize write SetresultSize stored resultSize_Specified;
    property responseSize:                      responseSizeType                            Index (IS_ATTR or IS_OPTN) read FresponseSize write SetresponseSize stored responseSize_Specified;
    property version:                           versionType                                 Index (IS_ATTR or IS_OPTN) read Fversion write Setversion stored version_Specified;
    property MBPerformanceAnalysisDriverReport: Array_Of_MBPerformanceAnalysisDriverReport  Index (IS_OPTN or IS_UNBD) read FMBPerformanceAnalysisDriverReport write SetMBPerformanceAnalysisDriverReport stored MBPerformanceAnalysisDriverReport_Specified;
  end;



  // ************************************************************************ //
  // XML       : GetMBPerformanceAnalysisDriverResponse, global, <element>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  GetMBPerformanceAnalysisDriverResponse = class(GetMBPerformanceAnalysisDriverResponseType)
  private
  published
  end;



  // ************************************************************************ //
  // XML       : GetUniversalPerformanceAnalysisDriverResponseType, global, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  GetUniversalPerformanceAnalysisDriverResponseType = class(TRemotable)
  private
    Flimit: limitType;
    Flimit_Specified: boolean;
    Foffset: offsetType;
    Foffset_Specified: boolean;
    FresultSize: resultSizeType;
    FresultSize_Specified: boolean;
    FresponseSize: responseSizeType;
    FresponseSize_Specified: boolean;
    FUniversalPerformanceAnalysisDriverReport: Array_Of_UniversalPerformanceAnalysisDriverReport;
    FUniversalPerformanceAnalysisDriverReport_Specified: boolean;
    procedure Setlimit(Index: Integer; const AlimitType: limitType);
    function  limit_Specified(Index: Integer): boolean;
    procedure Setoffset(Index: Integer; const AoffsetType: offsetType);
    function  offset_Specified(Index: Integer): boolean;
    procedure SetresultSize(Index: Integer; const AresultSizeType: resultSizeType);
    function  resultSize_Specified(Index: Integer): boolean;
    procedure SetresponseSize(Index: Integer; const AresponseSizeType: responseSizeType);
    function  responseSize_Specified(Index: Integer): boolean;
    procedure SetUniversalPerformanceAnalysisDriverReport(Index: Integer; const AArray_Of_UniversalPerformanceAnalysisDriverReport: Array_Of_UniversalPerformanceAnalysisDriverReport);
    function  UniversalPerformanceAnalysisDriverReport_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property limit:                                    limitType                                          Index (IS_ATTR or IS_OPTN) read Flimit write Setlimit stored limit_Specified;
    property offset:                                   offsetType                                         Index (IS_ATTR or IS_OPTN) read Foffset write Setoffset stored offset_Specified;
    property resultSize:                               resultSizeType                                     Index (IS_ATTR or IS_OPTN) read FresultSize write SetresultSize stored resultSize_Specified;
    property responseSize:                             responseSizeType                                   Index (IS_ATTR or IS_OPTN) read FresponseSize write SetresponseSize stored responseSize_Specified;
    property UniversalPerformanceAnalysisDriverReport: Array_Of_UniversalPerformanceAnalysisDriverReport  Index (IS_OPTN or IS_UNBD) read FUniversalPerformanceAnalysisDriverReport write SetUniversalPerformanceAnalysisDriverReport stored UniversalPerformanceAnalysisDriverReport_Specified;
  end;



  // ************************************************************************ //
  // XML       : GetUniversalPerformanceAnalysisDriverResponse, global, <element>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  GetUniversalPerformanceAnalysisDriverResponse = class(GetUniversalPerformanceAnalysisDriverResponseType)
  private
  published
  end;



  // ************************************************************************ //
  // XML       : AvgValues, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  AvgValues7 = class(TRemotable)
  private
    FWeight: weightType;
    FWeight_Specified: boolean;
    FSpeed: speedType;
    FSpeed_Specified: boolean;
    FOverallConsumption: consumptionType;
    FOverallConsumption_Specified: boolean;
    FDriveConsumption: consumptionType;
    FDriveConsumption_Specified: boolean;
    procedure SetWeight(Index: Integer; const AweightType: weightType);
    function  Weight_Specified(Index: Integer): boolean;
    procedure SetSpeed(Index: Integer; const AspeedType: speedType);
    function  Speed_Specified(Index: Integer): boolean;
    procedure SetOverallConsumption(Index: Integer; const AconsumptionType: consumptionType);
    function  OverallConsumption_Specified(Index: Integer): boolean;
    procedure SetDriveConsumption(Index: Integer; const AconsumptionType: consumptionType);
    function  DriveConsumption_Specified(Index: Integer): boolean;
  published
    property Weight:             weightType       Index (IS_OPTN) read FWeight write SetWeight stored Weight_Specified;
    property Speed:              speedType        Index (IS_OPTN) read FSpeed write SetSpeed stored Speed_Specified;
    property OverallConsumption: consumptionType  Index (IS_OPTN) read FOverallConsumption write SetOverallConsumption stored OverallConsumption_Specified;
    property DriveConsumption:   consumptionType  Index (IS_OPTN) read FDriveConsumption write SetDriveConsumption stored DriveConsumption_Specified;
  end;



  // ************************************************************************ //
  // XML       : Grades, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  Grades9 = class(TRemotable)
  private
    FDrivingStyle: gradeType;
    FDrivingStyle_Specified: boolean;
    FDegreeOfDifficulty: gradeType;
    FDegreeOfDifficulty_Specified: boolean;
    procedure SetDrivingStyle(Index: Integer; const AgradeType: gradeType);
    function  DrivingStyle_Specified(Index: Integer): boolean;
    procedure SetDegreeOfDifficulty(Index: Integer; const AgradeType: gradeType);
    function  DegreeOfDifficulty_Specified(Index: Integer): boolean;
  published
    property DrivingStyle:       gradeType  Index (IS_OPTN) read FDrivingStyle write SetDrivingStyle stored DrivingStyle_Specified;
    property DegreeOfDifficulty: gradeType  Index (IS_OPTN) read FDegreeOfDifficulty write SetDegreeOfDifficulty stored DegreeOfDifficulty_Specified;
  end;



  // ************************************************************************ //
  // XML       : Period, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  Period9 = class(TRemotable)
  private
    FBegin_: timestampType;
    FBegin__Specified: boolean;
    FEnd_: timestampType;
    FEnd__Specified: boolean;
    FPeriod: Period3Type;
    FPeriod_Specified: boolean;
    procedure SetBegin_(Index: Integer; const AtimestampType: timestampType);
    function  Begin__Specified(Index: Integer): boolean;
    procedure SetEnd_(Index: Integer; const AtimestampType: timestampType);
    function  End__Specified(Index: Integer): boolean;
    procedure SetPeriod(Index: Integer; const APeriod3Type: Period3Type);
    function  Period_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property Begin_: timestampType  Index (IS_OPTN) read FBegin_ write SetBegin_ stored Begin__Specified;
    property End_:   timestampType  Index (IS_OPTN) read FEnd_ write SetEnd_ stored End__Specified;
    property Period: Period3Type    Index (IS_OPTN) read FPeriod write SetPeriod stored Period_Specified;
  end;



  // ************************************************************************ //
  // XML       : GetMBPerformanceAnalysisVehicleOverviewResponseType, global, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  GetMBPerformanceAnalysisVehicleOverviewResponseType = class(TRemotable)
  private
    Flimit: limitType;
    Flimit_Specified: boolean;
    Foffset: offsetType;
    Foffset_Specified: boolean;
    FresultSize: resultSizeType;
    FresultSize_Specified: boolean;
    FresponseSize: responseSizeType;
    FresponseSize_Specified: boolean;
    Fversion: versionType;
    Fversion_Specified: boolean;
    FMBPerformanceAnalysisVehicleOverviewReport: Array_Of_MBPerformanceAnalysisVehicleOverviewReport;
    FMBPerformanceAnalysisVehicleOverviewReport_Specified: boolean;
    procedure Setlimit(Index: Integer; const AlimitType: limitType);
    function  limit_Specified(Index: Integer): boolean;
    procedure Setoffset(Index: Integer; const AoffsetType: offsetType);
    function  offset_Specified(Index: Integer): boolean;
    procedure SetresultSize(Index: Integer; const AresultSizeType: resultSizeType);
    function  resultSize_Specified(Index: Integer): boolean;
    procedure SetresponseSize(Index: Integer; const AresponseSizeType: responseSizeType);
    function  responseSize_Specified(Index: Integer): boolean;
    procedure Setversion(Index: Integer; const AversionType: versionType);
    function  version_Specified(Index: Integer): boolean;
    procedure SetMBPerformanceAnalysisVehicleOverviewReport(Index: Integer; const AArray_Of_MBPerformanceAnalysisVehicleOverviewReport: Array_Of_MBPerformanceAnalysisVehicleOverviewReport);
    function  MBPerformanceAnalysisVehicleOverviewReport_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property limit:                                      limitType                                            Index (IS_ATTR or IS_OPTN) read Flimit write Setlimit stored limit_Specified;
    property offset:                                     offsetType                                           Index (IS_ATTR or IS_OPTN) read Foffset write Setoffset stored offset_Specified;
    property resultSize:                                 resultSizeType                                       Index (IS_ATTR or IS_OPTN) read FresultSize write SetresultSize stored resultSize_Specified;
    property responseSize:                               responseSizeType                                     Index (IS_ATTR or IS_OPTN) read FresponseSize write SetresponseSize stored responseSize_Specified;
    property version:                                    versionType                                          Index (IS_ATTR or IS_OPTN) read Fversion write Setversion stored version_Specified;
    property MBPerformanceAnalysisVehicleOverviewReport: Array_Of_MBPerformanceAnalysisVehicleOverviewReport  Index (IS_OPTN or IS_UNBD) read FMBPerformanceAnalysisVehicleOverviewReport write SetMBPerformanceAnalysisVehicleOverviewReport stored MBPerformanceAnalysisVehicleOverviewReport_Specified;
  end;



  // ************************************************************************ //
  // XML       : GetMBPerformanceAnalysisVehicleOverviewResponse, global, <element>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  GetMBPerformanceAnalysisVehicleOverviewResponse = class(GetMBPerformanceAnalysisVehicleOverviewResponseType)
  private
  published
  end;



  // ************************************************************************ //
  // XML       : Analysis, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  Analysis = class(TRemotable)
  private
    FAnalysisKind: analysisModelType;
    FToursSummary: ToursSummary5;
    FToursSummary_Specified: boolean;
    FPeriodBreakDown: PeriodBreakDown6;
    FPeriodBreakDown_Specified: boolean;
    FPredefinedPeriod: PredefinedPeriod6;
    FPredefinedPeriod_Specified: boolean;
    procedure SetToursSummary(Index: Integer; const AToursSummary5: ToursSummary5);
    function  ToursSummary_Specified(Index: Integer): boolean;
    procedure SetPeriodBreakDown(Index: Integer; const APeriodBreakDown6: PeriodBreakDown6);
    function  PeriodBreakDown_Specified(Index: Integer): boolean;
    procedure SetPredefinedPeriod(Index: Integer; const APredefinedPeriod6: PredefinedPeriod6);
    function  PredefinedPeriod_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property AnalysisKind:     analysisModelType  read FAnalysisKind write FAnalysisKind;
    property ToursSummary:     ToursSummary5      Index (IS_OPTN) read FToursSummary write SetToursSummary stored ToursSummary_Specified;
    property PeriodBreakDown:  PeriodBreakDown6   Index (IS_OPTN) read FPeriodBreakDown write SetPeriodBreakDown stored PeriodBreakDown_Specified;
    property PredefinedPeriod: PredefinedPeriod6  Index (IS_OPTN) read FPredefinedPeriod write SetPredefinedPeriod stored PredefinedPeriod_Specified;
  end;



  // ************************************************************************ //
  // XML       : Analysis, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  Analysis2 = class(TRemotable)
  private
    FAnalysisKind: analysisModelType;
    FToursSummary: ToursSummary4;
    FToursSummary_Specified: boolean;
    FPeriodBreakDown: PeriodBreakDown5;
    FPeriodBreakDown_Specified: boolean;
    FPredefinedPeriod: PredefinedPeriod2;
    FPredefinedPeriod_Specified: boolean;
    procedure SetToursSummary(Index: Integer; const AToursSummary4: ToursSummary4);
    function  ToursSummary_Specified(Index: Integer): boolean;
    procedure SetPeriodBreakDown(Index: Integer; const APeriodBreakDown5: PeriodBreakDown5);
    function  PeriodBreakDown_Specified(Index: Integer): boolean;
    procedure SetPredefinedPeriod(Index: Integer; const APredefinedPeriod2: PredefinedPeriod2);
    function  PredefinedPeriod_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property AnalysisKind:     analysisModelType  read FAnalysisKind write FAnalysisKind;
    property ToursSummary:     ToursSummary4      Index (IS_OPTN) read FToursSummary write SetToursSummary stored ToursSummary_Specified;
    property PeriodBreakDown:  PeriodBreakDown5   Index (IS_OPTN) read FPeriodBreakDown write SetPeriodBreakDown stored PeriodBreakDown_Specified;
    property PredefinedPeriod: PredefinedPeriod2  Index (IS_OPTN) read FPredefinedPeriod write SetPredefinedPeriod stored PredefinedPeriod_Specified;
  end;



  // ************************************************************************ //
  // XML       : Analysis, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  Analysis3 = class(TRemotable)
  private
    FAnalysisKind: analysisModelType;
    FToursSummary: ToursSummary3;
    FToursSummary_Specified: boolean;
    FPeriodBreakDown: PeriodBreakDown3;
    FPeriodBreakDown_Specified: boolean;
    FPredefinedPeriod: PredefinedPeriod5;
    FPredefinedPeriod_Specified: boolean;
    procedure SetToursSummary(Index: Integer; const AToursSummary3: ToursSummary3);
    function  ToursSummary_Specified(Index: Integer): boolean;
    procedure SetPeriodBreakDown(Index: Integer; const APeriodBreakDown3: PeriodBreakDown3);
    function  PeriodBreakDown_Specified(Index: Integer): boolean;
    procedure SetPredefinedPeriod(Index: Integer; const APredefinedPeriod5: PredefinedPeriod5);
    function  PredefinedPeriod_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property AnalysisKind:     analysisModelType  read FAnalysisKind write FAnalysisKind;
    property ToursSummary:     ToursSummary3      Index (IS_OPTN) read FToursSummary write SetToursSummary stored ToursSummary_Specified;
    property PeriodBreakDown:  PeriodBreakDown3   Index (IS_OPTN) read FPeriodBreakDown write SetPeriodBreakDown stored PeriodBreakDown_Specified;
    property PredefinedPeriod: PredefinedPeriod5  Index (IS_OPTN) read FPredefinedPeriod write SetPredefinedPeriod stored PredefinedPeriod_Specified;
  end;



  // ************************************************************************ //
  // XML       : Analysis, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  Analysis4 = class(TRemotable)
  private
    FAnalysisKind: analysisModelType;
    FToursSummary: ToursSummary2;
    FToursSummary_Specified: boolean;
    FPeriodBreakDown: PeriodBreakDown;
    FPeriodBreakDown_Specified: boolean;
    FPredefinedPeriod: PredefinedPeriod;
    FPredefinedPeriod_Specified: boolean;
    procedure SetToursSummary(Index: Integer; const AToursSummary2: ToursSummary2);
    function  ToursSummary_Specified(Index: Integer): boolean;
    procedure SetPeriodBreakDown(Index: Integer; const APeriodBreakDown: PeriodBreakDown);
    function  PeriodBreakDown_Specified(Index: Integer): boolean;
    procedure SetPredefinedPeriod(Index: Integer; const APredefinedPeriod: PredefinedPeriod);
    function  PredefinedPeriod_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property AnalysisKind:     analysisModelType  read FAnalysisKind write FAnalysisKind;
    property ToursSummary:     ToursSummary2      Index (IS_OPTN) read FToursSummary write SetToursSummary stored ToursSummary_Specified;
    property PeriodBreakDown:  PeriodBreakDown    Index (IS_OPTN) read FPeriodBreakDown write SetPeriodBreakDown stored PeriodBreakDown_Specified;
    property PredefinedPeriod: PredefinedPeriod   Index (IS_OPTN) read FPredefinedPeriod write SetPredefinedPeriod stored PredefinedPeriod_Specified;
  end;



  // ************************************************************************ //
  // XML       : Analysis, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  Analysis5 = class(TRemotable)
  private
    FAnalysisKind: analysisModelType;
    FToursSummary: ToursSummary;
    FToursSummary_Specified: boolean;
    FPeriodBreakDown: PeriodBreakDown2;
    FPeriodBreakDown_Specified: boolean;
    FPredefinedPeriod: PredefinedPeriod3;
    FPredefinedPeriod_Specified: boolean;
    procedure SetToursSummary(Index: Integer; const AToursSummary: ToursSummary);
    function  ToursSummary_Specified(Index: Integer): boolean;
    procedure SetPeriodBreakDown(Index: Integer; const APeriodBreakDown2: PeriodBreakDown2);
    function  PeriodBreakDown_Specified(Index: Integer): boolean;
    procedure SetPredefinedPeriod(Index: Integer; const APredefinedPeriod3: PredefinedPeriod3);
    function  PredefinedPeriod_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property AnalysisKind:     analysisModelType  read FAnalysisKind write FAnalysisKind;
    property ToursSummary:     ToursSummary       Index (IS_OPTN) read FToursSummary write SetToursSummary stored ToursSummary_Specified;
    property PeriodBreakDown:  PeriodBreakDown2   Index (IS_OPTN) read FPeriodBreakDown write SetPeriodBreakDown stored PeriodBreakDown_Specified;
    property PredefinedPeriod: PredefinedPeriod3  Index (IS_OPTN) read FPredefinedPeriod write SetPredefinedPeriod stored PredefinedPeriod_Specified;
  end;



  // ************************************************************************ //
  // XML       : Analysis, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  Analysis6 = class(TRemotable)
  private
    FAnalysisKind: analysisModelType;
    FToursSummary: ToursSummary7;
    FToursSummary_Specified: boolean;
    FPeriodBreakDown: PeriodBreakDown7;
    FPeriodBreakDown_Specified: boolean;
    FPredefinedPeriod: PredefinedPeriod7;
    FPredefinedPeriod_Specified: boolean;
    procedure SetToursSummary(Index: Integer; const AToursSummary7: ToursSummary7);
    function  ToursSummary_Specified(Index: Integer): boolean;
    procedure SetPeriodBreakDown(Index: Integer; const APeriodBreakDown7: PeriodBreakDown7);
    function  PeriodBreakDown_Specified(Index: Integer): boolean;
    procedure SetPredefinedPeriod(Index: Integer; const APredefinedPeriod7: PredefinedPeriod7);
    function  PredefinedPeriod_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property AnalysisKind:     analysisModelType  read FAnalysisKind write FAnalysisKind;
    property ToursSummary:     ToursSummary7      Index (IS_OPTN) read FToursSummary write SetToursSummary stored ToursSummary_Specified;
    property PeriodBreakDown:  PeriodBreakDown7   Index (IS_OPTN) read FPeriodBreakDown write SetPeriodBreakDown stored PeriodBreakDown_Specified;
    property PredefinedPeriod: PredefinedPeriod7  Index (IS_OPTN) read FPredefinedPeriod write SetPredefinedPeriod stored PredefinedPeriod_Specified;
  end;



  // ************************************************************************ //
  // XML       : GetMBPerformanceAnalysisVehicleOverviewRequestType, global, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  GetMBPerformanceAnalysisVehicleOverviewRequestType = class(TRemotable)
  private
    Fsessionid: sessionidType;
    Fsessionid_Specified: boolean;
    Flimit: limitType;
    Flimit_Specified: boolean;
    Foffset: offsetType;
    Foffset_Specified: boolean;
    Fversion: versionType;
    Fversion_Specified: boolean;
    FVehicleID: vehicleidType;
    FVehicleID_Specified: boolean;
    FVehicleGroupID: vehicleGroupIDType;
    FVehicleGroupID_Specified: boolean;
    FTelematicGroupID: telematicGroupIDType;
    FTelematicGroupID_Specified: boolean;
    FAnalysis: Analysis6;
    FDrivingStyle: MinMaxGradeType;
    FDrivingStyle_Specified: boolean;
    FDegreeOfDifficulty: MinMaxGradeType;
    FDegreeOfDifficulty_Specified: boolean;
    FAvgTruckWeight: MinMaxWeightType;
    FAvgTruckWeight_Specified: boolean;
    FAvgSpeed: MinMaxSpeedType;
    FAvgSpeed_Specified: boolean;
    FAvgOverallConsumption: MinMaxConsumptionType;
    FAvgOverallConsumption_Specified: boolean;
    procedure Setsessionid(Index: Integer; const AsessionidType: sessionidType);
    function  sessionid_Specified(Index: Integer): boolean;
    procedure Setlimit(Index: Integer; const AlimitType: limitType);
    function  limit_Specified(Index: Integer): boolean;
    procedure Setoffset(Index: Integer; const AoffsetType: offsetType);
    function  offset_Specified(Index: Integer): boolean;
    procedure Setversion(Index: Integer; const AversionType: versionType);
    function  version_Specified(Index: Integer): boolean;
    procedure SetVehicleID(Index: Integer; const AvehicleidType: vehicleidType);
    function  VehicleID_Specified(Index: Integer): boolean;
    procedure SetVehicleGroupID(Index: Integer; const AvehicleGroupIDType: vehicleGroupIDType);
    function  VehicleGroupID_Specified(Index: Integer): boolean;
    procedure SetTelematicGroupID(Index: Integer; const AtelematicGroupIDType: telematicGroupIDType);
    function  TelematicGroupID_Specified(Index: Integer): boolean;
    procedure SetDrivingStyle(Index: Integer; const AMinMaxGradeType: MinMaxGradeType);
    function  DrivingStyle_Specified(Index: Integer): boolean;
    procedure SetDegreeOfDifficulty(Index: Integer; const AMinMaxGradeType: MinMaxGradeType);
    function  DegreeOfDifficulty_Specified(Index: Integer): boolean;
    procedure SetAvgTruckWeight(Index: Integer; const AMinMaxWeightType: MinMaxWeightType);
    function  AvgTruckWeight_Specified(Index: Integer): boolean;
    procedure SetAvgSpeed(Index: Integer; const AMinMaxSpeedType: MinMaxSpeedType);
    function  AvgSpeed_Specified(Index: Integer): boolean;
    procedure SetAvgOverallConsumption(Index: Integer; const AMinMaxConsumptionType: MinMaxConsumptionType);
    function  AvgOverallConsumption_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property sessionid:             sessionidType          Index (IS_ATTR or IS_OPTN) read Fsessionid write Setsessionid stored sessionid_Specified;
    property limit:                 limitType              Index (IS_ATTR or IS_OPTN) read Flimit write Setlimit stored limit_Specified;
    property offset:                offsetType             Index (IS_ATTR or IS_OPTN) read Foffset write Setoffset stored offset_Specified;
    property version:               versionType            Index (IS_ATTR or IS_OPTN) read Fversion write Setversion stored version_Specified;
    property VehicleID:             vehicleidType          Index (IS_OPTN) read FVehicleID write SetVehicleID stored VehicleID_Specified;
    property VehicleGroupID:        vehicleGroupIDType     Index (IS_OPTN) read FVehicleGroupID write SetVehicleGroupID stored VehicleGroupID_Specified;
    property TelematicGroupID:      telematicGroupIDType   Index (IS_OPTN) read FTelematicGroupID write SetTelematicGroupID stored TelematicGroupID_Specified;
    property Analysis:              Analysis6              read FAnalysis write FAnalysis;
    property DrivingStyle:          MinMaxGradeType        Index (IS_OPTN) read FDrivingStyle write SetDrivingStyle stored DrivingStyle_Specified;
    property DegreeOfDifficulty:    MinMaxGradeType        Index (IS_OPTN) read FDegreeOfDifficulty write SetDegreeOfDifficulty stored DegreeOfDifficulty_Specified;
    property AvgTruckWeight:        MinMaxWeightType       Index (IS_OPTN) read FAvgTruckWeight write SetAvgTruckWeight stored AvgTruckWeight_Specified;
    property AvgSpeed:              MinMaxSpeedType        Index (IS_OPTN) read FAvgSpeed write SetAvgSpeed stored AvgSpeed_Specified;
    property AvgOverallConsumption: MinMaxConsumptionType  Index (IS_OPTN) read FAvgOverallConsumption write SetAvgOverallConsumption stored AvgOverallConsumption_Specified;
  end;



  // ************************************************************************ //
  // XML       : GetMBPerformanceAnalysisVehicleOverviewRequest, global, <element>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  GetMBPerformanceAnalysisVehicleOverviewRequest = class(GetMBPerformanceAnalysisVehicleOverviewRequestType)
  private
  published
  end;



  // ************************************************************************ //
  // XML       : PredefinedPeriod, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  PredefinedPeriod7 = class(TRemotable)
  private
    FYear: yearType;
    FInterval: period1Type;
    FInterval_Specified: boolean;
    FCalendarWeek: calendarWeekType;
    FCalendarWeek_Specified: boolean;
    procedure SetInterval(Index: Integer; const Aperiod1Type: period1Type);
    function  Interval_Specified(Index: Integer): boolean;
    procedure SetCalendarWeek(Index: Integer; const AcalendarWeekType: calendarWeekType);
    function  CalendarWeek_Specified(Index: Integer): boolean;
  published
    property Year:         yearType          read FYear write FYear;
    property Interval:     period1Type       Index (IS_OPTN) read FInterval write SetInterval stored Interval_Specified;
    property CalendarWeek: calendarWeekType  Index (IS_OPTN) read FCalendarWeek write SetCalendarWeek stored CalendarWeek_Specified;
  end;



  // ************************************************************************ //
  // XML       : PeriodBreakDown, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  PeriodBreakDown7 = class(TRemotable)
  private
    FStartYear: yearType;
    FEndYear: yearType;
    FInterval: period2Type;
  published
    property StartYear: yearType     read FStartYear write FStartYear;
    property EndYear:   yearType     read FEndYear write FEndYear;
    property Interval:  period2Type  read FInterval write FInterval;
  end;



  // ************************************************************************ //
  // XML       : ToursSummary, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  ToursSummary7 = class(TRemotable)
  private
    FTimeRange: TimeRangeType;
    FTimeRange_Specified: boolean;
    procedure SetTimeRange(Index: Integer; const ATimeRangeType: TimeRangeType);
    function  TimeRange_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property TimeRange: TimeRangeType  Index (IS_OPTN) read FTimeRange write SetTimeRange stored TimeRange_Specified;
  end;



  // ************************************************************************ //
  // XML       : AvgValues, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  AvgValues8 = class(TRemotable)
  private
    FWeight: weightType;
    FWeight_Specified: boolean;
    FSpeed: speedType;
    FSpeed_Specified: boolean;
    FOverallConsumption: consumptionType;
    FOverallConsumption_Specified: boolean;
    FDriveConsumption: consumptionType;
    FDriveConsumption_Specified: boolean;
    procedure SetWeight(Index: Integer; const AweightType: weightType);
    function  Weight_Specified(Index: Integer): boolean;
    procedure SetSpeed(Index: Integer; const AspeedType: speedType);
    function  Speed_Specified(Index: Integer): boolean;
    procedure SetOverallConsumption(Index: Integer; const AconsumptionType: consumptionType);
    function  OverallConsumption_Specified(Index: Integer): boolean;
    procedure SetDriveConsumption(Index: Integer; const AconsumptionType: consumptionType);
    function  DriveConsumption_Specified(Index: Integer): boolean;
  published
    property Weight:             weightType       Index (IS_OPTN) read FWeight write SetWeight stored Weight_Specified;
    property Speed:              speedType        Index (IS_OPTN) read FSpeed write SetSpeed stored Speed_Specified;
    property OverallConsumption: consumptionType  Index (IS_OPTN) read FOverallConsumption write SetOverallConsumption stored OverallConsumption_Specified;
    property DriveConsumption:   consumptionType  Index (IS_OPTN) read FDriveConsumption write SetDriveConsumption stored DriveConsumption_Specified;
  end;



  // ************************************************************************ //
  // XML       : Grades, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  Grades10 = class(TRemotable)
  private
    FDrivingStyle: gradeType;
    FDrivingStyle_Specified: boolean;
    FDegreeOfDifficulty: gradeType;
    FDegreeOfDifficulty_Specified: boolean;
    procedure SetDrivingStyle(Index: Integer; const AgradeType: gradeType);
    function  DrivingStyle_Specified(Index: Integer): boolean;
    procedure SetDegreeOfDifficulty(Index: Integer; const AgradeType: gradeType);
    function  DegreeOfDifficulty_Specified(Index: Integer): boolean;
  published
    property DrivingStyle:       gradeType  Index (IS_OPTN) read FDrivingStyle write SetDrivingStyle stored DrivingStyle_Specified;
    property DegreeOfDifficulty: gradeType  Index (IS_OPTN) read FDegreeOfDifficulty write SetDegreeOfDifficulty stored DegreeOfDifficulty_Specified;
  end;



  // ************************************************************************ //
  // XML       : Period, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  Period10 = class(TRemotable)
  private
    FBegin_: timestampType;
    FBegin__Specified: boolean;
    FEnd_: timestampType;
    FEnd__Specified: boolean;
    FPeriod: Period3Type;
    FPeriod_Specified: boolean;
    procedure SetBegin_(Index: Integer; const AtimestampType: timestampType);
    function  Begin__Specified(Index: Integer): boolean;
    procedure SetEnd_(Index: Integer; const AtimestampType: timestampType);
    function  End__Specified(Index: Integer): boolean;
    procedure SetPeriod(Index: Integer; const APeriod3Type: Period3Type);
    function  Period_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property Begin_: timestampType  Index (IS_OPTN) read FBegin_ write SetBegin_ stored Begin__Specified;
    property End_:   timestampType  Index (IS_OPTN) read FEnd_ write SetEnd_ stored End__Specified;
    property Period: Period3Type    Index (IS_OPTN) read FPeriod write SetPeriod stored Period_Specified;
  end;



  // ************************************************************************ //
  // XML       : Analysis, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  Analysis7 = class(TRemotable)
  private
    FAnalysisKind: analysisModelType;
    FToursSummary: ToursSummary6;
    FToursSummary_Specified: boolean;
    FPeriodBreakDown: PeriodBreakDown4;
    FPeriodBreakDown_Specified: boolean;
    FPredefinedPeriod: PredefinedPeriod4;
    FPredefinedPeriod_Specified: boolean;
    procedure SetToursSummary(Index: Integer; const AToursSummary6: ToursSummary6);
    function  ToursSummary_Specified(Index: Integer): boolean;
    procedure SetPeriodBreakDown(Index: Integer; const APeriodBreakDown4: PeriodBreakDown4);
    function  PeriodBreakDown_Specified(Index: Integer): boolean;
    procedure SetPredefinedPeriod(Index: Integer; const APredefinedPeriod4: PredefinedPeriod4);
    function  PredefinedPeriod_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property AnalysisKind:     analysisModelType  read FAnalysisKind write FAnalysisKind;
    property ToursSummary:     ToursSummary6      Index (IS_OPTN) read FToursSummary write SetToursSummary stored ToursSummary_Specified;
    property PeriodBreakDown:  PeriodBreakDown4   Index (IS_OPTN) read FPeriodBreakDown write SetPeriodBreakDown stored PeriodBreakDown_Specified;
    property PredefinedPeriod: PredefinedPeriod4  Index (IS_OPTN) read FPredefinedPeriod write SetPredefinedPeriod stored PredefinedPeriod_Specified;
  end;

  getChartsType   =  type Boolean;      { "http://www.fleetboard.com/data"[GblSmpl] }


  // ************************************************************************ //
  // XML       : GetUniversalPerformanceAnalysisVehicleRequestType, global, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  GetUniversalPerformanceAnalysisVehicleRequestType = class(TRemotable)
  private
    Fsessionid: sessionidType;
    Fsessionid_Specified: boolean;
    Flimit: limitType;
    Flimit_Specified: boolean;
    Foffset: offsetType;
    Foffset_Specified: boolean;
    Fversion: versionType;
    Fversion_Specified: boolean;
    FGetCharts: getChartsType;
    FGetCharts_Specified: boolean;
    FVehicleID: vehicleidType;
    FVehicleID_Specified: boolean;
    FVehicleGroupID: vehicleGroupIDType;
    FVehicleGroupID_Specified: boolean;
    FAnalysis: Analysis4;
    FDrivingStyle: MinMaxGradeType;
    FDrivingStyle_Specified: boolean;
    FIdleTimeGrading: MinMaxGradeType;
    FIdleTimeGrading_Specified: boolean;
    FOverrevNumberGrading: MinMaxGradeType;
    FOverrevNumberGrading_Specified: boolean;
    FOverrevTimeGrading: MinMaxGradeType;
    FOverrevTimeGrading_Specified: boolean;
    FEcoDrivingGrading: MinMaxGradeType;
    FEcoDrivingGrading_Specified: boolean;
    FHarshBrakeGrading: MinMaxGradeType;
    FHarshBrakeGrading_Specified: boolean;
    FOverspeedTimeGrading: MinMaxGradeType;
    FOverspeedTimeGrading_Specified: boolean;
    FAvgTotalConsumption: MinMaxConsumptionType;
    FAvgTotalConsumption_Specified: boolean;
    procedure Setsessionid(Index: Integer; const AsessionidType: sessionidType);
    function  sessionid_Specified(Index: Integer): boolean;
    procedure Setlimit(Index: Integer; const AlimitType: limitType);
    function  limit_Specified(Index: Integer): boolean;
    procedure Setoffset(Index: Integer; const AoffsetType: offsetType);
    function  offset_Specified(Index: Integer): boolean;
    procedure Setversion(Index: Integer; const AversionType: versionType);
    function  version_Specified(Index: Integer): boolean;
    procedure SetGetCharts(Index: Integer; const AgetChartsType: getChartsType);
    function  GetCharts_Specified(Index: Integer): boolean;
    procedure SetVehicleID(Index: Integer; const AvehicleidType: vehicleidType);
    function  VehicleID_Specified(Index: Integer): boolean;
    procedure SetVehicleGroupID(Index: Integer; const AvehicleGroupIDType: vehicleGroupIDType);
    function  VehicleGroupID_Specified(Index: Integer): boolean;
    procedure SetDrivingStyle(Index: Integer; const AMinMaxGradeType: MinMaxGradeType);
    function  DrivingStyle_Specified(Index: Integer): boolean;
    procedure SetIdleTimeGrading(Index: Integer; const AMinMaxGradeType: MinMaxGradeType);
    function  IdleTimeGrading_Specified(Index: Integer): boolean;
    procedure SetOverrevNumberGrading(Index: Integer; const AMinMaxGradeType: MinMaxGradeType);
    function  OverrevNumberGrading_Specified(Index: Integer): boolean;
    procedure SetOverrevTimeGrading(Index: Integer; const AMinMaxGradeType: MinMaxGradeType);
    function  OverrevTimeGrading_Specified(Index: Integer): boolean;
    procedure SetEcoDrivingGrading(Index: Integer; const AMinMaxGradeType: MinMaxGradeType);
    function  EcoDrivingGrading_Specified(Index: Integer): boolean;
    procedure SetHarshBrakeGrading(Index: Integer; const AMinMaxGradeType: MinMaxGradeType);
    function  HarshBrakeGrading_Specified(Index: Integer): boolean;
    procedure SetOverspeedTimeGrading(Index: Integer; const AMinMaxGradeType: MinMaxGradeType);
    function  OverspeedTimeGrading_Specified(Index: Integer): boolean;
    procedure SetAvgTotalConsumption(Index: Integer; const AMinMaxConsumptionType: MinMaxConsumptionType);
    function  AvgTotalConsumption_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property sessionid:            sessionidType          Index (IS_ATTR or IS_OPTN) read Fsessionid write Setsessionid stored sessionid_Specified;
    property limit:                limitType              Index (IS_ATTR or IS_OPTN) read Flimit write Setlimit stored limit_Specified;
    property offset:               offsetType             Index (IS_ATTR or IS_OPTN) read Foffset write Setoffset stored offset_Specified;
    property version:              versionType            Index (IS_ATTR or IS_OPTN) read Fversion write Setversion stored version_Specified;
    property GetCharts:            getChartsType          Index (IS_OPTN) read FGetCharts write SetGetCharts stored GetCharts_Specified;
    property VehicleID:            vehicleidType          Index (IS_OPTN) read FVehicleID write SetVehicleID stored VehicleID_Specified;
    property VehicleGroupID:       vehicleGroupIDType     Index (IS_OPTN) read FVehicleGroupID write SetVehicleGroupID stored VehicleGroupID_Specified;
    property Analysis:             Analysis4              read FAnalysis write FAnalysis;
    property DrivingStyle:         MinMaxGradeType        Index (IS_OPTN) read FDrivingStyle write SetDrivingStyle stored DrivingStyle_Specified;
    property IdleTimeGrading:      MinMaxGradeType        Index (IS_OPTN) read FIdleTimeGrading write SetIdleTimeGrading stored IdleTimeGrading_Specified;
    property OverrevNumberGrading: MinMaxGradeType        Index (IS_OPTN) read FOverrevNumberGrading write SetOverrevNumberGrading stored OverrevNumberGrading_Specified;
    property OverrevTimeGrading:   MinMaxGradeType        Index (IS_OPTN) read FOverrevTimeGrading write SetOverrevTimeGrading stored OverrevTimeGrading_Specified;
    property EcoDrivingGrading:    MinMaxGradeType        Index (IS_OPTN) read FEcoDrivingGrading write SetEcoDrivingGrading stored EcoDrivingGrading_Specified;
    property HarshBrakeGrading:    MinMaxGradeType        Index (IS_OPTN) read FHarshBrakeGrading write SetHarshBrakeGrading stored HarshBrakeGrading_Specified;
    property OverspeedTimeGrading: MinMaxGradeType        Index (IS_OPTN) read FOverspeedTimeGrading write SetOverspeedTimeGrading stored OverspeedTimeGrading_Specified;
    property AvgTotalConsumption:  MinMaxConsumptionType  Index (IS_OPTN) read FAvgTotalConsumption write SetAvgTotalConsumption stored AvgTotalConsumption_Specified;
  end;



  // ************************************************************************ //
  // XML       : GetUniversalPerformanceAnalysisVehicleRequest, global, <element>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  GetUniversalPerformanceAnalysisVehicleRequest = class(GetUniversalPerformanceAnalysisVehicleRequestType)
  private
  published
  end;



  // ************************************************************************ //
  // XML       : GetMBPerformanceAnalysisVehicleRequestType, global, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  GetMBPerformanceAnalysisVehicleRequestType = class(TRemotable)
  private
    Fsessionid: sessionidType;
    Fsessionid_Specified: boolean;
    Flimit: limitType;
    Flimit_Specified: boolean;
    Foffset: offsetType;
    Foffset_Specified: boolean;
    Fversion: versionType;
    Fversion_Specified: boolean;
    FGetCharts: getChartsType;
    FGetCharts_Specified: boolean;
    FVehicleID: vehicleidType;
    FVehicleID_Specified: boolean;
    FVehicleGroupID: vehicleGroupIDType;
    FVehicleGroupID_Specified: boolean;
    FTelematicGroupID: telematicGroupIDType;
    FTelematicGroupID_Specified: boolean;
    FAnalysis: Analysis7;
    FDrivingStyle: MinMaxGradeType;
    FDrivingStyle_Specified: boolean;
    FDegreeOfDifficulty: MinMaxGradeType;
    FDegreeOfDifficulty_Specified: boolean;
    FAvgTruckWeight: MinMaxWeightType;
    FAvgTruckWeight_Specified: boolean;
    FAvgSpeed: MinMaxSpeedType;
    FAvgSpeed_Specified: boolean;
    FAvgOverallConsumption: MinMaxConsumptionType;
    FAvgOverallConsumption_Specified: boolean;
    procedure Setsessionid(Index: Integer; const AsessionidType: sessionidType);
    function  sessionid_Specified(Index: Integer): boolean;
    procedure Setlimit(Index: Integer; const AlimitType: limitType);
    function  limit_Specified(Index: Integer): boolean;
    procedure Setoffset(Index: Integer; const AoffsetType: offsetType);
    function  offset_Specified(Index: Integer): boolean;
    procedure Setversion(Index: Integer; const AversionType: versionType);
    function  version_Specified(Index: Integer): boolean;
    procedure SetGetCharts(Index: Integer; const AgetChartsType: getChartsType);
    function  GetCharts_Specified(Index: Integer): boolean;
    procedure SetVehicleID(Index: Integer; const AvehicleidType: vehicleidType);
    function  VehicleID_Specified(Index: Integer): boolean;
    procedure SetVehicleGroupID(Index: Integer; const AvehicleGroupIDType: vehicleGroupIDType);
    function  VehicleGroupID_Specified(Index: Integer): boolean;
    procedure SetTelematicGroupID(Index: Integer; const AtelematicGroupIDType: telematicGroupIDType);
    function  TelematicGroupID_Specified(Index: Integer): boolean;
    procedure SetDrivingStyle(Index: Integer; const AMinMaxGradeType: MinMaxGradeType);
    function  DrivingStyle_Specified(Index: Integer): boolean;
    procedure SetDegreeOfDifficulty(Index: Integer; const AMinMaxGradeType: MinMaxGradeType);
    function  DegreeOfDifficulty_Specified(Index: Integer): boolean;
    procedure SetAvgTruckWeight(Index: Integer; const AMinMaxWeightType: MinMaxWeightType);
    function  AvgTruckWeight_Specified(Index: Integer): boolean;
    procedure SetAvgSpeed(Index: Integer; const AMinMaxSpeedType: MinMaxSpeedType);
    function  AvgSpeed_Specified(Index: Integer): boolean;
    procedure SetAvgOverallConsumption(Index: Integer; const AMinMaxConsumptionType: MinMaxConsumptionType);
    function  AvgOverallConsumption_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property sessionid:             sessionidType          Index (IS_ATTR or IS_OPTN) read Fsessionid write Setsessionid stored sessionid_Specified;
    property limit:                 limitType              Index (IS_ATTR or IS_OPTN) read Flimit write Setlimit stored limit_Specified;
    property offset:                offsetType             Index (IS_ATTR or IS_OPTN) read Foffset write Setoffset stored offset_Specified;
    property version:               versionType            Index (IS_ATTR or IS_OPTN) read Fversion write Setversion stored version_Specified;
    property GetCharts:             getChartsType          Index (IS_OPTN) read FGetCharts write SetGetCharts stored GetCharts_Specified;
    property VehicleID:             vehicleidType          Index (IS_OPTN) read FVehicleID write SetVehicleID stored VehicleID_Specified;
    property VehicleGroupID:        vehicleGroupIDType     Index (IS_OPTN) read FVehicleGroupID write SetVehicleGroupID stored VehicleGroupID_Specified;
    property TelematicGroupID:      telematicGroupIDType   Index (IS_OPTN) read FTelematicGroupID write SetTelematicGroupID stored TelematicGroupID_Specified;
    property Analysis:              Analysis7              read FAnalysis write FAnalysis;
    property DrivingStyle:          MinMaxGradeType        Index (IS_OPTN) read FDrivingStyle write SetDrivingStyle stored DrivingStyle_Specified;
    property DegreeOfDifficulty:    MinMaxGradeType        Index (IS_OPTN) read FDegreeOfDifficulty write SetDegreeOfDifficulty stored DegreeOfDifficulty_Specified;
    property AvgTruckWeight:        MinMaxWeightType       Index (IS_OPTN) read FAvgTruckWeight write SetAvgTruckWeight stored AvgTruckWeight_Specified;
    property AvgSpeed:              MinMaxSpeedType        Index (IS_OPTN) read FAvgSpeed write SetAvgSpeed stored AvgSpeed_Specified;
    property AvgOverallConsumption: MinMaxConsumptionType  Index (IS_OPTN) read FAvgOverallConsumption write SetAvgOverallConsumption stored AvgOverallConsumption_Specified;
  end;



  // ************************************************************************ //
  // XML       : GetMBPerformanceAnalysisVehicleRequest, global, <element>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  GetMBPerformanceAnalysisVehicleRequest = class(GetMBPerformanceAnalysisVehicleRequestType)
  private
  published
  end;



  // ************************************************************************ //
  // XML       : ToursSummary, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  ToursSummary8 = class(TRemotable)
  private
    FTimeRange: TimeRangeType;
  public
    destructor Destroy; override;
  published
    property TimeRange: TimeRangeType  read FTimeRange write FTimeRange;
  end;



  // ************************************************************************ //
  // XML       : Analysis, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  Analysis8 = class(TRemotable)
  private
    FAnalysisKind: analysisModelType;
    FToursSummary: ToursSummary8;
    FToursSummary_Specified: boolean;
    FPeriodBreakDown: PeriodBreakDown8;
    FPeriodBreakDown_Specified: boolean;
    FPredefinedPeriod: PredefinedPeriod8;
    FPredefinedPeriod_Specified: boolean;
    procedure SetToursSummary(Index: Integer; const AToursSummary8: ToursSummary8);
    function  ToursSummary_Specified(Index: Integer): boolean;
    procedure SetPeriodBreakDown(Index: Integer; const APeriodBreakDown8: PeriodBreakDown8);
    function  PeriodBreakDown_Specified(Index: Integer): boolean;
    procedure SetPredefinedPeriod(Index: Integer; const APredefinedPeriod8: PredefinedPeriod8);
    function  PredefinedPeriod_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property AnalysisKind:     analysisModelType  read FAnalysisKind write FAnalysisKind;
    property ToursSummary:     ToursSummary8      Index (IS_OPTN) read FToursSummary write SetToursSummary stored ToursSummary_Specified;
    property PeriodBreakDown:  PeriodBreakDown8   Index (IS_OPTN) read FPeriodBreakDown write SetPeriodBreakDown stored PeriodBreakDown_Specified;
    property PredefinedPeriod: PredefinedPeriod8  Index (IS_OPTN) read FPredefinedPeriod write SetPredefinedPeriod stored PredefinedPeriod_Specified;
  end;

  driverNameIDType =  type Int64;      { "http://www.fleetboard.com/data"[GblSmpl] }


  // ************************************************************************ //
  // XML       : MBPerformanceAnalysisDriverOverviewReport, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  MBPerformanceAnalysisDriverOverviewReport = class(TRemotable)
  private
    FElementId: Int64;
    FDriverNameID: driverNameIDType;
    FPeriod: Period10;
    FGrades: Grades10;
    FAvgValues: AvgValues8;
    FConsumption: ConsumptionValuesType;
    FConsumption_Specified: boolean;
    FTotalDistance: distanceType;
    FTotalDistance_Specified: boolean;
    FVehicleStateTimes: VehicleStateTimeValuesType;
    FVehicleStateTimes_Specified: boolean;
    FNumberOfDrivers: Integer;
    FNumberOfDrivers_Specified: boolean;
    procedure SetConsumption(Index: Integer; const AConsumptionValuesType: ConsumptionValuesType);
    function  Consumption_Specified(Index: Integer): boolean;
    procedure SetTotalDistance(Index: Integer; const AdistanceType: distanceType);
    function  TotalDistance_Specified(Index: Integer): boolean;
    procedure SetVehicleStateTimes(Index: Integer; const AVehicleStateTimeValuesType: VehicleStateTimeValuesType);
    function  VehicleStateTimes_Specified(Index: Integer): boolean;
    procedure SetNumberOfDrivers(Index: Integer; const AInteger: Integer);
    function  NumberOfDrivers_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property ElementId:         Int64                       read FElementId write FElementId;
    property DriverNameID:      driverNameIDType            read FDriverNameID write FDriverNameID;
    property Period:            Period10                    Index (IS_NLBL) read FPeriod write FPeriod;
    property Grades:            Grades10                    Index (IS_NLBL) read FGrades write FGrades;
    property AvgValues:         AvgValues8                  Index (IS_NLBL) read FAvgValues write FAvgValues;
    property Consumption:       ConsumptionValuesType       Index (IS_OPTN) read FConsumption write SetConsumption stored Consumption_Specified;
    property TotalDistance:     distanceType                Index (IS_OPTN) read FTotalDistance write SetTotalDistance stored TotalDistance_Specified;
    property VehicleStateTimes: VehicleStateTimeValuesType  Index (IS_OPTN) read FVehicleStateTimes write SetVehicleStateTimes stored VehicleStateTimes_Specified;
    property NumberOfDrivers:   Integer                     Index (IS_OPTN) read FNumberOfDrivers write SetNumberOfDrivers stored NumberOfDrivers_Specified;
  end;



  // ************************************************************************ //
  // XML       : GetMBPerformanceAnalysisDriverOverviewRequestType, global, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  GetMBPerformanceAnalysisDriverOverviewRequestType = class(TRemotable)
  private
    Fsessionid: sessionidType;
    Fsessionid_Specified: boolean;
    Flimit: limitType;
    Flimit_Specified: boolean;
    Foffset: offsetType;
    Foffset_Specified: boolean;
    Fversion: versionType;
    Fversion_Specified: boolean;
    FDriverNameID: driverNameIDType;
    FDriverNameID_Specified: boolean;
    FDriverGroupID: driverGroupIDType;
    FDriverGroupID_Specified: boolean;
    FAnalysis: Analysis8;
    FDrivingStyle: MinMaxGradeType;
    FDrivingStyle_Specified: boolean;
    FDegreeOfDifficulty: MinMaxGradeType;
    FDegreeOfDifficulty_Specified: boolean;
    FAvgTruckWeight: MinMaxWeightType;
    FAvgTruckWeight_Specified: boolean;
    FAvgSpeed: MinMaxSpeedType;
    FAvgSpeed_Specified: boolean;
    FAvgOverallConsumption: MinMaxConsumptionType;
    FAvgOverallConsumption_Specified: boolean;
    procedure Setsessionid(Index: Integer; const AsessionidType: sessionidType);
    function  sessionid_Specified(Index: Integer): boolean;
    procedure Setlimit(Index: Integer; const AlimitType: limitType);
    function  limit_Specified(Index: Integer): boolean;
    procedure Setoffset(Index: Integer; const AoffsetType: offsetType);
    function  offset_Specified(Index: Integer): boolean;
    procedure Setversion(Index: Integer; const AversionType: versionType);
    function  version_Specified(Index: Integer): boolean;
    procedure SetDriverNameID(Index: Integer; const AdriverNameIDType: driverNameIDType);
    function  DriverNameID_Specified(Index: Integer): boolean;
    procedure SetDriverGroupID(Index: Integer; const AdriverGroupIDType: driverGroupIDType);
    function  DriverGroupID_Specified(Index: Integer): boolean;
    procedure SetDrivingStyle(Index: Integer; const AMinMaxGradeType: MinMaxGradeType);
    function  DrivingStyle_Specified(Index: Integer): boolean;
    procedure SetDegreeOfDifficulty(Index: Integer; const AMinMaxGradeType: MinMaxGradeType);
    function  DegreeOfDifficulty_Specified(Index: Integer): boolean;
    procedure SetAvgTruckWeight(Index: Integer; const AMinMaxWeightType: MinMaxWeightType);
    function  AvgTruckWeight_Specified(Index: Integer): boolean;
    procedure SetAvgSpeed(Index: Integer; const AMinMaxSpeedType: MinMaxSpeedType);
    function  AvgSpeed_Specified(Index: Integer): boolean;
    procedure SetAvgOverallConsumption(Index: Integer; const AMinMaxConsumptionType: MinMaxConsumptionType);
    function  AvgOverallConsumption_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property sessionid:             sessionidType          Index (IS_ATTR or IS_OPTN) read Fsessionid write Setsessionid stored sessionid_Specified;
    property limit:                 limitType              Index (IS_ATTR or IS_OPTN) read Flimit write Setlimit stored limit_Specified;
    property offset:                offsetType             Index (IS_ATTR or IS_OPTN) read Foffset write Setoffset stored offset_Specified;
    property version:               versionType            Index (IS_ATTR or IS_OPTN) read Fversion write Setversion stored version_Specified;
    property DriverNameID:          driverNameIDType       Index (IS_OPTN) read FDriverNameID write SetDriverNameID stored DriverNameID_Specified;
    property DriverGroupID:         driverGroupIDType      Index (IS_OPTN) read FDriverGroupID write SetDriverGroupID stored DriverGroupID_Specified;
    property Analysis:              Analysis8              read FAnalysis write FAnalysis;
    property DrivingStyle:          MinMaxGradeType        Index (IS_OPTN) read FDrivingStyle write SetDrivingStyle stored DrivingStyle_Specified;
    property DegreeOfDifficulty:    MinMaxGradeType        Index (IS_OPTN) read FDegreeOfDifficulty write SetDegreeOfDifficulty stored DegreeOfDifficulty_Specified;
    property AvgTruckWeight:        MinMaxWeightType       Index (IS_OPTN) read FAvgTruckWeight write SetAvgTruckWeight stored AvgTruckWeight_Specified;
    property AvgSpeed:              MinMaxSpeedType        Index (IS_OPTN) read FAvgSpeed write SetAvgSpeed stored AvgSpeed_Specified;
    property AvgOverallConsumption: MinMaxConsumptionType  Index (IS_OPTN) read FAvgOverallConsumption write SetAvgOverallConsumption stored AvgOverallConsumption_Specified;
  end;



  // ************************************************************************ //
  // XML       : GetMBPerformanceAnalysisDriverOverviewRequest, global, <element>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  GetMBPerformanceAnalysisDriverOverviewRequest = class(GetMBPerformanceAnalysisDriverOverviewRequestType)
  private
  published
  end;



  // ************************************************************************ //
  // XML       : UniversalPerformanceAnalysisDriverOverviewReport, global, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  UniversalPerformanceAnalysisDriverOverviewReport = class(TRemotable)
  private
    FDriverNameId: driverNameIDType;
    FDriverNameId_Specified: boolean;
    FDriverFirstName: string;
    FDriverFirstName_Specified: boolean;
    FDriverLastName: string;
    FDriverLastName_Specified: boolean;
    FDriverGroups: Array_Of_DriverGroup;
    FDriverGroups_Specified: boolean;
    FElementId: Int64;
    FPeriod: Period2;
    FGrades: Grades2;
    FGrades_Specified: boolean;
    FAverageTotalConsumption: consumptionType;
    FAverageTotalConsumption_Specified: boolean;
    FTotalDistance: distanceType;
    FTotalDistance_Specified: boolean;
    FNumberOfDrivers: Integer;
    FNumberOfDrivers_Specified: boolean;
    procedure SetDriverNameId(Index: Integer; const AdriverNameIDType: driverNameIDType);
    function  DriverNameId_Specified(Index: Integer): boolean;
    procedure SetDriverFirstName(Index: Integer; const Astring: string);
    function  DriverFirstName_Specified(Index: Integer): boolean;
    procedure SetDriverLastName(Index: Integer; const Astring: string);
    function  DriverLastName_Specified(Index: Integer): boolean;
    procedure SetDriverGroups(Index: Integer; const AArray_Of_DriverGroup: Array_Of_DriverGroup);
    function  DriverGroups_Specified(Index: Integer): boolean;
    procedure SetGrades(Index: Integer; const AGrades2: Grades2);
    function  Grades_Specified(Index: Integer): boolean;
    procedure SetAverageTotalConsumption(Index: Integer; const AconsumptionType: consumptionType);
    function  AverageTotalConsumption_Specified(Index: Integer): boolean;
    procedure SetTotalDistance(Index: Integer; const AdistanceType: distanceType);
    function  TotalDistance_Specified(Index: Integer): boolean;
    procedure SetNumberOfDrivers(Index: Integer; const AInteger: Integer);
    function  NumberOfDrivers_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property DriverNameId:            driverNameIDType      Index (IS_OPTN) read FDriverNameId write SetDriverNameId stored DriverNameId_Specified;
    property DriverFirstName:         string                Index (IS_OPTN) read FDriverFirstName write SetDriverFirstName stored DriverFirstName_Specified;
    property DriverLastName:          string                Index (IS_OPTN) read FDriverLastName write SetDriverLastName stored DriverLastName_Specified;
    property DriverGroups:            Array_Of_DriverGroup  Index (IS_OPTN or IS_UNBD) read FDriverGroups write SetDriverGroups stored DriverGroups_Specified;
    property ElementId:               Int64                 read FElementId write FElementId;
    property Period:                  Period2               read FPeriod write FPeriod;
    property Grades:                  Grades2               Index (IS_OPTN) read FGrades write SetGrades stored Grades_Specified;
    property AverageTotalConsumption: consumptionType       Index (IS_OPTN) read FAverageTotalConsumption write SetAverageTotalConsumption stored AverageTotalConsumption_Specified;
    property TotalDistance:           distanceType          Index (IS_OPTN) read FTotalDistance write SetTotalDistance stored TotalDistance_Specified;
    property NumberOfDrivers:         Integer               Index (IS_OPTN) read FNumberOfDrivers write SetNumberOfDrivers stored NumberOfDrivers_Specified;
  end;



  // ************************************************************************ //
  // XML       : GetMBPerformanceAnalysisDriverRequestType, global, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  GetMBPerformanceAnalysisDriverRequestType = class(TRemotable)
  private
    Fsessionid: sessionidType;
    Fsessionid_Specified: boolean;
    Flimit: limitType;
    Flimit_Specified: boolean;
    Foffset: offsetType;
    Foffset_Specified: boolean;
    Fversion: versionType;
    Fversion_Specified: boolean;
    FGetCharts: getChartsType;
    FGetCharts_Specified: boolean;
    FDriverNameID: driverNameIDType;
    FDriverNameID_Specified: boolean;
    FDriverGroupID: driverGroupIDType;
    FDriverGroupID_Specified: boolean;
    FAnalysis: Analysis2;
    FDrivingStyle: MinMaxGradeType;
    FDrivingStyle_Specified: boolean;
    FDegreeOfDifficulty: MinMaxGradeType;
    FDegreeOfDifficulty_Specified: boolean;
    FAvgTruckWeight: MinMaxWeightType;
    FAvgTruckWeight_Specified: boolean;
    FAvgSpeed: MinMaxSpeedType;
    FAvgSpeed_Specified: boolean;
    FAvgOverallConsumption: MinMaxConsumptionType;
    FAvgOverallConsumption_Specified: boolean;
    procedure Setsessionid(Index: Integer; const AsessionidType: sessionidType);
    function  sessionid_Specified(Index: Integer): boolean;
    procedure Setlimit(Index: Integer; const AlimitType: limitType);
    function  limit_Specified(Index: Integer): boolean;
    procedure Setoffset(Index: Integer; const AoffsetType: offsetType);
    function  offset_Specified(Index: Integer): boolean;
    procedure Setversion(Index: Integer; const AversionType: versionType);
    function  version_Specified(Index: Integer): boolean;
    procedure SetGetCharts(Index: Integer; const AgetChartsType: getChartsType);
    function  GetCharts_Specified(Index: Integer): boolean;
    procedure SetDriverNameID(Index: Integer; const AdriverNameIDType: driverNameIDType);
    function  DriverNameID_Specified(Index: Integer): boolean;
    procedure SetDriverGroupID(Index: Integer; const AdriverGroupIDType: driverGroupIDType);
    function  DriverGroupID_Specified(Index: Integer): boolean;
    procedure SetDrivingStyle(Index: Integer; const AMinMaxGradeType: MinMaxGradeType);
    function  DrivingStyle_Specified(Index: Integer): boolean;
    procedure SetDegreeOfDifficulty(Index: Integer; const AMinMaxGradeType: MinMaxGradeType);
    function  DegreeOfDifficulty_Specified(Index: Integer): boolean;
    procedure SetAvgTruckWeight(Index: Integer; const AMinMaxWeightType: MinMaxWeightType);
    function  AvgTruckWeight_Specified(Index: Integer): boolean;
    procedure SetAvgSpeed(Index: Integer; const AMinMaxSpeedType: MinMaxSpeedType);
    function  AvgSpeed_Specified(Index: Integer): boolean;
    procedure SetAvgOverallConsumption(Index: Integer; const AMinMaxConsumptionType: MinMaxConsumptionType);
    function  AvgOverallConsumption_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property sessionid:             sessionidType          Index (IS_ATTR or IS_OPTN) read Fsessionid write Setsessionid stored sessionid_Specified;
    property limit:                 limitType              Index (IS_ATTR or IS_OPTN) read Flimit write Setlimit stored limit_Specified;
    property offset:                offsetType             Index (IS_ATTR or IS_OPTN) read Foffset write Setoffset stored offset_Specified;
    property version:               versionType            Index (IS_ATTR or IS_OPTN) read Fversion write Setversion stored version_Specified;
    property GetCharts:             getChartsType          Index (IS_OPTN) read FGetCharts write SetGetCharts stored GetCharts_Specified;
    property DriverNameID:          driverNameIDType       Index (IS_OPTN) read FDriverNameID write SetDriverNameID stored DriverNameID_Specified;
    property DriverGroupID:         driverGroupIDType      Index (IS_OPTN) read FDriverGroupID write SetDriverGroupID stored DriverGroupID_Specified;
    property Analysis:              Analysis2              read FAnalysis write FAnalysis;
    property DrivingStyle:          MinMaxGradeType        Index (IS_OPTN) read FDrivingStyle write SetDrivingStyle stored DrivingStyle_Specified;
    property DegreeOfDifficulty:    MinMaxGradeType        Index (IS_OPTN) read FDegreeOfDifficulty write SetDegreeOfDifficulty stored DegreeOfDifficulty_Specified;
    property AvgTruckWeight:        MinMaxWeightType       Index (IS_OPTN) read FAvgTruckWeight write SetAvgTruckWeight stored AvgTruckWeight_Specified;
    property AvgSpeed:              MinMaxSpeedType        Index (IS_OPTN) read FAvgSpeed write SetAvgSpeed stored AvgSpeed_Specified;
    property AvgOverallConsumption: MinMaxConsumptionType  Index (IS_OPTN) read FAvgOverallConsumption write SetAvgOverallConsumption stored AvgOverallConsumption_Specified;
  end;



  // ************************************************************************ //
  // XML       : GetMBPerformanceAnalysisDriverRequest, global, <element>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  GetMBPerformanceAnalysisDriverRequest = class(GetMBPerformanceAnalysisDriverRequestType)
  private
  published
  end;



  // ************************************************************************ //
  // XML       : GetUniversalPerformanceAnalysisDriverOverviewRequestType, global, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  GetUniversalPerformanceAnalysisDriverOverviewRequestType = class(TRemotable)
  private
    Fsessionid: sessionidType;
    Fsessionid_Specified: boolean;
    Flimit: limitType;
    Flimit_Specified: boolean;
    Foffset: offsetType;
    Foffset_Specified: boolean;
    Fversion: versionType;
    Fversion_Specified: boolean;
    FDriverNameId: driverNameIDType;
    FDriverNameId_Specified: boolean;
    FDriverGroupId: driverGroupIDType;
    FDriverGroupId_Specified: boolean;
    FAnalysis: Analysis3;
    FDrivingStyle: MinMaxGradeType;
    FDrivingStyle_Specified: boolean;
    FIdleTimeGrading: MinMaxGradeType;
    FIdleTimeGrading_Specified: boolean;
    FOverrevNumberGrading: MinMaxGradeType;
    FOverrevNumberGrading_Specified: boolean;
    FOverrevTimeGrading: MinMaxGradeType;
    FOverrevTimeGrading_Specified: boolean;
    FEcoDrivingGrading: MinMaxGradeType;
    FEcoDrivingGrading_Specified: boolean;
    FHarshBrakeGrading: MinMaxGradeType;
    FHarshBrakeGrading_Specified: boolean;
    FOverspeedTimeGrading: MinMaxGradeType;
    FOverspeedTimeGrading_Specified: boolean;
    FAvgTotalConsumption: MinMaxConsumptionType;
    FAvgTotalConsumption_Specified: boolean;
    procedure Setsessionid(Index: Integer; const AsessionidType: sessionidType);
    function  sessionid_Specified(Index: Integer): boolean;
    procedure Setlimit(Index: Integer; const AlimitType: limitType);
    function  limit_Specified(Index: Integer): boolean;
    procedure Setoffset(Index: Integer; const AoffsetType: offsetType);
    function  offset_Specified(Index: Integer): boolean;
    procedure Setversion(Index: Integer; const AversionType: versionType);
    function  version_Specified(Index: Integer): boolean;
    procedure SetDriverNameId(Index: Integer; const AdriverNameIDType: driverNameIDType);
    function  DriverNameId_Specified(Index: Integer): boolean;
    procedure SetDriverGroupId(Index: Integer; const AdriverGroupIDType: driverGroupIDType);
    function  DriverGroupId_Specified(Index: Integer): boolean;
    procedure SetDrivingStyle(Index: Integer; const AMinMaxGradeType: MinMaxGradeType);
    function  DrivingStyle_Specified(Index: Integer): boolean;
    procedure SetIdleTimeGrading(Index: Integer; const AMinMaxGradeType: MinMaxGradeType);
    function  IdleTimeGrading_Specified(Index: Integer): boolean;
    procedure SetOverrevNumberGrading(Index: Integer; const AMinMaxGradeType: MinMaxGradeType);
    function  OverrevNumberGrading_Specified(Index: Integer): boolean;
    procedure SetOverrevTimeGrading(Index: Integer; const AMinMaxGradeType: MinMaxGradeType);
    function  OverrevTimeGrading_Specified(Index: Integer): boolean;
    procedure SetEcoDrivingGrading(Index: Integer; const AMinMaxGradeType: MinMaxGradeType);
    function  EcoDrivingGrading_Specified(Index: Integer): boolean;
    procedure SetHarshBrakeGrading(Index: Integer; const AMinMaxGradeType: MinMaxGradeType);
    function  HarshBrakeGrading_Specified(Index: Integer): boolean;
    procedure SetOverspeedTimeGrading(Index: Integer; const AMinMaxGradeType: MinMaxGradeType);
    function  OverspeedTimeGrading_Specified(Index: Integer): boolean;
    procedure SetAvgTotalConsumption(Index: Integer; const AMinMaxConsumptionType: MinMaxConsumptionType);
    function  AvgTotalConsumption_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property sessionid:            sessionidType          Index (IS_ATTR or IS_OPTN) read Fsessionid write Setsessionid stored sessionid_Specified;
    property limit:                limitType              Index (IS_ATTR or IS_OPTN) read Flimit write Setlimit stored limit_Specified;
    property offset:               offsetType             Index (IS_ATTR or IS_OPTN) read Foffset write Setoffset stored offset_Specified;
    property version:              versionType            Index (IS_ATTR or IS_OPTN) read Fversion write Setversion stored version_Specified;
    property DriverNameId:         driverNameIDType       Index (IS_OPTN) read FDriverNameId write SetDriverNameId stored DriverNameId_Specified;
    property DriverGroupId:        driverGroupIDType      Index (IS_OPTN) read FDriverGroupId write SetDriverGroupId stored DriverGroupId_Specified;
    property Analysis:             Analysis3              read FAnalysis write FAnalysis;
    property DrivingStyle:         MinMaxGradeType        Index (IS_OPTN) read FDrivingStyle write SetDrivingStyle stored DrivingStyle_Specified;
    property IdleTimeGrading:      MinMaxGradeType        Index (IS_OPTN) read FIdleTimeGrading write SetIdleTimeGrading stored IdleTimeGrading_Specified;
    property OverrevNumberGrading: MinMaxGradeType        Index (IS_OPTN) read FOverrevNumberGrading write SetOverrevNumberGrading stored OverrevNumberGrading_Specified;
    property OverrevTimeGrading:   MinMaxGradeType        Index (IS_OPTN) read FOverrevTimeGrading write SetOverrevTimeGrading stored OverrevTimeGrading_Specified;
    property EcoDrivingGrading:    MinMaxGradeType        Index (IS_OPTN) read FEcoDrivingGrading write SetEcoDrivingGrading stored EcoDrivingGrading_Specified;
    property HarshBrakeGrading:    MinMaxGradeType        Index (IS_OPTN) read FHarshBrakeGrading write SetHarshBrakeGrading stored HarshBrakeGrading_Specified;
    property OverspeedTimeGrading: MinMaxGradeType        Index (IS_OPTN) read FOverspeedTimeGrading write SetOverspeedTimeGrading stored OverspeedTimeGrading_Specified;
    property AvgTotalConsumption:  MinMaxConsumptionType  Index (IS_OPTN) read FAvgTotalConsumption write SetAvgTotalConsumption stored AvgTotalConsumption_Specified;
  end;



  // ************************************************************************ //
  // XML       : GetUniversalPerformanceAnalysisDriverOverviewRequest, global, <element>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  GetUniversalPerformanceAnalysisDriverOverviewRequest = class(GetUniversalPerformanceAnalysisDriverOverviewRequestType)
  private
  published
  end;

  Array_Of_driverNameIDType = array of driverNameIDType;   { "http://www.fleetboard.com/data"[GblUbnd] }


  // ************************************************************************ //
  // XML       : MBPerformanceAnalysisVehicleReport, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  MBPerformanceAnalysisVehicleReport = class(TRemotable)
  private
    FID: Int64;
    FVehicleID: vehicleidType;
    FDriverNameID: Array_Of_driverNameIDType;
    FDriverNameID_Specified: boolean;
    FTrailerName: Array_Of_string;
    FTrailerName_Specified: boolean;
    FPeriod: Period8;
    FGrades: Grades8;
    FAvgValues: AvgValues6;
    FConsumption: Consumption6;
    FPowerTakeOff: PowerTakeOff3;
    FBrakingDistance: distanceType;
    FBrakingDistance_Specified: boolean;
    FNrOfStops: Int64;
    FNrOfStops_Specified: boolean;
    FHandBrakeUsageCount: Int64;
    FHandBrakeUsageCount_Specified: boolean;
    FMovingTime: Int64;
    FMovingTime_Specified: boolean;
    FStopTimeEngineOn: timeType;
    FStopTimeEngineOn_Specified: boolean;
    FStopTimeEngineOff: timeType;
    FStopTimeEngineOff_Specified: boolean;
    FSpeedOver85PerTotalDistance: speedType;
    FSpeedOver85PerTotalDistance_Specified: boolean;
    FRetarderBrakingDistance: distanceType;
    FRetarderBrakingDistance_Specified: boolean;
    FTotalCoastingDistance: distanceType;
    FTotalCoastingDistance_Specified: boolean;
    FTotalDistance: distanceType;
    FTotalDistance_Specified: boolean;
    FCruiseControlDistance: distanceType;
    FCruiseControlDistance_Specified: boolean;
    FAssistanceSystems: assistanceSystems;
    FAssistanceSystems_Specified: boolean;
    FCharts: ChartsType;
    FCharts_Specified: boolean;
    procedure SetDriverNameID(Index: Integer; const AArray_Of_driverNameIDType: Array_Of_driverNameIDType);
    function  DriverNameID_Specified(Index: Integer): boolean;
    procedure SetTrailerName(Index: Integer; const AArray_Of_string: Array_Of_string);
    function  TrailerName_Specified(Index: Integer): boolean;
    procedure SetBrakingDistance(Index: Integer; const AdistanceType: distanceType);
    function  BrakingDistance_Specified(Index: Integer): boolean;
    procedure SetNrOfStops(Index: Integer; const AInt64: Int64);
    function  NrOfStops_Specified(Index: Integer): boolean;
    procedure SetHandBrakeUsageCount(Index: Integer; const AInt64: Int64);
    function  HandBrakeUsageCount_Specified(Index: Integer): boolean;
    procedure SetMovingTime(Index: Integer; const AInt64: Int64);
    function  MovingTime_Specified(Index: Integer): boolean;
    procedure SetStopTimeEngineOn(Index: Integer; const AtimeType: timeType);
    function  StopTimeEngineOn_Specified(Index: Integer): boolean;
    procedure SetStopTimeEngineOff(Index: Integer; const AtimeType: timeType);
    function  StopTimeEngineOff_Specified(Index: Integer): boolean;
    procedure SetSpeedOver85PerTotalDistance(Index: Integer; const AspeedType: speedType);
    function  SpeedOver85PerTotalDistance_Specified(Index: Integer): boolean;
    procedure SetRetarderBrakingDistance(Index: Integer; const AdistanceType: distanceType);
    function  RetarderBrakingDistance_Specified(Index: Integer): boolean;
    procedure SetTotalCoastingDistance(Index: Integer; const AdistanceType: distanceType);
    function  TotalCoastingDistance_Specified(Index: Integer): boolean;
    procedure SetTotalDistance(Index: Integer; const AdistanceType: distanceType);
    function  TotalDistance_Specified(Index: Integer): boolean;
    procedure SetCruiseControlDistance(Index: Integer; const AdistanceType: distanceType);
    function  CruiseControlDistance_Specified(Index: Integer): boolean;
    procedure SetAssistanceSystems(Index: Integer; const AassistanceSystems: assistanceSystems);
    function  AssistanceSystems_Specified(Index: Integer): boolean;
    procedure SetCharts(Index: Integer; const AChartsType: ChartsType);
    function  Charts_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property ID:                          Int64                      read FID write FID;
    property VehicleID:                   vehicleidType              read FVehicleID write FVehicleID;
    property DriverNameID:                Array_Of_driverNameIDType  Index (IS_OPTN or IS_UNBD) read FDriverNameID write SetDriverNameID stored DriverNameID_Specified;
    property TrailerName:                 Array_Of_string            Index (IS_OPTN or IS_UNBD) read FTrailerName write SetTrailerName stored TrailerName_Specified;
    property Period:                      Period8                    Index (IS_NLBL) read FPeriod write FPeriod;
    property Grades:                      Grades8                    Index (IS_NLBL) read FGrades write FGrades;
    property AvgValues:                   AvgValues6                 Index (IS_NLBL) read FAvgValues write FAvgValues;
    property Consumption:                 Consumption6               Index (IS_NLBL) read FConsumption write FConsumption;
    property PowerTakeOff:                PowerTakeOff3              Index (IS_NLBL) read FPowerTakeOff write FPowerTakeOff;
    property BrakingDistance:             distanceType               Index (IS_OPTN) read FBrakingDistance write SetBrakingDistance stored BrakingDistance_Specified;
    property NrOfStops:                   Int64                      Index (IS_OPTN) read FNrOfStops write SetNrOfStops stored NrOfStops_Specified;
    property HandBrakeUsageCount:         Int64                      Index (IS_OPTN) read FHandBrakeUsageCount write SetHandBrakeUsageCount stored HandBrakeUsageCount_Specified;
    property MovingTime:                  Int64                      Index (IS_OPTN) read FMovingTime write SetMovingTime stored MovingTime_Specified;
    property StopTimeEngineOn:            timeType                   Index (IS_OPTN) read FStopTimeEngineOn write SetStopTimeEngineOn stored StopTimeEngineOn_Specified;
    property StopTimeEngineOff:           timeType                   Index (IS_OPTN) read FStopTimeEngineOff write SetStopTimeEngineOff stored StopTimeEngineOff_Specified;
    property SpeedOver85PerTotalDistance: speedType                  Index (IS_OPTN) read FSpeedOver85PerTotalDistance write SetSpeedOver85PerTotalDistance stored SpeedOver85PerTotalDistance_Specified;
    property RetarderBrakingDistance:     distanceType               Index (IS_OPTN) read FRetarderBrakingDistance write SetRetarderBrakingDistance stored RetarderBrakingDistance_Specified;
    property TotalCoastingDistance:       distanceType               Index (IS_OPTN) read FTotalCoastingDistance write SetTotalCoastingDistance stored TotalCoastingDistance_Specified;
    property TotalDistance:               distanceType               Index (IS_OPTN) read FTotalDistance write SetTotalDistance stored TotalDistance_Specified;
    property CruiseControlDistance:       distanceType               Index (IS_OPTN) read FCruiseControlDistance write SetCruiseControlDistance stored CruiseControlDistance_Specified;
    property AssistanceSystems:           assistanceSystems          Index (IS_OPTN) read FAssistanceSystems write SetAssistanceSystems stored AssistanceSystems_Specified;
    property Charts:                      ChartsType                 Index (IS_OPTN) read FCharts write SetCharts stored Charts_Specified;
  end;



  // ************************************************************************ //
  // XML       : UniversalPerformanceAnalysisVehicleReport, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  UniversalPerformanceAnalysisVehicleReport = class(TRemotable)
  private
    FID: Int64;
    FVehicleID: vehicleidType;
    FDriverNameID: Array_Of_driverNameIDType;
    FDriverNameID_Specified: boolean;
    FTrailerName: Array_Of_string;
    FTrailerName_Specified: boolean;
    FPeriod: Period3;
    FGrades: Grades3;
    FAvgValues: AvgValues;
    FConsumption: Consumption;
    FTotalDistance: distanceType;
    FTotalDistance_Specified: boolean;
    FMovingTime: Int64;
    FMovingTime_Specified: boolean;
    FIdlingTimeEngineOn: timeType;
    FIdlingTimeEngineOn_Specified: boolean;
    FIdlingTimeEngineOff: timeType;
    FIdlingTimeEngineOff_Specified: boolean;
    FPTOTime: timeType;
    FPTOTime_Specified: boolean;
    FOverspeedTime: timeType;
    FOverspeedTime_Specified: boolean;
    FOverrevTime: timeType;
    FOverrevTime_Specified: boolean;
    FOverrevNumber: Int64;
    FOverrevNumber_Specified: boolean;
    FGreenBandRate: SmallInt;
    FGreenBandRate_Specified: boolean;
    FAccelerationNumber: Int64;
    FAccelerationNumber_Specified: boolean;
    FNumberOfStops: Int64;
    FNumberOfStops_Specified: boolean;
    FPTONumber: Int64;
    FPTONumber_Specified: boolean;
    FHarshBrakeNumber: Int64;
    FHarshBrakeNumber_Specified: boolean;
    FCharts: ChartsType;
    FCharts_Specified: boolean;
    procedure SetDriverNameID(Index: Integer; const AArray_Of_driverNameIDType: Array_Of_driverNameIDType);
    function  DriverNameID_Specified(Index: Integer): boolean;
    procedure SetTrailerName(Index: Integer; const AArray_Of_string: Array_Of_string);
    function  TrailerName_Specified(Index: Integer): boolean;
    procedure SetTotalDistance(Index: Integer; const AdistanceType: distanceType);
    function  TotalDistance_Specified(Index: Integer): boolean;
    procedure SetMovingTime(Index: Integer; const AInt64: Int64);
    function  MovingTime_Specified(Index: Integer): boolean;
    procedure SetIdlingTimeEngineOn(Index: Integer; const AtimeType: timeType);
    function  IdlingTimeEngineOn_Specified(Index: Integer): boolean;
    procedure SetIdlingTimeEngineOff(Index: Integer; const AtimeType: timeType);
    function  IdlingTimeEngineOff_Specified(Index: Integer): boolean;
    procedure SetPTOTime(Index: Integer; const AtimeType: timeType);
    function  PTOTime_Specified(Index: Integer): boolean;
    procedure SetOverspeedTime(Index: Integer; const AtimeType: timeType);
    function  OverspeedTime_Specified(Index: Integer): boolean;
    procedure SetOverrevTime(Index: Integer; const AtimeType: timeType);
    function  OverrevTime_Specified(Index: Integer): boolean;
    procedure SetOverrevNumber(Index: Integer; const AInt64: Int64);
    function  OverrevNumber_Specified(Index: Integer): boolean;
    procedure SetGreenBandRate(Index: Integer; const ASmallInt: SmallInt);
    function  GreenBandRate_Specified(Index: Integer): boolean;
    procedure SetAccelerationNumber(Index: Integer; const AInt64: Int64);
    function  AccelerationNumber_Specified(Index: Integer): boolean;
    procedure SetNumberOfStops(Index: Integer; const AInt64: Int64);
    function  NumberOfStops_Specified(Index: Integer): boolean;
    procedure SetPTONumber(Index: Integer; const AInt64: Int64);
    function  PTONumber_Specified(Index: Integer): boolean;
    procedure SetHarshBrakeNumber(Index: Integer; const AInt64: Int64);
    function  HarshBrakeNumber_Specified(Index: Integer): boolean;
    procedure SetCharts(Index: Integer; const AChartsType: ChartsType);
    function  Charts_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property ID:                  Int64                      read FID write FID;
    property VehicleID:           vehicleidType              read FVehicleID write FVehicleID;
    property DriverNameID:        Array_Of_driverNameIDType  Index (IS_OPTN or IS_UNBD) read FDriverNameID write SetDriverNameID stored DriverNameID_Specified;
    property TrailerName:         Array_Of_string            Index (IS_OPTN or IS_UNBD) read FTrailerName write SetTrailerName stored TrailerName_Specified;
    property Period:              Period3                    Index (IS_NLBL) read FPeriod write FPeriod;
    property Grades:              Grades3                    Index (IS_NLBL) read FGrades write FGrades;
    property AvgValues:           AvgValues                  Index (IS_NLBL) read FAvgValues write FAvgValues;
    property Consumption:         Consumption                Index (IS_NLBL) read FConsumption write FConsumption;
    property TotalDistance:       distanceType               Index (IS_OPTN) read FTotalDistance write SetTotalDistance stored TotalDistance_Specified;
    property MovingTime:          Int64                      Index (IS_OPTN) read FMovingTime write SetMovingTime stored MovingTime_Specified;
    property IdlingTimeEngineOn:  timeType                   Index (IS_OPTN) read FIdlingTimeEngineOn write SetIdlingTimeEngineOn stored IdlingTimeEngineOn_Specified;
    property IdlingTimeEngineOff: timeType                   Index (IS_OPTN) read FIdlingTimeEngineOff write SetIdlingTimeEngineOff stored IdlingTimeEngineOff_Specified;
    property PTOTime:             timeType                   Index (IS_OPTN) read FPTOTime write SetPTOTime stored PTOTime_Specified;
    property OverspeedTime:       timeType                   Index (IS_OPTN) read FOverspeedTime write SetOverspeedTime stored OverspeedTime_Specified;
    property OverrevTime:         timeType                   Index (IS_OPTN) read FOverrevTime write SetOverrevTime stored OverrevTime_Specified;
    property OverrevNumber:       Int64                      Index (IS_OPTN) read FOverrevNumber write SetOverrevNumber stored OverrevNumber_Specified;
    property GreenBandRate:       SmallInt                   Index (IS_OPTN) read FGreenBandRate write SetGreenBandRate stored GreenBandRate_Specified;
    property AccelerationNumber:  Int64                      Index (IS_OPTN) read FAccelerationNumber write SetAccelerationNumber stored AccelerationNumber_Specified;
    property NumberOfStops:       Int64                      Index (IS_OPTN) read FNumberOfStops write SetNumberOfStops stored NumberOfStops_Specified;
    property PTONumber:           Int64                      Index (IS_OPTN) read FPTONumber write SetPTONumber stored PTONumber_Specified;
    property HarshBrakeNumber:    Int64                      Index (IS_OPTN) read FHarshBrakeNumber write SetHarshBrakeNumber stored HarshBrakeNumber_Specified;
    property Charts:              ChartsType                 Index (IS_OPTN) read FCharts write SetCharts stored Charts_Specified;
  end;



  // ************************************************************************ //
  // XML       : GetUniversalPerformanceAnalysisDriverRequestType, global, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  GetUniversalPerformanceAnalysisDriverRequestType = class(TRemotable)
  private
    Fsessionid: sessionidType;
    Fsessionid_Specified: boolean;
    Flimit: limitType;
    Flimit_Specified: boolean;
    Foffset: offsetType;
    Foffset_Specified: boolean;
    Fversion: versionType;
    Fversion_Specified: boolean;
    FGetCharts: getChartsType;
    FGetCharts_Specified: boolean;
    FDriverNameID: driverNameIDType;
    FDriverNameID_Specified: boolean;
    FDriverGroupID: driverGroupIDType;
    FDriverGroupID_Specified: boolean;
    FAnalysis: Analysis5;
    FDrivingStyle: MinMaxGradeType;
    FDrivingStyle_Specified: boolean;
    FIdleTimeGrading: MinMaxGradeType;
    FIdleTimeGrading_Specified: boolean;
    FOverrevNumberGrading: MinMaxGradeType;
    FOverrevNumberGrading_Specified: boolean;
    FOverrevTimeGrading: MinMaxGradeType;
    FOverrevTimeGrading_Specified: boolean;
    FEcoDrivingGrading: MinMaxGradeType;
    FEcoDrivingGrading_Specified: boolean;
    FHarshBrakeGrading: MinMaxGradeType;
    FHarshBrakeGrading_Specified: boolean;
    FOverspeedTimeGrading: MinMaxGradeType;
    FOverspeedTimeGrading_Specified: boolean;
    FAvgTotalConsumption: MinMaxConsumptionType;
    FAvgTotalConsumption_Specified: boolean;
    procedure Setsessionid(Index: Integer; const AsessionidType: sessionidType);
    function  sessionid_Specified(Index: Integer): boolean;
    procedure Setlimit(Index: Integer; const AlimitType: limitType);
    function  limit_Specified(Index: Integer): boolean;
    procedure Setoffset(Index: Integer; const AoffsetType: offsetType);
    function  offset_Specified(Index: Integer): boolean;
    procedure Setversion(Index: Integer; const AversionType: versionType);
    function  version_Specified(Index: Integer): boolean;
    procedure SetGetCharts(Index: Integer; const AgetChartsType: getChartsType);
    function  GetCharts_Specified(Index: Integer): boolean;
    procedure SetDriverNameID(Index: Integer; const AdriverNameIDType: driverNameIDType);
    function  DriverNameID_Specified(Index: Integer): boolean;
    procedure SetDriverGroupID(Index: Integer; const AdriverGroupIDType: driverGroupIDType);
    function  DriverGroupID_Specified(Index: Integer): boolean;
    procedure SetDrivingStyle(Index: Integer; const AMinMaxGradeType: MinMaxGradeType);
    function  DrivingStyle_Specified(Index: Integer): boolean;
    procedure SetIdleTimeGrading(Index: Integer; const AMinMaxGradeType: MinMaxGradeType);
    function  IdleTimeGrading_Specified(Index: Integer): boolean;
    procedure SetOverrevNumberGrading(Index: Integer; const AMinMaxGradeType: MinMaxGradeType);
    function  OverrevNumberGrading_Specified(Index: Integer): boolean;
    procedure SetOverrevTimeGrading(Index: Integer; const AMinMaxGradeType: MinMaxGradeType);
    function  OverrevTimeGrading_Specified(Index: Integer): boolean;
    procedure SetEcoDrivingGrading(Index: Integer; const AMinMaxGradeType: MinMaxGradeType);
    function  EcoDrivingGrading_Specified(Index: Integer): boolean;
    procedure SetHarshBrakeGrading(Index: Integer; const AMinMaxGradeType: MinMaxGradeType);
    function  HarshBrakeGrading_Specified(Index: Integer): boolean;
    procedure SetOverspeedTimeGrading(Index: Integer; const AMinMaxGradeType: MinMaxGradeType);
    function  OverspeedTimeGrading_Specified(Index: Integer): boolean;
    procedure SetAvgTotalConsumption(Index: Integer; const AMinMaxConsumptionType: MinMaxConsumptionType);
    function  AvgTotalConsumption_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property sessionid:            sessionidType          Index (IS_ATTR or IS_OPTN) read Fsessionid write Setsessionid stored sessionid_Specified;
    property limit:                limitType              Index (IS_ATTR or IS_OPTN) read Flimit write Setlimit stored limit_Specified;
    property offset:               offsetType             Index (IS_ATTR or IS_OPTN) read Foffset write Setoffset stored offset_Specified;
    property version:              versionType            Index (IS_ATTR or IS_OPTN) read Fversion write Setversion stored version_Specified;
    property GetCharts:            getChartsType          Index (IS_OPTN) read FGetCharts write SetGetCharts stored GetCharts_Specified;
    property DriverNameID:         driverNameIDType       Index (IS_OPTN) read FDriverNameID write SetDriverNameID stored DriverNameID_Specified;
    property DriverGroupID:        driverGroupIDType      Index (IS_OPTN) read FDriverGroupID write SetDriverGroupID stored DriverGroupID_Specified;
    property Analysis:             Analysis5              read FAnalysis write FAnalysis;
    property DrivingStyle:         MinMaxGradeType        Index (IS_OPTN) read FDrivingStyle write SetDrivingStyle stored DrivingStyle_Specified;
    property IdleTimeGrading:      MinMaxGradeType        Index (IS_OPTN) read FIdleTimeGrading write SetIdleTimeGrading stored IdleTimeGrading_Specified;
    property OverrevNumberGrading: MinMaxGradeType        Index (IS_OPTN) read FOverrevNumberGrading write SetOverrevNumberGrading stored OverrevNumberGrading_Specified;
    property OverrevTimeGrading:   MinMaxGradeType        Index (IS_OPTN) read FOverrevTimeGrading write SetOverrevTimeGrading stored OverrevTimeGrading_Specified;
    property EcoDrivingGrading:    MinMaxGradeType        Index (IS_OPTN) read FEcoDrivingGrading write SetEcoDrivingGrading stored EcoDrivingGrading_Specified;
    property HarshBrakeGrading:    MinMaxGradeType        Index (IS_OPTN) read FHarshBrakeGrading write SetHarshBrakeGrading stored HarshBrakeGrading_Specified;
    property OverspeedTimeGrading: MinMaxGradeType        Index (IS_OPTN) read FOverspeedTimeGrading write SetOverspeedTimeGrading stored OverspeedTimeGrading_Specified;
    property AvgTotalConsumption:  MinMaxConsumptionType  Index (IS_OPTN) read FAvgTotalConsumption write SetAvgTotalConsumption stored AvgTotalConsumption_Specified;
  end;



  // ************************************************************************ //
  // XML       : GetUniversalPerformanceAnalysisDriverRequest, global, <element>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  GetUniversalPerformanceAnalysisDriverRequest = class(GetUniversalPerformanceAnalysisDriverRequestType)
  private
  published
  end;



  // ************************************************************************ //
  // XML       : MBPerformanceAnalysisDriverReport, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  MBPerformanceAnalysisDriverReport = class(TRemotable)
  private
    FID: Int64;
    FVehicleID: Array_Of_vehicleidType;
    FDriverNameID: driverNameIDType;
    FContainsVehicleDataSetList: ContainsVehicleDataSetList;
    FPeriod: Period4;
    FGrades: Grades4;
    FAvgValues: AvgValues2;
    FConsumption: Consumption3;
    FPowerTakeOff: PowerTakeOff2;
    FBrakingDistance: distanceType;
    FBrakingDistance_Specified: boolean;
    FNrOfStops: Int64;
    FNrOfStops_Specified: boolean;
    FHandBrakeUsageCount: Int64;
    FHandBrakeUsageCount_Specified: boolean;
    FMovingTime: Int64;
    FMovingTime_Specified: boolean;
    FStopTimeEngineOn: timeType;
    FStopTimeEngineOn_Specified: boolean;
    FStopTimeEngineOff: timeType;
    FStopTimeEngineOff_Specified: boolean;
    FSpeedOver85PerTotalDistance: speedType;
    FSpeedOver85PerTotalDistance_Specified: boolean;
    FRetarderBrakingDistance: distanceType;
    FRetarderBrakingDistance_Specified: boolean;
    FTotalCoastingDistance: distanceType;
    FTotalCoastingDistance_Specified: boolean;
    FTotalDistance: distanceType;
    FTotalDistance_Specified: boolean;
    FCruiseControlDistance: distanceType;
    FCruiseControlDistance_Specified: boolean;
    FAssistanceSystems: assistanceSystems;
    FAssistanceSystems_Specified: boolean;
    FVehicleDataSet: Array_Of_VehicleDataSet;
    FVehicleDataSet_Specified: boolean;
    FCharts: ChartsType;
    FCharts_Specified: boolean;
    procedure SetBrakingDistance(Index: Integer; const AdistanceType: distanceType);
    function  BrakingDistance_Specified(Index: Integer): boolean;
    procedure SetNrOfStops(Index: Integer; const AInt64: Int64);
    function  NrOfStops_Specified(Index: Integer): boolean;
    procedure SetHandBrakeUsageCount(Index: Integer; const AInt64: Int64);
    function  HandBrakeUsageCount_Specified(Index: Integer): boolean;
    procedure SetMovingTime(Index: Integer; const AInt64: Int64);
    function  MovingTime_Specified(Index: Integer): boolean;
    procedure SetStopTimeEngineOn(Index: Integer; const AtimeType: timeType);
    function  StopTimeEngineOn_Specified(Index: Integer): boolean;
    procedure SetStopTimeEngineOff(Index: Integer; const AtimeType: timeType);
    function  StopTimeEngineOff_Specified(Index: Integer): boolean;
    procedure SetSpeedOver85PerTotalDistance(Index: Integer; const AspeedType: speedType);
    function  SpeedOver85PerTotalDistance_Specified(Index: Integer): boolean;
    procedure SetRetarderBrakingDistance(Index: Integer; const AdistanceType: distanceType);
    function  RetarderBrakingDistance_Specified(Index: Integer): boolean;
    procedure SetTotalCoastingDistance(Index: Integer; const AdistanceType: distanceType);
    function  TotalCoastingDistance_Specified(Index: Integer): boolean;
    procedure SetTotalDistance(Index: Integer; const AdistanceType: distanceType);
    function  TotalDistance_Specified(Index: Integer): boolean;
    procedure SetCruiseControlDistance(Index: Integer; const AdistanceType: distanceType);
    function  CruiseControlDistance_Specified(Index: Integer): boolean;
    procedure SetAssistanceSystems(Index: Integer; const AassistanceSystems: assistanceSystems);
    function  AssistanceSystems_Specified(Index: Integer): boolean;
    procedure SetVehicleDataSet(Index: Integer; const AArray_Of_VehicleDataSet: Array_Of_VehicleDataSet);
    function  VehicleDataSet_Specified(Index: Integer): boolean;
    procedure SetCharts(Index: Integer; const AChartsType: ChartsType);
    function  Charts_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property ID:                          Int64                       read FID write FID;
    property VehicleID:                   Array_Of_vehicleidType      Index (IS_UNBD) read FVehicleID write FVehicleID;
    property DriverNameID:                driverNameIDType            read FDriverNameID write FDriverNameID;
    property ContainsVehicleDataSetList:  ContainsVehicleDataSetList  Index (IS_REF) read FContainsVehicleDataSetList write FContainsVehicleDataSetList;
    property Period:                      Period4                     Index (IS_NLBL) read FPeriod write FPeriod;
    property Grades:                      Grades4                     Index (IS_NLBL) read FGrades write FGrades;
    property AvgValues:                   AvgValues2                  Index (IS_NLBL) read FAvgValues write FAvgValues;
    property Consumption:                 Consumption3                Index (IS_NLBL) read FConsumption write FConsumption;
    property PowerTakeOff:                PowerTakeOff2               Index (IS_NLBL) read FPowerTakeOff write FPowerTakeOff;
    property BrakingDistance:             distanceType                Index (IS_OPTN) read FBrakingDistance write SetBrakingDistance stored BrakingDistance_Specified;
    property NrOfStops:                   Int64                       Index (IS_OPTN) read FNrOfStops write SetNrOfStops stored NrOfStops_Specified;
    property HandBrakeUsageCount:         Int64                       Index (IS_OPTN) read FHandBrakeUsageCount write SetHandBrakeUsageCount stored HandBrakeUsageCount_Specified;
    property MovingTime:                  Int64                       Index (IS_OPTN) read FMovingTime write SetMovingTime stored MovingTime_Specified;
    property StopTimeEngineOn:            timeType                    Index (IS_OPTN) read FStopTimeEngineOn write SetStopTimeEngineOn stored StopTimeEngineOn_Specified;
    property StopTimeEngineOff:           timeType                    Index (IS_OPTN) read FStopTimeEngineOff write SetStopTimeEngineOff stored StopTimeEngineOff_Specified;
    property SpeedOver85PerTotalDistance: speedType                   Index (IS_OPTN) read FSpeedOver85PerTotalDistance write SetSpeedOver85PerTotalDistance stored SpeedOver85PerTotalDistance_Specified;
    property RetarderBrakingDistance:     distanceType                Index (IS_OPTN) read FRetarderBrakingDistance write SetRetarderBrakingDistance stored RetarderBrakingDistance_Specified;
    property TotalCoastingDistance:       distanceType                Index (IS_OPTN) read FTotalCoastingDistance write SetTotalCoastingDistance stored TotalCoastingDistance_Specified;
    property TotalDistance:               distanceType                Index (IS_OPTN) read FTotalDistance write SetTotalDistance stored TotalDistance_Specified;
    property CruiseControlDistance:       distanceType                Index (IS_OPTN) read FCruiseControlDistance write SetCruiseControlDistance stored CruiseControlDistance_Specified;
    property AssistanceSystems:           assistanceSystems           Index (IS_OPTN) read FAssistanceSystems write SetAssistanceSystems stored AssistanceSystems_Specified;
    property VehicleDataSet:              Array_Of_VehicleDataSet     Index (IS_OPTN or IS_UNBD) read FVehicleDataSet write SetVehicleDataSet stored VehicleDataSet_Specified;
    property Charts:                      ChartsType                  Index (IS_OPTN) read FCharts write SetCharts stored Charts_Specified;
  end;



  // ************************************************************************ //
  // XML       : UniversalPerformanceAnalysisDriverReport, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  UniversalPerformanceAnalysisDriverReport = class(TRemotable)
  private
    FID: Int64;
    FVehicleID: Array_Of_vehicleidType;
    FContainsVehicleDataSetList: ContainsVehicleDataSetList;
    FDriverNameID: driverNameIDType;
    FPeriod: Period6;
    FGrades: Grades6;
    FAvgValues: AvgValues4;
    FConsumption: Consumption4;
    FTotalDistance: distanceType;
    FTotalDistance_Specified: boolean;
    FMovingTime: Int64;
    FMovingTime_Specified: boolean;
    FIdlingTimeEngineOn: timeType;
    FIdlingTimeEngineOn_Specified: boolean;
    FIdlingTimeEngineOff: timeType;
    FIdlingTimeEngineOff_Specified: boolean;
    FPTOTime: timeType;
    FPTOTime_Specified: boolean;
    FOverspeedTime: timeType;
    FOverspeedTime_Specified: boolean;
    FOverrevTime: timeType;
    FOverrevTime_Specified: boolean;
    FOverrevNumber: Int64;
    FOverrevNumber_Specified: boolean;
    FGreenBandRate: SmallInt;
    FGreenBandRate_Specified: boolean;
    FAccelerationNumber: Int64;
    FAccelerationNumber_Specified: boolean;
    FNumberOfStops: Int64;
    FNumberOfStops_Specified: boolean;
    FPTONumber: Int64;
    FPTONumber_Specified: boolean;
    FHarshBrakeNumber: Int64;
    FHarshBrakeNumber_Specified: boolean;
    FVehicleDataSet: Array_Of_VehicleDataSet2;
    FVehicleDataSet_Specified: boolean;
    FCharts: ChartsType;
    FCharts_Specified: boolean;
    procedure SetTotalDistance(Index: Integer; const AdistanceType: distanceType);
    function  TotalDistance_Specified(Index: Integer): boolean;
    procedure SetMovingTime(Index: Integer; const AInt64: Int64);
    function  MovingTime_Specified(Index: Integer): boolean;
    procedure SetIdlingTimeEngineOn(Index: Integer; const AtimeType: timeType);
    function  IdlingTimeEngineOn_Specified(Index: Integer): boolean;
    procedure SetIdlingTimeEngineOff(Index: Integer; const AtimeType: timeType);
    function  IdlingTimeEngineOff_Specified(Index: Integer): boolean;
    procedure SetPTOTime(Index: Integer; const AtimeType: timeType);
    function  PTOTime_Specified(Index: Integer): boolean;
    procedure SetOverspeedTime(Index: Integer; const AtimeType: timeType);
    function  OverspeedTime_Specified(Index: Integer): boolean;
    procedure SetOverrevTime(Index: Integer; const AtimeType: timeType);
    function  OverrevTime_Specified(Index: Integer): boolean;
    procedure SetOverrevNumber(Index: Integer; const AInt64: Int64);
    function  OverrevNumber_Specified(Index: Integer): boolean;
    procedure SetGreenBandRate(Index: Integer; const ASmallInt: SmallInt);
    function  GreenBandRate_Specified(Index: Integer): boolean;
    procedure SetAccelerationNumber(Index: Integer; const AInt64: Int64);
    function  AccelerationNumber_Specified(Index: Integer): boolean;
    procedure SetNumberOfStops(Index: Integer; const AInt64: Int64);
    function  NumberOfStops_Specified(Index: Integer): boolean;
    procedure SetPTONumber(Index: Integer; const AInt64: Int64);
    function  PTONumber_Specified(Index: Integer): boolean;
    procedure SetHarshBrakeNumber(Index: Integer; const AInt64: Int64);
    function  HarshBrakeNumber_Specified(Index: Integer): boolean;
    procedure SetVehicleDataSet(Index: Integer; const AArray_Of_VehicleDataSet2: Array_Of_VehicleDataSet2);
    function  VehicleDataSet_Specified(Index: Integer): boolean;
    procedure SetCharts(Index: Integer; const AChartsType: ChartsType);
    function  Charts_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property ID:                         Int64                       read FID write FID;
    property VehicleID:                  Array_Of_vehicleidType      Index (IS_UNBD) read FVehicleID write FVehicleID;
    property ContainsVehicleDataSetList: ContainsVehicleDataSetList  Index (IS_REF) read FContainsVehicleDataSetList write FContainsVehicleDataSetList;
    property DriverNameID:               driverNameIDType            read FDriverNameID write FDriverNameID;
    property Period:                     Period6                     Index (IS_NLBL) read FPeriod write FPeriod;
    property Grades:                     Grades6                     Index (IS_NLBL) read FGrades write FGrades;
    property AvgValues:                  AvgValues4                  Index (IS_NLBL) read FAvgValues write FAvgValues;
    property Consumption:                Consumption4                Index (IS_NLBL) read FConsumption write FConsumption;
    property TotalDistance:              distanceType                Index (IS_OPTN) read FTotalDistance write SetTotalDistance stored TotalDistance_Specified;
    property MovingTime:                 Int64                       Index (IS_OPTN) read FMovingTime write SetMovingTime stored MovingTime_Specified;
    property IdlingTimeEngineOn:         timeType                    Index (IS_OPTN) read FIdlingTimeEngineOn write SetIdlingTimeEngineOn stored IdlingTimeEngineOn_Specified;
    property IdlingTimeEngineOff:        timeType                    Index (IS_OPTN) read FIdlingTimeEngineOff write SetIdlingTimeEngineOff stored IdlingTimeEngineOff_Specified;
    property PTOTime:                    timeType                    Index (IS_OPTN) read FPTOTime write SetPTOTime stored PTOTime_Specified;
    property OverspeedTime:              timeType                    Index (IS_OPTN) read FOverspeedTime write SetOverspeedTime stored OverspeedTime_Specified;
    property OverrevTime:                timeType                    Index (IS_OPTN) read FOverrevTime write SetOverrevTime stored OverrevTime_Specified;
    property OverrevNumber:              Int64                       Index (IS_OPTN) read FOverrevNumber write SetOverrevNumber stored OverrevNumber_Specified;
    property GreenBandRate:              SmallInt                    Index (IS_OPTN) read FGreenBandRate write SetGreenBandRate stored GreenBandRate_Specified;
    property AccelerationNumber:         Int64                       Index (IS_OPTN) read FAccelerationNumber write SetAccelerationNumber stored AccelerationNumber_Specified;
    property NumberOfStops:              Int64                       Index (IS_OPTN) read FNumberOfStops write SetNumberOfStops stored NumberOfStops_Specified;
    property PTONumber:                  Int64                       Index (IS_OPTN) read FPTONumber write SetPTONumber stored PTONumber_Specified;
    property HarshBrakeNumber:           Int64                       Index (IS_OPTN) read FHarshBrakeNumber write SetHarshBrakeNumber stored HarshBrakeNumber_Specified;
    property VehicleDataSet:             Array_Of_VehicleDataSet2    Index (IS_OPTN or IS_UNBD) read FVehicleDataSet write SetVehicleDataSet stored VehicleDataSet_Specified;
    property Charts:                     ChartsType                  Index (IS_OPTN) read FCharts write SetCharts stored Charts_Specified;
  end;



  // ************************************************************************ //
  // XML       : GetMBPerformanceAnalysisDriverOverviewResponseType, global, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  GetMBPerformanceAnalysisDriverOverviewResponseType = class(TRemotable)
  private
    Flimit: limitType;
    Flimit_Specified: boolean;
    Foffset: offsetType;
    Foffset_Specified: boolean;
    FresultSize: resultSizeType;
    FresultSize_Specified: boolean;
    FresponseSize: responseSizeType;
    FresponseSize_Specified: boolean;
    Fversion: versionType;
    Fversion_Specified: boolean;
    FMBPerformanceAnalysisDriverOverviewReport: Array_Of_MBPerformanceAnalysisDriverOverviewReport;
    FMBPerformanceAnalysisDriverOverviewReport_Specified: boolean;
    procedure Setlimit(Index: Integer; const AlimitType: limitType);
    function  limit_Specified(Index: Integer): boolean;
    procedure Setoffset(Index: Integer; const AoffsetType: offsetType);
    function  offset_Specified(Index: Integer): boolean;
    procedure SetresultSize(Index: Integer; const AresultSizeType: resultSizeType);
    function  resultSize_Specified(Index: Integer): boolean;
    procedure SetresponseSize(Index: Integer; const AresponseSizeType: responseSizeType);
    function  responseSize_Specified(Index: Integer): boolean;
    procedure Setversion(Index: Integer; const AversionType: versionType);
    function  version_Specified(Index: Integer): boolean;
    procedure SetMBPerformanceAnalysisDriverOverviewReport(Index: Integer; const AArray_Of_MBPerformanceAnalysisDriverOverviewReport: Array_Of_MBPerformanceAnalysisDriverOverviewReport);
    function  MBPerformanceAnalysisDriverOverviewReport_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property limit:                                     limitType                                           Index (IS_ATTR or IS_OPTN) read Flimit write Setlimit stored limit_Specified;
    property offset:                                    offsetType                                          Index (IS_ATTR or IS_OPTN) read Foffset write Setoffset stored offset_Specified;
    property resultSize:                                resultSizeType                                      Index (IS_ATTR or IS_OPTN) read FresultSize write SetresultSize stored resultSize_Specified;
    property responseSize:                              responseSizeType                                    Index (IS_ATTR or IS_OPTN) read FresponseSize write SetresponseSize stored responseSize_Specified;
    property version:                                   versionType                                         Index (IS_ATTR or IS_OPTN) read Fversion write Setversion stored version_Specified;
    property MBPerformanceAnalysisDriverOverviewReport: Array_Of_MBPerformanceAnalysisDriverOverviewReport  Index (IS_OPTN or IS_UNBD) read FMBPerformanceAnalysisDriverOverviewReport write SetMBPerformanceAnalysisDriverOverviewReport stored MBPerformanceAnalysisDriverOverviewReport_Specified;
  end;



  // ************************************************************************ //
  // XML       : GetMBPerformanceAnalysisDriverOverviewResponse, global, <element>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  GetMBPerformanceAnalysisDriverOverviewResponse = class(GetMBPerformanceAnalysisDriverOverviewResponseType)
  private
  published
  end;



  // ************************************************************************ //
  // XML       : PredefinedPeriod, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  PredefinedPeriod8 = class(TRemotable)
  private
    FYear: yearType;
    FYear_Specified: boolean;
    FInterval: period1Type;
    FInterval_Specified: boolean;
    FCalendarWeek: calendarWeekType;
    FCalendarWeek_Specified: boolean;
    procedure SetYear(Index: Integer; const AyearType: yearType);
    function  Year_Specified(Index: Integer): boolean;
    procedure SetInterval(Index: Integer; const Aperiod1Type: period1Type);
    function  Interval_Specified(Index: Integer): boolean;
    procedure SetCalendarWeek(Index: Integer; const AcalendarWeekType: calendarWeekType);
    function  CalendarWeek_Specified(Index: Integer): boolean;
  published
    property Year:         yearType          Index (IS_OPTN) read FYear write SetYear stored Year_Specified;
    property Interval:     period1Type       Index (IS_OPTN) read FInterval write SetInterval stored Interval_Specified;
    property CalendarWeek: calendarWeekType  Index (IS_OPTN) read FCalendarWeek write SetCalendarWeek stored CalendarWeek_Specified;
  end;



  // ************************************************************************ //
  // XML       : PeriodBreakDown, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  PeriodBreakDown8 = class(TRemotable)
  private
    FStartYear: yearType;
    FEndYear: yearType;
    FInterval: period2Type;
  published
    property StartYear: yearType     read FStartYear write FStartYear;
    property EndYear:   yearType     read FEndYear write FEndYear;
    property Interval:  period2Type  read FInterval write FInterval;
  end;


  // ************************************************************************ //
  // Namespace : http://ws.fleetboard.com/PerformanceAnalysisService
  // soapAction: |getMBPerformanceAnalysisVehicle|getMBPerformanceAnalysisDriver|getUniversalPerformanceAnalysisVehicle|getUniversalPerformanceAnalysisDriver|getUniversalPerformanceAnalysisVehicleTimePeriodOverview|getUniversalPerformanceAnalysisDriverTimePeriodOverview|getMBPerformanceAnalysisVehicleTimePeriodOverview|getMBPerformanceAnalysisDriverTimePeriodOverview
  // transport : http://schemas.xmlsoap.org/soap/http
  // style     : document
  // use       : literal
  // binding   : PerformanceAnalysisService
  // service   : PerformanceAnalysisService
  // port      : PerformanceAnalysisService
  // URL       : http://www.fleetboard.com/vmsoap_v1_1/services/PerformanceAnalysisService
  // ************************************************************************ //
  PerformanceAnalysisServiceInterface = interface(IInvokable)
  ['{F21D5004-13D3-084C-400A-137021CAE5B7}']
    function  getMBPerformanceAnalysisVehicle(const GetMBPerformanceAnalysisVehicleRequest: GetMBPerformanceAnalysisVehicleRequest): GetMBPerformanceAnalysisVehicleResponse; stdcall;
    function  getMBPerformanceAnalysisDriver(const GetMBPerformanceAnalysisDriverRequest: GetMBPerformanceAnalysisDriverRequest): GetMBPerformanceAnalysisDriverResponse; stdcall;
    function  getUniversalPerformanceAnalysisVehicle(const GetUniversalPerformanceAnalysisVehicleRequest: GetUniversalPerformanceAnalysisVehicleRequest): GetUniversalPerformanceAnalysisVehicleResponse; stdcall;
    function  getUniversalPerformanceAnalysisDriver(const GetUniversalPerformanceAnalysisDriverRequest: GetUniversalPerformanceAnalysisDriverRequest): GetUniversalPerformanceAnalysisDriverResponse; stdcall;
    function  getUniversalPerformanceAnalysisVehicleOverview(const GetUniversalPerformanceAnalysisVehicleOverviewRequest: GetUniversalPerformanceAnalysisVehicleOverviewRequest): GetUniversalPerformanceAnalysisVehicleOverviewResponse; stdcall;
    function  getUniversalPerformanceAnalysisDriverOverview(const GetUniversalPerformanceAnalysisDriverOverviewRequest: GetUniversalPerformanceAnalysisDriverOverviewRequest): GetUniversalPerformanceAnalysisDriverOverviewResponse; stdcall;
    function  getMBPerformanceAnalysisVehicleOverview(const GetMBPerformanceAnalysisVehicleOverviewRequest: GetMBPerformanceAnalysisVehicleOverviewRequest): GetMBPerformanceAnalysisVehicleOverviewResponse; stdcall;
    function  getMBPerformanceAnalysisDriverOverview(const GetMBPerformanceAnalysisDriverOverviewRequest: GetMBPerformanceAnalysisDriverOverviewRequest): GetMBPerformanceAnalysisDriverOverviewResponse; stdcall;
  end;

function GetPerformanceAnalysisServiceInterface(UseWSDL: Boolean=System.False; Addr: string=''; HTTPRIO: THTTPRIO = nil): PerformanceAnalysisServiceInterface;


implementation
  uses SysUtils;

function GetPerformanceAnalysisServiceInterface(UseWSDL: Boolean; Addr: string; HTTPRIO: THTTPRIO): PerformanceAnalysisServiceInterface;
const
  defWSDL = 'P:\SRC\FUVAR_ADO\4.20\WSDL\Fleetboard_PerformanceAnalysisService.wsdl';
  defURL  = 'http://www.fleetboard.com/vmsoap_v1_1/services/PerformanceAnalysisService';
  defSvc  = 'PerformanceAnalysisService';
  defPrt  = 'PerformanceAnalysisService';
var
  RIO: THTTPRIO;
begin
  Result := nil;
  if (Addr = '') then
  begin
    if UseWSDL then
      Addr := defWSDL
    else
      Addr := defURL;
  end;
  if HTTPRIO = nil then
    RIO := THTTPRIO.Create(nil)
  else
    RIO := HTTPRIO;
  try
    Result := (RIO as PerformanceAnalysisServiceInterface);
    if UseWSDL then
    begin
      RIO.WSDLLocation := Addr;
      RIO.Service := defSvc;
      RIO.Port := defPrt;
    end else
      RIO.URL := Addr;
  finally
    if (Result = nil) and (HTTPRIO = nil) then
      RIO.Free;
  end;
end;


destructor ToursSummary.Destroy;
begin
  SysUtils.FreeAndNil(FTimeRange);
  inherited Destroy;
end;

destructor ToursSummary2.Destroy;
begin
  SysUtils.FreeAndNil(FTimeRange);
  inherited Destroy;
end;

destructor cruiseControlModes.Destroy;
begin
  SysUtils.FreeAndNil(FCruiseCtrlWithPPC);
  SysUtils.FreeAndNil(FCruiseCtrlWithoutPPC);
  SysUtils.FreeAndNil(FCruiseCtrlOff);
  SysUtils.FreeAndNil(FPPC);
  SysUtils.FreeAndNil(FLimiter);
  inherited Destroy;
end;

procedure cruiseControlModes.SetCruiseCtrlWithPPC(Index: Integer; const AcruiseCtrlWithPPC: cruiseCtrlWithPPC);
begin
  FCruiseCtrlWithPPC := AcruiseCtrlWithPPC;
  FCruiseCtrlWithPPC_Specified := True;
end;

function cruiseControlModes.CruiseCtrlWithPPC_Specified(Index: Integer): boolean;
begin
  Result := FCruiseCtrlWithPPC_Specified;
end;

procedure cruiseControlModes.SetCruiseCtrlWithoutPPC(Index: Integer; const AcruiseCtrlWithoutPPC: cruiseCtrlWithoutPPC);
begin
  FCruiseCtrlWithoutPPC := AcruiseCtrlWithoutPPC;
  FCruiseCtrlWithoutPPC_Specified := True;
end;

function cruiseControlModes.CruiseCtrlWithoutPPC_Specified(Index: Integer): boolean;
begin
  Result := FCruiseCtrlWithoutPPC_Specified;
end;

procedure cruiseControlModes.SetCruiseCtrlOff(Index: Integer; const AcruiseCtrlOff: cruiseCtrlOff);
begin
  FCruiseCtrlOff := AcruiseCtrlOff;
  FCruiseCtrlOff_Specified := True;
end;

function cruiseControlModes.CruiseCtrlOff_Specified(Index: Integer): boolean;
begin
  Result := FCruiseCtrlOff_Specified;
end;

procedure cruiseControlModes.SetPPC(Index: Integer; const Appc: ppc);
begin
  FPPC := Appc;
  FPPC_Specified := True;
end;

function cruiseControlModes.PPC_Specified(Index: Integer): boolean;
begin
  Result := FPPC_Specified;
end;

procedure cruiseControlModes.SetLimiter(Index: Integer; const Alimiter: limiter);
begin
  FLimiter := Alimiter;
  FLimiter_Specified := True;
end;

function cruiseControlModes.Limiter_Specified(Index: Integer): boolean;
begin
  Result := FLimiter_Specified;
end;

destructor assistanceSystems.Destroy;
begin
  SysUtils.FreeAndNil(FKickDown);
  SysUtils.FreeAndNil(FEcoRoll);
  SysUtils.FreeAndNil(FDrivingPrograms);
  SysUtils.FreeAndNil(FCruiseControlModes);
  inherited Destroy;
end;

procedure assistanceSystems.SetKickDown(Index: Integer; const AkickDown: kickDown);
begin
  FKickDown := AkickDown;
  FKickDown_Specified := True;
end;

function assistanceSystems.KickDown_Specified(Index: Integer): boolean;
begin
  Result := FKickDown_Specified;
end;

procedure assistanceSystems.SetEcoRoll(Index: Integer; const AecoRoll: ecoRoll);
begin
  FEcoRoll := AecoRoll;
  FEcoRoll_Specified := True;
end;

function assistanceSystems.EcoRoll_Specified(Index: Integer): boolean;
begin
  Result := FEcoRoll_Specified;
end;

procedure assistanceSystems.SetDrivingPrograms(Index: Integer; const AdrivingPrograms: drivingPrograms);
begin
  FDrivingPrograms := AdrivingPrograms;
  FDrivingPrograms_Specified := True;
end;

function assistanceSystems.DrivingPrograms_Specified(Index: Integer): boolean;
begin
  Result := FDrivingPrograms_Specified;
end;

procedure assistanceSystems.SetCruiseControlModes(Index: Integer; const AcruiseControlModes: cruiseControlModes);
begin
  FCruiseControlModes := AcruiseControlModes;
  FCruiseControlModes_Specified := True;
end;

function assistanceSystems.CruiseControlModes_Specified(Index: Integer): boolean;
begin
  Result := FCruiseControlModes_Specified;
end;

procedure PowerTakeOff2.SetPto1(Index: Integer; const AtimeType: timeType);
begin
  FPto1 := AtimeType;
  FPto1_Specified := True;
end;

function PowerTakeOff2.Pto1_Specified(Index: Integer): boolean;
begin
  Result := FPto1_Specified;
end;

procedure PowerTakeOff2.SetPto2(Index: Integer; const AtimeType: timeType);
begin
  FPto2 := AtimeType;
  FPto2_Specified := True;
end;

function PowerTakeOff2.Pto2_Specified(Index: Integer): boolean;
begin
  Result := FPto2_Specified;
end;

procedure PowerTakeOff2.SetPto3(Index: Integer; const AtimeType: timeType);
begin
  FPto3 := AtimeType;
  FPto3_Specified := True;
end;

function PowerTakeOff2.Pto3_Specified(Index: Integer): boolean;
begin
  Result := FPto3_Specified;
end;

procedure VehicleStateTimeValuesType.SetMovingTime(Index: Integer; const AtimeType: timeType);
begin
  FMovingTime := AtimeType;
  FMovingTime_Specified := True;
end;

function VehicleStateTimeValuesType.MovingTime_Specified(Index: Integer): boolean;
begin
  Result := FMovingTime_Specified;
end;

procedure VehicleStateTimeValuesType.SetIdleTimeEngineOn(Index: Integer; const AtimeType: timeType);
begin
  FIdleTimeEngineOn := AtimeType;
  FIdleTimeEngineOn_Specified := True;
end;

function VehicleStateTimeValuesType.IdleTimeEngineOn_Specified(Index: Integer): boolean;
begin
  Result := FIdleTimeEngineOn_Specified;
end;

procedure Cells.SetnumberOfRevolutions(Index: Integer; const AInteger: Integer);
begin
  FnumberOfRevolutions := AInteger;
  FnumberOfRevolutions_Specified := True;
end;

function Cells.numberOfRevolutions_Specified(Index: Integer): boolean;
begin
  Result := FnumberOfRevolutions_Specified;
end;

procedure ConsumptionValuesType.SetDriveConsumption(Index: Integer; const AconsumptionType: consumptionType);
begin
  FDriveConsumption := AconsumptionType;
  FDriveConsumption_Specified := True;
end;

function ConsumptionValuesType.DriveConsumption_Specified(Index: Integer): boolean;
begin
  Result := FDriveConsumption_Specified;
end;

procedure ConsumptionValuesType.SetTotalConsumption(Index: Integer; const AconsumptionType: consumptionType);
begin
  FTotalConsumption := AconsumptionType;
  FTotalConsumption_Specified := True;
end;

function ConsumptionValuesType.TotalConsumption_Specified(Index: Integer): boolean;
begin
  Result := FTotalConsumption_Specified;
end;

procedure ConsumptionValuesType.SetIdleConsumption(Index: Integer; const AconsumptionType: consumptionType);
begin
  FIdleConsumption := AconsumptionType;
  FIdleConsumption_Specified := True;
end;

function ConsumptionValuesType.IdleConsumption_Specified(Index: Integer): boolean;
begin
  Result := FIdleConsumption_Specified;
end;

procedure ConsumptionValuesType.SetAdBlueConsumption(Index: Integer; const AconsumptionType: consumptionType);
begin
  FAdBlueConsumption := AconsumptionType;
  FAdBlueConsumption_Specified := True;
end;

function ConsumptionValuesType.AdBlueConsumption_Specified(Index: Integer): boolean;
begin
  Result := FAdBlueConsumption_Specified;
end;

procedure ConsumptionValuesType.SetIdleConsumptionPTOActive(Index: Integer; const AconsumptionType: consumptionType);
begin
  FIdleConsumptionPTOActive := AconsumptionType;
  FIdleConsumptionPTOActive_Specified := True;
end;

function ConsumptionValuesType.IdleConsumptionPTOActive_Specified(Index: Integer): boolean;
begin
  Result := FIdleConsumptionPTOActive_Specified;
end;

procedure ConsumptionValuesType.SetIdleConsumptionPTOInactive(Index: Integer; const AconsumptionType: consumptionType);
begin
  FIdleConsumptionPTOInactive := AconsumptionType;
  FIdleConsumptionPTOInactive_Specified := True;
end;

function ConsumptionValuesType.IdleConsumptionPTOInactive_Specified(Index: Integer): boolean;
begin
  Result := FIdleConsumptionPTOInactive_Specified;
end;

procedure Consumption.SetDriveConsumption(Index: Integer; const AconsumptionType: consumptionType);
begin
  FDriveConsumption := AconsumptionType;
  FDriveConsumption_Specified := True;
end;

function Consumption.DriveConsumption_Specified(Index: Integer): boolean;
begin
  Result := FDriveConsumption_Specified;
end;

procedure Consumption.SetTotalConsumption(Index: Integer; const AconsumptionType: consumptionType);
begin
  FTotalConsumption := AconsumptionType;
  FTotalConsumption_Specified := True;
end;

function Consumption.TotalConsumption_Specified(Index: Integer): boolean;
begin
  Result := FTotalConsumption_Specified;
end;

procedure Consumption.SetStandConsumption(Index: Integer; const AconsumptionType: consumptionType);
begin
  FStandConsumption := AconsumptionType;
  FStandConsumption_Specified := True;
end;

function Consumption.StandConsumption_Specified(Index: Integer): boolean;
begin
  Result := FStandConsumption_Specified;
end;

procedure Consumption.SetPTOConsumption(Index: Integer; const AconsumptionType: consumptionType);
begin
  FPTOConsumption := AconsumptionType;
  FPTOConsumption_Specified := True;
end;

function Consumption.PTOConsumption_Specified(Index: Integer): boolean;
begin
  Result := FPTOConsumption_Specified;
end;

procedure Consumption2.SetDriveConsumption(Index: Integer; const AconsumptionType: consumptionType);
begin
  FDriveConsumption := AconsumptionType;
  FDriveConsumption_Specified := True;
end;

function Consumption2.DriveConsumption_Specified(Index: Integer): boolean;
begin
  Result := FDriveConsumption_Specified;
end;

procedure Consumption2.SetTotalConsumption(Index: Integer; const AconsumptionType: consumptionType);
begin
  FTotalConsumption := AconsumptionType;
  FTotalConsumption_Specified := True;
end;

function Consumption2.TotalConsumption_Specified(Index: Integer): boolean;
begin
  Result := FTotalConsumption_Specified;
end;

procedure Consumption2.SetIdleConsumption(Index: Integer; const AconsumptionType: consumptionType);
begin
  FIdleConsumption := AconsumptionType;
  FIdleConsumption_Specified := True;
end;

function Consumption2.IdleConsumption_Specified(Index: Integer): boolean;
begin
  Result := FIdleConsumption_Specified;
end;

procedure Consumption2.SetAdBlueConsumption(Index: Integer; const AconsumptionType: consumptionType);
begin
  FAdBlueConsumption := AconsumptionType;
  FAdBlueConsumption_Specified := True;
end;

function Consumption2.AdBlueConsumption_Specified(Index: Integer): boolean;
begin
  Result := FAdBlueConsumption_Specified;
end;

procedure Consumption2.SetIdleConsumptionPTOActive(Index: Integer; const AconsumptionType: consumptionType);
begin
  FIdleConsumptionPTOActive := AconsumptionType;
  FIdleConsumptionPTOActive_Specified := True;
end;

function Consumption2.IdleConsumptionPTOActive_Specified(Index: Integer): boolean;
begin
  Result := FIdleConsumptionPTOActive_Specified;
end;

procedure Consumption2.SetIdleConsumptionPTOInactive(Index: Integer; const AconsumptionType: consumptionType);
begin
  FIdleConsumptionPTOInactive := AconsumptionType;
  FIdleConsumptionPTOInactive_Specified := True;
end;

function Consumption2.IdleConsumptionPTOInactive_Specified(Index: Integer): boolean;
begin
  Result := FIdleConsumptionPTOInactive_Specified;
end;

procedure Consumption3.SetDriveConsumption(Index: Integer; const AconsumptionType: consumptionType);
begin
  FDriveConsumption := AconsumptionType;
  FDriveConsumption_Specified := True;
end;

function Consumption3.DriveConsumption_Specified(Index: Integer): boolean;
begin
  Result := FDriveConsumption_Specified;
end;

procedure Consumption3.SetTotalConsumption(Index: Integer; const AconsumptionType: consumptionType);
begin
  FTotalConsumption := AconsumptionType;
  FTotalConsumption_Specified := True;
end;

function Consumption3.TotalConsumption_Specified(Index: Integer): boolean;
begin
  Result := FTotalConsumption_Specified;
end;

procedure Consumption3.SetIdleConsumption(Index: Integer; const AconsumptionType: consumptionType);
begin
  FIdleConsumption := AconsumptionType;
  FIdleConsumption_Specified := True;
end;

function Consumption3.IdleConsumption_Specified(Index: Integer): boolean;
begin
  Result := FIdleConsumption_Specified;
end;

procedure Consumption3.SetAdBlueConsumption(Index: Integer; const AconsumptionType: consumptionType);
begin
  FAdBlueConsumption := AconsumptionType;
  FAdBlueConsumption_Specified := True;
end;

function Consumption3.AdBlueConsumption_Specified(Index: Integer): boolean;
begin
  Result := FAdBlueConsumption_Specified;
end;

procedure Consumption3.SetIdleConsumptionPTOActive(Index: Integer; const AconsumptionType: consumptionType);
begin
  FIdleConsumptionPTOActive := AconsumptionType;
  FIdleConsumptionPTOActive_Specified := True;
end;

function Consumption3.IdleConsumptionPTOActive_Specified(Index: Integer): boolean;
begin
  Result := FIdleConsumptionPTOActive_Specified;
end;

procedure Consumption3.SetIdleConsumptionPTOInactive(Index: Integer; const AconsumptionType: consumptionType);
begin
  FIdleConsumptionPTOInactive := AconsumptionType;
  FIdleConsumptionPTOInactive_Specified := True;
end;

function Consumption3.IdleConsumptionPTOInactive_Specified(Index: Integer): boolean;
begin
  Result := FIdleConsumptionPTOInactive_Specified;
end;

procedure Consumption4.SetDriveConsumption(Index: Integer; const AconsumptionType: consumptionType);
begin
  FDriveConsumption := AconsumptionType;
  FDriveConsumption_Specified := True;
end;

function Consumption4.DriveConsumption_Specified(Index: Integer): boolean;
begin
  Result := FDriveConsumption_Specified;
end;

procedure Consumption4.SetTotalConsumption(Index: Integer; const AconsumptionType: consumptionType);
begin
  FTotalConsumption := AconsumptionType;
  FTotalConsumption_Specified := True;
end;

function Consumption4.TotalConsumption_Specified(Index: Integer): boolean;
begin
  Result := FTotalConsumption_Specified;
end;

procedure Consumption4.SetStandConsumption(Index: Integer; const AconsumptionType: consumptionType);
begin
  FStandConsumption := AconsumptionType;
  FStandConsumption_Specified := True;
end;

function Consumption4.StandConsumption_Specified(Index: Integer): boolean;
begin
  Result := FStandConsumption_Specified;
end;

procedure Consumption4.SetPTOConsumption(Index: Integer; const AconsumptionType: consumptionType);
begin
  FPTOConsumption := AconsumptionType;
  FPTOConsumption_Specified := True;
end;

function Consumption4.PTOConsumption_Specified(Index: Integer): boolean;
begin
  Result := FPTOConsumption_Specified;
end;

procedure Consumption5.SetDriveConsumption(Index: Integer; const AconsumptionType: consumptionType);
begin
  FDriveConsumption := AconsumptionType;
  FDriveConsumption_Specified := True;
end;

function Consumption5.DriveConsumption_Specified(Index: Integer): boolean;
begin
  Result := FDriveConsumption_Specified;
end;

procedure Consumption5.SetTotalConsumption(Index: Integer; const AconsumptionType: consumptionType);
begin
  FTotalConsumption := AconsumptionType;
  FTotalConsumption_Specified := True;
end;

function Consumption5.TotalConsumption_Specified(Index: Integer): boolean;
begin
  Result := FTotalConsumption_Specified;
end;

procedure Consumption5.SetStandConsumption(Index: Integer; const AconsumptionType: consumptionType);
begin
  FStandConsumption := AconsumptionType;
  FStandConsumption_Specified := True;
end;

function Consumption5.StandConsumption_Specified(Index: Integer): boolean;
begin
  Result := FStandConsumption_Specified;
end;

procedure Consumption5.SetPTOConsumption(Index: Integer; const AconsumptionType: consumptionType);
begin
  FPTOConsumption := AconsumptionType;
  FPTOConsumption_Specified := True;
end;

function Consumption5.PTOConsumption_Specified(Index: Integer): boolean;
begin
  Result := FPTOConsumption_Specified;
end;

procedure AvgValues.SetSpeed(Index: Integer; const AspeedType: speedType);
begin
  FSpeed := AspeedType;
  FSpeed_Specified := True;
end;

function AvgValues.Speed_Specified(Index: Integer): boolean;
begin
  Result := FSpeed_Specified;
end;

procedure AvgValues.SetOverallConsumption(Index: Integer; const AconsumptionType: consumptionType);
begin
  FOverallConsumption := AconsumptionType;
  FOverallConsumption_Specified := True;
end;

function AvgValues.OverallConsumption_Specified(Index: Integer): boolean;
begin
  Result := FOverallConsumption_Specified;
end;

procedure AvgValues.SetDriveConsumption(Index: Integer; const AconsumptionType: consumptionType);
begin
  FDriveConsumption := AconsumptionType;
  FDriveConsumption_Specified := True;
end;

function AvgValues.DriveConsumption_Specified(Index: Integer): boolean;
begin
  Result := FDriveConsumption_Specified;
end;

procedure AvgValues2.SetWeight(Index: Integer; const AweightType: weightType);
begin
  FWeight := AweightType;
  FWeight_Specified := True;
end;

function AvgValues2.Weight_Specified(Index: Integer): boolean;
begin
  Result := FWeight_Specified;
end;

procedure AvgValues2.SetSpeed(Index: Integer; const AspeedType: speedType);
begin
  FSpeed := AspeedType;
  FSpeed_Specified := True;
end;

function AvgValues2.Speed_Specified(Index: Integer): boolean;
begin
  Result := FSpeed_Specified;
end;

procedure AvgValues2.SetOverallConsumption(Index: Integer; const AconsumptionType: consumptionType);
begin
  FOverallConsumption := AconsumptionType;
  FOverallConsumption_Specified := True;
end;

function AvgValues2.OverallConsumption_Specified(Index: Integer): boolean;
begin
  Result := FOverallConsumption_Specified;
end;

procedure AvgValues2.SetDriveConsumption(Index: Integer; const AconsumptionType: consumptionType);
begin
  FDriveConsumption := AconsumptionType;
  FDriveConsumption_Specified := True;
end;

function AvgValues2.DriveConsumption_Specified(Index: Integer): boolean;
begin
  Result := FDriveConsumption_Specified;
end;

procedure AvgValues2.SetAdBlueConsumption(Index: Integer; const AconsumptionType: consumptionType);
begin
  FAdBlueConsumption := AconsumptionType;
  FAdBlueConsumption_Specified := True;
end;

function AvgValues2.AdBlueConsumption_Specified(Index: Integer): boolean;
begin
  Result := FAdBlueConsumption_Specified;
end;

procedure AvgValues2.SetAdBlueToFuelRatioConsumption(Index: Integer; const AratioType: ratioType);
begin
  FAdBlueToFuelRatioConsumption := AratioType;
  FAdBlueToFuelRatioConsumption_Specified := True;
end;

function AvgValues2.AdBlueToFuelRatioConsumption_Specified(Index: Integer): boolean;
begin
  Result := FAdBlueToFuelRatioConsumption_Specified;
end;

procedure AvgValues3.SetWeight(Index: Integer; const AweightType: weightType);
begin
  FWeight := AweightType;
  FWeight_Specified := True;
end;

function AvgValues3.Weight_Specified(Index: Integer): boolean;
begin
  Result := FWeight_Specified;
end;

procedure AvgValues3.SetSpeed(Index: Integer; const AspeedType: speedType);
begin
  FSpeed := AspeedType;
  FSpeed_Specified := True;
end;

function AvgValues3.Speed_Specified(Index: Integer): boolean;
begin
  Result := FSpeed_Specified;
end;

procedure AvgValues3.SetOverallConsumption(Index: Integer; const AconsumptionType: consumptionType);
begin
  FOverallConsumption := AconsumptionType;
  FOverallConsumption_Specified := True;
end;

function AvgValues3.OverallConsumption_Specified(Index: Integer): boolean;
begin
  Result := FOverallConsumption_Specified;
end;

procedure AvgValues3.SetDriveConsumption(Index: Integer; const AconsumptionType: consumptionType);
begin
  FDriveConsumption := AconsumptionType;
  FDriveConsumption_Specified := True;
end;

function AvgValues3.DriveConsumption_Specified(Index: Integer): boolean;
begin
  Result := FDriveConsumption_Specified;
end;

procedure AvgValues3.SetAdBlueConsumption(Index: Integer; const AconsumptionType: consumptionType);
begin
  FAdBlueConsumption := AconsumptionType;
  FAdBlueConsumption_Specified := True;
end;

function AvgValues3.AdBlueConsumption_Specified(Index: Integer): boolean;
begin
  Result := FAdBlueConsumption_Specified;
end;

procedure AvgValues3.SetAdBlueToFuelRatioConsumption(Index: Integer; const AratioType: ratioType);
begin
  FAdBlueToFuelRatioConsumption := AratioType;
  FAdBlueToFuelRatioConsumption_Specified := True;
end;

function AvgValues3.AdBlueToFuelRatioConsumption_Specified(Index: Integer): boolean;
begin
  Result := FAdBlueToFuelRatioConsumption_Specified;
end;

procedure AvgValues4.SetSpeed(Index: Integer; const AspeedType: speedType);
begin
  FSpeed := AspeedType;
  FSpeed_Specified := True;
end;

function AvgValues4.Speed_Specified(Index: Integer): boolean;
begin
  Result := FSpeed_Specified;
end;

procedure AvgValues4.SetOverallConsumption(Index: Integer; const AconsumptionType: consumptionType);
begin
  FOverallConsumption := AconsumptionType;
  FOverallConsumption_Specified := True;
end;

function AvgValues4.OverallConsumption_Specified(Index: Integer): boolean;
begin
  Result := FOverallConsumption_Specified;
end;

procedure AvgValues4.SetDriveConsumption(Index: Integer; const AconsumptionType: consumptionType);
begin
  FDriveConsumption := AconsumptionType;
  FDriveConsumption_Specified := True;
end;

function AvgValues4.DriveConsumption_Specified(Index: Integer): boolean;
begin
  Result := FDriveConsumption_Specified;
end;

procedure AvgValues5.SetSpeed(Index: Integer; const AspeedType: speedType);
begin
  FSpeed := AspeedType;
  FSpeed_Specified := True;
end;

function AvgValues5.Speed_Specified(Index: Integer): boolean;
begin
  Result := FSpeed_Specified;
end;

procedure AvgValues5.SetOverallConsumption(Index: Integer; const AconsumptionType: consumptionType);
begin
  FOverallConsumption := AconsumptionType;
  FOverallConsumption_Specified := True;
end;

function AvgValues5.OverallConsumption_Specified(Index: Integer): boolean;
begin
  Result := FOverallConsumption_Specified;
end;

procedure AvgValues5.SetDriveConsumption(Index: Integer; const AconsumptionType: consumptionType);
begin
  FDriveConsumption := AconsumptionType;
  FDriveConsumption_Specified := True;
end;

function AvgValues5.DriveConsumption_Specified(Index: Integer): boolean;
begin
  Result := FDriveConsumption_Specified;
end;

procedure Grades.SetDrivingStyle(Index: Integer; const AgradeType: gradeType);
begin
  FDrivingStyle := AgradeType;
  FDrivingStyle_Specified := True;
end;

function Grades.DrivingStyle_Specified(Index: Integer): boolean;
begin
  Result := FDrivingStyle_Specified;
end;

procedure Grades.SetEconomicDrivingGrading(Index: Integer; const AgradeType: gradeType);
begin
  FEconomicDrivingGrading := AgradeType;
  FEconomicDrivingGrading_Specified := True;
end;

function Grades.EconomicDrivingGrading_Specified(Index: Integer): boolean;
begin
  Result := FEconomicDrivingGrading_Specified;
end;

procedure Grades.SetOverRevNumberGrading(Index: Integer; const AgradeType: gradeType);
begin
  FOverRevNumberGrading := AgradeType;
  FOverRevNumberGrading_Specified := True;
end;

function Grades.OverRevNumberGrading_Specified(Index: Integer): boolean;
begin
  Result := FOverRevNumberGrading_Specified;
end;

procedure Grades.SetOverRevTimeGrading(Index: Integer; const AgradeType: gradeType);
begin
  FOverRevTimeGrading := AgradeType;
  FOverRevTimeGrading_Specified := True;
end;

function Grades.OverRevTimeGrading_Specified(Index: Integer): boolean;
begin
  Result := FOverRevTimeGrading_Specified;
end;

procedure Grades.SetOverSpeedTimeGrading(Index: Integer; const AgradeType: gradeType);
begin
  FOverSpeedTimeGrading := AgradeType;
  FOverSpeedTimeGrading_Specified := True;
end;

function Grades.OverSpeedTimeGrading_Specified(Index: Integer): boolean;
begin
  Result := FOverSpeedTimeGrading_Specified;
end;

procedure Grades.SetHarshBrakeGrading(Index: Integer; const AgradeType: gradeType);
begin
  FHarshBrakeGrading := AgradeType;
  FHarshBrakeGrading_Specified := True;
end;

function Grades.HarshBrakeGrading_Specified(Index: Integer): boolean;
begin
  Result := FHarshBrakeGrading_Specified;
end;

procedure Grades.SetIdlingTimeGrading(Index: Integer; const AgradeType: gradeType);
begin
  FIdlingTimeGrading := AgradeType;
  FIdlingTimeGrading_Specified := True;
end;

function Grades.IdlingTimeGrading_Specified(Index: Integer): boolean;
begin
  Result := FIdlingTimeGrading_Specified;
end;

procedure Grades2.SetDrivingStyle(Index: Integer; const AgradeType: gradeType);
begin
  FDrivingStyle := AgradeType;
  FDrivingStyle_Specified := True;
end;

function Grades2.DrivingStyle_Specified(Index: Integer): boolean;
begin
  Result := FDrivingStyle_Specified;
end;

procedure Grades2.SetEconomicDrivingGrading(Index: Integer; const AgradeType: gradeType);
begin
  FEconomicDrivingGrading := AgradeType;
  FEconomicDrivingGrading_Specified := True;
end;

function Grades2.EconomicDrivingGrading_Specified(Index: Integer): boolean;
begin
  Result := FEconomicDrivingGrading_Specified;
end;

procedure Grades2.SetOverRevNumberGrading(Index: Integer; const AgradeType: gradeType);
begin
  FOverRevNumberGrading := AgradeType;
  FOverRevNumberGrading_Specified := True;
end;

function Grades2.OverRevNumberGrading_Specified(Index: Integer): boolean;
begin
  Result := FOverRevNumberGrading_Specified;
end;

procedure Grades2.SetOverRevTimeGrading(Index: Integer; const AgradeType: gradeType);
begin
  FOverRevTimeGrading := AgradeType;
  FOverRevTimeGrading_Specified := True;
end;

function Grades2.OverRevTimeGrading_Specified(Index: Integer): boolean;
begin
  Result := FOverRevTimeGrading_Specified;
end;

procedure Grades2.SetOverSpeedTimeGrading(Index: Integer; const AgradeType: gradeType);
begin
  FOverSpeedTimeGrading := AgradeType;
  FOverSpeedTimeGrading_Specified := True;
end;

function Grades2.OverSpeedTimeGrading_Specified(Index: Integer): boolean;
begin
  Result := FOverSpeedTimeGrading_Specified;
end;

procedure Grades2.SetHarshBrakeGrading(Index: Integer; const AgradeType: gradeType);
begin
  FHarshBrakeGrading := AgradeType;
  FHarshBrakeGrading_Specified := True;
end;

function Grades2.HarshBrakeGrading_Specified(Index: Integer): boolean;
begin
  Result := FHarshBrakeGrading_Specified;
end;

procedure Grades2.SetIdlingTimeGrading(Index: Integer; const AgradeType: gradeType);
begin
  FIdlingTimeGrading := AgradeType;
  FIdlingTimeGrading_Specified := True;
end;

function Grades2.IdlingTimeGrading_Specified(Index: Integer): boolean;
begin
  Result := FIdlingTimeGrading_Specified;
end;

procedure Grades3.SetDrivingStyle(Index: Integer; const AgradeType: gradeType);
begin
  FDrivingStyle := AgradeType;
  FDrivingStyle_Specified := True;
end;

function Grades3.DrivingStyle_Specified(Index: Integer): boolean;
begin
  Result := FDrivingStyle_Specified;
end;

procedure Grades3.SetOverrevNumberGrading(Index: Integer; const AgradeType: gradeType);
begin
  FOverrevNumberGrading := AgradeType;
  FOverrevNumberGrading_Specified := True;
end;

function Grades3.OverrevNumberGrading_Specified(Index: Integer): boolean;
begin
  Result := FOverrevNumberGrading_Specified;
end;

procedure Grades3.SetOverrevTimeGrading(Index: Integer; const AgradeType: gradeType);
begin
  FOverrevTimeGrading := AgradeType;
  FOverrevTimeGrading_Specified := True;
end;

function Grades3.OverrevTimeGrading_Specified(Index: Integer): boolean;
begin
  Result := FOverrevTimeGrading_Specified;
end;

procedure Grades3.SetOverspeedTimeGrading(Index: Integer; const AgradeType: gradeType);
begin
  FOverspeedTimeGrading := AgradeType;
  FOverspeedTimeGrading_Specified := True;
end;

function Grades3.OverspeedTimeGrading_Specified(Index: Integer): boolean;
begin
  Result := FOverspeedTimeGrading_Specified;
end;

procedure Grades3.SetEconomicDrivingGrading(Index: Integer; const AgradeType: gradeType);
begin
  FEconomicDrivingGrading := AgradeType;
  FEconomicDrivingGrading_Specified := True;
end;

function Grades3.EconomicDrivingGrading_Specified(Index: Integer): boolean;
begin
  Result := FEconomicDrivingGrading_Specified;
end;

procedure Grades3.SetIdlingTimeGrading(Index: Integer; const AgradeType: gradeType);
begin
  FIdlingTimeGrading := AgradeType;
  FIdlingTimeGrading_Specified := True;
end;

function Grades3.IdlingTimeGrading_Specified(Index: Integer): boolean;
begin
  Result := FIdlingTimeGrading_Specified;
end;

procedure Grades3.SetHarshBrakeGrading(Index: Integer; const AgradeType: gradeType);
begin
  FHarshBrakeGrading := AgradeType;
  FHarshBrakeGrading_Specified := True;
end;

function Grades3.HarshBrakeGrading_Specified(Index: Integer): boolean;
begin
  Result := FHarshBrakeGrading_Specified;
end;

procedure Grades4.SetDrivingStyle(Index: Integer; const AgradeType: gradeType);
begin
  FDrivingStyle := AgradeType;
  FDrivingStyle_Specified := True;
end;

function Grades4.DrivingStyle_Specified(Index: Integer): boolean;
begin
  Result := FDrivingStyle_Specified;
end;

procedure Grades4.SetDrivingStyleCons(Index: Integer; const AgradeType: gradeType);
begin
  FDrivingStyleCons := AgradeType;
  FDrivingStyleCons_Specified := True;
end;

function Grades4.DrivingStyleCons_Specified(Index: Integer): boolean;
begin
  Result := FDrivingStyleCons_Specified;
end;

procedure Grades4.SetDrivingStyleConsPreventive(Index: Integer; const AgradeType: gradeType);
begin
  FDrivingStyleConsPreventive := AgradeType;
  FDrivingStyleConsPreventive_Specified := True;
end;

function Grades4.DrivingStyleConsPreventive_Specified(Index: Integer): boolean;
begin
  Result := FDrivingStyleConsPreventive_Specified;
end;

procedure Grades4.SetDrivingStyleConsEngineOperationMN(Index: Integer; const AgradeType: gradeType);
begin
  FDrivingStyleConsEngineOperationMN := AgradeType;
  FDrivingStyleConsEngineOperationMN_Specified := True;
end;

function Grades4.DrivingStyleConsEngineOperationMN_Specified(Index: Integer): boolean;
begin
  Result := FDrivingStyleConsEngineOperationMN_Specified;
end;

procedure Grades4.SetDrivingStyleConsPedalMovements(Index: Integer; const AgradeType: gradeType);
begin
  FDrivingStyleConsPedalMovements := AgradeType;
  FDrivingStyleConsPedalMovements_Specified := True;
end;

function Grades4.DrivingStyleConsPedalMovements_Specified(Index: Integer): boolean;
begin
  Result := FDrivingStyleConsPedalMovements_Specified;
end;

procedure Grades4.SetDrivingStyleConsUniformSpeed(Index: Integer; const AgradeType: gradeType);
begin
  FDrivingStyleConsUniformSpeed := AgradeType;
  FDrivingStyleConsUniformSpeed_Specified := True;
end;

function Grades4.DrivingStyleConsUniformSpeed_Specified(Index: Integer): boolean;
begin
  Result := FDrivingStyleConsUniformSpeed_Specified;
end;

procedure Grades4.SetDrivingStyleConsStops(Index: Integer; const AgradeType: gradeType);
begin
  FDrivingStyleConsStops := AgradeType;
  FDrivingStyleConsStops_Specified := True;
end;

function Grades4.DrivingStyleConsStops_Specified(Index: Integer): boolean;
begin
  Result := FDrivingStyleConsStops_Specified;
end;

procedure Grades4.SetDrivingStyleBrake(Index: Integer; const AgradeType: gradeType);
begin
  FDrivingStyleBrake := AgradeType;
  FDrivingStyleBrake_Specified := True;
end;

function Grades4.DrivingStyleBrake_Specified(Index: Integer): boolean;
begin
  Result := FDrivingStyleBrake_Specified;
end;

procedure Grades4.SetDrivingStyleBrakePreventive(Index: Integer; const AgradeType: gradeType);
begin
  FDrivingStyleBrakePreventive := AgradeType;
  FDrivingStyleBrakePreventive_Specified := True;
end;

function Grades4.DrivingStyleBrakePreventive_Specified(Index: Integer): boolean;
begin
  Result := FDrivingStyleBrakePreventive_Specified;
end;

procedure Grades4.SetDrivingStyleBrakeDeceleration(Index: Integer; const AgradeType: gradeType);
begin
  FDrivingStyleBrakeDeceleration := AgradeType;
  FDrivingStyleBrakeDeceleration_Specified := True;
end;

function Grades4.DrivingStyleBrakeDeceleration_Specified(Index: Integer): boolean;
begin
  Result := FDrivingStyleBrakeDeceleration_Specified;
end;

procedure Grades4.SetDegreeOfDifficulty(Index: Integer; const AgradeType: gradeType);
begin
  FDegreeOfDifficulty := AgradeType;
  FDegreeOfDifficulty_Specified := True;
end;

function Grades4.DegreeOfDifficulty_Specified(Index: Integer): boolean;
begin
  Result := FDegreeOfDifficulty_Specified;
end;

procedure Grades4.SetDegreeOfDifficultyAverageSlope(Index: Integer; const AgradeType: gradeType);
begin
  FDegreeOfDifficultyAverageSlope := AgradeType;
  FDegreeOfDifficultyAverageSlope_Specified := True;
end;

function Grades4.DegreeOfDifficultyAverageSlope_Specified(Index: Integer): boolean;
begin
  Result := FDegreeOfDifficultyAverageSlope_Specified;
end;

procedure Grades4.SetDegreeOfDifficultyWeight(Index: Integer; const AgradeType: gradeType);
begin
  FDegreeOfDifficultyWeight := AgradeType;
  FDegreeOfDifficultyWeight_Specified := True;
end;

function Grades4.DegreeOfDifficultyWeight_Specified(Index: Integer): boolean;
begin
  Result := FDegreeOfDifficultyWeight_Specified;
end;

procedure Grades4.SetDegreeOfDifficultyStops(Index: Integer; const AgradeType: gradeType);
begin
  FDegreeOfDifficultyStops := AgradeType;
  FDegreeOfDifficultyStops_Specified := True;
end;

function Grades4.DegreeOfDifficultyStops_Specified(Index: Integer): boolean;
begin
  Result := FDegreeOfDifficultyStops_Specified;
end;

procedure Grades4.SetTorqueClassification(Index: Integer; const AgradeType: gradeType);
begin
  FTorqueClassification := AgradeType;
  FTorqueClassification_Specified := True;
end;

function Grades4.TorqueClassification_Specified(Index: Integer): boolean;
begin
  Result := FTorqueClassification_Specified;
end;

procedure Grades5.SetDrivingStyle(Index: Integer; const AgradeType: gradeType);
begin
  FDrivingStyle := AgradeType;
  FDrivingStyle_Specified := True;
end;

function Grades5.DrivingStyle_Specified(Index: Integer): boolean;
begin
  Result := FDrivingStyle_Specified;
end;

procedure Grades5.SetDrivingStyleCons(Index: Integer; const AgradeType: gradeType);
begin
  FDrivingStyleCons := AgradeType;
  FDrivingStyleCons_Specified := True;
end;

function Grades5.DrivingStyleCons_Specified(Index: Integer): boolean;
begin
  Result := FDrivingStyleCons_Specified;
end;

procedure Grades5.SetDrivingStyleConsPreventive(Index: Integer; const AgradeType: gradeType);
begin
  FDrivingStyleConsPreventive := AgradeType;
  FDrivingStyleConsPreventive_Specified := True;
end;

function Grades5.DrivingStyleConsPreventive_Specified(Index: Integer): boolean;
begin
  Result := FDrivingStyleConsPreventive_Specified;
end;

procedure Grades5.SetDrivingStyleConsEngineOperationMN(Index: Integer; const AgradeType: gradeType);
begin
  FDrivingStyleConsEngineOperationMN := AgradeType;
  FDrivingStyleConsEngineOperationMN_Specified := True;
end;

function Grades5.DrivingStyleConsEngineOperationMN_Specified(Index: Integer): boolean;
begin
  Result := FDrivingStyleConsEngineOperationMN_Specified;
end;

procedure Grades5.SetDrivingStyleConsPedalMovements(Index: Integer; const AgradeType: gradeType);
begin
  FDrivingStyleConsPedalMovements := AgradeType;
  FDrivingStyleConsPedalMovements_Specified := True;
end;

function Grades5.DrivingStyleConsPedalMovements_Specified(Index: Integer): boolean;
begin
  Result := FDrivingStyleConsPedalMovements_Specified;
end;

procedure Grades5.SetDrivingStyleConsUniformSpeed(Index: Integer; const AgradeType: gradeType);
begin
  FDrivingStyleConsUniformSpeed := AgradeType;
  FDrivingStyleConsUniformSpeed_Specified := True;
end;

function Grades5.DrivingStyleConsUniformSpeed_Specified(Index: Integer): boolean;
begin
  Result := FDrivingStyleConsUniformSpeed_Specified;
end;

procedure Grades5.SetDrivingStyleConsStops(Index: Integer; const AgradeType: gradeType);
begin
  FDrivingStyleConsStops := AgradeType;
  FDrivingStyleConsStops_Specified := True;
end;

function Grades5.DrivingStyleConsStops_Specified(Index: Integer): boolean;
begin
  Result := FDrivingStyleConsStops_Specified;
end;

procedure Grades5.SetDrivingStyleBrake(Index: Integer; const AgradeType: gradeType);
begin
  FDrivingStyleBrake := AgradeType;
  FDrivingStyleBrake_Specified := True;
end;

function Grades5.DrivingStyleBrake_Specified(Index: Integer): boolean;
begin
  Result := FDrivingStyleBrake_Specified;
end;

procedure Grades5.SetDrivingStyleBrakePreventive(Index: Integer; const AgradeType: gradeType);
begin
  FDrivingStyleBrakePreventive := AgradeType;
  FDrivingStyleBrakePreventive_Specified := True;
end;

function Grades5.DrivingStyleBrakePreventive_Specified(Index: Integer): boolean;
begin
  Result := FDrivingStyleBrakePreventive_Specified;
end;

procedure Grades5.SetDrivingStyleBrakeDeceleration(Index: Integer; const AgradeType: gradeType);
begin
  FDrivingStyleBrakeDeceleration := AgradeType;
  FDrivingStyleBrakeDeceleration_Specified := True;
end;

function Grades5.DrivingStyleBrakeDeceleration_Specified(Index: Integer): boolean;
begin
  Result := FDrivingStyleBrakeDeceleration_Specified;
end;

procedure Grades5.SetDegreeOfDifficulty(Index: Integer; const AgradeType: gradeType);
begin
  FDegreeOfDifficulty := AgradeType;
  FDegreeOfDifficulty_Specified := True;
end;

function Grades5.DegreeOfDifficulty_Specified(Index: Integer): boolean;
begin
  Result := FDegreeOfDifficulty_Specified;
end;

procedure Grades5.SetDegreeOfDifficultyAverageSlope(Index: Integer; const AgradeType: gradeType);
begin
  FDegreeOfDifficultyAverageSlope := AgradeType;
  FDegreeOfDifficultyAverageSlope_Specified := True;
end;

function Grades5.DegreeOfDifficultyAverageSlope_Specified(Index: Integer): boolean;
begin
  Result := FDegreeOfDifficultyAverageSlope_Specified;
end;

procedure Grades5.SetDegreeOfDifficultyWeight(Index: Integer; const AgradeType: gradeType);
begin
  FDegreeOfDifficultyWeight := AgradeType;
  FDegreeOfDifficultyWeight_Specified := True;
end;

function Grades5.DegreeOfDifficultyWeight_Specified(Index: Integer): boolean;
begin
  Result := FDegreeOfDifficultyWeight_Specified;
end;

procedure Grades5.SetDegreeOfDifficultyStops(Index: Integer; const AgradeType: gradeType);
begin
  FDegreeOfDifficultyStops := AgradeType;
  FDegreeOfDifficultyStops_Specified := True;
end;

function Grades5.DegreeOfDifficultyStops_Specified(Index: Integer): boolean;
begin
  Result := FDegreeOfDifficultyStops_Specified;
end;

procedure Grades5.SetTorqueClassification(Index: Integer; const AgradeType: gradeType);
begin
  FTorqueClassification := AgradeType;
  FTorqueClassification_Specified := True;
end;

function Grades5.TorqueClassification_Specified(Index: Integer): boolean;
begin
  Result := FTorqueClassification_Specified;
end;

procedure Grades6.SetDrivingStyle(Index: Integer; const AgradeType: gradeType);
begin
  FDrivingStyle := AgradeType;
  FDrivingStyle_Specified := True;
end;

function Grades6.DrivingStyle_Specified(Index: Integer): boolean;
begin
  Result := FDrivingStyle_Specified;
end;

procedure Grades6.SetOverrevNumberGrading(Index: Integer; const AgradeType: gradeType);
begin
  FOverrevNumberGrading := AgradeType;
  FOverrevNumberGrading_Specified := True;
end;

function Grades6.OverrevNumberGrading_Specified(Index: Integer): boolean;
begin
  Result := FOverrevNumberGrading_Specified;
end;

procedure Grades6.SetOverrevTimeGrading(Index: Integer; const AgradeType: gradeType);
begin
  FOverrevTimeGrading := AgradeType;
  FOverrevTimeGrading_Specified := True;
end;

function Grades6.OverrevTimeGrading_Specified(Index: Integer): boolean;
begin
  Result := FOverrevTimeGrading_Specified;
end;

procedure Grades6.SetHarshBrakeGrading(Index: Integer; const AgradeType: gradeType);
begin
  FHarshBrakeGrading := AgradeType;
  FHarshBrakeGrading_Specified := True;
end;

function Grades6.HarshBrakeGrading_Specified(Index: Integer): boolean;
begin
  Result := FHarshBrakeGrading_Specified;
end;

procedure Grades6.SetOverspeedTimeGrading(Index: Integer; const AgradeType: gradeType);
begin
  FOverspeedTimeGrading := AgradeType;
  FOverspeedTimeGrading_Specified := True;
end;

function Grades6.OverspeedTimeGrading_Specified(Index: Integer): boolean;
begin
  Result := FOverspeedTimeGrading_Specified;
end;

procedure Grades6.SetEconomicDrivingGrading(Index: Integer; const AgradeType: gradeType);
begin
  FEconomicDrivingGrading := AgradeType;
  FEconomicDrivingGrading_Specified := True;
end;

function Grades6.EconomicDrivingGrading_Specified(Index: Integer): boolean;
begin
  Result := FEconomicDrivingGrading_Specified;
end;

procedure Grades6.SetIdlingTimeGrading(Index: Integer; const AgradeType: gradeType);
begin
  FIdlingTimeGrading := AgradeType;
  FIdlingTimeGrading_Specified := True;
end;

function Grades6.IdlingTimeGrading_Specified(Index: Integer): boolean;
begin
  Result := FIdlingTimeGrading_Specified;
end;

procedure Grades7.SetDrivingStyle(Index: Integer; const AgradeType: gradeType);
begin
  FDrivingStyle := AgradeType;
  FDrivingStyle_Specified := True;
end;

function Grades7.DrivingStyle_Specified(Index: Integer): boolean;
begin
  Result := FDrivingStyle_Specified;
end;

procedure Grades7.SetOverrevNumberGrading(Index: Integer; const AgradeType: gradeType);
begin
  FOverrevNumberGrading := AgradeType;
  FOverrevNumberGrading_Specified := True;
end;

function Grades7.OverrevNumberGrading_Specified(Index: Integer): boolean;
begin
  Result := FOverrevNumberGrading_Specified;
end;

procedure Grades7.SetOverrevTimeGrading(Index: Integer; const AgradeType: gradeType);
begin
  FOverrevTimeGrading := AgradeType;
  FOverrevTimeGrading_Specified := True;
end;

function Grades7.OverrevTimeGrading_Specified(Index: Integer): boolean;
begin
  Result := FOverrevTimeGrading_Specified;
end;

procedure Grades7.SetHarshBrakeGrading(Index: Integer; const AgradeType: gradeType);
begin
  FHarshBrakeGrading := AgradeType;
  FHarshBrakeGrading_Specified := True;
end;

function Grades7.HarshBrakeGrading_Specified(Index: Integer): boolean;
begin
  Result := FHarshBrakeGrading_Specified;
end;

procedure Grades7.SetOverspeedTimeGrading(Index: Integer; const AgradeType: gradeType);
begin
  FOverspeedTimeGrading := AgradeType;
  FOverspeedTimeGrading_Specified := True;
end;

function Grades7.OverspeedTimeGrading_Specified(Index: Integer): boolean;
begin
  Result := FOverspeedTimeGrading_Specified;
end;

procedure Grades7.SetEconomicDrivingGrading(Index: Integer; const AgradeType: gradeType);
begin
  FEconomicDrivingGrading := AgradeType;
  FEconomicDrivingGrading_Specified := True;
end;

function Grades7.EconomicDrivingGrading_Specified(Index: Integer): boolean;
begin
  Result := FEconomicDrivingGrading_Specified;
end;

procedure Grades7.SetIdlingTimeGrading(Index: Integer; const AgradeType: gradeType);
begin
  FIdlingTimeGrading := AgradeType;
  FIdlingTimeGrading_Specified := True;
end;

function Grades7.IdlingTimeGrading_Specified(Index: Integer): boolean;
begin
  Result := FIdlingTimeGrading_Specified;
end;

destructor TimeRangeType.Destroy;
begin
  SysUtils.FreeAndNil(FPeriod);
  inherited Destroy;
end;

procedure TimeRangeType.SetModel(Index: Integer; const AModel: Model);
begin
  FModel := AModel;
  FModel_Specified := True;
end;

function TimeRangeType.Model_Specified(Index: Integer): boolean;
begin
  Result := FModel_Specified;
end;

procedure TimeRangeType.SetPeriod(Index: Integer; const ATPDate: TPDate);
begin
  FPeriod := ATPDate;
  FPeriod_Specified := True;
end;

function TimeRangeType.Period_Specified(Index: Integer): boolean;
begin
  Result := FPeriod_Specified;
end;

destructor Values.Destroy;
begin
  SysUtils.FreeAndNil(FX);
  SysUtils.FreeAndNil(FY1);
  SysUtils.FreeAndNil(FY2);
  inherited Destroy;
end;

procedure Values.SetY2(Index: Integer; const AY2: Y2);
begin
  FY2 := AY2;
  FY2_Specified := True;
end;

function Values.Y2_Specified(Index: Integer): boolean;
begin
  Result := FY2_Specified;
end;

destructor engineChartType.Destroy;
var
  I: Integer;
begin
  for I := 0 to System.Length(FRows)-1 do
    SysUtils.FreeAndNil(FRows[I]);
  System.SetLength(FRows, 0);
  inherited Destroy;
end;

procedure TPDate.SetBegin_(Index: Integer; const AtimestampType: timestampType);
begin
  FBegin_ := AtimestampType;
  FBegin__Specified := True;
end;

function TPDate.Begin__Specified(Index: Integer): boolean;
begin
  Result := FBegin__Specified;
end;

procedure TPDate.SetEnd_(Index: Integer; const AtimestampType: timestampType);
begin
  FEnd_ := AtimestampType;
  FEnd__Specified := True;
end;

function TPDate.End__Specified(Index: Integer): boolean;
begin
  Result := FEnd__Specified;
end;

procedure X.Setunit_(Index: Integer; const AunitType: unitType);
begin
  Funit_ := AunitType;
  Funit__Specified := True;
end;

function X.unit__Specified(Index: Integer): boolean;
begin
  Result := Funit__Specified;
end;

destructor Rows.Destroy;
var
  I: Integer;
begin
  for I := 0 to System.Length(FCells)-1 do
    SysUtils.FreeAndNil(FCells[I]);
  System.SetLength(FCells, 0);
  inherited Destroy;
end;

procedure Rows.Setname_(Index: Integer; const Astring: string);
begin
  Fname_ := Astring;
  Fname__Specified := True;
end;

function Rows.name__Specified(Index: Integer): boolean;
begin
  Result := Fname__Specified;
end;

procedure Rows.Setunit_(Index: Integer; const AunitType: unitType);
begin
  Funit_ := AunitType;
  Funit__Specified := True;
end;

function Rows.unit__Specified(Index: Integer): boolean;
begin
  Result := Funit__Specified;
end;

procedure Y1.Setunit_(Index: Integer; const AunitType: unitType);
begin
  Funit_ := AunitType;
  Funit__Specified := True;
end;

function Y1.unit__Specified(Index: Integer): boolean;
begin
  Result := Funit__Specified;
end;

procedure Y2.Setunit_(Index: Integer; const AunitType: unitType);
begin
  Funit_ := AunitType;
  Funit__Specified := True;
end;

function Y2.unit__Specified(Index: Integer): boolean;
begin
  Result := Funit__Specified;
end;

destructor Period.Destroy;
begin
  SysUtils.FreeAndNil(FPeriod);
  inherited Destroy;
end;

procedure Period.SetBegin_(Index: Integer; const AtimestampType: timestampType);
begin
  FBegin_ := AtimestampType;
  FBegin__Specified := True;
end;

function Period.Begin__Specified(Index: Integer): boolean;
begin
  Result := FBegin__Specified;
end;

procedure Period.SetEnd_(Index: Integer; const AtimestampType: timestampType);
begin
  FEnd_ := AtimestampType;
  FEnd__Specified := True;
end;

function Period.End__Specified(Index: Integer): boolean;
begin
  Result := FEnd__Specified;
end;

procedure Period.SetPeriod(Index: Integer; const APeriod3Type: Period3Type);
begin
  FPeriod := APeriod3Type;
  FPeriod_Specified := True;
end;

function Period.Period_Specified(Index: Integer): boolean;
begin
  Result := FPeriod_Specified;
end;

destructor Period2.Destroy;
begin
  SysUtils.FreeAndNil(FPeriod);
  inherited Destroy;
end;

procedure Period2.SetBegin_(Index: Integer; const AtimestampType: timestampType);
begin
  FBegin_ := AtimestampType;
  FBegin__Specified := True;
end;

function Period2.Begin__Specified(Index: Integer): boolean;
begin
  Result := FBegin__Specified;
end;

procedure Period2.SetEnd_(Index: Integer; const AtimestampType: timestampType);
begin
  FEnd_ := AtimestampType;
  FEnd__Specified := True;
end;

function Period2.End__Specified(Index: Integer): boolean;
begin
  Result := FEnd__Specified;
end;

procedure Period2.SetPeriod(Index: Integer; const APeriod3Type: Period3Type);
begin
  FPeriod := APeriod3Type;
  FPeriod_Specified := True;
end;

function Period2.Period_Specified(Index: Integer): boolean;
begin
  Result := FPeriod_Specified;
end;

destructor Period3.Destroy;
begin
  SysUtils.FreeAndNil(FPeriod);
  inherited Destroy;
end;

procedure Period3.SetBegin_(Index: Integer; const AtimestampType: timestampType);
begin
  FBegin_ := AtimestampType;
  FBegin__Specified := True;
end;

function Period3.Begin__Specified(Index: Integer): boolean;
begin
  Result := FBegin__Specified;
end;

procedure Period3.SetEnd_(Index: Integer; const AtimestampType: timestampType);
begin
  FEnd_ := AtimestampType;
  FEnd__Specified := True;
end;

function Period3.End__Specified(Index: Integer): boolean;
begin
  Result := FEnd__Specified;
end;

procedure Period3.SetPeriod(Index: Integer; const APeriod3Type: Period3Type);
begin
  FPeriod := APeriod3Type;
  FPeriod_Specified := True;
end;

function Period3.Period_Specified(Index: Integer): boolean;
begin
  Result := FPeriod_Specified;
end;

destructor Period4.Destroy;
begin
  SysUtils.FreeAndNil(FPeriod);
  inherited Destroy;
end;

procedure Period4.SetBegin_(Index: Integer; const AtimestampType: timestampType);
begin
  FBegin_ := AtimestampType;
  FBegin__Specified := True;
end;

function Period4.Begin__Specified(Index: Integer): boolean;
begin
  Result := FBegin__Specified;
end;

procedure Period4.SetEnd_(Index: Integer; const AtimestampType: timestampType);
begin
  FEnd_ := AtimestampType;
  FEnd__Specified := True;
end;

function Period4.End__Specified(Index: Integer): boolean;
begin
  Result := FEnd__Specified;
end;

procedure Period4.SetPeriod(Index: Integer; const APeriod3Type: Period3Type);
begin
  FPeriod := APeriod3Type;
  FPeriod_Specified := True;
end;

function Period4.Period_Specified(Index: Integer): boolean;
begin
  Result := FPeriod_Specified;
end;

destructor Period5.Destroy;
begin
  SysUtils.FreeAndNil(FPeriod);
  inherited Destroy;
end;

procedure Period5.SetBegin_(Index: Integer; const AtimestampType: timestampType);
begin
  FBegin_ := AtimestampType;
  FBegin__Specified := True;
end;

function Period5.Begin__Specified(Index: Integer): boolean;
begin
  Result := FBegin__Specified;
end;

procedure Period5.SetEnd_(Index: Integer; const AtimestampType: timestampType);
begin
  FEnd_ := AtimestampType;
  FEnd__Specified := True;
end;

function Period5.End__Specified(Index: Integer): boolean;
begin
  Result := FEnd__Specified;
end;

procedure Period5.SetPeriod(Index: Integer; const APeriod3Type: Period3Type);
begin
  FPeriod := APeriod3Type;
  FPeriod_Specified := True;
end;

function Period5.Period_Specified(Index: Integer): boolean;
begin
  Result := FPeriod_Specified;
end;

destructor Period6.Destroy;
begin
  SysUtils.FreeAndNil(FPeriod);
  inherited Destroy;
end;

procedure Period6.SetBegin_(Index: Integer; const AtimestampType: timestampType);
begin
  FBegin_ := AtimestampType;
  FBegin__Specified := True;
end;

function Period6.Begin__Specified(Index: Integer): boolean;
begin
  Result := FBegin__Specified;
end;

procedure Period6.SetEnd_(Index: Integer; const AtimestampType: timestampType);
begin
  FEnd_ := AtimestampType;
  FEnd__Specified := True;
end;

function Period6.End__Specified(Index: Integer): boolean;
begin
  Result := FEnd__Specified;
end;

procedure Period6.SetPeriod(Index: Integer; const APeriod3Type: Period3Type);
begin
  FPeriod := APeriod3Type;
  FPeriod_Specified := True;
end;

function Period6.Period_Specified(Index: Integer): boolean;
begin
  Result := FPeriod_Specified;
end;

destructor Period7.Destroy;
begin
  SysUtils.FreeAndNil(FPeriod);
  inherited Destroy;
end;

procedure Period7.SetBegin_(Index: Integer; const AtimestampType: timestampType);
begin
  FBegin_ := AtimestampType;
  FBegin__Specified := True;
end;

function Period7.Begin__Specified(Index: Integer): boolean;
begin
  Result := FBegin__Specified;
end;

procedure Period7.SetEnd_(Index: Integer; const AtimestampType: timestampType);
begin
  FEnd_ := AtimestampType;
  FEnd__Specified := True;
end;

function Period7.End__Specified(Index: Integer): boolean;
begin
  Result := FEnd__Specified;
end;

procedure Period7.SetPeriod(Index: Integer; const APeriod3Type: Period3Type);
begin
  FPeriod := APeriod3Type;
  FPeriod_Specified := True;
end;

function Period7.Period_Specified(Index: Integer): boolean;
begin
  Result := FPeriod_Specified;
end;

destructor barChartType.Destroy;
var
  I: Integer;
begin
  for I := 0 to System.Length(FValues)-1 do
    SysUtils.FreeAndNil(FValues[I]);
  System.SetLength(FValues, 0);
  inherited Destroy;
end;

destructor ChartsType.Destroy;
var
  I: Integer;
begin
  for I := 0 to System.Length(FBarCharts)-1 do
    SysUtils.FreeAndNil(FBarCharts[I]);
  System.SetLength(FBarCharts, 0);
  for I := 0 to System.Length(FEngineOperationCharts)-1 do
    SysUtils.FreeAndNil(FEngineOperationCharts[I]);
  System.SetLength(FEngineOperationCharts, 0);
  inherited Destroy;
end;

procedure ChartsType.SetBarCharts(Index: Integer; const AArray_Of_barChartType: Array_Of_barChartType);
begin
  FBarCharts := AArray_Of_barChartType;
  FBarCharts_Specified := True;
end;

function ChartsType.BarCharts_Specified(Index: Integer): boolean;
begin
  Result := FBarCharts_Specified;
end;

procedure ChartsType.SetEngineOperationCharts(Index: Integer; const AArray_Of_engineChartType: Array_Of_engineChartType);
begin
  FEngineOperationCharts := AArray_Of_engineChartType;
  FEngineOperationCharts_Specified := True;
end;

function ChartsType.EngineOperationCharts_Specified(Index: Integer): boolean;
begin
  Result := FEngineOperationCharts_Specified;
end;

destructor Period3Type.Destroy;
begin
  SysUtils.FreeAndNil(FTimeRange);
  inherited Destroy;
end;

procedure Period3Type.SetTimeRange(Index: Integer; const ATimeRangeType: TimeRangeType);
begin
  FTimeRange := ATimeRangeType;
  FTimeRange_Specified := True;
end;

function Period3Type.TimeRange_Specified(Index: Integer): boolean;
begin
  Result := FTimeRange_Specified;
end;

procedure Period3Type.SetStartYear(Index: Integer; const AyearType: yearType);
begin
  FStartYear := AyearType;
  FStartYear_Specified := True;
end;

function Period3Type.StartYear_Specified(Index: Integer): boolean;
begin
  Result := FStartYear_Specified;
end;

procedure Period3Type.SetEndYear(Index: Integer; const AyearType: yearType);
begin
  FEndYear := AyearType;
  FEndYear_Specified := True;
end;

function Period3Type.EndYear_Specified(Index: Integer): boolean;
begin
  Result := FEndYear_Specified;
end;

procedure Period3Type.SetInterval(Index: Integer; const Aperiod2Type: period2Type);
begin
  FInterval := Aperiod2Type;
  FInterval_Specified := True;
end;

function Period3Type.Interval_Specified(Index: Integer): boolean;
begin
  Result := FInterval_Specified;
end;

procedure Period3Type.SetYear(Index: Integer; const Astring: string);
begin
  FYear := Astring;
  FYear_Specified := True;
end;

function Period3Type.Year_Specified(Index: Integer): boolean;
begin
  Result := FYear_Specified;
end;

procedure Period3Type.SetPeriod(Index: Integer; const Aperiod1Type: period1Type);
begin
  FPeriod := Aperiod1Type;
  FPeriod_Specified := True;
end;

function Period3Type.Period_Specified(Index: Integer): boolean;
begin
  Result := FPeriod_Specified;
end;

procedure Period3Type.SetCalendarWeek(Index: Integer; const AcalendarWeekType: calendarWeekType);
begin
  FCalendarWeek := AcalendarWeekType;
  FCalendarWeek_Specified := True;
end;

function Period3Type.CalendarWeek_Specified(Index: Integer): boolean;
begin
  Result := FCalendarWeek_Specified;
end;

procedure PredefinedPeriod.SetYear(Index: Integer; const AyearType: yearType);
begin
  FYear := AyearType;
  FYear_Specified := True;
end;

function PredefinedPeriod.Year_Specified(Index: Integer): boolean;
begin
  Result := FYear_Specified;
end;

procedure PredefinedPeriod.SetInterval(Index: Integer; const Aperiod1Type: period1Type);
begin
  FInterval := Aperiod1Type;
  FInterval_Specified := True;
end;

function PredefinedPeriod.Interval_Specified(Index: Integer): boolean;
begin
  Result := FInterval_Specified;
end;

procedure PredefinedPeriod.SetCalendarWeek(Index: Integer; const AcalendarWeekType: calendarWeekType);
begin
  FCalendarWeek := AcalendarWeekType;
  FCalendarWeek_Specified := True;
end;

function PredefinedPeriod.CalendarWeek_Specified(Index: Integer): boolean;
begin
  Result := FCalendarWeek_Specified;
end;

procedure PredefinedPeriod2.SetYear(Index: Integer; const AyearType: yearType);
begin
  FYear := AyearType;
  FYear_Specified := True;
end;

function PredefinedPeriod2.Year_Specified(Index: Integer): boolean;
begin
  Result := FYear_Specified;
end;

procedure PredefinedPeriod2.SetInterval(Index: Integer; const Aperiod1Type: period1Type);
begin
  FInterval := Aperiod1Type;
  FInterval_Specified := True;
end;

function PredefinedPeriod2.Interval_Specified(Index: Integer): boolean;
begin
  Result := FInterval_Specified;
end;

procedure PredefinedPeriod2.SetCalendarWeek(Index: Integer; const AcalendarWeekType: calendarWeekType);
begin
  FCalendarWeek := AcalendarWeekType;
  FCalendarWeek_Specified := True;
end;

function PredefinedPeriod2.CalendarWeek_Specified(Index: Integer): boolean;
begin
  Result := FCalendarWeek_Specified;
end;

procedure PredefinedPeriod3.SetYear(Index: Integer; const AyearType: yearType);
begin
  FYear := AyearType;
  FYear_Specified := True;
end;

function PredefinedPeriod3.Year_Specified(Index: Integer): boolean;
begin
  Result := FYear_Specified;
end;

procedure PredefinedPeriod3.SetInterval(Index: Integer; const Aperiod1Type: period1Type);
begin
  FInterval := Aperiod1Type;
  FInterval_Specified := True;
end;

function PredefinedPeriod3.Interval_Specified(Index: Integer): boolean;
begin
  Result := FInterval_Specified;
end;

procedure PredefinedPeriod3.SetCalendarWeek(Index: Integer; const AcalendarWeekType: calendarWeekType);
begin
  FCalendarWeek := AcalendarWeekType;
  FCalendarWeek_Specified := True;
end;

function PredefinedPeriod3.CalendarWeek_Specified(Index: Integer): boolean;
begin
  Result := FCalendarWeek_Specified;
end;

destructor VehicleDataSet.Destroy;
begin
  SysUtils.FreeAndNil(FPeriod);
  SysUtils.FreeAndNil(FGrades);
  SysUtils.FreeAndNil(FAvgValues);
  SysUtils.FreeAndNil(FConsumption);
  SysUtils.FreeAndNil(FPowerTakeOff);
  SysUtils.FreeAndNil(FAssistanceSystems);
  SysUtils.FreeAndNil(FCharts);
  inherited Destroy;
end;

procedure VehicleDataSet.SetConsumption(Index: Integer; const AConsumption2: Consumption2);
begin
  FConsumption := AConsumption2;
  FConsumption_Specified := True;
end;

function VehicleDataSet.Consumption_Specified(Index: Integer): boolean;
begin
  Result := FConsumption_Specified;
end;

procedure VehicleDataSet.SetPowerTakeOff(Index: Integer; const APowerTakeOff: PowerTakeOff);
begin
  FPowerTakeOff := APowerTakeOff;
  FPowerTakeOff_Specified := True;
end;

function VehicleDataSet.PowerTakeOff_Specified(Index: Integer): boolean;
begin
  Result := FPowerTakeOff_Specified;
end;

procedure VehicleDataSet.SetBrakingDistance(Index: Integer; const AdistanceType: distanceType);
begin
  FBrakingDistance := AdistanceType;
  FBrakingDistance_Specified := True;
end;

function VehicleDataSet.BrakingDistance_Specified(Index: Integer): boolean;
begin
  Result := FBrakingDistance_Specified;
end;

procedure VehicleDataSet.SetNrOfStops(Index: Integer; const AInt64: Int64);
begin
  FNrOfStops := AInt64;
  FNrOfStops_Specified := True;
end;

function VehicleDataSet.NrOfStops_Specified(Index: Integer): boolean;
begin
  Result := FNrOfStops_Specified;
end;

procedure VehicleDataSet.SetHandBrakeUsageCount(Index: Integer; const AInt64: Int64);
begin
  FHandBrakeUsageCount := AInt64;
  FHandBrakeUsageCount_Specified := True;
end;

function VehicleDataSet.HandBrakeUsageCount_Specified(Index: Integer): boolean;
begin
  Result := FHandBrakeUsageCount_Specified;
end;

procedure VehicleDataSet.SetMovingTime(Index: Integer; const AInt64: Int64);
begin
  FMovingTime := AInt64;
  FMovingTime_Specified := True;
end;

function VehicleDataSet.MovingTime_Specified(Index: Integer): boolean;
begin
  Result := FMovingTime_Specified;
end;

procedure VehicleDataSet.SetStopTimeEngineOn(Index: Integer; const AtimeType: timeType);
begin
  FStopTimeEngineOn := AtimeType;
  FStopTimeEngineOn_Specified := True;
end;

function VehicleDataSet.StopTimeEngineOn_Specified(Index: Integer): boolean;
begin
  Result := FStopTimeEngineOn_Specified;
end;

procedure VehicleDataSet.SetStopTimeEngineOff(Index: Integer; const AtimeType: timeType);
begin
  FStopTimeEngineOff := AtimeType;
  FStopTimeEngineOff_Specified := True;
end;

function VehicleDataSet.StopTimeEngineOff_Specified(Index: Integer): boolean;
begin
  Result := FStopTimeEngineOff_Specified;
end;

procedure VehicleDataSet.SetSpeedOver85PerTotalDistance(Index: Integer; const AspeedType: speedType);
begin
  FSpeedOver85PerTotalDistance := AspeedType;
  FSpeedOver85PerTotalDistance_Specified := True;
end;

function VehicleDataSet.SpeedOver85PerTotalDistance_Specified(Index: Integer): boolean;
begin
  Result := FSpeedOver85PerTotalDistance_Specified;
end;

procedure VehicleDataSet.SetRetarderBrakingDistance(Index: Integer; const AdistanceType: distanceType);
begin
  FRetarderBrakingDistance := AdistanceType;
  FRetarderBrakingDistance_Specified := True;
end;

function VehicleDataSet.RetarderBrakingDistance_Specified(Index: Integer): boolean;
begin
  Result := FRetarderBrakingDistance_Specified;
end;

procedure VehicleDataSet.SetTotalCoastingDistance(Index: Integer; const AdistanceType: distanceType);
begin
  FTotalCoastingDistance := AdistanceType;
  FTotalCoastingDistance_Specified := True;
end;

function VehicleDataSet.TotalCoastingDistance_Specified(Index: Integer): boolean;
begin
  Result := FTotalCoastingDistance_Specified;
end;

procedure VehicleDataSet.SetTotalDistance(Index: Integer; const AdistanceType: distanceType);
begin
  FTotalDistance := AdistanceType;
  FTotalDistance_Specified := True;
end;

function VehicleDataSet.TotalDistance_Specified(Index: Integer): boolean;
begin
  Result := FTotalDistance_Specified;
end;

procedure VehicleDataSet.SetCruiseControlDistance(Index: Integer; const AdistanceType: distanceType);
begin
  FCruiseControlDistance := AdistanceType;
  FCruiseControlDistance_Specified := True;
end;

function VehicleDataSet.CruiseControlDistance_Specified(Index: Integer): boolean;
begin
  Result := FCruiseControlDistance_Specified;
end;

procedure VehicleDataSet.SetAssistanceSystems(Index: Integer; const AassistanceSystems: assistanceSystems);
begin
  FAssistanceSystems := AassistanceSystems;
  FAssistanceSystems_Specified := True;
end;

function VehicleDataSet.AssistanceSystems_Specified(Index: Integer): boolean;
begin
  Result := FAssistanceSystems_Specified;
end;

procedure VehicleDataSet.SetCharts(Index: Integer; const AChartsType: ChartsType);
begin
  FCharts := AChartsType;
  FCharts_Specified := True;
end;

function VehicleDataSet.Charts_Specified(Index: Integer): boolean;
begin
  Result := FCharts_Specified;
end;

destructor VehicleDataSet2.Destroy;
begin
  SysUtils.FreeAndNil(FPeriod);
  SysUtils.FreeAndNil(FGrades);
  SysUtils.FreeAndNil(FAvgValues);
  SysUtils.FreeAndNil(FConsumption);
  SysUtils.FreeAndNil(FCharts);
  inherited Destroy;
end;

procedure VehicleDataSet2.SetTotalDistance(Index: Integer; const AdistanceType: distanceType);
begin
  FTotalDistance := AdistanceType;
  FTotalDistance_Specified := True;
end;

function VehicleDataSet2.TotalDistance_Specified(Index: Integer): boolean;
begin
  Result := FTotalDistance_Specified;
end;

procedure VehicleDataSet2.SetMovingTime(Index: Integer; const AInt64: Int64);
begin
  FMovingTime := AInt64;
  FMovingTime_Specified := True;
end;

function VehicleDataSet2.MovingTime_Specified(Index: Integer): boolean;
begin
  Result := FMovingTime_Specified;
end;

procedure VehicleDataSet2.SetIdlingTimeEngineOn(Index: Integer; const AtimeType: timeType);
begin
  FIdlingTimeEngineOn := AtimeType;
  FIdlingTimeEngineOn_Specified := True;
end;

function VehicleDataSet2.IdlingTimeEngineOn_Specified(Index: Integer): boolean;
begin
  Result := FIdlingTimeEngineOn_Specified;
end;

procedure VehicleDataSet2.SetIdlingTimeEngineOff(Index: Integer; const AtimeType: timeType);
begin
  FIdlingTimeEngineOff := AtimeType;
  FIdlingTimeEngineOff_Specified := True;
end;

function VehicleDataSet2.IdlingTimeEngineOff_Specified(Index: Integer): boolean;
begin
  Result := FIdlingTimeEngineOff_Specified;
end;

procedure VehicleDataSet2.SetPTOTime(Index: Integer; const AtimeType: timeType);
begin
  FPTOTime := AtimeType;
  FPTOTime_Specified := True;
end;

function VehicleDataSet2.PTOTime_Specified(Index: Integer): boolean;
begin
  Result := FPTOTime_Specified;
end;

procedure VehicleDataSet2.SetOverspeedTime(Index: Integer; const AtimeType: timeType);
begin
  FOverspeedTime := AtimeType;
  FOverspeedTime_Specified := True;
end;

function VehicleDataSet2.OverspeedTime_Specified(Index: Integer): boolean;
begin
  Result := FOverspeedTime_Specified;
end;

procedure VehicleDataSet2.SetOverrevTime(Index: Integer; const AtimeType: timeType);
begin
  FOverrevTime := AtimeType;
  FOverrevTime_Specified := True;
end;

function VehicleDataSet2.OverrevTime_Specified(Index: Integer): boolean;
begin
  Result := FOverrevTime_Specified;
end;

procedure VehicleDataSet2.SetOverrevNumber(Index: Integer; const AInt64: Int64);
begin
  FOverrevNumber := AInt64;
  FOverrevNumber_Specified := True;
end;

function VehicleDataSet2.OverrevNumber_Specified(Index: Integer): boolean;
begin
  Result := FOverrevNumber_Specified;
end;

procedure VehicleDataSet2.SetGreenBandRate(Index: Integer; const ASmallInt: SmallInt);
begin
  FGreenBandRate := ASmallInt;
  FGreenBandRate_Specified := True;
end;

function VehicleDataSet2.GreenBandRate_Specified(Index: Integer): boolean;
begin
  Result := FGreenBandRate_Specified;
end;

procedure VehicleDataSet2.SetAccelerationNumber(Index: Integer; const AInt64: Int64);
begin
  FAccelerationNumber := AInt64;
  FAccelerationNumber_Specified := True;
end;

function VehicleDataSet2.AccelerationNumber_Specified(Index: Integer): boolean;
begin
  Result := FAccelerationNumber_Specified;
end;

procedure VehicleDataSet2.SetNumberOfStops(Index: Integer; const AInt64: Int64);
begin
  FNumberOfStops := AInt64;
  FNumberOfStops_Specified := True;
end;

function VehicleDataSet2.NumberOfStops_Specified(Index: Integer): boolean;
begin
  Result := FNumberOfStops_Specified;
end;

procedure VehicleDataSet2.SetPTONumber(Index: Integer; const AInt64: Int64);
begin
  FPTONumber := AInt64;
  FPTONumber_Specified := True;
end;

function VehicleDataSet2.PTONumber_Specified(Index: Integer): boolean;
begin
  Result := FPTONumber_Specified;
end;

procedure VehicleDataSet2.SetHarshBrakeNumber(Index: Integer; const AInt64: Int64);
begin
  FHarshBrakeNumber := AInt64;
  FHarshBrakeNumber_Specified := True;
end;

function VehicleDataSet2.HarshBrakeNumber_Specified(Index: Integer): boolean;
begin
  Result := FHarshBrakeNumber_Specified;
end;

procedure VehicleDataSet2.SetCharts(Index: Integer; const AChartsType: ChartsType);
begin
  FCharts := AChartsType;
  FCharts_Specified := True;
end;

function VehicleDataSet2.Charts_Specified(Index: Integer): boolean;
begin
  Result := FCharts_Specified;
end;

procedure PowerTakeOff3.SetPto1(Index: Integer; const AtimeType: timeType);
begin
  FPto1 := AtimeType;
  FPto1_Specified := True;
end;

function PowerTakeOff3.Pto1_Specified(Index: Integer): boolean;
begin
  Result := FPto1_Specified;
end;

procedure PowerTakeOff3.SetPto2(Index: Integer; const AtimeType: timeType);
begin
  FPto2 := AtimeType;
  FPto2_Specified := True;
end;

function PowerTakeOff3.Pto2_Specified(Index: Integer): boolean;
begin
  Result := FPto2_Specified;
end;

procedure PowerTakeOff3.SetPto3(Index: Integer; const AtimeType: timeType);
begin
  FPto3 := AtimeType;
  FPto3_Specified := True;
end;

function PowerTakeOff3.Pto3_Specified(Index: Integer): boolean;
begin
  Result := FPto3_Specified;
end;

procedure Consumption6.SetDriveConsumption(Index: Integer; const AconsumptionType: consumptionType);
begin
  FDriveConsumption := AconsumptionType;
  FDriveConsumption_Specified := True;
end;

function Consumption6.DriveConsumption_Specified(Index: Integer): boolean;
begin
  Result := FDriveConsumption_Specified;
end;

procedure Consumption6.SetTotalConsumption(Index: Integer; const AconsumptionType: consumptionType);
begin
  FTotalConsumption := AconsumptionType;
  FTotalConsumption_Specified := True;
end;

function Consumption6.TotalConsumption_Specified(Index: Integer): boolean;
begin
  Result := FTotalConsumption_Specified;
end;

procedure Consumption6.SetIdleConsumption(Index: Integer; const AconsumptionType: consumptionType);
begin
  FIdleConsumption := AconsumptionType;
  FIdleConsumption_Specified := True;
end;

function Consumption6.IdleConsumption_Specified(Index: Integer): boolean;
begin
  Result := FIdleConsumption_Specified;
end;

procedure Consumption6.SetAdBlueConsumption(Index: Integer; const AconsumptionType: consumptionType);
begin
  FAdBlueConsumption := AconsumptionType;
  FAdBlueConsumption_Specified := True;
end;

function Consumption6.AdBlueConsumption_Specified(Index: Integer): boolean;
begin
  Result := FAdBlueConsumption_Specified;
end;

procedure Consumption6.SetIdleConsumptionPTOActive(Index: Integer; const AconsumptionType: consumptionType);
begin
  FIdleConsumptionPTOActive := AconsumptionType;
  FIdleConsumptionPTOActive_Specified := True;
end;

function Consumption6.IdleConsumptionPTOActive_Specified(Index: Integer): boolean;
begin
  Result := FIdleConsumptionPTOActive_Specified;
end;

procedure Consumption6.SetIdleConsumptionPTOInactive(Index: Integer; const AconsumptionType: consumptionType);
begin
  FIdleConsumptionPTOInactive := AconsumptionType;
  FIdleConsumptionPTOInactive_Specified := True;
end;

function Consumption6.IdleConsumptionPTOInactive_Specified(Index: Integer): boolean;
begin
  Result := FIdleConsumptionPTOInactive_Specified;
end;

destructor ToursSummary3.Destroy;
begin
  SysUtils.FreeAndNil(FTimeRange);
  inherited Destroy;
end;

procedure AvgValues6.SetWeight(Index: Integer; const AweightType: weightType);
begin
  FWeight := AweightType;
  FWeight_Specified := True;
end;

function AvgValues6.Weight_Specified(Index: Integer): boolean;
begin
  Result := FWeight_Specified;
end;

procedure AvgValues6.SetSpeed(Index: Integer; const AspeedType: speedType);
begin
  FSpeed := AspeedType;
  FSpeed_Specified := True;
end;

function AvgValues6.Speed_Specified(Index: Integer): boolean;
begin
  Result := FSpeed_Specified;
end;

procedure AvgValues6.SetOverallConsumption(Index: Integer; const AconsumptionType: consumptionType);
begin
  FOverallConsumption := AconsumptionType;
  FOverallConsumption_Specified := True;
end;

function AvgValues6.OverallConsumption_Specified(Index: Integer): boolean;
begin
  Result := FOverallConsumption_Specified;
end;

procedure AvgValues6.SetDriveConsumption(Index: Integer; const AconsumptionType: consumptionType);
begin
  FDriveConsumption := AconsumptionType;
  FDriveConsumption_Specified := True;
end;

function AvgValues6.DriveConsumption_Specified(Index: Integer): boolean;
begin
  Result := FDriveConsumption_Specified;
end;

procedure AvgValues6.SetAdBlueConsumption(Index: Integer; const AconsumptionType: consumptionType);
begin
  FAdBlueConsumption := AconsumptionType;
  FAdBlueConsumption_Specified := True;
end;

function AvgValues6.AdBlueConsumption_Specified(Index: Integer): boolean;
begin
  Result := FAdBlueConsumption_Specified;
end;

procedure AvgValues6.SetAdBlueToFuelRatioConsumption(Index: Integer; const AconsumptionType: consumptionType);
begin
  FAdBlueToFuelRatioConsumption := AconsumptionType;
  FAdBlueToFuelRatioConsumption_Specified := True;
end;

function AvgValues6.AdBlueToFuelRatioConsumption_Specified(Index: Integer): boolean;
begin
  Result := FAdBlueToFuelRatioConsumption_Specified;
end;

procedure PredefinedPeriod4.SetInterval(Index: Integer; const Aperiod1Type: period1Type);
begin
  FInterval := Aperiod1Type;
  FInterval_Specified := True;
end;

function PredefinedPeriod4.Interval_Specified(Index: Integer): boolean;
begin
  Result := FInterval_Specified;
end;

procedure PredefinedPeriod4.SetCalendarWeek(Index: Integer; const AcalendarWeekType: calendarWeekType);
begin
  FCalendarWeek := AcalendarWeekType;
  FCalendarWeek_Specified := True;
end;

function PredefinedPeriod4.CalendarWeek_Specified(Index: Integer): boolean;
begin
  Result := FCalendarWeek_Specified;
end;

procedure Grades8.SetDrivingStyle(Index: Integer; const AgradeType: gradeType);
begin
  FDrivingStyle := AgradeType;
  FDrivingStyle_Specified := True;
end;

function Grades8.DrivingStyle_Specified(Index: Integer): boolean;
begin
  Result := FDrivingStyle_Specified;
end;

procedure Grades8.SetDrivingStyleCons(Index: Integer; const AgradeType: gradeType);
begin
  FDrivingStyleCons := AgradeType;
  FDrivingStyleCons_Specified := True;
end;

function Grades8.DrivingStyleCons_Specified(Index: Integer): boolean;
begin
  Result := FDrivingStyleCons_Specified;
end;

procedure Grades8.SetDrivingStyleConsPreventive(Index: Integer; const AgradeType: gradeType);
begin
  FDrivingStyleConsPreventive := AgradeType;
  FDrivingStyleConsPreventive_Specified := True;
end;

function Grades8.DrivingStyleConsPreventive_Specified(Index: Integer): boolean;
begin
  Result := FDrivingStyleConsPreventive_Specified;
end;

procedure Grades8.SetDrivingStyleConsEngineOperationMN(Index: Integer; const AgradeType: gradeType);
begin
  FDrivingStyleConsEngineOperationMN := AgradeType;
  FDrivingStyleConsEngineOperationMN_Specified := True;
end;

function Grades8.DrivingStyleConsEngineOperationMN_Specified(Index: Integer): boolean;
begin
  Result := FDrivingStyleConsEngineOperationMN_Specified;
end;

procedure Grades8.SetDrivingStyleConsPedalMovements(Index: Integer; const AgradeType: gradeType);
begin
  FDrivingStyleConsPedalMovements := AgradeType;
  FDrivingStyleConsPedalMovements_Specified := True;
end;

function Grades8.DrivingStyleConsPedalMovements_Specified(Index: Integer): boolean;
begin
  Result := FDrivingStyleConsPedalMovements_Specified;
end;

procedure Grades8.SetDrivingStyleConsUniformSpeed(Index: Integer; const AgradeType: gradeType);
begin
  FDrivingStyleConsUniformSpeed := AgradeType;
  FDrivingStyleConsUniformSpeed_Specified := True;
end;

function Grades8.DrivingStyleConsUniformSpeed_Specified(Index: Integer): boolean;
begin
  Result := FDrivingStyleConsUniformSpeed_Specified;
end;

procedure Grades8.SetDrivingStyleConsStops(Index: Integer; const AgradeType: gradeType);
begin
  FDrivingStyleConsStops := AgradeType;
  FDrivingStyleConsStops_Specified := True;
end;

function Grades8.DrivingStyleConsStops_Specified(Index: Integer): boolean;
begin
  Result := FDrivingStyleConsStops_Specified;
end;

procedure Grades8.SetDrivingStyleBrake(Index: Integer; const AgradeType: gradeType);
begin
  FDrivingStyleBrake := AgradeType;
  FDrivingStyleBrake_Specified := True;
end;

function Grades8.DrivingStyleBrake_Specified(Index: Integer): boolean;
begin
  Result := FDrivingStyleBrake_Specified;
end;

procedure Grades8.SetDrivingStyleBrakePreventive(Index: Integer; const AgradeType: gradeType);
begin
  FDrivingStyleBrakePreventive := AgradeType;
  FDrivingStyleBrakePreventive_Specified := True;
end;

function Grades8.DrivingStyleBrakePreventive_Specified(Index: Integer): boolean;
begin
  Result := FDrivingStyleBrakePreventive_Specified;
end;

procedure Grades8.SetDrivingStyleBrakeDeceleration(Index: Integer; const AgradeType: gradeType);
begin
  FDrivingStyleBrakeDeceleration := AgradeType;
  FDrivingStyleBrakeDeceleration_Specified := True;
end;

function Grades8.DrivingStyleBrakeDeceleration_Specified(Index: Integer): boolean;
begin
  Result := FDrivingStyleBrakeDeceleration_Specified;
end;

procedure Grades8.SetDegreeOfDifficulty(Index: Integer; const AgradeType: gradeType);
begin
  FDegreeOfDifficulty := AgradeType;
  FDegreeOfDifficulty_Specified := True;
end;

function Grades8.DegreeOfDifficulty_Specified(Index: Integer): boolean;
begin
  Result := FDegreeOfDifficulty_Specified;
end;

procedure Grades8.SetDegreeOfDifficultyAverageSlope(Index: Integer; const AgradeType: gradeType);
begin
  FDegreeOfDifficultyAverageSlope := AgradeType;
  FDegreeOfDifficultyAverageSlope_Specified := True;
end;

function Grades8.DegreeOfDifficultyAverageSlope_Specified(Index: Integer): boolean;
begin
  Result := FDegreeOfDifficultyAverageSlope_Specified;
end;

procedure Grades8.SetDegreeOfDifficultyWeight(Index: Integer; const AgradeType: gradeType);
begin
  FDegreeOfDifficultyWeight := AgradeType;
  FDegreeOfDifficultyWeight_Specified := True;
end;

function Grades8.DegreeOfDifficultyWeight_Specified(Index: Integer): boolean;
begin
  Result := FDegreeOfDifficultyWeight_Specified;
end;

procedure Grades8.SetDegreeOfDifficultyStops(Index: Integer; const AgradeType: gradeType);
begin
  FDegreeOfDifficultyStops := AgradeType;
  FDegreeOfDifficultyStops_Specified := True;
end;

function Grades8.DegreeOfDifficultyStops_Specified(Index: Integer): boolean;
begin
  Result := FDegreeOfDifficultyStops_Specified;
end;

procedure Grades8.SetTorqueClassification(Index: Integer; const AgradeType: gradeType);
begin
  FTorqueClassification := AgradeType;
  FTorqueClassification_Specified := True;
end;

function Grades8.TorqueClassification_Specified(Index: Integer): boolean;
begin
  Result := FTorqueClassification_Specified;
end;

destructor Period8.Destroy;
begin
  SysUtils.FreeAndNil(FPeriod);
  inherited Destroy;
end;

procedure Period8.SetBegin_(Index: Integer; const AtimestampType: timestampType);
begin
  FBegin_ := AtimestampType;
  FBegin__Specified := True;
end;

function Period8.Begin__Specified(Index: Integer): boolean;
begin
  Result := FBegin__Specified;
end;

procedure Period8.SetEnd_(Index: Integer; const AtimestampType: timestampType);
begin
  FEnd_ := AtimestampType;
  FEnd__Specified := True;
end;

function Period8.End__Specified(Index: Integer): boolean;
begin
  Result := FEnd__Specified;
end;

procedure Period8.SetPeriod(Index: Integer; const APeriod3Type: Period3Type);
begin
  FPeriod := APeriod3Type;
  FPeriod_Specified := True;
end;

function Period8.Period_Specified(Index: Integer): boolean;
begin
  Result := FPeriod_Specified;
end;

procedure PredefinedPeriod5.SetYear(Index: Integer; const AyearType: yearType);
begin
  FYear := AyearType;
  FYear_Specified := True;
end;

function PredefinedPeriod5.Year_Specified(Index: Integer): boolean;
begin
  Result := FYear_Specified;
end;

procedure PredefinedPeriod5.SetInterval(Index: Integer; const Aperiod1Type: period1Type);
begin
  FInterval := Aperiod1Type;
  FInterval_Specified := True;
end;

function PredefinedPeriod5.Interval_Specified(Index: Integer): boolean;
begin
  Result := FInterval_Specified;
end;

procedure PredefinedPeriod5.SetCalendarWeek(Index: Integer; const AcalendarWeekType: calendarWeekType);
begin
  FCalendarWeek := AcalendarWeekType;
  FCalendarWeek_Specified := True;
end;

function PredefinedPeriod5.CalendarWeek_Specified(Index: Integer): boolean;
begin
  Result := FCalendarWeek_Specified;
end;

destructor UniversalPerformanceAnalysisVehicleOverviewReport.Destroy;
var
  I: Integer;
begin
  for I := 0 to System.Length(FVehicleGroups)-1 do
    SysUtils.FreeAndNil(FVehicleGroups[I]);
  System.SetLength(FVehicleGroups, 0);
  SysUtils.FreeAndNil(FPeriod);
  SysUtils.FreeAndNil(FGrades);
  inherited Destroy;
end;

procedure UniversalPerformanceAnalysisVehicleOverviewReport.SetElementId(Index: Integer; const AInt64: Int64);
begin
  FElementId := AInt64;
  FElementId_Specified := True;
end;

function UniversalPerformanceAnalysisVehicleOverviewReport.ElementId_Specified(Index: Integer): boolean;
begin
  Result := FElementId_Specified;
end;

procedure UniversalPerformanceAnalysisVehicleOverviewReport.SetVehicleId(Index: Integer; const AvehicleidType: vehicleidType);
begin
  FVehicleId := AvehicleidType;
  FVehicleId_Specified := True;
end;

function UniversalPerformanceAnalysisVehicleOverviewReport.VehicleId_Specified(Index: Integer): boolean;
begin
  Result := FVehicleId_Specified;
end;

procedure UniversalPerformanceAnalysisVehicleOverviewReport.SetVehicleGroups(Index: Integer; const AArray_Of_VehicleGroup: Array_Of_VehicleGroup);
begin
  FVehicleGroups := AArray_Of_VehicleGroup;
  FVehicleGroups_Specified := True;
end;

function UniversalPerformanceAnalysisVehicleOverviewReport.VehicleGroups_Specified(Index: Integer): boolean;
begin
  Result := FVehicleGroups_Specified;
end;

procedure UniversalPerformanceAnalysisVehicleOverviewReport.SetVehicleName(Index: Integer; const Astring: string);
begin
  FVehicleName := Astring;
  FVehicleName_Specified := True;
end;

function UniversalPerformanceAnalysisVehicleOverviewReport.VehicleName_Specified(Index: Integer): boolean;
begin
  Result := FVehicleName_Specified;
end;

procedure UniversalPerformanceAnalysisVehicleOverviewReport.SetGrades(Index: Integer; const AGrades: Grades);
begin
  FGrades := AGrades;
  FGrades_Specified := True;
end;

function UniversalPerformanceAnalysisVehicleOverviewReport.Grades_Specified(Index: Integer): boolean;
begin
  Result := FGrades_Specified;
end;

procedure UniversalPerformanceAnalysisVehicleOverviewReport.SetAverageTotalConsumption(Index: Integer; const AconsumptionType: consumptionType);
begin
  FAverageTotalConsumption := AconsumptionType;
  FAverageTotalConsumption_Specified := True;
end;

function UniversalPerformanceAnalysisVehicleOverviewReport.AverageTotalConsumption_Specified(Index: Integer): boolean;
begin
  Result := FAverageTotalConsumption_Specified;
end;

procedure UniversalPerformanceAnalysisVehicleOverviewReport.SetTotalDistance(Index: Integer; const AdistanceType: distanceType);
begin
  FTotalDistance := AdistanceType;
  FTotalDistance_Specified := True;
end;

function UniversalPerformanceAnalysisVehicleOverviewReport.TotalDistance_Specified(Index: Integer): boolean;
begin
  Result := FTotalDistance_Specified;
end;

procedure UniversalPerformanceAnalysisVehicleOverviewReport.SetNumberOfVehicles(Index: Integer; const AInteger: Integer);
begin
  FNumberOfVehicles := AInteger;
  FNumberOfVehicles_Specified := True;
end;

function UniversalPerformanceAnalysisVehicleOverviewReport.NumberOfVehicles_Specified(Index: Integer): boolean;
begin
  Result := FNumberOfVehicles_Specified;
end;

destructor ToursSummary4.Destroy;
begin
  SysUtils.FreeAndNil(FTimeRange);
  inherited Destroy;
end;

procedure PredefinedPeriod6.SetYear(Index: Integer; const AyearType: yearType);
begin
  FYear := AyearType;
  FYear_Specified := True;
end;

function PredefinedPeriod6.Year_Specified(Index: Integer): boolean;
begin
  Result := FYear_Specified;
end;

procedure PredefinedPeriod6.SetInterval(Index: Integer; const Aperiod1Type: period1Type);
begin
  FInterval := Aperiod1Type;
  FInterval_Specified := True;
end;

function PredefinedPeriod6.Interval_Specified(Index: Integer): boolean;
begin
  Result := FInterval_Specified;
end;

procedure PredefinedPeriod6.SetCalendarWeek(Index: Integer; const AcalendarWeekType: calendarWeekType);
begin
  FCalendarWeek := AcalendarWeekType;
  FCalendarWeek_Specified := True;
end;

function PredefinedPeriod6.CalendarWeek_Specified(Index: Integer): boolean;
begin
  Result := FCalendarWeek_Specified;
end;

destructor GetUniversalPerformanceAnalysisVehicleOverviewRequestType.Destroy;
begin
  SysUtils.FreeAndNil(FAnalysis);
  SysUtils.FreeAndNil(FDrivingStyle);
  SysUtils.FreeAndNil(FIdleTimeGrading);
  SysUtils.FreeAndNil(FOverrevNumberGrading);
  SysUtils.FreeAndNil(FOverrevTimeGrading);
  SysUtils.FreeAndNil(FEcoDrivingGrading);
  SysUtils.FreeAndNil(FHarshBrakeGrading);
  SysUtils.FreeAndNil(FOverspeedTimeGrading);
  SysUtils.FreeAndNil(FAvgTotalConsumption);
  inherited Destroy;
end;

procedure GetUniversalPerformanceAnalysisVehicleOverviewRequestType.Setsessionid(Index: Integer; const AsessionidType: sessionidType);
begin
  Fsessionid := AsessionidType;
  Fsessionid_Specified := True;
end;

function GetUniversalPerformanceAnalysisVehicleOverviewRequestType.sessionid_Specified(Index: Integer): boolean;
begin
  Result := Fsessionid_Specified;
end;

procedure GetUniversalPerformanceAnalysisVehicleOverviewRequestType.Setlimit(Index: Integer; const AlimitType: limitType);
begin
  Flimit := AlimitType;
  Flimit_Specified := True;
end;

function GetUniversalPerformanceAnalysisVehicleOverviewRequestType.limit_Specified(Index: Integer): boolean;
begin
  Result := Flimit_Specified;
end;

procedure GetUniversalPerformanceAnalysisVehicleOverviewRequestType.Setoffset(Index: Integer; const AoffsetType: offsetType);
begin
  Foffset := AoffsetType;
  Foffset_Specified := True;
end;

function GetUniversalPerformanceAnalysisVehicleOverviewRequestType.offset_Specified(Index: Integer): boolean;
begin
  Result := Foffset_Specified;
end;

procedure GetUniversalPerformanceAnalysisVehicleOverviewRequestType.Setversion(Index: Integer; const AversionType: versionType);
begin
  Fversion := AversionType;
  Fversion_Specified := True;
end;

function GetUniversalPerformanceAnalysisVehicleOverviewRequestType.version_Specified(Index: Integer): boolean;
begin
  Result := Fversion_Specified;
end;

procedure GetUniversalPerformanceAnalysisVehicleOverviewRequestType.SetVehicleId(Index: Integer; const AvehicleidType: vehicleidType);
begin
  FVehicleId := AvehicleidType;
  FVehicleId_Specified := True;
end;

function GetUniversalPerformanceAnalysisVehicleOverviewRequestType.VehicleId_Specified(Index: Integer): boolean;
begin
  Result := FVehicleId_Specified;
end;

procedure GetUniversalPerformanceAnalysisVehicleOverviewRequestType.SetVehicleGroupId(Index: Integer; const AvehicleGroupIDType: vehicleGroupIDType);
begin
  FVehicleGroupId := AvehicleGroupIDType;
  FVehicleGroupId_Specified := True;
end;

function GetUniversalPerformanceAnalysisVehicleOverviewRequestType.VehicleGroupId_Specified(Index: Integer): boolean;
begin
  Result := FVehicleGroupId_Specified;
end;

procedure GetUniversalPerformanceAnalysisVehicleOverviewRequestType.SetDrivingStyle(Index: Integer; const AMinMaxGradeType: MinMaxGradeType);
begin
  FDrivingStyle := AMinMaxGradeType;
  FDrivingStyle_Specified := True;
end;

function GetUniversalPerformanceAnalysisVehicleOverviewRequestType.DrivingStyle_Specified(Index: Integer): boolean;
begin
  Result := FDrivingStyle_Specified;
end;

procedure GetUniversalPerformanceAnalysisVehicleOverviewRequestType.SetIdleTimeGrading(Index: Integer; const AMinMaxGradeType: MinMaxGradeType);
begin
  FIdleTimeGrading := AMinMaxGradeType;
  FIdleTimeGrading_Specified := True;
end;

function GetUniversalPerformanceAnalysisVehicleOverviewRequestType.IdleTimeGrading_Specified(Index: Integer): boolean;
begin
  Result := FIdleTimeGrading_Specified;
end;

procedure GetUniversalPerformanceAnalysisVehicleOverviewRequestType.SetOverrevNumberGrading(Index: Integer; const AMinMaxGradeType: MinMaxGradeType);
begin
  FOverrevNumberGrading := AMinMaxGradeType;
  FOverrevNumberGrading_Specified := True;
end;

function GetUniversalPerformanceAnalysisVehicleOverviewRequestType.OverrevNumberGrading_Specified(Index: Integer): boolean;
begin
  Result := FOverrevNumberGrading_Specified;
end;

procedure GetUniversalPerformanceAnalysisVehicleOverviewRequestType.SetOverrevTimeGrading(Index: Integer; const AMinMaxGradeType: MinMaxGradeType);
begin
  FOverrevTimeGrading := AMinMaxGradeType;
  FOverrevTimeGrading_Specified := True;
end;

function GetUniversalPerformanceAnalysisVehicleOverviewRequestType.OverrevTimeGrading_Specified(Index: Integer): boolean;
begin
  Result := FOverrevTimeGrading_Specified;
end;

procedure GetUniversalPerformanceAnalysisVehicleOverviewRequestType.SetEcoDrivingGrading(Index: Integer; const AMinMaxGradeType: MinMaxGradeType);
begin
  FEcoDrivingGrading := AMinMaxGradeType;
  FEcoDrivingGrading_Specified := True;
end;

function GetUniversalPerformanceAnalysisVehicleOverviewRequestType.EcoDrivingGrading_Specified(Index: Integer): boolean;
begin
  Result := FEcoDrivingGrading_Specified;
end;

procedure GetUniversalPerformanceAnalysisVehicleOverviewRequestType.SetHarshBrakeGrading(Index: Integer; const AMinMaxGradeType: MinMaxGradeType);
begin
  FHarshBrakeGrading := AMinMaxGradeType;
  FHarshBrakeGrading_Specified := True;
end;

function GetUniversalPerformanceAnalysisVehicleOverviewRequestType.HarshBrakeGrading_Specified(Index: Integer): boolean;
begin
  Result := FHarshBrakeGrading_Specified;
end;

procedure GetUniversalPerformanceAnalysisVehicleOverviewRequestType.SetOverspeedTimeGrading(Index: Integer; const AMinMaxGradeType: MinMaxGradeType);
begin
  FOverspeedTimeGrading := AMinMaxGradeType;
  FOverspeedTimeGrading_Specified := True;
end;

function GetUniversalPerformanceAnalysisVehicleOverviewRequestType.OverspeedTimeGrading_Specified(Index: Integer): boolean;
begin
  Result := FOverspeedTimeGrading_Specified;
end;

procedure GetUniversalPerformanceAnalysisVehicleOverviewRequestType.SetAvgTotalConsumption(Index: Integer; const AMinMaxConsumptionType: MinMaxConsumptionType);
begin
  FAvgTotalConsumption := AMinMaxConsumptionType;
  FAvgTotalConsumption_Specified := True;
end;

function GetUniversalPerformanceAnalysisVehicleOverviewRequestType.AvgTotalConsumption_Specified(Index: Integer): boolean;
begin
  Result := FAvgTotalConsumption_Specified;
end;

destructor ToursSummary5.Destroy;
begin
  SysUtils.FreeAndNil(FTimeRange);
  inherited Destroy;
end;

destructor ToursSummary6.Destroy;
begin
  SysUtils.FreeAndNil(FTimeRange);
  inherited Destroy;
end;

procedure ToursSummary6.SetTimeRange(Index: Integer; const ATimeRangeType: TimeRangeType);
begin
  FTimeRange := ATimeRangeType;
  FTimeRange_Specified := True;
end;

function ToursSummary6.TimeRange_Specified(Index: Integer): boolean;
begin
  Result := FTimeRange_Specified;
end;

destructor MBPerformanceAnalysisVehicleOverviewReport.Destroy;
begin
  SysUtils.FreeAndNil(FPeriod);
  SysUtils.FreeAndNil(FGrades);
  SysUtils.FreeAndNil(FAvgValues);
  SysUtils.FreeAndNil(FConsumption);
  SysUtils.FreeAndNil(FVehicleStateTimes);
  inherited Destroy;
end;

procedure MBPerformanceAnalysisVehicleOverviewReport.SetConsumption(Index: Integer; const AConsumptionValuesType: ConsumptionValuesType);
begin
  FConsumption := AConsumptionValuesType;
  FConsumption_Specified := True;
end;

function MBPerformanceAnalysisVehicleOverviewReport.Consumption_Specified(Index: Integer): boolean;
begin
  Result := FConsumption_Specified;
end;

procedure MBPerformanceAnalysisVehicleOverviewReport.SetTotalDistance(Index: Integer; const AdistanceType: distanceType);
begin
  FTotalDistance := AdistanceType;
  FTotalDistance_Specified := True;
end;

function MBPerformanceAnalysisVehicleOverviewReport.TotalDistance_Specified(Index: Integer): boolean;
begin
  Result := FTotalDistance_Specified;
end;

procedure MBPerformanceAnalysisVehicleOverviewReport.SetVehicleStateTimes(Index: Integer; const AVehicleStateTimeValuesType: VehicleStateTimeValuesType);
begin
  FVehicleStateTimes := AVehicleStateTimeValuesType;
  FVehicleStateTimes_Specified := True;
end;

function MBPerformanceAnalysisVehicleOverviewReport.VehicleStateTimes_Specified(Index: Integer): boolean;
begin
  Result := FVehicleStateTimes_Specified;
end;

procedure MBPerformanceAnalysisVehicleOverviewReport.SetNumberOfVehicles(Index: Integer; const AInteger: Integer);
begin
  FNumberOfVehicles := AInteger;
  FNumberOfVehicles_Specified := True;
end;

function MBPerformanceAnalysisVehicleOverviewReport.NumberOfVehicles_Specified(Index: Integer): boolean;
begin
  Result := FNumberOfVehicles_Specified;
end;

destructor GetUniversalPerformanceAnalysisDriverOverviewResponseType.Destroy;
var
  I: Integer;
begin
  for I := 0 to System.Length(FUniversalPerformanceAnalysisDriverOverviewReport)-1 do
    SysUtils.FreeAndNil(FUniversalPerformanceAnalysisDriverOverviewReport[I]);
  System.SetLength(FUniversalPerformanceAnalysisDriverOverviewReport, 0);
  inherited Destroy;
end;

procedure GetUniversalPerformanceAnalysisDriverOverviewResponseType.Setlimit(Index: Integer; const AlimitType: limitType);
begin
  Flimit := AlimitType;
  Flimit_Specified := True;
end;

function GetUniversalPerformanceAnalysisDriverOverviewResponseType.limit_Specified(Index: Integer): boolean;
begin
  Result := Flimit_Specified;
end;

procedure GetUniversalPerformanceAnalysisDriverOverviewResponseType.Setoffset(Index: Integer; const AoffsetType: offsetType);
begin
  Foffset := AoffsetType;
  Foffset_Specified := True;
end;

function GetUniversalPerformanceAnalysisDriverOverviewResponseType.offset_Specified(Index: Integer): boolean;
begin
  Result := Foffset_Specified;
end;

procedure GetUniversalPerformanceAnalysisDriverOverviewResponseType.SetresultSize(Index: Integer; const AresultSizeType: resultSizeType);
begin
  FresultSize := AresultSizeType;
  FresultSize_Specified := True;
end;

function GetUniversalPerformanceAnalysisDriverOverviewResponseType.resultSize_Specified(Index: Integer): boolean;
begin
  Result := FresultSize_Specified;
end;

procedure GetUniversalPerformanceAnalysisDriverOverviewResponseType.SetresponseSize(Index: Integer; const AresponseSizeType: responseSizeType);
begin
  FresponseSize := AresponseSizeType;
  FresponseSize_Specified := True;
end;

function GetUniversalPerformanceAnalysisDriverOverviewResponseType.responseSize_Specified(Index: Integer): boolean;
begin
  Result := FresponseSize_Specified;
end;

procedure GetUniversalPerformanceAnalysisDriverOverviewResponseType.SetUniversalPerformanceAnalysisDriverOverviewReport(Index: Integer; const AArray_Of_UniversalPerformanceAnalysisDriverOverviewReport: Array_Of_UniversalPerformanceAnalysisDriverOverviewReport);
begin
  FUniversalPerformanceAnalysisDriverOverviewReport := AArray_Of_UniversalPerformanceAnalysisDriverOverviewReport;
  FUniversalPerformanceAnalysisDriverOverviewReport_Specified := True;
end;

function GetUniversalPerformanceAnalysisDriverOverviewResponseType.UniversalPerformanceAnalysisDriverOverviewReport_Specified(Index: Integer): boolean;
begin
  Result := FUniversalPerformanceAnalysisDriverOverviewReport_Specified;
end;

destructor GetUniversalPerformanceAnalysisVehicleOverviewResponseType.Destroy;
var
  I: Integer;
begin
  for I := 0 to System.Length(FUniversalPerformanceAnalysisVehicleOverviewReport)-1 do
    SysUtils.FreeAndNil(FUniversalPerformanceAnalysisVehicleOverviewReport[I]);
  System.SetLength(FUniversalPerformanceAnalysisVehicleOverviewReport, 0);
  inherited Destroy;
end;

procedure GetUniversalPerformanceAnalysisVehicleOverviewResponseType.Setlimit(Index: Integer; const AlimitType: limitType);
begin
  Flimit := AlimitType;
  Flimit_Specified := True;
end;

function GetUniversalPerformanceAnalysisVehicleOverviewResponseType.limit_Specified(Index: Integer): boolean;
begin
  Result := Flimit_Specified;
end;

procedure GetUniversalPerformanceAnalysisVehicleOverviewResponseType.Setoffset(Index: Integer; const AoffsetType: offsetType);
begin
  Foffset := AoffsetType;
  Foffset_Specified := True;
end;

function GetUniversalPerformanceAnalysisVehicleOverviewResponseType.offset_Specified(Index: Integer): boolean;
begin
  Result := Foffset_Specified;
end;

procedure GetUniversalPerformanceAnalysisVehicleOverviewResponseType.SetresultSize(Index: Integer; const AresultSizeType: resultSizeType);
begin
  FresultSize := AresultSizeType;
  FresultSize_Specified := True;
end;

function GetUniversalPerformanceAnalysisVehicleOverviewResponseType.resultSize_Specified(Index: Integer): boolean;
begin
  Result := FresultSize_Specified;
end;

procedure GetUniversalPerformanceAnalysisVehicleOverviewResponseType.SetresponseSize(Index: Integer; const AresponseSizeType: responseSizeType);
begin
  FresponseSize := AresponseSizeType;
  FresponseSize_Specified := True;
end;

function GetUniversalPerformanceAnalysisVehicleOverviewResponseType.responseSize_Specified(Index: Integer): boolean;
begin
  Result := FresponseSize_Specified;
end;

procedure GetUniversalPerformanceAnalysisVehicleOverviewResponseType.SetUniversalPerformanceAnalysisVehicleOverviewReport(Index: Integer; const AArray_Of_UniversalPerformanceAnalysisVehicleOverviewReport: Array_Of_UniversalPerformanceAnalysisVehicleOverviewReport);
begin
  FUniversalPerformanceAnalysisVehicleOverviewReport := AArray_Of_UniversalPerformanceAnalysisVehicleOverviewReport;
  FUniversalPerformanceAnalysisVehicleOverviewReport_Specified := True;
end;

function GetUniversalPerformanceAnalysisVehicleOverviewResponseType.UniversalPerformanceAnalysisVehicleOverviewReport_Specified(Index: Integer): boolean;
begin
  Result := FUniversalPerformanceAnalysisVehicleOverviewReport_Specified;
end;

destructor GetMBPerformanceAnalysisVehicleResponseType.Destroy;
var
  I: Integer;
begin
  for I := 0 to System.Length(FMBPerformanceAnalysisVehicleReport)-1 do
    SysUtils.FreeAndNil(FMBPerformanceAnalysisVehicleReport[I]);
  System.SetLength(FMBPerformanceAnalysisVehicleReport, 0);
  inherited Destroy;
end;

procedure GetMBPerformanceAnalysisVehicleResponseType.Setlimit(Index: Integer; const AlimitType: limitType);
begin
  Flimit := AlimitType;
  Flimit_Specified := True;
end;

function GetMBPerformanceAnalysisVehicleResponseType.limit_Specified(Index: Integer): boolean;
begin
  Result := Flimit_Specified;
end;

procedure GetMBPerformanceAnalysisVehicleResponseType.Setoffset(Index: Integer; const AoffsetType: offsetType);
begin
  Foffset := AoffsetType;
  Foffset_Specified := True;
end;

function GetMBPerformanceAnalysisVehicleResponseType.offset_Specified(Index: Integer): boolean;
begin
  Result := Foffset_Specified;
end;

procedure GetMBPerformanceAnalysisVehicleResponseType.SetresultSize(Index: Integer; const AresultSizeType: resultSizeType);
begin
  FresultSize := AresultSizeType;
  FresultSize_Specified := True;
end;

function GetMBPerformanceAnalysisVehicleResponseType.resultSize_Specified(Index: Integer): boolean;
begin
  Result := FresultSize_Specified;
end;

procedure GetMBPerformanceAnalysisVehicleResponseType.SetresponseSize(Index: Integer; const AresponseSizeType: responseSizeType);
begin
  FresponseSize := AresponseSizeType;
  FresponseSize_Specified := True;
end;

function GetMBPerformanceAnalysisVehicleResponseType.responseSize_Specified(Index: Integer): boolean;
begin
  Result := FresponseSize_Specified;
end;

procedure GetMBPerformanceAnalysisVehicleResponseType.Setversion(Index: Integer; const AversionType: versionType);
begin
  Fversion := AversionType;
  Fversion_Specified := True;
end;

function GetMBPerformanceAnalysisVehicleResponseType.version_Specified(Index: Integer): boolean;
begin
  Result := Fversion_Specified;
end;

procedure GetMBPerformanceAnalysisVehicleResponseType.SetMBPerformanceAnalysisVehicleReport(Index: Integer; const AArray_Of_MBPerformanceAnalysisVehicleReport: Array_Of_MBPerformanceAnalysisVehicleReport);
begin
  FMBPerformanceAnalysisVehicleReport := AArray_Of_MBPerformanceAnalysisVehicleReport;
  FMBPerformanceAnalysisVehicleReport_Specified := True;
end;

function GetMBPerformanceAnalysisVehicleResponseType.MBPerformanceAnalysisVehicleReport_Specified(Index: Integer): boolean;
begin
  Result := FMBPerformanceAnalysisVehicleReport_Specified;
end;

destructor GetUniversalPerformanceAnalysisVehicleResponseType.Destroy;
var
  I: Integer;
begin
  for I := 0 to System.Length(FUniversalPerformanceAnalysisVehicleReport)-1 do
    SysUtils.FreeAndNil(FUniversalPerformanceAnalysisVehicleReport[I]);
  System.SetLength(FUniversalPerformanceAnalysisVehicleReport, 0);
  inherited Destroy;
end;

procedure GetUniversalPerformanceAnalysisVehicleResponseType.Setlimit(Index: Integer; const AlimitType: limitType);
begin
  Flimit := AlimitType;
  Flimit_Specified := True;
end;

function GetUniversalPerformanceAnalysisVehicleResponseType.limit_Specified(Index: Integer): boolean;
begin
  Result := Flimit_Specified;
end;

procedure GetUniversalPerformanceAnalysisVehicleResponseType.Setoffset(Index: Integer; const AoffsetType: offsetType);
begin
  Foffset := AoffsetType;
  Foffset_Specified := True;
end;

function GetUniversalPerformanceAnalysisVehicleResponseType.offset_Specified(Index: Integer): boolean;
begin
  Result := Foffset_Specified;
end;

procedure GetUniversalPerformanceAnalysisVehicleResponseType.SetresultSize(Index: Integer; const AresultSizeType: resultSizeType);
begin
  FresultSize := AresultSizeType;
  FresultSize_Specified := True;
end;

function GetUniversalPerformanceAnalysisVehicleResponseType.resultSize_Specified(Index: Integer): boolean;
begin
  Result := FresultSize_Specified;
end;

procedure GetUniversalPerformanceAnalysisVehicleResponseType.SetresponseSize(Index: Integer; const AresponseSizeType: responseSizeType);
begin
  FresponseSize := AresponseSizeType;
  FresponseSize_Specified := True;
end;

function GetUniversalPerformanceAnalysisVehicleResponseType.responseSize_Specified(Index: Integer): boolean;
begin
  Result := FresponseSize_Specified;
end;

procedure GetUniversalPerformanceAnalysisVehicleResponseType.SetUniversalPerformanceAnalysisVehicleReport(Index: Integer; const AArray_Of_UniversalPerformanceAnalysisVehicleReport: Array_Of_UniversalPerformanceAnalysisVehicleReport);
begin
  FUniversalPerformanceAnalysisVehicleReport := AArray_Of_UniversalPerformanceAnalysisVehicleReport;
  FUniversalPerformanceAnalysisVehicleReport_Specified := True;
end;

function GetUniversalPerformanceAnalysisVehicleResponseType.UniversalPerformanceAnalysisVehicleReport_Specified(Index: Integer): boolean;
begin
  Result := FUniversalPerformanceAnalysisVehicleReport_Specified;
end;

destructor GetMBPerformanceAnalysisDriverResponseType.Destroy;
var
  I: Integer;
begin
  for I := 0 to System.Length(FMBPerformanceAnalysisDriverReport)-1 do
    SysUtils.FreeAndNil(FMBPerformanceAnalysisDriverReport[I]);
  System.SetLength(FMBPerformanceAnalysisDriverReport, 0);
  inherited Destroy;
end;

procedure GetMBPerformanceAnalysisDriverResponseType.Setlimit(Index: Integer; const AlimitType: limitType);
begin
  Flimit := AlimitType;
  Flimit_Specified := True;
end;

function GetMBPerformanceAnalysisDriverResponseType.limit_Specified(Index: Integer): boolean;
begin
  Result := Flimit_Specified;
end;

procedure GetMBPerformanceAnalysisDriverResponseType.Setoffset(Index: Integer; const AoffsetType: offsetType);
begin
  Foffset := AoffsetType;
  Foffset_Specified := True;
end;

function GetMBPerformanceAnalysisDriverResponseType.offset_Specified(Index: Integer): boolean;
begin
  Result := Foffset_Specified;
end;

procedure GetMBPerformanceAnalysisDriverResponseType.SetresultSize(Index: Integer; const AresultSizeType: resultSizeType);
begin
  FresultSize := AresultSizeType;
  FresultSize_Specified := True;
end;

function GetMBPerformanceAnalysisDriverResponseType.resultSize_Specified(Index: Integer): boolean;
begin
  Result := FresultSize_Specified;
end;

procedure GetMBPerformanceAnalysisDriverResponseType.SetresponseSize(Index: Integer; const AresponseSizeType: responseSizeType);
begin
  FresponseSize := AresponseSizeType;
  FresponseSize_Specified := True;
end;

function GetMBPerformanceAnalysisDriverResponseType.responseSize_Specified(Index: Integer): boolean;
begin
  Result := FresponseSize_Specified;
end;

procedure GetMBPerformanceAnalysisDriverResponseType.Setversion(Index: Integer; const AversionType: versionType);
begin
  Fversion := AversionType;
  Fversion_Specified := True;
end;

function GetMBPerformanceAnalysisDriverResponseType.version_Specified(Index: Integer): boolean;
begin
  Result := Fversion_Specified;
end;

procedure GetMBPerformanceAnalysisDriverResponseType.SetMBPerformanceAnalysisDriverReport(Index: Integer; const AArray_Of_MBPerformanceAnalysisDriverReport: Array_Of_MBPerformanceAnalysisDriverReport);
begin
  FMBPerformanceAnalysisDriverReport := AArray_Of_MBPerformanceAnalysisDriverReport;
  FMBPerformanceAnalysisDriverReport_Specified := True;
end;

function GetMBPerformanceAnalysisDriverResponseType.MBPerformanceAnalysisDriverReport_Specified(Index: Integer): boolean;
begin
  Result := FMBPerformanceAnalysisDriverReport_Specified;
end;

destructor GetUniversalPerformanceAnalysisDriverResponseType.Destroy;
var
  I: Integer;
begin
  for I := 0 to System.Length(FUniversalPerformanceAnalysisDriverReport)-1 do
    SysUtils.FreeAndNil(FUniversalPerformanceAnalysisDriverReport[I]);
  System.SetLength(FUniversalPerformanceAnalysisDriverReport, 0);
  inherited Destroy;
end;

procedure GetUniversalPerformanceAnalysisDriverResponseType.Setlimit(Index: Integer; const AlimitType: limitType);
begin
  Flimit := AlimitType;
  Flimit_Specified := True;
end;

function GetUniversalPerformanceAnalysisDriverResponseType.limit_Specified(Index: Integer): boolean;
begin
  Result := Flimit_Specified;
end;

procedure GetUniversalPerformanceAnalysisDriverResponseType.Setoffset(Index: Integer; const AoffsetType: offsetType);
begin
  Foffset := AoffsetType;
  Foffset_Specified := True;
end;

function GetUniversalPerformanceAnalysisDriverResponseType.offset_Specified(Index: Integer): boolean;
begin
  Result := Foffset_Specified;
end;

procedure GetUniversalPerformanceAnalysisDriverResponseType.SetresultSize(Index: Integer; const AresultSizeType: resultSizeType);
begin
  FresultSize := AresultSizeType;
  FresultSize_Specified := True;
end;

function GetUniversalPerformanceAnalysisDriverResponseType.resultSize_Specified(Index: Integer): boolean;
begin
  Result := FresultSize_Specified;
end;

procedure GetUniversalPerformanceAnalysisDriverResponseType.SetresponseSize(Index: Integer; const AresponseSizeType: responseSizeType);
begin
  FresponseSize := AresponseSizeType;
  FresponseSize_Specified := True;
end;

function GetUniversalPerformanceAnalysisDriverResponseType.responseSize_Specified(Index: Integer): boolean;
begin
  Result := FresponseSize_Specified;
end;

procedure GetUniversalPerformanceAnalysisDriverResponseType.SetUniversalPerformanceAnalysisDriverReport(Index: Integer; const AArray_Of_UniversalPerformanceAnalysisDriverReport: Array_Of_UniversalPerformanceAnalysisDriverReport);
begin
  FUniversalPerformanceAnalysisDriverReport := AArray_Of_UniversalPerformanceAnalysisDriverReport;
  FUniversalPerformanceAnalysisDriverReport_Specified := True;
end;

function GetUniversalPerformanceAnalysisDriverResponseType.UniversalPerformanceAnalysisDriverReport_Specified(Index: Integer): boolean;
begin
  Result := FUniversalPerformanceAnalysisDriverReport_Specified;
end;

procedure AvgValues7.SetWeight(Index: Integer; const AweightType: weightType);
begin
  FWeight := AweightType;
  FWeight_Specified := True;
end;

function AvgValues7.Weight_Specified(Index: Integer): boolean;
begin
  Result := FWeight_Specified;
end;

procedure AvgValues7.SetSpeed(Index: Integer; const AspeedType: speedType);
begin
  FSpeed := AspeedType;
  FSpeed_Specified := True;
end;

function AvgValues7.Speed_Specified(Index: Integer): boolean;
begin
  Result := FSpeed_Specified;
end;

procedure AvgValues7.SetOverallConsumption(Index: Integer; const AconsumptionType: consumptionType);
begin
  FOverallConsumption := AconsumptionType;
  FOverallConsumption_Specified := True;
end;

function AvgValues7.OverallConsumption_Specified(Index: Integer): boolean;
begin
  Result := FOverallConsumption_Specified;
end;

procedure AvgValues7.SetDriveConsumption(Index: Integer; const AconsumptionType: consumptionType);
begin
  FDriveConsumption := AconsumptionType;
  FDriveConsumption_Specified := True;
end;

function AvgValues7.DriveConsumption_Specified(Index: Integer): boolean;
begin
  Result := FDriveConsumption_Specified;
end;

procedure Grades9.SetDrivingStyle(Index: Integer; const AgradeType: gradeType);
begin
  FDrivingStyle := AgradeType;
  FDrivingStyle_Specified := True;
end;

function Grades9.DrivingStyle_Specified(Index: Integer): boolean;
begin
  Result := FDrivingStyle_Specified;
end;

procedure Grades9.SetDegreeOfDifficulty(Index: Integer; const AgradeType: gradeType);
begin
  FDegreeOfDifficulty := AgradeType;
  FDegreeOfDifficulty_Specified := True;
end;

function Grades9.DegreeOfDifficulty_Specified(Index: Integer): boolean;
begin
  Result := FDegreeOfDifficulty_Specified;
end;

destructor Period9.Destroy;
begin
  SysUtils.FreeAndNil(FPeriod);
  inherited Destroy;
end;

procedure Period9.SetBegin_(Index: Integer; const AtimestampType: timestampType);
begin
  FBegin_ := AtimestampType;
  FBegin__Specified := True;
end;

function Period9.Begin__Specified(Index: Integer): boolean;
begin
  Result := FBegin__Specified;
end;

procedure Period9.SetEnd_(Index: Integer; const AtimestampType: timestampType);
begin
  FEnd_ := AtimestampType;
  FEnd__Specified := True;
end;

function Period9.End__Specified(Index: Integer): boolean;
begin
  Result := FEnd__Specified;
end;

procedure Period9.SetPeriod(Index: Integer; const APeriod3Type: Period3Type);
begin
  FPeriod := APeriod3Type;
  FPeriod_Specified := True;
end;

function Period9.Period_Specified(Index: Integer): boolean;
begin
  Result := FPeriod_Specified;
end;

destructor GetMBPerformanceAnalysisVehicleOverviewResponseType.Destroy;
var
  I: Integer;
begin
  for I := 0 to System.Length(FMBPerformanceAnalysisVehicleOverviewReport)-1 do
    SysUtils.FreeAndNil(FMBPerformanceAnalysisVehicleOverviewReport[I]);
  System.SetLength(FMBPerformanceAnalysisVehicleOverviewReport, 0);
  inherited Destroy;
end;

procedure GetMBPerformanceAnalysisVehicleOverviewResponseType.Setlimit(Index: Integer; const AlimitType: limitType);
begin
  Flimit := AlimitType;
  Flimit_Specified := True;
end;

function GetMBPerformanceAnalysisVehicleOverviewResponseType.limit_Specified(Index: Integer): boolean;
begin
  Result := Flimit_Specified;
end;

procedure GetMBPerformanceAnalysisVehicleOverviewResponseType.Setoffset(Index: Integer; const AoffsetType: offsetType);
begin
  Foffset := AoffsetType;
  Foffset_Specified := True;
end;

function GetMBPerformanceAnalysisVehicleOverviewResponseType.offset_Specified(Index: Integer): boolean;
begin
  Result := Foffset_Specified;
end;

procedure GetMBPerformanceAnalysisVehicleOverviewResponseType.SetresultSize(Index: Integer; const AresultSizeType: resultSizeType);
begin
  FresultSize := AresultSizeType;
  FresultSize_Specified := True;
end;

function GetMBPerformanceAnalysisVehicleOverviewResponseType.resultSize_Specified(Index: Integer): boolean;
begin
  Result := FresultSize_Specified;
end;

procedure GetMBPerformanceAnalysisVehicleOverviewResponseType.SetresponseSize(Index: Integer; const AresponseSizeType: responseSizeType);
begin
  FresponseSize := AresponseSizeType;
  FresponseSize_Specified := True;
end;

function GetMBPerformanceAnalysisVehicleOverviewResponseType.responseSize_Specified(Index: Integer): boolean;
begin
  Result := FresponseSize_Specified;
end;

procedure GetMBPerformanceAnalysisVehicleOverviewResponseType.Setversion(Index: Integer; const AversionType: versionType);
begin
  Fversion := AversionType;
  Fversion_Specified := True;
end;

function GetMBPerformanceAnalysisVehicleOverviewResponseType.version_Specified(Index: Integer): boolean;
begin
  Result := Fversion_Specified;
end;

procedure GetMBPerformanceAnalysisVehicleOverviewResponseType.SetMBPerformanceAnalysisVehicleOverviewReport(Index: Integer; const AArray_Of_MBPerformanceAnalysisVehicleOverviewReport: Array_Of_MBPerformanceAnalysisVehicleOverviewReport);
begin
  FMBPerformanceAnalysisVehicleOverviewReport := AArray_Of_MBPerformanceAnalysisVehicleOverviewReport;
  FMBPerformanceAnalysisVehicleOverviewReport_Specified := True;
end;

function GetMBPerformanceAnalysisVehicleOverviewResponseType.MBPerformanceAnalysisVehicleOverviewReport_Specified(Index: Integer): boolean;
begin
  Result := FMBPerformanceAnalysisVehicleOverviewReport_Specified;
end;

destructor Analysis.Destroy;
begin
  SysUtils.FreeAndNil(FToursSummary);
  SysUtils.FreeAndNil(FPeriodBreakDown);
  SysUtils.FreeAndNil(FPredefinedPeriod);
  inherited Destroy;
end;

procedure Analysis.SetToursSummary(Index: Integer; const AToursSummary5: ToursSummary5);
begin
  FToursSummary := AToursSummary5;
  FToursSummary_Specified := True;
end;

function Analysis.ToursSummary_Specified(Index: Integer): boolean;
begin
  Result := FToursSummary_Specified;
end;

procedure Analysis.SetPeriodBreakDown(Index: Integer; const APeriodBreakDown6: PeriodBreakDown6);
begin
  FPeriodBreakDown := APeriodBreakDown6;
  FPeriodBreakDown_Specified := True;
end;

function Analysis.PeriodBreakDown_Specified(Index: Integer): boolean;
begin
  Result := FPeriodBreakDown_Specified;
end;

procedure Analysis.SetPredefinedPeriod(Index: Integer; const APredefinedPeriod6: PredefinedPeriod6);
begin
  FPredefinedPeriod := APredefinedPeriod6;
  FPredefinedPeriod_Specified := True;
end;

function Analysis.PredefinedPeriod_Specified(Index: Integer): boolean;
begin
  Result := FPredefinedPeriod_Specified;
end;

destructor Analysis2.Destroy;
begin
  SysUtils.FreeAndNil(FToursSummary);
  SysUtils.FreeAndNil(FPeriodBreakDown);
  SysUtils.FreeAndNil(FPredefinedPeriod);
  inherited Destroy;
end;

procedure Analysis2.SetToursSummary(Index: Integer; const AToursSummary4: ToursSummary4);
begin
  FToursSummary := AToursSummary4;
  FToursSummary_Specified := True;
end;

function Analysis2.ToursSummary_Specified(Index: Integer): boolean;
begin
  Result := FToursSummary_Specified;
end;

procedure Analysis2.SetPeriodBreakDown(Index: Integer; const APeriodBreakDown5: PeriodBreakDown5);
begin
  FPeriodBreakDown := APeriodBreakDown5;
  FPeriodBreakDown_Specified := True;
end;

function Analysis2.PeriodBreakDown_Specified(Index: Integer): boolean;
begin
  Result := FPeriodBreakDown_Specified;
end;

procedure Analysis2.SetPredefinedPeriod(Index: Integer; const APredefinedPeriod2: PredefinedPeriod2);
begin
  FPredefinedPeriod := APredefinedPeriod2;
  FPredefinedPeriod_Specified := True;
end;

function Analysis2.PredefinedPeriod_Specified(Index: Integer): boolean;
begin
  Result := FPredefinedPeriod_Specified;
end;

destructor Analysis3.Destroy;
begin
  SysUtils.FreeAndNil(FToursSummary);
  SysUtils.FreeAndNil(FPeriodBreakDown);
  SysUtils.FreeAndNil(FPredefinedPeriod);
  inherited Destroy;
end;

procedure Analysis3.SetToursSummary(Index: Integer; const AToursSummary3: ToursSummary3);
begin
  FToursSummary := AToursSummary3;
  FToursSummary_Specified := True;
end;

function Analysis3.ToursSummary_Specified(Index: Integer): boolean;
begin
  Result := FToursSummary_Specified;
end;

procedure Analysis3.SetPeriodBreakDown(Index: Integer; const APeriodBreakDown3: PeriodBreakDown3);
begin
  FPeriodBreakDown := APeriodBreakDown3;
  FPeriodBreakDown_Specified := True;
end;

function Analysis3.PeriodBreakDown_Specified(Index: Integer): boolean;
begin
  Result := FPeriodBreakDown_Specified;
end;

procedure Analysis3.SetPredefinedPeriod(Index: Integer; const APredefinedPeriod5: PredefinedPeriod5);
begin
  FPredefinedPeriod := APredefinedPeriod5;
  FPredefinedPeriod_Specified := True;
end;

function Analysis3.PredefinedPeriod_Specified(Index: Integer): boolean;
begin
  Result := FPredefinedPeriod_Specified;
end;

destructor Analysis4.Destroy;
begin
  SysUtils.FreeAndNil(FToursSummary);
  SysUtils.FreeAndNil(FPeriodBreakDown);
  SysUtils.FreeAndNil(FPredefinedPeriod);
  inherited Destroy;
end;

procedure Analysis4.SetToursSummary(Index: Integer; const AToursSummary2: ToursSummary2);
begin
  FToursSummary := AToursSummary2;
  FToursSummary_Specified := True;
end;

function Analysis4.ToursSummary_Specified(Index: Integer): boolean;
begin
  Result := FToursSummary_Specified;
end;

procedure Analysis4.SetPeriodBreakDown(Index: Integer; const APeriodBreakDown: PeriodBreakDown);
begin
  FPeriodBreakDown := APeriodBreakDown;
  FPeriodBreakDown_Specified := True;
end;

function Analysis4.PeriodBreakDown_Specified(Index: Integer): boolean;
begin
  Result := FPeriodBreakDown_Specified;
end;

procedure Analysis4.SetPredefinedPeriod(Index: Integer; const APredefinedPeriod: PredefinedPeriod);
begin
  FPredefinedPeriod := APredefinedPeriod;
  FPredefinedPeriod_Specified := True;
end;

function Analysis4.PredefinedPeriod_Specified(Index: Integer): boolean;
begin
  Result := FPredefinedPeriod_Specified;
end;

destructor Analysis5.Destroy;
begin
  SysUtils.FreeAndNil(FToursSummary);
  SysUtils.FreeAndNil(FPeriodBreakDown);
  SysUtils.FreeAndNil(FPredefinedPeriod);
  inherited Destroy;
end;

procedure Analysis5.SetToursSummary(Index: Integer; const AToursSummary: ToursSummary);
begin
  FToursSummary := AToursSummary;
  FToursSummary_Specified := True;
end;

function Analysis5.ToursSummary_Specified(Index: Integer): boolean;
begin
  Result := FToursSummary_Specified;
end;

procedure Analysis5.SetPeriodBreakDown(Index: Integer; const APeriodBreakDown2: PeriodBreakDown2);
begin
  FPeriodBreakDown := APeriodBreakDown2;
  FPeriodBreakDown_Specified := True;
end;

function Analysis5.PeriodBreakDown_Specified(Index: Integer): boolean;
begin
  Result := FPeriodBreakDown_Specified;
end;

procedure Analysis5.SetPredefinedPeriod(Index: Integer; const APredefinedPeriod3: PredefinedPeriod3);
begin
  FPredefinedPeriod := APredefinedPeriod3;
  FPredefinedPeriod_Specified := True;
end;

function Analysis5.PredefinedPeriod_Specified(Index: Integer): boolean;
begin
  Result := FPredefinedPeriod_Specified;
end;

destructor Analysis6.Destroy;
begin
  SysUtils.FreeAndNil(FToursSummary);
  SysUtils.FreeAndNil(FPeriodBreakDown);
  SysUtils.FreeAndNil(FPredefinedPeriod);
  inherited Destroy;
end;

procedure Analysis6.SetToursSummary(Index: Integer; const AToursSummary7: ToursSummary7);
begin
  FToursSummary := AToursSummary7;
  FToursSummary_Specified := True;
end;

function Analysis6.ToursSummary_Specified(Index: Integer): boolean;
begin
  Result := FToursSummary_Specified;
end;

procedure Analysis6.SetPeriodBreakDown(Index: Integer; const APeriodBreakDown7: PeriodBreakDown7);
begin
  FPeriodBreakDown := APeriodBreakDown7;
  FPeriodBreakDown_Specified := True;
end;

function Analysis6.PeriodBreakDown_Specified(Index: Integer): boolean;
begin
  Result := FPeriodBreakDown_Specified;
end;

procedure Analysis6.SetPredefinedPeriod(Index: Integer; const APredefinedPeriod7: PredefinedPeriod7);
begin
  FPredefinedPeriod := APredefinedPeriod7;
  FPredefinedPeriod_Specified := True;
end;

function Analysis6.PredefinedPeriod_Specified(Index: Integer): boolean;
begin
  Result := FPredefinedPeriod_Specified;
end;

destructor GetMBPerformanceAnalysisVehicleOverviewRequestType.Destroy;
begin
  SysUtils.FreeAndNil(FAnalysis);
  SysUtils.FreeAndNil(FDrivingStyle);
  SysUtils.FreeAndNil(FDegreeOfDifficulty);
  SysUtils.FreeAndNil(FAvgTruckWeight);
  SysUtils.FreeAndNil(FAvgSpeed);
  SysUtils.FreeAndNil(FAvgOverallConsumption);
  inherited Destroy;
end;

procedure GetMBPerformanceAnalysisVehicleOverviewRequestType.Setsessionid(Index: Integer; const AsessionidType: sessionidType);
begin
  Fsessionid := AsessionidType;
  Fsessionid_Specified := True;
end;

function GetMBPerformanceAnalysisVehicleOverviewRequestType.sessionid_Specified(Index: Integer): boolean;
begin
  Result := Fsessionid_Specified;
end;

procedure GetMBPerformanceAnalysisVehicleOverviewRequestType.Setlimit(Index: Integer; const AlimitType: limitType);
begin
  Flimit := AlimitType;
  Flimit_Specified := True;
end;

function GetMBPerformanceAnalysisVehicleOverviewRequestType.limit_Specified(Index: Integer): boolean;
begin
  Result := Flimit_Specified;
end;

procedure GetMBPerformanceAnalysisVehicleOverviewRequestType.Setoffset(Index: Integer; const AoffsetType: offsetType);
begin
  Foffset := AoffsetType;
  Foffset_Specified := True;
end;

function GetMBPerformanceAnalysisVehicleOverviewRequestType.offset_Specified(Index: Integer): boolean;
begin
  Result := Foffset_Specified;
end;

procedure GetMBPerformanceAnalysisVehicleOverviewRequestType.Setversion(Index: Integer; const AversionType: versionType);
begin
  Fversion := AversionType;
  Fversion_Specified := True;
end;

function GetMBPerformanceAnalysisVehicleOverviewRequestType.version_Specified(Index: Integer): boolean;
begin
  Result := Fversion_Specified;
end;

procedure GetMBPerformanceAnalysisVehicleOverviewRequestType.SetVehicleID(Index: Integer; const AvehicleidType: vehicleidType);
begin
  FVehicleID := AvehicleidType;
  FVehicleID_Specified := True;
end;

function GetMBPerformanceAnalysisVehicleOverviewRequestType.VehicleID_Specified(Index: Integer): boolean;
begin
  Result := FVehicleID_Specified;
end;

procedure GetMBPerformanceAnalysisVehicleOverviewRequestType.SetVehicleGroupID(Index: Integer; const AvehicleGroupIDType: vehicleGroupIDType);
begin
  FVehicleGroupID := AvehicleGroupIDType;
  FVehicleGroupID_Specified := True;
end;

function GetMBPerformanceAnalysisVehicleOverviewRequestType.VehicleGroupID_Specified(Index: Integer): boolean;
begin
  Result := FVehicleGroupID_Specified;
end;

procedure GetMBPerformanceAnalysisVehicleOverviewRequestType.SetTelematicGroupID(Index: Integer; const AtelematicGroupIDType: telematicGroupIDType);
begin
  FTelematicGroupID := AtelematicGroupIDType;
  FTelematicGroupID_Specified := True;
end;

function GetMBPerformanceAnalysisVehicleOverviewRequestType.TelematicGroupID_Specified(Index: Integer): boolean;
begin
  Result := FTelematicGroupID_Specified;
end;

procedure GetMBPerformanceAnalysisVehicleOverviewRequestType.SetDrivingStyle(Index: Integer; const AMinMaxGradeType: MinMaxGradeType);
begin
  FDrivingStyle := AMinMaxGradeType;
  FDrivingStyle_Specified := True;
end;

function GetMBPerformanceAnalysisVehicleOverviewRequestType.DrivingStyle_Specified(Index: Integer): boolean;
begin
  Result := FDrivingStyle_Specified;
end;

procedure GetMBPerformanceAnalysisVehicleOverviewRequestType.SetDegreeOfDifficulty(Index: Integer; const AMinMaxGradeType: MinMaxGradeType);
begin
  FDegreeOfDifficulty := AMinMaxGradeType;
  FDegreeOfDifficulty_Specified := True;
end;

function GetMBPerformanceAnalysisVehicleOverviewRequestType.DegreeOfDifficulty_Specified(Index: Integer): boolean;
begin
  Result := FDegreeOfDifficulty_Specified;
end;

procedure GetMBPerformanceAnalysisVehicleOverviewRequestType.SetAvgTruckWeight(Index: Integer; const AMinMaxWeightType: MinMaxWeightType);
begin
  FAvgTruckWeight := AMinMaxWeightType;
  FAvgTruckWeight_Specified := True;
end;

function GetMBPerformanceAnalysisVehicleOverviewRequestType.AvgTruckWeight_Specified(Index: Integer): boolean;
begin
  Result := FAvgTruckWeight_Specified;
end;

procedure GetMBPerformanceAnalysisVehicleOverviewRequestType.SetAvgSpeed(Index: Integer; const AMinMaxSpeedType: MinMaxSpeedType);
begin
  FAvgSpeed := AMinMaxSpeedType;
  FAvgSpeed_Specified := True;
end;

function GetMBPerformanceAnalysisVehicleOverviewRequestType.AvgSpeed_Specified(Index: Integer): boolean;
begin
  Result := FAvgSpeed_Specified;
end;

procedure GetMBPerformanceAnalysisVehicleOverviewRequestType.SetAvgOverallConsumption(Index: Integer; const AMinMaxConsumptionType: MinMaxConsumptionType);
begin
  FAvgOverallConsumption := AMinMaxConsumptionType;
  FAvgOverallConsumption_Specified := True;
end;

function GetMBPerformanceAnalysisVehicleOverviewRequestType.AvgOverallConsumption_Specified(Index: Integer): boolean;
begin
  Result := FAvgOverallConsumption_Specified;
end;

procedure PredefinedPeriod7.SetInterval(Index: Integer; const Aperiod1Type: period1Type);
begin
  FInterval := Aperiod1Type;
  FInterval_Specified := True;
end;

function PredefinedPeriod7.Interval_Specified(Index: Integer): boolean;
begin
  Result := FInterval_Specified;
end;

procedure PredefinedPeriod7.SetCalendarWeek(Index: Integer; const AcalendarWeekType: calendarWeekType);
begin
  FCalendarWeek := AcalendarWeekType;
  FCalendarWeek_Specified := True;
end;

function PredefinedPeriod7.CalendarWeek_Specified(Index: Integer): boolean;
begin
  Result := FCalendarWeek_Specified;
end;

destructor ToursSummary7.Destroy;
begin
  SysUtils.FreeAndNil(FTimeRange);
  inherited Destroy;
end;

procedure ToursSummary7.SetTimeRange(Index: Integer; const ATimeRangeType: TimeRangeType);
begin
  FTimeRange := ATimeRangeType;
  FTimeRange_Specified := True;
end;

function ToursSummary7.TimeRange_Specified(Index: Integer): boolean;
begin
  Result := FTimeRange_Specified;
end;

procedure AvgValues8.SetWeight(Index: Integer; const AweightType: weightType);
begin
  FWeight := AweightType;
  FWeight_Specified := True;
end;

function AvgValues8.Weight_Specified(Index: Integer): boolean;
begin
  Result := FWeight_Specified;
end;

procedure AvgValues8.SetSpeed(Index: Integer; const AspeedType: speedType);
begin
  FSpeed := AspeedType;
  FSpeed_Specified := True;
end;

function AvgValues8.Speed_Specified(Index: Integer): boolean;
begin
  Result := FSpeed_Specified;
end;

procedure AvgValues8.SetOverallConsumption(Index: Integer; const AconsumptionType: consumptionType);
begin
  FOverallConsumption := AconsumptionType;
  FOverallConsumption_Specified := True;
end;

function AvgValues8.OverallConsumption_Specified(Index: Integer): boolean;
begin
  Result := FOverallConsumption_Specified;
end;

procedure AvgValues8.SetDriveConsumption(Index: Integer; const AconsumptionType: consumptionType);
begin
  FDriveConsumption := AconsumptionType;
  FDriveConsumption_Specified := True;
end;

function AvgValues8.DriveConsumption_Specified(Index: Integer): boolean;
begin
  Result := FDriveConsumption_Specified;
end;

procedure Grades10.SetDrivingStyle(Index: Integer; const AgradeType: gradeType);
begin
  FDrivingStyle := AgradeType;
  FDrivingStyle_Specified := True;
end;

function Grades10.DrivingStyle_Specified(Index: Integer): boolean;
begin
  Result := FDrivingStyle_Specified;
end;

procedure Grades10.SetDegreeOfDifficulty(Index: Integer; const AgradeType: gradeType);
begin
  FDegreeOfDifficulty := AgradeType;
  FDegreeOfDifficulty_Specified := True;
end;

function Grades10.DegreeOfDifficulty_Specified(Index: Integer): boolean;
begin
  Result := FDegreeOfDifficulty_Specified;
end;

destructor Period10.Destroy;
begin
  SysUtils.FreeAndNil(FPeriod);
  inherited Destroy;
end;

procedure Period10.SetBegin_(Index: Integer; const AtimestampType: timestampType);
begin
  FBegin_ := AtimestampType;
  FBegin__Specified := True;
end;

function Period10.Begin__Specified(Index: Integer): boolean;
begin
  Result := FBegin__Specified;
end;

procedure Period10.SetEnd_(Index: Integer; const AtimestampType: timestampType);
begin
  FEnd_ := AtimestampType;
  FEnd__Specified := True;
end;

function Period10.End__Specified(Index: Integer): boolean;
begin
  Result := FEnd__Specified;
end;

procedure Period10.SetPeriod(Index: Integer; const APeriod3Type: Period3Type);
begin
  FPeriod := APeriod3Type;
  FPeriod_Specified := True;
end;

function Period10.Period_Specified(Index: Integer): boolean;
begin
  Result := FPeriod_Specified;
end;

destructor Analysis7.Destroy;
begin
  SysUtils.FreeAndNil(FToursSummary);
  SysUtils.FreeAndNil(FPeriodBreakDown);
  SysUtils.FreeAndNil(FPredefinedPeriod);
  inherited Destroy;
end;

procedure Analysis7.SetToursSummary(Index: Integer; const AToursSummary6: ToursSummary6);
begin
  FToursSummary := AToursSummary6;
  FToursSummary_Specified := True;
end;

function Analysis7.ToursSummary_Specified(Index: Integer): boolean;
begin
  Result := FToursSummary_Specified;
end;

procedure Analysis7.SetPeriodBreakDown(Index: Integer; const APeriodBreakDown4: PeriodBreakDown4);
begin
  FPeriodBreakDown := APeriodBreakDown4;
  FPeriodBreakDown_Specified := True;
end;

function Analysis7.PeriodBreakDown_Specified(Index: Integer): boolean;
begin
  Result := FPeriodBreakDown_Specified;
end;

procedure Analysis7.SetPredefinedPeriod(Index: Integer; const APredefinedPeriod4: PredefinedPeriod4);
begin
  FPredefinedPeriod := APredefinedPeriod4;
  FPredefinedPeriod_Specified := True;
end;

function Analysis7.PredefinedPeriod_Specified(Index: Integer): boolean;
begin
  Result := FPredefinedPeriod_Specified;
end;

destructor GetUniversalPerformanceAnalysisVehicleRequestType.Destroy;
begin
  SysUtils.FreeAndNil(FAnalysis);
  SysUtils.FreeAndNil(FDrivingStyle);
  SysUtils.FreeAndNil(FIdleTimeGrading);
  SysUtils.FreeAndNil(FOverrevNumberGrading);
  SysUtils.FreeAndNil(FOverrevTimeGrading);
  SysUtils.FreeAndNil(FEcoDrivingGrading);
  SysUtils.FreeAndNil(FHarshBrakeGrading);
  SysUtils.FreeAndNil(FOverspeedTimeGrading);
  SysUtils.FreeAndNil(FAvgTotalConsumption);
  inherited Destroy;
end;

procedure GetUniversalPerformanceAnalysisVehicleRequestType.Setsessionid(Index: Integer; const AsessionidType: sessionidType);
begin
  Fsessionid := AsessionidType;
  Fsessionid_Specified := True;
end;

function GetUniversalPerformanceAnalysisVehicleRequestType.sessionid_Specified(Index: Integer): boolean;
begin
  Result := Fsessionid_Specified;
end;

procedure GetUniversalPerformanceAnalysisVehicleRequestType.Setlimit(Index: Integer; const AlimitType: limitType);
begin
  Flimit := AlimitType;
  Flimit_Specified := True;
end;

function GetUniversalPerformanceAnalysisVehicleRequestType.limit_Specified(Index: Integer): boolean;
begin
  Result := Flimit_Specified;
end;

procedure GetUniversalPerformanceAnalysisVehicleRequestType.Setoffset(Index: Integer; const AoffsetType: offsetType);
begin
  Foffset := AoffsetType;
  Foffset_Specified := True;
end;

function GetUniversalPerformanceAnalysisVehicleRequestType.offset_Specified(Index: Integer): boolean;
begin
  Result := Foffset_Specified;
end;

procedure GetUniversalPerformanceAnalysisVehicleRequestType.Setversion(Index: Integer; const AversionType: versionType);
begin
  Fversion := AversionType;
  Fversion_Specified := True;
end;

function GetUniversalPerformanceAnalysisVehicleRequestType.version_Specified(Index: Integer): boolean;
begin
  Result := Fversion_Specified;
end;

procedure GetUniversalPerformanceAnalysisVehicleRequestType.SetGetCharts(Index: Integer; const AgetChartsType: getChartsType);
begin
  FGetCharts := AgetChartsType;
  FGetCharts_Specified := True;
end;

function GetUniversalPerformanceAnalysisVehicleRequestType.GetCharts_Specified(Index: Integer): boolean;
begin
  Result := FGetCharts_Specified;
end;

procedure GetUniversalPerformanceAnalysisVehicleRequestType.SetVehicleID(Index: Integer; const AvehicleidType: vehicleidType);
begin
  FVehicleID := AvehicleidType;
  FVehicleID_Specified := True;
end;

function GetUniversalPerformanceAnalysisVehicleRequestType.VehicleID_Specified(Index: Integer): boolean;
begin
  Result := FVehicleID_Specified;
end;

procedure GetUniversalPerformanceAnalysisVehicleRequestType.SetVehicleGroupID(Index: Integer; const AvehicleGroupIDType: vehicleGroupIDType);
begin
  FVehicleGroupID := AvehicleGroupIDType;
  FVehicleGroupID_Specified := True;
end;

function GetUniversalPerformanceAnalysisVehicleRequestType.VehicleGroupID_Specified(Index: Integer): boolean;
begin
  Result := FVehicleGroupID_Specified;
end;

procedure GetUniversalPerformanceAnalysisVehicleRequestType.SetDrivingStyle(Index: Integer; const AMinMaxGradeType: MinMaxGradeType);
begin
  FDrivingStyle := AMinMaxGradeType;
  FDrivingStyle_Specified := True;
end;

function GetUniversalPerformanceAnalysisVehicleRequestType.DrivingStyle_Specified(Index: Integer): boolean;
begin
  Result := FDrivingStyle_Specified;
end;

procedure GetUniversalPerformanceAnalysisVehicleRequestType.SetIdleTimeGrading(Index: Integer; const AMinMaxGradeType: MinMaxGradeType);
begin
  FIdleTimeGrading := AMinMaxGradeType;
  FIdleTimeGrading_Specified := True;
end;

function GetUniversalPerformanceAnalysisVehicleRequestType.IdleTimeGrading_Specified(Index: Integer): boolean;
begin
  Result := FIdleTimeGrading_Specified;
end;

procedure GetUniversalPerformanceAnalysisVehicleRequestType.SetOverrevNumberGrading(Index: Integer; const AMinMaxGradeType: MinMaxGradeType);
begin
  FOverrevNumberGrading := AMinMaxGradeType;
  FOverrevNumberGrading_Specified := True;
end;

function GetUniversalPerformanceAnalysisVehicleRequestType.OverrevNumberGrading_Specified(Index: Integer): boolean;
begin
  Result := FOverrevNumberGrading_Specified;
end;

procedure GetUniversalPerformanceAnalysisVehicleRequestType.SetOverrevTimeGrading(Index: Integer; const AMinMaxGradeType: MinMaxGradeType);
begin
  FOverrevTimeGrading := AMinMaxGradeType;
  FOverrevTimeGrading_Specified := True;
end;

function GetUniversalPerformanceAnalysisVehicleRequestType.OverrevTimeGrading_Specified(Index: Integer): boolean;
begin
  Result := FOverrevTimeGrading_Specified;
end;

procedure GetUniversalPerformanceAnalysisVehicleRequestType.SetEcoDrivingGrading(Index: Integer; const AMinMaxGradeType: MinMaxGradeType);
begin
  FEcoDrivingGrading := AMinMaxGradeType;
  FEcoDrivingGrading_Specified := True;
end;

function GetUniversalPerformanceAnalysisVehicleRequestType.EcoDrivingGrading_Specified(Index: Integer): boolean;
begin
  Result := FEcoDrivingGrading_Specified;
end;

procedure GetUniversalPerformanceAnalysisVehicleRequestType.SetHarshBrakeGrading(Index: Integer; const AMinMaxGradeType: MinMaxGradeType);
begin
  FHarshBrakeGrading := AMinMaxGradeType;
  FHarshBrakeGrading_Specified := True;
end;

function GetUniversalPerformanceAnalysisVehicleRequestType.HarshBrakeGrading_Specified(Index: Integer): boolean;
begin
  Result := FHarshBrakeGrading_Specified;
end;

procedure GetUniversalPerformanceAnalysisVehicleRequestType.SetOverspeedTimeGrading(Index: Integer; const AMinMaxGradeType: MinMaxGradeType);
begin
  FOverspeedTimeGrading := AMinMaxGradeType;
  FOverspeedTimeGrading_Specified := True;
end;

function GetUniversalPerformanceAnalysisVehicleRequestType.OverspeedTimeGrading_Specified(Index: Integer): boolean;
begin
  Result := FOverspeedTimeGrading_Specified;
end;

procedure GetUniversalPerformanceAnalysisVehicleRequestType.SetAvgTotalConsumption(Index: Integer; const AMinMaxConsumptionType: MinMaxConsumptionType);
begin
  FAvgTotalConsumption := AMinMaxConsumptionType;
  FAvgTotalConsumption_Specified := True;
end;

function GetUniversalPerformanceAnalysisVehicleRequestType.AvgTotalConsumption_Specified(Index: Integer): boolean;
begin
  Result := FAvgTotalConsumption_Specified;
end;

destructor GetMBPerformanceAnalysisVehicleRequestType.Destroy;
begin
  SysUtils.FreeAndNil(FAnalysis);
  SysUtils.FreeAndNil(FDrivingStyle);
  SysUtils.FreeAndNil(FDegreeOfDifficulty);
  SysUtils.FreeAndNil(FAvgTruckWeight);
  SysUtils.FreeAndNil(FAvgSpeed);
  SysUtils.FreeAndNil(FAvgOverallConsumption);
  inherited Destroy;
end;

procedure GetMBPerformanceAnalysisVehicleRequestType.Setsessionid(Index: Integer; const AsessionidType: sessionidType);
begin
  Fsessionid := AsessionidType;
  Fsessionid_Specified := True;
end;

function GetMBPerformanceAnalysisVehicleRequestType.sessionid_Specified(Index: Integer): boolean;
begin
  Result := Fsessionid_Specified;
end;

procedure GetMBPerformanceAnalysisVehicleRequestType.Setlimit(Index: Integer; const AlimitType: limitType);
begin
  Flimit := AlimitType;
  Flimit_Specified := True;
end;

function GetMBPerformanceAnalysisVehicleRequestType.limit_Specified(Index: Integer): boolean;
begin
  Result := Flimit_Specified;
end;

procedure GetMBPerformanceAnalysisVehicleRequestType.Setoffset(Index: Integer; const AoffsetType: offsetType);
begin
  Foffset := AoffsetType;
  Foffset_Specified := True;
end;

function GetMBPerformanceAnalysisVehicleRequestType.offset_Specified(Index: Integer): boolean;
begin
  Result := Foffset_Specified;
end;

procedure GetMBPerformanceAnalysisVehicleRequestType.Setversion(Index: Integer; const AversionType: versionType);
begin
  Fversion := AversionType;
  Fversion_Specified := True;
end;

function GetMBPerformanceAnalysisVehicleRequestType.version_Specified(Index: Integer): boolean;
begin
  Result := Fversion_Specified;
end;

procedure GetMBPerformanceAnalysisVehicleRequestType.SetGetCharts(Index: Integer; const AgetChartsType: getChartsType);
begin
  FGetCharts := AgetChartsType;
  FGetCharts_Specified := True;
end;

function GetMBPerformanceAnalysisVehicleRequestType.GetCharts_Specified(Index: Integer): boolean;
begin
  Result := FGetCharts_Specified;
end;

procedure GetMBPerformanceAnalysisVehicleRequestType.SetVehicleID(Index: Integer; const AvehicleidType: vehicleidType);
begin
  FVehicleID := AvehicleidType;
  FVehicleID_Specified := True;
end;

function GetMBPerformanceAnalysisVehicleRequestType.VehicleID_Specified(Index: Integer): boolean;
begin
  Result := FVehicleID_Specified;
end;

procedure GetMBPerformanceAnalysisVehicleRequestType.SetVehicleGroupID(Index: Integer; const AvehicleGroupIDType: vehicleGroupIDType);
begin
  FVehicleGroupID := AvehicleGroupIDType;
  FVehicleGroupID_Specified := True;
end;

function GetMBPerformanceAnalysisVehicleRequestType.VehicleGroupID_Specified(Index: Integer): boolean;
begin
  Result := FVehicleGroupID_Specified;
end;

procedure GetMBPerformanceAnalysisVehicleRequestType.SetTelematicGroupID(Index: Integer; const AtelematicGroupIDType: telematicGroupIDType);
begin
  FTelematicGroupID := AtelematicGroupIDType;
  FTelematicGroupID_Specified := True;
end;

function GetMBPerformanceAnalysisVehicleRequestType.TelematicGroupID_Specified(Index: Integer): boolean;
begin
  Result := FTelematicGroupID_Specified;
end;

procedure GetMBPerformanceAnalysisVehicleRequestType.SetDrivingStyle(Index: Integer; const AMinMaxGradeType: MinMaxGradeType);
begin
  FDrivingStyle := AMinMaxGradeType;
  FDrivingStyle_Specified := True;
end;

function GetMBPerformanceAnalysisVehicleRequestType.DrivingStyle_Specified(Index: Integer): boolean;
begin
  Result := FDrivingStyle_Specified;
end;

procedure GetMBPerformanceAnalysisVehicleRequestType.SetDegreeOfDifficulty(Index: Integer; const AMinMaxGradeType: MinMaxGradeType);
begin
  FDegreeOfDifficulty := AMinMaxGradeType;
  FDegreeOfDifficulty_Specified := True;
end;

function GetMBPerformanceAnalysisVehicleRequestType.DegreeOfDifficulty_Specified(Index: Integer): boolean;
begin
  Result := FDegreeOfDifficulty_Specified;
end;

procedure GetMBPerformanceAnalysisVehicleRequestType.SetAvgTruckWeight(Index: Integer; const AMinMaxWeightType: MinMaxWeightType);
begin
  FAvgTruckWeight := AMinMaxWeightType;
  FAvgTruckWeight_Specified := True;
end;

function GetMBPerformanceAnalysisVehicleRequestType.AvgTruckWeight_Specified(Index: Integer): boolean;
begin
  Result := FAvgTruckWeight_Specified;
end;

procedure GetMBPerformanceAnalysisVehicleRequestType.SetAvgSpeed(Index: Integer; const AMinMaxSpeedType: MinMaxSpeedType);
begin
  FAvgSpeed := AMinMaxSpeedType;
  FAvgSpeed_Specified := True;
end;

function GetMBPerformanceAnalysisVehicleRequestType.AvgSpeed_Specified(Index: Integer): boolean;
begin
  Result := FAvgSpeed_Specified;
end;

procedure GetMBPerformanceAnalysisVehicleRequestType.SetAvgOverallConsumption(Index: Integer; const AMinMaxConsumptionType: MinMaxConsumptionType);
begin
  FAvgOverallConsumption := AMinMaxConsumptionType;
  FAvgOverallConsumption_Specified := True;
end;

function GetMBPerformanceAnalysisVehicleRequestType.AvgOverallConsumption_Specified(Index: Integer): boolean;
begin
  Result := FAvgOverallConsumption_Specified;
end;

destructor ToursSummary8.Destroy;
begin
  SysUtils.FreeAndNil(FTimeRange);
  inherited Destroy;
end;

destructor Analysis8.Destroy;
begin
  SysUtils.FreeAndNil(FToursSummary);
  SysUtils.FreeAndNil(FPeriodBreakDown);
  SysUtils.FreeAndNil(FPredefinedPeriod);
  inherited Destroy;
end;

procedure Analysis8.SetToursSummary(Index: Integer; const AToursSummary8: ToursSummary8);
begin
  FToursSummary := AToursSummary8;
  FToursSummary_Specified := True;
end;

function Analysis8.ToursSummary_Specified(Index: Integer): boolean;
begin
  Result := FToursSummary_Specified;
end;

procedure Analysis8.SetPeriodBreakDown(Index: Integer; const APeriodBreakDown8: PeriodBreakDown8);
begin
  FPeriodBreakDown := APeriodBreakDown8;
  FPeriodBreakDown_Specified := True;
end;

function Analysis8.PeriodBreakDown_Specified(Index: Integer): boolean;
begin
  Result := FPeriodBreakDown_Specified;
end;

procedure Analysis8.SetPredefinedPeriod(Index: Integer; const APredefinedPeriod8: PredefinedPeriod8);
begin
  FPredefinedPeriod := APredefinedPeriod8;
  FPredefinedPeriod_Specified := True;
end;

function Analysis8.PredefinedPeriod_Specified(Index: Integer): boolean;
begin
  Result := FPredefinedPeriod_Specified;
end;

destructor MBPerformanceAnalysisDriverOverviewReport.Destroy;
begin
  SysUtils.FreeAndNil(FPeriod);
  SysUtils.FreeAndNil(FGrades);
  SysUtils.FreeAndNil(FAvgValues);
  SysUtils.FreeAndNil(FConsumption);
  SysUtils.FreeAndNil(FVehicleStateTimes);
  inherited Destroy;
end;

procedure MBPerformanceAnalysisDriverOverviewReport.SetConsumption(Index: Integer; const AConsumptionValuesType: ConsumptionValuesType);
begin
  FConsumption := AConsumptionValuesType;
  FConsumption_Specified := True;
end;

function MBPerformanceAnalysisDriverOverviewReport.Consumption_Specified(Index: Integer): boolean;
begin
  Result := FConsumption_Specified;
end;

procedure MBPerformanceAnalysisDriverOverviewReport.SetTotalDistance(Index: Integer; const AdistanceType: distanceType);
begin
  FTotalDistance := AdistanceType;
  FTotalDistance_Specified := True;
end;

function MBPerformanceAnalysisDriverOverviewReport.TotalDistance_Specified(Index: Integer): boolean;
begin
  Result := FTotalDistance_Specified;
end;

procedure MBPerformanceAnalysisDriverOverviewReport.SetVehicleStateTimes(Index: Integer; const AVehicleStateTimeValuesType: VehicleStateTimeValuesType);
begin
  FVehicleStateTimes := AVehicleStateTimeValuesType;
  FVehicleStateTimes_Specified := True;
end;

function MBPerformanceAnalysisDriverOverviewReport.VehicleStateTimes_Specified(Index: Integer): boolean;
begin
  Result := FVehicleStateTimes_Specified;
end;

procedure MBPerformanceAnalysisDriverOverviewReport.SetNumberOfDrivers(Index: Integer; const AInteger: Integer);
begin
  FNumberOfDrivers := AInteger;
  FNumberOfDrivers_Specified := True;
end;

function MBPerformanceAnalysisDriverOverviewReport.NumberOfDrivers_Specified(Index: Integer): boolean;
begin
  Result := FNumberOfDrivers_Specified;
end;

destructor GetMBPerformanceAnalysisDriverOverviewRequestType.Destroy;
begin
  SysUtils.FreeAndNil(FAnalysis);
  SysUtils.FreeAndNil(FDrivingStyle);
  SysUtils.FreeAndNil(FDegreeOfDifficulty);
  SysUtils.FreeAndNil(FAvgTruckWeight);
  SysUtils.FreeAndNil(FAvgSpeed);
  SysUtils.FreeAndNil(FAvgOverallConsumption);
  inherited Destroy;
end;

procedure GetMBPerformanceAnalysisDriverOverviewRequestType.Setsessionid(Index: Integer; const AsessionidType: sessionidType);
begin
  Fsessionid := AsessionidType;
  Fsessionid_Specified := True;
end;

function GetMBPerformanceAnalysisDriverOverviewRequestType.sessionid_Specified(Index: Integer): boolean;
begin
  Result := Fsessionid_Specified;
end;

procedure GetMBPerformanceAnalysisDriverOverviewRequestType.Setlimit(Index: Integer; const AlimitType: limitType);
begin
  Flimit := AlimitType;
  Flimit_Specified := True;
end;

function GetMBPerformanceAnalysisDriverOverviewRequestType.limit_Specified(Index: Integer): boolean;
begin
  Result := Flimit_Specified;
end;

procedure GetMBPerformanceAnalysisDriverOverviewRequestType.Setoffset(Index: Integer; const AoffsetType: offsetType);
begin
  Foffset := AoffsetType;
  Foffset_Specified := True;
end;

function GetMBPerformanceAnalysisDriverOverviewRequestType.offset_Specified(Index: Integer): boolean;
begin
  Result := Foffset_Specified;
end;

procedure GetMBPerformanceAnalysisDriverOverviewRequestType.Setversion(Index: Integer; const AversionType: versionType);
begin
  Fversion := AversionType;
  Fversion_Specified := True;
end;

function GetMBPerformanceAnalysisDriverOverviewRequestType.version_Specified(Index: Integer): boolean;
begin
  Result := Fversion_Specified;
end;

procedure GetMBPerformanceAnalysisDriverOverviewRequestType.SetDriverNameID(Index: Integer; const AdriverNameIDType: driverNameIDType);
begin
  FDriverNameID := AdriverNameIDType;
  FDriverNameID_Specified := True;
end;

function GetMBPerformanceAnalysisDriverOverviewRequestType.DriverNameID_Specified(Index: Integer): boolean;
begin
  Result := FDriverNameID_Specified;
end;

procedure GetMBPerformanceAnalysisDriverOverviewRequestType.SetDriverGroupID(Index: Integer; const AdriverGroupIDType: driverGroupIDType);
begin
  FDriverGroupID := AdriverGroupIDType;
  FDriverGroupID_Specified := True;
end;

function GetMBPerformanceAnalysisDriverOverviewRequestType.DriverGroupID_Specified(Index: Integer): boolean;
begin
  Result := FDriverGroupID_Specified;
end;

procedure GetMBPerformanceAnalysisDriverOverviewRequestType.SetDrivingStyle(Index: Integer; const AMinMaxGradeType: MinMaxGradeType);
begin
  FDrivingStyle := AMinMaxGradeType;
  FDrivingStyle_Specified := True;
end;

function GetMBPerformanceAnalysisDriverOverviewRequestType.DrivingStyle_Specified(Index: Integer): boolean;
begin
  Result := FDrivingStyle_Specified;
end;

procedure GetMBPerformanceAnalysisDriverOverviewRequestType.SetDegreeOfDifficulty(Index: Integer; const AMinMaxGradeType: MinMaxGradeType);
begin
  FDegreeOfDifficulty := AMinMaxGradeType;
  FDegreeOfDifficulty_Specified := True;
end;

function GetMBPerformanceAnalysisDriverOverviewRequestType.DegreeOfDifficulty_Specified(Index: Integer): boolean;
begin
  Result := FDegreeOfDifficulty_Specified;
end;

procedure GetMBPerformanceAnalysisDriverOverviewRequestType.SetAvgTruckWeight(Index: Integer; const AMinMaxWeightType: MinMaxWeightType);
begin
  FAvgTruckWeight := AMinMaxWeightType;
  FAvgTruckWeight_Specified := True;
end;

function GetMBPerformanceAnalysisDriverOverviewRequestType.AvgTruckWeight_Specified(Index: Integer): boolean;
begin
  Result := FAvgTruckWeight_Specified;
end;

procedure GetMBPerformanceAnalysisDriverOverviewRequestType.SetAvgSpeed(Index: Integer; const AMinMaxSpeedType: MinMaxSpeedType);
begin
  FAvgSpeed := AMinMaxSpeedType;
  FAvgSpeed_Specified := True;
end;

function GetMBPerformanceAnalysisDriverOverviewRequestType.AvgSpeed_Specified(Index: Integer): boolean;
begin
  Result := FAvgSpeed_Specified;
end;

procedure GetMBPerformanceAnalysisDriverOverviewRequestType.SetAvgOverallConsumption(Index: Integer; const AMinMaxConsumptionType: MinMaxConsumptionType);
begin
  FAvgOverallConsumption := AMinMaxConsumptionType;
  FAvgOverallConsumption_Specified := True;
end;

function GetMBPerformanceAnalysisDriverOverviewRequestType.AvgOverallConsumption_Specified(Index: Integer): boolean;
begin
  Result := FAvgOverallConsumption_Specified;
end;

destructor UniversalPerformanceAnalysisDriverOverviewReport.Destroy;
var
  I: Integer;
begin
  for I := 0 to System.Length(FDriverGroups)-1 do
    SysUtils.FreeAndNil(FDriverGroups[I]);
  System.SetLength(FDriverGroups, 0);
  SysUtils.FreeAndNil(FPeriod);
  SysUtils.FreeAndNil(FGrades);
  inherited Destroy;
end;

procedure UniversalPerformanceAnalysisDriverOverviewReport.SetDriverNameId(Index: Integer; const AdriverNameIDType: driverNameIDType);
begin
  FDriverNameId := AdriverNameIDType;
  FDriverNameId_Specified := True;
end;

function UniversalPerformanceAnalysisDriverOverviewReport.DriverNameId_Specified(Index: Integer): boolean;
begin
  Result := FDriverNameId_Specified;
end;

procedure UniversalPerformanceAnalysisDriverOverviewReport.SetDriverFirstName(Index: Integer; const Astring: string);
begin
  FDriverFirstName := Astring;
  FDriverFirstName_Specified := True;
end;

function UniversalPerformanceAnalysisDriverOverviewReport.DriverFirstName_Specified(Index: Integer): boolean;
begin
  Result := FDriverFirstName_Specified;
end;

procedure UniversalPerformanceAnalysisDriverOverviewReport.SetDriverLastName(Index: Integer; const Astring: string);
begin
  FDriverLastName := Astring;
  FDriverLastName_Specified := True;
end;

function UniversalPerformanceAnalysisDriverOverviewReport.DriverLastName_Specified(Index: Integer): boolean;
begin
  Result := FDriverLastName_Specified;
end;

procedure UniversalPerformanceAnalysisDriverOverviewReport.SetDriverGroups(Index: Integer; const AArray_Of_DriverGroup: Array_Of_DriverGroup);
begin
  FDriverGroups := AArray_Of_DriverGroup;
  FDriverGroups_Specified := True;
end;

function UniversalPerformanceAnalysisDriverOverviewReport.DriverGroups_Specified(Index: Integer): boolean;
begin
  Result := FDriverGroups_Specified;
end;

procedure UniversalPerformanceAnalysisDriverOverviewReport.SetGrades(Index: Integer; const AGrades2: Grades2);
begin
  FGrades := AGrades2;
  FGrades_Specified := True;
end;

function UniversalPerformanceAnalysisDriverOverviewReport.Grades_Specified(Index: Integer): boolean;
begin
  Result := FGrades_Specified;
end;

procedure UniversalPerformanceAnalysisDriverOverviewReport.SetAverageTotalConsumption(Index: Integer; const AconsumptionType: consumptionType);
begin
  FAverageTotalConsumption := AconsumptionType;
  FAverageTotalConsumption_Specified := True;
end;

function UniversalPerformanceAnalysisDriverOverviewReport.AverageTotalConsumption_Specified(Index: Integer): boolean;
begin
  Result := FAverageTotalConsumption_Specified;
end;

procedure UniversalPerformanceAnalysisDriverOverviewReport.SetTotalDistance(Index: Integer; const AdistanceType: distanceType);
begin
  FTotalDistance := AdistanceType;
  FTotalDistance_Specified := True;
end;

function UniversalPerformanceAnalysisDriverOverviewReport.TotalDistance_Specified(Index: Integer): boolean;
begin
  Result := FTotalDistance_Specified;
end;

procedure UniversalPerformanceAnalysisDriverOverviewReport.SetNumberOfDrivers(Index: Integer; const AInteger: Integer);
begin
  FNumberOfDrivers := AInteger;
  FNumberOfDrivers_Specified := True;
end;

function UniversalPerformanceAnalysisDriverOverviewReport.NumberOfDrivers_Specified(Index: Integer): boolean;
begin
  Result := FNumberOfDrivers_Specified;
end;

destructor GetMBPerformanceAnalysisDriverRequestType.Destroy;
begin
  SysUtils.FreeAndNil(FAnalysis);
  SysUtils.FreeAndNil(FDrivingStyle);
  SysUtils.FreeAndNil(FDegreeOfDifficulty);
  SysUtils.FreeAndNil(FAvgTruckWeight);
  SysUtils.FreeAndNil(FAvgSpeed);
  SysUtils.FreeAndNil(FAvgOverallConsumption);
  inherited Destroy;
end;

procedure GetMBPerformanceAnalysisDriverRequestType.Setsessionid(Index: Integer; const AsessionidType: sessionidType);
begin
  Fsessionid := AsessionidType;
  Fsessionid_Specified := True;
end;

function GetMBPerformanceAnalysisDriverRequestType.sessionid_Specified(Index: Integer): boolean;
begin
  Result := Fsessionid_Specified;
end;

procedure GetMBPerformanceAnalysisDriverRequestType.Setlimit(Index: Integer; const AlimitType: limitType);
begin
  Flimit := AlimitType;
  Flimit_Specified := True;
end;

function GetMBPerformanceAnalysisDriverRequestType.limit_Specified(Index: Integer): boolean;
begin
  Result := Flimit_Specified;
end;

procedure GetMBPerformanceAnalysisDriverRequestType.Setoffset(Index: Integer; const AoffsetType: offsetType);
begin
  Foffset := AoffsetType;
  Foffset_Specified := True;
end;

function GetMBPerformanceAnalysisDriverRequestType.offset_Specified(Index: Integer): boolean;
begin
  Result := Foffset_Specified;
end;

procedure GetMBPerformanceAnalysisDriverRequestType.Setversion(Index: Integer; const AversionType: versionType);
begin
  Fversion := AversionType;
  Fversion_Specified := True;
end;

function GetMBPerformanceAnalysisDriverRequestType.version_Specified(Index: Integer): boolean;
begin
  Result := Fversion_Specified;
end;

procedure GetMBPerformanceAnalysisDriverRequestType.SetGetCharts(Index: Integer; const AgetChartsType: getChartsType);
begin
  FGetCharts := AgetChartsType;
  FGetCharts_Specified := True;
end;

function GetMBPerformanceAnalysisDriverRequestType.GetCharts_Specified(Index: Integer): boolean;
begin
  Result := FGetCharts_Specified;
end;

procedure GetMBPerformanceAnalysisDriverRequestType.SetDriverNameID(Index: Integer; const AdriverNameIDType: driverNameIDType);
begin
  FDriverNameID := AdriverNameIDType;
  FDriverNameID_Specified := True;
end;

function GetMBPerformanceAnalysisDriverRequestType.DriverNameID_Specified(Index: Integer): boolean;
begin
  Result := FDriverNameID_Specified;
end;

procedure GetMBPerformanceAnalysisDriverRequestType.SetDriverGroupID(Index: Integer; const AdriverGroupIDType: driverGroupIDType);
begin
  FDriverGroupID := AdriverGroupIDType;
  FDriverGroupID_Specified := True;
end;

function GetMBPerformanceAnalysisDriverRequestType.DriverGroupID_Specified(Index: Integer): boolean;
begin
  Result := FDriverGroupID_Specified;
end;

procedure GetMBPerformanceAnalysisDriverRequestType.SetDrivingStyle(Index: Integer; const AMinMaxGradeType: MinMaxGradeType);
begin
  FDrivingStyle := AMinMaxGradeType;
  FDrivingStyle_Specified := True;
end;

function GetMBPerformanceAnalysisDriverRequestType.DrivingStyle_Specified(Index: Integer): boolean;
begin
  Result := FDrivingStyle_Specified;
end;

procedure GetMBPerformanceAnalysisDriverRequestType.SetDegreeOfDifficulty(Index: Integer; const AMinMaxGradeType: MinMaxGradeType);
begin
  FDegreeOfDifficulty := AMinMaxGradeType;
  FDegreeOfDifficulty_Specified := True;
end;

function GetMBPerformanceAnalysisDriverRequestType.DegreeOfDifficulty_Specified(Index: Integer): boolean;
begin
  Result := FDegreeOfDifficulty_Specified;
end;

procedure GetMBPerformanceAnalysisDriverRequestType.SetAvgTruckWeight(Index: Integer; const AMinMaxWeightType: MinMaxWeightType);
begin
  FAvgTruckWeight := AMinMaxWeightType;
  FAvgTruckWeight_Specified := True;
end;

function GetMBPerformanceAnalysisDriverRequestType.AvgTruckWeight_Specified(Index: Integer): boolean;
begin
  Result := FAvgTruckWeight_Specified;
end;

procedure GetMBPerformanceAnalysisDriverRequestType.SetAvgSpeed(Index: Integer; const AMinMaxSpeedType: MinMaxSpeedType);
begin
  FAvgSpeed := AMinMaxSpeedType;
  FAvgSpeed_Specified := True;
end;

function GetMBPerformanceAnalysisDriverRequestType.AvgSpeed_Specified(Index: Integer): boolean;
begin
  Result := FAvgSpeed_Specified;
end;

procedure GetMBPerformanceAnalysisDriverRequestType.SetAvgOverallConsumption(Index: Integer; const AMinMaxConsumptionType: MinMaxConsumptionType);
begin
  FAvgOverallConsumption := AMinMaxConsumptionType;
  FAvgOverallConsumption_Specified := True;
end;

function GetMBPerformanceAnalysisDriverRequestType.AvgOverallConsumption_Specified(Index: Integer): boolean;
begin
  Result := FAvgOverallConsumption_Specified;
end;

destructor GetUniversalPerformanceAnalysisDriverOverviewRequestType.Destroy;
begin
  SysUtils.FreeAndNil(FAnalysis);
  SysUtils.FreeAndNil(FDrivingStyle);
  SysUtils.FreeAndNil(FIdleTimeGrading);
  SysUtils.FreeAndNil(FOverrevNumberGrading);
  SysUtils.FreeAndNil(FOverrevTimeGrading);
  SysUtils.FreeAndNil(FEcoDrivingGrading);
  SysUtils.FreeAndNil(FHarshBrakeGrading);
  SysUtils.FreeAndNil(FOverspeedTimeGrading);
  SysUtils.FreeAndNil(FAvgTotalConsumption);
  inherited Destroy;
end;

procedure GetUniversalPerformanceAnalysisDriverOverviewRequestType.Setsessionid(Index: Integer; const AsessionidType: sessionidType);
begin
  Fsessionid := AsessionidType;
  Fsessionid_Specified := True;
end;

function GetUniversalPerformanceAnalysisDriverOverviewRequestType.sessionid_Specified(Index: Integer): boolean;
begin
  Result := Fsessionid_Specified;
end;

procedure GetUniversalPerformanceAnalysisDriverOverviewRequestType.Setlimit(Index: Integer; const AlimitType: limitType);
begin
  Flimit := AlimitType;
  Flimit_Specified := True;
end;

function GetUniversalPerformanceAnalysisDriverOverviewRequestType.limit_Specified(Index: Integer): boolean;
begin
  Result := Flimit_Specified;
end;

procedure GetUniversalPerformanceAnalysisDriverOverviewRequestType.Setoffset(Index: Integer; const AoffsetType: offsetType);
begin
  Foffset := AoffsetType;
  Foffset_Specified := True;
end;

function GetUniversalPerformanceAnalysisDriverOverviewRequestType.offset_Specified(Index: Integer): boolean;
begin
  Result := Foffset_Specified;
end;

procedure GetUniversalPerformanceAnalysisDriverOverviewRequestType.Setversion(Index: Integer; const AversionType: versionType);
begin
  Fversion := AversionType;
  Fversion_Specified := True;
end;

function GetUniversalPerformanceAnalysisDriverOverviewRequestType.version_Specified(Index: Integer): boolean;
begin
  Result := Fversion_Specified;
end;

procedure GetUniversalPerformanceAnalysisDriverOverviewRequestType.SetDriverNameId(Index: Integer; const AdriverNameIDType: driverNameIDType);
begin
  FDriverNameId := AdriverNameIDType;
  FDriverNameId_Specified := True;
end;

function GetUniversalPerformanceAnalysisDriverOverviewRequestType.DriverNameId_Specified(Index: Integer): boolean;
begin
  Result := FDriverNameId_Specified;
end;

procedure GetUniversalPerformanceAnalysisDriverOverviewRequestType.SetDriverGroupId(Index: Integer; const AdriverGroupIDType: driverGroupIDType);
begin
  FDriverGroupId := AdriverGroupIDType;
  FDriverGroupId_Specified := True;
end;

function GetUniversalPerformanceAnalysisDriverOverviewRequestType.DriverGroupId_Specified(Index: Integer): boolean;
begin
  Result := FDriverGroupId_Specified;
end;

procedure GetUniversalPerformanceAnalysisDriverOverviewRequestType.SetDrivingStyle(Index: Integer; const AMinMaxGradeType: MinMaxGradeType);
begin
  FDrivingStyle := AMinMaxGradeType;
  FDrivingStyle_Specified := True;
end;

function GetUniversalPerformanceAnalysisDriverOverviewRequestType.DrivingStyle_Specified(Index: Integer): boolean;
begin
  Result := FDrivingStyle_Specified;
end;

procedure GetUniversalPerformanceAnalysisDriverOverviewRequestType.SetIdleTimeGrading(Index: Integer; const AMinMaxGradeType: MinMaxGradeType);
begin
  FIdleTimeGrading := AMinMaxGradeType;
  FIdleTimeGrading_Specified := True;
end;

function GetUniversalPerformanceAnalysisDriverOverviewRequestType.IdleTimeGrading_Specified(Index: Integer): boolean;
begin
  Result := FIdleTimeGrading_Specified;
end;

procedure GetUniversalPerformanceAnalysisDriverOverviewRequestType.SetOverrevNumberGrading(Index: Integer; const AMinMaxGradeType: MinMaxGradeType);
begin
  FOverrevNumberGrading := AMinMaxGradeType;
  FOverrevNumberGrading_Specified := True;
end;

function GetUniversalPerformanceAnalysisDriverOverviewRequestType.OverrevNumberGrading_Specified(Index: Integer): boolean;
begin
  Result := FOverrevNumberGrading_Specified;
end;

procedure GetUniversalPerformanceAnalysisDriverOverviewRequestType.SetOverrevTimeGrading(Index: Integer; const AMinMaxGradeType: MinMaxGradeType);
begin
  FOverrevTimeGrading := AMinMaxGradeType;
  FOverrevTimeGrading_Specified := True;
end;

function GetUniversalPerformanceAnalysisDriverOverviewRequestType.OverrevTimeGrading_Specified(Index: Integer): boolean;
begin
  Result := FOverrevTimeGrading_Specified;
end;

procedure GetUniversalPerformanceAnalysisDriverOverviewRequestType.SetEcoDrivingGrading(Index: Integer; const AMinMaxGradeType: MinMaxGradeType);
begin
  FEcoDrivingGrading := AMinMaxGradeType;
  FEcoDrivingGrading_Specified := True;
end;

function GetUniversalPerformanceAnalysisDriverOverviewRequestType.EcoDrivingGrading_Specified(Index: Integer): boolean;
begin
  Result := FEcoDrivingGrading_Specified;
end;

procedure GetUniversalPerformanceAnalysisDriverOverviewRequestType.SetHarshBrakeGrading(Index: Integer; const AMinMaxGradeType: MinMaxGradeType);
begin
  FHarshBrakeGrading := AMinMaxGradeType;
  FHarshBrakeGrading_Specified := True;
end;

function GetUniversalPerformanceAnalysisDriverOverviewRequestType.HarshBrakeGrading_Specified(Index: Integer): boolean;
begin
  Result := FHarshBrakeGrading_Specified;
end;

procedure GetUniversalPerformanceAnalysisDriverOverviewRequestType.SetOverspeedTimeGrading(Index: Integer; const AMinMaxGradeType: MinMaxGradeType);
begin
  FOverspeedTimeGrading := AMinMaxGradeType;
  FOverspeedTimeGrading_Specified := True;
end;

function GetUniversalPerformanceAnalysisDriverOverviewRequestType.OverspeedTimeGrading_Specified(Index: Integer): boolean;
begin
  Result := FOverspeedTimeGrading_Specified;
end;

procedure GetUniversalPerformanceAnalysisDriverOverviewRequestType.SetAvgTotalConsumption(Index: Integer; const AMinMaxConsumptionType: MinMaxConsumptionType);
begin
  FAvgTotalConsumption := AMinMaxConsumptionType;
  FAvgTotalConsumption_Specified := True;
end;

function GetUniversalPerformanceAnalysisDriverOverviewRequestType.AvgTotalConsumption_Specified(Index: Integer): boolean;
begin
  Result := FAvgTotalConsumption_Specified;
end;

destructor MBPerformanceAnalysisVehicleReport.Destroy;
begin
  SysUtils.FreeAndNil(FPeriod);
  SysUtils.FreeAndNil(FGrades);
  SysUtils.FreeAndNil(FAvgValues);
  SysUtils.FreeAndNil(FConsumption);
  SysUtils.FreeAndNil(FPowerTakeOff);
  SysUtils.FreeAndNil(FAssistanceSystems);
  SysUtils.FreeAndNil(FCharts);
  inherited Destroy;
end;

procedure MBPerformanceAnalysisVehicleReport.SetDriverNameID(Index: Integer; const AArray_Of_driverNameIDType: Array_Of_driverNameIDType);
begin
  FDriverNameID := AArray_Of_driverNameIDType;
  FDriverNameID_Specified := True;
end;

function MBPerformanceAnalysisVehicleReport.DriverNameID_Specified(Index: Integer): boolean;
begin
  Result := FDriverNameID_Specified;
end;

procedure MBPerformanceAnalysisVehicleReport.SetTrailerName(Index: Integer; const AArray_Of_string: Array_Of_string);
begin
  FTrailerName := AArray_Of_string;
  FTrailerName_Specified := True;
end;

function MBPerformanceAnalysisVehicleReport.TrailerName_Specified(Index: Integer): boolean;
begin
  Result := FTrailerName_Specified;
end;

procedure MBPerformanceAnalysisVehicleReport.SetBrakingDistance(Index: Integer; const AdistanceType: distanceType);
begin
  FBrakingDistance := AdistanceType;
  FBrakingDistance_Specified := True;
end;

function MBPerformanceAnalysisVehicleReport.BrakingDistance_Specified(Index: Integer): boolean;
begin
  Result := FBrakingDistance_Specified;
end;

procedure MBPerformanceAnalysisVehicleReport.SetNrOfStops(Index: Integer; const AInt64: Int64);
begin
  FNrOfStops := AInt64;
  FNrOfStops_Specified := True;
end;

function MBPerformanceAnalysisVehicleReport.NrOfStops_Specified(Index: Integer): boolean;
begin
  Result := FNrOfStops_Specified;
end;

procedure MBPerformanceAnalysisVehicleReport.SetHandBrakeUsageCount(Index: Integer; const AInt64: Int64);
begin
  FHandBrakeUsageCount := AInt64;
  FHandBrakeUsageCount_Specified := True;
end;

function MBPerformanceAnalysisVehicleReport.HandBrakeUsageCount_Specified(Index: Integer): boolean;
begin
  Result := FHandBrakeUsageCount_Specified;
end;

procedure MBPerformanceAnalysisVehicleReport.SetMovingTime(Index: Integer; const AInt64: Int64);
begin
  FMovingTime := AInt64;
  FMovingTime_Specified := True;
end;

function MBPerformanceAnalysisVehicleReport.MovingTime_Specified(Index: Integer): boolean;
begin
  Result := FMovingTime_Specified;
end;

procedure MBPerformanceAnalysisVehicleReport.SetStopTimeEngineOn(Index: Integer; const AtimeType: timeType);
begin
  FStopTimeEngineOn := AtimeType;
  FStopTimeEngineOn_Specified := True;
end;

function MBPerformanceAnalysisVehicleReport.StopTimeEngineOn_Specified(Index: Integer): boolean;
begin
  Result := FStopTimeEngineOn_Specified;
end;

procedure MBPerformanceAnalysisVehicleReport.SetStopTimeEngineOff(Index: Integer; const AtimeType: timeType);
begin
  FStopTimeEngineOff := AtimeType;
  FStopTimeEngineOff_Specified := True;
end;

function MBPerformanceAnalysisVehicleReport.StopTimeEngineOff_Specified(Index: Integer): boolean;
begin
  Result := FStopTimeEngineOff_Specified;
end;

procedure MBPerformanceAnalysisVehicleReport.SetSpeedOver85PerTotalDistance(Index: Integer; const AspeedType: speedType);
begin
  FSpeedOver85PerTotalDistance := AspeedType;
  FSpeedOver85PerTotalDistance_Specified := True;
end;

function MBPerformanceAnalysisVehicleReport.SpeedOver85PerTotalDistance_Specified(Index: Integer): boolean;
begin
  Result := FSpeedOver85PerTotalDistance_Specified;
end;

procedure MBPerformanceAnalysisVehicleReport.SetRetarderBrakingDistance(Index: Integer; const AdistanceType: distanceType);
begin
  FRetarderBrakingDistance := AdistanceType;
  FRetarderBrakingDistance_Specified := True;
end;

function MBPerformanceAnalysisVehicleReport.RetarderBrakingDistance_Specified(Index: Integer): boolean;
begin
  Result := FRetarderBrakingDistance_Specified;
end;

procedure MBPerformanceAnalysisVehicleReport.SetTotalCoastingDistance(Index: Integer; const AdistanceType: distanceType);
begin
  FTotalCoastingDistance := AdistanceType;
  FTotalCoastingDistance_Specified := True;
end;

function MBPerformanceAnalysisVehicleReport.TotalCoastingDistance_Specified(Index: Integer): boolean;
begin
  Result := FTotalCoastingDistance_Specified;
end;

procedure MBPerformanceAnalysisVehicleReport.SetTotalDistance(Index: Integer; const AdistanceType: distanceType);
begin
  FTotalDistance := AdistanceType;
  FTotalDistance_Specified := True;
end;

function MBPerformanceAnalysisVehicleReport.TotalDistance_Specified(Index: Integer): boolean;
begin
  Result := FTotalDistance_Specified;
end;

procedure MBPerformanceAnalysisVehicleReport.SetCruiseControlDistance(Index: Integer; const AdistanceType: distanceType);
begin
  FCruiseControlDistance := AdistanceType;
  FCruiseControlDistance_Specified := True;
end;

function MBPerformanceAnalysisVehicleReport.CruiseControlDistance_Specified(Index: Integer): boolean;
begin
  Result := FCruiseControlDistance_Specified;
end;

procedure MBPerformanceAnalysisVehicleReport.SetAssistanceSystems(Index: Integer; const AassistanceSystems: assistanceSystems);
begin
  FAssistanceSystems := AassistanceSystems;
  FAssistanceSystems_Specified := True;
end;

function MBPerformanceAnalysisVehicleReport.AssistanceSystems_Specified(Index: Integer): boolean;
begin
  Result := FAssistanceSystems_Specified;
end;

procedure MBPerformanceAnalysisVehicleReport.SetCharts(Index: Integer; const AChartsType: ChartsType);
begin
  FCharts := AChartsType;
  FCharts_Specified := True;
end;

function MBPerformanceAnalysisVehicleReport.Charts_Specified(Index: Integer): boolean;
begin
  Result := FCharts_Specified;
end;

destructor UniversalPerformanceAnalysisVehicleReport.Destroy;
begin
  SysUtils.FreeAndNil(FPeriod);
  SysUtils.FreeAndNil(FGrades);
  SysUtils.FreeAndNil(FAvgValues);
  SysUtils.FreeAndNil(FConsumption);
  SysUtils.FreeAndNil(FCharts);
  inherited Destroy;
end;

procedure UniversalPerformanceAnalysisVehicleReport.SetDriverNameID(Index: Integer; const AArray_Of_driverNameIDType: Array_Of_driverNameIDType);
begin
  FDriverNameID := AArray_Of_driverNameIDType;
  FDriverNameID_Specified := True;
end;

function UniversalPerformanceAnalysisVehicleReport.DriverNameID_Specified(Index: Integer): boolean;
begin
  Result := FDriverNameID_Specified;
end;

procedure UniversalPerformanceAnalysisVehicleReport.SetTrailerName(Index: Integer; const AArray_Of_string: Array_Of_string);
begin
  FTrailerName := AArray_Of_string;
  FTrailerName_Specified := True;
end;

function UniversalPerformanceAnalysisVehicleReport.TrailerName_Specified(Index: Integer): boolean;
begin
  Result := FTrailerName_Specified;
end;

procedure UniversalPerformanceAnalysisVehicleReport.SetTotalDistance(Index: Integer; const AdistanceType: distanceType);
begin
  FTotalDistance := AdistanceType;
  FTotalDistance_Specified := True;
end;

function UniversalPerformanceAnalysisVehicleReport.TotalDistance_Specified(Index: Integer): boolean;
begin
  Result := FTotalDistance_Specified;
end;

procedure UniversalPerformanceAnalysisVehicleReport.SetMovingTime(Index: Integer; const AInt64: Int64);
begin
  FMovingTime := AInt64;
  FMovingTime_Specified := True;
end;

function UniversalPerformanceAnalysisVehicleReport.MovingTime_Specified(Index: Integer): boolean;
begin
  Result := FMovingTime_Specified;
end;

procedure UniversalPerformanceAnalysisVehicleReport.SetIdlingTimeEngineOn(Index: Integer; const AtimeType: timeType);
begin
  FIdlingTimeEngineOn := AtimeType;
  FIdlingTimeEngineOn_Specified := True;
end;

function UniversalPerformanceAnalysisVehicleReport.IdlingTimeEngineOn_Specified(Index: Integer): boolean;
begin
  Result := FIdlingTimeEngineOn_Specified;
end;

procedure UniversalPerformanceAnalysisVehicleReport.SetIdlingTimeEngineOff(Index: Integer; const AtimeType: timeType);
begin
  FIdlingTimeEngineOff := AtimeType;
  FIdlingTimeEngineOff_Specified := True;
end;

function UniversalPerformanceAnalysisVehicleReport.IdlingTimeEngineOff_Specified(Index: Integer): boolean;
begin
  Result := FIdlingTimeEngineOff_Specified;
end;

procedure UniversalPerformanceAnalysisVehicleReport.SetPTOTime(Index: Integer; const AtimeType: timeType);
begin
  FPTOTime := AtimeType;
  FPTOTime_Specified := True;
end;

function UniversalPerformanceAnalysisVehicleReport.PTOTime_Specified(Index: Integer): boolean;
begin
  Result := FPTOTime_Specified;
end;

procedure UniversalPerformanceAnalysisVehicleReport.SetOverspeedTime(Index: Integer; const AtimeType: timeType);
begin
  FOverspeedTime := AtimeType;
  FOverspeedTime_Specified := True;
end;

function UniversalPerformanceAnalysisVehicleReport.OverspeedTime_Specified(Index: Integer): boolean;
begin
  Result := FOverspeedTime_Specified;
end;

procedure UniversalPerformanceAnalysisVehicleReport.SetOverrevTime(Index: Integer; const AtimeType: timeType);
begin
  FOverrevTime := AtimeType;
  FOverrevTime_Specified := True;
end;

function UniversalPerformanceAnalysisVehicleReport.OverrevTime_Specified(Index: Integer): boolean;
begin
  Result := FOverrevTime_Specified;
end;

procedure UniversalPerformanceAnalysisVehicleReport.SetOverrevNumber(Index: Integer; const AInt64: Int64);
begin
  FOverrevNumber := AInt64;
  FOverrevNumber_Specified := True;
end;

function UniversalPerformanceAnalysisVehicleReport.OverrevNumber_Specified(Index: Integer): boolean;
begin
  Result := FOverrevNumber_Specified;
end;

procedure UniversalPerformanceAnalysisVehicleReport.SetGreenBandRate(Index: Integer; const ASmallInt: SmallInt);
begin
  FGreenBandRate := ASmallInt;
  FGreenBandRate_Specified := True;
end;

function UniversalPerformanceAnalysisVehicleReport.GreenBandRate_Specified(Index: Integer): boolean;
begin
  Result := FGreenBandRate_Specified;
end;

procedure UniversalPerformanceAnalysisVehicleReport.SetAccelerationNumber(Index: Integer; const AInt64: Int64);
begin
  FAccelerationNumber := AInt64;
  FAccelerationNumber_Specified := True;
end;

function UniversalPerformanceAnalysisVehicleReport.AccelerationNumber_Specified(Index: Integer): boolean;
begin
  Result := FAccelerationNumber_Specified;
end;

procedure UniversalPerformanceAnalysisVehicleReport.SetNumberOfStops(Index: Integer; const AInt64: Int64);
begin
  FNumberOfStops := AInt64;
  FNumberOfStops_Specified := True;
end;

function UniversalPerformanceAnalysisVehicleReport.NumberOfStops_Specified(Index: Integer): boolean;
begin
  Result := FNumberOfStops_Specified;
end;

procedure UniversalPerformanceAnalysisVehicleReport.SetPTONumber(Index: Integer; const AInt64: Int64);
begin
  FPTONumber := AInt64;
  FPTONumber_Specified := True;
end;

function UniversalPerformanceAnalysisVehicleReport.PTONumber_Specified(Index: Integer): boolean;
begin
  Result := FPTONumber_Specified;
end;

procedure UniversalPerformanceAnalysisVehicleReport.SetHarshBrakeNumber(Index: Integer; const AInt64: Int64);
begin
  FHarshBrakeNumber := AInt64;
  FHarshBrakeNumber_Specified := True;
end;

function UniversalPerformanceAnalysisVehicleReport.HarshBrakeNumber_Specified(Index: Integer): boolean;
begin
  Result := FHarshBrakeNumber_Specified;
end;

procedure UniversalPerformanceAnalysisVehicleReport.SetCharts(Index: Integer; const AChartsType: ChartsType);
begin
  FCharts := AChartsType;
  FCharts_Specified := True;
end;

function UniversalPerformanceAnalysisVehicleReport.Charts_Specified(Index: Integer): boolean;
begin
  Result := FCharts_Specified;
end;

destructor GetUniversalPerformanceAnalysisDriverRequestType.Destroy;
begin
  SysUtils.FreeAndNil(FAnalysis);
  SysUtils.FreeAndNil(FDrivingStyle);
  SysUtils.FreeAndNil(FIdleTimeGrading);
  SysUtils.FreeAndNil(FOverrevNumberGrading);
  SysUtils.FreeAndNil(FOverrevTimeGrading);
  SysUtils.FreeAndNil(FEcoDrivingGrading);
  SysUtils.FreeAndNil(FHarshBrakeGrading);
  SysUtils.FreeAndNil(FOverspeedTimeGrading);
  SysUtils.FreeAndNil(FAvgTotalConsumption);
  inherited Destroy;
end;

procedure GetUniversalPerformanceAnalysisDriverRequestType.Setsessionid(Index: Integer; const AsessionidType: sessionidType);
begin
  Fsessionid := AsessionidType;
  Fsessionid_Specified := True;
end;

function GetUniversalPerformanceAnalysisDriverRequestType.sessionid_Specified(Index: Integer): boolean;
begin
  Result := Fsessionid_Specified;
end;

procedure GetUniversalPerformanceAnalysisDriverRequestType.Setlimit(Index: Integer; const AlimitType: limitType);
begin
  Flimit := AlimitType;
  Flimit_Specified := True;
end;

function GetUniversalPerformanceAnalysisDriverRequestType.limit_Specified(Index: Integer): boolean;
begin
  Result := Flimit_Specified;
end;

procedure GetUniversalPerformanceAnalysisDriverRequestType.Setoffset(Index: Integer; const AoffsetType: offsetType);
begin
  Foffset := AoffsetType;
  Foffset_Specified := True;
end;

function GetUniversalPerformanceAnalysisDriverRequestType.offset_Specified(Index: Integer): boolean;
begin
  Result := Foffset_Specified;
end;

procedure GetUniversalPerformanceAnalysisDriverRequestType.Setversion(Index: Integer; const AversionType: versionType);
begin
  Fversion := AversionType;
  Fversion_Specified := True;
end;

function GetUniversalPerformanceAnalysisDriverRequestType.version_Specified(Index: Integer): boolean;
begin
  Result := Fversion_Specified;
end;

procedure GetUniversalPerformanceAnalysisDriverRequestType.SetGetCharts(Index: Integer; const AgetChartsType: getChartsType);
begin
  FGetCharts := AgetChartsType;
  FGetCharts_Specified := True;
end;

function GetUniversalPerformanceAnalysisDriverRequestType.GetCharts_Specified(Index: Integer): boolean;
begin
  Result := FGetCharts_Specified;
end;

procedure GetUniversalPerformanceAnalysisDriverRequestType.SetDriverNameID(Index: Integer; const AdriverNameIDType: driverNameIDType);
begin
  FDriverNameID := AdriverNameIDType;
  FDriverNameID_Specified := True;
end;

function GetUniversalPerformanceAnalysisDriverRequestType.DriverNameID_Specified(Index: Integer): boolean;
begin
  Result := FDriverNameID_Specified;
end;

procedure GetUniversalPerformanceAnalysisDriverRequestType.SetDriverGroupID(Index: Integer; const AdriverGroupIDType: driverGroupIDType);
begin
  FDriverGroupID := AdriverGroupIDType;
  FDriverGroupID_Specified := True;
end;

function GetUniversalPerformanceAnalysisDriverRequestType.DriverGroupID_Specified(Index: Integer): boolean;
begin
  Result := FDriverGroupID_Specified;
end;

procedure GetUniversalPerformanceAnalysisDriverRequestType.SetDrivingStyle(Index: Integer; const AMinMaxGradeType: MinMaxGradeType);
begin
  FDrivingStyle := AMinMaxGradeType;
  FDrivingStyle_Specified := True;
end;

function GetUniversalPerformanceAnalysisDriverRequestType.DrivingStyle_Specified(Index: Integer): boolean;
begin
  Result := FDrivingStyle_Specified;
end;

procedure GetUniversalPerformanceAnalysisDriverRequestType.SetIdleTimeGrading(Index: Integer; const AMinMaxGradeType: MinMaxGradeType);
begin
  FIdleTimeGrading := AMinMaxGradeType;
  FIdleTimeGrading_Specified := True;
end;

function GetUniversalPerformanceAnalysisDriverRequestType.IdleTimeGrading_Specified(Index: Integer): boolean;
begin
  Result := FIdleTimeGrading_Specified;
end;

procedure GetUniversalPerformanceAnalysisDriverRequestType.SetOverrevNumberGrading(Index: Integer; const AMinMaxGradeType: MinMaxGradeType);
begin
  FOverrevNumberGrading := AMinMaxGradeType;
  FOverrevNumberGrading_Specified := True;
end;

function GetUniversalPerformanceAnalysisDriverRequestType.OverrevNumberGrading_Specified(Index: Integer): boolean;
begin
  Result := FOverrevNumberGrading_Specified;
end;

procedure GetUniversalPerformanceAnalysisDriverRequestType.SetOverrevTimeGrading(Index: Integer; const AMinMaxGradeType: MinMaxGradeType);
begin
  FOverrevTimeGrading := AMinMaxGradeType;
  FOverrevTimeGrading_Specified := True;
end;

function GetUniversalPerformanceAnalysisDriverRequestType.OverrevTimeGrading_Specified(Index: Integer): boolean;
begin
  Result := FOverrevTimeGrading_Specified;
end;

procedure GetUniversalPerformanceAnalysisDriverRequestType.SetEcoDrivingGrading(Index: Integer; const AMinMaxGradeType: MinMaxGradeType);
begin
  FEcoDrivingGrading := AMinMaxGradeType;
  FEcoDrivingGrading_Specified := True;
end;

function GetUniversalPerformanceAnalysisDriverRequestType.EcoDrivingGrading_Specified(Index: Integer): boolean;
begin
  Result := FEcoDrivingGrading_Specified;
end;

procedure GetUniversalPerformanceAnalysisDriverRequestType.SetHarshBrakeGrading(Index: Integer; const AMinMaxGradeType: MinMaxGradeType);
begin
  FHarshBrakeGrading := AMinMaxGradeType;
  FHarshBrakeGrading_Specified := True;
end;

function GetUniversalPerformanceAnalysisDriverRequestType.HarshBrakeGrading_Specified(Index: Integer): boolean;
begin
  Result := FHarshBrakeGrading_Specified;
end;

procedure GetUniversalPerformanceAnalysisDriverRequestType.SetOverspeedTimeGrading(Index: Integer; const AMinMaxGradeType: MinMaxGradeType);
begin
  FOverspeedTimeGrading := AMinMaxGradeType;
  FOverspeedTimeGrading_Specified := True;
end;

function GetUniversalPerformanceAnalysisDriverRequestType.OverspeedTimeGrading_Specified(Index: Integer): boolean;
begin
  Result := FOverspeedTimeGrading_Specified;
end;

procedure GetUniversalPerformanceAnalysisDriverRequestType.SetAvgTotalConsumption(Index: Integer; const AMinMaxConsumptionType: MinMaxConsumptionType);
begin
  FAvgTotalConsumption := AMinMaxConsumptionType;
  FAvgTotalConsumption_Specified := True;
end;

function GetUniversalPerformanceAnalysisDriverRequestType.AvgTotalConsumption_Specified(Index: Integer): boolean;
begin
  Result := FAvgTotalConsumption_Specified;
end;

destructor MBPerformanceAnalysisDriverReport.Destroy;
var
  I: Integer;
begin
  for I := 0 to System.Length(FVehicleDataSet)-1 do
    SysUtils.FreeAndNil(FVehicleDataSet[I]);
  System.SetLength(FVehicleDataSet, 0);
  SysUtils.FreeAndNil(FPeriod);
  SysUtils.FreeAndNil(FGrades);
  SysUtils.FreeAndNil(FAvgValues);
  SysUtils.FreeAndNil(FConsumption);
  SysUtils.FreeAndNil(FPowerTakeOff);
  SysUtils.FreeAndNil(FAssistanceSystems);
  SysUtils.FreeAndNil(FCharts);
  inherited Destroy;
end;

procedure MBPerformanceAnalysisDriverReport.SetBrakingDistance(Index: Integer; const AdistanceType: distanceType);
begin
  FBrakingDistance := AdistanceType;
  FBrakingDistance_Specified := True;
end;

function MBPerformanceAnalysisDriverReport.BrakingDistance_Specified(Index: Integer): boolean;
begin
  Result := FBrakingDistance_Specified;
end;

procedure MBPerformanceAnalysisDriverReport.SetNrOfStops(Index: Integer; const AInt64: Int64);
begin
  FNrOfStops := AInt64;
  FNrOfStops_Specified := True;
end;

function MBPerformanceAnalysisDriverReport.NrOfStops_Specified(Index: Integer): boolean;
begin
  Result := FNrOfStops_Specified;
end;

procedure MBPerformanceAnalysisDriverReport.SetHandBrakeUsageCount(Index: Integer; const AInt64: Int64);
begin
  FHandBrakeUsageCount := AInt64;
  FHandBrakeUsageCount_Specified := True;
end;

function MBPerformanceAnalysisDriverReport.HandBrakeUsageCount_Specified(Index: Integer): boolean;
begin
  Result := FHandBrakeUsageCount_Specified;
end;

procedure MBPerformanceAnalysisDriverReport.SetMovingTime(Index: Integer; const AInt64: Int64);
begin
  FMovingTime := AInt64;
  FMovingTime_Specified := True;
end;

function MBPerformanceAnalysisDriverReport.MovingTime_Specified(Index: Integer): boolean;
begin
  Result := FMovingTime_Specified;
end;

procedure MBPerformanceAnalysisDriverReport.SetStopTimeEngineOn(Index: Integer; const AtimeType: timeType);
begin
  FStopTimeEngineOn := AtimeType;
  FStopTimeEngineOn_Specified := True;
end;

function MBPerformanceAnalysisDriverReport.StopTimeEngineOn_Specified(Index: Integer): boolean;
begin
  Result := FStopTimeEngineOn_Specified;
end;

procedure MBPerformanceAnalysisDriverReport.SetStopTimeEngineOff(Index: Integer; const AtimeType: timeType);
begin
  FStopTimeEngineOff := AtimeType;
  FStopTimeEngineOff_Specified := True;
end;

function MBPerformanceAnalysisDriverReport.StopTimeEngineOff_Specified(Index: Integer): boolean;
begin
  Result := FStopTimeEngineOff_Specified;
end;

procedure MBPerformanceAnalysisDriverReport.SetSpeedOver85PerTotalDistance(Index: Integer; const AspeedType: speedType);
begin
  FSpeedOver85PerTotalDistance := AspeedType;
  FSpeedOver85PerTotalDistance_Specified := True;
end;

function MBPerformanceAnalysisDriverReport.SpeedOver85PerTotalDistance_Specified(Index: Integer): boolean;
begin
  Result := FSpeedOver85PerTotalDistance_Specified;
end;

procedure MBPerformanceAnalysisDriverReport.SetRetarderBrakingDistance(Index: Integer; const AdistanceType: distanceType);
begin
  FRetarderBrakingDistance := AdistanceType;
  FRetarderBrakingDistance_Specified := True;
end;

function MBPerformanceAnalysisDriverReport.RetarderBrakingDistance_Specified(Index: Integer): boolean;
begin
  Result := FRetarderBrakingDistance_Specified;
end;

procedure MBPerformanceAnalysisDriverReport.SetTotalCoastingDistance(Index: Integer; const AdistanceType: distanceType);
begin
  FTotalCoastingDistance := AdistanceType;
  FTotalCoastingDistance_Specified := True;
end;

function MBPerformanceAnalysisDriverReport.TotalCoastingDistance_Specified(Index: Integer): boolean;
begin
  Result := FTotalCoastingDistance_Specified;
end;

procedure MBPerformanceAnalysisDriverReport.SetTotalDistance(Index: Integer; const AdistanceType: distanceType);
begin
  FTotalDistance := AdistanceType;
  FTotalDistance_Specified := True;
end;

function MBPerformanceAnalysisDriverReport.TotalDistance_Specified(Index: Integer): boolean;
begin
  Result := FTotalDistance_Specified;
end;

procedure MBPerformanceAnalysisDriverReport.SetCruiseControlDistance(Index: Integer; const AdistanceType: distanceType);
begin
  FCruiseControlDistance := AdistanceType;
  FCruiseControlDistance_Specified := True;
end;

function MBPerformanceAnalysisDriverReport.CruiseControlDistance_Specified(Index: Integer): boolean;
begin
  Result := FCruiseControlDistance_Specified;
end;

procedure MBPerformanceAnalysisDriverReport.SetAssistanceSystems(Index: Integer; const AassistanceSystems: assistanceSystems);
begin
  FAssistanceSystems := AassistanceSystems;
  FAssistanceSystems_Specified := True;
end;

function MBPerformanceAnalysisDriverReport.AssistanceSystems_Specified(Index: Integer): boolean;
begin
  Result := FAssistanceSystems_Specified;
end;

procedure MBPerformanceAnalysisDriverReport.SetVehicleDataSet(Index: Integer; const AArray_Of_VehicleDataSet: Array_Of_VehicleDataSet);
begin
  FVehicleDataSet := AArray_Of_VehicleDataSet;
  FVehicleDataSet_Specified := True;
end;

function MBPerformanceAnalysisDriverReport.VehicleDataSet_Specified(Index: Integer): boolean;
begin
  Result := FVehicleDataSet_Specified;
end;

procedure MBPerformanceAnalysisDriverReport.SetCharts(Index: Integer; const AChartsType: ChartsType);
begin
  FCharts := AChartsType;
  FCharts_Specified := True;
end;

function MBPerformanceAnalysisDriverReport.Charts_Specified(Index: Integer): boolean;
begin
  Result := FCharts_Specified;
end;

destructor UniversalPerformanceAnalysisDriverReport.Destroy;
var
  I: Integer;
begin
  for I := 0 to System.Length(FVehicleDataSet)-1 do
    SysUtils.FreeAndNil(FVehicleDataSet[I]);
  System.SetLength(FVehicleDataSet, 0);
  SysUtils.FreeAndNil(FPeriod);
  SysUtils.FreeAndNil(FGrades);
  SysUtils.FreeAndNil(FAvgValues);
  SysUtils.FreeAndNil(FConsumption);
  SysUtils.FreeAndNil(FCharts);
  inherited Destroy;
end;

procedure UniversalPerformanceAnalysisDriverReport.SetTotalDistance(Index: Integer; const AdistanceType: distanceType);
begin
  FTotalDistance := AdistanceType;
  FTotalDistance_Specified := True;
end;

function UniversalPerformanceAnalysisDriverReport.TotalDistance_Specified(Index: Integer): boolean;
begin
  Result := FTotalDistance_Specified;
end;

procedure UniversalPerformanceAnalysisDriverReport.SetMovingTime(Index: Integer; const AInt64: Int64);
begin
  FMovingTime := AInt64;
  FMovingTime_Specified := True;
end;

function UniversalPerformanceAnalysisDriverReport.MovingTime_Specified(Index: Integer): boolean;
begin
  Result := FMovingTime_Specified;
end;

procedure UniversalPerformanceAnalysisDriverReport.SetIdlingTimeEngineOn(Index: Integer; const AtimeType: timeType);
begin
  FIdlingTimeEngineOn := AtimeType;
  FIdlingTimeEngineOn_Specified := True;
end;

function UniversalPerformanceAnalysisDriverReport.IdlingTimeEngineOn_Specified(Index: Integer): boolean;
begin
  Result := FIdlingTimeEngineOn_Specified;
end;

procedure UniversalPerformanceAnalysisDriverReport.SetIdlingTimeEngineOff(Index: Integer; const AtimeType: timeType);
begin
  FIdlingTimeEngineOff := AtimeType;
  FIdlingTimeEngineOff_Specified := True;
end;

function UniversalPerformanceAnalysisDriverReport.IdlingTimeEngineOff_Specified(Index: Integer): boolean;
begin
  Result := FIdlingTimeEngineOff_Specified;
end;

procedure UniversalPerformanceAnalysisDriverReport.SetPTOTime(Index: Integer; const AtimeType: timeType);
begin
  FPTOTime := AtimeType;
  FPTOTime_Specified := True;
end;

function UniversalPerformanceAnalysisDriverReport.PTOTime_Specified(Index: Integer): boolean;
begin
  Result := FPTOTime_Specified;
end;

procedure UniversalPerformanceAnalysisDriverReport.SetOverspeedTime(Index: Integer; const AtimeType: timeType);
begin
  FOverspeedTime := AtimeType;
  FOverspeedTime_Specified := True;
end;

function UniversalPerformanceAnalysisDriverReport.OverspeedTime_Specified(Index: Integer): boolean;
begin
  Result := FOverspeedTime_Specified;
end;

procedure UniversalPerformanceAnalysisDriverReport.SetOverrevTime(Index: Integer; const AtimeType: timeType);
begin
  FOverrevTime := AtimeType;
  FOverrevTime_Specified := True;
end;

function UniversalPerformanceAnalysisDriverReport.OverrevTime_Specified(Index: Integer): boolean;
begin
  Result := FOverrevTime_Specified;
end;

procedure UniversalPerformanceAnalysisDriverReport.SetOverrevNumber(Index: Integer; const AInt64: Int64);
begin
  FOverrevNumber := AInt64;
  FOverrevNumber_Specified := True;
end;

function UniversalPerformanceAnalysisDriverReport.OverrevNumber_Specified(Index: Integer): boolean;
begin
  Result := FOverrevNumber_Specified;
end;

procedure UniversalPerformanceAnalysisDriverReport.SetGreenBandRate(Index: Integer; const ASmallInt: SmallInt);
begin
  FGreenBandRate := ASmallInt;
  FGreenBandRate_Specified := True;
end;

function UniversalPerformanceAnalysisDriverReport.GreenBandRate_Specified(Index: Integer): boolean;
begin
  Result := FGreenBandRate_Specified;
end;

procedure UniversalPerformanceAnalysisDriverReport.SetAccelerationNumber(Index: Integer; const AInt64: Int64);
begin
  FAccelerationNumber := AInt64;
  FAccelerationNumber_Specified := True;
end;

function UniversalPerformanceAnalysisDriverReport.AccelerationNumber_Specified(Index: Integer): boolean;
begin
  Result := FAccelerationNumber_Specified;
end;

procedure UniversalPerformanceAnalysisDriverReport.SetNumberOfStops(Index: Integer; const AInt64: Int64);
begin
  FNumberOfStops := AInt64;
  FNumberOfStops_Specified := True;
end;

function UniversalPerformanceAnalysisDriverReport.NumberOfStops_Specified(Index: Integer): boolean;
begin
  Result := FNumberOfStops_Specified;
end;

procedure UniversalPerformanceAnalysisDriverReport.SetPTONumber(Index: Integer; const AInt64: Int64);
begin
  FPTONumber := AInt64;
  FPTONumber_Specified := True;
end;

function UniversalPerformanceAnalysisDriverReport.PTONumber_Specified(Index: Integer): boolean;
begin
  Result := FPTONumber_Specified;
end;

procedure UniversalPerformanceAnalysisDriverReport.SetHarshBrakeNumber(Index: Integer; const AInt64: Int64);
begin
  FHarshBrakeNumber := AInt64;
  FHarshBrakeNumber_Specified := True;
end;

function UniversalPerformanceAnalysisDriverReport.HarshBrakeNumber_Specified(Index: Integer): boolean;
begin
  Result := FHarshBrakeNumber_Specified;
end;

procedure UniversalPerformanceAnalysisDriverReport.SetVehicleDataSet(Index: Integer; const AArray_Of_VehicleDataSet2: Array_Of_VehicleDataSet2);
begin
  FVehicleDataSet := AArray_Of_VehicleDataSet2;
  FVehicleDataSet_Specified := True;
end;

function UniversalPerformanceAnalysisDriverReport.VehicleDataSet_Specified(Index: Integer): boolean;
begin
  Result := FVehicleDataSet_Specified;
end;

procedure UniversalPerformanceAnalysisDriverReport.SetCharts(Index: Integer; const AChartsType: ChartsType);
begin
  FCharts := AChartsType;
  FCharts_Specified := True;
end;

function UniversalPerformanceAnalysisDriverReport.Charts_Specified(Index: Integer): boolean;
begin
  Result := FCharts_Specified;
end;

destructor GetMBPerformanceAnalysisDriverOverviewResponseType.Destroy;
var
  I: Integer;
begin
  for I := 0 to System.Length(FMBPerformanceAnalysisDriverOverviewReport)-1 do
    SysUtils.FreeAndNil(FMBPerformanceAnalysisDriverOverviewReport[I]);
  System.SetLength(FMBPerformanceAnalysisDriverOverviewReport, 0);
  inherited Destroy;
end;

procedure GetMBPerformanceAnalysisDriverOverviewResponseType.Setlimit(Index: Integer; const AlimitType: limitType);
begin
  Flimit := AlimitType;
  Flimit_Specified := True;
end;

function GetMBPerformanceAnalysisDriverOverviewResponseType.limit_Specified(Index: Integer): boolean;
begin
  Result := Flimit_Specified;
end;

procedure GetMBPerformanceAnalysisDriverOverviewResponseType.Setoffset(Index: Integer; const AoffsetType: offsetType);
begin
  Foffset := AoffsetType;
  Foffset_Specified := True;
end;

function GetMBPerformanceAnalysisDriverOverviewResponseType.offset_Specified(Index: Integer): boolean;
begin
  Result := Foffset_Specified;
end;

procedure GetMBPerformanceAnalysisDriverOverviewResponseType.SetresultSize(Index: Integer; const AresultSizeType: resultSizeType);
begin
  FresultSize := AresultSizeType;
  FresultSize_Specified := True;
end;

function GetMBPerformanceAnalysisDriverOverviewResponseType.resultSize_Specified(Index: Integer): boolean;
begin
  Result := FresultSize_Specified;
end;

procedure GetMBPerformanceAnalysisDriverOverviewResponseType.SetresponseSize(Index: Integer; const AresponseSizeType: responseSizeType);
begin
  FresponseSize := AresponseSizeType;
  FresponseSize_Specified := True;
end;

function GetMBPerformanceAnalysisDriverOverviewResponseType.responseSize_Specified(Index: Integer): boolean;
begin
  Result := FresponseSize_Specified;
end;

procedure GetMBPerformanceAnalysisDriverOverviewResponseType.Setversion(Index: Integer; const AversionType: versionType);
begin
  Fversion := AversionType;
  Fversion_Specified := True;
end;

function GetMBPerformanceAnalysisDriverOverviewResponseType.version_Specified(Index: Integer): boolean;
begin
  Result := Fversion_Specified;
end;

procedure GetMBPerformanceAnalysisDriverOverviewResponseType.SetMBPerformanceAnalysisDriverOverviewReport(Index: Integer; const AArray_Of_MBPerformanceAnalysisDriverOverviewReport: Array_Of_MBPerformanceAnalysisDriverOverviewReport);
begin
  FMBPerformanceAnalysisDriverOverviewReport := AArray_Of_MBPerformanceAnalysisDriverOverviewReport;
  FMBPerformanceAnalysisDriverOverviewReport_Specified := True;
end;

function GetMBPerformanceAnalysisDriverOverviewResponseType.MBPerformanceAnalysisDriverOverviewReport_Specified(Index: Integer): boolean;
begin
  Result := FMBPerformanceAnalysisDriverOverviewReport_Specified;
end;

procedure PredefinedPeriod8.SetYear(Index: Integer; const AyearType: yearType);
begin
  FYear := AyearType;
  FYear_Specified := True;
end;

function PredefinedPeriod8.Year_Specified(Index: Integer): boolean;
begin
  Result := FYear_Specified;
end;

procedure PredefinedPeriod8.SetInterval(Index: Integer; const Aperiod1Type: period1Type);
begin
  FInterval := Aperiod1Type;
  FInterval_Specified := True;
end;

function PredefinedPeriod8.Interval_Specified(Index: Integer): boolean;
begin
  Result := FInterval_Specified;
end;

procedure PredefinedPeriod8.SetCalendarWeek(Index: Integer; const AcalendarWeekType: calendarWeekType);
begin
  FCalendarWeek := AcalendarWeekType;
  FCalendarWeek_Specified := True;
end;

function PredefinedPeriod8.CalendarWeek_Specified(Index: Integer): boolean;
begin
  Result := FCalendarWeek_Specified;
end;

initialization
  { PerformanceAnalysisServiceInterface }
  InvRegistry.RegisterInterface(TypeInfo(PerformanceAnalysisServiceInterface), 'http://ws.fleetboard.com/PerformanceAnalysisService', 'UTF-8');
  InvRegistry.RegisterAllSOAPActions(TypeInfo(PerformanceAnalysisServiceInterface), '|getMBPerformanceAnalysisVehicle'
                                                                                   +'|getMBPerformanceAnalysisDriver'
                                                                                   +'|getUniversalPerformanceAnalysisVehicle'
                                                                                   +'|getUniversalPerformanceAnalysisDriver'
                                                                                   +'|getUniversalPerformanceAnalysisVehicleTimePeriodOverview'
                                                                                   +'|getUniversalPerformanceAnalysisDriverTimePeriodOverview'
                                                                                   +'|getMBPerformanceAnalysisVehicleTimePeriodOverview'
                                                                                   +'|getMBPerformanceAnalysisDriverTimePeriodOverview'
                                                                                   );
  InvRegistry.RegisterInvokeOptions(TypeInfo(PerformanceAnalysisServiceInterface), ioDocument);
  { PerformanceAnalysisServiceInterface.getMBPerformanceAnalysisVehicle }
  InvRegistry.RegisterMethodInfo(TypeInfo(PerformanceAnalysisServiceInterface), 'getMBPerformanceAnalysisVehicle', '',
                                 '[ReturnName="GetMBPerformanceAnalysisVehicleResponse", RequestNS="http://www.fleetboard.com/data", ResponseNS="http://www.fleetboard.com/data"]', IS_REF);
  InvRegistry.RegisterParamInfo(TypeInfo(PerformanceAnalysisServiceInterface), 'getMBPerformanceAnalysisVehicle', 'GetMBPerformanceAnalysisVehicleRequest', '',
                                '', IS_REF);
  InvRegistry.RegisterParamInfo(TypeInfo(PerformanceAnalysisServiceInterface), 'getMBPerformanceAnalysisVehicle', 'GetMBPerformanceAnalysisVehicleResponse', '',
                                '', IS_REF);
  { PerformanceAnalysisServiceInterface.getMBPerformanceAnalysisDriver }
  InvRegistry.RegisterMethodInfo(TypeInfo(PerformanceAnalysisServiceInterface), 'getMBPerformanceAnalysisDriver', '',
                                 '[ReturnName="GetMBPerformanceAnalysisDriverResponse", RequestNS="http://www.fleetboard.com/data", ResponseNS="http://www.fleetboard.com/data"]', IS_REF);
  InvRegistry.RegisterParamInfo(TypeInfo(PerformanceAnalysisServiceInterface), 'getMBPerformanceAnalysisDriver', 'GetMBPerformanceAnalysisDriverRequest', '',
                                '', IS_REF);
  InvRegistry.RegisterParamInfo(TypeInfo(PerformanceAnalysisServiceInterface), 'getMBPerformanceAnalysisDriver', 'GetMBPerformanceAnalysisDriverResponse', '',
                                '', IS_REF);
  { PerformanceAnalysisServiceInterface.getUniversalPerformanceAnalysisVehicle }
  InvRegistry.RegisterMethodInfo(TypeInfo(PerformanceAnalysisServiceInterface), 'getUniversalPerformanceAnalysisVehicle', '',
                                 '[ReturnName="GetUniversalPerformanceAnalysisVehicleResponse", RequestNS="http://www.fleetboard.com/data", ResponseNS="http://www.fleetboard.com/data"]', IS_REF);
  InvRegistry.RegisterParamInfo(TypeInfo(PerformanceAnalysisServiceInterface), 'getUniversalPerformanceAnalysisVehicle', 'GetUniversalPerformanceAnalysisVehicleRequest', '',
                                '', IS_REF);
  InvRegistry.RegisterParamInfo(TypeInfo(PerformanceAnalysisServiceInterface), 'getUniversalPerformanceAnalysisVehicle', 'GetUniversalPerformanceAnalysisVehicleResponse', '',
                                '', IS_REF);
  { PerformanceAnalysisServiceInterface.getUniversalPerformanceAnalysisDriver }
  InvRegistry.RegisterMethodInfo(TypeInfo(PerformanceAnalysisServiceInterface), 'getUniversalPerformanceAnalysisDriver', '',
                                 '[ReturnName="GetUniversalPerformanceAnalysisDriverResponse", RequestNS="http://www.fleetboard.com/data", ResponseNS="http://www.fleetboard.com/data"]', IS_REF);
  InvRegistry.RegisterParamInfo(TypeInfo(PerformanceAnalysisServiceInterface), 'getUniversalPerformanceAnalysisDriver', 'GetUniversalPerformanceAnalysisDriverRequest', '',
                                '', IS_REF);
  InvRegistry.RegisterParamInfo(TypeInfo(PerformanceAnalysisServiceInterface), 'getUniversalPerformanceAnalysisDriver', 'GetUniversalPerformanceAnalysisDriverResponse', '',
                                '', IS_REF);
  { PerformanceAnalysisServiceInterface.getUniversalPerformanceAnalysisVehicleOverview }
  InvRegistry.RegisterMethodInfo(TypeInfo(PerformanceAnalysisServiceInterface), 'getUniversalPerformanceAnalysisVehicleOverview', '',
                                 '[ReturnName="GetUniversalPerformanceAnalysisVehicleOverviewResponse", RequestNS="http://www.fleetboard.com/data", ResponseNS="http://www.fleetboard.com/data"]', IS_REF);
  InvRegistry.RegisterParamInfo(TypeInfo(PerformanceAnalysisServiceInterface), 'getUniversalPerformanceAnalysisVehicleOverview', 'GetUniversalPerformanceAnalysisVehicleOverviewRequest', '',
                                '', IS_REF);
  InvRegistry.RegisterParamInfo(TypeInfo(PerformanceAnalysisServiceInterface), 'getUniversalPerformanceAnalysisVehicleOverview', 'GetUniversalPerformanceAnalysisVehicleOverviewResponse', '',
                                '', IS_REF);
  { PerformanceAnalysisServiceInterface.getUniversalPerformanceAnalysisDriverOverview }
  InvRegistry.RegisterMethodInfo(TypeInfo(PerformanceAnalysisServiceInterface), 'getUniversalPerformanceAnalysisDriverOverview', '',
                                 '[ReturnName="GetUniversalPerformanceAnalysisDriverOverviewResponse", RequestNS="http://www.fleetboard.com/data", ResponseNS="http://www.fleetboard.com/data"]', IS_REF);
  InvRegistry.RegisterParamInfo(TypeInfo(PerformanceAnalysisServiceInterface), 'getUniversalPerformanceAnalysisDriverOverview', 'GetUniversalPerformanceAnalysisDriverOverviewRequest', '',
                                '', IS_REF);
  InvRegistry.RegisterParamInfo(TypeInfo(PerformanceAnalysisServiceInterface), 'getUniversalPerformanceAnalysisDriverOverview', 'GetUniversalPerformanceAnalysisDriverOverviewResponse', '',
                                '', IS_REF);
  { PerformanceAnalysisServiceInterface.getMBPerformanceAnalysisVehicleOverview }
  InvRegistry.RegisterMethodInfo(TypeInfo(PerformanceAnalysisServiceInterface), 'getMBPerformanceAnalysisVehicleOverview', '',
                                 '[ReturnName="GetMBPerformanceAnalysisVehicleOverviewResponse", RequestNS="http://www.fleetboard.com/data", ResponseNS="http://www.fleetboard.com/data"]', IS_REF);
  InvRegistry.RegisterParamInfo(TypeInfo(PerformanceAnalysisServiceInterface), 'getMBPerformanceAnalysisVehicleOverview', 'GetMBPerformanceAnalysisVehicleOverviewRequest', '',
                                '', IS_REF);
  InvRegistry.RegisterParamInfo(TypeInfo(PerformanceAnalysisServiceInterface), 'getMBPerformanceAnalysisVehicleOverview', 'GetMBPerformanceAnalysisVehicleOverviewResponse', '',
                                '', IS_REF);
  { PerformanceAnalysisServiceInterface.getMBPerformanceAnalysisDriverOverview }
  InvRegistry.RegisterMethodInfo(TypeInfo(PerformanceAnalysisServiceInterface), 'getMBPerformanceAnalysisDriverOverview', '',
                                 '[ReturnName="GetMBPerformanceAnalysisDriverOverviewResponse", RequestNS="http://www.fleetboard.com/data", ResponseNS="http://www.fleetboard.com/data"]', IS_REF);
  InvRegistry.RegisterParamInfo(TypeInfo(PerformanceAnalysisServiceInterface), 'getMBPerformanceAnalysisDriverOverview', 'GetMBPerformanceAnalysisDriverOverviewRequest', '',
                                '', IS_REF);
  InvRegistry.RegisterParamInfo(TypeInfo(PerformanceAnalysisServiceInterface), 'getMBPerformanceAnalysisDriverOverview', 'GetMBPerformanceAnalysisDriverOverviewResponse', '',
                                '', IS_REF);
  RemClassRegistry.RegisterXSClass(ToursSummary, 'http://www.fleetboard.com/data', 'ToursSummary');
  RemClassRegistry.RegisterXSClass(ToursSummary2, 'http://www.fleetboard.com/data', 'ToursSummary2', 'ToursSummary');
  RemClassRegistry.RegisterXSInfo(TypeInfo(Array_Of_MBPerformanceAnalysisVehicleOverviewReport), 'http://www.fleetboard.com/data', 'Array_Of_MBPerformanceAnalysisVehicleOverviewReport');
  RemClassRegistry.RegisterXSInfo(TypeInfo(Array_Of_Cells), 'http://www.fleetboard.com/data', 'Array_Of_Cells');
  RemClassRegistry.RegisterXSInfo(TypeInfo(Array_Of_MBPerformanceAnalysisDriverOverviewReport), 'http://www.fleetboard.com/data', 'Array_Of_MBPerformanceAnalysisDriverOverviewReport');
  RemClassRegistry.RegisterXSInfo(TypeInfo(Array_Of_MBPerformanceAnalysisVehicleReport), 'http://www.fleetboard.com/data', 'Array_Of_MBPerformanceAnalysisVehicleReport');
  RemClassRegistry.RegisterXSInfo(TypeInfo(Array_Of_barChartType), 'http://www.fleetboard.com/data', 'Array_Of_barChartType');
  RemClassRegistry.RegisterXSInfo(TypeInfo(Array_Of_engineChartType), 'http://www.fleetboard.com/data', 'Array_Of_engineChartType');
  RemClassRegistry.RegisterXSInfo(TypeInfo(Array_Of_Rows), 'http://www.fleetboard.com/data', 'Array_Of_Rows');
  RemClassRegistry.RegisterXSInfo(TypeInfo(Array_Of_Values), 'http://www.fleetboard.com/data', 'Array_Of_Values');
  RemClassRegistry.RegisterXSInfo(TypeInfo(Array_Of_VehicleDataSet), 'http://www.fleetboard.com/data', 'Array_Of_VehicleDataSet');
  RemClassRegistry.RegisterXSInfo(TypeInfo(Array_Of_UniversalPerformanceAnalysisVehicleReport), 'http://www.fleetboard.com/data', 'Array_Of_UniversalPerformanceAnalysisVehicleReport');
  RemClassRegistry.RegisterXSInfo(TypeInfo(Array_Of_VehicleDataSet2), 'http://www.fleetboard.com/data', 'Array_Of_VehicleDataSet2', 'Array_Of_VehicleDataSet');
  RemClassRegistry.RegisterXSInfo(TypeInfo(Array_Of_UniversalPerformanceAnalysisDriverReport), 'http://www.fleetboard.com/data', 'Array_Of_UniversalPerformanceAnalysisDriverReport');
  RemClassRegistry.RegisterXSInfo(TypeInfo(Array_Of_DriverGroup), 'http://www.fleetboard.com/data', 'Array_Of_DriverGroup');
  RemClassRegistry.RegisterXSInfo(TypeInfo(Array_Of_UniversalPerformanceAnalysisDriverOverviewReport), 'http://www.fleetboard.com/data', 'Array_Of_UniversalPerformanceAnalysisDriverOverviewReport');
  RemClassRegistry.RegisterXSInfo(TypeInfo(Array_Of_UniversalPerformanceAnalysisVehicleOverviewReport), 'http://www.fleetboard.com/data', 'Array_Of_UniversalPerformanceAnalysisVehicleOverviewReport');
  RemClassRegistry.RegisterXSInfo(TypeInfo(Array_Of_MBPerformanceAnalysisDriverReport), 'http://www.fleetboard.com/data', 'Array_Of_MBPerformanceAnalysisDriverReport');
  RemClassRegistry.RegisterXSInfo(TypeInfo(Array_Of_VehicleGroup), 'http://www.fleetboard.com/data', 'Array_Of_VehicleGroup');
  RemClassRegistry.RegisterXSInfo(TypeInfo(ContainsVehicleDataSetList2), 'http://www.fleetboard.com/data', 'ContainsVehicleDataSetList2', 'ContainsVehicleDataSetList');
  RemClassRegistry.RegisterXSInfo(TypeInfo(ContainsVehicleDataSetList), 'http://www.fleetboard.com/data', 'ContainsVehicleDataSetList');
  RemClassRegistry.RegisterXSInfo(TypeInfo(distanceType), 'http://www.fleetboard.com/data', 'distanceType');
  RemClassRegistry.RegisterXSClass(kickDown, 'http://www.fleetboard.com/data', 'kickDown');
  RemClassRegistry.RegisterXSClass(ecoRoll, 'http://www.fleetboard.com/data', 'ecoRoll');
  RemClassRegistry.RegisterXSClass(cruiseControlModes, 'http://www.fleetboard.com/data', 'cruiseControlModes');
  RemClassRegistry.RegisterXSClass(assistanceSystems, 'http://www.fleetboard.com/data', 'assistanceSystems');
  RemClassRegistry.RegisterXSClass(drivingPrograms, 'http://www.fleetboard.com/data', 'drivingPrograms');
  RemClassRegistry.RegisterXSInfo(TypeInfo(versionType), 'http://www.fleetboard.com/data', 'versionType');
  RemClassRegistry.RegisterXSInfo(TypeInfo(offsetType), 'http://www.fleetboard.com/data', 'offsetType');
  RemClassRegistry.RegisterXSInfo(TypeInfo(limitType), 'http://www.fleetboard.com/data', 'limitType');
  RemClassRegistry.RegisterXSInfo(TypeInfo(timeType), 'http://www.fleetboard.com/data', 'timeType');
  RemClassRegistry.RegisterXSClass(PowerTakeOff, 'http://www.fleetboard.com/data', 'PowerTakeOff');
  RemClassRegistry.RegisterXSClass(PowerTakeOff2, 'http://www.fleetboard.com/data', 'PowerTakeOff2', 'PowerTakeOff');
  RemClassRegistry.RegisterXSClass(cruiseCtrlOff, 'http://www.fleetboard.com/data', 'cruiseCtrlOff');
  RemClassRegistry.RegisterXSClass(cruiseCtrlWithoutPPC, 'http://www.fleetboard.com/data', 'cruiseCtrlWithoutPPC');
  RemClassRegistry.RegisterXSClass(cruiseCtrlWithPPC, 'http://www.fleetboard.com/data', 'cruiseCtrlWithPPC');
  RemClassRegistry.RegisterXSClass(VehicleStateTimeValuesType, 'http://www.fleetboard.com/data', 'VehicleStateTimeValuesType');
  RemClassRegistry.RegisterXSClass(limiter, 'http://www.fleetboard.com/data', 'limiter');
  RemClassRegistry.RegisterXSClass(Cells, 'http://www.fleetboard.com/data', 'Cells');
  RemClassRegistry.RegisterXSInfo(TypeInfo(ratioType), 'http://www.fleetboard.com/data', 'ratioType');
  RemClassRegistry.RegisterXSInfo(TypeInfo(weightType), 'http://www.fleetboard.com/data', 'weightType');
  RemClassRegistry.RegisterXSClass(MinMaxWeightType, 'http://www.fleetboard.com/data', 'MinMaxWeightType');
  RemClassRegistry.RegisterXSInfo(TypeInfo(consumptionType), 'http://www.fleetboard.com/data', 'consumptionType');
  RemClassRegistry.RegisterXSClass(ConsumptionValuesType, 'http://www.fleetboard.com/data', 'ConsumptionValuesType');
  RemClassRegistry.RegisterXSClass(Consumption, 'http://www.fleetboard.com/data', 'Consumption');
  RemClassRegistry.RegisterXSClass(Consumption2, 'http://www.fleetboard.com/data', 'Consumption2', 'Consumption');
  RemClassRegistry.RegisterXSClass(Consumption3, 'http://www.fleetboard.com/data', 'Consumption3', 'Consumption');
  RemClassRegistry.RegisterXSClass(Consumption4, 'http://www.fleetboard.com/data', 'Consumption4', 'Consumption');
  RemClassRegistry.RegisterXSClass(Consumption5, 'http://www.fleetboard.com/data', 'Consumption5', 'Consumption');
  RemClassRegistry.RegisterXSInfo(TypeInfo(speedType), 'http://www.fleetboard.com/data', 'speedType');
  RemClassRegistry.RegisterXSClass(ppc, 'http://www.fleetboard.com/data', 'ppc');
  RemClassRegistry.RegisterXSClass(AvgValues, 'http://www.fleetboard.com/data', 'AvgValues');
  RemClassRegistry.RegisterXSClass(AvgValues2, 'http://www.fleetboard.com/data', 'AvgValues2', 'AvgValues');
  RemClassRegistry.RegisterXSClass(AvgValues3, 'http://www.fleetboard.com/data', 'AvgValues3', 'AvgValues');
  RemClassRegistry.RegisterXSClass(AvgValues4, 'http://www.fleetboard.com/data', 'AvgValues4', 'AvgValues');
  RemClassRegistry.RegisterXSClass(AvgValues5, 'http://www.fleetboard.com/data', 'AvgValues5', 'AvgValues');
  RemClassRegistry.RegisterXSClass(MinMaxSpeedType, 'http://www.fleetboard.com/data', 'MinMaxSpeedType');
  RemClassRegistry.RegisterXSClass(MinMaxConsumptionType, 'http://www.fleetboard.com/data', 'MinMaxConsumptionType');
  RemClassRegistry.RegisterXSInfo(TypeInfo(gradeType), 'http://www.fleetboard.com/data', 'gradeType');
  RemClassRegistry.RegisterXSClass(Grades, 'http://www.fleetboard.com/data', 'Grades');
  RemClassRegistry.RegisterXSClass(Grades2, 'http://www.fleetboard.com/data', 'Grades2', 'Grades');
  RemClassRegistry.RegisterXSClass(Grades3, 'http://www.fleetboard.com/data', 'Grades3', 'Grades');
  RemClassRegistry.RegisterXSClass(Grades4, 'http://www.fleetboard.com/data', 'Grades4', 'Grades');
  RemClassRegistry.RegisterXSClass(Grades5, 'http://www.fleetboard.com/data', 'Grades5', 'Grades');
  RemClassRegistry.RegisterXSClass(Grades6, 'http://www.fleetboard.com/data', 'Grades6', 'Grades');
  RemClassRegistry.RegisterXSClass(Grades7, 'http://www.fleetboard.com/data', 'Grades7', 'Grades');
  RemClassRegistry.RegisterXSClass(MinMaxGradeType, 'http://www.fleetboard.com/data', 'MinMaxGradeType');
  RemClassRegistry.RegisterXSInfo(TypeInfo(Model), 'http://www.fleetboard.com/data', 'Model');
  RemClassRegistry.RegisterXSClass(TimeRangeType, 'http://www.fleetboard.com/data', 'TimeRangeType');
  RemClassRegistry.RegisterXSInfo(TypeInfo(vehicleGroupIDType), 'http://www.fleetboard.com/data', 'vehicleGroupIDType');
  RemClassRegistry.RegisterXSClass(Values, 'http://www.fleetboard.com/data', 'Values');
  RemClassRegistry.RegisterXSInfo(TypeInfo(driverGroupIDType), 'http://www.fleetboard.com/data', 'driverGroupIDType');
  RemClassRegistry.RegisterXSInfo(TypeInfo(period1Type), 'http://www.fleetboard.com/data', 'period1Type');
  RemClassRegistry.RegisterXSClass(engineChartType, 'http://www.fleetboard.com/data', 'engineChartType');
  RemClassRegistry.RegisterXSClass(VehicleGroup, 'http://www.fleetboard.com/data', 'VehicleGroup');
  RemClassRegistry.RegisterXSClass(DriverGroup, 'http://www.fleetboard.com/data', 'DriverGroup');
  RemClassRegistry.RegisterXSInfo(TypeInfo(yearType), 'http://www.fleetboard.com/data', 'yearType');
  RemClassRegistry.RegisterXSInfo(TypeInfo(timestampType), 'http://www.fleetboard.com/data', 'timestampType');
  RemClassRegistry.RegisterXSClass(TPDate, 'http://www.fleetboard.com/data', 'TPDate');
  RemClassRegistry.RegisterExternalPropName(TypeInfo(TPDate), 'Begin_', '[ExtName="Begin"]');
  RemClassRegistry.RegisterExternalPropName(TypeInfo(TPDate), 'End_', '[ExtName="End"]');
  RemClassRegistry.RegisterXSInfo(TypeInfo(sessionidType), 'http://www.fleetboard.com/data', 'sessionidType');
  RemClassRegistry.RegisterXSInfo(TypeInfo(unitType), 'http://www.fleetboard.com/data', 'unitType');
  RemClassRegistry.RegisterXSClass(X, 'http://www.fleetboard.com/data', 'X');
  RemClassRegistry.RegisterExternalPropName(TypeInfo(X), 'unit_', '[ExtName="unit"]');
  RemClassRegistry.RegisterXSClass(Rows, 'http://www.fleetboard.com/data', 'Rows');
  RemClassRegistry.RegisterExternalPropName(TypeInfo(Rows), 'name_', '[ExtName="name"]');
  RemClassRegistry.RegisterExternalPropName(TypeInfo(Rows), 'unit_', '[ExtName="unit"]');
  RemClassRegistry.RegisterXSClass(Y1, 'http://www.fleetboard.com/data', 'Y1');
  RemClassRegistry.RegisterExternalPropName(TypeInfo(Y1), 'unit_', '[ExtName="unit"]');
  RemClassRegistry.RegisterXSClass(Y2, 'http://www.fleetboard.com/data', 'Y2');
  RemClassRegistry.RegisterExternalPropName(TypeInfo(Y2), 'unit_', '[ExtName="unit"]');
  RemClassRegistry.RegisterXSClass(Period, 'http://www.fleetboard.com/data', 'Period');
  RemClassRegistry.RegisterExternalPropName(TypeInfo(Period), 'Begin_', '[ExtName="Begin"]');
  RemClassRegistry.RegisterExternalPropName(TypeInfo(Period), 'End_', '[ExtName="End"]');
  RemClassRegistry.RegisterXSClass(Period2, 'http://www.fleetboard.com/data', 'Period2', 'Period');
  RemClassRegistry.RegisterExternalPropName(TypeInfo(Period2), 'Begin_', '[ExtName="Begin"]');
  RemClassRegistry.RegisterExternalPropName(TypeInfo(Period2), 'End_', '[ExtName="End"]');
  RemClassRegistry.RegisterXSInfo(TypeInfo(Array_Of_string), 'http://www.w3.org/2001/XMLSchema', 'Array_Of_string');
  RemClassRegistry.RegisterXSClass(Period3, 'http://www.fleetboard.com/data', 'Period3', 'Period');
  RemClassRegistry.RegisterExternalPropName(TypeInfo(Period3), 'Begin_', '[ExtName="Begin"]');
  RemClassRegistry.RegisterExternalPropName(TypeInfo(Period3), 'End_', '[ExtName="End"]');
  RemClassRegistry.RegisterXSClass(Period4, 'http://www.fleetboard.com/data', 'Period4', 'Period');
  RemClassRegistry.RegisterExternalPropName(TypeInfo(Period4), 'Begin_', '[ExtName="Begin"]');
  RemClassRegistry.RegisterExternalPropName(TypeInfo(Period4), 'End_', '[ExtName="End"]');
  RemClassRegistry.RegisterXSClass(Period5, 'http://www.fleetboard.com/data', 'Period5', 'Period');
  RemClassRegistry.RegisterExternalPropName(TypeInfo(Period5), 'Begin_', '[ExtName="Begin"]');
  RemClassRegistry.RegisterExternalPropName(TypeInfo(Period5), 'End_', '[ExtName="End"]');
  RemClassRegistry.RegisterXSClass(Period6, 'http://www.fleetboard.com/data', 'Period6', 'Period');
  RemClassRegistry.RegisterExternalPropName(TypeInfo(Period6), 'Begin_', '[ExtName="Begin"]');
  RemClassRegistry.RegisterExternalPropName(TypeInfo(Period6), 'End_', '[ExtName="End"]');
  RemClassRegistry.RegisterXSClass(Period7, 'http://www.fleetboard.com/data', 'Period7', 'Period');
  RemClassRegistry.RegisterExternalPropName(TypeInfo(Period7), 'Begin_', '[ExtName="Begin"]');
  RemClassRegistry.RegisterExternalPropName(TypeInfo(Period7), 'End_', '[ExtName="End"]');
  RemClassRegistry.RegisterXSInfo(TypeInfo(period2Type), 'http://www.fleetboard.com/data', 'period2Type');
  RemClassRegistry.RegisterXSClass(PeriodBreakDown, 'http://www.fleetboard.com/data', 'PeriodBreakDown');
  RemClassRegistry.RegisterXSClass(PeriodBreakDown2, 'http://www.fleetboard.com/data', 'PeriodBreakDown2', 'PeriodBreakDown');
  RemClassRegistry.RegisterXSClass(barChartType, 'http://www.fleetboard.com/data', 'barChartType');
  RemClassRegistry.RegisterXSClass(ChartsType, 'http://www.fleetboard.com/data', 'ChartsType');
  RemClassRegistry.RegisterXSInfo(TypeInfo(calendarWeekType), 'http://www.fleetboard.com/data', 'calendarWeekType');
  RemClassRegistry.RegisterXSClass(Period3Type, 'http://www.fleetboard.com/data', 'Period3Type');
  RemClassRegistry.RegisterXSClass(PredefinedPeriod, 'http://www.fleetboard.com/data', 'PredefinedPeriod');
  RemClassRegistry.RegisterXSClass(PredefinedPeriod2, 'http://www.fleetboard.com/data', 'PredefinedPeriod2', 'PredefinedPeriod');
  RemClassRegistry.RegisterXSClass(PredefinedPeriod3, 'http://www.fleetboard.com/data', 'PredefinedPeriod3', 'PredefinedPeriod');
  RemClassRegistry.RegisterXSInfo(TypeInfo(vehicleidType), 'http://www.fleetboard.com/data', 'vehicleidType');
  RemClassRegistry.RegisterXSInfo(TypeInfo(Array_Of_vehicleidType), 'http://www.fleetboard.com/data', 'Array_Of_vehicleidType');
  RemClassRegistry.RegisterXSClass(VehicleDataSet, 'http://www.fleetboard.com/data', 'VehicleDataSet');
  RemClassRegistry.RegisterXSClass(VehicleDataSet2, 'http://www.fleetboard.com/data', 'VehicleDataSet2', 'VehicleDataSet');
  RemClassRegistry.RegisterXSClass(PowerTakeOff3, 'http://www.fleetboard.com/data', 'PowerTakeOff3', 'PowerTakeOff');
  RemClassRegistry.RegisterXSClass(Consumption6, 'http://www.fleetboard.com/data', 'Consumption6', 'Consumption');
  RemClassRegistry.RegisterXSClass(PeriodBreakDown3, 'http://www.fleetboard.com/data', 'PeriodBreakDown3', 'PeriodBreakDown');
  RemClassRegistry.RegisterXSClass(ToursSummary3, 'http://www.fleetboard.com/data', 'ToursSummary3', 'ToursSummary');
  RemClassRegistry.RegisterXSClass(AvgValues6, 'http://www.fleetboard.com/data', 'AvgValues6', 'AvgValues');
  RemClassRegistry.RegisterXSClass(PredefinedPeriod4, 'http://www.fleetboard.com/data', 'PredefinedPeriod4', 'PredefinedPeriod');
  RemClassRegistry.RegisterXSClass(PeriodBreakDown4, 'http://www.fleetboard.com/data', 'PeriodBreakDown4', 'PeriodBreakDown');
  RemClassRegistry.RegisterXSClass(Grades8, 'http://www.fleetboard.com/data', 'Grades8', 'Grades');
  RemClassRegistry.RegisterXSClass(Period8, 'http://www.fleetboard.com/data', 'Period8', 'Period');
  RemClassRegistry.RegisterExternalPropName(TypeInfo(Period8), 'Begin_', '[ExtName="Begin"]');
  RemClassRegistry.RegisterExternalPropName(TypeInfo(Period8), 'End_', '[ExtName="End"]');
  RemClassRegistry.RegisterXSClass(PredefinedPeriod5, 'http://www.fleetboard.com/data', 'PredefinedPeriod5', 'PredefinedPeriod');
  RemClassRegistry.RegisterXSClass(UniversalPerformanceAnalysisVehicleOverviewReport, 'http://www.fleetboard.com/data', 'UniversalPerformanceAnalysisVehicleOverviewReport');
  RemClassRegistry.RegisterXSClass(PeriodBreakDown5, 'http://www.fleetboard.com/data', 'PeriodBreakDown5', 'PeriodBreakDown');
  RemClassRegistry.RegisterXSClass(ToursSummary4, 'http://www.fleetboard.com/data', 'ToursSummary4', 'ToursSummary');
  RemClassRegistry.RegisterXSClass(PredefinedPeriod6, 'http://www.fleetboard.com/data', 'PredefinedPeriod6', 'PredefinedPeriod');
  RemClassRegistry.RegisterXSClass(GetUniversalPerformanceAnalysisVehicleOverviewRequestType, 'http://www.fleetboard.com/data', 'GetUniversalPerformanceAnalysisVehicleOverviewRequestType');
  RemClassRegistry.RegisterXSClass(GetUniversalPerformanceAnalysisVehicleOverviewRequest, 'http://www.fleetboard.com/data', 'GetUniversalPerformanceAnalysisVehicleOverviewRequest');
  RemClassRegistry.RegisterXSClass(PeriodBreakDown6, 'http://www.fleetboard.com/data', 'PeriodBreakDown6', 'PeriodBreakDown');
  RemClassRegistry.RegisterXSClass(ToursSummary5, 'http://www.fleetboard.com/data', 'ToursSummary5', 'ToursSummary');
  RemClassRegistry.RegisterXSClass(ToursSummary6, 'http://www.fleetboard.com/data', 'ToursSummary6', 'ToursSummary');
  RemClassRegistry.RegisterXSClass(MBPerformanceAnalysisVehicleOverviewReport, 'http://www.fleetboard.com/data', 'MBPerformanceAnalysisVehicleOverviewReport');
  RemClassRegistry.RegisterXSInfo(TypeInfo(responseSizeType), 'http://www.fleetboard.com/data', 'responseSizeType');
  RemClassRegistry.RegisterXSInfo(TypeInfo(resultSizeType), 'http://www.fleetboard.com/data', 'resultSizeType');
  RemClassRegistry.RegisterXSClass(GetUniversalPerformanceAnalysisDriverOverviewResponseType, 'http://www.fleetboard.com/data', 'GetUniversalPerformanceAnalysisDriverOverviewResponseType');
  RemClassRegistry.RegisterXSClass(GetUniversalPerformanceAnalysisDriverOverviewResponse, 'http://www.fleetboard.com/data', 'GetUniversalPerformanceAnalysisDriverOverviewResponse');
  RemClassRegistry.RegisterXSClass(GetUniversalPerformanceAnalysisVehicleOverviewResponseType, 'http://www.fleetboard.com/data', 'GetUniversalPerformanceAnalysisVehicleOverviewResponseType');
  RemClassRegistry.RegisterXSClass(GetUniversalPerformanceAnalysisVehicleOverviewResponse, 'http://www.fleetboard.com/data', 'GetUniversalPerformanceAnalysisVehicleOverviewResponse');
  RemClassRegistry.RegisterXSClass(GetMBPerformanceAnalysisVehicleResponseType, 'http://www.fleetboard.com/data', 'GetMBPerformanceAnalysisVehicleResponseType');
  RemClassRegistry.RegisterXSClass(GetMBPerformanceAnalysisVehicleResponse, 'http://www.fleetboard.com/data', 'GetMBPerformanceAnalysisVehicleResponse');
  RemClassRegistry.RegisterXSClass(GetUniversalPerformanceAnalysisVehicleResponseType, 'http://www.fleetboard.com/data', 'GetUniversalPerformanceAnalysisVehicleResponseType');
  RemClassRegistry.RegisterXSClass(GetUniversalPerformanceAnalysisVehicleResponse, 'http://www.fleetboard.com/data', 'GetUniversalPerformanceAnalysisVehicleResponse');
  RemClassRegistry.RegisterXSClass(GetMBPerformanceAnalysisDriverResponseType, 'http://www.fleetboard.com/data', 'GetMBPerformanceAnalysisDriverResponseType');
  RemClassRegistry.RegisterXSClass(GetMBPerformanceAnalysisDriverResponse, 'http://www.fleetboard.com/data', 'GetMBPerformanceAnalysisDriverResponse');
  RemClassRegistry.RegisterXSClass(GetUniversalPerformanceAnalysisDriverResponseType, 'http://www.fleetboard.com/data', 'GetUniversalPerformanceAnalysisDriverResponseType');
  RemClassRegistry.RegisterXSClass(GetUniversalPerformanceAnalysisDriverResponse, 'http://www.fleetboard.com/data', 'GetUniversalPerformanceAnalysisDriverResponse');
  RemClassRegistry.RegisterXSClass(AvgValues7, 'http://www.fleetboard.com/data', 'AvgValues7', 'AvgValues');
  RemClassRegistry.RegisterXSClass(Grades9, 'http://www.fleetboard.com/data', 'Grades9', 'Grades');
  RemClassRegistry.RegisterXSClass(Period9, 'http://www.fleetboard.com/data', 'Period9', 'Period');
  RemClassRegistry.RegisterExternalPropName(TypeInfo(Period9), 'Begin_', '[ExtName="Begin"]');
  RemClassRegistry.RegisterExternalPropName(TypeInfo(Period9), 'End_', '[ExtName="End"]');
  RemClassRegistry.RegisterXSClass(GetMBPerformanceAnalysisVehicleOverviewResponseType, 'http://www.fleetboard.com/data', 'GetMBPerformanceAnalysisVehicleOverviewResponseType');
  RemClassRegistry.RegisterXSClass(GetMBPerformanceAnalysisVehicleOverviewResponse, 'http://www.fleetboard.com/data', 'GetMBPerformanceAnalysisVehicleOverviewResponse');
  RemClassRegistry.RegisterXSInfo(TypeInfo(analysisModelType), 'http://www.fleetboard.com/data', 'analysisModelType');
  RemClassRegistry.RegisterXSClass(Analysis, 'http://www.fleetboard.com/data', 'Analysis');
  RemClassRegistry.RegisterXSClass(Analysis2, 'http://www.fleetboard.com/data', 'Analysis2', 'Analysis');
  RemClassRegistry.RegisterXSClass(Analysis3, 'http://www.fleetboard.com/data', 'Analysis3', 'Analysis');
  RemClassRegistry.RegisterXSClass(Analysis4, 'http://www.fleetboard.com/data', 'Analysis4', 'Analysis');
  RemClassRegistry.RegisterXSClass(Analysis5, 'http://www.fleetboard.com/data', 'Analysis5', 'Analysis');
  RemClassRegistry.RegisterXSClass(Analysis6, 'http://www.fleetboard.com/data', 'Analysis6', 'Analysis');
  RemClassRegistry.RegisterXSInfo(TypeInfo(telematicGroupIDType), 'http://www.fleetboard.com/data', 'telematicGroupIDType');
  RemClassRegistry.RegisterXSClass(GetMBPerformanceAnalysisVehicleOverviewRequestType, 'http://www.fleetboard.com/data', 'GetMBPerformanceAnalysisVehicleOverviewRequestType');
  RemClassRegistry.RegisterXSClass(GetMBPerformanceAnalysisVehicleOverviewRequest, 'http://www.fleetboard.com/data', 'GetMBPerformanceAnalysisVehicleOverviewRequest');
  RemClassRegistry.RegisterXSClass(PredefinedPeriod7, 'http://www.fleetboard.com/data', 'PredefinedPeriod7', 'PredefinedPeriod');
  RemClassRegistry.RegisterXSClass(PeriodBreakDown7, 'http://www.fleetboard.com/data', 'PeriodBreakDown7', 'PeriodBreakDown');
  RemClassRegistry.RegisterXSClass(ToursSummary7, 'http://www.fleetboard.com/data', 'ToursSummary7', 'ToursSummary');
  RemClassRegistry.RegisterXSClass(AvgValues8, 'http://www.fleetboard.com/data', 'AvgValues8', 'AvgValues');
  RemClassRegistry.RegisterXSClass(Grades10, 'http://www.fleetboard.com/data', 'Grades10', 'Grades');
  RemClassRegistry.RegisterXSClass(Period10, 'http://www.fleetboard.com/data', 'Period10', 'Period');
  RemClassRegistry.RegisterExternalPropName(TypeInfo(Period10), 'Begin_', '[ExtName="Begin"]');
  RemClassRegistry.RegisterExternalPropName(TypeInfo(Period10), 'End_', '[ExtName="End"]');
  RemClassRegistry.RegisterXSClass(Analysis7, 'http://www.fleetboard.com/data', 'Analysis7', 'Analysis');
  RemClassRegistry.RegisterXSInfo(TypeInfo(getChartsType), 'http://www.fleetboard.com/data', 'getChartsType');
  RemClassRegistry.RegisterXSClass(GetUniversalPerformanceAnalysisVehicleRequestType, 'http://www.fleetboard.com/data', 'GetUniversalPerformanceAnalysisVehicleRequestType');
  RemClassRegistry.RegisterXSClass(GetUniversalPerformanceAnalysisVehicleRequest, 'http://www.fleetboard.com/data', 'GetUniversalPerformanceAnalysisVehicleRequest');
  RemClassRegistry.RegisterXSClass(GetMBPerformanceAnalysisVehicleRequestType, 'http://www.fleetboard.com/data', 'GetMBPerformanceAnalysisVehicleRequestType');
  RemClassRegistry.RegisterXSClass(GetMBPerformanceAnalysisVehicleRequest, 'http://www.fleetboard.com/data', 'GetMBPerformanceAnalysisVehicleRequest');
  RemClassRegistry.RegisterXSClass(ToursSummary8, 'http://www.fleetboard.com/data', 'ToursSummary8', 'ToursSummary');
  RemClassRegistry.RegisterXSClass(Analysis8, 'http://www.fleetboard.com/data', 'Analysis8', 'Analysis');
  RemClassRegistry.RegisterXSInfo(TypeInfo(driverNameIDType), 'http://www.fleetboard.com/data', 'driverNameIDType');
  RemClassRegistry.RegisterXSClass(MBPerformanceAnalysisDriverOverviewReport, 'http://www.fleetboard.com/data', 'MBPerformanceAnalysisDriverOverviewReport');
  RemClassRegistry.RegisterXSClass(GetMBPerformanceAnalysisDriverOverviewRequestType, 'http://www.fleetboard.com/data', 'GetMBPerformanceAnalysisDriverOverviewRequestType');
  RemClassRegistry.RegisterXSClass(GetMBPerformanceAnalysisDriverOverviewRequest, 'http://www.fleetboard.com/data', 'GetMBPerformanceAnalysisDriverOverviewRequest');
  RemClassRegistry.RegisterXSClass(UniversalPerformanceAnalysisDriverOverviewReport, 'http://www.fleetboard.com/data', 'UniversalPerformanceAnalysisDriverOverviewReport');
  RemClassRegistry.RegisterXSClass(GetMBPerformanceAnalysisDriverRequestType, 'http://www.fleetboard.com/data', 'GetMBPerformanceAnalysisDriverRequestType');
  RemClassRegistry.RegisterXSClass(GetMBPerformanceAnalysisDriverRequest, 'http://www.fleetboard.com/data', 'GetMBPerformanceAnalysisDriverRequest');
  RemClassRegistry.RegisterXSClass(GetUniversalPerformanceAnalysisDriverOverviewRequestType, 'http://www.fleetboard.com/data', 'GetUniversalPerformanceAnalysisDriverOverviewRequestType');
  RemClassRegistry.RegisterXSClass(GetUniversalPerformanceAnalysisDriverOverviewRequest, 'http://www.fleetboard.com/data', 'GetUniversalPerformanceAnalysisDriverOverviewRequest');
  RemClassRegistry.RegisterXSInfo(TypeInfo(Array_Of_driverNameIDType), 'http://www.fleetboard.com/data', 'Array_Of_driverNameIDType');
  RemClassRegistry.RegisterXSClass(MBPerformanceAnalysisVehicleReport, 'http://www.fleetboard.com/data', 'MBPerformanceAnalysisVehicleReport');
  RemClassRegistry.RegisterXSClass(UniversalPerformanceAnalysisVehicleReport, 'http://www.fleetboard.com/data', 'UniversalPerformanceAnalysisVehicleReport');
  RemClassRegistry.RegisterXSClass(GetUniversalPerformanceAnalysisDriverRequestType, 'http://www.fleetboard.com/data', 'GetUniversalPerformanceAnalysisDriverRequestType');
  RemClassRegistry.RegisterXSClass(GetUniversalPerformanceAnalysisDriverRequest, 'http://www.fleetboard.com/data', 'GetUniversalPerformanceAnalysisDriverRequest');
  RemClassRegistry.RegisterXSClass(MBPerformanceAnalysisDriverReport, 'http://www.fleetboard.com/data', 'MBPerformanceAnalysisDriverReport');
  RemClassRegistry.RegisterExternalPropName(TypeInfo(MBPerformanceAnalysisDriverReport), 'ContainsVehicleDataSetList', '[Namespace="http://www.fleetboard.com/data"]');
  RemClassRegistry.RegisterXSClass(UniversalPerformanceAnalysisDriverReport, 'http://www.fleetboard.com/data', 'UniversalPerformanceAnalysisDriverReport');
  RemClassRegistry.RegisterExternalPropName(TypeInfo(UniversalPerformanceAnalysisDriverReport), 'ContainsVehicleDataSetList', '[Namespace="http://www.fleetboard.com/data"]');
  RemClassRegistry.RegisterXSClass(GetMBPerformanceAnalysisDriverOverviewResponseType, 'http://www.fleetboard.com/data', 'GetMBPerformanceAnalysisDriverOverviewResponseType');
  RemClassRegistry.RegisterXSClass(GetMBPerformanceAnalysisDriverOverviewResponse, 'http://www.fleetboard.com/data', 'GetMBPerformanceAnalysisDriverOverviewResponse');
  RemClassRegistry.RegisterXSClass(PredefinedPeriod8, 'http://www.fleetboard.com/data', 'PredefinedPeriod8', 'PredefinedPeriod');
  RemClassRegistry.RegisterXSClass(PeriodBreakDown8, 'http://www.fleetboard.com/data', 'PeriodBreakDown8', 'PeriodBreakDown');

end.