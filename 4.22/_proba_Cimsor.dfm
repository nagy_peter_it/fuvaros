object FlowPanelDlg: TFlowPanelDlg
  Left = 0
  Top = 0
  Caption = 'Flowpanel demo'
  ClientHeight = 559
  ClientWidth = 864
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object FlowPanel1: TFlowPanel
    Left = 48
    Top = 232
    Width = 697
    Height = 57
    BevelOuter = bvLowered
    TabOrder = 0
    object GridPanel1: TGridPanel
      Left = 1
      Top = 1
      Width = 216
      Height = 32
      ColumnCollection = <
        item
          Value = 70.059880239520960000
        end
        item
          Value = 29.940119760479040000
        end>
      ControlCollection = <
        item
          Column = 0
          Control = Label1
          Row = 0
        end
        item
          Column = 1
          Control = Button3
          Row = 0
        end>
      RowCollection = <
        item
          Value = 100.000000000000000000
        end>
      TabOrder = 0
      DesignSize = (
        216
        32)
      object Label1: TLabel
        Left = 1
        Top = 9
        Width = 149
        Height = 13
        Anchors = []
        Caption = 'Andreas Vollenweider <kakukk@js.hu>'
      end
      object Button3: TButton
        Left = 150
        Top = 7
        Width = 65
        Height = 25
        Anchors = []
        Caption = 'x'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Pitch = fpVariable
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 0
        ExplicitTop = 3
      end
    end
  end
  object FlowPanel2: TFlowPanel
    Left = 48
    Top = 320
    Width = 697
    Height = 57
    BevelOuter = bvLowered
    TabOrder = 1
    DesignSize = (
      697
      57)
    object Label2: TLabel
      Left = 1
      Top = 1
      Width = 191
      Height = 13
      Anchors = []
      Caption = ' Andreas Vollenweider <kakukk@js.hu>'
    end
    object Button4: TButton
      Left = 192
      Top = 1
      Width = 15
      Height = 15
      Caption = 'x'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Pitch = fpVariable
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
    end
    object Label3: TLabel
      Left = 207
      Top = 1
      Width = 12
      Height = 13
      Anchors = []
      Caption = '    '
    end
    object Label4: TLabel
      Left = 219
      Top = 1
      Width = 177
      Height = 13
      Anchors = []
      Caption = 'Herbert Straumlebig <eltern@js.hu>'
    end
    object Button5: TButton
      Left = 396
      Top = 1
      Width = 15
      Height = 15
      Caption = 'x'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Pitch = fpVariable
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
    end
    object Label5: TLabel
      Left = 411
      Top = 1
      Width = 15
      Height = 13
      Anchors = []
      Caption = '     '
    end
    object Label9: TLabel
      Left = 426
      Top = 1
      Width = 256
      Height = 13
      Anchors = []
      Caption = 'Herbert Karl Friedrich Jo Straumlebig <eltern@js.hu>'
    end
    object Button9: TButton
      Left = 1
      Top = 16
      Width = 15
      Height = 15
      Caption = 'x'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Pitch = fpVariable
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 2
    end
    object Label10: TLabel
      Left = 16
      Top = 16
      Width = 15
      Height = 13
      Anchors = []
      Caption = '     '
    end
  end
  object FlowPanel3: TFlowPanel
    Left = 50
    Top = 402
    Width = 696
    Height = 95
    BevelOuter = bvSpace
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    object FlowPanel8: TFlowPanel
      Left = 1
      Top = 1
      Width = 344
      Height = 18
      Alignment = taLeftJustify
      AutoSize = True
      AutoWrap = False
      BevelEdges = [beBottom]
      BevelOuter = bvNone
      TabOrder = 0
      DesignSize = (
        344
        18)
      object Label12: TLabel
        Left = 0
        Top = 0
        Width = 323
        Height = 18
        Anchors = []
        Caption = ' Andreas Herbert Michael Vollenweider <kakukk@js.hu>'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object Button11: TButton
        Left = 323
        Top = 0
        Width = 15
        Height = 15
        Caption = 'x'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Pitch = fpVariable
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 0
      end
      object Label7: TLabel
        Left = 338
        Top = 0
        Width = 6
        Height = 13
        Anchors = []
        Caption = '  '
      end
    end
    object FlowPanel5: TFlowPanel
      Left = 345
      Top = 1
      Width = 338
      Height = 18
      Alignment = taLeftJustify
      AutoSize = True
      AutoWrap = False
      BevelEdges = [beBottom]
      BevelOuter = bvNone
      TabOrder = 1
      DesignSize = (
        338
        18)
      object Label6: TLabel
        Left = 0
        Top = 0
        Width = 323
        Height = 18
        Anchors = []
        Caption = ' Andreas Herbert Michael Vollenweider <kakukk@js.hu>'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object Button7: TButton
        Left = 323
        Top = 0
        Width = 15
        Height = 15
        Caption = 'x'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Pitch = fpVariable
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 0
      end
    end
    object FlowPanel9: TFlowPanel
      Left = 1
      Top = 19
      Width = 174
      Height = 18
      Alignment = taLeftJustify
      AutoSize = True
      AutoWrap = False
      BevelEdges = [beBottom]
      BevelOuter = bvNone
      TabOrder = 2
      DesignSize = (
        174
        18)
      object Label13: TLabel
        Left = 0
        Top = 0
        Width = 159
        Height = 18
        Anchors = []
        Caption = ' Benny Hill <kakukk@js.hu>'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object Button12: TButton
        Left = 159
        Top = 0
        Width = 15
        Height = 15
        Caption = 'x'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Pitch = fpVariable
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 0
      end
    end
  end
  object Edit1: TEdit
    Left = 32
    Top = 16
    Width = 121
    Height = 21
    TabOrder = 3
    Text = 'Edit1'
  end
  object CheckBox1: TCheckBox
    Left = 170
    Top = 17
    Width = 97
    Height = 17
    Caption = 'CheckBox1'
    TabOrder = 4
  end
  object ScrollBox1: TScrollBox
    Left = 51
    Top = 43
    Width = 102
    Height = 78
    TabOrder = 5
    object fpCimzett: TFlowPanel
      Left = 0
      Top = 0
      Width = 98
      Height = 34
      Align = alTop
      AutoSize = True
      BevelOuter = bvLowered
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      ExplicitWidth = 450
      object Label8: TLabel
        Left = 1
        Top = 1
        Width = 38
        Height = 16
        Caption = 'Label8'
      end
      object Label11: TLabel
        Left = 39
        Top = 1
        Width = 44
        Height = 16
        Caption = 'Label11'
      end
      object Label14: TLabel
        Left = 83
        Top = 1
        Width = 45
        Height = 16
        Caption = 'Label14'
      end
      object Label15: TLabel
        Left = 128
        Top = 1
        Width = 45
        Height = 16
        Caption = 'Label15'
      end
      object Label16: TLabel
        Left = 173
        Top = 1
        Width = 45
        Height = 16
        Caption = 'Label16'
      end
      object Label17: TLabel
        Left = 218
        Top = 1
        Width = 45
        Height = 16
        Caption = 'Label17'
      end
      object Label18: TLabel
        Left = 263
        Top = 1
        Width = 45
        Height = 16
        Caption = 'Label18'
      end
      object Label19: TLabel
        Left = 308
        Top = 1
        Width = 45
        Height = 16
        Caption = 'Label19'
      end
      object Label20: TLabel
        Left = 353
        Top = 1
        Width = 45
        Height = 16
        Caption = 'Label20'
      end
      object Label21: TLabel
        Left = 398
        Top = 1
        Width = 45
        Height = 16
        Caption = 'Label21'
      end
      object Label22: TLabel
        Left = 1
        Top = 17
        Width = 45
        Height = 16
        Caption = 'Label22'
      end
      object Label23: TLabel
        Left = 46
        Top = 17
        Width = 45
        Height = 16
        Caption = 'Label23'
      end
    end
  end
end
