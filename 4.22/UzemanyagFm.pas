unit UzemanyagFm;

interface

uses
	SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
	StdCtrls, Forms, ExtCtrls, Dialogs, J_FOFORMUJ;

type
	TUzemanyagFmDlg = class(TJ_FOFORMUJDLG)
	  procedure FormCreate(Sender: TObject);override;
    procedure Adatlap(cim, kod : string);override;
	end;

var
	UzemanyagFmDlg : TUzemanyagFmDlg;

implementation

uses
	Egyeb, J_SQL, Uzemanyagbe;

{$R *.DFM}

procedure TUzemanyagFmDlg.FormCreate(Sender: TObject);
begin
	Inherited FormCreate(Sender);
	FelTolto('�j �zemanyag �r felvitele ', '�zemanyag �r adatok m�dos�t�sa ',
			'�zemanyag �rak list�ja', 'UA_UAKOD', 'Select * from UZEMANYAG ','UZEMANYAG', QUERY_KOZOS_TAG );
	width	:= DLG_WIDTH;
	height	:= DLG_HEIGHT;
end;

procedure TUzemanyagFmDlg.Adatlap(cim, kod : string);
begin
	Application.CreateForm(TUzemanyagbeDlg, AlForm);
	Inherited Adatlap(cim, kod );
end;

end.

