unit J_SQL;

interface

uses
	DBTables, SysUtils, StdCtrls, Classes, ADODB, DateUtils, DB, Forms, Dialogs,Clipbrd, Math, KozosTipusok,
  Vcl.ExtCtrls, Vcl.CheckLst;

type

   TLogThread = class(TThread)
       private
           fnev    : string;
           logs    : string;
       protected
           Procedure Execute ; OverRide ;
       Public
           Constructor CreateNew(fn, ls : string) ;
   end;

	Rpozicio = record
		location	: string;
		datum		: string;
		digit		: string;
		sebesseg    : integer;
	end;

  TGetNewIDMode = (modeGetNextCode, modeGetNextStrCode);

  TPartnerKontakt = record
    telefon: string;
    email: string;
    ugyintezo: string;
    end;

	function 	SqlSzamString(szamstr: double; hossz, tizedes : integer) : string;
	function 	Query_Run  (sourcequery : TADOQuery; sqlstr : string; alert : boolean = true; log : boolean = true) : boolean;
	function 	Query_Update (sourcequery : TADOQuery; maintable : string;
						mezok : array of string; wherestr : string; Noticealert : boolean = true) : boolean;
	function 	Query_Insert (sourcequery : TADOQuery; maintable : string; mezok : array of string; Noticealert : boolean = true) : boolean;
	function 	Query_Insert_Identity (sourcequery : TADOQuery; maintable : string; mezok : array of string; identityfield: string) : integer;
	function 	Query_Select( tabla, mezo, keres, output : string) : string;
	function 	Query_Select2( tabla, mezo,mezo2, keres,keres2, output : string) : variant;
  function  Query_SelectString( tabla, SQL: string) : string;
  function  SQLFuttat(tabla, SQL: string) : string;
  function  Command_Run (sourcecommand : TADOCommand; sqlstr : string; alert : boolean = true) : boolean;
  procedure SqlHiba(SQLList : TStrings;  uzenet : string; Noticealert : boolean = true);
	function 	Query_Log_Str(q_str : string; q_duration : integer; q_result : boolean; use_thread: boolean = true) : boolean;
  procedure Writelog_NoThread(const FN, LogString: string; KellTimestamp: boolean = false);
  procedure SQL_List(squery : TADOQuery; cim : string; DialSzam : integer; cegnev : string  = '';
      FontSize: integer = 8; DisplaySzorzo: double = 7; FieldMargin: integer = 20);
	function 	BoolToStr( bool : boolean ) : string;
	function	GetNextCode(tablanev, mezonev : string; tipus : integer = 0; hossz : integer = 0) : integer;
	function	GetNextCode_Core(tablanev, mezonev : string; tipus : integer = 0; hossz : integer = 0) : integer;
	function 	GetNextStrCode(tablanev, mezonev : string; tipus : integer = 0; hossz : integer = 0; maxhossz: boolean = False; Prefix: string = '') : integer;
	function 	GetNextStrCode_Core(tablanev, mezonev : string; tipus : integer = 0; hossz : integer = 0; maxhossz: boolean = False; Prefix: string = '') : integer;
	function 	StrToBool( str : string ) : boolean;
	// function 	GetNextBcCode(tablanev, mezonev : string) : string;
	procedure SzotarTolt(Combo : TCustomComboBox; alkod : string; kodlist : TStringList; kodkell : boolean;
          kellures : boolean = false; sorrend_menev: boolean = false);
  procedure SzotarTolt_Multi(CheckListBox : TCheckListBox; alkod : string; kodlist : TStringList; kodkell : boolean;
    sorrend_menev: boolean = false);
  procedure SzotarTolt_CsakLista(Combo : TCustomComboBox; alkod : string);
  procedure DistinctTolt(Combo : TCustomComboBox; TablaNev, SelectSQL : string; kodlist : TStringList; kellures : boolean = false);
  procedure FlowPanelTolt(FlowPanel : TFlowPanel; alkod : string; kodlist : TStringList; kodkell : boolean;
    sorrend_menev: boolean = false);
	procedure 	ModLocate (Query : TADOQuery; fname, source : string );
	function 	EllenorzottJarat(kodstr : string; notice: boolean=True) : integer;
	function 	EllenorzottJarat2(alkodstr,ev : string) : integer;
  function EllenorzottJarat3(jaratkod: string) : integer;
//	function 	GetUjszamla : string;
	function 	GetUjszamla2(ev: string) : string;
	procedure 	HibaKiiro(uzenet : string);
	procedure  	NaploKiiro(uzenet : string);
	function 	PotosUtkozes(jarat : string) : boolean;
	procedure	SofornevKepzes(rensz : string);
	procedure	SofornevFrissites(nev, rensz : string);
	// procedure	SoforTelefonFrissites(soforkod : string);
	function	SoforUtkozes(jkod, sofkod, km1, km2 : string) : string;
	// function	GetApehUzemanyag(dat : string; tipus : integer = 0) : double; overload;
  function	GetApehUzemanyag(dat : string; tipus : integer = 0; korabbi_is_jo: boolean = false; csendes: boolean = false) : double;
	function	GetUzemanyagPotlekSz(vekod : string; uzemanyagar : double; tedat:string; legyen_notice: boolean) : double;
	function	DolgozoMezoEllenor : boolean;
	function	NevKiiro(kodlista, elvalaszto : string) : string;
	function	GetFelrakoDbStatus( query : TADOQuery) : integer;
	function	GetFelrakoStatus(msid : string) : integer;
	function	SetFelrakoStatus(msid : string; statusz : integer) : boolean;
	function 	GetTypename (ftype : TFieldType): string;
	function 	Create_Table_Str ( tablename : string; Query : TAdoQuery ) : string;
	procedure 	QueryExport (query : TAdoQuery);
	procedure 	QueryExportToFile (query : TAdoQuery; fn : string);
	function 	GetSzamlaValuta( szujkod : string) : string;
	function	GetHutoOsszeg(rendsz, uz1, uz2 : string) : double;
	function 	GetLocation(rsz : string; var poz : Rpozicio) : boolean;
	procedure 	FillOrszagkod(jarat_alkod, datum : string);
	function 	VanNyitottSzamla(jarat_kod : string) : boolean;
	function 	GetTableFullName(dbname, table_name : string) : string;
	function 	GetTableSchema(dbname, table_name : string) : string;
	function	GetJaratszam(jakod : string) : string;
	function  	GetJaratszamFromDb(QuerySource : TADOQuery) : string;
	function  	GetTelepiAr(dat : string; tipus : integer = 0) : double;	// A telepi tankol�s egys�g�ra egy adott d�tumra
	function    GepStatTolto(rsz : string) : boolean;
	function 	PotStatTolto(rsz : string) : boolean;
	function 	GetKoltsegAtlag(ktkod : string) : double;
	function 	GetAdBlueKoltsegAtlag(ktkod : string) : double;
	function 	GetHutoKoltsegAtlag(ktkod : string) : double;
	function 	KoltsegJaratTolto(ktkod : string) : boolean;
	function	HutoPotkocsi(potrsz : string) : boolean;
	function	FuvarHovaUpdate(ftkod : string) : boolean;
  function GetArfolyamDat(MBKOD: string): string;
	procedure	GetRszLista(li : TStringList; tipus : integer=0);
	function 	KlonSzamla(szkod, szujszam : string) : integer;
	function	FuvardijTolto(jkod : string) : boolean;
	function	KoltsegOsszevono(rsz, km, tipus : string) : boolean;
	function	MegbizasSzamlaTolto(mbkod : string) : boolean;
  function    GetTonnaAtlag(rsz: string; kmtol, kmig : integer; kellonsuly : boolean) : double;
  function	MegbizasKieg_Utvonal_is(mbkod : string) : boolean;
	function	MegbizasKieg(mbkod : string) : boolean;
   function	MegSegedJarat(mbkod, mskod : string ) : boolean;
   function    GetJaratAlkod(evstr : string) : integer;
   procedure   JaratCalc(jakod : string);
   function    GetServerDateTime(dt_type : integer = 0) : string;
   function    AlvallalkozoiJarat(jakod: string) : Boolean;
	function 	GetRightTag(tagszam	: integer; LehetSuperuser: boolean = True)	: integer;
   function	GetMegys(nyelv, megys : string) : string;
   function	GetAfakod(nyelv, hunkod : string) : string;
   function    GetSzoveg(vkod, alap, nyelv : string) : string;
   function	AposztrofCsere(szoveg : string) : string;
   function	AposztrofCsere_kozepen(szoveg : string) : string;
   procedure SzabadMemoriaNaploz(Hol: string);
   function SzabadMemoria(): longint;
   function EgyediElemek(BE: TStringList): TStringList;
   function TitleCase(const sText: String): String;
   function GetNewIDLoop(Mode: TGetNewIDMode; TablaNev, MezoNev, Prefix : string; tipus : integer = 0; hossz : integer = 0; maxhossz: boolean = False) : integer;
   function GetNewID(Mode: TGetNewIDMode; TablaNev, MezoNev, Prefix: string; tipus : integer = 0; hossz : integer = 0; maxhossz: boolean = False) : integer;
   function FindURLQuery(url: string): string;
   function StoreURLQuery(url, json: string; pointcount: integer): string;
   function DelDoubleSpaces(OldText:String):string;
   function GetPartnerKontakt(vkod: string) : TPartnerKontakt;
   function LehetEgyediDij(partnerkod: string; var ManCode: string): boolean;
   procedure MegbizasPaletta(mskod: string);
   function  GetPaletta(helykod, datum, idopt : string) : integer;
   function  GetHelynev(helykod : string) : string;
   function RecordsetToJSON(SQL: string): string;
   function SetTelefonDolgozo(TKID, DOKOD: string): string;
   function SetElofizetesDolgozo(TEID, DOKOD: string): string;
   function SetElofizetesTelefon(TEID, TKID: string): string;
   function GetTelMegnevezes(TelKod: string): string;
   function GetTelDokod(TelKod: string): string;
   function GetTelDoNev(TelKod: string): string;
   function GetElsodlegesSzamCount(DOKOD: string): integer;
   procedure SetElsodlegesSzam(DOKOD, TEID: string);
   function GetElsodlegesSzam(DOKOD: string; LehetTobb: boolean = False): string;
   function GetBarmelyikSzam(DOKOD: string): string;
   function ExecuteSQLTransaction(SQLArray: TStringList): string;
   function StoreOneSMS(Telefon, Szoveg: string): string;
   function LegutobbFelvittTelefonTipus: string;
   function FelrakoElment(a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, FelrakoKod: string; Formbol: boolean): integer;
   function GetUjFelrakoNev(FELNEV: string): string;
   function QueryCoordinates(OrszagKod, ZIP, Varos, UtcaHazszam: string): TGeoCodeInfo;
   function GetOrszagNemzetkoziNev(OrszagKod: string): string;
   function GetSoforEagleSMS(rendszam: string): string;
 	 procedure SetMegbizasModositoUser(MBKOD: string);
   function VanHozzaIgaziSzamla(MBKOD: string): string;
   function LezaratlanSzamlaTorles(MBKOD: string): string;
   function Query_Update_FormatSQL(maintable : string; mezok : array of string; wherestr : string): string;
   function Query_Insert_FormatSQL(maintable : string; mezok : array of string): string;
   function GetFelrakoKod(const CegNev, Orszag, ZIP, Varos, UtcaHazszam: string): string;
   function SzotarBovito(const Fokod: string; UjElem: string): string;
   function GetDolgozoCsoportKod(const DOKOD: string; Datum: string): string;
   function KoltsegTorles(const KTKOD: integer; const Gyujtobe_vissza: boolean): string;
   procedure NemHasznaltSzamlaKod(szakod: string);
 implementation

uses
	Egyeb, SqlList, Kozos, J_Barcode, J_Export, KOZOS_LOCAL, Windows, J_Datamodule, Data.DBXJSON, GoogleGeoCode;

var
	t1	    : TDateTime;
   ex_str  : string;
   ex_num  : integer;

function AlvallalkozoiJarat(jakod: string) : Boolean;
var
	QueryDat1	: TADOQuery;
begin
  Result:=False;
	QueryDat1 		:= TADOQuery.Create(nil);
	QueryDat1.Tag := QUERY_ADAT_TAG;
	EgyebDlg.SeTADOQueryDatabase(QueryDat1);
 	if Query_Run (QueryDat1, 'SELECT * FROM ALJARAT WHERE AJ_JAKOD = '''+jakod+''' ',true) then
  begin
    Result:=not QueryDat1.Eof;
  end;
  QueryDat1.Close;
  QueryDat1.Free;
end;

procedure SqlHiba(SQLList : TStrings;  uzenet : string; Noticealert : boolean = true);
var
	str, S: string;
   i	: integer;
begin
	str	:= '';
	for i := 0 to SQLList.Count - 1 do begin
   	str := str + SQLList[i];
	end;
   HibaKiiro(str);
   if NoticeAlert then begin
     if pos('duplicate key',uzenet)>0 then
        NoticeKi('A megadott elem m�r szerepel az adatt�bl�ban! Az adatok NEM lettek let�rolva!')
     else begin
     	  // NoticeKi(uzenet, NOT_MESSAGE);
        S:= 'SQL hiba: '+str;
        if ex_str <> '' then
   	        S:= S+ ' ('+ex_str+')';
        NoticeKi(S, NOT_MESSAGE);
        end;
     end;
end;


function SqlSzamString(szamstr: double; hossz, tizedes : integer) : string;
var
  locstr	: string;
begin
	locstr := SzamString(szamstr, hossz, tizedes);
  	while Pos(',',locstr) > 0 do begin
     	locstr[Pos(',',locstr)] := '.';
  	end;
	Result := locstr;
end;

{******************************************************************************
F�ggv�ny neve : Query_Run
Funci�        : 	A megadott param�terek alapj�n v�grehajtja a Query-t, �s
                 hiba eset�n az SQL sz�veget bet�lti egy txt-be.
Param�terek   : 	sourcequery 	- a forr�s query
						sqlstr			- az SQL string
Visszaadott �.: 	siker�lt vagy nem a v�grehajt�s true - false
Hiba�rt�kek	: 	-

*********************************************************************************************}
function Query_Run (sourcequery : TADOQuery; sqlstr : string; alert : boolean = true; log : boolean = true) : boolean;
{********************************************************************************************}
var
   kellog          : boolean;
const
   repeat_nr       : integer = 1;
begin
	Result	   	:= true;
	kellog		:= false;
   t1			:= now;
   ex_str      := '';
   ex_num      := 0;

   // clipboard.Clear;
   // Clipboard.AsText:=sqlstr;

  sourcequery.CommandTimeout:= EgyebDlg.SQL_TIMEOUT;
 	sourcequery.Close;
	sourcequery.SQL.Clear;
 	sourcequery.Sql.Add(sqlstr);
 	try
       // if UpperCase(copy(sqlstr,1,7)) = 'SELECT '  then begin
       // Azok az INSERT-ek, ahol identity mez� van, szint�n adnak vissza adathalmazt.
       // �s a t�rolt elj�r�s is adhat vissza
       if (UpperCase(copy(trim(sqlstr),1,7)) = 'SELECT ') or (Pos('OUTPUT inserted.', sqlstr)>0)
         or (UpperCase(copy(trim(sqlstr),1,5)) = 'EXEC ') then begin
           if EgyebDlg.log_level > 1 then begin
               kellog := true;
           end;
           sourcequery.Open;			// Megnyit�s
       end else begin
			if EgyebDlg.log_level > 0 then begin
               kellog := true;
			end;
           sourcequery.ExecSQL;		// Futtat�s
       end;
 	except
       on E: Exception do begin// ErrorDialog(E.Message , E.HelpContext);
		       ex_str := E.Message;
           ex_num := repeat_nr;
           if alert then begin
              SqlHiba(sourcequery.SQL, 'Hib�s SQL! ('+sqlstr+')');
              end
           else if log then begin
              HibaKiiro('Hib�s SQL! ('+sqlstr+'): '+E.Message);
              end;
           Result	:= false;
           end;  // on E
       	end;  // try-except
   if kellog then begin
       if ex_str <> '' then begin
   	    Query_Log_Str(sqlstr+'('+ex_str+')', MilliSecondsBetween(now, t1), result);
       end else begin
   	    Query_Log_Str(sqlstr, MilliSecondsBetween(now, t1), result);
       end;
   end;
end;


function Query_Update_FormatSQL(maintable : string; mezok : array of string; wherestr : string): string;
var
  i: integer;
  SQLStr, str: string;
begin
 	if High(mezok) < 1 then begin
 		Result := '';
    Exit;
    end;
 	i	:= 0;
  SQLStr:= 'UPDATE '+maintable+' SET ';
 	while i <= High(mezok) do begin
    str	:= trim(mezok[i]) + '= '+trim(AposztrofCsere_kozepen(mezok[i+1]));    // 2016-11-03 NP
    	if i < ( High(mezok) -1 ) then begin
    		str := str + ', ';
    	end;
 		SQLStr:= SQLStr + str;
   	i := i+2;
   	end;  // while
 	if wherestr <> '' then begin
 		SQLStr:= SQLStr + wherestr;
  	end;  // if
  Result:= SQLStr;
end;


{******************************************************************************
F�ggv�ny neve : Query_Update
Funci�        : 	A megadott param�terek alapj�n update-et hajt v�gre az

						adatb�zisban. A mezonevek ill. �rt�k�k egy nyilt t�mbb�n
                	kereszt�l ker�l �tad�sra.
Param�terek   : 	sourcequery 	- a forr�s query
						maintable	- a fo t�bla neve (UPDATE INTO maintable SET ...)
                 mezok			- a mezo �s tartalmuk egy nyilt t�mbben
                 wherestr		- a where string
Visszaadott �.: 	siker�lt vagy nem a v�grehajt�s true - false
Hiba�rt�kek	: 	-
******************************************************************************}
function Query_Update (sourcequery : TADOQuery; maintable : string;
						mezok : array of string; wherestr : string; Noticealert : boolean = true) : boolean;
// *****************************************************************************
var
	i: integer;
 	str: string;

begin
	t1			:= now;
	Result		:= true;
  ex_str      := '';
  ex_num      := 0;
 	if High(mezok) < 1 then begin
 		Result := false;
    Exit;
 	end;
  // ---- itt �ll�tjuk �ssze az SQL-t ----- //
  str:= Query_Update_FormatSQL(maintable, mezok, wherestr);
  sourcequery.CommandTimeout:= EgyebDlg.SQL_TIMEOUT;
	sourcequery.Close;
	sourcequery.SQL.Clear;
 	sourcequery.Sql.Add(str);
 	try
  		sourcequery.ExecSQL;
  	except
      on E : Exception do begin
           ex_str := E.Message;
           if Noticealert then begin
             SqlHiba(sourcequery.Sql, 'Hib�s SQL! ('+str+')', NoticeAlert);
             end;  // if alert
           Result	:= false;
          end; // on E
    	end;  // try-except
  	sourcequery.Close;
   if EgyebDlg.log_level > 0 then begin
       if ex_str <> '' then begin
   	      Query_Log_Str(str+'('+ex_str+')', MilliSecondsBetween(now, t1), result);
          end  // if
       else begin
   	      Query_Log_Str(str, MilliSecondsBetween(now, t1), result);
          end;  // else
       end;  // if
end;

{function Query_Update (sourcequery : TADOQuery; maintable : string;
						mezok : array of string; wherestr : string; Noticealert : boolean = true) : boolean;
// *****************************************************************************
var
	i 		: integer;
  	str		: string;
   logstr	: string;
const
   repeat_nr       : integer = 1;
begin
	t1			:= now;
	Result		:= true;
   ex_str      := '';
   ex_num      := 0;
 	if High(mezok) < 1 then begin
 		Result := false;
    	Exit;
 	end;
	sourcequery.Close;
	sourcequery.SQL.Clear;
 	sourcequery.Sql.Add('UPDATE '+maintable+' SET ');
   logstr	:= 'UPDATE '+maintable+' SET ';
 	i	:= 0;
 	while i <= High(mezok) do begin
 		// str	:= mezok[i] + '='+mezok[i+1];
    str	:= mezok[i] + '='+AposztrofCsere_kozepen(mezok[i+1]);    // 2016-11-03 NP
    	if i < ( High(mezok) -1 ) then begin
    		str := str + ', ';
    	end;
 		sourcequery.Sql.Add(str);
	   	logstr	:= logstr + str;
    	i := i+2;
 	end;
	if wherestr <> '' then begin
  		sourcequery.Sql.Add(wherestr);
	   	logstr	:= logstr +' '+ wherestr;
      //sourcequery.SQL.Text:=logstr;
  	end;

  	try
  		sourcequery.ExecSQL;
  	except
      on E : Exception do begin
           ex_str := E.Message;
           if Noticealert then begin
             SqlHiba(sourcequery.SQL, 'Hib�s SQL! ('+sourcequery.SQL[0]+')', NoticeAlert);
             end;  // if alert
           Result	:= false;
          end; // on E
    	end;  // try-except
  	sourcequery.Close;
   if EgyebDlg.log_level > 0 then begin
       if ex_str <> '' then begin
   	    Query_Log_Str(logstr+'('+ex_str+')', MilliSecondsBetween(now, t1), result);
       end else begin
   	    Query_Log_Str(logstr, MilliSecondsBetween(now, t1), result);
       end;
   end;
end;
}

{******************************************************************************
F�ggv�ny neve : Query_Select
Funci�        : 	A megadott param�terek alapj�n megkeresi egy adott t�bla
						egyik rekordj�t �s visszaadja a k�v�nt mez�t.
Param�terek   : 	tabla			- a t�bla neve
                 mezo			- a mezo neve
                 keres			- a keresett sz�veg
                 output		- a k�v�nt mez� neve
Visszaadott �.: 	a megfelel� mez� tartalma
Hiba�rt�kek	: 	-
******************************************************************************}
function Query_Select( tabla, mezo, keres, output : string) : string;
{*****************************************************************************}
var
	Query1	: TADOQuery;
begin
	Result	:= '';
  Query1	:= TADOQuery.Create(nil);
	if Pos( tabla , DAT_TABLAK ) > 0 then begin
		Query1.Tag := 1;
	  end;
  Query1.CommandTimeout:= EgyebDlg.SQL_TIMEOUT;
	EgyebDlg.SeTADOQueryDatabase(Query1);
	// if Query_Run(Query1, 'SELECT * FROM '+tabla+' WHERE '+mezo+'='''+keres+'''') then begin
	//	Result	:= Query1.FieldByname(output).AsString;

	// if Query_Run(Query1, 'SELECT '+output+' FROM '+tabla+' WHERE '+mezo+'='''+keres+'''') then begin
  if Query_Run(Query1, 'SELECT '+output+' FROM '+tabla+' WHERE '+mezo+'='''+AposztrofCsere(keres)+'''') then begin
	 	Result	:= Query1.Fields[0].AsString;    // �gy megenged kifejez�st is, pl. count(KOD)
		Query1.Close;
	end;
	Query1.Destroy;
end;

function Query_Select2( tabla, mezo,mezo2, keres,keres2, output : string) : variant;
{*****************************************************************************}
var
	Query1	: TADOQuery;
  sql: string;
begin
	Result	:= '';
  	Query1	:= TADOQuery.Create(nil);
	if Pos( tabla , DAT_TABLAK ) > 0 then begin
		Query1.Tag := 1;
	end;
  Query1.CommandTimeout:= EgyebDlg.SQL_TIMEOUT;
	EgyebDlg.SeTADOQueryDatabase(Query1);
	sql:='SELECT * FROM '+tabla+' WHERE '+mezo+'='''+keres+''''+' and '+mezo2+'='''+keres2+'''';
	if Query_Run(Query1,sql) then begin
		if Query1.FieldByname(output).IsNull then
      Result:=0
    else
    	Result	:= Query1.FieldByname(output).AsVariant;
		Query1.Close;
	end;
	Query1.Destroy;
end;

{*********************************************************************************************}
function Command_Run (sourcecommand : TADOCommand; sqlstr : string; alert : boolean = true) : boolean;
{********************************************************************************************}
var
   kellog : boolean;
   SQLList  : TStrings;            // az SqlHiba kompatibilit�shoz
const
   repeat_nr       : integer = 1;
begin
	Result	   	:= true;
	kellog		:= false;
  t1			:= now;
  ex_str      := '';
  ex_num      := 0;

 	sourcecommand.CommandText:= sqlstr;
  sourcecommand.CommandTimeout:= EgyebDlg.SQL_TIMEOUT;
 	try
     if EgyebDlg.log_level > 0 then begin
        kellog := true;
		  	end;
     sourcecommand.Execute;		// Futtat�s
 	except
       on E: Exception do begin// ErrorDialog(E.Message , E.HelpContext);
		    ex_str := E.Message;
           ex_num := repeat_nr;
           if alert then begin
               SQLList := TStrings.Create;
               SQLList.Add(sqlstr);
               SqlHiba(SQLList, 'Hib�s SQL! ('+sqlstr+')');
               SQLList.Free;
               end;  // if alert
           Result	:= false;
         end;  // try-except
 	end;
   if kellog then begin
       if ex_str <> '' then begin
   	    Query_Log_Str(sqlstr+'('+ex_str+')', MilliSecondsBetween(now, t1), result);
       end else begin
   	    Query_Log_Str(sqlstr, MilliSecondsBetween(now, t1), result);
       end;
   end;
end;

{******************************************************************************}
function Query_SelectString( tabla, SQL: string) : string;
{*****************************************************************************}
var
	Query1	: TADOQuery;
begin
	Result	:= '';
  	Query1	:= TADOQuery.Create(nil);
	if Pos( tabla , DAT_TABLAK ) > 0 then begin
		Query1.Tag := 1;
	end;
  Query1.CommandTimeout:= EgyebDlg.SQL_TIMEOUT;
	EgyebDlg.SeTADOQueryDatabase(Query1);
	if Query_Run(Query1, SQL) then begin
	 	Result	:= Query1.Fields[0].AsString;    // �gy megenged kifejez�st is, pl. count(KOD)
		Query1.Close;
	end;
	Query1.Free;
end;

{******************************************************************************}
function SQLFuttat(tabla, SQL: string) : string;
{*****************************************************************************}
var
	Query1	: TADOQuery;
begin
	Result	:= '';
  	Query1	:= TADOQuery.Create(nil);
	if Pos( tabla , DAT_TABLAK ) > 0 then begin
		Query1.Tag := 1;
	end;
  Query1.CommandTimeout:= EgyebDlg.SQL_TIMEOUT;
	EgyebDlg.SeTADOQueryDatabase(Query1);
	if Query_Run(Query1, SQL) then begin
		Query1.Close;
	end;
	Query1.Free;
end;

{******************************************************************************
F�ggv�ny neve : BoolToStr
Funci�        : 	A megadott logikai �rt�k sz�veges le�r�s�t adja.
Param�terek   : 	bool			- a logikai �rt�k
Visszaadott �.: 	a megfelel� sz�veg
Hiba�rt�kek	: 	-
******************************************************************************}
function BoolToStr( bool : boolean ) : string;
{*****************************************************************************}
begin
	Result	:= 'FALSE';
	if bool then begin
		Result	:= 'TRUE';
	end;
end;

{******************************************************************************
F�ggv�ny neve : StrToBool
Funci�        : 	A megadott sz�vegnek megfelel� logikai �rt�ket adja vissza.
Param�terek   : 	str			- a sz�veg
Visszaadott �.: 	a megfelel� logikai �rt�k (csak a 'TRUE' sz�veg lesz igaz)
Hiba�rt�kek	: 	-
******************************************************************************}
function StrToBool( str : string ) : boolean;
{*****************************************************************************}
begin
	Result	:= FALSE;
	if UpperCase(str) = 'TRUE' then begin
		Result	:= TRUE;
	end;
end;


{******************************************************************************
F�ggv�ny neve : 	Query_Log_Str
Funci�        : 	A megadott query elemeit bejegyzi a LOG t�bl�ba.
Param�terek   : 	q_str	- a query; q_duration - id�tartam (milisec)
					q_result- eredm�ny
Visszaadott �.: 	TRUE - siker�lt a m�velet
Hiba�rt�kek	: 	-
******************************************************************************}
function Query_Log_Str(q_str : string; q_duration : integer; q_result : boolean; use_thread: boolean = true) : boolean;
{*****************************************************************************}
var
//	Query1		: TADOQuery;
//	str 		: string;
//  	sqlstr  	: string;
   eredstr		: string;
   idopt       : string;
   fn          : string;
   lstr        : string;
   szabadmem   : string;
begin
   // A log ki�r�sa �llom�nyba k�l�n sz�lon
   Result  := true;
   eredstr	:= '0';
   if q_result then begin
   	eredstr	:= '1';
   end;
   idopt   := GetServerDateTime(2);
   if EgyebDlg.log_memory = 1 then begin
      szabadmem:=formatFloat('0.000', SzabadMemoria() / (1024*1024));  // MByte-ban
      end
   else begin
      szabadmem:='';  // a hely�t az�rt meghagyom a ;-s strukt�r�ban
      end;
   // KI_DATUM, KI_IDOPT, KI_IDOMS, KI_SZKOD, KI_ITART, KI_EREDM, KI_SQLSZ
   lstr    := copy(idopt, 1, 11)+'; '+copy(idopt, 13, 8)+';'+copy(idopt, 22, 10)+';'+
           //    EgyebDlg.user_code+';'+IntToStr(q_duration)+';'+eredstr+';'+q_str+';';
              EgyebDlg.user_code+';'+IntToStr(q_duration)+';'+eredstr+';'+szabadmem+';'+q_str+';'+'['+SW_VERZIO+']';
   fn      := ExtractFileName(Application.ExeName);
   // fn      := EgyebDlg.TempList + copy(fn, 1, Pos('.', fn) -1 ) +'_'+egyebDlg.user_code+'_log.txt';
   fn      := EgyebDlg.NaploPath + copy(fn, 1, Pos('.', fn) -1 ) +'_'+egyebDlg.user_code+'_log.txt';
   if use_thread then
     TLogThread.CreateNew(fn, lstr)
   else
     Writelog_NoThread(fn, lstr);  // pl. debug-n�l fontos lehet, hogy id�rendben �rja ki a sorokat

end;

procedure Writelog_NoThread(const FN, LogString: string; KellTimestamp: boolean = false);
var
  F: textfile;
  i: integer;
  Siker: boolean;
  S: string;
begin
	AssignFile(F, FN);
  Siker:= False;
  i:= 0;
  while (not Siker and (i<=100)) do begin  // max 100 pr�b�lkoz�s
  	{$I-}
    if FileExists(FN) then begin
      Append(F)
      end
    else begin
      Rewrite(F);
      end;
    {$I+}
    if IOResult = 0 then
        Siker:= True
    else Sleep(10); // egy kis v�rakoz�s
    Inc(i);
    end;  // while
  if Siker then begin
       if KellTimestamp then
          S:= GetServerDateTime(2) + ' '+LogString
       else S:= LogString;
       Writeln(F, S);
       end;
  // DEBUG .... else MessageDlg('Nincs ki�rva :-(  '+logs, mtError, [mbOK], 0);
	Flush(f);
  // DEBUG...if i <> 0 then MessageDlg('Flush error!', mtError, [mbOK], 0);
	CloseFile(f);
end;

procedure SQL_List(squery : TADOQuery; cim : string; DialSzam : integer; cegnev : string  = '';
      FontSize: integer = 8; DisplaySzorzo: double = 7; FieldMargin: integer = 20);
begin
	if cegnev = '' then begin
		cegnev := EgyebDlg.Read_SZGrid('999','100');
	end;
	SQLListDlg := TSQLListDlg.Create(nil);
  SQLListDlg.pFontSize:= FontSize;
  SQLListDlg.pDisplaySzorzo:= DisplaySzorzo;
  SQLListDlg.pFieldMargin:= FieldMargin;
  SQLListDlg.Tolto(squery, cegnev, cim, EgyebDlg.GetSzuroStr(IntToStr(DialSzam)));
	SQLListDlg.Rep.Preview;
  SQLListDlg.Destroy;
end;

function GetNextCode(tablanev, mezonev : string; tipus : integer = 0; hossz : integer = 0) : integer;
var
	Query1		: TADOQuery;
	ujkodszam	: integer;
  jokod		: boolean;
  formatstr	: string;
begin
  // ---------   � T I R � N Y � T � S   G E T N E W I D   I R � N Y B A   ------- //
  // �gy nem kell a programk�dban mindenhol �t�rni GetNewId-re
  if ((uppercase(tablanev) = 'MEGSEGED') and  (uppercase(mezonev) = 'MS_MSKOD')) then begin
      	Result:= GetNewIDLoop(modeGetNextCode, tablanev, mezonev, '', tipus, hossz);
        end  // if
  // ----------------------------------------------------------------------------- //
  else begin
    Result:= GetNextCode_Core(tablanev, mezonev, tipus, hossz);
    end;  // if
end;

function GetNextCode_Core(tablanev, mezonev : string; tipus : integer = 0; hossz : integer = 0) : integer;
var
	Query1		: TADOQuery;
	ujkodszam	: integer;
  jokod		: boolean;
  formatstr	: string;
begin
  t1	:= now;
  // Visszaadja a k�vetkez� k�dot
    Query1	:= TADOQuery.Create(nil);
    if Pos( tablanev , DAT_TABLAK ) > 0 then begin
    Query1.Tag := 1;
    end;
    EgyebDlg.SeTADOQueryDatabase(Query1);
  Result 		:= 0;
  ujkodszam	:= 1;
  if not Query_Run(Query1, 'SELECT MAX('+mezonev+') MAXKOD FROM '+tablanev) then begin
      Query1.Destroy;
      Exit;
      end;
  if Query1.RecordCount > 0 then begin
    ujkodszam	:= StrToIntDef(Query1.FieldByname('MAXKOD').AsString,0) + 1;
    end;
    Query1.Close;
    jokod	:= false;
    while not jokod do begin
      if tipus = 0 then begin		// sz�veges mez�r�l van sz�
        formatstr	:= '''%'+IntToStr(hossz)+'.'+IntToStr(hossz)+'d''';
//			formatstr	:= '''%'+IntToStr(hossz)+'d''';
        end
      else begin					// integer mez�r�l van sz�
        formatstr	:= '%d';
        end;
      if not Query_Run(Query1, 'SELECT '+mezonev+' FROM '+tablanev+' WHERE '+mezonev+'='+Format(formatstr,[ujkodszam])+' ') then begin
        Query1.Destroy;
        Exit;
        end;
      if Query1.RecordCount > 0 then begin
        Inc(ujkodszam);
        end
      else begin
        jokod	:= true;
        end;
      end;  // while
  Query1.Destroy;
  Result	:= ujkodszam;
end;


function GetNextStrCode(tablanev, mezonev : string; tipus : integer = 0; hossz : integer = 0; maxhossz: boolean = False; Prefix: string = '') : integer;
var
	Query1		: TADOQuery;
	ujkodszam	: integer;
  jokod		: boolean;
  formatstr	: string;
begin
  // ---------   � T I R � N Y � T � S   G E T N E W I D   I R � N Y B A   ------- //
  // �gy nem kell a programk�dban mindenhol �t�rni GetNewId-re
  if False {((uppercase(tablanev) = 'MEGSEGED') and  (uppercase(mezonev) = 'MS_MSKOD'))} then begin
      	Result:= GetNewIDLoop(modeGetNextStrCode, tablanev, mezonev, '', tipus, hossz);
        end  // if
  // ----------------------------------------------------------------------------- //
  else begin
    Result:= GetNextStrCode_Core(tablanev, mezonev, tipus, hossz, maxhossz, Prefix);
    end;  // if
end;

function GetNextStrCode_Core(tablanev, mezonev : string; tipus : integer = 0; hossz : integer = 0; maxhossz: boolean = False; Prefix: string = '') : integer;
var
	Query1		: TADOQuery;
	ujkodszam	: integer;
	jokod	  	: boolean;
	formatstr, S: string;
  i: integer;
begin
	t1	:= now;
	// Visszaadja a k�vetkez� k�dot
	Query1	:= TADOQuery.Create(nil);
	if Pos( tablanev , DAT_TABLAK ) > 0 then begin
		Query1.Tag := 1;
	end;
	EgyebDlg.SeTADOQueryDatabase(Query1);
	Result 		:= 0;

  S:= 'SELECT MAX(CAST('+mezonev+' AS INTEGER)) MAXKOD FROM '+tablanev;
  if Prefix <> '' then
    S:= S+' where '+mezonev+ ' like '''+ Prefix+'%''';
	if not Query_Run(Query1, S) then begin
		Query1.Destroy;
		Exit;
	end;
	// if Query1.RecordCount > 0 then begin
  if StrToIntDef(Query1.FieldByname('MAXKOD').AsString,0) <> 0 then begin
		ujkodszam	:= StrToIntDef(Query1.FieldByname('MAXKOD').AsString,0) + 1;
	  end
  else begin
    ujkodszam	:= 1;
    if maxhossz and (Prefix <> '') then begin
      ujkodszam:= StrToInt(Prefix)*Floor(IntPower(10, hossz-length(Prefix))) + ujkodszam ;    // ha nincs prefix, akkor a r�gi m�k�d�s
      end;  // if maxhossz
    end;  // else
	Query1.Close;
	jokod	:= false;
	while not jokod do begin
		if tipus = 0 then begin		// sz�veges mez�r�l van sz�
			formatstr	:= '''%'+IntToStr(hossz)+'.'+IntToStr(hossz)+'d''';
//			formatstr	:= '''%'+IntToStr(hossz)+'d''';
		end else begin					// integer mez�r�l van sz�
			formatstr	:= '%d';
		end;
		if not Query_Run(Query1, 'SELECT '+mezonev+' FROM '+tablanev+' WHERE '+mezonev+'='+Format(formatstr,[ujkodszam])+' ') then begin
			Query1.Destroy;
			Exit;
		end;
		if Query1.RecordCount > 0 then begin
			Inc(ujkodszam);
		end else begin
			jokod	:= true;
		end;
	end;
	Query1.Destroy;
	Result	:= ujkodszam;
end;



function Query_Insert_FormatSQL(maintable : string; mezok : array of string): string;
var
  i: integer;
  SQLStr, str, str2: string;
begin
  if High(mezok) < 1 then begin
 		Result := '';
    Exit;
    end;
 	i	:= 0;
  str	:= '';
  str2	:= '';
 	while i <= High(mezok) do begin
 		str	:= str + trim(mezok[i]) ;
    str2	:= str2 + trim(AposztrofCsere_kozepen(mezok[i+1]));
    if i < ( High(mezok) -1 ) then begin
      str 	:= str + ', ';
    	str2 	:= str2 + ', ';
    	end;  // if
    	i := i + 2;
   	end;  // while
 	SQLStr:= 'INSERT INTO ' + maintable+' (' + str +' ) VALUES ( '+ str2 + ' ) ';
  Result:= SQLStr;
end;
{******************************************************************************
F�ggv�ny neve : Query_Insert
Funci�        : 	A megadott param�terek alapj�n insert-et hajt v�gre az
						adatb�zisban. A mezonevek ill. �rt�k�k egy nyilt t�mbb�n
					kereszt�l ker�l �tad�sra.
Param�terek   : 	sourcequery 	- a forr�s query
						maintable	- a fo t�bla neve (UPDATE INTO maintable SET ...)
				  mezok			- a mezo �s tartalmuk egy nyilt t�mbben
Visszaadott �.: 	siker�lt vagy nem a v�grehajt�s true - false
Hiba�rt�kek	: 	-
******************************************************************************}
function Query_Insert (sourcequery : TADOQuery; maintable : string;
						mezok : array of string; Noticealert : boolean = true) : boolean;
// *****************************************************************************
var
	i 		: integer;
 	SQLStr: string;
const
   repeat_nr       : integer = 1;
begin
	t1			:= now;
	Result		:= true;
   ex_str      := '';
   ex_num      := 0;
 	if High(mezok) < 1 then begin
 		Result := false;
    	Exit;
	end;
  sourcequery.CommandTimeout:= EgyebDlg.SQL_TIMEOUT;
 	sourcequery.Close;
	sourcequery.SQL.Clear;
  // itt �ll�tjuk �ssze az SQL-t
  SQLStr:= Query_Insert_FormatSQL(maintable, mezok);
 	sourcequery.Sql.Add(SQLStr);
  	try
  		sourcequery.ExecSQL;
  	except
      on E : Exception do begin
          ex_str := E.Message;
  		    SqlHiba(sourcequery.Sql, 'Hib�s SQL! ('+SQLStr+')', NoticeAlert);
	  	    Result	:= false;
          end;  // on E
    	end;  // try-except
  	sourcequery.Close;
  	if EgyebDlg.log_level > 0 then begin
       if ex_str <> '' then begin
   	    Query_Log_Str(SQLStr+' ('+ex_str+')',
			MilliSecondsBetween(now, t1), result);
       end else begin
   	    Query_Log_Str(SQLStr,	MilliSecondsBetween(now, t1), result);
       end;
   end;
end;

{function Query_Insert (sourcequery : TADOQuery; maintable : string;
						mezok : array of string; Noticealert : boolean = true) : boolean;
// *****************************************************************************
var
	i 		: integer;
  	str		: string;
  	str2	: string;
const
   repeat_nr       : integer = 1;
begin
	t1			:= now;
	Result		:= true;
   ex_str      := '';
   ex_num      := 0;
 	if High(mezok) < 1 then begin
 		Result := false;
    	Exit;
	end;
 	sourcequery.Close;
	sourcequery.SQL.Clear;
 	i	:= 0;
  	str	:= '';
  	str2	:= '';
 	while i <= High(mezok) do begin
 		str	:= str + mezok[i] ;
    // str2	:= str2 + mezok[i+1] ;  // 2016-11-03 NP
    str2	:= str2 + AposztrofCsere_kozepen(mezok[i+1]);
    if i < ( High(mezok) -1 ) then begin
    		str 	:= str + ', ';
    		str2 	:= str2 + ', ';
    	end;
    	i := i + 2;
 	end;
 	sourcequery.Sql.Add('INSERT INTO ' + maintable );
	sourcequery.Sql.Add('(' + str +' ) VALUES ( ');
	sourcequery.Sql.Add( str2 + ' ) ');
  	try
  		sourcequery.ExecSQL;
  	except
      on E : Exception do begin
          ex_str := E.Message;
  		    SqlHiba(sourcequery.SQL, 'Hib�s SQL! ('+sourcequery.SQL[0]+')', NoticeAlert);
	  	    Result	:= false;
          end;  // on E
    	end;  // try-except
  	sourcequery.Close;
  	if EgyebDlg.log_level > 0 then begin
       if ex_str <> '' then begin
   	    Query_Log_Str('INSERT INTO '+maintable+'('+str+') VALUES ('+str2+')  '+'('+ex_str+')',
			MilliSecondsBetween(now, t1), result);
       end else begin
   	    Query_Log_Str('INSERT INTO '+maintable+'('+str+') VALUES ('+str2+')',
			MilliSecondsBetween(now, t1), result);
       end;
   end;
end;
}

function 	Query_Insert_Identity (sourcequery : TADOQuery; maintable : string; mezok : array of string; identityfield: string) : integer;
var
	i 		: integer;
  	str, str2, fullsql: string;
const
   repeat_nr       : integer = 1;
begin
	t1			:= now;
	Result		:= -1;
   ex_str      := '';
   ex_num      := 0;
 	if High(mezok) < 1 then begin
 		Result := -1;
    	Exit;
	end;
  sourcequery.CommandTimeout:= EgyebDlg.SQL_TIMEOUT;
 	sourcequery.Close;
	sourcequery.SQL.Clear;
 	i	:= 0;
  	str	:= '';
  	str2	:= '';
 	while i <= High(mezok) do begin
 		str	:= str + mezok[i] ;
    //	str2	:= str2 + mezok[i+1] ;
    str2	:= str2 + AposztrofCsere_kozepen(mezok[i+1]);   // 2016-11-03 NP
    	if i < ( High(mezok) -1 ) then begin
    		str 	:= str + ', ';
    		str2 	:= str2 + ', ';
    	end;
    	i := i + 2;
 	end;
  fullsql:= 'INSERT INTO ' + maintable + '(' + str +' ) '+ ' OUTPUT inserted.' + identityfield+ ' '+' VALUES ( '+ str2 + ' ) ';
 	sourcequery.Sql.Add(fullsql);
  	try
  		sourcequery.Open;
      Result:= sourcequery.Fields[0].AsInteger;
  	except
      on E : Exception do begin
           ex_str := E.Message;
  		    SqlHiba(sourcequery.SQL, 'Hib�s SQL! ('+fullsql+')');
	  	    Result	:= -1;
       end;
  	end;
  	sourcequery.Close;
  	if EgyebDlg.log_level > 0 then begin
       if ex_str <> '' then begin
   	    Query_Log_Str(fullsql+'('+ex_str+')', MilliSecondsBetween(now, t1), (result>=0));
       end else begin
   	    Query_Log_Str(fullsql, MilliSecondsBetween(now, t1), (result>=0));
       end;
   end;
end;


{
function GetNextBcCode(tablanev, mezonev : string) : string;
var
	Query1		: TADOQuery;
	ujkodszam	: integer;
  str			: string;
begin
	// Visszaadja a k�vetkez� k�dot
  	Query1	:= TADOQuery.Create(nil);
	if Pos( tablanev , DAT_TABLAK ) > 0 then begin
  		Query1.Tag := 1;
  	end;
  	EgyebDlg.SeTADOQueryDatabase(Query1);
  Query1.CommandTimeout:= EgyebDlg.SQL_TIMEOUT;
	Result 		:= '';
 	ujkodszam	:= 1;
	if not Query_Run(Query1, 'SELECT MAX('+mezonev+') MAXKOD FROM '+tablanev) then begin
	Query1.Destroy;
		Exit;
	end;
	if Query1.RecordCount > 0 then begin
		ujkodszam	:= StrToIntDef(copy(Query1.FieldByname('MAXKOD').AsString,8,5),0) + 1;
	end;
	str		:= copy(Query1.FieldByname('MAXKOD').AsString,1,7) + Format('%5.5d',[ujkodszam]);
	Result	:= str + IntToStr(GetBcLastDigit(str,StrToIntDef(ReadIniValue('VONALKOD','CODETYPE'),1)));
	Query1.Close;
	Query1.Destroy;
end;
}

procedure SzotarTolt(Combo : TCustomComboBox; alkod : string; kodlist : TStringList; kodkell : boolean;
    kellures : boolean = false; sorrend_menev: boolean = false);
var
	Query1		: TADOQuery;
  S: string;
begin
	Query1		:= TADOQuery.Create(nil);
	EgyebDlg.SeTADOQueryDatabase(Query1);
	Combo.Clear;
	kodlist.Clear;
	if kellures then begin
		Combo.Items.Add(' ');
		kodlist.Add(' ');
	end;
  S:= 'SELECT SZ_ALKOD, SZ_MENEV, SZ_ERVNY FROM SZOTAR WHERE SZ_FOKOD='''+alkod+''' '+
		' AND SZ_ERVNY = 1 ORDER BY ';
  if sorrend_menev then
    S:= S+ ' SZ_MENEV '
  else S:= S+ ' SZ_ALKOD ';

	if Query_Run(Query1, S) then begin
		while not Query1.EOF do begin
			if kodkell then begin
				// Nem a megnevez�s, hanem a k�d ker�l bele a list�ba -> pl. gk. kateg�ria, orsz�g
				Combo.Items.Add(Query1.FieldByName('SZ_ALKOD').AsString);
			end else begin
				Combo.Items.Add(Query1.FieldByName('SZ_MENEV').AsString);
			end;
			kodlist.Add(Query1.FieldByName('SZ_ALKOD').AsString);
			Query1.Next;
		end;
		if Combo.Items.Count > 0 then begin
			Combo.ItemIndex	:= 0;
		end;
	end;
	Query1.Destroy;
end;

procedure SzotarTolt_Multi(CheckListBox : TCheckListBox; alkod : string; kodlist : TStringList; kodkell : boolean;
    sorrend_menev: boolean = false);
var
	Query1: TADOQuery;
  S: string;
begin
	Query1		:= TADOQuery.Create(nil);
	EgyebDlg.SeTADOQueryDatabase(Query1);
  CheckListBox.Items.Clear;
	kodlist.Clear;
  S:= 'SELECT SZ_ALKOD, SZ_MENEV, SZ_ERVNY FROM SZOTAR WHERE SZ_FOKOD='''+alkod+''' '+
		' AND SZ_ERVNY = 1 ORDER BY ';
  if sorrend_menev then
    S:= S+ ' SZ_MENEV '
  else S:= S+ ' SZ_ALKOD ';
	if Query_Run(Query1, S) then begin
		while not Query1.EOF do begin
      kodlist.Add(Query1.FieldByName('SZ_ALKOD').AsString);
      if kodkell then 	// Nem a megnevez�s, hanem a k�d ker�l bele a list�ba -> pl. gk. kateg�ria, orsz�g
        CheckListBox.Items.Add(Query1.FieldByName('SZ_ALKOD').AsString)
      else
        CheckListBox.Items.Add(Query1.FieldByName('SZ_MENEV').AsString);
      Query1.Next;
	    end;  // while
		end;  // if Query_run
	Query1.Destroy;
end;

procedure FlowPanelTolt(FlowPanel : TFlowPanel; alkod : string; kodlist : TStringList; kodkell : boolean;
    sorrend_menev: boolean = false);
var
	Query1: TADOQuery;
  S: string;
  // ***** MyCanvas: TCanvas;
begin
{
	Query1		:= TADOQuery.Create(nil);
	EgyebDlg.SeTADOQueryDatabase(Query1);
  while FlowPanel.ControlCount > 0 do FlowPanel.Controls[0].Free;  // az �sszes al-panel t�rl�se
	kodlist.Clear;
  S:= 'SELECT SZ_ALKOD, SZ_MENEV, SZ_ERVNY FROM SZOTAR WHERE SZ_FOKOD='''+alkod+''' '+
		' AND SZ_ERVNY = 1 ORDER BY ';
  if sorrend_menev then
    S:= S+ ' SZ_MENEV '
  else S:= S+ ' SZ_ALKOD ';
	if Query_Run(Query1, S) then begin
		while not Query1.EOF do begin
      kodlist.Add(Query1.FieldByName('SZ_ALKOD').AsString);
      with TCheckBox.Create(FlowPanel) do begin
        Parent:= FlowPanel;
        Tag:= kodlist.Count-1;  //
  			if kodkell then 	// Nem a megnevez�s, hanem a k�d ker�l bele a list�ba -> pl. gk. kateg�ria, orsz�g
          Caption:= Query1.FieldByName('SZ_ALKOD').AsString
			  else
  				Caption:= trim(Query1.FieldByName('SZ_MENEV').AsString);
        Canvas.Font := Font;
        Width := Canvas.TextWidth(Caption) + 20;
	  		Query1.Next;
        end;  // with
	    end;  // while
		end;  // if Query_run
	Query1.Destroy;
  }
end;

procedure SzotarTolt_CsakLista(Combo : TCustomComboBox; alkod : string);
var
	Query1		: TADOQuery;
  S: string;
begin
	Query1		:= TADOQuery.Create(nil);
	EgyebDlg.SeTADOQueryDatabase(Query1);
	Combo.Clear;
  S:= 'SELECT SZ_ALKOD, SZ_MENEV, SZ_ERVNY FROM SZOTAR WHERE SZ_FOKOD='''+alkod+''' '+
		' AND SZ_ERVNY = 1 ORDER BY SZ_MENEV ';

	if Query_Run(Query1, S) then begin
		while not Query1.EOF do begin
			Combo.Items.Add(Query1.FieldByName('SZ_MENEV').AsString);
			Query1.Next;
		end;
		if Combo.Items.Count > 0 then begin
			Combo.ItemIndex	:= 0;
		end;
	end;
	Query1.Destroy;
end;


procedure DistinctTolt(Combo : TCustomComboBox; TablaNev, SelectSQL : string; kodlist : TStringList; kellures : boolean = false);
var
	Query1		: TADOQuery;
begin
	Query1		:= TADOQuery.Create(nil);
 	if Pos(TablaNev, DAT_TABLAK ) > 0 then begin
 		Query1.Tag := 1;
  	end;
	EgyebDlg.SeTADOQueryDatabase(Query1);
	Combo.Clear;
	if kodlist<>nil then kodlist.Clear;
	if kellures then begin
		Combo.Items.Add(' ');
		if kodlist<>nil then kodlist.Add(' ');
	end;
	if Query_Run(Query1, selectSQL) then begin
		while not Query1.EOF do begin
			Combo.Items.Add(Query1.Fields[0].AsString); // a select list�ban az els� elem
			if kodlist<>nil then kodlist.Add(Query1.Fields[0].AsString);
			Query1.Next;
		end;
		if Combo.Items.Count > 0 then begin
			Combo.ItemIndex	:= 0;
		end;
	end;
	Query1.Destroy;
end;

procedure ModLocate (Query : TADOQuery; fname, source : string );
begin
  try
 		Query.Close;
 		Query.Open;
       Query.First;
 		Query.Locate(fname, source, [] );
  except
  end;
end;

function EllenorzottJarat(kodstr : string; notice: boolean=True) : integer;
var
	Query1	: TADOQuery;
begin
	Query1		:= TADOQuery.Create(nil);
	Query1.Tag  := 1;
	EgyebDlg.SeTADOQueryDatabase(Query1);
	{Ellen�rizz�k, hogy a j�rat ellen�rz�tt-e}
	Query_Run (Query1, 'SELECT JA_FAZIS FROM JARAT WHERE JA_KOD = '''+ kodstr +''' ', true);
	if (Query1.FieldByname('JA_FAZIS').AsString = '1')and notice then begin
		NoticeKi('A j�rat m�r ellen�rz�tt!');
	end;
	if (Query1.FieldByname('JA_FAZIS').AsString = '2')and notice then begin
		NoticeKi('A j�rat storn�zott!');
	end;
	Result	:= StrToIntDef(Query1.FieldByname('JA_FAZIS').AsString, 0);
	Query1.Destroy;
end;

function EllenorzottJarat2(alkodstr,ev : string) : integer;
var
	Query1	: TADOQuery;
begin
	Query1		:= TADOQuery.Create(nil);
	Query1.Tag  := 1;
	EgyebDlg.SeTADOQueryDatabase(Query1);
 //	{Ellen�rizz�k, hogy a j�rat ellen�rz�tt-e}
	Query_Run (Query1, 'SELECT JA_FAZIS FROM JARAT WHERE JA_ALKOD = '''+ alkodstr +''' '+'and SUBSTRING(ja_jvege,1,4)='''+ev+''' ', true);
//	Query_Run (Query1, 'SELECT JA_FAZIS FROM JARAT WHERE JA_KOD = '''+ alkodstr +''' ', true);
	if Query1.FieldByname('JA_FAZIS').AsString = '1' then begin
		NoticeKi('A j�rat m�r ellen�rz�tt!');
	end;
	if Query1.FieldByname('JA_FAZIS').AsString = '2' then begin
		NoticeKi('A j�rat storn�zott!');
	end;
	Result	:= StrToIntDef(Query1.FieldByname('JA_FAZIS').AsString, 0);
	Query1.Destroy;

end;

function EllenorzottJarat3(jaratkod: string) : integer;
var
	Query1	: TADOQuery;
begin
	Query1		:= TADOQuery.Create(nil);
	Query1.Tag  := 1;
	EgyebDlg.SeTADOQueryDatabase(Query1);
 //	{Ellen�rizz�k, hogy a j�rat ellen�rz�tt-e}
	Query_Run (Query1, 'SELECT JA_FAZIS FROM JARAT WHERE JA_KOD = ''' + jaratkod+ '''', true);
	if Query1.FieldByname('JA_FAZIS').AsString = '1' then begin
		NoticeKi('A j�rat m�r ellen�rz�tt!');
	end;
	if Query1.FieldByname('JA_FAZIS').AsString = '2' then begin
		NoticeKi('A j�rat storn�zott!');
	end;
	Result	:= StrToIntDef(Query1.FieldByname('JA_FAZIS').AsString, 0);
	Query1.Destroy;

end;


{function 	GetUjszamla : string;
var
	Query1		: TADOQuery;
  elotag		: string;
  elotaghossz: integer;
begin
	Result		:= '';
	Query1		:= TADOQuery.Create(nil);
	Query1.Tag  := 1;
	elotag		:=   EgyebDlg.Read_SZGrid('CEG', '310');
  elotaghossz:= Length(elotag);
	EgyebDlg.SeTADOQueryDatabase(Query1);
	if Query_Run(Query1, 'SELECT SA_KOD FROM SZFEJ WHERE SA_KOD IS NOT NULL ORDER BY SA_KOD', true) then begin
		if Query1.RecordCount < 1 then begin
			Result := elotag + '00001';
		end else begin
			Query1.Last;
			Result := elotag + Format('%5.5d',[StrToIntDef(copy(Query1.FieldByName('SA_KOD').AsString,elotaghossz+1,5),0) + 1]);
		end;
	end;
	if Query_Run(Query1, 'SELECT SA_KOD FROM SZFEJ2 ORDER BY SA_KOD', true) then begin
		if Query1.RecordCount > 0 then begin
			Query1.Last;
			if Result <= Query1.FieldByName('SA_KOD').AsString then begin
				Result := elotag + Format('%5.5d',[StrToIntDef(copy(Query1.FieldByName('SA_KOD').AsString,elotaghossz+1,5),1) + 1]);
			end;
		end;
	end;
	Query1.Destroy;
end;
}

function 	GetUjszamla2(ev: string) : string;
var
	Query1		: TADOQuery;
  elotag,last_kod, S: string;
  elotaghossz: integer;
  hiba: boolean;
begin
	Result		:= '';
	// Query1		:= TADOQuery.Create(nil);
	// Query1.Tag  := 1;
	// EgyebDlg.SeTADOQueryDatabase(Query1);
	elotag		:=   EgyebDlg.Read_SZGrid('CEG', '310');
  if ev<>'' then  begin
    if Length(elotag)=3  then  // pl. 131
      elotag:=ev+copy(elotag,3,1)
    else
      elotag:=ev+elotag;
  end;

  elotaghossz:= Length(elotag);

	// if Query_Run(Query1, 'select max(sa_kod) max from SZFEJ where SUBSTRING(sa_kod,1,3)='+elotag, true) then begin
	//		last_kod:= elotag + Format('%5.5d',[StrToIntDef(copy(Query1.FieldByName('max').AsString,elotaghossz+1,5),0) + 0]);
	//		Result  := elotag + Format('%5.5d',[StrToIntDef(copy(Query1.FieldByName('max').AsString,elotaghossz+1,5),0) + 1]);
  // end;

  // 2017-Dec-t�l
  Result := IntToStr(GetNewIDLoop(modeGetNextStrCode, 'SZFEJ', 'SA_KOD', elotag, 0, 8, True));

  hiba:=False;
  if (copy(last_kod,elotaghossz+1,5)<>'00000')and(Query_Select('SZFEJ','SA_KOD',last_kod,'SA_KOD')<>last_kod) then       // nincs olyan sz�mlasz�m amit el�bb az utols�nak olvasott ki
    hiba:=True;
  if Query_Select('SZFEJ','SA_KOD',Result,'SA_KOD')=Result then            // van olyan sz�mlasz�m amit kiosztana az SZFEJ-ben
    hiba:=True;
  if Query_Select('SZFEJ2','SA_KOD',Result,'SA_KOD')=Result then           // van olyan sz�mlasz�m amit kiosztana az SZFEJ2-ben
    hiba:=True;
  if hiba then begin
    S:= 'Hib�s sz�mlasz�m! ('+last_kod+';'+result+')'+#13+#10+'A m�velet megszak�tva!';
    // MessageBox(0, Pchar('Hib�s sz�mlasz�m! ('+last_kod+';'+result+')'+#13+#10+'A m�velet megszak�tva!'), 'Sz�mlasz�m kioszt�s', MB_ICONERROR or MB_OK);
    HibaKiiro(S);
    NoticeKi(S);
    Result:='';
  end;
	// Query1.Destroy;
end;

procedure 	HibaKiiro(uzenet : string);
var
   uz2         : string;
begin
   if ex_str <> '' then begin
       uz2 := GetServerdateTime(2)+ ' (' + EgyebDlg.user_code + ') ' + uzenet+' ('+ex_str+') '+IntToStr(ex_num)+' ['+SW_VERZIO+']';
   end else begin
       uz2 := GetServerdateTime(2)+ ' (' + EgyebDlg.user_code + ') ' + uzenet+' ['+SW_VERZIO+']';
   end;
   // TLogThread.CreateNew(EgyebDlg.TempList + 'sql_hiba.txt', uz2);
   TLogThread.CreateNew(EgyebDlg.NaploPath + 'sql_hiba.txt', uz2);
end;

procedure  NaploKiiro(uzenet : string);
begin
  Query_Log_Str(uzenet, 0, true);
end;


function 	PotosUtkozes(jarat : string) : boolean;
var
	Query1	: TADOQuery;
	oldkm	: integer;
begin
	// Ellen�rizz�k, hogy a megadott j�ratban a felvitt p�tkocsis kilom�terekn�l nincs-e �tk�z�s
	Result			:= false;
	Query1			:= TADOQuery.Create(nil);
	Query1.Tag  	:= 1;
	EgyebDlg.SeTADOQueryDatabase(Query1);
	Query_Run(Query1, 'SELECT * FROM JARPOTOS WHERE JP_JAKOD = '+jarat+' ORDER BY JP_PORKM ');
	oldkm:= StrToIntDef(Query1.FieldByName('JP_PORK2').AsString,0);
	Query1.Next;
	while not Query1.Eof do begin
		if StrToIntDef(Query1.FieldByName('JP_PORKM').AsString,0) < oldkm then begin
			NoticeKi('�tk�z�s az eddigi �s az �j p�tkocsis km-ek k�z�tt!  '+Query1.FieldByName('JP_POREN').AsString+'; '+
				Query1.FieldByName('JP_PORKM').AsString);
			Result			:= true;
		end;
		Query1.Next;
	end;
	Query1.Destroy;
end;

procedure	SofornevKepzes(rensz : string);
var
	QueryK1    	: TADOQuery;
	sofornev,sbelep    : string;
begin
   // A megadott rendsz�mhoz be�rja a hozz� tartoz� sof�r�ket
  	QueryK1			:= TADOQuery.Create(nil);
  	QueryK1.Tag  	:= 0;
  	EgyebDlg.SeTADOQueryDatabase(QueryK1);
   sofornev		:= '';
   if rensz <> '' then begin
       Query_Run(QueryK1, 'SELECT DO_NAME,DO_BELEP FROM DOLGOZO WHERE DO_RENDSZ = '''+rensz+''' AND DO_ARHIV = 0 ' );
		while not QueryK1.Eof do begin
           sofornev	:= sofornev + QueryK1.FieldByName('DO_NAME').AsString + ', ' ;
           sbelep	:= sbelep + QueryK1.FieldByName('DO_BELEP').AsString + ', ' ;
           QueryK1.Next;
       end;
   end;
   Query_Run(QueryK1, 'UPDATE GEPKOCSI SET GK_SONEV = '''+copy(sofornev, 1, 80)+''' WHERE GK_RESZ = '''+rensz+''' ' );
   Query_Run(QueryK1, 'UPDATE GEPKOCSI SET GK_SOBEL = '''+copy(sbelep, 1, 80)+''' WHERE GK_RESZ = '''+rensz+''' ' );
end;

// N�v alapj�n friss�ti a sof�r oszlopot az adatb�zisban
procedure	SofornevFrissites(nev, rensz : string);
var
   QueryK1    	: TADOQuery;
   sofornev    : string;
begin
   // A megadott rendsz�mhoz be�rja a hozz� tartoz� sof�r�ket
  	QueryK1			:= TADOQuery.Create(nil);
  	QueryK1.Tag  	:= 0;
  	EgyebDlg.SeTADOQueryDatabase(QueryK1);
   sofornev		:= '';
	Query_Run(QueryK1, 'SELECT GK_RESZ FROM GEPKOCSI WHERE GK_SONEV LIKE ''%'+nev+'%'' ' );
   while not QueryK1.Eof do begin
       SoforNevkepzes(QueryK1.FieldByName('GK_RESZ').AsString);
       QueryK1.Next;
   end;
   SoforNevkepzes(rensz);
   QueryK1.Destroy;
end;

function	SoforUtkozes(jkod, sofkod, km1, km2 : string) : string;
begin
	Result	:= '';
	// Itt kell ellen�rizni, hogy egy j�ratban az adott sof�rh�z tartoz� adatok �tk�z�s n�lk�l felvihet�k-e!!!
end;


{function	GetApehUzemanyag(dat : string; tipus : integer = 0) : double;
begin
   Result:=GetApehUzemanyag(dat, tipus, FALSE);
end;
}

function GetApehUzemanyag(dat: string; tipus: integer = 0; korabbi_is_jo: boolean = false; csendes: boolean = false) : double;
var
	QueryK1    	: TADOQuery;
	korrek		: double;
  S: string;
begin
	Result			:= 0;
	QueryK1			:= TADOQuery.Create(nil);
	QueryK1.Tag  	:= 0;
	EgyebDlg.SeTADOQueryDatabase(QueryK1);
  if dat='' then begin  // a legut�bbit hozza
    S:= 'SELECT * FROM UZEMANYAG WHERE UA_DATKI= (select MAX(UA_DATKI) from UZEMANYAG)';
    end
  else begin
    if korabbi_is_jo then begin
       S:= 'SELECT * FROM UZEMANYAG WHERE UA_DATUM <= ''' + dat + ''' AND (UA_DATKI >= ''' + dat + '''  or UA_DATKI= (select MAX(UA_DATKI) from UZEMANYAG))';
       end
    else begin
       S:= 'SELECT * FROM UZEMANYAG WHERE UA_DATUM <= ''' + dat + ''' AND UA_DATKI >= ''' + dat + '''  ';
       end;
    end;
	Query_Run(QueryK1, S);
	if QueryK1.RecordCount > 0 then begin
		QueryK1.Last;
		Result	:= StringSzam(QueryK1.FieldByName('UA_ERTEK').AsString);
		if tipus = 1 then begin
			// �rv�nyes�teni kell az �zemanyag korrekci�s %-ot
			korrek := StringSZam(EgyebDlg.Read_SZGrid('999','741'));
			if korrek <> 0 then begin
				Result	:= (1 - (korrek/100)) * Result;
			  end;
  		end;
  	end
  else begin
    if not csendes then
			NoticeKi('NINCS �zemanyag�r! A keresett d�tum: '+dat+', t�pus: '+IntToStr(tipus)+' Kor�bbi lehet? '+BoolToStr(korabbi_is_jo) );
    end;
end;

function	GetUzemanyagPotlekSz(vekod : string; uzemanyagar : double; tedat:string; legyen_notice: boolean) : double;      // KG 20111219    UZEMFIX miatt ; Havi �zemanyagp�tl�kok
var
	QueryK1,QueryU     	: TADOQuery;
begin
	Result			:= 0;
	QueryK1			:= TADOQuery.Create(nil);
	QueryK1.Tag  	:= 0;
	EgyebDlg.SeTADOQueryDatabase(QueryK1);
	Query_Run(QueryK1, 'SELECT V_UPTIP, V_UPERT FROM VEVO WHERE V_KOD = '''+vekod+''' ' );
	case StrToIntDef(QueryK1.FieldByName('V_UPTIP').AsString, 0) of
   	0 : // Nincs p�tl�k
       	Result	:= 0;
		1 :	// A t�bl�zatb�l olvassuk ki az �rt�ket
       	begin
               Query_Run(QueryK1, 'SELECT * FROM UZEMERT WHERE UA_VEKOD = '''+vekod+''' '+
                	'AND UA_USAV1 <= '+SqlSzamString(uzemanyagar,12,2)+ ' ' +
                   'AND UA_USAV2 >= '+SqlSzamString(uzemanyagar,12,2));
               if QueryK1.RecordCount > 0 then begin
                   Result	:= StringSzam(QueryK1.FieldByName('UA_UPERT').AsString);
               end;
           end;
       2 : // Konkr�t �rt�k
        begin
          if StringSzam(QueryK1.FieldByName('V_UPERT').AsString)<>0 then
           	Result	:= StringSzam(QueryK1.FieldByName('V_UPERT').AsString)   // ha nem nulla, akkor m�g a r�gi adat haszn�lata
          else
          begin
          	QueryU:= TADOQuery.Create(nil);
          	EgyebDlg.SeTADOQueryDatabase(QueryU);
          	Query_Run(QueryU, 'SELECT * FROM UZEMFIX WHERE UX_VEKOD = '''+vekod+''' and UX_EV='''+copy(tedat,1,4) +'''   ' );
            if QueryU.Eof then
            begin
              Result:=0;
              if legyen_notice then
                  showmessage('Fix �zemanyagp�tl�k 0. Ellen�rizze az adatokat!');
            end
            else
              if  (QueryU.FieldByName('UX_H'+copy(tedat,6,2)).IsNull)or(QueryU.FieldByName('UX_H'+copy(tedat,6,2)).IsNull) then
              begin
                Result:=0;
                if legyen_notice then
                   showmessage('Fix �zemanyagp�tl�k 0. Ellen�rizze az adatokat!');
              end
              else
                Result	:= QueryU.FieldByName('UX_H'+copy(tedat,6,2)).Value;
          end;
        end;
   end;
end;

{procedure	SoforTelefonFrissites(soforkod : string);
var
   QueryK1    	: TADOQuery;
   telsz		: string;
begin
  	QueryK1			:= TADOQuery.Create(nil);
  	QueryK1.Tag  	:= 0;
  	EgyebDlg.SeTADOQueryDatabase(QueryK1);
   Query_Run(QueryK1,  'SELECT * FROM TELEFON WHERE TE_TULAJ = '''+soforkod+''' ' );
   telsz	:= '';
   while not Queryk1.Eof do begin
      	telsz	:= telsz + QueryK1.FieldByName('TE_TSZAM').AsString+';';
       Queryk1.Next;
	end;
	Query_Run(QueryK1,  'UPDATE DOLGOZO SET DO_TELEF = '''+copy(telsz,1,40)+''' WHERE DO_KOD = '''+soforkod+''' ' );
	QueryK1.Destroy;
end;
 }

function	DolgozoMezoEllenor : boolean;
var
   QueryK1    	: TADOQuery;
   QueryK2    	: TADOQuery;
   mezonev		: string;
begin
	Result			:= false;
	QueryK1			:= TADOQuery.Create(nil);
  	QueryK1.Tag  	:= 0;
  	EgyebDlg.SeTADOQueryDatabase(QueryK1);
  	QueryK2			:= TADOQuery.Create(nil);
  	QueryK2.Tag  	:= 0;
  	EgyebDlg.SeTADOQueryDatabase(QueryK2);
   if not Query_Run(QueryK1,  'SELECT * FROM DOLGOZOMEG ', false ) then begin
   	if not Query_Run(QueryK1,  'CREATE TABLE DOLGOZOMEG ( DM_DOKOD CHAR(5) ) ', FALSE) then begin
       	Exit;
       end;
       if not Query_Run(QueryK1,  'INSERT INTO DOLGOZOMEG SELECT DO_KOD FROM DOLGOZO ', FALSE) then begin
       	Exit;
       end;
   end;
	// Beolvassuk a megadott mez�ket �s l�trehozzuk, ha m�g nem l�teznek
	Query_Run(QueryK1,  'SELECT * FROM SZOTAR WHERE SZ_FOKOD = ''360'' ' );
   while not QueryK1.Eof do begin
   	mezonev	:= Trim('DM_'+QueryK1.FieldByName('SZ_ALKOD').AsString);
   	if not Query_Run(QueryK2,  'SELECT '+mezonev+' FROM DOLGOZOMEG ', false ) then begin
       	// M�g nem l�tezik a mez�, l�tre kell hozni
	   		Query_Run(QueryK2,  'ALTER TABLE DOLGOZOMEG ADD '+mezonev+' CHAR(50)', FALSE);
       end;
   	QueryK1.Next;
   end;
	Result	:= true;
   QueryK1.Destroy;
   QueryK2.Destroy;
end;

function	NevKiiro(kodlista, elvalaszto : string) : string;
var
   QueryK1    	: TADOQuery;
   nevstr		: string;
   kodli		: TStringList;
   i			: integer;
begin
	nevstr		:= '';
   kodli		:= TStringList.Create;
   StringParse(kodlista, elvalaszto, kodli);
  	QueryK1			:= TADOQuery.Create(nil);
  	QueryK1.Tag  	:= 0;
  	EgyebDlg.SeTADOQueryDatabase(QueryK1);
	if Query_Run(QueryK1,  'SELECT JE_FEKOD, JE_FENEV FROM JELSZO', false ) then begin
   	for i := 0 to kodli.Count - 1 do begin
       	if QueryK1.Locate('JE_FEKOD', kodli[i], [] ) then begin
           	nevstr	:= nevstr + QueryK1.FieldByName('JE_FENEV').AsString + elvalaszto;
           end else begin
           	nevstr	:= nevstr + kodli[i] + '?' + elvalaszto;
           end;
       end;
   end;
   kodli.Free;
   Result	:= nevstr;
   QueryK1.Free;
end;

function	GetFelrakoStatus(msid : string) : integer;
var
   QueryK1    	: TADOQuery;
begin
	// Megkeress�k a felrak�s/lerak�s st�tusz�t
   Result			:= 0;
  	QueryK1			:= TADOQuery.Create(nil);
  	QueryK1.Tag  	:= 1;
  	EgyebDlg.SeTADOQueryDatabase(QueryK1);
   // if Query_Run(QueryK1,  'SELECT * FROM MEGSEGED, MEGBIZAS WHERE MS_MBKOD = MB_MBKOD AND MS_MSKOD = '+mskod, false ) then begin
   if Query_Run(QueryK1,  'SELECT * FROM MEGSEGED, MEGBIZAS WHERE MS_MBKOD = MB_MBKOD AND MS_ID = '+msid, false ) then begin
       if QueryK1.RecordCount > 0 then begin
           Result		:= GetFelrakoDbStatus( QueryK1);
       end;
	end;
   QueryK1.Free;
end;

function	GetFelrakoDbStatus( query : TADOQuery) : integer;
var
   QueryK1    	: TADOQuery;
   v_export	: boolean;
begin
	// Be�ll�tjuk a felrak�s/lerak�s st�tusz�t
   Result			:= 0;
   v_export 	:= false;
   if query.FieldByName('MB_EXPOR').AsString = '1' then begin
       v_export 	:= true;
   end;
	// Felrak�sr�l van sz�
   if not v_export then begin
       if Query.FieldByName('MS_FETIDO').AsString <> '' then begin
           // import - felrakott
           Result	:= 230;
       end;
       if Query.FieldByName('MS_LETIDO').AsString <> '' then begin
           // import - lerakott
           Result	:= 240;
       end;
   end else begin
       if Query.FieldByName('MS_FETIDO').AsString <> '' then begin
			// export - felrakott
           Result	:= 130;
       end;
       if Query.FieldByName('MS_LETIDO').AsString <> '' then begin
           // export - lerakott
           Result	:= 140;
       end;
   end;
   if Result > 0 then begin
   	Exit;
	end;
  	QueryK1			:= TADOQuery.Create(nil);
  	QueryK1.Tag  	:= 1;
  	EgyebDlg.SeTADOQueryDatabase(QueryK1);
   Query_Run(QueryK1,  'SELECT COUNT(*) DARAB FROM MEGLEVEL WHERE ML_MBKOD = '+Query.FieldByName('MB_MBKOD').AsString, false );
   if StrToIntDef(QueryK1.FieldByName('DARAB').AsString, 0) > 0 then begin
		if not v_export then begin
           // import - leadott
           Result	:= 220;
       end else begin
           // export - leadott
           Result	:= 120;
       end;
   end;
   if Result = 0 then begin
       if not v_export then begin
           // import - szabad
			Result	:= 210;
       end else begin
           // export - szabad
           Result	:= 110;
       end;
   end;
   QueryK1.Free;
end;

function	SetFelrakoStatus(msid : string; statusz : integer) : boolean;
var
   QueryK1    	: TADOQuery;
begin
  	QueryK1			:= TADOQuery.Create(nil);
  	QueryK1.Tag  	:= 1;
  	EgyebDlg.SeTADOQueryDatabase(QueryK1);
	Result	:= Query_Update (QueryK1, 'MEGSEGED', [
					'MS_STATK', IntToStr(statusz),
       			'MS_STATN', ''''+copy(EgyebDlg.Read_SZGrid('960',IntToStr(statusz)), 1, 20)+''''
       			], ' WHERE MS_ID = '+msid );
   QueryK1.Free;
end;

function GetTypename (ftype : TFieldType): string;
begin
	Result	:= '';
   case ftype of
		ftString, ftFixedChar, ftWideString:
			Result	:= 'A';
  		ftInteger	:
			Result	:= 'I';
     	ftFloat , ftBCD:
			Result	:= 'N';
   	ftBoolean :
			Result	:= 'B';
   	ftMemo :
			Result	:= 'M';
   	ftDate, ftDateTime :
			Result	:= 'D';
   end;
end;

function Create_Table_Str ( tablename : string; Query : TAdoQuery ) : string;
var
	str			: string;
	i			: integer;
   mezostr  	: string;
   mezotipus	: string;
   hossz		: integer;
begin
	str	:= 'CREATE TABLE '+tablename + ' ( ';
	// A megfelel� parancs besz�r�sa a memo-ba
  	mezostr	:= '';
	for i := 0 to Query.FieldCount - 1 do begin
    	mezostr 		:= UpperCase(Query.FieldDefs[i].Name);
     	mezotipus 		:= GetTypename(Query.FieldDefs[i].DataType);
		if mezotipus = 'A' then begin
     		// Karakteres mez�
        	str := str + mezostr + ' VARCHAR('+IntToStr(Query.FieldDefs[i].Size)+'), ';
     	end;
     	if mezotipus = 'M' then begin
     		// Memo mez�
        	str := str + mezostr + ' VARCHAR(250), ';
     	end;
     	if mezotipus = 'N' then begin
     		// Numerikus mez�
           hossz	:= Query.FieldDefs[i].Precision;
           if hossz < 1 then begin
           	hossz	:= 10;
           end;
        	str := str + mezostr + ' NUMERIC('+IntToStr(hossz)+','+IntToStr(Query.FieldDefs[i].Size)+'), ';
     	end;
     	if mezotipus = 'I' then begin
			// Integer mez�
        	str := str + mezostr + ' INTEGER, ';
     	end;
     	if mezotipus = 'B' then begin
     		// Logikai mez�
        	str := str + mezostr + ' INTEGER, ';
     	end;
		if mezotipus = 'D' then begin
     		// D�tum mez�
        	str := str + mezostr + ' DATE, ';
     	end;
   end;
	str := copy (str, 1, Length(str) - 2) + ' ) ';
  	Result	:= str;
end;

procedure 	QueryExport (query : TAdoQuery);
begin
	// Tetsz�leges grid kiexport�l�sa valamibe
	Application.CreateForm(TExportDlg, ExportDlg);
	ExportDlg.Tolto('Query export',query);
	ExportDlg.ShowModal;
	ExportDlg.Destroy;
end;

procedure 	QueryExportToFile (query : TAdoQuery; fn : string);
begin
	// Tetsz�leges grid kiexport�l�sa valamibe
	Application.CreateForm(TExportDlg, ExportDlg);
	ExportDlg.Tolto('Query export',query);
	ExportDlg.SilentXls(fn);
	ExportDlg.Destroy;
end;

function GetSzamlaValuta( szujkod : string) : string;
var
	Query1	: TADOQuery;
begin
	Result		:= '';
	Query1		:= TADOQuery.Create(nil);
	Query1.Tag  := 1;
	EgyebDlg.SetAdoQueryDatabase(Query1);
	if Query_Run(Query1, 'SELECT DISTINCT SS_VALNEM FROM SZSOR WHERE SS_VALNEM <> '''' AND SS_UJKOD = '+szujkod, true) then begin
		if Query1.RecordCount > 1 then begin
			Result := '';
		end else begin
			Result := Query1.FieldByName('SS_VALNEM').AsString;
		end;
	end;
	Query1.Destroy;
end;

function	GetHutoOsszeg(rendsz, uz1, uz2 : string) : double;
var
	Query1	: TADOQuery;
	osszeg	: double;
	kezdokm	: double;
	vegzokm	: double;
begin
	// A h�t� tankol�s kisz�m�t�sa egy adott g�pkocsihoz
	Result	:= 0;
	Query1		:= TADOQuery.Create(nil);
	Query1.Tag  := 1;
	EgyebDlg.SetAdoQueryDatabase(Query1);
	if Query_Run(Query1, 'SELECT * FROM KOLTSEG WHERE KS_TIPUS = 3 AND KS_RENDSZ = '''+rendsz+''' AND '+SqlSzamString(stringszam(uz1), 12,2)+' >= KS_KMORA ORDER BY KS_KMORA', true) then begin
		if Query1.RecordCount > 1 then begin
			Query1.Last;
			kezdokm	:= StringSzam(Query1.FieldByName('KS_KMORA').AsString);
			Query_Run(Query1, 'SELECT * FROM KOLTSEG WHERE KS_TIPUS = 3 AND KS_RENDSZ = '''+rendsz+''' AND KS_KMORA >= '+SqlSzamString(kezdokm, 12,2)+' ORDER BY KS_KMORA', true);
			Query1.First;
			osszeg	:= 0;
			Query1.Next;
			while ( ( not Query1.Eof ) and (StringSzam(uz2) > StringSzam(Query1.FieldByName('KS_KMORA').AsString) ) ) do begin

				osszeg	:= osszeg + StringSzam(Query1.FieldByName('KS_ERTEK').AsString);
				Query1.Next;
			end;
			osszeg	:= osszeg + StringSzam(Query1.FieldByName('KS_ERTEK').AsString);
			vegzokm	:= StringSzam(Query1.FieldByName('KS_KMORA').AsString);
			if vegzokm >= Stringszam(uz2) then begin
				if vegzokm <> kezdokm then begin
					Result	:= osszeg * (StringSzam(uz2)-StringSzam(uz1)) / (vegzokm - kezdokm);
				end;
			end;
		end;
	end;
	Query1.Destroy;
end;

function 	GetLocation(rsz : string; var poz : Rpozicio) : boolean;
var
	Query1	: TADOQuery;
begin
	Result			:= false;
	poz.location	:= '';
	poz.datum		:= '';
	poz.digit		:= '';
	poz.sebesseg	:= 0;
	if rsz = '' then begin
		Exit;
	  end;
	if query_select('GEPKOCSI', 'GK_RESZ', rsz, 'GK_NOPOS') = '1' then begin
		Exit;  // ehhez az aut�hoz nem mutatunk poz�ci�t
	  end;
	Query1			:= TADOQuery.Create(nil);
	Query1.Tag  	:= QUERY_KOZOS_TAG;
	EgyebDlg.SetAdoQueryDatabase(Query1);
	try
		Query_Run(Query1, 'SELECT * FROM POZICIOK WHERE PO_RENSZ = '''+rsz+''' ', FALSE);
		if Query1.RecordCount > 0 then begin
			poz.location	:= copy(Query1.FieldByName('PO_GEKOD').AsString,1,60);
			poz.datum		:= copy(Query1.FieldByName('PO_DATUM').AsString,1,20);
			poz.sebesseg	:= StrToIntDef(Query1.FieldByName('PO_SEBES').AsString,0);
			poz.digit		:= Query1.FieldByName('PO_DIGIT').AsString;
			if copy(poz.digit, 5, 1) = '0' then begin
				poz.digit	:= 'GYUJT�S BE';
			end else begin
				poz.digit	:= 'GYUJT�S KI';
			end;
			Result	:= true;
		end;
	except
	end;
	Query1.Destroy;
end;

procedure FillOrszagkod(jarat_alkod, datum : string);
var
	Query1	: TADOQuery;
begin
	// Felt�ltj�k a sz�mla j�rat�hoz tartoz� orsz�gk�dot
	Query1		:= TADOQuery.Create(nil);
	Query1.Tag  := QUERY_ADAT_TAG;
	EgyebDlg.SetAdoQueryDatabase(Query1);
	Query_Run(Query1, 'SELECT * FROM JARAT WHERE JA_ALKOD = '+IntToStr(StrToIntDef(jarat_alkod, -1))+
       ' AND JA_JKEZD LIKE '''+copy( datum, 1, 4 )+'%'' ', FALSE);
	if Query1.RecordCount > 0 then begin
		Query_Run(Query1, 'UPDATE SZFEJ SET SA_ORSZA = '''+Query1.FieldByName('JA_ORSZ').AsString+''' WHERE SA_JARAT = '''+jarat_alkod+''' '+
           ' AND SA_DATUM LIKE '''+copy( datum, 1, 4 )+'%'' ', FALSE);
	end else begin
		Query_Run(Query1, 'UPDATE SZFEJ SET SA_ORSZA = '''+''+''' WHERE SA_JARAT = '''+jarat_alkod+''' '+
           ' AND SA_DATUM LIKE '''+copy( datum, 1, 4 )+'%'' ', FALSE);
	end;
	Query1.Destroy;
end;

function VanNyitottSzamla(jarat_kod : string) : boolean;
var
	Query1	: TADOQuery;
begin
	// Felt�ltj�k a sz�mla j�rat�hoz tartoz� orsz�gk�dot
	result		:= false;
	Query1		:= TADOQuery.Create(nil);
	Query1.Tag  := QUERY_ADAT_TAG;
	EgyebDlg.SetAdoQueryDatabase(Query1);
	// Query_Run(Query1,'SELECT SA_UJKOD FROM SZFEJ WHERE SA_UJKOD IN (SELECT VI_UJKOD FROM VISZONY WHERE VI_JAKOD = '''+jarat_kod+''' AND SA_FLAG = 1 '+')',true);
  Query_Run(Query1,'SELECT SA_UJKOD FROM SZFEJ WHERE SA_UJKOD IN (SELECT VI_UJKOD FROM VISZONY WHERE VI_JAKOD = '''+jarat_kod+''') AND SA_FLAG <> 0 ',true);  // sztorn�zott se j�
	if Query1.RecordCount > 0 then begin
		Result	:= true;
	end;
	Query1.Destroy;
end;

function GetTableFullName(dbname, table_name : string) : string;
var
	Query1	: TADOQuery;
begin
	result		:= Trim(table_name);
	Query1		:= TADOQuery.Create(nil);
	Query1.Tag  := QUERY_ADAT_TAG;
	EgyebDlg.SetAdoQueryDatabase(Query1);
   try
	Query_Run(Query1,'select * from sys.databases where upper(name) = '''+UpperCase(dbname)+''' ' );
	if Query1.RecordCount > 0 then begin
		if Query_Run(Query1,'select * from '+dbname+'.information_schema.tables WHERE TABLE_NAME = '''+Trim(table_name)+''' ',true) then begin
			if Query1.RecordCount > 0 then begin
				Result	:= dbname+'.'+Query1.FieldByName('TABLE_SCHEMA').AsString+'.'+table_name;
			end;
		end;
	end;
   except

   end;
	Query1.Destroy;
end;

function GetTableSchema(dbname, table_name : string) : string;
var
	Query1	: TADOQuery;
begin
	result		:= '';
	Query1		:= TADOQuery.Create(nil);
	Query1.Tag  := QUERY_ADAT_TAG;
	EgyebDlg.SetAdoQueryDatabase(Query1);
	Query_Run(Query1,'select * from sys.databases where upper(name) = '''+UpperCase(dbname)+''' ' );
	if Query1.RecordCount > 0 then begin
		if Query_Run(Query1,'select * from '+dbname+'.information_schema.tables WHERE TABLE_NAME = '''+Trim(table_name)+''' ',true) then begin
			if Query1.RecordCount > 0 then begin
				Result	:= Query1.FieldByName('TABLE_SCHEMA').AsString;
			end;
		end;
	end;
	Query1.Destroy;
end;

function	GetJaratszam(jakod : string) : string;
var
	Query1	: TADOQuery;
begin
	// Felt�ltj�k a sz�mla j�rat�hoz tartoz� orsz�gk�dot
	Result		:= '';
	Query1		:= TADOQuery.Create(nil);
	Query1.Tag  := QUERY_ADAT_TAG;
	EgyebDlg.SetAdoQueryDatabase(Query1);
	Query_Run(Query1,'SELECT JA_ORSZ, JA_ALKOD FROM JARAT WHERE JA_KOD = '''+jakod+''' ',true);
	if Query1.RecordCount > 0 then begin
		Result	:= GetJaratszamFromDb(Query1);
	end;
	Query1.Destroy;
end;

function  GetJaratszamFromDb(QuerySource : TADOQuery) : string;
begin
	try
		Result	:= QuerySource.FieldByname('JA_ORSZ').AsString + '-' +
			   IntToStr( StrToIntDef(QuerySource.FieldByname('JA_ALKOD').AsString,0));
			//   Format('%5.5d',[StrToIntDef(QuerySource.FieldByname('JA_ALKOD').AsString,0)]);
	except
		Result	:= '';
	end;
end;

function  GetTelepiAr(dat : string; tipus : integer) : double;
var
	Query1	: TADOQuery;
begin
	Result		:= 0;
	Query1		:= TADOQuery.Create(nil);
	Query1.Tag  := QUERY_KOZOS_TAG;
	EgyebDlg.SetAdoQueryDatabase(Query1);
	Query_Run(Query1,'SELECT * FROM TETANAR WHERE TT_DATUM <= '''+dat+''' AND TT_TIPUS = '+IntToStr(tipus)
		+' ORDER BY TT_DATUM, TT_TTKOD',true);
	if Query1.RecordCount > 0 then begin
		Query1.Last;
		Result	:= StringSzam(Query1.FieldByName('TT_EGYAR').AsString);
	end;
	Query1.Destroy;
end;

function GepStatTolto(rsz : string) : boolean;
const
  UseLogThread = False;
var
	utdat		: string;
	utkod		: string;
	QueryUj1	: TADOQuery;
	QueryUj2	: TADOQuery;
	QueryUj3	: TADOQuery;
  MySQL: string;

  procedure FreeQueries;  // a t�bbsz�r�s kil�p�si pont miatt csin�ltam �gy
    begin
      QueryUj1.Free;
      QueryUj2.Free;
      QueryUj3.Free;
    end;

begin
	Result			:= false;
	if rsz = '' then begin
		Exit;
	end;
 try
	QueryUj1 		:= TADOQuery.Create(nil);
	QueryUj1.Tag  	:= QUERY_KOZOS_TAG;
	EgyebDlg.SeTADOQueryDatabase(QueryUj1);
	QueryUj2 		:= TADOQuery.Create(nil);
	QueryUj2.Tag  	:= QUERY_KOZOS_TAG;
	EgyebDlg.SeTADOQueryDatabase(QueryUj2);
	QueryUj3 		:= TADOQuery.Create(nil);
	QueryUj3.Tag  	:= QUERY_ADAT_TAG;
	EgyebDlg.SeTADOQueryDatabase(QueryUj3);
  MySQL:= 'SELECT * FROM GEPKOCSI WHERE GK_POTOS <> 1 AND GK_ARHIV = 0 AND GK_KIVON = 0 AND GK_KULSO = 0 AND GK_RESZ = '''+rsz+''' ';
  Query_Log_Str('Futtat: '+MySQL, 0, false, UseLogThread);
	Query_Run(QueryUj1, MySQL);
	if QueryUj1.RecordCount < 1 then begin
		Query_Run(QueryUj3, 'DELETE FROM T_GEPK WHERE TG_RENSZ = '''+rsz+''' ');
    // FreeQueries;  majd a "finally" blokkban lefut
		Exit;
	end;
	// Ha r�szrak� a g�pkocsi, akkor az utols� felrak�st keress�k meg, egy�bk�nt a legutols� lerak�st
	QueryUj2.Close;
	QueryUj2.Tag  	:= QUERY_ADAT_TAG;
	EgyebDlg.SeTADOQueryDatabase(QueryUj2);

  // 2018.05.17. NP: a GK_RESZR mez�t nem haszn�ljuk, kikommentezem ezt az �gat.
	if StrToIntDef(QueryUj1.FieldByName('GK_RESZR').AsString,0) = 1 then begin
		// A g�pkocsi r�szrak�, az utols� felrak�st jelen�tj�k meg t�nylegesre �s tervezettre is n�zz�k
		// Query_Run(QueryUj2, 'SELECT MS_MSKOD, MS_FETDAT, MS_FETIDO, MS_FELDAT, MS_FELIDO, MS_SNEV1 FROM MEGSEGED '+
    {
    MySQL:= 'SELECT top 50 MS_MSKOD, MS_FETDAT, MS_FETIDO, MS_FELDAT, MS_FELIDO, MS_SNEV1 FROM MEGSEGED '+
			' WHERE MS_RENSZ =  '''+QueryUj1.FieldByName('GK_RESZ').AsString+''' '+
			' AND MS_TIPUS = 0'+
			// ' ORDER BY MS_FETDAT, MS_FETIDO');
      ' ORDER BY MS_FETDAT desc, MS_FETIDO desc';
    Query_Log_Str('Futtat: '+MySQL, 0, false, UseLogThread);
    Query_Run(QueryUj2, MySQL);
		utdat	:= '';
		utkod	:= '';
		while not QueryUj2.Eof do begin
			if QueryUj2.FieldByName('MS_FETDAT').AsString + QueryUj2.FieldByName('MS_FETIDO').AsString >= utdat then begin
				utdat	:= QueryUj2.FieldByName('MS_FETDAT').AsString + QueryUj2.FieldByName('MS_FETIDO').AsString;
				utkod	:= QueryUj2.FieldByName('MS_MSKOD').AsString;
			  end; // if
			if QueryUj2.FieldByName('MS_FETIDO').AsString = '' then begin
				if QueryUj2.FieldByName('MS_FELDAT').AsString + QueryUj2.FieldByName('MS_FELIDO').AsString >= utdat then begin
					utdat	:= QueryUj2.FieldByName('MS_FELDAT').AsString + QueryUj2.FieldByName('MS_FELIDO').AsString;
					utkod	:= QueryUj2.FieldByName('MS_MSKOD').AsString;
				  end; // if
			  end;  // if
			QueryUj2.Next;
		  end; // while QueryUj2
		utkod	:= IntToStr(StrToIntDef(utkod,0));
		if utkod <> '0' then begin
      // ------ gyors�tsunk, nem kell m�nusz v�gtelent�l lek�rdezni
       //MySQL:= 'SELECT MS_LETDAT, MS_LETIDO, MS_LSULY, MS_LERCIM, MS_LERTEL, MS_LELIR, MS_ORSZA, MS_LERNEV, MS_TIPUS, MS_DATUM, MS_IDOPT, MS_FELNEV, MS_SNEV1, '+
       MySQL:= 'SELECT top 50 MS_LETDAT, MS_LETIDO, MS_LSULY, MS_LERCIM, MS_LERTEL, MS_LELIR, MS_ORSZA, MS_LERNEV, MS_TIPUS, MS_DATUM, MS_IDOPT, MS_FELNEV, MS_SNEV1, '+
				' MS_FORSZ, MS_FELIR, MS_FELTEL, MS_FELCIM, MS_FSULY, MS_FELARU, MS_FETDAT, MS_FETIDO, MS_MSKOD, MS_TEFNEV, MS_TEFOR, MS_TEFIR, MS_TEFTEL, MS_TEFCIM, MS_POTSZ '+
				' FROM MEGSEGED, MEGBIZAS WHERE MS_MBKOD = MB_MBKOD AND MS_MSKOD = '+utkod+
              ' ORDER BY MS_LETDAT desc, MS_LETIDO desc';
      Query_Log_Str('Futtat: '+MySQL, 0, false, UseLogThread);
      Query_Run(QueryUj2, MySQL);
			// A rendsz�m ellen�rz�se a g�pkocsi statisztik�ban
			Query_Run(QueryUj3, 'SELECT * FROM T_GEPK WHERE TG_RENSZ = '''+rsz+''' ');
			if QueryUj3.RecordCount < 1 then begin
        Query_Log_Str('Futtat: insert into T_GEPK (1) ...', 0, false, UseLogThread);
				Query_Insert( QueryUj3, 'T_GEPK',
				[
				'TG_RENSZ', ''''+rsz+''''
				]);
			  end; // if QueryUj3
      Query_Log_Str('Futtat: update T_GEPK (1) ...', 0, false, UseLogThread);
			Query_Update( QueryUj3, 'T_GEPK',
				[
				'TG_RENSZ', ''''+QueryUj1.FieldByName('GK_RESZ').AsString+'''',
				'TG_GKKAT', ''''+QueryUj1.FieldByName('GK_GKKAT').AsString+'''',
				'TG_RESZR', ''''+QueryUj1.FieldByName('GK_RESZR').AsString+'''',
				'TG_GKTIP', ''''+QueryUj1.FieldByName('GK_TIPUS').AsString+'''',
				'TG_TIPUS', ''''+'0'+'''',
				'TG_TINEV', ''''+'felrak�'+'''',
				'TG_DATUM', ''''+QueryUj2.FieldByName('MS_DATUM').AsString		+'''',
				'TG_IDOPT', ''''+QueryUj2.FieldByName('MS_IDOPT').AsString	+'''',
				'TG_TGNEV', ''''+QueryUj2.FieldByName('MS_FELNEV').AsString	+'''',
				'TG_TORSZ', ''''+QueryUj2.FieldByName('MS_FORSZ').AsString	+'''',
				'TG_TIRSZ', ''''+QueryUj2.FieldByName('MS_FELIR').AsString	+'''',
				'TG_TELEP', ''''+QueryUj2.FieldByName('MS_FELTEL').AsString	+'''',
				'TG_TGCIM', ''''+QueryUj2.FieldByName('MS_FELCIM').AsString	+'''',
				'TG_TENYN', ''''+QueryUj2.FieldByName('MS_TEFNEV').AsString	+'''',
				'TG_TENYO', ''''+QueryUj2.FieldByName('MS_TEFOR').AsString	+'''',
				'TG_TENYI', ''''+QueryUj2.FieldByName('MS_TEFIR').AsString	+'''',
				'TG_TENYT', ''''+QueryUj2.FieldByName('MS_TEFTEL').AsString	+'''',
				'TG_TENYC', ''''+QueryUj2.FieldByName('MS_TEFCIM').AsString	+'''',
				'TG_POTSZ', ''''+QueryUj2.FieldByName('MS_POTSZ').AsString	+'''',
				'TG_PSULY', SqlSzamString(StringSzam(QueryUj2.FieldByName('MS_FSULY').AsString), 12,2),
				'TG_GKARU', ''''+QueryUj2.FieldByName('MS_FELARU').AsString	+'''',
				'TG_TEDAT', ''''+QueryUj2.FieldByName('MS_FETDAT').AsString	+'''',
				'TG_TEIDO', ''''+QueryUj2.FieldByName('MS_FETIDO').AsString	+'''',
				// 'TG_MSKOD', QueryUj2.FieldByName('MS_MSKOD').AsString,
        'TG_MSKOD', IntToStr(StrToIntDef(QueryUj2.FieldByName('MS_MSKOD').AsString, 0)),
				'TG_SONEV', ''''+QueryUj2.FieldByName('MS_SNEV1').AsString	+''''
				], ' WHERE TG_RENSZ = '''+rsz+''' ' );
		  end; // if utkod <> '0'
      }
	  end  // if GK_RESZR = 1
  else begin
		// Nem r�szrak�, j�het az utols� lerak�s  t�nylegesre �s tervezettre is n�zz�k
    // ------ gyors�tsunk, nem kell m�nusz v�gtelent�l lek�rdezni
  	// Query_Run(QueryUj2, 'SELECT MS_MSKOD, MS_LETDAT, MS_LETIDO, MS_LERDAT, MS_LERIDO, MS_SNEV1 FROM MEGSEGED '+
    MySQL:= 'SELECT top 50 MS_MSKOD, MS_LETDAT, MS_LETIDO, MS_LERDAT, MS_LERIDO, MS_SNEV1 FROM MEGSEGED '+
			' WHERE MS_RENSZ =  '''+QueryUj1.FieldByName('GK_RESZ').AsString+''' '+
			' AND MS_TIPUS = 1'+
			// ' ORDER BY MS_LETDAT, MS_LETIDO');
      ' ORDER BY MS_LETDAT desc, MS_LETIDO desc';
    Query_Log_Str('Futtat: '+MySQL, 0, false, UseLogThread);
    Query_Run(QueryUj2, MySQL);
		utdat	:= '';
		utkod	:= '';
		while not QueryUj2.Eof do begin
			if QueryUj2.FieldByName('MS_LETDAT').AsString + QueryUj2.FieldByName('MS_LETIDO').AsString >= utdat then begin
				utdat	:= QueryUj2.FieldByName('MS_LETDAT').AsString + QueryUj2.FieldByName('MS_LETIDO').AsString;
				utkod	:= QueryUj2.FieldByName('MS_MSKOD').AsString;
			end;
			if QueryUj2.FieldByName('MS_LETIDO').AsString = '' then begin
				if QueryUj2.FieldByName('MS_LERDAT').AsString + QueryUj2.FieldByName('MS_LERIDO').AsString >= utdat then begin
					utdat	:= QueryUj2.FieldByName('MS_LERDAT').AsString + QueryUj2.FieldByName('MS_LERIDO').AsString;
					utkod	:= QueryUj2.FieldByName('MS_MSKOD').AsString;
				end;
			end;
			QueryUj2.Next;
		end;
		utkod	:= IntToStr(StrToIntDef(utkod,0));
		if utkod <> '0' then begin
      MySQL:= 'SELECT MS_LETDAT, MS_LETIDO, MS_LSULY, MS_LERCIM, MS_LERTEL, MS_LELIR, MS_ORSZA, MS_LERNEV, MS_TIPUS, MS_DATUM, MS_IDOPT, MS_FELNEV, MS_SNEV1, '+
				' MS_FORSZ, MS_FELIR, MS_FELTEL, MS_FELCIM, MS_FSULY, MS_FELARU, MS_FETDAT, MS_FETIDO, MS_MSKOD, MS_TENNEV, MS_TENOR, MS_TENIR, MS_TENTEL, MS_TENCIM, MS_POTSZ '+
				' FROM MEGSEGED, MEGBIZAS WHERE MS_MBKOD = MB_MBKOD AND MS_MSKOD = '+utkod;
      Query_Log_Str('Futtat: '+MySQL, 0, false, UseLogThread);
			Query_Run(QueryUj2, MySQL);
			// A rendsz�m ellen�rz�se a g�pkocsi statisztik�ban
			Query_Run(QueryUj3, 'SELECT * FROM T_GEPK WHERE TG_RENSZ = '''+rsz+''' ');
			if QueryUj3.RecordCount < 1 then begin
        Query_Log_Str('Futtat: insert into T_GEPK (2) ...', 0, false, UseLogThread);
				Query_Insert( QueryUj3, 'T_GEPK',
				[
				'TG_RENSZ', ''''+rsz+''''
				]);
			end;
      Query_Log_Str('Futtat: update T_GEPK (2) ...', 0, false, UseLogThread);
			Query_Update( QueryUj3, 'T_GEPK',
				[
				'TG_RENSZ', ''''+QueryUj1.FieldByName('GK_RESZ').AsString+'''',
				'TG_GKKAT', ''''+QueryUj1.FieldByName('GK_GKKAT').AsString+'''',
				'TG_RESZR', ''''+QueryUj1.FieldByName('GK_RESZR').AsString+'''',
				'TG_GKTIP', ''''+QueryUj1.FieldByName('GK_TIPUS').AsString+'''',
				'TG_TIPUS', ''''+'1'+'''',
				'TG_TINEV', ''''+'lerak�'+'''',
				'TG_DATUM', ''''+QueryUj2.FieldByName('MS_DATUM').AsString		+'''',
				'TG_IDOPT', ''''+QueryUj2.FieldByName('MS_IDOPT').AsString	+'''',
				'TG_TGNEV', ''''+QueryUj2.FieldByName('MS_LERNEV').AsString	+'''',
				'TG_TORSZ', ''''+QueryUj2.FieldByName('MS_ORSZA').AsString	+'''',
				'TG_TIRSZ', ''''+QueryUj2.FieldByName('MS_LELIR').AsString	+'''',
				'TG_TELEP', ''''+QueryUj2.FieldByName('MS_LERTEL').AsString	+'''',
				'TG_TGCIM', ''''+QueryUj2.FieldByName('MS_LERCIM').AsString	+'''',
				'TG_TENYN', ''''+QueryUj2.FieldByName('MS_TENNEV').AsString	+'''',
				'TG_TENYO', ''''+QueryUj2.FieldByName('MS_TENOR').AsString	+'''',
				'TG_TENYI', ''''+QueryUj2.FieldByName('MS_TENIR').AsString	+'''',
				'TG_TENYT', ''''+QueryUj2.FieldByName('MS_TENTEL').AsString	+'''',
				'TG_TENYC', ''''+QueryUj2.FieldByName('MS_TENCIM').AsString	+'''',
				'TG_POTSZ', ''''+QueryUj2.FieldByName('MS_POTSZ').AsString	+'''',
				'TG_PSULY', SqlSzamString(StringSzam(QueryUj2.FieldByName('MS_LSULY').AsString), 12,2),
				'TG_GKARU', ''''+QueryUj2.FieldByName('MS_FELARU').AsString	+'''',
				'TG_TEDAT', ''''+QueryUj2.FieldByName('MS_LETDAT').AsString	+'''',
				'TG_TEIDO', ''''+QueryUj2.FieldByName('MS_LETIDO').AsString	+'''',
				// 'TG_MSKOD', QueryUj2.FieldByName('MS_MSKOD').AsString,
        'TG_MSKOD', IntToStr(StrToIntDef(QueryUj2.FieldByName('MS_MSKOD').AsString, 0)),
				'TG_SONEV', ''''+QueryUj2.FieldByName('MS_SNEV1').AsString	+''''
				], ' WHERE TG_RENSZ = '''+rsz+''' ' );
		end;
	end;
	Result	:= true;
 finally
   FreeQueries;
 end;  // try-finally
end;

function PotStatTolto(rsz : string) : boolean;
const
  UseLogThread = False;
var
	QueryUj1	: TADOQuery;
	QueryUj2	: TADOQuery;
	QueryUj3	: TADOQuery;
	tinev		: string;
	geprsz		: string;
	reszrak		: string;

  procedure FreeQueries;  // a t�bbsz�r�s kil�p�si pont miatt csin�ltam �gy
    begin
      QueryUj1.Free;
      QueryUj2.Free;
      QueryUj3.Free;
      // SzabadMemoriaNaploz('PotStatTolto Queries FREE');
    end;

begin
	Result			:= false;
	if rsz = '' then begin
		Exit;
	end;
try
  // SzabadMemoriaNaploz('PotStatTolto 1');
	QueryUj1 		:= TADOQuery.Create(nil);
	QueryUj1.Tag  	:= QUERY_KOZOS_TAG;
	EgyebDlg.SeTADOQueryDatabase(QueryUj1);
	QueryUj2 		:= TADOQuery.Create(nil);
	QueryUj2.Tag  	:= QUERY_ADAT_TAG;
	EgyebDlg.SeTADOQueryDatabase(QueryUj2);
	QueryUj3 		:= TADOQuery.Create(nil);
	QueryUj3.Tag  	:= QUERY_ADAT_TAG;
	EgyebDlg.SeTADOQueryDatabase(QueryUj3);
  // SzabadMemoriaNaploz('PotStatTolto 2-Queries created');
  Query_Log_Str('Futtat: SELECT * FROM GEPKOCSI ...', 0, false, UseLogThread);
	Query_Run(QueryUj1, 'SELECT * FROM GEPKOCSI WHERE GK_POTOS = 1 AND GK_ARHIV = 0 AND GK_KIVON = 0 AND GK_KULSO = 0 AND GK_RESZ = '''+rsz+''' ');
	if QueryUj1.RecordCount < 1 then begin
    Query_Log_Str('Futtat: DELETE FROM T_POTK ...', 0, false, UseLogThread);
		Query_Run(QueryUj3, 'DELETE FROM T_POTK WHERE TP_POTSZ = '''+rsz+''' ');
    // FreeQueries;  majd a "finally" blokkban lefut
		Exit;
	end;
	Query_Run(QueryUj3, 'SELECT * FROM T_POTK WHERE TP_POTSZ = '''+rsz+''' ');
	if QueryUj3.RecordCount < 1 then begin
    Query_Log_Str('Futtat: INSERT INTO T_POTK (1)...', 0, false, UseLogThread);
		Query_Insert( QueryUj3, 'T_POTK',
		[
		'TP_POTSZ', ''''+rsz+''''
		]);
	end;

	// Megkeress�k az utols� elemet

  // ------ gyors�tsunk
	// Query_Run(QueryUj2, 'SELECT * '+
 	Query_Run(QueryUj2, 'SELECT top 1 * '+
		' FROM MEGSEGED, MEGBIZAS WHERE MS_MBKOD = MB_MBKOD '+
		' AND MS_POTSZ =  '''+rsz+''' '+
//		' AND MS_TIPUS = 0'+
 //		' ORDER BY MS_DATUM, MS_IDOPT');
 		' ORDER BY MS_DATUM desc, MS_IDOPT desc');
	// Megkeress�k az aktu�lis rekordot
	if QueryUj2.RecordCount > 0 then begin
		QueryUj2.Last;
		// Eld�ntj�k, hogy a g�pkocsi r�szrak�-e
		geprsz	:= QueryUj2.FieldByName('MS_RENSZ').AsString;
		reszrak	:= Query_Select('GEPKOCSI',  'GK_RESZ', geprsz, 'GK_RESZR' );
		if StrToIntDef(reszrak, 0) > 0 then begin
			// A g�pkocsi r�szrak�, az utols� felrak�st jelen�tj�k meg t�nylegesre �s tervezettre is n�zz�k
			// Query_Run(QueryUj2, 'SELECT * '+
      Query_Run(QueryUj2, 'SELECT top 1 * '+
				' FROM MEGSEGED, MEGBIZAS WHERE MS_MBKOD = MB_MBKOD '+
				' AND MS_POTSZ =  '''+rsz+''' '+
				' AND MS_RENSZ =  '''+geprsz+''' '+
				' AND MS_TIPUS = 0'+
			 // 	' ORDER BY MS_DATUM, MS_IDOPT');
       	' ORDER BY MS_DATUM desc, MS_IDOPT desc');
			if QueryUj2.RecordCount < 1 then begin
				NoticeKi('Hi�nyz� utols� felrak�s : '+rsz+ '('+geprsz+')');
        // FreeQueries;  majd a "finally" blokkban lefut
				Exit;
			end;
			QueryUj2.Last;
			tinev	:= 'felrak�';

			Query_Update( QueryUj3, 'T_POTK',
				[
				'TP_RENSZ', ''''+QueryUj2.FieldByName('MS_RENSZ').AsString+'''',
				'TP_GKKAT', ''''+QueryUj1.FieldByName('GK_GKKAT').AsString+'''',
				'TP_RESZR', ''''+QueryUj1.FieldByName('GK_RESZR').AsString+'''',
				'TP_GKTIP', ''''+QueryUj1.FieldByName('GK_TIPUS').AsString+'''',
				'TP_TIPUS', ''''+QueryUj2.FieldByName('MS_TIPUS').AsString+'''',
				'TP_TINEV', ''''+tinev+'''',
				'TP_DATUM', ''''+QueryUj2.FieldByName('MS_DATUM').AsString	+'''',
				'TP_IDOPT', ''''+QueryUj2.FieldByName('MS_IDOPT').AsString	+'''',
				'TP_TGNEV', ''''+QueryUj2.FieldByName('MS_FELNEV').AsString	+'''',
				'TP_TORSZ', ''''+QueryUj2.FieldByName('MS_FORSZ').AsString	+'''',
				'TP_TIRSZ', ''''+QueryUj2.FieldByName('MS_FELIR').AsString	+'''',
				'TP_TELEP', ''''+QueryUj2.FieldByName('MS_FELTEL').AsString	+'''',
				'TP_TGCIM', ''''+QueryUj2.FieldByName('MS_FELCIM').AsString	+'''',
				'TP_TENYN', ''''+QueryUj2.FieldByName('MS_TEFNEV').AsString	+'''',
				'TP_TENYO', ''''+QueryUj2.FieldByName('MS_TEFOR').AsString	+'''',
				'TP_TENYI', ''''+QueryUj2.FieldByName('MS_TEFIR').AsString	+'''',
				'TP_TENYT', ''''+QueryUj2.FieldByName('MS_TEFTEL').AsString	+'''',
				'TP_TENYC', ''''+QueryUj2.FieldByName('MS_TEFCIM').AsString	+'''',
				'TP_POTSZ', ''''+QueryUj2.FieldByName('MS_POTSZ').AsString	+'''',
				'TP_PSULY', SqlSzamString(StringSzam(QueryUj2.FieldByName('MS_FSULY').AsString), 12,2),
				'TP_GKARU', ''''+QueryUj2.FieldByName('MS_FELARU').AsString	+'''',
				'TP_TEDAT', ''''+QueryUj2.FieldByName('MS_FETDAT').AsString	+'''',
				'TP_TEIDO', ''''+QueryUj2.FieldByName('MS_FETIDO').AsString	+'''',
				// 'TP_MSKOD', QueryUj2.FieldByName('MS_MSKOD').AsString,
        'TP_MSKOD', IntToStr(StrToIntDef(QueryUj2.FieldByName('MS_MSKOD').AsString, 0)),
				'TP_SONEV', ''''+QueryUj2.FieldByName('MS_SNEV1').AsString	+''''
				], ' WHERE TP_POTSZ = '''+rsz+''' ' );

		end else begin
			// Nem r�szrak�, j�het az utols� lerak�s  t�nylegesre �s tervezettre is n�zz�k
			// Query_Run(QueryUj2, 'SELECT * '+
      Query_Run(QueryUj2, 'SELECT top 1 * '+
				' FROM MEGSEGED, MEGBIZAS WHERE MS_MBKOD = MB_MBKOD '+
				' AND MS_POTSZ =  '''+rsz+''' '+
				' AND MS_RENSZ =  '''+geprsz+''' '+
				' AND MS_TIPUS = 1'+
			 //	' ORDER BY MS_DATUM, MS_IDOPT');
 			 	' ORDER BY MS_DATUM desc, MS_IDOPT desc');  // �gy pont az utols�t fogja hozni
			if QueryUj2.RecordCount < 1 then begin
				NoticeKi('Hi�nyz� utols� lerak�s : '+rsz+ '('+geprsz+')');
        // FreeQueries;  majd a "finally" blokkban lefut
				Exit;
			end;
			QueryUj2.Last; // elvileg felesleges
			tinev	:= 'lerak�';
			Query_Update( QueryUj3, 'T_POTK',
				[
				'TP_RENSZ', ''''+QueryUj2.FieldByName('MS_RENSZ').AsString+'''',
				'TP_GKKAT', ''''+QueryUj1.FieldByName('GK_GKKAT').AsString+'''',
				'TP_RESZR', ''''+QueryUj1.FieldByName('GK_RESZR').AsString+'''',
				'TP_GKTIP', ''''+QueryUj1.FieldByName('GK_TIPUS').AsString+'''',
				'TP_TIPUS', ''''+QueryUj2.FieldByName('MS_TIPUS').AsString+'''',
				'TP_TINEV', ''''+tinev+'''',
				'TP_DATUM', ''''+QueryUj2.FieldByName('MS_DATUM').AsString	+'''',
				'TP_IDOPT', ''''+QueryUj2.FieldByName('MS_IDOPT').AsString	+'''',
				'TP_TGNEV', ''''+QueryUj2.FieldByName('MS_LERNEV').AsString	+'''',
				'TP_TORSZ', ''''+QueryUj2.FieldByName('MS_ORSZA').AsString	+'''',
				'TP_TIRSZ', ''''+QueryUj2.FieldByName('MS_LELIR').AsString	+'''',
				'TP_TELEP', ''''+QueryUj2.FieldByName('MS_LERTEL').AsString	+'''',
				'TP_TGCIM', ''''+QueryUj2.FieldByName('MS_LERCIM').AsString	+'''',
				'TP_TENYN', ''''+QueryUj2.FieldByName('MS_TENNEV').AsString	+'''',
				'TP_TENYO', ''''+QueryUj2.FieldByName('MS_TENOR').AsString	+'''',
				'TP_TENYI', ''''+QueryUj2.FieldByName('MS_TENIR').AsString	+'''',
				'TP_TENYT', ''''+QueryUj2.FieldByName('MS_TENTEL').AsString	+'''',
				'TP_TENYC', ''''+QueryUj2.FieldByName('MS_TENCIM').AsString	+'''',
				'TP_POTSZ', ''''+QueryUj2.FieldByName('MS_POTSZ').AsString	+'''',
				'TP_PSULY', SqlSzamString(StringSzam(QueryUj2.FieldByName('MS_LSULY').AsString), 12,2),
				'TP_GKARU', ''''+QueryUj2.FieldByName('MS_FELARU').AsString	+'''',
				'TP_TEDAT', ''''+QueryUj2.FieldByName('MS_LETDAT').AsString	+'''',
				'TP_TEIDO', ''''+QueryUj2.FieldByName('MS_LETIDO').AsString	+'''',
				// 'TP_MSKOD', QueryUj2.FieldByName('MS_MSKOD').AsString,
        'TP_MSKOD', IntToStr(StrToIntDef(QueryUj2.FieldByName('MS_MSKOD').AsString, 0)),
				'TP_SONEV', ''''+QueryUj2.FieldByName('MS_SNEV1').AsString	+''''
				], ' WHERE TP_POTSZ = '''+rsz+''' ' );
		end;
	end;
 finally
   FreeQueries;
 end;  // try-finally
end;

function GetKoltsegAtlag(ktkod : string) : double;
var
	QueryUj1	: TADOQuery;
	kezdokm		: string;
	vegzokm		: string;
	liter		: double;
	osszkm		: double;
	rendszam	: string;
	elozoev_str	: string;
begin
	Result			:= 0;
	QueryUj1 		:= TADOQuery.Create(nil);
	QueryUj1.Tag  	:= QUERY_ADAT_TAG;
	EgyebDlg.SeTADOQueryDatabase(QueryUj1);
	// Csak tele tankolt �zemanyag k�lts�ggel foglalkozunk, ami nem telepi tankol�s volt
	Query_Run(QueryUj1, 'SELECT * FROM KOLTSEG WHERE KS_KTKOD = '+ktkod+
		' AND KS_TELE > 0 ' 	+
		' AND KS_TIPUS = 0 ' 	+
		' AND KS_TETAN = 0 ' );
	if QueryUj1.RecordCount < 1 then begin
		Exit;
	end;
	kezdokm		:= '0';
	vegzokm		:= QueryUj1.FieldByName('KS_KMORA').AsString;
	rendszam	:= QueryUj1.FieldByName('KS_RENDSZ').AsString;
	liter		:= 0;
	if Query_Run (QueryUj1, 'SELECT * FROM KOLTSEG WHERE KS_RENDSZ = '''+rendszam+ ''' '+
		' AND KS_TELE > 0 AND KS_KMORA < '+vegzokm+
		' AND KS_TIPUS = 0 ' +
		' AND KS_TETAN = 0 '+
		' ORDER BY KS_KMORA ' ,true) then begin
		if QueryUj1.RecordCount < 1 then begin
			// Ki kell sz�molni az el�z� �vi adatokat
			elozoev_str     := copy(EgyebDlg.v_alap_db, 1, Length(EgyebDlg.v_alap_db) - 4 ) + IntToStr(StrToIntDef(EgyebDlg.Evszam,1000)-1);
			// Ha nincs el�z� �v, akkor megint most keres de nem tal�l!!!
			Query_Run(QueryUj1, 'SELECT * FROM '+GetTableFullName(elozoev_str, 'KOLTSEG')+' WHERE KS_RENDSZ = '''+rendszam+''' '+
				' AND KS_KMORA < '+vegzokm+
				' AND KS_TELE  = 1 ' +
				' AND KS_TETAN = 0' +
				' AND KS_TIPUS = 0 ' );
			if QueryUj1.RecordCount < 1 then begin
				Exit;
			end;
			QueryUj1.Last;
			kezdokm		:= QueryUj1.FieldByName('KS_KMORA').AsString;
			Query_Run (QueryUj1, 'SELECT SUM(KS_UZMENY) MENNY FROM '+GetTableFullName(elozoev_str, 'KOLTSEG')+' KOLTSEG WHERE KS_RENDSZ = '''+rendszam+''' '+
				' AND KS_KMORA <= '+vegzokm+
				' AND KS_KMORA > '+kezdokm+
				' AND KS_TIPUS = 0 ' +
				' AND KS_TETAN = 0 ' ,true);
			liter	:= StringSzam(QueryUj1.FieldByname('MENNY').AsString);
		end else begin
			QueryUj1.Last;
			kezdokm		:= QueryUj1.FieldByName('KS_KMORA').AsString;
		end;
		// Az idei �vi �sszegek
		Query_Run (QueryUj1, 'SELECT SUM(KS_UZMENY) MENNY FROM KOLTSEG WHERE KS_RENDSZ = '''+rendszam+''' '+
			' AND KS_KMORA <= '+vegzokm+
			' AND KS_KMORA > '+kezdokm+
			' AND KS_TIPUS = 0 ' +
			' AND KS_TETAN = 0 ' ,true);
		liter	:= liter + StringSzam(QueryUj1.FieldByname('MENNY').AsString);
		osszkm 	:= StringSzam(vegzokm)-StringSzam(kezdokm);
		if osszkm > 0 then begin
			Result := liter * 100 / osszkm;
		end;
	end;
end;

function 	KoltsegJaratTolto(ktkod : string) : boolean;
var
	QueryUj1	: TADOQuery;
	jarkod		: string;
	jarszam		: string;
	sofkod		: string;
	sofnev  	: string;
begin
	result	:= false;
	QueryUj1 		:= TADOQuery.Create(nil);
	QueryUj1.Tag  	:= QUERY_ADAT_TAG;
	EgyebDlg.SeTADOQueryDatabase(QueryUj1);
	Query_Run(QueryUj1, 'SELECT * FROM KOLTSEG WHERE KS_KTKOD = '+ktkod);
	if QueryUj1.RecordCount < 1 then begin
		Exit;
	end;
	jarkod	:= QueryUj1.FieldByName('KS_JAKOD').AsString;
	if jarkod = '' then begin
		Exit;
	end;
	jarszam	:= '';
	sofkod	:= '';
	sofnev	:= '';
	Query_Run(QueryUj1, 'SELECT * FROM JARAT WHERE JA_KOD = '+jarkod);
	if jarkod <> '' then begin
		jarszam	:= GetJaratszamFromDb(QueryUj1);
		sofkod	:= QueryUj1.FieldByName('JA_SOFK1').AsString;
		sofnev	:= QueryUj1.FieldByName('JA_SNEV1').AsString;
	end;
	Query_Update(QueryUj1, 'KOLTSEG',
		[
		'KS_JARAT', ''''+jarszam+'''',
		'KS_SOKOD', ''''+sofkod+'''',
		'KS_SONEV', ''''+sofnev+''''
		], ' WHERE KS_KTKOD = '+ktkod);
	Result	:= true;
	QueryUj1.Destroy;
end;

function	HutoPotkocsi(potrsz : string) : boolean;
var
	QueryUj1	: TADOQuery;
begin
	Result	:= false;
	QueryUj1 		:= TADOQuery.Create(nil);
	QueryUj1.Tag  	:= QUERY_ADAT_TAG;
	EgyebDlg.SeTADOQueryDatabase(QueryUj1);
	Query_Run(QueryUj1, 'SELECT KS_RENDSZ FROM KOLTSEG WHERE KS_TIPUS = 3 AND KS_RENDSZ = '''+potrsz+''' ');
	if QueryUj1.RecordCount > 0 then begin
		Result	:= true;
	end;
	QueryUj1.Destroy;
end;

function	FuvarHovaUpdate(ftkod : string) : boolean;
var
	QueryUj1	: TADOQuery;
	kodli		: TStringList;
begin
	Result			:= false;
	QueryUj1 		:= TADOQuery.Create(nil);
	QueryUj1.Tag  	:= QUERY_ADAT_TAG;
	EgyebDlg.SeTADOQueryDatabase(QueryUj1);
	Query_Run(QueryUj1, 'SELECT * FROM FUVARTABLA WHERE FT_FTKOD = '+ftkod);
	if QueryUj1.RecordCount > 0 then begin
		kodli		:= TStringList.Create;
		StringParse(QueryUj1.FieldByName('FT_DESCR').AsString, '-', kodli);
		kodli.Add('');
		kodli.Add('');
		Query_Update(QueryUj1, 'FUVARTABLA',
				[
				'FT_KEZDO', ''''+trim(kodli[0])+'''',
				'FT_VEGZO', ''''+trim(kodli[1])+''''
				], ' WHERE FT_FTKOD = '+ ftkod);
		Result	:= true;
	end;
	QueryUj1.Destroy;
end;

procedure	GetRszLista(li : TStringList; tipus : integer=0);
var
	QueryUj1	: TADOQuery;
	gkstr		: string;
begin
	QueryUj1 		:= TADOQuery.Create(nil);
	QueryUj1.Tag  	:= QUERY_ADAT_TAG;
	EgyebDlg.SeTADOQueryDatabase(QueryUj1);
	if tipus = 0 then begin
		// A g�pkocsik beolvas�sa
		gkstr	:= 'SELECT GK_RESZ FROM '+GetTableFullName(EgyebDlg.v_koz_db, 'GEPKOCSI')+' WHERE GK_POTOS <> 1 AND GK_ARHIV = 0 AND GK_KIVON = 0 AND GK_KULSO = 0 ';
		Query_Run(QueryUj1, 'SELECT DISTINCT MB_RENSZ RSZ FROM MEGBIZAS WHERE MB_RENSZ IN ( '+gkstr+' ) '+
			' UNION SELECT DISTINCT MS_RENSZ RSZ FROM MEGSEGED  WHERE MS_RENSZ IN ( '+gkstr+' ) ');
	end else begin
		// A p�tkocsik beolvas�sa
		gkstr	:= 'SELECT GK_RESZ FROM '+GetTableFullName(EgyebDlg.v_koz_db, 'GEPKOCSI')+' WHERE GK_POTOS = 1 AND GK_ARHIV = 0 AND GK_KIVON = 0 AND GK_KULSO = 0 ';
		Query_Run(QueryUj1, 'SELECT DISTINCT MB_POTSZ RSZ FROM MEGBIZAS WHERE MB_POTSZ IN ( '+gkstr+' ) '+
			' UNION SELECT DISTINCT MS_POTSZ RSZ FROM MEGSEGED  WHERE MS_POTSZ IN ( '+gkstr+' ) ');
	end;
	li.Clear;
	while not QueryUj1.Eof do begin
		li.Add(QueryUj1.FieldByName('RSZ').AsString);
		QueryUj1.Next;
	end;
	li.Sorted	:= true;
	QueryUj1.Destroy;
end;

function KlonSzamla(szkod, szujszam : string) : integer;
(************************************************
A l�tez� sz�mla duplik�l�sa -> visszat�r�si �rt�k
az �j sz�mla SA_UJKOD �rt�ke, vagy 0, ha hiba
van.
************************************************)
var
	ujkod		: integer;
	ujazon		: integer;
	QueryUj1	: TADOQuery;
	QueryUj2	: TADOQuery;
	QueryVi1	: TADOQuery;
	szszam		: string;
  kidat,esdat,ukidat,uesdat, S: string;
  lejarat: integer;

  procedure FreeResources;
   begin
  	QueryUj1.Destroy;
  	QueryUj2.Destroy;
   end;

begin
	Result	:= 0;
	QueryUj1 		:= TADOQuery.Create(nil);
	QueryUj1.Tag  	:= QUERY_ADAT_TAG;
	EgyebDlg.SeTADOQueryDatabase(QueryUj1);
	QueryVi1 		:= TADOQuery.Create(nil);
	QueryVi1.Tag  	:= QUERY_ADAT_TAG;
	EgyebDlg.SeTADOQueryDatabase(QueryVi1);
	QueryUj2 		:= TADOQuery.Create(nil);
	QueryUj2.Tag  	:= QUERY_ADAT_TAG;
	EgyebDlg.SeTADOQueryDatabase(QueryUj2);
	Query_Run(QueryUj1, 'SELECT * FROM SZFEJ WHERE SA_UJKOD = '+szkod);
	if QueryUj1.RecordCount < 1 then begin
		// Nincs megfelel� sz�mla!!!
    FreeResources;
		Exit;
	end;
  ///////////////////////////////////////
  kidat:=QueryUj1.FieldByName('SA_KIDAT').AsString;
  esdat:=QueryUj1.FieldByName('SA_ESDAT').AsString;
  if (kidat='')or(esdat='') then
  begin
    ukidat:='';
    uesdat:='';
  end
  else
  begin
    lejarat:=Trunc(StrToDate(esdat)-StrToDate(kidat));
    ukidat:=DateToStr(date);
	  uesdat:= DatumHoznap(ukidat, lejarat, true);
  end;
  // ---------------------------------------------------------
  //---- A JS-n�l 2013-as sz�mlasz�mokat gener�lt, amit azt�n nem haszn�lt fel. A Trans-n�l viszont �les
  // --  sz�mlasz�mokat gener�lt, majd eldobta. Az UTOLSOKOD-os megold�s miatt lett bel�le gond.
	{if szujszam = '' then begin
		//szszam			:= GetUjszamla;
		szszam			:= GetUjszamla2('');
    if szszam='' then
      exit;
	end else begin
		szszam  		:= szujszam;		// �sszes�tett sz�ml�kat egyetlen sz�mlasz�m al� rakjuk
	end;
  szszam:='';
   }
  // --------------------------
	// ujkod			:= GetNextCode('SZFEJ', 'SA_UJKOD');
  ujkod			:= GetNewIDLoop(modeGetNextCode, 'SZFEJ', 'SA_UJKOD', '');

	// A viszonylat duplik�ci�ja
	Query_Run(QueryVi1, 'SELECT * FROM VISZONY WHERE VI_UJKOD = '+szkod);
	if QueryVi1.RecordCount < 1 then begin
    FreeResources;
		Exit;
	end;

	// ujazon := GetNextStrCode('VISZONY', 'VI_VIKOD', 0, 5);
  ujazon := GetNewIDLoop(modeGetNextStrCode,'VISZONY', 'VI_VIKOD', '', 0, 6);
  if (ujkod<=0) or (ujazon<=0) then begin
     FreeResources;
     S:= 'Sikertelen k�d gener�l�s, k�rem pr�b�lja �jra! (KlonSzamla)';
     HibaKiiro(S);
     NoticeKi(S);
     Exit;
     end;
	// Az �j rekord felvitele
	Query_Insert (QueryUj2, 'VISZONY', [
		'VI_VIKOD', 	''''+IntToStr(ujazon)+'''',
		'VI_EVIKOD',	''''+QueryVi1.FieldByName('VI_VIKOD').AsString+'''',
		'VI_JARAT',	''''+QueryUj1.FieldByName('SA_JARAT').AsString+'''',
		'VI_UJKOD',		IntToStr(ujkod),
		'VI_HONNAN',	''''+QueryVi1.FieldByName('VI_HONNAN').AsString+'''',
		'VI_HOVA',		''''+QueryVi1.FieldByName('VI_HOVA').AsString+'''',
		'VI_JAKOD',		''''+QueryVi1.FieldByName('VI_JAKOD').AsString+'''',
		'VI_VEKOD',		''''+QueryVi1.FieldByName('VI_VEKOD').AsString+'''',
		'VI_VENEV',		''''+QueryVi1.FieldByName('VI_VENEV').AsString+'''',
		'VI_VALNEM',	''''+QueryVi1.FieldByName('VI_VALNEM').AsString+'''',
		'VI_KULF', 		QueryVi1.FieldByName('VI_KULF').AsString,
		'VI_ARFOLY',	SqlSzamString(StringSzam(QueryVi1.FieldByName('VI_ARFOLY').AsString),12,4),
		'VI_FDEXP',		SqlSzamString(StringSzam(QueryVi1.FieldByName('VI_FDEXP').AsString),12,4),
		'VI_FDIMP',		SqlSzamString(StringSzam(QueryVi1.FieldByName('VI_FDIMP').AsString),12,4),
		'VI_FDKOV',		SqlSzamString(StringSzam(QueryVi1.FieldByName('VI_FDKOV').AsString),12,4),
		'VI_TIPUS',		QueryVi1.FieldByName('VI_TIPUS').AsString,
		'VI_BEDAT',		''''+QueryVi1.FieldByName('VI_BEDAT').AsString+'''',
	  'VI_KIDAT',		''''+QueryVi1.FieldByName('VI_KIDAT').AsString+'''',
		//'VI_KIDAT',		''''+ukidat+'''',
		'VI_POZIC',		''''+QueryVi1.FieldByName('VI_POZIC').AsString+'''',
		'VI_SAKOD',		''''+szszam+'''',
		'VI_MEGJ',		''''+QueryVi1.FieldByName('VI_MEGJ').AsString+''''
	] );


	// A sz�mla duplik�ci�ja
	Query_Insert ( QueryUj2, 'SZFEJ',
	[
		'SA_UJKOD', 	IntToStr(ujkod),
		'SA_KOD',		''''+szszam+'''',
		'SA_DATUM',		''''+EgyebDlg.MaiDatum+'''',
		'SA_TEDAT',		''''+QueryUj1.FieldByName('SA_TEDAT').AsString 	+'''',
		'SA_KIDAT',		''''+QueryUj1.FieldByName('SA_KIDAT').AsString 	+'''',
		'SA_ESDAT',		''''+QueryUj1.FieldByName('SA_ESDAT').AsString 	+'''',
		//'SA_KIDAT',		''''+ukidat	+'''',
		//'SA_ESDAT',		''''+uesdat	+'''',
		'SA_VEVONEV',	''''+QueryUj1.FieldByName('SA_VEVONEV').AsString 	+'''',
		'SA_VEIRSZ',	''''+QueryUj1.FieldByName('SA_VEIRSZ').AsString  	+'''',
		'SA_VEVAROS',	''''+QueryUj1.FieldByName('SA_VEVAROS').AsString 	+'''',
		'SA_VECIM',		''''+QueryUj1.FieldByName('SA_VECIM').AsString	+'''',
		'SA_VEADO',		''''+QueryUj1.FieldByName('SA_VEADO').AsString 	+'''',
		'SA_EUADO',		''''+QueryUj1.FieldByName('SA_EUADO').AsString 	+'''',
		'SA_VEBANK',	''''+QueryUj1.FieldByName('SA_VEBANK').AsString 	+'''',
		'SA_VEKEDV',	SqlSzamString(StringSzam(QueryUj1.FieldByName('SA_VEKEDV').AsString),6,2),
		'SA_PELDANY',	'0',
		'SA_FIMOD',		''''+QueryUj1.FieldByName('SA_FIMOD').AsString 	+'''',
		'SA_OSSZEG',	SqlSzamString(StringSzam(QueryUj1.FieldByName('SA_OSSZEG').AsString),12,2),
		'SA_AFA',		SqlSzamString(StringSzam(QueryUj1.FieldByName('SA_AFA').AsString),12,2),
		'SA_MEGJ',		''''+QueryUj1.FieldByName('SA_MEGJ').AsString 	+'''',
		'SA_JARAT',		''''+QueryUj1.FieldByName('SA_JARAT').AsString 	+'''',
		'SA_POZICIO',	''''+QueryUj1.FieldByName('SA_POZICIO').AsString 	+'''',
		'SA_RSZ',		''''+QueryUj1.FieldByName('SA_RSZ').AsString 		+'''',
		'SA_MELL1',		''''+QueryUj1.FieldByName('SA_MELL1').AsString	+'''',
		'SA_MELL2',		''''+QueryUj1.FieldByName('SA_MELL2').AsString 	+'''',
		'SA_MELL3',		''''+QueryUj1.FieldByName('SA_MELL3').AsString 	+'''',
		'SA_MELL4',		''''+QueryUj1.FieldByName('SA_MELL3').AsString 	+'''',
		'SA_MELL5',		''''+QueryUj1.FieldByName('SA_MELL5').AsString 	+'''',
		'SA_MELL6',		''''+QueryUj1.FieldByName('SA_MELL6').AsString 	+'''',
		'SA_MELL7',		''''+QueryUj1.FieldByName('SA_MELL7').AsString 	+'''',
		'SA_FEJL1',		''''+QueryUj1.FieldByName('SA_FEJL1').AsString	+'''',
		'SA_FEJL2',		''''+QueryUj1.FieldByName('SA_FEJL2').AsString	+'''',
		'SA_FEJL3',		''''+QueryUj1.FieldByName('SA_FEJL3').AsString	+'''',
		'SA_FEJL4',		''''+QueryUj1.FieldByName('SA_FEJL4').AsString	+'''',
		'SA_FEJL5',		''''+QueryUj1.FieldByName('SA_FEJL5').AsString	+'''',
		'SA_FEJL6',		''''+QueryUj1.FieldByName('SA_FEJL6').AsString	+'''',
		'SA_FEJL7',		''''+QueryUj1.FieldByName('SA_FEJL7').AsString	+'''',
		'SA_FOKON',		''''+QueryUj1.FieldByName('SA_FOKON').AsString	+'''',
		'SA_VEVOKOD',  	''''+QueryUj1.FieldByName('SA_VEVOKOD').AsString	+'''',
		'SA_EGYA1',		''''+QueryUj1.FieldByName('SA_EGYA1').AsString	+'''',
		'SA_EGYA2',		''''+QueryUj1.FieldByName('SA_EGYA2').AsString	+'''',
		'SA_EGYA3',		''''+QueryUj1.FieldByName('SA_EGYA3').AsString	+'''',
		'SA_MEGRE',		''''+QueryUj1.FieldByName('SA_MEGRE').AsString	+'''',
		'SA_ORSZA',		''''+QueryUj1.FieldByName('SA_ORSZA').AsString	+'''',
		'SA_ALLST',		''''+GetSzamlaAllapot(1)+'''',
		'SA_RESZE',		'0',
	//	'SA_NEMET',		IntToStr(StrToIntDef(QueryUj1.FieldByName('SA_NEMET').AsString,0)),
		'SA_HELYE',		IntToStr(StrToIntDef(QueryUj1.FieldByName('SA_HELYE').AsString,0)),
		'SA_SAEUR',		IntToStr(StrToIntDef(QueryUj1.FieldByName('SA_SAEUR').AsString,0)),
		'SA_ATAD',		'0',
		'SA_FLAG', 		'1',	// M�g m�dos�that� a sz�mla
       'SA_VALNEM',  ''''+QueryUj1.FieldByName('SA_VALNEM').AsString	+'''',
		'SA_VALOSS',		SqlSzamString(StringSzam(QueryUj1.FieldByName('SA_VALOSS').AsString),12,2),
		'SA_VALAFA',		SqlSzamString(StringSzam(QueryUj1.FieldByName('SA_VALAFA').AsString),12,2),
       'SA_VALARF',		SqlSzamString(StringSzam(QueryUj1.FieldByName('SA_VALARF').AsString),12,2),
		'SA_ARFDAT',		''''+QueryUj1.FieldByName('SA_ARFDAT').AsString	+'''',
       'SA_UZPOT',		SqlSzamString(StringSzam(QueryUj1.FieldByName('SA_UZPOT').AsString),12,2),
		'SA_NYELV',		''''+QueryUj1.FieldByName('SA_NYELV').AsString	+'''',
		'SA_VEFEJ',		''''+QueryUj1.FieldByName('SA_VEFEJ').AsString	+'''',
		'SA_TIPUS',		SqlSzamString(StringSzam(QueryUj1.FieldByName('SA_TIPUS').AsString),12,2)
		]);

	// A sz�mla sorok duplik�ci�ja
	Query_Run (QueryUj1, 'SELECT * FROM SZSOR WHERE SS_UJKOD = '+szkod,true);
	while not QueryUj1.EOF do begin
		Query_Insert( QueryUj2, 'SZSOR',
		[
		 'SS_KOD', 			''''+szszam+'''',
		 'SS_SOR',			QueryUj1.FieldByName('SS_SOR').AsString,
		 'SS_UJKOD',	   	IntToStr(ujkod),
		 'SS_TERKOD', 		''''+QueryUj1.FieldByName('SS_TERKOD').AsString +'''',
		 'SS_TERNEV', 		''''+QueryUj1.FieldByName('SS_TERNEV').AsString +'''',
		 'SS_CIKKSZ', 		''''+QueryUj1.FieldByName('SS_CIKKSZ').AsString +'''',
		 'SS_ITJSZJ', 		''''+QueryUj1.FieldByName('SS_ITJSZJ').AsString +'''',
		 'SS_MEGY', 		''''+QueryUj1.FieldByName('SS_MEGY').AsString +'''',
		 'SS_AFA', 			SqlSzamString(StringSzam(QueryUj1.FieldByName('SS_AFA').AsString),   	12,2),
		 'SS_EGYAR', 		SqlSzamString(StringSzam(QueryUj1.FieldByName('SS_EGYAR').AsString),	12,2),
		 'SS_KEDV', 		SqlSzamString(StringSzam(QueryUj1.FieldByName('SS_KEDV').AsString),  	12,2),
		 'SS_VALERT', 		SqlSzamString(StringSzam(QueryUj1.FieldByName('SS_VALERT').AsString),	12,2),
		 'SS_VALARF', 		SqlSzamString(StringSzam(QueryUj1.FieldByName('SS_VALARF').AsString),	12,2),
		 'SS_DARAB', 		SqlSzamString(StringSzam(QueryUj1.FieldByName('SS_DARAB').AsString),	12,2),
		 'SS_ARFOL', 		SqlSzamString(StringSzam(QueryUj1.FieldByName('SS_ARFOL').AsString),	12,4),
		 'SS_NEMEU',		IntToStr(StrToIntDef(QueryUj1.FieldByName('SS_NEMEU').AsString,0)),
		 'SS_VALNEM', 		''''+ QueryUj1.FieldByName('SS_VALNEM').AsString +'''',
		 'SS_AFAKOD', 		''''+ QueryUj1.FieldByName('SS_AFAKOD').AsString +'''',
		 'SS_LEIRAS', 		''''+ QueryUj1.FieldByName('SS_LEIRAS').AsString +'''',
		 'SS_KULNEV', 		''''+ QueryUj1.FieldByName('SS_KULNEV').AsString +'''',
		 'SS_KULLEI', 		''''+ QueryUj1.FieldByName('SS_KULLEI').AsString +'''',
		 'SS_KULME', 		''''+ QueryUj1.FieldByName('SS_KULME').AsString  +'''',
		 'SS_ARDAT', 		''''+ QueryUj1.FieldByName('SS_ARDAT').AsString  +''''
		]);
		QueryUj1.Next;
	end;

  FreeResources;
	Result	:= ujkod;
end;

function	FuvardijTolto(jkod : string) : boolean;
var
	QueryUj1	: TADOQuery;
	vnem		: string;
begin
	// A j�ratba betessz�k a megb�z�sok fuvard�jainak az �sszeg�t
	QueryUj1 		:= TADOQuery.Create(nil);
	QueryUj1.Tag  	:= QUERY_ADAT_TAG;
	EgyebDlg.SeTADOQueryDatabase(QueryUj1);
	Query_Run (QueryUj1, 'SELECT AJ_ALNEV FROM JARAT, ALJARAT WHERE AJ_JAKOD = JA_KOD AND JA_KOD = '''+jkod+''' ',true);
	if QueryUj1.FieldByName('AJ_ALNEV').AsString = '' then begin
		// Nem alv�llalkoz�i j�rat
		Result	:= true;
		Exit;
	end;
  // KG 20111128
//	Query_Run (QueryUj1, 'SELECT DISTINCT MB_ALNEM FROM MEGBIZAS WHERE MB_JAKOD = '''+jkod+''' AND MB_DATUM > '''+EgyebDlg.MaiDatum+''' ',true);
	Query_Run (QueryUj1, 'SELECT DISTINCT MB_ALNEM FROM MEGBIZAS WHERE MB_JAKOD = '''+jkod+''' AND SUBSTRING(MB_DATUM,1,4)= '''+copy(EgyebDlg.MaiDatum,1,4)+'''',true);
	if QueryUj1.RecordCount = 1 then begin
		vnem	:= QueryUj1.FieldByName('MB_ALNEM').AsString;
		//Query_Run (QueryUj1, 'SELECT SUM(MB_ALDIJ) ODIJ FROM MEGBIZAS WHERE MB_JAKOD = '''+jkod+''' AND MB_DATUM > '''+EgyebDlg.MaiDatum+''' ',true);
		Query_Run (QueryUj1, 'SELECT SUM(MB_ALDIJ) ODIJ FROM MEGBIZAS WHERE MB_JAKOD = '''+jkod+''' AND SUBSTRING(MB_DATUM,1,4)= '''+copy(EgyebDlg.MaiDatum,1,4)+''' ',true);
		Query_Run (QueryUj1, 'UPDATE ALJARAT SET AJ_FUDIJ = '+SqlSzamString(StringSzam(QueryUj1.FieldByName('ODIJ').AsString), 13,3)+' WHERE AJ_JAKOD = '''+jkod+''' ',true);
	end;
	QueryUj1.Destroy;
	Result	:= true;
end;

function GetAdBlueKoltsegAtlag(ktkod : string) : double;
var
	QueryUj1	: TADOQuery;
	kezdokm		: string;
	vegzokm		: string;
	liter		: double;
	osszkm		: double;
	rendszam	: string;
	elozoev_str	: string;
begin
	Result			:= 0;
	QueryUj1 		:= TADOQuery.Create(nil);
	QueryUj1.Tag  	:= QUERY_ADAT_TAG;
	EgyebDlg.SeTADOQueryDatabase(QueryUj1);
	// Csak tele tankolt �zemanyag k�lts�ggel foglalkozunk, ami nem telepi tankol�s volt
	Query_Run(QueryUj1, 'SELECT KS_KMORA, KS_RENDSZ FROM KOLTSEG WHERE KS_KTKOD = '+ktkod+
		' AND KS_TELE > 0 ' 	+
		' AND KS_TIPUS = 2 ' );
	if QueryUj1.RecordCount < 1 then begin
		Exit;
	end;
	kezdokm		:= '0';
	vegzokm		:= QueryUj1.FieldByName('KS_KMORA').AsString;
	rendszam	:= QueryUj1.FieldByName('KS_RENDSZ').AsString;
	liter		:= 0;
	if Query_Run (QueryUj1, 'SELECT KS_KMORA FROM KOLTSEG WHERE KS_RENDSZ = '''+rendszam+ ''' '+
		' AND KS_TELE > 0 AND KS_KMORA < '+vegzokm+
		' AND KS_TIPUS = 2 ' +
		' ORDER BY KS_KMORA ' ,true) then begin
		if QueryUj1.RecordCount < 1 then begin
			// Ki kell sz�molni az el�z� �vi adatokat
			elozoev_str     := copy(EgyebDlg.v_alap_db, 1, Length(EgyebDlg.v_alap_db) - 4 ) + IntToStr(StrToIntDef(EgyebDlg.Evszam,1000)-1);
			// Ha nincs el�z� �v, akkor megint most keres de nem tal�l!!!
			Query_Run(QueryUj1, 'SELECT KS_KMORA FROM '+GetTableFullName(elozoev_str, 'KOLTSEG')+' WHERE KS_RENDSZ = '''+rendszam+''' '+
				' AND KS_KMORA < '+vegzokm+
				' AND KS_TELE  = 1 ' +
				' AND KS_TIPUS = 2 ' );
			if QueryUj1.RecordCount < 1 then begin
				Exit;
			end;
			QueryUj1.Last;
			kezdokm		:= QueryUj1.FieldByName('KS_KMORA').AsString;
			Query_Run (QueryUj1, 'SELECT SUM(KS_UZMENY) MENNY FROM '+GetTableFullName(elozoev_str, 'KOLTSEG')+' KOLTSEG WHERE KS_RENDSZ = '''+rendszam+''' '+
				' AND KS_KMORA <= '+vegzokm+
				' AND KS_KMORA > '+kezdokm+
				' AND KS_TIPUS = 2 ' ,true);
			liter	:= StringSzam(QueryUj1.FieldByname('MENNY').AsString);
		end else begin
			QueryUj1.Last;
			kezdokm		:= QueryUj1.FieldByName('KS_KMORA').AsString;
		end;
		// Az idei �vi �sszegek
		Query_Run (QueryUj1, 'SELECT SUM(KS_UZMENY) MENNY FROM KOLTSEG WHERE KS_RENDSZ = '''+rendszam+''' '+
			' AND KS_KMORA <= '+vegzokm+
			' AND KS_KMORA > '+kezdokm+
			' AND KS_TIPUS = 2 ' ,true);
		liter	:= liter + StringSzam(QueryUj1.FieldByname('MENNY').AsString);
		osszkm 	:= StringSzam(vegzokm)-StringSzam(kezdokm);
		if osszkm > 0 then begin
			Result := liter * 100 / osszkm;
		end;
	end;
	QueryUj1.Destroy;
end;

function GetHutoKoltsegAtlag(ktkod : string) : double;
var
	QueryUj1	: TADOQuery;
	kezdokm		: string;
	vegzokm		: string;
	liter		: double;
	osszkm		: double;
	rendszam	: string;
	elozoev_str	: string;
begin
	Result			:= 0;
	QueryUj1 		:= TADOQuery.Create(nil);
	QueryUj1.Tag  	:= QUERY_ADAT_TAG;
	EgyebDlg.SeTADOQueryDatabase(QueryUj1);
	// Csak tele tankolt �zemanyag k�lts�ggel foglalkozunk, ami nem telepi tankol�s volt
	Query_Run(QueryUj1, 'SELECT KS_KMORA, KS_RENDSZ FROM KOLTSEG WHERE KS_KTKOD = '+ktkod+
		' AND KS_TELE > 0 ' 	+
		' AND KS_TIPUS = 3 ' );
	if QueryUj1.RecordCount < 1 then begin
		Exit;
	end;
	kezdokm		:= '0';
	vegzokm		:= QueryUj1.FieldByName('KS_KMORA').AsString;
	rendszam	:= QueryUj1.FieldByName('KS_RENDSZ').AsString;
	liter		:= 0;
	if Query_Run (QueryUj1, 'SELECT KS_KMORA FROM KOLTSEG WHERE KS_RENDSZ = '''+rendszam+ ''' '+
		' AND KS_TELE > 0 AND KS_KMORA < '+vegzokm+
		' AND KS_TIPUS = 3 ' +
		' ORDER BY KS_KMORA ' ,true) then begin
		if QueryUj1.RecordCount < 1 then begin
			// Ki kell sz�molni az el�z� �vi adatokat
			elozoev_str     := copy(EgyebDlg.v_alap_db, 1, Length(EgyebDlg.v_alap_db) - 4 ) + IntToStr(StrToIntDef(EgyebDlg.Evszam,1000)-1);
			// Ha nincs el�z� �v, akkor megint most keres de nem tal�l!!!
			Query_Run(QueryUj1, 'SELECT KS_KMORA FROM '+GetTableFullName(elozoev_str, 'KOLTSEG')+' WHERE KS_RENDSZ = '''+rendszam+''' '+
				' AND KS_KMORA < '+vegzokm+
				' AND KS_TELE  = 1 ' +
				' AND KS_TIPUS = 3 ' );
			if QueryUj1.RecordCount < 1 then begin
				Exit;
			end;
			QueryUj1.Last;
			kezdokm		:= QueryUj1.FieldByName('KS_KMORA').AsString;
			Query_Run (QueryUj1, 'SELECT SUM(KS_UZMENY) MENNY FROM '+GetTableFullName(elozoev_str, 'KOLTSEG')+' KOLTSEG WHERE KS_RENDSZ = '''+rendszam+''' '+
				' AND KS_KMORA <= '+vegzokm+
				' AND KS_KMORA > '+kezdokm+
				' AND KS_TIPUS = 3 ' ,true);
			liter	:= StringSzam(QueryUj1.FieldByname('MENNY').AsString);
		end else begin
			QueryUj1.Last;
			kezdokm		:= QueryUj1.FieldByName('KS_KMORA').AsString;
		end;
		// Az idei �vi �sszegek
		Query_Run (QueryUj1, 'SELECT SUM(KS_UZMENY) MENNY FROM KOLTSEG WHERE KS_RENDSZ = '''+rendszam+''' '+
			' AND KS_KMORA <= '+vegzokm+
			' AND KS_KMORA > '+kezdokm+
			' AND KS_TIPUS = 3 ' ,true);
		liter	:= liter + StringSzam(QueryUj1.FieldByname('MENNY').AsString);
		osszkm 	:= StringSzam(vegzokm)-StringSzam(kezdokm);
		if osszkm > 0 then begin
			Result := liter / osszkm;
		end;
	end;
	QueryUj1.Destroy;
end;

function	KoltsegOsszevono(rsz, km, tipus : string) : boolean;
var
	QueryUj1	: TADOQuery;
	QueryUj2	: TADOQuery;
	uzmeny		: double;
	osszeg		: double;
	arfoly		: double;
	afasz		: double;
	afaert		: double;
	vantele		: boolean;
	ktkod		: string;
	megj		: string;
	telestr2	: string;
begin
	Result			:= true;
	QueryUj1 		:= TADOQuery.Create(nil);
	QueryUj1.Tag  	:= QUERY_ADAT_TAG;
	EgyebDlg.SeTADOQueryDatabase(QueryUj1);
	QueryUj2 		:= TADOQuery.Create(nil);
	QueryUj2.Tag  	:= QUERY_ADAT_TAG;
	EgyebDlg.SeTADOQueryDatabase(QueryUj2);
	Query_Run (QueryUj1, 'SELECT * FROM KOLTSEG WHERE KS_RENDSZ = '''+rsz+''' AND KS_KMORA = '+km+' AND KS_TIPUS = '+tipus+' ORDER BY KS_DATUM, KS_KTKOD',true);
	if QueryUj1.RecordCount > 1 then begin
		afasz	:= StringSzam(QueryUj1.FieldByName('KS_AFASZ').AsString);
		uzmeny	:= StringSzam(QueryUj1.FieldByName('KS_UZMENY').AsString);
		arfoly	:= StringSzam(QueryUj1.FieldByName('KS_ARFOLY').AsString);
		if arfoly = 0 then begin
			arfoly	:= 9999999999;
		end;
		ktkod   := QueryUj1.FieldByName('KS_KTKOD').AsString;
		osszeg	:= StringSzam(QueryUj1.FieldByName('KS_OSSZEG').AsString);
		megj	:= QueryUj1.FieldByName('KS_MEGJ').AsString;
		vantele	:= ( QueryUj1.FieldByName('KS_TELE').AsString = '1' ) ;
		QueryUj1.Next;
		while not QueryUj1.Eof do begin
			uzmeny 	:= uzmeny + StringSzam(QueryUj1.FieldByName('KS_UZMENY').AsString);
			osszeg  := osszeg + ( StringSzam(QueryUj1.FieldByName('KS_ARFOLY').AsString) * StringSzam(QueryUj1.FieldByName('KS_OSSZEG').AsString) ) / arfoly;
			if not vantele then begin
				vantele	:= ( QueryUj1.FieldByName('KS_TELE').AsString = '1' ) ;
			end;
			if QueryUj1.FieldByName('KS_MEGJ').AsString <> '' then begin
				megj	:= megj+';'+QueryUj1.FieldByName('KS_MEGJ').AsString;
			end;
			QueryUj1.Next;
		end;
		// A m�dos�t�sok felvitele
		afaert := 0;
		if afasz > 0 then begin
			afaert := ( osszeg * arfoly ) - (osszeg*arfoly)/(1+(afasz/100));
		end;
		telestr2	:= ' N ';
		if vantele then begin
			telestr2	:= ' I ';
		end;
		Query_Update (QueryUj2, 'KOLTSEG',
			[
			'KS_UZMENY', 	SqlSzamString(uzmeny,12,3),
			'KS_OSSZEG',	SqlSzamString(osszeg,12,2),
			'KS_ERTEK',		SqlSzamString(osszeg * arfoly,12,2),
			'KS_AFAERT',	SqlSzamString(afaert,12,2),
			'KS_ATLAG',		'0',
			'KS_TELE',		BoolToStr01(vantele),
			'KS_TELES',		''''+telestr2+'''',
			'KS_MEGJ',		''''+copy(megj, 1, 80)+''''
		], ' WHERE KS_KTKOD = '+ktkod );
		// A f�l�sleges rekordok t�rl�se
		Query_Run (QueryUj1, 'DELETE FROM KOLTSEG WHERE KS_RENDSZ = '''+rsz+''' AND KS_KMORA = '+km+' AND KS_TIPUS = '+tipus+' AND KS_KTKOD <> '+ktkod,true);
		// Az �tlag kisz�m�t�sa
		osszeg	:= GetKoltsegAtlag(ktkod);
		Query_Update (QueryUj2, 'KOLTSEG',
			[
			'KS_ATLAG',		SqlSzamString(osszeg,10,2)
			], ' WHERE KS_KTKOD = '+ktkod );
	end;
	QueryUj1.Destroy;
	QueryUj2.Destroy;
end;


function	MegbizasSzamlaTolto(mbkod : string) : boolean;
var
	QueryUj1	: TADOQuery;
begin
	// A megb�z�sba betessz�k a sz�mlasz�mot
	QueryUj1 		:= TADOQuery.Create(nil);
	QueryUj1.Tag  	:= QUERY_ADAT_TAG;
	EgyebDlg.SeTADOQueryDatabase(QueryUj1);
	Query_Run (QueryUj1, 'SELECT MAX(SA_KOD) SAKOD FROM SZFEJ WHERE SA_UJKOD = (SELECT MAX(VI_UJKOD) FROM VISZONY WHERE VI_VIKOD = ( SELECT MAX(MB_VIKOD) FROM MEGBIZAS WHERE MB_MBKOD = '+mbkod+' )) ',true);
	// Query_Run (QueryUj1, 'UPDATE MEGBIZAS SET MB_SZKOD = '''+QueryUj1.FieldByName('SAKOD').AsString+''' WHERE MB_MBKOD = '+mbkod,true);
  Query_Run (QueryUj1, 'UPDATE MEGBIZAS SET MB_SAKO2 = '''+QueryUj1.FieldByName('SAKOD').AsString+''' WHERE MB_MBKOD = '+mbkod,true);
	QueryUj1.Destroy;
	Result	:= true;
end;


function GetTonnaAtlag(rsz: string; kmtol, kmig : integer; kellonsuly : boolean) : double;
var
   kezdotkm    : integer;
   vegzotkm    : integer;
   atlag       : double;
   atlag2      : double;
   suly        : double;
	QueryDat1	: TADOQuery;
begin
   Result      := 0;
   if (kmig - kmtol) = 0 then begin
       Exit;
   end;
	QueryDat1 		:= TADOQuery.Create(nil);
	QueryDat1.Tag  	:= QUERY_ADAT_TAG;
	EgyebDlg.SeTADOQueryDatabase(QueryDat1);
	Query_Run(QueryDat1, 'SELECT SU_KMORA, SU_KMOR2, SU_ERTEK, SU_JAKOD FROM SULYOK, JARAT WHERE SU_JAKOD = JA_KOD AND JA_RENDSZ = '''+rsz+''' '+
       ' AND ( '+
            '( SU_KMORA >= '+IntToStr(kmtol)+' AND SU_KMORA <  '+IntToStr(kmig)+' ) OR '+
            '( SU_KMOR2 >  '+IntToStr(kmtol)+' AND SU_KMOR2 <= '+IntToStr(kmig)+' ) OR '+
            '( SU_KMORA <  '+IntToStr(kmtol)+' AND SU_KMOR2 >  '+IntToStr(kmig)+' ) '+
       ' ) ORDER BY SU_KMORA ');
   atlag   := 0;
   atlag2  := 0;
   while not QueryDat1.Eof do begin
       kezdotkm    := Max(StrToIntDef(QueryDat1.FieldByName('SU_KMORA').AsString, 0), kmtol);
       vegzotkm    := Min(StrToIntDef(QueryDat1.FieldByName('SU_KMOR2').AsString, 0), kmig);
       atlag       := atlag + ( vegzotkm - kezdotkm )* StringSzam(QueryDat1.FieldByName('SU_ERTEK').AsString);
       QueryDat1.Next;
   end;
   atlag   := atlag / (kmig - kmtol);       // Ez a rakod�si s�ly �tlaga
   if kellonsuly then begin
       // Kisz�moljuk a p�tkocsi �ns�lyokhoz tartoz� �tlagot is
       Query_Run(QueryDat1, 'SELECT JP_POREN, JP_PORKM, JP_PORK2 FROM JARPOTOS, JARAT WHERE JP_JAKOD = JA_KOD AND JA_RENDSZ = '''+rsz+''' '+
           ' AND ( '+
                '( JP_PORKM >= '+IntToStr(kmtol)+' AND JP_PORKM <  '+IntToStr(kmig)+' ) OR '+
                '( JP_PORK2 >  '+IntToStr(kmtol)+' AND JP_PORK2 <= '+IntToStr(kmig)+' ) OR '+
                '( JP_PORKM <  '+IntToStr(kmtol)+' AND JP_PORK2 >  '+IntToStr(kmig)+' ) '+
           ' ) ORDER BY JP_PORKM ');
       atlag2   := 0;
       while not QueryDat1.Eof do begin
           kezdotkm    := Max(StrToIntDef(QueryDat1.FieldByName('JP_PORKM').AsString, 0), kmtol);
           vegzotkm    := Min(StrToIntDef(QueryDat1.FieldByName('JP_PORK2').AsString, 0), kmig);
           suly        := StringSzam(Query_Select('GEPKOCSI', 'GK_RESZ', QueryDat1.FieldByName('JP_POREN').AsString, 'GK_OSULY'));
           atlag2      := atlag2 + ( vegzotkm - kezdotkm )* suly;
           QueryDat1.Next;
       end;
       atlag2   := atlag2 / (kmig - kmtol);       // Ez a p�tkocsi �ns�ly �tlaga
   end;
   Result  := atlag + atlag2;
   QueryDat1.Destroy;
end;

function	MegbizasKieg_Utvonal_is(mbkod : string) : boolean;
var
   utinfo: TUtvonalInfo;
   MB_EUDIJ: double;
   QueryDat1: TADOQuery;
begin
	 QueryDat1:= TADOQuery.Create(nil);
	 QueryDat1.Tag:= QUERY_ADAT_TAG;
	 EgyebDlg.SeTADOQueryDatabase(QueryDat1);

   MB_EUDIJ:= StrToFloat(Query_select('MEGBIZAS', 'MB_MBKOD', mbkod, 'MB_EUDIJ'));
   utinfo:= EgyebDlg.Megbizas_valtozas(StrToInt(mbkod), MB_EUDIJ, mbMODOSIT);
   Query_Update (QueryDat1, 'MEGBIZAS', [
     'MB_TAVKM', IntToStr(UTINFO.Kilometer),
     'MB_TAVMI',     ''''+UTINFO.Leiras+'''',
     'MB_ATAKM', IntToStr(UTINFO.Atallas)
     ], ' WHERE MB_MBKOD = '+mbkod );
   Result:= MegbizasKieg(mbkod);
   QueryDat1.Destroy;
end;


function GetArfolyamDat(MBKOD: string): string;
var
   S: string;
   QueryDat1: TADOQuery;
begin
   QueryDat1:= TADOQuery.Create(nil);
   try
     QueryDat1.Tag:= QUERY_ADAT_TAG;
     EgyebDlg.SeTADOQueryDatabase(QueryDat1);
      S:= 'select isnull(case when (select V_ARNAP from MEGBIZAS left join VEVO on V_KOD = MB_VEKOD where MB_MBKOD= '+MBKOD+')=''0'' then '+
          '	MIN(case when MS_FETDAT is null then MS_FELDAT else MS_FETDAT end)  '+
          '	  else MAX(case when MS_LETDAT is null then MS_LERDAT else MS_LETDAT end)  '+
          '	  end, '''')  '+
          '	 FROM MEGSEGED WHERE MS_MBKOD = '+ MBKOD;
      Query_Run(QueryDat1, S);
      if not QueryDat1.Eof then
        Result:= QueryDat1.Fields[0].AsString
      else Result:= '';
   finally
     QueryDat1.Free;
     end; // try-finally
end;

function	MegbizasKieg(mbkod : string) : boolean;
var
	QueryDat1	: TADOQuery;
   eudij       : double;
   arfoly      : double;
   ardat, tedat: string;
   alap        : double;
   viszony     : string;
   szdij       : double;

   felceg      : string;
   felors      : string;
   felrak      : string;
   leceg       : string;
   leors       : string;
   lerak       : string;
   GKKAT       : string;
   S           : string;
   vevokod, V_ARNAP, MB_DATUM, KEDAT: string;
   SzamlaStatus, Szamlakod, LogInfo: string;
   OutRecord: T_TeljesDijSzamitasRec;

begin
   // A megb�z�s kieg�sz�t� inform�ci�k el��ll�t�sa
   Result          := true;
   if mbkod = '' then begin
       Result  := false;
       Exit;
   end;
  Query_Log_Str('Megbizaskieg('+mbkod+')', 0, true);  // napl�zzuk a h�v�s t�ny�t
	QueryDat1 		:= TADOQuery.Create(nil);
	QueryDat1.Tag  	:= QUERY_ADAT_TAG;
	EgyebDlg.SeTADOQueryDatabase(QueryDat1);

  // A t�ny d�tum meghat�roz�sa
   Query_Run(QueryDat1, 'SELECT MIN(MS_FETDAT) MINDAT FROM MEGSEGED WHERE MS_MBKOD = '+IntToStr(StrToIntdef(mbkod, 0))+' AND MS_FELNEV = MS_TEFNEV AND MS_FORSZ = MS_TEFOR AND MS_FELIR = MS_TEFIR AND MS_FELTEL = MS_TEFTEL AND MS_TEFCIM = MS_FELCIM');
   tedat   := QueryDat1.FieldByName('MINDAT').AsString;
   // ----- van hogy m�s n�vvel szerepel a k�t helyen: akkor hagyjuk a nevet
   if tedat='' then begin
     Query_Run(QueryDat1, 'SELECT MIN(MS_FETDAT) MINDAT FROM MEGSEGED WHERE MS_MBKOD = '+IntToStr(StrToIntdef(mbkod, 0)));
     tedat   := QueryDat1.FieldByName('MINDAT').AsString;
     end;
   // ----- ha m�g mindig �res, akkor MS_DATUM alapj�n. Nem j� az �res MB_TEDAT, mert �gy nem l�tszik a "Megb�z�sok" t�bl�zatban.
   if tedat='' then begin
     Query_Run(QueryDat1, 'SELECT MIN(MS_DATUM) MINDAT FROM MEGSEGED WHERE MS_MBKOD = '+IntToStr(StrToIntdef(mbkod, 0)));
     tedat:= QueryDat1.FieldByName('MINDAT').AsString;
     end;


  // A tervezett felrak�si d�tum meghat�roz�sa
   Query_Run(QueryDat1, 'SELECT MIN(MS_FELDAT) MINDAT FROM MEGSEGED WHERE MS_MBKOD = '+IntToStr(StrToIntdef(mbkod, 0))+' AND MS_FELNEV = MS_TEFNEV AND MS_FORSZ = MS_TEFOR AND MS_FELIR = MS_TEFIR AND MS_FELTEL = MS_TEFTEL AND MS_TEFCIM = MS_FELCIM');
   KEDAT   := QueryDat1.FieldByName('MINDAT').AsString;

   // Az eur�s �td�j meghat�roz�sa
   Query_Run(QueryDat1, 'SELECT MB_FUDIJ, MB_FUNEM, MB_SEDIJ, MB_SENEM, MB_DATUM, MB_VEKOD FROM MEGBIZAS WHERE MB_MBKOD = '+mbkod);
   MB_DATUM:= QueryDat1.FieldByName('MB_DATUM').AsString;
   vevokod:= QueryDat1.FieldByName('MB_VEKOD').AsString;
   {V_ARNAP:= Query_Select('VEVO','V_KOD',vevokod,'V_ARNAP');
   // A t�ny d�tum meghat�roz�sa
   if V_ARNAP='0' then begin  // �rfolyam: felrak�s napja
     // a MEGSEGED felrak�s d�tumai k�z�l a legkisebb, lehet el�rak�s is!  (nem kell MS_FAJTA=''norm�l'' )
     // amelyik sorban van t�ny id�, ott az, egy�bk�nt a tervezett
     S:='SELECT MIN(case when MS_FETDAT is null then MS_FELDAT else MS_FETDAT end) MINDAT FROM MEGSEGED WHERE MS_MBKOD = '+mbkod;
     end
   else begin
     // a MEGSEGED lerak�s d�tumai k�z�l a legnagyobb
     // amelyik sorban van t�ny id�, ott az, egy�bk�nt a tervezett
     S:= 'SELECT MAX(case when MS_LETDAT is null then MS_LERDAT else MS_LETDAT end) MAXDAT FROM MEGSEGED WHERE MS_MBKOD = '+mbkod;
     end;
   Query_Run(QueryDat1, S);
   ardat   := QueryDat1.Fields[0].AsString;
    }
   ardat:= GetArfolyamDat (mbkod);
   if ardat = '' then begin
      ardat   := MB_DATUM;
      end;

   // A viszony meghat�roz�sa
   viszony := '';
   Query_Run(QueryDat1, 'SELECT DISTINCT MS_EXSTR FROM MEGSEGED WHERE MS_MBKOD = '+mbkod);
   while not QueryDat1.Eof do begin
       if QueryDat1.FieldByName('MS_EXSTR').AsString <> '' then begin
           viszony := viszony + QueryDat1.FieldByName('MS_EXSTR').AsString +', ';
       end;
       QueryDat1.Next;
   end;
   // A sz�mla d�j�nak meghat�roz�sa
   szdij   := 0;
   Query_Run(QueryDat1, 'SELECT VI_UJKOD, SA_FLAG FROM VISZONY, MEGBIZAS, SZFEJ WHERE MB_VIKOD = VI_VIKOD AND VI_UJKOD = SA_UJKOD AND MB_MBKOD = '+mbkod);
   if not QueryDat1.Eof then begin
      SzamlaStatus:= QueryDat1.FieldByName('SA_FLAG').AsString;
      Szamlakod:= QueryDat1.FieldByName('VI_UJKOD').AsString;
      If (Szamlakod <> '') and ((SzamlaStatus = '0') or (SzamlaStatus = '3')) then begin  // csak v�gleges�tett vagy sztorn� sz�mla
         JSzamla		:= TJSzamla.Create(Application);
         JSzamla.FillSzamla(QueryDat1.FieldByName('VI_UJKOD').AsString);
         // szdij       := JSzamla.ossz_nettoeur;
         szdij:= JSzamla.ossz_nettoeur2;
         JSzamla.Destroy;
         end;  // if
      LogInfo:='SzamlaStatus='+SzamlaStatus+', Szamlakod='+Szamlakod+', szdij='+FormatFloat('0.00',szdij);
      Query_Log_Str('Megbizaskieg('+mbkod+'): '+LogInfo, 0, true);  // napl�zzuk a h�v�s t�ny�t
      end;

   // A megb�z�s kieg�sz�t� inform�ci�k el��ll�t�sa
   Query_Run(QueryDat1, 'SELECT MS_FELNEV, MS_FORSZ, MS_FELIR, MS_FELTEL, MS_FELCIM, MS_LERNEV, MS_ORSZA, MS_LELIR, MS_LERTEL, MS_LERCIM FROM MEGSEGED WHERE MS_MBKOD = '+mbkod+' AND MS_FAJKO = 0 ORDER BY MS_MBKOD, MS_SORSZ, MS_TIPUS');
   if QueryDat1.RecordCount > 0 then begin
       QueryDat1.First;
       felceg  := QueryDat1.FieldByName('MS_FELNEV').AsString;
       felrak  := copy(QueryDat1.FieldByName('MS_FORSZ').AsString+'-'+QueryDat1.FieldByName('MS_FELIR').AsString+' '+
           QueryDat1.FieldByName('MS_FELTEL').AsString+'; '+QueryDat1.FieldByName('MS_FELCIM').AsString, 1, 100);
       felors  := QueryDat1.FieldByName('MS_FORSZ').AsString;
       QueryDat1.Last;
       leceg   := QueryDat1.FieldByName('MS_LERNEV').AsString;
       lerak   := copy(QueryDat1.FieldByName('MS_ORSZA').AsString+'-'+QueryDat1.FieldByName('MS_LELIR').AsString+' '+
       QueryDat1.FieldByName('MS_LERTEL').AsString+'; '+QueryDat1.FieldByName('MS_LERCIM').AsString, 1, 100);
       leors   := QueryDat1.FieldByName('MS_ORSZA').AsString;
   end;

   // teljes fuvard�j (EUR) friss�t�se az �rfolyam d�tumok miatt
   Query_Run(QueryDat1, 'SELECT MB_FUDIJ, MB_FUNEM, MB_SEDIJ, MB_SENEM, MB_VEKOD, MB_TAVKM, MB_ATAKM, MB_GKKAT, MB_ALDIJ, MB_ALNEM '+
        ' FROM MEGBIZAS WHERE MB_MBKOD = '+mbkod);
   GKKAT:= QueryDat1.FieldByName('MB_GKKAT').AsString;

   OutRecord:= EgyebDlg.TeljesDijSzamitas_mag(StrToInt(mbkod), ardat,
                  QueryDat1.FieldByName('MB_VEKOD').AsString,  // vev�k�d (�zemanyag p�tl�khoz)
                  QueryDat1.FieldByName('MB_TAVKM').AsInteger, // �ssz KM, 0 ha NINCS_ADAT
                  QueryDat1.FieldByName('MB_ATAKM').AsInteger, // �t�ll�s KM
                  QueryDat1.FieldByName('MB_FUDIJ').AsFloat, // Fuvard�j
                  QueryDat1.FieldByName('MB_SEDIJ').AsFloat,  // Egy�b d�j
                  QueryDat1.FieldByName('MB_ALDIJ').AsFloat, // alv�llalkoz�i d�j
                  QueryDat1.FieldByName('MB_FUNEM').AsString, // Fuvard�j devizanem
                  QueryDat1.FieldByName('MB_SENEM').AsString, // Egy�b d�j devizanem
                  QueryDat1.FieldByName('MB_ALNEM').AsString); // alv�llakoz�i d�j devizanem
   Query_Update(QueryDat1, 'MEGBIZAS',
       [
       'MB_SZDIJ',  SqlSzamString(szdij, 12, 2),
       'MB_VISZO',  ''''+viszony+'''',
       'MB_TEDAT',  ''''+tedat+'''',
       'MB_KEDAT',  ''''+KEDAT+'''',
       'MB_FECEG',  ''''+felceg+'''',
       'MB_FERAK',  ''''+felrak+'''',
       'MB_LECEG',  ''''+leceg+'''',
       'MB_LERAK',  ''''+lerak+'''',
       'MB_FORSZ',  ''''+felors+'''',
       'MB_LORSZ',  ''''+leors+'''',
       'MB_EUDIJ',   SqlSzamString(OutRecord.teljesdij_EUR, 12, 2),
       'MB_ALEUR',   SqlSzamString(OutRecord.alvallalkozo_EUR, 12, 2),
       'MB_ALEURKM', SqlSzamString(OutRecord.AlEURKm, 12, 3),
       'MB_EURKM',   SqlSzamString(OutRecord.EURKm, 12, 3),
       'MB_EURKMU',  SqlSzamString(OutRecord.EURKmAtallas, 12, 3)
       ], ' WHERE MB_MBKOD = '+mbkod);

   QueryDat1.Destroy;
end;

function	MegSegedJarat(mbkod, mskod : string ) : boolean;
var
	QueryDat1	: TADOQuery;
	QueryDat2	: TADOQuery;
	QueryDat3	: TADOQuery;
   jakod       : string;
   alkod       : string;
   kezdat      : string;
   kezido      : string;
   vegdat      : string;
   vegido      : string;
   orsza       : string;
   potsz       : string;
   sof1        : string;
   sof2        : string;
   kellupdate  : boolean;
begin
   // A megseged t�bla alapj�n l�trehozzuk a j�ratot
	QueryDat1 		:= TADOQuery.Create(nil);
	QueryDat2 		:= TADOQuery.Create(nil);
	QueryDat3 		:= TADOQuery.Create(nil);
	QueryDat1.Tag  	:= QUERY_ADAT_TAG;
	QueryDat2.Tag  	:= QUERY_ADAT_TAG;
	QueryDat3.Tag  	:= QUERY_ADAT_TAG;
	EgyebDlg.SeTADOQueryDatabase(QueryDat1);
	EgyebDlg.SeTADOQueryDatabase(QueryDat2);
	EgyebDlg.SeTADOQueryDatabase(QueryDat3);
   // Ellen�rizz�k a l�tez�st
   Query_Run(QueryDat1, 'SELECT * FROM JARATMEGSEGED WHERE JG_MBKOD = '+mbkod+' AND JG_MSKOD = '+mskod);
   if QueryDat1.RecordCount > 0 then begin
       Result  := true;
       Exit;
   end;

   Query_Run ( QueryDat1, 'SELECT * FROM MEGSEGED WHERE MS_MBKOD = '+mbkod+' AND MS_MSKOD = '+mskod);
   if QueryDat1.FieldByName('MS_FAJKO').AsString = '0' then begin
       HibaKiiro('Rossz MEGSEGED ! Norm�l rakod�s: '+mskod);
   end;

   Query_Run(QueryDat2, 'SELECT JA_KOD FROM JARAT, JARATMEGSEGED WHERE JG_MBKOD = '+mbkod+
       ' AND JA_ALVAL = -1 AND JA_RENDSZ = '''+QueryDat1.FieldByName('MS_RENSZ').AsString+''' AND JA_KOD = JG_JAKOD ');
   if QueryDat2.RecordCount < 1 then begin
       // L�tre kell hozni az �j j�ratot
       jakod	:= Format('%6.6d',[GetNextStrCode('JARAT', 'JA_KOD', 0, 6)]);
       {�j j�ratsz�m k�d gener�l�sa}
       if Query_Run (QueryDat2, 'SELECT MAX(JA_ALKOD) MAXERT FROM JARAT ',true) then begin
           alkod		:= Format('%5.5d',[StrToIntDef(QueryDat2.FieldByName('MAXERT').AsString,0) + 1]);
       end;
       {Az �j j�rat l�trehoz�sa}
       Query_Run ( QueryDat2, 'INSERT INTO JARAT (JA_KOD, JA_ALVAL, JA_ALKOD) VALUES ('''+jakod+''', -1, '''+alkod+''' )',true);
//       Query_Run ( QueryDat2, 'INSERT INTO ALJARAT (AJ_JAKOD) VALUES ('''+jakod+''' )',true);
   end else begin
       // Nem kell �j j�rat, csak j�ratk�d
       jakod   := QueryDat2.FieldByName('JA_KOD').AsString
   end;

   // Az �j JARATMEGSEGED l�trehoz�sa
   Query_Run ( QueryDat2, 'INSERT INTO JARATMEGSEGED ( JG_JAKOD, JG_MBKOD, JG_MSKOD, JG_FOJAR ) VALUES ( '+
       ''''+ jakod + ''', '+mbkod +','+mskod +','''+ Query_Select('MEGBIZAS', 'MB_MBKOD', mbkod, 'MB_JAKOD')+''' )');

   // A j�rat ellen�rz�se, hogy m�dosult-e a dolog
   kellupdate  := false;
   Query_Run ( QueryDat3, 'SELECT * FROM JARAT WHERE JA_KOD = '''+jakod+''' ');
   kezdat  := QueryDat1.FieldByName('MS_FETDAT').AsString;
   kezido  := QueryDat1.FieldByName('MS_FETIDO').AsString;
   vegdat  := QueryDat1.FieldByName('MS_LETDAT').AsString;
   vegido  := QueryDat1.FieldByName('MS_LETIDO').AsString;
   orsza   := QueryDat3.FieldByName('JA_ORSZ').AsString;
   potsz   := QueryDat3.FieldByName('JA_JPOTK').AsString;
   sof1    := QueryDat3.FieldByName('JA_SOFK1').AsString;
   sof2    := QueryDat3.FieldByName('JA_SOFK2').AsString;
   if QueryDat1.FieldByName('MS_TIPUS').AsString = '0' then begin
       // Felrak�sr�l van sz�
       if QueryDat3.FieldByName('JA_JKEZD').AsString + QueryDat3.FieldByName('JA_KEIDO').AsString > QueryDat1.FieldByName('MS_FETDAT').AsString + QueryDat1.FieldByName('MS_FETIDO').AsString then begin
           kellupdate  := true;
           kezdat      := QueryDat1.FieldByName('MS_FETDAT').AsString;
           kezido      := QueryDat1.FieldByName('MS_FETIDO').AsString;
       end;
   end else begin
       // Lerak�sr�l van sz�
       if QueryDat3.FieldByName('JA_JVEGE').AsString + QueryDat3.FieldByName('JA_VEIDO').AsString < QueryDat1.FieldByName('MS_LETDAT').AsString + QueryDat1.FieldByName('MS_LETIDO').AsString then begin
           kellupdate  := true;
           vegdat      := QueryDat1.FieldByName('MS_LETDAT').AsString;
           vegido      := QueryDat1.FieldByName('MS_LETIDO').AsString;
       end;
   end;

   if ( ( orsza <> '' ) and (orsza  <> QueryDat1.FieldByName('MS_ORSZA').AsString) ) then begin
       // T�bb orsz�g van benne
       // NoticeKi('T�bb orsz�g!')
       HibaKiiro('T�bb orsz�g : '+jakod);
   end;
   if ( (  '' <> QueryDat1.FieldByName('MS_ORSZA').AsString ) and ( orsza <> QueryDat1.FieldByName('MS_ORSZA').AsString ) )then begin
       kellupdate  := true;
       orsza       := QueryDat1.FieldByName('MS_ORSZA').AsString;
   end;
   if ( ( potsz <> '' ) and (potsz  <> QueryDat1.FieldByName('MS_POTSZ').AsString) ) then begin
       // NoticeKi('T�bb p�tkocsi!')
   end;
   if ( ( '' <> QueryDat1.FieldByName('MS_POTSZ').AsString ) and ( potsz <> QueryDat1.FieldByName('MS_POTSZ').AsString ) ) then begin
       kellupdate  := true;
       potsz       := QueryDat1.FieldByName('MS_POTSZ').AsString;
   end;
   if ( ( sof1 <> '' ) and (sof1  <> QueryDat1.FieldByName('MS_DOKOD').AsString) ) then begin
       // NoticeKi('T�bb sof�r 1!')
   end;
   if ( ( '' <> QueryDat1.FieldByName('MS_DOKOD').AsString ) and (sof1 <> QueryDat1.FieldByName('MS_DOKOD').AsString) ) then begin
       kellupdate  := true;
       sof1        := QueryDat1.FieldByName('MS_DOKOD').AsString;
   end;
   if ( ( sof2 <> '' ) and (sof2  <> QueryDat1.FieldByName('MS_DOKO2').AsString) ) then begin
       // NoticeKi('T�bb sof�r 2!')
   end;
   if ( ( '' <> QueryDat1.FieldByName('MS_DOKO2').AsString ) and (sof2 <> QueryDat1.FieldByName('MS_DOKO2').AsString) ) then begin
       kellupdate  := true;
       sof2        := QueryDat1.FieldByName('MS_DOKO2').AsString;
   end;

   if kellupdate then begin
       Query_Update ( QueryDat2, 'JARAT' , [
           'JA_ORSZ',		''''+orsza+'''',
//         'JA_ALKOD',		alkod,
           'JA_MEGJ',		''''+'Gener�lt j�rat'+'''',
           'JA_JARAT',		''''+''+'''',
           'JA_JKEZD',		''''+kezdat+'''',
           'JA_KEIDO',		''''+kezido+'''',
           'JA_JVEGE',		''''+vegdat+'''',
           'JA_VEIDO',		''''+vegido+'''',
    //       'JA_JAORA',		idotart,
    //       'JA_LFDAT',		''''+M2B.Text+'''',
           'JA_RENDSZ',	''''+QueryDat1.FieldByName('MS_RENSZ').AsString+'''',
    //       'JA_MENSZ',		''''+M13.Text+'''',
           'JA_FAZIS',		'0',
           'JA_FAZST',     ''''+GetJaratFazis(0)+'''',
    //       'JA_NYITKM',	IntToStr(StrToIntDef(M14.Text,0)),
    //       'JA_ZAROKM',	IntToStr(StrToIntDef(M15.Text,0)),
    //       'JA_OSSZKM',	IntToStr(StrToIntDef(MaskEdit1.Text,0)),
    //       'JA_BELKM',	  	IntToStr(StrToIntDef(M27.Text,0)),
    //       'JA_URESK',	  	IntToStr(StrToIntDef(M28.Text,0)),
    //       'JA_UBKM',	  	IntToStr(StrToIntDef(M29.Text,0)),
    //       'JA_UKKM',	  	IntToStr(StrToIntDef(M30.Text,0)),
    //       'JA_EUROS', 	IntToStr(RG1.ItemIndex),
           'JA_JPOTK',		''''+potsz+'''',
           'JA_SOFK1',		''''+sof1+'''',
           'JA_SOFK2',		''''+sof2+'''',
    //       'JA_SZAK1',		s5,
           'JA_SNEV1',		''''+Query_Select('DOLGOZO','DO_KOD', sof1, 'DO_NAME')+'''',
           'JA_SNEV2',		''''+Query_Select('DOLGOZO','DO_KOD', sof2, 'DO_NAME')+'''',
    //       'JA_SZAK2',		s6,
           'JA_FAJKO',     '1'
           ], ' WHERE JA_KOD = '''+jakod+''' ' );
   end;

   QueryDat1.Destroy;
   QueryDat2.Destroy;
   QueryDat3.Destroy;
   Result          := true;
end;

function GetJaratAlkod(evstr : string) : integer;
var
	QueryDat1	: TADOQuery;
begin
  Result  := -1;
	QueryDat1 		:= TADOQuery.Create(nil);
	QueryDat1.Tag  	:= QUERY_ADAT_TAG;
	EgyebDlg.SeTADOQueryDatabase(QueryDat1);
  QueryDat1.CommandTimeout:= 180;  // sajnos itt rendszeresen elakad, most nem tudok okosabbat csin�lni
  // if Query_Run (QueryDat1, 'SELECT MAX(JA_ALKOD) MAXERT FROM JARAT WHERE JA_JKEZD LIKE '''+evstr+'%'' ',true) then begin
  if Query_Run (QueryDat1, 'SELECT MAX(JA_ALKOD) MAXERT FROM JARAT WHERE JA_EV= '+evstr, true) then begin
       Result	:= StrToIntDef(QueryDat1.FieldByName('MAXERT').AsString,0) + 1;
	end;
   QueryDat1.Destroy;
end;

procedure JaratCalc(jakod : string);
var
	QueryDat1	: TADOQuery;
	QueryDat2	: TADOQuery;
   kdat, vdat  : string;
   kido, vido  : string;
   perckul     : integer;
   orakul      : integer;
begin
   // Kisz�m�tjuk a j�raton bel�l eltelt id�t
	QueryDat1 		:= TADOQuery.Create(nil);
	QueryDat1.Tag  	:= QUERY_ADAT_TAG;
	EgyebDlg.SeTADOQueryDatabase(QueryDat1);
	QueryDat2 		:= TADOQuery.Create(nil);
	QueryDat2.Tag  	:= QUERY_ADAT_TAG;
	EgyebDlg.SeTADOQueryDatabase(QueryDat2);
   if Query_Run (QueryDat1, 'SELECT * FROM JARAT WHERE JA_KOD = '''+jakod+''' ',true) then begin
       kdat    := QueryDat1.FieldByName('JA_JKEZD').AsString;
       vdat    := QueryDat1.FieldByName('JA_JVEGE').AsString;
       kido    := QueryDat1.FieldByName('JA_KEIDO').AsString;
       vido    := QueryDat1.FieldByName('JA_VEIDO').AsString;
       if ( ( kdat = '' ) or ( vdat = '' ) ) then begin
           Exit;
       end;
       perckul	:= 	MinutesBetween( EncodeDateTime(StrToIntDef(copy(vdat,1,4),0), StrToIntDef(copy(vdat,6,2),0), StrToIntDef(copy(vdat,9,2),0), StrToIntDef(copy(vido,1,2),0), StrToIntDef(copy(vido,4,2),0), 0, 0 ),
       						    EncodeDateTime(StrToIntDef(copy(kdat,1,4),0), StrToIntDef(copy(kdat,6,2),0), StrToIntDef(copy(kdat,9,2),0), StrToIntDef(copy(kido,1,2),0), StrToIntDef(copy(kido,4,2),0), 0, 0 )
                                 );
       orakul  := perckul mod 60;
       if perckul div 60 > 30 then begin
           orakul  := orakul + 1;
       end;
       Query_Run (QueryDat1, 'UPDATE JARAT SET JA_JAORA = '+IntToStr(orakul)+' WHERE JA_KOD = '''+jakod+''' ',true);
       // A sof�r�k kiv�tele
       Query_Run (QueryDat1, 'SELECT DISTINCT JS_SOFOR, JS_SZAKM FROM JARSOFOR WHERE JS_JAKOD = '''+jakod+''' ',true);
       if QueryDat1.RecordCount > 0 then begin
           Query_Update ( QueryDat2, 'JARAT' , [
               'JA_SOFK1',		''''+QueryDat1.FieldByName('JS_SOFOR').AsString+'''',
               'JA_SZAK1',		IntToStr(StrToIntDef(QueryDat1.FieldByName('JS_SZAKM').AsString, 0)),
               'JA_SNEV1',		''''+Query_Select('DOLGOZO','DO_KOD', QueryDat1.FieldByName('JS_SOFOR').AsString, 'DO_NAME')+''''
               ], ' WHERE JA_KOD = '''+jakod+''' ' );
           QueryDat1.Next;
           if not QueryDat1.Eof then begin
               Query_Update ( QueryDat2, 'JARAT' , [
                   'JA_SOFK2',		''''+QueryDat1.FieldByName('JS_SOFOR').AsString+'''',
                   'JA_SZAK2',		IntToStr(StrToIntDef(QueryDat1.FieldByName('JS_SZAKM').AsString, 0)),
                   'JA_SNEV2',		''''+Query_Select('DOLGOZO','DO_KOD', QueryDat1.FieldByName('JS_SOFOR').AsString, 'DO_NAME')+''''
                   ], ' WHERE JA_KOD = '''+jakod+''' ' );
           end;
       end;

       // A p�tkocsi kiv�tele
       Query_Run (QueryDat1, 'SELECT DISTINCT JP_POREN FROM JARPOTOS WHERE JP_JAKOD = '''+jakod+''' ',true);
       if QueryDat1.RecordCount > 0 then begin
           Query_Update ( QueryDat2, 'JARAT' ,
               [
                   'JA_JPOTK',		''''+QueryDat1.FieldByName('JP_POREN').AsString+''''
               ], ' WHERE JA_KOD = '''+jakod+''' ' );
       end;
   end;
   QueryDat1.Destroy;
end;

function GetServerDateTime(dt_type : integer = 0) : string;
var
	QueryDat1	: TADOQuery;
   dt          : string;
   dt_date     : string;
   dt_time     : string;
begin
   Result          := '';
	QueryDat1 		:= TADOQuery.Create(nil);
	QueryDat1.Tag  	:= QUERY_ADAT_TAG;
	EgyebDlg.SeTADOQueryDatabase(QueryDat1);
   QueryDat1.Sql.Clear;
   QueryDat1.SQL.Add('SELECT CONVERT( NVARCHAR, SYSDATETIME(), 121) DT');
   try
       QueryDat1.Open;
       dt      := QueryDat1.FieldByName('DT').AsString;
       dt_date := copy(dt, 1, 4)+'.'+copy(dt, 6, 2)+'.'+copy(dt, 9, 2)+'.';
       dt_time := copy(dt, 12, 100);
       case dt_type of
           0 : Result := dt_date;                  // Csak a d�tum kell    ����.HH.NN.
           1 : Result := dt_time;                  // Csak az id�pont kell ��:PP:SS.MMMMMMM
           2 : Result := dt_date + ' ' + dt_time;  // Minden kell          ����.HH.NN. ��:PP:SS.MMMMMMM
       end;
   except
       // Nem siker�lt az id�pont lek�r�s
       on E: Exception do begin// ErrorDialog(E.Message , E.HelpContext);
		    ex_str := E.Message;
       end;
   end;
   QueryDat1.Destroy;
end;

function GetRightTag(tagszam	: integer; LehetSuperuser: boolean = True)	: integer;
var
	Query1	: TADOQuery;
begin
	Result := RG_ALLRIGHT;
  if LehetSuperuser and EgyebDlg.user_super then begin
		Exit;
  	end;
	Query1		:= TADOQuery.Create(nil);
	EgyebDlg.SetAdoQueryDatabase(Query1);
	if not Query_Run(Query1, 'SELECT * from JOGOS where JO_DIKOD = '+IntToStr(tagszam)+
		' AND JO_FEKOD = '''+EgyebDlg.user_code+''' ') then begin
		Exit;
	end;
	Result := Query1.FieldByName('JO_JOGOS').AsInteger;
	Query1.Destroy;
end;


Procedure TLogThread.Execute ;
var
	F       : TextFile;
  Siker: boolean;
  i: integer;
begin
   // Be�rjuk az �rt�ket
	AssignFile(F, fnev);
  Siker:= False;
  i:= 0;
  while (not Siker or (i<=100)) do begin  // max 100 pr�b�lkoz�s
  	{$I-}
    if FileExists(fnev) then begin
      Append(F)
    end else begin
      Rewrite(F);
    end;
    {$I+}
    if IOResult = 0 then
        Siker:= True
    else Sleep(10); // egy kis v�rakoz�s
    Inc(i);
    end;  // while
  if Siker then
       Writeln(F, logs);
  // DEBUG .... else MessageDlg('Nincs ki�rva :-(  '+logs, mtError, [mbOK], 0);
	Flush(f);
  // DEBUG...if i <> 0 then MessageDlg('Flush error!', mtError, [mbOK], 0);
	CloseFile(f);
end;

{ a r�gi....Procedure TLogThread.Execute ;
var
	F       : TextFile;
  i: integer;
begin
   // Be�rjuk az �rt�ket
	// *******hogy ki tudjam egyben kommentezni... '$I-'
	AssignFile(F, fnev);
	if FileExists(fnev) then begin
		Append(F)
	end else begin
		Rewrite(F);
	end;
	// *****hogy ki tudjam egyben kommentezni... '$I+'
   if IOResult <> 0 then begin
       // Nem siker�lt ki�rni a sz�veget
       // DEBUG...MessageDlg('IOResult='+IntToStr(IOResult), mtError, [mbOK], 0);
   end else begin
       Writeln(F, logs);
   end;
	i:= Flush(f);
  // DEBUG...if i <> 0 then MessageDlg('Flush error!', mtError, [mbOK], 0);
	CloseFile(f);
end;}

Constructor TLogThread.CreateNew(fn, ls : string) ;
begin
   Create(false);
   FreeOnTerminate := True ;
   fnev    := fn;
   logs    := ls;
end;

function	GetMegys(nyelv, megys : string) : string;
var
	QueryUj1	: TADOQuery;
   kodstr      : string;
   kodsz       : integer;
begin
	// A k�lf�ldi mennyis�gi egys�g beolvas�sa a magyar alapj�n
	QueryUj1 		:= TADOQuery.Create(nil);
	QueryUj1.Tag  	:= QUERY_KOZOS_TAG;
	EgyebDlg.SeTADOQueryDatabase(QueryUj1);
   if nyelv = 'HUN' then begin
       Result  := megys;
       Exit;
   end;
   if Query_Run(QueryUj1, 'SELECT * FROM SZOTAR WHERE SZ_FOKOD = ''220'' AND SZ_MENEV = '''+megys+''' ',true) then begin
       if QueryUj1.RecordCount > 0 then begin
           // L�tezik m�r ez a magyar bejegyz�s
           kodstr  := nyelv + copy(QueryUj1.FieldByName('SZ_ALKOD').AsString, 4, 2);
       end else begin
           // Nincs m�g meg a magyar sem
           kodsz   := 1;
           kodstr  := Format('HUN%2.2d', [kodsz]);
           Query_Run(QueryUj1, 'SELECT * FROM SZOTAR WHERE SZ_FOKOD = ''220'' AND SZ_ALKOD = '''+kodstr+''' ',true);
           while QueryUj1.RecordCount > 0 do begin
               Inc(kodsz);
               kodstr  := Format('HUN%2.2d', [kodsz]);
               Query_Run(QueryUj1, 'SELECT * FROM SZOTAR WHERE SZ_FOKOD = ''220'' AND SZ_ALKOD = '''+kodstr+''' ',true);
           end;
           // Be�rjuk az �j magyar elemet
           Query_Run(QueryUj1,  'INSERT INTO SZOTAR ( SZ_FOKOD, SZ_ALKOD, SZ_MENEV, SZ_ERVNY ) VALUES ( ''220'', '''+kodstr+''', '''+megys+''', 1 ) ', FALSE);
           kodstr  := nyelv + Format('%2.2d', [kodsz]);
       end;
   end;
   if Query_Run(QueryUj1, 'SELECT * FROM SZOTAR WHERE SZ_FOKOD = ''220'' AND SZ_ALKOD = '''+kodstr+''' ',true) then begin
       if QueryUj1.RecordCount = 0 then begin
           Query_Run(QueryUj1,  'INSERT INTO SZOTAR ( SZ_FOKOD, SZ_ALKOD, SZ_MENEV, SZ_ERVNY ) VALUES ( ''220'', '''+kodstr+''', '''+'Hi�nyz� k�lf�ldi mennyis�gi egys�g'+''', 1 ) ', FALSE);
           Result  := '';
       end else begin
           Result  := QueryUj1.FieldByName('SZ_MENEV').AsString;
       end;
   end;
end;

function	GetAfakod(nyelv, hunkod : string) : string;
var
	QueryUj1	: TADOQuery;
   kodstr      : string;
   kodsz       : integer;
begin
	// A k�lf�ldi mennyis�gi egys�g beolvas�sa a magyar alapj�n
	QueryUj1 		:= TADOQuery.Create(nil);
	QueryUj1.Tag  	:= QUERY_KOZOS_TAG;
	EgyebDlg.SeTADOQueryDatabase(QueryUj1);
   if nyelv = 'HUN' then begin
       Result  := hunkod;
       Exit;
   end;
   if Query_Run(QueryUj1, 'SELECT * FROM SZOTAR WHERE SZ_FOKOD = ''230'' AND SZ_MENEV = '''+hunkod+''' ',true) then begin
       if QueryUj1.RecordCount > 0 then begin
           // L�tezik m�r ez a magyar bejegyz�s
           kodstr  := nyelv + copy(QueryUj1.FieldByName('SZ_ALKOD').AsString, 4, 2);
       end else begin
           // Nincs m�g meg a magyar sem
           kodsz   := 1;
           kodstr  := Format('HUN%2.2d', [kodsz]);
           Query_Run(QueryUj1, 'SELECT * FROM SZOTAR WHERE SZ_FOKOD = ''230'' AND SZ_ALKOD = '''+kodstr+''' ',true);
           while QueryUj1.RecordCount > 0 do begin
               Inc(kodsz);
               kodstr  := Format('HUN%2.2d', [kodsz]);
               Query_Run(QueryUj1, 'SELECT * FROM SZOTAR WHERE SZ_FOKOD = ''230'' AND SZ_ALKOD = '''+kodstr+''' ',true);
           end;
           // Be�rjuk az �j magyar elemet
           Query_Run(QueryUj1,  'INSERT INTO SZOTAR ( SZ_FOKOD, SZ_ALKOD, SZ_MENEV, SZ_ERVNY ) VALUES ( ''230'', '''+kodstr+''', '''+hunkod+''', 1 ) ', FALSE);
           kodstr  := nyelv + Format('%2.2d', [kodsz]);
       end;
   end;
   if Query_Run(QueryUj1, 'SELECT * FROM SZOTAR WHERE SZ_FOKOD = ''230'' AND SZ_ALKOD = '''+kodstr+''' ',true) then begin
       if QueryUj1.RecordCount = 0 then begin
           Query_Run(QueryUj1,  'INSERT INTO SZOTAR ( SZ_FOKOD, SZ_ALKOD, SZ_MENEV, SZ_ERVNY ) VALUES ( ''230'', '''+kodstr+''', '''+'Hi�nyz� k�lf�ldi mennyis�gi egys�g'+''', 1 ) ', FALSE);
           Result  := '';
       end else begin
           Result  := QueryUj1.FieldByName('SZ_MENEV').AsString;
       end;
   end;
end;

// A sz�mla c�mk�k sz�vegeinek megkeres�se
function GetSzoveg(vkod, alap, nyelv : string) : string;
var
	QueryUj1	: TADOQuery;
   nyelvek     : Tstringlist;
   i           : integer;
   str         : string;
begin
	QueryUj1 		:= TADOQuery.Create(nil);
	QueryUj1.Tag  	:= QUERY_KOZOS_TAG;
	EgyebDlg.SeTADOQueryDatabase(QueryUj1);
   nyelvek := Tstringlist.Create;
   Query_Run(QueryUj1,  'SELECT SZ_ALKOD FROM SZOTAR WHERE SZ_FOKOD = ''430'' ', FALSE);
   if QueryUj1.RecordCount > 0 then begin
       while not QueryUj1.Eof do begin
           nyelvek.Add( QueryUj1.FieldByName('SZ_ALKOD').AsString);
           QueryUj1.Next;
       end;
   end else begin
       nyelvek.Add( 'HUN');
       nyelvek.Add( 'ENG');
       nyelvek.Add( 'GER');
   end;
   for i := 0 to nyelvek.Count-1 do begin
       Query_Run(QueryUj1,  'SELECT SZ_ALKOD FROM SZOTAR WHERE SZ_FOKOD = ''210'' AND SZ_ALKOD = '''+nyelvek[i]+vkod+''' ', FALSE);
       if QueryUj1.RecordCount = 0 then begin
           str := alap;
           if nyelvek[i] <> 'HUN' then begin
               str := '.';
           end;
           // L�trehozzuk az �j sz�t�r elemet
           Query_Run(QueryUj1,  'INSERT INTO SZOTAR ( SZ_FOKOD, SZ_ALKOD, SZ_MENEV, SZ_LEIRO, SZ_ERVNY ) VALUES ( ''210'', '''+nyelvek[i]+vkod+''', '''+str+''', '''+alap+' -> '+nyelvek[i]+''',  1 ) ', FALSE);
       end;
   end;
   nyelvek.Free;
   Result  := EgyebDlg.Read_SZGrid( '210',nyelv+vkod);
end;

// Az SQL �ssze�ll�t�sakor egy sz�vegkonstansban cser�l�nk
function	AposztrofCsere(szoveg : string) : string;
begin
    Result :=  StringReplace(szoveg, '''', '''''', [rfReplaceAll]);
end;

// Az UPDATE, INSERT SQL �ssze�ll�t�sakor a sz�vegkonstans BELSEJ�BEN cser�l�nk
function	AposztrofCsere_kozepen(szoveg : string) : string;
var
   Elso, Utolso, Kozepe: string;
begin
   Elso := copy(szoveg, 1, 1);
   Utolso := copy(szoveg, length(szoveg), 1);
   if (Elso='''') and (Utolso='''') then begin
     Kozepe := copy(szoveg, 2, length(szoveg)-2);
     Kozepe := StringReplace(Kozepe, '''', '''''', [rfReplaceAll]);
     Result :=  Elso + Kozepe + Utolso;
     end
   else begin
     Result :=  szoveg;           // ha nem sz�vegkonstans, hanem pl. sz�m
     end;
end;



procedure SzabadMemoriaNaploz(Hol: string);
var
  S: string;
begin
  S:=Hol+'['+formatFloat('0.000', SzabadMemoria() / (1024*1024))+']';
  // HibaKiiro(S);
  Query_Log_Str(S, 0, true);
end;

function SzabadMemoria():longint;
var
  MemoryStatus: TMemoryStatus;
begin
  MemoryStatus.dwLength := SizeOf(MemoryStatus) ;
  GlobalMemoryStatus(MemoryStatus) ;
  Result:= MemoryStatus.dwAvailPhys + MemoryStatus.dwAvailVirtual;
end;

function EgyediElemek(BE: TStringList): TStringList;
var
  KI: TStringList;
  i: integer;
begin
  KI:=TStringList.Create;
  KI.Sorted := True;
  KI.Duplicates := dupIgnore;
  for i:=0 to BE.Count - 1 do begin
    KI.Add(BE[i]);
    end;  // for
  Result:=KI;
end;

function TitleCase(const sText: String): String;
const
   cDelimiters = [#9, #10, #13, ' ', ',', '.', ':', ';', '"',
                  '\', '/', '(', ')', '[', ']', '{', '}'];

   function MyUcase(const C: char):Char;
     var S, S2: string;
     begin
       SetLength(S,1);
       S[1]:=C;
       S2:=AnsiUpperCase(S);
       Result:=S2[1];
     end;
var
  iLoop: Integer;
begin
  Result := sText;
  if (Result <> '') then begin
    Result := AnsiLowerCase(Result);
    Result[1] := MyUcase(Result[1]);
    for iLoop := 2 to Length(Result) do
      if (Result[iLoop - 1] in cDelimiters) then
        Result[iLoop] := MyUcase(Result[iLoop]);
    end; // if
end;

function GetNewIDLoop(Mode: TGetNewIDMode; TablaNev, MezoNev, Prefix : string; tipus : integer = 0; hossz : integer = 0; maxhossz: boolean = False) : integer;
const
  MaxProbalkozas = 3;
var
  Probalkozas: integer;
  siker: boolean;
begin
  Probalkozas:= 1;
  siker:= False;
  while (Probalkozas <= MaxProbalkozas) and not siker do begin
    Result:= GetNewID(Mode, TablaNev, MezoNev, Prefix, tipus, hossz, maxhossz);
    if Result>=1 then siker:= True
    else inc(Probalkozas);
    end;  // loop
  // ha harmadszorra sincs j� k�d, akkor visszat�r a 0-val
end;


// A GetNextCode, GetNextStrCode kiv�lt�s�ra - a "Mode" param�ter d�nti el, hogy melyiket h�vjuk az els� �j azonos�t�n�l
// p�rhuzamosan is haszn�lhat�k, de egy t�bl�ra csak egyf�le (GetNextCode vagy GetNewID)
// Az �t�ll�shoz nem kell semmi, az els� GetNewID megh�vja a megfelel� GetNextCode-ot, bejegyzi az �j k�dot az UTOLSOKOD-ba, a k�s�bbiekben onnan veszi
// az �j azonos�t�t.
function GetNewID(Mode: TGetNewIDMode; TablaNev, MezoNev, Prefix : string; tipus : integer = 0; hossz : integer = 0; maxhossz: boolean = False) : integer;
// a t�bla lock-ol�s �s a tranzakci� biztos�tja, hogy nem osztjuk ki k�tszer ugyanazt a k�dot.
const
    PrefixSep = '|';  // a sz�mlasz�mn�l haszn�ljuk: k�l�n t�rolnunk kell �vente az utols� k�dot
var
    S, TeljesMezonev: string;
   	Query: TADOQuery;
    UjKod: integer;
    rendben: boolean;
begin
	Query:= TADOQuery.Create(nil);
	Query.Tag:= QUERY_AUTONOM_TAG;
	EgyebDlg.SetADOQueryDatabase(Query);
  if Prefix = '' then
     TeljesMezonev:= MezoNev
  else TeljesMezonev:= MezoNev+PrefixSep+Prefix;
  Query_Run(Query, 'BEGIN TRANSACTION');
  try
    if not Query_Run(Query, 'select UK_KOD from UTOLSOKOD WITH (TABLOCK, HOLDLOCK) where UK_TABLA='''+TablaNev+''' and UK_MEZO='''+TeljesMezonev+'''') then // z�roljuk a t�bl�t a tranzakci� v�g�ig
       Raise Exception.Create('Hiba');
    if Query.Eof then begin  // nincs m�g adatsor az UTOLSOKOD-ban ehhez a t�bl�hoz
        if Mode = modeGetNextCode then
            UjKod:= GetNextCode_Core(TablaNev, MezoNev, tipus, hossz)   // a select(max)+1 adja a k�vetkez� szabad k�dot
        else UjKod:= GetNextStrCode_Core(TablaNev, MezoNev, tipus, hossz, maxhossz, Prefix);
        if not Query_Run(Query, 'insert into UTOLSOKOD (UK_TABLA, UK_MEZO, UK_KOD) values ('''+TablaNev+''','''+TeljesMezonev+''','+IntToStr(UjKod)+')') then
           Raise Exception.Create('Hiba');
        end
    else begin
      UjKod:=Query.Fields[0].AsInteger + 1;
      // --- biztos ami biztos, 2016-03-20 ----
      // a r�gi Fuvarosban lehetett az UTOLSOKOD kiker�l�s�vel �j adatsort felvinni
      rendben := false;
      repeat
         S:= 'select '+MezoNev+' from '+TablaNev+' where '+MezoNev+' = '+IntToStr(UjKod);
         Query_Run(Query, S);
         if Query.Eof then  // nincs m�g ilyen k�d
            rendben := true
         else UjKod := UjKod + 1;
         until rendben;  // addig n�velj�k m�g �res k�dot nem tal�lunk
      // --------------------------
      if not Query_Run(Query, 'update UTOLSOKOD set UK_KOD='+IntToStr(UjKod)+' where UK_TABLA='''+TablaNev+''' and UK_MEZO='''+TeljesMezonev+'''') then
         Raise Exception.Create('Hiba');
      end; // else
    // Sleep(5000);  // a p�rhuzamoss�g tesztel�s�hez
    Query_Run(Query, 'COMMIT TRANSACTION');
    if Query<>nil then Query.Free;
    Result:=Ujkod;
  except
      Query_Run(Query, 'ROLLBACK TRANSACTION');
      if Query<>nil then Query.Free;
      Result:=0;  // �rv�nytelen azonos�t�, a GetNextCode is ezt adja vissza hiba eset�n
      end;  // try-except
end;


procedure NemHasznaltSzamlaKod(szakod: string);
const
    PrefixSep = '|';  // a sz�mlasz�mn�l haszn�ljuk: k�l�n t�rolnunk kell �vente az utols� k�dot
var
  Query: TADOQuery;
  S, ErrMsg, prefix, TeljesMezonev: string;
  csakszam, elozoszamlaszam: integer;
begin
 	Query:= TADOQuery.Create(nil);
  try
  	Query.Tag:= QUERY_ADAT_TAG;
  	EgyebDlg.SetADOQueryDatabase(Query);
    S:= 'select SA_KOD from SZFEJ where SA_KOD='''+szakod+'''';
    Query_Run(Query, S);
    if Query.Eof then begin   // nincs ilyen sz�mlasz�m
      prefix:= copy(szakod,1,3);
      TeljesMezonev:= 'SA_KOD'+PrefixSep+Prefix;
      S:= 'select UK_KOD from UTOLSOKOD where UK_TABLA=''SZFEJ'' and UK_MEZO='''+TeljesMezonev+''' and UK_KOD='''+szakod+'''';
      Query_Run(Query, S);
      if not Query.Eof then begin  // ez a bejegyzett urols� sz�mlasz�m
        csakszam:= StrToInt( copy(szakod,4,99));
        elozoszamlaszam:= StrToInt(prefix)*Floor(IntPower(10, 8-length(prefix))) + csakszam -1;
        S:='update UTOLSOKOD set UK_KOD='+IntToStr(elozoszamlaszam)+' where UK_TABLA=''SZFEJ'' and UK_MEZO='''+TeljesMezonev+'''';
        if not Query_Run(Query, S) then begin
         ErrMsg:= 'Az utols� sz�mlasz�m vissza�ll�t�sa nem siker�lt! (UTOLSOKOD, '+TeljesMezonev+')';
         HibaKiiro(ErrMsg);
         Raise Exception.Create(ErrMsg);
         end; // if
        end;  // if not eof
      end;  // if eof
  finally
    if Query<>nil then Query.Free;
    end; // try-finally
end;


function FindURLQuery(url: string): string;
// const
//   CacheIdoszak = 120;  // h�ny napig haszn�ljuk fel az eredm�nyt
var
  Query: TADOQuery;
  Mikortol, S: string;
  CacheIdoszak: integer;  // h�ny napig haszn�ljuk fel az eredm�nyt
begin
  CacheIdoszak:= StrToIntDef(EgyebDlg.Read_SZGrid('990', '002_URLPuf'),120);
  Result:='';
 	Query:= TADOQuery.Create(nil);
  try
  	Query.Tag:= QUERY_ADAT_TAG;
  	EgyebDlg.SetADOQueryDatabase(Query);
    Mikortol:= DatumHozNap(EgyebDlg.MaiDatum, -1*CacheIdoszak, True);
    S:= 'select UR_VALASZ from URLPUFFER where UR_KERES='''+AposztrofCsere(url)+''' and UR_DATUM>='''+Mikortol+'''';
    // Query_Run(Query, S);
    // ne napl�zzuk, mert rettenetesen nagy a v�lasz...
    with Query do begin
      Close; SQL.Clear;
 	    Sql.Add(S);
      Open;
      Result:= Fields[0].AsString;
      end;  // with
  finally
    if Query<>nil then Query.Free;
    end; // try-finally
end;

function StoreURLQuery(url, json: string; pointcount: integer): string;
const
  MaxJSONSize = 7990;  // 8000 - ez-az
  MaxURLSize = 1990;  // 2000 - ez-az
var
  Query: TADOQuery;
  S, REGI_KOD: string;
  UR_KOD, ismetles: integer;
begin
  Result:='';
  url:=StringReplace(url, '''', '''''', [rfReplaceAll]);
  json:=StringReplace(json, '''', '''''', [rfReplaceAll]);
  json:=DelDoubleSpaces(json);  // t�bbsz�r�s sz�k�zt egy sz�k�zre cser�l - sokat nyer�nk m�retben!
  if Length(url)>=MaxURLSize then begin
     Result:='URL>2000';
     Exit;
     end;
  if Length(json)>=MaxJSONSize then begin
     Result:='VALASZ>8000';
     Exit;
     end;
 	Query:= TADOQuery.Create(nil);
  try
  	Query.Tag:= QUERY_ADAT_TAG;
  	EgyebDlg.SetADOQueryDatabase(Query);
    // lehet hogy van m�r egy r�gi adatsor, ami elavult - azt ki kell t�r�lni el�tte
    REGI_KOD:= Query_Select('URLPUFFER', 'UR_KERES', url, 'UR_KOD');
    if (REGI_KOD<> '') then begin
      SQLFuttat('URLPUFFER', 'delete from URLPUFFER where UR_KOD='+REGI_KOD);
      end;
    UR_KOD:= GetNewIDLoop(modeGetNextCode, 'URLPUFFER', 'UR_KOD', '');
    if (UR_KOD<=0) then begin
      if Query<>nil then Query.Free;
       S:= 'Sikertelen k�d gener�l�s, k�rem pr�b�lja �jra! (StoreURLQuery)';
       HibaKiiro(S);
       NoticeKi(S);
       Exit;
       end;
    S:='insert into URLPUFFER (UR_KOD, UR_DATUM, UR_KERES, UR_VALASZ, UR_TAG) ';
    S:=S+ 'values ('+IntToStr(UR_KOD)+', '''+EgyebDlg.MaiDatum+''','''+url+''','''+json+''','+IntToStr(pointcount)+')';
    // Query_Run(Query, S);
    // ne napl�zzuk, mert rettenetesen nagy lehet a v�lasz...
    with Query do begin
      Close; SQL.Clear;
 	    Sql.Add(S);
      ExecSQL;
      end;  // with
  finally
    if Query<>nil then Query.Free;
    end; // try-finally
end;

function DelDoubleSpaces(OldText:String):string;
var
  i: integer;
  s: string;
begin
  if length(OldText)>0 then
    s:=OldText[1]
  else
    s:='';
  for i:=1 to length(OldText) do begin
    if OldText[i]=' ' then begin
      if not (OldText[i-1]=' ') then
        s:=s+' ';
      end
    else begin
      s:=s+OldText[i];
    end;
  end;
  DelDoubleSpaces:=s;
end;

function GetPartnerKontakt(vkod: string) : TPartnerKontakt;
var
	QueryDat1	: TADOQuery;
begin
	QueryDat1:= TADOQuery.Create(nil);
	QueryDat1.Tag := QUERY_ADAT_TAG;
	EgyebDlg.SeTADOQueryDatabase(QueryDat1);
 	if Query_Run (QueryDat1, 'select V_TEL, V_UGYINT, VE_EMAIL from vevo where V_KOD='''+vkod+''' ',true) and not QueryDat1.Eof then begin
      Result.telefon:= QueryDat1.FieldByName('V_TEL').AsString;
      Result.email:= QueryDat1.FieldByName('VE_EMAIL').AsString;
      Result.ugyintezo:= QueryDat1.FieldByName('V_UGYINT').AsString;
      end
   else begin
      Result.telefon:= '';
      Result.email:= '';
      Result.ugyintezo:= '';
      end;  // else
  QueryDat1.Close;
  QueryDat1.Free;
end;

function LehetEgyediDij(partnerkod: string; var ManCode: string): boolean;
var
	Query1	: TADOQuery;
  S: string;
begin
  Query1	:= TADOQuery.Create(nil);
	Query1.Tag := 1;
	EgyebDlg.SeTADOQueryDatabase(Query1);
  S:= 'select SZ_MENEV from szotar where SZ_FOKOD=''377'' and SZ_ERVNY=1 and SZ_ALKOD='+partnerkod;
	if Query_Run(Query1, S) then begin
    if Query1.Eof then begin  // nincs a list�ban lehet hozz� egyedi d�j
      ManCode:= '';
      Result:= true;
      end
    else begin
      ManCode:= Query1.Fields[0].AsString;
      if ManCode <> '' then   // van hozz� "TEMAN" k�d
          Result:= True
      else Result:= False;   // TYCO-s c�g, de nincs hozz� �rv�nyes "TEMAN" k�d - nem sz�ml�zhatunk nekik �gy
      end;  // else
		Query1.Close;
	end;
	Query1.Free;
end;


procedure MegbizasPaletta(mskod: string);
var
	Q1	    : TADOQuery;
	Q2	    : TADOQuery;
   ujkod   : integer;
   menny   : integer;
begin
   // A megseg�d rekord alapj�n legener�ljuk a PALVALT paletta v�ltoz�s rekordot
   Q1	:= TADOQuery.Create(nil);
	Q1.Tag := 1;
	EgyebDlg.SeTADOQueryDatabase(Q1);
   Q2	:= TADOQuery.Create(nil);
	Q2.Tag := 1;
	EgyebDlg.SeTADOQueryDatabase(Q2);
   Query_Run(Q1, 'SELECT * FROM MEGSEGED WHERE MS_MSKOD = '+mskod+' AND MS_POTSZ <> '''' AND MS_POTSZ IN (SELECT GK_RESZ FROM GEPKOCSI)');
	if Q1.RecordCount > 0 then begin
       if  ( ( ( StrToIntdef( Q1.FieldByName('MS_FPAFEL').AsString , 0 ) > 0 ) or ( StrToIntdef( Q1.FieldByName('MS_FPALER').AsString , 0 ) > 0 ) ) and (Q1.FieldByName('MS_TIPUS').AsString = '0') and
           ( ( Trim(Q1.FieldByName('MS_FETDAT').AsString) <> '' ) or ( Trim(Q1.FieldByName('MS_FPAFEL').AsString) <> '' ) or ( Trim(Q1.FieldByName('MS_FPALER').AsString) <> '' ) ) ) then begin
           // A felrak�shoz kell paletta rekord
           Query_Run(Q2, 'SELECT PV_MSKOD FROM PALVALT WHERE PV_MSKOD = '+mskod+' AND PV_MSTIP = 0 ');
           menny   := StrToIntDef(Q1.FieldByName('MS_FPAFEL').AsString, 0) - StrToIntDef(Q1.FieldByName('MS_FPALER').AsString, 0);
           if Q2.RecordCount = 0 then begin
               // L�tre kell hozni az �j elemet
               ujkod   := GetNextCode('PALVALT', 'PV_PVKOD', 1);
               Query_Insert(Q2, 'PALVALT',
                  [
                  'PV_PVKOD', IntToStr(ujkod),
                  'PV_DATUM', ''''+Q1.FieldByName('MS_FETDAT').AsString+'''',
                  'PV_IDOPT', ''''+Q1.FieldByName('MS_FETIDO').AsString+'''',
                  'PV_TIPUS', '2',        // Megb�z�sr�l van sz�
                  'PV_HETI1', '1',        // Rendsz�mr�l van sz�
                  'PV_HEKO1', ''''+Q1.FieldByName('MS_POTSZ').AsString+'''',  // A rendsz�m
                  'PV_MENNY', IntToStr(menny),
                  'PV_CSERE', '0',
                  'PV_MSKOD', mskod,
                  'PV_MSTIP', '0',        // Felrak�s
                  'PV_MSFEL', IntToStr(StrToIntDef(Q1.FieldByName('MS_FPAFEL').AsString, 0)),
                  'PV_MSLER', IntToStr(StrToIntDef(Q1.FieldByName('MS_FPALER').AsString, 0)),
                  'PV_MEGJE', ''''+Q1.FieldByName('MS_FELNEV').AsString+''''  // A rendsz�m
                  ]);
           end else begin
              Query_Update(Q2, 'PALVALT',
                  [
                  'PV_DATUM', ''''+Q1.FieldByName('MS_FETDAT').AsString+'''',
                  'PV_IDOPT', ''''+Q1.FieldByName('MS_FETIDO').AsString+'''',
                  'PV_TIPUS', '2',        // Megb�z�sr�l van sz�
                  'PV_HETI1', '1',        // Rendsz�mr�l van sz�
                  'PV_HEKO1', ''''+Q1.FieldByName('MS_POTSZ').AsString+'''',  // A rendsz�m
                  'PV_MENNY', IntToStr(menny),
                  'PV_CSERE', '0',
                  'PV_MSKOD', mskod,
                  'PV_MSTIP', '0',        // Felrak�s
                  'PV_MSFEL', IntToStr(StrToIntDef(Q1.FieldByName('MS_FPAFEL').AsString, 0)),
                  'PV_MSLER', IntToStr(StrToIntDef(Q1.FieldByName('MS_FPALER').AsString, 0)),
                  'PV_MEGJE', ''''+Q1.FieldByName('MS_FELNEV').AsString+''''  // A rendsz�m
                  ], ' WHERE PV_PVKOD = '+IntToStr(ujkod) );
           end;
       end;
       if ( ( ( StrToIntdef( Q1.FieldByName('MS_LPAFEL').AsString , 0 ) > 0 ) or ( StrToIntdef( Q1.FieldByName('MS_LPALER').AsString , 0 ) > 0 ) ) and (Q1.FieldByName('MS_TIPUS').AsString = '1')
           and ( ( Trim(Q1.FieldByName('MS_LETDAT').AsString) <> '' ) or ( Trim(Q1.FieldByName('MS_LPAFEL').AsString) <> '' ) or ( Trim(Q1.FieldByName('MS_LPALER').AsString) <> '' ) ) ) then begin
           // A lerak�shoz kell paletta rekord
           Query_Run(Q2, 'SELECT PV_MSKOD FROM PALVALT WHERE PV_MSKOD = '+mskod+' AND PV_MSTIP = 1 ');
           menny   := StrToIntDef(Q1.FieldByName('MS_LPAFEL').AsString, 0) - StrToIntDef(Q1.FieldByName('MS_LPALER').AsString, 0);
           if Q2.RecordCount = 0 then begin
               // L�tre kell hozni az �j elemet
               ujkod   := GetNextCode('PALVALT', 'PV_PVKOD', 1);
               Query_Insert(Q2, 'PALVALT',
                  [
                  'PV_PVKOD', IntToStr(ujkod),
                  'PV_DATUM', ''''+Q1.FieldByName('MS_LETDAT').AsString+'''',
                  'PV_IDOPT', ''''+Q1.FieldByName('MS_LETIDO').AsString+'''',
                  'PV_TIPUS', '2',        // Megb�z�sr�l van sz�
                  'PV_HETI1', '1',        // Rendsz�mr�l van sz�
                  'PV_HEKO1', ''''+Q1.FieldByName('MS_POTSZ').AsString+'''',  // A rendsz�m
                  'PV_MENNY', IntToStr(menny),
                  'PV_CSERE', '0',
                  'PV_MSKOD', mskod,
                  'PV_MSTIP', '1',        // Lerak�s
                  'PV_MSFEL', IntToStr(StrToIntDef(Q1.FieldByName('MS_LPAFEL').AsString, 0)),
                  'PV_MSLER', IntToStr(StrToIntDef(Q1.FieldByName('MS_LPALER').AsString, 0)),
                  'PV_MEGJE', ''''+Q1.FieldByName('MS_LERNEV').AsString+''''  // A rendsz�m
                  ]);
           end else begin
              Query_Update(Q2, 'PALVALT',
                  [
                  'PV_DATUM', ''''+Q1.FieldByName('MS_LETDAT').AsString+'''',
                  'PV_IDOPT', ''''+Q1.FieldByName('MS_LETIDO').AsString+'''',
                  'PV_TIPUS', '2',        // Megb�z�sr�l van sz�
                  'PV_HETI1', '1',        // Rendsz�mr�l van sz�
                  'PV_HEKO1', ''''+Q1.FieldByName('MS_POTSZ').AsString+'''',  // A rendsz�m
                  'PV_MENNY', IntToStr(menny),
                  'PV_CSERE', '0',
                  'PV_MSKOD', mskod,
                  'PV_MSTIP', '1',        // Lerak�s
                  'PV_MSFEL', IntToStr(StrToIntDef(Q1.FieldByName('MS_LPAFEL').AsString, 0)),
                  'PV_MSLER', IntToStr(StrToIntDef(Q1.FieldByName('MS_LPALER').AsString, 0)),
                  'PV_MEGJE', ''''+Q1.FieldByName('MS_LERNEV').AsString+''''  // A rendsz�m
                  ], ' WHERE PV_PVKOD = '+IntToStr(ujkod) );
           end;
       end;
	end;
	Q1.Close;
	Q1.Free;
	Q2.Close;
	Q2.Free;
end;

function GetPaletta(helykod, datum, idopt : string) : integer;
var
   Q1	    : TADOQuery;
   db      : integer;
   menny   : integer;
begin
   // A palettasz�m visszaad�sa
   Result  := 0;
   if helykod = '' then begin
      Exit;
   end;
   Q1	    := TADOQuery.Create(nil);
	Q1.Tag  := 1;
	EgyebDlg.SeTADOQueryDatabase(Q1);
   Query_Run (Q1, 'SELECT  ''1'' ELSO, PV_HETI1 AS HETIP, PV_HEKO1 AS HEKOD, PV_HETI2 AS HETI2, PV_HEKO2 AS HEKO2, '+
       ' PV_CSERE, PV_DATUM, PV_IDOPT, PV_MEGJE, PV_MENNY, PV_MSFEL, PV_MSKOD, '+
       ' PV_MSLER, PV_MSTIP, PV_TIPUS  FROM PALVALT  WHERE PV_HEKO1 = '''+copy(helykod, 2, 999)+''' AND PV_DATUM+PV_IDOPT <= '''+datum+idopt+''''+
       ' UNION SELECT  ''2'' ELSO, PV_HETI2 AS HETIP, PV_HEKO2 AS HEKOD, PV_HETI1 AS HETI2, PV_HEKO1 AS HEKO2, PV_CSERE, PV_DATUM, '+
       ' PV_IDOPT, PV_MEGJE, PV_MENNY, PV_MSFEL, PV_MSKOD, PV_MSLER, PV_MSTIP, PV_TIPUS '+
       ' FROM PALVALT  WHERE PV_HEKO2 = '''+copy(helykod, 2, 999)+''' AND PV_DATUM+PV_IDOPT <= '''+datum+idopt+''''+
       ' ORDER BY 2, 3, PV_DATUM, PV_IDOPT');
   db  := 0;
   while not Q1.Eof do begin
       menny		:= StrToIntDef(Q1.FieldByName('PV_MENNY').AsString, 0);
       if StrToIntDef(Q1.FieldByName('PV_TIPUS').AsString, -1) = 0 then begin
          if StrToIntDef(Q1.FieldByName('ELSO').AsString, 0) < 2 then begin
              menny  := -1 * menny;
          end;
       end;
       case StrToIntDef(Q1.FieldByName('PV_TIPUS').AsString, 0) of
          0 :     // v�ltoz�s
          begin
              // Csak akkor v�ltozik a dolog, ha nem cser�lt�nk
              if StrToIntDef(Q1.FieldByName('PV_CSERE').AsString, 0) = 0 then begin
                  db := db + menny;
              end;
          end;
          1 :     // lelt�r
          begin
              db  := menny;
          end;
          2 :     // megb�z�s
          begin
              db  := db + menny;
          end;
      end;
       Q1.Next;
   end;
   Result  := db;
	Q1.Close;
	Q1.Free;
end;

function GetHelynev(helykod : string) : string;
var
   kod     : integer;
   kodstr  : string;
   helystr : string;
begin
   // A paletta hely meghat�roz�sa a k�d alapj�n
   Result  := '';
   kod     := StrToIntDef(copy(helykod, 1, 1), 0);
   case kod of
       0 :     // B�zissz�t�r 140 -es f�csoportban l�v� elemek megnevez�se
       begin
           kodstr  := copy(helykod, 2, 999);
           Result  := EgyebDlg.Read_SZGrid('140',kodstr);
       end;
       1 :     // Ez egy rendsz�m a 2. karaktert�l
       begin
           Result  := copy(helykod, 2, 999);
       end;
   end;

end;

function RecordsetToJSON(SQL: string): string;
var
  JSONResordSet: TJSONArray;
  JSONRecord: TJSONObject;
	QueryDat1: TADOQuery;
  Res: string;
  i: integer;
begin
	QueryDat1:= TADOQuery.Create(nil);
	QueryDat1.Tag := QUERY_ADAT_TAG;
	EgyebDlg.SeTADOQueryDatabase(QueryDat1);
  Res:='';
  if Query_Run (QueryDat1, SQL, true) then with QueryDat1 do begin
    JSONResordSet:= TJSONArray.Create;
    while not QueryDat1.eof do begin
      JSONRecord:=TJSONObject.Create;
      for i:=0 to FieldCount-1 do begin
         JSONRecord.AddPair(TJSONPair.Create(TJSONString.Create(Fields[i].FieldName),TJSONString.Create(Fields[i].AsString)));
         end;  // for
      JSONResordSet.Add(JSONRecord);
      QueryDat1.Next;
      end;  // while
    Res:= JSONResordSet.ToString;
    end;  // if + with
  QueryDat1.Close;
  QueryDat1.Free;
  Result:= Res;
end;

function SetTelefonDolgozo(TKID, DOKOD: string): string;  // kapcsolt el�fizet�s napl�z�s�st is kezel
var
   S, RegiTelDOKOD, RegiElofizetesDOKOD, RegiElofizetesTelkod, TEID, Res: string;
   SQLArray: TStringList;
   Query1	: TADOQuery;
   Siker1: boolean;
begin
   Result:= '';
   if (TKID = '') then begin
      Result:= 'Nincs telefonk�sz�l�k kiv�lasztva!';
      Exit;  // sub
      end;
   SQLArray:= TStringList.Create;
   Query1		:= TADOQuery.Create(nil);
   Query1.Tag  := 1;
	 EgyebDlg.SetAdoQueryDatabase(Query1);
   try  // finally
      S:='update TELKESZULEK set TK_DOKOD='''+DOKOD+''' where TK_ID= '+TKID;
      SQLArray.Add(S);
      Siker1:= True;
      RegiTelDOKOD:= Query_Select('TELKESZULEK', 'TK_ID', TKID, 'TK_DOKOD');
      // if RegiTelDOKOD <> '' then begin
        S:= 'select count(*) from TELHIST where TH_KATEG=''TELEFON'' and TH_DOKOD='+RegiTelDOKOD +
          ' and TH_MEDDIG='''+EgyebDlg.MaiDatum+''' and TH_KOD= '+TKID;
        if Query_SelectString('TELHIST', S) = '0' then begin  // csak akkor, ha nem l�tezett m�g
          S:='insert into telhist (TH_MEDDIG, TH_DOKOD, TH_KATEG, TH_KOD )';
          S:= S+ ' values ('''+EgyebDlg.MaiDatum+''', '+RegiTelDOKOD+', ''TELEFON'', '+TKID+')';
          SQLArray.Add(S);
          end;
        if Query_Run(Query1, 'SELECT TE_ID FROM TELELOFIZETES WHERE TE_TKID = '+TKID, true) then begin // a kapcsolt el�fizet�sek napl�z�sa
          while not Query1.Eof do begin
             TEID:= Query1.FieldByName('TE_ID').AsString;
             S:= 'select count(*) from TELHIST where TH_KATEG=''SIM'' and TH_DOKOD='+RegiTelDOKOD +
                ' and TH_MEDDIG='''+EgyebDlg.MaiDatum+''' and TH_KOD= '+TEID;
            if Query_SelectString('TELHIST', S) = '0' then begin  // csak akkor, ha nem l�tezett m�g
               S:='insert into telhist (TH_MEDDIG, TH_DOKOD, TH_KATEG, TH_KOD )';
               S:= S+ ' values ('''+EgyebDlg.MaiDatum+''', '+RegiTelDOKOD+', ''SIM'', '+TEID+')';
               SQLArray.Add(S);
               end; // if
             Query1.Next;
             end;  // while
           end  // if
        else begin  // Query_Run nem siker�lt
          Siker1:= False;
          end; //else
       // end;
      if Siker1 then
         Res:= ExecuteSQLTransaction(SQLArray)
      else Res:= 'SetTelefonDolgozo: kapcsolt el�fizet�s lek�rdez�s nem siker�lt!'
   finally
      if SQLArray<>nil then SQLArray.Free;
      if Query1<>nil then Query1.Free;
      end;  // try-finally
end;

{function SetTelefonDolgozo(TKID, DOKOD: string): string;  // kapcsolt el�fizet�s napl�z�s�st is kezel
var
   S, RegiTelDOKOD, RegiElofizetesDOKOD, RegiElofizetesTelkod, TEID, Res: string;
   SQLArray: TStringList;
   Query1	: TADOQuery;
   Siker1: boolean;
begin
   Result:= '';
   if (TKID = '') then begin
      Result:= 'Nincs telefonk�sz�l�k kiv�lasztva!';
      Exit;  // sub
      end;
   SQLArray:= TStringList.Create;
   Query1		:= TADOQuery.Create(nil);
   Query1.Tag  := 1;
	 EgyebDlg.SetAdoQueryDatabase(Query1);
   try  // finally
      S:='update TELKESZULEK set TK_DOKOD='''+DOKOD+''' where TK_ID= '+TKID;
      SQLArray.Add(S);
      Siker1:= True;
      RegiTelDOKOD:= Query_Select('TELKESZULEK', 'TK_ID', TKID, 'TK_DOKOD');
      if RegiTelDOKOD <> '' then begin
        S:= 'select count(*) from TELHIST where TH_KATEG=''TELEFON'' and TH_DOKOD='+RegiTelDOKOD +
          ' and TH_MEDDIG='''+EgyebDlg.MaiDatum+''' and TH_KOD= '+TKID;
        if Query_SelectString('TELHIST', S) = '0' then begin  // csak akkor, ha nem l�tezett m�g
          S:='insert into telhist (TH_MEDDIG, TH_DOKOD, TH_KATEG, TH_KOD )';
          S:= S+ ' values ('''+EgyebDlg.MaiDatum+''', '+RegiTelDOKOD+', ''TELEFON'', '+TKID+')';
          SQLArray.Add(S);
          end;
        if Query_Run(Query1, 'SELECT TE_ID FROM TELELOFIZETES WHERE TE_TKID = '+TKID, true) then begin // a kapcsolt el�fizet�sek napl�z�sa
          while not Query1.Eof do begin
             TEID:= Query1.FieldByName('TE_ID').AsString;
             S:= 'select count(*) from TELHIST where TH_KATEG=''SIM'' and TH_DOKOD='+RegiTelDOKOD +
                ' and TH_MEDDIG='''+EgyebDlg.MaiDatum+''' and TH_KOD= '+TEID;
            if Query_SelectString('TELHIST', S) = '0' then begin  // csak akkor, ha nem l�tezett m�g
               S:='insert into telhist (TH_MEDDIG, TH_DOKOD, TH_KATEG, TH_KOD )';
               S:= S+ ' values ('''+EgyebDlg.MaiDatum+''', '+RegiTelDOKOD+', ''SIM'', '+TEID+')';
               SQLArray.Add(S);
               end;
             Query1.Next;
             end;  // while
           end
        else begin  // Query_Run nem siker�lt
          Siker1:= False;
          end;
        end;
      if Siker1 then
         Res:= ExecuteSQLTransaction(SQLArray)
      else Res:= 'SetTelefonDolgozo: kapcsolt el�fizet�s lek�rdez�s nem siker�lt!'
   finally
      if SQLArray<>nil then SQLArray.Free;
      if Query1<>nil then Query1.Free;
      end;  // try-finally
end;
}

function SetElofizetesDolgozo(TEID, DOKOD: string): string;
var
   S, RegiTelDOKOD, RegiElofizetesDOKOD, RegiElofizetesTelkod, Res: string;
   SQLArray: TStringList;
begin
   Result:= '';
   if (TEID = '') then begin
      Result:= 'Nincs el�fizet�s kiv�lasztva!';
      Exit;  // sub
      end;
   SQLArray:= TStringList.Create;
   try  // finally
      RegiElofizetesDOKOD:= Query_Select('TELELOFIZETES', 'TE_ID', TEID, 'TE_DOKOD');
      if RegiElofizetesDOKOD <> '' then begin
        S:= 'select count(*) from TELHIST where TH_KATEG=''SIM'' and TH_DOKOD='+RegiElofizetesDOKOD +
            ' and TH_MEDDIG='''+EgyebDlg.MaiDatum+''' and TH_KOD= '+TEID;
        if Query_SelectString('TELHIST', S) = '0' then begin  // csak akkor, ha nem l�tezett m�g
          S:='insert into telhist (TH_MEDDIG, TH_DOKOD, TH_KATEG, TH_KOD )';
          S:= S+ ' values ('''+EgyebDlg.MaiDatum+''', '+RegiElofizetesDOKOD+', ''SIM'', '+TEID+')';
          SQLArray.Add(S);
          end;  // if
        end;  // if
      S:='update TELELOFIZETES set TE_DOKOD='''+DOKOD+''', TE_TKID='''' where TE_ID= '+TEID;  // ha esetleg telefonban volt, kivessz�k
      SQLArray.Add(S);

      Res:= ExecuteSQLTransaction(SQLArray);

   finally
      if SQLArray<>nil then SQLArray.Free;
      end;  // try-finally
end;


function SetElofizetesTelefon(TEID, TKID: string): string;
var
   S, RegiTelDOKOD, RegiElofizetesDOKOD, RegiElofizetesTKID, Res: string;
   SQLArray: TStringList;
begin
   Result:= '';
   if (TEID = '') then begin
      Result:= 'Nincs el�fizet�s kiv�lasztva!';
      Exit;  // sub
      end;
   SQLArray:= TStringList.Create;
   try  // finally
      RegiElofizetesDOKOD:= Query_Select('TELELOFIZETES', 'TE_ID', TEID, 'TE_DOKOD');
      RegiElofizetesTKID:= Query_Select('TELELOFIZETES', 'TE_ID', TEID, 'TE_TKID');

      if RegiElofizetesDOKOD <> '' then begin  // dolgoz�hoz volt rendelve
        S:= 'select count(*) from TELHIST where TH_KATEG=''SIM'' and TH_DOKOD='+RegiElofizetesDOKOD +
          ' and TH_MEDDIG='''+EgyebDlg.MaiDatum+''' and TH_KOD= '+TEID;
        if Query_SelectString('TELHIST', S) = '0' then begin  // csak akkor, ha nem l�tezett m�g
          S:='insert into telhist (TH_MEDDIG, TH_DOKOD, TH_KATEG, TH_KOD )';
          S:= S+ ' values ('''+EgyebDlg.MaiDatum+''', '+RegiElofizetesDOKOD+', ''SIM'', '+TEID+')';
          SQLArray.Add(S);
          end;
        end;
      if RegiElofizetesTKID <> '' then begin  // telefonhoz volt rendelve
        RegiTelDOKOD:= Query_Select('TELKESZULEK', 'TK_ID', RegiElofizetesTKID, 'TK_DOKOD');
        if RegiTelDOKOD <> '' then begin
         S:= 'select count(*) from TELHIST where TH_KATEG=''SIM'' and TH_DOKOD='+RegiTelDOKOD +
          ' and TH_MEDDIG='''+EgyebDlg.MaiDatum+''' and TH_KOD= '+TEID;
         if Query_SelectString('TELHIST', S) = '0' then begin  // csak akkor, ha nem l�tezett m�g
            S:='insert into telhist (TH_MEDDIG, TH_DOKOD, TH_KATEG, TH_KOD )';
            S:= S+ ' values ('''+EgyebDlg.MaiDatum+''', '+RegiTelDOKOD+', ''SIM'', '+TEID+')';
            SQLArray.Add(S);
            end;
          end;
        end;

      S:='update TELELOFIZETES set TE_TKID='''+TKID+''' where TE_ID= '+TEID;
      SQLArray.Add(S);
      if TKID <> '' then begin
        S:='update TELELOFIZETES set TE_DOKOD='''' where TE_ID= '+TEID;  // ha esetleg dolgoz�hoz volt rendelve, kivessz�k
        SQLArray.Add(S);
        end;

      Res:= ExecuteSQLTransaction(SQLArray);

   finally
      if SQLArray<>nil then SQLArray.Free;
      end;  // try-finally
end;

function StoreOneSMS(Telefon, Szoveg: string): string;
var
   S, Szal, Res: string;
   SUID: integer;
   SQLArray: TStringList;
begin
   Result:= '';
   if (trim(Telefon) = '') then begin
      Result:= 'Nincs telefonsz�m megadva!';
      Exit;  // sub
      end;
   SQLArray:= TStringList.Create;
   try  // finally
      Szal:= Query_Select('SMSSZALAK', 'SZ_TELEF', Telefon, 'SZ_TELEF');
      if Szal = '' then begin  // nem l�tez� sz�l
         S:='insert into SMSSZALAK (SZ_TELEF )';
         S:= S+ ' values ('''+Telefon+''')';
         SQLArray.Add(S);
         end;
      SUID:= GetNewIDLoop(modeGetNextCode, 'SMSUZENETEK', 'SU_ID', ''); //
      if (SUID<=0) then begin
       if SQLArray<>nil then SQLArray.Free;
       S:= 'Sikertelen k�d gener�l�s, k�rem pr�b�lja �jra! (StoreOneSMS)';
       HibaKiiro(S);
       NoticeKi(S);
       Exit;
       end;
      S:='insert into SMSUZENETEK (SU_ID, SU_TELEF, SU_UZENE, SU_BEJOVO, SU_STATUS, SU_KULDI )';
      S:= S+ ' values ('+IntToStr(SUID)+', '''+Telefon+''', '''+AposztrofCsere(Szoveg)+''', 0, 2, ';
      S:= S+ ''''+formatdatetime('yyyy.mm.dd. HH:mm:ss', Now)+''')';
      SQLArray.Add(S);

      Res:= ExecuteSQLTransaction(SQLArray);

   finally
      if SQLArray<>nil then SQLArray.Free;
      end;  // try-finally
end;

function GetTelMegnevezes(TelKod: string): string;
begin
  if TelKod <> '' then
    Result:= Query_SelectString('TELKESZULEK', 'select SZ_MENEV from TELKESZULEK, SZOTAR where SZ_ALKOD = TK_TIPUSKOD and SZ_FOKOD=146 and TK_ID = ' +TelKod)
  else Result:= '';
end;

function GetTelDokod(TelKod: string): string;
begin
  if TelKod <> '' then
     Result:= Query_SelectString('TELKESZULEK', 'select TK_DOKOD from TELKESZULEK where TK_ID = ' +TelKod)
  else Result:= '';
end;

function GetTelDoNev(TelKod: string): string;
begin
  if TelKod <> '' then
     Result:= Query_SelectString('TELKESZULEK', 'select DO_NAME from TELKESZULEK, DOLGOZO where TK_DOKOD = DO_KOD and TK_ID = ' +TelKod)
  else Result:= '';
end;

function GetElsodlegesSzamCount(DOKOD: string): integer;
var
  OsszesTelefon, ElsodlegesTelefon: integer;
  DolgozoSelect: string;
begin
  DolgozoSelect:= '((TE_DOKOD = ''' +DOKOD+''') or (TE_TKID in (select TK_ID from TELKESZULEK where TK_DOKOD = ''' +DOKOD+''')))';
  OsszesTelefon:= StrToInt(Query_SelectString('TELELOFIZETES', 'select count(TE_DOKOD) from TELELOFIZETES where '+DolgozoSelect));
  if OsszesTelefon>0 then begin
    if OsszesTelefon = 1 then begin  // ha pontosan egy telefonja van, nem k�vetelj�k meg hogy be legyen jel�lve els�dlegesnek
       Result:= 1;
       end
    else begin
      ElsodlegesTelefon:= StrToInt(Query_SelectString('TELELOFIZETES', 'select count(TE_DOKOD) from TELELOFIZETES where '+DolgozoSelect+' and TE_DOELS=1'));
      Result:= ElsodlegesTelefon;
      end;
    end
  else Result:= -1;  // nincs telefonja
end;

procedure SetElsodlegesSzam(DOKOD, TEID: string);
var
  DolgozoSelect: string;
begin
   DolgozoSelect:= '((TE_DOKOD = ''' +DOKOD+''') or (TE_TKID in (select TK_ID from TELKESZULEK where TK_DOKOD = ''' +DOKOD+''')))';
   Query_Update (EgyebDlg.QueryAlap, 'TELELOFIZETES',
        [
        'TE_DOELS', 	 '0'
        ], ' WHERE '+DolgozoSelect );  // a dolgoz�n�l minden sort null�zunk...
   Query_Update (EgyebDlg.QueryAlap, 'TELELOFIZETES',
        [
        'TE_DOELS', 	 '1'
        ], ' WHERE TE_ID = '+ TEID  );  // ... ezt az egyet be�ll�tjuk.
end;

function GetElsodlegesSzam(DOKOD: string; LehetTobb: boolean = False): string;
var
  OsszesTelefon, ElsodlegesTelefon: integer;
  DolgozoSelect, ElsodlegesSelect: string;
begin
  DolgozoSelect:= '((TE_DOKOD = ''' +DOKOD+''') or (TE_TKID in (select TK_ID from TELKESZULEK where TK_DOKOD = ''' +DOKOD+''')))';
  OsszesTelefon:= StrToInt(Query_SelectString('TELELOFIZETES', 'select count(TE_DOKOD) from TELELOFIZETES where '+DolgozoSelect));
  if OsszesTelefon > 1 then
     ElsodlegesSelect:=' and TE_DOELS=1'
  else ElsodlegesSelect:= '';  // egy telefon eset�n nem sz�r�nk TE_DOELS-re
  Query_run(EgyebDlg.QueryAlap, 'select TE_ID from TELELOFIZETES where '+DolgozoSelect+' '+ElsodlegesSelect);
  case EgyebDlg.QueryAlap.RecordCount of
    0:  Result:= '';
    1:  Result:= EgyebDlg.QueryAlap.Fields[0].AsString;
    else begin
        if LehetTobb then
           Result:= EgyebDlg.QueryAlap.Fields[0].AsString  // az els� j� lesz
        else Result:= '-1'; // t�l sok van: nincs �rv�nyes
        end; // else
    end;  // case
end;

function GetBarmelyikSzam(DOKOD: string): string;
var
  OsszesTelefon, ElsodlegesTelefon: integer;
  DolgozoSelect: string;
begin
  DolgozoSelect:= '((TE_DOKOD = ''' +DOKOD+''') or (TE_TKID in (select TK_ID from TELKESZULEK where TK_DOKOD = ''' +DOKOD+''')))';
  Query_run(EgyebDlg.QueryAlap, 'select TE_ID from TELELOFIZETES where '+DolgozoSelect);
  if not EgyebDlg.QueryAlap.Eof then
      Result:= EgyebDlg.QueryAlap.Fields[0].AsString
  else Result:= '';
end;

function LegutobbFelvittTelefonTipus: string;
begin
  Result:= Query_SelectString('TELKESZULEK', 'select TK_TIPUSKOD from telkeszulek where TK_ID = (select MAX(TK_ID) from telkeszulek)');
end;

function ExecuteSQLTransaction(SQLArray: TStringList): string;
var
  TransactionQuery: TADOQuery;
  ActSQL: string;
  i: integer;
  Siker: boolean;
begin
  TransactionQuery:= TADOQuery.Create(nil);
	TransactionQuery.Tag:= QUERY_TRANSACTION_TAG;
	EgyebDlg.SetADOQueryDatabase(TransactionQuery);
  try  // finally
  try  // except
    Query_Run(TransactionQuery, 'BEGIN TRANSACTION');
    for i:=0 to SQLArray.Count-1 do begin
      ActSQL:= SQLArray[i];
      Siker:= Query_Run(TransactionQuery, ActSQL, True);
      if not Siker then
          Raise Exception.Create('SQL hiba'); // az Except �gon rollback-el majd
      end;  // for
    Query_Run(TransactionQuery, 'COMMIT TRANSACTION');
  except
    on E: Exception do begin
        Query_Run(TransactionQuery, 'ROLLBACK TRANSACTION');
        Result:= 'Hiba az SQL tranzakci� sor�n. SQL = '+ActSQL;
        end;  // on E
      end;  // try-except
  finally
      if TransactionQuery<>nil then TransactionQuery.Free;
      end;  // try-finally
end;

function FelrakoElment(a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, FelrakoKod: string; Formbol: boolean): integer;  // visszaadja a k�dot
// K�tf�le m�k�d�s: Formbol= True: a form-b�l h�vtuk, engedjen n�v m�dos�t�st �s �j felvitelt is.
//                  Formbol= False: csak adatokkal h�vtuk. Ha l�tezik a n�v, update-elje a c�met. Ha nem l�tezik, vegyen fel �jat.
const
  CRLF = chr(13)+chr(10);
  TAB = chr(9);
  LF = chr(10);
var
	LongS, LatS, JSONSzoveg, letezokod: string;
  GeoCoordinates: TGeoCoordinates;
  GeoCodeInfo: TGeoCodeInfo;
  kod: integer;
begin
  LongS:= 'null';
  LatS:= 'null';

  if EgyebDlg.GPS_KALKULACIOK_HASZNALATA then begin
    GeoCodeInfo:= QueryCoordinates (a2, a3, a4, a5);
    if GeoCodeInfo.Coordinates.Longitude <> -1 then
      // LongS:= FormatFloat('0.00000000000000', GeoCodeInfo.Coordinates.Longitude)
      LongS:= SqlSzamString(GeoCodeInfo.Coordinates.Longitude, 17, 14);
    if GeoCodeInfo.Coordinates.Latitude <> -1 then
      // LatS:= FormatFloat('0.00000000000000', GeoCodeInfo.Coordinates.Latitude)
      LatS:= SqlSzamString(GeoCodeInfo.Coordinates.Latitude, 17, 14);
    end;

  JSONSzoveg:= GeoCodeInfo.FullJSON;
  JSONSzoveg:= StringReplace(JSONSzoveg, CRLF, ' ', [rfReplaceAll]);
  JSONSzoveg:= StringReplace(JSONSzoveg, LF, ' ', [rfReplaceAll]);
  // A TAB-ot sem
  JSONSzoveg:= StringReplace(JSONSzoveg, TAB, ' ', [rfReplaceAll]);
  // A Query_update le fogja cser�lni
  // JSONSzoveg:= StringReplace(JSONSzoveg, '''', '''''', [rfReplaceAll]); // mivel SQL-be ker�l
  JSONSzoveg:= DelDoubleSpaces(JSONSzoveg);  // t�bbsz�r�s sz�k�zt egy sz�k�zre cser�l - sokat nyer�nk m�retben!
  JSONSzoveg:= copy(JSONSzoveg, 1, 4000);  // csak ami belef�r, ezen ne legyen SQL hiba!

  letezokod:= Query_Select('FELRAKO', 'FF_FELNEV', trim(a1), 'FF_FEKOD'); // ha l�tezik m�r ilyen n�ven
  // ha �j felrak� = INSERT
  if (Formbol and (FelrakoKod='')) or (not Formbol and (letezokod='')) then begin
      kod := GetNextCode('FELRAKO', 'FF_FEKOD', 1, 0);
      Query_Insert (EgyebDlg.QueryAlap, 'FELRAKO',
        [
        'FF_FEKOD', 	IntToStr(kod),
        'FF_FELNEV', 	''''+a1+'''',
        'FF_ORSZA',	''''+a2+'''',
        'FF_FELIR', 	''''+a3+'''',
        'FF_FELTEL', 	''''+a4+'''',
        'FF_FELCIM', 	''''+a5+'''',
        'FF_FELSE1', 	''''+a6+'''',
        'FF_FELSE2', 	''''+a7+'''',
        'FF_FELSE3', 	''''+a8+'''',
        'FF_FELSE4', 	''''+a9+'''',
        'FF_FELSE5', 	''''+a10+'''',
        'FF_SZELE', 	LatS,
        'FF_HOSZA', 	LongS,
        'FF_GEOJSON', ''''+JSONSzoveg+''''
        ] );
        Result:= kod;
      end
  else begin
     if Formbol then begin
       Query_Update (EgyebDlg.QueryAlap, 'FELRAKO',
          [
          'FF_FELNEV', 	''''+a1+'''',   // a felrak� nev�t is be�rjuk
          'FF_ORSZA',	''''+a2+'''',
          'FF_FELIR', 	''''+a3+'''',
          'FF_FELTEL', 	''''+a4+'''',
          'FF_FELCIM', 	''''+a5+'''',
          'FF_FELSE1', 	''''+a6+'''',
          'FF_FELSE2', 	''''+a7+'''',
          'FF_FELSE3', 	''''+a8+'''',
          'FF_FELSE4', 	''''+a9+'''',
          'FF_FELSE5', 	''''+a10+'''',
          'FF_SZELE', 	LatS,
          'FF_HOSZA', 	LongS,
          'FF_GEOJSON', ''''+JSONSzoveg+''''
          ], '  WHERE FF_FEKOD = '+ FelrakoKod );
        Result:= StrToInt(FelrakoKod);
        end
     else begin
        Query_Update (EgyebDlg.QueryAlap, 'FELRAKO',
          [
          'FF_ORSZA',	''''+a2+'''',
          'FF_FELIR', 	''''+a3+'''',
          'FF_FELTEL', 	''''+a4+'''',
          'FF_FELCIM', 	''''+a5+'''',
          'FF_FELSE1', 	''''+a6+'''',
          'FF_FELSE2', 	''''+a7+'''',
          'FF_FELSE3', 	''''+a8+'''',
          'FF_FELSE4', 	''''+a9+'''',
          'FF_FELSE5', 	''''+a10+'''',
          'FF_SZELE', 	LatS,
          'FF_HOSZA', 	LongS,
          'FF_GEOJSON', ''''+JSONSzoveg+''''
          ], '  WHERE FF_FELNEV = '''+ a1 + '''');
        Result:= -1;  // nem adunk vissza k�dot
        end;
    end;  // else, vagyis update �g
end;

function GetUjFelrakoNev(FELNEV: string): string;
var
  i: integer;
  ujnev, res: string;
begin
  i:= 1;
  res:= 'XXX';  // b�rmi csak ne �res
  while (res <> '') and (i<1000) do begin
    ujnev:=FELNEV+'_'+IntToStr(i);
    res:= Query_Select('FELRAKO', 'FF_FELNEV', ujnev, 'FF_FELNEV'); // ha nem tal�lja, �res �rt�ket ad vissza (=null)
    inc(i);
    end;  // while
  Result:= ujnev;

end;

function GetFelrakoKod(const CegNev, Orszag, ZIP, Varos, UtcaHazszam: string): string;
var
  S, Res: string;
begin
  S:= 'select FF_FEKOD from FELRAKO '+
        ' where FF_FELNEV='''+CegNev+'''';
  Res:= Query_SelectString('', S);
  if Res='' then begin
    S:= 'select FF_FEKOD from FELRAKO '+
        ' where FF_ORSZA='''+Orszag+''' and FF_FELIR='''+ZIP+''' and FF_FELTEL='''+AposztrofCsere(Varos)+''' and FF_FELCIM='''+AposztrofCsere(UtcaHazszam)+'''';
    Res:= Query_SelectString('', S);
    end;
  Result:= Res;
end;

function GetOrszagNemzetkoziNev(OrszagKod: string): string;
var
  S: string;
begin
  S:= 'select case when isnull(SZ_LEIRO, '''') <> '''' then SZ_LEIRO else SZ_MENEV end Orszagnev'+
      ' from szotar where SZ_FOKOD=''300'' and SZ_ALKOD='''+OrszagKod+'''';
  Result:= Query_SelectString('', S);
end;

function QueryCoordinates(OrszagKod, ZIP, Varos, UtcaHazszam: string): TGeoCodeInfo;
var
  S, CIM, UTCA, OrszagNev, Res, Hibauzenet: string;
  GoogleGeoCode: TGoogleGeoCode;
begin
  S:= 'select FF_SZELE, FF_HOSZA from FELRAKO '+
      ' where FF_ORSZA='''+OrszagKod+''' and FF_FELIR='''+ZIP+''' and FF_FELTEL='''+AposztrofCsere(Varos)+''' and FF_FELCIM='''+AposztrofCsere(UtcaHazszam)+'''  '+
      ' and FF_SZELE is not null and FF_HOSZA is not null ';
  Query_run(EgyebDlg.QueryAlap, S);
  if not EgyebDlg.QueryAlap.Eof then begin  // a c�mhez vannak m�r koordin�t�k
    Result.Coordinates.Longitude:= EgyebDlg.QueryAlap.FieldByName('FF_HOSZA').AsFloat;
    Result.Coordinates.Latitude:= EgyebDlg.QueryAlap.FieldByName('FF_SZELE').AsFloat;
    end
  else begin // m�g nincs hozz� koordin�ta
    OrszagNev:= GetOrszagNemzetkoziNev(OrszagKod);
    if (OrszagNev = '') then
        OrszagNev:= OrszagKod;  // ha ismeretlen
    CIM:= OrszagNev+' '+ZIP+' '+Varos;
    UTCA:= UtcaHazszam;
    Result.Coordinates.Longitude:= -1;
    Result.Coordinates.Latitude:= -1;
    GoogleGeoCode:= TGoogleGeoCode.Create(nil);
    try
      try
        Res:= GoogleGeoCode.CalculateGeoCode(CIM, UTCA, True);
        if Res='' then begin
          Result:= GoogleGeoCode.GetResultGeoCode;
          end  // if
        else begin // hib�t adott vissza a sz�m�t�s
          if pos(Ismeretlen_Geocode_cim, Res)=1 then  // ha a c�m nem j�, akkor ne zavarjuk �ssze a felhaszn�l�t a "Hiba" sz�val! :-)
              Hibauzenet:= Res
          else
              Hibauzenet:= 'Hiba a "GoogleGeoCode.CalculateGeoCode" elj�r�sban: '+Res;
          NoticeKi(Hibauzenet);
          Hibakiiro(Hibauzenet);
          end;  // else
      except
        on E: exception do begin
            Hibauzenet:= 'Hiba a "GoogleGeoCode.CalculateGeoCode" elj�r�sban: '+E.Message;
            NoticeKi(Hibauzenet);
            Hibakiiro(Hibauzenet);
            end; // on E
           end;  // try-except
       finally
          if GoogleGeoCode<>nil then GoogleGeoCode.Free;
       end;  // try-finally
    end;  // else
end;

function GetSoforEagleSMS(rendszam: string): string;
var
   S, DOKOD, CSOPKOD, Kontakt: string;
   QueryDat1	: TADOQuery;
begin
	QueryDat1 		:= TADOQuery.Create(nil);
	QueryDat1.Tag := QUERY_ADAT_TAG;
	EgyebDlg.SeTADOQueryDatabase(QueryDat1);
  S:= 'select DO_KOD from rendszam_sofor where RENDSZ='''+rendszam+'''';
   with QueryDat1 do begin
      Close; SQL.Clear;
      SQL.Add(S);
      Open;
      S:='';
      while not eof do begin
        DOKOD:=Fields[0].AsString;
        CSOPKOD:= GetDolgozoCsoportKod(DOKOD, '');
        if Pos(','+CSOPKOD+',', ','+EgyebDlg.IRODAI_CSOPORT_LISTA+',')=0 then begin  // nem iroda, telep, stb = sof�r
           Kontakt:= Trim(EgyebDlg.GetDolgozoSMSEagleCimzett(DOKOD, false));
           S:= Felsorolashoz(S, Kontakt, '; ', False);
           end;  // if
        Next;
        end;  // if
      Result:=S;
      end;  // with
   QueryDat1.Free;
end;

procedure SetMegbizasModositoUser(MBKOD: string);
var
  QueryLocal: TADOQuery;
begin
	QueryLocal 		:= TADOQuery.Create(nil);
  try
    QueryLocal.Tag  	:= QUERY_ADAT_TAG;
    EgyebDlg.SeTADOQueryDatabase(QueryLocal);
    Query_Update( QueryLocal, 'MEGBIZAS',
          [
          'MB_MSMOD', ''''+EgyebDlg.user_code+''''
          ], ' WHERE MB_MBKOD = '+MBKOD );
  finally
    if QueryLocal <> nil then QueryLocal.Free;
    end;  // try-finally
end;

function VanHozzaIgaziSzamla(MBKOD: string): string;
var
  S, Res: string;
  QueryLocal: TADOQuery;
begin
	QueryLocal 		:= TADOQuery.Create(nil);
  try
    Res:= '';
    QueryLocal.Tag  	:= QUERY_ADAT_TAG;
    EgyebDlg.SeTADOQueryDatabase(QueryLocal);
    S:= 'select SA_KOD, SA_UJKOD from VISZONY, MEGBIZAS, SZFEJ where VI_VIKOD = MB_VIKOD and VI_UJKOD=SA_UJKOD and MB_MBKOD='+MBKOD+
       //  ' and SA_FLAG not in (2,3)';
        ' and SA_FLAG in (0,2,3)';
    Query_Run(QueryLocal, S);
    if not QueryLocal.Eof then Res:= 'V�gleges�tett sz�mla: '+ QueryLocal.Fields[0].AsString+ ' ('+QueryLocal.Fields[1].AsString+')';

    if Res = '' then begin  // megn�zz�k, hogy nincs-e gy�jt�sz�ml�ban
      S:= 'select SA_KOD, SA_UJKOD from VISZONY, MEGBIZAS, SZFEJ2, SZSOR2 '+
      ' where VI_VIKOD = MB_VIKOD and VI_UJKOD=S2_UJKOD and S2_S2KOD=SA_UJKOD and MB_MBKOD='+MBKOD;
      Query_Run(QueryLocal, S);
      if not QueryLocal.Eof then Res:= 'Gy�jt�sz�mla: '+ QueryLocal.Fields[0].AsString+ ' ('+QueryLocal.Fields[1].AsString+')';
      end; // if not Res1
    Result:= Res;
  finally
    if QueryLocal <> nil then QueryLocal.Free;
    end;  // try-finally
end;

function LezaratlanSzamlaTorles(MBKOD: string): string;
var
  S, Res, UJKOD: string;
  QueryLocal: TADOQuery;
  SQLArray: TStringList;
begin
	QueryLocal 		:= TADOQuery.Create(nil);
  SQLArray:= TStringList.Create;
  try
    Result:= 'A kapcsol�d�, nem v�gleges�tett sz�ml�k t�rl�se nem siker�lt!'; //
    Res:= '';
    QueryLocal.Tag  	:= QUERY_ADAT_TAG;
    EgyebDlg.SeTADOQueryDatabase(QueryLocal);
    S:= 'select SA_UJKOD from VISZONY, MEGBIZAS, SZFEJ where VI_VIKOD = MB_VIKOD and VI_UJKOD=SA_UJKOD and MB_MBKOD='+MBKOD+
        ' and SA_FLAG in (1) and SA_RESZE=0'+
        ' and isnull(VI_SAKOD, '''')='''' ';  // biztosan nincs v�gleges�tve
    Query_Run(QueryLocal, S);
    if not QueryLocal.Eof then begin
       UJKOD:= QueryLocal.Fields[0].AsString;
       S:= 'delete from SZSOR where SS_UJKOD = '+UJKOD;
       SQLArray.Add(S);
       S:= 'delete from SZFEJ where SA_UJKOD = '+UJKOD;
       SQLArray.Add(S);
       S:= 'update viszony set VI_UJKOD = null where VI_UJKOD = '+UJKOD;
       SQLArray.Add(S);
       Result:= ExecuteSQLTransaction(SQLArray);
       end;
  finally
    if SQLArray<>nil then SQLArray.Free;
    if QueryLocal <> nil then QueryLocal.Free;
    end;  // try-finally
end;


function SzotarBovito(const Fokod: string; UjElem: string): string;
// felveszi az elemet a sz�t�rba, ha m�g nem l�tezik
var
	Query1: TADOQuery;
  Alkod: string;
begin
  Query1	:= TADOQuery.Create(nil);
	EgyebDlg.SeTADOQueryDatabase(Query1);
 	Query_Run(Query1, 'select SZ_ALKOD from SZOTAR where SZ_FOKOD= '''+Fokod+''' and SZ_MENEV='''+UjElem+'''');
  if Query1.RecordCount > 0 then begin  // m�r l�tezik
    Result:= Query1.Fields[0].AsString;
    Exit;
    end;
 	Query_Run(Query1, 'select max(convert(int, SZ_ALKOD))+1 from SZOTAR where SZ_FOKOD= '''+Fokod+'''');
  if Query1.RecordCount > 0 then begin
    Alkod:= Query1.Fields[0].AsString;
  	Query_Insert(Query1, 'SZOTAR',
    [ 'SZ_FOKOD', ''''+Fokod+'''',
      'SZ_ALKOD', ''''+Alkod+'''',
      'SZ_MENEV', ''''+UjElem+'''',
      'SZ_LEIRO', ''''+''+'''',
      'SZ_ERVNY', '1'
				]);
   	Result:= Alkod;
  	end; // if
end;

function GetDolgozoCsoportKod(const DOKOD: string; Datum: string): string;
var
  S: string;
begin
  if Datum='' then Datum:= EgyebDlg.MaiDatum;
  S:= 'select DC_CSKOD from DOLGOZO '+
               ' left outer join DOLGOZOCSOP on DC_DOKOD = DO_KOD '+
               ' where '+
               ' DC_DATTOL= (select max(cs2.DC_DATTOL) from DOLGOZOCSOP cs2 where cs2.DC_DOKOD=DO_KOD and DC_DATTOL<='''+Datum+''') '+
               ' and DO_ARHIV=0 and isnull(DO_KOZOSLISTA, 0) = 0 '+
               ' and DO_KOD='''+DOKOD+'''';
  Result:= Query_SelectString('', S);
end;

function KoltsegTorles(const KTKOD: integer; const Gyujtobe_vissza: boolean): string;
var
   S, Res: string;
   SQLArray: TStringList;
begin
   Result:= '';
   SQLArray:= TStringList.Create;
   try  // finally
      if Gyujtobe_vissza then begin
         S:= 'update KOLTSEG_GYUJTO set KS_JAKOD='''', KS_JARAT='''', KS_KTKOD2=0 where KS_KTKOD2 = '+IntToStr(KTKOD);
         SQLArray.Add(S);
         end;
      S:= 'DELETE FROM KOLTSEG WHERE KS_KTKOD = '+IntToStr(KTKOD);
      SQLArray.Add(S);
      Res:= ExecuteSQLTransaction(SQLArray);
      Result:= Res;
   finally
      if SQLArray<>nil then SQLArray.Free;
      end;  // try-finally
end;

end.

