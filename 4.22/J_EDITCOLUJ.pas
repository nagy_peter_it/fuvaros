unit J_EDITCOLUJ;

interface

uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, DBTables, DB, StdCtrls, Buttons, Mask, DBCtrls,
  Printers, ExtCtrls, CheckLst, ADODB, Grids, Windows;

type
  TEditcolUjDlg = class(TForm)
    SG1: TStringGrid;
    Panel1: TPanel;
    BitElkuld: TBitBtn;
    BitKilep: TBitBtn;
    Panel2: TPanel;
    SpeedButton6: TSpeedButton;
    SpeedButton5: TSpeedButton;
    SpeedButton4: TSpeedButton;
    SpeedButton3: TSpeedButton;
	 RadioButton1: TRadioButton;
    RadioButton2: TRadioButton;
    BitBtn1: TBitBtn;
    M1: TMaskEdit;
    BitBtn26: TBitBtn;
    procedure BitBtn6Click(Sender: TObject);
	 procedure BitElkuldClick(Sender: TObject);
    procedure BitKilepClick(Sender: TObject);
	 procedure Tolt2(grid : TStringGrid; tag : integer);
	 procedure SpeedButton3Click(Sender: TObject);
	 procedure SpeedButton4Click(Sender: TObject);
	 procedure SpeedButton5Click(Sender: TObject);
	 procedure SpeedButton6Click(Sender: TObject);
	 procedure FormCanResize(Sender: TObject; var NewWidth,
	   NewHeight: Integer; var Resize: Boolean);
	 procedure FormKeyDown(Sender: TObject; var Key: Word;
	   Shift: TShiftState);
	 procedure Mentes(user : string);
	 procedure SG1GetEditText(Sender: TObject; ACol, ARow: Integer;
	   var Value: String);
	 procedure RadioButton1Click(Sender: TObject);
	 procedure BitBtn1Click(Sender: TObject);
	 procedure MezoEllenor;
    procedure SG1SelectCell(Sender: TObject; ACol, ARow: Integer;
      var CanSelect: Boolean);
    procedure BitBtn26Click(Sender: TObject);
    procedure M1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
		tagszam		: integer;
		modosult	: boolean;
		kulsoGrid	: TStringGrid;
  public
  		ret_value	: boolean;
  end;

var
  EditcolUjDlg: TEditcolUjDlg;

implementation

uses
	Egyeb, Kozos, J_SQL;
{$R *.DFM}

procedure TEditcolUjDlg.BitBtn6Click(Sender: TObject);
begin
	Close;
end;

procedure TEditcolUjDlg.BitElkuldClick(Sender: TObject);
var
	QueryMezo	: TADOQuery;
begin
	// Lementj�k a t�bl�zatot
	QueryMezo		:= TADOQuery.Create(nil);
	QueryMezo.Tag  	:= 0;
	EgyebDlg.SeTADOQueryDatabase(QueryMezo);
	// Lementj�k a t�bl�zatot
	if RadioButton1.Checked then begin
		// Be�ll�tjuk, hogy a k�z�set olvassa
		Query_Run(QueryMezo, 'DELETE FROM MEZOK WHERE MZ_DIKOD = '+IntToStr(tagszam)+' AND MZ_FEKOD = '+IntToStr(StrToIntDef(EgyebDlg.user_code, 0))+' AND MZ_SORSZ = -1 ');
		Query_Run(QueryMezo, 'INSERT INTO MEZOK ( MZ_DIKOD, MZ_FEKOD, MZ_SORSZ ) VALUES ( '+IntToStr(tagszam)+', '+IntToStr(StrToIntDef(EgyebDlg.user_code, 0))+', -1 )');
	end else begin
		Query_Run(QueryMezo, 'DELETE FROM MEZOK WHERE MZ_DIKOD = '+IntToStr(tagszam)+' AND MZ_FEKOD = '+IntToStr(StrToIntDef(EgyebDlg.user_code, 0))+' AND MZ_SORSZ = -1 ');
		Mentes(EgyebDlg.user_code);
	end;
	QueryMezo.Destroy;
	ret_value	:= true;
	Close;
end;

procedure TEditcolUjDlg.BitKilepClick(Sender: TObject);
begin
	ret_value	:= false;
	Close;
end;

procedure TEditcolUjDlg.SpeedButton3Click(Sender: TObject);
var
	i		: integer;
begin
	// Helycsere az el�z� elemmel
   if SG1.Row > 1 then begin
		for i := SG1.Row downto 2 do begin
			SpeedButton4Click(Sender);
       end;
       SG1.Row := 1;
	end;
end;

procedure TEditcolUjDlg.SpeedButton4Click(Sender: TObject);
var
	i		: integer;
   str		: string;
begin
	// Helycsere az el�z� elemmel
   if SG1.Row > 1 then begin
		for i := 1 to SG1.ColCount - 1 do begin
       	str						:= SG1.Cells[i,SG1.Row];
       	SG1.Cells[i,SG1.Row]	:= SG1.Cells[i,SG1.Row-1];
       	SG1.Cells[i,SG1.Row-1] 	:= str;
       end;
       SG1.Row := SG1.Row - 1;
   end;
end;

procedure TEditcolUjDlg.SpeedButton5Click(Sender: TObject);
var
	i		: integer;
   str		: string;
begin
	// Helycsere a k�vetkez� elemmel
   if SG1.Row < SG1.RowCount -1 then begin
		for i := 1 to SG1.ColCount - 1 do begin
       	str						:= SG1.Cells[i,SG1.Row];
       	SG1.Cells[i,SG1.Row]	:= SG1.Cells[i,SG1.Row+1];
			SG1.Cells[i,SG1.Row+1] 	:= str;
       end;
       SG1.Row := SG1.Row + 1;
	end;
end;

procedure TEditcolUjDlg.SpeedButton6Click(Sender: TObject);
var
	i		: integer;
begin
	// Helycsere az utols� elemmel
   if SG1.Row < SG1.RowCount -1 then begin
		for i := SG1.Row to SG1.RowCount - 2 do begin
       	SpeedButton5Click(Sender);
       end;
       SG1.Row := SG1.RowCount - 1;
   end;
end;

procedure TEditcolUjDlg.FormCanResize(Sender: TObject; var NewWidth,
  NewHeight: Integer; var Resize: Boolean);
begin
	if ((NewWidth < 700) or (NewHeight < 500) ) then begin
   	Resize	:= false;
   end;
end;

procedure TEditcolUjDlg.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
	if ssAlt in Shift then begin
     	if key = VK_F3 then begin
			if NoticeKi('Val�ban el k�v�nja menteni a be�ll�t�sokat alap�rtelmezettk�nt? ', NOT_QUESTION) = 0 then begin
           	// Ment�s alap�rtelmezettk�nt
   			Mentes('');
       	end;
     	end;
    	Exit;
 	end;
end;

procedure TEditcolUjDlg.Mentes(user : string);
var
	i 			: integer;
	QueryMezo	: TADOQuery;
begin
	// Lementj�k a t�bl�zatot
  	QueryMezo		:= TADOQuery.Create(nil);
  	QueryMezo.Tag  	:= 0;
	EgyebDlg.SeTADOQueryDatabase(QueryMezo);
	Query_Run(QueryMezo, 'DELETE FROM MEZOK WHERE MZ_DIKOD = '+IntToStr(tagszam)+
		' AND MZ_FEKOD = '+IntToStr(StrToIntDef(user, 0)));
	for i := 1 to SG1.RowCount -1 do begin
		Query_Insert( QueryMezo, 'MEZOK', [
			'MZ_DIKOD', IntToStr(tagszam),
			'MZ_FEKOD', IntToStr(StrToIntDef(user, 0)),
			'MZ_SORSZ', IntToStr(i),
			'MZ_TIPUS', SG1.Cells[4,i],
			'MZ_MZNEV', ''''+SG1.Cells[1,i]+'''',
			'MZ_MKIND', SG1.Cells[9,i],
			'MZ_LABEL', ''''+SG1.Cells[2,i]+'''',
			'MZ_WIDTH', IntToStr(StrToIntDef(SG1.Cells[5,i],0)),
			'MZ_MSIZE', IntToStr(StrToIntDef(SG1.Cells[7,i],0)),
			'MZ_VISIB', IntToStr(StrToIntDef(SG1.Cells[3,i],0)),
			'MZ_HIDEN', IntToStr(StrToIntDef(SG1.Cells[11,i],0)),
			'MZ_MZTAG', IntToStr(StrToIntDef(SG1.Cells[6,i],0)),
			'MZ_ALIGN', SG1.Cells[8,i],
			'MZ_MEGJE', ''''+SG1.Cells[10,i]+''''
			]);
	end;
	QueryMezo.Free;
	modosult	:= false;
end;

procedure TEditcolUjDlg.Tolt2(grid : TStringGrid; tag : integer);
var
	QueryMezo	: TADOQuery;
begin
	// A grid felt�lt�se
	KulsoGrid			:= grid;
	tagszam				:= tag;
	Sg1.RowCount		:= 2;
	SG1.Rows[1].Clear;
	SG1.Cells[0,0] 		:= 'Ssz.';
	SG1.Cells[1,0] 		:= 'Mez�n�v';
	SG1.Cells[2,0] 		:= 'Cimke';
	SG1.Cells[3,0] 		:= 'L�that�';
	SG1.Cells[4,0] 		:= 'Tipus';
	SG1.Cells[5,0] 		:= 'Hossz';
	SG1.Cells[6,0] 		:= 'Tag';
	SG1.Cells[7,0] 		:= 'M�ret';
	SG1.Cells[8,0] 		:= 'Ir�ny';
	SG1.Cells[9,0] 		:= 'Fajta';
	SG1.Cells[10,0] 	:= 'Megjegyz�s';
	SG1.Cells[11,0] 	:= 'Hidden';
	SG1.ColWidths[4]	:= -1;
	SG1.ColWidths[6]	:= -1;
	SG1.ColWidths[7]	:= -1;
	SG1.ColWidths[8]	:= -1;
	SG1.ColWidths[9]	:= -1;
	SG1.ColWidths[11]	:= -1;
	SG1.ColWidths[10]	:= 400;
(*
	SG1.RowCount 		:= grid.RowCount + 1;
	for i := 0 to grid.RowCount - 1 do begin
		SG1.Rows[i+1] 	:= grid.Rows[i];
		SG1.Cells[0,i+1]:= grid.Cells[0,i];
	end;
*)
	Caption			:= 'Az oszlopok meghat�roz�sa ( Tag = '+IntToStr(tagszam)+'; User = '+EgyebDlg.user_code+' )';
	QueryMezo		:= TADOQuery.Create(nil);
	QueryMezo.Tag  	:= 0;
	EgyebDlg.SeTADOQueryDatabase(QueryMezo);
	Query_Run(QueryMezo, 'SELECT * FROM MEZOK WHERE MZ_DIKOD = '+IntToStr(tagszam)+
			' AND MZ_FEKOD = '+IntToStr(StrToIntDef(EgyebDlg.user_code, 0))+' AND MZ_SORSZ = -1 ');
	if QueryMezo.RecordCount > 0 then begin
		RadioButton1.Checked := true;
	end else begin
		RadioButton2.Checked := true;
	end;
	QueryMezo.Free;
//	RadioButton1Click(Self);
end;

procedure TEditcolUjDlg.SG1GetEditText(Sender: TObject; ACol,
  ARow: Integer; var Value: String);
begin
	modosult	:= true;
end;

procedure TEditcolUjDlg.RadioButton1Click(Sender: TObject);
var
	QueryMezo	: TADOQuery;
	mezoszam	: integer;
begin
	// Az alap�rtelmezett elrendez�s beolva�sa
	QueryMezo		:= TADOQuery.Create(nil);
	QueryMezo.Tag  	:= 0;
	EgyebDlg.SeTADOQueryDatabase(QueryMezo);
	if RadioButton1.Checked then begin
		// Az alap�rtelmezett haszn�lata
		Query_Run(QueryMezo, 'SELECT * FROM MEZOK WHERE MZ_DIKOD = '+IntToStr(tagszam)+
			' AND MZ_FEKOD = 0 AND MZ_SORSZ > -1 ORDER BY MZ_SORSZ');
	end else begin
		// A saj�t haszn�lata
		Query_Run(QueryMezo, 'SELECT * FROM MEZOK WHERE MZ_DIKOD = '+IntToStr(tagszam)+
			' AND MZ_FEKOD = '+IntToStr(StrToIntDef(EgyebDlg.user_code, 0))+' AND MZ_SORSZ > -1 ORDER BY MZ_SORSZ');
	end;
	if QueryMezo.RecordCount > 0 then begin
		SG1.RowCount   	:=  QueryMezo.RecordCount + 1 ;
		for mezoszam	:= 1 to SG1.RowCount -1 do begin
			SG1.Cells[0,mezoszam] 	:= IntToStr(mezoszam);
			SG1.Cells[1,mezoszam] 	:= QueryMezo.FieldByName('MZ_MZNEV').AsString;
			SG1.Cells[2,mezoszam] 	:= QueryMezo.FieldByName('MZ_LABEL').AsString;
			SG1.Cells[3,mezoszam]	:= QueryMezo.FieldByName('MZ_VISIB').AsString;
			SG1.Cells[4,mezoszam] 	:= QueryMezo.FieldByName('MZ_TIPUS').AsString;
			SG1.Cells[5,mezoszam]	:= QueryMezo.FieldByName('MZ_WIDTH').AsString;
			SG1.Cells[6,mezoszam]	:= QueryMezo.FieldByName('MZ_MZTAG').AsString;
			SG1.Cells[7,mezoszam]	:= QueryMezo.FieldByName('MZ_MSIZE').AsString;
			SG1.Cells[8,mezoszam]	:= QueryMezo.FieldByName('MZ_ALIGN').AsString;
			SG1.Cells[9,mezoszam]	:= QueryMezo.FieldByName('MZ_MKIND').AsString;
			SG1.Cells[10,mezoszam]	:= QueryMezo.FieldByName('MZ_MEGJE').AsString;
			SG1.Cells[11,mezoszam]	:= QueryMezo.FieldByName('MZ_HIDEN').AsString;
			QueryMezo.Next;
			modosult	:= false;
		end;
	end;
	QueryMezo.Free;
	MezoEllenor;
end;

procedure TEditcolUjDlg.BitBtn1Click(Sender: TObject);
var
	QueryMezo	: TADOQuery;
	mezoszam	: integer;
begin
	if NoticeKi('Val�ban be kiv�nja t�lteni az alap�rtelmezett be�llit�sokat? ', NOT_QUESTION) = 0 then begin
		// Az alap�rtelmezett elrendez�s beolva�sa
		QueryMezo		:= TADOQuery.Create(nil);
		QueryMezo.Tag  	:= 0;
		EgyebDlg.SeTADOQueryDatabase(QueryMezo);
		Query_Run(QueryMezo, 'SELECT * FROM MEZOK WHERE MZ_DIKOD = '+IntToStr(tagszam)+' AND MZ_FEKOD = 0 ORDER BY MZ_SORSZ');
		if QueryMezo.RecordCount > 0 then begin
			SG1.RowCount   	:=  QueryMezo.RecordCount + 1 ;
			for mezoszam	:= 1 to SG1.RowCount -1 do begin
				SG1.Cells[0,mezoszam] 	:= IntToStr(mezoszam);
				SG1.Cells[1,mezoszam] 	:= QueryMezo.FieldByName('MZ_MZNEV').AsString;
				SG1.Cells[2,mezoszam] 	:= QueryMezo.FieldByName('MZ_LABEL').AsString;
				SG1.Cells[3,mezoszam]	:= QueryMezo.FieldByName('MZ_VISIB').AsString;
				SG1.Cells[4,mezoszam] 	:= QueryMezo.FieldByName('MZ_TIPUS').AsString;
				SG1.Cells[5,mezoszam]	:= QueryMezo.FieldByName('MZ_WIDTH').AsString;
				SG1.Cells[6,mezoszam]	:= QueryMezo.FieldByName('MZ_MZTAG').AsString;
				SG1.Cells[7,mezoszam]	:= QueryMezo.FieldByName('MZ_MSIZE').AsString;
				SG1.Cells[8,mezoszam]	:= QueryMezo.FieldByName('MZ_ALIGN').AsString;
				SG1.Cells[9,mezoszam]	:= QueryMezo.FieldByName('MZ_MKIND').AsString;
				SG1.Cells[10,mezoszam]	:= QueryMezo.FieldByName('MZ_MEGJE').AsString;
				SG1.Cells[11,mezoszam]	:= QueryMezo.FieldByName('MZ_HIDEN').AsString;
				QueryMezo.Next;
			end;
			MezoEllenor;
		end;
		QueryMezo.Free;
		modosult	:= false;
	end;
end;

procedure TEditcolUjDlg.MezoEllenor;
var
	i 		: integer;
	mezszam : integer;
	mezonev	: string;
begin
	// Hozz�tessz�k a plusz mez�ket �s levessz�k a felesleget
	for i := 0 to kulsogrid.Rowcount - 1 do begin
		mezonev	:= kulsogrid.Cells[1,i];
		mezszam	:= SG1.Cols[1].IndexOf(mezonev);
		if mezszam = -1 then begin
			// A mez� hozz�rak�sa
			if SG1.Cells[0, SG1.RowCount-1] <> '' then begin
				SG1.RowCount := SG1.RowCount + 1;
			end;
			SG1.Rows [  SG1.RowCount - 1] 	:= kulsogrid.Rows[i];
			SG1.Cells[0,SG1.RowCount - 1]	:= IntToStr(SG1.RowCount - 1);
		end;
	end;
end;

procedure TEditcolUjDlg.SG1SelectCell(Sender: TObject; ACol, ARow: Integer;
  var CanSelect: Boolean);
begin
	if ( ( ACol = 4 ) or ( ACol = 6 ) or ( ACol = 7 ) or ( ACol = 8 ) or ( ACol = 9 ) or ( ACol = 11 ) ) then begin
		CanSelect := false;
	end;
end;

procedure TEditcolUjDlg.BitBtn26Click(Sender: TObject);
var
   i       : integer;
   sor     : integer;
   sorstr  : string;
   voltertek   : integer;
begin
   // Megkeress�k a k�vetkez� sz�veget
   if M1.Text = '' then begin
       Exit;
   end;
   sor         := SG1.Row + 1;
   voltertek   := -1;
   while ( sor mod SG1.RowCount ) <> SG1.Row do begin
       sorstr  := ';';
       for i := 0 to SG1.ColCount - 1 do begin
           sorstr  := sorstr + SG1.Cells[i, sor mod SG1.RowCount]+';';
       end;
       if Pos(UpperCase(M1.Text), UpperCase(sorstr)) > 0 then begin
           voltertek   := sor mod SG1.RowCount;
           sor         := SG1.Row - 1;
       end;
       Inc(sor);
   end;
   if voltertek > -1 then begin
       SG1.Row := voltertek;
   end;
end;

procedure TEditcolUjDlg.M1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
   if Key = VK_RETURN then begin
       BitBtn26Click(Sender);       
   end;
end;

end.


