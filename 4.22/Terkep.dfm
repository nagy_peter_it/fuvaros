object FTerkep: TFTerkep
  Left = 409
  Top = 236
  VertScrollBar.Visible = False
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = 'T'#233'rk'#233'p'
  ClientHeight = 516
  ClientWidth = 772
  Color = clBtnFace
  Constraints.MinHeight = 492
  Constraints.MinWidth = 652
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poMainFormCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 772
    Height = 57
    Align = alTop
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 0
    DesignSize = (
      772
      57)
    object Label1: TLabel
      Left = 8
      Top = 6
      Width = 53
      Height = 16
      Caption = 'Id'#337'pont'
      FocusControl = DBEdit1
    end
    object Label2: TLabel
      Left = 243
      Top = 6
      Width = 72
      Height = 16
      Caption = 'Sebess'#233'g'
      FocusControl = DBEdit2
    end
    object Label3: TLabel
      Left = 8
      Top = 30
      Width = 33
      Height = 16
      Caption = 'Hely'
      FocusControl = DBEdit3
    end
    object DBText1: TDBText
      Left = 511
      Top = 2
      Width = 110
      Height = 32
      AutoSize = True
      DataField = 'PO_RENSZ'
      DataSource = DataSource1
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -27
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label4: TLabel
      Left = 648
      Top = 9
      Width = 17
      Height = 16
      Hint = 'H'#225'tral'#233'v'#337' id'#337' a friss'#237't'#233'sig'
      Alignment = taRightJustify
      Anchors = [akTop, akRight]
      Caption = '00'
      ParentShowHint = False
      ShowHint = True
    end
    object Button1: TButton
      Left = 666
      Top = 4
      Width = 86
      Height = 25
      Hint = 'A g'#233'pkocsi aktu'#225'lis poz'#237'ci'#243'j'#225'nak megjelen'#237't'#233'se'
      Anchors = [akTop, akRight]
      Caption = 'Friss'#237't'#233's'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = Button1Click
    end
    object DBEdit1: TDBEdit
      Left = 75
      Top = 4
      Width = 153
      Height = 24
      DataField = 'PO_DAT'
      DataSource = DataSource1
      TabOrder = 1
      OnEnter = DBEdit1Enter
    end
    object DBEdit2: TDBEdit
      Left = 323
      Top = 4
      Width = 73
      Height = 24
      DataField = 'PO_SEBES'
      DataSource = DataSource1
      TabOrder = 2
      OnEnter = DBEdit1Enter
    end
    object DBEdit3: TDBEdit
      Left = 75
      Top = 28
      Width = 425
      Height = 24
      DataField = 'PO_GEKOD'
      DataSource = DataSource1
      ParentShowHint = False
      ShowHint = True
      TabOrder = 3
      OnEnter = DBEdit1Enter
    end
    object DBEdit4: TDBEdit
      Left = 395
      Top = 4
      Width = 105
      Height = 24
      DataField = 'PO_GYUJTAS'
      DataSource = DataSource1
      TabOrder = 4
      OnEnter = DBEdit1Enter
    end
    object CheckBox1: TCheckBox
      Left = 666
      Top = 35
      Width = 86
      Height = 17
      Hint = 'Automatikus friss'#237't'#233's'
      Anchors = [akTop, akRight]
      Caption = 'Automatikus'
      Checked = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      State = cbChecked
      TabOrder = 5
      OnClick = CheckBox1Click
    end
    object CheckBoxTraffic: TCheckBox
      Left = 511
      Top = 35
      Width = 121
      Height = 17
      Hint = 'Nem minden orsz'#225'gban '#233'rhet'#337' el a szolg'#225'ltat'#225's'
      Caption = 'Forgalomfigyel'#233's'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 6
      OnClick = CheckBoxTrafficClick
    end
  end
  object Button2: TButton
    Left = -100
    Top = 80
    Width = 75
    Height = 25
    Cancel = True
    Caption = 'Button2'
    TabOrder = 1
    OnClick = Button2Click
  end
  object Panel2: TPanel
    Left = 0
    Top = 57
    Width = 772
    Height = 64
    Align = alTop
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 2
    DesignSize = (
      772
      64)
    object Label5: TLabel
      Left = 8
      Top = 36
      Width = 57
      Height = 16
      Caption = #201'rkez'#233's'
      FocusControl = DBEdit3
    end
    object Label7: TLabel
      Left = 511
      Top = 35
      Width = 39
      Height = 16
      Anchors = [akTop, akRight]
      Caption = 'Ir'#225'ny:'
      FocusControl = DBEdit3
      Visible = False
      ExplicitLeft = 477
    end
    object Edit1: TEdit
      Left = 75
      Top = 34
      Width = 425
      Height = 24
      Hint = 'Szerkeszthet'#337
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      Text = 'Edit1'
    end
    object Button3: TButton
      Left = 612
      Top = 6
      Width = 140
      Height = 25
      Hint = 'A teljes '#250'tvonal megmutat'#225'sa'
      Anchors = [akTop, akRight]
      Caption = 'Teljes '#250'tvonal'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = Button3Click
    end
    object Edit2: TEdit
      Left = 75
      Top = 6
      Width = 425
      Height = 24
      Hint = 'Szerkeszthet'#337
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      Text = 'Edit1'
    end
    object Button4: TButton
      Left = 511
      Top = 6
      Width = 86
      Height = 25
      Hint = #218'tvonal a g'#233'pkocsi poz'#237'ci'#243'j'#225't'#243'l a felrak'#243' helyhez'
      Anchors = [akTop, akRight]
      Caption = 'Felrak'#243'hoz'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 3
      OnClick = Button4Click
    end
    object Button5: TButton
      Left = 511
      Top = 33
      Width = 86
      Height = 25
      Hint = #218'tvonal a g'#233'pkocsi poz'#237'ci'#243'j'#225't'#243'l a lerak'#243' helyhez'
      Anchors = [akTop, akRight]
      Caption = 'Lerak'#243'hoz'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 4
      OnClick = Button5Click
    end
    object Button6: TButton
      Left = 3
      Top = 6
      Width = 66
      Height = 25
      Caption = 'Felrak'#243
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 5
      OnClick = Button6Click
    end
    object Button7: TButton
      Left = 3
      Top = 34
      Width = 66
      Height = 25
      Caption = 'Lerak'#243
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 6
      OnClick = Button7Click
    end
    object CheckBox2: TCheckBox
      Left = 612
      Top = 38
      Width = 140
      Height = 17
      Hint = 
        'Forgalomfriss'#237't'#233's KI-BE kapcsol'#225'sa.'#13#10'Nem minden orsz'#225'gban '#233'rhet'#337 +
        ' el a szolg'#225'ltat'#225's'
      Anchors = [akTop, akRight]
      Caption = 'Forgalomfigyel'#233's'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 7
      OnClick = CheckBox2Click
    end
    object Panel3: TPanel
      Left = 3
      Top = 6
      Width = 66
      Height = 25
      Hint = 'A felrak'#243'hely megmutat'#225'sa'
      BevelWidth = 2
      Caption = 'Felrak'#243
      ParentBackground = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 8
      OnClick = Panel3Click
    end
    object Panel4: TPanel
      Left = 3
      Top = 34
      Width = 66
      Height = 25
      Hint = 'A lerak'#243'hely megmutat'#225'sa'
      BevelWidth = 2
      Caption = 'Lerak'#243
      ParentBackground = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 9
      OnClick = Panel4Click
    end
  end
  object Chromium1: TChromium
    Left = 0
    Top = 121
    Width = 772
    Height = 380
    Color = clWhite
    Align = alClient
    DefaultUrl = 'about:blank'
    TabOrder = 3
    OnConsoleMessage = Chromium1ConsoleMessage
  end
  object pnlStatus: TPanel
    Left = 0
    Top = 501
    Width = 772
    Height = 15
    Align = alBottom
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -8
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 4
  end
  object Query1: TADOQuery
    Connection = EgyebDlg.ADOConnectionKozos
    CursorType = ctStatic
    OnCalcFields = Query1CalcFields
    Parameters = <>
    SQL.Strings = (
      'select * from poziciok')
    Left = 408
    Top = 112
    object Query1PO_RENSZ: TStringField
      FieldName = 'PO_RENSZ'
      Size = 10
    end
    object Query1PO_GEKOD: TStringField
      FieldName = 'PO_GEKOD'
      Size = 200
    end
    object Query1PO_DATUM: TStringField
      FieldName = 'PO_DATUM'
    end
    object Query1PO_SEBES: TIntegerField
      FieldName = 'PO_SEBES'
      DisplayFormat = '0 km/h'
    end
    object Query1PO_DIGIT: TStringField
      FieldName = 'PO_DIGIT'
      Size = 8
    end
    object Query1PO_SZELE: TFloatField
      FieldName = 'PO_SZELE'
    end
    object Query1PO_HOSZA: TFloatField
      FieldName = 'PO_HOSZA'
    end
    object Query1PO_GYUJTAS: TStringField
      FieldKind = fkCalculated
      FieldName = 'PO_GYUJTAS'
      Calculated = True
    end
    object Query1PO_DAT: TDateTimeField
      FieldName = 'PO_DAT'
    end
    object Query1PO_IRANYSZOG: TSmallintField
      FieldName = 'PO_IRANYSZOG'
    end
    object Query1PO_UDAT: TDateTimeField
      FieldName = 'PO_UDAT'
    end
    object Query1PO_DIGIT2: TWordField
      FieldName = 'PO_DIGIT2'
    end
    object Query1PO_VALID: TWordField
      FieldName = 'PO_VALID'
    end
    object Query1PO_ETIPUS: TWordField
      FieldName = 'PO_ETIPUS'
    end
  end
  object Timer1: TTimer
    Enabled = False
    Interval = 30000
    OnTimer = Timer1Timer
    Left = 336
    Top = 112
  end
  object DataSource1: TDataSource
    DataSet = Query1
    Left = 376
    Top = 112
  end
  object Timer2: TTimer
    Enabled = False
    OnTimer = Timer2Timer
    Left = 264
    Top = 112
  end
end
