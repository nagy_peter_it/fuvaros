unit J_DataFunc;

interface

uses
	DBTables, SysUtils, StdCtrls, Classes, ADODB, DateUtils, DB, Forms;

	function 	Create_Field(tabla, mezonev, leiras, megj, formatum : string;
		dialkod, tipus, dispszel, szelesseg, lathato, tag, rend, rejt, tized : integer ) : boolean;
	function    DialogTolto(kod, nev, leiras : string) : boolean;


implementation

uses
	Egyeb, Kozos, J_Sql, J_Export;

function Create_Field(tabla, mezonev, leiras, megj, formatum : string;
		dialkod, tipus, dispszel, szelesseg, lathato, tag, rend, rejt, tized : integer ) : boolean;
var
	Query1		: TADOQuery;
	sqlstr		: string;
	sorsz		: integer;
	voltmezo	: boolean;
begin
	Result 		:= false;
	Query1		:= TADOQuery.Create(nil);
	if tipus > 2 then begin	// Csak string, float, integer l�trehoz�sa
		Exit;
	end;
	if Pos(UpperCase(tabla), DAT_TABLAK) = 0 then begin
		Query1.Tag	:= QUERY_KOZOS_TAG;
	end else begin
		Query1.Tag	:= QUERY_ADAT_TAG;
	end;
	EgyebDlg.SetAdoQueryDatabase(Query1);
	// A mez� l�trehoz�sa
	sqlstr	:= 'ALTER TABLE '+UpperCase(tabla)+' ADD '+mezonev;
	case tipus of
		0 : // string
			sqlstr	:= sqlstr + ' VARCHAR('+IntToStr(szelesseg)+')';
		1 : // float
			sqlstr	:= sqlstr + Format(' NUMERIC( %3d, %3d )', [szelesseg, tized]);
		2 : // integer
			sqlstr	:= sqlstr + ' INTEGER ';
	end;
	if not Query_Run(Query1, sqlstr, FALSE) then begin
		// M�r l�tezik a mez�
		Exit;
	end;
	Result	:= true;
	// A mez�tulajdons�gok besz�r�sa a t�bl�ba
	Query1.Close;
	Query1.Tag	:= QUERY_KOZOS_TAG;
	EgyebDlg.SetAdoQueryDatabase(Query1);
	Query_Run(Query1,'SELECT MZ_MZNEV, MZ_SORSZ FROM MEZOK WHERE MZ_DIKOD = '+IntToStr(dialkod)+
		' AND MZ_FEKOD = ''0'' ORDER BY MZ_SORSZ ');
	voltmezo	:= false;
	sorsz		:= 1;
	while not Query1.Eof do begin
		if UpperCase(Query1.FieldByName('MZ_MZNEV').AsString) = UpperCase(mezonev) then begin
			// M�r l�tezik ilyen mez�
			voltmezo	:= true;
		end;
		if StrToIntDef(Query1.FieldByName('MZ_SORSZ').AsString, 0) > sorsz then begin
			sorsz	:= StrToIntDef(Query1.FieldByName('MZ_SORSZ').AsString, 0);
		end;
		Query1.Next;
	end;
	if not voltmezo  then begin
		// L�tre kell hozni az adatb�zisban a mez�t
		Query_Insert( Query1, 'MEZOK', [
			'MZ_DIKOD', IntToStr(dialkod),
			'MZ_FEKOD', ''''+'0'+'''',		// Ez lesz az alap�rtelmezett elrendez�s
			'MZ_MZNEV', ''''+UpperCase(mezonev)+'''',
			'MZ_SORSZ', IntToStr(sorsz + 1)
			]);
	end;
	Query_Update( Query1, 'MEZOK', [
		'MZ_TIPUS', IntToStr(tipus),
		'MZ_MKIND', '0',
		'MZ_LABEL', ''''+leiras+'''',
		'MZ_WIDTH', IntToStr(dispszel),
		'MZ_MSIZE', IntToStr(szelesseg),
		'MZ_VISIB', IntToStr(lathato),
		'MZ_HIDEN', IntToStr(rejt),
		'MZ_MZTAG', IntToStr(tag),
		'MZ_ALIGN', IntToStr(rend),
		'MZ_FORMA', ''''+formatum+'''',
		'MZ_MEGJE', ''''+megj+''''
		], ' WHERE MZ_DIKOD = '+IntToStr(dialkod)+' AND MZ_MZNEV = '''+UpperCase(mezonev)+''' ' );
	Query1.Destroy;
end;

function  DialogTolto(kod, nev, leiras : string) : boolean;
var
	Query1		: TADOQuery;
begin
	Query1		:= TADOQuery.Create(nil);
	Query1.Tag	:= QUERY_KOZOS_TAG;
	EgyebDlg.SetAdoQueryDatabase(Query1);
	Query_Run(Query1, 'SELECT * FROM DIALOGS WHERE DL_DLKOD = '+IntToStr(StrToIntDef(kod, 0)), false) ;
	if Query1.RecordCount < 1 then begin
		Result := Query_Run(Query1, 'INSERT INTO DIALOGS ( DL_DLNEV, DL_DLKOD, DL_LEIRS ) VALUES '+
		 '( '''+nev+''', '+IntToStr(StrToIntDef(kod, 0))+' , '''+leiras+''' )');
	end else begin
		Result		:= true;
	end;
	Query1.Destroy;
end;

end.

