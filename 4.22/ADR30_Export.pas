unit ADR30_Export;

interface

uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls, Egyeb,
  Forms, Dialogs, DBTables, DB, StdCtrls, Buttons, Mask, DBCtrls, Grids,
  Printers, ExtCtrls, ADODB, XMLDoc, StrUtils;

type
  TADR30_ExportDlg = class(TForm)
	 Label1: TLabel;
	 BitBtn4: TBitBtn;
	 BitBtn6: TBitBtn;
    M1: TMaskEdit;
	 M2: TMaskEdit;
	 Label2: TLabel;
	 Label3: TLabel;
	 BitBtn10: TBitBtn;
	 BitBtn1: TBitBtn;
	 Query1: TADOQuery;
    Label4: TLabel;
    Query2: TADOQuery;
    Query3: TADOQuery;
    Query4: TADOQuery;
    QueryFel: TADOQuery;
    QueryLe: TADOQuery;
    Query5: TADOQuery;
    M50: TMaskEdit;
    M5: TMaskEdit;
    BitBtn5: TBitBtn;
	 procedure FormCreate(Sender: TObject);
	 procedure BitBtn4Click(Sender: TObject);
	 procedure BitBtn6Click(Sender: TObject);
	 procedure M1Click(Sender: TObject);
	 procedure BitBtn10Click(Sender: TObject);
	 procedure BitBtn1Click(Sender: TObject);
	 procedure M1Exit(Sender: TObject);
	 procedure ADR_Generalas;
	 function  RovidDatum(dd : string) : string;
	 function  Szamkiiro(szam : double; hossz, tizedes : integer) : string;
	 procedure BitBtn5Click(Sender: TObject);
  private
		kilepes		: boolean;
  public
		gr			: TStringGrid;
		voltkilepes	: boolean;
		kelluzenet	: boolean;
		kombinalt	: boolean;
  end;

var
  ADR30_ExportDlg: TADR30_ExportDlg;

implementation

uses
	j_sql, Kozos, ValVevo, J_DataModule, Kozos_Export,Kombi_Export;
{$R *.DFM}

procedure TADR30_ExportDlg.FormCreate(Sender: TObject);
begin
	gr			:= nil;
	kilepes     := true;
	voltkilepes	:= false;
	kelluzenet	:= true;
	kombinalt	:= false;
	EgyebDlg.SetADOQueryDatabase(Query1);
	EgyebDlg.SetADOQueryDatabase(Query2);
	EgyebDlg.SetADOQueryDatabase(Query3);
	EgyebDlg.SetADOQueryDatabase(Query4);
	EgyebDlg.SetADOQueryDatabase(Query5);
	EgyebDlg.SetADOQueryDatabase(QueryFel);
	EgyebDlg.SetADOQueryDatabase(QueryLe);
	Kezdoertekek(M1, M2, M5, M50);
end;

procedure TADR30_ExportDlg.BitBtn4Click(Sender: TObject);
begin
	if not DatumExit(M1, false) then begin
		Exit;
	end;
	if not DatumExit(M2, false) then begin
		Exit;
	end;
	if M50.Text = '' then begin
		NoticeKi('A megb�z� kiv�laszt�sa k�telez�!');
		M5.SetFocus;
		Exit;
	end;
	// Az ADR-030 �llom�ny gener�l�sa
	Screen.Cursor	:= crHourGlass;
	ADR_Generalas;
	Screen.Cursor	:= crDefault;
	if kombinalt then begin
		Close;
	end else begin
		NoticeKi('A riport gener�l�sa befejez�d�tt!');
	end;
end;

procedure TADR30_ExportDlg.BitBtn6Click(Sender: TObject);
begin
	if not kilepes then begin
		kilepes	:= true;
	end else begin
		Close;
	end;
end;

procedure TADR30_ExportDlg.M1Click(Sender: TObject);
begin
	with Sender as TMaskEdit do begin
		SelectAll;
	end;
end;

procedure TADR30_ExportDlg.BitBtn10Click(Sender: TObject);
begin
	// EgyebDlg.Calendarbe(M1);
	// EgyebDlg.ElsoNapEllenor(M1, M2);
  EgyebDlg.CalendarBe_idoszak(M1, M2);
end;

procedure TADR30_ExportDlg.BitBtn1Click(Sender: TObject);
begin
	EgyebDlg.Calendarbe(M2, True);
end;

procedure TADR30_ExportDlg.M1Exit(Sender: TObject);
begin
	DatumExit(M1,false);
	EgyebDlg.ElsoNapEllenor(M1, M2);
end;

procedure TADR30_ExportDlg.ADR_Generalas;
var
	fn			: String;
	fileazon	: TextFile;
	sor			: string;
	rekordszam	: integer;
	sorszam		: integer;
	ujkod		: string;
	eurnetto	: double;
	lanossz		: double;
	potlek		: double;
	suly		: double;
	kelluzpotl  : boolean;
   reszrak     : string;
   te_kodok    : string;
   te_fekod    : string;
   te_lekod    : string;
   te_szoveg   : string;
begin

	Query_Run(Query1, 'SELECT MB_MBKOD, MB_DATUM, MB_RENSZ, MB_VIKOD, MB_JAKOD, MB_POZIC, MB_FUDIJ, MB_VEKOD,MB_ALVAL,MB_NUZPOT, MB_KISCS FROM MEGBIZAS '+
			' WHERE MB_MBKOD IN ( SELECT MS_MBKOD FROM MEGSEGED WHERE MS_LETDAT >= '''+M1.Text+''' AND MS_LETDAT <= '''+M2.Text+''' ) '+
			' AND MB_VEKOD IN ('+M50.Text+') '+
			' ORDER BY MB_DATUM, MB_MBKOD ' );
	if Query1.RecordCount <= 1 then begin
		if kelluzenet then begin
			NoticeKi('Nincs a felt�teleknek megfelel� elem!');
		end;
		Exit;
	end;
	// fn	:= EgyebDlg.Temppath + 'ADR030_'+copy(M1.Text,1,4)+copy(M1.Text,6,2)+'.TXT';
  fn	:= EgyebDlg.Temppath + 'TE_TED_JSSPEED_IN_FFDATA.'+FormatDateTime('YYYYMMDDHHMMSS', Now)+'.TXT';    // NagyP 2016-02-18: TE_TED_JSSPEED_IN_FFDATA.<YYYYMMDDHHMMSS>.txt
	if FileExists(fn) then begin
		DeleteFile(PChar(fn));
	end;
	// Az ADR megnyit�sa
	{$I-}
	AssignFile(fileazon, fn);
	Rewrite(fileazon);
	{$I+}
	if IOResult <> 0 then begin
		Exit;
	end;
	EllenListTorol;
	BitBtn4.Hide;
	rekordszam			:= 0;
	Query1.DisableControls;
	kilepes				:= false;
	while ( ( not Query1.Eof ) and (not kilepes) ) do begin
	 Query_Run(Query4, 'SELECT MS_MBKOD FROM MEGSEGED WHERE MS_MBKOD = '+Query1.FieldByName('MB_MBKOD').AsString+' AND MS_FETDAT <'''+ M1.Text+''' ');
   if Query4.RecordCount=0 then
   begin
		Application.ProcessMessages;
    kelluzpotl:= StrToIntDef( Query1.FieldByName('MB_NUZPOT').AsString,0) = 0 ;
		Label4.Caption	:= 'D�tum : '+Query1.FieldByName('MB_DATUM').AsString + ' - megb�z�s k�d : ' +Query1.FieldByName('MB_MBKOD').AsString;
		Label4.Update;
    if Query1.FieldByName('MB_MBKOD').AsString='47250' then
      date;
		sor		:= 'F586';             //1-4 Record Layout ID 4
		// Megkeress�k a megb�z�shoz tartoz� sz�ml�t
		if Query1.FieldByName('MB_VIKOD').AsString = '' then begin
			EllenListAppend('Hi�nyz� sz�mlasz�m: '+'D�tum : '+Query1.FieldByName('MB_DATUM').AsString + ' - megb�z�s k�d : ' +Query1.FieldByName('MB_MBKOD').AsString);
			Query1.Next;
			Continue;
		end;
		ujkod 	:= '';
		Query_Run(Query3, 'SELECT * FROM VISZONY WHERE VI_VIKOD = '+Query1.FieldByName('MB_VIKOD').AsString + ' AND VI_JAKOD = '''+Query1.FieldByName('MB_JAKOD').AsString+''' ');
		if Query3.RecordCount = 1 then begin
			ujkod	:= Query3.FieldByName('VI_UJKOD').AsString
		end;
		Query_Run(Query3, 'SELECT * FROM SZFEJ WHERE SA_UJKOD = '+IntToStr(StrToIntDef(ujkod, 0)));
		if Query3.RecordCount < 1 then begin
			EllenListAppend('Nem l�tez� sz�mlasz�m: '+'D�tum : '+Query1.FieldByName('MB_DATUM').AsString + ' - megb�z�s k�d : ' +Query1.FieldByName('MB_MBKOD').AsString+
				'  SA_UJKOD = '+IntToStr(StrToIntDef(ujkod, 0)));
			Query1.Next;
			Continue;
		end;
		//sor		:= sor +  copy(Query3.FieldByName('SA_KOD').AsString+URESSTRING,1,15);        //  5-22	Filler   15       // 1.9-t�l KG 20120110
		sor		:= sor +  copy(URESSTRING,1,15);                                                                            // 1.9-t�l KG 20120110
		sor		:= sor +  '   ';
		// Megkeress�k az els� felrak�st
		Query_Run(QueryFel, 'SELECT * FROM MEGSEGED WHERE MS_MBKOD = '+Query1.FieldByName('MB_MBKOD').AsString+' AND MS_TIPUS = 0 ORDER BY MS_MSKOD ');
		sor		:= sor +  copy(QueryFel.FieldByName('MS_FELTEL').AsString+URESSTRING,1,30); //23-52	Origin City 	30
		sor		:= sor +  '      ';          //53-58	Origin City State Province Abbreviation	6
		sor		:= sor +  copy(QueryFel.FieldByName('MS_FORSZ').AsString+URESSTRING,1,2);   //59-60	Origin City ISO Country Code	2
		sor		:= sor +  '   ';     //61-63	Origin Airport ID	3
		//Az utols� lerak� meghat�roz�sa
		Query_Run(QueryLe, 'SELECT * FROM MEGSEGED WHERE MS_MBKOD = '+Query1.FieldByName('MB_MBKOD').AsString+' AND MS_TIPUS = 1 ORDER BY MS_MSKOD ');
		QueryLe.Last;
		sor			:= sor +  copy(QueryLe.FieldByName('MS_LERTEL').AsString+URESSTRING,1,30);      //64-93	Destination City 	30
		sor			:= sor +  '      ';
		sor			:= sor +  copy(QueryLe.FieldByName('MS_ORSZA').AsString+URESSTRING,1,2);    //100-101	Destination City ISO Country Code	2
		sor			:= sor +  '   ';          // 102-104	Destination Airport ID	3
		sor			:= sor +  copy(RovidDatum(QueryFel.FieldByName('MS_FETDAT').AsString)+URESSTRING,1,8);   //105-112	Shipment Pickup Date	8
		sor			:= sor +  copy(RovidDatum(QueryLe.FieldByName('MS_LETDAT').AsString)+URESSTRING,1,8);    //113-120	Shipment Delivered Date	8
		sor			:= sor +  'XXX';                                                                         //121-123	Contract Transit Commitment Days	3
		sor			:= sor +  '3';                                                                           //124-124	Transport Mode Code	1
       // Az express - standard besz�r�sa
       if StrToIntDef(Query1.FieldByName('MB_KISCS').AsString, 0) > 0 then begin
		    sor			:= sor +  copy('express '+URESSTRING,1,25);                                          //125-149  Forwarder�s Transport Service Level Code 	25
       end else begin
		    sor			:= sor +  copy('standard'+URESSTRING,1,25);                                          //125-149  Forwarder�s Transport Service Level Code 	25
       end;
		sor			:= sor +  '1';                                                                           //150-150	Tyco Transport Service Level Code	1
		lanossz	 	:= StringSzam(Query1.FieldByName('MB_FUDIJ').AsString);
		JSzamla		:= TJSzamla.Create(Self);
		JSzamla.FillSzamla(ujkod);
		potlek	:= JSzamla.uapotlekeur;
		if kelluzpotl and (potlek = 0) then begin
			EllenListAppend('Az �zemanyag p�tl�k �sszege 0! Sz�mlasz�m : '+JSzamla.szamlaszam);
			Query1.Next;
			Continue;
		end else begin

    //if ( Abs ( JSzamla.ossz_nettoeur - JSzamla.spedicioeur - potlek - lanossz ) > 0.00001 ) then begin
		//if ( Abs ( JSzamla.ossz_nettoeur - JSzamla.spedicioeur - potlek - lanossz ) > 0.00001 )and(pos(Query1.FieldByName('MB_ALVAL').AsString,Kombi_ExportDlg.belf_alval)=0) then begin
			if (kombinalt)and( Abs ( JSzamla.ossz_nettoeur - JSzamla.spedicioeur - potlek - lanossz ) > 0.00001 )and(pos(Query1.FieldByName('MB_ALVAL').AsString,Kombi_ExportDlg.belf_alval)=0) then begin
				EllenListAppend('A sz�mla nett� eur - sped�ci�s d�j <> megb�z�si fuvard�j! Sz�mla : '+JSzamla.szamlaszam+'  Megb�z�s k�d : '+Query1.FieldByName('MB_MBKOD').AsString);
				Query1.Next;
				Continue;
			end else begin
				eurnetto	:= JSzamla.ossz_nettoeur - JSzamla.spedicioeur;
			end;
		end;
		JSzamla.Destroy;

		sor		:= sor + Szamkiiro(eurnetto,21,4);                          //151-171	Total Invoice Amount	21
		sor		:= sor + Szamkiiro(potlek,21,4);                                 //172-192	Fuel Surcharge Amount	21
       // K�b�s s�ly ki�r�sa
		suly	:= GetKobSuly( Query1.FieldByName('MB_MBKOD').AsString );
		sor		:= sor + Szamkiiro(suly,21,4);                                 //193-213	Shipment Chargeable KG Weight	21
		suly	:= GetSuly( Query1.FieldByName('MB_MBKOD').AsString );
		sor		:= sor + Szamkiiro(suly, 21, 4) ;                           //214-234	Shipment Actual KG Weight	21

       // prepaid - collect jel�l�se                                                              //
       te_kodok    := ',253,330,362,374,657,741,1223,1333,1914,1979,2113,2341,2617,3433,3831,5297,6496,6499,6706,6809,7512,7908,11469,11913,15940,18701,21273,21478,21812,23245,';
       // Az els� felrak� tervezett felrak�hely�nek k�dja
       te_fekod    := QueryFel.FieldByName('MS_FELNEV').AsString;
       te_fekod    := Query_Select('FELRAKO', 'FF_FELNEV', te_fekod, 'FF_FEKOD');
       te_lekod    := QueryLe.FieldByName('MS_LERNEV').AsString;
       te_lekod    := Query_Select('FELRAKO', 'FF_FELNEV', te_lekod, 'FF_FEKOD');
       te_szoveg   := '       ';
       // A felrak� TE de a lerak� NEM!!!
       if ( ( Pos(','+te_fekod+',', te_kodok) > 0 ) and ( Pos(','+te_lekod+',', te_kodok) = 0 ) ) then begin
           te_szoveg   := '2';             // 'prepaid';
       end;
       if ( ( ( Pos(','+te_fekod+',', te_kodok) > 0 ) and ( Pos(','+te_lekod+',', te_kodok) > 0 ) ) or
            ( ( Pos(','+te_fekod+',', te_kodok) = 0 ) and ( Pos(','+te_lekod+',', te_kodok) > 0 ) ) ) then begin
           te_szoveg   := '1';             // 'collect';
       end;
		sor		:= sor +  te_szoveg;                                              //235-235	Freight Payment Terms Code	1
       // A r�szrak�s�g eld�nt�se
       reszrak := Query_Select('GEPKOCSI', 'GK_RESZ', QueryLe.FieldByName('MS_RENSZ').AsString, 'GK_RESZR');
       if StrToIntDef(reszrak,0) = 0 then begin
           reszrak := 'FTL';
       end else begin
           reszrak := 'LTL';
       end;
		sor		:= sor +  reszrak;                                            //236-238	Container Utilization Code	3
		sor		:= sor + Szamkiiro(0,21,4);                                 //239-259	Shipment Total Volume in Cubic Meters	21
		// Els� felrak� adatai
		sor		:= sor +  copy(QueryFel.FieldByName('MS_FELNEV').AsString+URESSTRING,1,30);   //260-289	Shipper Name	30
		sor		:= sor +  copy(QueryFel.FieldByName('MS_FELTEL').AsString+URESSTRING,1,30);   //290-319	Shipper City Name	30
		sor		:= sor +  '      ';                                                           //320-325	Shipper State Province Abbreviation	6
		sor		:= sor +  copy(QueryFel.FieldByName('MS_FELIR').AsString+URESSTRING,1,12);    //326-337	Shipper Postal Code	12
		sor		:= sor +  copy(QueryFel.FieldByName('MS_FORSZ').AsString+URESSTRING,1,2);     //338-339	Shipper ISO Country Code	2
		// A megb�z� c�g neve
(*
		Query_Run(Query5, 'SELECT V_NEV, V_VAROS, V_IRSZ, V_ORSZ FROM VEVO WHERE V_KOD = '''+Query1.FieldByName('MB_VEKOD').AsString+''' ');
		if Query5.RecordCount < 1 then begin
			sor		:= sor +  copy(URESSTRING,1,30);                                            //340-369	Consignee Name	30
			sor		:= sor +  copy(URESSTRING,1,30);                                            //370-399	Consignee City Name	30
			sor		:= sor +  '      ';                                                         //400-405	Consignee State Province Abbreviation	6
			sor		:= sor +  copy(URESSTRING,1,12);                                            //406-417	Consignee Postal Code	12
			sor		:= sor +  copy(URESSTRING,1,2);                                             //418-419	Consignee ISO Country Code	2
		end else begin
			sor		:= sor +  copy(Query5.FieldByName('V_NEV').AsString+URESSTRING,1,30);
			sor		:= sor +  copy(Query5.FieldByName('V_VAROS').AsString+URESSTRING,1,30);
			sor		:= sor +  '      ';
			sor		:= sor +  copy(Query5.FieldByName('V_IRSZ').AsString+URESSTRING,1,12);
			sor		:= sor +  copy(Query5.FieldByName('V_ORSZ').AsString+URESSTRING,1,2);
		end;
*)
       // Az utols� lerak� adatai
       sor		:= sor +  copy(QueryLe.FieldByName('MS_LERNEV').AsString+URESSTRING,1,30);
       sor		:= sor +  copy(QueryLe.FieldByName('MS_LERTEL').AsString+URESSTRING,1,30);
       sor		:= sor +  '      ';
       sor		:= sor +  copy(QueryLe.FieldByName('MS_LELIR').AsString+URESSTRING,1,12);
       sor		:= sor +  copy(QueryLe.FieldByName('MS_ORSZA').AsString+URESSTRING,1,2);

		sor		:= sor +  'EUR';                                                                   //420-422	Local ISO Currency Code 	3
		sor		:= sor +  copy(Query1.FieldByName('MB_POZIC').AsString+URESSTRING,1,15);           //423-437	Tyco Bill of Lading Number	15
		sor		:= sor +  copy(URESSTRING,1,25);                                                   //438-462	Tyco Reference Number	25
		sor		:= sor +  copy(URESSTRING,1,50);                                                   //463-512	Forwarder Shipment Comment Text	50
		sor		:= sor +  copy(Query3.FieldByName('SA_KOD').AsString+URESSTRING,1,22);             //513-534	Invoice Number	22          // 1.9-t�l KG 20120110
		sor		:= sor +  copy(RovidDatum(Query3.FieldByName('SA_KIDAT').AsString)+URESSTRING,1,8);//535-542	Invoice Date	8             // 1.9-t�l KG 20120110
		sor		:= sor +  copy(Query3.FieldByName('SA_POZICIO').AsString+URESSTRING,1,28);         //543-570	TE Load Id	28              // 1.9-t�l KG 20120110
		sor		:= sor +  copy(Query3.FieldByName('SA_JARAT').AsString+URESSTRING,1,20);           //571-590	Forwarder House Bill Id	20  // 1.9-t�l KG 20120110
	 //	sor		:= sor +  copy(URESSTRING,1,38);                                                                                        // 1.9-t�l KG 20120110
		sor		:= sor +  copy(URESSTRING,1,18);                                                   //591-608	Filler 	18                  // 1.9-t�l KG 20120110

		SorLogolas(gr, Query1.FieldByName('MB_MBKOD').AsString, suly, eurnetto, potlek, 3);
//		SorLogolas(gr, Query1.FieldByName('MB_MBKOD').AsString, suly, lanossz, potlek, 3);    eurnetto
		writeln(fileazon, sor);
		Inc(rekordszam);
   end;
		Query1.Next;
	end;
	Query1.EnableControls;

	// A kontroll rekord hozz�rak�sa
	sorszam	:= StrToIntDef(EgyebDlg.Read_SZGrid('888', '200'), 0);
	Inc(sorszam);
	Query_Run(Query2, 'DELETE FROM SZOTAR WHERE SZ_FOKOD = ''888'' AND SZ_ALKOD = ''200'' ' );
	Query_Run(Query2, 'INSERT INTO SZOTAR ( SZ_FOKOD, SZ_ALKOD, SZ_MENEV, SZ_LEIRO, SZ_ERVNY ) VALUES '+
		' ( ''888'', ''200'', '''+IntToStr(sorszam)+''',''Az ADR-030 export sorsz�ma'', 1 ) ', FALSE);
	sor		:= 'F566';
//	sor		:= sor + Format('%10.10d', [sorszam]);
	sor		:= sor + '0000002728';
	sor		:= sor + '  ';
	sor		:= sor + '   ';
	sor		:= sor + RovidDatum(M2.Text);
	sor		:= sor + Format('%10.10d', [rekordszam]);
	sor		:= sor + DupeString(' ', 513);
	Writeln(fileazon, sor);
	CloseFile(fileazon);
	Label4.Caption			:= '';
	Label4.Update;
	voltkilepes	:= kilepes;
	if ( ( EgyebDlg.ellenlista.Count > 0 ) and kelluzenet ) then begin
		EllenListMutat('Az ADR gener�l�s eredm�nye', false);
	end;
	BitBtn4.Show;
	kilepes	:= true;
end;

function TADR30_ExportDlg.RovidDatum(dd : string) : string;
var
	res	: string;
begin
	res	:= copy(dd,1,4)+copy(dd,6,2)+copy(dd,9,2);
	Result	:= res;
end;

function TADR30_ExportDlg.Szamkiiro(szam : double; hossz, tizedes : integer) : string;
var
	res		: string;
	pozic	: integer;
begin
	if szam >= 0 then begin
		res		:= '+';
	end else begin
		res		:= '-';
	end;
	res			:= res + RightStr(DupeString('0', hossz) + Format('%'+'.'+IntToStr(tizedes)+'f', [szam]), hossz-1);
	pozic		:= Pos(',', res);
	while pozic > 0 do begin
		res		:= copy(res,1, pozic-1)+'.'+copy(res,pozic+1, Length(res));
		pozic	:= Pos(',', res);
	end;
	Result		:= res;
end;

procedure TADR30_ExportDlg.BitBtn5Click(Sender: TObject);
begin
	// T�bb vev� kiv�laszt�sa
	Application.CreateForm(TValvevoDlg, ValvevoDlg);
	ValvevoDlg.Tolt(M50.Text);
	ValvevoDlg.ShowModal;
	if ValvevoDlg.kodkilist.Count > 0 then begin
		M5.Text		:= ValvevoDlg.nevlista;
		M50.Text	:= ValvevoDlg.kodlista;
	end;
	ValvevoDlg.Destroy;
end;

end.




