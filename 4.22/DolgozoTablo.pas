unit DolgozoTablo;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, Data.Win.ADODB, Vcl.ExtCtrls,
  Vcl.Imaging.jpeg, Vcl.StdCtrls, Math;

type
  TDolgozoTabloDlg = class(TForm)
    Query1: TADOQuery;
    ScrollBox1: TScrollBox;
    FlowPanel1: TFlowPanel;
    procedure FormCreate(Sender: TObject);
  private
    procedure DisplayTablo;
    procedure AddTabloElem(DisplayName: string; ms: TMemoryStream);
    procedure AdjustJPG (jpg: TJPegImage);
  public
    { Public declarations }
  end;

var
  DolgozoTabloDlg: TDolgozoTabloDlg;

implementation

{$R *.dfm}

uses J_SQL, Kozos, Egyeb, PictureUtils;


procedure TDolgozoTabloDlg.FormCreate(Sender: TObject);
begin
	EgyebDlg.SetADOQueryDatabase(Query1);
  Screen.Cursor	:= crHourGlass;
  DisplayTablo;
  Screen.Cursor	:= crDefault;
end;

procedure TDolgozoTabloDlg.DisplayTablo;
var
  S, DisplayName: string;
  ms: TMemoryStream;
  // MyBLOBField: TBLOBField;
begin
 ms:=TMemoryStream.Create;
 // MyBLOBField:= TBLOBField.Create(nil);
 try
     while FlowPanel1.ControlCount > 0 do FlowPanel1.Controls[0].Free;  // az �sszes al-panel t�rl�se
     // S:= 'select d.DO_KOD, d.DO_NAME, f.DO_FOTO from dolgozo d, dolgozofoto f '+
     //   ' where DO_ARHIV=0 and d.DO_KOD=f.DO_KOD '+
     //   ' order by d.DO_NAME';
     S:= 'select d.DO_KOD, d.DO_NAME, f.DO_FOTO '+
        ' from dolgozo d left outer join dolgozofoto f on d.DO_KOD=f.DO_KOD '+
        ' where DO_ARHIV=0 order by d.DO_NAME';

     Query_Run(Query1, S);
     while not Query1.eof do begin
       DisplayName:= Query1.FieldByName('DO_NAME').AsString;
       ms.Clear;
       // MyBLOBField:= (Query1.FieldByName('DO_FOTO') as TBLOBField);
       // MyBLOBField.SaveToStream(ms);
       if not Query1.FieldByName('DO_FOTO').IsNull then
         (Query1.FieldByName('DO_FOTO') as TBLOBField).SaveToStream(ms);
       AddTabloElem(DisplayName, ms);
       Query1.Next;
       end; // while
  finally
    if ms <> nil then ms.Free;
    // if MyBLOBField <>nil then MyBLOBField.Free;
    end;  // try-finally
end;

procedure TDolgozoTabloDlg.AddTabloElem(DisplayName: string; ms: TMemoryStream);
const
  ElemWidth = 160;
  ElemHeight = 180;
  PictureHeight = 160;
  BelsoMargin = 1;
var
  Elem: TPanel;
  jpg:TJPegImage;

begin
   jpg:=TJPegImage.Create;
   try
       Elem:= TPanel.Create(FlowPanel1);
       with Elem do begin
          Parent:= FlowPanel1;
          Height:=ElemHeight;
          Width:=ElemWidth;
          end;
       with TImage.Create(Elem) do begin
          Parent:= Elem;
          Left:=BelsoMargin;
          Top:=BelsoMargin;
          Height:=PictureHeight;
          Width:=ElemWidth-(2*BelsoMargin);
          Stretch:= True;
          if ms.Size>0 then begin
            ms.Position := 0;
            JPG.LoadFromStream(ms);
            AdjustJPG(jpg);
            Picture.Assign(jpg);
            end;  // if
          end;
       with TPanel.Create(Elem) do begin
          Parent:= Elem;
          Top:= PictureHeight +BelsoMargin;
          Left:=1;
          Height:= ElemHeight - PictureHeight - (3*BelsoMargin);
          Width:=ElemWidth-(2*BelsoMargin);
          Caption:= DisplayName;
          Font.Size:=10;
          end;
   finally
      if jpg <> nil then jpg.free;
      end;
end;

{procedure TDolgozoTabloDlg.AddTabloElem(DisplayName: string; ms: TMemoryStream);
const
  ElemWidth = 160;
  ElemHeight = 180;
  PictureHeight = 160;
  BelsoMargin = 1;
var
  Elem: TPanel;
  jpg:TJPegImage;

begin
 if ms.Size>0 then begin
   jpg:=TJPegImage.Create;
   try
       Elem:= TPanel.Create(FlowPanel1);
       with Elem do begin
          Parent:= FlowPanel1;
          Height:=ElemHeight;
          Width:=ElemWidth;
          end;
       with TImage.Create(Elem) do begin
          Parent:= Elem;
          Left:=BelsoMargin;
          Top:=BelsoMargin;
          Height:=PictureHeight;
          Width:=ElemWidth-(2*BelsoMargin);
          Stretch:= True;
          ms.Position := 0;
          JPG.LoadFromStream(ms);
          AdjustJPG(jpg);
          Picture.Assign(jpg);
          end;
       with TPanel.Create(Elem) do begin
          Parent:= Elem;
          Top:= PictureHeight +BelsoMargin;
          Left:=1;
          Height:= ElemHeight - PictureHeight - (3*BelsoMargin);
          Width:=ElemWidth-(2*BelsoMargin);
          Caption:= DisplayName;
          Font.Size:=10;
          end;
   finally
      if jpg <> nil then jpg.free;
      end;
   end
 else begin  // nincs k�p: csak a nevet tessz�k ki
    Elem:= TPanel.Create(FlowPanel1);
    with Elem do begin
       Parent:= FlowPanel1;
       Height:=ElemHeight - PictureHeight;
       Width:=ElemWidth;
       end;
    with TPanel.Create(Elem) do begin
      Parent:= Elem;
      Top:= BelsoMargin;
      Left:=1;
      Height:= ElemHeight - PictureHeight - (3*BelsoMargin);
      Width:=ElemWidth-(2*BelsoMargin);
      Caption:= DisplayName;
      Font.Size:=10;
      end;
    end;
end;
}

procedure TDolgozoTabloDlg.AdjustJPG (jpg: TJPegImage);
var
  bmp1: TBitMap;
  eredetiwidth, eredetiheight, ujwidth, ujheight, ujkezdox, ujkezdoy: integer;
begin
  // ha nem n�gyzetes a k�p, akkor "kieg�sz�tj�k" - nem v�gjuk, mert van ahol belev�gn�nk az arcba
  eredetiwidth:=JPG.width;
  eredetiheight:=JPG.height;
  if eredetiwidth <> eredetiheight then begin
    if eredetiwidth > eredetiheight then begin
        ujwidth:=eredetiwidth;
        ujheight:=eredetiwidth;
        ujkezdox:= 0;
        ujkezdoy:=floor((ujheight-eredetiheight)/2);
        end;
    if eredetiwidth < eredetiheight then begin
        ujwidth:=eredetiheight;
        ujheight:=eredetiheight;
        ujkezdox:=floor((ujwidth-eredetiwidth)/2);
        ujkezdoy:=0;
        end;
    bmp1:=TBitmap.Create;
    try
      bmp1.Assign(jpg);
      CropBitmap(bmp1, 0, 0, ujkezdox, ujkezdoy, ujheight, ujwidth, clBtnFace);
      // bmp1.SaveToFile('D:\Nagyp\X\x.bmp');
      jpg.Assign(bmp1);
    finally
      bmp1.Free;
      end;  // try-finally
    end;  // if nem n�gyzetes
end;


end.
