unit KoltsegFm;

interface

uses
	SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
	StdCtrls, Forms, DBCtrls, DB, DBGrids, DBTables, Grids, ExtCtrls, Buttons,
	ShellApi, Dialogs, ADODB, J_FOFORMUJ, Mask;

type
	TKoltsegFmDlg = class(TJ_FOFORMUJDLG)
	 Query3: TADOQuery;
	 IntegerField1: TIntegerField;
	 IntegerField2: TIntegerField;
	 StringField1: TStringField;
	 StringField2: TStringField;
	 FloatField1: TFloatField;
	 StringField3: TStringField;                               
	 FloatField2: TFloatField;
	 BCDField1: TBCDField;
	 FloatField3: TFloatField;
	 StringField4: TStringField;
	 IntegerField3: TIntegerField;
	 IntegerField4: TIntegerField;
	 FloatField4: TFloatField;
	 FloatField5: TFloatField;
	 StringField5: TStringField;
	 StringField6: TStringField;
	 StringField7: TStringField;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    M2: TMaskEdit;
    BitBtn5: TBitBtn;
    M3: TMaskEdit;
    BitBtn4: TBitBtn;
    BitBtn3: TBitBtn;
    BitBtn6: TBitBtn;
    BitBtn10: TBitBtn;
    Query4: TADOQuery;
    BitBtn7: TBitBtn;
    DSQ4: TDataSource;
    Query4ertek: TBCDField;
    DBEdit1: TDBEdit;
    LQ4: TLabel;
	  procedure FormCreate(Sender: TObject);override;
 	 procedure Adatlap(cim, kod : string);override;
	 procedure ButtonTorClick(Sender: TObject);override;
	 procedure Mezoiro;override;
	 procedure FormKeyDown(Sender: TObject; var Key: Word;
	   Shift: TShiftState);override;
    procedure Query1AfterOpen(DataSet: TDataSet);
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BitBtn5Click(Sender: TObject);
    procedure BitBtn4Click(Sender: TObject);
    procedure BitBtn10Click(Sender: TObject);
	 private
       szur140     :string;
	end;

var
	KoltsegFmDlg : TKoltsegFmDlg;

implementation

uses
	Egyeb, J_SQL, Koltsegbe, Kozos, {KoltsegImport,} Jaratbe,Kozos_Local;

{$R *.DFM}

procedure TKoltsegFmDlg.FormCreate(Sender: TObject);
begin
	Inherited FormCreate(Sender) ;
	AddSzuromezoRovid('KS_RENDSZ', 'KOLTSEG');
	AddSzuromezoRovid('KS_FIMOD',  'KOLTSEG');
	AddSzuromezoRovid('KS_TELE',   'KOLTSEG');
	AddSzuromezoRovid('KS_TIPUS',  'KOLTSEG');
	AddSzuromezoRovid('KS_TETAN',  'KOLTSEG');
	AddSzuromezoRovid('KS_LEIRAS',  'KOLTSEG');
	AddSzuromezoRovid('KS_JARAT',  'KOLTSEG');
	AddSzuromezoRovid('KS_ARFOLY',  'KOLTSEG');
	AddSzuromezoRovid('KS_ORSZA',  'KOLTSEG');
	AddSzuromezoRovid('KS_GKKAT',  'KOLTSEG');
	AddSzuromezoRovid('KS_MEGJ',  'KOLTSEG');


	M2.Text		:= copy(EgyebDlg.MaiDatum,1,5)+'01.01.';
	M3.Text		:= EgyebDlg.MaiDatum;

//  if EgyebDlg.stralap then
  	ALAPSTR		:= 'Select * from KOLTSEG  '+szur140  ;
//  else
//  	ALAPSTR		:= 'Select * from KOLTSEG  WHERE KS_DATUM = '''+M2.Text+''' '+szur140;

   EgyebDlg.stralap:=False;

	FelTolto('�j k�lts�g felvitele ', 'K�lts�g adatok m�dos�t�sa ',
			'K�lts�gek list�ja', 'KS_KTKOD', ALAPSTR,'KOLTSEG', QUERY_ADAT_TAG);
		//	'K�lts�gek list�ja', 'KS_KTKOD', 'Select * from KOLTSEG ','KOLTSEG', QUERY_ADAT_TAG);
		//	'K�lts�gek list�ja', 'KS_KTKOD', 'select k.*,g.gk_gkkat from koltseg as k left join '+EgyebDlg.v_koz_db+'.szami.gepkocsi as g on gk_resz=ks_rendsz','KOLTSEG', QUERY_ADAT_TAG);

	width	:= DLG_WIDTH;
	height	:= DLG_HEIGHT;
	EgyebDLg.SetADOQueryDatabase(Query3);
	//EgyebDLg.SetADOQueryDatabase(Query4);
	BitBtn1.Parent		:= PanelBottom;
	BitBtn2.Parent		:= PanelBottom;
	BitBtn3.Parent		:= PanelBottom;
	M2.Parent			:= PanelBottom;
	M3.Parent			:= PanelBottom;
	BitBtn5.Parent		:= PanelBottom;
	BitBtn4.Parent		:= PanelBottom;
	BitBtn6.Parent		:= PanelBottom;
	BitBtn10.Parent		:= PanelBottom;
	BitBtn7.Parent		:= PanelBottom;
	DBEdit1.Parent		:= PanelBottom;
	LQ4.Parent		:= PanelBottom;
end;

procedure TKoltsegFmDlg.Adatlap(cim, kod : string);
begin
	Application.CreateForm(TKoltsegbeDlg, AlForm);
	Inherited Adatlap(cim, kod );
end;

procedure TKoltsegFmDlg.Mezoiro;
begin
	Query1.FieldByname('TELE').AsString := ' I ';
	if Query1.FieldByName('KS_TELE').AsString = '0' then begin
		Query1.FieldByname('TELE').AsString := ' N ';
	end;
end;

procedure TKoltsegFmDlg.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
	atlag			: double;
begin
	Inherited FormKeyDown(Sender, Key, Shift);
	if ( EgyebDlg.CtrlGomb and ( StrToIntDef(Query1.FieldByName('KS_TIPUS').AsString,0) = 0 ) and EgyebDlg.user_super ) then begin
		// Az �tlag lek�r�se �s be�r�sa
		voltalt	:= true;
		atlag	:= GetKoltsegAtlag(Query1.FieldByName('KS_KTKOD').AsString);
		Query_Update (Query3, 'KOLTSEG',
			[
			'KS_ATLAG',		SqlSzamString(atlag,10,2)
			], ' WHERE KS_KTKOD = '+Query1.FieldByName('KS_KTKOD').AsString );
		ModLocate (Query1, FOMEZO, Query1.FieldByName(FOMEZO).AsString );
	end;
	if ( EgyebDlg.CtrlGomb and ( StrToIntDef(Query1.FieldByName('KS_TIPUS').AsString,0) = 2 ) and EgyebDlg.user_super ) then begin
		// Az �tlag lek�r�se �s be�r�sa
		voltalt	:= true;
		atlag	:= GetAdBlueKoltsegAtlag(Query1.FieldByName('KS_KTKOD').AsString);
		Query_Update (Query3, 'KOLTSEG',
			[
			'KS_ATLAG',		SqlSzamString(atlag,10,2)
			], ' WHERE KS_KTKOD = '+Query1.FieldByName('KS_KTKOD').AsString );
		ModLocate (Query1, FOMEZO, Query1.FieldByName(FOMEZO).AsString );
	end;
end;

procedure TKoltsegFmDlg.ButtonTorClick(Sender: TObject);
begin
	if not voltalt then begin
		voltalt 	:= true;
		Exit;
	end;
	if EllenorzottJarat(Query1.FieldByName('KS_JAKOD').AsString) > 0 then begin
		Exit;
	end;
	if not EgyebDlg.RekordTorles(Query1, alapstr, GetOrderBy(false) ,Tag,
		'DELETE from '+FOTABLA+' where '+FOMEZO+'='''+Query1.FieldByName(FOMEZO).AsString+''' ') then begin
		Close;
	end;
	Label3.Caption	:= '('+IntToStr(Query1.RecordCount)+')';
	Label3.Update;
end;

procedure TKoltsegFmDlg.BitBtn1Click(Sender: TObject);
begin
 {
	if ( (GetRightTag(568) <> RG_NORIGHT ) or EgyebDlg.user_super) then begin
       Application.CreateForm(TFKoltsegImport, FKoltsegImport);
       FKoltsegImport.ShowModal;
       FKoltsegImport.Free;
  	    ModLocate (Query1, FOMEZO, Query1.FieldByName(FOMEZO).AsString );
   end else begin
       NoticeKi('Nincs jogosults�g!');
   end;
   }
end;

procedure TKoltsegFmDlg.BitBtn2Click(Sender: TObject);
begin
		Application.CreateForm(TJaratbeDlg, JaratbeDlg);
    JaratbeDlg.megtekint:=True;  // (QueryUj3.FieldByName('JA_FAZIS').AsInteger > 0);
		JaratbeDlg.Tolto('J�rat megekint�s',Query1.FieldByName('KS_JAKOD').AsString);
		JaratbeDlg.ShowModal;
		JaratbeDlg.Destroy;
    JaratbeDlg:=nil;
end;

procedure TKoltsegFmDlg.FormResize(Sender: TObject);
begin
  inherited;
	M2.Top			:= BitBtn3.Top+2;
	M2.Left			:= BitBtn3.Left;
	BitBtn5.Top		:= BitBtn3.Top+2;
	BitBtn5.Left	:= BitBtn3.Left + M2.Width;
	BitBtn5.Width	:= BitBtn5.Height;

	M3.Top			:= BitBtn6.Top+2;
	M3.Left			:= BitBtn5.Left + BitBtn5.Width + 10;
	BitBtn4.Top		:= M3.Top;
	BitBtn4.Left	:= M3.Left + M3.Width;
	BitBtn4.Width	:= BitBtn4.Height;

	BitBtn10.Top	:= BitBtn6.Top;
	BitBtn10.Left	:= BitBtn4.Left + BitBtn4.Width + 10;
	BitBtn10.Width	:= 40;

  DBEdit1.Top:=BitBtn7.Top;
  DBEdit1.Left:=BitBtn7.Left;
  DBEdit1.Width:=BitBtn7.Width;
  LQ4.Top:=DBEdit1.Top+3;
  LQ4.Left:=DBEdit1.Left-110;

end;

procedure TKoltsegFmDlg.BitBtn5Click(Sender: TObject);
var
   d1, d2  : string;
begin
  d1      := M2.Text;
	CalendarRead(d1, d2);
  M2.Text := d1;
	M3.Text	:= d1;

	alapstr	:= 'SELECT * FROM KOLTSEG WHERE KS_DATUM = '''+M2.Text+''' '+szur140;
  if d2 <> '' then begin
	    M3.Text	:= d2;
	    alapstr	:= 'SELECT * FROM KOLTSEG WHERE KS_DATUM >= '''+M2.Text+''' '+
		    'AND KS_DATUM <= '''+M3.Text+''' '+szur140;
  end;
	SQL_ToltoFo(GetOrderBy(true));

end;

procedure TKoltsegFmDlg.BitBtn4Click(Sender: TObject);
begin
	EgyebDlg.Calendarbe(M3, True);
	// Az intervallum megad�sa
	alapstr	:= 'SELECT * FROM KOLTSEG WHERE KS_DATUM >= '''+M2.Text+''' '+
		'AND KS_DATUM <= '''+M3.Text+''' '+szur140;
	SQL_ToltoFo(GetOrderBy(true));

end;

procedure TKoltsegFmDlg.BitBtn10Click(Sender: TObject);
begin
	// Let�r�lj�k a felt�teleket
	alapstr	:= 'SELECT * FROM KOLTSEG '+szur140;
	SQL_ToltoFo(GetOrderBy(true));

end;

procedure TKoltsegFmDlg.Query1AfterOpen(DataSet: TDataSet);
var
  i,j: integer;
  sql:string;
  ertek: integer;
begin
  i:=pos('where',LowerCase( Query1.SQL.Text));
  j:=pos('order',LowerCase( Query1.SQL.Text));
  Query4.Close;
  Query4.SQL.Clear;
  //Query4.SQL.Add('Select sum(ks_ertek) ertek '+copy(Query1.SQL.Text,i,j-i-1);
  sql:='Select sum(ks_ertek) ertek from koltseg '+copy(Query1.SQL.Text,i,j-i-1);
  //ShowMessage(sql);

  if sql<>'' then begin
     Query4.SQL.Add(sql);
     Query4.Open;
     Query4.First;
   //  ertek:= Query4.fieldbyname('ertek').AsInteger;
  end;
 // Query4.Close;
end;

end.

