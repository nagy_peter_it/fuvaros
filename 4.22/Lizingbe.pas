unit Lizingbe;

interface

uses
	SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
	Forms, Dialogs, StdCtrls, Buttons, Mask, DB, DBTables, J_ALFORM, ADODB;

type
	TLizingbeDlg = class(TJ_AlformDlg)
	BitElkuld: TBitBtn;
	BitKilep: TBitBtn;
	Label2: TLabel;
	Label7: TLabel;
	MaskEdit2: TMaskEdit;
	MaskEdit7: TMaskEdit;
	Label8: TLabel;
	Label9: TLabel;
	Label12: TLabel;
  MaskEdit10: TMaskEdit;
	 CBVnem: TComboBox;
	 BitBtn7: TBitBtn;
	 Query1: TADOQuery;
	 Label1: TLabel;
	 MaskEdit1: TMaskEdit;
    Label3: TLabel;
    MaskEdit3: TMaskEdit;
    BitBtn1: TBitBtn;
	procedure BitKilepClick(Sender: TObject);
	procedure FormCreate(Sender: TObject);
	procedure Tolto(cim : string; teko, rsz, ossz, vnem : string);
	procedure BitElkuldClick(Sender: TObject);
  procedure MaskEditClick(Sender: TObject);
  procedure MaskEditExit(Sender: TObject);
	procedure MaskEditKeyPress(Sender: TObject; var Key: Char);
	 procedure FormKeyDown(Sender: TObject; var Key: Word;
	   Shift: TShiftState);
	 procedure MaskEdit10Exit(Sender: TObject);
	 procedure BitBtn7Click(Sender: TObject);
    procedure MaskEdit3Exit(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
	private
		rendszam	: string;
	public
	end;

var
	LizingbeDlg: TLizingbeDlg;

implementation

uses
	Egyeb, J_SQL, Kozos;
{$R *.DFM}

procedure TLizingbeDlg.BitKilepClick(Sender: TObject);
begin
	ret_kod := '';
	Close;
end;

procedure TLizingbeDlg.FormCreate(Sender: TObject);
begin
	EgyebDlg.SetADOQueryDatabase(Query1);
	SetMaskEdits([MaskEdit2]);
	ret_kod 	:= '';
	Query_Run(Query1, 'SELECT SZ_ALKOD FROM SZOTAR WHERE SZ_FOKOD = ''110'' ');
	CBVnem.Clear;
	while not Query1.Eof do begin
		CBVnem.Items.Add(Query1.FieldByName('SZ_ALKOD').AsString);
		Query1.Next;
	end;
end;

procedure TLizingbeDlg.Tolto(cim : string; teko, rsz, ossz, vnem : string);
var
  i	: integer;

begin
	ret_kod 		:= teko;
	Caption 		:= cim;
	rendszam		:= rsz;
	MaskEdit2.Text 	:= teko;
	MaskEdit10.Text := EgyebDlg.MaiDatum;
	for i := 0 to CBVnem.Items.Count - 1 do begin
		if CBVnem.Items[i] = vnem then begin
			CBVnem.ItemIndex	:= i;
		end;
	end;
	if ret_kod <> '' then begin		{M�dos�t�s}
		if Query_Run (Query1, 'SELECT * FROM LIZING WHERE LZ_LZKOD = '+teko,true) then begin
			MaskEdit2.Text 	:= teko;
			MaskEdit7.Text 	:= SzamString(Query1.FieldByName('LZ_ERTEK').AsFloat,12,2);
			MaskEdit10.Text := Query1.FieldByName('LZ_DATUM').AsString;
			MaskEdit3.Text 	:= Query1.FieldByName('LZ_BEDAT').AsString;
			MaskEdit1.Text 	:= Query1.FieldByName('LZ_MEGJE').AsString;
			for i := 0 to CBVnem.Items.Count - 1 do begin
				if CBVnem.Items[i] = Query1.FieldByName('LZ_VANEM').AsString then begin
					CBVnem.ItemIndex	:= i;
				end;
			end;
		end;
	end;
end;

procedure TLizingbeDlg.BitElkuldClick(Sender: TObject);
begin
	if not DatumExit(MaskEdit10, true) then begin
		Exit;
	end;
	if MaskEdit10.Text > EgyebDlg.MaiDatum then begin
		NoticeKi('�rv�nytelen d�tum (nagyobb a main�l)!');
		Exit;
	end;
	if ret_kod = '' then begin
		ret_kod		:= Format('%5.5d',[GetNextCode('LIZING', 'LZ_LZKOD', 1, 0)]);
		Query_Run ( Query1, 'INSERT INTO LIZING (LZ_LZKOD) VALUES (' +ret_kod+ ' )',true);
	end;
	{A r�gi rekord m�dos�t�sa}
	Query_Update (Query1, 'LIZING',
		[
		'LZ_RENSZ', 	''''+rendszam+'''',
		'LZ_VANEM', 	''''+CBVnem.Text+'''',
		'LZ_ERTEK', SqlSzamString(StringSzam(MaskEdit7.Text),12,2),
		'LZ_DATUM', ''''+MaskEdit10.Text+'''',
		'LZ_BEDAT', ''''+MaskEdit3.Text+'''',
		'LZ_MEGJE', ''''+MaskEdit1.Text+''''
		],
		' WHERE LZ_LZKOD = '+ret_kod);
	Query1.Close;
	Close;
end;

procedure TLizingbeDlg.MaskEditClick(Sender: TObject);
begin
	with Sender as TMaskEdit do begin
		SelectAll;
	end;
end;

procedure TLizingbeDlg.MaskEditExit(Sender: TObject);
begin
	with Sender as TMaskEdit do begin
		Text := StrSzamStr(Text,12,2);
	end;
end;

procedure TLizingbeDlg.MaskEditKeyPress(Sender: TObject; var Key: Char);
begin
  with Sender as TMaskEdit do begin
	Key := EgyebDlg.Jochar(Text,Key,12,2);
  end;
end;

procedure TLizingbeDlg.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
	EgyebDlg.FormReturn(Key);
end;

procedure TLizingbeDlg.MaskEdit10Exit(Sender: TObject);
begin
	DatumExit(MaskEdit10);
end;

procedure TLizingbeDlg.BitBtn7Click(Sender: TObject);
begin
	EgyebDlg.Calendarbe(MaskEdit10);
end;

procedure TLizingbeDlg.MaskEdit3Exit(Sender: TObject);
begin
	DatumExit(MaskEdit3);
end;

procedure TLizingbeDlg.BitBtn1Click(Sender: TObject);
begin
	EgyebDlg.Calendarbe(MaskEdit3);
end;

end.
