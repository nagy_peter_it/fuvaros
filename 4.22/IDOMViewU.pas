unit IDOMViewU;

{
  A generic viewer for XML documents.
  Based on Delphi DOM framework interfaces.

  Copyright © Keith Wood (kbwood@iprimus.com.au)
  Written 24 September, 2002.
}

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, Menus, ComCtrls, Grids, ImgList, xmldom,
{$IFDEF VER150}  { Delphi 7 }
  xercesxmldom,
{$ENDIF}    
  msxmldom; // , oxmldom;

type
  TfrmXMLViewer = class(TForm)
    pgcMain: TPageControl;
      tshStructure: TTabSheet;
        trvXML: TTreeView;
        pgcDetails: TPageControl;
          tshDocument: TTabSheet;
            Label1: TLabel;
            edtDocType: TEdit;
            cbxStandAlone: TCheckBox;
            Label2: TLabel;
            edtPublicId: TEdit;
            Label3: TLabel;
            edtSystemId: TEdit;
            Label4: TLabel;
            edtVersion: TEdit;
            Label5: TLabel;
            edtEncoding: TEdit;
            Label6: TLabel;
            stgEntities: TStringGrid;
            Label7: TLabel;
            stgNotations: TStringGrid;
          tshElement: TTabSheet;
            pnlNames: TPanel;
              Label8: TLabel;
              edtURI: TEdit;
              Label9: TLabel;
              edtLocalName: TEdit;
            stgAttributes: TStringGrid;
          tshText: TTabSheet;
            lblNodeType: TLabel;
            memText: TMemo;
      tshSource: TTabSheet;
        memSource: TRichEdit;
    mnuMain: TMainMenu;
      mniFile: TMenuItem;
        mniOpen: TMenuItem;
        mniSep1: TMenuItem;
        mniVendor: TMenuItem;
        mniSep2: TMenuItem;
        mniPreserveWhitespace: TMenuItem;
        mniResolveExternals: TMenuItem;
        mniValidateOnParse: TMenuItem;
        mniSep3: TMenuItem;
        mniExit: TMenuItem;
      mniView: TMenuItem;
        mniExpandAll: TMenuItem;
        mniCollapseAll: TMenuItem;
        mniSep4: TMenuItem;
        mniViewSource: TMenuItem;
    imlXML: TImageList;
    dlgOpen: TOpenDialog;
    splHoriz: TSplitter;
    Walkin1: TMenuItem;
    Memo1: TMemo;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure mniOpenClick(Sender: TObject);
    procedure mniVendorClick(Sender: TObject);
    procedure mniOptionClick(Sender: TObject);
    procedure mniExitClick(Sender: TObject);
    procedure mniExpandAllClick(Sender: TObject);
    procedure mniCollapseAllClick(Sender: TObject);
    procedure mniViewSourceClick(Sender: TObject);
    procedure trvXMLChange(Sender: TObject; Node: TTreeNode);
    procedure Walkin1Click(Sender: TObject);
  private
    FList: TList;
    FXMLDoc: IDOMDocument;
    procedure ClearTree;
    procedure WalkTree(ActRootNode: TTreeNode);
    procedure ReadTree(RootNode: TTreeNode);
    procedure ReadSubtree(ActRootNode: TTreeNode; SubTreeName: string);

  public
    procedure LoadDoc(Filename: string);
    function GetRootNode: TTreeNode;
    function GotoChild(ActRootNode: TTreeNode; SearchText: string):TTreeNode ;
    function GetChildValue(ActRootNode: TTreeNode; ChildText: string): string ;
    function GetFirstChild(ActRootNode: TTreeNode):TTreeNode;
    function GetNextChild(ActRootNode, ActChildNode: TTreeNode):TTreeNode;
    function GetNodeAttrib(ActNode: TTreeNode; AttribName: string): string;
  end;

var
  frmXMLViewer: TfrmXMLViewer;

implementation

{$R *.DFM}

resourcestring
  AttributeDesc   = 'Attribute';
  CommentDesc     = 'Comment';
  DTDDesc         = 'DTD';
  EncodingAttr    = 'encoding';
  EntityRefDesc   = 'Entity Reference';
  InstructionDesc = 'Processing Instruction';
  NameDesc        = 'Name';
  NoDOMError      = 'Couldn''t create the DOM';
  NoLoadError     = 'Couldn''t load the document:'#13 +
                    'Line: %d  Column: %d'#13 +
                    '%s';
  NotationDesc    = 'Notation';
  PublicDesc      = 'Public Id';
  StandAloneAttr  = 'standalone';
  SystemDesc      = 'System Id';
  TextDesc        = 'Text/CDATA Section';
  ValueDesc       = 'Value';
  VersionAttr     = 'version';
  XMLValue        = 'XML';
  XMLDocDesc      = 'XML Document';
  YesValue        = 'yes';

type

{ TNodeInfo -------------------------------------------------------------------}

  { XML node types }
  TNodeType = (ntDocument, ntElement, ntComment, ntInstruction,
    ntText, ntCData, ntEntityRef);

  { XML Node.
    Elements have a name and/or a value depending on their type.
    Attributes are name=value pairs in a string list.
  }
  TNodeInfo = class(TObject)
  private
    FAttributes: TStringList;
    FLocalName: string;
    FName: string;
    FNamespaceURI: string;
    FNodeType: TNodeType;
    FValue: string;
  public
    constructor Create(NodeType: TNodeType;
      const Name, NamespaceURI, LocalName, Value: string; Attributes: TStrings);
    destructor Destroy; override;
    property Attributes: TStringList read FAttributes write FAttributes;
    property LocalName: string read FLocalName write FLocalName;
    property Name: string read FName write FName;
    property NamespaceURI: string read FNamespaceURI write FNamespaceURI;
    property NodeType: TNodeType read FNodeType write FNodeType;
    property Value: string read FValue write FValue;
  end;

{ Initialisation }
constructor TNodeInfo.Create(NodeType: TNodeType;
  const Name, NamespaceURI, LocalName, Value: string; Attributes: TStrings);
begin
  inherited Create;
  FLocalName    := LocalName;
  FName         := Name;
  FNamespaceURI := NamespaceURI;
  FNodeType     := NodeType;
  FValue        := Value;
  FAttributes   := TStringList.Create;
  if Assigned(Attributes) then
    FAttributes.Assign(Attributes);
end;

{Release resources }
destructor TNodeInfo.Destroy;
begin
  Attributes.Free;
  inherited Destroy;
end;

{ TfrmXMLViewer ---------------------------------------------------------------}

{ Initialisation - try to load an XML document on start up }
procedure TfrmXMLViewer.FormCreate(Sender: TObject);
var
  Index, DefIndex: Integer;
  MenuItem: TMenuItem;
begin
  FList              := TList.Create;
  dlgOpen.InitialDir := ExtractFilePath(Application.ExeName);
  with stgEntities do
  begin
    Cells[0, 0] := NameDesc;
    Cells[1, 0] := PublicDesc;
    Cells[2, 0] := SystemDesc;
    Cells[3, 0] := NotationDesc;
  end;
  with stgNotations do
  begin
    Cells[0, 0] := NameDesc;
    Cells[1, 0] := PublicDesc;
    Cells[2, 0] := SystemDesc;
  end;
  with stgAttributes do
  begin
    Cells[0, 0] := AttributeDesc;
    Cells[1, 0] := ValueDesc;
  end;
  // Load list of DOM vendors
  DefIndex := 0;
  for Index := 0 to DOMVendors.Count - 1 do
    begin
      MenuItem            := TMenuItem.Create(Self);
      MenuItem.Caption    := DOMVendors[Index].Description;
      MenuItem.GroupIndex := 1;
      MenuItem.OnClick    := mniVendorClick;
      MenuItem.RadioItem  := True;
      if MenuItem.Caption = DefaultDOMVendor then
        DefIndex := Index;
      mniVendor.Add(MenuItem);
    end;
  mniVendor.Items[DefIndex].Click;
  // And parse a document (perhaps)
  // --- Ezt kivettem, elkapta a FUVAR_TIMER command-line paraméterét --- //
  // if ParamStr(1) <> '' then
  //   LoadDoc(ParamStr(1));
end;

{ Release resources }
procedure TfrmXMLViewer.FormDestroy(Sender: TObject);
begin
  ClearTree;
  FList.Free;
end;

{ Release resources }
procedure TfrmXMLViewer.ClearTree;
var
  Index: Integer;
begin
  for Index := 0 to FList.Count - 1 do
    TNodeInfo(FList[Index]).Free;
  FList.Clear;
  trvXML.OnChange := nil;
  trvXML.Items.Clear;
  trvXML.OnChange := trvXMLChange;
end;

{ Load an XML document }
procedure TfrmXMLViewer.LoadDoc(Filename: string);

  { Initialise document-wide details for display }
  procedure InitDocumentDetails;
  begin
    // Clear entries
    edtDocType.Text       := '';
    edtPublicId.Text      := '';
    edtSystemId.Text      := '';
    edtVersion.Text       := '';
    edtEncoding.Text      := '';
    cbxStandAlone.Checked := False;
    with stgEntities do
    begin
      RowCount := 2;
      Rows[1].Clear;
    end;
    with stgNotations do
    begin
      RowCount := 2;
      Rows[1].Clear;
    end;
    ClearTree;
  end;

  { Add a TNodeInfo to the tree view }
  function AddNodeInfo(Parent: TTreeNode; Name: string; NodeInfo: TNodeInfo):
    TTreeNode;
  begin
    FList.Add(NodeInfo);
    Result := trvXML.Items.AddChildObject(Parent, Name, NodeInfo);
    with Result do
    begin
      ImageIndex    := Ord(NodeInfo.NodeType);
      SelectedIndex := ImageIndex;
    end;
  end;

  { Add current node to the treeview and then recurse through children }
  procedure AddNodeToTree(Node: IDOMNode; TreeParent: TTreeNode);
  var
    Index: Integer;
    DisplayName: string;
    NewNode: TTreeNode;
    Attribs: TStringList;
    XMLProlog: IDOMXMLProlog;
  begin
    // Generate name for display in the tree
    if Node.nodeType in [TEXT_NODE, COMMENT_NODE, CDATA_SECTION_NODE] then
    begin
      // --- shortened values ...   ---- //
      // if Length(Node.nodeValue) > 20 then
      //  DisplayName := Copy(Node.nodeValue, 1, 17) + '...'
      // else
      //  DisplayName := Node.nodeValue;
      // ------------------------------- //
      DisplayName := Node.nodeValue;
    end
    else
      DisplayName := Node.nodeName;
    // Create storage for later display of node values
    case Node.nodeType of
      ELEMENT_NODE:
        begin
          Attribs := TStringList.Create;
          try
            for Index := 0 to Node.attributes.length - 1 do
              with Node.attributes[Index] do
                Attribs.Values[nodeName] := nodeValue;
            NewNode := AddNodeInfo(TreeParent, DisplayName,
              TNodeInfo.Create(ntElement, Node.nodeName,
              Node.namespaceURI, Node.localName, '', Attribs));
          finally
            Attribs.Free;
          end;
        end;
      TEXT_NODE:
        with Node as IDOMText do
          NewNode := AddNodeInfo(TreeParent, DisplayName,
            TNodeInfo.Create(ntText, '', '', '', data, nil));
      CDATA_SECTION_NODE:
        with Node as IDOMCDATASection do
          NewNode := AddNodeInfo(TreeParent, DisplayName,
            TNodeINfo.Create(ntCData, '', '', '', data, nil));
      ENTITY_REFERENCE_NODE:
        NewNode := AddNodeInfo(TreeParent, DisplayName,
          TNodeInfo.Create(ntEntityRef, Node.nodeName, '', '', '', nil));
      PROCESSING_INSTRUCTION_NODE:
        with Node as IDOMProcessingInstruction do
          NewNode := AddNodeInfo(TreeParent, DisplayName,
            TNodeInfo.Create(ntInstruction, target, '', '', data, nil));
      COMMENT_NODE:
        with Node as IDOMComment do
          NewNode := AddNodeInfo(TreeParent, DisplayName,
            TNodeInfo.Create(ntComment, '', '', '', data, nil));
      DOCUMENT_NODE:
        begin
          NewNode := AddNodeInfo(TreeParent, XMLDocDesc,
            TNodeInfo.Create(ntDocument, XMLDocDesc, '', '', '', nil));
          if Supports(Node, IDOMXMLProlog, XMLProlog) then
          begin
            edtVersion.Text       := XMLProlog.Version;
            edtEncoding.Text      := XMLProlog.Encoding;
            cbxStandAlone.Checked := (XMLProlog.Standalone = YesValue);
          end;
        end;
      DOCUMENT_TYPE_NODE:
        with Node as IDOMDocumentType do
        begin
          edtDocType.Text := name;
          NewNode := AddNodeInfo(TreeParent, DTDDesc,
            TNodeInfo.Create(ntEntityRef, DTDDesc, '', '', '', nil));
        end;
      ENTITY_NODE:
        with (Node as IDOMEntity), stgEntities do
          if notationName <> '' then
          begin
            // Unparsed entity
            if Cells[0, RowCount - 1] <> '' then
              RowCount := RowCount + 1;
            Cells[0, RowCount - 1] := nodeName;
            Cells[1, RowCount - 1] := publicId;
            Cells[2, RowCount - 1] := systemId;
            Cells[3, RowCount - 1] := notationName;
          end
          else
            // Parsed entity
            NewNode := AddNodeInfo(TreeParent, DisplayName,
              TNodeInfo.Create(ntEntityRef, nodeName, '', '', '', nil));
      NOTATION_NODE:
        with (Node as IDOMNotation), stgNotations do
        begin
          if Cells[0, RowCount - 1] <> '' then
            RowCount := RowCount + 1;
          Cells[0, RowCount - 1] := nodeName;
          Cells[1, RowCount - 1] := publicId;
          Cells[2, RowCount - 1] := systemId;
        end;
    end;
    // And recurse through any children
    if Node.hasChildNodes then
      for Index := 0 to Node.childNodes.length - 1 do
        AddNodeToTree(Node.childNodes[Index], NewNode);
  end;

var
  ParseOptions: IDOMParseOptions;
  Persister: IDOMPersist;
begin
  Screen.Cursor := crHourglass;
  try
    pgcDetails.ActivePage := tshDocument;
    // Initialise document-wide details for display
    InitDocumentDetails;
    // Load the source document
    memSource.Lines.LoadFromFile(Filename);
    dlgOpen.Filename := Filename;
    trvXML.Items.BeginUpdate;
    try
      // Parse the document
      if Supports(FXMLDoc, IDOMParseOptions, ParseOptions) then
        with ParseOptions do
        begin
          preserveWhiteSpace := mniPreserveWhitespace.Checked;
          resolveExternals   := mniResolveExternals.Checked;
          validate           := mniValidateOnParse.Checked;
        end;
      try
        if Supports(FXMLDoc, IDOMPersist, Persister) then
        begin
          if not Persister.load(Filename) then
            MessageDlg('Error during loading!', mtError, [mbOK], 0);
        end
        else
          MessageDlg('Document does not support loading!', mtError, [mbOK], 0);
      except on E: EDOMParseError do
        MessageDlg(Format(NoLoadError, [E.Line, E.LinePos, E.Reason]),
          mtError, [mbOK], 0);
      end;
      edtSystemId.Text := Filename;

      // Add the structure to the tree view
      AddNodeToTree(FXMLDoc, nil);
      trvXML.Items[0].Expand(False);
    finally
      trvXML.Items.EndUpdate;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

{ Select a file to open }
procedure TfrmXMLViewer.mniOpenClick(Sender: TObject);
begin
  with dlgOpen do
    if Execute then
      LoadDoc(Filename);
end;

{ Select a new DOM vendor }
procedure TfrmXMLViewer.mniVendorClick(Sender: TObject);

  function StripAccelerator(Value: string): string;
  begin
    Result := Value;
    Delete(Result, Pos('&', Result), 1);
  end;

begin
  with Sender as TMenuItem do
  begin
    Checked      := True;
    FXMLDoc      :=
      GetDOM(StripAccelerator(Caption)).createDocument('', '', nil);
    Self.Caption :=
      'XML Viewer (DOM Framework - ' + StripAccelerator(Caption) + ')';
  end;
  ClearTree;
  memSource.Lines.Clear;
end;

{ Toggle parser property }
procedure TfrmXMLViewer.mniOptionClick(Sender: TObject);
begin
  with Sender as TMenuItem do
    Checked := not Checked;
end;

{ Exit the application }
procedure TfrmXMLViewer.mniExitClick(Sender: TObject);
begin
  Close;
end;

{ Expand all nodes below the current one }
procedure TfrmXMLViewer.mniExpandAllClick(Sender: TObject);
begin
  if Assigned(trvXML.Selected) then
    trvXML.Selected.Expand(True);
end;

{ Collapse all nodes below the current one }
procedure TfrmXMLViewer.mniCollapseAllClick(Sender: TObject);
begin
  if Assigned(trvXML.Selected) then
    trvXML.Selected.Collapse(True);
end;

{ Toggle between structure and source }
procedure TfrmXMLViewer.mniViewSourceClick(Sender: TObject);
begin
  mniViewSource.Checked := not mniViewSource.Checked;
  if mniViewSource.Checked then
    pgcMain.ActivePage := tshSource
  else
    pgcMain.ActivePage := tshStructure;
end;

{ Display details for the selected XML element }
procedure TfrmXMLViewer.trvXMLChange(Sender: TObject; Node: TTreeNode);
var
  Index: Integer;
begin
  with TNodeInfo(trvXML.Selected.Data) do
    case NodeType of
      ntDocument:
        // Show document details, including entities and notations
        pgcDetails.ActivePage := tshDocument;
      ntElement:
        begin
          // Show element details, including attributes
          pgcDetails.ActivePage := tshElement;
          edtURI.Text           := NamespaceURI;
          edtLocalName.Text     := LocalName;
          with stgAttributes do
          begin
            if Attributes.Count = 0 then
              RowCount := 2
            else
              RowCount := Attributes.Count + 1;
            Rows[1].Clear;
            for Index := 0 to Attributes.Count - 1 do
            begin
              Cells[0, Index + 1] := Attributes.Names[Index];
              Cells[1, Index + 1] := Attributes.Values[Attributes.Names[Index]];
            end;
          end;
        end;
      else
        begin
          // Show details for other nodes - text type
          pgcDetails.ActivePage := tshText;
          memText.Lines.Text    := Value;
          case NodeType of
            ntComment:     lblNodeType.Caption := CommentDesc;
            ntInstruction: lblNodeType.Caption := InstructionDesc;
            ntEntityRef:   lblNodeType.Caption := EntityRefDesc;
            else           lblNodeType.Caption := TextDesc;
          end;
        end;
    end;
end;

procedure TfrmXMLViewer.Walkin1Click(Sender: TObject);
var
   i, RootID: integer;
   Found: boolean;
begin
   with trvXML do begin
      i:=0;
      Found:=False;
      while (i<Items.Count-1) and (not Found) do begin
         if Items[i].Parent=nil then begin
            Found:=True;
            RootID:=i;
            ShowMessage(Items[i].Text);
            end;  // if
         Inc(i);
         end;  // while
      memo1.Clear;
      ReadTree(Items[RootID]);

         // Items[i].getFirstChild;

      end; // with
end;

function TfrmXMLViewer.GetRootNode: TTreeNode;
var
   i, RootID: integer;
   Found: boolean;
begin
   with trvXML do begin
      i:=0;
      Found:=False;
      while (i<Items.Count-1) and (not Found) do begin
         if Items[i].Parent=nil then begin
            Found:=True;
            RootID:=i;
            // ShowMessage(Items[i].Text);
            end;  // if
         Inc(i);
         end;  // while
      Result:=Items[RootID];
      end; // with
end;

procedure TfrmXMLViewer.ReadTree(RootNode: TTreeNode);
var
  CsomagKeresNode, NextNode, tempChildNode: TTreeNode;
begin
  CsomagKeresNode:=GotoChild(RootNode, 'khr:khrCsomagKeres');
  if CsomagKeresNode<>nil then begin
      tempChildNode:=GotoChild(CsomagKeresNode, 'khr:fej');
      if tempChildNode<>nil then
          ReadSubtree(tempChildNode, 'fej');
      tempChildNode:=GotoChild(CsomagKeresNode, 'khr:lakossagiSajatLekerdezes');
      if tempChildNode<>nil then
          ReadSubtree(tempChildNode, 'lakossagiSajatLekerdezes');
      end;  // if
end;

function TfrmXMLViewer.GotoChild(ActRootNode: TTreeNode; SearchText: string):TTreeNode ;
var
  NextNode: TTreeNode;
  Found: boolean;
begin
  Found:=False;
  NextNode:=ActRootNode.getFirstChild;
  while (NextNode<>nil) and (not Found) do begin
    if NextNode.Text=SearchText then begin
        Found:=True;
        Result:=NextNode;
        end;  // if
    NextNode:=ActRootNode.getNextChild(NextNode);
    end;  // while
  if not Found then Result:=nil;
end;

function TfrmXMLViewer.GetFirstChild(ActRootNode: TTreeNode):TTreeNode;
begin
  Result:=ActRootNode.getFirstChild;
end;

function TfrmXMLViewer.GetNextChild(ActRootNode, ActChildNode: TTreeNode):TTreeNode;
begin
  Result:=ActRootNode.getNextChild(ActChildNode);
end;

function TfrmXMLViewer.GetChildValue(ActRootNode: TTreeNode; ChildText: string): string ;
var
  tempChildNode, tempTextNode: TTreeNode;
  Found: boolean;
begin
  tempChildNode:=GotoChild(ActRootNode, ChildText);
  if tempChildNode<>nil then begin
     tempTextNode:=tempChildNode.getFirstChild;
     if tempTextNode<>nil then
        Result:=tempTextNode.Text;
     end;  // if
end;

procedure TfrmXMLViewer.ReadSubtree(ActRootNode: TTreeNode; SubTreeName: string);
var
  NextNode, tempChildNode: TTreeNode;
  S: string;
begin
  if SubTreeName='fej' then begin
     S:=GetChildValue(ActRootNode, 'khr:raszKhrAzonosito');
     memo1.Lines.Add('raszKhrAzonosito='+S);
     S:=GetChildValue(ActRootNode, 'khr:felhasznaloKhrAzonosito');
     memo1.Lines.Add('felhasznaloKhrAzonosito='+S);
     end;  // fej
end;

function TfrmXMLViewer.GetNodeAttrib(ActNode: TTreeNode; AttribName: string): string;
var
  i: integer;
  Found: boolean;
  Res: string;
begin
  with TNodeInfo(ActNode.Data) do begin
    if NodeType = ntElement then begin
       i:=0;
       Found := False;
       while (i < Attributes.Count) and not Found do begin
          if Attributes.Names[i]=AttribName then begin
              Res:= Attributes.Values[Attributes.Names[i]];
              Found:= True;
              end; // if
          Inc(i);
          end;  // while
       end  // if
    else begin
      Res:='';
      end;  // else
    end;  // with
  if Found then Result:= Res;
end;

procedure TfrmXMLViewer.WalkTree(ActRootNode: TTreeNode);
var
  NextNode, tempChildNode: TTreeNode;
begin
  // ... itt lehet hogy hiányzik valami
  if (tempChildNode.getFirstChild=nil) and (ActRootNode.Text<>'XML Document')
    // and ((tempChildNode.Data as TNodeInfo).NodeType=TEXT_NODE)
    then begin
     memo1.Lines.Add('LEAF: '+ActRootNode.Text+'='+tempChildNode.Text);
     end
  else begin
     memo1.Lines.Add('NODE: '+ActRootNode.Text);
     NextNode:=ActRootNode.getFirstChild;
     while NextNode<>nil do begin
        WalkTree(NextNode);
        NextNode:=ActRootNode.getNextChild(NextNode);
        end;  // while
     end; // else
end;

{procedure TfrmXMLViewer.WalkTree(ActRootNode: TTreeNode);
var
  NextNode, tempChildNode: TTreeNode;
begin
  tempChildNode:=ActRootNode.getFirstChild;
  if (tempChildNode.getFirstChild=nil) and (ActRootNode.Text<>'XML Document')
    // and ((tempChildNode.Data as TNodeInfo).NodeType=TEXT_NODE)
    then begin
     memo1.Lines.Add('LEAF: '+ActRootNode.Text+'='+tempChildNode.Text);
     end
  else begin
     memo1.Lines.Add('NODE: '+ActRootNode.Text);
     NextNode:=ActRootNode.getFirstChild;
     while NextNode<>nil do begin
        WalkTree(NextNode);
        NextNode:=ActRootNode.getNextChild(NextNode);
        end;  // while
     end; // else
end;}

end.
