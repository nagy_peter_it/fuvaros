unit Ureskmlis;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, QRCtrls, QuickRpt, ExtCtrls, DB, ADODB;

type
  TUreskmlisDlg = class(TForm)
    Rep: TQuickRep;
    QRBand3: TQRBand;
    QRShape5: TQRShape;
    QRShape3: TQRShape;
    QRLSzamla: TQRLabel;
    QRLabel17: TQRLabel;
    QRLabel6: TQRLabel;
    QRSysData1: TQRSysData;
    QRLabel7: TQRLabel;
    QRSysData4: TQRSysData;
    QRLabel3: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel12: TQRLabel;
    QRBand1: TQRBand;
    QRBand2: TQRBand;
    QRDBText1: TQRDBText;
    QRDBText2: TQRDBText;
    QRDBText3: TQRDBText;
    QRShape2: TQRShape;
    QRLabel1: TQRLabel;
    QRShape4: TQRShape;
    QRLabel2: TQRLabel;
    QRDBText4: TQRDBText;
    QRShape6: TQRShape;
    QRLabel5: TQRLabel;
    QRDBText5: TQRDBText;
    QRShape7: TQRShape;
    QRLabel8: TQRLabel;
    QRDBText6: TQRDBText;
    QRBand5: TQRBand;
    QRGroup1: TQRGroup;
    QRDBText7: TQRDBText;
    QRDBText8: TQRDBText;
    Query2: TADOQuery;
    Query2gk_gkkat: TStringField;
    Query2OSSZ_KM: TBCDField;
    Query2OSSZ_U_KM: TIntegerField;
    Query2OSSZ_UB_KM: TIntegerField;
    Query2OSSZ_UK_KM: TIntegerField;
    Query2ja_rendsz: TStringField;
    QRLabel9: TQRLabel;
    QRShape1: TQRShape;
    QRLabel10: TQRLabel;
    QRLabel11: TQRLabel;
    QRLabel13: TQRLabel;
    QRLabel14: TQRLabel;
    QRShape8: TQRShape;
    QRLabel15: TQRLabel;
    QRLabel16: TQRLabel;
    QRShape9: TQRShape;
    QRLabel18: TQRLabel;
    QRLabel19: TQRLabel;
    QRShape10: TQRShape;
    QRLabel20: TQRLabel;
    QRLabel21: TQRLabel;
    QRLabel22: TQRLabel;
    QRLabel23: TQRLabel;
    QRLabel24: TQRLabel;
    QRShape11: TQRShape;
    QRShape12: TQRShape;
    QRLabel25: TQRLabel;
    QRLabel26: TQRLabel;
    QRLabel27: TQRLabel;
    QRLabel28: TQRLabel;
    QRLabel29: TQRLabel;
    QRLabel30: TQRLabel;
    QRLabel31: TQRLabel;
    QRLabel32: TQRLabel;
    procedure QRBand3BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure RepBeforePrint(Sender: TCustomQuickRep;
      var PrintReport: Boolean);
    procedure QRBand5AfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRBand1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBand5BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBand2BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  UreskmlisDlg: TUreskmlisDlg;
  okm,oukm,oubkm,oukkm: integer;
  ookm,ooukm,ooubkm,ooukkm: integer;
  usz,ubsz,uksz: integer    ;

implementation

uses Ureskmnyil, Math;

{$R *.dfm}

procedure TUreskmlisDlg.QRBand3BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  QRLabel17.Caption:= UreskmnyilDlg.MD1.Text+' - '+UreskmnyilDlg.MD2.Text;
end;

procedure TUreskmlisDlg.RepBeforePrint(Sender: TCustomQuickRep;
  var PrintReport: Boolean);
begin
  okm:=0;
  oukm:=0;
  oubkm:=0;
  oukkm:=0;
  ookm:=0;
  ooukm:=0;
  ooubkm:=0;
  ooukkm:=0;
end;

procedure TUreskmlisDlg.QRBand5AfterPrint(Sender: TQRCustomBand;
  BandPrinted: Boolean);
begin
  okm:=0;
  oukm:=0;
  oubkm:=0;
  oukkm:=0;
end;

procedure TUreskmlisDlg.QRBand1BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  okm:=okm+Query2OSSZ_KM.AsInteger;
  oukm:=oukm+Query2OSSZ_U_KM.AsInteger;
  oubkm:=oubkm+Query2OSSZ_UB_KM.AsInteger;
  oukkm:=oukkm+Query2OSSZ_UK_KM.AsInteger;

  if Query2OSSZ_KM.AsInteger<>0 then
  begin
    QRLabel16.Caption:=IntToStr( Trunc(RoundTo( Query2OSSZ_U_KM.AsInteger / Query2OSSZ_KM.AsInteger * 100  ,0)))+' %';
    QRLabel19.Caption:=IntToStr( Trunc(RoundTo( Query2OSSZ_UB_KM.AsInteger / Query2OSSZ_KM.AsInteger * 100  ,0)))+' %';
    QRLabel21.Caption:=IntToStr( Trunc(RoundTo( Query2OSSZ_UK_KM.AsInteger / Query2OSSZ_KM.AsInteger * 100  ,0)))+' %';
  end
  else
  begin
    QRLabel16.Caption:='';
    QRLabel19.Caption:='';
    QRLabel21.Caption:='';
  end;

  PrintBand:=UreskmnyilDlg.CheckBox1.Checked;
end;

procedure TUreskmlisDlg.QRBand5BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  QRLabel10.Caption:=IntToStr(okm);
  QRLabel11.Caption:=IntToStr(oukm);
  QRLabel13.Caption:=IntToStr(oubkm);
  QRLabel14.Caption:=IntToStr(oukkm);

  if okm<>0 then
  begin
    QRLabel24.Caption:=IntToStr( Trunc(RoundTo( oukm  / okm * 100  ,0)))+' %';
    QRLabel22.Caption:=IntToStr( Trunc(RoundTo( oubkm / okm * 100  ,0)))+' %';
    QRLabel23.Caption:=IntToStr( Trunc(RoundTo( oukkm / okm * 100  ,0)))+' %';
  end
  else
  begin
    QRLabel24.Caption:='';
    QRLabel22.Caption:='';
    QRLabel23.Caption:='';
  end;
  ookm:=ookm+okm;
  ooukm:=ooukm+oukm;
  ooubkm:=ooubkm+oubkm;
  ooukkm:=ooukkm+oukkm;

end;

procedure TUreskmlisDlg.QRBand2BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  QRLabel26.Caption:=IntToStr(ookm);
  QRLabel27.Caption:=IntToStr(ooukm);
  QRLabel28.Caption:=IntToStr(ooubkm);
  QRLabel29.Caption:=IntToStr(ooukkm);

  if ookm<>0 then
  begin
    QRLabel32.Caption:=IntToStr( Trunc(RoundTo( ooukm  / ookm * 100  ,0)))+' %';
    QRLabel30.Caption:=IntToStr( Trunc(RoundTo( ooubkm / ookm * 100  ,0)))+' %';
    QRLabel31.Caption:=IntToStr( Trunc(RoundTo( ooukkm / ookm * 100  ,0)))+' %';
  end
  else
  begin
    QRLabel32.Caption:='';
    QRLabel30.Caption:='';
    QRLabel31.Caption:='';
  end;

end;

end.
