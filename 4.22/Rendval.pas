unit Rendval;

interface

uses
	SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
	Forms, Dialogs, StdCtrls, Mask, Buttons, Egyeb, DB, DBTables, J_SQL,
  Grids, DBGrids, ADODB;

type
	TRendvalDlg = class(TForm)
	BitElkuld: TBitBtn;
	BitKilep: TBitBtn;
	Label1: TLabel;
    DBGrid1: TDBGrid;
    DataSource1: TDataSource;
    Query1: TADOQuery;
    Query1VI_JAKOD: TStringField;
    Query1VI_HONNAN: TStringField;
    Query1VI_HOVA: TStringField;
    Query1VI_VENEV: TStringField;
    Query1VI_VEKOD: TStringField;
    Query1VI_VIKOD: TStringField;
	procedure Tolt(cim : string; terko : string);
	procedure FormCreate(Sender: TObject);
	procedure BitKilepClick(Sender: TObject);
	procedure BitElkuldClick(Sender: TObject);
	private
	public
		ret_kod		: string;
		ret_vikod	: string;
		ret_nev		: string;
     	vanrek		: boolean;
	end;

var
	RendvalDlg: TRendvalDlg;

implementation

{$R *.DFM}

procedure TRendvalDlg.Tolt(cim : string; terko : string);
begin
	Caption := cim;
  	ret_kod	:= '';
	vanrek	:= false;
  	if Query_Run (Query1, 'SELECT * FROM VISZONY WHERE VI_JAKOD = '''+terko+''' ',true) then begin
  		if Query1.RecordCount < 1 then begin
     		{Nincs megfelel� rekord}
     		Exit;
     	end;
     	vanrek	:= true;
  	end;
end;

procedure TRendvalDlg.FormCreate(Sender: TObject);
begin
	EgyebDlg.SetADOQueryDatabase(Query1);
  	ret_kod		:= '';
  	ret_nev		:= '';
	ret_vikod 	:= '';
  	vanrek		:= false;
end;

procedure TRendvalDlg.BitKilepClick(Sender: TObject);
begin
	ret_kod 	:= '';
	ret_vikod 	:= '';
	Close;
end;

procedure TRendvalDlg.BitElkuldClick(Sender: TObject);
begin
	ret_kod 	:= Query1.FieldByname('VI_VEKOD').AsString;
	ret_vikod 	:= Query1.FieldByname('VI_VIKOD').AsString;
	ret_nev 	:= Query1.FieldByname('VI_VENEV').AsString;
	Close;
end;

end.
