unit PontStatFm;

interface

uses
	SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
	StdCtrls, Forms, DBCtrls, DB, DBGrids, DBTables, Grids, ExtCtrls, Buttons,
	ShellApi, Dialogs, J_FOFORMUJ, ADODB, Mask, Menus, Variants;

type
	TPontStatFmDlg = class(TJ_FOFORMUJDLG)
      procedure FormCreate(Sender: TObject);override;
	 private
	 public
	end;

var
	PontStatFmDlg : TPontStatFmDlg;

implementation

uses
	Egyeb, J_SQL, Kozos;

{$R *.DFM}

procedure TPontStatFmDlg.FormCreate(Sender: TObject);
var
  S: string;
begin
	Inherited FormCreate(Sender);
	kellujures	:= false;
  // Nem tudom, hogy kell-e ilyen
	AddSzuromezoRovid( 'PP_DONEV', 'PONTOK' );
 //  S:=' select ROW_NUMBER() OVER (ORDER BY DOLGOZO) SORSZAM, DOLGOZO, CSOPORT, Pontsz�m, EddigiAtlaga, Honapok, CsoportAtlag from ';
  S:='select PP_DONEV, SZ_MENEV, PP_UPONT, PP_ATLAG, PP_HODAR, PP_CSOPAT, HONAP from ';
  S:=S+' (select PP_DONEV, SZ_MENEV, ';
  S:=S+' substring(PP_DATUM,1,7) HONAP,    ';
  S:=S+' avg(PP_UPONT) PP_UPONT, ';  // �tlagolunk, mert egy h�napra t�bben is pontozhatj�k
  S:=S+' avg(avg(PP_UPONT)) ';
	S:=S+' OVER (PARTITION BY SZ_MENEV, PP_DONEV) ';
	S:=S+' AS PP_ATLAG, ';  // dolgoz� pont�tlaga
	S:=S+' count(avg(PP_UPONT))  ';
	S:=S+' OVER (PARTITION BY SZ_MENEV, PP_DONEV) ';
	S:=S+' AS PP_HODAR, ';  // h�ny havi pontot �tlagoltunk
  S:=S+' avg(avg(PP_UPONT)) ';
	S:=S+' OVER (PARTITION BY SZ_MENEV) ';
	S:=S+' AS PP_CSOPAT ';  // csoport �tlag
  S:=S+' from PONTOK, SZOTAR ';
  S:=S+' where SZ_FOKOD=400 and SZ_ALKOD=PP_ALKOD ';

  // ha a felhaszn�l� pontoz�, akkor a saj�t maga �ltal adott pontok alapj�n k�sz�lt statisztik�t l�thatja
  if Query_Select('PONTOZO', 'PO_FEKOD', EgyebDlg.user_code, 'PO_FEKOD') = EgyebDlg.user_code then begin
    S:=S+' and PP_FEKOD= '+EgyebDlg.user_code;
    end; // if

  S:=S+' group by SZ_MENEV, PP_DONEV, PP_DATUM ) a  ';
  S:=S+' where HONAP = (select MAX (substring(PP_DATUM,1,7)) from PONTOK) ';

	alapstr	:= S;
  // FELVISZ, MODOSIT, LISTCIM, FOMEZO, ALAPSTR, FOTABLA
	FelTolto('', '', 'Pontoz�s statisztika', 'PP_DONEV', alapstr, '__', QUERY_KOZOS_TAG );
	kulcsmezo			:= 'PP_DONEV';
	nevmezo				:= 'PP_DONEV';
	width				:= DLG_WIDTH;
	height				:= DLG_HEIGHT;
  ButtonTor.Enabled :=False;
  ButtonUj.Enabled :=False;
  ButtonMod.Enabled :=False;
  ButtonNez.Enabled :=False;
end;

end.


