unit Jarat_ell;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ADODB, StdCtrls, ExtCtrls, Buttons, Mask,DateUtils;

type
  TFJarat_ell = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    ListBox1: TListBox;
    Button1: TButton;
    Button2: TButton;
    jarat: TADOQuery;
    jaratja_kod: TStringField;
    viszony: TADOQuery;
    ADOQuery2: TADOQuery;
    viszonyvi_jakod: TStringField;
    viszonyvi_ujkod: TIntegerField;
    viszonyvi_sakod: TStringField;
    Label1: TLabel;
    Label2: TLabel;
    MD1: TMaskEdit;
    MD2: TMaskEdit;
    BitBtn2: TBitBtn;
    BitBtn3: TBitBtn;
    Label3: TLabel;
    Button3: TButton;
    jaratja_fajko: TIntegerField;
    CheckBox1: TCheckBox;
    CheckBox2: TCheckBox;
    CheckBox3: TCheckBox;
    jaratja_jkezd: TStringField;
    procedure Button2Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure ListBox1DblClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure CheckBox2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FJarat_ell: TFJarat_ell;

implementation

uses Egyeb, Fomenu,Jaratbe,J_SQL;

{$R *.dfm}

procedure TFJarat_ell.Button2Click(Sender: TObject);
var
  db,hiba:integer;
begin
  db:=0;
  hiba:=0;
  ListBox1.Items.Clear;
  Application.ProcessMessages;
  jarat.Close;
  jarat.SQL.Clear;
  //jarat.SQL.Add('select ja_kod, ja_jvege , ja_fajko from jarat where  ja_jvege>= :KEZD and ja_jvege<= :VEGE ');
  jarat.SQL.Add('select ja_kod, ja_jkezd , ja_fajko from jarat where  ja_jkezd>= :KEZD and ja_jkezd<= :VEGE ');

  if CheckBox1.Checked and not CheckBox2.Checked then       // csak norm�l
    jarat.SQL.Add(' and ja_fajko=0 ');
  if CheckBox2.Checked and not CheckBox1.Checked then       // csak NEM norm�l
    jarat.SQL.Add(' and ja_fajko>0 ');

  jarat.SQL.Add(' order by ja_jkezd');


  jarat.Parameters[0].Value:=MD1.Text;
  jarat.Parameters[1].Value:=MD2.Text;
  jarat.Open;
  while not jarat.Eof do
  begin
    inc(db);
    Label3.Caption:=IntToStr(db)+'  '+jaratja_jkezd.AsString+'  '+IntToStr(hiba);
    Label3.Update;
    viszony.Close;
    viszony.Parameters[0].Value:=jaratja_kod.Value;
    viszony.Open;
    viszony.First;
    if viszony.Eof then
    begin
     if not CheckBox3.Enabled or (CheckBox3.Enabled and not CheckBox3.Checked) then
     begin
      // nincs viszony
      inc(hiba);
      ListBox1.Items.Add('Nincs viszony! '+jaratja_fajko.AsString +'  J�ratk�d: '+jaratja_kod.AsString);
      ListBox1.Update;
     end;
    end
    else
    begin
      if (jaratja_fajko.Value=0)and(viszonyvi_sakod.AsString='') then
      begin
        // nincs sz�mlasz�m
        inc(hiba);
        ListBox1.Items.Add('Nincs sz�mlasz�m! J�ratk�d: '+jaratja_kod.AsString) ;
        ListBox1.Update;
      end;
    end;
    jarat.Next;
  end;
  ShowMessage('K�sz!');
end;

procedure TFJarat_ell.BitBtn2Click(Sender: TObject);
begin
	// EgyebDlg.Calendarbe(MD1);
  // EgyebDlg.ElsoNapEllenor(MD1, MD2);
  EgyebDlg.CalendarBe_idoszak(MD1, MD2);
end;

procedure TFJarat_ell.BitBtn3Click(Sender: TObject);
begin
	EgyebDlg.Calendarbe(MD2);
  EgyebDlg.UtolsoNap(MD2);
  // MD2.Text:=DateToStr(EndOfTheMonth(StrToDate(MD2.Text)));
//  Ellenorzes;
end;

procedure TFJarat_ell.Button1Click(Sender: TObject);
begin
  close;
end;

procedure TFJarat_ell.Button3Click(Sender: TObject);
var
  jakod: string;
begin
	// J�rat megjelen�t�se
  if ListBox1.ItemIndex=-1 then
  begin
    ShowMessage('V�lasszon j�ratot!');
    exit;
  end;  
	jakod:= ListBox1.Items[ListBox1.itemindex] ;
  jakod:= copy(jakod,pos('J�ratk�d',jakod)+10,6);
	if jakod <> '' then begin
		Application.CreateForm(TJaratbeDlg, JaratbeDlg);
    JaratbeDlg.megtekint:=Query_Select('JARAT','JA_KOD',jakod,'JA_FAZIS')>'0';
    //JaratbeDlg.megtekint:=True; // (QueryUj3.FieldByName('JA_FAZIS').AsInteger > 0);
		JaratbeDlg.Tolto('J�rat m�dos�t�s',jakod);
		JaratbeDlg.ShowModal;
		JaratbeDlg.Destroy;
	end;
end;

procedure TFJarat_ell.ListBox1DblClick(Sender: TObject);
begin
  Button3.OnClick(self);
end;

procedure TFJarat_ell.FormCreate(Sender: TObject);
begin
  MD1.Text:=copy(DateToStr(date),1,8)+'01.';
  EgyebDlg.ElsoNapEllenor(MD1, MD2);
end;

procedure TFJarat_ell.CheckBox2Click(Sender: TObject);
begin
  CheckBox3.Enabled:= CheckBox1.Checked and not CheckBox2.Checked ;
end;

end.
