unit Szurbe;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, Mask, DbTables, ADODB, DB, Vcl.ExtCtrls;

type
  TSzuroMezoTipus = (szmtSzam, szmtSzoveg, szmtDatum, szmtLogikai);
type
  TSzurbeDlg = class(TForm)
    Panel1: TPanel;
    Label1: TLabel;
    ComboCimke1: TComboBox;
    ComboMezo1: TComboBox;
    Panel2: TPanel;
    Label4: TLabel;
    ComboCimke2: TComboBox;
    ComboMezo2: TComboBox;
    Panel3: TPanel;
    Label2: TLabel;
    ComboBox2: TComboBox;
    Panel4: TPanel;
    Panel5: TPanel;
    Label3: TLabel;
    MaskEdit1: TMaskEdit;
    ButtonKuld: TBitBtn;
    ButtonKilep: TBitBtn;
    procedure FormCreate(Sender: TObject);
    procedure ButtonKilepClick(Sender: TObject);
    procedure ButtonKuldClick(Sender: TObject);
    procedure Tolt (str : string);
    procedure ComboBox2Change(Sender: TObject);
    function GetMezoTipus(ComboCimke, ComboMezo: TComboBox): TSzuroMezoTipus;
  private
  public
    ret_str	: string;
    ret_str2	: string;
    querymas 	: TADOQuery;
  end;

const
  Fejlec_keret = 29;  // a form magass�g�nak be�ll�t�s�hoz

var
  SzurbeDlg: TSzurbeDlg;

implementation

{$R *.DFM}

uses
	Egyeb, Kozos;

procedure TSzurbeDlg.FormCreate(Sender: TObject);
begin
	ComboBox2.ItemIndex 	:= 0;
  ret_str              := '';
  ret_str2             := '';
end;

procedure TSzurbeDlg.ButtonKilepClick(Sender: TObject);
begin
	ret_str 	:= '';
	ret_str2	:= '';
	Close;
end;

procedure TSzurbeDlg.ButtonKuldClick(Sender: TObject);
var
 	 muvelet, regifor, operandus, Mezo1Nev, Mezo2Nev: string;
   MuveletID, ev, ho, na: integer;
   v_date: TDate;
   Mezo1Tipus, Mezo2Tipus: TSzuroMezoTipus;
begin
  Mezo1Tipus := GetMezoTipus(ComboCimke1, ComboMezo1);
  if (Mezo1Tipus = szmtSzam) then begin  // csak numerikus mez�kre ellen�rz�nk
   if Trim(MaskEdit1.Text) = '' then begin
      NoticeKi('A sz�r�s �rt�ke nem lehet �res!');
      MaskEdit1.SetFocus;
      Exit;
      end;  // if
    end;
  MuveletID:= ComboBox2.ItemIndex;
  case MuveletID of
    0,1,2,3,4,5,6,7: begin   // egy operandus� m�veletek
    	case MuveletID of
      	0 : muvelet := '<';
      	1 : muvelet := '<=';
      	2 : muvelet := '=';
      	3 : muvelet := '>=';
      	4 : muvelet := '>';
       	5 : muvelet := 'like';    // tartalmaz
        6 : muvelet := 'not like';  // nem tartalmaz
        7 : muvelet := 'like';   // minta
      else
      	muvelet := '<>';
      end;
      // A sorsz�m a display name-b�l j�n (ComboCimke1), a mez� n�v a FieldName -b�l (Combo1)
      Mezo1Tipus := GetMezoTipus(ComboCimke1, ComboMezo1);
      Mezo1Nev:= ComboMezo1.Items[ComboCimke1.ItemIndex];
      // Sz�m adatok lekezel�se
      if Mezo1Tipus = szmtSzam then begin
    		ret_str 	:= '('+Mezo1Nev+')'+ ' ' + muvelet + ' ' + MaskEdit1.Text;
    		ret_str2	:= '('+Mezo1Nev+')'+ ' ' + muvelet + ' ' + MaskEdit1.Text;
        end;
      // Sz�veges adatok lekezel�se
      if Mezo1Tipus = szmtSzoveg then begin
        if (MuveletID = 5) or (MuveletID = 6) then begin   // tartalmaz, nem tartalmaz
            operandus:='%'+MaskEdit1.Text+'%';     // �rthet�bb a felhaszn�l�nak, ha nem kell ki�rnia a %-ot
            end
        else begin
            operandus:=MaskEdit1.Text;
            end;
    		ret_str 	:= '('+Mezo1Nev+')' + ' ' + muvelet + ' ''' + operandus + ''' ';
    		ret_str2	:= '('+Mezo1Nev+')' + ' ' + muvelet + ' ''' + operandus + ''' ';
      end;
      // Logikai adatok lekezel�se
      if Mezo1Tipus = szmtLogikai then begin
      	if UpperCase(MaskEdit1.Text) = 'IGEN' then begin
         	MaskEdit1.Text	:= 'TRUE';
       end;
      	if UpperCase(MaskEdit1.Text) <> 'TRUE' then begin
         	MaskEdit1.Text	:= 'FALSE';
         end;
    		ret_str 	:= '('+Mezo1Nev+')' + ' ' + muvelet + ' ' + MaskEdit1.Text;
    		ret_str2	:= '('+Mezo1Nev+')' + ' ' + muvelet + ' ' + MaskEdit1.Text;
        end;
      // D�tum adatok lekezel�se
      if Mezo1Tipus = szmtDatum then begin
      	// A mezo d�tum t�pus�
         ev := StrToIntDef(copy(MaskEdit1.Text,1,4),0);
         ho := StrToIntDef(copy(MaskEdit1.Text,6,2),0);
         na := StrToIntDef(copy(MaskEdit1.Text,9,2),0);
    		try
         v_date := EncodeDate(ev,ho,na);
         except
         	NoticeKi('�rv�nytelen d�tum!', NOT_MESSAGE);
            Exit;
         end;
    		regifor			 := FormatSettings.ShortDateFormat;
      	FormatSettings.ShortDateFormat := 'dd/mm/yy';
    		ret_str 	:= '('+Mezo1Nev+')' +' '+ muvelet+''''+DateToStr(v_date)+'''';
    		ret_str2	:= '('+Mezo1Nev+')' +' '+ muvelet+''''+DateToStr(v_date)+'''';
  	    FormatSettings.ShortDateFormat := regifor;
        end;
      end;  // 1 operandus
     8: begin   // k�t operandus� m�veletek
       Mezo1Tipus := GetMezoTipus(ComboCimke1, ComboMezo1);
       Mezo2Tipus := GetMezoTipus(ComboCimke2, ComboMezo2);
       Mezo1Nev:= ComboMezo1.Items[ComboCimke1.ItemIndex];
       Mezo2Nev:= ComboMezo2.Items[ComboCimke2.ItemIndex];
       case MuveletID of
        	8 : begin   // "elt�r�s�k nagyobb mint"
            if ((Mezo1Tipus <> szmtSzam) or (Mezo1Tipus <> szmtSzam)) then begin
                NoticeKi('Ez a m�velet csak sz�m adatmez�kre alkalmazhat�!', NOT_MESSAGE);
                Exit;
                end;  // if
            ret_str 	:= 'abs('+'('+Mezo1Nev+')' + '-' + '('+Mezo2Nev+')' + ') > ' + MaskEdit1.Text;
        		ret_str2	:= 'abs('+'('+Mezo1Nev+')' + '-' + '('+Mezo2Nev+')' + ') > ' + MaskEdit1.Text;
            end;  // 8
          end;  // case MuveletID
        end;  // k�t op.
     end;  // case
     Close; // form
end;

function TSzurbeDlg.GetMezoTipus(ComboCimke, ComboMezo: TComboBox): TSzuroMezoTipus;
begin
  if querymas.FindField(ComboMezo.Items[ComboCimke.ItemIndex]).DataType in [ ftSmallint, ftInteger, ftWord, ftFloat, ftCurrency, ftBCD ] then begin
		Result:= szmtSzam;
    end;
  if querymas.FindField(ComboMezo.Items[ComboCimke.ItemIndex]).DataType in [ ftString ] then begin
    Result:= szmtSzoveg;
    end;
  if querymas.FindField(ComboMezo1.Items[ComboCimke1.ItemIndex]).DataType in [ ftBoolean ] then begin
  	Result:= szmtLogikai;
    end;
  if querymas.FindField(ComboMezo1.Items[ComboCimke1.ItemIndex]).DataType in [ ftDate ] then begin
  	Result:= szmtDatum;
    end;
end;


{procedure TSzurbeDlg.ButtonKuldClick(Sender: TObject);
var
	muvelet 	: string;
   ev			: integer;
   ho			: integer;
   na			: integer;
   v_date		: TDate;
   regifor		: string;
   operandus: string;
begin
	case ComboBox2.ItemIndex of
  	0 : muvelet := '<';
  	1 : muvelet := '<=';
  	2 : muvelet := '=';
  	3 : muvelet := '>=';
  	4 : muvelet := '>';
   	5 : muvelet := 'like';    // tartalmaz
    6 : muvelet := 'not like';  // nem tartalmaz
    7 : muvelet := 'like';   // minta
    8 : muvelet := '';   // ez kicsit m�sk�nt m�k�dik
  else
  	muvelet := '<>';
  end;
  // A sorsz�m a display name-b�l j�n (ComboCimke1), a mez� n�v a FieldName -b�l (Combo1)

  // Sz�m adatok lekezel�se
  if querymas.FindField(ComboMezo1.Items[ComboCimke1.ItemIndex]).DataType in [ ftSmallint, ftInteger, ftWord, ftFloat, ftCurrency, ftBCD ] then begin
		ret_str 	:= ComboMezo1.Items[ComboCimke1.ItemIndex] + ' ' + muvelet + ' ' + MaskEdit1.Text;
		ret_str2	:= ComboCimke1.Items[ComboCimke1.ItemIndex] + ' ' + muvelet + ' ' + MaskEdit1.Text;
  end;
  // Sz�veges adatok lekezel�se
  if querymas.FindField(ComboMezo1.Items[ComboCimke1.ItemIndex]).DataType in [ ftString ] then begin
    if (ComboBox2.ItemIndex=5) or (ComboBox2.ItemIndex=6) then begin   // tartalmaz, nem tartalmaz
        operandus:='%'+MaskEdit1.Text+'%';     // �rthet�bb a felhaszn�l�nak, ha nem kell ki�rnia a %-ot
        end
    else begin
        operandus:=MaskEdit1.Text;
        end;
		ret_str 	:= ComboMezo1.Items[ComboCimke1.ItemIndex] + ' ' + muvelet + ' ''' + operandus + ''' ';
		ret_str2	:= ComboCimke1.Items[ComboCimke1.ItemIndex] + ' ' + muvelet + ' ''' + operandus + ''' ';
  end;
  // Logikai adatok lekezel�se
  if querymas.FindField(ComboMezo1.Items[ComboCimke1.ItemIndex]).DataType in [ ftBoolean ] then begin
  	if UpperCase(MaskEdit1.Text) = 'IGEN' then begin
     	MaskEdit1.Text	:= 'TRUE';
     end;
  	if UpperCase(MaskEdit1.Text) <> 'TRUE' then begin
     	MaskEdit1.Text	:= 'FALSE';
     end;
		ret_str 	:= ComboMezo1.Items[ComboCimke1.ItemIndex] + ' ' + muvelet + ' ' + MaskEdit1.Text;
		ret_str2	:= ComboCimke1.Items[ComboCimke1.ItemIndex] + ' ' + muvelet + ' ' + MaskEdit1.Text;
  end;
  // D�tum adatok lekezel�se
  if querymas.FindField(ComboMezo1.Items[ComboCimke1.ItemIndex]).DataType in [ ftDate ] then begin
  	// A mezo d�tum t�pus�
     ev := StrToIntDef(copy(MaskEdit1.Text,1,4),0);
     ho := StrToIntDef(copy(MaskEdit1.Text,6,2),0);
     na := StrToIntDef(copy(MaskEdit1.Text,9,2),0);
		try
     v_date := EncodeDate(ev,ho,na);
     except
     	NoticeKi('�rv�nytelen d�tum!', NOT_MESSAGE);
        Exit;
     end;
		regifor			 := FormatSettings.ShortDateFormat;
  	FormatSettings.ShortDateFormat := 'dd/mm/yy';
		ret_str 	:= ComboMezo1.Items[ComboCimke1.ItemIndex]+' '+ muvelet+''''+DateToStr(v_date)+'''';
		ret_str2	:= ComboCimke1.Items[ComboCimke1.ItemIndex]+' '+ muvelet+''''+DateToStr(v_date)+'''';
  	FormatSettings.ShortDateFormat := regifor;
  end;
  Close;
end;
}

procedure TSzurbeDlg.ComboBox2Change(Sender: TObject);
begin
  case ComboBox2.ItemIndex of
  	0,1,2,3,4,5,6,7 : begin  // egy mez�t ig�nyl� m�veletek
        Panel2.Visible:= False;
        end;
    8 : begin  // k�t mez�t ig�nyl� m�veletek
        Panel2.Visible:= True;
        end;
    end;  // case
    Height:= panel5.Top + Panel5.Height + Fejlec_keret;
end;

procedure TSzurbeDlg.Tolt(str : string);
type
  TParseResult = record
      Mezo: string;
      Maradek: string;
      end;  // record
var
	i, muveletid: integer;
  nev, muvelet, szoveg, eredetistr, nev1, nev2: string;
  ParseResult: TParseResult;

  function MezonevKicsomagol (i_nev: string): string;
    var
      S: string;
    begin
      if (copy(i_nev, 1, 1)<>'(') or (copy(i_nev, length(i_nev), 1)<>')') then begin
         Result:='';
         NoticeKi('Hib�s sz�r�s form�tum (mez� azonos�t� nincs z�r�jelben): '+i_nev+'. Teljes kifejez�s: '+eredetistr);
         end
      else begin
        Result:= copy(i_nev, 2, length(i_nev)-2);
        end;
    end;

  function GetNextMezonev (i_str: string): TParseResult;
    var
      S: string;
      Vege: integer;
    begin
      if copy(i_str, 1, 1)<>'(' then begin
         Result.Mezo:='';
         NoticeKi('Hib�s sz�r�s form�tum (z�r�jelet v�runk): '+str+'. Teljes kifejez�s: '+eredetistr);
         end
      else begin
        Vege:= Pos(')', i_str);
        Result.Mezo := copy(i_str, 1, Vege);
        Result.Maradek := copy(i_str, Vege+1, 999);
        end;
    end;

begin
  eredetistr:= str;  // k�s�bbi megjelen�t�shez
  ComboMezo1.Clear;
  ComboMezo2.Clear;
  ComboCimke1.Clear;
  ComboCimke2.Clear;
  for i := 0 to querymas.FieldCount  - 1 do begin
  	if not querymas.Fields[i].Calculated then begin
  		if querymas.Fields[i].Visible then begin
  			ComboMezo1.Items.Add(querymas.Fields[i].FieldName);
        ComboMezo2.Items.Add(querymas.Fields[i].FieldName);
  			ComboCimke1.Items.Add(querymas.Fields[i].DisplayName);
  			ComboCimke2.Items.Add(querymas.Fields[i].DisplayName);
     	end;
     end;
  end;
	ComboMezo1.ItemIndex := 0;
	ComboMezo2.ItemIndex := 0;
	ComboCimke1.ItemIndex := 0;
	ComboCimke2.ItemIndex := 0;
  // a t�rolt sz�r�s sor (SQL) megjelen�t�se , p�lda: (�v) < '2014'
  // hogyan lehet majd visszafejteni a t�bbf�le, szint�n "<" -be ford�tott felt�telt?
  // pr�b�ljuk felismerni a komplexebbet, ha nem illeszkedik, akkor az egyszer�bb.
  if str <> '' then begin
    if copy(str, 1, 4) = 'abs(' then begin  // "elt�r�s�k nagyobb mint" = abs((x)-(y)) > 1
       Panel2.Visible:= True;
       muveletid:= 8;
       str:= copy(str, 5, length(str)- 4); //  (x)-(y)) > 1
       ParseResult:= GetNextMezonev(str);
       nev1:= MezonevKicsomagol(ParseResult.Mezo);
       if nev1='' then Exit;
       str:= ParseResult.Maradek;    // -(y)) > 1
       str:= copy(str, 2, length(str)- 1); // (y)) > 1

       ParseResult:= GetNextMezonev(str);
       nev2:= MezonevKicsomagol(ParseResult.Mezo);
       if nev2='' then Exit;
       str:= ParseResult.Maradek;  // ) > 1
       szoveg	:= Trim(EgyebDlg.SepRest('>', str));

       // megjelen�t�s
       i := ComboMezo1.Items.IndexOf(nev1);
       if i < 0 then i := 0;
       ComboMezo1.ItemIndex	:= i;
       ComboCimke1.ItemIndex	:= i;

       i := ComboMezo2.Items.IndexOf(nev2);
       if i < 0 then i := 0;
       ComboMezo2.ItemIndex	:= i;
       ComboCimke2.ItemIndex	:= i;

       ComboBox2.ItemIndex	:= muveletid;
       if copy(szoveg,1,1) = '''' then begin
       	szoveg := copy(szoveg, 2, Length(szoveg)-3);
       end;
       MaskEdit1.Text			:= szoveg;
       end
    else begin  // hagyom�nyos egy operandus� m�velet
      Panel2.Visible:= False;
  	  nev		:= '';
      muvelet	:= '';
      szoveg	:= '';
      if Pos(' ', str) > 0 then begin
     	  nev	:= Trim(copy(str, 1, Pos(' ', str) - 1));  // mez� n�v: a sz�k�z el�tti r�sz
        nev := MezonevKicsomagol(nev);
        if nev='' then Exit;
        str	:= copy(str, Pos(' ', str) + 1, 999);
        if Pos(' ', str) > 0 then begin
          muvelet	:= copy(str, 1, Pos(' ', str) - 1); // a k�vetkez� sz�k�zig a m�velet
          szoveg	:= copy(str, Pos(' ', str) + 1, 999);  // �s a marad�k a konstans
          end;  // if
        end; // if
      i := ComboMezo1.Items.IndexOf(nev);
       if i < 0 then begin
       	 i := 0;
         end;
       ComboMezo1.ItemIndex	:= i;
       ComboCimke1.ItemIndex	:= i;
       i := ComboBox2.Items.IndexOf(muvelet);
       if i < 0 then begin
       	i := 5;	// Ez itt a tartalmaz�s
       end;
       ComboBox2.ItemIndex	:= i;
       if copy(szoveg,1,1) = '''' then begin
       	szoveg := copy(szoveg, 2, Length(szoveg)-3);
       end;
       MaskEdit1.Text			:= szoveg;
       end;  // else
     Height:= panel5.Top + Panel5.Height + Fejlec_keret;
  end;
end;

end.

