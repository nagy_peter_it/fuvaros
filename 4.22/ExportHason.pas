unit ExportHason;

interface

uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, DBTables, DB, StdCtrls, Buttons, Mask, DBCtrls,
  ExtCtrls, ADODB, FileCtrl, StrUtils, Grids;

type
  TExportHasonDlg = class(TForm)
	 BitBtn6: TBitBtn;
	 Label4: TLabel;
	 M1: TMaskEdit;
	 BitBtn5: TBitBtn;
	 Query1: TADOQuery;
	 Query2: TADOQuery;
	 Label1: TLabel;
	 OpenDialog1: TOpenDialog;
	 Label2: TLabel;
	 MaskEdit1: TMaskEdit;
	 Label3: TLabel;
	 MaskEdit2: TMaskEdit;
	 BitBtn1: TBitBtn;
	 CheckBox1: TCheckBox;
	 BtnExport: TBitBtn;
	 procedure FormCreate(Sender: TObject);
	 procedure BitBtn6Click(Sender: TObject);
	 procedure BitBtn5Click(Sender: TObject);
	 procedure BtnExportClick(Sender: TObject);
	 procedure Feldolgozas;
	 procedure Feldolgozas2;
  private
	  kilepes		: boolean;
  public
  end;

var
  ExportHasonDlg: TExportHasonDlg;

implementation

uses
	Egyeb, Kozos, J_SQL, J_Excel, HianyVevobe;
{$R *.DFM}

procedure TExportHasonDlg.FormCreate(Sender: TObject);
begin
 	M1.Text 		:= '';
	EgyebDlg.SetADOQueryDatabase(Query1);
	EgyebDlg.SetADOQueryDatabase(Query2);
	Label1.Caption	:= '';
	Label1.Update;
end;

procedure TExportHasonDlg.BitBtn6Click(Sender: TObject);
begin
	if not kilepes then begin
		kilepes	:= true;
	end else begin
		Close;
	end;
end;

procedure TExportHasonDlg.BitBtn5Click(Sender: TObject);
begin
	// Az �llom�ny kiv�laszt�sa
	OpenDialog1.Filter		:= 'XLS �llom�nyok (*.xls)|*.XLS';
	OpenDialog1.DefaultExt  := 'XLS';
	OpenDialog1.InitialDir	:= EgyebDlg.TempPath;
	if OpenDialog1.Execute then begin
		M1.Text := OpenDialog1.FileName;
	end;
end;

procedure TExportHasonDlg.BtnExportClick(Sender: TObject);
begin
	if CheckBox1.Checked then begin
		Feldolgozas2;
	end else begin
		Feldolgozas;
	end;
end;

procedure TExportHasonDlg.Feldolgozas;
var
	fn		   	: string;
	EX	  		: TJExcel;
	sorszam		: integer;
	kod			: string;
	szkod		: string;
begin
	if M1.Text	= '' then begin
		NoticeKi('A bemeneti �lom�ny nem lehet �res!');
		M1.SetFocus;
		Exit;
	end;
	if not FileExists(M1.Text) then begin
		NoticeKi('Nem l�tez� �llom�ny! ');
		Exit;
	end;
	fn	:= M1.Text;
	EX 	:= TJexcel.Create(Self);
	if not EX.OpenXlsFile(fn) then begin
		NoticeKi('Nem siker�lt megnyitnom az Excel �llom�nyt!');
	end else begin
		EX.ColNumber	:= 30;
		EX.InitRow;
		EX.SetActiveSheet(0);
		sorszam		:= 1;
		kod			:= EX.ReadCell(sorszam,3);
		Inc(sorszam);
		kod	  	  := EX.ReadCell(sorszam,3);
		while kod <> '' do begin
			// Feldolgoz�s
			szkod	:= 'JS09/0'+kod;
			Query_Run(Query1, 'UPDATE SZFEJ  SET SA_ATAD = 2 WHERE SA_KOD = '''+szkod+''' ');
			Query_Run(Query1, 'UPDATE SZFEJ2 SET SA_ATAD = 2 WHERE SA_KOD = '''+szkod+''' ');
			Label1.Caption	:= 'A beolvasott sz�mlak�d : '+kod;
			Label1.Update;
			Inc(sorszam);
			kod	  := EX.ReadCell(sorszam,3);
		end;
		EX.Free;
		NoticeKi('Az ellen�rz�s befejez�d�tt!');
		Label1.Caption	:= '';
		Label1.Update;
		kilepes	:= true;
	end;
end;

procedure TExportHasonDlg.Feldolgozas2;
var
	fn		   	: string;
	EX	  		: TJExcel;
	sorszam		: integer;
	kod			: string;
	szkod		: string;
begin
	if M1.Text	= '' then begin
		NoticeKi('A bemeneti �lom�ny nem lehet �res!');
		M1.SetFocus;
		Exit;
	end;
	if not FileExists(M1.Text) then begin
		NoticeKi('Nem l�tez� �llom�ny! ');
		Exit;
	end;
	fn	:= M1.Text;
	EX 	:= TJexcel.Create(Self);
	if not EX.OpenXlsFile(fn) then begin
		NoticeKi('Nem siker�lt megnyitnom az Excel �llom�nyt!');
	end else begin
		EX.ColNumber	:= 10;
		EX.InitRow;
		EX.SetActiveSheet(0);
		sorszam		:= 1;
		kod	  	  	:= EX.ReadCell(sorszam,1);
		while kod <> '' do begin
			// Feldolgoz�s
			szkod	:= 'JS09/0'+Format('%4.4d', [StrToIntDef(kod,0)]);
			Query_Run(Query1, 'SELECT * FROM SZFEJ WHERE SA_KOD = '''+szkod+''' ');
			if 1 > Query1.RecordCount then begin
				NoticeKi('Hi�nyz� sz�mla : '+szkod);
			end;
			Query_Run(Query1, 'UPDATE SZFEJ  SET SA_ATAD = 4 WHERE SA_KOD = '''+szkod+''' ');
			Query_Run(Query1, 'UPDATE SZFEJ2 SET SA_ATAD = 4 WHERE SA_KOD = '''+szkod+''' ');
			Label1.Caption	:= 'A beolvasott sz�mlak�d : '+kod;
			Label1.Update;
			Inc(sorszam);
			kod	  := EX.ReadCell(sorszam,1);
		end;
		EX.Free;
		NoticeKi('Az ellen�rz�s befejez�d�tt!');
		Label1.Caption	:= '';
		Label1.Update;
		kilepes	:= true;
	end;
end;

end.



