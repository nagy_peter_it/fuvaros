unit Levelbe;

interface

uses
	SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
	Forms, Dialogs, StdCtrls, Buttons, Mask, DB, DBTables, J_ALFORM, ADODB;

type
	TLevelbeDlg = class(TJ_AlformDlg)
	BitElkuld: TBitBtn;
	Label2: TLabel;
    M1: TMaskEdit;
	Label8: TLabel;
    M2: TMaskEdit;
    Label6: TLabel;
    M3: TMaskEdit;
    Query1: TADOQuery;
    Label1: TLabel;
    Memo1: TMemo;
	procedure FormCreate(Sender: TObject);
	procedure Tolto(cim : string; teko : string); override;
	procedure BitElkuldClick(Sender: TObject);
	private
	public
   	vanadat	: boolean;
	end;

var
	LevelbeDlg: TLevelbeDlg;

implementation

uses
	Egyeb, J_SQL, Kozos;
{$R *.DFM}

procedure TLevelbeDlg.FormCreate(Sender: TObject);
begin
  	EgyebDlg.SetADOQueryDatabase(Query1);
	ret_kod 	:= '';
end;

procedure TLevelbeDlg.Tolto(cim : string; teko:string);
begin
	ret_kod 		:= teko;
	M1.Text 		:= '';
	M2.Text 		:= '';
	M3.Text 		:= '';
   vanadat			:= false;
   if Query_Run (Query1, 'SELECT * FROM MEGLEVEL WHERE ML_MBKOD = '+teko+' ORDER BY ML_DATUM, ML_IDOPT ',true) then begin
		if Query1.RecordCount > 0 then begin
       	Query1.Last;
			M1.Text 			:= Query1.FieldByName('ML_FENEV').AsString;
			M2.Text 			:= Query1.FieldByName('ML_DATUM').AsString + ' ' + Query1.FieldByName('ML_IDOPT').AsString;
			M3.Text 			:= Query1.FieldByName('ML_EMCIM').AsString;
           Memo1.Lines.Add(Query1.FieldByName('ML_SZOV1').AsString+Query1.FieldByName('ML_SZOV2').AsString);
           vanadat	:= true;
       end;
	end;
   SetMaskEdits([M1, M2, M3]);
   Memo1.ReadOnly	:= true;
   Memo1.Color		:= TILTOTTSZIN;
end;

procedure TLevelbeDlg.BitElkuldClick(Sender: TObject);
begin
  Close;
end;

end.
