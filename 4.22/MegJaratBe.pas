unit MegJaratBe;
interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ADODB, Grids, DBGrids, StdCtrls, Buttons, Mask, J_ALFORM,
  ExtCtrls, ComCtrls;

type
  TMegjaratbeDlg = class(TJ_AlformDlg)
    DataSource1: TDataSource;
    Query1: TADOQuery;
    Panel1: TPanel;
    BitElkuld: TBitBtn;
    BitKilep: TBitBtn;
    Panel3: TPanel;
    Label8: TLabel;
    Label7: TLabel;
    Label1: TLabel;
    Label10: TLabel;
    Label46: TLabel;
    Label47: TLabel;
    Label48: TLabel;
    M1: TMaskEdit;
    M2: TMaskEdit;
    M3: TMaskEdit;
    M4: TMaskEdit;
    CBRendszam: TComboBox;
    CBPotkocsi: TComboBox;
    CBSofor: TComboBox;
    Query2: TADOQuery;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    SG1: TStringGrid;
    SG2: TStringGrid;
    Panel2: TPanel;
    Panel4: TPanel;
    BitBtn53: TBitBtn;
    BitBtn54: TBitBtn;
    Timer1: TTimer;
    CBSofor2: TComboBox;
    BitBtn1: TBitBtn;
    QueryKoz: TADOQuery;
    SGSor: TStringGrid;
    BitBtn7: TBitBtn;
	 procedure Tolto(cim : string; teko : string); override;
    procedure FormCreate(Sender: TObject);
    procedure BitKilepClick(Sender: TObject);
    procedure AddRadioGroup(iRowNumber: Integer);
    procedure SG1DrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure OnGroupClick(Sender: TObject);
    procedure Csoportosito;
    procedure FormCanResize(Sender: TObject; var NewWidth,
      NewHeight: Integer; var Resize: Boolean);
    procedure FormResize(Sender: TObject);
    procedure BitBtn54Click(Sender: TObject);
    procedure BitBtn53Click(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    function  KellRekord : boolean;
    procedure BitElkuldClick(Sender: TObject);
    procedure M1Exit(Sender: TObject);
    procedure M2Exit(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure SG2DrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure IdoSzamito;
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Hozzafuzes(recsz : integer);
    procedure CBRendszamChange(Sender: TObject);
    procedure BitBtn7Click(Sender: TObject);
  private
       lisof           : TStringList;
       lirsz           : TStringList;
       lipot           : TStringList;
       kellkerdes      : boolean;
  public
  end;

var
  MegjaratbeDlg: TMegjaratbeDlg;

implementation

uses Egyeb, J_Sql, Kozos, MegbizasUjFm, KOzos_Local, MegjaratAl1, Segedbe2;

{$R *.dfm}

procedure TMegjaratbeDlg.FormCreate(Sender: TObject);
var
   i   : integer;
begin
   BitElkuld.Enabled := false; // Csak r�szletek ut�n lehessen elk�ldeni
	EgyebDLg.SetADOQueryDatabase(Query1);
	EgyebDLg.SetADOQueryDatabase(Query2);
	EgyebDLg.SetADOQueryDatabase(QueryKoz);
   // A Comboboxok felt�lt�se
   lisof  := TStringList.Create;
   lirsz  := TStringList.Create;
   lipot  := TStringList.Create;
   kellkerdes  := true;
   // A g�pkocsi bet�lt�se
   cbRendszam.Items.Add('');
   Query_Run(QueryKoz, 'SELECT GK_RESZ FROM GEPKOCSI WHERE GK_ARHIV = 0 AND GK_POTOS <> 1 AND GK_KULSO < 1 AND GK_GKKAT NOT IN (''MG'', ''SZGK.'', '''') ORDER BY GK_RESZ');
   while not QueryKoz.Eof do begin
       cbRendszam.Items.Add(QueryKoz.FieldByName('GK_RESZ').AsString);
       QueryKoz.Next;
   end;
   cbRendszam.ItemIndex   := 0;
   // A p�tkocsi bet�lt�se
   cbPotkocsi.Items.Add('');
   Query_Run(QueryKoz, 'SELECT GK_RESZ FROM GEPKOCSI WHERE GK_ARHIV = 0 AND GK_POTOS = 1 AND GK_KULSO < 1 ORDER BY GK_RESZ');
   while not QueryKoz.Eof do begin
       cbPotkocsi.Items.Add(QueryKoz.FieldByName('GK_RESZ').AsString);
       QueryKoz.Next;
   end;
   cbRendszam.ItemIndex   := 0;
   // A dolgoz�k bet�lt�se
   lisof.Add('');
   cbSofor.Items.Add('');
   cbSofor2.Items.Add('');
   Query_Run(QueryKoz, 'SELECT DO_NAME, DO_KOD FROM DOLGOZO WHERE DO_ARHIV = 0 AND DO_KULSO < 1 ORDER BY DO_NAME ');
   while not QueryKoz.Eof do begin
       lisof.Add(QueryKoz.FieldByName('DO_KOD').AsString);
       cbSofor.Items.Add(QueryKoz.FieldByName('DO_NAME').AsString);
       cbSofor2.Items.Add(QueryKoz.FieldByName('DO_NAME').AsString);
       QueryKoz.Next;
   end;
   cbSofor.ItemINdex   := 0;
   cbSofor2.ItemINdex  := 0;
   M1.Text             := EgyebDlg.MaiDatum;
   // A stringGrid felt�lt�se
   SG1.ColCount        := 22;
   SG2.ColCount        := 20;
   SG1.Cells[0, 0]     := 'Ssz.';
   SG1.Cells[1, 0]     := 'T�pus';
   SG1.Cells[2, 0]     := 'K�d';
   SG1.Cells[3, 0]     := 'Felrak� c�g';
   SG1.Cells[4, 0]     := 'Felrak�';
   SG1.Cells[5, 0]     := 'Lerak� c�g';
   SG1.Cells[6, 0]     := 'Lerak�';
   SG1.Cells[7, 0]     := 'MBKOD';
   SG1.Cells[8, 0]     := 'Sorsz.';        // MB_SORSZ
   SG1.Cells[9, 0]     := 'T�pus';
   SG1.Cells[10, 0]    := 'Fel dat1';
   SG1.Cells[11, 0]    := 'Fel id�1';
   SG1.Cells[12, 0]    := 'Fel dat2';
   SG1.Cells[13, 0]    := 'Fel id�2';
   SG1.Cells[14, 0]    := 'Le dat1';
   SG1.Cells[15, 0]    := 'Le id�1';
   SG1.Cells[16, 0]    := 'Le dat2';
   SG1.Cells[17, 0]    := 'Le id�2';
   SG1.Cells[18, 0]    := 'Fel pal';
   SG1.Cells[19, 0]    := 'Fel s�ly';
   SG1.Cells[20, 0]    := 'Le pal';
   SG1.Cells[21, 0]    := 'Le s�ly';

   SG1.ColWidths[0]    := 30;
   SG1.ColWidths[1]    := 160;
   SG1.ColWidths[2]    := 60;
   SG1.ColWidths[3]    := 120;
   SG1.ColWidths[4]    := 220;
   SG1.ColWidths[5]    := 120;
   SG1.ColWidths[6]    := 240;
   SG1.ColWidths[7]    := 70;
   SG1.ColWidths[8]    := 50;
   SG1.ColWidths[9]    := -1;
   SG1.ColWidths[10]   := 70;
   SG1.ColWidths[11]   := 70;
   SG1.ColWidths[12]   := 70;
   SG1.ColWidths[13]   := 70;

   SG2.Cells[0, 0]     := 'Ssz.';
   SG2.Cells[1, 0]     := 'T�pus';
   SG2.Cells[2, 0]     := 'K�d';
   SG2.Cells[3, 0]     := 'Felrak� c�g';
   SG2.Cells[4, 0]     := 'Felrak�';
   SG2.Cells[5, 0]     := 'Lerak� c�g';
   SG2.Cells[6, 0]     := 'Lerak�';
   SG2.Cells[ 7, 0]    := 'Feldat1';
   SG2.Cells[ 8, 0]    := 'Felid�1';
   SG2.Cells[ 9, 0]    := 'Feldat2';
   SG2.Cells[10, 0]    := 'Felid�2';
   SG2.Cells[11, 0]    := 'Ledat1';
   SG2.Cells[12, 0]    := 'Leid�1';
   SG2.Cells[13, 0]    := 'Ledat2';
   SG2.Cells[14, 0]    := 'Leid�2';
   SG2.Cells[16, 0]    := 'Fel pal';
   SG2.Cells[17, 0]    := 'Fel s�ly';
   SG2.Cells[18, 0]    := 'Le pal';
   SG2.Cells[19, 0]    := 'Le s�ly';
   SG2.ColWidths[0]    := 30;
   SG2.ColWidths[1]    := 30;
   SG2.ColWidths[2]    := -1;
   SG2.ColWidths[3]    := 120;
   SG2.ColWidths[4]    := 240;
   SG2.ColWidths[5]    := 120;
   SG2.ColWidths[6]    := 240;
   SG2.ColWidths[15]   := -1;
   SetMaskEdits([M1, M2, M3, M4]);

   CBRendszam.AutoCompleteDelay:= EgyebDlg.combo_autocomplete_delay;
   CBPotkocsi.AutoCompleteDelay:= EgyebDlg.combo_autocomplete_delay;
   CBSofor.AutoCompleteDelay:= EgyebDlg.combo_autocomplete_delay;
end;

procedure TMegjaratbeDlg.Tolto(cim : string; teko : string);
var
   recsz   : integer;
   dat     : string;
begin
   // Itt t�ltj�k be az adatokat
   Caption := cim;
   Query_Run(Query1, 'SELECT * FROM MEGSEGED WHERE MS_MSKOD IN ('+teko+') AND MS_FAJKO = 0 ');
  //  Query_Run(Query1, 'SELECT * FROM MEGSEGED WHERE MS_ID IN ('+teko+') AND MS_FAJKO = 0 ');
   Query1.DisableControls;
   recsz   := 1;
   SG1.RowCount := 2;
   SG1.Rows[1].Clear;
   while not Query1.Eof do begin
       if KellRekord then begin    // Ellen�rizz�k, hogy a rekord megfelel�-e
           if SG1.Cells[2, 1] <> '' then begin
               SG1.RowCount    := SG1.RowCount + 1;
           end;
           recsz   := SG1.RowCount - 1;
           Hozzafuzes(recsz);
           Inc(recsz);
       end;
       Query1.Next;
   end;
   Query1.First;
   Query1.EnableControls;
   Csoportosito;
end;

procedure TMegjaratbeDlg.BitKilepClick(Sender: TObject);
begin
   ret_kod := '';
   Close;
end;

procedure TMegjaratbeDlg.AddRadioGroup(iRowNumber: Integer);
var
  NewGroup     :TRadioGroup;
  Rect         :TRect;
begin
   NewGroup            := TRadioGroup.Create(SG1);
   NewGroup.Parent     := SG1.Parent; // Place string grid on one panel
   NewGroup.Name       := 'Group'+IntToStr(iRowNumber);
   NewGroup.Caption    := '';
   NewGroup.Font.Size  := 9;
   NewGroup.Color      := clWhite;
   NewGroup.Items.Add('el�rak�s');
   NewGroup.Items.Add('lerak�');
   NewGroup.Columns    := 2;
   NewGroup.ItemIndex  := 0;
   NewGroup.OnClick    := OnGroupClick;
   Rect                := SG1.CellRect(1,iRowNumber);
   NewGroup.Left       := SG1.Left + Rect.Left   + 4;
   NewGroup.Top        := SG1.Top  + Rect.Top    + 2;
   NewGroup.Width      := Rect.Right - Rect.Left - 6;
   NewGroup.Height     := Rect.Bottom - Rect.Top - 4;
   NewGroup.Visible    := false;
   SG1.Objects[1, iRowNumber] := NewGroup;
end;

procedure TMegjaratbeDlg.SG1DrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
var
  NewGroup     : TRadioGroup;
  ertek        : integer;
begin
   if ACol = 1 then begin
       NewGroup                := SG1.Objects[1, ARow] as TRadioGroup;
       if Assigned(NewGroup) then begin
           NewGroup.Left       := SG1.Left + Rect.Left   + 4;
           NewGroup.Top        := SG1.Top  + Rect.Top    + 2;
           NewGroup.Width      := Rect.Right - Rect.Left - 6;
           NewGroup.Height     := Rect.Bottom - Rect.Top - 4;
           if NewGroup.Top + NewGroup.Height < SG1.Height then begin
               NewGroup.Visible    := True;
           end else begin
               NewGroup.Visible    := false;
           end;
       end;
   end;
end;

procedure TMegjaratbeDlg.OnGroupClick(Sender: TObject);
var
   sorsz   : integer;
   i       : integer;
begin
   // Az aktu�lis sor
   sorsz   := StrToIntDef(copy((Sender as TRadioGroup).Name, 6, 999), 0);
   for i := 1 to SG1.RowCount - 1 do begin
       if i <> sorsz then begin
           // Egyez� mbkod + sorsz eset�n be�ll�tjuk ugyanarra a group-ot
           if ( ( SG1.Cells[7, i] = SG1.Cells[7, sorsz] ) and
                ( SG1.Cells[8, i] = SG1.Cells[8, sorsz] ) ) then begin
                ( SG1.Objects[1, i] as TRadioGroup).ItemIndex := ( SG1.Objects[1, sorsz] as TRadioGroup).ItemIndex;
           end;
       end;
   end;
   Csoportosito;
end;

procedure TMegjaratbeDlg.Csoportosito;
var
   i, j : integer;
begin
   // Az els� t�bl�zat csoportos�t�sa alapj�n elk�sz�tj�k a tervezett megseg�dek list�j�t
   SG2.RowCount := 2;
   SG2.Rows[1].Clear;
   if SG1.Cells[2, 1] = '' then begin
       Exit;
   end;

   // A sorsz�mos t�bla ki�r�t�se
   SGSor.ColCount := 3;
   SGSor.RowCount := 1;
   SGSor.Rows[1].Clear;

   // Bem�soljuk a tartalmat
   SG2.RowCount    := SG1.RowCount;
   for i := 1 to SG1.RowCount - 1 do begin
       SG2.Rows[i].Clear;
       SG2.Cells[1, i] := IntToStr(( SG1.Objects[1, i] as TRadioGroup).ItemIndex);
       for j := 2 to 6 do begin
           SG2.Cells[j, i] := SG1.Cells[j, i];
       end;
       for j := 0 to 7 do begin
           SG2.Cells[7+j, i] := SG1.Cells[10+j, i];
       end;
       SG2.Cells[15, i] := '0';
       SG2.Cells[16, i] := SG1.Cells[18, i];
       SG2.Cells[17, i] := SG1.Cells[19, i];
       SG2.Cells[18, i] := SG1.Cells[20, i];
       SG2.Cells[19, i] := SG1.Cells[21, i];

       case StrToIntDef(SG2.Cells[1, i], -1) of
           0 : // felrak�
               begin
                   SG2.Cells[2, i] := '';
                   SG2.Cells[5, i] := '';
                   SG2.Cells[6, i] := 'Telephely';
               end;
           1 : // lerak�
               begin
                   SG2.Cells[2, i] := '';
                   SG2.Cells[3, i] := '';
                   SG2.Cells[4, i] := 'Telephely';
               end;
       end;
   end;
   // Itt j�nnek az �sszehasonl�t�sok
   TablaRendezo( SG2, [1, 4, 6], [0, 0, 0], 1, true );
//   SaveGridToFile(SG2, 'C:\Grid0.txt');
   i := 1;
   while i < SG2.RowCount - 1 do begin
       if (    (SG2.Cells[1, i] = SG2.Cells[1, i+1]) and
               (SG2.Cells[3, i] = SG2.Cells[3, i+1]) and
               (SG2.Cells[4, i] = SG2.Cells[4, i+1]) and
               (SG2.Cells[5, i] = SG2.Cells[5, i+1]) and
               (SG2.Cells[6, i] = SG2.Cells[6, i+1])
           ) then begin
           // A d�tumok t�lt�se
           if SG2.Cells[7, i+1] < SG2.Cells[7, i] then begin
               SG2.Cells[7, i] := SG2.Cells[7, i+1];
               SG2.Cells[8, i] := SG2.Cells[8, i+1];
           end;
           if SG2.Cells[11, i+1] > SG2.Cells[11, i] then begin
               SG2.Cells[11, i] := SG2.Cells[11, i+1];
               SG2.Cells[12, i] := SG2.Cells[12, i+1];
           end;
           // A s�lyok �s palett�k �sszead�sa
           for j := 16 to 19 do begin
               SG2.Cells[j, i] := SzamString(StringSzam(SG2.Cells[j, i]) + StringSzam(SG2.Cells[j, i+1]), 12,2);
           end;

           // Ki kell t�r�lni a duplik�lt sort
           GridTorles(SG2, i+1);
//           SaveGridToFile(SG2, 'C:\Grid'+IntToStr(i)+'.txt');
       end else begin
           Inc(i);
       end ;
   end;
//   SaveGridToFile(SG2, 'C:\Grid'+IntToStr(i)+'_vege.txt');
   // Sorsz�mozzuk f�l a t�bl�t
   for i := 1 to SG2.RowCount - 1 do begin
       SG2.Cells[0, i] := IntToStr(i);
   end;
   IdoSzamito;
end;

procedure TMegjaratbeDlg.FormCanResize(Sender: TObject; var NewWidth,
  NewHeight: Integer; var Resize: Boolean);
begin
	if ( ( NewWidth < 900 ) or (NewHeight < 600 ) ) then begin
   	Resize	:= false;
   end;
end;

procedure TMegjaratbeDlg.FormResize(Sender: TObject);
begin
   // A gombok elrendez�se
   SetHorEqual(Panel1, [BitElkuld, BitKilep] );
end;

procedure TMegjaratbeDlg.BitBtn54Click(Sender: TObject);
var
   i           : integer;
   NewGroup    : TRadioGroup;
   j           : integer;
begin
   if SG1.Cells[2, 1] = '' then begin
       Exit;
   end;
   if kellkerdes then begin
       if NoticeKi('Val�ban t�r�lni k�v�nja a kijel�lt sort?', NOT_QUESTION) <> 0 then begin
           Exit;
       end;
   end;
   kellkerdes  := true;
   // A sor kit�rl�se a t�bl�zatb�l
   for i := SG1.Row to SG1.RowCount - 2 do begin
       ( SG1.Objects[1, i] as TRadioGroup ).ItemIndex := ( SG1.Objects[1, i+1] as TRadioGroup ).ItemIndex;
       for j := 2 to SG1.ColCount - 1 do begin
           SG1.Cells[j, i] := SG1.Cells[j, i+1];
       end;
   end;
   NewGroup  := SG1.Objects[1, SG1.RowCount - 1] as TRadioGroup;
   NewGroup.Destroy;
   if SG1.RowCount > 2 then begin
       GridTorles( SG1, SG1.RowCount - 1 );
   end else begin
       SG1.Rows[1].Clear;
   end;
   Csoportosito;
   // Sorsz�mozzuk f�l a t�bl�t
   for i := 1 to SG1.RowCount - 1 do begin
       SG1.Cells[0, i] := IntToStr(i);
   end;
   SG1.Repaint;
   if SG1.Cells[2, 1] = '' then begin
       BitBtn54.Enabled    := false;
   end;
end;

procedure TMegjaratbeDlg.BitBtn53Click(Sender: TObject);
var
   MEGIDLG : TMegbizasUjFmDlg;
   recsz   : integer;
begin
   // �j sor hozz�ad�sa
   Application.CreateForm(TMegbizasUjFmDlg, MEGIDLG);
   MEGIDLG.valaszt             := true;
   MEGIDLG.multiselect         := false;
   MEGIDLG.DBGrid2.PopupMenu   := nil;
   MEGIDLG.ShowModal;
   if MEGIDLG.ret_mskod <> '' then begin
   // if MEGIDLG.ret_msid <> '' then begin

       // Az �j rekord beilleszt�se
       Query_Run(Query1, 'SELECT * FROM MEGSEGED WHERE MS_MSKOD = ' + MEGIDLG.ret_mskod);
       // Query_Run(Query1, 'SELECT * FROM MEGSEGED WHERE MS_ID = ' + MEGIDLG.ret_msid);  // mert ez egyedi
       if KellRekord then begin    // Ellen�rizz�k, hogy a rekord megfelel�-e
           recsz           := SG1.RowCount;
           SG1.RowCount    := SG1.RowCount + 1;
           if SG1.Cells[2, 1] = '' then begin
               recsz           := 1;
               SG1.RowCount    := 2;
           end;
           while not Query1.Eof do begin
               Hozzafuzes(recsz);
               Inc(recsz);
               Query1.Next;
           end;
           Csoportosito;
           BitBtn54.Enabled    := true;
       end;
   end;
   MEGIDLG.Destroy;
end;

procedure TMegjaratbeDlg.Timer1Timer(Sender: TObject);
begin
   SG1.Repaint;
end;

function TMegjaratbeDlg.KellRekord : boolean;
var
   i   : integer;
begin
   Result  := false;
   for i := 1 to SG1.RowCount -1 do begin
       if ( ( SG1.Cells[7, i] = Query1.FieldByName('MS_MBKOD').AsString ) and ( SG1.Cells[8, i] = Query1.FieldByName('MS_SORSZ').AsString ) ) then begin
           // A rekord m�r l�tezik a t�bl�zatban
           Exit;
       end;
   end;
   Result  := true;
end;

procedure TMegjaratbeDlg.BitElkuldClick(Sender: TObject);
var
	QueryDat1	: TADOQuery;
   jakod       : string;
   alkod       : string;
   kezdat      : string;
   kezido      : string;
   vegdat      : string;
   vegido      : string;
   orsza       : string;
   potsz       : string;
   rensz       : string;
   sof1        : string;
   sof2        : string;
   i           : integer;
   j           : integer;
   k           : integer;
   l           : integer;
   tipus       : integer;
   ujsorszam   : integer;
   ujkodszam   : integer;
   jaratszam   : string;
   tipusstr    : string;
   masiktipus  : string;
   voltelem    : boolean;
begin
   i := 1;
   while i < SG2.RowCount do begin
       if SG2.Cells[15, i] <> '1' then begin
           NoticeKi('Le nem kezelt t�tel !!!');
           SG2.Row := i;
           Exit;
       end;
       Inc(i);
   end;

   // A t�ny d�tumok megl�t�nek vizsg�lata
   j           := 1;
   voltelem    := false;
   while j < SG1.RowCount do begin
       tipus := ( SG1.Objects[1, j] as TRadioGroup ).ItemIndex;
       Query_Run (Query2, 'SELECT * FROM MEGSEGED WHERE MS_MSKOD = '+SG1.Cells[2, j]);
       // Query_Run (Query2, 'SELECT * FROM MEGSEGED WHERE MS_ID = '+SG1.Cells[2, j]);
       if ( ( tipus = 0 ) and (Query2.FieldByName('MS_FETIDO').AsString <> '') ) then begin
           NoticeKi('L�tez� felrak�s t�nyid�!');
           SG1.Row := j;
           j   := SG1.RowCount;
           voltelem    := true;
       end;
       if ( ( tipus = 1 ) and (Query2.FieldByName('MS_LETIDO').AsString <> '') ) then begin
           NoticeKi('L�tez� lerak� t�nyid�!');
           SG1.Row := j;
           j   := SG1.RowCount;
           voltelem    := true;
       end;
       Inc(j);
   end;
   if voltelem then begin
       Exit;
   end;

   // A j�rat legener�l�sa
	QueryDat1 		:= TADOQuery.Create(nil);
	QueryDat1.Tag  	:= QUERY_ADAT_TAG;
	EgyebDlg.SeTADOQueryDatabase(QueryDat1);

   if not Jodatum2(M1) then begin
       Exit;
   end;
   if CBRendszam.Text  = '' then begin
       NoticeKi('A rendsz�m kiv�laszt�sa k�telez�');
       Exit;
   end;

   if CBSofor.Text  = '' then begin
       if NoticeKi('A sof�r nincs kiv�lasztva! Folytatja?', NOT_QUESTION) <> 0 then begin
           Exit;
       end;
   end;

   Screen.Cursor   := crHourGlass;
   // L�tre kell hozni az �j j�ratot
   jakod	    := Format('%6.6d',[GetNextStrCode('JARAT', 'JA_KOD', 0, 6)]);
   alkod	    := Format('%5.5d',[ GetJaratAlkod(copy(M1.Text, 1, 4))]);
   jaratszam   := 'HU-'+ alkod;

   {Az �j j�rat l�trehoz�sa}
   Query_Run ( QueryDat1, 'INSERT INTO JARAT (JA_KOD, JA_ALVAL, JA_ALKOD) VALUES ('''+jakod+''', -2, '''+alkod+''' )',true);

   kezdat  := M1.Text;
   kezido  := M2.text;
   vegdat  := M3.text;
   vegido  := M4.Text;
   orsza   := 'HU';
   rensz   := CBRendszam.Text;
   potsz   := CBPotkocsi.Text;
   sof1    := lisof[CBSofor.ItemIndex];
   sof2    := lisof[CBSofor2.ItemIndex];

   if sof1 <> '' then begin
       Query_Insert(  QueryDat1, 'JARSOFOR' ,
           [
           'JS_JAKOD',     ''''+jakod+'''',
           'JS_SOFOR',     ''''+sof1+'''',
           'JS_JADIJ',     SqlSzamString(StringSzam(Query_select('GEPKOCSI', 'GK_RESZ', rensz, 'GK_DIJ1')),8,2),
           'JS_SORSZ',     '1'
           ]);
   end;

   if sof2 <> '' then begin
       Query_Insert(  QueryDat1, 'JARSOFOR' ,
           [
           'JS_JAKOD',     ''''+jakod+'''',
           'JS_SOFOR',     ''''+sof2+'''',
           'JS_JADIJ',     SqlSzamString(StringSzam(Query_select('GEPKOCSI', 'GK_RESZ', rensz, 'GK_DIJ1')),8,2),
           'JS_SORSZ',     '2'
           ]);
   end;

   if potsz <> '' then begin
       Query_Insert(  QueryDat1, 'JARPOTOS' ,
           [
           'JP_JAKOD',     ''''+jakod+'''',
           'JP_POREN',     ''''+potsz+'''',
           'JP_JPKOD',     IntToStr(GetNextCode('JARPOTOS', 'JP_JPKOD', 1, 0))
           ]);
   end;

   tipusstr    := '';
   if SG2.Cols[1].IndexOf('0') > 0 then begin
       tipusstr := tipusstr + 'el�rak�s,';
   end;
   if SG2.Cols[1].IndexOf('1') > 0 then begin
      tipusstr    := tipusstr + 'lerak�,';
   end;
   tipusstr := copy(tipusstr, 1, Length(tipusstr)-1);

   Query_Update ( QueryDat1, 'JARAT' , [
           'JA_ORSZ',		''''+orsza+'''',
           'JA_EUROS',     '2',
           'JA_ALKOD',		alkod,
           'JA_MEGJ',		''''+'Gener�lt j�rat'+'''',
           'JA_JARAT',		''''+''+'''',
           'JA_JKEZD',		''''+kezdat+'''',
           'JA_KEIDO',		''''+kezido+'''',
           'JA_JVEGE',		''''+vegdat+'''',
           'JA_VEIDO',		''''+vegido+'''',
    //       'JA_JAORA',		idotart,
    //       'JA_LFDAT',		''''+M2B.Text+'''',
           'JA_RENDSZ',	''''+rensz+'''',
    //       'JA_MENSZ',		''''+M13.Text+'''',
           'JA_FAZIS',		'0',
           'JA_FAZST',     ''''+GetJaratFazis(0)+'''',
           'JA_MSFAJ',     ''''+tipusstr+'''',
           'JA_FAJKO',     '1'
           ], ' WHERE JA_KOD = '''+jakod+''' ' );

   QueryDat1.Destroy;

   JaratCalc(jakod);
   // A MEGSEGED rekordok l�trehoz�sa
   for i := 1 to SG2.RowCount - 1 do begin
       for j := 1 to SG1.RowCount - 1 do begin
           // V�gigmegy�nk a kiindul� t�bl�zaton �s ha megtal�ljuk, hogy melyik �sszes�tett sor van hozz�, akkor l�trehozzuk a megfelel� rekordokat
           tipus   := StrToIntDef(SG2.Cells[1, i], -1);
           if ( ( ( SG1.Objects[1, j] as TRadioGroup ).ItemIndex = tipus ) and
                (
                ( (tipus = 0) and (SG1.Cells[3, j] = SG2.Cells[3, i]) and (SG1.Cells[4, j] = SG2.Cells[4, i]) ) or
                ( (tipus = 1) and (SG1.Cells[5, j] = SG2.Cells[5, i]) and (SG1.Cells[6, j] = SG2.Cells[6, i]) )
                ) ) then begin
               Query_Run (Query2, 'SELECT * FROM MEGSEGED WHERE MS_MSKOD = '+SG1.Cells[2, j]);
               // Query_Run (Query2, 'SELECT * FROM MEGSEGED WHERE MS_ID = '+SG1.Cells[2, j]);
               if Query2.RecordCount <> 1 then begin
                   NoticeKi('Nem egyedi az MSKOD ('+SG1.Cells[2, j]+')');
                   // NoticeKi('Nem egyedi az MSID ('+SG1.Cells[2, j]+')');  // ... az�rt ez meglepi lenne
                   Exit;
               end;
               // Megtal�ltuk a j� elemet -> megseged l�trehoz�sa

               // A sorsz�m ellen�rz�se az SGSOR t�bl�ban
               ujsorszam	:= 1;
               for k := 0 to SGSor.RowCount - 1 do begin
                   if ( ( SGSor.Cells[0, k] = Query2.FieldByName('MS_MBKOD').AsString ) and
                        ( SGSor.Cells[1, k] = Query2.FieldByName('MS_SORSZ').AsString ) ) then begin
                       ujsorszam := StrToIntDef( SGSor.Cells[2, k], 1 ) ;
                   end;
               end;
               if ujsorszam = 1 then begin
                   if Query_Run (Query1, 'SELECT MAX(MS_SORSZ) MAXKOD FROM MEGSEGED WHERE MS_MBKOD = '+Query2.FieldByName('MS_MBKOD').AsString,true) then begin
                       ujsorszam	:= StrToIntDef(Query1.FieldByName('MAXKOD').AsString,0) + 1;
                   end;
                   SGSor.RowCount := SGSor.RowCount + 1;
                   SGSor.Cells[0, SGSor.RowCount - 1] := Query2.FieldByName('MS_MBKOD').AsString;
                   SGSor.Cells[1, SGSor.RowCount - 1] := Query2.FieldByName('MS_SORSZ').AsString;
                   SGSor.Cells[2, SGSor.RowCount - 1] := IntToStr(ujsorszam);
               end;

               ujkodszam 	:= GetNextCode('MEGSEGED', 'MS_MSKOD', 1, 0);
               case tipus of
                   0 : // �j felrak� l�trehoz�sa
                   begin
                       Query_Run(QueryKoz, 'SELECT * FROM FELRAKO WHERE FF_FEKOD = '+EgyebDlg.TELEPFELKOD);
                       for l := 0 to 1 do begin
                           masiktipus := 'fel';
                           if l = 1 then begin
                               masiktipus := 'le';
                           end;
                           Query_Insert (Query1, 'MEGSEGED',
                           [
                           'MS_MSKOD',     IntToStr(ujkodszam),
                           'MS_MBKOD',     Query2.FieldByName('MS_MBKOD').AsString,
                           'MS_SORSZ',     IntToStr(ujsorszam),
                           'MS_FELNEV', 	''''+Query2.FieldByName('MS_FELNEV').AsString+'''',
                           'MS_FELIR', 	''''+Query2.FieldByName('MS_FELIR').AsString+'''',
                           'MS_FELTEL', 	''''+Query2.FieldByName('MS_FELTEL').AsString+'''',
                           'MS_FELCIM', 	''''+Query2.FieldByName('MS_FELCIM').AsString+'''',
                           'MS_FELSE1',	''''+Query2.FieldByName('MS_FELSE1').AsString+'''',
                           'MS_FELSE2',	''''+Query2.FieldByName('MS_FELSE2').AsString+'''',
                           'MS_FELSE3',	''''+Query2.FieldByName('MS_FELSE3').AsString+'''',
                           'MS_FELSE4',	''''+Query2.FieldByName('MS_FELSE4').AsString+'''',
                           'MS_FELSE5',	''''+Query2.FieldByName('MS_FELSE5').AsString+'''',
                           'MS_LOADID',	''''+Query2.FieldByName('MS_LOADID').AsString+'''',
                           'MS_FELDAT', 	''''+Query2.FieldByName('MS_FELDAT').AsString+'''',
                           'MS_FELDAT2', 	''''+Query2.FieldByName('MS_FELDAT2').AsString+'''',
                           'MS_FELIDO', 	''''+Query2.FieldByName('MS_FELIDO').AsString+'''',
                           'MS_FELIDO2', 	''''+Query2.FieldByName('MS_FELIDO2').AsString+'''',
                           'MS_FERKDAT', 	''''+Query2.FieldByName('MS_FERKDAT').AsString+'''',
                           'MS_FERKIDO', 	''''+Query2.FieldByName('MS_FERKIDO').AsString+'''',
                           'MS_FETED',		''''+Query2.FieldByName('MS_FETED').AsString+'''',
                           'MS_FETED2',	''''+Query2.FieldByName('MS_FETED2').AsString+'''',
                           'MS_FETEI',		''''+Query2.FieldByName('MS_FETEI').AsString+'''',
                           'MS_FETEI2',	''''+Query2.FieldByName('MS_FETEI2').AsString+'''',
                           'MS_TEIDO', 	''''+Query2.FieldByName('MS_TEIDO').AsString+'''',
                           'MS_FSULY', 	SqlSzamString(StringSzam(Query2.FieldByName('MS_FSULY').AsString),8,2),
                           'MS_FEPAL', 	SqlSzamString(StringSzam(Query2.FieldByName('MS_FEPAL').AsString),8,2),
                           'MS_FCPAL', 	SqlSzamString(StringSzam(Query2.FieldByName('MS_FCPAL').AsString),8,2),
                           'MS_LERNEV', 	''''+Query2.FieldByName('MS_LERNEV').AsString+'''',
                           'MS_LELIR', 	''''+Query2.FieldByName('MS_LELIR').AsString+'''',
                           'MS_LERTEL', 	''''+Query2.FieldByName('MS_LERTEL').AsString+'''',
                           'MS_LERCIM', 	''''+Query2.FieldByName('MS_LERCIM').AsString+'''',
                           'MS_LERSE1',	''''+Query2.FieldByName('MS_LERSE1').AsString+'''',
                           'MS_LERSE2',	''''+Query2.FieldByName('MS_LERSE2').AsString+'''',
                           'MS_LERSE3',	''''+Query2.FieldByName('MS_LERSE3').AsString+'''',
                           'MS_LERSE4',	''''+Query2.FieldByName('MS_LERSE4').AsString+'''',
                           'MS_LERSE5',	''''+Query2.FieldByName('MS_LERSE5').AsString+'''',
                           'MS_LETED',		''''+Query2.FieldByName('MS_LETED').AsString+'''',
                           'MS_LETED2',	''''+Query2.FieldByName('MS_LETED2').AsString+'''',
                           'MS_LETEI',		''''+Query2.FieldByName('MS_LETEI').AsString+'''',
                           'MS_LETEI2',	''''+Query2.FieldByName('MS_LETEI2').AsString+'''',

                           // A t�ny lerak�hoz ker�lnek a tervezett lerak� adatai
                           'MS_TENNEV', 	''''+QueryKoz.FieldByName('FF_FELNEV').AsString+'''',
                           'MS_TENOR',		''''+QueryKoz.FieldByName('FF_ORSZA').AsString+'''',
                           'MS_TENIR', 	''''+QueryKoz.FieldByName('FF_FELIR').AsString+'''',
                           'MS_TENTEL', 	''''+QueryKoz.FieldByName('FF_FELTEL').AsString+'''',
                           'MS_TENCIM', 	''''+QueryKoz.FieldByName('FF_FELCIM').AsString+'''',
                           'MS_TENYL1',	''''+QueryKoz.FieldByName('FF_FELSE1').AsString+'''',
                           'MS_TENYL2',	''''+QueryKoz.FieldByName('FF_FELSE2').AsString+'''',
                           'MS_TENYL3',	''''+QueryKoz.FieldByName('FF_FELSE3').AsString+'''',
                           'MS_TENYL4',	''''+QueryKoz.FieldByName('FF_FELSE4').AsString+'''',
                           'MS_TENYL5',	''''+QueryKoz.FieldByName('FF_FELSE5').AsString+'''',

                           // A t�ny felrak�hoz ker�lnek a mostani t�ny lerak� adatai
                           'MS_TEFNEV', 	''''+Query2.FieldByName('MS_TEFNEV').AsString+'''',
                           'MS_TEFOR',		''''+Query2.FieldByName('MS_TEFOR').AsString+'''',
                           'MS_TEFIR', 	''''+Query2.FieldByName('MS_TEFIR').AsString+'''',
                           'MS_TEFTEL', 	''''+Query2.FieldByName('MS_TEFTEL').AsString+'''',
                           'MS_TEFCIM', 	''''+Query2.FieldByName('MS_TEFCIM').AsString+'''',
                           'MS_TEFL1',		''''+Query2.FieldByName('MS_TEFL1').AsString+'''',
                           'MS_TEFL2',		''''+Query2.FieldByName('MS_TEFL2').AsString+'''',
                           'MS_TEFL3',		''''+Query2.FieldByName('MS_TEFL3').AsString+'''',
                           'MS_TEFL4',		''''+Query2.FieldByName('MS_TEFL4').AsString+'''',
                           'MS_TEFL5',		''''+Query2.FieldByName('MS_TEFL5').AsString+'''',
                           'MS_FETDAT', 	''''+Query2.FieldByName('MS_FETDAT').AsString+'''',
                           'MS_FETIDO', 	''''+Query2.FieldByName('MS_FETIDO').AsString+'''',

                           'MS_JAKOD',     ''''+jakod+'''',
                           'MS_JASZA',     ''''+jaratszam+'''',
                           'MS_LERDAT', 	''''+SG2.Cells[11, i]+'''',
                           'MS_LERDAT2', 	''''+SG2.Cells[13, i]+'''',
                           'MS_LERIDO', 	''''+SG2.Cells[12, i]+'''',
                           'MS_LERIDO2', 	''''+SG2.Cells[14, i]+'''',
                           'MS_LERKDAT', 	''''+SG2.Cells[11, i]+'''',
                           'MS_LERKIDO', 	''''+''+'''',
                           'MS_LETDAT', 	''''+SG2.Cells[11, i]+'''',
                           'MS_LETIDO', 	''''+''+'''',
                           'MS_FORSZ',		''''+Query2.FieldByName('MS_FORSZ').AsString+'''',
                           'MS_ORSZA',		''''+Query2.FieldByName('MS_ORSZA').AsString+'''',
                           'MS_EXPOR',		IntToStr(StrToIntDef(Query2.FieldByName('MS_EXPOR').AsString, 0)),
                           'MS_EXSTR', 	''''+GetExportStr(StrToIntDef(Query2.FieldByName('MS_EXPOR').AsString, -1))+'''',
                           'MS_LSULY', 	SqlSzamString(StringSzam(Query2.FieldByName('MS_LSULY').AsString),8,2),
                           'MS_LEPAL', 	SqlSzamString(StringSzam(Query2.FieldByName('MS_LEPAL').AsString),8,2),
                           'MS_LCPAL', 	SqlSzamString(StringSzam(Query2.FieldByName('MS_LCPAL').AsString),8,2),
                           'MS_DATUM',     ''''+SG2.Cells[11, i]+'''',
                           'MS_IDOPT',     ''''+SG2.Cells[12, i]+'''',
                           'MS_TIPUS', 	IntToStr(l),
                           'MS_TINEV', 	''''+masiktipus+'''',
                           'MS_AKTNEV', 	''''+Query2.FieldByName('MS_AKTNEV').AsString+'''',
                           'MS_AKTTEL', 	''''+Query2.FieldByName('MS_AKTTEL').AsString+'''',
                           'MS_AKTCIM', 	''''+Query2.FieldByName('MS_AKTCIM').AsString+'''',
                           'MS_AKTOR', 	''''+Query2.FieldByName('MS_AKTOR').AsString+'''',
                           'MS_AKTIR', 	''''+Query2.FieldByName('MS_AKTIR').AsString+'''',
                           'MS_REGIK',		'0',

                           'MS_RENSZ', 	''''+CBRendszam.Text+'''',
                           'MS_POTSZ', 	''''+CBPotkocsi.Text+'''',
                           'MS_DOKOD', 	''''+sof1+'''',
                           'MS_DOKO2', 	''''+sof2+'''',
                           'MS_GKKAT', 	''''+Query_Select('GEPKOCSI','GK_RESZ', CBRendszam.Text, 'GK_GKKAT')+'''',
                           'MS_SNEV1', 	''''+Query_Select('DOLGOZO','DO_KOD', sof1, 'DO_NAME')+'''',
                           'MS_SNEV2', 	''''+Query_Select('DOLGOZO','DO_KOD', sof2, 'DO_NAME')+'''',
                           'MS_KIFON',		IntToStr(StrToIntDef(Query2.FieldByName('MS_KIFON').AsString, 0)),
                           'MS_CSPAL',		IntToStr(StrToIntDef(Query2.FieldByName('MS_CSPAL').AsString, 0)),
                           'MS_FAJKO', 	'1',
                           'MS_FAJTA',		''''+'el�rak�s'+'''',
                           'MS_FELARU', 	''''+Query2.FieldByName('MS_FELARU').AsString+'''',
                           'MS_HFOK1', 	''''+Query2.FieldByName('MS_HFOK1').AsString+'''' ,
                           'MS_HFOK2', 	''''+Query2.FieldByName('MS_HFOK2').AsString+''''
                           ]);
                       end;

                       // Az eredeti megv�ltoztat�sa
                       Query_Update (Query1, 'MEGSEGED',
                           [
                           'MS_TEFNEV', 	''''+QueryKoz.FieldByName('FF_FELNEV').AsString+'''',
                           'MS_TEFOR',		''''+QueryKoz.FieldByName('FF_ORSZA').AsString+'''',
                           'MS_TEFIR', 	''''+QueryKoz.FieldByName('FF_FELIR').AsString+'''',
                           'MS_TEFTEL', 	''''+QueryKoz.FieldByName('FF_FELTEL').AsString+'''',
                           'MS_TEFCIM', 	''''+QueryKoz.FieldByName('FF_FELCIM').AsString+'''',
                           'MS_TEFL1',		''''+QueryKoz.FieldByName('FF_FELSE1').AsString+'''',
                           'MS_TEFL2',		''''+QueryKoz.FieldByName('FF_FELSE2').AsString+'''',
                           'MS_TEFL3',		''''+QueryKoz.FieldByName('FF_FELSE3').AsString+'''',
                           'MS_TEFL4',		''''+QueryKoz.FieldByName('FF_FELSE4').AsString+'''',
                           'MS_TEFL5',		''''+QueryKoz.FieldByName('FF_FELSE5').AsString+'''',
                           'MS_FETDAT', 	''''+SG2.Cells[11, i]+'''',
                           'MS_FETIDO', 	''''+''+'''',
                           'MS_FELDAT', 	''''+SG2.Cells[11, i]+'''',
                           'MS_FELDAT2', 	''''+SG2.Cells[13, i]+'''',
                           'MS_FERKDAT', 	''''+SG2.Cells[11, i]+'''',
                           'MS_FERKIDO', 	''''+''+'''',
                           'MS_FELIDO', 	''''+SG2.Cells[12, i]+'''',
                           'MS_FELIDO2', 	''''+SG2.Cells[14, i]+''''
                           ], ' WHERE MS_MBKOD = '+Query2.FieldByName('MS_MBKOD').AsString+' AND MS_SORSZ = '+Query2.FieldByName('MS_SORSZ').AsString);

                       // A JARATMEGSEGED t�bla t�lt�se
                       Query_Run ( Query1, 'INSERT INTO JARATMEGSEGED ( JG_JAKOD, JG_MBKOD, JG_MSKOD, JG_FOJAR ) VALUES ( '+
                           ''''+ jakod + ''', '+Query2.FieldByName('MS_MBKOD').AsString +','+ IntToStr(ujkodszam) +','''+ Query_Select('MEGBIZAS', 'MB_MBKOD', Query2.FieldByName('MS_MBKOD').AsString, 'MB_JAKOD')+''' )');
                   end;

                   1 : // �j lerak� l�trehoz�sa
                   begin
                       Query_Run(QueryKoz, 'SELECT * FROM FELRAKO WHERE FF_FEKOD = '+EgyebDlg.TELEPFELKOD);
                       for l := 0 to 1 do begin
                           masiktipus := 'fel';
                           if l = 1 then begin
                               masiktipus := 'le';
                           end;
                           Query_Insert (Query1, 'MEGSEGED',
                           [
                           'MS_MSKOD',     IntToStr(ujkodszam),
                           'MS_MBKOD',     Query2.FieldByName('MS_MBKOD').AsString,
                           'MS_SORSZ',     IntToStr(ujsorszam),
                           'MS_FELNEV', 	''''+Query2.FieldByName('MS_FELNEV').AsString+'''',
                           'MS_FELIR', 	''''+Query2.FieldByName('MS_FELIR').AsString+'''',
                           'MS_FELTEL', 	''''+Query2.FieldByName('MS_FELTEL').AsString+'''',
                           'MS_FELCIM', 	''''+Query2.FieldByName('MS_FELCIM').AsString+'''',
                           'MS_FELSE1',	''''+Query2.FieldByName('MS_FELSE1').AsString+'''',
                           'MS_FELSE2',	''''+Query2.FieldByName('MS_FELSE2').AsString+'''',
                           'MS_FELSE3',	''''+Query2.FieldByName('MS_FELSE3').AsString+'''',
                           'MS_FELSE4',	''''+Query2.FieldByName('MS_FELSE4').AsString+'''',
                           'MS_FELSE5',	''''+Query2.FieldByName('MS_FELSE5').AsString+'''',
                           'MS_LOADID',	''''+Query2.FieldByName('MS_LOADID').AsString+'''',
                           'MS_FELDAT', 	''''+SG2.Cells[7, i]+'''',
                           'MS_FELDAT2', 	''''+SG2.Cells[9, i]+'''',
                           'MS_FELIDO', 	''''+SG2.Cells[8, i]+'''',
                           'MS_FELIDO2', 	''''+SG2.Cells[10, i]+'''',
                           'MS_FERKDAT', 	''''+SG2.Cells[7, i]+'''',
                           'MS_FERKIDO', 	''''+''+'''',
                           'MS_FETDAT', 	''''+SG2.Cells[7, i]+'''',
                           'MS_FETIDO', 	''''+''+'''',
                           'MS_LERKDAT', 	''''+Query2.FieldByName('MS_LERKDAT').AsString+'''',
                           'MS_LERKIDO', 	''''+Query2.FieldByName('MS_LERKIDO').AsString+'''',
                           'MS_FETED',		''''+Query2.FieldByName('MS_FETED').AsString+'''',
                           'MS_FETED2',	''''+Query2.FieldByName('MS_FETED2').AsString+'''',
                           'MS_FETEI',		''''+Query2.FieldByName('MS_FETEI').AsString+'''',
                           'MS_FETEI2',	''''+Query2.FieldByName('MS_FETEI2').AsString+'''',
                           'MS_TEIDO', 	''''+Query2.FieldByName('MS_TEIDO').AsString+'''',
                           'MS_FSULY', 	SqlSzamString(StringSzam(Query2.FieldByName('MS_FSULY').AsString),8,2),
                           'MS_FEPAL', 	SqlSzamString(StringSzam(Query2.FieldByName('MS_FEPAL').AsString),8,2),
                           'MS_FCPAL', 	SqlSzamString(StringSzam(Query2.FieldByName('MS_FCPAL').AsString),8,2),
                           'MS_LERNEV', 	''''+Query2.FieldByName('MS_LERNEV').AsString+'''',
                           'MS_LELIR', 	''''+Query2.FieldByName('MS_LELIR').AsString+'''',
                           'MS_LERTEL', 	''''+Query2.FieldByName('MS_LERTEL').AsString+'''',
                           'MS_LERCIM', 	''''+Query2.FieldByName('MS_LERCIM').AsString+'''',
                           'MS_LERSE1',	''''+Query2.FieldByName('MS_LERSE1').AsString+'''',
                           'MS_LERSE2',	''''+Query2.FieldByName('MS_LERSE2').AsString+'''',
                           'MS_LERSE3',	''''+Query2.FieldByName('MS_LERSE3').AsString+'''',
                           'MS_LERSE4',	''''+Query2.FieldByName('MS_LERSE4').AsString+'''',
                           'MS_LERSE5',	''''+Query2.FieldByName('MS_LERSE5').AsString+'''',
                           'MS_LETED',		''''+Query2.FieldByName('MS_LETED').AsString+'''',
                           'MS_LETED2',	''''+Query2.FieldByName('MS_LETED2').AsString+'''',
                           'MS_LETEI',		''''+Query2.FieldByName('MS_LETEI').AsString+'''',
                           'MS_LETEI2',	''''+Query2.FieldByName('MS_LETEI2').AsString+'''',

                           // A t�ny lerak�hoz ker�lnek a tervezett lerak� adatai
                           'MS_TENNEV', 	''''+Query2.FieldByName('MS_TENNEV').AsString+'''',
                           'MS_TENOR',		''''+Query2.FieldByName('MS_TENOR').AsString+'''',
                           'MS_TENIR', 	''''+Query2.FieldByName('MS_TENIR').AsString+'''',
                           'MS_TENTEL', 	''''+Query2.FieldByName('MS_TENTEL').AsString+'''',
                           'MS_TENCIM', 	''''+Query2.FieldByName('MS_TENCIM').AsString+'''',
                           'MS_TENYL1',	''''+Query2.FieldByName('MS_TENYL1').AsString+'''',
                           'MS_TENYL2',	''''+Query2.FieldByName('MS_TENYL2').AsString+'''',
                           'MS_TENYL3',	''''+Query2.FieldByName('MS_TENYL3').AsString+'''',
                           'MS_TENYL4',	''''+Query2.FieldByName('MS_TENYL4').AsString+'''',
                           'MS_TENYL5',	''''+Query2.FieldByName('MS_TENYL5').AsString+'''',
                           'MS_LETDAT', 	''''+Query2.FieldByName('MS_LETDAT').AsString+'''',
                           'MS_LETIDO', 	''''+Query2.FieldByName('MS_LETIDO').AsString+'''',

                           // A t�ny felrak�hoz ker�lnek a mostani t�ny lerak� adatai
                           'MS_TEFNEV', 	''''+QueryKoz.FieldByName('FF_FELNEV').AsString+'''',
                           'MS_TEFOR',		''''+QueryKoz.FieldByName('FF_ORSZA').AsString+'''',
                           'MS_TEFIR', 	''''+QueryKoz.FieldByName('FF_FELIR').AsString+'''',
                           'MS_TEFTEL', 	''''+QueryKoz.FieldByName('FF_FELTEL').AsString+'''',
                           'MS_TEFCIM', 	''''+QueryKoz.FieldByName('FF_FELCIM').AsString+'''',
                           'MS_TEFL1',		''''+QueryKoz.FieldByName('FF_FELSE1').AsString+'''',
                           'MS_TEFL2',		''''+QueryKoz.FieldByName('FF_FELSE2').AsString+'''',
                           'MS_TEFL3',		''''+QueryKoz.FieldByName('FF_FELSE3').AsString+'''',
                           'MS_TEFL4',		''''+QueryKoz.FieldByName('FF_FELSE4').AsString+'''',
                           'MS_TEFL5',		''''+QueryKoz.FieldByName('FF_FELSE5').AsString+'''',
                           'MS_LERDAT', 	''''+Query2.FieldByName('MS_LERDAT').AsString+'''',
                           'MS_LERDAT2', 	''''+Query2.FieldByName('MS_LERDAT2').AsString+'''',
                           'MS_LERIDO', 	''''+Query2.FieldByName('MS_LERIDO').AsString+'''',
                           'MS_LERIDO2', 	''''+Query2.FieldByName('MS_LERIDO2').AsString+'''',
                           'MS_FORSZ',		''''+Query2.FieldByName('MS_FORSZ').AsString+'''',
                           'MS_ORSZA',		''''+Query2.FieldByName('MS_ORSZA').AsString+'''',
                           'MS_EXPOR',		IntToStr(StrToIntDef(Query2.FieldByName('MS_EXPOR').AsString, 0)),
                           'MS_EXSTR', 	''''+GetExportStr(StrToIntDef(Query2.FieldByName('MS_EXPOR').AsString, -1))+'''',
                           'MS_LSULY', 	SqlSzamString(StringSzam(Query2.FieldByName('MS_LSULY').AsString),8,2),
                           'MS_LEPAL', 	SqlSzamString(StringSzam(Query2.FieldByName('MS_LEPAL').AsString),8,2),
                           'MS_LCPAL', 	SqlSzamString(StringSzam(Query2.FieldByName('MS_LCPAL').AsString),8,2),
                           'MS_DATUM',     ''''+SG2.Cells[7, i]+'''',
                           'MS_IDOPT',     ''''+SG2.Cells[8, i]+'''',
                           'MS_TIPUS', 	IntToStr(l),
                           'MS_TINEV', 	''''+masiktipus+'''',
                           'MS_AKTNEV', 	''''+Query2.FieldByName('MS_AKTNEV').AsString+'''',
                           'MS_AKTTEL', 	''''+Query2.FieldByName('MS_AKTTEL').AsString+'''',
                           'MS_AKTCIM', 	''''+Query2.FieldByName('MS_AKTCIM').AsString+'''',
                           'MS_AKTOR', 	''''+Query2.FieldByName('MS_AKTOR').AsString+'''',
                           'MS_AKTIR', 	''''+Query2.FieldByName('MS_AKTIR').AsString+'''',
                           'MS_REGIK',		'0',

                           'MS_RENSZ', 	''''+CBRendszam.Text+'''',
                           'MS_POTSZ', 	''''+CBPotkocsi.Text+'''',
                           'MS_DOKOD', 	''''+sof1+'''',
                           'MS_DOKO2', 	''''+sof2+'''',
                           'MS_GKKAT', 	''''+Query_Select('GEPKOCSI','GK_RESZ', CBRendszam.Text, 'GK_GKKAT')+'''',
                           'MS_SNEV1', 	''''+Query_Select('DOLGOZO','DO_KOD', sof1, 'DO_NAME')+'''',
                           'MS_SNEV2', 	''''+Query_Select('DOLGOZO','DO_KOD', sof2, 'DO_NAME')+'''',
                           'MS_KIFON',		IntToStr(StrToIntDef(Query2.FieldByName('MS_KIFON').AsString, 0)) ,
                           'MS_JAKOD',     ''''+jakod+'''',
                           'MS_JASZA',     ''''+jaratszam+'''',

                           'MS_CSPAL',		IntToStr(StrToIntDef(Query2.FieldByName('MS_CSPAL').AsString, 0)),
                           'MS_FAJKO', 	'3',
                           'MS_FAJTA',		''''+'lerak�'+'''',
                           'MS_FELARU', 	''''+Query2.FieldByName('MS_FELARU').AsString+'''',
                           'MS_HFOK1', 	''''+Query2.FieldByName('MS_HFOK1').AsString+'''' ,
                           'MS_HFOK2', 	''''+Query2.FieldByName('MS_HFOK2').AsString+''''
                           ]);
                       end;

                       // Az eredeti megv�ltoztat�sa
                       Query_Update (Query1, 'MEGSEGED',
                           [
                           'MS_TENNEV', 	''''+QueryKoz.FieldByName('FF_FELNEV').AsString+'''',
                           'MS_TENOR',		''''+QueryKoz.FieldByName('FF_ORSZA').AsString+'''',
                           'MS_TENIR', 	''''+QueryKoz.FieldByName('FF_FELIR').AsString+'''',
                           'MS_TENTEL', 	''''+QueryKoz.FieldByName('FF_FELTEL').AsString+'''',
                           'MS_TENCIM', 	''''+QueryKoz.FieldByName('FF_FELCIM').AsString+'''',
                           'MS_TENYL1',	''''+QueryKoz.FieldByName('FF_FELSE1').AsString+'''',
                           'MS_TENYL2',	''''+QueryKoz.FieldByName('FF_FELSE2').AsString+'''',
                           'MS_TENYL3',	''''+QueryKoz.FieldByName('FF_FELSE3').AsString+'''',
                           'MS_TENYL4',	''''+QueryKoz.FieldByName('FF_FELSE4').AsString+'''',
                           'MS_TENYL5',	''''+QueryKoz.FieldByName('FF_FELSE5').AsString+'''',

                           'MS_LETDAT', 	''''+SG2.Cells[7, i]+'''',
                           'MS_LETIDO', 	''''+''+'''',
                           'MS_LERDAT', 	''''+SG2.Cells[7, i]+'''',
                           'MS_LERDAT2', 	''''+SG2.Cells[9, i]+'''',
                           'MS_LERKDAT', 	''''+SG2.Cells[7, i]+'''',
                           'MS_LERKIDO', 	''''+''+'''',
                           'MS_LERIDO', 	''''+SG2.Cells[8, i]+'''',
                           'MS_LERIDO2', 	''''+SG2.Cells[10, i]+''''
                           ], ' WHERE MS_MBKOD = '+Query2.FieldByName('MS_MBKOD').AsString+' AND MS_SORSZ = '+Query2.FieldByName('MS_SORSZ').AsString);

                       // A JARATMEGSEGED t�bla t�lt�se
                       Query_Run ( Query1, 'INSERT INTO JARATMEGSEGED ( JG_JAKOD, JG_MBKOD, JG_MSKOD, JG_FOJAR ) VALUES ( '+
                           ''''+ jakod + ''', '+Query2.FieldByName('MS_MBKOD').AsString +','+ IntToStr(ujkodszam) +','''+ Query_Select('MEGBIZAS', 'MB_MBKOD', Query2.FieldByName('MS_MBKOD').AsString, 'MB_JAKOD')+''' )');
                   end;
               end;
           end;
       end;
   end;
   Screen.Cursor   := crDefault;
   ret_kod := '1';
   Close;
end;

procedure TMegjaratbeDlg.M1Exit(Sender: TObject);
begin
	DatumExit(Sender, false);
end;

procedure TMegjaratbeDlg.M2Exit(Sender: TObject);
begin
   IdoExit(Sender, false);
end;

procedure TMegjaratbeDlg.BitBtn1Click(Sender: TObject);
var
   tipus       : integer;
   captionstr  : string;
   i           : integer;
   mskod       : string;
begin
   // �res eset�n ne csin�ljunk semmit
   if SG2.Cells[1, SG2.Row] = '' then begin    // A t�pus �res
       Exit;
   end;

   // A r�szletes ablak megjelen�t�se
   tipus   := StrToIntDef(SG2.Cells[1, SG2.Row], -1);
   case tipus of
       0 : captionstr := 'El�rak�s defini�l�sa';
       1 : captionstr := 'Lerak� defini�l�sa';
   end;
   // A kiindul� megseged meghat�roz�sa
   i       := 1;
   mskod   := '';
   while i < SG1.RowCount do begin
       if ( ( ( SG1.Objects[1, i] as TRadioGroup ).ItemIndex = tipus ) and
            (
            ( (tipus = 0) and (SG1.Cells[3, i] = SG2.Cells[3, SG2.Row]) and (SG1.Cells[4, i] = SG2.Cells[4, SG2.Row]) ) or
            ( (tipus = 1) and (SG1.Cells[5, i] = SG2.Cells[5, SG2.Row]) and (SG1.Cells[6, i] = SG2.Cells[6, SG2.Row]) )
            ) ) then begin
           // Megtal�ltuk a j� elemet
           mskod   := SG1.Cells[2, i];
           i :=  SG1.RowCount;
       end;
       Inc(i);
   end;
   if mskod = ''  then begin
       NoticeKi('Nem tal�ltam kiindul� elemet!');
       Exit;
   end;
   // Felrak�s vagy lerak�s
   Screen.Cursor   := crHourGlass;
   Application.CreateForm(TMegjaratAl1Dlg, MegjaratAl1Dlg);
   MegjaratAl1Dlg.tipus    := tipus;
   MegjaratAl1Dlg.Tolto(captionstr, mskod);
   // Adatokat felt�lt�se
   MegjaratAl1Dlg.M5.Text      := SG2.Cells[ 7, SG2.Row];
   MegjaratAl1Dlg.M6.Text      := SG2.Cells[ 8, SG2.Row];
   MegjaratAl1Dlg.M5__.Text    := SG2.Cells[ 9, SG2.Row];
   MegjaratAl1Dlg.M6__.Text    := SG2.Cells[10, SG2.Row];
   MegjaratAl1Dlg.M50.Text     := SG2.Cells[11, SG2.Row];
   MegjaratAl1Dlg.M60.Text     := SG2.Cells[12, SG2.Row];
   MegjaratAl1Dlg.M50__.Text   := SG2.Cells[13, SG2.Row];
   MegjaratAl1Dlg.M60__.Text   := SG2.Cells[14, SG2.Row];
   if SG2.Cells[15, SG2.Row] <> '1' then begin
       case tipus of
           0 :
           begin
               // A d�tumok kezdetben �resek
               MegjaratAl1Dlg.M50.Text        := '';
               MegjaratAl1Dlg.M50__.Text      := '';
               MegjaratAl1Dlg.M60.Text        := '';
               MegjaratAl1Dlg.M60__.Text      := '';
           end;
           1 :
           begin
               // A d�tumok kezdetben �resek
               MegjaratAl1Dlg.M5.Text        := '';
               MegjaratAl1Dlg.M5__.Text      := '';
               MegjaratAl1Dlg.M6.Text        := '';
               MegjaratAl1Dlg.M6__.Text      := '';
           end;
       end;
   end;
   Screen.Cursor   := crDefault;
   MegjaratAl1Dlg.ShowModal;
   if MegjaratAl1Dlg.ret_kod <> '' then begin
       SG2.Cells[ 7, SG2.Row] := MegjaratAl1Dlg.M5.Text;
       SG2.Cells[ 8, SG2.Row] := MegjaratAl1Dlg.M6.Text;
       SG2.Cells[ 9, SG2.Row] := MegjaratAl1Dlg.M5__.Text;
       SG2.Cells[10, SG2.Row] := MegjaratAl1Dlg.M6__.Text;
       SG2.Cells[11, SG2.Row] := MegjaratAl1Dlg.M50.Text;
       SG2.Cells[12, SG2.Row] := MegjaratAl1Dlg.M60.Text;
       SG2.Cells[13, SG2.Row] := MegjaratAl1Dlg.M50__.Text;
       SG2.Cells[14, SG2.Row] := MegjaratAl1Dlg.M60__.Text;
       SG2.Cells[15, SG2.Row] := '1';
       BitElkuld.Enabled := true;
   end;
   MegjaratAl1Dlg.Destroy;
   SG2.Refresh;
   IdoSzamito;
end;

procedure TMegjaratbeDlg.SG2DrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
begin
   if ( ( ACol > 0 ) and ( ARow > 0 ) ) then begin
       SG2.Canvas.Brush.Color := clRed;
       if SG2.Cells[15, ARow] = '1' then begin
           SG2.Canvas.Brush.Color := clLime;
       end;
       SG2.Canvas.FillRect(Rect);
       Sg2.Canvas.TextOut(Rect.Left + 1, Rect.Top + 1, SG2.Cells[ACol, ARow]);
   end;
end;

procedure TMegjaratbeDlg.IdoSzamito;
var
   i : integer;
begin
   // V�gigmegy�nk a t�bl�zaton �s kit�ltj�k a kezd� il. befejez� id�t
   M1.Text := SG2.Cells[ 7, 1];
   M2.Text := SG2.Cells[ 8, 1];
   M3.Text := SG2.Cells[11, 1];
   M4.Text := SG2.Cells[12, 1];
   for i := 1 to SG2.RowCount - 1 do begin
       if M1.Text + ' ' + M2.Text > SG2.Cells[ 7, i] + ' ' + SG2.Cells[ 8, i] then begin
           M1.Text := SG2.Cells[ 7, i];
           M2.Text := SG2.Cells[ 8, i];
       end;
       if M3.Text + ' ' + M4.Text < SG2.Cells[11, i] + ' ' + SG2.Cells[12, i] then begin
           M3.Text := SG2.Cells[11, i];
           M4.Text := SG2.Cells[12, i];
       end;
       if M3.Text + ' ' + M4.Text < SG2.Cells[13, i] + ' ' + SG2.Cells[14, i] then begin
           M3.Text := SG2.Cells[13, i];
           M4.Text := SG2.Cells[14, i];
       end;
   end;
end;

procedure TMegjaratbeDlg.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
	EgyebDlg.FormReturn(Key);
end;


procedure TMegjaratbeDlg.Hozzafuzes(recsz : integer);
begin
   SG1.Cells[0, recsz]     := IntToStr(recsz);
   SG1.Cells[2, recsz]     := Query1.FieldByName('MS_MSKOD').AsString;
   // SG1.Cells[2, recsz]     := Query1.FieldByName('MS_ID').AsString;
   SG1.Cells[3, recsz]     := Query1.FieldByName('MS_FELNEV').AsString;
   SG1.Cells[4, recsz]     := Query1.FieldByName('MS_FORSZ').AsString+'-'+Query1.FieldByName('MS_FELIR').AsString+' '+
       Query1.FieldByName('MS_FELTEL').AsString+'; '+Query1.FieldByName('MS_FELCIM').AsString;
   SG1.Cells[5, recsz]     := Query1.FieldByName('MS_LERNEV').AsString;
   SG1.Cells[6, recsz]     := Query1.FieldByName('MS_ORSZA').AsString+'-'+Query1.FieldByName('MS_LELIR').AsString+' '+
       Query1.FieldByName('MS_LERTEL').AsString+'; '+Query1.FieldByName('MS_LERCIM').AsString;
   SG1.Cells[7, recsz]     := Query1.FieldByName('MS_MBKOD').AsString;
   SG1.Cells[8, recsz]     := Query1.FieldByName('MS_SORSZ').AsString;
   SG1.Cells[9, recsz]     := Query1.FieldByName('MS_TINEV').AsString;
   AddRadioGroup(recsz);
   // D�tumok/id�pontok felt�lt�se
   // Felrak�s
   SG1.Cells[10, recsz]    := Query1.FieldByName('MS_FETDAT').AsString;
   SG1.Cells[11, recsz]    := Query1.FieldByName('MS_FETIDO').AsString;
   SG1.Cells[12, recsz]    := '';
   SG1.Cells[13, recsz]    := '';
   if SG1.Cells[10, recsz] = '' then begin
       SG1.Cells[10, recsz]    := Query1.FieldByName('MS_FERKDAT').AsString;
       SG1.Cells[11, recsz]    := Query1.FieldByName('MS_FERKIDO').AsString;
       if SG1.Cells[10, recsz] = '' then begin
           SG1.Cells[10, recsz]    := Query1.FieldByName('MS_FELDAT').AsString;
           SG1.Cells[11, recsz]    := Query1.FieldByName('MS_FELIDO').AsString;
           SG1.Cells[12, recsz]    := Query1.FieldByName('MS_FELDAT2').AsString;
           SG1.Cells[13, recsz]    := Query1.FieldByName('MS_FELIDO2').AsString;
       end;
   end;
   if ( ( Query1.FieldByName('MS_FETDAT').AsString = Query1.FieldByName('MS_FELDAT').AsString ) and
        ( Query1.FieldByName('MS_FETIDO').AsString = '' ) ) then begin
       // �j rekordokn�l id�pont m�g nincs kit�ltve, a t�bbi meg azonos
       SG1.Cells[10, recsz]    := Query1.FieldByName('MS_FELDAT').AsString;
       SG1.Cells[11, recsz]    := Query1.FieldByName('MS_FELIDO').AsString;
       SG1.Cells[12, recsz]    := Query1.FieldByName('MS_FELDAT2').AsString;
       SG1.Cells[13, recsz]    := Query1.FieldByName('MS_FELIDO2').AsString;
   end;
   // Lerak�s
   SG1.Cells[14, recsz]    := Query1.FieldByName('MS_LETDAT').AsString;
   SG1.Cells[15, recsz]    := Query1.FieldByName('MS_LETIDO').AsString;
   SG1.Cells[16, recsz]    := '';
   SG1.Cells[17, recsz]    := '';
   if SG1.Cells[14, recsz] = '' then begin
       SG1.Cells[14, recsz]    := Query1.FieldByName('MS_LERDAT2').AsString;
       SG1.Cells[15, recsz]    := Query1.FieldByName('MS_LERIDO2').AsString;
       if SG1.Cells[14, recsz] = '' then begin
           SG1.Cells[14, recsz]    := Query1.FieldByName('MS_LERDAT').AsString;
           SG1.Cells[15, recsz]    := Query1.FieldByName('MS_LERIDO').AsString;
           SG1.Cells[16, recsz]    := Query1.FieldByName('MS_LERDAT2').AsString;
           SG1.Cells[17, recsz]    := Query1.FieldByName('MS_LERIDO2').AsString;
       end;
   end;
   if ( ( Query1.FieldByName('MS_LETDAT').AsString = Query1.FieldByName('MS_LERDAT').AsString ) and
        ( Query1.FieldByName('MS_LETIDO').AsString = '' ) ) then begin
       // �j rekordokn�l id�pont m�g nincs kit�ltve, a t�bbi meg azonos
       SG1.Cells[14, recsz]    := Query1.FieldByName('MS_LERDAT').AsString;
       SG1.Cells[15, recsz]    := Query1.FieldByName('MS_LERIDO').AsString;
       SG1.Cells[16, recsz]    := Query1.FieldByName('MS_LERDAT2').AsString;
       SG1.Cells[17, recsz]    := Query1.FieldByName('MS_LERIDO2').AsString;
   end;

   SG1.Cells[18, recsz]    := Query1.FieldByName('MS_FEPAL').AsString;
   SG1.Cells[19, recsz]    := Query1.FieldByName('MS_FSULY').AsString;
   SG1.Cells[20, recsz]    := Query1.FieldByName('MS_LEPAL').AsString;
   SG1.Cells[21, recsz]    := Query1.FieldByName('MS_LSULY').AsString;
end;

procedure TMegjaratbeDlg.CBRendszamChange(Sender: TObject);
var
   potkod  : string;
   potssz  : integer;
   dolkod  : string;
   dolssz  : integer;
begin

   // A p�tkocsi felsorol�sa a g�pkocsihoz
   if CBRendszam.ItemIndex = 0 then begin
       CBPotkocsi.ItemIndex    := 0;
   end else begin
       potkod  := Query_select('GEPKOCSI', 'GK_RESZ', CBRendszam.Text, 'GK_POKOD');
       potkod  := Query_select('GEPKOCSI', 'GK_KOD', potkod, 'GK_RESZ');
       if potkod <> '' then begin
           potssz  := CBPotKocsi.Items.IndexOf(potkod);
           if potssz > -1 then begin
               CBPotKocsi.ItemIndex    := potssz;
           end;
       end;
   end;
   // A dolgoz� be�ll�t�sa a g�pkocsihoz
   if CBRendszam.ItemIndex = 0 then begin
       CBSofor.ItemIndex    := 0;
   end else begin
       dolkod  := Query_select('DOLGOZO', 'DO_RENDSZ', CBRendszam.Text, 'DO_KOD');
       if dolkod <> '' then begin
           dolssz  := lisof.IndexOf(dolkod);
           if dolssz > -1 then begin
               CBSofor.ItemIndex    := dolssz;
           end;
       end;
   end;
end;

procedure TMegjaratbeDlg.BitBtn7Click(Sender: TObject);
var
   i           : integer;
   mbkod       : string;
   mbsorsz     : string;
   mskod       : string;
   recsz       : integer;
begin
	// Ellen�rizz�k, hogy ki dolgozik m�g ezen a megb�z�son
   if SG1.Cells[7, SG1.Row] = '' then begin
       Exit;
   end;
   mbkod   := SG1.Cells[7, SG1.Row];
   mbsorsz := SG1.Cells[8, SG1.Row];
   mskod   := SG1.Cells[2, SG1.Row];
   Query_Run(Query1, 'SELECT * FROM MEGBIZAS WHERE MB_MBKOD = '+mbkod);
   if Query1.FieldByName('MB_AFKOD').AsString <> '' then begin
		if Query1.FieldByName('MB_AFKOD').AsString <> EgyebDlg.user_code then begin
			NoticeKi('A megb�z�st �ppen m�s m�dos�tja!');
			Exit;
		end;
   end;
	// A kijel�lt felrak� megjelenit�se
   Screen.Cursor:=crHourGlass;
   Application.ProcessMessages;
   EgyebDlg.MBujtetel:=False;
   Application.CreateForm(TSegedbe2Dlg,Segedbe2Dlg);
   Segedbe2Dlg.MegForm		:= nil;
   Segedbe2Dlg.kellelore	:= false;
   Segedbe2Dlg.tolt('Felrak�s/lerak�s m�dos�t�sa', mbkod, mbsorsz, mskod);
   Screen.Cursor:=crDefault;
   Application.ProcessMessages;
   Segedbe2Dlg.ShowModal;
	if Segedbe2Dlg.voltenter then begin
       // Beolvassuk a megv�ltozott adatokat
       kellkerdes   := false;
       BitBtn54Click(Sender);   // Elt�vol�tjuk
       // Az �j rekord beilleszt�se
       Query_Run(Query1, 'SELECT * FROM MEGSEGED WHERE MS_MSKOD = ' + mskod);
       // Query_Run(Query1, 'SELECT * FROM MEGSEGED WHERE MS_ID = ' + msid);
       if KellRekord then begin    // Ellen�rizz�k, hogy a rekord megfelel�-e
           recsz           := SG1.RowCount;
           SG1.RowCount    := SG1.RowCount + 1;
           if SG1.Cells[2, 1] = '' then begin
               recsz           := 1;
               SG1.RowCount    := 2;
           end;
           while not Query1.Eof do begin
               Hozzafuzes(recsz);
               Inc(recsz);
               Query1.Next;
           end;
           Csoportosito;
           BitBtn54.Enabled    := true;
       end;
       SG1.Repaint;
       // Sorsz�mozzuk f�l a t�bl�t
       for i := 1 to SG1.RowCount - 1 do begin
           SG1.Cells[0, i] := IntToStr(i);
       end;
       SG1.Row     := SG1.RowCount - 1;    
	end;
	Segedbe2Dlg.Destroy;
   MegbizasKieg(mbkod);
end;

end.
