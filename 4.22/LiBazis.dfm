�
 TLIBAZISDLG 0�'  TPF0TLiBazisDlg
LiBazisDlgTag� Left� Top� Width�HeightCaption   Bázisszótár listázásaColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrder	ScaledOnCreate
FormCreatePixelsPerInchx
TextHeight 	TQuickRep	QuickRep1Left Top Width�Height{Frame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightDataSetQuery1Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style Functions.Strings
PAGENUMBERCOLUMNNUMBERREPORTTITLE Functions.DATA00'' OptionsFirstPageHeaderLastPageFooter Page.ColumnsPage.Orientation
poPortraitPage.PaperSizeA4Page.Values       �@      ��
@       �@      @�
@       �@       �@           PrinterSettings.CopiesPrinterSettings.OutputBinFirstPrinterSettings.DuplexPrinterSettings.FirstPage PrinterSettings.LastPage PrinterSettings.ExtendedDuplex "PrinterSettings.UseStandardprinter PrinterSettings.UseCustomBinCodePrinterSettings.CustomBinCode "PrinterSettings.UseCustomPaperCodePrinterSettings.CustomPaperCode PrinterSettings.PrintMetaFilePrintIfEmptyReportTitle   Bázisszótár listája
SnapToGrid	UnitsNativeZoomdPrevFormStylefsNormalPreviewInitialStatewsNormal TQRBandQRBand1Left/Top/Width�Height@Frame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightAlignToBottomColorclWhiteForceNewColumnForceNewPage	Size.Values xwwwww�@ �������	@ BandTyperbPageHeader 
TQRSysData
QRSysData1Left TopWidth[HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@ �������	@ XUUUUU�@UUUUUU%�@ 	AlignmenttaCenterAlignToBandAutoSizeColorclWhiteDataqrsDateFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentFontSize
  TQRLabel	QRLabel15Left�TopWidthGHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@      ��	@ XUUUUU�@      Ж@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaption   Dátum :ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentWordWrap	FontSize
  TQRLabel	QRLabel11Left TopWidth{HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values��������@                 �@ `UUUU��	@ 	AlignmenttaCenterAlignToBandAutoSizeAutoStretchCaption   Bázisszótár listájaColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBoldfsUnderline 
ParentFontTransparentWordWrap	FontSize  TQRLabelQLNevLeft TopWidthsHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@          UUUUUUU�@      t�@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaptionNEVColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentWordWrap	FontSize
   TQRBandQRBand3Left/Top� Width�HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightAlignToBottomColorclWhiteForceNewColumnForceNewPageSize.Values �����̱@ �������	@ BandTyperbDetail 	TQRDBText	QRDBText4Left
Top Width3HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values hfffff�@ XUUUUU�@           pffff��@ 	AlignmenttaCenterAlignToBandAutoSizeAutoStretchColorclWhiteDataSetQuery1	DataFieldSZ_ALKODFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentWordWrap	FontSize	  	TQRDBText	QRDBText5LeftATop Width�HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values hfffff�@ XUUUU��@           �wwww��@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchColorclWhiteDataSetQuery1	DataFieldSZ_MENEVFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentWordWrap	FontSize	  	TQRDBText	QRDBText3Left�Top WidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values hfffff�@       �@           �����;�@ 	AlignmenttaCenterAlignToBandAutoSizeAutoStretchColorclWhiteDataSetQuery1	DataFieldSZ_ERVNYFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentWordWrap	FontSize	  	TQRDBText	QRDBText6LeftTop WidthsHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values hfffff�@ XUUUU��	@            """"R�@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchColorclWhiteDataSetQuery1	DataFieldSZ_LEIROFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRShapeQRShape2Left
TopWidthqHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values xwwwww�@ XUUUUU�@ hfffff�@ �	@ Shape
qrsHorLine
VertAdjust    TQRGroupQRGroup1Left/TopoWidth�HeightPFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightAlignToBottomBeforePrintQRGroup1BeforePrintColorclWhiteForceNewColumnForceNewPageSize.Values XUUUUU�@ �������	@ 
ExpressionQuery1.SZ_FOKODMaster	QuickRep1ReprintOnNewPage	 TQRShapeQRShape1Left
TopWidthqHeight(Frame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values XUUUUU�@ XUUUUU�@ xwwwww� @ �	@ Brush.ColorclSilverShapeqrsRectangle
VertAdjust   TQRLabelQRLabel1LeftTopWidthlHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values 033333�@ XUUUUU�@ �@ �������@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaption   Főcsoport : ColorclSilverFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentWordWrap	FontSize  	TQRDBText	QRDBText1Left� TopWidthGHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values��������@ ������@ �@ �����H�@ 	AlignmenttaCenterAlignToBandAutoSizeAutoStretchColorclSilverDataSetQuery1	DataFieldSZ_FOKODFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRLabelQRLabel2Left
Top4Width3HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values �����̱@ XUUUUU�@  """""�@ pffff��@ 	AlignmenttaCenterAlignToBandAutoSizeAutoStretchCaption   Alkód ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize
  TQRLabelQRLabel3LeftATop4Width� HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values �����̱@ XUUUU��@  """""�@ HDDDD��@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaption   MegnevezésColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize
  TQRLabelQRLabel4LeftTop4WidthyHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values �����̱@ XUUUU��	@  """""�@ ������@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaption   LeírásColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize
  TQRShapeQRShape3Left
TopIWidthqHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values xwwwww�@ XUUUUU�@ HDDDD��@ �	@ 	Pen.WidthShape
qrsHorLine
VertAdjust   TQRLabelQRLabel5Left�Top4WidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values �����̱@       �@  """""�@ �����;�@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaption   Érv.ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize
  TQRLabelQRLabel6Left� TopWidth�HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values �������@ �����]�@ �@ ������	@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaption   Főcsoport : ColorclSilverFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize    	TADOQueryQuery1ConnectionString|Provider=SQLNCLI.1;Integrated Security=SSPI;Persist Security Info=False;Initial Catalog=KOZ2008;Data Source=DEV32\sqlexpress
Parameters SQL.StringsSELECT * from szotar LeftTopX TStringFieldQuery1SZ_FOKOD	FieldNameSZ_FOKODSize  TStringFieldQuery1SZ_ALKOD	FieldNameSZ_ALKODSize  TStringFieldQuery1SZ_MENEV	FieldNameSZ_MENEVSizex  TStringFieldQuery1SZ_LEIRO	FieldNameSZ_LEIROSizeP  	TBCDFieldQuery1SZ_KODHO	FieldNameSZ_KODHO	Precision  	TBCDFieldQuery1SZ_NEVHO	FieldNameSZ_NEVHO	Precision  TIntegerFieldQuery1SZ_ERVNY	FieldNameSZ_ERVNY    