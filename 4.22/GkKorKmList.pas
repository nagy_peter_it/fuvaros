unit GkKorKmList;

interface

uses
  WinTypes, WinProcs, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, QuickRpt, QRExpr, Qrctrls, ExtCtrls, printers, DB, DBTables, Egyeb, Forgalom, J_SQL,
  Grids, Kozos, ADODB ;

type
  TGkKorKmlisDlg = class(TForm)
    Rep: TQuickRep;
    QRBand3: TQRBand;
    QRLSzamla: TQRLabel;
    QRBand1: TQRBand;
    QRLabel6: TQRLabel;
    QRSysData1: TQRSysData;
    QRLabel7: TQRLabel;
    QRSysData4: TQRSysData;
    QRLabel1: TQRLabel;
    QRLabel13: TQRLabel;
    QRLabel14: TQRLabel;
    Query1: TADOQuery;
    QRGroup1: TQRGroup;
    QRBand2: TQRBand;
    QRLabel2: TQRLabel;
    QRLabel21: TQRLabel;
    QRLabel18: TQRLabel;
    QRShape1: TQRShape;
    QRLabel3: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel8: TQRLabel;
    QRShape2: TQRShape;
    QRLabel9: TQRLabel;
    QRBand4: TQRBand;
    QRLabel10: TQRLabel;
    QRShape3: TQRShape;
    QRLabel11: TQRLabel;
    QRLabel12: TQRLabel;
    QRLabel15: TQRLabel;
    QRLabel16: TQRLabel;
    QRLabel17: TQRLabel;
    QRLabel19: TQRLabel;
    QRLabel20: TQRLabel;
    QRLabel22: TQRLabel;
    QRLabel23: TQRLabel;
    QRLabel24: TQRLabel;
	 procedure FormCreate(Sender: TObject);
	 procedure	Tolt(resz : boolean);
	 procedure QRBand1BeforePrint(Sender: TQRCustomBand;
	   var PrintBand: Boolean);
	 procedure RepBeforePrint(Sender: TCustomQuickRep;
	   var PrintReport: Boolean);
	 procedure QRGroup1BeforePrint(Sender: TQRCustomBand;
	   var PrintBand: Boolean);
	 procedure QRBand2BeforePrint(Sender: TQRCustomBand;
	   var PrintBand: Boolean);
    procedure QRBand4BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
  private
		reszletes		: boolean;
		atlag,oatlag,okm,ookm			: double;
		letszam,no			: integer;
  public
	  vanadat			: boolean;
  end;

var
  GkKorKmlisDlg: TGkKorKmlisDlg;
  HUTOS: boolean;
  HUTOTIP:string;

implementation

uses DateUtils,GkKorKm;

{$R *.DFM}

procedure TGkKorKmlisDlg.FormCreate(Sender: TObject);
begin
	EgyebDlg.SetADOQueryDatabase(Query1);
	HUTOTIP:= EgyebDlg.Read_SZGrid( '999', '922' );
end;

procedure	TGkKorKmlisDlg.Tolt(resz : boolean);
begin
	reszletes 	:= resz;
  if GkKorKmDlg.RadioButton1.Checked then
  	Query_Run(Query1, 'SELECT * FROM GEPKOCSI WHERE GK_ARHIV = 0 and GK_KIVON = 0 and GK_KULSO = 0    ORDER BY GK_GKKAT, GK_RESZ');
  if GkKorKmDlg.RadioButton2.Checked then
  	Query_Run(Query1, 'SELECT * FROM GEPKOCSI WHERE GK_ARHIV = 0 and GK_KIVON = 0 and GK_KULSO = 0 and GK_POTOS<>1   ORDER BY GK_GKKAT, GK_RESZ');
  if GkKorKmDlg.RadioButton3.Checked then
  	Query_Run(Query1, 'SELECT * FROM GEPKOCSI WHERE GK_ARHIV = 0 and GK_KIVON = 0 and GK_KULSO = 0 and GK_POTOS=1   ORDER BY GK_GKKAT, GK_RESZ');

	vanadat 			:= Query1.RecordCount > 0;
//	QrGroup1.Expression	:= 'Query1.DO_GKKAT';
	QrGroup1.Expression	:= 'Query1.GK_GKKAT';
end;

procedure TGkKorKmlisDlg.QRBand1BeforePrint(Sender: TQRCustomBand;
	   var PrintBand: Boolean);
var
  kor,km:integer;
begin
  kor:=Trunc(YearOf(date)-  StringSzam(Query1.FieldByName('GK_EVJAR').AsString));
  km :=Trunc(StringSzam(Query1.FieldByName('GK_UT_KM').AsString));
	QrLabel3.Caption	:= Query1.FieldByName('GK_RESZ').AsString;
	QrLabel12.Caption	:= Query1.FieldByName('GK_TIPUS').AsString;
	QrLabel23.Caption	:= Query1.FieldByName('GK_UT_DA').AsString;
  if HUTOS then
  	QrLabel15.Caption	:= IntToStr( km) +' �ra'
  else
	  QrLabel15.Caption	:= IntToStr( km) +' km';
	QrLabel4.Caption	:= IntToStr( kor)+' �v';
	Inc(letszam);
	atlag	:= atlag + kor;
  okm:=okm+km;
  ookm:=ookm+km;
  oatlag:=oatlag+kor;
  inc(no);
  QRLabel9.Caption:=IntToStr(no)+'.';
	PrintBand			:= reszletes;
end;

procedure TGkKorKmlisDlg.RepBeforePrint(Sender: TCustomQuickRep;
  var PrintReport: Boolean);
begin
	letszam		:= 0;
	atlag		:= 0;
	oatlag		:= 0;
  okm:=0;
  ookm:=0;
  no:=0;
end;

procedure TGkKorKmlisDlg.QRGroup1BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
//	QrLabel2.Caption	:= 'Gk. kateg�ria : '+Query1.FieldByName('DO_GKKAT').AsString;
	QrLabel2.Caption	:= 'G�pkocsi kateg�ria : '+Query1.FieldByName('GK_GKKAT').AsString;
  HUTOS:=pos(Query1.FieldByName('GK_GKKAT').AsString,HUTOTIP)>0;
	PrintBand			:= reszletes;
end;

procedure TGkKorKmlisDlg.QRBand2BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
//	QrLabel5.Caption	:= '�tlag a gk. kateg�ri�ra : '+Query1.FieldByName('DO_GKKAT').AsString;
	QrLabel5.Caption	:= '�tlag a g�pkocsi kateg�ri�ra: '+Query1.FieldByName('GK_GKKAT').AsString;
	if letszam = 0 then begin
		QrLabel8.Caption	:= '0';
		QrLabel17.Caption	:= '0';
	end else begin
		QrLabel8.Caption	:= Format('%.1f �v', [atlag/letszam]);
//		QrLabel17.Caption	:= Format('%f km', [okm/letszam]);
  if HUTOS then
		QrLabel17.Caption	:= IntToStr( Trunc((okm/letszam)))+' �ra'
  else
		QrLabel17.Caption	:= IntToStr( Trunc((okm/letszam)))+' km';
	end;
	QrLabel20.Caption	:= IntToStr(letszam)+' db';
	atlag				:= 0;
  okm:=0;
	letszam				:= 0;
	if not reszletes then begin
		QrShape2.Width		:= 0;
	end;
end;

procedure TGkKorKmlisDlg.QRBand4BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
		QrLabel11.Caption	:= Format('%.1f �v', [oatlag/no]);
 		QrLabel19.Caption	:= IntToStr( Trunc((ookm/no)))+' km';
  	QrLabel22.Caption	:= IntToStr(no)+' db';
end;

end.
