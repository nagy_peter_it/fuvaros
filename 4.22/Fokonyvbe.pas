unit Fokonyvbe;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons;

type
  TFokonybeDlg = class(TForm)
    BitElkuld: TBitBtn;
    BitKilep: TBitBtn;
    Label1: TLabel;
    CBFok: TComboBox;
    procedure FormCreate(Sender: TObject);
    procedure BitElkuldClick(Sender: TObject);
    procedure BitKilepClick(Sender: TObject);
    procedure Tolt(fok : string);
  private
     foklista	: TStringList;
  public
  		ret_fokonyv	: string;
  end;

var
  FokonybeDlg: TFokonybeDlg;

implementation

uses
	Kozos, J_SQL;

{$R *.dfm}

procedure TFokonybeDlg.FormCreate(Sender: TObject);
begin
  	foklista := TStringList.Create;
	SzotarTolt(CBFok, '350' , foklista, false, false );
  	CBFok.ItemIndex	:= 0;
end;

procedure TFokonybeDlg.BitElkuldClick(Sender: TObject);
begin
	ret_fokonyv 	:= copy(CBFok.Text, 1, 10);
   Close;
end;

procedure TFokonybeDlg.BitKilepClick(Sender: TObject);
begin
	ret_fokonyv 	:= '';
   Close;
end;

procedure TFokonybeDlg.Tolt(fok : string);
var
	sorsz	: integer;
begin
	if fok <> '' then begin
   	sorsz	:= foklista.IndexOf(fok);
       if sorsz < 0 then begin
       	sorsz := 0;
       end;
       CBFok.ItemIndex	:= sorsz;
   end;
end;

end.
