unit XML_Export;

interface

uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls, Egyeb,
  Forms, Dialogs, DBTables, DB, StdCtrls, Buttons, Mask, DBCtrls, Grids,
  Printers, ExtCtrls, ADODB, XMLDoc, XMLIntf, ComObj;

type
  TXML_ExportDlg = class(TForm)
    Label1: TLabel;
    BitBtn4: TBitBtn;
    BitBtn6: TBitBtn;
    M1: TMaskEdit;
    M2: TMaskEdit;
    Label2: TLabel;
    Label3: TLabel;
    BitBtn10: TBitBtn;
    BitBtn1: TBitBtn;
    Query1: TADOQuery;
    Label4: TLabel;
    Query2: TADOQuery;
    Query3: TADOQuery;
    Query4: TADOQuery;
    Label5: TLabel;
    M3: TMaskEdit;
    M50: TMaskEdit;
    M5: TMaskEdit;
    BitBtn5: TBitBtn;
    CheckBox11: TCheckBox;
    Query5: TADOQuery;
    CheckBox1: TCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure BitBtn4Click(Sender: TObject);
    procedure BitBtn6Click(Sender: TObject);
    procedure M1Click(Sender: TObject);
    procedure BitBtn10Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure M1Exit(Sender: TObject);
    function XML_Generalas: boolean;
    function XML_Generalas2: boolean;
    function XML_Generalas3: boolean;
    procedure BitBtn5Click(Sender: TObject);
    procedure M3KeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure CheckBox1Click(Sender: TObject);
    procedure SulyEllenor(su : double; kateg, mbkod : string );
  private
    kilepes: boolean;
    fn: string;
    XMLDoc1: TXMLDocument;
    DocNode: IXMLNode;
    ShipNode: IXMLNode;
    FieldNode: IXMLNode;
  public
    gr: TStringGrid;
    kellshell: boolean;
    kelluzenet: boolean;
    voltkilepes: boolean;
    kombinalt: boolean;
  end;

var
  XML_ExportDlg: TXML_ExportDlg;

implementation

uses
  j_sql, Kozos, ValVevo, Kozos_Export, J_Datamodule, Windows, Kombi_Export;
{$R *.DFM}

procedure TXML_ExportDlg.FormCreate(Sender: TObject);
begin
  kilepes := true;
  gr := nil;
  kellshell := true;
  voltkilepes := false;
  kelluzenet := true;
  kombinalt := false;
  EgyebDlg.SetADOQueryDatabase(Query1);
  EgyebDlg.SetADOQueryDatabase(Query2);
  EgyebDlg.SetADOQueryDatabase(Query3);
  EgyebDlg.SetADOQueryDatabase(Query4);
  EgyebDlg.SetADOQueryDatabase(Query5);
  Kezdoertekek(M1, M2, M5, M50);
end;

procedure TXML_ExportDlg.BitBtn4Click(Sender: TObject);
begin
  { if not DatumExit(M1, false) then begin
    Exit;
    end;
    if not DatumExit(M2, false) then begin
    Exit;
    end;
    if M50.Text = '' then begin
    NoticeKi('A megb�z� kiv�laszt�sa k�telez�!');
    M5.SetFocus;
    Exit;
    end; }
  // Az XML �llom�ny gener�l�sa
  Screen.Cursor := crHourGlass;
  // if CheckBox1.Checked then
  // XML_Generalas2
  // else
  if XML_Generalas then
  begin
    if (not kombinalt) then
    begin
      if (not CheckBox1.Checked) then
        NoticeKi('Az XML gener�l�sa befejez�d�tt!');
      M3.Text := '';
      M3.SetFocus;
    end;
  end;
  if kombinalt then
  begin
    Close;
  end;
  Screen.Cursor := crDefault;
end;

procedure TXML_ExportDlg.BitBtn6Click(Sender: TObject);
begin
  if not kilepes then
  begin
    kilepes := true;
  end
  else
  begin
    Close;
  end;
end;

procedure TXML_ExportDlg.M1Click(Sender: TObject);
begin
  with Sender as TMaskEdit do
  begin
    SelectAll;
  end;
end;

procedure TXML_ExportDlg.BitBtn10Click(Sender: TObject);
begin
  // EgyebDlg.Calendarbe(M1);
  // EgyebDlg.ElsoNapEllenor(M1, M2);
  EgyebDlg.CalendarBe_idoszak(M1, M2);
end;

procedure TXML_ExportDlg.BitBtn1Click(Sender: TObject);
begin
  EgyebDlg.Calendarbe(M2, True);
end;

procedure TXML_ExportDlg.M1Exit(Sender: TObject);
begin
  DatumExit(M1, false);
  EgyebDlg.ElsoNapEllenor(M1, M2);
end;

function TXML_ExportDlg.XML_Generalas: boolean;
var
  // fn			: String;
  // XMLDoc1     : TXMLDocument;
  // DocNode     : IXMLNode;
  // ShipNode    : IXMLNode;
  // FieldNode   : IXMLNode;
  sorszam: integer;
  ujkod: string;
  eurnetto: double;
  eurafasz: double;
  eurafaer: double;
  gktipus: string;
  gkkat: string;
  laneid: string;
  lanossz: double;
  euado: string;
  szamlastr: string;
  szamszam: string;
  szamdb: integer;
  szamdb2: integer;
  szamhiany: string;
  suly: double;
  potlek: double;
  spedicio: double;
  ExcelApp: OleVariant;
  lanidDij, arfolyam: double;
  egyedidij: boolean;
  VAL, sz2kidat: string;
  kelluzpotl, kellujfajl: boolean;
begin
  result := true;
  kellujfajl := not CheckBox1.Checked;

  if (M3.Text = '') and (not kellujfajl) and (XMLDoc1 <> nil) then
  begin
    if (MessageBox(0, 'K�ri a f�jl lez�r�s�t?', '', MB_ICONQUESTION or
      MB_YESNO or MB_DEFBUTTON2) in [idYes]) then
    begin
      XMLDoc1.SaveToFile(fn);
      Try
        ExcelApp := CreateOleObject('Excel.Application');
        ExcelApp.Workbooks.open(fn);
        ExcelApp.Visible := true;
      Except
      End;
      exit;
    end;
  end;
  if Trim(M3.Text) <> '' then begin
    if false then begin
      // if kelluzenet then begin
      if NoticeKi ('Figyelem! Sz�mlasz�m megad�sa eset�n a megb�z�t �s az id�szakot a rendszer nem veszi figyelembe! Folytatja?', NOT_QUESTION) <> 0 then   begin
           result := false;
           exit;
      end;
    end;
    if Length(M3.Text) < 8 then
      szamszam := EgyebDlg.Read_SZGrid('CEG', '310') + Format('%5.5d', [StrToIntDef(M3.Text, 0)])
    else
      szamszam := M3.Text;

    // KG 20120203
    Query_Run(Query1,
      'SELECT MB_KISCS, MB_MBKOD, MB_RENSZ, MB_POZIC, MB_FUTID, MB_FUDIJ, MB_VEKOD, MB_VIKOD, MB_JAKOD, MB_DATUM, MB_VENEV, MB_JASZA, MB_ALVAL, MB_EDIJ,MB_NUZPOT FROM MEGBIZAS, VISZONY WHERE '
      + ' MB_VIKOD = VI_VIKOD AND MB_JAKOD = VI_JAKOD AND vi_ujkod in (select SA_UJKOD from szfej where sa_kod= '''
      + szamszam + ''' )');

    // Az �llom�ny nev�nek �sszerak�sa
    // fn	:=ExtractFilePath(Application.ExeName)+'JS'+copy(EgyebDlg.EvSzam, 3, 2)+'_'+Format('%4.4d',[StrToIntDef(M3.Text,0)])+'.xml';
    if kellujfajl or (fn = '') then
    begin
      fn := ExtractFilePath(Application.ExeName) + szamszam + '.xml';
      fn := StringReplace(fn, '/', '_', [rfReplaceAll]);
    end;
  end
  else
  begin
    if M50.Text <> '' then
      Query_Run(Query1,
        'SELECT MB_KISCS, MB_MBKOD, MB_RENSZ, MB_POZIC, MB_FUTID, MB_FUDIJ, MB_VEKOD, MB_VIKOD, MB_JAKOD, MB_DATUM, MB_VENEV, MB_JASZA, MB_ALVAL, MB_EDIJ,MB_NUZPOT FROM MEGBIZAS '
        + ' WHERE MB_MBKOD IN ( SELECT MS_MBKOD FROM MEGSEGED WHERE MS_LETDAT >= '''+ M1.Text + ''' AND MS_LETDAT <= ''' + M2.Text + ''' ) ' +
        ' AND MB_VEKOD IN (' + M50.Text + ') ' +
        ' ORDER BY MB_DATUM, MB_MBKOD ');
  end;
  if kellujfajl and FileExists(fn) then
  begin
    DeleteFile(PChar(fn));
  end;
  if Query1.RecordCount < 1 then
  begin
    if kelluzenet then
    begin
      NoticeKi('Nincs a felt�teleknek megfelel� elem!');
    end;
    result := false;
    exit;
  end;
  // Az XML megnyit�sa
  EllenListTorol;
  if kellujfajl or (XMLDoc1 = Nil) then
  begin
    XMLDoc1 := TXMLDocument.Create(nil);
    XMLDoc1.Active := true;
    XMLDoc1.Version := '1.0';
    DocNode := XMLDoc1.AddChild('DOCUMENT');
  end;
  Query_Run(EgyebDlg.QueryKozos,
    'SELECT SZ_MENEV FROM SZOTAR WHERE SZ_FOKOD = ''888'' AND SZ_ALKOD = ''120'' ',
    false);
  sorszam := StrToIntDef(EgyebDlg.QueryKozos.FieldByName('SZ_MENEV')
    .AsString, 1);
  kilepes := false;
  szamlastr := ';';
  while ((not Query1.Eof) and (not kilepes)) do begin
   //  DEBUG
   //  if Query1.FieldByName('MB_MBKOD').AsString = '42342' then
   //     NoticeKi('Itt a 42342');
    egyedidij := Query1.FieldByName('MB_EDIJ').AsInteger = 1;
    Application.ProcessMessages;
    kelluzpotl := StrToIntDef(Query1.FieldByName('MB_NUZPOT').AsString, 0) = 0;
    Label4.Caption := szamszam + '  D�tum : ' + Query1.FieldByName('MB_DATUM').AsString + ' - megb�z�s k�d : ' +
      Query1.FieldByName('MB_MBKOD').AsString;
    Label4.Update;
    // Megkeress�k a megb�z�shoz tartoz� sz�ml�t
    if Query1.FieldByName('MB_VIKOD').AsString = '' then
    begin
      EllenListAppend('Nincs viszonylat : MBKOD : ' +
        Query1.FieldByName('MB_MBKOD').AsString);
      Query1.Next;
      Continue;
    end;
    ujkod := '';
    Query_Run(Query3, 'SELECT * FROM VISZONY WHERE VI_VIKOD = ' +
      Query1.FieldByName('MB_VIKOD').AsString + ' AND VI_JAKOD = ''' +
      Query1.FieldByName('MB_JAKOD').AsString + ''' ');
    if Query3.RecordCount = 1 then
    begin
      ujkod := Query3.FieldByName('VI_UJKOD').AsString
    end;
    if Query3.RecordCount < 1 then
    begin
      EllenListAppend('Nincs megfelel� viszony : sz�mlasz�m : ' + Trim(M3.Text)
        + ' vikod = ' + Query1.FieldByName('MB_VIKOD').AsString + '  J�rat: ' +
        Query1.FieldByName('MB_JASZA').AsString + '  Rendsz�m : ' +
        Query1.FieldByName('MB_RENSZ').AsString);
    end;
    if Query3.RecordCount > 1 then
    begin
      EllenListAppend('T�l sok viszony : sz�mlasz�m : ' + Trim(M3.Text) +
        ' vikod = ' + Query1.FieldByName('MB_VIKOD').AsString);
    end;
    // �sszehasonl�tjuk a sz�mla t�teleket �s a megb�z�sokat
    szamszam := Query3.FieldByName('VI_SAKOD').AsString;
    if szamszam = '' then
    begin
      // Nincs sz�mlasz�m, nem kell feldolgozni a t�telt !!!
      EllenListAppend('Nincs sz�mlasz�m : vikod = ' +
        Query1.FieldByName('MB_VIKOD').AsString + '  MBKOD = ' +
        Query1.FieldByName('MB_MBKOD').AsString);
      Query1.Next;
      Continue;
    end;
    if Pos(';' + szamszam + ';', szamlastr) < 1 then
    begin
      szamlastr := szamlastr + szamszam + ';';
      Query_Run(Query2, 'SELECT COUNT(*) DB FROM SZFEJ WHERE SA_KOD = ''' +
        szamszam + ''' ');
      szamdb := StrToIntDef(Query2.FieldByName('DB').AsString, 0);
      Query_Run(Query2,
        'SELECT COUNT(*) DB FROM MEGBIZAS, VISZONY WHERE MB_VIKOD = VI_VIKOD AND MB_JAKOD = VI_JAKOD AND  VI_SAKOD = '''
        + szamszam + ''' ');
      szamdb2 := StrToIntDef(Query2.FieldByName('DB').AsString, 0);
      if szamdb <> szamdb2 then
      begin
        // A m�sodik fajta ellen�rz�s !!!
        Query_Run(Query2, 'SELECT SA_UJKOD UJKOD FROM SZFEJ WHERE SA_KOD = ''' +
          szamszam +
          ''' AND SA_UJKOD NOT IN (SELECT VI_UJKOD FROM MEGBIZAS, VISZONY ' +
          'WHERE MB_VIKOD = VI_VIKOD AND MB_JAKOD = VI_JAKOD AND  VI_SAKOD = '''
          + szamszam + ''') UNION ' +
          'SELECT VI_UJKOD UJKOD FROM MEGBIZAS, VISZONY WHERE MB_VIKOD = VI_VIKOD AND MB_JAKOD = VI_JAKOD AND  VI_SAKOD = '''
          + szamszam +
          ''' AND VI_UJKOD NOT IN (SELECT SA_UJKOD FROM SZFEJ WHERE SA_KOD = '''
          + szamszam + ''') ');
        if Query2.RecordCount > 0 then
        begin
          szamhiany := '';
          while not Query2.Eof do
          begin
            szamhiany := szamhiany + Query2.FieldByName('UJKOD')
              .AsString + ', ';
            Query2.Next;
          end;
        end;
        EllenListAppend
          ('   A SZ�MLA T�TEL �S A MEGB�Z�SOK SZ�MA ELT�R�! Sz�mlasz�m : ' +
          szamszam + '  ' + Format('%d <> %d', [szamdb, szamdb2]) +
          '    A hi�nyz� sz�ml�k k�dja : ' + szamhiany);
      end;
    end;
    Query_Run(Query3, 'SELECT * FROM SZFEJ WHERE SA_UJKOD = ' +
      IntToStr(StrToIntDef(ujkod, 0)));
    if Query3.RecordCount < 1 then
    begin
      EllenListAppend('Nincs sz�mla! Sz�mlak�d : ' + IntToStr(StrToIntDef(ujkod,
        0)) + '  J�rat: ' + Query1.FieldByName('MB_JASZA').AsString +
        '  Rendsz�m : ' + Query1.FieldByName('MB_RENSZ').AsString);
      Query1.Next;
      Continue;
    end;
    // A LAN ID ellen�rz�se
    laneid := Query_Select('FUVARTABLA', 'FT_FTKOD',
      IntToStr(StrToIntDef(Query1.FieldByName('MB_FUTID').AsString, 0)),
      'FT_LANID');
    if laneid = '' then
    begin
      EllenListAppend('Hi�nyz� LAN_ID! Sz�mlasz�m : ' +
        Query3.FieldByName('SA_KOD').AsString + '  J�rat: ' +
        Query1.FieldByName('MB_JASZA').AsString + '  Rendsz�m : ' +
        Query1.FieldByName('MB_RENSZ').AsString);
      Query1.Next;
      Continue;
    end;
    gktipus := Query_Select('FUVARTABLA', 'FT_FTKOD',
      IntToStr(StrToIntDef(Query1.FieldByName('MB_FUTID').AsString, 0)),
     //  'FT_KATEG');
      'FT_GKKAT');
    // lanidDij := Query_Select2('FUVARTABLA', 'FT_LANID', 'FT_KATEG', laneid,
    //  gktipus, 'FT_FTEUR');
      lanidDij := StringSzam(Query_Select('FUVARTABLA', 'FT_FTKOD', IntToStr(StrToIntDef(Query1.FieldByName('MB_FUTID').AsString, 0)), 'FT_FTEUR'));


    VAL := Query3.FieldByName('SA_VALNEM').AsString;
    arfolyam := EgyebDlg.ArfolyamErtek('EUR', Query3.FieldByName('SA_TEDAT').AsString, true);
    // A sz�ll�t�si f�t�tel l�trehoz�sa
    ShipNode := DocNode.AddChild('shipment');
    // A mez�k gener�l�sa
    FieldNode := ShipNode.AddChild('feedno');
    FieldNode.Text := IntToStr(sorszam);
    FieldNode := ShipNode.AddChild('scac');
    FieldNode.Text := 'HU12939848';
    euado := Query_Select('VEVO', 'V_KOD', Query1.FieldByName('MB_VEKOD')
      .AsString, 'VE_EUADO');
    FieldNode := ShipNode.AddChild('clientno');
    FieldNode.Text := euado;
    FieldNode := ShipNode.AddChild('clientname');
    FieldNode.Text := Query1.FieldByName('MB_VENEV').AsString;
    FieldNode := ShipNode.AddChild('carracct');
    FieldNode.Text := Query1.FieldByName('MB_VEKOD').AsString;
    FieldNode := ShipNode.AddChild('paid_flag');
    FieldNode.Text := 'EUR';
    FieldNode := ShipNode.AddChild('mastinv_no');
    FieldNode.Text := Query3.FieldByName('SA_KOD').AsString;
    FieldNode := ShipNode.AddChild('inv_date');
    if Query3.FieldByName('SA_KIDAT').AsString <> '' then
    begin
      FieldNode.Text := copy(Query3.FieldByName('SA_KIDAT').AsString, 1, 4) +
        copy(Query3.FieldByName('SA_KIDAT').AsString, 6, 2) +
        copy(Query3.FieldByName('SA_KIDAT').AsString, 9, 2)
    end
    else
    begin
      sz2kidat := Query_Select('SZFEJ2', 'SA_KOD', szamszam, 'SA_KIDAT');
      FieldNode.Text := copy(sz2kidat, 1, 4) + copy(sz2kidat, 6, 2) +
        copy(sz2kidat, 9, 2);
    end;
    FieldNode := ShipNode.AddChild('rccode');
    FieldNode.Text := Query3.FieldByName('SA_MEGRE').AsString;
    FieldNode := ShipNode.AddChild('pronumber');
    FieldNode.Text := Query1.FieldByName('MB_MBKOD').AsString;
    // Megkeress�k az els� felrak�st
    Query_Run(Query4,
      'SELECT MS_FETDAT, MS_GKKAT FROM MEGSEGED WHERE MS_MBKOD = ' +
      Query1.FieldByName('MB_MBKOD').AsString +
      ' AND MS_TIPUS = 0 ORDER BY MS_MSKOD ');
    if Query4.RecordCount > 0 then
    begin
      FieldNode := ShipNode.AddChild('prodate');
      FieldNode.Text := copy(Query4.FieldByName('MS_FETDAT').AsString, 1, 4) +
        copy(Query4.FieldByName('MS_FETDAT').AsString, 6, 2) +
        copy(Query4.FieldByName('MS_FETDAT').AsString, 9, 2);
    end
    else
    begin
      FieldNode := ShipNode.AddChild('prodate');
      FieldNode.Text := '';
    end;
    gkkat := Query4.FieldByName('MS_GKKAT').AsString;
    // A CMR sz�m meghat�roz�sa
    FieldNode := ShipNode.AddChild('lading');
    FieldNode.Text := Query_Select('CMRDAT', 'CM_VIKOD',
      Query1.FieldByName('MB_VIKOD').AsString, 'CM_CSZAM');
    FieldNode := ShipNode.AddChild('po'); // Poz�ci�sz�m
    FieldNode.Text := Query1.FieldByName('MB_POZIC').AsString;
    FieldNode := ShipNode.AddChild('pieces');
    FieldNode.Text := '1.00';
    Query_Run(Query4,
      'SELECT SUM(MS_FEPAL) PALETTAK FROM MEGSEGED WHERE MS_MBKOD = ' +
      Query1.FieldByName('MB_MBKOD').AsString + ' AND MS_TIPUS = 0');
    FieldNode := ShipNode.AddChild('pallets');
    FieldNode.Text := SqlSzamString(StringSzam(Query4.FieldByName('PALETTAK')
      .AsString), 12, 2);
    suly := GetKobSuly(Query1.FieldByName('MB_MBKOD').AsString);
    if suly = 0 then
    begin
      suly := GetSuly(Query1.FieldByName('MB_MBKOD').AsString);
    end;
    FieldNode := ShipNode.AddChild('weight');
    FieldNode.Text := SqlSzamString(suly, 12, 2);
    FieldNode := ShipNode.AddChild('wt_flag');
    FieldNode.Text := '1';
    FieldNode := ShipNode.AddChild('cubicmt');
    FieldNode.Text := '0.00';
    FieldNode := ShipNode.AddChild('dim_wt');
    FieldNode.Text := SqlSzamString(suly, 12, 2);
    FieldNode := ShipNode.AddChild('loadingmt');
    FieldNode.Text := '0.00';
    FieldNode := ShipNode.AddChild('terms');
    FieldNode.Text := '1';
    FieldNode := ShipNode.AddChild('premiumflg');
    FieldNode.Text := '0';
    FieldNode := ShipNode.AddChild('inco_terms');
    FieldNode.Text := 'EXW';
    FieldNode := ShipNode.AddChild('mode');
    FieldNode.Text := 'GROUND';
    FieldNode := ShipNode.AddChild('serv_type');
    FieldNode.Text := 'DTD';
    // A lane_id t�lt�se
    lanossz := StringSzam(Query1.FieldByName('MB_FUDIJ').AsString);
    gktipus := Query_Select('FUVARTABLA', 'FT_FTKOD',
      IntToStr(StrToIntDef(Query1.FieldByName('MB_FUTID').AsString, 0)),
      // 'FT_KATEG');
      'FT_GKKAT');
    if gktipus = '' then begin
      gktipus := Query1.FieldByName('MB_GKKAT').AsString;
    end;
    // A g�pkocsi t�pus bet�lt�se
    if copy(gktipus, 1, 3) = '1,5' then begin
      gktipus := '1.5T';
    end else begin
      if copy(gktipus, 1, 3) = '3,5' then begin
        gktipus := '3.5T';
      end else begin
        if copy(gktipus, 1, 2) = '24' then begin
          gktipus := '24T';
        end else begin
          gktipus := '';
        end;
      end;
    end;
    SulyEllenor(suly, gktipus, Query1.FieldByName('MB_MBKOD').AsString);

    FieldNode := ShipNode.AddChild('serv_categ');
    FieldNode.Text := gktipus;
    FieldNode := ShipNode.AddChild('lane_id');
    if egyedidij then
      FieldNode.Text := ''
    else
      FieldNode.Text := laneid;
    FieldNode := ShipNode.AddChild('serv_level');
    // Az express - standard besz�r�sa
    if StrToIntDef(Query1.FieldByName('MB_KISCS').AsString, 0) > 0 then begin
      FieldNode.Text := 'express ';
    end else begin
      FieldNode.Text := 'standard';
    end;
    // Az utols� lerak� meghat�roz�sa
    Query_Run(Query4,
      'SELECT MS_FELNEV, MS_FELIR, MS_FORSZ, MS_FELTEL, MS_LERNEV, MS_LELIR, MS_ORSZA, MS_LERTEL FROM MEGSEGED WHERE MS_MBKOD = '
      + Query1.FieldByName('MB_MBKOD').AsString +
      ' AND MS_TIPUS = 1 ORDER BY MS_MSKOD ');
    Query4.Last;

    FieldNode := ShipNode.AddChild('consignor');
    FieldNode.Text := Query4.FieldByName('MS_FELNEV').AsString;
    FieldNode := ShipNode.AddChild('origin');
    FieldNode.Text := Query4.FieldByName('MS_FELIR').AsString;
    FieldNode := ShipNode.AddChild('org_cty');
    FieldNode.Text := Query4.FieldByName('MS_FORSZ').AsString;
    FieldNode := ShipNode.AddChild('org_state');
    FieldNode.Text := '';
    FieldNode := ShipNode.AddChild('org_city');
    FieldNode.Text := Query4.FieldByName('MS_FELTEL').AsString;

    FieldNode := ShipNode.AddChild('org_apt');
    FieldNode.Text := '';
    FieldNode := ShipNode.AddChild('consignee');
    FieldNode.Text := Query4.FieldByName('MS_LERNEV').AsString;
    FieldNode := ShipNode.AddChild('dest');
    FieldNode.Text := Query4.FieldByName('MS_LELIR').AsString;
    FieldNode := ShipNode.AddChild('dest_cty');
    FieldNode.Text := Query4.FieldByName('MS_ORSZA').AsString;
    FieldNode := ShipNode.AddChild('dest_state');
    FieldNode.Text := '';
    FieldNode := ShipNode.AddChild('dest_city');
    FieldNode.Text := Query4.FieldByName('MS_LERTEL').AsString;
    FieldNode := ShipNode.AddChild('dest_apt');
    FieldNode.Text := '';

    JSzamla := TJSzamla.Create(Self);
    JSzamla.FillSzamla(ujkod);
    eurnetto := JSzamla.ossz_nettoeur;
    if eurnetto <> 0 then
    begin
      eurafasz := Kerekit(JSzamla.ossz_afaeur * 100 / eurnetto);
    end;
    eurafaer := JSzamla.ossz_afaeur;
    if kelluzpotl and (JSzamla.uapotlekeur = 0) then begin
      EllenListAppend('Az �zemanyag p�tl�k �sszege 0! Sz�mlasz�m : ' + JSzamla.szamlaszam);
    end else begin
      // if ( Abs ( JSzamla.ossz_nettoeur - JSzamla.spedicioeur - JSzamla.uapotlekeur - lanossz ) > 0.00001 ) then begin
      if (Abs(JSzamla.ossz_nettoeur - JSzamla.spedicioeur - JSzamla.uapotlekeur
        - lanossz) > 0.00001) and
        ((kombinalt) and (Pos(Query1.FieldByName('MB_ALVAL').AsString,
        Kombi_ExportDlg.belf_alval) = 0)) then begin
        EllenListAppend
          ('A sz�mla nett� eur - sped�ci�s d�j <> megb�z�si fuvard�j! Sz�mla : '
          + ujkod + '  Megb�z�s k�d : ' + Query1.FieldByName('MB_MBKOD')
          .AsString);
      end;
    end;
    // Mindig mentj�k a sort !!!
    potlek := JSzamla.uapotlekeur;
    spedicio := JSzamla.spedicioeur;
    SorLogolas(gr, Query1.FieldByName('MB_MBKOD').AsString, suly, lanossz,
      JSzamla.uapotlekeur, 4);
    JSzamla.Destroy;

    FieldNode := ShipNode.AddChild('billed');
    FieldNode.Text := SqlSzamString(eurnetto + eurafaer, 12, 2);
    FieldNode := ShipNode.AddChild('vatperc');
    FieldNode.Text := SqlSzamString(eurafasz, 12, 2);
    FieldNode := ShipNode.AddChild('vat');
    FieldNode.Text := SqlSzamString(eurafaer, 12, 2);
    FieldNode := ShipNode.AddChild('trans');
    if egyedidij then
    begin
      if VAL <> 'EUR' then begin
        lanossz := lanossz / arfolyam;
      end;
      FieldNode.Text := SqlSzamString(lanossz, 12, 2);
    end else begin
      FieldNode.Text := SqlSzamString(lanidDij, 12, 2);
    end;
    FieldNode := ShipNode.AddChild('fuel_schgperc');
    // if lanossz = 0 then begin
    if lanidDij = 0 then begin
      FieldNode.Text := '0';
    end else begin
      // FieldNode.Text		 	:= SqlSzamString(potlek*100/lanossz, 12, 2) ;
      FieldNode.Text := SqlSzamString(potlek * 100 / lanidDij, 12, 2);
      if StringSzam(Format('%.2f', [potlek * 100 / lanidDij])) <> StringSzam(Format('%.0f', [potlek * 100 / lanidDij])) then begin
           EllenListAppend('�zemanyag szorz� miatti hiba! '+Format('%.2f', [potlek * 100 / lanidDij]));
      end;
    end;
    FieldNode := ShipNode.AddChild('fuel_schg');
    FieldNode.Text := SqlSzamString(potlek, 12, 2);
    FieldNode := ShipNode.AddChild('handling');
    FieldNode.Text := SqlSzamString(spedicio, 12, 2);
    FieldNode := ShipNode.AddChild('misc');
    FieldNode.Text := '0.00';
    FieldNode := ShipNode.AddChild('toll_fee');
    FieldNode.Text := '0.00';
    FieldNode := ShipNode.AddChild('insurance');
    FieldNode.Text := '0.00';
    // A sorsz�m bejegyz�se a megb�z�shoz
    Query_Run(Query2, 'UPDATE MEGBIZAS SET MB_XMLSZ = ' + IntToStr(sorszam) +
      ' WHERE MB_MBKOD = ' + Query1.FieldByName('MB_MBKOD').AsString);
    Inc(sorszam);
    Query1.Next;
  end;
  Query_Run(EgyebDlg.QueryKozos,
    'DELETE FROM SZOTAR WHERE SZ_FOKOD = ''888'' AND SZ_ALKOD = ''120'' ',
    false);
  Query_Run(EgyebDlg.QueryKozos,
    'INSERT INTO SZOTAR ( SZ_FOKOD, SZ_ALKOD, SZ_MENEV, SZ_LEIRO, SZ_ERVNY ) VALUES '
    + '( ''888'', ''120'', ''' + IntToStr(sorszam) +
    ''', ''Az utols� XML export sorsz�m '', 1 ) ', false);

  if kellujfajl then
    XMLDoc1.SaveToFile(fn);

  Try
    if kellujfajl then begin
      ExcelApp := CreateOleObject('Excel.Application');
      ExcelApp.Workbooks.open(fn);
      ExcelApp.Visible := true;
    end;
  Except
  End;

  voltkilepes := kilepes;
  if ((EgyebDlg.ellenlista.Count > 0) and kelluzenet) then begin
    EllenListMutat('A gener�l�s eredm�nye', false);
  end;
  kilepes := true;
end;

procedure TXML_ExportDlg.BitBtn5Click(Sender: TObject);
begin
  // T�bb vev� kiv�laszt�sa
  Application.CreateForm(TValvevoDlg, ValvevoDlg);
  ValvevoDlg.Tolt(M50.Text);
  ValvevoDlg.ShowModal;
  if ValvevoDlg.kodkilist.Count > 0 then
  begin
    M5.Text := ValvevoDlg.nevlista;
    M50.Text := ValvevoDlg.kodlista;
  end;
  ValvevoDlg.Destroy;
end;

function TXML_ExportDlg.XML_Generalas2: boolean;
var
  fn: String;
  XMLDoc1: TXMLDocument;
  DocNode: IXMLNode;
  ShipNode: IXMLNode;
  FieldNode: IXMLNode;
  sorszam: integer;
  ujkod: string;
  eurnetto: double;
  eurafasz: double;
  eurafaer: double;
  gktipus: string;
  gkkat: string;
  laneid: string;
  lanossz: double;
  euado: string;
  szamlastr: string;
  szamszam: string;
  szamdb: integer;
  szamdb2: integer;
  suly: double;
  potlek: double;
  spedicio: double;
  egyedidij: boolean;
  kelluzpotl: boolean;
begin
  result := true;
  // Query_Run(Query1, 'SELECT MB_MBKOD, MB_RENSZ, MB_POZIC, MB_FUTID, MB_FUDIJ, MB_VEKOD, MB_VIKOD, MB_JAKOD, MB_DATUM, MB_VENEV, MB_JASZA,MB_EDIJ,MB_NUZPOT FROM MEGBIZAS, VISZONY WHERE MB_DATUM > '''+EgyebDlg.EvSzam+''' '+
  Query_Run(Query1,
    'SELECT MB_KISCS, MB_MBKOD, MB_RENSZ, MB_POZIC, MB_FUTID, MB_FUDIJ, MB_VEKOD, MB_VIKOD, MB_JAKOD, MB_DATUM, MB_VENEV, MB_JASZA,MB_EDIJ,MB_NUZPOT FROM MEGBIZAS, VISZONY WHERE MB_DATUM > '''
    + '2000' + ''' ' + ' AND MB_VIKOD = VI_VIKOD AND MB_JAKOD = VI_JAKOD  AND vi_ujkod=' +
    M3.Text + '');
  // Az �llom�ny nev�nek �sszerak�sa
  // fn	:= 'TESZT_JS'+copy(EgyebDlg.EvSzam, 3, 2)+'_'+Format('%4.4d',[StrToIntDef(M3.Text,0)])+'.xml';
  // fn	:= 'TESZT_'+EgyebDlg.Read_SZGrid('CEG', '310')+Format('%5.5d',[StrToIntDef(M3.Text,0)])+'.xml';
  fn := 'TESZT_' + szamszam + '.xml';
  if FileExists(fn) then begin
    DeleteFile(PChar(fn));
  end;
  EllenListTorol;
  if Query1.RecordCount < 1 then
  begin
    EllenListAppend('Nincs ilyen t�tel a Megb�z�sban!  ' + 'UJKOD = ' +
      Query1.FieldByName('MB_VIKOD').AsString);
    EllenListMutat('A gener�l�s eredm�nye', false);
    result := false;
    exit;
  end;
  // Az XML megnyit�sa
  XMLDoc1 := TXMLDocument.Create(nil);
  XMLDoc1.Active := true;
  XMLDoc1.Version := '1.0';
  DocNode := XMLDoc1.AddChild('DOCUMENT');
  Query_Run(EgyebDlg.QueryKozos,
    'SELECT SZ_MENEV FROM SZOTAR WHERE SZ_FOKOD = ''888'' AND SZ_ALKOD = ''120'' ',
    false);
  sorszam := StrToIntDef(EgyebDlg.QueryKozos.FieldByName('SZ_MENEV')
    .AsString, 1);
  kilepes := false;
  szamlastr := ';';
  // egyedidij:= Query1.FieldByName('MB_DATUM').AsInteger=1;
  while ((not Query1.Eof) and (not kilepes)) do
  begin
    egyedidij := Query1.FieldByName('MB_EDIJ').AsInteger = 1;
    Application.ProcessMessages;
    kelluzpotl := StrToIntDef(Query1.FieldByName('MB_NUZPOT').AsString, 0) = 0;
    // Label4.Caption	:= 'D�tum : '+Query1.FieldByName('MB_DATUM').AsString + ' - megb�z�s k�d : ' +Query1.FieldByName('MB_MBKOD').AsString;
    // Label4.Update;
    // Megkeress�k a megb�z�shoz tartoz� sz�ml�t
    if Query1.FieldByName('MB_VIKOD').AsString = '' then
    begin
      EllenListAppend('Nincs viszonylat : MBKOD : ' +
        Query1.FieldByName('MB_MBKOD').AsString);
      Query1.Next;
      Continue;
    end;
    ujkod := '';
    Query_Run(Query3, 'SELECT * FROM VISZONY WHERE VI_VIKOD = ' +
      Query1.FieldByName('MB_VIKOD').AsString + ' AND VI_JAKOD = ''' +
      Query1.FieldByName('MB_JAKOD').AsString + ''' ');
    if Query3.RecordCount = 1 then
    begin
      ujkod := Query3.FieldByName('VI_UJKOD').AsString
    end;
    if Query3.RecordCount < 1 then
    begin
      EllenListAppend('Nincs megfelel� viszony : sz�mlasz�m : ' + Trim(M3.Text)
        + ' vikod = ' + Query1.FieldByName('MB_VIKOD').AsString + '  J�rat: ' +
        Query1.FieldByName('MB_JASZA').AsString + '  Rendsz�m : ' +
        Query1.FieldByName('MB_RENSZ').AsString);
    end;
    if Query3.RecordCount > 1 then
    begin
      EllenListAppend('T�l sok viszony : sz�mlasz�m : ' + Trim(M3.Text) +
        ' vikod = ' + Query1.FieldByName('MB_VIKOD').AsString);
    end;
    // �sszehasonl�tjuk a sz�mla t�teleket �s a megb�z�sokat
    // szamszam	:= Query3.FieldByName('VI_SAKOD').AsString;
    szamszam := Query3.FieldByName('VI_UJKOD').AsString;
    if szamszam = '' then
    begin
      // Nincs sz�mlasz�m, nem kell feldolgozni a t�telt !!!
      EllenListAppend('Nincs sz�mlasz�m : vikod = ' +
        Query1.FieldByName('MB_VIKOD').AsString + '  MBKOD = ' +
        Query1.FieldByName('MB_MBKOD').AsString);
      Query1.Next;
      Continue;
    end;
    if Pos(';' + szamszam + ';', szamlastr) < 1 then
    begin
      szamlastr := szamlastr + szamszam + ';';
      Query_Run(Query2, 'SELECT COUNT(*) DB FROM SZFEJ WHERE SA_UJKOD = ''' +
        szamszam + ''' ');
      szamdb := StrToIntDef(Query2.FieldByName('DB').AsString, 0);
      Query_Run(Query2,
        'SELECT COUNT(*) DB FROM MEGBIZAS, VISZONY WHERE MB_VIKOD = VI_VIKOD AND MB_JAKOD = VI_JAKOD AND  VI_UJKOD = '''
        + szamszam + ''' ');
      szamdb2 := StrToIntDef(Query2.FieldByName('DB').AsString, 0);
      if szamdb <> szamdb2 then
      begin
        // A m�sodik fajta ellen�rz�s !!!
        { Query_Run(Query2, 'SELECT SA_UJKOD UJKOD FROM SZFEJ WHERE SA_KOD = '''+szamszam+''' AND SA_UJKOD NOT IN (SELECT VI_UJKOD FROM MEGBIZAS, VISZONY WHERE MB_VIKOD = VI_VIKOD AND MB_JAKOD = VI_JAKOD AND  VI_SAKOD = '''+szamszam+''') UNION '+
          'SELECT VI_UJKOD UJKOD FROM MEGBIZAS, VISZONY WHERE MB_VIKOD = VI_VIKOD AND MB_JAKOD = VI_JAKOD AND  VI_SAKOD = '''+szamszam+''' AND VI_UJKOD NOT IN (SELECT SA_UJKOD FROM SZFEJ WHERE SA_KOD = '''+szamszam+''') ');
          if Query2.RecordCount > 0 then begin
          szamhiany := '';
          while not Query2.Eof do begin
          szamhiany := szamhiany + Query2.FieldByName('UJKOD').AsString+', ';
          Query2.Next;
          end;
          end; }
        EllenListAppend
          ('   A SZ�MLA T�TEL �S A MEGB�Z�SOK SZ�MA ELT�R�! Sz�mlasz�m : ' +
          szamszam + '  ' + Format('%d <> %d', [szamdb, szamdb2]));
      end;
    end;
    Query_Run(Query3, 'SELECT * FROM SZFEJ WHERE SA_UJKOD = ' +
      IntToStr(StrToIntDef(ujkod, 0)));
    if Query3.RecordCount < 1 then
    begin
      EllenListAppend('Nincs sz�mla! Sz�mlak�d : ' + IntToStr(StrToIntDef(ujkod,
        0)) + '  J�rat: ' + Query1.FieldByName('MB_JASZA').AsString +
        '  Rendsz�m : ' + Query1.FieldByName('MB_RENSZ').AsString);
      Query1.Next;
      Continue;
    end;
    // A LAN ID ellen�rz�se
    laneid := Query_Select('FUVARTABLA', 'FT_FTKOD',
      IntToStr(StrToIntDef(Query1.FieldByName('MB_FUTID').AsString, 0)),
      'FT_LANID');
    if laneid = '' then
    begin
      EllenListAppend('Hi�nyz� LAN_ID! Sz�mlasz�m : ' +
        Query3.FieldByName('SA_KOD').AsString + '  J�rat: ' +
        Query1.FieldByName('MB_JASZA').AsString + '  Rendsz�m : ' +
        Query1.FieldByName('MB_RENSZ').AsString);
      Query1.Next;
      Continue;
    end;
    // A sz�ll�t�si f�t�tel l�trehoz�sa
    ShipNode := DocNode.AddChild('shipment');
    // A mez�k gener�l�sa
    FieldNode := ShipNode.AddChild('feedno');
    FieldNode.Text := IntToStr(sorszam);
    FieldNode := ShipNode.AddChild('scac');
    FieldNode.Text := 'HU12939848';
    euado := Query_Select('VEVO', 'V_KOD', Query1.FieldByName('MB_VEKOD')
      .AsString, 'VE_EUADO');
    FieldNode := ShipNode.AddChild('clientno');
    FieldNode.Text := euado;
    FieldNode := ShipNode.AddChild('clientname');
    FieldNode.Text := Query1.FieldByName('MB_VENEV').AsString;
    FieldNode := ShipNode.AddChild('carracct');
    FieldNode.Text := Query1.FieldByName('MB_VEKOD').AsString;
    FieldNode := ShipNode.AddChild('paid_flag');
    FieldNode.Text := 'EUR';
    FieldNode := ShipNode.AddChild('mastinv_no');
    FieldNode.Text := Query3.FieldByName('SA_KOD').AsString;
    FieldNode := ShipNode.AddChild('inv_date');
    FieldNode.Text := copy(Query3.FieldByName('SA_KIDAT').AsString, 1, 4) +
      copy(Query3.FieldByName('SA_KIDAT').AsString, 6, 2) +
      copy(Query3.FieldByName('SA_KIDAT').AsString, 9, 2);
    FieldNode := ShipNode.AddChild('rccode');
    FieldNode.Text := Query3.FieldByName('SA_MEGRE').AsString;
    FieldNode := ShipNode.AddChild('pronumber');
    FieldNode.Text := Query1.FieldByName('MB_MBKOD').AsString;
    // Megkeress�k az els� felrak�st
    Query_Run(Query4,
      'SELECT MS_FETDAT, MS_GKKAT FROM MEGSEGED WHERE MS_MBKOD = ' +
      Query1.FieldByName('MB_MBKOD').AsString +
      ' AND MS_TIPUS = 0 ORDER BY MS_MSKOD ');
    if Query4.RecordCount > 0 then
    begin
      FieldNode := ShipNode.AddChild('prodate');
      FieldNode.Text := copy(Query4.FieldByName('MS_FETDAT').AsString, 1, 4) +
        copy(Query4.FieldByName('MS_FETDAT').AsString, 6, 2) +
        copy(Query4.FieldByName('MS_FETDAT').AsString, 9, 2);
    end
    else
    begin
      FieldNode := ShipNode.AddChild('prodate');
      FieldNode.Text := '';
    end;
    gkkat := Query4.FieldByName('MS_GKKAT').AsString;
    // A CMR sz�m meghat�roz�sa
    FieldNode := ShipNode.AddChild('lading');
    FieldNode.Text := Query_Select('CMRDAT', 'CM_VIKOD',
      Query1.FieldByName('MB_VIKOD').AsString, 'CM_CSZAM');
    FieldNode := ShipNode.AddChild('po'); // Poz�ci�sz�m
    FieldNode.Text := Query1.FieldByName('MB_POZIC').AsString;
    FieldNode := ShipNode.AddChild('pieces');
    FieldNode.Text := '1.00';
    Query_Run(Query4,
      'SELECT SUM(MS_FEPAL) PALETTAK FROM MEGSEGED WHERE MS_MBKOD = ' +
      Query1.FieldByName('MB_MBKOD').AsString + ' AND MS_TIPUS = 0');
    FieldNode := ShipNode.AddChild('pallets');
    FieldNode.Text := SqlSzamString(StringSzam(Query4.FieldByName('PALETTAK')
      .AsString), 12, 2);
    suly := GetKobSuly(Query1.FieldByName('MB_MBKOD').AsString);
    if suly = 0 then
    begin
      suly := GetSuly(Query1.FieldByName('MB_MBKOD').AsString);
    end;
    FieldNode := ShipNode.AddChild('weight');
    FieldNode.Text := SqlSzamString(suly, 12, 2);
    FieldNode := ShipNode.AddChild('wt_flag');
    FieldNode.Text := '1';
    FieldNode := ShipNode.AddChild('cubicmt');
    FieldNode.Text := '0.00';
    FieldNode := ShipNode.AddChild('dim_wt');
    FieldNode.Text := SqlSzamString(suly, 12, 2);
    FieldNode := ShipNode.AddChild('loadingmt');
    FieldNode.Text := '0.00';
    FieldNode := ShipNode.AddChild('terms');
    FieldNode.Text := '1';
    FieldNode := ShipNode.AddChild('premiumflg');
    FieldNode.Text := '0';
    FieldNode := ShipNode.AddChild('inco_terms');
    FieldNode.Text := 'EXW';
    FieldNode := ShipNode.AddChild('mode');
    FieldNode.Text := 'GROUND';
    FieldNode := ShipNode.AddChild('serv_type');
    FieldNode.Text := 'DTD';
    // A lane_id t�lt�se
    lanossz := StringSzam(Query1.FieldByName('MB_FUDIJ').AsString);
    gktipus := Query_Select('FUVARTABLA', 'FT_FTKOD',
      IntToStr(StrToIntDef(Query1.FieldByName('MB_FUTID').AsString, 0)),
      // 'FT_KATEG');
      'FT_GKKAT');
    if gktipus = '' then
    begin
      gktipus := Query1.FieldByName('MB_GKKAT').AsString;
    end;
    // A g�pkocsi t�pus bet�lt�se
    if copy(gktipus, 1, 3) = '1,5' then begin
      gktipus := '1.5T';
    end else begin
      if copy(gktipus, 1, 3) = '3,5' then begin
        gktipus := '3.5T';
      end else begin
        if copy(gktipus, 1, 2) = '24' then begin
          gktipus := '24T';
        end else begin
          gktipus := '';
        end;
      end;
    end;
    SulyEllenor(suly, gktipus, Query1.FieldByName('MB_MBKOD').AsString);

    FieldNode := ShipNode.AddChild('serv_categ');
    FieldNode.Text := gktipus;
    FieldNode := ShipNode.AddChild('lane_id');
    if egyedidij then
      FieldNode.Text := ''
    else
      FieldNode.Text := laneid;
    FieldNode := ShipNode.AddChild('serv_level');
    // Az express - standard besz�r�sa
    if StrToIntDef(Query1.FieldByName('MB_KISCS').AsString, 0) > 0 then begin
      FieldNode.Text := 'express ';
    end else begin
      FieldNode.Text := 'standard';
    end;

    // Az utols� lerak� meghat�roz�sa
    Query_Run(Query4,
      'SELECT MS_FELNEV, MS_FELIR, MS_FORSZ, MS_FELTEL, MS_LERNEV, MS_LELIR, MS_ORSZA, MS_LERTEL FROM MEGSEGED WHERE MS_MBKOD = '
      + Query1.FieldByName('MB_MBKOD').AsString +
      ' AND MS_TIPUS = 1 ORDER BY MS_MSKOD ');
    Query4.Last;

    FieldNode := ShipNode.AddChild('consignor');
    FieldNode.Text := Query4.FieldByName('MS_FELNEV').AsString;
    FieldNode := ShipNode.AddChild('origin');
    FieldNode.Text := Query4.FieldByName('MS_FELIR').AsString;
    FieldNode := ShipNode.AddChild('org_cty');
    FieldNode.Text := Query4.FieldByName('MS_FORSZ').AsString;
    FieldNode := ShipNode.AddChild('org_state');
    FieldNode.Text := '';
    FieldNode := ShipNode.AddChild('org_city');
    FieldNode.Text := Query4.FieldByName('MS_FELTEL').AsString;

    FieldNode := ShipNode.AddChild('org_apt');
    FieldNode.Text := '';
    FieldNode := ShipNode.AddChild('consignee');
    FieldNode.Text := Query4.FieldByName('MS_LERNEV').AsString;
    FieldNode := ShipNode.AddChild('dest');
    FieldNode.Text := Query4.FieldByName('MS_LELIR').AsString;
    FieldNode := ShipNode.AddChild('dest_cty');
    FieldNode.Text := Query4.FieldByName('MS_ORSZA').AsString;
    FieldNode := ShipNode.AddChild('dest_state');
    FieldNode.Text := '';
    FieldNode := ShipNode.AddChild('dest_city');
    FieldNode.Text := Query4.FieldByName('MS_LERTEL').AsString;
    FieldNode := ShipNode.AddChild('dest_apt');
    FieldNode.Text := '';

    JSzamla := TJSzamla.Create(Self);
    JSzamla.FillSzamla(ujkod);
    eurnetto := JSzamla.ossz_nettoeur;
    if eurnetto <> 0 then
    begin
      eurafasz := Kerekit(JSzamla.ossz_afaeur * 100 / eurnetto);
    end;
    eurafaer := JSzamla.ossz_afaeur;
    if kelluzpotl and (JSzamla.uapotlekeur = 0) then
    begin
      EllenListAppend('Az �zemanyag p�tl�k �sszege 0! Sz�mlasz�m : ' +
        JSzamla.szamlaszam);
    end
    else
    begin
      if (Abs(JSzamla.ossz_nettoeur - JSzamla.spedicioeur - JSzamla.uapotlekeur - lanossz) > 0.00001) then
      begin
        EllenListAppend
          ('A sz�mla nett� eur - sped�ci�s d�j <> megb�z�si fuvard�j! Sz�mla : '
          + JSzamla.szamlaszam + '  Megb�z�s k�d : ' +
          Query1.FieldByName('MB_MBKOD').AsString);
      end;
    end;
    // Mindig mentj�k a sort !!!
    potlek := JSzamla.uapotlekeur;
    spedicio := JSzamla.spedicioeur;
    SorLogolas(gr, Query1.FieldByName('MB_MBKOD').AsString, suly, lanossz, JSzamla.uapotlekeur, 4);
    JSzamla.Destroy;

    FieldNode := ShipNode.AddChild('billed');
    FieldNode.Text := SqlSzamString(eurnetto + eurafaer, 12, 2);
    FieldNode := ShipNode.AddChild('vatperc');
    FieldNode.Text := SqlSzamString(eurafasz, 12, 2);
    FieldNode := ShipNode.AddChild('vat');
    FieldNode.Text := SqlSzamString(eurafaer, 12, 2);
    FieldNode := ShipNode.AddChild('trans');
    if egyedidij then
      FieldNode.Text := SqlSzamString(lanossz, 12, 2)
    else
      FieldNode.Text := SqlSzamString(eurnetto + eurafaer, 12, 2);
    // FieldNode.Text		 	:= SqlSzamString(lanossz, 12, 2) ;
    // FieldNode.Text		 	:= SqlSzamString(eurnetto+eurafaer, 12, 2) ;
    FieldNode := ShipNode.AddChild('fuel_schgperc');
    if lanossz = 0 then begin
      FieldNode.Text := '0';
    end else begin
       FieldNode.Text := SqlSzamString(potlek * 100 / lanossz, 12, 2);
       if StringSzam(Format('%.2f', [potlek * 100 / lanossz])) <> StringSzam(Format('%.0f', [potlek * 100 / lanossz])) then begin
           EllenListAppend('�zemanyag szorz� miatti hiba! '+Format('%.2f', [potlek * 100 / lanossz]));
       end;
    end;
    FieldNode := ShipNode.AddChild('fuel_schg');
    FieldNode.Text := SqlSzamString(potlek, 12, 2);
    FieldNode := ShipNode.AddChild('handling');
    FieldNode.Text := SqlSzamString(spedicio, 12, 2);
    FieldNode := ShipNode.AddChild('misc');
    FieldNode.Text := '0.00';
    FieldNode := ShipNode.AddChild('toll_fee');
    FieldNode.Text := '0.00';
    FieldNode := ShipNode.AddChild('insurance');
    FieldNode.Text := '0.00';
    // A sorsz�m bejegyz�se a megb�z�shoz
    // Query_Run(Query2, 'UPDATE MEGBIZAS SET MB_XMLSZ = '+IntToStr(sorszam)+' WHERE MB_MBKOD = '+Query1.FieldByName('MB_MBKOD').AsString);
    // Inc(sorszam);
    Query1.Next;
  end;
  // Query_Run(EgyebDlg.QueryKozos,  'DELETE FROM SZOTAR WHERE SZ_FOKOD = ''888'' AND SZ_ALKOD = ''120'' ', FALSE);
  // Query_Run(EgyebDlg.QueryKozos,  'INSERT INTO SZOTAR ( SZ_FOKOD, SZ_ALKOD, SZ_MENEV, SZ_LEIRO, SZ_ERVNY ) VALUES '+
  // '( ''888'', ''120'', '''+IntToStr(sorszam)+''', ''Az utols� XML export sorsz�m '', 1 ) ', FALSE);
  XMLDoc1.SaveToFile(fn);
  voltkilepes := kilepes;
  if ((EgyebDlg.ellenlista.Count > 0)) then
  begin
    EllenListMutat('A gener�l�s eredm�nye', false);
    result := false;
  end;
  // Label4.Caption	:= '';
  // Label4.Update;
  kilepes := true;
end;

function TXML_ExportDlg.XML_Generalas3: boolean;
// Csak ellen�rz�s a gy�jt� sz�ml�khoz
var
  fn: String;
  XMLDoc1: TXMLDocument;
  DocNode: IXMLNode;
  ShipNode: IXMLNode;
  FieldNode: IXMLNode;
  sorszam: integer;
  ujkod: string;
  eurnetto: double;
  eurafasz: double;
  eurafaer: double;
  gktipus: string;
  gkkat: string;
  laneid: string;
  lanossz: double;
  euado: string;
  szamlastr: string;
  szamszam: string;
  szamdb: integer;
  szamdb2: integer;
  suly: double;
  potlek: double;
  spedicio: double;
  kelluzpotl: boolean;
begin
  result := true;
  // fn	:= 'TESZT_JS'+copy(EgyebDlg.EvSzam, 3, 2)+'_'+Format('%4.4d',[StrToIntDef(M3.Text,0)])+'.xml';
  // fn	:= 'TESZT_'+EgyebDlg.Read_SZGrid('CEG', '310')+Format('%5.5d',[StrToIntDef(M3.Text,0)])+'.xml';
  fn := 'TESZT_' + szamszam + '.xml';
  if FileExists(fn) then
  begin
    DeleteFile(PChar(fn));
  end;
  EllenListTorol;
  Query_Run(Query5, 'SELECT * FROM SZSOR2 WHERE S2_S2KOD=' + M3.Text +
    ' order by s2_s2sor' + '');
  while not Query5.Eof do begin
    // Query_Run(Query1, 'SELECT MB_MBKOD, MB_RENSZ, MB_POZIC, MB_FUTID, MB_FUDIJ, MB_VEKOD, MB_VIKOD, MB_JAKOD, MB_DATUM, MB_VENEV, MB_JASZA,MB_NUZPOT FROM MEGBIZAS, VISZONY WHERE MB_DATUM > '''+EgyebDlg.EvSzam+''' '+
    Query_Run(Query1,
      'SELECT MB_KISCS, MB_MBKOD, MB_RENSZ, MB_POZIC, MB_FUTID, MB_FUDIJ, MB_VEKOD, MB_VIKOD, MB_JAKOD, MB_DATUM, MB_VENEV, MB_JASZA,MB_NUZPOT FROM MEGBIZAS, VISZONY WHERE MB_DATUM > '''
      + '2000' + ''' ' + ' AND MB_VIKOD = VI_VIKOD AND MB_JAKOD = VI_JAKOD  AND vi_ujkod=' +
      Query5.FieldByName('S2_UJKOD').AsString + '');
    // Az �llom�ny nev�nek �sszerak�sa
    if Query1.RecordCount < 1 then begin
       EllenListAppend('Nincs ilyen t�tel a Megb�z�sban!  ' + 'Sz�mlak�d = ' +
        Query5.FieldByName('S2_UJKOD').AsString);
       EllenListMutat('A gener�l�s eredm�nye', false);
       result := false;
       exit;
    end;
    // Az XML megnyit�sa
    XMLDoc1 := TXMLDocument.Create(nil);
    XMLDoc1.Active := true;
    XMLDoc1.Version := '1.0';
    DocNode := XMLDoc1.AddChild('DOCUMENT');
    Query_Run(EgyebDlg.QueryKozos,
      'SELECT SZ_MENEV FROM SZOTAR WHERE SZ_FOKOD = ''888'' AND SZ_ALKOD = ''120'' ',
      false);
    sorszam := StrToIntDef(EgyebDlg.QueryKozos.FieldByName('SZ_MENEV')
      .AsString, 1);
    kilepes := false;
    szamlastr := ';';
    while ((not Query1.Eof) and (not kilepes)) do begin
      Application.ProcessMessages;
      kelluzpotl := StrToIntDef(Query1.FieldByName('MB_NUZPOT')
        .AsString, 0) = 0;
      // Label4.Caption	:= 'D�tum : '+Query1.FieldByName('MB_DATUM').AsString + ' - megb�z�s k�d : ' +Query1.FieldByName('MB_MBKOD').AsString;
      // Label4.Update;
      // Megkeress�k a megb�z�shoz tartoz� sz�ml�t
      if Query1.FieldByName('MB_VIKOD').AsString = '' then
      begin
        EllenListAppend('Nincs viszonylat : MBKOD : ' +
          Query1.FieldByName('MB_MBKOD').AsString);
        Query1.Next;
        Continue;
      end;
      ujkod := '';
      Query_Run(Query3, 'SELECT * FROM VISZONY WHERE VI_VIKOD = ' +
        Query1.FieldByName('MB_VIKOD').AsString + ' AND VI_JAKOD = ''' +
        Query1.FieldByName('MB_JAKOD').AsString + ''' ');
      if Query3.RecordCount = 1 then
      begin
        ujkod := Query3.FieldByName('VI_UJKOD').AsString
      end;
      if Query3.RecordCount < 1 then
      begin
        EllenListAppend('Nincs megfelel� viszony : sz�mlasz�m : ' +
          Trim(M3.Text) + ' vikod = ' + Query1.FieldByName('MB_VIKOD').AsString
          + '  J�rat: ' + Query1.FieldByName('MB_JASZA').AsString +
          '  Rendsz�m : ' + Query1.FieldByName('MB_RENSZ').AsString);
      end;
      if Query3.RecordCount > 1 then
      begin
        EllenListAppend('T�l sok viszony : sz�mlasz�m : ' + Trim(M3.Text) +
          ' vikod = ' + Query1.FieldByName('MB_VIKOD').AsString);
      end;
      // SZFEJ2 vev� �s MEGBIZAS vev� �sszehaonl�t�sa
      if Query1.FieldByName('MB_VEKOD').AsString <> Trim(M50.Text) then
      begin
        EllenListAppend('Gy�jt� sz�mla - Megb�z�s vev�k�d elt�r : ' +
          Trim(M50.Text) + ' - ' + Query1.FieldByName('MB_VEKOD').AsString +
          ' (' + Query1.FieldByName('MB_MBKOD').AsString + ')');
      end;

      /// ///////////////////////////////////////////////

       // �sszehasonl�tjuk a sz�mla t�teleket �s a megb�z�sokat
       szamszam := Query3.FieldByName('VI_UJKOD').AsString;
           if szamszam = '' then begin
           // Nincs sz�mlasz�m, nem kell feldolgozni a t�telt !!!
           EllenListAppend('Nincs sz�mlasz�m : vikod = ' +
          Query1.FieldByName('MB_VIKOD').AsString + '  MBKOD = ' +
          Query1.FieldByName('MB_MBKOD').AsString);
        Query1.Next;
        Continue;
      end;
      if Pos(';' + szamszam + ';', szamlastr) < 1 then
      begin
        szamlastr := szamlastr + szamszam + ';';
        Query_Run(Query2, 'SELECT COUNT(*) DB FROM SZFEJ WHERE SA_UJKOD = ''' +
          szamszam + ''' ');
        szamdb := StrToIntDef(Query2.FieldByName('DB').AsString, 0);
        Query_Run(Query2,
          'SELECT COUNT(*) DB FROM MEGBIZAS, VISZONY WHERE MB_VIKOD = VI_VIKOD AND MB_JAKOD = VI_JAKOD AND  VI_UJKOD = '''
          + szamszam + ''' ');
        szamdb2 := StrToIntDef(Query2.FieldByName('DB').AsString, 0);
        if szamdb <> szamdb2 then
        begin
          // A m�sodik fajta ellen�rz�s !!!
          { Query_Run(Query2, 'SELECT SA_UJKOD UJKOD FROM SZFEJ WHERE SA_KOD = '''+szamszam+''' AND SA_UJKOD NOT IN (SELECT VI_UJKOD FROM MEGBIZAS, VISZONY WHERE MB_VIKOD = VI_VIKOD AND MB_JAKOD = VI_JAKOD AND  VI_SAKOD = '''+szamszam+''') UNION '+
            'SELECT VI_UJKOD UJKOD FROM MEGBIZAS, VISZONY WHERE MB_VIKOD = VI_VIKOD AND MB_JAKOD = VI_JAKOD AND  VI_SAKOD = '''+szamszam+''' AND VI_UJKOD NOT IN (SELECT SA_UJKOD FROM SZFEJ WHERE SA_KOD = '''+szamszam+''') ');
            if Query2.RecordCount > 0 then begin
            szamhiany := '';
            while not Query2.Eof do begin
            szamhiany := szamhiany + Query2.FieldByName('UJKOD').AsString+', ';
            Query2.Next;
            end;
            end; }
          EllenListAppend
            ('   A SZ�MLA T�TEL �S A MEGB�Z�SOK SZ�MA ELT�R�! Sz�mlasz�m : ' +
            szamszam + '  ' + Format('%d <> %d', [szamdb, szamdb2]));
        end;
      end;
      Query_Run(Query3, 'SELECT * FROM SZFEJ WHERE SA_UJKOD = ' +
        IntToStr(StrToIntDef(ujkod, 0)));
      if Query3.RecordCount < 1 then
      begin
        EllenListAppend('Nincs sz�mla! Sz�mlak�d : ' +
          IntToStr(StrToIntDef(ujkod, 0)) + '  J�rat: ' +
          Query1.FieldByName('MB_JASZA').AsString + '  Rendsz�m : ' +
          Query1.FieldByName('MB_RENSZ').AsString);
        Query1.Next;
        Continue;
      end;
      // A LAN ID ellen�rz�se
      laneid := Query_Select('FUVARTABLA', 'FT_FTKOD',
        IntToStr(StrToIntDef(Query1.FieldByName('MB_FUTID').AsString, 0)),
        'FT_LANID');
      if laneid = '' then
      begin
        EllenListAppend('Hi�nyz� LAN_ID! Sz�mlasz�m : ' +
          Query3.FieldByName('SA_KOD').AsString + '  J�rat: ' +
          Query1.FieldByName('MB_JASZA').AsString + '  Rendsz�m : ' +
          Query1.FieldByName('MB_RENSZ').AsString);
        Query1.Next;
        Continue;
      end;
      // A sz�ll�t�si f�t�tel l�trehoz�sa
      ShipNode := DocNode.AddChild('shipment');
      // A mez�k gener�l�sa
      FieldNode := ShipNode.AddChild('feedno');
      FieldNode.Text := IntToStr(sorszam);
      FieldNode := ShipNode.AddChild('scac');
      FieldNode.Text := 'HU12939848';
      euado := Query_Select('VEVO', 'V_KOD', Query1.FieldByName('MB_VEKOD')
        .AsString, 'VE_EUADO');
      FieldNode := ShipNode.AddChild('clientno');
      FieldNode.Text := euado;
      FieldNode := ShipNode.AddChild('clientname');
      FieldNode.Text := Query1.FieldByName('MB_VENEV').AsString;
      FieldNode := ShipNode.AddChild('carracct');
      FieldNode.Text := Query1.FieldByName('MB_VEKOD').AsString;
      FieldNode := ShipNode.AddChild('paid_flag');
      FieldNode.Text := 'EUR';
      FieldNode := ShipNode.AddChild('mastinv_no');
      FieldNode.Text := Query3.FieldByName('SA_KOD').AsString;
      FieldNode := ShipNode.AddChild('inv_date');
      FieldNode.Text := copy(Query3.FieldByName('SA_KIDAT').AsString, 1, 4) +
        copy(Query3.FieldByName('SA_KIDAT').AsString, 6, 2) +
        copy(Query3.FieldByName('SA_KIDAT').AsString, 9, 2);
      FieldNode := ShipNode.AddChild('rccode');
      FieldNode.Text := Query3.FieldByName('SA_MEGRE').AsString;
      FieldNode := ShipNode.AddChild('pronumber');
      FieldNode.Text := Query1.FieldByName('MB_MBKOD').AsString;
      // Megkeress�k az els� felrak�st
      Query_Run(Query4,
        'SELECT MS_FETDAT, MS_GKKAT FROM MEGSEGED WHERE MS_MBKOD = ' +
        Query1.FieldByName('MB_MBKOD').AsString +
        ' AND MS_TIPUS = 0 ORDER BY MS_MSKOD ');
      if Query4.RecordCount > 0 then
      begin
        FieldNode := ShipNode.AddChild('prodate');
        FieldNode.Text := copy(Query4.FieldByName('MS_FETDAT').AsString, 1, 4) +
          copy(Query4.FieldByName('MS_FETDAT').AsString, 6, 2) +
          copy(Query4.FieldByName('MS_FETDAT').AsString, 9, 2);
      end
      else
      begin
        FieldNode := ShipNode.AddChild('prodate');
        FieldNode.Text := '';
      end;
      gkkat := Query4.FieldByName('MS_GKKAT').AsString;
      // A CMR sz�m meghat�roz�sa
      FieldNode := ShipNode.AddChild('lading');
      FieldNode.Text := Query_Select('CMRDAT', 'CM_VIKOD',
        Query1.FieldByName('MB_VIKOD').AsString, 'CM_CSZAM');
      FieldNode := ShipNode.AddChild('po'); // Poz�ci�sz�m
      FieldNode.Text := Query1.FieldByName('MB_POZIC').AsString;
      FieldNode := ShipNode.AddChild('pieces');
      FieldNode.Text := '1.00';
      Query_Run(Query4,
        'SELECT SUM(MS_FEPAL) PALETTAK FROM MEGSEGED WHERE MS_MBKOD = ' +
        Query1.FieldByName('MB_MBKOD').AsString + ' AND MS_TIPUS = 0');
      FieldNode := ShipNode.AddChild('pallets');
      FieldNode.Text := SqlSzamString
        (StringSzam(Query4.FieldByName('PALETTAK').AsString), 12, 2);
      suly := GetKobSuly(Query1.FieldByName('MB_MBKOD').AsString);
      if suly = 0 then
      begin
        suly := GetSuly(Query1.FieldByName('MB_MBKOD').AsString);
      end;
      FieldNode := ShipNode.AddChild('weight');
      FieldNode.Text := SqlSzamString(suly, 12, 2);
      FieldNode := ShipNode.AddChild('wt_flag');
      FieldNode.Text := '1';
      FieldNode := ShipNode.AddChild('cubicmt');
      FieldNode.Text := '0.00';
      FieldNode := ShipNode.AddChild('dim_wt');
      FieldNode.Text := SqlSzamString(suly, 12, 2);
      FieldNode := ShipNode.AddChild('loadingmt');
      FieldNode.Text := '0.00';
      FieldNode := ShipNode.AddChild('terms');
      FieldNode.Text := '1';
      FieldNode := ShipNode.AddChild('premiumflg');
      FieldNode.Text := '0';
      FieldNode := ShipNode.AddChild('inco_terms');
      FieldNode.Text := 'EXW';
      FieldNode := ShipNode.AddChild('mode');
      FieldNode.Text := 'GROUND';
      FieldNode := ShipNode.AddChild('serv_type');
      FieldNode.Text := 'DTD';
      // A lane_id t�lt�se
      lanossz := StringSzam(Query1.FieldByName('MB_FUDIJ').AsString);
      gktipus := Query_Select('FUVARTABLA', 'FT_FTKOD',
        IntToStr(StrToIntDef(Query1.FieldByName('MB_FUTID').AsString, 0)),
        // 'FT_KATEG');
        'FT_GKKAT');
      if gktipus = '' then
      begin
        gktipus := Query1.FieldByName('MB_GKKAT').AsString;
      end;
    // A g�pkocsi t�pus bet�lt�se
    if copy(gktipus, 1, 3) = '1,5' then begin
      gktipus := '1.5T';
    end else begin
      if copy(gktipus, 1, 3) = '3,5' then begin
        gktipus := '3.5T';
      end else begin
        if copy(gktipus, 1, 2) = '24' then begin
          gktipus := '24T';
        end else begin
          gktipus := '';
        end;
      end;
    end;
    SulyEllenor(suly, gktipus, Query1.FieldByName('MB_MBKOD').AsString);

      FieldNode := ShipNode.AddChild('serv_categ');
      FieldNode.Text := gktipus;
      FieldNode := ShipNode.AddChild('lane_id');
      FieldNode.Text := laneid;
      FieldNode := ShipNode.AddChild('serv_level');
       // Az express - standard besz�r�sa
       if StrToIntDef(Query1.FieldByName('MB_KISCS').AsString, 0) > 0 then begin
           FieldNode.Text := 'express ';
       end else begin
           FieldNode.Text := 'standard';
       end;

      // Az utols� lerak� meghat�roz�sa
      Query_Run(Query4,
        'SELECT MS_FELNEV, MS_FELIR, MS_FORSZ, MS_FELTEL, MS_LERNEV, MS_LELIR, MS_ORSZA, MS_LERTEL FROM MEGSEGED WHERE MS_MBKOD = '
        + Query1.FieldByName('MB_MBKOD').AsString +
        ' AND MS_TIPUS = 1 ORDER BY MS_MSKOD ');
      Query4.Last;

      FieldNode := ShipNode.AddChild('consignor');
      FieldNode.Text := Query4.FieldByName('MS_FELNEV').AsString;
      FieldNode := ShipNode.AddChild('origin');
      FieldNode.Text := Query4.FieldByName('MS_FELIR').AsString;
      FieldNode := ShipNode.AddChild('org_cty');
      FieldNode.Text := Query4.FieldByName('MS_FORSZ').AsString;
      FieldNode := ShipNode.AddChild('org_state');
      FieldNode.Text := '';
      FieldNode := ShipNode.AddChild('org_city');
      FieldNode.Text := Query4.FieldByName('MS_FELTEL').AsString;

      FieldNode := ShipNode.AddChild('org_apt');
      FieldNode.Text := '';
      FieldNode := ShipNode.AddChild('consignee');
      FieldNode.Text := Query4.FieldByName('MS_LERNEV').AsString;
      FieldNode := ShipNode.AddChild('dest');
      FieldNode.Text := Query4.FieldByName('MS_LELIR').AsString;
      FieldNode := ShipNode.AddChild('dest_cty');
      FieldNode.Text := Query4.FieldByName('MS_ORSZA').AsString;
      FieldNode := ShipNode.AddChild('dest_state');
      FieldNode.Text := '';
      FieldNode := ShipNode.AddChild('dest_city');
      FieldNode.Text := Query4.FieldByName('MS_LERTEL').AsString;
      FieldNode := ShipNode.AddChild('dest_apt');
      FieldNode.Text := '';

      JSzamla := TJSzamla.Create(Self);
      JSzamla.FillSzamla(ujkod);
      eurnetto := JSzamla.ossz_nettoeur;
      if eurnetto <> 0 then
      begin
        eurafasz := Kerekit(JSzamla.ossz_afaeur * 100 / eurnetto);
      end;
      eurafaer := JSzamla.ossz_afaeur;
      if kelluzpotl and (JSzamla.uapotlekeur = 0) then
      begin
        EllenListAppend('Az �zemanyag p�tl�k �sszege 0! Sz�mlasz�m : ' +
          JSzamla.szamlaszam);
      end
      else
      begin
        if (Abs(JSzamla.ossz_nettoeur - JSzamla.spedicioeur -
          JSzamla.uapotlekeur - lanossz) > 0.00001) then
        begin
          EllenListAppend
            ('A sz�mla nett� eur - sped�ci�s d�j <> megb�z�si fuvard�j! Sz�mla : '
            + JSzamla.szamlaszam + '  Megb�z�s k�d : ' +
            Query1.FieldByName('MB_MBKOD').AsString);
        end;
      end;
      // Mindig mentj�k a sort !!!
      potlek := JSzamla.uapotlekeur;
      spedicio := JSzamla.spedicioeur;
      SorLogolas(gr, Query1.FieldByName('MB_MBKOD').AsString, suly, lanossz,
        JSzamla.uapotlekeur, 4);
      JSzamla.Destroy;

      FieldNode := ShipNode.AddChild('billed');
      FieldNode.Text := SqlSzamString(eurnetto + eurafaer, 12, 2);
      FieldNode := ShipNode.AddChild('vatperc');
      FieldNode.Text := SqlSzamString(eurafasz, 12, 2);
      FieldNode := ShipNode.AddChild('vat');
      FieldNode.Text := SqlSzamString(eurafaer, 12, 2);
      FieldNode := ShipNode.AddChild('trans');
      // FieldNode.Text		 	:= SqlSzamString(lanossz, 12, 2) ;
      FieldNode.Text := SqlSzamString(eurnetto + eurafaer, 12, 2);
      FieldNode := ShipNode.AddChild('fuel_schgperc');
      if lanossz = 0 then begin
        FieldNode.Text := '0';
      end else begin
        FieldNode.Text := SqlSzamString(potlek * 100 / lanossz, 12, 2);
        if StringSzam(Format('%.2f', [potlek * 100 / lanossz])) <> StringSzam(Format('%.0f', [potlek * 100 / lanossz])) then begin
           EllenListAppend('�zemanyag szorz� miatti hiba! '+Format('%.2f', [potlek * 100 / lanossz]));
        end;
      end;
      FieldNode := ShipNode.AddChild('fuel_schg');
      FieldNode.Text := SqlSzamString(potlek, 12, 2);
      FieldNode := ShipNode.AddChild('handling');
      FieldNode.Text := SqlSzamString(spedicio, 12, 2);
      FieldNode := ShipNode.AddChild('misc');
      FieldNode.Text := '0.00';
      FieldNode := ShipNode.AddChild('toll_fee');
      FieldNode.Text := '0.00';
      FieldNode := ShipNode.AddChild('insurance');
      FieldNode.Text := '0.00';
      // A sorsz�m bejegyz�se a megb�z�shoz
      // Query_Run(Query2, 'UPDATE MEGBIZAS SET MB_XMLSZ = '+IntToStr(sorszam)+' WHERE MB_MBKOD = '+Query1.FieldByName('MB_MBKOD').AsString);
      // Inc(sorszam);
      Query1.Next;
    end;
    // Query_Run(EgyebDlg.QueryKozos,  'DELETE FROM SZOTAR WHERE SZ_FOKOD = ''888'' AND SZ_ALKOD = ''120'' ', FALSE);
    // Query_Run(EgyebDlg.QueryKozos,  'INSERT INTO SZOTAR ( SZ_FOKOD, SZ_ALKOD, SZ_MENEV, SZ_LEIRO, SZ_ERVNY ) VALUES '+
    // '( ''888'', ''120'', '''+IntToStr(sorszam)+''', ''Az utols� XML export sorsz�m '', 1 ) ', FALSE);
    Query5.Next;
  end;
  XMLDoc1.SaveToFile(fn);
  voltkilepes := kilepes;
  if ((EgyebDlg.ellenlista.Count > 0)) then
  begin
    EllenListMutat('A gener�l�s eredm�nye', false);
    result := false;
  end;
  // Label4.Caption	:= '';
  // Label4.Update;
  kilepes := true;

end;

procedure TXML_ExportDlg.M3KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  // if (Key=VK_RETURN)and(M3.Text<>'') then
  if (Key = VK_RETURN) then
    BitBtn4.OnClick(Self);
end;

procedure TXML_ExportDlg.CheckBox1Click(Sender: TObject);
begin
  M3.SetFocus;
end;

procedure TXML_ExportDlg.SulyEllenor(su : double; kateg, mbkod : string );
var
  jasza: string;
begin
   if kateg = '' then begin
       Exit;
   end;
   if ( ( (su <= 1500) and (kateg <> '1.5T'))  or
        ( (su > 1500) and (su <= 3500) and (kateg <> '3.5T') ) or
        ( (su > 3500) and (kateg <> '24T') ) ) then begin
       jasza:= Query_SelectString('JARAT', 'select JA_ORSZ+''-''+convert(varchar, CONVERT(int, JA_ALKOD)) from JARAT, MEGBIZAS where JA_KOD = MB_JAKOD and MB_MBKOD='+mbkod);
       if jasza='' then jasza:= '(nincs j�rat)';
       EllenListAppend('K�b�s s�ly elt�r�s! '+Format('%.2f', [su])+' <-> '+kateg+'  Megb�z�sk�d: '+mbkod+', J�rat: '+jasza);
       end;
end;

end.
