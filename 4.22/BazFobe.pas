unit Bazfobe;

interface

uses
	SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
	Forms, Dialogs, StdCtrls, Buttons, Mask, DB, DBTables, Egyeb, Kozos, ADODB;

type
	TBazFobeDlg = class(TForm)
	BitElkuld: TBitBtn;
	BitKilep: TBitBtn;
	Label2: TLabel;
	Label3: TLabel;
    M2: TMaskEdit;
    Label1: TLabel;
    M3: TMaskEdit;
    M4: TMaskEdit;
    Label4: TLabel;
    M1: TMaskEdit;
    Query1: TADOQuery;
	procedure 	FormCreate(Sender: TObject);
   function 	Tolt(cim : string; kod : string) : boolean;
	procedure 	BitElkuldClick(Sender: TObject);
	procedure 	BitKilepClick(Sender: TObject);
	private
	public
		ret_kod		: string;
	end;

var
	BazFobeDlg: TBazFobeDlg;

implementation

uses
	J_SQL;
{$R *.DFM}

procedure TBazFobeDlg.FormCreate(Sender: TObject);
begin
	EgyebDlg.SeTADOQueryDatabase(Query1);
	M1.Text 	:= '';
	M2.Text 	:= '';
	M3.Text 	:= '';
	M4.Text 	:= '';
end;

function TBazFobeDlg.Tolt(cim : string; kod : string) : boolean;
begin
	Result 		:= true;
	ret_kod 		:= kod;
	Caption 		:= cim;
	M1.Text 		:= kod;
  	if kod <> '' then begin
		SetMaskEdits([M1]);
 		if Query_Run(Query1,'SELECT * FROM SZOTAR WHERE SZ_FOKOD=''000'' AND SZ_ALKOD='''+kod+''' ') then begin
			if Query1.RecordCount > 0 then begin
				M2.Text 	:= Query1.FieldByName('SZ_MENEV').AsString;
				M3.Text 	:= Query1.FieldByName('SZ_KODHO').AsString;
				M4.Text 	:= Query1.FieldByName('SZ_NEVHO').AsString;
			end;
		end else begin
			Result 	:= false;
		end;
	end;
end;

procedure TBazFobeDlg.BitElkuldClick(Sender: TObject);
begin
	if M1.Text = '' then begin
		NoticeKi('A k�d nem lehet �res!', NOT_ERROR);
		M1.SetFocus;
      	Exit;
	end else begin
   	if ret_kod = '' then begin
			ret_kod		:= M1.Text;
			{F�k�d felvitele}
     		if Query_Run(Query1,'SELECT * FROM SZOTAR WHERE SZ_FOKOD=''000'' AND SZ_ALKOD='''+M1.Text+''' ') then begin
				if Query1.RecordCount > 0 then begin
					NoticeKi('Ez a k�d m�r l�tezik!', NOT_ERROR);
					M1.SetFocus;
              	Exit;
				end else begin
     				Query_Run(Query1,'INSERT INTO SZOTAR (SZ_FOKOD, SZ_ALKOD, SZ_MENEV, SZ_KODHO, SZ_NEVHO ) '+
				     	'VALUES(''000'', '''+M1.Text+''', '''+M2.Text+''', '+M3.Text+', '+M4.Text+') ');
            end;
         end else begin
         	// Hiba a lek�rdez�sn�l
            Exit;
         end;
      end else begin
      	// M�r volt ilyen rekord -> UPDATE
			Query_Run(Query1,'UPDATE SZOTAR SET  SZ_MENEV = '+
			  	' '''+M2.Text+''', SZ_KODHO = '+M3.Text+', SZ_NEVHO = '+M4.Text+
            ' WHERE SZ_FOKOD = ''000'' AND SZ_ALKOD = '''+M1.Text+''' ');
      end;
	end;
 	Close;
end;

procedure TBazFobeDlg.BitKilepClick(Sender: TObject);
begin
	ret_kod			:= '';
	Close;
end;

end.
