unit SzamlaDK_New;

interface

uses
  WinTypes, WinProcs, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, QuickRpt, QRExpr, Qrctrls, ExtCtrls, printers, DB, DBTables,
  ADODB, J_DataModule, jpeg, System.Contnrs, System.UITypes ;

type
  TSzamla_DKDlg = class(TForm)
    Rep: TQuickRep;
    QRBand3: TQRBand;
    QRLNEV: TQRLabel;
    QRLCIMB: TQRLabel;
	 QRLSzamla: TQRLabel;
	 QRBand1: TQRBand;
    QRBand2: TQRBand;
    QRLabel2: TQRLabel;
	 QRLabel3: TQRLabel;
	 QRLabel4: TQRLabel;
    QRLabel5: TQRLabel;
    QRShape1: TQRShape;
    QRShape2: TQRShape;
    QRLabel6: TQRLabel;
    QRLabel7: TQRLabel;
    QRLIRSZ: TQRLabel;
	 QRLabel8: TQRLabel;
	 QRLabel9: TQRLabel;
	 QRLabel10: TQRLabel;
    QRLabel11: TQRLabel;
    QRLabel12: TQRLabel;
	 QRLabel13: TQRLabel;
    QRLabel14: TQRLabel;
    QRShape3: TQRShape;
	 QRLabel15: TQRLabel;
    QRShape4: TQRShape;
    QRLabel16: TQRLabel;
	 QRShape5: TQRShape;
    QRLabel17: TQRLabel;
    QRShape6: TQRShape;
    QRLabel18: TQRLabel;
    QRShape7: TQRShape;
    QRLabel19: TQRLabel;
    QRShape8: TQRShape;
    QRShape9: TQRShape;
    QRShape10: TQRShape;
    QRShape11: TQRShape;
    QRShape12: TQRShape;
    QRLabel20: TQRLabel;
    QRLabel21: TQRLabel;
    QRLabel22: TQRLabel;
    QRLabel23: TQRLabel;
    QRLabel24: TQRLabel;
	 QRShape13: TQRShape;
    QRLabel25: TQRLabel;
	 QRLabel26: TQRLabel;
	 QRShape14: TQRShape;
	 QRLabel27: TQRLabel;
    QRLabel28: TQRLabel;
    QRShape15: TQRShape;
    QRShape16: TQRShape;
    QRLabel29: TQRLabel;
	 QRShape17: TQRShape;
	 QRShape18: TQRShape;
    QRShape19: TQRShape;
    QRShape20: TQRShape;
    QRShape21: TQRShape;
    QRShape22: TQRShape;
    QRShape23: TQRShape;
    QRShape24: TQRShape;
    QRShape25: TQRShape;
    QRLabel1: TQRLabel;
    QRLabel34: TQRLabel;
    QRLabel35: TQRLabel;
    QRLabel36: TQRLabel;
	 QRLabel37: TQRLabel;
    QRLabel38: TQRLabel;
    QRLabel39: TQRLabel;
    QRLabel40: TQRLabel;
    QRLabel41: TQRLabel;
	 QRLabel42: TQRLabel;
    QRLabel43: TQRLabel;
    QRLabel44: TQRLabel;
	 QRLabel45: TQRLabel;
    QRLabel46: TQRLabel;
    QRLabel48: TQRLabel;
	 LabelO1: TQRLabel;
    QRLabel47: TQRLabel;
    QRLabel49: TQRLabel;
    QRLabel50: TQRLabel;
	 QRLabel51: TQRLabel;
    QRLabel52: TQRLabel;
    QRLabel53: TQRLabel;
    QRLabel54: TQRLabel;
	 QRLabel55: TQRLabel;
	 QRShape26: TQRShape;
    QRLabel56: TQRLabel;
    QRLabel57: TQRLabel;
    QRLabel58: TQRLabel;
    QRShape27: TQRShape;
    QRLabel59: TQRLabel;
    QRLabel60: TQRLabel;
    QRLabel61: TQRLabel;
    QRLabel64: TQRLabel;
	 QRLabel67: TQRLabel;
    QRLabel68: TQRLabel;
    QRLabel70: TQRLabel;
    QRLabel30: TQRLabel;
	 QRLabel31: TQRLabel;
    QRLabel69: TQRLabel;
	 QRLabel71: TQRLabel;
    QRLabel74: TQRLabel;
	 QRLabel75: TQRLabel;
    QRLabel77: TQRLabel;
	 QRLabel32: TQRLabel;
    QRShape33: TQRShape;
    QRShape34: TQRShape;
    QRLabel81: TQRLabel;
    QRLabel73: TQRLabel;
    QRLabel80: TQRLabel;
    QRBand4: TQRBand;
    QRLabel82: TQRLabel;
	 QRLabel83: TQRLabel;
    QRLabel84: TQRLabel;
	 QRLabel100: TQRLabel;
	 QRLabel101: TQRLabel;
    QRLabel65: TQRLabel;
    Query1: TADOQuery;
    QueryAl: TADOQuery;
    QueryFej: TADOQuery;
	 QuerySor: TADOQuery;
	 QRLabel8A: TQRLabel;
	 SZ_BOLD1: TQRLabel;
	 SZ_CIM1: TQRLabel;
	 SZ_NORM1: TQRLabel;
    QRLabel103: TQRLabel;
    QRLabel104: TQRLabel;
    QRLabel105: TQRLabel;
    QRLabel106: TQRLabel;
    QRLabel107: TQRLabel;
    QRLabel63_: TQRLabel;
    QRLabel141: TQRLabel;
    QRShape28: TQRShape;
    QRLabel109: TQRLabel;
    QRLabel63: TQRLabel;
    QRLabel33: TQRLabel;
    QRLabel62: TQRLabel;
    QRLabel66: TQRLabel;
    QRLabel72: TQRLabel;
    QRLabel76: TQRLabel;
	 procedure FormCreate(Sender: TObject);
	 procedure QRBand3BeforePrint(Sender: TQRCustomBand;
	   var PrintBand: Boolean);
	 function	Tolt(szkod	: string) : boolean;
	 procedure QRBand1BeforePrint(Sender: TQRCustomBand;
	   var PrintBand: Boolean);
	 procedure QRBand2BeforePrint(Sender: TQRCustomBand;
	   var PrintBand: Boolean);
	 procedure RepBeforePrint(Sender: TCustomQuickRep;
	   var PrintReport: Boolean);
	 procedure FormDestroy(Sender: TObject);
  private
		oldalszam		: integer;
		JSzamla     	: TJSzamla;
       nyelv           : string;
       tizedstr        : string;
  public
    FTotalPages: Integer;
  end;

var
  Szamla_DKDlg: TSzamla_DKDlg;

implementation

uses
	Egyeb, J_SQL, Kozos;

{$R *.DFM}

procedure TSzamla_DKDlg.FormCreate(Sender: TObject);
var
	i : integer;
begin
	EgyebDlg.SetADOQueryDatabase(Query1);
	EgyebDlg.SetADOQueryDatabase(QueryAl);
	EgyebDlg.SetADOQueryDatabase(QueryFej);
	EgyebDlg.SetADOQueryDatabase(QuerySor);
	oldalszam	:= 1;
   nyelv       := 'HUN';
end;

procedure TSzamla_DKDlg.QRBand3BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
var
  str     : string;
  str2    : string;
  kodstr	: string;
  orsz		: string;
  skod    : string;
  csopaz  : string;
begin
   // Manu�lis sz�mla felirat�nak kezel�se
   // if Pos('TEMAN', QueryFej.FieldByName('SA_FEJL1').AsString ) = 0 then begin
   if QueryFej.FieldByName('SA_MAN').AsInteger = 0 then begin
       skod:='';
       if Query_Select('VEVO','V_KOD',QueryFej.FieldByName('SA_VEVOKOD').AsString,'VE_SKOD')<>'' then begin
           skod:='  ('+Query_Select('VEVO','V_KOD',QueryFej.FieldByName('SA_VEVOKOD').AsString,'VE_SKOD')+')';
       end;
   end;

	if QueryFej.FieldByName('SA_PELDANY').AsInteger = 0 then begin
		QrLabel76.Caption := GetSzoveg('51', 'Eredeti', nyelv);
	end else begin
		QrLabel76.Caption := GetSzoveg('52', 'M�solat', nyelv);
	end;
 	// QrLabel81.Caption := GetSzoveg('27', 'Oldal:', nyelv)+Format('%3d',[oldalszam]) + ' ';
  QrLabel81.Caption := GetSzoveg('27', 'Oldal:', nyelv)+Format('%3d',[oldalszam]) + ' / '+ IntToStr(FTotalPages)+' ';
	Inc(oldalszam);
	{A fejl�c adatainak ki�r�sa}
	QRLSzamla.Caption	:= GetSzoveg('01', 'Sz�mla', nyelv);
	if StrToIntDef(QueryFej.FieldByName('SA_RESZE').AsString, 0) > 0 then begin
		QRLSzamla.Caption	:= 'FIGYELEM!!! R�SZSZ�MLA!!!';
	end;
	QrLabel3.Caption 	:= GetSzoveg('02', 'Elad�', nyelv);
	QrLabel6.Caption 	:= GetSzoveg('02', 'Elad�', nyelv);
	QrLabel7.Caption 	:= GetSzoveg('03', 'Vev�', nyelv);
	QrLabel15.Caption 	:= GetSzoveg('04', 'Fizet�si m�d', nyelv);
	QrLabel16.Caption 	:= GetSzoveg('05', 'Teljes�t�s ideje', nyelv);
	QrLabel17.Caption 	:= GetSzoveg('06', 'Sz�mla kelte', nyelv);
	QrLabel18.Caption 	:= GetSzoveg('07', 'Lej�rat', nyelv);
	QrLabel19.Caption 	:= GetSzoveg('08', 'Sz�mlasz�m', nyelv);

	QrLabel27.Caption 	:= GetSzoveg('09', 'VTSZ/SZJ, Megnevez�s', nyelv);
	QrLabel28.Caption 	:= GetSzoveg('10', 'Menny.', nyelv);
	QrLabel67.Caption 	:= GetSzoveg('11', 'Me.e.', nyelv);
	QrLabel70.Caption 	:= GetSzoveg('12', 'Nett� egys.', nyelv);
	QrLabel29.Caption 	:= GetSzoveg('13', 'Nett� �rt�k', nyelv);
	QrLabel30.Caption 	:= GetSzoveg('14', '�FA %', nyelv);
	QrLabel31.Caption 	:= GetSzoveg('15', '�FA �rt�k', nyelv);
	QrLabel74.Caption 	:= GetSzoveg('16', 'Brutt�', nyelv);
	QrLabel75.Caption 	:= GetSzoveg('17', '�rt�k', nyelv);
	QrLabel72.Caption 	:= GetSzoveg('50', '�FA (Ft)', nyelv);
   if JSzamla.szamlaeuro < 1 then begin
       QrLabel72.Caption 	:= '';              // Forintos sz�ml�ra nem kell m�gegyszer a sorafa
   end;
	QrLabel77.Caption 	:= GetSzoveg('19', '�rfolyam', nyelv);

   QrLabel26.Caption 	:= GetSzoveg('33', 'Viszony :', nyelv);
   QrLabel55.Caption 	:= GetSzoveg('34', 'J�ratsz�m :', nyelv);
   QrLabel58.Caption 	:= GetSzoveg('35', 'Rendsz�m :', nyelv);
	QrLabel109.Caption 	:= GetSzoveg('20', 'Poz�ci� sz�m:', nyelv);
	QrLabel106.Caption 	:= GetSzoveg('48', 'A sz�mla k�zhezv�tel�t�l ... 1. sor', nyelv);
	QrLabel107.Caption 	:= GetSzoveg('49', 'A sz�mla k�zhezv�tel�t�l ... 2. sor', nyelv);

   QrLNEV.Caption 		:= EgyebDlg.Read_SZGrid('CEG', '100')+skod;
   QrLIRSZ.Caption 	:= EgyebDlg.Read_SZGrid('CEG', '102');
   QRLabel105.Caption  := QRLNEV.Caption+' - '+QRLIRSZ.Caption;
   QrLabel8.Caption 	:= EgyebDlg.Read_SZGrid('CEG', '104');
   QrLabel9.Caption 	:= EgyebDlg.Read_SZGrid('CEG', '106');
   QrLabel10.Caption 	:= EgyebDlg.Read_SZGrid('CEG', '107');
   QrLabel8A.Caption 	:= EgyebDlg.Read_SZGrid('CEG', '108');
   QrLabel80.Caption 	:= EgyebDlg.Read_SZGrid('CEG', '109');
   if QueryFej.FieldByName('SA_FEJL1').AsString <> '' then begin
	    {L�tezik fejl�c adat, azt kell betenni a sz�ml�ba}
       QrLNEV.Caption 	:= QueryFej.FieldByName('SA_FEJL1').AsString+skod;
       QrLIRSZ.Caption 	:= QueryFej.FieldByName('SA_FEJL2').AsString;
       QrLabel8.Caption 	:= QueryFej.FieldByName('SA_FEJL3').AsString;
       QRLabel105.Caption:=QRLNEV.Caption+' - '+QRLIRSZ.Caption;
       QrLabel9.Caption 	:= QueryFej.FieldByName('SA_FEJL4').AsString;
       QrLabel10.Caption := QueryFej.FieldByName('SA_FEJL5').AsString;
       QrLabel8A.Caption := QueryFej.FieldByName('SA_FEJL6').AsString;
	    QrLabel80.Caption := QueryFej.FieldByName('SA_FEJL7').AsString;
   end;
	QrLabel11.Caption := QueryFej.FieldByName('SA_VEVONEV').AsString;
  // Lehetnek azonos nev� �gyfelek, egyik AT, m�sik DE
	// orsz			  := Query_Select('VEVO', 'V_NEV', QueryFej.FieldByName('SA_VEVONEV').AsString, 'V_ORSZ');
  orsz:= Query_Select('VEVO', 'V_KOD', QueryFej.FieldByName('SA_VEVOKOD').AsString, 'V_ORSZ');

	QrLabel12.Caption := orsz + '-' + QueryFej.FieldByName('SA_VEIRSZ').AsString;
	QrLabel13.Caption := QueryFej.FieldByName('SA_VEVAROS').AsString;
	QrLabel14.Caption := QueryFej.FieldByName('SA_VECIM').AsString;
	if Pos('.', QrLabel14.Caption) < 1 then begin
		QrLabel14.Caption := QrLabel14.Caption + '.';
	end;
	QrLabel101.Caption 		:= JSzamla.szamlaadoszam;
	csopaz:= Query_Select('VEVO', 'V_KOD', QueryFej.FieldByName('SA_VEVOKOD').AsString, 'VE_CSOPAZ');
   if csopaz<>'' then begin
       QRLabel141.Caption := GetSzoveg('30', 'Csoport az.: ', nyelv)+csopaz
   end else begin
       QRLabel141.Caption := '';
   end;

	{A k�lf�ldi fizet�si m�d megkeres�se}
	str	:= QueryFej.FieldByName('SA_FIMOD').AsString;
   if nyelv <> 'HUN' then begin
	    if Query_Run(Query1, 'SELECT * FROM SZOTAR WHERE SZ_FOKOD = ''100'' AND SZ_MENEV = '''+str+''' ',true) then begin
		    if Query1.RecordCount > 0 then begin
               // A k�lf�ldi fizet�si m�d k�dj�hoz lev�gjuk a k�d els� karakter�t: 111 -> 11; 112 -> 12; 113 -> 13
			    kodstr 	:= nyelv+copy(Query1.FieldByName('SZ_ALKOD').AsString, 2, 2);
			    str	    := EgyebDlg.Read_SZGrid( '200',kodstr);
               if str = '' then begin
                   // Be�rjuk a sz�t�rba a hi�nyz� elemet ha m�g nincs
	                if Query_Run(Query1, 'SELECT * FROM SZOTAR WHERE SZ_FOKOD = ''200'' AND SZ_ALKOD = '''+kodstr+''' ',true) then begin
		                if Query1.RecordCount = 0 then begin
                           Query_Run(Query1,  'INSERT INTO SZOTAR ( SZ_FOKOD, SZ_ALKOD, SZ_MENEV, SZ_ERVNY ) VALUES ( ''200'', '''+kodstr+''', '''+'Hi�nyz� k�lf�ldi fizet�si m�d'+''', 1 ) ', FALSE);
                       end;
                   end;
               end;
           end;
		end;
	end;
   QrLabel20.Caption := str;
   QrLabel21.Caption := QueryFej.FieldByName('SA_TEDAT').AsString;
   QrLabel22.Caption := QueryFej.FieldByName('SA_KIDAT').AsString;
   QrLabel23.Caption := QueryFej.FieldByName('SA_ESDAT').AsString;
   QrLabel24.Caption := QueryFej.FieldByName('SA_KOD').AsString;
   QrLabel25.Caption := QueryFej.FieldByName('SA_MEGJ').AsString;
   QrLabel56.Caption := QueryFej.FieldByName('SA_JARAT').AsString;
   { A j�ratsz�m alapj�n megkeress�k a bet�jelet is }
   if Query_Run(QueryAl, 'SELECT * FROM JARAT WHERE JA_ALKOD = '+
  	    IntToStr(Round(StringSzam(QueryFej.FieldByName('SA_JARAT').AsString)))+' and substring(JA_JKEZD,1,4)='''+copy(QueryFej.FieldByName('SA_TEDAT').AsString,1,4)+''' ' ,true) then begin
	    QrLabel56.Caption := QueryAl.FieldByName('JA_ORSZ').AsString + ' - ' +
		QueryFej.FieldByName('SA_JARAT').AsString;
   end;
   QrLabel57.Caption := QueryFej.FieldByName('SA_RSZ').AsString;
   QrLabel63.Caption := QueryFej.FieldByName('SA_POZICIO').AsString;
end;

function	TSzamla_DKDlg.Tolt(szkod	: string) : boolean;
begin
	Result	:= false;
	Query_Run(QueryFej, 'SELECT * FROM SZFEJ WHERE SA_UJKOD = '+szkod);
	if QueryFej.RecordCount < 1 then begin
		NoticeKi('Hi�nyz� sz�mlarekord : ('+szkod+')');
		Exit;
	end;
	JSzamla	:= TJSzamla.Create(Self);
	JSzamla.FillSzamla(QueryFej.FieldByName('SA_UJKOD').AsString);
	Result		:= true;
	Query_Run(QuerySor, 'SELECT * FROM SZSOR WHERE SS_UJKOD = '+szkod+' ORDER BY SS_SOR');
   nyelv       := JSzamla.szamla_nyelv;
end;

procedure TSzamla_DKDlg.QRBand1BeforePrint(Sender: TQRCustomBand;
	   var PrintBand: Boolean);
var
	sor_szama	: integer;
	JSzamlaSor	: TJSzamlaSor;
begin
	sor_szama	:= StrToIntDef(QuerySor.FieldByName('SS_SOR').AsString,-1) -1;
	if sor_szama >= 0 then begin
		JSzamlasor	:= (JSzamla.SzamlaSorok[sor_szama] as TJszamlaSor);
	end else begin
		NoticeKi('�rv�nytelen sorsz�m : '+QuerySor.FieldByName('SS_SOR').AsString);
		Exit;
	end;
	if QuerySor.FieldByName('SS_ITJSZJ').AsString = '' then begin
		QrLabel1.Caption := QuerySor.FieldByName('SS_TERNEV').AsString;
	end else begin
		QrLabel1.Caption := QuerySor.FieldByName('SS_ITJSZJ').AsString+', '+QuerySor.FieldByName('SS_TERNEV').AsString;
	end;
	{A k�lf�ldi le�r�s megjelen�t�se}
	QrLabel100.Caption := QuerySor.FieldByName('SS_LEIRAS').AsString;
	if QuerySor.FieldByName('SS_EGYAR').AsFloat = 0 then begin
		QrLabel32.Caption := '';
		QrLabel37.Caption := '';
		QrLabel68.Caption := '';
		QrLabel61.Caption := '';
		QrLabel34.Caption := '';
		QrLabel36.Caption := '';
		QrLabel35.Caption := '';
		QrLabel73.Caption := '';
		QrLabel66.Caption := '';
	end else begin
		QrLabel32.Caption := GetAfaKod(nyelv, EgyebDlg.Read_SZGrid('101',QuerySor.FieldByName('SS_AFAKOD').AsString));
		QrLabel37.Caption := Format('%.2f',[StringSzam(QuerySor.FieldByName('SS_DARAB').AsString)]) ;
		QrLabel68.Caption := GetMegys(nyelv, JSzamlasor.megyseg);   // QuerySor.FieldByName('SS_MEGY').AsString;
		QrLabel73.Caption := Format('%.2f',[JSzamlasor.valutaarfoly]) ;
       if JSzamla.szamlaeuro < 1 then begin
           QrLabel61.Caption 	:= Format('%.0f',[QuerySor.FieldByName('SS_EGYAR').AsFloat]);
           QrLabel34.Caption 	:= Format('%.0f',[jszamlasor.sornetto]);
           QrLabel36.Caption 	:= Format('%.0f',[JSzamlaSor.sorafa]);
           QrLabel35.Caption 	:= Format('%.0f',[JSzamlaSor.sornetto+JSzamlaSor.sorafa]);
           QrLabel66.Caption 	:= '';              // Forintos sz�ml�ra nem kell m�gegyszer a sorafa
       end else begin
           QrLabel61.Caption 	:= Format('%.2f',[JSzamlasor.valutaegyar]);
           QrLabel34.Caption 	:= Format('%.2f',[jszamlasor.sorneteur]);
           QrLabel36.Caption 	:= Format('%.2f',[JSzamlaSor.sorafaeur]);
           QrLabel35.Caption 	:= Format('%.2f',[JSzamlaSor.sorbruteur]);
		    QrLabel66.Caption 	:= Format('%.0f Ft',[JSzamlaSor.sorafa]);
       end;
	end;
end;

procedure TSzamla_DKDlg.QRBand2BeforePrint(Sender: TQRCustomBand;
	   var PrintBand: Boolean);
begin
	QrLabel38.Caption 	:= GetSzoveg('21', 'Nett� �sszesen:', nyelv);
	QrLabel39.Caption 	:= GetSzoveg('22', '0% ad�alap:', nyelv);
	QrLabel44.Caption 	:= GetSzoveg('23', 'Nem ad�k�teles:', nyelv);
	QrLabel83.Caption 	:= GetSzoveg('37', '�.K. ad�alap:', nyelv);
	QrLabel43.Caption 	:= GetSzoveg('38', '15 % ad�alap:', nyelv);
	QrLabel42.Caption 	:= GetSzoveg('39', '27 % ad�alap:', nyelv);
	QrLabel40.Caption 	:= GetSzoveg('40', '15 % �FA:', nyelv);
	QrLabel41.Caption 	:= GetSzoveg('41', '27 % �FA:', nyelv);
	QrLabel45.Caption 	:= GetSzoveg('25', 'Mind�sszesen:', nyelv);
	QrLabel82.Caption 	:= GetSzoveg('28', 'K�sz�tette: ...', nyelv);
   if JSzamla.szamlaeuro < 1 then begin
       LabelO1.Caption   	:= Format('%.0f Ft',[JSzamla.ossz_netto]);
       QrLabel47.Caption 	:= Format('%.0f Ft',[( JSzamla.Afasorok[JSzamla.GetAfaSorszam('102')] as TJAfasor ).afaertek]);
       QrLabel50.Caption 	:= Format('%.0f Ft',[ JSzamla.ossz_brutto]);
       QrLabel51.Caption 	:= Format('%.0f Ft',[( JSzamla.Afasorok[JSzamla.GetAfaSorszam('102')] as TJAfasor ).afaalap]);
       QrLabel53.Caption 	:= Format('%.0f Ft',[( JSzamla.Afasorok[JSzamla.GetAfaSorszam('103')] as TJAfasor ).afaalap]);
       QrLabel54.Caption 	:= Format('%.0f Ft',[( JSzamla.Afasorok[JSzamla.GetAfaSorszam('104')] as TJAfasor ).afaalap]);
       QrLabel84.Caption 	:= Format('%.0f Ft',[( JSzamla.Afasorok[JSzamla.GetAfaSorszam('105')] as TJAfasor ).afaalap]);
       if nyelv = 'HUN' then begin
           QrLabel48.Caption 	:= SzamText(Format('%.0f',[JSzamla.ossz_brutto]))+' forint / 00';
       end else begin
           QrLabel46.Caption 	:= '';
           QrLabel48.Caption 	:= '';
       end;
       QrLabel49.Caption   := Format('%.0f Ft',[( JSzamla.Afasorok[JSzamla.GetAfaSorszam(JSzamla.afakod27)] as TJAfasor ).afaertek]);
       QrLabel52.Caption   := Format('%.0f Ft',[( JSzamla.Afasorok[JSzamla.GetAfaSorszam(JSzamla.afakod27)] as TJAfasor ).afaalap]);
       QrLabel33.Caption   := GetSzoveg('42', 'Mind�sszesen EUR-ban:', nyelv);
       QrLabel62.Caption   := Format('%.2f EUR',[ JSzamla.ossz_bruttoeur]);
   end else begin
       LabelO1.Caption   	:= Format('%.2f EUR',[JSzamla.ossz_nettoeur]);
       QrLabel47.Caption 	:= Format('%.2f EUR',[( JSzamla.Afasorok[JSzamla.GetAfaSorszam('102')] as TJAfasor ).afaerteur]);
       QrLabel50.Caption 	:= Format('%.2f EUR',[ JSzamla.ossz_bruttoeur]);
       QrLabel51.Caption 	:= Format('%.2f EUR',[( JSzamla.Afasorok[JSzamla.GetAfaSorszam('102')] as TJAfasor ).afaaleur]);
       QrLabel53.Caption 	:= Format('%.2f EUR',[( JSzamla.Afasorok[JSzamla.GetAfaSorszam('103')] as TJAfasor ).afaaleur]);
       QrLabel54.Caption 	:= Format('%.2f EUR',[( JSzamla.Afasorok[JSzamla.GetAfaSorszam('104')] as TJAfasor ).afaaleur]);
       QrLabel84.Caption 	:= Format('%.2f EUR',[( JSzamla.Afasorok[JSzamla.GetAfaSorszam('105')] as TJAfasor ).afaaleur]);
       QrLabel46.Caption 	:= '';
       // A sz�mla �rt�ke forintban ki�r�sa
       if JSzamla.ossz_afaeur = 0 then begin
           QrLabel48.Caption 	:= GetSzoveg('31', 'Az �FA �rt�ke forintban : 0 Ft  ', nyelv);
       end else begin
           QrLabel48.Caption 	:= Format(GetSzoveg('32', 'Az �FA �rt�ke forintban : %.0f Ft', nyelv), [Jszamla.ossz_afa]);
       end;
       QrLabel49.Caption   := Format('%.2f EUR',[( JSzamla.Afasorok[JSzamla.GetAfaSorszam(JSzamla.afakod27)] as TJAfasor ).afaerteur]);
       QrLabel52.Caption   := Format('%.2f EUR',[( JSzamla.Afasorok[JSzamla.GetAfaSorszam(JSzamla.afakod27)] as TJAfasor ).afaaleur]);
       QrLabel33.Caption   := GetSzoveg('36', 'Mind�sszesen FT-ban:', nyelv);
       QrLabel62.Caption   := Format('%.0f Ft',[ JSzamla.ossz_brutto]);
   end;
	QrLabel4.Caption  	:= QueryFej.FieldByName('SA_MELL1').AsString;
	QrLabel5.Caption  	:= QueryFej.FieldByName('SA_MELL2').AsString;
	QrLabel59.Caption 	:= QueryFej.FieldByName('SA_MELL3').AsString;
	QrLabel60.Caption 	:= QueryFej.FieldByName('SA_MELL4').AsString;
	QrLabel65.Caption 	:= QueryFej.FieldByName('SA_MELL5').AsString;
	QrLabel103.Caption 	:= QueryFej.FieldByName('SA_MELL6').AsString;
	QrLabel104.Caption 	:= QueryFej.FieldByName('SA_MELL7').AsString;
end;

procedure TSzamla_DKDlg.RepBeforePrint(Sender: TCustomQuickRep;
      var PrintReport: Boolean);
begin
	oldalszam		:= 1;
end;

procedure TSzamla_DKDlg.FormDestroy(Sender: TObject);
begin
	JSzamla.Destroy
end;

end.
