object Szamla_DK_HUFForm: TSzamla_DK_HUFForm
  Left = 307
  Top = 166
  Width = 1001
  Height = 812
  HorzScrollBar.Range = 1200
  VertScrollBar.Range = 2000
  Caption = 'Szamla_DK_HUFForm'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clBlack
  Font.Height = -13
  Font.Name = 'Courier New'
  Font.Style = []
  OldCreateOrder = True
  Scaled = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 16
  object QRLabel69: TQRLabel
    Left = 302
    Top = 653
    Width = 8
    Height = 8
    Size.Values = (
      21.166666666666670000
      797.983333333333400000
      1128.183333333333000000
      21.166666666666670000)
    XLColumn = 0
    Alignment = taCenter
    AlignToBand = False
    AutoSize = False
    Caption = 'Nett'#243
    Color = clSilver
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Times New Roman'
    Font.Style = [fsBold]
    ParentFont = False
    Transparent = True
    ExportAs = exptText
    WrapStyle = BreakOnSpaces
    FontSize = 8
  end
  object QRLabel71: TQRLabel
    Left = 302
    Top = 665
    Width = 8
    Height = 8
    Size.Values = (
      21.166666666666670000
      797.983333333333400000
      1159.933333333333000000
      21.166666666666670000)
    XLColumn = 0
    Alignment = taCenter
    AlignToBand = False
    AutoSize = False
    Caption = 'egys'#233'g'#225'r'
    Color = clSilver
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Times New Roman'
    Font.Style = [fsBold]
    ParentFont = False
    Transparent = True
    ExportAs = exptText
    WrapStyle = BreakOnSpaces
    FontSize = 8
  end
  object Rep: TQuickRep
    Left = 0
    Top = 0
    Width = 794
    Height = 1123
    BeforePrint = RepBeforePrint
    DataSet = QuerySor
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -17
    Font.Name = 'Arial'
    Font.Style = []
    Functions.Strings = (
      'PAGENUMBER'
      'COLUMNNUMBER'
      'REPORTTITLE')
    Functions.DATA = (
      '0'
      '0'
      #39#39)
    Options = [FirstPageHeader, LastPageFooter]
    Page.Columns = 1
    Page.Orientation = poPortrait
    Page.PaperSize = A4
    Page.Continuous = False
    Page.Values = (
      100.000000000000000000
      2970.000000000000000000
      100.000000000000000000
      2100.000000000000000000
      100.000000000000000000
      100.000000000000000000
      0.000000000000000000)
    PrinterSettings.Copies = 1
    PrinterSettings.OutputBin = Auto
    PrinterSettings.Duplex = False
    PrinterSettings.FirstPage = 0
    PrinterSettings.LastPage = 0
    PrinterSettings.UseStandardprinter = False
    PrinterSettings.UseCustomBinCode = False
    PrinterSettings.CustomBinCode = 0
    PrinterSettings.ExtendedDuplex = 0
    PrinterSettings.UseCustomPaperCode = False
    PrinterSettings.CustomPaperCode = 0
    PrinterSettings.PrintMetaFile = False
    PrinterSettings.PrintQuality = 0
    PrinterSettings.Collate = 0
    PrinterSettings.ColorOption = 0
    PrintIfEmpty = True
    ReportTitle = 'Sz'#225'mla'
    ShowProgress = False
    SnapToGrid = True
    Units = MM
    Zoom = 100
    PrevFormStyle = fsNormal
    PreviewInitialState = wsNormal
    PreviewWidth = 500
    PreviewHeight = 500
    PrevInitialZoom = qrZoomToFit
    PreviewDefaultSaveType = stQRP
    PreviewLeft = 0
    PreviewTop = 0
    object QRBand3: TQRBand
      Left = 38
      Top = 38
      Width = 718
      Height = 379
      AlignToBottom = False
      BeforePrint = QRBand3BeforePrint
      TransparentBand = False
      ForceNewColumn = False
      ForceNewPage = False
      Size.Values = (
        1002.770833333333000000
        1899.708333333333000000)
      PreCaluculateBandHeight = False
      KeepOnOnePage = False
      BandType = rbPageHeader
      object QRShape19: TQRShape
        Left = 620
        Top = 305
        Width = 90
        Height = 35
        Size.Values = (
          92.604166666666680000
          1640.416666666667000000
          806.979166666666800000
          238.125000000000000000)
        XLColumn = 0
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape15: TQRShape
        Left = 290
        Top = 305
        Width = 60
        Height = 35
        Size.Values = (
          92.604166666666680000
          767.291666666666800000
          806.979166666666800000
          158.750000000000000000)
        XLColumn = 0
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRLSzamla: TQRLabel
        Left = 8
        Top = 0
        Width = 705
        Height = 56
        Size.Values = (
          148.166666666666700000
          21.166666666666670000
          0.000000000000000000
          1865.312500000000000000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Sz'#225'mla / Rechnung'
        Color = clWhite
        Font.Charset = EASTEUROPE_CHARSET
        Font.Color = clBlack
        Font.Height = -39
        Font.Name = 'Arial Black'
        Font.Style = [fsBold, fsItalic]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 29
      end
      object QRShape1: TQRShape
        Left = 10
        Top = 78
        Width = 350
        Height = 112
        Size.Values = (
          296.333333333333400000
          26.458333333333330000
          206.375000000000000000
          926.041666666666800000)
        XLColumn = 0
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape2: TQRShape
        Left = 360
        Top = 78
        Width = 350
        Height = 112
        Size.Values = (
          296.333333333333400000
          952.500000000000000000
          206.375000000000000000
          926.041666666666800000)
        XLColumn = 0
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRLabel6: TQRLabel
        Left = 10
        Top = 60
        Width = 153
        Height = 17
        Size.Values = (
          44.979166666666670000
          26.458333333333330000
          158.750000000000000000
          404.812500000000000000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Elad'#243'/Verk'#228'ufer'
        Color = clWhite
        Font.Charset = EASTEUROPE_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 10
      end
      object QRLabel7: TQRLabel
        Left = 360
        Top = 60
        Width = 119
        Height = 17
        Size.Values = (
          44.979166666666670000
          952.500000000000000000
          158.750000000000000000
          314.854166666666700000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Vev'#337'/K'#228'ufer'
        Color = clWhite
        Font.Charset = EASTEUROPE_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 10
      end
      object QRLNEV: TQRLabel
        Left = 16
        Top = 80
        Width = 337
        Height = 20
        Size.Values = (
          52.916666666666660000
          42.333333333333340000
          211.666666666666700000
          891.645833333333200000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'QRLNEV'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -16
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold, fsItalic, fsUnderline]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 12
      end
      object QRLCIMB: TQRLabel
        Left = 35
        Top = 38
        Width = 57
        Height = 14
        Enabled = False
        Size.Values = (
          37.041666666666670000
          92.604166666666670000
          100.541666666666700000
          150.812500000000000000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'QRLCIMB'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 8
      end
      object QRLIRSZ: TQRLabel
        Left = 16
        Top = 102
        Width = 340
        Height = 14
        Size.Values = (
          37.041666666666670000
          42.333333333333340000
          269.875000000000000000
          899.583333333333400000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'IRSZ'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 8
      end
      object QRLabel8: TQRLabel
        Left = 16
        Top = 116
        Width = 340
        Height = 14
        Size.Values = (
          37.041666666666670000
          42.333333333333340000
          306.916666666666700000
          899.583333333333400000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'QRLCIM'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 8
      end
      object QRLabel9: TQRLabel
        Left = 16
        Top = 130
        Width = 340
        Height = 14
        Size.Values = (
          37.041666666666670000
          42.333333333333340000
          343.958333333333400000
          899.583333333333400000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'QRLCIM'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 8
      end
      object QRLabel10: TQRLabel
        Left = 16
        Top = 144
        Width = 340
        Height = 14
        Size.Values = (
          37.041666666666670000
          42.333333333333340000
          381.000000000000000000
          899.583333333333400000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'QRLCIM'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 8
      end
      object QRLabel11: TQRLabel
        Left = 370
        Top = 82
        Width = 327
        Height = 42
        Size.Values = (
          111.125000000000000000
          978.958333333333200000
          216.958333333333300000
          865.187500000000000000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Vev'#337' neve'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -16
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold, fsItalic, fsUnderline]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 12
      end
      object QRLabel12: TQRLabel
        Left = 370
        Top = 124
        Width = 79
        Height = 16
        Size.Values = (
          42.333333333333340000
          978.958333333333200000
          328.083333333333400000
          209.020833333333300000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'IRSZ'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 9
      end
      object QRLabel13: TQRLabel
        Left = 448
        Top = 124
        Width = 252
        Height = 16
        Size.Values = (
          42.333333333333340000
          1185.333333333333000000
          328.083333333333400000
          666.750000000000000000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'QRLCIM'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 9
      end
      object QRLabel14: TQRLabel
        Left = 370
        Top = 140
        Width = 330
        Height = 16
        Size.Values = (
          42.333333333333340000
          978.958333333333200000
          370.416666666666700000
          873.125000000000000000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'QRLCIM'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 9
      end
      object QRShape3: TQRShape
        Left = 10
        Top = 190
        Width = 140
        Height = 35
        Size.Values = (
          92.604166666666680000
          26.458333333333330000
          502.708333333333400000
          370.416666666666700000)
        XLColumn = 0
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRLabel15: TQRLabel
        Left = 12
        Top = 192
        Width = 136
        Height = 16
        Size.Values = (
          42.333333333333340000
          31.750000000000000000
          508.000000000000000000
          359.833333333333400000)
        XLColumn = 0
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = 'Fizet'#233'si m'#243'd'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 10
      end
      object QRShape4: TQRShape
        Left = 150
        Top = 190
        Width = 140
        Height = 35
        Size.Values = (
          92.604166666666680000
          396.875000000000000000
          502.708333333333400000
          370.416666666666700000)
        XLColumn = 0
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRLabel16: TQRLabel
        Left = 152
        Top = 193
        Width = 136
        Height = 16
        Size.Values = (
          42.333333333333330000
          402.166666666666700000
          510.645833333333300000
          359.833333333333300000)
        XLColumn = 0
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = 'Teljes'#237't'#233's ideje'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 10
      end
      object QRShape5: TQRShape
        Left = 290
        Top = 190
        Width = 140
        Height = 35
        Size.Values = (
          92.604166666666680000
          767.291666666666800000
          502.708333333333400000
          370.416666666666700000)
        XLColumn = 0
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRLabel17: TQRLabel
        Left = 292
        Top = 192
        Width = 136
        Height = 16
        Size.Values = (
          42.333333333333340000
          772.583333333333400000
          508.000000000000000000
          359.833333333333400000)
        XLColumn = 0
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = 'Sz'#225'mla kelte'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 10
      end
      object QRShape6: TQRShape
        Left = 430
        Top = 190
        Width = 140
        Height = 35
        Size.Values = (
          92.604166666666680000
          1137.708333333333000000
          502.708333333333400000
          370.416666666666700000)
        XLColumn = 0
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRLabel18: TQRLabel
        Left = 432
        Top = 192
        Width = 136
        Height = 16
        Size.Values = (
          42.333333333333340000
          1143.000000000000000000
          508.000000000000000000
          359.833333333333400000)
        XLColumn = 0
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = 'Lej'#225'rat'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 10
      end
      object QRShape7: TQRShape
        Left = 570
        Top = 190
        Width = 140
        Height = 35
        Size.Values = (
          92.604166666666680000
          1508.125000000000000000
          502.708333333333400000
          370.416666666666700000)
        XLColumn = 0
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRLabel19: TQRLabel
        Left = 572
        Top = 192
        Width = 136
        Height = 16
        Size.Values = (
          42.333333333333340000
          1513.416666666667000000
          508.000000000000000000
          359.833333333333400000)
        XLColumn = 0
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = 'Sz'#225'mlasz'#225'm'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 10
      end
      object QRShape8: TQRShape
        Left = 10
        Top = 225
        Width = 140
        Height = 25
        Size.Values = (
          66.145833333333340000
          26.458333333333330000
          595.312500000000000000
          370.416666666666700000)
        XLColumn = 0
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape9: TQRShape
        Left = 150
        Top = 225
        Width = 140
        Height = 25
        Size.Values = (
          66.145833333333340000
          396.875000000000000000
          595.312500000000000000
          370.416666666666700000)
        XLColumn = 0
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape10: TQRShape
        Left = 290
        Top = 225
        Width = 140
        Height = 25
        Size.Values = (
          66.145833333333340000
          767.291666666666800000
          595.312500000000000000
          370.416666666666700000)
        XLColumn = 0
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape11: TQRShape
        Left = 430
        Top = 225
        Width = 140
        Height = 25
        Size.Values = (
          66.145833333333340000
          1137.708333333333000000
          595.312500000000000000
          370.416666666666700000)
        XLColumn = 0
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape12: TQRShape
        Left = 570
        Top = 225
        Width = 140
        Height = 25
        Size.Values = (
          66.145833333333340000
          1508.125000000000000000
          595.312500000000000000
          370.416666666666700000)
        XLColumn = 0
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRLabel20: TQRLabel
        Left = 12
        Top = 229
        Width = 136
        Height = 16
        Size.Values = (
          42.333333333333340000
          31.750000000000000000
          605.895833333333400000
          359.833333333333400000)
        XLColumn = 0
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = '.'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 10
      end
      object QRLabel21: TQRLabel
        Left = 152
        Top = 229
        Width = 136
        Height = 16
        Size.Values = (
          42.333333333333340000
          402.166666666666600000
          605.895833333333400000
          359.833333333333400000)
        XLColumn = 0
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = '.'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 10
      end
      object QRLabel22: TQRLabel
        Left = 292
        Top = 229
        Width = 136
        Height = 16
        Size.Values = (
          42.333333333333340000
          772.583333333333400000
          605.895833333333400000
          359.833333333333400000)
        XLColumn = 0
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = '.'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 10
      end
      object QRLabel23: TQRLabel
        Left = 432
        Top = 229
        Width = 136
        Height = 16
        Size.Values = (
          42.333333333333340000
          1143.000000000000000000
          605.895833333333400000
          359.833333333333400000)
        XLColumn = 0
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = '.'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 10
      end
      object QRLabel24: TQRLabel
        Left = 572
        Top = 229
        Width = 136
        Height = 16
        Size.Values = (
          42.333333333333340000
          1513.416666666667000000
          605.895833333333400000
          359.833333333333400000)
        XLColumn = 0
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = '.'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 10
      end
      object QRShape13: TQRShape
        Left = 10
        Top = 250
        Width = 700
        Height = 25
        Size.Values = (
          66.145833333333340000
          26.458333333333330000
          661.458333333333400000
          1852.083333333333000000)
        XLColumn = 0
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRLabel25: TQRLabel
        Left = 200
        Top = 254
        Width = 505
        Height = 16
        Size.Values = (
          42.333333333333330000
          529.166666666666700000
          672.041666666666800000
          1336.145833333333000000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = '.'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 10
      end
      object QRLabel26: TQRLabel
        Left = 16
        Top = 254
        Width = 169
        Height = 16
        Size.Values = (
          42.333333333333340000
          42.333333333333340000
          672.041666666666800000
          447.145833333333400000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Viszony / Art der Lieferung :'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 10
      end
      object QRShape14: TQRShape
        Left = 10
        Top = 305
        Width = 280
        Height = 35
        Size.Values = (
          92.604166666666680000
          26.458333333333330000
          806.979166666666800000
          740.833333333333400000)
        XLColumn = 0
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRLabel27: TQRLabel
        Left = 16
        Top = 314
        Width = 270
        Height = 16
        Size.Values = (
          42.333333333333340000
          42.333333333333340000
          830.791666666666800000
          714.375000000000000000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'VTSZ/SZJ,  Megnevez'#233's'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 10
      end
      object QRLabel28: TQRLabel
        Left = 293
        Top = 307
        Width = 54
        Height = 16
        Size.Values = (
          42.333333333333340000
          775.229166666666800000
          812.270833333333400000
          142.875000000000000000)
        XLColumn = 0
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = 'Menny.'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 10
      end
      object QRShape16: TQRShape
        Left = 350
        Top = 305
        Width = 90
        Height = 35
        Size.Values = (
          92.604166666666680000
          926.041666666666800000
          806.979166666666800000
          238.125000000000000000)
        XLColumn = 0
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRLabel29: TQRLabel
        Left = 353
        Top = 323
        Width = 84
        Height = 16
        Size.Values = (
          42.333333333333340000
          933.979166666666600000
          854.604166666666800000
          222.250000000000000000)
        XLColumn = 0
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = 'Nett'#243' '#233'rt'#233'k'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 10
      end
      object QRShape17: TQRShape
        Left = 440
        Top = 305
        Width = 90
        Height = 35
        Size.Values = (
          92.604166666666680000
          1164.166666666667000000
          806.979166666666800000
          238.125000000000000000)
        XLColumn = 0
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape18: TQRShape
        Left = 530
        Top = 305
        Width = 90
        Height = 35
        Size.Values = (
          92.604166666666680000
          1402.291666666667000000
          806.979166666666800000
          238.125000000000000000)
        XLColumn = 0
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape26: TQRShape
        Left = 10
        Top = 275
        Width = 350
        Height = 25
        Size.Values = (
          66.145833333333340000
          26.458333333333330000
          727.604166666666800000
          926.041666666666800000)
        XLColumn = 0
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRLabel55: TQRLabel
        Left = 16
        Top = 279
        Width = 169
        Height = 16
        Size.Values = (
          42.333333333333340000
          42.333333333333340000
          738.187500000000000000
          447.145833333333400000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'J'#225'ratsz./Nr.der Importlizenz :'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 10
      end
      object QRLabel56: TQRLabel
        Left = 200
        Top = 279
        Width = 153
        Height = 16
        Size.Values = (
          42.333333333333330000
          529.166666666666700000
          738.187500000000000000
          404.812500000000000000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = '.'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 10
      end
      object QRShape27: TQRShape
        Left = 360
        Top = 275
        Width = 350
        Height = 25
        Size.Values = (
          66.145833333333340000
          952.500000000000000000
          727.604166666666800000
          926.041666666666800000)
        XLColumn = 0
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRLabel58: TQRLabel
        Left = 366
        Top = 279
        Width = 163
        Height = 16
        Size.Values = (
          42.333333333333340000
          968.375000000000000000
          738.187500000000000000
          431.270833333333400000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Rendsz'#225'm/Kennzeichnen :'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 10
      end
      object QRLabel57: TQRLabel
        Left = 534
        Top = 279
        Width = 171
        Height = 16
        Size.Values = (
          42.333333333333340000
          1412.875000000000000000
          738.187500000000000000
          452.437500000000000000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = '.'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 10
      end
      object QRLabel66: TQRLabel
        Left = 568
        Top = 43
        Width = 142
        Height = 16
        Size.Values = (
          42.333333333333330000
          1502.833333333333000000
          113.770833333333300000
          375.708333333333300000)
        XLColumn = 0
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'P'#233'ld'#225'ny : 123'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 9
      end
      object QRLabel67: TQRLabel
        Left = 293
        Top = 323
        Width = 54
        Height = 16
        Size.Values = (
          42.333333333333340000
          775.229166666666800000
          854.604166666666800000
          142.875000000000000000)
        XLColumn = 0
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = 'Me.e.'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 10
      end
      object QRLabel70: TQRLabel
        Left = 353
        Top = 307
        Width = 84
        Height = 16
        Size.Values = (
          42.333333333333340000
          933.979166666666600000
          812.270833333333400000
          222.250000000000000000)
        XLColumn = 0
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = 'Nett'#243' egys.'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 10
      end
      object QRLabel30: TQRLabel
        Left = 443
        Top = 307
        Width = 84
        Height = 16
        Size.Values = (
          42.333333333333340000
          1172.104166666667000000
          812.270833333333400000
          222.250000000000000000)
        XLColumn = 0
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = #193'FA %'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 10
      end
      object QRLabel31: TQRLabel
        Left = 443
        Top = 323
        Width = 84
        Height = 16
        Size.Values = (
          42.333333333333340000
          1172.104166666667000000
          854.604166666666800000
          222.250000000000000000)
        XLColumn = 0
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = #193'FA '#233'rt'#233'k'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 10
      end
      object QRLabel74: TQRLabel
        Left = 533
        Top = 307
        Width = 84
        Height = 16
        Size.Values = (
          42.333333333333340000
          1410.229166666667000000
          812.270833333333400000
          222.250000000000000000)
        XLColumn = 0
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = 'Brutt'#243
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 10
      end
      object QRLabel75: TQRLabel
        Left = 533
        Top = 323
        Width = 84
        Height = 16
        Size.Values = (
          42.333333333333340000
          1410.229166666667000000
          854.604166666666800000
          222.250000000000000000)
        XLColumn = 0
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = #233'rt'#233'k'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 10
      end
      object QRLabel76: TQRLabel
        Left = 623
        Top = 307
        Width = 84
        Height = 16
        Size.Values = (
          42.333333333333340000
          1648.354166666667000000
          812.270833333333400000
          222.250000000000000000)
        XLColumn = 0
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = 'Valut'#225's '#233'rt'#233'k'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 10
      end
      object QRLabel77: TQRLabel
        Left = 623
        Top = 323
        Width = 84
        Height = 16
        Size.Values = (
          42.333333333333340000
          1648.354166666667000000
          854.604166666666800000
          222.250000000000000000)
        XLColumn = 0
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = #193'rfolyam'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 10
      end
      object QRLabel81: TQRLabel
        Left = 568
        Top = 60
        Width = 142
        Height = 16
        Size.Values = (
          42.333333333333330000
          1502.833333333333000000
          158.750000000000000000
          375.708333333333300000)
        XLColumn = 0
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'P'#233'ld'#225'ny : 123'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 9
      end
      object QRLabel80: TQRLabel
        Left = 16
        Top = 172
        Width = 340
        Height = 14
        Size.Values = (
          37.041666666666670000
          42.333333333333340000
          455.083333333333300000
          899.583333333333400000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'QRLCIM'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 8
      end
      object QRLabel85: TQRLabel
        Left = 572
        Top = 208
        Width = 136
        Height = 16
        Size.Values = (
          42.333333333333340000
          1513.416666666667000000
          550.333333333333400000
          359.833333333333400000)
        XLColumn = 0
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = 'Rechnungs Nr.'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 10
      end
      object QRLabel86: TQRLabel
        Left = 12
        Top = 208
        Width = 136
        Height = 16
        Size.Values = (
          42.333333333333340000
          31.750000000000000000
          550.333333333333400000
          359.833333333333400000)
        XLColumn = 0
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = 'Zahlungsweise'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 10
      end
      object QRLabel88: TQRLabel
        Left = 152
        Top = 209
        Width = 136
        Height = 16
        Size.Values = (
          42.333333333333330000
          402.166666666666700000
          552.979166666666700000
          359.833333333333300000)
        XLColumn = 0
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = 'Leistungsdatum'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 10
      end
      object QRLabel89: TQRLabel
        Left = 292
        Top = 208
        Width = 136
        Height = 16
        Size.Values = (
          42.333333333333340000
          772.583333333333400000
          550.333333333333400000
          359.833333333333400000)
        XLColumn = 0
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = 'Rechnungsdatum'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 10
      end
      object QRLabel90: TQRLabel
        Left = 432
        Top = 208
        Width = 136
        Height = 16
        Size.Values = (
          42.333333333333340000
          1143.000000000000000000
          550.333333333333400000
          359.833333333333400000)
        XLColumn = 0
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = 'Zahlungsfrist'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 10
      end
      object QRShape29: TQRShape
        Left = 620
        Top = 340
        Width = 90
        Height = 35
        Size.Values = (
          92.604166666666680000
          1640.416666666667000000
          899.583333333333400000
          238.125000000000000000)
        XLColumn = 0
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape30: TQRShape
        Left = 290
        Top = 340
        Width = 60
        Height = 35
        Size.Values = (
          92.604166666666680000
          767.291666666666800000
          899.583333333333400000
          158.750000000000000000)
        XLColumn = 0
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape31: TQRShape
        Left = 10
        Top = 340
        Width = 280
        Height = 35
        Size.Values = (
          92.604166666666680000
          26.458333333333330000
          899.583333333333400000
          740.833333333333400000)
        XLColumn = 0
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRLabel33: TQRLabel
        Left = 16
        Top = 349
        Width = 270
        Height = 16
        Size.Values = (
          42.333333333333340000
          42.333333333333340000
          923.395833333333400000
          714.375000000000000000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Einordnungsnummer, Waren (Leistungs) bezeichnung'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 10
      end
      object QRLabel87: TQRLabel
        Left = 293
        Top = 342
        Width = 54
        Height = 16
        Size.Values = (
          42.333333333333340000
          775.229166666666800000
          904.875000000000000000
          142.875000000000000000)
        XLColumn = 0
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = 'Menge'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 10
      end
      object QRShape32: TQRShape
        Left = 350
        Top = 340
        Width = 90
        Height = 35
        Size.Values = (
          92.604166666666680000
          926.041666666666800000
          899.583333333333400000
          238.125000000000000000)
        XLColumn = 0
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRLabel91: TQRLabel
        Left = 353
        Top = 358
        Width = 84
        Height = 16
        Size.Values = (
          42.333333333333340000
          933.979166666666600000
          947.208333333333400000
          222.250000000000000000)
        XLColumn = 0
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = 'Nett'#243' '#233'rt'#233'k'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 10
      end
      object QRShape35: TQRShape
        Left = 440
        Top = 340
        Width = 90
        Height = 35
        Size.Values = (
          92.604166666666680000
          1164.166666666667000000
          899.583333333333400000
          238.125000000000000000)
        XLColumn = 0
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape36: TQRShape
        Left = 530
        Top = 340
        Width = 90
        Height = 35
        Size.Values = (
          92.604166666666680000
          1402.291666666667000000
          899.583333333333400000
          238.125000000000000000)
        XLColumn = 0
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRLabel92: TQRLabel
        Left = 293
        Top = 358
        Width = 54
        Height = 16
        Size.Values = (
          42.333333333333340000
          775.229166666666800000
          947.208333333333400000
          142.875000000000000000)
        XLColumn = 0
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = 'M.einh.'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 10
      end
      object QRLabel93: TQRLabel
        Left = 353
        Top = 342
        Width = 84
        Height = 16
        Size.Values = (
          42.333333333333340000
          933.979166666666600000
          904.875000000000000000
          222.250000000000000000)
        XLColumn = 0
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = 'Nett'#243' egys.'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 10
      end
      object QRLabel94: TQRLabel
        Left = 443
        Top = 342
        Width = 84
        Height = 16
        Size.Values = (
          42.333333333333340000
          1172.104166666667000000
          904.875000000000000000
          222.250000000000000000)
        XLColumn = 0
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = #193'FA %'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 10
      end
      object QRLabel95: TQRLabel
        Left = 443
        Top = 358
        Width = 84
        Height = 16
        Size.Values = (
          42.333333333333340000
          1172.104166666667000000
          947.208333333333400000
          222.250000000000000000)
        XLColumn = 0
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = #193'FA '#233'rt'#233'k'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 10
      end
      object QRLabel96: TQRLabel
        Left = 533
        Top = 342
        Width = 84
        Height = 16
        Size.Values = (
          42.333333333333340000
          1410.229166666667000000
          904.875000000000000000
          222.250000000000000000)
        XLColumn = 0
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = 'Brutt'#243
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 10
      end
      object QRLabel97: TQRLabel
        Left = 533
        Top = 358
        Width = 84
        Height = 16
        Size.Values = (
          42.333333333333340000
          1410.229166666667000000
          947.208333333333400000
          222.250000000000000000)
        XLColumn = 0
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = #233'rt'#233'k'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 10
      end
      object QRLabel98: TQRLabel
        Left = 623
        Top = 342
        Width = 84
        Height = 16
        Size.Values = (
          42.333333333333340000
          1648.354166666667000000
          904.875000000000000000
          222.250000000000000000)
        XLColumn = 0
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = 'Valut'#225's '#233'rt'#233'k'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 10
      end
      object QRLabel99: TQRLabel
        Left = 623
        Top = 358
        Width = 84
        Height = 16
        Size.Values = (
          42.333333333333340000
          1648.354166666667000000
          947.208333333333400000
          222.250000000000000000)
        XLColumn = 0
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = #193'rfolyam'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 10
      end
      object QRLabel101: TQRLabel
        Left = 370
        Top = 156
        Width = 330
        Height = 16
        Size.Values = (
          42.333333333333340000
          978.958333333333200000
          412.750000000000100000
          873.125000000000000000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'QRLCIM'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 9
      end
      object QRLabel8A: TQRLabel
        Left = 16
        Top = 158
        Width = 340
        Height = 14
        Size.Values = (
          37.041666666666670000
          42.333333333333340000
          418.041666666666700000
          899.583333333333400000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'QRLCIM'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 8
      end
      object SZ_BOLD1: TQRLabel
        Left = 155
        Top = 8
        Width = 57
        Height = 18
        Enabled = False
        Size.Values = (
          48.683333333333340000
          410.633333333333400000
          21.166666666666670000
          150.283333333333300000)
        XLColumn = 0
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = 'Sz'#225'mla / Rechnung'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 10
      end
      object SZ_NORM1: TQRLabel
        Left = 155
        Top = 27
        Width = 57
        Height = 18
        Enabled = False
        Size.Values = (
          48.683333333333340000
          410.633333333333400000
          71.966666666666660000
          150.283333333333300000)
        XLColumn = 0
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = 'Sz'#225'mla / Rechnung'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 10
      end
      object QRLabel141: TQRLabel
        Left = 370
        Top = 172
        Width = 330
        Height = 16
        Size.Values = (
          42.333333333333340000
          978.958333333333200000
          455.083333333333300000
          873.125000000000000000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'QRLabel141'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 9
      end
    end
    object QRBand1: TQRBand
      Left = 38
      Top = 417
      Width = 718
      Height = 33
      AlignToBottom = False
      BeforePrint = QRBand1BeforePrint
      TransparentBand = False
      ForceNewColumn = False
      ForceNewPage = False
      Size.Values = (
        87.312500000000000000
        1899.708333333333000000)
      PreCaluculateBandHeight = False
      KeepOnOnePage = False
      BandType = rbDetail
      object QRShape24: TQRShape
        Left = 530
        Top = 0
        Width = 90
        Height = 32
        Enabled = False
        Size.Values = (
          84.666666666666680000
          1402.291666666667000000
          0.000000000000000000
          238.125000000000000000)
        XLColumn = 0
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape20: TQRShape
        Left = 10
        Top = 0
        Width = 280
        Height = 32
        Enabled = False
        Size.Values = (
          84.666666666666680000
          26.458333333333330000
          0.000000000000000000
          740.833333333333400000)
        XLColumn = 0
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape21: TQRShape
        Left = 290
        Top = 0
        Width = 60
        Height = 32
        Enabled = False
        Size.Values = (
          84.666666666666680000
          767.291666666666800000
          0.000000000000000000
          158.750000000000000000)
        XLColumn = 0
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape22: TQRShape
        Left = 350
        Top = 0
        Width = 90
        Height = 32
        Enabled = False
        Size.Values = (
          84.666666666666680000
          926.041666666666800000
          0.000000000000000000
          238.125000000000000000)
        XLColumn = 0
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape23: TQRShape
        Left = 440
        Top = 0
        Width = 90
        Height = 32
        Enabled = False
        Size.Values = (
          84.666666666666680000
          1164.166666666667000000
          0.000000000000000000
          238.125000000000000000)
        XLColumn = 0
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape25: TQRShape
        Left = 620
        Top = 0
        Width = 90
        Height = 32
        Enabled = False
        Size.Values = (
          84.666666666666680000
          1640.416666666667000000
          0.000000000000000000
          238.125000000000000000)
        XLColumn = 0
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRLabel1: TQRLabel
        Left = 16
        Top = 1
        Width = 270
        Height = 16
        Size.Values = (
          42.333333333333330000
          42.333333333333330000
          2.645833333333333000
          714.375000000000000000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'QRLabel1'
        Color = clWhite
        Font.Charset = EASTEUROPE_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 9
      end
      object QRLabel34: TQRLabel
        Left = 353
        Top = 17
        Width = 84
        Height = 16
        Size.Values = (
          42.333333333333330000
          933.979166666666800000
          44.979166666666670000
          222.250000000000000000)
        XLColumn = 0
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = '123456789'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Courier New'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 9
      end
      object QRLabel35: TQRLabel
        Left = 533
        Top = 9
        Width = 84
        Height = 16
        Size.Values = (
          42.333333333333330000
          1410.229166666667000000
          23.812500000000000000
          222.250000000000000000)
        XLColumn = 0
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = '4567890123'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Courier New'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 9
      end
      object QRLabel36: TQRLabel
        Left = 443
        Top = 17
        Width = 84
        Height = 16
        Size.Values = (
          42.333333333333330000
          1172.104166666667000000
          44.979166666666670000
          222.250000000000000000)
        XLColumn = 0
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = '123456789012'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Courier New'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 9
      end
      object QRLabel37: TQRLabel
        Left = 293
        Top = 1
        Width = 54
        Height = 16
        Size.Values = (
          42.333333333333330000
          775.229166666666800000
          2.645833333333333000
          142.875000000000000000)
        XLColumn = 0
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = 'QRLabel1'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Courier New'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 9
      end
      object QRLabel61: TQRLabel
        Left = 353
        Top = 1
        Width = 84
        Height = 16
        Size.Values = (
          42.333333333333330000
          933.979166666666800000
          2.645833333333333000
          222.250000000000000000)
        XLColumn = 0
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = 'QRLabel1'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Courier New'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 9
      end
      object QRLabel68: TQRLabel
        Left = 293
        Top = 17
        Width = 54
        Height = 16
        Size.Values = (
          42.333333333333330000
          775.229166666666800000
          44.979166666666670000
          142.875000000000000000)
        XLColumn = 0
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = 'QRLabel1'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Courier New'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 9
      end
      object QRLabel32: TQRLabel
        Left = 443
        Top = 1
        Width = 84
        Height = 16
        Size.Values = (
          42.333333333333330000
          1172.104166666667000000
          2.645833333333333000
          222.250000000000000000)
        XLColumn = 0
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = #193'.M.'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Courier New'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 9
      end
      object QRLabel72: TQRLabel
        Left = 623
        Top = 3
        Width = 84
        Height = 13
        Size.Values = (
          34.395833333333330000
          1648.354166666667000000
          7.937500000000000000
          222.250000000000000000)
        XLColumn = 0
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = '7456.22 DMO'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Courier New'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object QRLabel73: TQRLabel
        Left = 623
        Top = 16
        Width = 84
        Height = 13
        Size.Values = (
          34.395833333333340000
          1648.354166666667000000
          42.333333333333340000
          222.250000000000000000)
        XLColumn = 0
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = '123456789012'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Courier New'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object QRLabel100: TQRLabel
        Left = 16
        Top = 17
        Width = 270
        Height = 16
        Size.Values = (
          42.333333333333330000
          42.333333333333330000
          44.979166666666670000
          714.375000000000000000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'QRLabel1'
        Color = clWhite
        Font.Charset = EASTEUROPE_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 9
      end
    end
    object QRBand2: TQRBand
      Left = 38
      Top = 450
      Width = 718
      Height = 311
      AlignToBottom = False
      BeforePrint = QRBand2BeforePrint
      TransparentBand = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -17
      Font.Name = 'Arial'
      Font.Style = []
      ForceNewColumn = False
      ForceNewPage = False
      ParentFont = False
      Size.Values = (
        822.854166666666700000
        1899.708333333333000000)
      PreCaluculateBandHeight = False
      KeepOnOnePage = False
      BandType = rbSummary
      object QRLabel2: TQRLabel
        Left = 445
        Top = 267
        Width = 200
        Height = 17
        Size.Values = (
          44.979166666666670000
          1177.395833333333000000
          706.437500000000000000
          529.166666666666700000)
        XLColumn = 0
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = '............................'
        Color = clWhite
        Font.Charset = EASTEUROPE_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 10
      end
      object QRLabel3: TQRLabel
        Left = 445
        Top = 284
        Width = 200
        Height = 17
        Size.Values = (
          44.979166666666670000
          1177.395833333333000000
          751.416666666666700000
          529.166666666666700000)
        XLColumn = 0
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = 'Elad'#243'/Verk'#228'ufer'
        Color = clWhite
        Font.Charset = EASTEUROPE_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 10
      end
      object QRLabel4: TQRLabel
        Left = 10
        Top = 147
        Width = 700
        Height = 17
        Size.Values = (
          44.979166666666670000
          26.458333333333330000
          388.937500000000000000
          1852.083333333333000000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'AAAAAAAA'
        Color = clWhite
        Font.Charset = EASTEUROPE_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 10
      end
      object QRLabel5: TQRLabel
        Left = 10
        Top = 173
        Width = 700
        Height = 17
        Size.Values = (
          44.979166666666670000
          26.458333333333330000
          457.729166666666700000
          1852.083333333333000000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'AAAA'
        Color = clWhite
        Font.Charset = EASTEUROPE_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 10
      end
      object QRLabel38: TQRLabel
        Left = 368
        Top = 7
        Width = 191
        Height = 16
        Size.Values = (
          42.333333333333340000
          973.666666666666900000
          18.520833333333330000
          505.354166666666700000)
        XLColumn = 0
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Nett'#243' '#246'sszesen :'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 10
      end
      object QRLabel39: TQRLabel
        Left = 10
        Top = 58
        Width = 100
        Height = 16
        Enabled = False
        Size.Values = (
          42.333333333333340000
          26.458333333333330000
          153.458333333333300000
          264.583333333333400000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = '0 % ad'#243'alap :'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 10
      end
      object QRLabel40: TQRLabel
        Left = 488
        Top = 23
        Width = 71
        Height = 16
        Size.Values = (
          42.333333333333340000
          1291.166666666667000000
          60.854166666666680000
          187.854166666666700000)
        XLColumn = 0
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = '15 % '#193'FA :'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 10
      end
      object QRLabel41: TQRLabel
        Left = 488
        Top = 39
        Width = 71
        Height = 16
        Size.Values = (
          42.333333333333340000
          1291.166666666667000000
          103.187500000000000000
          187.854166666666700000)
        XLColumn = 0
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = '25 % '#193'FA :'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 10
      end
      object QRLabel42: TQRLabel
        Left = 248
        Top = 39
        Width = 92
        Height = 16
        Size.Values = (
          42.333333333333340000
          656.166666666666800000
          103.187500000000000000
          243.416666666666700000)
        XLColumn = 0
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = '25 % ad'#243'alap :'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 10
      end
      object QRLabel43: TQRLabel
        Left = 248
        Top = 23
        Width = 92
        Height = 16
        Size.Values = (
          42.333333333333340000
          656.166666666666800000
          60.854166666666680000
          243.416666666666700000)
        XLColumn = 0
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = '15 % ad'#243'alap :'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 10
      end
      object QRLabel44: TQRLabel
        Left = 10
        Top = 39
        Width = 100
        Height = 16
        Size.Values = (
          42.333333333333340000
          26.458333333333330000
          103.187500000000000000
          264.583333333333400000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Nem ad'#243'k'#246'teles :'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 10
      end
      object QRLabel45: TQRLabel
        Left = 344
        Top = 58
        Width = 215
        Height = 16
        Size.Values = (
          42.333333333333340000
          910.166666666666600000
          153.458333333333300000
          568.854166666666800000)
        XLColumn = 0
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Mind'#246'sszesen :'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 10
      end
      object QRLabel46: TQRLabel
        Left = 10
        Top = 92
        Width = 39
        Height = 16
        Size.Values = (
          42.333333333333340000
          26.458333333333330000
          243.416666666666700000
          103.187500000000000000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'azaz'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 10
      end
      object QRLabel48: TQRLabel
        Left = 48
        Top = 92
        Width = 657
        Height = 16
        Size.Values = (
          42.333333333333340000
          127.000000000000000000
          243.416666666666700000
          1738.312500000000000000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = '.....'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 10
      end
      object LabelO1: TQRLabel
        Left = 565
        Top = 7
        Width = 85
        Height = 16
        Size.Values = (
          42.333333333333340000
          1494.895833333333000000
          18.520833333333330000
          224.895833333333300000)
        XLColumn = 0
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Ossz'
        Color = clWhite
        Font.Charset = EASTEUROPE_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 10
      end
      object QRLabel47: TQRLabel
        Left = 565
        Top = 23
        Width = 85
        Height = 16
        Size.Values = (
          42.333333333333340000
          1494.895833333333000000
          60.854166666666680000
          224.895833333333300000)
        XLColumn = 0
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Ossz'
        Color = clWhite
        Font.Charset = EASTEUROPE_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 10
      end
      object QRLabel49: TQRLabel
        Left = 565
        Top = 39
        Width = 85
        Height = 16
        Size.Values = (
          42.333333333333340000
          1494.895833333333000000
          103.187500000000000000
          224.895833333333300000)
        XLColumn = 0
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Ossz'
        Color = clWhite
        Font.Charset = EASTEUROPE_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 10
      end
      object QRLabel50: TQRLabel
        Left = 565
        Top = 58
        Width = 85
        Height = 16
        Size.Values = (
          42.333333333333340000
          1494.895833333333000000
          153.458333333333300000
          224.895833333333300000)
        XLColumn = 0
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Ossz'
        Color = clWhite
        Font.Charset = EASTEUROPE_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 10
      end
      object QRLabel51: TQRLabel
        Left = 344
        Top = 24
        Width = 79
        Height = 16
        Size.Values = (
          42.333333333333340000
          910.166666666666600000
          63.500000000000000000
          209.550000000000000000)
        XLColumn = 0
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Ossz'
        Color = clWhite
        Font.Charset = EASTEUROPE_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 10
      end
      object QRLabel52: TQRLabel
        Left = 344
        Top = 40
        Width = 79
        Height = 16
        Size.Values = (
          42.333333333333340000
          910.166666666666600000
          105.833333333333300000
          209.550000000000000000)
        XLColumn = 0
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Ossz'
        Color = clWhite
        Font.Charset = EASTEUROPE_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 10
      end
      object QRLabel53: TQRLabel
        Left = 104
        Top = 59
        Width = 76
        Height = 16
        Enabled = False
        Size.Values = (
          42.333333333333340000
          275.166666666666700000
          156.633333333333300000
          201.083333333333300000)
        XLColumn = 0
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Ossz'
        Color = clWhite
        Font.Charset = EASTEUROPE_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 10
      end
      object QRLabel54: TQRLabel
        Left = 104
        Top = 40
        Width = 76
        Height = 16
        Size.Values = (
          42.333333333333340000
          275.166666666666700000
          105.833333333333300000
          201.083333333333300000)
        XLColumn = 0
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = '-99888777'
        Color = clWhite
        Font.Charset = EASTEUROPE_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 10
      end
      object QRLabel59: TQRLabel
        Left = 10
        Top = 189
        Width = 700
        Height = 17
        Size.Values = (
          44.979166666666670000
          26.458333333333330000
          500.062500000000000000
          1852.083333333333000000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'AAAA'
        Color = clWhite
        Font.Charset = EASTEUROPE_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 10
      end
      object QRLabel60: TQRLabel
        Left = 10
        Top = 205
        Width = 700
        Height = 17
        Size.Values = (
          44.979166666666670000
          26.458333333333330000
          542.395833333333300000
          1852.083333333333000000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'AAAA'
        Color = clWhite
        Font.Charset = EASTEUROPE_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 10
      end
      object QRLabel63: TQRLabel
        Left = 114
        Top = 8
        Width = 247
        Height = 15
        Size.Values = (
          39.687500000000000000
          301.625000000000000000
          21.166666666666670000
          653.520833333333400000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = '.'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 8
      end
      object QRLabel62: TQRLabel
        Left = 10
        Top = 7
        Width = 100
        Height = 16
        Size.Values = (
          42.333333333333340000
          26.458333333333330000
          18.520833333333330000
          264.583333333333400000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Poz'#237'ci'#243' sz'#225'm:'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 10
      end
      object QRShape33: TQRShape
        Left = 10
        Top = 0
        Width = 700
        Height = 2
        Size.Values = (
          5.291666666666667000
          26.458333333333330000
          0.000000000000000000
          1852.083333333333000000)
        XLColumn = 0
        Shape = qrsHorLine
        VertAdjust = 0
      end
      object QRShape34: TQRShape
        Left = 470
        Top = 55
        Width = 240
        Height = 2
        Size.Values = (
          5.291666666666667000
          1243.541666666667000000
          145.520833333333300000
          635.000000000000000000)
        XLColumn = 0
        Shape = qrsHorLine
        VertAdjust = 0
      end
      object QRLabel83: TQRLabel
        Left = 10
        Top = 23
        Width = 100
        Height = 16
        Size.Values = (
          42.333333333333340000
          26.458333333333330000
          60.854166666666680000
          264.583333333333400000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = #193'.K. ad'#243'alap :'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 10
      end
      object QRLabel84: TQRLabel
        Left = 104
        Top = 24
        Width = 76
        Height = 16
        Size.Values = (
          42.333333333333340000
          275.166666666666700000
          63.500000000000000000
          201.083333333333300000)
        XLColumn = 0
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Ossz'
        Color = clWhite
        Font.Charset = EASTEUROPE_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 10
      end
      object QRLabel78: TQRLabel
        Left = 13
        Top = 74
        Width = 546
        Height = 19
        Size.Values = (
          50.800000000000000000
          33.866666666666670000
          196.850000000000000000
          1445.683333333333000000)
        XLColumn = 0
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Valut'#225's '#233'rt'#233'k :'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 10
      end
      object QRLabel79: TQRLabel
        Left = 565
        Top = 74
        Width = 85
        Height = 16
        Size.Values = (
          42.333333333333340000
          1494.895833333333000000
          195.791666666666700000
          224.895833333333300000)
        XLColumn = 0
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Ossz'
        Color = clWhite
        Font.Charset = EASTEUROPE_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 10
      end
      object QE1: TQRLabel
        Left = 179
        Top = 59
        Width = 68
        Height = 13
        Enabled = False
        Size.Values = (
          33.866666666666670000
          474.133333333333300000
          156.633333333333300000
          179.916666666666700000)
        XLColumn = 0
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = '999999'
        Color = clWhite
        Font.Charset = EASTEUROPE_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object QE2: TQRLabel
        Left = 179
        Top = 41
        Width = 68
        Height = 13
        Size.Values = (
          33.866666666666670000
          474.133333333333300000
          107.950000000000000000
          179.916666666666700000)
        XLColumn = 0
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Ossz'
        Color = clWhite
        Font.Charset = EASTEUROPE_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object QE3: TQRLabel
        Left = 179
        Top = 25
        Width = 68
        Height = 13
        Size.Values = (
          33.866666666666670000
          474.133333333333300000
          65.616666666666680000
          179.916666666666700000)
        XLColumn = 0
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Ossz'
        Color = clWhite
        Font.Charset = EASTEUROPE_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object QE4: TQRLabel
        Left = 422
        Top = 25
        Width = 65
        Height = 13
        Size.Values = (
          33.866666666666670000
          1117.600000000000000000
          65.616666666666680000
          171.450000000000000000)
        XLColumn = 0
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = '999999'
        Color = clWhite
        Font.Charset = EASTEUROPE_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object QE5: TQRLabel
        Left = 422
        Top = 41
        Width = 65
        Height = 13
        Size.Values = (
          33.866666666666670000
          1117.600000000000000000
          107.950000000000000000
          171.450000000000000000)
        XLColumn = 0
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Ossz'
        Color = clWhite
        Font.Charset = EASTEUROPE_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object QE6: TQRLabel
        Left = 650
        Top = 25
        Width = 64
        Height = 13
        Size.Values = (
          33.866666666666670000
          1720.850000000000000000
          65.616666666666680000
          169.333333333333300000)
        XLColumn = 0
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = '999999'
        Color = clWhite
        Font.Charset = EASTEUROPE_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object QE7: TQRLabel
        Left = 650
        Top = 41
        Width = 64
        Height = 13
        Size.Values = (
          33.866666666666670000
          1720.850000000000000000
          107.950000000000000000
          169.333333333333300000)
        XLColumn = 0
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Ossz'
        Color = clWhite
        Font.Charset = EASTEUROPE_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object QRLabel65: TQRLabel
        Left = 10
        Top = 221
        Width = 700
        Height = 17
        Size.Values = (
          44.979166666666670000
          26.458333333333330000
          584.729166666666700000
          1852.083333333333000000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'AAAA'
        Color = clWhite
        Font.Charset = EASTEUROPE_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 10
      end
      object QRLabel102: TQRLabel
        Left = 48
        Top = 108
        Width = 657
        Height = 16
        Size.Values = (
          42.333333333333340000
          127.000000000000000000
          285.750000000000000000
          1738.312500000000000000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = '.....'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 10
      end
      object QRLabel103: TQRLabel
        Left = 10
        Top = 237
        Width = 700
        Height = 17
        Size.Values = (
          44.979166666666670000
          26.458333333333330000
          627.062500000000000000
          1852.083333333333000000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'AAAA'
        Color = clWhite
        Font.Charset = EASTEUROPE_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 10
      end
      object QRLabel104: TQRLabel
        Left = 10
        Top = 254
        Width = 700
        Height = 17
        Size.Values = (
          44.979166666666670000
          26.458333333333330000
          672.041666666666700000
          1852.083333333333000000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'AAAA'
        Color = clWhite
        Font.Charset = EASTEUROPE_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 10
      end
      object QRLabel106: TQRLabel
        Left = 10
        Top = 127
        Width = 700
        Height = 17
        Size.Values = (
          44.979166666666670000
          26.458333333333330000
          336.020833333333400000
          1852.083333333333000000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 
          'A sz'#225'mla k'#233'zhezv'#233'tel'#233't'#337'l sz'#225'm'#237'tott 5 napt'#225'ri napon bel'#252'l kifog'#225's' +
          ' hi'#225'ny'#225'ban sz'#225'ml'#225'nkat elfogadottnak tekintj'#252'k.'
        Color = clWhite
        Font.Charset = EASTEUROPE_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 8
      end
      object QRLabel107: TQRLabel
        Left = 10
        Top = 143
        Width = 700
        Height = 17
        Size.Values = (
          44.979166666666670000
          26.458333333333330000
          378.354166666666700000
          1852.083333333333000000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 
          'We only accept any remark within 5 calendar days after receiptio' +
          'n of this invoice.  After this date the invoice is treated fully' +
          ' accepted.'
        Color = clWhite
        Font.Charset = EASTEUROPE_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 8
      end
    end
    object QRBand4: TQRBand
      Left = 38
      Top = 761
      Width = 718
      Height = 19
      AlignToBottom = False
      TransparentBand = False
      ForceNewColumn = False
      ForceNewPage = False
      Size.Values = (
        50.270833333333330000
        1899.708333333333000000)
      PreCaluculateBandHeight = False
      KeepOnOnePage = False
      BandType = rbPageFooter
      object QRLabel82: TQRLabel
        Left = 26
        Top = 2
        Width = 198
        Height = 15
        Size.Values = (
          39.687500000000000000
          68.791666666666670000
          5.291666666666667000
          523.875000000000000000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'K'#233'sz'#237'tette : DMK COMP Kft. , Tatab'#225'nya'
        Color = clWhite
        Font.Charset = EASTEUROPE_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 8
      end
    end
  end
  object QueryKoz: TADOQuery
    Parameters = <>
    Left = 464
    Top = 16
  end
  object QueryAl: TADOQuery
    Tag = 1
    Parameters = <>
    Left = 496
    Top = 16
  end
  object QueryFej: TADOQuery
    Tag = 1
    Parameters = <>
    Left = 528
    Top = 16
  end
  object QuerySor: TADOQuery
    Tag = 1
    Parameters = <>
    Left = 560
    Top = 16
  end
end
