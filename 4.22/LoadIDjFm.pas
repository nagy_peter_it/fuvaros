unit LoadIDjFm;

interface

uses
	SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
	StdCtrls, Forms, DBCtrls, DB, DBGrids, DBTables, Grids, ExtCtrls, Buttons,
	ShellApi, Dialogs, J_FOFORMUJ, ADODB, Mask, Menus, Variants;

type
  TLoadIDjFmDlg = class(TJ_FOFORMUJDLG)
    BitBtn8: TBitBtn;
    procedure FormCreate(Sender: TObject);
    procedure BitBtn8Click(Sender: TObject);
  private
    procedure MegbizasMutat;
  public
    { Public declarations }
  end;

var
  LoadIDjFmDlg: TLoadIDjFmDlg;

implementation

uses 	Egyeb, J_SQL, Kozos, Megbizasbe;

{$R *.dfm}

procedure TLoadIDjFmDlg.BitBtn8Click(Sender: TObject);
begin
  MegbizasMutat;
end;

procedure TLoadIDjFmDlg.FormCreate(Sender: TObject);
begin
	Inherited FormCreate(Sender);

	alapstr		:= 'select * from megtetel,MEGSEGED where ms_mbkod=mt_mbkod ';
	FelTolto('', ' ',
			'', 'MT_MBKOD', alapstr, 'megtetel', QUERY_ADAT_TAG );
	kulcsmezo			:= 'MT_MBKOD';
	//nevmezo				:= 'DO_NAME';
	width				:= DLG_WIDTH;
	height				:= DLG_HEIGHT;
 	BitBtn8.Parent		:= PanelBottom;

  ButtonTor.Hide;
  ButtonUj.Hide;
  ButtonMod.Hide;
  ButtonPrint.Hide;

end;

procedure TLoadIDjFmDlg.MegbizasMutat;
var
   MBKOD: integer;
   meguj: TMegbizasbeDlg;
begin
   MBKOD:= Query1.FieldByName('MT_MBKOD').AsInteger;
   if MBKOD <> 0 then begin
       Application.CreateForm(TMegbizasbeDlg, meguj);
       meguj.Tolto('Megbizas megjelen�t�se', IntToStr(MBKOD));
       meguj.ShowModal;
       if meguj.ret_kod <> '' then begin
           MegbizasKieg(meguj.ret_kod);
           end;
       meguj.Destroy;
   end;
end;


end.
