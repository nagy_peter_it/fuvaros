unit utnyilis;

interface

uses
  WinTypes, WinProcs, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, QuickRpt, QRExpr, Qrctrls, ExtCtrls, printers, DB, DBTables, Egyeb, Forgalom, J_SQL,
  Grids, Kozos, ADODB ;

type
  TUtnyilisDlg = class(TForm)
    Rep: TQuickRep;
    QRLSzamla: TQRLabel;
    QRBand1: TQRBand;
    QRLabel15: TQRLabel;
    QRLabel16: TQRLabel;
    QRLabel17: TQRLabel;
    QRLabel6: TQRLabel;
    QRSysData1: TQRSysData;
    QRLabel7: TQRLabel;
    QRSysData4: TQRSysData;
    QRLabel1: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel3: TQRLabel;
    QRShape1: TQRShape;
    QRShape3: TQRShape;
    QRShape4: TQRShape;
    QRShape6: TQRShape;
    QRShape7: TQRShape;
    QRShape8: TQRShape;
    QRLabel4: TQRLabel;
    QRLabel9: TQRLabel;
    QRLabel13: TQRLabel;
    QRLabel14: TQRLabel;
    QRLabel19: TQRLabel;
    QRLabel20: TQRLabel;
    QRLabel22: TQRLabel;
    QRLabel23: TQRLabel;
    QRLabel24: TQRLabel;
    QRLabel25: TQRLabel;
    QRLabel2: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel11: TQRLabel;
    QRLabel18: TQRLabel;
    QRShape2: TQRShape;
    QRShape9: TQRShape;
    QRShape10: TQRShape;
    QRShape12: TQRShape;
    QRShape13: TQRShape;
    QRShape14: TQRShape;
    QRSysData2: TQRSysData;
    QL1: TQRLabel;
    QL2: TQRLabel;
    QL0: TQRLabel;
    QL00: TQRLabel;
    QRShape5: TQRShape;
    QRLabel10: TQRLabel;
    QRLabel12: TQRLabel;
    QRShape11: TQRShape;
    QL11: TQRLabel;
    QL12: TQRLabel;
    QRGroup1: TQRGroup;
    QRBand4: TQRBand;
    Q3: TQRLabel;
    Q1: TQRLabel;
    QRLabel30: TQRLabel;
    QRShape18: TQRShape;
    QRShape19: TQRShape;
    Q2: TQRLabel;
    QRShape20: TQRShape;
    SG: TStringGrid;
    QRLabel21: TQRLabel;
    QRLabel26: TQRLabel;
    Query1: TADOQuery;
    Query2: TADOQuery;
    Query3: TADOQuery;
    Query4: TADOQuery;
    QL3: TQRLabel;
    QL4: TQRLabel;
    Q4: TQRLabel;
    QRShape15: TQRShape;
    procedure FormCreate(Sender: TObject);
    procedure	Tolt(rsz, datumtol, datumig, datol, datig : string );
    procedure QRBand1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure RepBeforePrint(Sender: TCustomQuickRep;
      var PrintReport: Boolean);
    procedure QRBand3BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBand4BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure FormDestroy(Sender: TObject);
  	private
  		kezdat			: string;
     	befdat   		: string;
     	reszossz 		: double;
     	reszbel  		: double;
       osszelsz		: double;
       osszmegt		: double;
  public
     	vanadat			: boolean;
     	rendezes  		: integer;
  end;

var
  UtnyilisDlg: TUtnyilisDlg;

implementation

uses
	J_FOGYASZT;
{$R *.DFM}

procedure TUtnyilisDlg.FormCreate(Sender: TObject);
begin
	EgyebDlg.SetADOQueryDatabase(Query1);
	EgyebDlg.SetADOQueryDatabase(Query2);
	EgyebDlg.SetADOQueryDatabase(Query3);
	EgyebDlg.SetADOQueryDatabase(Query4);
  	rendezes		:= 0;
   osszelsz		:= 0;
   osszmegt		:= 0;
  	EllenListTorol;
   Application.CreateForm(TFogyasztasDlg, FogyasztasDlg);
end;

procedure	TUtnyilisDlg.Tolt(rsz, datumtol, datumig, datol, datig : string );
var
  sqlstr	: string;
  reszlist : string;
begin
	kezdat	:= datol;
  	befdat	:= datig;
	reszlist	:= '('+copy(reszlist,1,Length(reszlist)-1 )+')';
	{A kapott sql alapj�n v�grehajtja a lek�rdez�st}

  	if rsz = '' then begin
		sqlstr	:= 'SELECT * FROM JARAT, GEPKOCSI WHERE GK_RESZ = JA_RENDSZ AND JA_JKEZD >= '''+
		datumtol+''' AND JA_JKEZD <= '''+ datumig+''' ';
	end else begin
		sqlstr	:= 'SELECT * FROM JARAT, GEPKOCSI WHERE GK_RESZ = JA_RENDSZ AND JA_JKEZD >= '''+
  		datumtol+''' AND JA_JKEZD <= '''+ datumig+''' AND JA_RENDSZ IN ('+rsz+') ';
  	end;
	case rendezes of
       0 :
  		sqlstr := sqlstr + ' ORDER BY GK_RESZ';
       1 :
  		sqlstr := sqlstr + ' ORDER BY GK_TIPUS, GK_RESZ';
       2 :
  		sqlstr := sqlstr + ' ORDER BY GK_GKKAT, GK_RESZ';
   end;
	vanadat := false;
  	if Query_Run(Query1, sqlstr) then begin
  		if Query1.RecordCount > 0 then begin
 			vanadat := true;
  		end;
  	end;
end;

procedure TUtnyilisDlg.QRBand1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
var
	ert 		: double;
  visz			: string;
  hol			: integer;
  folytat  	: boolean;
  nyitkm		: double;
  zarokm		: double;
begin
 	JaratEllenor( Query1.FieldByname('JA_KOD').AsString );
	ert := Query1.FieldByname('JA_OSSZKM').AsFloat;
  	if ert < 0 then begin
  		ert := 0;
  	end;
  	nyitkm			:= StringSzam(Query1.FieldByname('JA_NYITKM').AsString);
  	zarokm			:= StringSzam(Query1.FieldByname('JA_ZAROKM').AsString);
	QL1.Caption 	:= SzamString(ert, 8, 0);
	QL11.Caption 	:= SzamString(nyitkm, 8, 0);
	QL12.Caption 	:= SzamString(zarokm, 8, 0);
	QL2.Caption 	:= SzamString(Query1.FieldByname('JA_BELKM').AsFloat, 8, 0);
   // A j�rathoz tartoz� adatok meghat�roz�sa
   FogyasztasDlg.ClearAllList;
   FogyasztasDlg.JaratBetoltes(Query1.FieldByname('JA_KOD').AsString);
   FogyasztasDlg.CreateTankLista;
	FogyasztasDlg.CreateAdBlueLista;
	FogyasztasDlg.Szakaszolo;
	QL3.Caption 	:= Format('%.2f', [FogyasztasDlg.GetTenyFogyasztas]);
	QL4.Caption 	:= Format('%.2f', [FogyasztasDlg.GetMegtakaritas]);
	osszelsz    := osszelsz + FogyasztasDlg.GetTenyFogyasztas;
   osszmegt    := osszmegt + FogyasztasDlg.GetMegtakaritas;
  {A viszonyok �sszegy�jt�se}
	visz := '';
  	Query_Run(Query2,'select * from viszony where VI_JAKOD = '''+Query1.FieldByname('JA_KOD').AsString+''' ', true);
 	while not Query2.EOF do begin
 		visz := visz + Query2.FieldByname('VI_HONNAN').AsString + ' - ' +Query2.FieldByname('VI_HOVA').AsString+'; ';
     	Query2.Next;
 	end;
  	hol := 60;
  	folytat	:= Length(visz) > 60;
  	while ( ( hol > 0 ) and folytat ) do begin
  		if ( ( visz[hol] = ' ')  or ( visz[hol]= '-' ) ) then begin
     		Inc(hol);
     		folytat := false;
     	end;
     	Dec(hol);
  	end;
	if hol = 0 then begin
  		hol := 60;
  	end;
	QRLabel21.Caption	:= Query1.FieldByName('JA_JKEZD').AsString;
	QRLabel26.Caption	:= Query1.FieldByName('JA_MENSZ').AsString;
  	QL0.Caption			:= copy(visz,1,hol);
 	QL00.Caption		:= copy(visz,hol+1,255);
  	Query2.Close;
  	reszossz			:= reszossz + ert;
  	reszbel				:= reszbel  + Query1.FieldByname('JA_BELKM').AsFloat;
end;

procedure TUtnyilisDlg.RepBeforePrint(Sender: TCustomQuickRep;
      var PrintReport: Boolean);
begin
  	reszossz		:= 0;
  	reszbel			:= 0;
  	SG.RowCount  	:= 1;
   osszelsz		:= 0;
   osszmegt		:= 0;
  	SG.Rows[0].Clear;
end;

procedure TUtnyilisDlg.QRBand3BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
begin
  	QRLabel16.Caption 	:= Query1.FieldByname('JA_RENDSZ').AsString;
  	QRLabel17.Caption 	:= kezdat + ' - ' + befdat;
  	QRLabel5.Caption 	:= '';
  	QRLabel18.Caption 	:= '';
  	Query_Run(Query3,'SELECT * FROM GEPKOCSI WHERE GK_RESZ = '''+Query1.FieldByName('JA_RENDSZ').AsString+''' ', true );
  	if Query3.RecordCount > 0 then begin
     	{A fejl�c felt�lt�se}
     	QRLabel5.Caption 	:= Query3.FieldByname('GK_TIPUS').AsString + '/' + EgyebDlg.Read_SzGrid('340', Query3.FieldByName('GK_GKKAT').AsString);
     	QRLabel18.Caption 	:= SzamString(Query3.FieldByname('GK_NORMA').AsFloat,8,2)+
            ' / ' +SzamString(Query3.FieldByname('GK_NORNY').AsFloat,8,2);
  	end;
end;

procedure TUtnyilisDlg.QRBand4BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
begin
	Q1.Caption 	:= SzamString(reszossz, 8, 0);
  	Q2.Caption 	:= SzamString(reszbel, 	8, 0);
  	Q3.Caption 	:= SzamString(osszelsz, 8, 2);
  	Q4.Caption 	:= SzamString(osszmegt, 8, 2);
	osszelsz   	:= 0;
   osszmegt   	:= 0;
   reszossz	:= 0;
   reszbel		:= 0;

  	if SG.Cells[0,0] <> '' then begin
  		SG.RowCount := SG.RowCount + 1;
  	end;
  	SG.Cells[0, SG.RowCount - 1] :=	QRLabel16.Caption;	// Rendsz�m
  	SG.Cells[1, SG.RowCount - 1] :=	QRLabel5.Caption;	// T�pus / kateg�ria
  	SG.Cells[2, SG.RowCount - 1] :=	QRLabel18.Caption;  // Norm�k
  	SG.Cells[3, SG.RowCount - 1] :=	Q1.Caption;         // Megtett km
  	SG.Cells[4, SG.RowCount - 1] :=	Q2.Caption;			// Belf�ldi km
  	SG.Cells[5, SG.RowCount - 1] :=	Q3.Caption;         // Elsz�molhat� �zemanyag
  	SG.Cells[6, SG.RowCount - 1] :=	Q4.Caption;			// Megtakar�t�s
end;

procedure TUtnyilisDlg.FormDestroy(Sender: TObject);
begin
   FogyasztasDlg.Destroy;
end;

end.
