﻿unit GoogleGeoCode;
{******************************************************************************
   Távolság meghatározása a Google Distance Matrix API segítségével.
*******************************************************************************}

interface

uses
   Classes, SysUtils, Variants,  Vcl.ExtCtrls,
   GooglePoints, json, J_SQL, KozosTipusok, Kozos, HTTPRutinok;
type
   TGraph = array[1..MaxPoints, 1..MaxPoints] of integer;  // az útvonalpontok távolsága párosával

const
    ELEMENT_NOT_FOUND = -1;

type
   TGoogleGeoCode = Class (TComponent)
      public
       function CalculateGeoCode(AddressS, UtcaS: string; LogKell: boolean): string;
       function GetResultGeoCode: TGeoCodeInfo;
       function ProcessJSON(JSONText: string; TeljesCim: boolean; KeresettCim: string): string;
     private
       GeoCodeInfo: TGeoCodeInfo;
       function QueryGeoCode(AddressS, UtcaS: string; TeljesCim, LogKell: boolean): string;
       function NemFeldolgozandoJSON(JSONText: string): boolean;
       function Plus2Space(S: string): string;
   end;

implementation
   uses Egyeb, Data.DBXJSON;


function TGoogleGeoCode.CalculateGeoCode(AddressS, UtcaS: string; LogKell: boolean): string;
var
  JSON, MyResult: string;
begin
  if (AddressS <> '') then begin
    JSON:= QueryGeoCode(AddressS, UtcaS, True, LogKell); // teljes címmel
    MyResult:= ProcessJSON(JSON, True, AddressS+' '+UtcaS);
    if MyResult<>'' then begin  // van hibás cím
      JSON:= QueryGeoCode(AddressS, UtcaS, False, LogKell); // csak ország-város, de csak azoknál, amit nem ismert fel!!
      MyResult:= ProcessJSON(JSON, False, AddressS);
      end;  // if
    Result:=MyResult;
    end;
end;

function TGoogleGeoCode.NemFeldolgozandoJSON(JSONText: string): boolean;
begin
  if (Pos('HHiba', JSONText) > 0)  // ezen átment valahogyan ... (copy(JSONText, 1, 5) = 'HHiba')
    or (Pos('UNKNOWN_ERROR', JSONText) > 0)
    or (Pos('Connection timed out', JSONText) > 0)
    or (Pos('Bad Request', JSONText) > 0)
    or (Pos('Host not found', JSONText) > 0) then Result:=True
  else Result:=False;
end;

function TGoogleGeoCode.QueryGeoCode(AddressS, UtcaS: string; TeljesCim, LogKell: boolean): string;
const
  CRLF = chr(13)+chr(10);
  TAB = chr(9);
  LF = chr(10);
var
   // str, json_output: string;
   UTF8Url, json_output, Cim: string;
   str, resultstring, StoreResult, Honnan: string;
   TotalPoints: integer;
begin

   Cim:= trim(AddressS);
   if TeljesCim then
          Cim:= Cim + '+'+Trim(UtcaS);  // + utca-házszám

   Cim:=StringReplace(Cim, ' ', '+', [rfReplaceAll]);  // lehet hogy ország kód->név átalakítás is kell

   str:='https://maps.googleapis.com/maps/api/geocode/json?';
   str:=str+'address=';
   str:=str+ Cim;
   str:=str+'&key='+EgyebDlg.MATRIXAPIKEY;  // az .INI-ből, API key for jsspeedmatrix@gmail.com
   UTF8Url:=StringEncodeAsUTF8(str);
   json_output:=FindURLQuery(UTF8Url);
   json_output:= StringReplace(json_output, LF, ' ', [rfReplaceAll]);
   Honnan:='CACHE';
   if json_output='' then begin
      json_output:= HttpGet(UTF8Url);
      if Pos('exceeded', json_output)=0 then
          Honnan:='QUERY'
      else Honnan:='Q----';  // ezzel jelezzük a logban, hogy túlfutott az ingyenes limiten
      if Honnan='QUERY' then begin // sikeres lekérdezés esetén tárolunk
         if not NemFeldolgozandoJSON(json_output) then  // ne tároljuk, ha pl. "HTTP/1.1 500 Internal Server Error"  vagy egyéb zűr
           StoreResult:=StoreURLQuery(UTF8Url, json_output, 1);  // ha a JSON nem fér bele a táblába, nem tároljuk, majd lekérdezzük újra, ez van.
         end;
      end;
   if LogKell then begin
      EgyebDlg.AppendLog('GOOGLEGEOCODE','['+Honnan+' '+StoreResult+'] '+UTF8Url);
      end;
   Result:= json_output;
end;

function TGoogleGeoCode.Plus2Space(S: string): string;
    begin
      Result:= StringReplace(S, '+', ' ', [rfReplaceAll]);
    end;

function TGoogleGeoCode.ProcessJSON(JSONText: string; TeljesCim: boolean; KeresettCim: string): string;
var
  // JSON: TJSONObject;
  vJSONValue, vResults, vGeometry, vLocation: TJSONValue;
  vJSONScenario: TJSONObject;
  vJSONString: TJSONString;
  vJSONPair: TJSONPair;
  MyLatitude, MyLongitude: double;
  retvalue, ActValue, HibaHely, Hibauzenet, S, ResultStatus, ElemNeve: string;
  FoundGood: boolean;

begin
  if NemFeldolgozandoJSON(JSONText) then begin
     result:='HTTP hiba!';
     Exit;   // exit function
     end;
  retvalue:='';
  HibaHely:='01';
  // --- a GeoCode válasz néha szintaktikai hibás (egy felesleges kapcsos zárójellel kezdődik) ---- //
  if copy(JSONText,1,2)='{{' then begin
    JSONText:= copy(JSONText,2,99999999);  // levesszük
    end;
  // ------- //
  try
   try
     try
        vJSONValue := TJSONObject.ParseJSONValue(JSONText);
        vJSONScenario:= vJSONValue as TJSONObject;
     except
         on E: exception do begin
            Hibakiiro('JSON Parse error ('+E.Message+'), text='+JSONText);
            retvalue:='JSON Parse error';
            end; // on E
        end;  // try-esxept
    HibaHely:='02';
    vJSONPair:= vJSONScenario.Get('status');
    // --- úgy tűnik nincs ilyen ..... vJSONPair:= vJSONScenario.GetPairByName('status');
    HibaHely:='02.1';
    vJSONString:= vJSONPair.JsonValue as TJSONString;
    HibaHely:='02.2';
    ResultStatus:= vJSONString.Value;
    if ResultStatus = 'OK' then begin
        HibaHely:='03';
        vResults:= vJSONScenario.Get('results').JsonValue;
        for vJSONValue in vResults as TJSONArray do begin  // nálunk egy elemű tömbök lesznek, egyszerre csak egy pontot kérdezünk le
          vGeometry:= (vJSONValue as TJSONObject).Get('geometry').JsonValue;
          vLocation:= (vGeometry as TJSONObject).Get('location').JsonValue;
          S:=((vLocation as TJSONObject).Get('lat').JsonValue as TJSONString).Value;
          if JoSzam(S, False) then
            MyLatitude:= StrToFloat(S)
          else Raise Exception.Create('Hibás számérték: '+ S);
          HibaHely:='04';
          S:=((vLocation as TJSONObject).Get('lng').JsonValue as TJSONString).Value;
          if JoSzam(S, False) then
            MyLongitude:= StrToFloat(S)
          else Raise Exception.Create('Hibás számérték: '+ S);
          GeoCodeInfo.Coordinates.Latitude:= MyLatitude;
          GeoCodeInfo.Coordinates.Longitude:= MyLongitude;
          GeoCodeInfo.FullJSON:= JSONText;
          end;  // for
        end // if result "OK"
    else begin  // a visszaadott státusz nem OK
        if Pos('ZERO_RESULTS', JSONText)>0 then
          retvalue:= Ismeretlen_Geocode_cim+'! ('+KeresettCim+')'
        else
          retvalue:= 'A válasz státusza nem OK! JSON='+JSONText+' Keresett cím:('+KeresettCim+')';
        end;
   except
      on E: exception do begin
          Hibauzenet:= 'Hiba a "GoogleGeoCode.ProcessJSON" eljárásban ('+HibaHely+'): '+E.Message;
          NoticeKi(Hibauzenet);
          Hibakiiro(Hibauzenet+' JSON='+JSONText);
          retvalue:=Hibauzenet;
          end; // on E
      end;  // try-except
  finally
    if vJSONScenario<>nil then vJSONScenario.free;
    end;  // try-finally
  result:=retvalue;
end;

{function TGoogleGeoCode.ProcessJSON(JSONText: string; TeljesCim: boolean): string;
var
  JSON: TJSON;
  MyLatitude, MyLongitude: double;
  row, element, address: TJSON;
  retvalue, ActValue, HibaHely, Hibauzenet, S: string;
  FoundGood: boolean;

begin
  if NemFeldolgozandoJSON(JSONText) then begin
     result:='HTTP hiba!';
     Exit;   // exit function
     end;
  retvalue:='';
  HibaHely:='01';
  try
   try
     try
        JSON := TJSON.Parse(JSONText);
     except
         on E: exception do begin
            Hibakiiro('JSON Parse error ('+E.Message+'), text='+JSONText);
            retvalue:='JSON Parse error';
            end; // on E
        end;  // try-esxept
     HibaHely:='02';

    if JSON['status'].AsString = 'OK' then begin
        HibaHely:='03';
        S:= JSON['results']['geometry']['location']['lat'].AsString;
        if JoSzam(S, False) then
          MyLatitude:= StrToFloat(S)
        else Raise Exception.Create('Hibás számérték: '+ S);
        HibaHely:='04';
        S:= JSON['results']['geometry']['location']['lng'].AsString;
        if JoSzam(S, False) then
          MyLongitude:= StrToFloat(S)
        else Raise Exception.Create('Hibás számérték: '+ S);
        GeoCodeInfo.Coordinates.Latitude:= MyLatitude;
        GeoCodeInfo.Coordinates.Longitude:= MyLongitude;
        GeoCodeInfo.FullJSON:= JSONText;
        end
    else begin  // a visszaadott státusz nem OK
        retvalue:= 'A válasz státusza nem OK!';
        end;
   except
      on E: exception do begin
          Hibauzenet:= 'Hiba a "GoogleGeoCode.ProcessJSON" eljárásban ('+HibaHely+'): '+E.Message;
          NoticeKi(Hibauzenet);
          Hibakiiro(Hibauzenet+' JSON='+JSONText);
          retvalue:=Hibauzenet;
          end; // on E
      end;  // try-except
  finally
    if JSON<>nil then JSON.free;
    end;  // try-finally
  result:=retvalue;
end;
}

function TGoogleGeoCode.GetResultGeoCode: TGeoCodeInfo;
begin
  Result:=GeoCodeInfo;
end;


end.
