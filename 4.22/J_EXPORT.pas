unit J_Export;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Buttons, StdCtrls, Mask, Kozos, J_SQL, DB, DBTables,
  Grids, FileCtrl, WinTypes, WinProcs, ExtCtrls, ADODB;

const
	URESSTRING     	= '                                                                                                    ' +
		'                                                                                                    ';
type
	TExportDlg = class(TForm)
	BitElkuld: TBitBtn;
	BitKilep: TBitBtn;
	Label2: TLabel;
	 M1: TMaskEdit;
	 SpeedButton1: TSpeedButton;
	 OpenDialog1: TOpenDialog;
	 Label1: TLabel;
	 Memo1: TMemo;
	 ComboBox1: TComboBox;
	 RadioGroup1: TRadioGroup;
	 Label3: TLabel;
	 Query1: TADOQuery;
	 Query2: TADOQuery;
	 Label4: TLabel;
	procedure 	FormCreate(Sender: TObject);
	procedure 	BitElkuldClick(Sender: TObject);
	procedure 	BitKilepClick(Sender: TObject);
  procedure 	SpeedButton1Click(Sender: TObject);
	procedure 	Tolto (cim : string; qe : TAdoQuery);
	procedure 	ToltoGrid (cim : string; gr : TStringGrid);
	function 	FileEllenor : boolean;
	procedure 	MemoToltoGrid;
	procedure 	RadioGroup1Click(Sender: TObject);
	function 	XlsIro : boolean;
	function 	XlsIroGrid : boolean;
	procedure 	Beallitas;
	function 	SilentXls(fn : string) : boolean;	// Az ablak menyit�sa n�lk�l leteszi a grid-et vagy az sql-t xls-be
	function 	SilentFile(fn : string) : boolean;	// Az ablak menyit�sa n�lk�l leteszi a grid-et vagy az sql-t file-ba
	procedure 	FileTolto;
	procedure 	Kiiras(rek, ossz : integer);
  function NeLegyenSorveg(const S: string): string;

	private
		kilepes			: boolean;
		focim			: string;
		query			: TAdoQuery;
		megszakit   	: boolean;
	public
		grid	    	: TStringGrid;
		gridexport		: boolean;
		filename	  	: string;
		kellmezonev		: boolean;
		IS_HUNGARIAN	: boolean;
		kezdosor	  	: integer;
		kezdooszlop		: integer;
		elvalaszto		: string;
	end;

var
	ExportDlg: TExportDlg;

implementation

uses
	J_Excel;

{$R *.DFM}

procedure TExportDlg.FormCreate(Sender: TObject);
begin
	IS_HUNGARIAN	 	:= false;
	kellmezonev			:= true;
	gridexport			:= false;
	megszakit           := true;
	filename	 		:= '';
	M1.Text 	 		:= '';
	kezdosor	 		:= 0;
	kezdooszlop			:= 0;
	elvalaszto			:= Chr(9);
	Label1.Caption 		:= '';
	Label1.Update;
	RadioGroup1Click(Sender);
end;

procedure TExportDlg.BitElkuldClick(Sender: TObject);
begin
	if not FileEllenor then begin
		Exit;
	end;
	Screen.Cursor	:= crHourGlass;
	elvalaszto		:= ComboBox1.Text;
	if elvalaszto = 'TAB' then begin
		elvalaszto := Chr(9);
	end;
	if elvalaszto = 'SPACE' then begin
		elvalaszto := ' ';
	end;

	if gridexport then begin
		// A grid-et kell export�lni
		case RadioGroup1.ItemIndex of
			0 : // V�g�lap
			begin
			   MemoToltoGrid;
			   Memo1.SelectAll;
			   Memo1.CopyToClipboard;
			end;
			1 : // �llom�ny
			begin
			   MemoToltoGrid;
			   Memo1.Lines.SaveToFile(M1.Text);
			end;
			2 : // XLS
			begin
			   XlsIroGrid;
			end;
		end;
	end else begin
	  case RadioGroup1.ItemIndex of
		 0 : // V�g�lap
			begin
			   FileTolto;
			   Memo1.SelectAll;
			   Memo1.CopyToClipboard;
			end;
		 1 : // �llom�ny
			begin
			   FileTolto;
			end;
		 2 : // XLS
			begin
			   XlsIro;
			end;
	  end;
	end;
	filename		:= M1.Text;
	Screen.Cursor	:= crDefault;
	Close;
end;

procedure TExportDlg.BitKilepClick(Sender: TObject);
begin
	if not megszakit then begin
		megszakit   := true;
	end else begin
		kilepes	:= true;
		Close;
	end;
end;

procedure TExportDlg.SpeedButton1Click(Sender: TObject);
begin
	if RadioGroup1.ItemIndex = 1 then begin
		OpenDialog1.Filter		:= 'Text files (*.txt)|*.TXT';
		OpenDialog1.DefaultExt  := 'TXT';
	end else begin
		OpenDialog1.Filter		:= 'Excel files (*.xls)|*.XLS';
		OpenDialog1.DefaultExt  := 'XLS';
	end;
	if OpenDialog1.Execute then begin
		M1.Text := OpenDialog1.FileName;
	end;
end;

procedure TExportDlg.Tolto (cim : string; qe : TAdoQuery);
begin
	focim	:= cim;
	query	:= qe;
	Beallitas;
end;

procedure TExportDlg.ToltoGrid (cim : string; gr : TStringGrid);
begin
	focim			:= cim;
	grid			:= gr;
	gridexport		:= true;
  	Beallitas;
end;

function TExportDlg.FileEllenor : boolean;
var
	valaszt	: integer;
begin
	Result	:= false;
	if RadioGroup1.ItemIndex = 0 then begin
		Result	:= true;
		Exit;
	end;
	If M1.Text = '' then begin
		if IS_HUNGARIAN then begin
			Application.MessageBox('Hi�nyz� �llom�nyn�v !','Hiba',MB_OK);
		end else begin
			Application.MessageBox('Hi�nyz� �llom�nyn�v ! / File name is missing !','Hiba/Error',MB_OK);
		end;
		Exit;
	end;
	if FileExists(M1.Text) then begin
		if IS_HUNGARIAN then begin
			valaszt := Application.MessageBox('L�tez� �llom�ny. Fel�l�rja ?','Hiba',MB_OKCANCEL);
		end else begin
			valaszt := Application.MessageBox('L�tez� �llom�ny. Fel�l�rja ? / Existing file. Overwrite ?','Hiba / Error',MB_OKCANCEL);
		end;
		if valaszt = 1 then begin
			// Az eredeti �llom�ny t�rl�se
			Result	:= DeleteFile(PChar(M1.Text));
		end else begin
			Exit;
		end;
  end else begin
		Result	:= true;
  end;
end;

procedure TExportDlg.RadioGroup1Click(Sender: TObject);
begin
	case RadioGroup1.ItemIndex of
	0 : // Export�l�s v�g�lapra
		begin
			M1.Text					:= '';
			M1.Enabled				:= false;
			M1.Color  				:= clSilver;
			SpeedButton1.Enabled	:= false;
			ComboBox1.Enabled		:= true;
		end;
	  1 : // Export�l�s sz�veges �llom�nyba
		begin
			M1.Enabled				:= true;
			M1.Color  				:= clWindow;
			SpeedButton1.Enabled	:= true;
			ComboBox1.Enabled		:= true;
		end;
	  2 : // Export�l�s XLS �llom�nyba
		begin
			M1.Enabled				:= true;
			M1.Color  				:= clWindow;
			SpeedButton1.Enabled	:= true;
			ComboBox1.Enabled		:= false;
		end;
	end;
end;

function TExportDlg.XlsIro : boolean;
var
  EX		   	: TJExcel;
  i		   	: integer;
  boki			: TBookMark;
  rekszam		: integer;
  oszlop		: integer;
begin
	// Az SQL tartalm�nak ki�r�sa XLS-be
	result	:= false;
	EX := TJexcel.Create(ExportDlg);
	if not EX.OpenXlsFile(M1.Text) then begin
		Exit;
	end;
	rekszam 	:= 1;
	boki 		:= Query.GetBookMark;
	Query.DisableControls;
	Query.First;

	// Az SQL tartalm�nak ki�r�sa
	EX.ColNumber	:= Query.FieldCount;
	EX.InitRow;
	if kellmezonev then begin
		// A fejl�c ki�r�sa
	  oszlop	:= 1;
	  for i := 0 to Query.FieldCount - 1 do begin
		 if Query.Fields[i].Visible then begin
			EX.SetRowcell(oszlop, Query.Fields[i].DisplayLabel);
			Inc(oszlop);
		 end;
	  end;
	  EX.SaveRow(rekszam);
	  Inc(rekszam);
	end;
//  Excel.InitRow;
	megszakit   := false;
	while ( ( not Query.EOF ) and (not megszakit) ) do begin
		Application.ProcessMessages;
		Kiiras(rekszam, Query.RecordCount);
		oszlop	:= 1;
		for i := 0 to Query.FieldCount - 1 do begin
			if Query.Fields[i].Visible then begin
				EX.SetRowcell(oszlop, Query.Fields[i].AsString);
				Inc(oszlop);
			end;
		end;
		EX.SaveRow(rekszam);
		Inc(rekszam);
		Query.Next;
	end;
	megszakit   := true;
	Query.EnableControls;
	Query.GotoBookMark(boki);
	Query.FreeBookMark(boki);
	EX.SaveXlsFile;
	EX.Free;
	result	:= true;
end;

function TExportDlg.XlsIroGrid : boolean;
var
  EX				: TJExcel;
  i				: integer;
  j				: integer;
begin
	// Az SQL tartalm�nak ki�r�sa XLS-be
	result	:= false;
	EX := TJexcel.Create(ExportDlg);
	if not EX.OpenXlsFile(M1.Text) then begin
		Exit;
	end;
	EX.ColNumber	:= grid.ColCount;;
	for j := kezdosor+1 to grid.RowCount do begin
		Label1.Caption 	:= IntToStr(j-1) + '/' + IntToStr(grid.RowCount - 1);
		Label1.Update;
		EX.InitRow;
		for i := kezdooszlop+1 to grid.ColCount do begin
			EX.SetRowcell(i, grid.Cells[i-1,j-1]);
		end;
		EX.SaveRow(j-kezdosor);
	end;
	EX.SaveXlsFile;
	EX.Free;
	result	:= true;
end;

procedure TExportDlg.MemoToltoGrid;
var
  i   			: integer;
  j   			: integer;
  sor 			: string;
begin
	// Az SQL tartalm�nak ki�r�sa a MEMO1 v�ltoz�ba
	Memo1.Clear;
	for j := kezdosor to grid.RowCount -1 do begin
		Label1.Caption 	:= IntToStr(j) + '/' + IntToStr(grid.RowCount - 1);
		Label1.Update;
		sor := '';
		for i := kezdooszlop to grid.ColCount - 1 do begin
			// sor := sor + grid.Cells[i,j]+elvalaszto;
      sor := sor + NeLegyenSorveg(grid.Cells[i,j]) +elvalaszto;
		end;
		Memo1.Lines.Add(copy(sor,1,Length(sor)-1));
	end;
end;

procedure TExportDlg.Beallitas;
begin
	if not IS_HUNGARIAN then begin
		// Kellenek az angol sz�vegek is
		Caption					:= 'Adatok export�l�sa / Data export';
		RadioGroup1.Caption		:= ' Az export helye / Target ';
		RadioGroup1.Items[0]	:= ' V�g�lap / Clipboard';
		RadioGroup1.Items[1]	:= ' Sz�vegf�jl / Text file';
		RadioGroup1.Items[2]	:= ' XLS �llom�ny / XLS file';
		Label3.Caption			:= 'Elv�laszt� karakter/Del. char.:';
		Label2.Caption			:= '�llom�ny neve / File name :';
		BitElkuld.Caption		:= '&Ind�t�s / Start';
		BitKilep.Caption		:= '&Kil�p�s / Cancel';
	end;
end;

function TExportDlg.SilentXls(fn : string) : boolean;
begin
	Result	:= false;
	if fn = '' then begin
		Exit;
	end;
	if FileExists(fn) then begin
		DeleteFile(PChar(fn));
	end;
	Screen.Cursor	:= crHourGlass;
	M1.Text	:= fn;
	if gridexport then begin
		Result	:= XlsIroGrid;
	end else begin
		Result	:= XlsIro;
	end;
	Screen.Cursor	:= crDefault;
end;

procedure TExportDlg.FileTolto;
var
	i			: integer;
	rekszam		: integer;
	sor			: string;
	t           : TDateTime;
	F			: TextFile;
	fn			: string;
begin
	// Az SQL tartalm�nak ki�r�sa �llom�nyba
	fn			:= M1.Text;
	if RadioGroup1.ItemIndex = 0 then begin
		// Gener�lni kell egy v�letlen �llom�nyt, amit azt�n let�rl�nk
		Randomize;
		fn	:= ExtractFilePath(Application.ExeName)+'TMP'+IntToStr(Random(99999))+'.TXT';
	end;
	AssignFile(F, fn);
	{$I-}
	Rewrite(F);
	{$I+}
	if IOResult <> 0 then begin
		Exit;
	end;
	rekszam 	:= 1;
	Query.DisableControls;
	Query.First;
	if kellmezonev then begin
		for i := 0 to Query.FieldCount - 1 do begin
			// Mez�nevek ki�r�sa
			if Query.Fields[i].DataType in [ftString, ftWideString, ftSmallint, ftInteger, ftWord, ftBoolean, ftFloat, ftCurrency, ftDate, ftTime, ftDateTime,  ftBCD, ftAutoInc, ftBlob, ftFixedChar, ftMemo ] then begin
	//	  		sor := sor + Query.Fields[i].FieldName+elvalaszto;
				if Query.Fields[i].Visible then begin
					sor := sor + Query.Fields[i].DisplayLabel+elvalaszto;
				end;
			end;
		end;
		Writeln(F, sor);
	end;
	t	:= now;
	megszakit	:= false;
	while ( ( not Query.EOF ) and (not megszakit) ) do begin
		Inc(rekszam);
		Kiiras(rekszam, Query.RecordCount);
		if rekszam mod 1000 = 0 then begin
			Application.ProcessMessages;
			Label4.Caption 	:= FormatDateTime('nn.ss', now-t);
			Label4.Update;
		end;
		sor				:= '';
		for i := 0 to Query.FieldCount - 1 do begin
			if Query.Fields[i].DataType in [ftString, ftWideString, ftSmallint, ftInteger, ftWord, ftBoolean, ftFloat, ftCurrency, ftDate, ftTime, ftDateTime,  ftBCD, ftAutoInc, ftBlob, ftFixedChar, ftMemo ] then begin
				if Query.Fields[i].Visible then begin
					// sor := sor + Query.Fields[i].AsString+elvalaszto;
          sor := sor + NeLegyenSorveg(Query.Fields[i].AsString) + elvalaszto;
				end;
			end;
		end;
		Writeln(F, sor);
		Query.Next;
	end;
	megszakit   := true;
	CloseFile(F);
	Query.EnableControls;
	Label4.Caption 	:= '';
	Label4.Update;
	if RadioGroup1.ItemIndex = 0 then begin
		// Beolvassuk az �llom�nyt a Memo-ba
		if IS_HUNGARIAN then begin
			Label1.Caption 	:= 'Adatt�lt�s ...';
		end else begin
			Label1.Caption 	:= 'Loading data ...';
		end;
		Label1.Update;
		Memo1.Lines.LoadFromFile(fn);
		DeleteFile(PChar(fn));
	end;
end;

function TExportDlg.NeLegyenSorveg(const S: string): string;
var
  elem: string;
begin
  elem:= S;
  elem:=StringReplace(elem, const_CRLF, ' ', [rfReplaceAll]);  // nem �rhatjuk ki a sort�r�s jeleket
  Result:=StringReplace(elem, const_LF, ' ', [rfReplaceAll]);
end;

function TExportDlg.SilentFile(fn : string) : boolean;
begin
	Result	:= false;
	if fn = '' then begin
		Exit;
	end;
	if FileExists(fn) then begin
		DeleteFile(PChar(fn));
	end;
	Screen.Cursor	:= crHourGlass;
	RadioGroup1.ItemIndex	:= 1;
	M1.Text					:= fn;
	if gridexport then begin
		MemoToltoGrid;
		Memo1.Lines.SaveToFile(fn);
	end else begin
		FileTolto;
	end;
	Screen.Cursor	:= crDefault;
	Result			:= true;
end;

procedure TExportDlg.Kiiras(rek, ossz : integer);
begin
	if ossz < 10000 then begin
		Label1.Caption 	:= IntToStr(rek)+' / '+IntToStr(ossz);
		Label1.Update;
	end else begin
		if rek mod 1000 = 0 then begin
			Label1.Caption 	:= IntToStr(rek)+' / '+IntToStr(ossz);
			Label1.Update;
		end;
	end;
end;

end.
