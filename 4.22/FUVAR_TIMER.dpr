program FUVAR_TIMER;

uses
  Forms,
  windows,
  Aki1 in 'Aki1.pas' {Form1},
  mnb in 'mnb.pas',
  Egyeb in 'Egyeb.pas',
  J_SQL in 'J_SQL.pas',
  SMSKuld in 'SMSKuld.pas' {SMSKuldDlg},
  IDOMViewU in 'IDOMViewU.pas' {frmXMLViewer},
  Fleetboard_PosService in 'Fleetboard_PosService.pas',
  Fleetboard_PerformanceAnalysisService in 'Fleetboard_PerformanceAnalysisService.pas',
  Fleetboard_BasicService in 'Fleetboard_BasicService.pas',
  Email in 'Email.pas',
  RESTAPI_Kozos in 'RESTAPI_Kozos.pas' {RESTAPIKozosDlg};

{$R *.res}
var ExtendedStyle : Integer;
begin
  Application.Initialize;
//  ExtendedStyle:=GetWindowLong(Application.Handle, GWL_EXSTYLE);
//  SetWindowLong(Application.Handle, GWL_EXSTYLE,ExtendedStyle or WS_EX_TOOLWINDOW and not WS_EX_APPWINDOW);

  Application.ShowMainForm:=False;
  Application.Title := 'FUVAROS_TIMER';
  Application.CreateForm(TForm1, Form1);
  // Form1.WriteAppLog('Form1 created');
  Application.CreateForm(TEgyebDlg, EgyebDlg);
  // Form1.WriteAppLog('EgyebDlg created');
	EgyebDlg.v_evszam	:= '0';
	EgyebDlg.AlapToltes;
  // Form1.WriteAppLog('AlapToltes finished');
  EgyebDlg.VanAdat;  // az ADOConnection kapcsol�d�s�hoz
  // Form1.WriteAppLog('VanAdat finished');
  Application.CreateForm(TfrmXMLViewer, frmXMLViewer);
  Application.CreateForm(TEmailDlg, EmailDlg);
  Application.CreateForm(TRESTAPIKozosDlg, RESTAPIKozosDlg);
  RESTAPIKozosDlg.InitTokenRequest(EgyebDlg.TimerTruckpitTelszam, EgyebDlg.TimerTruckpitJelszo);
  // Form1.WriteAppLog('frmXMLViewer created');
  // Application.CreateForm(TSMSKuldDlg, SMSKuldDlg);

  // SMSKuldDlg.APIKEY:= Query_Select('RENDSZERKOD', 'RK_MI', 'MYSMS_API_KEY', 'RK_KOD');
  // SMSKuldDlg.APIPassword:= Query_Select('RENDSZERKOD', 'RK_MI', 'MYSMS_API_PASSWORD', 'RK_KOD');
  // SMSKuldDlg.SendingPhone:= Query_Select('RENDSZERKOD', 'RK_MI', 'MYSMS_API_PHONE', 'RK_KOD');
  // SMSKuldDlg.Init;

  Form1.EnableTimer;
  Application.Run;

//  Form1.Button2.OnClick(Form1.Button2);
end.
