unit PictureUtils;

interface

    uses Windows, SysUtils, WinTypes, Graphics, Forms, Classes, GDIPOBJ, GDIPAPI, {GdiPlus,} ActiveX, Math;

    procedure ImgResize(RefBitmap, DescBitmap:TBitmap; ujXmeret,ujYmeret:Integer);
    procedure RotateBitmap(Bmp: TBitmap; Degs: Integer; AdjustSize: Boolean;  BkColor: TColor = clNone);
    procedure CropBitmap(InBitmap : TBitmap; HonnanX, HonnanY, HovaX, HovaY, W, H :Integer; BackgroundColor: TColor);

implementation

procedure CropBitmap(InBitmap : TBitmap; HonnanX, HonnanY, HovaX, HovaY, W, H :Integer; BackgroundColor: TColor);
var
  bmp1: TBitmap;
begin
  bmp1:=TBitmap.Create;
  try
    bmp1.PixelFormat:=pf24bit;
    bmp1.Width:=W; // a kimeneti m�ret
    bmp1.Height:=H; // a kimeneti m�ret
    bmp1.Canvas.Brush.Color := BackgroundColor;
    bmp1.Canvas.FillRect(Rect(0, 0, H, W));
    BitBlt(bmp1.Canvas.Handle, HovaX, HovaY, W, H, InBitmap.Canvas.Handle, HonnanX, HonnanY, SRCCOPY);
    bmp1.Width :=W;
    bmp1.Height:=H;
    InBitmap.Assign(bmp1);
  finally
    if bmp1 <> nil then bmp1.Free;
    end;  // try-finally
end;

procedure ImgResize(RefBitmap, DescBitmap: TBitmap; ujXmeret,
  ujYmeret: Integer);
var
  XRatio, YRatio: Double;
  dx, dy: Double;          // (x,y) ??? (l,k) ??
  x, y: Double;            // (x,y) ?? pDestBitmap(c,r) ??? pRefBitmap ????
  c1, c2, c3, c4: Integer;  // Bilinear Eq.: f = c1 + c2*x + c3*y + c4*x*y
  k, l: Integer;            // (l,k) ?? pRefBitmap ?????(x,y)
  r, c: Integer;            // (c,r) ?? pDestBitmap ?????(x,y)
  b: Integer;
  pRef: array of PByteArray;
  pDest: PByteArray;
begin
  RefBitmap.PixelFormat:=pf24Bit;
  DescBitmap.PixelFormat:=pf24Bit;
  DescBitmap.Width:=ujXmeret;
  DescBitmap.Height:=ujYmeret;
  XRatio := DescBitmap.Width / RefBitmap.Width;
  YRatio := DescBitmap.Height / RefBitmap.Height;
  // ????pRefBitmap????????
  SetLength(pRef, RefBitmap.Height);
  for k := Low(pRef) to High(pRef) do
    pRef[k] := RefBitmap.ScanLine[k];
  // bilinear ??
  for r := 0 to DescBitmap.Height - 1 do
  begin
    pDest := DescBitmap.ScanLine[r];
    for c := 0 to DescBitmap.Width - 1 do
    begin
      if XRatio < 1 then x := c / XRatio + XRatio
      else              x := c / XRatio;
      if YRatio < 1 then y := r / YRatio + YRatio
      else              y := r / YRatio;
      l := Floor(x);
      k := Floor(y);
      dx := x - l;
      dy := y - k;
      // ???BGR????
      for b := 0 to 2 do
      begin
        c1 := pRef[k][l * 3 + b];
        if l = RefBitmap.Width - 1 then
          c2 := 0
        else
          c2 := pRef[k][(l + 1) * 3 + b] - pRef[k][l * 3 + b];
        if k = RefBitmap.Height - 1 then
          c3 := 0
        else
          c3 := pRef[k + 1][l * 3 + b] - pRef[k][l * 3 + b];
        if (l = RefBitmap.Width - 1) or (k = RefBitmap.Height - 1) then
          c4 := 0
        else
          c4 := pRef[k][l * 3 + b] + pRef[k + 1][(l + 1) * 3 + b] -
                pRef[k][(l + 1) * 3 + b] - pRef[k + 1][l * 3 + b];
        pDest[c * 3 + b] := Byte(Trunc(c1 + c2 * dx + c3 * dy + c4 * dx * dy));
      end;
    end;
  end;

end;

procedure RotateBitmap(Bmp: TBitmap; Degs: Integer; AdjustSize: Boolean;
  BkColor: TColor = clNone);
var
  Tmp: TGPBitmap;
  Matrix: TGPMatrix;
  C: Single;
  S: Single;
  NewSize: TSize;
  Graphs: TGPGraphics;
  P: TGPPointF;
  DrawX, DrawY: Cardinal;
begin
  Tmp := TGPBitmap.Create(Bmp.Handle, Bmp.Palette);
  Matrix := TGPMatrix.Create;
  try
    Matrix.RotateAt(Degs, MakePoint(0.5 * Bmp.Width, 0.5 * Bmp.Height));
    if AdjustSize then begin
      C := Cos(DegToRad(Degs));
      S := Sin(DegToRad(Degs));
      NewSize.cx := Round(Bmp.Width * Abs(C) + Bmp.Height * Abs(S));
      NewSize.cy := Round(Bmp.Width * Abs(S) + Bmp.Height * Abs(C));
      Bmp.Width := NewSize.cx;
      Bmp.Height := NewSize.cy;
      end;
    Graphs := TGPGraphics.Create(Bmp.Canvas.Handle);
    try
      Graphs.Clear(ColorRefToARGB(ColorToRGB(BkColor)));
      Graphs.SetTransform(Matrix);
      Graphs.DrawImage(Tmp, (Cardinal(Bmp.Width) - Tmp.GetWidth) div 2, (Cardinal(Bmp.Height) - Tmp.GetHeight) div 2);
      // Graphs.DrawImage(Tmp, 0, 0);  // a der�ksz�g� forgat�sokn�l nekem (0,0)-b�l indul a rajz
      // DrawX:= abs(Cardinal(Bmp.Width) - Tmp.GetWidth) div 2;
      // if DrawX<0 then DrawX:=0;
      // DrawY:= abs(Cardinal(Bmp.Height) - Tmp.GetHeight) div 2;
      // if DrawY<0 then DrawY:=0;
      // Graphs.DrawImage(Tmp, DrawX, DrawY);
    finally
      Graphs.Free;
    end;
  finally
    Matrix.Free;
    Tmp.Free;
  end;
end;

end.
