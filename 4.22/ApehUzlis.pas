unit ApehUzlis;

interface

uses
  WinTypes, WinProcs, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, QuickRpt, QRExpr, Qrctrls, ExtCtrls, printers, DB, DBTables, Egyeb, Forgalom, J_SQL,
  Grids, Kozos, ADODB, Variants ;

type
  TApehUzlisDlg = class(TForm)
    Rep: TQuickRep;
    QRLSzamla: TQRLabel;
    QRBand1: TQRBand;
    QRLabel15: TQRLabel;
    QRLabel16: TQRLabel;
    QRLabel17: TQRLabel;
    QRLabel6: TQRLabel;
    QRSysData1: TQRSysData;
    QRLabel7: TQRLabel;
	 QRSysData4: TQRSysData;
    QRLabel1: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel3: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel13: TQRLabel;
    QRLabel14: TQRLabel;
    QRLabel19: TQRLabel;
    QRLabel22: TQRLabel;
    QRLabel24: TQRLabel;
    QL7: TQRLabel;
    QL8: TQRLabel;
    QRLabel10: TQRLabel;
    QL5: TQRLabel;
    QRGroup1: TQRGroup;
    QRBand4: TQRBand;
    QRLabel30: TQRLabel;
    SG: TStringGrid;
    QL2: TQRLabel;
    QL3: TQRLabel;
	 Query1: TADOQuery;
    Query2: TADOQuery;
    Query3: TADOQuery;
    Query4: TADOQuery;
    QRShape1: TQRShape;
    QRShape2: TQRShape;
    QL1: TQRLabel;
    QL4: TQRLabel;
    QL6: TQRLabel;
    QRLabel2: TQRLabel;
    QRLabel5: TQRLabel;
    QHiba: TQRLabel;
    QO1: TQRLabel;
    QO2: TQRLabel;
    QO3: TQRLabel;
    QO4: TQRLabel;
    QO5: TQRLabel;
    QueryZarokm: TADOQuery;
    procedure FormCreate(Sender: TObject);
    procedure	Tolt(sofor, datol, datig : string );
    procedure QRBand1BeforePrint(Sender: TQRCustomBand;
	   var PrintBand: Boolean);
    procedure RepBeforePrint(Sender: TCustomQuickRep;
      var PrintReport: Boolean);
    procedure QRBand3BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBand4BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure FormDestroy(Sender: TObject);
  private
     kezdat 		: string;
     befdat		: string;
  	  lista			: TStringList;
     ertekek		: array [1..5] of double;
     sorszam		: integer;
     vanhiany		: boolean;
  public
     vanadat		: boolean;
  end;

var
  ApehUzlisDlg: TApehUzlisDlg;

implementation

uses
	J_Fogyaszt;

{$R *.DFM}

procedure TApehUzlisDlg.FormCreate(Sender: TObject);
begin
	EgyebDlg.SetADOQueryDatabase(Query1);
	EgyebDlg.SetADOQueryDatabase(Query2);
	EgyebDlg.SetADOQueryDatabase(Query3);
	EgyebDlg.SetADOQueryDatabase(Query4);
	EgyebDlg.SetADOQueryDatabase(QueryZarokm);
	lista		:= TStringList.Create;
  	EllenListTorol;
   Application.CreateForm(TFogyasztasDlg, FogyasztasDlg);
	Query_Run(QueryZarokm,  'SELECT ZK_KMORA, ZK_RENSZ, ZK_DATUM FROM ZAROKM ORDER BY ZK_RENSZ, ZK_DATUM ');
end;

procedure	TApehUzlisDlg.Tolt(sofor, datol, datig : string );
var
  sqlstr	: string;
begin
	vanadat := false;
	kezdat	:= datol;
	befdat	:= datig;
	sqlstr	:= 'SELECT DISTINCT JA_ORSZ, JA_ALKOD, JA_RENDSZ, JA_KOD, JA_NYITKM, JA_ZAROKM, JS_SOFOR, DO_NAME FROM JARSOFOR, JARAT, DOLGOZO '+
		' WHERE JS_JAKOD = JA_KOD AND JS_SOFOR = DO_KOD '+
		' AND ( ( JA_JKEZD >= '''+kezdat+''' AND JA_JKEZD <= '''+befdat+''' ) OR '+
			  ' ( JA_JVEGE >= '''+kezdat+''' AND JA_JVEGE <= '''+befdat+''' ) OR '+
			  ' ( JA_JKEZD <  '''+kezdat+''' AND JA_JVEGE >  '''+befdat+''' ) ) ';
	if sofor <> '' then begin
   	sqlstr := sqlstr + ' AND JS_SOFOR IN ( '+sofor+ ') ';
	end;
	sqlstr := sqlstr + ' ORDER BY DO_NAME, JS_SOFOR ';
	Query_Run(Query1, sqlstr );
//   Query1.SQL.SaveToFile(EgyebDlg.TempLIst + 'UJSQL.txt');
	if Query1.RecordCount < 1 then begin
   	Exit;
   end;
   vanadat	:= true;
end;

procedure TApehUzlisDlg.QRBand1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
var
	str		: string;
   kezdokm	: integer;
	vegzokm	: integer;
   k1, k2	: integer;
begin
	QL1.Caption			:= IntToStr(sorszam);
   Inc(sorszam);
   QL2.Caption			:= GetJaratszamFromDb(Query1);
   QL3.Caption			:= Query1.FieldByName('JA_RENDSZ').AsString;
	QHiba.Enabled		:= false;	// A hiba csak akkor kell, ha probl�ma van!!!
   QL4.Enabled			:= true;
   QL5.Enabled			:= true;
   QL6.Enabled			:= true;
   QL7.Enabled			:= true;
   QL8.Enabled			:= true;
   // Ellen�rzz�k a g�pkocsihoz a z�r�si adatokat
	str	:= DatumHozNap(kezdat, -1, true) ;
// 	Query_Run(Query2, 'SELECT ZK_KMORA FROM ZAROKM WHERE ZK_RENSZ = '''+Query1.FieldByName('JA_RENDSZ').AsString+''' AND ZK_DATUM = '''+str+''' ', true);
//   if Query2.RecordCount < 1 then begin
    if not QueryZarokm.Locate('ZK_RENSZ;ZK_DATUM',VarArrayOf([Query1.FieldByName('JA_RENDSZ').AsString,str]),[]) then begin
		QHiba.Enabled	:= true;
		QHiba.Caption	:= 'Hi�nyzik a kezd� d�tumhoz tartoz� z�r� km! ( '+str+' )';
       QL4.Enabled		:= false;
       QL5.Enabled		:= false;
       QL6.Enabled		:= false;
       QL7.Enabled		:= false;
       QL8.Enabled		:= false;
   	vanhiany		:= true;
		Exit;
   end;
   kezdokm	:= StrToIntDef(QueryZarokm.FieldByName('ZK_KMORA').AsString,0);
// 	Query_Run(Query2, 'SELECT ZK_KMORA FROM ZAROKM WHERE ZK_RENSZ = '''+Query1.FieldByName('JA_RENDSZ').AsString+''' AND ZK_DATUM = '''+befdat+''' ', true);
//	if Query2.RecordCount < 1 then begin
    if not QueryZarokm.Locate('ZK_RENSZ;ZK_DATUM',VarArrayOf([Query1.FieldByName('JA_RENDSZ').AsString,befdat]),[]) then begin
		QHiba.Enabled	:= true;
   	QHiba.Caption	:= 'Hi�nyzik a befejez� d�tumhoz tartoz� z�r� km! ( '+str+' )';
       QL4.Enabled		:= false;
       QL5.Enabled		:= false;
       QL6.Enabled		:= false;
       QL7.Enabled		:= false;
       QL8.Enabled		:= false;
   	vanhiany		:= true;
		Exit;
   end;
   vegzokm	:= StrToIntDef(QueryZarokm.FieldByName('ZK_KMORA').AsString,0);
	// Lehet futtatni a j�ratonk�nti lek�rdez�st
	FogyasztasDlg.ClearAllList;
	k1	:=  StrToIntDef(Query1.FieldByName('JA_NYITKM').AsString, 0);
	if k1 < kezdokm then begin
		k1 := kezdokm;
		QL2.Caption	 := '* ' + QL2.Caption;
	end;
	k2	:=  StrToIntDef(Query1.FieldByName('JA_ZAROKM').AsString, 0);
	if k2 > vegzokm then begin
		k2 := vegzokm;
		QL2.Caption	 := '* ' + QL2.Caption;
	end;
   FogyasztasDlg.GeneralBetoltes(Query1.FieldByName('JA_RENDSZ').AsString, k1, k2);
	FogyasztasDlg.CreateTankLista;
	FogyasztasDlg.CreateAdBlueLista;
	FogyasztasDlg.Szakaszolo;

	QL4.Caption 	:= Format('%.0f', [FogyasztasDlg.GetSoforKm(Query1.FieldByName('JS_SOFOR').AsString)]);
	QL5.Caption 	:= Format('%.2f', [FogyasztasDlg.GetSoforApehTeny(Query1.FieldByName('JS_SOFOR').AsString)]);
	QL6.Caption 	:= Format('%.2f', [FogyasztasDlg.GetSoforApehTerv(Query1.FieldByName('JS_SOFOR').AsString)]);
	QL7.Caption 	:= Format('%.2f', [FogyasztasDlg.GetSoforApehMegtakaritas(Query1.FieldByName('JS_SOFOR').AsString)]);
	QL8.Caption 	:= Format('%.2f', [FogyasztasDlg.GetSoforSulyatlag(Query1.FieldByName('JS_SOFOR').AsString)]);
	ertekek[1]		:= ertekek[1] + StringSzam(QL4.Caption);
	ertekek[2]		:= ertekek[2] + StringSzam(QL5.Caption);
	ertekek[3]		:= ertekek[3] + StringSzam(QL6.Caption);
	ertekek[4]		:= ertekek[4] + StringSzam(QL7.Caption);
	ertekek[5]		:= ertekek[5] + StringSzam(QL8.Caption) * StringSzam(QL4.Caption);

	// A lista kib�v�t�se a fogyaszt�si list�val
(*
	FogyasztasDlg.TeljesLista;
	for i := 0 to FogyasztasDlg.Memo1.Lines.Count - 1 do begin
		lista.Add(FogyasztasDlg.Memo1.Lines[i]);
	end;
	lista.Add('');
	lista.Add('*****************************************************');
	lista.Add('');
*)
end;

procedure TApehUzlisDlg.RepBeforePrint(Sender: TCustomQuickRep;
	   var PrintReport: Boolean);
var
	i : integer;
begin
  	SG.RowCount 	:= 1;
  	SG.Rows[0].Clear;
  	lista.Clear;
   sorszam			:= 1;
   for i := 1 to 5 do begin
   	ertekek[i]	:= 0;
	end;
   vanhiany		:= false;
end;

procedure TApehUzlisDlg.QRBand3BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
begin
  	QRLabel16.Caption 	:= Query1.FieldByname('DO_NAME').AsString;
  	QRLabel17.Caption 	:= kezdat + ' - ' + befdat;
end;

procedure TApehUzlisDlg.QRBand4BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
var
	i : integer;
begin
	QO1.Caption 	:= Format('%.0f', [ertekek[1]]);
	QO2.Caption 	:= Format('%.2f', [ertekek[2]]);
	QO3.Caption 	:= Format('%.2f', [ertekek[3]]);
	QO4.Caption 	:= Format('%.2f', [ertekek[4]]);
   if ertekek[1] <> 0 then begin
		QO5.Caption 	:= Format('%.2f', [ertekek[5]/ertekek[1]]);
   end else begin
		QO5.Caption 	:= '0.00';
   end;
	// Be�rjuk az �rt�keket a gridbe
  	if SG.Cells[0,0] <> '' then begin
  		SG.RowCount := SG.RowCount + 1;
  	end;
  	SG.Cells[0, SG.RowCount - 1] :=	QRLabel16.Caption;
  	SG.Cells[1, SG.RowCount - 1] :=	QO1.Caption;
  	SG.Cells[2, SG.RowCount - 1] :=	QO2.Caption;
  	SG.Cells[3, SG.RowCount - 1] :=	QO3.Caption;
  	SG.Cells[4, SG.RowCount - 1] :=	QO4.Caption;
   if vanhiany then begin
	  	SG.Cells[5, SG.RowCount - 1] :=	'-1';
   end else begin
	  	SG.Cells[5, SG.RowCount - 1] :=	QO5.Caption;
	end;
   // Kinull�zzuk az �rt�keket
   for i := 1 to 5 do begin
   	ertekek[i]	:= 0;
	end;
   sorszam			:= 1;
   vanhiany		:= false;
end;

procedure TApehUzlisDlg.FormDestroy(Sender: TObject);
begin
//  lista.SaveToFile(EgyebDlg.TempLIst + 'AULISTA.TXT');
  lista.Free;
  FogyasztasDlg.Destroy;
end;

end.
