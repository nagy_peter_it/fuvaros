unit CMRfm;

interface

uses
	SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
	StdCtrls, Forms, DBCtrls, DB, DBGrids, DBTables, Grids, ExtCtrls, Buttons,
	ShellApi, Dialogs, J_FOFORMUJ, ADODB, Mask, Menus, Variants;

type
	TCmrFmDlg = class(TJ_FOFORMUJDLG)
	 QueryUj3: TADOQuery;
	 BitBtn7: TBitBtn;
	  procedure FormCreate(Sender: TObject);override;
	 procedure Adatlap(cim, kod : string);override;
	 procedure ButtonKuldClick(Sender: TObject);override;
	 procedure ButtonTorClick(Sender: TObject);override;
	 procedure BitBtn7Click(Sender: TObject);
	 function  KeresChange(mezo, szoveg : string) : string; override;
	 private
	 public
	end;

var
	CmrFmDlg : TCmrFmDlg;

implementation

uses
	Egyeb, J_SQL, CmrUj, CmrBe, Kozos, CmrKiad;

{$R *.DFM}

procedure TCmrFmDlg.FormCreate(Sender: TObject);
begin
	Inherited FormCreate(Sender);
	EgyebDlg.SeTADOQueryDatabase(QueryUj3);
	kellujures	:= true;
	AddSzuromezoRovid( 'CM_KIDAT', 'CMRDAT' );
	AddSzuromezoRovid( 'CM_VIDAT', 'CMRDAT' );
	AddSzuromezoRovid( 'CM_DOKOD', 'CMRDAT' );
	AddSzuromezoRovid( 'CM_DONEV', 'CMRDAT' );
	AddSzuromezoRovid( 'CM_JARAT', 'CMRDAT' );
	AddSzuromezoRovid( 'CM_JAKOD', 'CMRDAT' );
	alapstr		:= 'SELECT * FROM CMRDAT ';
	FelTolto('�j CMR felvitele ', 'CMR adatok m�dos�t�sa ',
			'CMR lista', 'CM_CMKOD', alapstr, 'CMRDAT', QUERY_ADAT_TAG );
	kulcsmezo			:= 'CM_CMKOD';
	nevmezo				:= 'CM_CMKOD';
	width				:= DLG_WIDTH;
	height				:= DLG_HEIGHT;
	BitBtn7.Parent		:= PanelBottom;
end;

procedure TCmrFmDlg.ButtonKuldClick(Sender: TObject);
begin
//	ret_vnev	:= Query1.FieldByName('GK_RESZ').AsString;
	Inherited ButtonKuldClick(Sender);
end;

procedure TCmrFmDlg.ButtonTorClick(Sender: TObject);
begin
	if not voltalt then begin
		voltalt 	:= true;
		Exit;
	end;
	if NoticeKi( TORLOTEXT, NOT_QUESTION ) = 0 then begin
		Query_Run(Query2, 'DELETE from CMRDAT where CM_CMKOD  ='''+ Query1.FieldByName('CM_CMKOD').AsString+''' ');
		ModLocate(Query1, FOMEZO, Query1.FieldByName(FOMEZO).AsString );
	end;
	Label3.Caption	:= '('+IntToStr(Query1.RecordCount)+')';
	Label3.Update;
end;

procedure TCmrFmDlg.Adatlap(cim, kod : string);
begin
	if kod = '' then begin
		Application.CreateForm(TCmrUjDlg, AlForm);
	end else begin
		Application.CreateForm(TCmrbeDlg, AlForm);
	end;
	Inherited Adatlap(cim, kod );
end;

procedure TCmrFmDlg.BitBtn7Click(Sender: TObject);
begin
	// CMR-ek kiad�sa g�pkocsivezet�nek
	Application.CreateForm(TCmrKiadDlg, CmrKiadDlg);
	CmrKiadDlg.Tolto('CMR kiad�sa', Query1.FieldByName('CM_CSZAM').AsString);
	CmrKiadDlg.ShowModal;
	if CmrKiadDlg.ret_kod <> '' then begin
		ModLocate(Query1, FOMEZO, Query1.FieldByName(FOMEZO).AsString );
	end;
	CmrKiadDlg.Destroy;
end;

function TCmrFmDlg.KeresChange(mezo, szoveg : string) : string;
begin
	Result	:= szoveg;
	if UpperCase(mezo) = 'CM_CSZAM' then begin
{		if Length(szoveg) >= 4 then begin
//			Result := copy(EgyebDlg.EvSzam,2,3) + Format('%4.4d',[StrToIntDef(szoveg,0)]);
			Result := Format('%7.7d', [StrToIntDef(szoveg, 0)]);
		end;    }
	end;
end;
     
end.


