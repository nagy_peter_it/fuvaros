// ************************************************************************ //
// The types declared in this file were generated from data read from the
// WSDL File described below:
// WSDL     : P:\SRC\FUVAR_ADO\4.20\WSDL\Fleetboard_PosService.wsdl
//  >Import : P:\SRC\FUVAR_ADO\4.20\WSDL\Fleetboard_PosService.wsdl>0
// Encoding : UTF-8
// Version  : 1.0
// (2017.09.29. 15:03:25 - - $Rev: 56641 $)
// ************************************************************************ //

unit Fleetboard_PosService;

interface

uses InvokeRegistry, SOAPHTTPClient, Types, XSBuiltIns;

const
  IS_OPTN = $0001;
  IS_UNBD = $0002;
  IS_NLBL = $0004;
  IS_ATTR = $0010;
  IS_TEXT = $0020;
  IS_REF  = $0080;


type

  // ************************************************************************ //
  // The following types, referred to in the WSDL document are not being represented
  // in this file. They are either aliases[@] of other types represented or were referred
  // to but never[!] declared in the document. The types from the latter category
  // typically map to predefined/known XML or Embarcadero types; however, they could also 
  // indicate incorrect WSDL documents that failed to declare or import a schema type.
  // ************************************************************************ //
  // !:dateTime        - "http://www.w3.org/2001/XMLSchema"[Gbl]
  // !:double          - "http://www.w3.org/2001/XMLSchema"[Gbl]
  // !:short           - "http://www.w3.org/2001/XMLSchema"[Gbl]
  // !:boolean         - "http://www.w3.org/2001/XMLSchema"[Gbl]
  // !:long            - "http://www.w3.org/2001/XMLSchema"[Gbl]
  // !:float           - "http://www.w3.org/2001/XMLSchema"[Gbl]
  // !:int             - "http://www.w3.org/2001/XMLSchema"[Gbl]
  // !:string          - "http://www.w3.org/2001/XMLSchema"[Gbl]
  // !:integer         - "http://www.w3.org/2001/XMLSchema"[Gbl]

  AutoPosDef           = class;                 { "http://www.fleetboard.com/data"[GblElm] }
  TourEventUptimeMsg   = class;                 { "http://www.fleetboard.com/data"[GblElm] }
  ReasonDTO            = class;                 { "http://www.fleetboard.com/data"[GblElm] }
  TempLoggerMonitoringAlarm = class;            { "http://www.fleetboard.com/data"[GblElm] }
  VelocityAlarm        = class;                 { "http://www.fleetboard.com/data"[GblElm] }
  HumidityAlarm        = class;                 { "http://www.fleetboard.com/data"[GblElm] }
  BogieLoadAlarm       = class;                 { "http://www.fleetboard.com/data"[GblElm] }
  Ebs24NAlarm          = class;                 { "http://www.fleetboard.com/data"[GblElm] }
  MilageAlarm          = class;                 { "http://www.fleetboard.com/data"[GblElm] }
  Coordinate           = class;                 { "http://www.fleetboard.com/data"[GblCplx] }
  LocationDescription  = class;                 { "http://www.fleetboard.com/data"[GblElm] }
  FieldsDef            = class;                 { "http://www.fleetboard.com/data"[GblElm] }
  TrailerEventMsg      = class;                 { "http://www.fleetboard.com/data"[GblElm] }
  TmInfoDTO            = class;                 { "http://www.fleetboard.com/data"[GblElm] }
  ForeignRef           = class;                 { "http://www.fleetboard.com/data"[Cplx] }
  DriverStatusMsg      = class;                 { "http://www.fleetboard.com/data"[GblElm] }
  VehiclestatusMsg     = class;                 { "http://www.fleetboard.com/data"[GblElm] }
  RefID                = class;                 { "http://www.fleetboard.com/data"[Cplx] }
  TourstatusMsg        = class;                 { "http://www.fleetboard.com/data"[GblElm] }
  ReplystatusMsg       = class;                 { "http://www.fleetboard.com/data"[GblElm] }
  Body                 = class;                 { "http://www.fleetboard.com/data"[GblElm] }
  EventStatusMsg       = class;                 { "http://www.fleetboard.com/data"[GblElm] }
  OrderstatusMsg       = class;                 { "http://www.fleetboard.com/data"[GblElm] }
  InboxDataMsg         = class;                 { "http://www.fleetboard.com/data"[GblElm] }
  AreaMonitoring       = class;                 { "http://www.fleetboard.com/data"[Cplx] }
  AutoPos              = class;                 { "http://www.fleetboard.com/data"[GblElm] }
  SetpointData         = class;                 { "http://www.fleetboard.com/data"[GblElm] }
  TrailerPositionDTO   = class;                 { "http://www.fleetboard.com/data"[GblElm] }
  FormDefMsg           = class;                 { "http://www.fleetboard.com/data"[GblElm] }
  LastPositionState    = class;                 { "http://www.fleetboard.com/data"[GblCplx] }
  TPDate               = class;                 { "http://www.fleetboard.com/data"[GblCplx] }
  Position2            = class;                 { "http://www.fleetboard.com/data"[Cplx] }
  CacheMsg             = class;                 { "http://www.fleetboard.com/data"[GblElm] }
  GetLastPositionRequestType = class;           { "http://www.fleetboard.com/data"[GblCplx] }
  GetLastPositionRequest = class;               { "http://www.fleetboard.com/data"[GblElm] }
  GetLocationDescriptionResponseType = class;   { "http://www.fleetboard.com/data"[GblCplx] }
  GetLocationDescriptionResponse = class;       { "http://www.fleetboard.com/data"[GblElm] }
  GetLastPositionResponseType = class;          { "http://www.fleetboard.com/data"[GblCplx] }
  GetLastPositionResponse = class;              { "http://www.fleetboard.com/data"[GblElm] }
  GetReplyResponseType = class;                 { "http://www.fleetboard.com/data"[GblCplx] }
  GetReplyResponse     = class;                 { "http://www.fleetboard.com/data"[GblElm] }
  GetPosReportTraceResponseType = class;        { "http://www.fleetboard.com/data"[GblCplx] }
  GetPosReportTraceResponse = class;            { "http://www.fleetboard.com/data"[GblElm] }
  GetCurrentStateResponseType = class;          { "http://www.fleetboard.com/data"[GblCplx] }
  GetCurrentStateResponse = class;              { "http://www.fleetboard.com/data"[GblElm] }
  GetLocationDescriptionRequestType = class;    { "http://www.fleetboard.com/data"[GblCplx] }
  GetLocationDescriptionRequest = class;        { "http://www.fleetboard.com/data"[GblElm] }
  GetReplyRequestType  = class;                 { "http://www.fleetboard.com/data"[GblCplx] }
  GetReplyRequest      = class;                 { "http://www.fleetboard.com/data"[GblElm] }
  GetPosReportTraceRequestType = class;         { "http://www.fleetboard.com/data"[GblCplx] }
  GetPosReportTraceRequest = class;             { "http://www.fleetboard.com/data"[GblElm] }
  GetPosReportResponseType = class;             { "http://www.fleetboard.com/data"[GblCplx] }
  GetPosReportResponse = class;                 { "http://www.fleetboard.com/data"[GblElm] }
  GetPosReportRequestType = class;              { "http://www.fleetboard.com/data"[GblCplx] }
  GetPosReportRequest  = class;                 { "http://www.fleetboard.com/data"[GblElm] }
  GetCurrentStateRequestType = class;           { "http://www.fleetboard.com/data"[GblCplx] }
  GetCurrentStateRequest = class;               { "http://www.fleetboard.com/data"[GblElm] }
  Field                = class;                 { "http://www.fleetboard.com/data"[GblElm] }
  TracePosition        = class;                 { "http://www.fleetboard.com/data"[Cplx] }
  State                = class;                 { "http://www.fleetboard.com/data"[Cplx] }
  Header               = class;                 { "http://www.fleetboard.com/data"[GblElm] }
  Form                 = class;                 { "http://www.fleetboard.com/data"[GblElm] }
  VhcLayout            = class;                 { "http://www.fleetboard.com/data"[GblElm] }
  FormLayout           = class;                 { "http://www.fleetboard.com/data"[GblElm] }
  Behavior             = class;                 { "http://www.fleetboard.com/data"[GblElm] }
  Hidden               = class;                 { "http://www.fleetboard.com/data"[GblElm] }
  OrderMsg             = class;                 { "http://www.fleetboard.com/data"[GblElm] }
  VhcStatusLayout      = class;                 { "http://www.fleetboard.com/data"[GblElm] }
  Semantics            = class;                 { "http://www.fleetboard.com/data"[Cplx] }
  FieldDef             = class;                 { "http://www.fleetboard.com/data"[GblElm] }
  StatusDefMsg         = class;                 { "http://www.fleetboard.com/data"[GblElm] }
  ReeferStartStopContinousChanged = class;      { "http://www.fleetboard.com/data"[GblElm] }
  SetpointAlarm        = class;                 { "http://www.fleetboard.com/data"[GblElm] }
  ReeferDieselElectricChanged = class;          { "http://www.fleetboard.com/data"[GblElm] }
  FuelLevelAlarm       = class;                 { "http://www.fleetboard.com/data"[GblElm] }
  CurrentCompartmentDataList = class;           { "http://www.fleetboard.com/data"[GblElm] }
  Positions            = class;                 { "http://www.fleetboard.com/data"[Cplx] }
  TemperatureAlarm     = class;                 { "http://www.fleetboard.com/data"[GblElm] }
  PairingMsg           = class;                 { "http://www.fleetboard.com/data"[GblElm] }
  TirePressureAlarm    = class;                 { "http://www.fleetboard.com/data"[GblElm] }
  User                 = class;                 { "http://www.fleetboard.com/data"[GblElm] }
  Position             = class;                 { "http://www.fleetboard.com/data"[GblElm] }
  Msg                  = class;                 { "http://www.fleetboard.com/data"[GblElm] }
  WatchboxAlarm        = class;                 { "http://www.fleetboard.com/data"[GblElm] }
  UndervoltageAlarm    = class;                 { "http://www.fleetboard.com/data"[GblElm] }
  TrailerRegisterMsg   = class;                 { "http://www.fleetboard.com/data"[GblElm] }
  TrailerCouplingMsg   = class;                 { "http://www.fleetboard.com/data"[GblElm] }
  TrailerSyncMsg       = class;                 { "http://www.fleetboard.com/data"[GblElm] }

  {$SCOPEDENUMS ON}
  { "http://www.fleetboard.com/data"[GblSmpl] }
  serviceType = (VM, TM, ALL);

  { "http://www.fleetboard.com/data"[Smpl] }
  AdditionalState = (STATE_OF_VEHICLE, STATE_OF_TIMEMANAGEMENT, STATE_OF_ORDERMANAGEMENT);

  { "http://www.fleetboard.com/data"[GblSmpl] }
  telematicGroupIDType = (MB, AM, DISPOSITION);

  { "http://www.fleetboard.com/data"[GblSmpl] }
  CurrentReeferOperationModeDTO = (StartStop, Continuous);

  { "http://www.fleetboard.com/data"[GblSmpl] }
  SetpointSensorDTO = (Setpoint1, Setpoint2, Setpoint3);

  { "http://www.fleetboard.com/data"[GblSmpl] }
  CurrentPowerModeDTO = (Electric, Diesel, Unknown);

  { "http://www.fleetboard.com/data"[GblSmpl] }
  CurrentFuelLevelColorDTO = (red, yellow, green);

  { "http://www.fleetboard.com/data"[GblSmpl] }
  CompartmentOperationModeDTO = (On_, Off, Cooling, Heating, Defrost, Pretrip, Null, Invalid, Unknown, Diagnostic);

  { "http://www.fleetboard.com/data"[GblSmpl] }
  TemperatureSensorDTO = (TemperaturSensor1, TemperaturSensor2, TemperaturSensor3, TemperaturSensor4, TemperaturSensor5, TemperaturSensor6);

  { "http://www.fleetboard.com/data"[GblSmpl] }
  Device2VehicleConnectionStatusType = (FREE, REQUESTED, CONNECTED, REJECTED, UNAVAILABLE);

  { "http://www.fleetboard.com/data"[GblSmpl] }
  TirePositionDTO = (FrontLeftWheel, FrontRightWheel, MiddleLeftWheel, MiddleRightWheel, RearLeftWheel, RearRightWheel, SpareWheel);

  { "http://www.fleetboard.com/data"[GblSmpl] }
  WatchboxAlarmReasonDTO = (WatchboxEntry, WatchboxExit, WatchboxEntryTimeDifference, WatchboxExitTimeDifference, WatchboxMonitoringFinished);

  { "http://www.fleetboard.com/data"[GblSmpl] }
  BatteryConcernedDTO = (CTU1_Battery, CTU2_LIN_Battery, External_Battery, Boardnet_12V, Boardnet_24V);

  { "http://www.fleetboard.com/data"[GblSmpl] }
  TspAccountState = (UNREGISTERED, REGISTER_PENDING, REGISTER_ERROR, REGISTERED, UNREGISTER_PENDING, UNREGISTER_ERROR);

  { "http://www.fleetboard.com/data"[GblSmpl] }
  TrailerCoupled = (Coupled, Uncoupled, Unknown);

  { "http://www.fleetboard.com/data"[GblSmpl] }
  TrailerState = (ACTIVE, INACTIVE);

  {$SCOPEDENUMS OFF}

  ReeferOperationModeChanged = array of CurrentCompartmentDataList;   { "http://www.fleetboard.com/data"[GblElm] }


  // ************************************************************************ //
  // XML       : AutoPosDef, global, <element>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  AutoPosDef = class(TRemotable)
  private
    FAutoPos: AutoPos;
  public
    destructor Destroy; override;
  published
    property AutoPos: AutoPos  Index (IS_REF) read FAutoPos write FAutoPos;
  end;

  Array_Of_Msg = array of Msg;                  { "http://www.fleetboard.com/data"[GblUbnd] }
  Array_Of_Positions = array of Positions;      { "http://www.fleetboard.com/data"[Ubnd] }
  Array_Of_TmInfoDTO = array of TmInfoDTO;      { "http://www.fleetboard.com/data"[GblUbnd] }
  Array_Of_FieldDef = array of FieldDef;        { "http://www.fleetboard.com/data"[GblUbnd] }
  Array_Of_Position = array of Position2;       { "http://www.fleetboard.com/data"[Ubnd] }
  Array_Of_ForeignRef = array of ForeignRef;    { "http://www.fleetboard.com/data"[Ubnd] }
  Array_Of_LocationDescription = array of LocationDescription;   { "http://www.fleetboard.com/data"[GblUbnd] }
  Array_Of_Coordinate = array of Coordinate;    { "http://www.fleetboard.com/data"[GblUbnd] }


  // ************************************************************************ //
  // XML       : TourEventUptimeMsg, global, <element>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  TourEventUptimeMsg = class(TRemotable)
  private
    FTourEventType: SmallInt;
    FTourEventStatus: SmallInt;
  published
    property TourEventType:   SmallInt  read FTourEventType write FTourEventType;
    property TourEventStatus: SmallInt  read FTourEventStatus write FTourEventStatus;
  end;



  // ************************************************************************ //
  // XML       : ReasonDTO, global, <element>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  ReasonDTO = class(TRemotable)
  private
    Fstandard: Boolean;
    Fstandard_Specified: boolean;
    FstatusRequest: Boolean;
    FstatusRequest_Specified: boolean;
    FtagChangedAlarm: Boolean;
    FtagChangedAlarm_Specified: boolean;
    FebsNotConnected: Boolean;
    FebsNotConnected_Specified: boolean;
    FbrakeLiningAlarm: Boolean;
    FbrakeLiningAlarm_Specified: boolean;
    FcouplingAlarm: Boolean;
    FcouplingAlarm_Specified: boolean;
    FdoorAlarm: Boolean;
    FdoorAlarm_Specified: boolean;
    FignitionAlarm: Boolean;
    FignitionAlarm_Specified: boolean;
    FreeferAlarm: Boolean;
    FreeferAlarm_Specified: boolean;
    FreeferAlarmQueueChanged: Boolean;
    FreeferAlarmQueueChanged_Specified: boolean;
    FreeferPresetListChanged: Boolean;
    FreeferPresetListChanged_Specified: boolean;
    FreeferCurrentPresetChanged: Boolean;
    FreeferCurrentPresetChanged_Specified: boolean;
    FtrailerStartStopAlarm: Boolean;
    FtrailerStartStopAlarm_Specified: boolean;
    FTemperatureAlarm: TemperatureAlarm;
    FTemperatureAlarm_Specified: boolean;
    FSetpointAlarm: SetpointAlarm;
    FSetpointAlarm_Specified: boolean;
    FEbs24NAlarm: Ebs24NAlarm;
    FEbs24NAlarm_Specified: boolean;
    FMilageAlarm: MilageAlarm;
    FMilageAlarm_Specified: boolean;
    FReeferStartStopContinousChanged: ReeferStartStopContinousChanged;
    FReeferStartStopContinousChanged_Specified: boolean;
    FReeferDieselElectricChanged: ReeferDieselElectricChanged;
    FReeferDieselElectricChanged_Specified: boolean;
    FReeferOperationModeChanged: ReeferOperationModeChanged;
    FReeferOperationModeChanged_Specified: boolean;
    FFuelLevelAlarm: FuelLevelAlarm;
    FFuelLevelAlarm_Specified: boolean;
    FTempLoggerMonitoringAlarm: TempLoggerMonitoringAlarm;
    FTempLoggerMonitoringAlarm_Specified: boolean;
    FTirePressureAlarm: TirePressureAlarm;
    FTirePressureAlarm_Specified: boolean;
    FUndervoltageAlarm: UndervoltageAlarm;
    FUndervoltageAlarm_Specified: boolean;
    FVelocityAlarm: VelocityAlarm;
    FVelocityAlarm_Specified: boolean;
    FWatchboxAlarm: WatchboxAlarm;
    FWatchboxAlarm_Specified: boolean;
    FBogieLoadAlarm: BogieLoadAlarm;
    FBogieLoadAlarm_Specified: boolean;
    FHumidityAlarm: HumidityAlarm;
    FHumidityAlarm_Specified: boolean;
    procedure Setstandard(Index: Integer; const ABoolean: Boolean);
    function  standard_Specified(Index: Integer): boolean;
    procedure SetstatusRequest(Index: Integer; const ABoolean: Boolean);
    function  statusRequest_Specified(Index: Integer): boolean;
    procedure SettagChangedAlarm(Index: Integer; const ABoolean: Boolean);
    function  tagChangedAlarm_Specified(Index: Integer): boolean;
    procedure SetebsNotConnected(Index: Integer; const ABoolean: Boolean);
    function  ebsNotConnected_Specified(Index: Integer): boolean;
    procedure SetbrakeLiningAlarm(Index: Integer; const ABoolean: Boolean);
    function  brakeLiningAlarm_Specified(Index: Integer): boolean;
    procedure SetcouplingAlarm(Index: Integer; const ABoolean: Boolean);
    function  couplingAlarm_Specified(Index: Integer): boolean;
    procedure SetdoorAlarm(Index: Integer; const ABoolean: Boolean);
    function  doorAlarm_Specified(Index: Integer): boolean;
    procedure SetignitionAlarm(Index: Integer; const ABoolean: Boolean);
    function  ignitionAlarm_Specified(Index: Integer): boolean;
    procedure SetreeferAlarm(Index: Integer; const ABoolean: Boolean);
    function  reeferAlarm_Specified(Index: Integer): boolean;
    procedure SetreeferAlarmQueueChanged(Index: Integer; const ABoolean: Boolean);
    function  reeferAlarmQueueChanged_Specified(Index: Integer): boolean;
    procedure SetreeferPresetListChanged(Index: Integer; const ABoolean: Boolean);
    function  reeferPresetListChanged_Specified(Index: Integer): boolean;
    procedure SetreeferCurrentPresetChanged(Index: Integer; const ABoolean: Boolean);
    function  reeferCurrentPresetChanged_Specified(Index: Integer): boolean;
    procedure SettrailerStartStopAlarm(Index: Integer; const ABoolean: Boolean);
    function  trailerStartStopAlarm_Specified(Index: Integer): boolean;
    procedure SetTemperatureAlarm(Index: Integer; const ATemperatureAlarm: TemperatureAlarm);
    function  TemperatureAlarm_Specified(Index: Integer): boolean;
    procedure SetSetpointAlarm(Index: Integer; const ASetpointAlarm: SetpointAlarm);
    function  SetpointAlarm_Specified(Index: Integer): boolean;
    procedure SetEbs24NAlarm(Index: Integer; const AEbs24NAlarm: Ebs24NAlarm);
    function  Ebs24NAlarm_Specified(Index: Integer): boolean;
    procedure SetMilageAlarm(Index: Integer; const AMilageAlarm: MilageAlarm);
    function  MilageAlarm_Specified(Index: Integer): boolean;
    procedure SetReeferStartStopContinousChanged(Index: Integer; const AReeferStartStopContinousChanged: ReeferStartStopContinousChanged);
    function  ReeferStartStopContinousChanged_Specified(Index: Integer): boolean;
    procedure SetReeferDieselElectricChanged(Index: Integer; const AReeferDieselElectricChanged: ReeferDieselElectricChanged);
    function  ReeferDieselElectricChanged_Specified(Index: Integer): boolean;
    procedure SetReeferOperationModeChanged(Index: Integer; const AReeferOperationModeChanged: ReeferOperationModeChanged);
    function  ReeferOperationModeChanged_Specified(Index: Integer): boolean;
    procedure SetFuelLevelAlarm(Index: Integer; const AFuelLevelAlarm: FuelLevelAlarm);
    function  FuelLevelAlarm_Specified(Index: Integer): boolean;
    procedure SetTempLoggerMonitoringAlarm(Index: Integer; const ATempLoggerMonitoringAlarm: TempLoggerMonitoringAlarm);
    function  TempLoggerMonitoringAlarm_Specified(Index: Integer): boolean;
    procedure SetTirePressureAlarm(Index: Integer; const ATirePressureAlarm: TirePressureAlarm);
    function  TirePressureAlarm_Specified(Index: Integer): boolean;
    procedure SetUndervoltageAlarm(Index: Integer; const AUndervoltageAlarm: UndervoltageAlarm);
    function  UndervoltageAlarm_Specified(Index: Integer): boolean;
    procedure SetVelocityAlarm(Index: Integer; const AVelocityAlarm: VelocityAlarm);
    function  VelocityAlarm_Specified(Index: Integer): boolean;
    procedure SetWatchboxAlarm(Index: Integer; const AWatchboxAlarm: WatchboxAlarm);
    function  WatchboxAlarm_Specified(Index: Integer): boolean;
    procedure SetBogieLoadAlarm(Index: Integer; const ABogieLoadAlarm: BogieLoadAlarm);
    function  BogieLoadAlarm_Specified(Index: Integer): boolean;
    procedure SetHumidityAlarm(Index: Integer; const AHumidityAlarm: HumidityAlarm);
    function  HumidityAlarm_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property standard:                        Boolean                          Index (IS_OPTN) read Fstandard write Setstandard stored standard_Specified;
    property statusRequest:                   Boolean                          Index (IS_OPTN) read FstatusRequest write SetstatusRequest stored statusRequest_Specified;
    property tagChangedAlarm:                 Boolean                          Index (IS_OPTN) read FtagChangedAlarm write SettagChangedAlarm stored tagChangedAlarm_Specified;
    property ebsNotConnected:                 Boolean                          Index (IS_OPTN) read FebsNotConnected write SetebsNotConnected stored ebsNotConnected_Specified;
    property brakeLiningAlarm:                Boolean                          Index (IS_OPTN) read FbrakeLiningAlarm write SetbrakeLiningAlarm stored brakeLiningAlarm_Specified;
    property couplingAlarm:                   Boolean                          Index (IS_OPTN) read FcouplingAlarm write SetcouplingAlarm stored couplingAlarm_Specified;
    property doorAlarm:                       Boolean                          Index (IS_OPTN) read FdoorAlarm write SetdoorAlarm stored doorAlarm_Specified;
    property ignitionAlarm:                   Boolean                          Index (IS_OPTN) read FignitionAlarm write SetignitionAlarm stored ignitionAlarm_Specified;
    property reeferAlarm:                     Boolean                          Index (IS_OPTN) read FreeferAlarm write SetreeferAlarm stored reeferAlarm_Specified;
    property reeferAlarmQueueChanged:         Boolean                          Index (IS_OPTN) read FreeferAlarmQueueChanged write SetreeferAlarmQueueChanged stored reeferAlarmQueueChanged_Specified;
    property reeferPresetListChanged:         Boolean                          Index (IS_OPTN) read FreeferPresetListChanged write SetreeferPresetListChanged stored reeferPresetListChanged_Specified;
    property reeferCurrentPresetChanged:      Boolean                          Index (IS_OPTN) read FreeferCurrentPresetChanged write SetreeferCurrentPresetChanged stored reeferCurrentPresetChanged_Specified;
    property trailerStartStopAlarm:           Boolean                          Index (IS_OPTN) read FtrailerStartStopAlarm write SettrailerStartStopAlarm stored trailerStartStopAlarm_Specified;
    property TemperatureAlarm:                TemperatureAlarm                 Index (IS_OPTN or IS_REF) read FTemperatureAlarm write SetTemperatureAlarm stored TemperatureAlarm_Specified;
    property SetpointAlarm:                   SetpointAlarm                    Index (IS_OPTN or IS_REF) read FSetpointAlarm write SetSetpointAlarm stored SetpointAlarm_Specified;
    property Ebs24NAlarm:                     Ebs24NAlarm                      Index (IS_OPTN or IS_REF) read FEbs24NAlarm write SetEbs24NAlarm stored Ebs24NAlarm_Specified;
    property MilageAlarm:                     MilageAlarm                      Index (IS_OPTN or IS_REF) read FMilageAlarm write SetMilageAlarm stored MilageAlarm_Specified;
    property ReeferStartStopContinousChanged: ReeferStartStopContinousChanged  Index (IS_OPTN or IS_REF) read FReeferStartStopContinousChanged write SetReeferStartStopContinousChanged stored ReeferStartStopContinousChanged_Specified;
    property ReeferDieselElectricChanged:     ReeferDieselElectricChanged      Index (IS_OPTN or IS_REF) read FReeferDieselElectricChanged write SetReeferDieselElectricChanged stored ReeferDieselElectricChanged_Specified;
    property ReeferOperationModeChanged:      ReeferOperationModeChanged       Index (IS_OPTN or IS_REF) read FReeferOperationModeChanged write SetReeferOperationModeChanged stored ReeferOperationModeChanged_Specified;
    property FuelLevelAlarm:                  FuelLevelAlarm                   Index (IS_OPTN or IS_REF) read FFuelLevelAlarm write SetFuelLevelAlarm stored FuelLevelAlarm_Specified;
    property TempLoggerMonitoringAlarm:       TempLoggerMonitoringAlarm        Index (IS_OPTN or IS_REF) read FTempLoggerMonitoringAlarm write SetTempLoggerMonitoringAlarm stored TempLoggerMonitoringAlarm_Specified;
    property TirePressureAlarm:               TirePressureAlarm                Index (IS_OPTN or IS_REF) read FTirePressureAlarm write SetTirePressureAlarm stored TirePressureAlarm_Specified;
    property UndervoltageAlarm:               UndervoltageAlarm                Index (IS_OPTN or IS_REF) read FUndervoltageAlarm write SetUndervoltageAlarm stored UndervoltageAlarm_Specified;
    property VelocityAlarm:                   VelocityAlarm                    Index (IS_OPTN or IS_REF) read FVelocityAlarm write SetVelocityAlarm stored VelocityAlarm_Specified;
    property WatchboxAlarm:                   WatchboxAlarm                    Index (IS_OPTN or IS_REF) read FWatchboxAlarm write SetWatchboxAlarm stored WatchboxAlarm_Specified;
    property BogieLoadAlarm:                  BogieLoadAlarm                   Index (IS_OPTN or IS_REF) read FBogieLoadAlarm write SetBogieLoadAlarm stored BogieLoadAlarm_Specified;
    property HumidityAlarm:                   HumidityAlarm                    Index (IS_OPTN or IS_REF) read FHumidityAlarm write SetHumidityAlarm stored HumidityAlarm_Specified;
  end;



  // ************************************************************************ //
  // XML       : TempLoggerMonitoringAlarm, global, <element>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  TempLoggerMonitoringAlarm = class(TRemotable)
  private
    FisAllClear: Boolean;
    FisAllClear_Specified: boolean;
    procedure SetisAllClear(Index: Integer; const ABoolean: Boolean);
    function  isAllClear_Specified(Index: Integer): boolean;
  published
    property isAllClear: Boolean  Index (IS_OPTN) read FisAllClear write SetisAllClear stored isAllClear_Specified;
  end;

  fleetidType     =  type Int64;      { "http://www.fleetboard.com/data"[GblSmpl] }
  Array_Of_long = array of Int64;               { "http://www.w3.org/2001/XMLSchema"[GblUbnd] }


  // ************************************************************************ //
  // XML       : VelocityAlarm, global, <element>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  VelocityAlarm = class(TRemotable)
  private
    FcurrentVelocity: Int64;
    FcurrentVelocity_Specified: boolean;
    Fthreshold: Int64;
    Fthreshold_Specified: boolean;
    procedure SetcurrentVelocity(Index: Integer; const AInt64: Int64);
    function  currentVelocity_Specified(Index: Integer): boolean;
    procedure Setthreshold(Index: Integer; const AInt64: Int64);
    function  threshold_Specified(Index: Integer): boolean;
  published
    property currentVelocity: Int64  Index (IS_OPTN) read FcurrentVelocity write SetcurrentVelocity stored currentVelocity_Specified;
    property threshold:       Int64  Index (IS_OPTN) read Fthreshold write Setthreshold stored threshold_Specified;
  end;



  // ************************************************************************ //
  // XML       : HumidityAlarm, global, <element>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  HumidityAlarm = class(TRemotable)
  private
    FcurrentHumidity: Int64;
    FcurrentHumidity_Specified: boolean;
    FminThreshold: Int64;
    FminThreshold_Specified: boolean;
    FmaxThreshold: Int64;
    FmaxThreshold_Specified: boolean;
    procedure SetcurrentHumidity(Index: Integer; const AInt64: Int64);
    function  currentHumidity_Specified(Index: Integer): boolean;
    procedure SetminThreshold(Index: Integer; const AInt64: Int64);
    function  minThreshold_Specified(Index: Integer): boolean;
    procedure SetmaxThreshold(Index: Integer; const AInt64: Int64);
    function  maxThreshold_Specified(Index: Integer): boolean;
  published
    property currentHumidity: Int64  Index (IS_OPTN) read FcurrentHumidity write SetcurrentHumidity stored currentHumidity_Specified;
    property minThreshold:    Int64  Index (IS_OPTN) read FminThreshold write SetminThreshold stored minThreshold_Specified;
    property maxThreshold:    Int64  Index (IS_OPTN) read FmaxThreshold write SetmaxThreshold stored maxThreshold_Specified;
  end;



  // ************************************************************************ //
  // XML       : BogieLoadAlarm, global, <element>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  BogieLoadAlarm = class(TRemotable)
  private
    FcurrentBogieLoad: Double;
    FcurrentBogieLoad_Specified: boolean;
    Fthreshold: Int64;
    Fthreshold_Specified: boolean;
    procedure SetcurrentBogieLoad(Index: Integer; const ADouble: Double);
    function  currentBogieLoad_Specified(Index: Integer): boolean;
    procedure Setthreshold(Index: Integer; const AInt64: Int64);
    function  threshold_Specified(Index: Integer): boolean;
  published
    property currentBogieLoad: Double  Index (IS_OPTN) read FcurrentBogieLoad write SetcurrentBogieLoad stored currentBogieLoad_Specified;
    property threshold:        Int64   Index (IS_OPTN) read Fthreshold write Setthreshold stored threshold_Specified;
  end;



  // ************************************************************************ //
  // XML       : Ebs24NAlarm, global, <element>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  Ebs24NAlarm = class(TRemotable)
  private
    Febs24NDelta: Int64;
    Febs24NDelta_Specified: boolean;
    procedure Setebs24NDelta(Index: Integer; const AInt64: Int64);
    function  ebs24NDelta_Specified(Index: Integer): boolean;
  published
    property ebs24NDelta: Int64  Index (IS_OPTN) read Febs24NDelta write Setebs24NDelta stored ebs24NDelta_Specified;
  end;



  // ************************************************************************ //
  // XML       : MilageAlarm, global, <element>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  MilageAlarm = class(TRemotable)
  private
    FcurrentMileage: Double;
    FcurrentMileage_Specified: boolean;
    Fthreshold: Int64;
    Fthreshold_Specified: boolean;
    procedure SetcurrentMileage(Index: Integer; const ADouble: Double);
    function  currentMileage_Specified(Index: Integer): boolean;
    procedure Setthreshold(Index: Integer; const AInt64: Int64);
    function  threshold_Specified(Index: Integer): boolean;
  published
    property currentMileage: Double  Index (IS_OPTN) read FcurrentMileage write SetcurrentMileage stored currentMileage_Specified;
    property threshold:      Int64   Index (IS_OPTN) read Fthreshold write Setthreshold stored threshold_Specified;
  end;

  Array_Of_AdditionalState = array of AdditionalState;   { "http://www.fleetboard.com/data"[Ubnd] }


  // ************************************************************************ //
  // XML       : Coordinate, global, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  Coordinate = class(TRemotable)
  private
    Fid: Integer;
    FLongitude: Single;
    FLatitude: Single;
  published
    property id:        Integer  read Fid write Fid;
    property Longitude: Single   read FLongitude write FLongitude;
    property Latitude:  Single   read FLatitude write FLatitude;
  end;



  // ************************************************************************ //
  // XML       : LocationDescription, global, <element>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  LocationDescription = class(TRemotable)
  private
    Fid: Integer;
    FDistance: Single;
    FDistance_Specified: boolean;
    FLongitude: Single;
    FLongitude_Specified: boolean;
    FLatitude: Single;
    FLatitude_Specified: boolean;
    FLabel_: string;
    FLabel__Specified: boolean;
    FCountry: string;
    FCountry_Specified: boolean;
    FState: string;
    FState_Specified: boolean;
    FCounty: string;
    FCounty_Specified: boolean;
    FCity: string;
    FCity_Specified: boolean;
    FDistrict: string;
    FDistrict_Specified: boolean;
    FStreet: string;
    FStreet_Specified: boolean;
    FHouseNumber: string;
    FHouseNumber_Specified: boolean;
    FPostalCode: string;
    FPostalCode_Specified: boolean;
    procedure SetDistance(Index: Integer; const ASingle: Single);
    function  Distance_Specified(Index: Integer): boolean;
    procedure SetLongitude(Index: Integer; const ASingle: Single);
    function  Longitude_Specified(Index: Integer): boolean;
    procedure SetLatitude(Index: Integer; const ASingle: Single);
    function  Latitude_Specified(Index: Integer): boolean;
    procedure SetLabel_(Index: Integer; const Astring: string);
    function  Label__Specified(Index: Integer): boolean;
    procedure SetCountry(Index: Integer; const Astring: string);
    function  Country_Specified(Index: Integer): boolean;
    procedure SetState(Index: Integer; const Astring: string);
    function  State_Specified(Index: Integer): boolean;
    procedure SetCounty(Index: Integer; const Astring: string);
    function  County_Specified(Index: Integer): boolean;
    procedure SetCity(Index: Integer; const Astring: string);
    function  City_Specified(Index: Integer): boolean;
    procedure SetDistrict(Index: Integer; const Astring: string);
    function  District_Specified(Index: Integer): boolean;
    procedure SetStreet(Index: Integer; const Astring: string);
    function  Street_Specified(Index: Integer): boolean;
    procedure SetHouseNumber(Index: Integer; const Astring: string);
    function  HouseNumber_Specified(Index: Integer): boolean;
    procedure SetPostalCode(Index: Integer; const Astring: string);
    function  PostalCode_Specified(Index: Integer): boolean;
  published
    property id:          Integer  read Fid write Fid;
    property Distance:    Single   Index (IS_OPTN) read FDistance write SetDistance stored Distance_Specified;
    property Longitude:   Single   Index (IS_OPTN) read FLongitude write SetLongitude stored Longitude_Specified;
    property Latitude:    Single   Index (IS_OPTN) read FLatitude write SetLatitude stored Latitude_Specified;
    property Label_:      string   Index (IS_OPTN) read FLabel_ write SetLabel_ stored Label__Specified;
    property Country:     string   Index (IS_OPTN) read FCountry write SetCountry stored Country_Specified;
    property State:       string   Index (IS_OPTN) read FState write SetState stored State_Specified;
    property County:      string   Index (IS_OPTN) read FCounty write SetCounty stored County_Specified;
    property City:        string   Index (IS_OPTN) read FCity write SetCity stored City_Specified;
    property District:    string   Index (IS_OPTN) read FDistrict write SetDistrict stored District_Specified;
    property Street:      string   Index (IS_OPTN) read FStreet write SetStreet stored Street_Specified;
    property HouseNumber: string   Index (IS_OPTN) read FHouseNumber write SetHouseNumber stored HouseNumber_Specified;
    property PostalCode:  string   Index (IS_OPTN) read FPostalCode write SetPostalCode stored PostalCode_Specified;
  end;



  // ************************************************************************ //
  // XML       : FieldsDef, global, <element>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  FieldsDef = class(TRemotable)
  private
    FFieldDef: Array_Of_FieldDef;
    FFieldDef_Specified: boolean;
    FOrderInfo: string;
    FOrderInfo_Specified: boolean;
    procedure SetFieldDef(Index: Integer; const AArray_Of_FieldDef: Array_Of_FieldDef);
    function  FieldDef_Specified(Index: Integer): boolean;
    procedure SetOrderInfo(Index: Integer; const Astring: string);
    function  OrderInfo_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property FieldDef:  Array_Of_FieldDef  Index (IS_OPTN or IS_UNBD or IS_REF) read FFieldDef write SetFieldDef stored FieldDef_Specified;
    property OrderInfo: string             Index (IS_OPTN) read FOrderInfo write SetOrderInfo stored OrderInfo_Specified;
  end;



  // ************************************************************************ //
  // XML       : TrailerEventMsg, global, <element>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  TrailerEventMsg = class(TRemotable)
  private
    Fid: string;
    FtrailerId: string;
    FvehicleId: Int64;
    FvehicleId_Specified: boolean;
    FdriverNameId: Int64;
    FdriverNameId_Specified: boolean;
    Fcoupled: Boolean;
    Fcoupled_Specified: boolean;
    FignitionOn: Boolean;
    FignitionOn_Specified: boolean;
    FinMotion: Boolean;
    FinMotion_Specified: boolean;
    FReasonDTO: ReasonDTO;
    FReasonDTO_Specified: boolean;
    FTrailerPositionDTO: TrailerPositionDTO;
    FTrailerPositionDTO_Specified: boolean;
    FdeviceTime: TXSDateTime;
    FreceiveTime: TXSDateTime;
    FreceiveTime_Specified: boolean;
    FisDoor1Open: Boolean;
    FisDoor1Open_Specified: boolean;
    FisDoor2Open: Boolean;
    FisDoor2Open_Specified: boolean;
    FisDoor3Open: Boolean;
    FisDoor3Open_Specified: boolean;
    FisDoor4Open: Boolean;
    FisDoor4Open_Specified: boolean;
    FTmInfoDTO: Array_Of_TmInfoDTO;
    FTmInfoDTO_Specified: boolean;
    procedure SetvehicleId(Index: Integer; const AInt64: Int64);
    function  vehicleId_Specified(Index: Integer): boolean;
    procedure SetdriverNameId(Index: Integer; const AInt64: Int64);
    function  driverNameId_Specified(Index: Integer): boolean;
    procedure Setcoupled(Index: Integer; const ABoolean: Boolean);
    function  coupled_Specified(Index: Integer): boolean;
    procedure SetignitionOn(Index: Integer; const ABoolean: Boolean);
    function  ignitionOn_Specified(Index: Integer): boolean;
    procedure SetinMotion(Index: Integer; const ABoolean: Boolean);
    function  inMotion_Specified(Index: Integer): boolean;
    procedure SetReasonDTO(Index: Integer; const AReasonDTO: ReasonDTO);
    function  ReasonDTO_Specified(Index: Integer): boolean;
    procedure SetTrailerPositionDTO(Index: Integer; const ATrailerPositionDTO: TrailerPositionDTO);
    function  TrailerPositionDTO_Specified(Index: Integer): boolean;
    procedure SetreceiveTime(Index: Integer; const ATXSDateTime: TXSDateTime);
    function  receiveTime_Specified(Index: Integer): boolean;
    procedure SetisDoor1Open(Index: Integer; const ABoolean: Boolean);
    function  isDoor1Open_Specified(Index: Integer): boolean;
    procedure SetisDoor2Open(Index: Integer; const ABoolean: Boolean);
    function  isDoor2Open_Specified(Index: Integer): boolean;
    procedure SetisDoor3Open(Index: Integer; const ABoolean: Boolean);
    function  isDoor3Open_Specified(Index: Integer): boolean;
    procedure SetisDoor4Open(Index: Integer; const ABoolean: Boolean);
    function  isDoor4Open_Specified(Index: Integer): boolean;
    procedure SetTmInfoDTO(Index: Integer; const AArray_Of_TmInfoDTO: Array_Of_TmInfoDTO);
    function  TmInfoDTO_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property id:                 string              read Fid write Fid;
    property trailerId:          string              read FtrailerId write FtrailerId;
    property vehicleId:          Int64               Index (IS_OPTN) read FvehicleId write SetvehicleId stored vehicleId_Specified;
    property driverNameId:       Int64               Index (IS_OPTN) read FdriverNameId write SetdriverNameId stored driverNameId_Specified;
    property coupled:            Boolean             Index (IS_OPTN) read Fcoupled write Setcoupled stored coupled_Specified;
    property ignitionOn:         Boolean             Index (IS_OPTN) read FignitionOn write SetignitionOn stored ignitionOn_Specified;
    property inMotion:           Boolean             Index (IS_OPTN) read FinMotion write SetinMotion stored inMotion_Specified;
    property ReasonDTO:          ReasonDTO           Index (IS_OPTN or IS_REF) read FReasonDTO write SetReasonDTO stored ReasonDTO_Specified;
    property TrailerPositionDTO: TrailerPositionDTO  Index (IS_OPTN or IS_REF) read FTrailerPositionDTO write SetTrailerPositionDTO stored TrailerPositionDTO_Specified;
    property deviceTime:         TXSDateTime         read FdeviceTime write FdeviceTime;
    property receiveTime:        TXSDateTime         Index (IS_OPTN) read FreceiveTime write SetreceiveTime stored receiveTime_Specified;
    property isDoor1Open:        Boolean             Index (IS_OPTN) read FisDoor1Open write SetisDoor1Open stored isDoor1Open_Specified;
    property isDoor2Open:        Boolean             Index (IS_OPTN) read FisDoor2Open write SetisDoor2Open stored isDoor2Open_Specified;
    property isDoor3Open:        Boolean             Index (IS_OPTN) read FisDoor3Open write SetisDoor3Open stored isDoor3Open_Specified;
    property isDoor4Open:        Boolean             Index (IS_OPTN) read FisDoor4Open write SetisDoor4Open stored isDoor4Open_Specified;
    property TmInfoDTO:          Array_Of_TmInfoDTO  Index (IS_OPTN or IS_UNBD or IS_REF) read FTmInfoDTO write SetTmInfoDTO stored TmInfoDTO_Specified;
  end;



  // ************************************************************************ //
  // XML       : TmInfoDTO, global, <element>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  TmInfoDTO = class(TRemotable)
  private
    FtmObjectId: Int64;
    FdocumentInfo: string;
    FdocumentInfo_Specified: boolean;
    procedure SetdocumentInfo(Index: Integer; const Astring: string);
    function  documentInfo_Specified(Index: Integer): boolean;
  published
    property tmObjectId:   Int64   read FtmObjectId write FtmObjectId;
    property documentInfo: string  Index (IS_OPTN) read FdocumentInfo write SetdocumentInfo stored documentInfo_Specified;
  end;



  // ************************************************************************ //
  // XML       : ForeignRef, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  ForeignRef = class(TRemotable)
  private
    Fname_: string;
    Fname__Specified: boolean;
    Fvalue: string;
    Fvalue_Specified: boolean;
    procedure Setname_(Index: Integer; const Astring: string);
    function  name__Specified(Index: Integer): boolean;
    procedure Setvalue(Index: Integer; const Astring: string);
    function  value_Specified(Index: Integer): boolean;
  published
    property name_: string  Index (IS_ATTR or IS_OPTN) read Fname_ write Setname_ stored name__Specified;
    property value: string  Index (IS_ATTR or IS_OPTN) read Fvalue write Setvalue stored value_Specified;
  end;

  Times      = array of string;                 { "http://www.fleetboard.com/data"[Cplx] }


  // ************************************************************************ //
  // XML       : DriverStatusMsg, global, <element>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  DriverStatusMsg = class(TRemotable)
  private
    FStatusID: string;
    FInfo: string;
  published
    property StatusID: string  read FStatusID write FStatusID;
    property Info:     string  read FInfo write FInfo;
  end;



  // ************************************************************************ //
  // XML       : VehiclestatusMsg, global, <element>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  VehiclestatusMsg = class(TRemotable)
  private
    FStatus: string;
    FAddInformation: string;
    FForm: Form;
    FForm_Specified: boolean;
    procedure SetForm(Index: Integer; const AForm: Form);
    function  Form_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property Status:         string  read FStatus write FStatus;
    property AddInformation: string  read FAddInformation write FAddInformation;
    property Form:           Form    Index (IS_OPTN or IS_REF) read FForm write SetForm stored Form_Specified;
  end;



  // ************************************************************************ //
  // XML       : RefID, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  RefID = class(TRemotable)
  private
    FText: string;
    Fdoc: string;
    Fdoc_Specified: boolean;
    procedure Setdoc(Index: Integer; const Astring: string);
    function  doc_Specified(Index: Integer): boolean;
  published
    property Text: string  Index (IS_TEXT) read FText write FText;
    property doc:  string  Index (IS_ATTR or IS_OPTN) read Fdoc write Setdoc stored doc_Specified;
  end;



  // ************************************************************************ //
  // XML       : TourstatusMsg, global, <element>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  TourstatusMsg = class(TRemotable)
  private
    FTourID: string;
    FStatus: string;
    FAddInformation: string;
    FAddInformation_Specified: boolean;
    FForm: Form;
    FForm_Specified: boolean;
    procedure SetAddInformation(Index: Integer; const Astring: string);
    function  AddInformation_Specified(Index: Integer): boolean;
    procedure SetForm(Index: Integer; const AForm: Form);
    function  Form_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property TourID:         string  read FTourID write FTourID;
    property Status:         string  read FStatus write FStatus;
    property AddInformation: string  Index (IS_OPTN) read FAddInformation write SetAddInformation stored AddInformation_Specified;
    property Form:           Form    Index (IS_OPTN or IS_REF) read FForm write SetForm stored Form_Specified;
  end;



  // ************************************************************************ //
  // XML       : ReplystatusMsg, global, <element>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  ReplystatusMsg = class(TRemotable)
  private
    FMessageID: string;
    FStatus: string;
    FRefID: RefID;
    FRefID_Specified: boolean;
    procedure SetRefID(Index: Integer; const ARefID: RefID);
    function  RefID_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property MessageID: string  read FMessageID write FMessageID;
    property Status:    string  read FStatus write FStatus;
    property RefID:     RefID   Index (IS_OPTN) read FRefID write SetRefID stored RefID_Specified;
  end;

  TextConfMsg = array of string;                { "http://www.fleetboard.com/data"[GblElm] }


  // ************************************************************************ //
  // XML       : Body, global, <element>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  Body = class(TRemotable)
  private
    FFreetextMsg: string;
    FFreetextMsg_Specified: boolean;
    FOrderMsg: OrderMsg;
    FOrderMsg_Specified: boolean;
    FStatusDefMsg: StatusDefMsg;
    FStatusDefMsg_Specified: boolean;
    FFormDefMsg: FormDefMsg;
    FFormDefMsg_Specified: boolean;
    FAutoPosDef: AutoPosDef;
    FAutoPosDef_Specified: boolean;
    FVehiclestatusMsg: VehiclestatusMsg;
    FVehiclestatusMsg_Specified: boolean;
    FDriverStatusMsg: DriverStatusMsg;
    FDriverStatusMsg_Specified: boolean;
    FOrderstatusMsg: OrderstatusMsg;
    FOrderstatusMsg_Specified: boolean;
    FTourstatusMsg: TourstatusMsg;
    FTourstatusMsg_Specified: boolean;
    FReplystatusMsg: ReplystatusMsg;
    FReplystatusMsg_Specified: boolean;
    FSimcardChgMsg: string;
    FSimcardChgMsg_Specified: boolean;
    FHexMsg: string;
    FHexMsg_Specified: boolean;
    FCacheMsg: CacheMsg;
    FCacheMsg_Specified: boolean;
    FTextConfMsg: TextConfMsg;
    FTextConfMsg_Specified: boolean;
    FVehiclereplyMsg: string;
    FVehiclereplyMsg_Specified: boolean;
    FInboxDataMsg: InboxDataMsg;
    FInboxDataMsg_Specified: boolean;
    FEventStatusMsg: EventStatusMsg;
    FEventStatusMsg_Specified: boolean;
    FPairingMsg: PairingMsg;
    FPairingMsg_Specified: boolean;
    FTrailerEventMsg: TrailerEventMsg;
    FTrailerEventMsg_Specified: boolean;
    FTrailerRegisterMsg: TrailerRegisterMsg;
    FTrailerRegisterMsg_Specified: boolean;
    FTrailerSyncMsg: TrailerSyncMsg;
    FTrailerSyncMsg_Specified: boolean;
    FTourEventUptimeMsg: TourEventUptimeMsg;
    FTourEventUptimeMsg_Specified: boolean;
    FTrailerCouplingMsg: TrailerCouplingMsg;
    FTrailerCouplingMsg_Specified: boolean;
    procedure SetFreetextMsg(Index: Integer; const Astring: string);
    function  FreetextMsg_Specified(Index: Integer): boolean;
    procedure SetOrderMsg(Index: Integer; const AOrderMsg: OrderMsg);
    function  OrderMsg_Specified(Index: Integer): boolean;
    procedure SetStatusDefMsg(Index: Integer; const AStatusDefMsg: StatusDefMsg);
    function  StatusDefMsg_Specified(Index: Integer): boolean;
    procedure SetFormDefMsg(Index: Integer; const AFormDefMsg: FormDefMsg);
    function  FormDefMsg_Specified(Index: Integer): boolean;
    procedure SetAutoPosDef(Index: Integer; const AAutoPosDef: AutoPosDef);
    function  AutoPosDef_Specified(Index: Integer): boolean;
    procedure SetVehiclestatusMsg(Index: Integer; const AVehiclestatusMsg: VehiclestatusMsg);
    function  VehiclestatusMsg_Specified(Index: Integer): boolean;
    procedure SetDriverStatusMsg(Index: Integer; const ADriverStatusMsg: DriverStatusMsg);
    function  DriverStatusMsg_Specified(Index: Integer): boolean;
    procedure SetOrderstatusMsg(Index: Integer; const AOrderstatusMsg: OrderstatusMsg);
    function  OrderstatusMsg_Specified(Index: Integer): boolean;
    procedure SetTourstatusMsg(Index: Integer; const ATourstatusMsg: TourstatusMsg);
    function  TourstatusMsg_Specified(Index: Integer): boolean;
    procedure SetReplystatusMsg(Index: Integer; const AReplystatusMsg: ReplystatusMsg);
    function  ReplystatusMsg_Specified(Index: Integer): boolean;
    procedure SetSimcardChgMsg(Index: Integer; const Astring: string);
    function  SimcardChgMsg_Specified(Index: Integer): boolean;
    procedure SetHexMsg(Index: Integer; const Astring: string);
    function  HexMsg_Specified(Index: Integer): boolean;
    procedure SetCacheMsg(Index: Integer; const ACacheMsg: CacheMsg);
    function  CacheMsg_Specified(Index: Integer): boolean;
    procedure SetTextConfMsg(Index: Integer; const ATextConfMsg: TextConfMsg);
    function  TextConfMsg_Specified(Index: Integer): boolean;
    procedure SetVehiclereplyMsg(Index: Integer; const Astring: string);
    function  VehiclereplyMsg_Specified(Index: Integer): boolean;
    procedure SetInboxDataMsg(Index: Integer; const AInboxDataMsg: InboxDataMsg);
    function  InboxDataMsg_Specified(Index: Integer): boolean;
    procedure SetEventStatusMsg(Index: Integer; const AEventStatusMsg: EventStatusMsg);
    function  EventStatusMsg_Specified(Index: Integer): boolean;
    procedure SetPairingMsg(Index: Integer; const APairingMsg: PairingMsg);
    function  PairingMsg_Specified(Index: Integer): boolean;
    procedure SetTrailerEventMsg(Index: Integer; const ATrailerEventMsg: TrailerEventMsg);
    function  TrailerEventMsg_Specified(Index: Integer): boolean;
    procedure SetTrailerRegisterMsg(Index: Integer; const ATrailerRegisterMsg: TrailerRegisterMsg);
    function  TrailerRegisterMsg_Specified(Index: Integer): boolean;
    procedure SetTrailerSyncMsg(Index: Integer; const ATrailerSyncMsg: TrailerSyncMsg);
    function  TrailerSyncMsg_Specified(Index: Integer): boolean;
    procedure SetTourEventUptimeMsg(Index: Integer; const ATourEventUptimeMsg: TourEventUptimeMsg);
    function  TourEventUptimeMsg_Specified(Index: Integer): boolean;
    procedure SetTrailerCouplingMsg(Index: Integer; const ATrailerCouplingMsg: TrailerCouplingMsg);
    function  TrailerCouplingMsg_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property FreetextMsg:        string              Index (IS_OPTN) read FFreetextMsg write SetFreetextMsg stored FreetextMsg_Specified;
    property OrderMsg:           OrderMsg            Index (IS_OPTN or IS_REF) read FOrderMsg write SetOrderMsg stored OrderMsg_Specified;
    property StatusDefMsg:       StatusDefMsg        Index (IS_OPTN or IS_REF) read FStatusDefMsg write SetStatusDefMsg stored StatusDefMsg_Specified;
    property FormDefMsg:         FormDefMsg          Index (IS_OPTN or IS_REF) read FFormDefMsg write SetFormDefMsg stored FormDefMsg_Specified;
    property AutoPosDef:         AutoPosDef          Index (IS_OPTN or IS_REF) read FAutoPosDef write SetAutoPosDef stored AutoPosDef_Specified;
    property VehiclestatusMsg:   VehiclestatusMsg    Index (IS_OPTN or IS_REF) read FVehiclestatusMsg write SetVehiclestatusMsg stored VehiclestatusMsg_Specified;
    property DriverStatusMsg:    DriverStatusMsg     Index (IS_OPTN or IS_REF) read FDriverStatusMsg write SetDriverStatusMsg stored DriverStatusMsg_Specified;
    property OrderstatusMsg:     OrderstatusMsg      Index (IS_OPTN or IS_REF) read FOrderstatusMsg write SetOrderstatusMsg stored OrderstatusMsg_Specified;
    property TourstatusMsg:      TourstatusMsg       Index (IS_OPTN or IS_REF) read FTourstatusMsg write SetTourstatusMsg stored TourstatusMsg_Specified;
    property ReplystatusMsg:     ReplystatusMsg      Index (IS_OPTN or IS_REF) read FReplystatusMsg write SetReplystatusMsg stored ReplystatusMsg_Specified;
    property SimcardChgMsg:      string              Index (IS_OPTN) read FSimcardChgMsg write SetSimcardChgMsg stored SimcardChgMsg_Specified;
    property HexMsg:             string              Index (IS_OPTN) read FHexMsg write SetHexMsg stored HexMsg_Specified;
    property CacheMsg:           CacheMsg            Index (IS_OPTN or IS_REF) read FCacheMsg write SetCacheMsg stored CacheMsg_Specified;
    property TextConfMsg:        TextConfMsg         Index (IS_OPTN or IS_REF) read FTextConfMsg write SetTextConfMsg stored TextConfMsg_Specified;
    property VehiclereplyMsg:    string              Index (IS_OPTN) read FVehiclereplyMsg write SetVehiclereplyMsg stored VehiclereplyMsg_Specified;
    property InboxDataMsg:       InboxDataMsg        Index (IS_OPTN or IS_REF) read FInboxDataMsg write SetInboxDataMsg stored InboxDataMsg_Specified;
    property EventStatusMsg:     EventStatusMsg      Index (IS_OPTN or IS_REF) read FEventStatusMsg write SetEventStatusMsg stored EventStatusMsg_Specified;
    property PairingMsg:         PairingMsg          Index (IS_OPTN or IS_REF) read FPairingMsg write SetPairingMsg stored PairingMsg_Specified;
    property TrailerEventMsg:    TrailerEventMsg     Index (IS_OPTN or IS_REF) read FTrailerEventMsg write SetTrailerEventMsg stored TrailerEventMsg_Specified;
    property TrailerRegisterMsg: TrailerRegisterMsg  Index (IS_OPTN or IS_REF) read FTrailerRegisterMsg write SetTrailerRegisterMsg stored TrailerRegisterMsg_Specified;
    property TrailerSyncMsg:     TrailerSyncMsg      Index (IS_OPTN or IS_REF) read FTrailerSyncMsg write SetTrailerSyncMsg stored TrailerSyncMsg_Specified;
    property TourEventUptimeMsg: TourEventUptimeMsg  Index (IS_OPTN or IS_REF) read FTourEventUptimeMsg write SetTourEventUptimeMsg stored TourEventUptimeMsg_Specified;
    property TrailerCouplingMsg: TrailerCouplingMsg  Index (IS_OPTN or IS_REF) read FTrailerCouplingMsg write SetTrailerCouplingMsg stored TrailerCouplingMsg_Specified;
  end;



  // ************************************************************************ //
  // XML       : EventStatusMsg, global, <element>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  EventStatusMsg = class(TRemotable)
  private
    FStatus: string;
    FAddInformation: string;
    FAddInformation_Specified: boolean;
    FForm: Form;
    FForm_Specified: boolean;
    procedure SetAddInformation(Index: Integer; const Astring: string);
    function  AddInformation_Specified(Index: Integer): boolean;
    procedure SetForm(Index: Integer; const AForm: Form);
    function  Form_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property Status:         string  read FStatus write FStatus;
    property AddInformation: string  Index (IS_OPTN) read FAddInformation write SetAddInformation stored AddInformation_Specified;
    property Form:           Form    Index (IS_OPTN or IS_REF) read FForm write SetForm stored Form_Specified;
  end;



  // ************************************************************************ //
  // XML       : OrderstatusMsg, global, <element>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  OrderstatusMsg = class(TRemotable)
  private
    FOrderID: string;
    FStatus: string;
    FAddInformation: string;
    FAddInformation_Specified: boolean;
    FForm: Form;
    FForm_Specified: boolean;
    procedure SetAddInformation(Index: Integer; const Astring: string);
    function  AddInformation_Specified(Index: Integer): boolean;
    procedure SetForm(Index: Integer; const AForm: Form);
    function  Form_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property OrderID:        string  read FOrderID write FOrderID;
    property Status:         string  read FStatus write FStatus;
    property AddInformation: string  Index (IS_OPTN) read FAddInformation write SetAddInformation stored AddInformation_Specified;
    property Form:           Form    Index (IS_OPTN or IS_REF) read FForm write SetForm stored Form_Specified;
  end;



  // ************************************************************************ //
  // XML       : InboxDataMsg, global, <element>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  InboxDataMsg = class(TRemotable)
  private
    FID: string;
    FTrailerName: string;
    FTrailerName_Specified: boolean;
    FMessageType: string;
    FMessage_: string;
    FAreaMonitoring: AreaMonitoring;
    FAreaMonitoring_Specified: boolean;
    procedure SetTrailerName(Index: Integer; const Astring: string);
    function  TrailerName_Specified(Index: Integer): boolean;
    procedure SetAreaMonitoring(Index: Integer; const AAreaMonitoring: AreaMonitoring);
    function  AreaMonitoring_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property ID:             string          read FID write FID;
    property TrailerName:    string          Index (IS_OPTN) read FTrailerName write SetTrailerName stored TrailerName_Specified;
    property MessageType:    string          read FMessageType write FMessageType;
    property Message_:       string          read FMessage_ write FMessage_;
    property AreaMonitoring: AreaMonitoring  Index (IS_OPTN) read FAreaMonitoring write SetAreaMonitoring stored AreaMonitoring_Specified;
  end;



  // ************************************************************************ //
  // XML       : AreaMonitoring, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  AreaMonitoring = class(TRemotable)
  private
    FAreaId: Int64;
    FAreaName: string;
  published
    property AreaId:   Int64   read FAreaId write FAreaId;
    property AreaName: string  read FAreaName write FAreaName;
  end;



  // ************************************************************************ //
  // XML       : AutoPos, global, <element>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  AutoPos = class(TRemotable)
  private
    FInterval: string;
    FInterval_Specified: boolean;
    FDistance: string;
    FDistance_Specified: boolean;
    FTimes: Times;
    FTimes_Specified: boolean;
    FZonetype: string;
    FZonetype_Specified: boolean;
    FRefPOIsID: string;
    FRefPOIsID_Specified: boolean;
    FAccmode: string;
    FAccmode_Specified: boolean;
    procedure SetInterval(Index: Integer; const Astring: string);
    function  Interval_Specified(Index: Integer): boolean;
    procedure SetDistance(Index: Integer; const Astring: string);
    function  Distance_Specified(Index: Integer): boolean;
    procedure SetTimes(Index: Integer; const ATimes: Times);
    function  Times_Specified(Index: Integer): boolean;
    procedure SetZonetype(Index: Integer; const Astring: string);
    function  Zonetype_Specified(Index: Integer): boolean;
    procedure SetRefPOIsID(Index: Integer; const Astring: string);
    function  RefPOIsID_Specified(Index: Integer): boolean;
    procedure SetAccmode(Index: Integer; const Astring: string);
    function  Accmode_Specified(Index: Integer): boolean;
  published
    property Interval:  string  Index (IS_OPTN) read FInterval write SetInterval stored Interval_Specified;
    property Distance:  string  Index (IS_OPTN) read FDistance write SetDistance stored Distance_Specified;
    property Times:     Times   Index (IS_OPTN) read FTimes write SetTimes stored Times_Specified;
    property Zonetype:  string  Index (IS_OPTN) read FZonetype write SetZonetype stored Zonetype_Specified;
    property RefPOIsID: string  Index (IS_OPTN) read FRefPOIsID write SetRefPOIsID stored RefPOIsID_Specified;
    property Accmode:   string  Index (IS_OPTN) read FAccmode write SetAccmode stored Accmode_Specified;
  end;



  // ************************************************************************ //
  // XML       : SetpointData, global, <element>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  SetpointData = class(TRemotable)
  private
    Ftemperature: Double;
    Ftemperature_Specified: boolean;
    FreturnAirTemperature: Double;
    FreturnAirTemperature_Specified: boolean;
    FsupplyTemperature: Double;
    FsupplyTemperature_Specified: boolean;
    FevaporatorCoilTemperature: Double;
    FevaporatorCoilTemperature_Specified: boolean;
    FcompartmentOperationMode: string;
    FcompartmentOperationMode_Specified: boolean;
    procedure Settemperature(Index: Integer; const ADouble: Double);
    function  temperature_Specified(Index: Integer): boolean;
    procedure SetreturnAirTemperature(Index: Integer; const ADouble: Double);
    function  returnAirTemperature_Specified(Index: Integer): boolean;
    procedure SetsupplyTemperature(Index: Integer; const ADouble: Double);
    function  supplyTemperature_Specified(Index: Integer): boolean;
    procedure SetevaporatorCoilTemperature(Index: Integer; const ADouble: Double);
    function  evaporatorCoilTemperature_Specified(Index: Integer): boolean;
    procedure SetcompartmentOperationMode(Index: Integer; const Astring: string);
    function  compartmentOperationMode_Specified(Index: Integer): boolean;
  published
    property temperature:               Double  Index (IS_OPTN) read Ftemperature write Settemperature stored temperature_Specified;
    property returnAirTemperature:      Double  Index (IS_OPTN) read FreturnAirTemperature write SetreturnAirTemperature stored returnAirTemperature_Specified;
    property supplyTemperature:         Double  Index (IS_OPTN) read FsupplyTemperature write SetsupplyTemperature stored supplyTemperature_Specified;
    property evaporatorCoilTemperature: Double  Index (IS_OPTN) read FevaporatorCoilTemperature write SetevaporatorCoilTemperature stored evaporatorCoilTemperature_Specified;
    property compartmentOperationMode:  string  Index (IS_OPTN) read FcompartmentOperationMode write SetcompartmentOperationMode stored compartmentOperationMode_Specified;
  end;



  // ************************************************************************ //
  // XML       : TrailerPositionDTO, global, <element>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  TrailerPositionDTO = class(TRemotable)
  private
    FpositionDataTime: TXSDateTime;
    FpositionDataTime_Specified: boolean;
    FgpsTime: TXSDateTime;
    FgpsTime_Specified: boolean;
    Flongitude: Double;
    Flongitude_Specified: boolean;
    Flatitude: Double;
    Flatitude_Specified: boolean;
    FgpsHeading: Int64;
    FgpsHeading_Specified: boolean;
    FgpsSpeed: Int64;
    FgpsSpeed_Specified: boolean;
    FgpsMileage: Double;
    FgpsMileage_Specified: boolean;
    FpositionText: string;
    FpositionText_Specified: boolean;
    procedure SetpositionDataTime(Index: Integer; const ATXSDateTime: TXSDateTime);
    function  positionDataTime_Specified(Index: Integer): boolean;
    procedure SetgpsTime(Index: Integer; const ATXSDateTime: TXSDateTime);
    function  gpsTime_Specified(Index: Integer): boolean;
    procedure Setlongitude(Index: Integer; const ADouble: Double);
    function  longitude_Specified(Index: Integer): boolean;
    procedure Setlatitude(Index: Integer; const ADouble: Double);
    function  latitude_Specified(Index: Integer): boolean;
    procedure SetgpsHeading(Index: Integer; const AInt64: Int64);
    function  gpsHeading_Specified(Index: Integer): boolean;
    procedure SetgpsSpeed(Index: Integer; const AInt64: Int64);
    function  gpsSpeed_Specified(Index: Integer): boolean;
    procedure SetgpsMileage(Index: Integer; const ADouble: Double);
    function  gpsMileage_Specified(Index: Integer): boolean;
    procedure SetpositionText(Index: Integer; const Astring: string);
    function  positionText_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property positionDataTime: TXSDateTime  Index (IS_OPTN) read FpositionDataTime write SetpositionDataTime stored positionDataTime_Specified;
    property gpsTime:          TXSDateTime  Index (IS_OPTN) read FgpsTime write SetgpsTime stored gpsTime_Specified;
    property longitude:        Double       Index (IS_OPTN) read Flongitude write Setlongitude stored longitude_Specified;
    property latitude:         Double       Index (IS_OPTN) read Flatitude write Setlatitude stored latitude_Specified;
    property gpsHeading:       Int64        Index (IS_OPTN) read FgpsHeading write SetgpsHeading stored gpsHeading_Specified;
    property gpsSpeed:         Int64        Index (IS_OPTN) read FgpsSpeed write SetgpsSpeed stored gpsSpeed_Specified;
    property gpsMileage:       Double       Index (IS_OPTN) read FgpsMileage write SetgpsMileage stored gpsMileage_Specified;
    property positionText:     string       Index (IS_OPTN) read FpositionText write SetpositionText stored positionText_Specified;
  end;



  // ************************************************************************ //
  // XML       : FormDefMsg, global, <element>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  FormDefMsg = class(TRemotable)
  private
    FName_: string;
    FFormDefID: Int64;
    FFieldsDef: FieldsDef;
    FFormDefInoID: string;
    FFormDefInoID_Specified: boolean;
    procedure SetFormDefInoID(Index: Integer; const Astring: string);
    function  FormDefInoID_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property Name_:        string     read FName_ write FName_;
    property FormDefID:    Int64      read FFormDefID write FFormDefID;
    property FieldsDef:    FieldsDef  Index (IS_REF) read FFieldsDef write FFieldsDef;
    property FormDefInoID: string     Index (IS_OPTN) read FFormDefInoID write SetFormDefInoID stored FormDefInoID_Specified;
  end;

  limitType       =  type Int64;      { "http://www.fleetboard.com/data"[GblSmpl] }
  sessionidType   =  type string;      { "http://www.fleetboard.com/data"[GblSmpl] }
  timestampType   =  type string;      { "http://www.fleetboard.com/data"[GblSmpl] }


  // ************************************************************************ //
  // XML       : LastPositionState, global, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  LastPositionState = class(TRemotable)
  private
    FText: SmallInt;
    Ftimestamp: timestampType;
    Ftimestamp_Specified: boolean;
    procedure Settimestamp(Index: Integer; const AtimestampType: timestampType);
    function  timestamp_Specified(Index: Integer): boolean;
  published
    property Text:      SmallInt       Index (IS_TEXT) read FText write FText;
    property timestamp: timestampType  Index (IS_ATTR or IS_OPTN) read Ftimestamp write Settimestamp stored timestamp_Specified;
  end;



  // ************************************************************************ //
  // XML       : TPDate, global, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  TPDate = class(TRemotable)
  private
    FBegin_: timestampType;
    FBegin__Specified: boolean;
    FEnd_: timestampType;
    FEnd__Specified: boolean;
    procedure SetBegin_(Index: Integer; const AtimestampType: timestampType);
    function  Begin__Specified(Index: Integer): boolean;
    procedure SetEnd_(Index: Integer; const AtimestampType: timestampType);
    function  End__Specified(Index: Integer): boolean;
  published
    property Begin_: timestampType  Index (IS_OPTN) read FBegin_ write SetBegin_ stored Begin__Specified;
    property End_:   timestampType  Index (IS_OPTN) read FEnd_ write SetEnd_ stored End__Specified;
  end;

  vehicleidType   =  type Int64;      { "http://www.fleetboard.com/data"[GblSmpl] }


  // ************************************************************************ //
  // XML       : Position, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  Position2 = class(TRemotable)
  private
    FVehicleId: vehicleidType;
    FFleetId: fleetidType;
    FGpsTime: timestampType;
    FLong: string;
    FLat: string;
    FVehicleTimestamp: timestampType;
    FVehicleTimestamp_Specified: boolean;
    FServerTimestamp: timestampType;
    FServerTimestamp_Specified: boolean;
    procedure SetVehicleTimestamp(Index: Integer; const AtimestampType: timestampType);
    function  VehicleTimestamp_Specified(Index: Integer): boolean;
    procedure SetServerTimestamp(Index: Integer; const AtimestampType: timestampType);
    function  ServerTimestamp_Specified(Index: Integer): boolean;
  published
    property VehicleId:        vehicleidType  read FVehicleId write FVehicleId;
    property FleetId:          fleetidType    read FFleetId write FFleetId;
    property GpsTime:          timestampType  read FGpsTime write FGpsTime;
    property Long:             string         read FLong write FLong;
    property Lat:              string         read FLat write FLat;
    property VehicleTimestamp: timestampType  Index (IS_OPTN) read FVehicleTimestamp write SetVehicleTimestamp stored VehicleTimestamp_Specified;
    property ServerTimestamp:  timestampType  Index (IS_OPTN) read FServerTimestamp write SetServerTimestamp stored ServerTimestamp_Specified;
  end;

  Array_Of_vehicleidType = array of vehicleidType;   { "http://www.fleetboard.com/data"[GblUbnd] }
  fbidType        =  type string;      { "http://www.fleetboard.com/data"[GblSmpl] }
  Array_Of_fbidType = array of fbidType;        { "http://www.fleetboard.com/data"[GblUbnd] }
  inoidType       =  type string;      { "http://www.fleetboard.com/data"[GblSmpl] }
  Array_Of_inoidType = array of inoidType;      { "http://www.fleetboard.com/data"[GblUbnd] }


  // ************************************************************************ //
  // XML       : CacheMsg, global, <element>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  CacheMsg = class(TRemotable)
  private
    Fdoc_inoid: inoidType;
    Fdoc_inoid_Specified: boolean;
    FDocType: string;
    FCacheType: string;
    FInfo: string;
    procedure Setdoc_inoid(Index: Integer; const AinoidType: inoidType);
    function  doc_inoid_Specified(Index: Integer): boolean;
  published
    property doc_inoid: inoidType  Index (IS_ATTR or IS_OPTN) read Fdoc_inoid write Setdoc_inoid stored doc_inoid_Specified;
    property DocType:   string     read FDocType write FDocType;
    property CacheType: string     read FCacheType write FCacheType;
    property Info:      string     read FInfo write FInfo;
  end;

  offsetType      =  type Int64;      { "http://www.fleetboard.com/data"[GblSmpl] }


  // ************************************************************************ //
  // XML       : GetLastPositionRequestType, global, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  GetLastPositionRequestType = class(TRemotable)
  private
    Fsessionid: sessionidType;
    Fsessionid_Specified: boolean;
    Flimit: limitType;
    Flimit_Specified: boolean;
    Foffset: offsetType;
    Foffset_Specified: boolean;
    FQueryType: SmallInt;
    FGroupID: Int64;
    FGroupID_Specified: boolean;
    FEntityID: Array_Of_long;
    FEntityID_Specified: boolean;
    FAdditionalState: Array_Of_AdditionalState;
    FAdditionalState_Specified: boolean;
    procedure Setsessionid(Index: Integer; const AsessionidType: sessionidType);
    function  sessionid_Specified(Index: Integer): boolean;
    procedure Setlimit(Index: Integer; const AlimitType: limitType);
    function  limit_Specified(Index: Integer): boolean;
    procedure Setoffset(Index: Integer; const AoffsetType: offsetType);
    function  offset_Specified(Index: Integer): boolean;
    procedure SetGroupID(Index: Integer; const AInt64: Int64);
    function  GroupID_Specified(Index: Integer): boolean;
    procedure SetEntityID(Index: Integer; const AArray_Of_long: Array_Of_long);
    function  EntityID_Specified(Index: Integer): boolean;
    procedure SetAdditionalState(Index: Integer; const AArray_Of_AdditionalState: Array_Of_AdditionalState);
    function  AdditionalState_Specified(Index: Integer): boolean;
  published
    property sessionid:       sessionidType             Index (IS_ATTR or IS_OPTN) read Fsessionid write Setsessionid stored sessionid_Specified;
    property limit:           limitType                 Index (IS_ATTR or IS_OPTN) read Flimit write Setlimit stored limit_Specified;
    property offset:          offsetType                Index (IS_ATTR or IS_OPTN) read Foffset write Setoffset stored offset_Specified;
    property QueryType:       SmallInt                  read FQueryType write FQueryType;
    property GroupID:         Int64                     Index (IS_OPTN) read FGroupID write SetGroupID stored GroupID_Specified;
    property EntityID:        Array_Of_long             Index (IS_OPTN or IS_UNBD) read FEntityID write SetEntityID stored EntityID_Specified;
    property AdditionalState: Array_Of_AdditionalState  Index (IS_OPTN or IS_UNBD) read FAdditionalState write SetAdditionalState stored AdditionalState_Specified;
  end;



  // ************************************************************************ //
  // XML       : GetLastPositionRequest, global, <element>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  GetLastPositionRequest = class(GetLastPositionRequestType)
  private
  published
  end;

  responseSizeType =  type Int64;      { "http://www.fleetboard.com/data"[GblSmpl] }
  resultSizeType  =  type Int64;      { "http://www.fleetboard.com/data"[GblSmpl] }


  // ************************************************************************ //
  // XML       : GetLocationDescriptionResponseType, global, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  GetLocationDescriptionResponseType = class(TRemotable)
  private
    FresultSize: resultSizeType;
    FresultSize_Specified: boolean;
    FresponseSize: responseSizeType;
    FresponseSize_Specified: boolean;
    FLocationDescription: Array_Of_LocationDescription;
    FLocationDescription_Specified: boolean;
    procedure SetresultSize(Index: Integer; const AresultSizeType: resultSizeType);
    function  resultSize_Specified(Index: Integer): boolean;
    procedure SetresponseSize(Index: Integer; const AresponseSizeType: responseSizeType);
    function  responseSize_Specified(Index: Integer): boolean;
    procedure SetLocationDescription(Index: Integer; const AArray_Of_LocationDescription: Array_Of_LocationDescription);
    function  LocationDescription_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property resultSize:          resultSizeType                Index (IS_ATTR or IS_OPTN) read FresultSize write SetresultSize stored resultSize_Specified;
    property responseSize:        responseSizeType              Index (IS_ATTR or IS_OPTN) read FresponseSize write SetresponseSize stored responseSize_Specified;
    property LocationDescription: Array_Of_LocationDescription  Index (IS_OPTN or IS_UNBD or IS_REF) read FLocationDescription write SetLocationDescription stored LocationDescription_Specified;
  end;



  // ************************************************************************ //
  // XML       : GetLocationDescriptionResponse, global, <element>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  GetLocationDescriptionResponse = class(GetLocationDescriptionResponseType)
  private
  published
  end;



  // ************************************************************************ //
  // XML       : GetLastPositionResponseType, global, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  GetLastPositionResponseType = class(TRemotable)
  private
    Flimit: limitType;
    Flimit_Specified: boolean;
    Foffset: offsetType;
    Foffset_Specified: boolean;
    FresultSize: resultSizeType;
    FresultSize_Specified: boolean;
    FresponseSize: responseSizeType;
    FresponseSize_Specified: boolean;
    FPositions: Array_Of_Positions;
    FPositions_Specified: boolean;
    procedure Setlimit(Index: Integer; const AlimitType: limitType);
    function  limit_Specified(Index: Integer): boolean;
    procedure Setoffset(Index: Integer; const AoffsetType: offsetType);
    function  offset_Specified(Index: Integer): boolean;
    procedure SetresultSize(Index: Integer; const AresultSizeType: resultSizeType);
    function  resultSize_Specified(Index: Integer): boolean;
    procedure SetresponseSize(Index: Integer; const AresponseSizeType: responseSizeType);
    function  responseSize_Specified(Index: Integer): boolean;
    procedure SetPositions(Index: Integer; const AArray_Of_Positions: Array_Of_Positions);
    function  Positions_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property limit:        limitType           Index (IS_ATTR or IS_OPTN) read Flimit write Setlimit stored limit_Specified;
    property offset:       offsetType          Index (IS_ATTR or IS_OPTN) read Foffset write Setoffset stored offset_Specified;
    property resultSize:   resultSizeType      Index (IS_ATTR or IS_OPTN) read FresultSize write SetresultSize stored resultSize_Specified;
    property responseSize: responseSizeType    Index (IS_ATTR or IS_OPTN) read FresponseSize write SetresponseSize stored responseSize_Specified;
    property Positions:    Array_Of_Positions  Index (IS_OPTN or IS_UNBD) read FPositions write SetPositions stored Positions_Specified;
  end;



  // ************************************************************************ //
  // XML       : GetLastPositionResponse, global, <element>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  GetLastPositionResponse = class(GetLastPositionResponseType)
  private
  published
  end;



  // ************************************************************************ //
  // XML       : GetReplyResponseType, global, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  GetReplyResponseType = class(TRemotable)
  private
    Flimit: limitType;
    Flimit_Specified: boolean;
    Foffset: offsetType;
    Foffset_Specified: boolean;
    FresultSize: resultSizeType;
    FresultSize_Specified: boolean;
    FresponseSize: responseSizeType;
    FresponseSize_Specified: boolean;
    FMsg: Array_Of_Msg;
    FMsg_Specified: boolean;
    procedure Setlimit(Index: Integer; const AlimitType: limitType);
    function  limit_Specified(Index: Integer): boolean;
    procedure Setoffset(Index: Integer; const AoffsetType: offsetType);
    function  offset_Specified(Index: Integer): boolean;
    procedure SetresultSize(Index: Integer; const AresultSizeType: resultSizeType);
    function  resultSize_Specified(Index: Integer): boolean;
    procedure SetresponseSize(Index: Integer; const AresponseSizeType: responseSizeType);
    function  responseSize_Specified(Index: Integer): boolean;
    procedure SetMsg(Index: Integer; const AArray_Of_Msg: Array_Of_Msg);
    function  Msg_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property limit:        limitType         Index (IS_ATTR or IS_OPTN) read Flimit write Setlimit stored limit_Specified;
    property offset:       offsetType        Index (IS_ATTR or IS_OPTN) read Foffset write Setoffset stored offset_Specified;
    property resultSize:   resultSizeType    Index (IS_ATTR or IS_OPTN) read FresultSize write SetresultSize stored resultSize_Specified;
    property responseSize: responseSizeType  Index (IS_ATTR or IS_OPTN) read FresponseSize write SetresponseSize stored responseSize_Specified;
    property Msg:          Array_Of_Msg      Index (IS_OPTN or IS_UNBD or IS_REF) read FMsg write SetMsg stored Msg_Specified;
  end;



  // ************************************************************************ //
  // XML       : GetReplyResponse, global, <element>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  GetReplyResponse = class(GetReplyResponseType)
  private
  published
  end;



  // ************************************************************************ //
  // XML       : GetPosReportTraceResponseType, global, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  GetPosReportTraceResponseType = class(TRemotable)
  private
    Flimit: limitType;
    Flimit_Specified: boolean;
    Foffset: offsetType;
    Foffset_Specified: boolean;
    FresultSize: resultSizeType;
    FresultSize_Specified: boolean;
    FresponseSize: responseSizeType;
    FresponseSize_Specified: boolean;
    FPosition: Array_Of_Position;
    FPosition_Specified: boolean;
    procedure Setlimit(Index: Integer; const AlimitType: limitType);
    function  limit_Specified(Index: Integer): boolean;
    procedure Setoffset(Index: Integer; const AoffsetType: offsetType);
    function  offset_Specified(Index: Integer): boolean;
    procedure SetresultSize(Index: Integer; const AresultSizeType: resultSizeType);
    function  resultSize_Specified(Index: Integer): boolean;
    procedure SetresponseSize(Index: Integer; const AresponseSizeType: responseSizeType);
    function  responseSize_Specified(Index: Integer): boolean;
    procedure SetPosition(Index: Integer; const AArray_Of_Position: Array_Of_Position);
    function  Position_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property limit:        limitType          Index (IS_ATTR or IS_OPTN) read Flimit write Setlimit stored limit_Specified;
    property offset:       offsetType         Index (IS_ATTR or IS_OPTN) read Foffset write Setoffset stored offset_Specified;
    property resultSize:   resultSizeType     Index (IS_ATTR or IS_OPTN) read FresultSize write SetresultSize stored resultSize_Specified;
    property responseSize: responseSizeType   Index (IS_ATTR or IS_OPTN) read FresponseSize write SetresponseSize stored responseSize_Specified;
    property Position:     Array_Of_Position  Index (IS_OPTN or IS_UNBD) read FPosition write SetPosition stored Position_Specified;
  end;



  // ************************************************************************ //
  // XML       : GetPosReportTraceResponse, global, <element>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  GetPosReportTraceResponse = class(GetPosReportTraceResponseType)
  private
  published
  end;



  // ************************************************************************ //
  // XML       : GetCurrentStateResponseType, global, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  GetCurrentStateResponseType = class(TRemotable)
  private
    Flimit: limitType;
    Flimit_Specified: boolean;
    Foffset: offsetType;
    Foffset_Specified: boolean;
    FresultSize: resultSizeType;
    FresultSize_Specified: boolean;
    FresponseSize: responseSizeType;
    FresponseSize_Specified: boolean;
    FMsg: Array_Of_Msg;
    FMsg_Specified: boolean;
    procedure Setlimit(Index: Integer; const AlimitType: limitType);
    function  limit_Specified(Index: Integer): boolean;
    procedure Setoffset(Index: Integer; const AoffsetType: offsetType);
    function  offset_Specified(Index: Integer): boolean;
    procedure SetresultSize(Index: Integer; const AresultSizeType: resultSizeType);
    function  resultSize_Specified(Index: Integer): boolean;
    procedure SetresponseSize(Index: Integer; const AresponseSizeType: responseSizeType);
    function  responseSize_Specified(Index: Integer): boolean;
    procedure SetMsg(Index: Integer; const AArray_Of_Msg: Array_Of_Msg);
    function  Msg_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property limit:        limitType         Index (IS_ATTR or IS_OPTN) read Flimit write Setlimit stored limit_Specified;
    property offset:       offsetType        Index (IS_ATTR or IS_OPTN) read Foffset write Setoffset stored offset_Specified;
    property resultSize:   resultSizeType    Index (IS_ATTR or IS_OPTN) read FresultSize write SetresultSize stored resultSize_Specified;
    property responseSize: responseSizeType  Index (IS_ATTR or IS_OPTN) read FresponseSize write SetresponseSize stored responseSize_Specified;
    property Msg:          Array_Of_Msg      Index (IS_OPTN or IS_UNBD or IS_REF) read FMsg write SetMsg stored Msg_Specified;
  end;



  // ************************************************************************ //
  // XML       : GetCurrentStateResponse, global, <element>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  GetCurrentStateResponse = class(GetCurrentStateResponseType)
  private
  published
  end;

  versionType     =  type Int64;      { "http://www.fleetboard.com/data"[GblSmpl] }


  // ************************************************************************ //
  // XML       : GetLocationDescriptionRequestType, global, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  GetLocationDescriptionRequestType = class(TRemotable)
  private
    Fsessionid: sessionidType;
    Fsessionid_Specified: boolean;
    Fversion: versionType;
    Fversion_Specified: boolean;
    FLanguage: string;
    FLanguage_Specified: boolean;
    FCoordinate: Array_Of_Coordinate;
    FCoordinate_Specified: boolean;
    procedure Setsessionid(Index: Integer; const AsessionidType: sessionidType);
    function  sessionid_Specified(Index: Integer): boolean;
    procedure Setversion(Index: Integer; const AversionType: versionType);
    function  version_Specified(Index: Integer): boolean;
    procedure SetLanguage(Index: Integer; const Astring: string);
    function  Language_Specified(Index: Integer): boolean;
    procedure SetCoordinate(Index: Integer; const AArray_Of_Coordinate: Array_Of_Coordinate);
    function  Coordinate_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property sessionid:  sessionidType        Index (IS_ATTR or IS_OPTN) read Fsessionid write Setsessionid stored sessionid_Specified;
    property version:    versionType          Index (IS_ATTR or IS_OPTN) read Fversion write Setversion stored version_Specified;
    property Language:   string               Index (IS_OPTN) read FLanguage write SetLanguage stored Language_Specified;
    property Coordinate: Array_Of_Coordinate  Index (IS_OPTN or IS_UNBD) read FCoordinate write SetCoordinate stored Coordinate_Specified;
  end;



  // ************************************************************************ //
  // XML       : GetLocationDescriptionRequest, global, <element>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  GetLocationDescriptionRequest = class(GetLocationDescriptionRequestType)
  private
  published
  end;



  // ************************************************************************ //
  // XML       : GetReplyRequestType, global, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  GetReplyRequestType = class(TRemotable)
  private
    Fsessionid: sessionidType;
    Fsessionid_Specified: boolean;
    Flimit: limitType;
    Flimit_Specified: boolean;
    Foffset: offsetType;
    Foffset_Specified: boolean;
    Fversion: versionType;
    Fversion_Specified: boolean;
    FVehicleID: Array_Of_vehicleidType;
    FVehicleID_Specified: boolean;
    FServerTimestamp: TPDate;
    FServerTimestamp_Specified: boolean;
    procedure Setsessionid(Index: Integer; const AsessionidType: sessionidType);
    function  sessionid_Specified(Index: Integer): boolean;
    procedure Setlimit(Index: Integer; const AlimitType: limitType);
    function  limit_Specified(Index: Integer): boolean;
    procedure Setoffset(Index: Integer; const AoffsetType: offsetType);
    function  offset_Specified(Index: Integer): boolean;
    procedure Setversion(Index: Integer; const AversionType: versionType);
    function  version_Specified(Index: Integer): boolean;
    procedure SetVehicleID(Index: Integer; const AArray_Of_vehicleidType: Array_Of_vehicleidType);
    function  VehicleID_Specified(Index: Integer): boolean;
    procedure SetServerTimestamp(Index: Integer; const ATPDate: TPDate);
    function  ServerTimestamp_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property sessionid:       sessionidType           Index (IS_ATTR or IS_OPTN) read Fsessionid write Setsessionid stored sessionid_Specified;
    property limit:           limitType               Index (IS_ATTR or IS_OPTN) read Flimit write Setlimit stored limit_Specified;
    property offset:          offsetType              Index (IS_ATTR or IS_OPTN) read Foffset write Setoffset stored offset_Specified;
    property version:         versionType             Index (IS_ATTR or IS_OPTN) read Fversion write Setversion stored version_Specified;
    property VehicleID:       Array_Of_vehicleidType  Index (IS_OPTN or IS_UNBD) read FVehicleID write SetVehicleID stored VehicleID_Specified;
    property ServerTimestamp: TPDate                  Index (IS_OPTN) read FServerTimestamp write SetServerTimestamp stored ServerTimestamp_Specified;
  end;



  // ************************************************************************ //
  // XML       : GetReplyRequest, global, <element>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  GetReplyRequest = class(GetReplyRequestType)
  private
  published
  end;



  // ************************************************************************ //
  // XML       : GetPosReportTraceRequestType, global, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  GetPosReportTraceRequestType = class(TRemotable)
  private
    Fsessionid: sessionidType;
    Fsessionid_Specified: boolean;
    Fversion: versionType;
    Fversion_Specified: boolean;
    Flimit: limitType;
    Flimit_Specified: boolean;
    Foffset: offsetType;
    Foffset_Specified: boolean;
    FVehicleId: vehicleidType;
    FVehicleTimestamp: TPDate;
    FVehicleTimestamp_Specified: boolean;
    FServerTimestamp: TPDate;
    FServerTimestamp_Specified: boolean;
    procedure Setsessionid(Index: Integer; const AsessionidType: sessionidType);
    function  sessionid_Specified(Index: Integer): boolean;
    procedure Setversion(Index: Integer; const AversionType: versionType);
    function  version_Specified(Index: Integer): boolean;
    procedure Setlimit(Index: Integer; const AlimitType: limitType);
    function  limit_Specified(Index: Integer): boolean;
    procedure Setoffset(Index: Integer; const AoffsetType: offsetType);
    function  offset_Specified(Index: Integer): boolean;
    procedure SetVehicleTimestamp(Index: Integer; const ATPDate: TPDate);
    function  VehicleTimestamp_Specified(Index: Integer): boolean;
    procedure SetServerTimestamp(Index: Integer; const ATPDate: TPDate);
    function  ServerTimestamp_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property sessionid:        sessionidType  Index (IS_ATTR or IS_OPTN) read Fsessionid write Setsessionid stored sessionid_Specified;
    property version:          versionType    Index (IS_ATTR or IS_OPTN) read Fversion write Setversion stored version_Specified;
    property limit:            limitType      Index (IS_ATTR or IS_OPTN) read Flimit write Setlimit stored limit_Specified;
    property offset:           offsetType     Index (IS_ATTR or IS_OPTN) read Foffset write Setoffset stored offset_Specified;
    property VehicleId:        vehicleidType  read FVehicleId write FVehicleId;
    property VehicleTimestamp: TPDate         Index (IS_OPTN) read FVehicleTimestamp write SetVehicleTimestamp stored VehicleTimestamp_Specified;
    property ServerTimestamp:  TPDate         Index (IS_OPTN) read FServerTimestamp write SetServerTimestamp stored ServerTimestamp_Specified;
  end;



  // ************************************************************************ //
  // XML       : GetPosReportTraceRequest, global, <element>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  GetPosReportTraceRequest = class(GetPosReportTraceRequestType)
  private
  published
  end;

  vehicleGroupIdType =  type Int64;      { "http://www.fleetboard.com/data"[GblSmpl] }
  Array_Of_vehicleGroupIdType = array of vehicleGroupIdType;   { "http://www.fleetboard.com/data"[GblUbnd] }
  driverNameIDType =  type Int64;      { "http://www.fleetboard.com/data"[GblSmpl] }
  Array_Of_driverNameIDType = array of driverNameIDType;   { "http://www.fleetboard.com/data"[GblUbnd] }


  // ************************************************************************ //
  // XML       : GetPosReportResponseType, global, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  GetPosReportResponseType = class(TRemotable)
  private
    Flimit: limitType;
    Flimit_Specified: boolean;
    Foffset: offsetType;
    Foffset_Specified: boolean;
    FresultSize: resultSizeType;
    FresultSize_Specified: boolean;
    FresponseSize: responseSizeType;
    FresponseSize_Specified: boolean;
    FMsg: Array_Of_Msg;
    FMsg_Specified: boolean;
    procedure Setlimit(Index: Integer; const AlimitType: limitType);
    function  limit_Specified(Index: Integer): boolean;
    procedure Setoffset(Index: Integer; const AoffsetType: offsetType);
    function  offset_Specified(Index: Integer): boolean;
    procedure SetresultSize(Index: Integer; const AresultSizeType: resultSizeType);
    function  resultSize_Specified(Index: Integer): boolean;
    procedure SetresponseSize(Index: Integer; const AresponseSizeType: responseSizeType);
    function  responseSize_Specified(Index: Integer): boolean;
    procedure SetMsg(Index: Integer; const AArray_Of_Msg: Array_Of_Msg);
    function  Msg_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property limit:        limitType         Index (IS_ATTR or IS_OPTN) read Flimit write Setlimit stored limit_Specified;
    property offset:       offsetType        Index (IS_ATTR or IS_OPTN) read Foffset write Setoffset stored offset_Specified;
    property resultSize:   resultSizeType    Index (IS_ATTR or IS_OPTN) read FresultSize write SetresultSize stored resultSize_Specified;
    property responseSize: responseSizeType  Index (IS_ATTR or IS_OPTN) read FresponseSize write SetresponseSize stored responseSize_Specified;
    property Msg:          Array_Of_Msg      Index (IS_OPTN or IS_UNBD or IS_REF) read FMsg write SetMsg stored Msg_Specified;
  end;



  // ************************************************************************ //
  // XML       : GetPosReportResponse, global, <element>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  GetPosReportResponse = class(GetPosReportResponseType)
  private
  published
  end;

  driverGroupIdType =  type Int64;      { "http://www.fleetboard.com/data"[GblSmpl] }


  // ************************************************************************ //
  // XML       : GetPosReportRequestType, global, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  GetPosReportRequestType = class(TRemotable)
  private
    Fsessionid: sessionidType;
    Fsessionid_Specified: boolean;
    Flimit: limitType;
    Flimit_Specified: boolean;
    Foffset: offsetType;
    Foffset_Specified: boolean;
    FVehicleID: Array_Of_vehicleidType;
    FVehicleID_Specified: boolean;
    FVehicleGroupID: Array_Of_vehicleGroupIdType;
    FVehicleGroupID_Specified: boolean;
    FTelematicGroupID: telematicGroupIDType;
    FTelematicGroupID_Specified: boolean;
    FDriverNameID: Array_Of_driverNameIDType;
    FDriverNameID_Specified: boolean;
    FDriverGroupID: driverGroupIdType;
    FDriverGroupID_Specified: boolean;
    FVehicleTimestamp: TPDate;
    FVehicleTimestamp_Specified: boolean;
    FRealVehicleTimestamp: TPDate;
    FRealVehicleTimestamp_Specified: boolean;
    FInoid: Array_Of_inoidType;
    FInoid_Specified: boolean;
    FFbid: Array_Of_fbidType;
    FFbid_Specified: boolean;
    procedure Setsessionid(Index: Integer; const AsessionidType: sessionidType);
    function  sessionid_Specified(Index: Integer): boolean;
    procedure Setlimit(Index: Integer; const AlimitType: limitType);
    function  limit_Specified(Index: Integer): boolean;
    procedure Setoffset(Index: Integer; const AoffsetType: offsetType);
    function  offset_Specified(Index: Integer): boolean;
    procedure SetVehicleID(Index: Integer; const AArray_Of_vehicleidType: Array_Of_vehicleidType);
    function  VehicleID_Specified(Index: Integer): boolean;
    procedure SetVehicleGroupID(Index: Integer; const AArray_Of_vehicleGroupIdType: Array_Of_vehicleGroupIdType);
    function  VehicleGroupID_Specified(Index: Integer): boolean;
    procedure SetTelematicGroupID(Index: Integer; const AtelematicGroupIDType: telematicGroupIDType);
    function  TelematicGroupID_Specified(Index: Integer): boolean;
    procedure SetDriverNameID(Index: Integer; const AArray_Of_driverNameIDType: Array_Of_driverNameIDType);
    function  DriverNameID_Specified(Index: Integer): boolean;
    procedure SetDriverGroupID(Index: Integer; const AdriverGroupIdType: driverGroupIdType);
    function  DriverGroupID_Specified(Index: Integer): boolean;
    procedure SetVehicleTimestamp(Index: Integer; const ATPDate: TPDate);
    function  VehicleTimestamp_Specified(Index: Integer): boolean;
    procedure SetRealVehicleTimestamp(Index: Integer; const ATPDate: TPDate);
    function  RealVehicleTimestamp_Specified(Index: Integer): boolean;
    procedure SetInoid(Index: Integer; const AArray_Of_inoidType: Array_Of_inoidType);
    function  Inoid_Specified(Index: Integer): boolean;
    procedure SetFbid(Index: Integer; const AArray_Of_fbidType: Array_Of_fbidType);
    function  Fbid_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property sessionid:            sessionidType                Index (IS_ATTR or IS_OPTN) read Fsessionid write Setsessionid stored sessionid_Specified;
    property limit:                limitType                    Index (IS_ATTR or IS_OPTN) read Flimit write Setlimit stored limit_Specified;
    property offset:               offsetType                   Index (IS_ATTR or IS_OPTN) read Foffset write Setoffset stored offset_Specified;
    property VehicleID:            Array_Of_vehicleidType       Index (IS_OPTN or IS_UNBD) read FVehicleID write SetVehicleID stored VehicleID_Specified;
    property VehicleGroupID:       Array_Of_vehicleGroupIdType  Index (IS_OPTN or IS_UNBD) read FVehicleGroupID write SetVehicleGroupID stored VehicleGroupID_Specified;
    property TelematicGroupID:     telematicGroupIDType         Index (IS_OPTN) read FTelematicGroupID write SetTelematicGroupID stored TelematicGroupID_Specified;
    property DriverNameID:         Array_Of_driverNameIDType    Index (IS_OPTN or IS_UNBD) read FDriverNameID write SetDriverNameID stored DriverNameID_Specified;
    property DriverGroupID:        driverGroupIdType            Index (IS_OPTN) read FDriverGroupID write SetDriverGroupID stored DriverGroupID_Specified;
    property VehicleTimestamp:     TPDate                       Index (IS_OPTN) read FVehicleTimestamp write SetVehicleTimestamp stored VehicleTimestamp_Specified;
    property RealVehicleTimestamp: TPDate                       Index (IS_OPTN) read FRealVehicleTimestamp write SetRealVehicleTimestamp stored RealVehicleTimestamp_Specified;
    property Inoid:                Array_Of_inoidType           Index (IS_OPTN or IS_UNBD) read FInoid write SetInoid stored Inoid_Specified;
    property Fbid:                 Array_Of_fbidType            Index (IS_OPTN or IS_UNBD) read FFbid write SetFbid stored Fbid_Specified;
  end;



  // ************************************************************************ //
  // XML       : GetPosReportRequest, global, <element>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  GetPosReportRequest = class(GetPosReportRequestType)
  private
  published
  end;



  // ************************************************************************ //
  // XML       : GetCurrentStateRequestType, global, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  GetCurrentStateRequestType = class(TRemotable)
  private
    Fsessionid: sessionidType;
    Fsessionid_Specified: boolean;
    Flimit: limitType;
    Flimit_Specified: boolean;
    Foffset: offsetType;
    Foffset_Specified: boolean;
    Fversion: versionType;
    Fversion_Specified: boolean;
    FVehicleID: Array_Of_vehicleidType;
    FVehicleID_Specified: boolean;
    FVehicleGroupID: vehicleGroupIdType;
    FVehicleGroupID_Specified: boolean;
    FDriverNameID: Array_Of_driverNameIDType;
    FDriverNameID_Specified: boolean;
    FDriverGroupID: driverGroupIdType;
    FDriverGroupID_Specified: boolean;
    FServiceType: serviceType;
    FServiceType_Specified: boolean;
    FQueryType: SmallInt;
    FQueryType_Specified: boolean;
    FOnlyActive: Boolean;
    FOnlyActive_Specified: boolean;
    procedure Setsessionid(Index: Integer; const AsessionidType: sessionidType);
    function  sessionid_Specified(Index: Integer): boolean;
    procedure Setlimit(Index: Integer; const AlimitType: limitType);
    function  limit_Specified(Index: Integer): boolean;
    procedure Setoffset(Index: Integer; const AoffsetType: offsetType);
    function  offset_Specified(Index: Integer): boolean;
    procedure Setversion(Index: Integer; const AversionType: versionType);
    function  version_Specified(Index: Integer): boolean;
    procedure SetVehicleID(Index: Integer; const AArray_Of_vehicleidType: Array_Of_vehicleidType);
    function  VehicleID_Specified(Index: Integer): boolean;
    procedure SetVehicleGroupID(Index: Integer; const AvehicleGroupIdType: vehicleGroupIdType);
    function  VehicleGroupID_Specified(Index: Integer): boolean;
    procedure SetDriverNameID(Index: Integer; const AArray_Of_driverNameIDType: Array_Of_driverNameIDType);
    function  DriverNameID_Specified(Index: Integer): boolean;
    procedure SetDriverGroupID(Index: Integer; const AdriverGroupIdType: driverGroupIdType);
    function  DriverGroupID_Specified(Index: Integer): boolean;
    procedure SetServiceType(Index: Integer; const AserviceType: serviceType);
    function  ServiceType_Specified(Index: Integer): boolean;
    procedure SetQueryType(Index: Integer; const ASmallInt: SmallInt);
    function  QueryType_Specified(Index: Integer): boolean;
    procedure SetOnlyActive(Index: Integer; const ABoolean: Boolean);
    function  OnlyActive_Specified(Index: Integer): boolean;
  published
    property sessionid:      sessionidType              Index (IS_ATTR or IS_OPTN) read Fsessionid write Setsessionid stored sessionid_Specified;
    property limit:          limitType                  Index (IS_ATTR or IS_OPTN) read Flimit write Setlimit stored limit_Specified;
    property offset:         offsetType                 Index (IS_ATTR or IS_OPTN) read Foffset write Setoffset stored offset_Specified;
    property version:        versionType                Index (IS_ATTR or IS_OPTN) read Fversion write Setversion stored version_Specified;
    property VehicleID:      Array_Of_vehicleidType     Index (IS_OPTN or IS_UNBD) read FVehicleID write SetVehicleID stored VehicleID_Specified;
    property VehicleGroupID: vehicleGroupIdType         Index (IS_OPTN) read FVehicleGroupID write SetVehicleGroupID stored VehicleGroupID_Specified;
    property DriverNameID:   Array_Of_driverNameIDType  Index (IS_OPTN or IS_UNBD) read FDriverNameID write SetDriverNameID stored DriverNameID_Specified;
    property DriverGroupID:  driverGroupIdType          Index (IS_OPTN) read FDriverGroupID write SetDriverGroupID stored DriverGroupID_Specified;
    property ServiceType:    serviceType                Index (IS_OPTN) read FServiceType write SetServiceType stored ServiceType_Specified;
    property QueryType:      SmallInt                   Index (IS_OPTN) read FQueryType write SetQueryType stored QueryType_Specified;
    property OnlyActive:     Boolean                    Index (IS_OPTN) read FOnlyActive write SetOnlyActive stored OnlyActive_Specified;
  end;



  // ************************************************************************ //
  // XML       : GetCurrentStateRequest, global, <element>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  GetCurrentStateRequest = class(GetCurrentStateRequestType)
  private
  published
  end;

  FieldID         =  type Int64;      { "http://www.fleetboard.com/data"[Smpl] }
  Value           =  type string;      { "http://www.fleetboard.com/data"[Smpl] }


  // ************************************************************************ //
  // XML       : Field, global, <element>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  Field = class(TRemotable)
  private
    FFieldID: FieldID;
    FValue: Value;
  published
    property FieldID: FieldID  read FFieldID write FFieldID;
    property Value:   Value    read FValue write FValue;
  end;

  Fields     = array of Field;                  { "http://www.fleetboard.com/data"[GblElm] }
  ReadInfo   = array of User;                   { "http://www.fleetboard.com/data"[GblElm] }


  // ************************************************************************ //
  // XML       : TracePosition, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  TracePosition = class(TRemotable)
  private
    FGpsTime: timestampType;
    FLong: Single;
    FLat: Single;
  published
    property GpsTime: timestampType  read FGpsTime write FGpsTime;
    property Long:    Single         read FLong write FLong;
    property Lat:     Single         read FLat write FLat;
  end;



  // ************************************************************************ //
  // XML       : State, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  State = class(TRemotable)
  private
    FText: string;
    Fname_: string;
    Ftimestamp: timestampType;
    Ftimestamp_Specified: boolean;
    procedure Settimestamp(Index: Integer; const AtimestampType: timestampType);
    function  timestamp_Specified(Index: Integer): boolean;
  published
    property Text:      string         Index (IS_TEXT) read FText write FText;
    property name_:     string         Index (IS_ATTR) read Fname_ write Fname_;
    property timestamp: timestampType  Index (IS_ATTR or IS_OPTN) read Ftimestamp write Settimestamp stored timestamp_Specified;
  end;

  States     = array of State;                  { "http://www.fleetboard.com/data"[Cplx] }


  // ************************************************************************ //
  // XML       : Header, global, <element>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  Header = class(TRemotable)
  private
    FMessagetype: string;
    FSentstatus: string;
    FSentstatus_Specified: boolean;
    FInbound: string;
    FInbound_Specified: boolean;
    FClientTimestamp: string;
    FClientTimestamp_Specified: boolean;
    FServerTimestamp: string;
    FServerTimestamp_Specified: boolean;
    FVehicleTimestamp: string;
    FVehicleTimestamp_Specified: boolean;
    FCreationBeginTimestamp: string;
    FCreationBeginTimestamp_Specified: boolean;
    FCreationEndTimestamp: string;
    FCreationEndTimestamp_Specified: boolean;
    FPosition: Position;
    FPosition_Specified: boolean;
    FReadInfo: ReadInfo;
    FReadInfo_Specified: boolean;
    FConfirmation: string;
    FConfirmation_Specified: boolean;
    FScheduled: string;
    FScheduled_Specified: boolean;
    FVehicleID: vehicleidType;
    FVehicleID_Specified: boolean;
    FDriverID: string;
    FDriverID_Specified: boolean;
    FDeliveryPriority: string;
    FDeliveryPriority_Specified: boolean;
    FStates: States;
    FStates_Specified: boolean;
    procedure SetSentstatus(Index: Integer; const Astring: string);
    function  Sentstatus_Specified(Index: Integer): boolean;
    procedure SetInbound(Index: Integer; const Astring: string);
    function  Inbound_Specified(Index: Integer): boolean;
    procedure SetClientTimestamp(Index: Integer; const Astring: string);
    function  ClientTimestamp_Specified(Index: Integer): boolean;
    procedure SetServerTimestamp(Index: Integer; const Astring: string);
    function  ServerTimestamp_Specified(Index: Integer): boolean;
    procedure SetVehicleTimestamp(Index: Integer; const Astring: string);
    function  VehicleTimestamp_Specified(Index: Integer): boolean;
    procedure SetCreationBeginTimestamp(Index: Integer; const Astring: string);
    function  CreationBeginTimestamp_Specified(Index: Integer): boolean;
    procedure SetCreationEndTimestamp(Index: Integer; const Astring: string);
    function  CreationEndTimestamp_Specified(Index: Integer): boolean;
    procedure SetPosition(Index: Integer; const APosition: Position);
    function  Position_Specified(Index: Integer): boolean;
    procedure SetReadInfo(Index: Integer; const AReadInfo: ReadInfo);
    function  ReadInfo_Specified(Index: Integer): boolean;
    procedure SetConfirmation(Index: Integer; const Astring: string);
    function  Confirmation_Specified(Index: Integer): boolean;
    procedure SetScheduled(Index: Integer; const Astring: string);
    function  Scheduled_Specified(Index: Integer): boolean;
    procedure SetVehicleID(Index: Integer; const AvehicleidType: vehicleidType);
    function  VehicleID_Specified(Index: Integer): boolean;
    procedure SetDriverID(Index: Integer; const Astring: string);
    function  DriverID_Specified(Index: Integer): boolean;
    procedure SetDeliveryPriority(Index: Integer; const Astring: string);
    function  DeliveryPriority_Specified(Index: Integer): boolean;
    procedure SetStates(Index: Integer; const AStates: States);
    function  States_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property Messagetype:            string         read FMessagetype write FMessagetype;
    property Sentstatus:             string         Index (IS_OPTN) read FSentstatus write SetSentstatus stored Sentstatus_Specified;
    property Inbound:                string         Index (IS_OPTN) read FInbound write SetInbound stored Inbound_Specified;
    property ClientTimestamp:        string         Index (IS_OPTN) read FClientTimestamp write SetClientTimestamp stored ClientTimestamp_Specified;
    property ServerTimestamp:        string         Index (IS_OPTN) read FServerTimestamp write SetServerTimestamp stored ServerTimestamp_Specified;
    property VehicleTimestamp:       string         Index (IS_OPTN) read FVehicleTimestamp write SetVehicleTimestamp stored VehicleTimestamp_Specified;
    property CreationBeginTimestamp: string         Index (IS_OPTN) read FCreationBeginTimestamp write SetCreationBeginTimestamp stored CreationBeginTimestamp_Specified;
    property CreationEndTimestamp:   string         Index (IS_OPTN) read FCreationEndTimestamp write SetCreationEndTimestamp stored CreationEndTimestamp_Specified;
    property Position:               Position       Index (IS_OPTN or IS_REF) read FPosition write SetPosition stored Position_Specified;
    property ReadInfo:               ReadInfo       Index (IS_OPTN or IS_REF) read FReadInfo write SetReadInfo stored ReadInfo_Specified;
    property Confirmation:           string         Index (IS_OPTN) read FConfirmation write SetConfirmation stored Confirmation_Specified;
    property Scheduled:              string         Index (IS_OPTN) read FScheduled write SetScheduled stored Scheduled_Specified;
    property VehicleID:              vehicleidType  Index (IS_OPTN) read FVehicleID write SetVehicleID stored VehicleID_Specified;
    property DriverID:               string         Index (IS_OPTN) read FDriverID write SetDriverID stored DriverID_Specified;
    property DeliveryPriority:       string         Index (IS_OPTN) read FDeliveryPriority write SetDeliveryPriority stored DeliveryPriority_Specified;
    property States:                 States         Index (IS_OPTN) read FStates write SetStates stored States_Specified;
  end;

  FormDefID       =  type Int64;      { "http://www.fleetboard.com/data"[Smpl] }


  // ************************************************************************ //
  // XML       : Form, global, <element>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  Form = class(TRemotable)
  private
    FFormDefID: FormDefID;
    FFields: Fields;
  public
    destructor Destroy; override;
  published
    property FormDefID: FormDefID  read FFormDefID write FFormDefID;
    property Fields:    Fields     Index (IS_REF) read FFields write FFields;
  end;



  // ************************************************************************ //
  // XML       : VhcLayout, global, <element>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  VhcLayout = class(TRemotable)
  private
    FVehicleColumn: string;
    FVehicleColumn_Specified: boolean;
    FVehicleRow: string;
    FVehicleRow_Specified: boolean;
    FHidden: Hidden;
    FHidden_Specified: boolean;
    procedure SetVehicleColumn(Index: Integer; const Astring: string);
    function  VehicleColumn_Specified(Index: Integer): boolean;
    procedure SetVehicleRow(Index: Integer; const Astring: string);
    function  VehicleRow_Specified(Index: Integer): boolean;
    procedure SetHidden(Index: Integer; const AHidden: Hidden);
    function  Hidden_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property VehicleColumn: string  Index (IS_OPTN) read FVehicleColumn write SetVehicleColumn stored VehicleColumn_Specified;
    property VehicleRow:    string  Index (IS_OPTN) read FVehicleRow write SetVehicleRow stored VehicleRow_Specified;
    property Hidden:        Hidden  Index (IS_OPTN or IS_REF) read FHidden write SetHidden stored Hidden_Specified;
  end;



  // ************************************************************************ //
  // XML       : FormLayout, global, <element>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  FormLayout = class(TRemotable)
  private
    FTop: string;
    FLeft: string;
    FWidth: string;
    FHeight: string;
    FBGColor: string;
    FFontColor: string;
  published
    property Top:       string  read FTop write FTop;
    property Left:      string  read FLeft write FLeft;
    property Width:     string  read FWidth write FWidth;
    property Height:    string  read FHeight write FHeight;
    property BGColor:   string  read FBGColor write FBGColor;
    property FontColor: string  read FFontColor write FFontColor;
  end;



  // ************************************************************************ //
  // XML       : Behavior, global, <element>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  Behavior = class(TRemotable)
  private
    FMaxlength: string;
    FTaborder: string;
    FAddressmapping: string;
    FPriority: string;
  published
    property Maxlength:      string  read FMaxlength write FMaxlength;
    property Taborder:       string  read FTaborder write FTaborder;
    property Addressmapping: string  read FAddressmapping write FAddressmapping;
    property Priority:       string  read FPriority write FPriority;
  end;



  // ************************************************************************ //
  // XML       : Hidden, global, <element>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  Hidden = class(TRemotable)
  private
    FDCFAP: Int64;
    FDCFAP_Specified: boolean;
    procedure SetDCFAP(Index: Integer; const AInt64: Int64);
    function  DCFAP_Specified(Index: Integer): boolean;
  published
    property DCFAP: Int64  Index (IS_OPTN) read FDCFAP write SetDCFAP stored DCFAP_Specified;
  end;



  // ************************************************************************ //
  // XML       : OrderMsg, global, <element>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  OrderMsg = class(TRemotable)
  private
    FOrderID: string;
    FTourID: string;
    FTourRank: Int64;
    FTourRank_Specified: boolean;
    FOrderCount: Int64;
    FOrderCount_Specified: boolean;
    FForm: Form;
    procedure SetTourRank(Index: Integer; const AInt64: Int64);
    function  TourRank_Specified(Index: Integer): boolean;
    procedure SetOrderCount(Index: Integer; const AInt64: Int64);
    function  OrderCount_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property OrderID:    string  read FOrderID write FOrderID;
    property TourID:     string  read FTourID write FTourID;
    property TourRank:   Int64   Index (IS_OPTN) read FTourRank write SetTourRank stored TourRank_Specified;
    property OrderCount: Int64   Index (IS_OPTN) read FOrderCount write SetOrderCount stored OrderCount_Specified;
    property Form:       Form    Index (IS_REF) read FForm write FForm;
  end;



  // ************************************************************************ //
  // XML       : VhcStatusLayout, global, <element>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  VhcStatusLayout = class(TRemotable)
  private
    FHidden: Hidden;
    FHidden_Specified: boolean;
    procedure SetHidden(Index: Integer; const AHidden: Hidden);
    function  Hidden_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property Hidden: Hidden  Index (IS_OPTN or IS_REF) read FHidden write SetHidden stored Hidden_Specified;
  end;



  // ************************************************************************ //
  // XML       : Semantics, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  Semantics = class(TRemotable)
  private
    FDenotation: string;
    FSemanticId: Int64;
    FSemanticId_Specified: boolean;
    FScope: Times;
    FScope_Specified: boolean;
    FValue: string;
    FValue_Specified: boolean;
    procedure SetSemanticId(Index: Integer; const AInt64: Int64);
    function  SemanticId_Specified(Index: Integer): boolean;
    procedure SetScope(Index: Integer; const ATimes: Times);
    function  Scope_Specified(Index: Integer): boolean;
    procedure SetValue(Index: Integer; const Astring: string);
    function  Value_Specified(Index: Integer): boolean;
  published
    property Denotation: string  read FDenotation write FDenotation;
    property SemanticId: Int64   Index (IS_OPTN) read FSemanticId write SetSemanticId stored SemanticId_Specified;
    property Scope:      Times   Index (IS_OPTN or IS_UNBD) read FScope write SetScope stored Scope_Specified;
    property Value:      string  Index (IS_OPTN) read FValue write SetValue stored Value_Specified;
  end;

  SemanticsDef = array of Semantics;            { "http://www.fleetboard.com/data"[GblElm] }


  // ************************************************************************ //
  // XML       : FieldDef, global, <element>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  FieldDef = class(TRemotable)
  private
    FFieldDefID: Int64;
    FName_: string;
    FType_: string;
    FVhcLayout: VhcLayout;
    FFormLayout: FormLayout;
    FBehavior: Behavior;
    FSearchable: Int64;
    FSearchable_Specified: boolean;
    FSuppressInVehicle: Int64;
    FSuppressInVehicle_Specified: boolean;
    FCopyFrom: Int64;
    FCopyFrom_Specified: boolean;
    FSemanticsDef: SemanticsDef;
    FSemanticsDef_Specified: boolean;
    procedure SetSearchable(Index: Integer; const AInt64: Int64);
    function  Searchable_Specified(Index: Integer): boolean;
    procedure SetSuppressInVehicle(Index: Integer; const AInt64: Int64);
    function  SuppressInVehicle_Specified(Index: Integer): boolean;
    procedure SetCopyFrom(Index: Integer; const AInt64: Int64);
    function  CopyFrom_Specified(Index: Integer): boolean;
    procedure SetSemanticsDef(Index: Integer; const ASemanticsDef: SemanticsDef);
    function  SemanticsDef_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property FieldDefID:        Int64         read FFieldDefID write FFieldDefID;
    property Name_:             string        read FName_ write FName_;
    property Type_:             string        read FType_ write FType_;
    property VhcLayout:         VhcLayout     Index (IS_REF) read FVhcLayout write FVhcLayout;
    property FormLayout:        FormLayout    Index (IS_REF) read FFormLayout write FFormLayout;
    property Behavior:          Behavior      Index (IS_REF) read FBehavior write FBehavior;
    property Searchable:        Int64         Index (IS_OPTN) read FSearchable write SetSearchable stored Searchable_Specified;
    property SuppressInVehicle: Int64         Index (IS_OPTN) read FSuppressInVehicle write SetSuppressInVehicle stored SuppressInVehicle_Specified;
    property CopyFrom:          Int64         Index (IS_OPTN) read FCopyFrom write SetCopyFrom stored CopyFrom_Specified;
    property SemanticsDef:      SemanticsDef  Index (IS_OPTN or IS_REF) read FSemanticsDef write SetSemanticsDef stored SemanticsDef_Specified;
  end;



  // ************************************************************************ //
  // XML       : StatusDefMsg, global, <element>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  StatusDefMsg = class(TRemotable)
  private
    FATSRegister: string;
    FText: string;
    FStatusDefID: string;
    FStatusDefID_Specified: boolean;
    FVhcStatusLayout: VhcStatusLayout;
    FVhcStatusLayout_Specified: boolean;
    FSemanticsDef: SemanticsDef;
    FSemanticsDef_Specified: boolean;
    procedure SetStatusDefID(Index: Integer; const Astring: string);
    function  StatusDefID_Specified(Index: Integer): boolean;
    procedure SetVhcStatusLayout(Index: Integer; const AVhcStatusLayout: VhcStatusLayout);
    function  VhcStatusLayout_Specified(Index: Integer): boolean;
    procedure SetSemanticsDef(Index: Integer; const ASemanticsDef: SemanticsDef);
    function  SemanticsDef_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property ATSRegister:     string           read FATSRegister write FATSRegister;
    property Text:            string           read FText write FText;
    property StatusDefID:     string           Index (IS_OPTN) read FStatusDefID write SetStatusDefID stored StatusDefID_Specified;
    property VhcStatusLayout: VhcStatusLayout  Index (IS_OPTN or IS_REF) read FVhcStatusLayout write SetVhcStatusLayout stored VhcStatusLayout_Specified;
    property SemanticsDef:    SemanticsDef     Index (IS_OPTN or IS_REF) read FSemanticsDef write SetSemanticsDef stored SemanticsDef_Specified;
  end;



  // ************************************************************************ //
  // XML       : ReeferStartStopContinousChanged, global, <element>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  ReeferStartStopContinousChanged = class(TRemotable)
  private
    FcurrentReeferOperationMode: CurrentReeferOperationModeDTO;
    FcurrentReeferOperationMode_Specified: boolean;
    procedure SetcurrentReeferOperationMode(Index: Integer; const ACurrentReeferOperationModeDTO: CurrentReeferOperationModeDTO);
    function  currentReeferOperationMode_Specified(Index: Integer): boolean;
  published
    property currentReeferOperationMode: CurrentReeferOperationModeDTO  Index (IS_OPTN) read FcurrentReeferOperationMode write SetcurrentReeferOperationMode stored currentReeferOperationMode_Specified;
  end;



  // ************************************************************************ //
  // XML       : SetpointAlarm, global, <element>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  SetpointAlarm = class(TRemotable)
  private
    FsetpointSensor: SetpointSensorDTO;
    FsetpointSensor_Specified: boolean;
    FSetpointData: SetpointData;
    FSetpointData_Specified: boolean;
    Fthreshold: Int64;
    Fthreshold_Specified: boolean;
    FisAllClear: Boolean;
    FisAllClear_Specified: boolean;
    procedure SetsetpointSensor(Index: Integer; const ASetpointSensorDTO: SetpointSensorDTO);
    function  setpointSensor_Specified(Index: Integer): boolean;
    procedure SetSetpointData(Index: Integer; const ASetpointData: SetpointData);
    function  SetpointData_Specified(Index: Integer): boolean;
    procedure Setthreshold(Index: Integer; const AInt64: Int64);
    function  threshold_Specified(Index: Integer): boolean;
    procedure SetisAllClear(Index: Integer; const ABoolean: Boolean);
    function  isAllClear_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property setpointSensor: SetpointSensorDTO  Index (IS_OPTN) read FsetpointSensor write SetsetpointSensor stored setpointSensor_Specified;
    property SetpointData:   SetpointData       Index (IS_OPTN or IS_REF) read FSetpointData write SetSetpointData stored SetpointData_Specified;
    property threshold:      Int64              Index (IS_OPTN) read Fthreshold write Setthreshold stored threshold_Specified;
    property isAllClear:     Boolean            Index (IS_OPTN) read FisAllClear write SetisAllClear stored isAllClear_Specified;
  end;



  // ************************************************************************ //
  // XML       : ReeferDieselElectricChanged, global, <element>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  ReeferDieselElectricChanged = class(TRemotable)
  private
    FcurrentPowerMode: CurrentPowerModeDTO;
    FcurrentPowerMode_Specified: boolean;
    procedure SetcurrentPowerMode(Index: Integer; const ACurrentPowerModeDTO: CurrentPowerModeDTO);
    function  currentPowerMode_Specified(Index: Integer): boolean;
  published
    property currentPowerMode: CurrentPowerModeDTO  Index (IS_OPTN) read FcurrentPowerMode write SetcurrentPowerMode stored currentPowerMode_Specified;
  end;



  // ************************************************************************ //
  // XML       : FuelLevelAlarm, global, <element>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  FuelLevelAlarm = class(TRemotable)
  private
    FcurrentFuelLevelColor: CurrentFuelLevelColorDTO;
    FcurrentFuelLevelColor_Specified: boolean;
    procedure SetcurrentFuelLevelColor(Index: Integer; const ACurrentFuelLevelColorDTO: CurrentFuelLevelColorDTO);
    function  currentFuelLevelColor_Specified(Index: Integer): boolean;
  published
    property currentFuelLevelColor: CurrentFuelLevelColorDTO  Index (IS_OPTN) read FcurrentFuelLevelColor write SetcurrentFuelLevelColor stored currentFuelLevelColor_Specified;
  end;



  // ************************************************************************ //
  // XML       : CurrentCompartmentDataList, global, <element>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  CurrentCompartmentDataList = class(TRemotable)
  private
    FcompartmentNumber: Int64;
    FcompartmentNumber_Specified: boolean;
    FcompartmentOperationMode: CompartmentOperationModeDTO;
    FcompartmentOperationMode_Specified: boolean;
    procedure SetcompartmentNumber(Index: Integer; const AInt64: Int64);
    function  compartmentNumber_Specified(Index: Integer): boolean;
    procedure SetcompartmentOperationMode(Index: Integer; const ACompartmentOperationModeDTO: CompartmentOperationModeDTO);
    function  compartmentOperationMode_Specified(Index: Integer): boolean;
  published
    property compartmentNumber:        Int64                        Index (IS_OPTN) read FcompartmentNumber write SetcompartmentNumber stored compartmentNumber_Specified;
    property compartmentOperationMode: CompartmentOperationModeDTO  Index (IS_OPTN) read FcompartmentOperationMode write SetcompartmentOperationMode stored compartmentOperationMode_Specified;
  end;



  // ************************************************************************ //
  // XML       : Positions, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  Positions = class(TRemotable)
  private
    FFleetID: fleetidType;
    FVehicleID: vehicleidType;
    FDriverNameID: driverNameIDType;
    FMotorStatus: SmallInt;
    FMotorStatus_Specified: boolean;
    FIgnitionStatus: SmallInt;
    FIgnitionStatus_Specified: boolean;
    FPosition: Position;
    FStateOfVehicle: LastPositionState;
    FStateOfVehicle_Specified: boolean;
    FStateOfTimeManagement: LastPositionState;
    FStateOfTimeManagement_Specified: boolean;
    FStateOfOrderManagement: LastPositionState;
    FStateOfOrderManagement_Specified: boolean;
    procedure SetMotorStatus(Index: Integer; const ASmallInt: SmallInt);
    function  MotorStatus_Specified(Index: Integer): boolean;
    procedure SetIgnitionStatus(Index: Integer; const ASmallInt: SmallInt);
    function  IgnitionStatus_Specified(Index: Integer): boolean;
    procedure SetStateOfVehicle(Index: Integer; const ALastPositionState: LastPositionState);
    function  StateOfVehicle_Specified(Index: Integer): boolean;
    procedure SetStateOfTimeManagement(Index: Integer; const ALastPositionState: LastPositionState);
    function  StateOfTimeManagement_Specified(Index: Integer): boolean;
    procedure SetStateOfOrderManagement(Index: Integer; const ALastPositionState: LastPositionState);
    function  StateOfOrderManagement_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property FleetID:                fleetidType        read FFleetID write FFleetID;
    property VehicleID:              vehicleidType      read FVehicleID write FVehicleID;
    property DriverNameID:           driverNameIDType   read FDriverNameID write FDriverNameID;
    property MotorStatus:            SmallInt           Index (IS_OPTN) read FMotorStatus write SetMotorStatus stored MotorStatus_Specified;
    property IgnitionStatus:         SmallInt           Index (IS_OPTN) read FIgnitionStatus write SetIgnitionStatus stored IgnitionStatus_Specified;
    property Position:               Position           Index (IS_REF) read FPosition write FPosition;
    property StateOfVehicle:         LastPositionState  Index (IS_OPTN) read FStateOfVehicle write SetStateOfVehicle stored StateOfVehicle_Specified;
    property StateOfTimeManagement:  LastPositionState  Index (IS_OPTN) read FStateOfTimeManagement write SetStateOfTimeManagement stored StateOfTimeManagement_Specified;
    property StateOfOrderManagement: LastPositionState  Index (IS_OPTN) read FStateOfOrderManagement write SetStateOfOrderManagement stored StateOfOrderManagement_Specified;
  end;

  useridType      =  type Int64;      { "http://www.fleetboard.com/data"[GblSmpl] }


  // ************************************************************************ //
  // XML       : TemperatureAlarm, global, <element>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  TemperatureAlarm = class(TRemotable)
  private
    FtemperatureSensor: TemperatureSensorDTO;
    FtemperatureSensor_Specified: boolean;
    FcurrentTemperature: Double;
    FcurrentTemperature_Specified: boolean;
    FminThreshold: Int64;
    FminThreshold_Specified: boolean;
    FmaxThreshold: Int64;
    FmaxThreshold_Specified: boolean;
    FisAllClear: Boolean;
    FisAllClear_Specified: boolean;
    procedure SettemperatureSensor(Index: Integer; const ATemperatureSensorDTO: TemperatureSensorDTO);
    function  temperatureSensor_Specified(Index: Integer): boolean;
    procedure SetcurrentTemperature(Index: Integer; const ADouble: Double);
    function  currentTemperature_Specified(Index: Integer): boolean;
    procedure SetminThreshold(Index: Integer; const AInt64: Int64);
    function  minThreshold_Specified(Index: Integer): boolean;
    procedure SetmaxThreshold(Index: Integer; const AInt64: Int64);
    function  maxThreshold_Specified(Index: Integer): boolean;
    procedure SetisAllClear(Index: Integer; const ABoolean: Boolean);
    function  isAllClear_Specified(Index: Integer): boolean;
  published
    property temperatureSensor:  TemperatureSensorDTO  Index (IS_OPTN) read FtemperatureSensor write SettemperatureSensor stored temperatureSensor_Specified;
    property currentTemperature: Double                Index (IS_OPTN) read FcurrentTemperature write SetcurrentTemperature stored currentTemperature_Specified;
    property minThreshold:       Int64                 Index (IS_OPTN) read FminThreshold write SetminThreshold stored minThreshold_Specified;
    property maxThreshold:       Int64                 Index (IS_OPTN) read FmaxThreshold write SetmaxThreshold stored maxThreshold_Specified;
    property isAllClear:         Boolean               Index (IS_OPTN) read FisAllClear write SetisAllClear stored isAllClear_Specified;
  end;



  // ************************************************************************ //
  // XML       : PairingMsg, global, <element>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  PairingMsg = class(TRemotable)
  private
    FdeviceId: string;
    FvehicleId: Int64;
    Fstatus: Device2VehicleConnectionStatusType;
    FstatusTime: string;
  published
    property deviceId:   string                              read FdeviceId write FdeviceId;
    property vehicleId:  Int64                               read FvehicleId write FvehicleId;
    property status:     Device2VehicleConnectionStatusType  read Fstatus write Fstatus;
    property statusTime: string                              read FstatusTime write FstatusTime;
  end;



  // ************************************************************************ //
  // XML       : TirePressureAlarm, global, <element>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  TirePressureAlarm = class(TRemotable)
  private
    FtirePosition: TirePositionDTO;
    FtirePosition_Specified: boolean;
    FcurrentPressure: Double;
    FcurrentPressure_Specified: boolean;
    Fthreshold: Double;
    Fthreshold_Specified: boolean;
    procedure SettirePosition(Index: Integer; const ATirePositionDTO: TirePositionDTO);
    function  tirePosition_Specified(Index: Integer): boolean;
    procedure SetcurrentPressure(Index: Integer; const ADouble: Double);
    function  currentPressure_Specified(Index: Integer): boolean;
    procedure Setthreshold(Index: Integer; const ADouble: Double);
    function  threshold_Specified(Index: Integer): boolean;
  published
    property tirePosition:    TirePositionDTO  Index (IS_OPTN) read FtirePosition write SettirePosition stored tirePosition_Specified;
    property currentPressure: Double           Index (IS_OPTN) read FcurrentPressure write SetcurrentPressure stored currentPressure_Specified;
    property threshold:       Double           Index (IS_OPTN) read Fthreshold write Setthreshold stored threshold_Specified;
  end;

  USERID          =  type useridType;      { "http://www.fleetboard.com/data"[Smpl] }
  MsgID           =  type inoidType;      { "http://www.fleetboard.com/data"[Smpl] }


  // ************************************************************************ //
  // XML       : User, global, <element>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  User = class(TRemotable)
  private
    FUsertype: string;
    FUsertype_Specified: boolean;
    FUSERID: USERID;
    FTimestamp: timestampType;
    FMsgID: MsgID;
    FMsgID_Specified: boolean;
    procedure SetUsertype(Index: Integer; const Astring: string);
    function  Usertype_Specified(Index: Integer): boolean;
    procedure SetMsgID(Index: Integer; const AMsgID: MsgID);
    function  MsgID_Specified(Index: Integer): boolean;
  published
    property Usertype:  string         Index (IS_ATTR or IS_OPTN) read FUsertype write SetUsertype stored Usertype_Specified;
    property USERID:    USERID         read FUSERID write FUSERID;
    property Timestamp: timestampType  read FTimestamp write FTimestamp;
    property MsgID:     MsgID          Index (IS_OPTN) read FMsgID write SetMsgID stored MsgID_Specified;
  end;

  TracePositions = array of TracePosition;      { "http://www.fleetboard.com/data"[Cplx] }


  // ************************************************************************ //
  // XML       : Position, global, <element>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  Position = class(TRemotable)
  private
    Ftimestamp: timestampType;
    Ftimestamp_Specified: boolean;
    FLong: Single;
    FLong_Specified: boolean;
    FLat: Single;
    FLat_Specified: boolean;
    FPosText: string;
    FPosText_Specified: boolean;
    FCourse: string;
    FSpeed: string;
    FKM: string;
    FGPSStatus: string;
    FGPSStatus_Specified: boolean;
    FTracePositions: TracePositions;
    FTracePositions_Specified: boolean;
    procedure Settimestamp(Index: Integer; const AtimestampType: timestampType);
    function  timestamp_Specified(Index: Integer): boolean;
    procedure SetLong(Index: Integer; const ASingle: Single);
    function  Long_Specified(Index: Integer): boolean;
    procedure SetLat(Index: Integer; const ASingle: Single);
    function  Lat_Specified(Index: Integer): boolean;
    procedure SetPosText(Index: Integer; const Astring: string);
    function  PosText_Specified(Index: Integer): boolean;
    procedure SetGPSStatus(Index: Integer; const Astring: string);
    function  GPSStatus_Specified(Index: Integer): boolean;
    procedure SetTracePositions(Index: Integer; const ATracePositions: TracePositions);
    function  TracePositions_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property timestamp:      timestampType   Index (IS_ATTR or IS_OPTN) read Ftimestamp write Settimestamp stored timestamp_Specified;
    property Long:           Single          Index (IS_OPTN) read FLong write SetLong stored Long_Specified;
    property Lat:            Single          Index (IS_OPTN) read FLat write SetLat stored Lat_Specified;
    property PosText:        string          Index (IS_OPTN) read FPosText write SetPosText stored PosText_Specified;
    property Course:         string          Index (IS_NLBL) read FCourse write FCourse;
    property Speed:          string          Index (IS_NLBL) read FSpeed write FSpeed;
    property KM:             string          Index (IS_NLBL) read FKM write FKM;
    property GPSStatus:      string          Index (IS_OPTN) read FGPSStatus write SetGPSStatus stored GPSStatus_Specified;
    property TracePositions: TracePositions  Index (IS_OPTN) read FTracePositions write SetTracePositions stored TracePositions_Specified;
  end;

  History    = array of User;                   { "http://www.fleetboard.com/data"[GblElm] }


  // ************************************************************************ //
  // XML       : Msg, global, <element>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  Msg = class(TRemotable)
  private
    Finoid: inoidType;
    Finoid_Specified: boolean;
    Ffbid: fbidType;
    Ffbid_Specified: boolean;
    FFLEETID: fleetidType;
    FHistory: History;
    FHistory_Specified: boolean;
    FHeader: Header;
    FBody: Body;
    FBody_Specified: boolean;
    FForeignRef: Array_Of_ForeignRef;
    FForeignRef_Specified: boolean;
    procedure Setinoid(Index: Integer; const AinoidType: inoidType);
    function  inoid_Specified(Index: Integer): boolean;
    procedure Setfbid(Index: Integer; const AfbidType: fbidType);
    function  fbid_Specified(Index: Integer): boolean;
    procedure SetHistory(Index: Integer; const AHistory: History);
    function  History_Specified(Index: Integer): boolean;
    procedure SetBody(Index: Integer; const ABody: Body);
    function  Body_Specified(Index: Integer): boolean;
    procedure SetForeignRef(Index: Integer; const AArray_Of_ForeignRef: Array_Of_ForeignRef);
    function  ForeignRef_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property inoid:      inoidType            Index (IS_ATTR or IS_OPTN) read Finoid write Setinoid stored inoid_Specified;
    property fbid:       fbidType             Index (IS_ATTR or IS_OPTN) read Ffbid write Setfbid stored fbid_Specified;
    property FLEETID:    fleetidType          read FFLEETID write FFLEETID;
    property History:    History              Index (IS_OPTN or IS_REF) read FHistory write SetHistory stored History_Specified;
    property Header:     Header               Index (IS_REF) read FHeader write FHeader;
    property Body:       Body                 Index (IS_OPTN or IS_REF) read FBody write SetBody stored Body_Specified;
    property ForeignRef: Array_Of_ForeignRef  Index (IS_OPTN or IS_UNBD) read FForeignRef write SetForeignRef stored ForeignRef_Specified;
  end;



  // ************************************************************************ //
  // XML       : WatchboxAlarm, global, <element>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  WatchboxAlarm = class(TRemotable)
  private
    FwatchboxName: string;
    FwatchboxName_Specified: boolean;
    FwatchboxAlarmReason: WatchboxAlarmReasonDTO;
    FwatchboxAlarmReason_Specified: boolean;
    procedure SetwatchboxName(Index: Integer; const Astring: string);
    function  watchboxName_Specified(Index: Integer): boolean;
    procedure SetwatchboxAlarmReason(Index: Integer; const AWatchboxAlarmReasonDTO: WatchboxAlarmReasonDTO);
    function  watchboxAlarmReason_Specified(Index: Integer): boolean;
  published
    property watchboxName:        string                  Index (IS_OPTN) read FwatchboxName write SetwatchboxName stored watchboxName_Specified;
    property watchboxAlarmReason: WatchboxAlarmReasonDTO  Index (IS_OPTN) read FwatchboxAlarmReason write SetwatchboxAlarmReason stored watchboxAlarmReason_Specified;
  end;



  // ************************************************************************ //
  // XML       : UndervoltageAlarm, global, <element>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  UndervoltageAlarm = class(TRemotable)
  private
    FbatteryConcerned: BatteryConcernedDTO;
    FbatteryConcerned_Specified: boolean;
    procedure SetbatteryConcerned(Index: Integer; const ABatteryConcernedDTO: BatteryConcernedDTO);
    function  batteryConcerned_Specified(Index: Integer): boolean;
  published
    property batteryConcerned: BatteryConcernedDTO  Index (IS_OPTN) read FbatteryConcerned write SetbatteryConcerned stored batteryConcerned_Specified;
  end;



  // ************************************************************************ //
  // XML       : TrailerRegisterMsg, global, <element>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  TrailerRegisterMsg = class(TRemotable)
  private
    Fid: string;
    FuserName: string;
    FuserName_Specified: boolean;
    FphoneNumber: string;
    Femail: string;
    FcontactPersonName: string;
    Fdescription: string;
    Fdescription_Specified: boolean;
    FtrailerServiceProviderId: string;
    FtspAccountState: TspAccountState;
    FerrorCode: string;
    FerrorMessage: string;
    FfleetId: Int64;
    FupdateTime: TXSDateTime;
    procedure SetuserName(Index: Integer; const Astring: string);
    function  userName_Specified(Index: Integer): boolean;
    procedure Setdescription(Index: Integer; const Astring: string);
    function  description_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property id:                       string           read Fid write Fid;
    property userName:                 string           Index (IS_OPTN) read FuserName write SetuserName stored userName_Specified;
    property phoneNumber:              string           read FphoneNumber write FphoneNumber;
    property email:                    string           read Femail write Femail;
    property contactPersonName:        string           read FcontactPersonName write FcontactPersonName;
    property description:              string           Index (IS_OPTN) read Fdescription write Setdescription stored description_Specified;
    property trailerServiceProviderId: string           read FtrailerServiceProviderId write FtrailerServiceProviderId;
    property tspAccountState:          TspAccountState  read FtspAccountState write FtspAccountState;
    property errorCode:                string           read FerrorCode write FerrorCode;
    property errorMessage:             string           read FerrorMessage write FerrorMessage;
    property fleetId:                  Int64            read FfleetId write FfleetId;
    property updateTime:               TXSDateTime      read FupdateTime write FupdateTime;
  end;



  // ************************************************************************ //
  // XML       : TrailerCouplingMsg, global, <element>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  TrailerCouplingMsg = class(TRemotable)
  private
    FtrailerId: string;
    FvehicleId: Int64;
    FvehicleId_Specified: boolean;
    FtrailerCoupled: TrailerCoupled;
    FupdateTime: TXSDateTime;
    procedure SetvehicleId(Index: Integer; const AInt64: Int64);
    function  vehicleId_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property trailerId:      string          read FtrailerId write FtrailerId;
    property vehicleId:      Int64           Index (IS_OPTN) read FvehicleId write SetvehicleId stored vehicleId_Specified;
    property trailerCoupled: TrailerCoupled  read FtrailerCoupled write FtrailerCoupled;
    property updateTime:     TXSDateTime     read FupdateTime write FupdateTime;
  end;



  // ************************************************************************ //
  // XML       : TrailerSyncMsg, global, <element>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  TrailerSyncMsg = class(TRemotable)
  private
    Fid: string;
    Fname_: string;
    Fvin: string;
    Fvin_Specified: boolean;
    Fnationality: string;
    FlicencePlateNumber: string;
    FcustomerMatchCode: string;
    FfleetId: Int64;
    FtrailerGroupIds: Times;
    FtrailerGroupIds_Specified: boolean;
    FtrailerState: TrailerState;
    FupdateTime: TXSDateTime;
    FtrailerServiceProviderAccountId: string;
    FtrailerServiceProviderAccountId_Specified: boolean;
    FtrailerServiceProviderId: string;
    FtrailerServiceProviderId_Specified: boolean;
    procedure Setvin(Index: Integer; const Astring: string);
    function  vin_Specified(Index: Integer): boolean;
    procedure SettrailerGroupIds(Index: Integer; const ATimes: Times);
    function  trailerGroupIds_Specified(Index: Integer): boolean;
    procedure SettrailerServiceProviderAccountId(Index: Integer; const Astring: string);
    function  trailerServiceProviderAccountId_Specified(Index: Integer): boolean;
    procedure SettrailerServiceProviderId(Index: Integer; const Astring: string);
    function  trailerServiceProviderId_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property id:                              string        read Fid write Fid;
    property name_:                           string        read Fname_ write Fname_;
    property vin:                             string        Index (IS_OPTN) read Fvin write Setvin stored vin_Specified;
    property nationality:                     string        read Fnationality write Fnationality;
    property licencePlateNumber:              string        read FlicencePlateNumber write FlicencePlateNumber;
    property customerMatchCode:               string        read FcustomerMatchCode write FcustomerMatchCode;
    property fleetId:                         Int64         read FfleetId write FfleetId;
    property trailerGroupIds:                 Times         Index (IS_OPTN or IS_UNBD) read FtrailerGroupIds write SettrailerGroupIds stored trailerGroupIds_Specified;
    property trailerState:                    TrailerState  read FtrailerState write FtrailerState;
    property updateTime:                      TXSDateTime   read FupdateTime write FupdateTime;
    property trailerServiceProviderAccountId: string        Index (IS_OPTN) read FtrailerServiceProviderAccountId write SettrailerServiceProviderAccountId stored trailerServiceProviderAccountId_Specified;
    property trailerServiceProviderId:        string        Index (IS_OPTN) read FtrailerServiceProviderId write SettrailerServiceProviderId stored trailerServiceProviderId_Specified;
  end;


  // ************************************************************************ //
  // Namespace : http://ws.fleetboard.com/PosService
  // soapAction: %operationName%
  // transport : http://schemas.xmlsoap.org/soap/http
  // style     : document
  // use       : literal
  // binding   : PosServiceBinding
  // service   : PosService
  // port      : PosService
  // URL       : http://www.fleetboard.com/soap_v1_1/services/PosService
  // ************************************************************************ //
  PosService = interface(IInvokable)
  ['{03709D30-7907-0E6F-70C6-2B788D8CB44B}']
    function  getPosReport(const GetPosReportRequest: GetPosReportRequest): GetPosReportResponse; stdcall;
    function  getPosReportTrace(const GetPosReportTraceRequest: GetPosReportTraceRequest): GetPosReportTraceResponse; stdcall;
    function  getReply(const GetReplyRequest: GetReplyRequest): GetReplyResponse; stdcall;
    function  getCurrentState(const GetCurrentStateRequest: GetCurrentStateRequest): GetCurrentStateResponse; stdcall;
    function  getLocationDescription(const GetLocationDescriptionRequest: GetLocationDescriptionRequest): GetLocationDescriptionResponse; stdcall;
    function  getLastPosition(const GetLastPositionRequest: GetLastPositionRequest): GetLastPositionResponse; stdcall;
  end;

function GetPosService(UseWSDL: Boolean=System.False; Addr: string=''; HTTPRIO: THTTPRIO = nil): PosService;


implementation
  uses SysUtils;

function GetPosService(UseWSDL: Boolean; Addr: string; HTTPRIO: THTTPRIO): PosService;
const
  defWSDL = 'P:\SRC\FUVAR_ADO\4.20\WSDL\Fleetboard_PosService.wsdl';
  defURL  = 'http://www.fleetboard.com/soap_v1_1/services/PosService';
  defSvc  = 'PosService';
  defPrt  = 'PosService';
var
  RIO: THTTPRIO;
begin
  Result := nil;
  if (Addr = '') then
  begin
    if UseWSDL then
      Addr := defWSDL
    else
      Addr := defURL;
  end;
  if HTTPRIO = nil then
    RIO := THTTPRIO.Create(nil)
  else
    RIO := HTTPRIO;
  try
    Result := (RIO as PosService);
    if UseWSDL then
    begin
      RIO.WSDLLocation := Addr;
      RIO.Service := defSvc;
      RIO.Port := defPrt;
    end else
      RIO.URL := Addr;
  finally
    if (Result = nil) and (HTTPRIO = nil) then
      RIO.Free;
  end;
end;


destructor AutoPosDef.Destroy;
begin
  SysUtils.FreeAndNil(FAutoPos);
  inherited Destroy;
end;

destructor ReasonDTO.Destroy;
var
  I: Integer;
begin
  for I := 0 to System.Length(FReeferOperationModeChanged)-1 do
    SysUtils.FreeAndNil(FReeferOperationModeChanged[I]);
  System.SetLength(FReeferOperationModeChanged, 0);
  SysUtils.FreeAndNil(FTemperatureAlarm);
  SysUtils.FreeAndNil(FSetpointAlarm);
  SysUtils.FreeAndNil(FEbs24NAlarm);
  SysUtils.FreeAndNil(FMilageAlarm);
  SysUtils.FreeAndNil(FReeferStartStopContinousChanged);
  SysUtils.FreeAndNil(FReeferDieselElectricChanged);
  SysUtils.FreeAndNil(FFuelLevelAlarm);
  SysUtils.FreeAndNil(FTempLoggerMonitoringAlarm);
  SysUtils.FreeAndNil(FTirePressureAlarm);
  SysUtils.FreeAndNil(FUndervoltageAlarm);
  SysUtils.FreeAndNil(FVelocityAlarm);
  SysUtils.FreeAndNil(FWatchboxAlarm);
  SysUtils.FreeAndNil(FBogieLoadAlarm);
  SysUtils.FreeAndNil(FHumidityAlarm);
  inherited Destroy;
end;

procedure ReasonDTO.Setstandard(Index: Integer; const ABoolean: Boolean);
begin
  Fstandard := ABoolean;
  Fstandard_Specified := True;
end;

function ReasonDTO.standard_Specified(Index: Integer): boolean;
begin
  Result := Fstandard_Specified;
end;

procedure ReasonDTO.SetstatusRequest(Index: Integer; const ABoolean: Boolean);
begin
  FstatusRequest := ABoolean;
  FstatusRequest_Specified := True;
end;

function ReasonDTO.statusRequest_Specified(Index: Integer): boolean;
begin
  Result := FstatusRequest_Specified;
end;

procedure ReasonDTO.SettagChangedAlarm(Index: Integer; const ABoolean: Boolean);
begin
  FtagChangedAlarm := ABoolean;
  FtagChangedAlarm_Specified := True;
end;

function ReasonDTO.tagChangedAlarm_Specified(Index: Integer): boolean;
begin
  Result := FtagChangedAlarm_Specified;
end;

procedure ReasonDTO.SetebsNotConnected(Index: Integer; const ABoolean: Boolean);
begin
  FebsNotConnected := ABoolean;
  FebsNotConnected_Specified := True;
end;

function ReasonDTO.ebsNotConnected_Specified(Index: Integer): boolean;
begin
  Result := FebsNotConnected_Specified;
end;

procedure ReasonDTO.SetbrakeLiningAlarm(Index: Integer; const ABoolean: Boolean);
begin
  FbrakeLiningAlarm := ABoolean;
  FbrakeLiningAlarm_Specified := True;
end;

function ReasonDTO.brakeLiningAlarm_Specified(Index: Integer): boolean;
begin
  Result := FbrakeLiningAlarm_Specified;
end;

procedure ReasonDTO.SetcouplingAlarm(Index: Integer; const ABoolean: Boolean);
begin
  FcouplingAlarm := ABoolean;
  FcouplingAlarm_Specified := True;
end;

function ReasonDTO.couplingAlarm_Specified(Index: Integer): boolean;
begin
  Result := FcouplingAlarm_Specified;
end;

procedure ReasonDTO.SetdoorAlarm(Index: Integer; const ABoolean: Boolean);
begin
  FdoorAlarm := ABoolean;
  FdoorAlarm_Specified := True;
end;

function ReasonDTO.doorAlarm_Specified(Index: Integer): boolean;
begin
  Result := FdoorAlarm_Specified;
end;

procedure ReasonDTO.SetignitionAlarm(Index: Integer; const ABoolean: Boolean);
begin
  FignitionAlarm := ABoolean;
  FignitionAlarm_Specified := True;
end;

function ReasonDTO.ignitionAlarm_Specified(Index: Integer): boolean;
begin
  Result := FignitionAlarm_Specified;
end;

procedure ReasonDTO.SetreeferAlarm(Index: Integer; const ABoolean: Boolean);
begin
  FreeferAlarm := ABoolean;
  FreeferAlarm_Specified := True;
end;

function ReasonDTO.reeferAlarm_Specified(Index: Integer): boolean;
begin
  Result := FreeferAlarm_Specified;
end;

procedure ReasonDTO.SetreeferAlarmQueueChanged(Index: Integer; const ABoolean: Boolean);
begin
  FreeferAlarmQueueChanged := ABoolean;
  FreeferAlarmQueueChanged_Specified := True;
end;

function ReasonDTO.reeferAlarmQueueChanged_Specified(Index: Integer): boolean;
begin
  Result := FreeferAlarmQueueChanged_Specified;
end;

procedure ReasonDTO.SetreeferPresetListChanged(Index: Integer; const ABoolean: Boolean);
begin
  FreeferPresetListChanged := ABoolean;
  FreeferPresetListChanged_Specified := True;
end;

function ReasonDTO.reeferPresetListChanged_Specified(Index: Integer): boolean;
begin
  Result := FreeferPresetListChanged_Specified;
end;

procedure ReasonDTO.SetreeferCurrentPresetChanged(Index: Integer; const ABoolean: Boolean);
begin
  FreeferCurrentPresetChanged := ABoolean;
  FreeferCurrentPresetChanged_Specified := True;
end;

function ReasonDTO.reeferCurrentPresetChanged_Specified(Index: Integer): boolean;
begin
  Result := FreeferCurrentPresetChanged_Specified;
end;

procedure ReasonDTO.SettrailerStartStopAlarm(Index: Integer; const ABoolean: Boolean);
begin
  FtrailerStartStopAlarm := ABoolean;
  FtrailerStartStopAlarm_Specified := True;
end;

function ReasonDTO.trailerStartStopAlarm_Specified(Index: Integer): boolean;
begin
  Result := FtrailerStartStopAlarm_Specified;
end;

procedure ReasonDTO.SetTemperatureAlarm(Index: Integer; const ATemperatureAlarm: TemperatureAlarm);
begin
  FTemperatureAlarm := ATemperatureAlarm;
  FTemperatureAlarm_Specified := True;
end;

function ReasonDTO.TemperatureAlarm_Specified(Index: Integer): boolean;
begin
  Result := FTemperatureAlarm_Specified;
end;

procedure ReasonDTO.SetSetpointAlarm(Index: Integer; const ASetpointAlarm: SetpointAlarm);
begin
  FSetpointAlarm := ASetpointAlarm;
  FSetpointAlarm_Specified := True;
end;

function ReasonDTO.SetpointAlarm_Specified(Index: Integer): boolean;
begin
  Result := FSetpointAlarm_Specified;
end;

procedure ReasonDTO.SetEbs24NAlarm(Index: Integer; const AEbs24NAlarm: Ebs24NAlarm);
begin
  FEbs24NAlarm := AEbs24NAlarm;
  FEbs24NAlarm_Specified := True;
end;

function ReasonDTO.Ebs24NAlarm_Specified(Index: Integer): boolean;
begin
  Result := FEbs24NAlarm_Specified;
end;

procedure ReasonDTO.SetMilageAlarm(Index: Integer; const AMilageAlarm: MilageAlarm);
begin
  FMilageAlarm := AMilageAlarm;
  FMilageAlarm_Specified := True;
end;

function ReasonDTO.MilageAlarm_Specified(Index: Integer): boolean;
begin
  Result := FMilageAlarm_Specified;
end;

procedure ReasonDTO.SetReeferStartStopContinousChanged(Index: Integer; const AReeferStartStopContinousChanged: ReeferStartStopContinousChanged);
begin
  FReeferStartStopContinousChanged := AReeferStartStopContinousChanged;
  FReeferStartStopContinousChanged_Specified := True;
end;

function ReasonDTO.ReeferStartStopContinousChanged_Specified(Index: Integer): boolean;
begin
  Result := FReeferStartStopContinousChanged_Specified;
end;

procedure ReasonDTO.SetReeferDieselElectricChanged(Index: Integer; const AReeferDieselElectricChanged: ReeferDieselElectricChanged);
begin
  FReeferDieselElectricChanged := AReeferDieselElectricChanged;
  FReeferDieselElectricChanged_Specified := True;
end;

function ReasonDTO.ReeferDieselElectricChanged_Specified(Index: Integer): boolean;
begin
  Result := FReeferDieselElectricChanged_Specified;
end;

procedure ReasonDTO.SetReeferOperationModeChanged(Index: Integer; const AReeferOperationModeChanged: ReeferOperationModeChanged);
begin
  FReeferOperationModeChanged := AReeferOperationModeChanged;
  FReeferOperationModeChanged_Specified := True;
end;

function ReasonDTO.ReeferOperationModeChanged_Specified(Index: Integer): boolean;
begin
  Result := FReeferOperationModeChanged_Specified;
end;

procedure ReasonDTO.SetFuelLevelAlarm(Index: Integer; const AFuelLevelAlarm: FuelLevelAlarm);
begin
  FFuelLevelAlarm := AFuelLevelAlarm;
  FFuelLevelAlarm_Specified := True;
end;

function ReasonDTO.FuelLevelAlarm_Specified(Index: Integer): boolean;
begin
  Result := FFuelLevelAlarm_Specified;
end;

procedure ReasonDTO.SetTempLoggerMonitoringAlarm(Index: Integer; const ATempLoggerMonitoringAlarm: TempLoggerMonitoringAlarm);
begin
  FTempLoggerMonitoringAlarm := ATempLoggerMonitoringAlarm;
  FTempLoggerMonitoringAlarm_Specified := True;
end;

function ReasonDTO.TempLoggerMonitoringAlarm_Specified(Index: Integer): boolean;
begin
  Result := FTempLoggerMonitoringAlarm_Specified;
end;

procedure ReasonDTO.SetTirePressureAlarm(Index: Integer; const ATirePressureAlarm: TirePressureAlarm);
begin
  FTirePressureAlarm := ATirePressureAlarm;
  FTirePressureAlarm_Specified := True;
end;

function ReasonDTO.TirePressureAlarm_Specified(Index: Integer): boolean;
begin
  Result := FTirePressureAlarm_Specified;
end;

procedure ReasonDTO.SetUndervoltageAlarm(Index: Integer; const AUndervoltageAlarm: UndervoltageAlarm);
begin
  FUndervoltageAlarm := AUndervoltageAlarm;
  FUndervoltageAlarm_Specified := True;
end;

function ReasonDTO.UndervoltageAlarm_Specified(Index: Integer): boolean;
begin
  Result := FUndervoltageAlarm_Specified;
end;

procedure ReasonDTO.SetVelocityAlarm(Index: Integer; const AVelocityAlarm: VelocityAlarm);
begin
  FVelocityAlarm := AVelocityAlarm;
  FVelocityAlarm_Specified := True;
end;

function ReasonDTO.VelocityAlarm_Specified(Index: Integer): boolean;
begin
  Result := FVelocityAlarm_Specified;
end;

procedure ReasonDTO.SetWatchboxAlarm(Index: Integer; const AWatchboxAlarm: WatchboxAlarm);
begin
  FWatchboxAlarm := AWatchboxAlarm;
  FWatchboxAlarm_Specified := True;
end;

function ReasonDTO.WatchboxAlarm_Specified(Index: Integer): boolean;
begin
  Result := FWatchboxAlarm_Specified;
end;

procedure ReasonDTO.SetBogieLoadAlarm(Index: Integer; const ABogieLoadAlarm: BogieLoadAlarm);
begin
  FBogieLoadAlarm := ABogieLoadAlarm;
  FBogieLoadAlarm_Specified := True;
end;

function ReasonDTO.BogieLoadAlarm_Specified(Index: Integer): boolean;
begin
  Result := FBogieLoadAlarm_Specified;
end;

procedure ReasonDTO.SetHumidityAlarm(Index: Integer; const AHumidityAlarm: HumidityAlarm);
begin
  FHumidityAlarm := AHumidityAlarm;
  FHumidityAlarm_Specified := True;
end;

function ReasonDTO.HumidityAlarm_Specified(Index: Integer): boolean;
begin
  Result := FHumidityAlarm_Specified;
end;

procedure TempLoggerMonitoringAlarm.SetisAllClear(Index: Integer; const ABoolean: Boolean);
begin
  FisAllClear := ABoolean;
  FisAllClear_Specified := True;
end;

function TempLoggerMonitoringAlarm.isAllClear_Specified(Index: Integer): boolean;
begin
  Result := FisAllClear_Specified;
end;

procedure VelocityAlarm.SetcurrentVelocity(Index: Integer; const AInt64: Int64);
begin
  FcurrentVelocity := AInt64;
  FcurrentVelocity_Specified := True;
end;

function VelocityAlarm.currentVelocity_Specified(Index: Integer): boolean;
begin
  Result := FcurrentVelocity_Specified;
end;

procedure VelocityAlarm.Setthreshold(Index: Integer; const AInt64: Int64);
begin
  Fthreshold := AInt64;
  Fthreshold_Specified := True;
end;

function VelocityAlarm.threshold_Specified(Index: Integer): boolean;
begin
  Result := Fthreshold_Specified;
end;

procedure HumidityAlarm.SetcurrentHumidity(Index: Integer; const AInt64: Int64);
begin
  FcurrentHumidity := AInt64;
  FcurrentHumidity_Specified := True;
end;

function HumidityAlarm.currentHumidity_Specified(Index: Integer): boolean;
begin
  Result := FcurrentHumidity_Specified;
end;

procedure HumidityAlarm.SetminThreshold(Index: Integer; const AInt64: Int64);
begin
  FminThreshold := AInt64;
  FminThreshold_Specified := True;
end;

function HumidityAlarm.minThreshold_Specified(Index: Integer): boolean;
begin
  Result := FminThreshold_Specified;
end;

procedure HumidityAlarm.SetmaxThreshold(Index: Integer; const AInt64: Int64);
begin
  FmaxThreshold := AInt64;
  FmaxThreshold_Specified := True;
end;

function HumidityAlarm.maxThreshold_Specified(Index: Integer): boolean;
begin
  Result := FmaxThreshold_Specified;
end;

procedure BogieLoadAlarm.SetcurrentBogieLoad(Index: Integer; const ADouble: Double);
begin
  FcurrentBogieLoad := ADouble;
  FcurrentBogieLoad_Specified := True;
end;

function BogieLoadAlarm.currentBogieLoad_Specified(Index: Integer): boolean;
begin
  Result := FcurrentBogieLoad_Specified;
end;

procedure BogieLoadAlarm.Setthreshold(Index: Integer; const AInt64: Int64);
begin
  Fthreshold := AInt64;
  Fthreshold_Specified := True;
end;

function BogieLoadAlarm.threshold_Specified(Index: Integer): boolean;
begin
  Result := Fthreshold_Specified;
end;

procedure Ebs24NAlarm.Setebs24NDelta(Index: Integer; const AInt64: Int64);
begin
  Febs24NDelta := AInt64;
  Febs24NDelta_Specified := True;
end;

function Ebs24NAlarm.ebs24NDelta_Specified(Index: Integer): boolean;
begin
  Result := Febs24NDelta_Specified;
end;

procedure MilageAlarm.SetcurrentMileage(Index: Integer; const ADouble: Double);
begin
  FcurrentMileage := ADouble;
  FcurrentMileage_Specified := True;
end;

function MilageAlarm.currentMileage_Specified(Index: Integer): boolean;
begin
  Result := FcurrentMileage_Specified;
end;

procedure MilageAlarm.Setthreshold(Index: Integer; const AInt64: Int64);
begin
  Fthreshold := AInt64;
  Fthreshold_Specified := True;
end;

function MilageAlarm.threshold_Specified(Index: Integer): boolean;
begin
  Result := Fthreshold_Specified;
end;

procedure LocationDescription.SetDistance(Index: Integer; const ASingle: Single);
begin
  FDistance := ASingle;
  FDistance_Specified := True;
end;

function LocationDescription.Distance_Specified(Index: Integer): boolean;
begin
  Result := FDistance_Specified;
end;

procedure LocationDescription.SetLongitude(Index: Integer; const ASingle: Single);
begin
  FLongitude := ASingle;
  FLongitude_Specified := True;
end;

function LocationDescription.Longitude_Specified(Index: Integer): boolean;
begin
  Result := FLongitude_Specified;
end;

procedure LocationDescription.SetLatitude(Index: Integer; const ASingle: Single);
begin
  FLatitude := ASingle;
  FLatitude_Specified := True;
end;

function LocationDescription.Latitude_Specified(Index: Integer): boolean;
begin
  Result := FLatitude_Specified;
end;

procedure LocationDescription.SetLabel_(Index: Integer; const Astring: string);
begin
  FLabel_ := Astring;
  FLabel__Specified := True;
end;

function LocationDescription.Label__Specified(Index: Integer): boolean;
begin
  Result := FLabel__Specified;
end;

procedure LocationDescription.SetCountry(Index: Integer; const Astring: string);
begin
  FCountry := Astring;
  FCountry_Specified := True;
end;

function LocationDescription.Country_Specified(Index: Integer): boolean;
begin
  Result := FCountry_Specified;
end;

procedure LocationDescription.SetState(Index: Integer; const Astring: string);
begin
  FState := Astring;
  FState_Specified := True;
end;

function LocationDescription.State_Specified(Index: Integer): boolean;
begin
  Result := FState_Specified;
end;

procedure LocationDescription.SetCounty(Index: Integer; const Astring: string);
begin
  FCounty := Astring;
  FCounty_Specified := True;
end;

function LocationDescription.County_Specified(Index: Integer): boolean;
begin
  Result := FCounty_Specified;
end;

procedure LocationDescription.SetCity(Index: Integer; const Astring: string);
begin
  FCity := Astring;
  FCity_Specified := True;
end;

function LocationDescription.City_Specified(Index: Integer): boolean;
begin
  Result := FCity_Specified;
end;

procedure LocationDescription.SetDistrict(Index: Integer; const Astring: string);
begin
  FDistrict := Astring;
  FDistrict_Specified := True;
end;

function LocationDescription.District_Specified(Index: Integer): boolean;
begin
  Result := FDistrict_Specified;
end;

procedure LocationDescription.SetStreet(Index: Integer; const Astring: string);
begin
  FStreet := Astring;
  FStreet_Specified := True;
end;

function LocationDescription.Street_Specified(Index: Integer): boolean;
begin
  Result := FStreet_Specified;
end;

procedure LocationDescription.SetHouseNumber(Index: Integer; const Astring: string);
begin
  FHouseNumber := Astring;
  FHouseNumber_Specified := True;
end;

function LocationDescription.HouseNumber_Specified(Index: Integer): boolean;
begin
  Result := FHouseNumber_Specified;
end;

procedure LocationDescription.SetPostalCode(Index: Integer; const Astring: string);
begin
  FPostalCode := Astring;
  FPostalCode_Specified := True;
end;

function LocationDescription.PostalCode_Specified(Index: Integer): boolean;
begin
  Result := FPostalCode_Specified;
end;

destructor FieldsDef.Destroy;
var
  I: Integer;
begin
  for I := 0 to System.Length(FFieldDef)-1 do
    SysUtils.FreeAndNil(FFieldDef[I]);
  System.SetLength(FFieldDef, 0);
  inherited Destroy;
end;

procedure FieldsDef.SetFieldDef(Index: Integer; const AArray_Of_FieldDef: Array_Of_FieldDef);
begin
  FFieldDef := AArray_Of_FieldDef;
  FFieldDef_Specified := True;
end;

function FieldsDef.FieldDef_Specified(Index: Integer): boolean;
begin
  Result := FFieldDef_Specified;
end;

procedure FieldsDef.SetOrderInfo(Index: Integer; const Astring: string);
begin
  FOrderInfo := Astring;
  FOrderInfo_Specified := True;
end;

function FieldsDef.OrderInfo_Specified(Index: Integer): boolean;
begin
  Result := FOrderInfo_Specified;
end;

destructor TrailerEventMsg.Destroy;
var
  I: Integer;
begin
  for I := 0 to System.Length(FTmInfoDTO)-1 do
    SysUtils.FreeAndNil(FTmInfoDTO[I]);
  System.SetLength(FTmInfoDTO, 0);
  SysUtils.FreeAndNil(FReasonDTO);
  SysUtils.FreeAndNil(FTrailerPositionDTO);
  SysUtils.FreeAndNil(FdeviceTime);
  SysUtils.FreeAndNil(FreceiveTime);
  inherited Destroy;
end;

procedure TrailerEventMsg.SetvehicleId(Index: Integer; const AInt64: Int64);
begin
  FvehicleId := AInt64;
  FvehicleId_Specified := True;
end;

function TrailerEventMsg.vehicleId_Specified(Index: Integer): boolean;
begin
  Result := FvehicleId_Specified;
end;

procedure TrailerEventMsg.SetdriverNameId(Index: Integer; const AInt64: Int64);
begin
  FdriverNameId := AInt64;
  FdriverNameId_Specified := True;
end;

function TrailerEventMsg.driverNameId_Specified(Index: Integer): boolean;
begin
  Result := FdriverNameId_Specified;
end;

procedure TrailerEventMsg.Setcoupled(Index: Integer; const ABoolean: Boolean);
begin
  Fcoupled := ABoolean;
  Fcoupled_Specified := True;
end;

function TrailerEventMsg.coupled_Specified(Index: Integer): boolean;
begin
  Result := Fcoupled_Specified;
end;

procedure TrailerEventMsg.SetignitionOn(Index: Integer; const ABoolean: Boolean);
begin
  FignitionOn := ABoolean;
  FignitionOn_Specified := True;
end;

function TrailerEventMsg.ignitionOn_Specified(Index: Integer): boolean;
begin
  Result := FignitionOn_Specified;
end;

procedure TrailerEventMsg.SetinMotion(Index: Integer; const ABoolean: Boolean);
begin
  FinMotion := ABoolean;
  FinMotion_Specified := True;
end;

function TrailerEventMsg.inMotion_Specified(Index: Integer): boolean;
begin
  Result := FinMotion_Specified;
end;

procedure TrailerEventMsg.SetReasonDTO(Index: Integer; const AReasonDTO: ReasonDTO);
begin
  FReasonDTO := AReasonDTO;
  FReasonDTO_Specified := True;
end;

function TrailerEventMsg.ReasonDTO_Specified(Index: Integer): boolean;
begin
  Result := FReasonDTO_Specified;
end;

procedure TrailerEventMsg.SetTrailerPositionDTO(Index: Integer; const ATrailerPositionDTO: TrailerPositionDTO);
begin
  FTrailerPositionDTO := ATrailerPositionDTO;
  FTrailerPositionDTO_Specified := True;
end;

function TrailerEventMsg.TrailerPositionDTO_Specified(Index: Integer): boolean;
begin
  Result := FTrailerPositionDTO_Specified;
end;

procedure TrailerEventMsg.SetreceiveTime(Index: Integer; const ATXSDateTime: TXSDateTime);
begin
  FreceiveTime := ATXSDateTime;
  FreceiveTime_Specified := True;
end;

function TrailerEventMsg.receiveTime_Specified(Index: Integer): boolean;
begin
  Result := FreceiveTime_Specified;
end;

procedure TrailerEventMsg.SetisDoor1Open(Index: Integer; const ABoolean: Boolean);
begin
  FisDoor1Open := ABoolean;
  FisDoor1Open_Specified := True;
end;

function TrailerEventMsg.isDoor1Open_Specified(Index: Integer): boolean;
begin
  Result := FisDoor1Open_Specified;
end;

procedure TrailerEventMsg.SetisDoor2Open(Index: Integer; const ABoolean: Boolean);
begin
  FisDoor2Open := ABoolean;
  FisDoor2Open_Specified := True;
end;

function TrailerEventMsg.isDoor2Open_Specified(Index: Integer): boolean;
begin
  Result := FisDoor2Open_Specified;
end;

procedure TrailerEventMsg.SetisDoor3Open(Index: Integer; const ABoolean: Boolean);
begin
  FisDoor3Open := ABoolean;
  FisDoor3Open_Specified := True;
end;

function TrailerEventMsg.isDoor3Open_Specified(Index: Integer): boolean;
begin
  Result := FisDoor3Open_Specified;
end;

procedure TrailerEventMsg.SetisDoor4Open(Index: Integer; const ABoolean: Boolean);
begin
  FisDoor4Open := ABoolean;
  FisDoor4Open_Specified := True;
end;

function TrailerEventMsg.isDoor4Open_Specified(Index: Integer): boolean;
begin
  Result := FisDoor4Open_Specified;
end;

procedure TrailerEventMsg.SetTmInfoDTO(Index: Integer; const AArray_Of_TmInfoDTO: Array_Of_TmInfoDTO);
begin
  FTmInfoDTO := AArray_Of_TmInfoDTO;
  FTmInfoDTO_Specified := True;
end;

function TrailerEventMsg.TmInfoDTO_Specified(Index: Integer): boolean;
begin
  Result := FTmInfoDTO_Specified;
end;

procedure TmInfoDTO.SetdocumentInfo(Index: Integer; const Astring: string);
begin
  FdocumentInfo := Astring;
  FdocumentInfo_Specified := True;
end;

function TmInfoDTO.documentInfo_Specified(Index: Integer): boolean;
begin
  Result := FdocumentInfo_Specified;
end;

procedure ForeignRef.Setname_(Index: Integer; const Astring: string);
begin
  Fname_ := Astring;
  Fname__Specified := True;
end;

function ForeignRef.name__Specified(Index: Integer): boolean;
begin
  Result := Fname__Specified;
end;

procedure ForeignRef.Setvalue(Index: Integer; const Astring: string);
begin
  Fvalue := Astring;
  Fvalue_Specified := True;
end;

function ForeignRef.value_Specified(Index: Integer): boolean;
begin
  Result := Fvalue_Specified;
end;

destructor VehiclestatusMsg.Destroy;
begin
  SysUtils.FreeAndNil(FForm);
  inherited Destroy;
end;

procedure VehiclestatusMsg.SetForm(Index: Integer; const AForm: Form);
begin
  FForm := AForm;
  FForm_Specified := True;
end;

function VehiclestatusMsg.Form_Specified(Index: Integer): boolean;
begin
  Result := FForm_Specified;
end;

procedure RefID.Setdoc(Index: Integer; const Astring: string);
begin
  Fdoc := Astring;
  Fdoc_Specified := True;
end;

function RefID.doc_Specified(Index: Integer): boolean;
begin
  Result := Fdoc_Specified;
end;

destructor TourstatusMsg.Destroy;
begin
  SysUtils.FreeAndNil(FForm);
  inherited Destroy;
end;

procedure TourstatusMsg.SetAddInformation(Index: Integer; const Astring: string);
begin
  FAddInformation := Astring;
  FAddInformation_Specified := True;
end;

function TourstatusMsg.AddInformation_Specified(Index: Integer): boolean;
begin
  Result := FAddInformation_Specified;
end;

procedure TourstatusMsg.SetForm(Index: Integer; const AForm: Form);
begin
  FForm := AForm;
  FForm_Specified := True;
end;

function TourstatusMsg.Form_Specified(Index: Integer): boolean;
begin
  Result := FForm_Specified;
end;

destructor ReplystatusMsg.Destroy;
begin
  SysUtils.FreeAndNil(FRefID);
  inherited Destroy;
end;

procedure ReplystatusMsg.SetRefID(Index: Integer; const ARefID: RefID);
begin
  FRefID := ARefID;
  FRefID_Specified := True;
end;

function ReplystatusMsg.RefID_Specified(Index: Integer): boolean;
begin
  Result := FRefID_Specified;
end;

destructor Body.Destroy;
begin
  SysUtils.FreeAndNil(FOrderMsg);
  SysUtils.FreeAndNil(FStatusDefMsg);
  SysUtils.FreeAndNil(FFormDefMsg);
  SysUtils.FreeAndNil(FAutoPosDef);
  SysUtils.FreeAndNil(FVehiclestatusMsg);
  SysUtils.FreeAndNil(FDriverStatusMsg);
  SysUtils.FreeAndNil(FOrderstatusMsg);
  SysUtils.FreeAndNil(FTourstatusMsg);
  SysUtils.FreeAndNil(FReplystatusMsg);
  SysUtils.FreeAndNil(FCacheMsg);
  SysUtils.FreeAndNil(FInboxDataMsg);
  SysUtils.FreeAndNil(FEventStatusMsg);
  SysUtils.FreeAndNil(FPairingMsg);
  SysUtils.FreeAndNil(FTrailerEventMsg);
  SysUtils.FreeAndNil(FTrailerRegisterMsg);
  SysUtils.FreeAndNil(FTrailerSyncMsg);
  SysUtils.FreeAndNil(FTourEventUptimeMsg);
  SysUtils.FreeAndNil(FTrailerCouplingMsg);
  inherited Destroy;
end;

procedure Body.SetFreetextMsg(Index: Integer; const Astring: string);
begin
  FFreetextMsg := Astring;
  FFreetextMsg_Specified := True;
end;

function Body.FreetextMsg_Specified(Index: Integer): boolean;
begin
  Result := FFreetextMsg_Specified;
end;

procedure Body.SetOrderMsg(Index: Integer; const AOrderMsg: OrderMsg);
begin
  FOrderMsg := AOrderMsg;
  FOrderMsg_Specified := True;
end;

function Body.OrderMsg_Specified(Index: Integer): boolean;
begin
  Result := FOrderMsg_Specified;
end;

procedure Body.SetStatusDefMsg(Index: Integer; const AStatusDefMsg: StatusDefMsg);
begin
  FStatusDefMsg := AStatusDefMsg;
  FStatusDefMsg_Specified := True;
end;

function Body.StatusDefMsg_Specified(Index: Integer): boolean;
begin
  Result := FStatusDefMsg_Specified;
end;

procedure Body.SetFormDefMsg(Index: Integer; const AFormDefMsg: FormDefMsg);
begin
  FFormDefMsg := AFormDefMsg;
  FFormDefMsg_Specified := True;
end;

function Body.FormDefMsg_Specified(Index: Integer): boolean;
begin
  Result := FFormDefMsg_Specified;
end;

procedure Body.SetAutoPosDef(Index: Integer; const AAutoPosDef: AutoPosDef);
begin
  FAutoPosDef := AAutoPosDef;
  FAutoPosDef_Specified := True;
end;

function Body.AutoPosDef_Specified(Index: Integer): boolean;
begin
  Result := FAutoPosDef_Specified;
end;

procedure Body.SetVehiclestatusMsg(Index: Integer; const AVehiclestatusMsg: VehiclestatusMsg);
begin
  FVehiclestatusMsg := AVehiclestatusMsg;
  FVehiclestatusMsg_Specified := True;
end;

function Body.VehiclestatusMsg_Specified(Index: Integer): boolean;
begin
  Result := FVehiclestatusMsg_Specified;
end;

procedure Body.SetDriverStatusMsg(Index: Integer; const ADriverStatusMsg: DriverStatusMsg);
begin
  FDriverStatusMsg := ADriverStatusMsg;
  FDriverStatusMsg_Specified := True;
end;

function Body.DriverStatusMsg_Specified(Index: Integer): boolean;
begin
  Result := FDriverStatusMsg_Specified;
end;

procedure Body.SetOrderstatusMsg(Index: Integer; const AOrderstatusMsg: OrderstatusMsg);
begin
  FOrderstatusMsg := AOrderstatusMsg;
  FOrderstatusMsg_Specified := True;
end;

function Body.OrderstatusMsg_Specified(Index: Integer): boolean;
begin
  Result := FOrderstatusMsg_Specified;
end;

procedure Body.SetTourstatusMsg(Index: Integer; const ATourstatusMsg: TourstatusMsg);
begin
  FTourstatusMsg := ATourstatusMsg;
  FTourstatusMsg_Specified := True;
end;

function Body.TourstatusMsg_Specified(Index: Integer): boolean;
begin
  Result := FTourstatusMsg_Specified;
end;

procedure Body.SetReplystatusMsg(Index: Integer; const AReplystatusMsg: ReplystatusMsg);
begin
  FReplystatusMsg := AReplystatusMsg;
  FReplystatusMsg_Specified := True;
end;

function Body.ReplystatusMsg_Specified(Index: Integer): boolean;
begin
  Result := FReplystatusMsg_Specified;
end;

procedure Body.SetSimcardChgMsg(Index: Integer; const Astring: string);
begin
  FSimcardChgMsg := Astring;
  FSimcardChgMsg_Specified := True;
end;

function Body.SimcardChgMsg_Specified(Index: Integer): boolean;
begin
  Result := FSimcardChgMsg_Specified;
end;

procedure Body.SetHexMsg(Index: Integer; const Astring: string);
begin
  FHexMsg := Astring;
  FHexMsg_Specified := True;
end;

function Body.HexMsg_Specified(Index: Integer): boolean;
begin
  Result := FHexMsg_Specified;
end;

procedure Body.SetCacheMsg(Index: Integer; const ACacheMsg: CacheMsg);
begin
  FCacheMsg := ACacheMsg;
  FCacheMsg_Specified := True;
end;

function Body.CacheMsg_Specified(Index: Integer): boolean;
begin
  Result := FCacheMsg_Specified;
end;

procedure Body.SetTextConfMsg(Index: Integer; const ATextConfMsg: TextConfMsg);
begin
  FTextConfMsg := ATextConfMsg;
  FTextConfMsg_Specified := True;
end;

function Body.TextConfMsg_Specified(Index: Integer): boolean;
begin
  Result := FTextConfMsg_Specified;
end;

procedure Body.SetVehiclereplyMsg(Index: Integer; const Astring: string);
begin
  FVehiclereplyMsg := Astring;
  FVehiclereplyMsg_Specified := True;
end;

function Body.VehiclereplyMsg_Specified(Index: Integer): boolean;
begin
  Result := FVehiclereplyMsg_Specified;
end;

procedure Body.SetInboxDataMsg(Index: Integer; const AInboxDataMsg: InboxDataMsg);
begin
  FInboxDataMsg := AInboxDataMsg;
  FInboxDataMsg_Specified := True;
end;

function Body.InboxDataMsg_Specified(Index: Integer): boolean;
begin
  Result := FInboxDataMsg_Specified;
end;

procedure Body.SetEventStatusMsg(Index: Integer; const AEventStatusMsg: EventStatusMsg);
begin
  FEventStatusMsg := AEventStatusMsg;
  FEventStatusMsg_Specified := True;
end;

function Body.EventStatusMsg_Specified(Index: Integer): boolean;
begin
  Result := FEventStatusMsg_Specified;
end;

procedure Body.SetPairingMsg(Index: Integer; const APairingMsg: PairingMsg);
begin
  FPairingMsg := APairingMsg;
  FPairingMsg_Specified := True;
end;

function Body.PairingMsg_Specified(Index: Integer): boolean;
begin
  Result := FPairingMsg_Specified;
end;

procedure Body.SetTrailerEventMsg(Index: Integer; const ATrailerEventMsg: TrailerEventMsg);
begin
  FTrailerEventMsg := ATrailerEventMsg;
  FTrailerEventMsg_Specified := True;
end;

function Body.TrailerEventMsg_Specified(Index: Integer): boolean;
begin
  Result := FTrailerEventMsg_Specified;
end;

procedure Body.SetTrailerRegisterMsg(Index: Integer; const ATrailerRegisterMsg: TrailerRegisterMsg);
begin
  FTrailerRegisterMsg := ATrailerRegisterMsg;
  FTrailerRegisterMsg_Specified := True;
end;

function Body.TrailerRegisterMsg_Specified(Index: Integer): boolean;
begin
  Result := FTrailerRegisterMsg_Specified;
end;

procedure Body.SetTrailerSyncMsg(Index: Integer; const ATrailerSyncMsg: TrailerSyncMsg);
begin
  FTrailerSyncMsg := ATrailerSyncMsg;
  FTrailerSyncMsg_Specified := True;
end;

function Body.TrailerSyncMsg_Specified(Index: Integer): boolean;
begin
  Result := FTrailerSyncMsg_Specified;
end;

procedure Body.SetTourEventUptimeMsg(Index: Integer; const ATourEventUptimeMsg: TourEventUptimeMsg);
begin
  FTourEventUptimeMsg := ATourEventUptimeMsg;
  FTourEventUptimeMsg_Specified := True;
end;

function Body.TourEventUptimeMsg_Specified(Index: Integer): boolean;
begin
  Result := FTourEventUptimeMsg_Specified;
end;

procedure Body.SetTrailerCouplingMsg(Index: Integer; const ATrailerCouplingMsg: TrailerCouplingMsg);
begin
  FTrailerCouplingMsg := ATrailerCouplingMsg;
  FTrailerCouplingMsg_Specified := True;
end;

function Body.TrailerCouplingMsg_Specified(Index: Integer): boolean;
begin
  Result := FTrailerCouplingMsg_Specified;
end;

destructor EventStatusMsg.Destroy;
begin
  SysUtils.FreeAndNil(FForm);
  inherited Destroy;
end;

procedure EventStatusMsg.SetAddInformation(Index: Integer; const Astring: string);
begin
  FAddInformation := Astring;
  FAddInformation_Specified := True;
end;

function EventStatusMsg.AddInformation_Specified(Index: Integer): boolean;
begin
  Result := FAddInformation_Specified;
end;

procedure EventStatusMsg.SetForm(Index: Integer; const AForm: Form);
begin
  FForm := AForm;
  FForm_Specified := True;
end;

function EventStatusMsg.Form_Specified(Index: Integer): boolean;
begin
  Result := FForm_Specified;
end;

destructor OrderstatusMsg.Destroy;
begin
  SysUtils.FreeAndNil(FForm);
  inherited Destroy;
end;

procedure OrderstatusMsg.SetAddInformation(Index: Integer; const Astring: string);
begin
  FAddInformation := Astring;
  FAddInformation_Specified := True;
end;

function OrderstatusMsg.AddInformation_Specified(Index: Integer): boolean;
begin
  Result := FAddInformation_Specified;
end;

procedure OrderstatusMsg.SetForm(Index: Integer; const AForm: Form);
begin
  FForm := AForm;
  FForm_Specified := True;
end;

function OrderstatusMsg.Form_Specified(Index: Integer): boolean;
begin
  Result := FForm_Specified;
end;

destructor InboxDataMsg.Destroy;
begin
  SysUtils.FreeAndNil(FAreaMonitoring);
  inherited Destroy;
end;

procedure InboxDataMsg.SetTrailerName(Index: Integer; const Astring: string);
begin
  FTrailerName := Astring;
  FTrailerName_Specified := True;
end;

function InboxDataMsg.TrailerName_Specified(Index: Integer): boolean;
begin
  Result := FTrailerName_Specified;
end;

procedure InboxDataMsg.SetAreaMonitoring(Index: Integer; const AAreaMonitoring: AreaMonitoring);
begin
  FAreaMonitoring := AAreaMonitoring;
  FAreaMonitoring_Specified := True;
end;

function InboxDataMsg.AreaMonitoring_Specified(Index: Integer): boolean;
begin
  Result := FAreaMonitoring_Specified;
end;

procedure AutoPos.SetInterval(Index: Integer; const Astring: string);
begin
  FInterval := Astring;
  FInterval_Specified := True;
end;

function AutoPos.Interval_Specified(Index: Integer): boolean;
begin
  Result := FInterval_Specified;
end;

procedure AutoPos.SetDistance(Index: Integer; const Astring: string);
begin
  FDistance := Astring;
  FDistance_Specified := True;
end;

function AutoPos.Distance_Specified(Index: Integer): boolean;
begin
  Result := FDistance_Specified;
end;

procedure AutoPos.SetTimes(Index: Integer; const ATimes: Times);
begin
  FTimes := ATimes;
  FTimes_Specified := True;
end;

function AutoPos.Times_Specified(Index: Integer): boolean;
begin
  Result := FTimes_Specified;
end;

procedure AutoPos.SetZonetype(Index: Integer; const Astring: string);
begin
  FZonetype := Astring;
  FZonetype_Specified := True;
end;

function AutoPos.Zonetype_Specified(Index: Integer): boolean;
begin
  Result := FZonetype_Specified;
end;

procedure AutoPos.SetRefPOIsID(Index: Integer; const Astring: string);
begin
  FRefPOIsID := Astring;
  FRefPOIsID_Specified := True;
end;

function AutoPos.RefPOIsID_Specified(Index: Integer): boolean;
begin
  Result := FRefPOIsID_Specified;
end;

procedure AutoPos.SetAccmode(Index: Integer; const Astring: string);
begin
  FAccmode := Astring;
  FAccmode_Specified := True;
end;

function AutoPos.Accmode_Specified(Index: Integer): boolean;
begin
  Result := FAccmode_Specified;
end;

procedure SetpointData.Settemperature(Index: Integer; const ADouble: Double);
begin
  Ftemperature := ADouble;
  Ftemperature_Specified := True;
end;

function SetpointData.temperature_Specified(Index: Integer): boolean;
begin
  Result := Ftemperature_Specified;
end;

procedure SetpointData.SetreturnAirTemperature(Index: Integer; const ADouble: Double);
begin
  FreturnAirTemperature := ADouble;
  FreturnAirTemperature_Specified := True;
end;

function SetpointData.returnAirTemperature_Specified(Index: Integer): boolean;
begin
  Result := FreturnAirTemperature_Specified;
end;

procedure SetpointData.SetsupplyTemperature(Index: Integer; const ADouble: Double);
begin
  FsupplyTemperature := ADouble;
  FsupplyTemperature_Specified := True;
end;

function SetpointData.supplyTemperature_Specified(Index: Integer): boolean;
begin
  Result := FsupplyTemperature_Specified;
end;

procedure SetpointData.SetevaporatorCoilTemperature(Index: Integer; const ADouble: Double);
begin
  FevaporatorCoilTemperature := ADouble;
  FevaporatorCoilTemperature_Specified := True;
end;

function SetpointData.evaporatorCoilTemperature_Specified(Index: Integer): boolean;
begin
  Result := FevaporatorCoilTemperature_Specified;
end;

procedure SetpointData.SetcompartmentOperationMode(Index: Integer; const Astring: string);
begin
  FcompartmentOperationMode := Astring;
  FcompartmentOperationMode_Specified := True;
end;

function SetpointData.compartmentOperationMode_Specified(Index: Integer): boolean;
begin
  Result := FcompartmentOperationMode_Specified;
end;

destructor TrailerPositionDTO.Destroy;
begin
  SysUtils.FreeAndNil(FpositionDataTime);
  SysUtils.FreeAndNil(FgpsTime);
  inherited Destroy;
end;

procedure TrailerPositionDTO.SetpositionDataTime(Index: Integer; const ATXSDateTime: TXSDateTime);
begin
  FpositionDataTime := ATXSDateTime;
  FpositionDataTime_Specified := True;
end;

function TrailerPositionDTO.positionDataTime_Specified(Index: Integer): boolean;
begin
  Result := FpositionDataTime_Specified;
end;

procedure TrailerPositionDTO.SetgpsTime(Index: Integer; const ATXSDateTime: TXSDateTime);
begin
  FgpsTime := ATXSDateTime;
  FgpsTime_Specified := True;
end;

function TrailerPositionDTO.gpsTime_Specified(Index: Integer): boolean;
begin
  Result := FgpsTime_Specified;
end;

procedure TrailerPositionDTO.Setlongitude(Index: Integer; const ADouble: Double);
begin
  Flongitude := ADouble;
  Flongitude_Specified := True;
end;

function TrailerPositionDTO.longitude_Specified(Index: Integer): boolean;
begin
  Result := Flongitude_Specified;
end;

procedure TrailerPositionDTO.Setlatitude(Index: Integer; const ADouble: Double);
begin
  Flatitude := ADouble;
  Flatitude_Specified := True;
end;

function TrailerPositionDTO.latitude_Specified(Index: Integer): boolean;
begin
  Result := Flatitude_Specified;
end;

procedure TrailerPositionDTO.SetgpsHeading(Index: Integer; const AInt64: Int64);
begin
  FgpsHeading := AInt64;
  FgpsHeading_Specified := True;
end;

function TrailerPositionDTO.gpsHeading_Specified(Index: Integer): boolean;
begin
  Result := FgpsHeading_Specified;
end;

procedure TrailerPositionDTO.SetgpsSpeed(Index: Integer; const AInt64: Int64);
begin
  FgpsSpeed := AInt64;
  FgpsSpeed_Specified := True;
end;

function TrailerPositionDTO.gpsSpeed_Specified(Index: Integer): boolean;
begin
  Result := FgpsSpeed_Specified;
end;

procedure TrailerPositionDTO.SetgpsMileage(Index: Integer; const ADouble: Double);
begin
  FgpsMileage := ADouble;
  FgpsMileage_Specified := True;
end;

function TrailerPositionDTO.gpsMileage_Specified(Index: Integer): boolean;
begin
  Result := FgpsMileage_Specified;
end;

procedure TrailerPositionDTO.SetpositionText(Index: Integer; const Astring: string);
begin
  FpositionText := Astring;
  FpositionText_Specified := True;
end;

function TrailerPositionDTO.positionText_Specified(Index: Integer): boolean;
begin
  Result := FpositionText_Specified;
end;

destructor FormDefMsg.Destroy;
begin
  SysUtils.FreeAndNil(FFieldsDef);
  inherited Destroy;
end;

procedure FormDefMsg.SetFormDefInoID(Index: Integer; const Astring: string);
begin
  FFormDefInoID := Astring;
  FFormDefInoID_Specified := True;
end;

function FormDefMsg.FormDefInoID_Specified(Index: Integer): boolean;
begin
  Result := FFormDefInoID_Specified;
end;

procedure LastPositionState.Settimestamp(Index: Integer; const AtimestampType: timestampType);
begin
  Ftimestamp := AtimestampType;
  Ftimestamp_Specified := True;
end;

function LastPositionState.timestamp_Specified(Index: Integer): boolean;
begin
  Result := Ftimestamp_Specified;
end;

procedure TPDate.SetBegin_(Index: Integer; const AtimestampType: timestampType);
begin
  FBegin_ := AtimestampType;
  FBegin__Specified := True;
end;

function TPDate.Begin__Specified(Index: Integer): boolean;
begin
  Result := FBegin__Specified;
end;

procedure TPDate.SetEnd_(Index: Integer; const AtimestampType: timestampType);
begin
  FEnd_ := AtimestampType;
  FEnd__Specified := True;
end;

function TPDate.End__Specified(Index: Integer): boolean;
begin
  Result := FEnd__Specified;
end;

procedure Position2.SetVehicleTimestamp(Index: Integer; const AtimestampType: timestampType);
begin
  FVehicleTimestamp := AtimestampType;
  FVehicleTimestamp_Specified := True;
end;

function Position2.VehicleTimestamp_Specified(Index: Integer): boolean;
begin
  Result := FVehicleTimestamp_Specified;
end;

procedure Position2.SetServerTimestamp(Index: Integer; const AtimestampType: timestampType);
begin
  FServerTimestamp := AtimestampType;
  FServerTimestamp_Specified := True;
end;

function Position2.ServerTimestamp_Specified(Index: Integer): boolean;
begin
  Result := FServerTimestamp_Specified;
end;

procedure CacheMsg.Setdoc_inoid(Index: Integer; const AinoidType: inoidType);
begin
  Fdoc_inoid := AinoidType;
  Fdoc_inoid_Specified := True;
end;

function CacheMsg.doc_inoid_Specified(Index: Integer): boolean;
begin
  Result := Fdoc_inoid_Specified;
end;

procedure GetLastPositionRequestType.Setsessionid(Index: Integer; const AsessionidType: sessionidType);
begin
  Fsessionid := AsessionidType;
  Fsessionid_Specified := True;
end;

function GetLastPositionRequestType.sessionid_Specified(Index: Integer): boolean;
begin
  Result := Fsessionid_Specified;
end;

procedure GetLastPositionRequestType.Setlimit(Index: Integer; const AlimitType: limitType);
begin
  Flimit := AlimitType;
  Flimit_Specified := True;
end;

function GetLastPositionRequestType.limit_Specified(Index: Integer): boolean;
begin
  Result := Flimit_Specified;
end;

procedure GetLastPositionRequestType.Setoffset(Index: Integer; const AoffsetType: offsetType);
begin
  Foffset := AoffsetType;
  Foffset_Specified := True;
end;

function GetLastPositionRequestType.offset_Specified(Index: Integer): boolean;
begin
  Result := Foffset_Specified;
end;

procedure GetLastPositionRequestType.SetGroupID(Index: Integer; const AInt64: Int64);
begin
  FGroupID := AInt64;
  FGroupID_Specified := True;
end;

function GetLastPositionRequestType.GroupID_Specified(Index: Integer): boolean;
begin
  Result := FGroupID_Specified;
end;

procedure GetLastPositionRequestType.SetEntityID(Index: Integer; const AArray_Of_long: Array_Of_long);
begin
  FEntityID := AArray_Of_long;
  FEntityID_Specified := True;
end;

function GetLastPositionRequestType.EntityID_Specified(Index: Integer): boolean;
begin
  Result := FEntityID_Specified;
end;

procedure GetLastPositionRequestType.SetAdditionalState(Index: Integer; const AArray_Of_AdditionalState: Array_Of_AdditionalState);
begin
  FAdditionalState := AArray_Of_AdditionalState;
  FAdditionalState_Specified := True;
end;

function GetLastPositionRequestType.AdditionalState_Specified(Index: Integer): boolean;
begin
  Result := FAdditionalState_Specified;
end;

destructor GetLocationDescriptionResponseType.Destroy;
var
  I: Integer;
begin
  for I := 0 to System.Length(FLocationDescription)-1 do
    SysUtils.FreeAndNil(FLocationDescription[I]);
  System.SetLength(FLocationDescription, 0);
  inherited Destroy;
end;

procedure GetLocationDescriptionResponseType.SetresultSize(Index: Integer; const AresultSizeType: resultSizeType);
begin
  FresultSize := AresultSizeType;
  FresultSize_Specified := True;
end;

function GetLocationDescriptionResponseType.resultSize_Specified(Index: Integer): boolean;
begin
  Result := FresultSize_Specified;
end;

procedure GetLocationDescriptionResponseType.SetresponseSize(Index: Integer; const AresponseSizeType: responseSizeType);
begin
  FresponseSize := AresponseSizeType;
  FresponseSize_Specified := True;
end;

function GetLocationDescriptionResponseType.responseSize_Specified(Index: Integer): boolean;
begin
  Result := FresponseSize_Specified;
end;

procedure GetLocationDescriptionResponseType.SetLocationDescription(Index: Integer; const AArray_Of_LocationDescription: Array_Of_LocationDescription);
begin
  FLocationDescription := AArray_Of_LocationDescription;
  FLocationDescription_Specified := True;
end;

function GetLocationDescriptionResponseType.LocationDescription_Specified(Index: Integer): boolean;
begin
  Result := FLocationDescription_Specified;
end;

destructor GetLastPositionResponseType.Destroy;
var
  I: Integer;
begin
  for I := 0 to System.Length(FPositions)-1 do
    SysUtils.FreeAndNil(FPositions[I]);
  System.SetLength(FPositions, 0);
  inherited Destroy;
end;

procedure GetLastPositionResponseType.Setlimit(Index: Integer; const AlimitType: limitType);
begin
  Flimit := AlimitType;
  Flimit_Specified := True;
end;

function GetLastPositionResponseType.limit_Specified(Index: Integer): boolean;
begin
  Result := Flimit_Specified;
end;

procedure GetLastPositionResponseType.Setoffset(Index: Integer; const AoffsetType: offsetType);
begin
  Foffset := AoffsetType;
  Foffset_Specified := True;
end;

function GetLastPositionResponseType.offset_Specified(Index: Integer): boolean;
begin
  Result := Foffset_Specified;
end;

procedure GetLastPositionResponseType.SetresultSize(Index: Integer; const AresultSizeType: resultSizeType);
begin
  FresultSize := AresultSizeType;
  FresultSize_Specified := True;
end;

function GetLastPositionResponseType.resultSize_Specified(Index: Integer): boolean;
begin
  Result := FresultSize_Specified;
end;

procedure GetLastPositionResponseType.SetresponseSize(Index: Integer; const AresponseSizeType: responseSizeType);
begin
  FresponseSize := AresponseSizeType;
  FresponseSize_Specified := True;
end;

function GetLastPositionResponseType.responseSize_Specified(Index: Integer): boolean;
begin
  Result := FresponseSize_Specified;
end;

procedure GetLastPositionResponseType.SetPositions(Index: Integer; const AArray_Of_Positions: Array_Of_Positions);
begin
  FPositions := AArray_Of_Positions;
  FPositions_Specified := True;
end;

function GetLastPositionResponseType.Positions_Specified(Index: Integer): boolean;
begin
  Result := FPositions_Specified;
end;

destructor GetReplyResponseType.Destroy;
var
  I: Integer;
begin
  for I := 0 to System.Length(FMsg)-1 do
    SysUtils.FreeAndNil(FMsg[I]);
  System.SetLength(FMsg, 0);
  inherited Destroy;
end;

procedure GetReplyResponseType.Setlimit(Index: Integer; const AlimitType: limitType);
begin
  Flimit := AlimitType;
  Flimit_Specified := True;
end;

function GetReplyResponseType.limit_Specified(Index: Integer): boolean;
begin
  Result := Flimit_Specified;
end;

procedure GetReplyResponseType.Setoffset(Index: Integer; const AoffsetType: offsetType);
begin
  Foffset := AoffsetType;
  Foffset_Specified := True;
end;

function GetReplyResponseType.offset_Specified(Index: Integer): boolean;
begin
  Result := Foffset_Specified;
end;

procedure GetReplyResponseType.SetresultSize(Index: Integer; const AresultSizeType: resultSizeType);
begin
  FresultSize := AresultSizeType;
  FresultSize_Specified := True;
end;

function GetReplyResponseType.resultSize_Specified(Index: Integer): boolean;
begin
  Result := FresultSize_Specified;
end;

procedure GetReplyResponseType.SetresponseSize(Index: Integer; const AresponseSizeType: responseSizeType);
begin
  FresponseSize := AresponseSizeType;
  FresponseSize_Specified := True;
end;

function GetReplyResponseType.responseSize_Specified(Index: Integer): boolean;
begin
  Result := FresponseSize_Specified;
end;

procedure GetReplyResponseType.SetMsg(Index: Integer; const AArray_Of_Msg: Array_Of_Msg);
begin
  FMsg := AArray_Of_Msg;
  FMsg_Specified := True;
end;

function GetReplyResponseType.Msg_Specified(Index: Integer): boolean;
begin
  Result := FMsg_Specified;
end;

destructor GetPosReportTraceResponseType.Destroy;
var
  I: Integer;
begin
  for I := 0 to System.Length(FPosition)-1 do
    SysUtils.FreeAndNil(FPosition[I]);
  System.SetLength(FPosition, 0);
  inherited Destroy;
end;

procedure GetPosReportTraceResponseType.Setlimit(Index: Integer; const AlimitType: limitType);
begin
  Flimit := AlimitType;
  Flimit_Specified := True;
end;

function GetPosReportTraceResponseType.limit_Specified(Index: Integer): boolean;
begin
  Result := Flimit_Specified;
end;

procedure GetPosReportTraceResponseType.Setoffset(Index: Integer; const AoffsetType: offsetType);
begin
  Foffset := AoffsetType;
  Foffset_Specified := True;
end;

function GetPosReportTraceResponseType.offset_Specified(Index: Integer): boolean;
begin
  Result := Foffset_Specified;
end;

procedure GetPosReportTraceResponseType.SetresultSize(Index: Integer; const AresultSizeType: resultSizeType);
begin
  FresultSize := AresultSizeType;
  FresultSize_Specified := True;
end;

function GetPosReportTraceResponseType.resultSize_Specified(Index: Integer): boolean;
begin
  Result := FresultSize_Specified;
end;

procedure GetPosReportTraceResponseType.SetresponseSize(Index: Integer; const AresponseSizeType: responseSizeType);
begin
  FresponseSize := AresponseSizeType;
  FresponseSize_Specified := True;
end;

function GetPosReportTraceResponseType.responseSize_Specified(Index: Integer): boolean;
begin
  Result := FresponseSize_Specified;
end;

procedure GetPosReportTraceResponseType.SetPosition(Index: Integer; const AArray_Of_Position: Array_Of_Position);
begin
  FPosition := AArray_Of_Position;
  FPosition_Specified := True;
end;

function GetPosReportTraceResponseType.Position_Specified(Index: Integer): boolean;
begin
  Result := FPosition_Specified;
end;

destructor GetCurrentStateResponseType.Destroy;
var
  I: Integer;
begin
  for I := 0 to System.Length(FMsg)-1 do
    SysUtils.FreeAndNil(FMsg[I]);
  System.SetLength(FMsg, 0);
  inherited Destroy;
end;

procedure GetCurrentStateResponseType.Setlimit(Index: Integer; const AlimitType: limitType);
begin
  Flimit := AlimitType;
  Flimit_Specified := True;
end;

function GetCurrentStateResponseType.limit_Specified(Index: Integer): boolean;
begin
  Result := Flimit_Specified;
end;

procedure GetCurrentStateResponseType.Setoffset(Index: Integer; const AoffsetType: offsetType);
begin
  Foffset := AoffsetType;
  Foffset_Specified := True;
end;

function GetCurrentStateResponseType.offset_Specified(Index: Integer): boolean;
begin
  Result := Foffset_Specified;
end;

procedure GetCurrentStateResponseType.SetresultSize(Index: Integer; const AresultSizeType: resultSizeType);
begin
  FresultSize := AresultSizeType;
  FresultSize_Specified := True;
end;

function GetCurrentStateResponseType.resultSize_Specified(Index: Integer): boolean;
begin
  Result := FresultSize_Specified;
end;

procedure GetCurrentStateResponseType.SetresponseSize(Index: Integer; const AresponseSizeType: responseSizeType);
begin
  FresponseSize := AresponseSizeType;
  FresponseSize_Specified := True;
end;

function GetCurrentStateResponseType.responseSize_Specified(Index: Integer): boolean;
begin
  Result := FresponseSize_Specified;
end;

procedure GetCurrentStateResponseType.SetMsg(Index: Integer; const AArray_Of_Msg: Array_Of_Msg);
begin
  FMsg := AArray_Of_Msg;
  FMsg_Specified := True;
end;

function GetCurrentStateResponseType.Msg_Specified(Index: Integer): boolean;
begin
  Result := FMsg_Specified;
end;

destructor GetLocationDescriptionRequestType.Destroy;
var
  I: Integer;
begin
  for I := 0 to System.Length(FCoordinate)-1 do
    SysUtils.FreeAndNil(FCoordinate[I]);
  System.SetLength(FCoordinate, 0);
  inherited Destroy;
end;

procedure GetLocationDescriptionRequestType.Setsessionid(Index: Integer; const AsessionidType: sessionidType);
begin
  Fsessionid := AsessionidType;
  Fsessionid_Specified := True;
end;

function GetLocationDescriptionRequestType.sessionid_Specified(Index: Integer): boolean;
begin
  Result := Fsessionid_Specified;
end;

procedure GetLocationDescriptionRequestType.Setversion(Index: Integer; const AversionType: versionType);
begin
  Fversion := AversionType;
  Fversion_Specified := True;
end;

function GetLocationDescriptionRequestType.version_Specified(Index: Integer): boolean;
begin
  Result := Fversion_Specified;
end;

procedure GetLocationDescriptionRequestType.SetLanguage(Index: Integer; const Astring: string);
begin
  FLanguage := Astring;
  FLanguage_Specified := True;
end;

function GetLocationDescriptionRequestType.Language_Specified(Index: Integer): boolean;
begin
  Result := FLanguage_Specified;
end;

procedure GetLocationDescriptionRequestType.SetCoordinate(Index: Integer; const AArray_Of_Coordinate: Array_Of_Coordinate);
begin
  FCoordinate := AArray_Of_Coordinate;
  FCoordinate_Specified := True;
end;

function GetLocationDescriptionRequestType.Coordinate_Specified(Index: Integer): boolean;
begin
  Result := FCoordinate_Specified;
end;

destructor GetReplyRequestType.Destroy;
begin
  SysUtils.FreeAndNil(FServerTimestamp);
  inherited Destroy;
end;

procedure GetReplyRequestType.Setsessionid(Index: Integer; const AsessionidType: sessionidType);
begin
  Fsessionid := AsessionidType;
  Fsessionid_Specified := True;
end;

function GetReplyRequestType.sessionid_Specified(Index: Integer): boolean;
begin
  Result := Fsessionid_Specified;
end;

procedure GetReplyRequestType.Setlimit(Index: Integer; const AlimitType: limitType);
begin
  Flimit := AlimitType;
  Flimit_Specified := True;
end;

function GetReplyRequestType.limit_Specified(Index: Integer): boolean;
begin
  Result := Flimit_Specified;
end;

procedure GetReplyRequestType.Setoffset(Index: Integer; const AoffsetType: offsetType);
begin
  Foffset := AoffsetType;
  Foffset_Specified := True;
end;

function GetReplyRequestType.offset_Specified(Index: Integer): boolean;
begin
  Result := Foffset_Specified;
end;

procedure GetReplyRequestType.Setversion(Index: Integer; const AversionType: versionType);
begin
  Fversion := AversionType;
  Fversion_Specified := True;
end;

function GetReplyRequestType.version_Specified(Index: Integer): boolean;
begin
  Result := Fversion_Specified;
end;

procedure GetReplyRequestType.SetVehicleID(Index: Integer; const AArray_Of_vehicleidType: Array_Of_vehicleidType);
begin
  FVehicleID := AArray_Of_vehicleidType;
  FVehicleID_Specified := True;
end;

function GetReplyRequestType.VehicleID_Specified(Index: Integer): boolean;
begin
  Result := FVehicleID_Specified;
end;

procedure GetReplyRequestType.SetServerTimestamp(Index: Integer; const ATPDate: TPDate);
begin
  FServerTimestamp := ATPDate;
  FServerTimestamp_Specified := True;
end;

function GetReplyRequestType.ServerTimestamp_Specified(Index: Integer): boolean;
begin
  Result := FServerTimestamp_Specified;
end;

destructor GetPosReportTraceRequestType.Destroy;
begin
  SysUtils.FreeAndNil(FVehicleTimestamp);
  SysUtils.FreeAndNil(FServerTimestamp);
  inherited Destroy;
end;

procedure GetPosReportTraceRequestType.Setsessionid(Index: Integer; const AsessionidType: sessionidType);
begin
  Fsessionid := AsessionidType;
  Fsessionid_Specified := True;
end;

function GetPosReportTraceRequestType.sessionid_Specified(Index: Integer): boolean;
begin
  Result := Fsessionid_Specified;
end;

procedure GetPosReportTraceRequestType.Setversion(Index: Integer; const AversionType: versionType);
begin
  Fversion := AversionType;
  Fversion_Specified := True;
end;

function GetPosReportTraceRequestType.version_Specified(Index: Integer): boolean;
begin
  Result := Fversion_Specified;
end;

procedure GetPosReportTraceRequestType.Setlimit(Index: Integer; const AlimitType: limitType);
begin
  Flimit := AlimitType;
  Flimit_Specified := True;
end;

function GetPosReportTraceRequestType.limit_Specified(Index: Integer): boolean;
begin
  Result := Flimit_Specified;
end;

procedure GetPosReportTraceRequestType.Setoffset(Index: Integer; const AoffsetType: offsetType);
begin
  Foffset := AoffsetType;
  Foffset_Specified := True;
end;

function GetPosReportTraceRequestType.offset_Specified(Index: Integer): boolean;
begin
  Result := Foffset_Specified;
end;

procedure GetPosReportTraceRequestType.SetVehicleTimestamp(Index: Integer; const ATPDate: TPDate);
begin
  FVehicleTimestamp := ATPDate;
  FVehicleTimestamp_Specified := True;
end;

function GetPosReportTraceRequestType.VehicleTimestamp_Specified(Index: Integer): boolean;
begin
  Result := FVehicleTimestamp_Specified;
end;

procedure GetPosReportTraceRequestType.SetServerTimestamp(Index: Integer; const ATPDate: TPDate);
begin
  FServerTimestamp := ATPDate;
  FServerTimestamp_Specified := True;
end;

function GetPosReportTraceRequestType.ServerTimestamp_Specified(Index: Integer): boolean;
begin
  Result := FServerTimestamp_Specified;
end;

destructor GetPosReportResponseType.Destroy;
var
  I: Integer;
begin
  for I := 0 to System.Length(FMsg)-1 do
    SysUtils.FreeAndNil(FMsg[I]);
  System.SetLength(FMsg, 0);
  inherited Destroy;
end;

procedure GetPosReportResponseType.Setlimit(Index: Integer; const AlimitType: limitType);
begin
  Flimit := AlimitType;
  Flimit_Specified := True;
end;

function GetPosReportResponseType.limit_Specified(Index: Integer): boolean;
begin
  Result := Flimit_Specified;
end;

procedure GetPosReportResponseType.Setoffset(Index: Integer; const AoffsetType: offsetType);
begin
  Foffset := AoffsetType;
  Foffset_Specified := True;
end;

function GetPosReportResponseType.offset_Specified(Index: Integer): boolean;
begin
  Result := Foffset_Specified;
end;

procedure GetPosReportResponseType.SetresultSize(Index: Integer; const AresultSizeType: resultSizeType);
begin
  FresultSize := AresultSizeType;
  FresultSize_Specified := True;
end;

function GetPosReportResponseType.resultSize_Specified(Index: Integer): boolean;
begin
  Result := FresultSize_Specified;
end;

procedure GetPosReportResponseType.SetresponseSize(Index: Integer; const AresponseSizeType: responseSizeType);
begin
  FresponseSize := AresponseSizeType;
  FresponseSize_Specified := True;
end;

function GetPosReportResponseType.responseSize_Specified(Index: Integer): boolean;
begin
  Result := FresponseSize_Specified;
end;

procedure GetPosReportResponseType.SetMsg(Index: Integer; const AArray_Of_Msg: Array_Of_Msg);
begin
  FMsg := AArray_Of_Msg;
  FMsg_Specified := True;
end;

function GetPosReportResponseType.Msg_Specified(Index: Integer): boolean;
begin
  Result := FMsg_Specified;
end;

destructor GetPosReportRequestType.Destroy;
begin
  SysUtils.FreeAndNil(FVehicleTimestamp);
  SysUtils.FreeAndNil(FRealVehicleTimestamp);
  inherited Destroy;
end;

procedure GetPosReportRequestType.Setsessionid(Index: Integer; const AsessionidType: sessionidType);
begin
  Fsessionid := AsessionidType;
  Fsessionid_Specified := True;
end;

function GetPosReportRequestType.sessionid_Specified(Index: Integer): boolean;
begin
  Result := Fsessionid_Specified;
end;

procedure GetPosReportRequestType.Setlimit(Index: Integer; const AlimitType: limitType);
begin
  Flimit := AlimitType;
  Flimit_Specified := True;
end;

function GetPosReportRequestType.limit_Specified(Index: Integer): boolean;
begin
  Result := Flimit_Specified;
end;

procedure GetPosReportRequestType.Setoffset(Index: Integer; const AoffsetType: offsetType);
begin
  Foffset := AoffsetType;
  Foffset_Specified := True;
end;

function GetPosReportRequestType.offset_Specified(Index: Integer): boolean;
begin
  Result := Foffset_Specified;
end;

procedure GetPosReportRequestType.SetVehicleID(Index: Integer; const AArray_Of_vehicleidType: Array_Of_vehicleidType);
begin
  FVehicleID := AArray_Of_vehicleidType;
  FVehicleID_Specified := True;
end;

function GetPosReportRequestType.VehicleID_Specified(Index: Integer): boolean;
begin
  Result := FVehicleID_Specified;
end;

procedure GetPosReportRequestType.SetVehicleGroupID(Index: Integer; const AArray_Of_vehicleGroupIdType: Array_Of_vehicleGroupIdType);
begin
  FVehicleGroupID := AArray_Of_vehicleGroupIdType;
  FVehicleGroupID_Specified := True;
end;

function GetPosReportRequestType.VehicleGroupID_Specified(Index: Integer): boolean;
begin
  Result := FVehicleGroupID_Specified;
end;

procedure GetPosReportRequestType.SetTelematicGroupID(Index: Integer; const AtelematicGroupIDType: telematicGroupIDType);
begin
  FTelematicGroupID := AtelematicGroupIDType;
  FTelematicGroupID_Specified := True;
end;

function GetPosReportRequestType.TelematicGroupID_Specified(Index: Integer): boolean;
begin
  Result := FTelematicGroupID_Specified;
end;

procedure GetPosReportRequestType.SetDriverNameID(Index: Integer; const AArray_Of_driverNameIDType: Array_Of_driverNameIDType);
begin
  FDriverNameID := AArray_Of_driverNameIDType;
  FDriverNameID_Specified := True;
end;

function GetPosReportRequestType.DriverNameID_Specified(Index: Integer): boolean;
begin
  Result := FDriverNameID_Specified;
end;

procedure GetPosReportRequestType.SetDriverGroupID(Index: Integer; const AdriverGroupIdType: driverGroupIdType);
begin
  FDriverGroupID := AdriverGroupIdType;
  FDriverGroupID_Specified := True;
end;

function GetPosReportRequestType.DriverGroupID_Specified(Index: Integer): boolean;
begin
  Result := FDriverGroupID_Specified;
end;

procedure GetPosReportRequestType.SetVehicleTimestamp(Index: Integer; const ATPDate: TPDate);
begin
  FVehicleTimestamp := ATPDate;
  FVehicleTimestamp_Specified := True;
end;

function GetPosReportRequestType.VehicleTimestamp_Specified(Index: Integer): boolean;
begin
  Result := FVehicleTimestamp_Specified;
end;

procedure GetPosReportRequestType.SetRealVehicleTimestamp(Index: Integer; const ATPDate: TPDate);
begin
  FRealVehicleTimestamp := ATPDate;
  FRealVehicleTimestamp_Specified := True;
end;

function GetPosReportRequestType.RealVehicleTimestamp_Specified(Index: Integer): boolean;
begin
  Result := FRealVehicleTimestamp_Specified;
end;

procedure GetPosReportRequestType.SetInoid(Index: Integer; const AArray_Of_inoidType: Array_Of_inoidType);
begin
  FInoid := AArray_Of_inoidType;
  FInoid_Specified := True;
end;

function GetPosReportRequestType.Inoid_Specified(Index: Integer): boolean;
begin
  Result := FInoid_Specified;
end;

procedure GetPosReportRequestType.SetFbid(Index: Integer; const AArray_Of_fbidType: Array_Of_fbidType);
begin
  FFbid := AArray_Of_fbidType;
  FFbid_Specified := True;
end;

function GetPosReportRequestType.Fbid_Specified(Index: Integer): boolean;
begin
  Result := FFbid_Specified;
end;

procedure GetCurrentStateRequestType.Setsessionid(Index: Integer; const AsessionidType: sessionidType);
begin
  Fsessionid := AsessionidType;
  Fsessionid_Specified := True;
end;

function GetCurrentStateRequestType.sessionid_Specified(Index: Integer): boolean;
begin
  Result := Fsessionid_Specified;
end;

procedure GetCurrentStateRequestType.Setlimit(Index: Integer; const AlimitType: limitType);
begin
  Flimit := AlimitType;
  Flimit_Specified := True;
end;

function GetCurrentStateRequestType.limit_Specified(Index: Integer): boolean;
begin
  Result := Flimit_Specified;
end;

procedure GetCurrentStateRequestType.Setoffset(Index: Integer; const AoffsetType: offsetType);
begin
  Foffset := AoffsetType;
  Foffset_Specified := True;
end;

function GetCurrentStateRequestType.offset_Specified(Index: Integer): boolean;
begin
  Result := Foffset_Specified;
end;

procedure GetCurrentStateRequestType.Setversion(Index: Integer; const AversionType: versionType);
begin
  Fversion := AversionType;
  Fversion_Specified := True;
end;

function GetCurrentStateRequestType.version_Specified(Index: Integer): boolean;
begin
  Result := Fversion_Specified;
end;

procedure GetCurrentStateRequestType.SetVehicleID(Index: Integer; const AArray_Of_vehicleidType: Array_Of_vehicleidType);
begin
  FVehicleID := AArray_Of_vehicleidType;
  FVehicleID_Specified := True;
end;

function GetCurrentStateRequestType.VehicleID_Specified(Index: Integer): boolean;
begin
  Result := FVehicleID_Specified;
end;

procedure GetCurrentStateRequestType.SetVehicleGroupID(Index: Integer; const AvehicleGroupIdType: vehicleGroupIdType);
begin
  FVehicleGroupID := AvehicleGroupIdType;
  FVehicleGroupID_Specified := True;
end;

function GetCurrentStateRequestType.VehicleGroupID_Specified(Index: Integer): boolean;
begin
  Result := FVehicleGroupID_Specified;
end;

procedure GetCurrentStateRequestType.SetDriverNameID(Index: Integer; const AArray_Of_driverNameIDType: Array_Of_driverNameIDType);
begin
  FDriverNameID := AArray_Of_driverNameIDType;
  FDriverNameID_Specified := True;
end;

function GetCurrentStateRequestType.DriverNameID_Specified(Index: Integer): boolean;
begin
  Result := FDriverNameID_Specified;
end;

procedure GetCurrentStateRequestType.SetDriverGroupID(Index: Integer; const AdriverGroupIdType: driverGroupIdType);
begin
  FDriverGroupID := AdriverGroupIdType;
  FDriverGroupID_Specified := True;
end;

function GetCurrentStateRequestType.DriverGroupID_Specified(Index: Integer): boolean;
begin
  Result := FDriverGroupID_Specified;
end;

procedure GetCurrentStateRequestType.SetServiceType(Index: Integer; const AserviceType: serviceType);
begin
  FServiceType := AserviceType;
  FServiceType_Specified := True;
end;

function GetCurrentStateRequestType.ServiceType_Specified(Index: Integer): boolean;
begin
  Result := FServiceType_Specified;
end;

procedure GetCurrentStateRequestType.SetQueryType(Index: Integer; const ASmallInt: SmallInt);
begin
  FQueryType := ASmallInt;
  FQueryType_Specified := True;
end;

function GetCurrentStateRequestType.QueryType_Specified(Index: Integer): boolean;
begin
  Result := FQueryType_Specified;
end;

procedure GetCurrentStateRequestType.SetOnlyActive(Index: Integer; const ABoolean: Boolean);
begin
  FOnlyActive := ABoolean;
  FOnlyActive_Specified := True;
end;

function GetCurrentStateRequestType.OnlyActive_Specified(Index: Integer): boolean;
begin
  Result := FOnlyActive_Specified;
end;

procedure State.Settimestamp(Index: Integer; const AtimestampType: timestampType);
begin
  Ftimestamp := AtimestampType;
  Ftimestamp_Specified := True;
end;

function State.timestamp_Specified(Index: Integer): boolean;
begin
  Result := Ftimestamp_Specified;
end;

destructor Header.Destroy;
var
  I: Integer;
begin
  for I := 0 to System.Length(FReadInfo)-1 do
    SysUtils.FreeAndNil(FReadInfo[I]);
  System.SetLength(FReadInfo, 0);
  for I := 0 to System.Length(FStates)-1 do
    SysUtils.FreeAndNil(FStates[I]);
  System.SetLength(FStates, 0);
  SysUtils.FreeAndNil(FPosition);
  inherited Destroy;
end;

procedure Header.SetSentstatus(Index: Integer; const Astring: string);
begin
  FSentstatus := Astring;
  FSentstatus_Specified := True;
end;

function Header.Sentstatus_Specified(Index: Integer): boolean;
begin
  Result := FSentstatus_Specified;
end;

procedure Header.SetInbound(Index: Integer; const Astring: string);
begin
  FInbound := Astring;
  FInbound_Specified := True;
end;

function Header.Inbound_Specified(Index: Integer): boolean;
begin
  Result := FInbound_Specified;
end;

procedure Header.SetClientTimestamp(Index: Integer; const Astring: string);
begin
  FClientTimestamp := Astring;
  FClientTimestamp_Specified := True;
end;

function Header.ClientTimestamp_Specified(Index: Integer): boolean;
begin
  Result := FClientTimestamp_Specified;
end;

procedure Header.SetServerTimestamp(Index: Integer; const Astring: string);
begin
  FServerTimestamp := Astring;
  FServerTimestamp_Specified := True;
end;

function Header.ServerTimestamp_Specified(Index: Integer): boolean;
begin
  Result := FServerTimestamp_Specified;
end;

procedure Header.SetVehicleTimestamp(Index: Integer; const Astring: string);
begin
  FVehicleTimestamp := Astring;
  FVehicleTimestamp_Specified := True;
end;

function Header.VehicleTimestamp_Specified(Index: Integer): boolean;
begin
  Result := FVehicleTimestamp_Specified;
end;

procedure Header.SetCreationBeginTimestamp(Index: Integer; const Astring: string);
begin
  FCreationBeginTimestamp := Astring;
  FCreationBeginTimestamp_Specified := True;
end;

function Header.CreationBeginTimestamp_Specified(Index: Integer): boolean;
begin
  Result := FCreationBeginTimestamp_Specified;
end;

procedure Header.SetCreationEndTimestamp(Index: Integer; const Astring: string);
begin
  FCreationEndTimestamp := Astring;
  FCreationEndTimestamp_Specified := True;
end;

function Header.CreationEndTimestamp_Specified(Index: Integer): boolean;
begin
  Result := FCreationEndTimestamp_Specified;
end;

procedure Header.SetPosition(Index: Integer; const APosition: Position);
begin
  FPosition := APosition;
  FPosition_Specified := True;
end;

function Header.Position_Specified(Index: Integer): boolean;
begin
  Result := FPosition_Specified;
end;

procedure Header.SetReadInfo(Index: Integer; const AReadInfo: ReadInfo);
begin
  FReadInfo := AReadInfo;
  FReadInfo_Specified := True;
end;

function Header.ReadInfo_Specified(Index: Integer): boolean;
begin
  Result := FReadInfo_Specified;
end;

procedure Header.SetConfirmation(Index: Integer; const Astring: string);
begin
  FConfirmation := Astring;
  FConfirmation_Specified := True;
end;

function Header.Confirmation_Specified(Index: Integer): boolean;
begin
  Result := FConfirmation_Specified;
end;

procedure Header.SetScheduled(Index: Integer; const Astring: string);
begin
  FScheduled := Astring;
  FScheduled_Specified := True;
end;

function Header.Scheduled_Specified(Index: Integer): boolean;
begin
  Result := FScheduled_Specified;
end;

procedure Header.SetVehicleID(Index: Integer; const AvehicleidType: vehicleidType);
begin
  FVehicleID := AvehicleidType;
  FVehicleID_Specified := True;
end;

function Header.VehicleID_Specified(Index: Integer): boolean;
begin
  Result := FVehicleID_Specified;
end;

procedure Header.SetDriverID(Index: Integer; const Astring: string);
begin
  FDriverID := Astring;
  FDriverID_Specified := True;
end;

function Header.DriverID_Specified(Index: Integer): boolean;
begin
  Result := FDriverID_Specified;
end;

procedure Header.SetDeliveryPriority(Index: Integer; const Astring: string);
begin
  FDeliveryPriority := Astring;
  FDeliveryPriority_Specified := True;
end;

function Header.DeliveryPriority_Specified(Index: Integer): boolean;
begin
  Result := FDeliveryPriority_Specified;
end;

procedure Header.SetStates(Index: Integer; const AStates: States);
begin
  FStates := AStates;
  FStates_Specified := True;
end;

function Header.States_Specified(Index: Integer): boolean;
begin
  Result := FStates_Specified;
end;

destructor Form.Destroy;
var
  I: Integer;
begin
  for I := 0 to System.Length(FFields)-1 do
    SysUtils.FreeAndNil(FFields[I]);
  System.SetLength(FFields, 0);
  inherited Destroy;
end;

destructor VhcLayout.Destroy;
begin
  SysUtils.FreeAndNil(FHidden);
  inherited Destroy;
end;

procedure VhcLayout.SetVehicleColumn(Index: Integer; const Astring: string);
begin
  FVehicleColumn := Astring;
  FVehicleColumn_Specified := True;
end;

function VhcLayout.VehicleColumn_Specified(Index: Integer): boolean;
begin
  Result := FVehicleColumn_Specified;
end;

procedure VhcLayout.SetVehicleRow(Index: Integer; const Astring: string);
begin
  FVehicleRow := Astring;
  FVehicleRow_Specified := True;
end;

function VhcLayout.VehicleRow_Specified(Index: Integer): boolean;
begin
  Result := FVehicleRow_Specified;
end;

procedure VhcLayout.SetHidden(Index: Integer; const AHidden: Hidden);
begin
  FHidden := AHidden;
  FHidden_Specified := True;
end;

function VhcLayout.Hidden_Specified(Index: Integer): boolean;
begin
  Result := FHidden_Specified;
end;

procedure Hidden.SetDCFAP(Index: Integer; const AInt64: Int64);
begin
  FDCFAP := AInt64;
  FDCFAP_Specified := True;
end;

function Hidden.DCFAP_Specified(Index: Integer): boolean;
begin
  Result := FDCFAP_Specified;
end;

destructor OrderMsg.Destroy;
begin
  SysUtils.FreeAndNil(FForm);
  inherited Destroy;
end;

procedure OrderMsg.SetTourRank(Index: Integer; const AInt64: Int64);
begin
  FTourRank := AInt64;
  FTourRank_Specified := True;
end;

function OrderMsg.TourRank_Specified(Index: Integer): boolean;
begin
  Result := FTourRank_Specified;
end;

procedure OrderMsg.SetOrderCount(Index: Integer; const AInt64: Int64);
begin
  FOrderCount := AInt64;
  FOrderCount_Specified := True;
end;

function OrderMsg.OrderCount_Specified(Index: Integer): boolean;
begin
  Result := FOrderCount_Specified;
end;

destructor VhcStatusLayout.Destroy;
begin
  SysUtils.FreeAndNil(FHidden);
  inherited Destroy;
end;

procedure VhcStatusLayout.SetHidden(Index: Integer; const AHidden: Hidden);
begin
  FHidden := AHidden;
  FHidden_Specified := True;
end;

function VhcStatusLayout.Hidden_Specified(Index: Integer): boolean;
begin
  Result := FHidden_Specified;
end;

procedure Semantics.SetSemanticId(Index: Integer; const AInt64: Int64);
begin
  FSemanticId := AInt64;
  FSemanticId_Specified := True;
end;

function Semantics.SemanticId_Specified(Index: Integer): boolean;
begin
  Result := FSemanticId_Specified;
end;

procedure Semantics.SetScope(Index: Integer; const ATimes: Times);
begin
  FScope := ATimes;
  FScope_Specified := True;
end;

function Semantics.Scope_Specified(Index: Integer): boolean;
begin
  Result := FScope_Specified;
end;

procedure Semantics.SetValue(Index: Integer; const Astring: string);
begin
  FValue := Astring;
  FValue_Specified := True;
end;

function Semantics.Value_Specified(Index: Integer): boolean;
begin
  Result := FValue_Specified;
end;

destructor FieldDef.Destroy;
var
  I: Integer;
begin
  for I := 0 to System.Length(FSemanticsDef)-1 do
    SysUtils.FreeAndNil(FSemanticsDef[I]);
  System.SetLength(FSemanticsDef, 0);
  SysUtils.FreeAndNil(FVhcLayout);
  SysUtils.FreeAndNil(FFormLayout);
  SysUtils.FreeAndNil(FBehavior);
  inherited Destroy;
end;

procedure FieldDef.SetSearchable(Index: Integer; const AInt64: Int64);
begin
  FSearchable := AInt64;
  FSearchable_Specified := True;
end;

function FieldDef.Searchable_Specified(Index: Integer): boolean;
begin
  Result := FSearchable_Specified;
end;

procedure FieldDef.SetSuppressInVehicle(Index: Integer; const AInt64: Int64);
begin
  FSuppressInVehicle := AInt64;
  FSuppressInVehicle_Specified := True;
end;

function FieldDef.SuppressInVehicle_Specified(Index: Integer): boolean;
begin
  Result := FSuppressInVehicle_Specified;
end;

procedure FieldDef.SetCopyFrom(Index: Integer; const AInt64: Int64);
begin
  FCopyFrom := AInt64;
  FCopyFrom_Specified := True;
end;

function FieldDef.CopyFrom_Specified(Index: Integer): boolean;
begin
  Result := FCopyFrom_Specified;
end;

procedure FieldDef.SetSemanticsDef(Index: Integer; const ASemanticsDef: SemanticsDef);
begin
  FSemanticsDef := ASemanticsDef;
  FSemanticsDef_Specified := True;
end;

function FieldDef.SemanticsDef_Specified(Index: Integer): boolean;
begin
  Result := FSemanticsDef_Specified;
end;

destructor StatusDefMsg.Destroy;
var
  I: Integer;
begin
  for I := 0 to System.Length(FSemanticsDef)-1 do
    SysUtils.FreeAndNil(FSemanticsDef[I]);
  System.SetLength(FSemanticsDef, 0);
  SysUtils.FreeAndNil(FVhcStatusLayout);
  inherited Destroy;
end;

procedure StatusDefMsg.SetStatusDefID(Index: Integer; const Astring: string);
begin
  FStatusDefID := Astring;
  FStatusDefID_Specified := True;
end;

function StatusDefMsg.StatusDefID_Specified(Index: Integer): boolean;
begin
  Result := FStatusDefID_Specified;
end;

procedure StatusDefMsg.SetVhcStatusLayout(Index: Integer; const AVhcStatusLayout: VhcStatusLayout);
begin
  FVhcStatusLayout := AVhcStatusLayout;
  FVhcStatusLayout_Specified := True;
end;

function StatusDefMsg.VhcStatusLayout_Specified(Index: Integer): boolean;
begin
  Result := FVhcStatusLayout_Specified;
end;

procedure StatusDefMsg.SetSemanticsDef(Index: Integer; const ASemanticsDef: SemanticsDef);
begin
  FSemanticsDef := ASemanticsDef;
  FSemanticsDef_Specified := True;
end;

function StatusDefMsg.SemanticsDef_Specified(Index: Integer): boolean;
begin
  Result := FSemanticsDef_Specified;
end;

procedure ReeferStartStopContinousChanged.SetcurrentReeferOperationMode(Index: Integer; const ACurrentReeferOperationModeDTO: CurrentReeferOperationModeDTO);
begin
  FcurrentReeferOperationMode := ACurrentReeferOperationModeDTO;
  FcurrentReeferOperationMode_Specified := True;
end;

function ReeferStartStopContinousChanged.currentReeferOperationMode_Specified(Index: Integer): boolean;
begin
  Result := FcurrentReeferOperationMode_Specified;
end;

destructor SetpointAlarm.Destroy;
begin
  SysUtils.FreeAndNil(FSetpointData);
  inherited Destroy;
end;

procedure SetpointAlarm.SetsetpointSensor(Index: Integer; const ASetpointSensorDTO: SetpointSensorDTO);
begin
  FsetpointSensor := ASetpointSensorDTO;
  FsetpointSensor_Specified := True;
end;

function SetpointAlarm.setpointSensor_Specified(Index: Integer): boolean;
begin
  Result := FsetpointSensor_Specified;
end;

procedure SetpointAlarm.SetSetpointData(Index: Integer; const ASetpointData: SetpointData);
begin
  FSetpointData := ASetpointData;
  FSetpointData_Specified := True;
end;

function SetpointAlarm.SetpointData_Specified(Index: Integer): boolean;
begin
  Result := FSetpointData_Specified;
end;

procedure SetpointAlarm.Setthreshold(Index: Integer; const AInt64: Int64);
begin
  Fthreshold := AInt64;
  Fthreshold_Specified := True;
end;

function SetpointAlarm.threshold_Specified(Index: Integer): boolean;
begin
  Result := Fthreshold_Specified;
end;

procedure SetpointAlarm.SetisAllClear(Index: Integer; const ABoolean: Boolean);
begin
  FisAllClear := ABoolean;
  FisAllClear_Specified := True;
end;

function SetpointAlarm.isAllClear_Specified(Index: Integer): boolean;
begin
  Result := FisAllClear_Specified;
end;

procedure ReeferDieselElectricChanged.SetcurrentPowerMode(Index: Integer; const ACurrentPowerModeDTO: CurrentPowerModeDTO);
begin
  FcurrentPowerMode := ACurrentPowerModeDTO;
  FcurrentPowerMode_Specified := True;
end;

function ReeferDieselElectricChanged.currentPowerMode_Specified(Index: Integer): boolean;
begin
  Result := FcurrentPowerMode_Specified;
end;

procedure FuelLevelAlarm.SetcurrentFuelLevelColor(Index: Integer; const ACurrentFuelLevelColorDTO: CurrentFuelLevelColorDTO);
begin
  FcurrentFuelLevelColor := ACurrentFuelLevelColorDTO;
  FcurrentFuelLevelColor_Specified := True;
end;

function FuelLevelAlarm.currentFuelLevelColor_Specified(Index: Integer): boolean;
begin
  Result := FcurrentFuelLevelColor_Specified;
end;

procedure CurrentCompartmentDataList.SetcompartmentNumber(Index: Integer; const AInt64: Int64);
begin
  FcompartmentNumber := AInt64;
  FcompartmentNumber_Specified := True;
end;

function CurrentCompartmentDataList.compartmentNumber_Specified(Index: Integer): boolean;
begin
  Result := FcompartmentNumber_Specified;
end;

procedure CurrentCompartmentDataList.SetcompartmentOperationMode(Index: Integer; const ACompartmentOperationModeDTO: CompartmentOperationModeDTO);
begin
  FcompartmentOperationMode := ACompartmentOperationModeDTO;
  FcompartmentOperationMode_Specified := True;
end;

function CurrentCompartmentDataList.compartmentOperationMode_Specified(Index: Integer): boolean;
begin
  Result := FcompartmentOperationMode_Specified;
end;

destructor Positions.Destroy;
begin
  SysUtils.FreeAndNil(FPosition);
  SysUtils.FreeAndNil(FStateOfVehicle);
  SysUtils.FreeAndNil(FStateOfTimeManagement);
  SysUtils.FreeAndNil(FStateOfOrderManagement);
  inherited Destroy;
end;

procedure Positions.SetMotorStatus(Index: Integer; const ASmallInt: SmallInt);
begin
  FMotorStatus := ASmallInt;
  FMotorStatus_Specified := True;
end;

function Positions.MotorStatus_Specified(Index: Integer): boolean;
begin
  Result := FMotorStatus_Specified;
end;

procedure Positions.SetIgnitionStatus(Index: Integer; const ASmallInt: SmallInt);
begin
  FIgnitionStatus := ASmallInt;
  FIgnitionStatus_Specified := True;
end;

function Positions.IgnitionStatus_Specified(Index: Integer): boolean;
begin
  Result := FIgnitionStatus_Specified;
end;

procedure Positions.SetStateOfVehicle(Index: Integer; const ALastPositionState: LastPositionState);
begin
  FStateOfVehicle := ALastPositionState;
  FStateOfVehicle_Specified := True;
end;

function Positions.StateOfVehicle_Specified(Index: Integer): boolean;
begin
  Result := FStateOfVehicle_Specified;
end;

procedure Positions.SetStateOfTimeManagement(Index: Integer; const ALastPositionState: LastPositionState);
begin
  FStateOfTimeManagement := ALastPositionState;
  FStateOfTimeManagement_Specified := True;
end;

function Positions.StateOfTimeManagement_Specified(Index: Integer): boolean;
begin
  Result := FStateOfTimeManagement_Specified;
end;

procedure Positions.SetStateOfOrderManagement(Index: Integer; const ALastPositionState: LastPositionState);
begin
  FStateOfOrderManagement := ALastPositionState;
  FStateOfOrderManagement_Specified := True;
end;

function Positions.StateOfOrderManagement_Specified(Index: Integer): boolean;
begin
  Result := FStateOfOrderManagement_Specified;
end;

procedure TemperatureAlarm.SettemperatureSensor(Index: Integer; const ATemperatureSensorDTO: TemperatureSensorDTO);
begin
  FtemperatureSensor := ATemperatureSensorDTO;
  FtemperatureSensor_Specified := True;
end;

function TemperatureAlarm.temperatureSensor_Specified(Index: Integer): boolean;
begin
  Result := FtemperatureSensor_Specified;
end;

procedure TemperatureAlarm.SetcurrentTemperature(Index: Integer; const ADouble: Double);
begin
  FcurrentTemperature := ADouble;
  FcurrentTemperature_Specified := True;
end;

function TemperatureAlarm.currentTemperature_Specified(Index: Integer): boolean;
begin
  Result := FcurrentTemperature_Specified;
end;

procedure TemperatureAlarm.SetminThreshold(Index: Integer; const AInt64: Int64);
begin
  FminThreshold := AInt64;
  FminThreshold_Specified := True;
end;

function TemperatureAlarm.minThreshold_Specified(Index: Integer): boolean;
begin
  Result := FminThreshold_Specified;
end;

procedure TemperatureAlarm.SetmaxThreshold(Index: Integer; const AInt64: Int64);
begin
  FmaxThreshold := AInt64;
  FmaxThreshold_Specified := True;
end;

function TemperatureAlarm.maxThreshold_Specified(Index: Integer): boolean;
begin
  Result := FmaxThreshold_Specified;
end;

procedure TemperatureAlarm.SetisAllClear(Index: Integer; const ABoolean: Boolean);
begin
  FisAllClear := ABoolean;
  FisAllClear_Specified := True;
end;

function TemperatureAlarm.isAllClear_Specified(Index: Integer): boolean;
begin
  Result := FisAllClear_Specified;
end;

procedure TirePressureAlarm.SettirePosition(Index: Integer; const ATirePositionDTO: TirePositionDTO);
begin
  FtirePosition := ATirePositionDTO;
  FtirePosition_Specified := True;
end;

function TirePressureAlarm.tirePosition_Specified(Index: Integer): boolean;
begin
  Result := FtirePosition_Specified;
end;

procedure TirePressureAlarm.SetcurrentPressure(Index: Integer; const ADouble: Double);
begin
  FcurrentPressure := ADouble;
  FcurrentPressure_Specified := True;
end;

function TirePressureAlarm.currentPressure_Specified(Index: Integer): boolean;
begin
  Result := FcurrentPressure_Specified;
end;

procedure TirePressureAlarm.Setthreshold(Index: Integer; const ADouble: Double);
begin
  Fthreshold := ADouble;
  Fthreshold_Specified := True;
end;

function TirePressureAlarm.threshold_Specified(Index: Integer): boolean;
begin
  Result := Fthreshold_Specified;
end;

procedure User.SetUsertype(Index: Integer; const Astring: string);
begin
  FUsertype := Astring;
  FUsertype_Specified := True;
end;

function User.Usertype_Specified(Index: Integer): boolean;
begin
  Result := FUsertype_Specified;
end;

procedure User.SetMsgID(Index: Integer; const AMsgID: MsgID);
begin
  FMsgID := AMsgID;
  FMsgID_Specified := True;
end;

function User.MsgID_Specified(Index: Integer): boolean;
begin
  Result := FMsgID_Specified;
end;

destructor Position.Destroy;
var
  I: Integer;
begin
  for I := 0 to System.Length(FTracePositions)-1 do
    SysUtils.FreeAndNil(FTracePositions[I]);
  System.SetLength(FTracePositions, 0);
  inherited Destroy;
end;

procedure Position.Settimestamp(Index: Integer; const AtimestampType: timestampType);
begin
  Ftimestamp := AtimestampType;
  Ftimestamp_Specified := True;
end;

function Position.timestamp_Specified(Index: Integer): boolean;
begin
  Result := Ftimestamp_Specified;
end;

procedure Position.SetLong(Index: Integer; const ASingle: Single);
begin
  FLong := ASingle;
  FLong_Specified := True;
end;

function Position.Long_Specified(Index: Integer): boolean;
begin
  Result := FLong_Specified;
end;

procedure Position.SetLat(Index: Integer; const ASingle: Single);
begin
  FLat := ASingle;
  FLat_Specified := True;
end;

function Position.Lat_Specified(Index: Integer): boolean;
begin
  Result := FLat_Specified;
end;

procedure Position.SetPosText(Index: Integer; const Astring: string);
begin
  FPosText := Astring;
  FPosText_Specified := True;
end;

function Position.PosText_Specified(Index: Integer): boolean;
begin
  Result := FPosText_Specified;
end;

procedure Position.SetGPSStatus(Index: Integer; const Astring: string);
begin
  FGPSStatus := Astring;
  FGPSStatus_Specified := True;
end;

function Position.GPSStatus_Specified(Index: Integer): boolean;
begin
  Result := FGPSStatus_Specified;
end;

procedure Position.SetTracePositions(Index: Integer; const ATracePositions: TracePositions);
begin
  FTracePositions := ATracePositions;
  FTracePositions_Specified := True;
end;

function Position.TracePositions_Specified(Index: Integer): boolean;
begin
  Result := FTracePositions_Specified;
end;

destructor Msg.Destroy;
var
  I: Integer;
begin
  for I := 0 to System.Length(FHistory)-1 do
    SysUtils.FreeAndNil(FHistory[I]);
  System.SetLength(FHistory, 0);
  for I := 0 to System.Length(FForeignRef)-1 do
    SysUtils.FreeAndNil(FForeignRef[I]);
  System.SetLength(FForeignRef, 0);
  SysUtils.FreeAndNil(FHeader);
  SysUtils.FreeAndNil(FBody);
  inherited Destroy;
end;

procedure Msg.Setinoid(Index: Integer; const AinoidType: inoidType);
begin
  Finoid := AinoidType;
  Finoid_Specified := True;
end;

function Msg.inoid_Specified(Index: Integer): boolean;
begin
  Result := Finoid_Specified;
end;

procedure Msg.Setfbid(Index: Integer; const AfbidType: fbidType);
begin
  Ffbid := AfbidType;
  Ffbid_Specified := True;
end;

function Msg.fbid_Specified(Index: Integer): boolean;
begin
  Result := Ffbid_Specified;
end;

procedure Msg.SetHistory(Index: Integer; const AHistory: History);
begin
  FHistory := AHistory;
  FHistory_Specified := True;
end;

function Msg.History_Specified(Index: Integer): boolean;
begin
  Result := FHistory_Specified;
end;

procedure Msg.SetBody(Index: Integer; const ABody: Body);
begin
  FBody := ABody;
  FBody_Specified := True;
end;

function Msg.Body_Specified(Index: Integer): boolean;
begin
  Result := FBody_Specified;
end;

procedure Msg.SetForeignRef(Index: Integer; const AArray_Of_ForeignRef: Array_Of_ForeignRef);
begin
  FForeignRef := AArray_Of_ForeignRef;
  FForeignRef_Specified := True;
end;

function Msg.ForeignRef_Specified(Index: Integer): boolean;
begin
  Result := FForeignRef_Specified;
end;

procedure WatchboxAlarm.SetwatchboxName(Index: Integer; const Astring: string);
begin
  FwatchboxName := Astring;
  FwatchboxName_Specified := True;
end;

function WatchboxAlarm.watchboxName_Specified(Index: Integer): boolean;
begin
  Result := FwatchboxName_Specified;
end;

procedure WatchboxAlarm.SetwatchboxAlarmReason(Index: Integer; const AWatchboxAlarmReasonDTO: WatchboxAlarmReasonDTO);
begin
  FwatchboxAlarmReason := AWatchboxAlarmReasonDTO;
  FwatchboxAlarmReason_Specified := True;
end;

function WatchboxAlarm.watchboxAlarmReason_Specified(Index: Integer): boolean;
begin
  Result := FwatchboxAlarmReason_Specified;
end;

procedure UndervoltageAlarm.SetbatteryConcerned(Index: Integer; const ABatteryConcernedDTO: BatteryConcernedDTO);
begin
  FbatteryConcerned := ABatteryConcernedDTO;
  FbatteryConcerned_Specified := True;
end;

function UndervoltageAlarm.batteryConcerned_Specified(Index: Integer): boolean;
begin
  Result := FbatteryConcerned_Specified;
end;

destructor TrailerRegisterMsg.Destroy;
begin
  SysUtils.FreeAndNil(FupdateTime);
  inherited Destroy;
end;

procedure TrailerRegisterMsg.SetuserName(Index: Integer; const Astring: string);
begin
  FuserName := Astring;
  FuserName_Specified := True;
end;

function TrailerRegisterMsg.userName_Specified(Index: Integer): boolean;
begin
  Result := FuserName_Specified;
end;

procedure TrailerRegisterMsg.Setdescription(Index: Integer; const Astring: string);
begin
  Fdescription := Astring;
  Fdescription_Specified := True;
end;

function TrailerRegisterMsg.description_Specified(Index: Integer): boolean;
begin
  Result := Fdescription_Specified;
end;

destructor TrailerCouplingMsg.Destroy;
begin
  SysUtils.FreeAndNil(FupdateTime);
  inherited Destroy;
end;

procedure TrailerCouplingMsg.SetvehicleId(Index: Integer; const AInt64: Int64);
begin
  FvehicleId := AInt64;
  FvehicleId_Specified := True;
end;

function TrailerCouplingMsg.vehicleId_Specified(Index: Integer): boolean;
begin
  Result := FvehicleId_Specified;
end;

destructor TrailerSyncMsg.Destroy;
begin
  SysUtils.FreeAndNil(FupdateTime);
  inherited Destroy;
end;

procedure TrailerSyncMsg.Setvin(Index: Integer; const Astring: string);
begin
  Fvin := Astring;
  Fvin_Specified := True;
end;

function TrailerSyncMsg.vin_Specified(Index: Integer): boolean;
begin
  Result := Fvin_Specified;
end;

procedure TrailerSyncMsg.SettrailerGroupIds(Index: Integer; const ATimes: Times);
begin
  FtrailerGroupIds := ATimes;
  FtrailerGroupIds_Specified := True;
end;

function TrailerSyncMsg.trailerGroupIds_Specified(Index: Integer): boolean;
begin
  Result := FtrailerGroupIds_Specified;
end;

procedure TrailerSyncMsg.SettrailerServiceProviderAccountId(Index: Integer; const Astring: string);
begin
  FtrailerServiceProviderAccountId := Astring;
  FtrailerServiceProviderAccountId_Specified := True;
end;

function TrailerSyncMsg.trailerServiceProviderAccountId_Specified(Index: Integer): boolean;
begin
  Result := FtrailerServiceProviderAccountId_Specified;
end;

procedure TrailerSyncMsg.SettrailerServiceProviderId(Index: Integer; const Astring: string);
begin
  FtrailerServiceProviderId := Astring;
  FtrailerServiceProviderId_Specified := True;
end;

function TrailerSyncMsg.trailerServiceProviderId_Specified(Index: Integer): boolean;
begin
  Result := FtrailerServiceProviderId_Specified;
end;

initialization
  { PosService }
  InvRegistry.RegisterInterface(TypeInfo(PosService), 'http://ws.fleetboard.com/PosService', 'UTF-8');
  InvRegistry.RegisterDefaultSOAPAction(TypeInfo(PosService), '%operationName%');
  InvRegistry.RegisterInvokeOptions(TypeInfo(PosService), ioDocument);
  { PosService.getPosReport }
  InvRegistry.RegisterMethodInfo(TypeInfo(PosService), 'getPosReport', '',
                                 '[ReturnName="GetPosReportResponse", RequestNS="http://www.fleetboard.com/data", ResponseNS="http://www.fleetboard.com/data"]', IS_REF);
  InvRegistry.RegisterParamInfo(TypeInfo(PosService), 'getPosReport', 'GetPosReportRequest', '',
                                '', IS_REF);
  InvRegistry.RegisterParamInfo(TypeInfo(PosService), 'getPosReport', 'GetPosReportResponse', '',
                                '', IS_REF);
  { PosService.getPosReportTrace }
  InvRegistry.RegisterMethodInfo(TypeInfo(PosService), 'getPosReportTrace', '',
                                 '[ReturnName="GetPosReportTraceResponse", RequestNS="http://www.fleetboard.com/data", ResponseNS="http://www.fleetboard.com/data"]', IS_REF);
  InvRegistry.RegisterParamInfo(TypeInfo(PosService), 'getPosReportTrace', 'GetPosReportTraceRequest', '',
                                '', IS_REF);
  InvRegistry.RegisterParamInfo(TypeInfo(PosService), 'getPosReportTrace', 'GetPosReportTraceResponse', '',
                                '', IS_REF);
  { PosService.getReply }
  InvRegistry.RegisterMethodInfo(TypeInfo(PosService), 'getReply', '',
                                 '[ReturnName="GetReplyResponse", RequestNS="http://www.fleetboard.com/data", ResponseNS="http://www.fleetboard.com/data"]', IS_REF);
  InvRegistry.RegisterParamInfo(TypeInfo(PosService), 'getReply', 'GetReplyRequest', '',
                                '', IS_REF);
  InvRegistry.RegisterParamInfo(TypeInfo(PosService), 'getReply', 'GetReplyResponse', '',
                                '', IS_REF);
  { PosService.getCurrentState }
  InvRegistry.RegisterMethodInfo(TypeInfo(PosService), 'getCurrentState', '',
                                 '[ReturnName="GetCurrentStateResponse", RequestNS="http://www.fleetboard.com/data", ResponseNS="http://www.fleetboard.com/data"]', IS_REF);
  InvRegistry.RegisterParamInfo(TypeInfo(PosService), 'getCurrentState', 'GetCurrentStateRequest', '',
                                '', IS_REF);
  InvRegistry.RegisterParamInfo(TypeInfo(PosService), 'getCurrentState', 'GetCurrentStateResponse', '',
                                '', IS_REF);
  { PosService.getLocationDescription }
  InvRegistry.RegisterMethodInfo(TypeInfo(PosService), 'getLocationDescription', '',
                                 '[ReturnName="GetLocationDescriptionResponse", RequestNS="http://www.fleetboard.com/data", ResponseNS="http://www.fleetboard.com/data"]', IS_REF);
  InvRegistry.RegisterParamInfo(TypeInfo(PosService), 'getLocationDescription', 'GetLocationDescriptionRequest', '',
                                '', IS_REF);
  InvRegistry.RegisterParamInfo(TypeInfo(PosService), 'getLocationDescription', 'GetLocationDescriptionResponse', '',
                                '', IS_REF);
  { PosService.getLastPosition }
  InvRegistry.RegisterMethodInfo(TypeInfo(PosService), 'getLastPosition', '',
                                 '[ReturnName="GetLastPositionResponse", RequestNS="http://www.fleetboard.com/data", ResponseNS="http://www.fleetboard.com/data"]', IS_REF);
  InvRegistry.RegisterParamInfo(TypeInfo(PosService), 'getLastPosition', 'GetLastPositionRequest', '',
                                '', IS_REF);
  InvRegistry.RegisterParamInfo(TypeInfo(PosService), 'getLastPosition', 'GetLastPositionResponse', '',
                                '', IS_REF);
  RemClassRegistry.RegisterXSInfo(TypeInfo(ReeferOperationModeChanged), 'http://www.fleetboard.com/data', 'ReeferOperationModeChanged');
  RemClassRegistry.RegisterXSClass(AutoPosDef, 'http://www.fleetboard.com/data', 'AutoPosDef');
  RemClassRegistry.RegisterXSInfo(TypeInfo(Array_Of_Msg), 'http://www.fleetboard.com/data', 'Array_Of_Msg');
  RemClassRegistry.RegisterXSInfo(TypeInfo(Array_Of_Positions), 'http://www.fleetboard.com/data', 'Array_Of_Positions');
  RemClassRegistry.RegisterXSInfo(TypeInfo(Array_Of_TmInfoDTO), 'http://www.fleetboard.com/data', 'Array_Of_TmInfoDTO');
  RemClassRegistry.RegisterXSInfo(TypeInfo(Array_Of_FieldDef), 'http://www.fleetboard.com/data', 'Array_Of_FieldDef');
  RemClassRegistry.RegisterXSInfo(TypeInfo(Array_Of_Position), 'http://www.fleetboard.com/data', 'Array_Of_Position');
  RemClassRegistry.RegisterXSInfo(TypeInfo(Array_Of_ForeignRef), 'http://www.fleetboard.com/data', 'Array_Of_ForeignRef');
  RemClassRegistry.RegisterXSInfo(TypeInfo(Array_Of_LocationDescription), 'http://www.fleetboard.com/data', 'Array_Of_LocationDescription');
  RemClassRegistry.RegisterXSInfo(TypeInfo(Array_Of_Coordinate), 'http://www.fleetboard.com/data', 'Array_Of_Coordinate');
  RemClassRegistry.RegisterXSInfo(TypeInfo(serviceType), 'http://www.fleetboard.com/data', 'serviceType');
  RemClassRegistry.RegisterXSClass(TourEventUptimeMsg, 'http://www.fleetboard.com/data', 'TourEventUptimeMsg');
  RemClassRegistry.RegisterXSClass(ReasonDTO, 'http://www.fleetboard.com/data', 'ReasonDTO');
  RemClassRegistry.RegisterExternalPropName(TypeInfo(ReasonDTO), 'ReeferOperationModeChanged', '[ArrayItemName="CurrentCompartmentDataList"]');
  RemClassRegistry.RegisterXSClass(TempLoggerMonitoringAlarm, 'http://www.fleetboard.com/data', 'TempLoggerMonitoringAlarm');
  RemClassRegistry.RegisterXSInfo(TypeInfo(fleetidType), 'http://www.fleetboard.com/data', 'fleetidType');
  RemClassRegistry.RegisterXSInfo(TypeInfo(Array_Of_long), 'http://www.w3.org/2001/XMLSchema', 'Array_Of_long');
  RemClassRegistry.RegisterXSClass(VelocityAlarm, 'http://www.fleetboard.com/data', 'VelocityAlarm');
  RemClassRegistry.RegisterXSClass(HumidityAlarm, 'http://www.fleetboard.com/data', 'HumidityAlarm');
  RemClassRegistry.RegisterXSClass(BogieLoadAlarm, 'http://www.fleetboard.com/data', 'BogieLoadAlarm');
  RemClassRegistry.RegisterXSClass(Ebs24NAlarm, 'http://www.fleetboard.com/data', 'Ebs24NAlarm');
  RemClassRegistry.RegisterXSClass(MilageAlarm, 'http://www.fleetboard.com/data', 'MilageAlarm');
  RemClassRegistry.RegisterXSInfo(TypeInfo(AdditionalState), 'http://www.fleetboard.com/data', 'AdditionalState');
  RemClassRegistry.RegisterXSInfo(TypeInfo(Array_Of_AdditionalState), 'http://www.fleetboard.com/data', 'Array_Of_AdditionalState');
  RemClassRegistry.RegisterXSClass(Coordinate, 'http://www.fleetboard.com/data', 'Coordinate');
  RemClassRegistry.RegisterXSClass(LocationDescription, 'http://www.fleetboard.com/data', 'LocationDescription');
  RemClassRegistry.RegisterExternalPropName(TypeInfo(LocationDescription), 'Label_', '[ExtName="Label"]');
  RemClassRegistry.RegisterXSClass(FieldsDef, 'http://www.fleetboard.com/data', 'FieldsDef');
  RemClassRegistry.RegisterXSClass(TrailerEventMsg, 'http://www.fleetboard.com/data', 'TrailerEventMsg');
  RemClassRegistry.RegisterXSClass(TmInfoDTO, 'http://www.fleetboard.com/data', 'TmInfoDTO');
  RemClassRegistry.RegisterXSClass(ForeignRef, 'http://www.fleetboard.com/data', 'ForeignRef');
  RemClassRegistry.RegisterExternalPropName(TypeInfo(ForeignRef), 'name_', '[ExtName="name"]');
  RemClassRegistry.RegisterXSInfo(TypeInfo(Times), 'http://www.fleetboard.com/data', 'Times');
  RemClassRegistry.RegisterXSClass(DriverStatusMsg, 'http://www.fleetboard.com/data', 'DriverStatusMsg');
  RemClassRegistry.RegisterXSClass(VehiclestatusMsg, 'http://www.fleetboard.com/data', 'VehiclestatusMsg');
  RemClassRegistry.RegisterXSClass(RefID, 'http://www.fleetboard.com/data', 'RefID');
  RemClassRegistry.RegisterXSClass(TourstatusMsg, 'http://www.fleetboard.com/data', 'TourstatusMsg');
  RemClassRegistry.RegisterXSClass(ReplystatusMsg, 'http://www.fleetboard.com/data', 'ReplystatusMsg');
  RemClassRegistry.RegisterXSInfo(TypeInfo(TextConfMsg), 'http://www.fleetboard.com/data', 'TextConfMsg');
  RemClassRegistry.RegisterXSClass(Body, 'http://www.fleetboard.com/data', 'Body');
  RemClassRegistry.RegisterExternalPropName(TypeInfo(Body), 'TextConfMsg', '[ArrayItemName="Command", Namespace="http://www.fleetboard.com/data"]');
  RemClassRegistry.RegisterXSClass(EventStatusMsg, 'http://www.fleetboard.com/data', 'EventStatusMsg');
  RemClassRegistry.RegisterXSClass(OrderstatusMsg, 'http://www.fleetboard.com/data', 'OrderstatusMsg');
  RemClassRegistry.RegisterXSClass(InboxDataMsg, 'http://www.fleetboard.com/data', 'InboxDataMsg');
  RemClassRegistry.RegisterExternalPropName(TypeInfo(InboxDataMsg), 'Message_', '[ExtName="Message"]');
  RemClassRegistry.RegisterXSClass(AreaMonitoring, 'http://www.fleetboard.com/data', 'AreaMonitoring');
  RemClassRegistry.RegisterXSClass(AutoPos, 'http://www.fleetboard.com/data', 'AutoPos');
  RemClassRegistry.RegisterExternalPropName(TypeInfo(AutoPos), 'Times', '[ArrayItemName="Time"]');
  RemClassRegistry.RegisterXSClass(SetpointData, 'http://www.fleetboard.com/data', 'SetpointData');
  RemClassRegistry.RegisterXSClass(TrailerPositionDTO, 'http://www.fleetboard.com/data', 'TrailerPositionDTO');
  RemClassRegistry.RegisterXSClass(FormDefMsg, 'http://www.fleetboard.com/data', 'FormDefMsg');
  RemClassRegistry.RegisterExternalPropName(TypeInfo(FormDefMsg), 'Name_', '[ExtName="Name"]');
  RemClassRegistry.RegisterXSInfo(TypeInfo(limitType), 'http://www.fleetboard.com/data', 'limitType');
  RemClassRegistry.RegisterXSInfo(TypeInfo(sessionidType), 'http://www.fleetboard.com/data', 'sessionidType');
  RemClassRegistry.RegisterXSInfo(TypeInfo(timestampType), 'http://www.fleetboard.com/data', 'timestampType');
  RemClassRegistry.RegisterXSClass(LastPositionState, 'http://www.fleetboard.com/data', 'LastPositionState');
  RemClassRegistry.RegisterXSClass(TPDate, 'http://www.fleetboard.com/data', 'TPDate');
  RemClassRegistry.RegisterExternalPropName(TypeInfo(TPDate), 'Begin_', '[ExtName="Begin"]');
  RemClassRegistry.RegisterExternalPropName(TypeInfo(TPDate), 'End_', '[ExtName="End"]');
  RemClassRegistry.RegisterXSInfo(TypeInfo(vehicleidType), 'http://www.fleetboard.com/data', 'vehicleidType');
  RemClassRegistry.RegisterXSClass(Position2, 'http://www.fleetboard.com/data', 'Position2', 'Position');
  RemClassRegistry.RegisterXSInfo(TypeInfo(Array_Of_vehicleidType), 'http://www.fleetboard.com/data', 'Array_Of_vehicleidType');
  RemClassRegistry.RegisterXSInfo(TypeInfo(fbidType), 'http://www.fleetboard.com/data', 'fbidType');
  RemClassRegistry.RegisterXSInfo(TypeInfo(Array_Of_fbidType), 'http://www.fleetboard.com/data', 'Array_Of_fbidType');
  RemClassRegistry.RegisterXSInfo(TypeInfo(inoidType), 'http://www.fleetboard.com/data', 'inoidType');
  RemClassRegistry.RegisterXSInfo(TypeInfo(Array_Of_inoidType), 'http://www.fleetboard.com/data', 'Array_Of_inoidType');
  RemClassRegistry.RegisterXSClass(CacheMsg, 'http://www.fleetboard.com/data', 'CacheMsg');
  RemClassRegistry.RegisterXSInfo(TypeInfo(offsetType), 'http://www.fleetboard.com/data', 'offsetType');
  RemClassRegistry.RegisterXSClass(GetLastPositionRequestType, 'http://www.fleetboard.com/data', 'GetLastPositionRequestType');
  RemClassRegistry.RegisterXSClass(GetLastPositionRequest, 'http://www.fleetboard.com/data', 'GetLastPositionRequest');
  RemClassRegistry.RegisterXSInfo(TypeInfo(responseSizeType), 'http://www.fleetboard.com/data', 'responseSizeType');
  RemClassRegistry.RegisterXSInfo(TypeInfo(resultSizeType), 'http://www.fleetboard.com/data', 'resultSizeType');
  RemClassRegistry.RegisterXSClass(GetLocationDescriptionResponseType, 'http://www.fleetboard.com/data', 'GetLocationDescriptionResponseType');
  RemClassRegistry.RegisterXSClass(GetLocationDescriptionResponse, 'http://www.fleetboard.com/data', 'GetLocationDescriptionResponse');
  RemClassRegistry.RegisterXSClass(GetLastPositionResponseType, 'http://www.fleetboard.com/data', 'GetLastPositionResponseType');
  RemClassRegistry.RegisterXSClass(GetLastPositionResponse, 'http://www.fleetboard.com/data', 'GetLastPositionResponse');
  RemClassRegistry.RegisterXSClass(GetReplyResponseType, 'http://www.fleetboard.com/data', 'GetReplyResponseType');
  RemClassRegistry.RegisterXSClass(GetReplyResponse, 'http://www.fleetboard.com/data', 'GetReplyResponse');
  RemClassRegistry.RegisterXSClass(GetPosReportTraceResponseType, 'http://www.fleetboard.com/data', 'GetPosReportTraceResponseType');
  RemClassRegistry.RegisterXSClass(GetPosReportTraceResponse, 'http://www.fleetboard.com/data', 'GetPosReportTraceResponse');
  RemClassRegistry.RegisterXSClass(GetCurrentStateResponseType, 'http://www.fleetboard.com/data', 'GetCurrentStateResponseType');
  RemClassRegistry.RegisterXSClass(GetCurrentStateResponse, 'http://www.fleetboard.com/data', 'GetCurrentStateResponse');
  RemClassRegistry.RegisterXSInfo(TypeInfo(versionType), 'http://www.fleetboard.com/data', 'versionType');
  RemClassRegistry.RegisterXSClass(GetLocationDescriptionRequestType, 'http://www.fleetboard.com/data', 'GetLocationDescriptionRequestType');
  RemClassRegistry.RegisterXSClass(GetLocationDescriptionRequest, 'http://www.fleetboard.com/data', 'GetLocationDescriptionRequest');
  RemClassRegistry.RegisterXSClass(GetReplyRequestType, 'http://www.fleetboard.com/data', 'GetReplyRequestType');
  RemClassRegistry.RegisterXSClass(GetReplyRequest, 'http://www.fleetboard.com/data', 'GetReplyRequest');
  RemClassRegistry.RegisterXSClass(GetPosReportTraceRequestType, 'http://www.fleetboard.com/data', 'GetPosReportTraceRequestType');
  RemClassRegistry.RegisterXSClass(GetPosReportTraceRequest, 'http://www.fleetboard.com/data', 'GetPosReportTraceRequest');
  RemClassRegistry.RegisterXSInfo(TypeInfo(telematicGroupIDType), 'http://www.fleetboard.com/data', 'telematicGroupIDType');
  RemClassRegistry.RegisterXSInfo(TypeInfo(vehicleGroupIdType), 'http://www.fleetboard.com/data', 'vehicleGroupIdType');
  RemClassRegistry.RegisterXSInfo(TypeInfo(Array_Of_vehicleGroupIdType), 'http://www.fleetboard.com/data', 'Array_Of_vehicleGroupIdType');
  RemClassRegistry.RegisterXSInfo(TypeInfo(driverNameIDType), 'http://www.fleetboard.com/data', 'driverNameIDType');
  RemClassRegistry.RegisterXSInfo(TypeInfo(Array_Of_driverNameIDType), 'http://www.fleetboard.com/data', 'Array_Of_driverNameIDType');
  RemClassRegistry.RegisterXSClass(GetPosReportResponseType, 'http://www.fleetboard.com/data', 'GetPosReportResponseType');
  RemClassRegistry.RegisterXSClass(GetPosReportResponse, 'http://www.fleetboard.com/data', 'GetPosReportResponse');
  RemClassRegistry.RegisterXSInfo(TypeInfo(driverGroupIdType), 'http://www.fleetboard.com/data', 'driverGroupIdType');
  RemClassRegistry.RegisterXSClass(GetPosReportRequestType, 'http://www.fleetboard.com/data', 'GetPosReportRequestType');
  RemClassRegistry.RegisterXSClass(GetPosReportRequest, 'http://www.fleetboard.com/data', 'GetPosReportRequest');
  RemClassRegistry.RegisterXSClass(GetCurrentStateRequestType, 'http://www.fleetboard.com/data', 'GetCurrentStateRequestType');
  RemClassRegistry.RegisterXSClass(GetCurrentStateRequest, 'http://www.fleetboard.com/data', 'GetCurrentStateRequest');
  RemClassRegistry.RegisterXSInfo(TypeInfo(FieldID), 'http://www.fleetboard.com/data', 'FieldID');
  RemClassRegistry.RegisterXSInfo(TypeInfo(Value), 'http://www.fleetboard.com/data', 'Value');
  RemClassRegistry.RegisterXSClass(Field, 'http://www.fleetboard.com/data', 'Field');
  RemClassRegistry.RegisterXSInfo(TypeInfo(Fields), 'http://www.fleetboard.com/data', 'Fields');
  RemClassRegistry.RegisterXSInfo(TypeInfo(ReadInfo), 'http://www.fleetboard.com/data', 'ReadInfo');
  RemClassRegistry.RegisterXSClass(TracePosition, 'http://www.fleetboard.com/data', 'TracePosition');
  RemClassRegistry.RegisterXSClass(State, 'http://www.fleetboard.com/data', 'State');
  RemClassRegistry.RegisterExternalPropName(TypeInfo(State), 'name_', '[ExtName="name"]');
  RemClassRegistry.RegisterXSInfo(TypeInfo(States), 'http://www.fleetboard.com/data', 'States');
  RemClassRegistry.RegisterXSClass(Header, 'http://www.fleetboard.com/data', 'Header');
  RemClassRegistry.RegisterExternalPropName(TypeInfo(Header), 'ReadInfo', '[ArrayItemName="User", Namespace="http://www.fleetboard.com/data"]');
  RemClassRegistry.RegisterExternalPropName(TypeInfo(Header), 'States', '[ArrayItemName="State"]');
  RemClassRegistry.RegisterXSInfo(TypeInfo(FormDefID), 'http://www.fleetboard.com/data', 'FormDefID');
  RemClassRegistry.RegisterXSClass(Form, 'http://www.fleetboard.com/data', 'Form');
  RemClassRegistry.RegisterExternalPropName(TypeInfo(Form), 'Fields', '[ArrayItemName="Field", Namespace="http://www.fleetboard.com/data"]');
  RemClassRegistry.RegisterXSClass(VhcLayout, 'http://www.fleetboard.com/data', 'VhcLayout');
  RemClassRegistry.RegisterXSClass(FormLayout, 'http://www.fleetboard.com/data', 'FormLayout');
  RemClassRegistry.RegisterXSClass(Behavior, 'http://www.fleetboard.com/data', 'Behavior');
  RemClassRegistry.RegisterXSClass(Hidden, 'http://www.fleetboard.com/data', 'Hidden');
  RemClassRegistry.RegisterXSClass(OrderMsg, 'http://www.fleetboard.com/data', 'OrderMsg');
  RemClassRegistry.RegisterXSClass(VhcStatusLayout, 'http://www.fleetboard.com/data', 'VhcStatusLayout');
  RemClassRegistry.RegisterXSClass(Semantics, 'http://www.fleetboard.com/data', 'Semantics');
  RemClassRegistry.RegisterExternalPropName(TypeInfo(Semantics), 'Scope', '[ArrayItemName="Time"]');
  RemClassRegistry.RegisterXSInfo(TypeInfo(SemanticsDef), 'http://www.fleetboard.com/data', 'SemanticsDef');
  RemClassRegistry.RegisterXSClass(FieldDef, 'http://www.fleetboard.com/data', 'FieldDef');
  RemClassRegistry.RegisterExternalPropName(TypeInfo(FieldDef), 'Name_', '[ExtName="Name"]');
  RemClassRegistry.RegisterExternalPropName(TypeInfo(FieldDef), 'Type_', '[ExtName="Type"]');
  RemClassRegistry.RegisterExternalPropName(TypeInfo(FieldDef), 'SemanticsDef', '[ArrayItemName="Semantics", Namespace="http://www.fleetboard.com/data"]');
  RemClassRegistry.RegisterXSClass(StatusDefMsg, 'http://www.fleetboard.com/data', 'StatusDefMsg');
  RemClassRegistry.RegisterExternalPropName(TypeInfo(StatusDefMsg), 'SemanticsDef', '[ArrayItemName="Semantics", Namespace="http://www.fleetboard.com/data"]');
  RemClassRegistry.RegisterXSInfo(TypeInfo(CurrentReeferOperationModeDTO), 'http://www.fleetboard.com/data', 'CurrentReeferOperationModeDTO');
  RemClassRegistry.RegisterXSClass(ReeferStartStopContinousChanged, 'http://www.fleetboard.com/data', 'ReeferStartStopContinousChanged');
  RemClassRegistry.RegisterXSInfo(TypeInfo(SetpointSensorDTO), 'http://www.fleetboard.com/data', 'SetpointSensorDTO');
  RemClassRegistry.RegisterXSClass(SetpointAlarm, 'http://www.fleetboard.com/data', 'SetpointAlarm');
  RemClassRegistry.RegisterXSInfo(TypeInfo(CurrentPowerModeDTO), 'http://www.fleetboard.com/data', 'CurrentPowerModeDTO');
  RemClassRegistry.RegisterXSClass(ReeferDieselElectricChanged, 'http://www.fleetboard.com/data', 'ReeferDieselElectricChanged');
  RemClassRegistry.RegisterXSInfo(TypeInfo(CurrentFuelLevelColorDTO), 'http://www.fleetboard.com/data', 'CurrentFuelLevelColorDTO');
  RemClassRegistry.RegisterXSClass(FuelLevelAlarm, 'http://www.fleetboard.com/data', 'FuelLevelAlarm');
  RemClassRegistry.RegisterXSInfo(TypeInfo(CompartmentOperationModeDTO), 'http://www.fleetboard.com/data', 'CompartmentOperationModeDTO');
  RemClassRegistry.RegisterExternalPropName(TypeInfo(CompartmentOperationModeDTO), 'On_', 'On');
  RemClassRegistry.RegisterXSClass(CurrentCompartmentDataList, 'http://www.fleetboard.com/data', 'CurrentCompartmentDataList');
  RemClassRegistry.RegisterXSClass(Positions, 'http://www.fleetboard.com/data', 'Positions');
  RemClassRegistry.RegisterXSInfo(TypeInfo(useridType), 'http://www.fleetboard.com/data', 'useridType');
  RemClassRegistry.RegisterXSInfo(TypeInfo(TemperatureSensorDTO), 'http://www.fleetboard.com/data', 'TemperatureSensorDTO');
  RemClassRegistry.RegisterXSClass(TemperatureAlarm, 'http://www.fleetboard.com/data', 'TemperatureAlarm');
  RemClassRegistry.RegisterXSInfo(TypeInfo(Device2VehicleConnectionStatusType), 'http://www.fleetboard.com/data', 'Device2VehicleConnectionStatusType');
  RemClassRegistry.RegisterXSClass(PairingMsg, 'http://www.fleetboard.com/data', 'PairingMsg');
  RemClassRegistry.RegisterXSInfo(TypeInfo(TirePositionDTO), 'http://www.fleetboard.com/data', 'TirePositionDTO');
  RemClassRegistry.RegisterXSClass(TirePressureAlarm, 'http://www.fleetboard.com/data', 'TirePressureAlarm');
  RemClassRegistry.RegisterXSInfo(TypeInfo(USERID), 'http://www.fleetboard.com/data', 'USERID');
  RemClassRegistry.RegisterXSInfo(TypeInfo(MsgID), 'http://www.fleetboard.com/data', 'MsgID');
  RemClassRegistry.RegisterXSClass(User, 'http://www.fleetboard.com/data', 'User');
  RemClassRegistry.RegisterXSInfo(TypeInfo(TracePositions), 'http://www.fleetboard.com/data', 'TracePositions');
  RemClassRegistry.RegisterXSClass(Position, 'http://www.fleetboard.com/data', 'Position');
  RemClassRegistry.RegisterExternalPropName(TypeInfo(Position), 'TracePositions', '[ArrayItemName="TracePosition"]');
  RemClassRegistry.RegisterXSInfo(TypeInfo(History), 'http://www.fleetboard.com/data', 'History');
  RemClassRegistry.RegisterXSClass(Msg, 'http://www.fleetboard.com/data', 'Msg');
  RemClassRegistry.RegisterExternalPropName(TypeInfo(Msg), 'History', '[ArrayItemName="User"]');
  RemClassRegistry.RegisterXSInfo(TypeInfo(WatchboxAlarmReasonDTO), 'http://www.fleetboard.com/data', 'WatchboxAlarmReasonDTO');
  RemClassRegistry.RegisterXSClass(WatchboxAlarm, 'http://www.fleetboard.com/data', 'WatchboxAlarm');
  RemClassRegistry.RegisterXSInfo(TypeInfo(BatteryConcernedDTO), 'http://www.fleetboard.com/data', 'BatteryConcernedDTO');
  RemClassRegistry.RegisterXSClass(UndervoltageAlarm, 'http://www.fleetboard.com/data', 'UndervoltageAlarm');
  RemClassRegistry.RegisterXSInfo(TypeInfo(TspAccountState), 'http://www.fleetboard.com/data', 'TspAccountState');
  RemClassRegistry.RegisterXSClass(TrailerRegisterMsg, 'http://www.fleetboard.com/data', 'TrailerRegisterMsg');
  RemClassRegistry.RegisterXSInfo(TypeInfo(TrailerCoupled), 'http://www.fleetboard.com/data', 'TrailerCoupled');
  RemClassRegistry.RegisterXSClass(TrailerCouplingMsg, 'http://www.fleetboard.com/data', 'TrailerCouplingMsg');
  RemClassRegistry.RegisterXSInfo(TypeInfo(TrailerState), 'http://www.fleetboard.com/data', 'TrailerState');
  RemClassRegistry.RegisterXSClass(TrailerSyncMsg, 'http://www.fleetboard.com/data', 'TrailerSyncMsg');
  RemClassRegistry.RegisterExternalPropName(TypeInfo(TrailerSyncMsg), 'name_', '[ExtName="name"]');
  RemClassRegistry.RegisterExternalPropName(TypeInfo(TrailerSyncMsg), 'trailerGroupIds', '[ArrayItemName="Time"]');

end.