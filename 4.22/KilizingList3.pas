unit KilizingList3;

interface

uses
  WinTypes, WinProcs, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, QuickRpt, QRExpr, Qrctrls, ExtCtrls, printers, DB, DBTables, Grids, IniFiles,
  ADODB, ClipBrd;

type
  TKilizingList3Dlg = class(TForm)
    Rep: TQuickRep;
    QRBand3: TQRBand;
    QRLabel2: TQRLabel;
	 procedure RepBeforePrint(Sender: TCustomQuickRep;
	   var PrintReport: Boolean);
    procedure RepNeedData(Sender: TObject; var MoreData: Boolean);
  private
		sorszam			: integer;
  public
	  vanadat			: boolean;
  end;

var
  KilizingList3Dlg: TKilizingList3Dlg;

implementation

uses
	J_SQL, Egyeb, KOzos;

{$R *.DFM}

procedure TKilizingList3Dlg.RepBeforePrint(Sender: TCustomQuickRep;
	   var PrintReport: Boolean);
begin
	sorszam			:= 0;
end;

procedure TKilizingList3Dlg.RepNeedData(Sender: TObject;
  var MoreData: Boolean);
begin
	MoreData	:= sorszam = 0;
	Inc(sorszam);
end;

end.
