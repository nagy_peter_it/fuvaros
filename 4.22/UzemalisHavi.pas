unit UzemalisHavi;

interface

uses
  WinTypes, WinProcs, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, QuickRpt, QRExpr, Qrctrls, ExtCtrls, printers, DB, DBTables, Egyeb, J_SQL,
  Kozos, ADODB ;

type
  TUzemaLisHaviDlg = class(TForm)
    QRLabel1: TQRLabel;
	 QRLabel13: TQRLabel;
    QRLabel14: TQRLabel;
    Query1: TADOQuery;
    Query2: TADOQuery;
    Query3: TADOQuery;
    RepUzem: TQuickRep;
    QRBand3: TQRBand;
    QRLSzamla: TQRLabel;
    QRLabel17: TQRLabel;
    QRLabel6: TQRLabel;
    QRSysData1: TQRSysData;
    QRLabel7: TQRLabel;
    QRSysData4: TQRSysData;
    QRLabel3: TQRLabel;
    QRBand1: TQRBand;
    QRBand2: TQRBand;
    QRLabel21: TQRLabel;
    Query4: TADOQuery;
    QRSubDetail1: TQRSubDetail;
    QRLabel8: TQRLabel;
    QRLabel16: TQRLabel;
    QRLabel2: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel11: TQRLabel;
    QRLabel18: TQRLabel;
    QRBand4: TQRBand;
    QRLabel10: TQRLabel;
    QRLabel12: TQRLabel;
    QRLabel15: TQRLabel;
    QL0: TQRLabel;
    QL2: TQRLabel;
    QL3: TQRLabel;
    QL4: TQRLabel;
    QL5: TQRLabel;
    QL6: TQRLabel;
    QL7: TQRLabel;
    QL8: TQRLabel;
    QL9: TQRLabel;
    QRShape1: TQRShape;
    QRLabel23: TQRLabel;
    QRLabel27: TQRLabel;
    QRShape19: TQRShape;
    QRLabel26: TQRLabel;
    QRShape18: TQRShape;
    QRLabel28: TQRLabel;
    QRLabel29: TQRLabel;
    QRShape7: TQRShape;
    QRLabel30: TQRLabel;
    QRLabel31: TQRLabel;
    QRShape5: TQRShape;
    QRLabel32: TQRLabel;
    QRLabel33: TQRLabel;
    QRShape8: TQRShape;
    QRLabel34: TQRLabel;
    QRLabel35: TQRLabel;
    QRShape6: TQRShape;
    QRLabel36: TQRLabel;
    QRLabel37: TQRLabel;
    QRShape2: TQRShape;
    QRLabel43: TQRLabel;
    QRLabel42: TQRLabel;
    QRShape29: TQRShape;
    QR0: TQRLabel;
    QR2: TQRLabel;
    QR3: TQRLabel;
    QR4: TQRLabel;
    QR5: TQRLabel;
    QR6: TQRLabel;
    QR7: TQRLabel;
    QR8: TQRLabel;
    QR9: TQRLabel;
    QRShape9: TQRShape;
    QO4: TQRLabel;
    QO5: TQRLabel;
    QO6: TQRLabel;
    QO7: TQRLabel;
    QO8: TQRLabel;
    QO9: TQRLabel;
    QL14: TQRLabel;
    QL15: TQRLabel;
    QL18: TQRLabel;
    QRShape3: TQRShape;
    QRShape4: TQRShape;
    QRLabel4: TQRLabel;
    QL10: TQRLabel;
    QR10: TQRLabel;
    procedure FormCreate(Sender: TObject);
    procedure	Tolt(rsz, datumtol, datumig, apnorma, telep : string );
    function SoforNevsor(SL: TStringList): string;
    procedure QRBand1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBand2BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRSubDetail1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBand4BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure RepUzemBeforePrint(Sender: TCustomQuickRep;
      var PrintReport: Boolean);
    procedure FormDestroy(Sender: TObject);

  private
       rsz             : string;
       d1              : string;
       d2              : string;
       nyikm           : string;
       zarkm           : string;
       kezdo           : string;
       sorsz           : integer;
       okm             : double;
       olit            : double;
       omeg            : double;
       kellsumma       : boolean;
       hianyvan        : boolean;
  public
	  vanadat			: boolean;
     kategoriak        : string;
  end;

var
  UzemaLisHaviDlg: TUzemaLisHaviDlg;

implementation

uses
	J_Fogyaszt, StrUtils;
{$R *.DFM}

procedure TUzemaLisHaviDlg.FormCreate(Sender: TObject);
begin
   Application.CreateForm(TFogyasztasDlg, FogyasztasDlg);
	EgyebDlg.SetADOQueryDatabase(Query1);
	EgyebDlg.SetADOQueryDatabase(Query2);
	EgyebDlg.SetADOQueryDatabase(Query3);
	EgyebDlg.SetADOQueryDatabase(Query4);
   rsz             := '';
end;

procedure	TUzemaLisHaviDlg.Tolt(rsz, datumtol, datumig, apnorma, telep  : string );
var
  sqlkateg, S : string;
begin
	d1		:= copy(datumtol,1,11);
	d2		:= copy(datumig,1,11);
   QRLabel17.Caption 	:= datumtol + ' - ' + datumig;
   sqlkateg    := '';
   if kategoriak <> '' then begin
       sqlkateg := ' AND GK_GKKAT IN ('+kategoriak+') ';
   end;
   if rsz <> '' then begin
      S:= 'SELECT GK_RESZ, GK_GKKAT, SZ_EGYEB1 FROM GEPKOCSI, SZOTAR WHERE ( GK_POTOS = 2 OR GK_POTOS = 0) ';
      // S:=S+' AND GK_ARHIV = 0 AND GK_KIVON = 0 ';
      // a kimutat�si id�szakban �l� aut� volt
      S:=S+' and GK_SFORH <= '''+d2+''' and ((GK_FORKI >= '''+d1+''') or (GK_FORKI = '''')) and ((GK_IFORK >= '''+d1+''') or (GK_IFORK = ''''))';
      S:=S+' AND SZ_FOKOD=''340'' AND SZ_ALKOD=GK_GKKAT AND GK_RESZ = '''+rsz+''' ';
	    Query_Run(Query1, S , true);
       QRBand2.Enabled := false;
       kellsumma       := false;
   end else begin
        S:='SELECT GK_RESZ, GK_GKKAT, SZ_EGYEB1 FROM GEPKOCSI, SZOTAR WHERE ( GK_POTOS = 2 OR GK_POTOS = 0) ';
        // S:=S+' AND GK_ARHIV = 0 AND GK_KIVON = 0 ';
        // a kimutat�si id�szakban �l� aut� volt
        S:=S+' and GK_SFORH <= '''+d2+''' and ((GK_FORKI >= '''+d1+''') or (GK_FORKI = '''')) and ((GK_IFORK >= '''+d1+''') or (GK_IFORK = ''''))';
        S:=S+' AND SZ_FOKOD=''340'' AND SZ_ALKOD=GK_GKKAT AND GK_NOKIM = 0 '+sqlkateg+' ORDER BY GK_GKKAT, GK_RESZ';
	      Query_Run(Query1, S , true);
       kellsumma       := true;
   end;
   try
       vanadat := Query1.RecordCount > 0;
   except
       vanadat := false;
   end;
end;

procedure TUzemaLisHaviDlg.QRBand1BeforePrint(Sender: TQRCustomBand;
	   var PrintBand: Boolean);
var
   notank: boolean;  // a kateg�ri�hoz nincsenek tankol�si adatok
begin
   rsz                 := Query1.FieldByName('GK_RESZ').AsString;
   if Query1.FieldByName('SZ_EGYEB1').AsInteger = 1 then notank:=True else notank:=False;
   QrLabel16.Caption   := rsz;
  	if Query_Run(Query3, 'SELECT * FROM GEPKOCSI WHERE GK_RESZ = '''+rsz+''' ', true) then begin
       QRLabel5.Caption 	:= Query3.FieldByname('GK_TIPUS').AsString;
       if Query3.FieldByname('GK_KLIMA').AsInteger > 0 then begin
 				QRLabel5.Caption 		:= QRLabel5.Caption 	+ ' (kl�m�s)';
		end;
       QRLabel18.Caption 	:= SzamString(Query3.FieldByname('GK_NORMA').AsFloat,8,2)+' / ' +SzamString(Query3.FieldByname('GK_NORNY').AsFloat,8,2);
   end else begin
       QRLabel5.Caption 	:= 'HI�NYZ� RENDSZ�M!!!';
   end;
   // A km-ek meghat�roz�sa
   hianyvan            := false;
   Query_Run(Query4, 'SELECT * FROM ZAROKM WHERE ZK_RENSZ = '''+rsz+''' AND ZK_DATUM = '''+d2+''' ');
   if Query4.RecordCount < 1 then begin
       QRLabel15.Caption   := 'Hi�nyz� z�r� km ('+d2+')';
       hianyvan            := true;
       nyikm               := '0';
       zarkm               := '0';
       kezdo               := '0';
       QL0.Caption	        := '* '+QL0.Caption ;
       Exit;
   end;
   nyikm               := Query4.FieldByName('ZK_NYITOKM').AsString;
   zarkm               := Query4.FieldByName('ZK_KMORA').AsString;
   QRLabel15.Caption   := nyikm + ' - ' + zarkm + ' = ' + IntToStr(StrToIntDef(zarkm, 0) - StrToIntDef(nyikm, 0))+' km';

   Query_Run(Query2, 'SELECT * FROM KOLTSEG WHERE KS_RENDSZ = '''+rsz+''' AND KS_TIPUS = 0 AND KS_TELE > 0 '+
       ' AND KS_KMORA >= '+IntToStr(StrToIntdef(nyikm, 0)) +' AND KS_KMORA <= '+IntToStr(StrToIntdef(zarkm, 0))+' ORDER BY KS_KMORA', true);
      // ezt kivettem, ha nincs k�lts�g akkor sem
      {
      try
       if Query2.RecordCount > 0 then begin
           kezdo   := nyikm;
           end;
      except
       // Nincs megfelel� elem
       kezdo   := nyikm;
      end;
      }

   kezdo   := nyikm;
   sorsz   := 1;
end;

procedure TUzemaLisHaviDlg.QRBand2BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
begin
   PrintBand       := kellsumma;
   QL14.Caption    := Format('%.0f', [okm]);
   QL15.Caption    := Format('%.2f', [olit]);
   QL18.Caption    := Format('%.2f', [omeg]);
end;

procedure TUzemaLisHaviDlg.QRSubDetail1BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
   notank: boolean;  // a kateg�ri�hoz nincsenek tankol�si adatok
begin
   QL2.Caption     := '';
   QL3.Caption     := '';
   // if not hianyvan then begin
   if not hianyvan and not (Query2.Eof) then begin
       QL0.Caption     := IntToStr(sorsz);
       QL2.Caption     := kezdo;
       QL3.Caption     := Query2.FieldByName('KS_KMORA').AsString;
       if Query1.FieldByName('SZ_EGYEB1').AsInteger = 1 then notank:=True else notank:=False;
       FogyasztasDlg.ClearAllList;
       FogyasztasDlg.GeneralBetoltes(rsz, StrToIntdef(kezdo, 0), StrToIntDef(Query2.FieldByName('KS_KMORA').AsString, 0) );
       FogyasztasDlg.CreateTankLista;
       FogyasztasDlg.CreateAdBlueLista;
       FogyasztasDlg.Szakaszolo;
       QL4.Caption     := Format('%.0f', [FogyasztasDlg.GetOsszKm]);
       if not(notank) then begin
          QL5.Caption     := Format('%.2f', [FogyasztasDlg.GetTenyFogyasztas]);
          QL6.Caption     := Format('%.2f', [FogyasztasDlg.GetAtlag]);
          QL7.Caption     := Format('%.2f', [FogyasztasDlg.GetSulyAtlag]);
          QL8.Caption     := Format('%.2f', [FogyasztasDlg.GetMegtakaritas]);
          if FogyasztasDlg.GetOsszKm > 0 then begin
            QL9.Caption     := Format('%.2f', [FogyasztasDlg.GetMegtakaritas / (FogyasztasDlg.GetOsszKm / 100)]);
          end else begin
            QL9.Caption     := '0.00';
            end;
          QL10.Caption:= SoforNevsor(FogyasztasDlg.GetSoforLista);
          end
       else begin
          QL5.Caption:= '';
          QL6.Caption:= '';
          QL7.Caption:= '';
          QL8.Caption:= '';
          QL9.Caption:= '';
          QL10.Caption:= '';
          end;
       kezdo := Query2.FieldByName('KS_KMORA').AsString;
       if FogyasztasDlg.GetAtlag = 0 then begin
           // Nincs k�vetkez� tankol�s
           if QL2.Caption <> QL3.Caption then  // csak ha mozgott
                QL0.Caption	    := '* '+QL0.Caption ;
       end;
   end;
   // PrintBand       := (QL2.Caption <> QL3.Caption) and ( NOT hianyvan);
   PrintBand:= (QL2.Caption <> QL3.Caption) and (QL3.Caption<>'') and ( NOT hianyvan);
   if PrintBand then begin
       Inc(sorsz);
   end;
end;

procedure TUzemaLisHaviDlg.QRBand4BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
   notank: boolean;  // a kateg�ri�hoz nincsenek tankol�si adatok
begin
   if Query1.FieldByName('SZ_EGYEB1').AsInteger = 1 then notank:=True else notank:=False;
   QR0.Caption     := IntToStr(sorsz);
   QR2.Caption     := kezdo;
   QR3.Caption     := zarkm;
   if QR2.Caption = QR3.Caption then begin
       // Mindent kinull�zunk
       QR0.Caption     := '';
       QR2.Caption     := '';
       QR3.Caption     := '';
       QR4.Caption     := '';
       QR5.Caption     := '';
       QR6.Caption     := '';
       QR7.Caption     := '';
       QR8.Caption     := '';
       QR9.Caption     := '';
       QR10.Caption     := '';
       end
   else begin
       // Az utols� szakasz sz�mol�sa
       FogyasztasDlg.ClearAllList;
       FogyasztasDlg.GeneralBetoltes(rsz, StrToIntdef(kezdo, 0), StrToIntDef(zarkm, 0) );
       FogyasztasDlg.CreateTankLista;
       FogyasztasDlg.CreateAdBlueLista;
       FogyasztasDlg.Szakaszolo;
       QR4.Caption     := Format('%.0f', [FogyasztasDlg.GetOsszKm]);
       if not(notank) then begin
          QR5.Caption     := Format('%.2f', [FogyasztasDlg.GetTenyFogyasztas]);
          QR6.Caption     := Format('%.2f', [FogyasztasDlg.GetAtlag]);
          QR7.Caption     := Format('%.2f', [FogyasztasDlg.GetSulyAtlag]);
          QR8.Caption     := Format('%.2f', [FogyasztasDlg.GetMegtakaritas]);
          if FogyasztasDlg.GetOsszKm > 0 then begin
           QR9.Caption     := Format('%.2f', [FogyasztasDlg.GetMegtakaritas / (FogyasztasDlg.GetOsszKm / 100)]);
           end
          else begin
           QR9.Caption     := '0.00';
           end;
          QR10.Caption:= SoforNevsor(FogyasztasDlg.GetSoforLista);
          end
       else begin
          QR5.Caption:= '';
          QR6.Caption:= '';
          QR7.Caption:= '';
          QR8.Caption:= '';
          QR9.Caption:= '';
          QR10.Caption:= '';
          end; // else
       end;  // else

   // A r�sz �sszesen sz�mol�sa
   QrLabel10.Caption   := rsz+' �sszesen : ';

   FogyasztasDlg.ClearAllList;
   FogyasztasDlg.GeneralBetoltes(rsz, StrToIntdef(nyikm, 0), StrToIntDef(zarkm, 0) );
   FogyasztasDlg.CreateTankLista;
   FogyasztasDlg.CreateAdBlueLista;
   FogyasztasDlg.Szakaszolo;
   QO4.Caption     := Format('%.0f', [FogyasztasDlg.GetOsszKm]);
   if not(notank) then begin
     QO5.Caption     := Format('%.2f', [FogyasztasDlg.GetTenyFogyasztas]);
     QO6.Caption     := Format('%.2f', [FogyasztasDlg.GetAtlag]);
     QO7.Caption     := Format('%.2f', [FogyasztasDlg.GetSulyAtlag]);
     QO8.Caption     := Format('%.2f', [FogyasztasDlg.GetMegtakaritas]);
     if FogyasztasDlg.GetOsszKm > 0 then begin
         QO9.Caption     := Format('%.2f', [FogyasztasDlg.GetMegtakaritas / (FogyasztasDlg.GetOsszKm / 100)]);
     end else begin
         QO9.Caption     := '0.00';
        end;
     end
   else begin
      QO5.Caption:= '';
      QO6.Caption:= '';
      QO7.Caption:= '';
      QO8.Caption:= '';
      QO9.Caption:= '';
      end;

   okm             := okm  + StrToIntDef(QO4.Caption, 0);
   olit            := olit + StringSzam(QO5.Caption);
   omeg            := omeg + StringSzam(QO8.Caption);
end;

procedure TUzemaLisHaviDlg.RepUzemBeforePrint(Sender: TCustomQuickRep;
  var PrintReport: Boolean);
begin
   okm     := 0;
   olit    := 0;
   omeg    := 0;
end;

procedure TUzemaLisHaviDlg.FormDestroy(Sender: TObject);
begin
   FogyasztasDlg.Destroy;
end;

function TUzemaLisHaviDlg.SoforNevsor(SL: TStringList): string;
var
  i: integer;
  S, ActSofor: string;
  kellmeg: boolean;
  SL2: TStringList;
begin
  SL2:=TStringList.Create;
  // a duplik�tumokat kisz�rj�k
  SL2:= EgyediElemek(SL);
  S:='';
  i:=0;
  kellmeg:= True;
  while (i<=SL2.Count - 1) and kellmeg  do begin
    ActSofor:= TitleCase(Query_Select('DOLGOZO', 'DO_KOD', SL2[i], 'DO_NAME'));  // egys�gesen csak az els� karakter nagybet�
    if S='' then begin
      S:=ActSofor;
      end
    else begin
      S:=S+', '+ActSofor;
      end;  // else
    Inc(i);
    end;  // while
  Result:=S;
end;


// "egyszerre csak egy n�v" verzi�, megn�velt�k a helyet, elf�r t�bb is.
{function TUzemaLisHaviDlg.SoforNevsor(SL: TStringList): string;
var
  i: integer;
  S, ActSofor: string;
  kellmeg: boolean;
begin
  // a kimeneti �rt�kben csak egy sof�r nev�t v�rjuk (a riporton nem f�r el t�bb). Ha esetleg t�bb van, "..." -al jelezz�k.
  S:='';
  i:=0;
  kellmeg:= True;
  while (i<=SL.Count - 1) and kellmeg  do begin
    ActSofor:=Query_Select('DOLGOZO', 'DO_KOD', SL[i], 'DO_NAME');
    if S='' then begin
      S:=ActSofor;
      end
    else begin
      if ActSofor <> S then begin
         S:=S+'...';
         kellmeg:=False;   // �gysem haszn�ljuk fel, ha van is t�bb n�v
         end;
      end;  // else
    Inc(i);
    end;  // while
  Result:=S;
end;
}
end.

