unit Adatimport;

interface

uses
	SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
	Forms, Dialogs, StdCtrls, Buttons, DB, DBTables, ADODB, FileCtrl, Mask,
  ExtCtrls ,Clipbrd ,IniFiles ;

type
  TDuplaString = record
       Egyik: string;
       Masik: string;
       end;  // record

type
	TAdatimportDlg = class(TForm)
	 QueryA1: TADOQuery;
	 Panel2: TPanel;
	 BitBtn6: TBitBtn;
	 BitBtn2: TBitBtn;
    Panel1: TPanel;
    Panel3: TPanel;
    Memo1: TMemo;
    Label1: TLabel;
    Label2: TLabel;
    Edit1: TEdit;
    Edit2: TEdit;
    ComboBox1: TComboBox;
    ADOQuery1: TADOQuery;
    ADOQuery2: TADOQuery;
    ADOQuery2DATABASE_NAME: TWideStringField;
    ADOQuery2DATABASE_SIZE: TIntegerField;
    ADOQuery2REMARKS: TStringField;
    ADOQuery3: TADOQuery;
    ADOQuery3spid: TSmallintField;
    ADOQuery3ecid: TSmallintField;
    ADOQuery3status: TWideStringField;
    ADOQuery3loginame: TWideStringField;
    ADOQuery3hostname: TWideStringField;
    ADOQuery3blk: TStringField;
    ADOQuery3dbname: TWideStringField;
    ADOQuery3cmd: TWideStringField;
    ADOQuery3request_id: TIntegerField;
    ADOConnection1: TADOConnection;
    TTrigger: TADOQuery;
    TTriggername: TWideStringField;
    TTriggerobject_id: TIntegerField;
    TTriggerparent_class: TWordField;
    TTriggerparent_class_desc: TWideStringField;
    TTriggerparent_id: TIntegerField;
    TTriggertype: TStringField;
    TTriggertype_desc: TWideStringField;
    TTriggercreate_date: TDateTimeField;
    TTriggermodify_date: TDateTimeField;
    TTriggeris_ms_shipped: TBooleanField;
    TTriggeris_disabled: TBooleanField;
    TTriggeris_not_for_replication: TBooleanField;
    TTriggeris_instead_of_trigger: TBooleanField;
    SQL1: TADOCommand;
    Button1: TButton;
    CheckBox1: TCheckBox;
	 procedure BitBtn2Click(Sender: TObject);
	 procedure BitBtn6Click(Sender: TObject);
	 procedure TablaMasolas( tname, dbname : string);
    procedure FormCreate(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    function TablaMezok(tabla:string): String;
    function TablaMezokListaba(db, tabla: string; var Lista: TStringList): boolean;
    function TablaMezokMindketto(forrasdb, forrastabla, celdb, celtabla: string): TDuplaString;
    procedure TriggerKiBe(tablanev:string; kibe:integer);
    procedure Button1Click(Sender: TObject);

	 private
       kilepes         : boolean;
       import_koz, import_dat: string;
       ForrasDB, celDB: string;
       forras: array of string;
       Inifn			: TIniFile;
       hiba:integer;
       Mezolistak: TDuplaString;

  end;
var
	AdatimportDlg: TAdatimportDlg;

implementation

uses
	Egyeb, J_SQL, Kozos, Fomenu;

{$R *.DFM}

procedure TAdatimportDlg.BitBtn2Click(Sender: TObject);
begin
   if not kilepes then begin
		kilepes := true;
		Exit;
	end;
	Close;
end;

{ R�GI: k�l�n DAT �s KOZ

procedure TAdatimportDlg.BitBtn6Click(Sender: TObject);
var
	str 		: string;
	tablename 	: string;
	t           : TDateTime;
	regilog	  	: integer;
begin
   import_koz  := forras[ComboBox1.Itemindex];
   import_dat  := StringReplace(import_koz,'KOZ','DAT',[rfReplaceAll]);
	kilepes     := true;
	if import_dat = '' then begin
		NoticeKi('A DAT t�blat�r forr�s megnevez�se hi�nyzik!');
		Exit;
	end;
	if import_koz = '' then begin
		NoticeKi('A KOZ t�blat�r forr�s megnevez�se hi�nyzik!');
		Exit;
	end;
	// A forr�s nem lehet azonos a mostanival
	if import_dat = EgyebDlg.v_alap_db then begin
		NoticeKi('A forr�s �s a c�l DAT t�blat�r nem lehet azonos!');
		Exit;
	end;
	if import_koz = EgyebDlg.v_koz_db then begin
		NoticeKi('A forr�s �s a c�l KOZ t�blat�r nem lehet azonos!');
		Exit;
	end;

   FomenuDlg.Timer1.Enabled:=False;
   FomenuDlg.Timer2.Enabled:=False;

	Screen.Cursor	:= crHourGlass;
//	EgyebDlg.log_level	:= 0;
	kilepes     	:= false;
	t           	:= now;

	Inifn.WriteString('IMPORT', 'TESZTDB','HIB�S ADATB�ZIS');
	Inifn.WriteString('IMPORT', 'TESZTDAT','');
   hiba:=0;

	Memo1.Lines.Clear;
	Memo1.Lines.Add('');
	Memo1.Lines.Add('Az import�l�s elkezd�d�tt ');
	Memo1.Lines.Add('');
	Memo1.Lines.Add('Forr�s : '+import_dat);
	QueryA1.Close;
	QueryA1.Tag	:= QUERY_ADAT_TAG;
	EgyebDlg.SeTADOQueryDatabase(QueryA1);

   TTrigger.Close;
	TTrigger.Connection:=QueryA1.Connection;
   SQL1.Connection:=QueryA1.Connection;

	Memo1.Lines.Add('');
   celDB:=EgyebDlg.v_alap_db;

	// V�gigmegy�nk a DAT t�bl�kon
  ForrasDB:=import_dat;
	str	:= DAT_TABLAK;
	BitBtn6.Hide;
	while ( ( str <> '' ) and ( not kilepes) ) do begin
		Application.ProcessMessages;
		if Pos(',', str) > 0 then begin
			tablename	:= copy(str, 1, Pos(',', str) - 1 );
			str			:= Trim( copy(str, Pos(',', str) + 1, 999 ));
		end else begin
			tablename	:= str;
			str	:= '';
		end;
		// A t�bla feldolgoz�sa
    TriggerKiBe(tablename,0); // Triggerek ki
	 TablaMasolas (tablename, import_dat);
    TriggerKiBe(tablename,1); // Triggerek be
	end;
	Memo1.Lines.Add('');
	Memo1.Lines.Add('Forr�s : '+import_koz);
	QueryA1.Close;
	QueryA1.Tag	:= QUERY_KOZOS_TAG;
	EgyebDlg.SeTADOQueryDatabase(QueryA1);
   TTrigger.Close;
	TTrigger.Connection:=QueryA1.Connection;
   SQL1.Connection:=QueryA1.Connection;

	Memo1.Lines.Add('');
   celDB   :=  EgyebDlg.v_koz_db;

	// V�gigmegy�nk a KOZ t�bl�kon
  ForrasDB:=import_koz;
	str	:= KOZ_TABLAK;
	while ( ( str <> '' ) and ( not kilepes) ) do begin
		Application.ProcessMessages;
		if Pos(',', str) > 0 then begin
			tablename	:= copy(str, 1, Pos(',', str) - 1 );
			str			:= Trim(copy(str, Pos(',', str) + 1, 999 ));
		end else begin
			tablename	:= str;
			str	:= '';
		end;
		// A t�bla feldolgoz�sa
    TriggerKiBe(tablename,0); // Triggerek ki
    TablaMasolas (tablename, import_koz);
    TriggerKiBe(tablename,1); // Triggerek be
	end;
	Memo1.Lines.Add('');
	BitBtn6.Show;

	if kilepes then begin
		Memo1.Lines.Add('A feldolgoz�s megszak�t�sra ker�lt');
	end else begin
  	Inifn.WriteString('IMPORT', 'TESZTDB',ComboBox1.Text);
	  Inifn.WriteString('IMPORT', 'TESZTDAT',DateToStr(date));
		Memo1.Lines.Add('A feldolgoz�s befejez�d�tt. ('+FormatDateTime('nn:ss',now-t)+')');
    //if hiba>0 then
  		Memo1.Lines.Add('HIBA: '+IntToStr(hiba));
		BitBtn6.Hide;
	end;
	Screen.Cursor	:= crDefault;
	Application.ProcessMessages;
	kilepes             := true;
//	EgyebDlg.log_level	:= regilog;
end;
}

procedure TAdatimportDlg.BitBtn6Click(Sender: TObject);
var
	str 		: string;
	tablename 	: string;
	t           : TDateTime;
	regilog	  	: integer;
begin
  import_dat  := forras[ComboBox1.Itemindex];
  import_koz  := StringReplace(import_dat,'DAT','KOZ',[rfReplaceAll]);
	kilepes     := true;
	if import_dat = '' then begin
		NoticeKi('A DAT t�blat�r forr�s megnevez�se hi�nyzik!');
		Exit;
	end;

	// A forr�s nem lehet azonos a mostanival
	if import_dat = EgyebDlg.v_alap_db then begin
		NoticeKi('A forr�s �s a c�l DAT t�blat�r nem lehet azonos!');
		Exit;
	end;

   FomenuDlg.Timer1.Enabled:=False;
   FomenuDlg.Timer2.Enabled:=False;

	Screen.Cursor	:= crHourGlass;
//	EgyebDlg.log_level	:= 0;
	kilepes     	:= false;
	t           	:= now;

	Inifn.WriteString('IMPORT', 'TESZTDB','HIB�S ADATB�ZIS');
	Inifn.WriteString('IMPORT', 'TESZTDAT','');
   hiba:=0;

	Memo1.Lines.Clear;
	Memo1.Lines.Add('');
	Memo1.Lines.Add('Az import�l�s elkezd�d�tt ');
	Memo1.Lines.Add('');
	Memo1.Lines.Add('Forr�s : '+import_dat);
  Memo1.Lines.Add('Opcion�lis KOZ forr�s : '+import_koz);
	QueryA1.Close;
	QueryA1.Tag	:= QUERY_ADAT_TAG;
	EgyebDlg.SeTADOQueryDatabase(QueryA1);

   TTrigger.Close;
	TTrigger.Connection:=QueryA1.Connection;
   SQL1.Connection:=QueryA1.Connection;

	Memo1.Lines.Add('');
   celDB:=EgyebDlg.v_alap_db;

	// V�gigmegy�nk az adattt�bl�kon
  ForrasDB:=import_dat;
	str	:= DAT_TABLAK + ', '+ KOZ_TABLAK;
	BitBtn6.Hide;
	while ( ( str <> '' ) and ( not kilepes) ) do begin
		Application.ProcessMessages;
		if Pos(',', str) > 0 then begin
			tablename	:= copy(str, 1, Pos(',', str) - 1 );
			str			:= Trim( copy(str, Pos(',', str) + 1, 999 ));
		end else begin
			tablename	:= str;
			str	:= '';
		end;
		// A t�bla feldolgoz�sa
    // a r�gi, k�t adatb�zisos szerkezetb�l val� import�l�shoz
    if CheckBox1.Checked then begin
      if Pos(tablename, DAT_TABLAK)>0 then
         ForrasDB:=import_dat
      else ForrasDB:=import_koz;
      Memo1.Lines.Add('-- '+ForrasDB+' / '+tablename);
      end;
    // ----------------------------------------

    TriggerKiBe(tablename,0); // Triggerek ki
	  TablaMasolas (tablename, ForrasDB);
    TriggerKiBe(tablename,1); // Triggerek be
	  end;  // while


	BitBtn6.Show;

	if kilepes then begin
		Memo1.Lines.Add('A feldolgoz�s megszak�t�sra ker�lt');
	end else begin
  	Inifn.WriteString('IMPORT', 'TESZTDB',ComboBox1.Text);
	  Inifn.WriteString('IMPORT', 'TESZTDAT',DateToStr(date));
		Memo1.Lines.Add('A feldolgoz�s befejez�d�tt. ('+FormatDateTime('nn:ss',now-t)+')');
    //if hiba>0 then
  		Memo1.Lines.Add('HIBA: '+IntToStr(hiba));
		BitBtn6.Hide;
	end;
	Screen.Cursor	:= crDefault;
	Application.ProcessMessages;
	kilepes             := true;
//	EgyebDlg.log_level	:= regilog;
end;

procedure TAdatimportDlg.Button1Click(Sender: TObject);
begin
	QueryA1.Close;
	QueryA1.Tag	:= QUERY_ADAT_TAG;
	EgyebDlg.SeTADOQueryDatabase(QueryA1);

   TTrigger.Close;
	TTrigger.Connection:=QueryA1.Connection;
   SQL1.Connection:=QueryA1.Connection;
  celDB:=EgyebDlg.v_alap_db;
  import_dat  := forras[ComboBox1.Itemindex];
  TablaMasolas ('MEGBIZASRESZ', import_dat);
end;

procedure TAdatimportDlg.TablaMasolas( tname, dbname : string);
var
	tabla_str,etname		: string;
	sorsz		: integer;
	createsql	: string;
 // 	sqlstr,mezok, mezok_		: string;
 	sqlstr,mezok		: string;
	rekszam		: integer;
   i           : integer;
   maxi        : integer;
   mezoksql: TDuplaString;
begin
	// A t�bl�k t�rl�se �s l�trehoz�sa
	tabla_str	:= GetTableFullName(dbname, tname);
   tabla_str   := StringReplace(tabla_str,' ','',[rfReplaceAll])  ;

   etname  := tname;
	tname	:= GetTableFullName(celDB, tname);
   tname   := StringReplace(tname,' ','',[rfReplaceAll])  ;

	sorsz	:= Memo1.Lines.Add('    '+tname+' t�bla import�l�sa folyamatban ...');
	if not Query_Run(QueryA1, 'SELECT * FROM '+tabla_str) then begin
		Memo1.lines[sorsz]	:= '!!! '+tname+' t�bla import�l�sa nem siker�lt!';
       inc(hiba);
		Exit;
	end;

	rekszam		:= QueryA1.RecordCount;
	//createsql	:= Create_Table_Str(tname, QueryA1 );
  {
	if  Pos('KOLTSEG_GYUJTO', trim(tname)) = 0 then begin
       sqlstr		:= 'DELETE FROM '+tname;
       if not Query_Run(QueryA1, sqlstr) then begin
           Memo1.lines[sorsz]	:= '!!! '+tname+' t�bla �r�t�se nem siker�lt!';
           inc(hiba);
           Exit;
       end;
       Memo1.lines[sorsz]	:= '    '+tname+' t�bla �r�t�se siker�lt';
   end else begin
       Query_Run(QueryA1, 'SELECT COUNT(*) AS DB FROM '+tabla_str);
       i       := 0;
       maxi    := StrToIntDef(QueryA1.FieldByName('DB').AsString, 0) div 100000 ;
       while i <= maxi do begin
           sqlstr	:= ' DELETE '+tname+' WHERE KS_KTKOD >= '+IntToStr(i*100000)+' AND  KS_KTKOD < '+IntToStr((i+1)*100000);
           if not Query_Run(QueryA1, sqlstr) then begin
               Memo1.lines[sorsz]	:= '!!! '+tname+' t�bla �r�t�se nem siker�lt!';
               inc(hiba);
               Exit;
           end;
           Inc(i);
       end;
   Memo1.lines[sorsz]	:= '    '+tname+' t�bla �r�t�se siker�lt';
   end;
        }
  sqlstr		:= 'TRUNCATE TABLE '+tname;
  if not Query_Run(QueryA1, sqlstr) then begin
      Memo1.lines[sorsz]	:= '!!! '+tname+' t�bla �r�t�se nem siker�lt!';
      inc(hiba);
      Exit;
      end;  // if
  Memo1.lines[sorsz]	:= '    '+tname+' t�bla �r�t�se siker�lt';

  // --- teszthez ----
  // if etname='MEGBIZASRESZ' then
  //  NoticeKi('MEGBIZASRESZ!');

  mezoksql:=TablaMezokMindketto(ForrasDB, etname, celDB, etname);
  if Trim(mezoksql.Egyik) = '' then begin
   	Memo1.lines[sorsz]	:= '    '+tname+' t�bla �resen maradt, a t�bla nem l�tezik a forr�s oldalon!';
    exit;  // procedure
    end;

	if  Pos('KOLTSEG_GYUJTO', trim(tname)) = 0 then begin
       sqlstr  :=  '';
       // mezok_  :=  TablaMezok(etname);
		// sqlstr		:= ' INSERT INTO '+tname+' ('+mezok_+') '+' SELECT '+mezok_+' FROM '+tabla_str;
    sqlstr		:= ' INSERT INTO '+tname+' ('+mezoksql.Masik+') '+' SELECT '+mezoksql.Egyik+' FROM '+tabla_str;
		if not Query_Run(QueryA1, sqlstr) then begin
			Memo1.lines[sorsz]	:= '!!! '+tname+' t�bla felt�lt�se nem siker�lt!';
           inc(hiba);
			Exit;
		end;
		Memo1.lines[sorsz]	:= '    '+tname+' t�bla felt�lt�se siker�lt';
		sqlstr		:= 'SELECT COUNT(*) AS DB FROM '+tname;
		if not Query_Run(QueryA1, sqlstr) then begin
			Memo1.lines[sorsz]	:= '!!! '+tname+' darabsz�m lek�rdez�s nem siker�lt!';
           inc(hiba);
			Exit;
		end;
		if StrToIntDef(QueryA1.FieldByName('DB').AsString, 0) <> rekszam then begin
			Memo1.lines[sorsz]	:= '!!! '+tname+' t�bla import�l�s�n�l r�gi <> �j : '+
			'('+IntToStr(rekszam) + ' <> '+QueryA1.FieldByName('DB').AsString +')';
           inc(hiba);
		end else begin
			Memo1.lines[sorsz]	:= '    '+tname+' t�bla import�l�sa sikeresen befejez�d�tt ('+IntToStr(rekszam)+')';
		end;
	end else begin
       // Ezt darabonk�nt kell import�lni
       sqlstr  := '';
       // mezok_  := TablaMezok(etname);
       Query_Run(QueryA1, 'SELECT COUNT(*) AS DB FROM '+tabla_str);
       i       := 0;
       maxi    := StrToIntDef(QueryA1.FieldByName('DB').AsString, 0) div 100000 ;
       while i <= maxi do begin
           // sqlstr	:= ' INSERT INTO '+tname+' ('+mezok_+') '+' SELECT '+mezok_+' FROM '+tabla_str+' WHERE KS_KTKOD >= '+IntToStr(i*100000)+' AND  KS_KTKOD < '+IntToStr((i+1)*100000);
           sqlstr	:= ' INSERT INTO '+tname+' ('+mezoksql.Masik+') '+' SELECT '+mezoksql.Egyik+' FROM '+tabla_str+' WHERE KS_KTKOD >= '+IntToStr(i*100000)+' AND  KS_KTKOD < '+IntToStr((i+1)*100000);
           if not Query_Run(QueryA1, sqlstr) then begin
               Memo1.lines[sorsz]	:= '!!! '+tname+' t�bla felt�lt�se nem siker�lt!';
               inc(hiba);
               Exit;
           end;
           Inc(i);
       end;
       Memo1.lines[sorsz]	:= '    '+tname+' t�bla felt�lt�se siker�lt';
       sqlstr		:= 'SELECT COUNT(*) AS DB FROM '+tname;
       if not Query_Run(QueryA1, sqlstr) then begin
           Memo1.lines[sorsz]	:= '!!! '+tname+' darabsz�m lek�rdez�s nem siker�lt!';
           inc(hiba);
           Exit;
       end;
       if StrToIntDef(QueryA1.FieldByName('DB').AsString, 0) <> rekszam then begin
           Memo1.lines[sorsz]	:= '!!! '+tname+' t�bla import�l�s�n�l r�gi <> �j : '+
           '('+IntToStr(rekszam) + ' <> '+QueryA1.FieldByName('DB').AsString +')';
           inc(hiba);
       end else begin
           Memo1.lines[sorsz]	:= '    '+tname+' t�bla import�l�sa sikeresen befejez�d�tt ('+IntToStr(rekszam)+')';
       end;
	end;
end;

procedure TAdatimportDlg.FormCreate(Sender: TObject);
var
  i:integer;
  adatb, DBName: string;
  Teszt_vagy_dev: boolean;
begin
	Inifn 				:= TIniFile.Create( EgyebDlg.globalini );
  Teszt_vagy_dev:=(pos('TESZT',UpperCase(EgyebDlg.v_alap_db))>0) or (pos('DEV',UpperCase(EgyebDlg.v_alap_db))>0);
  if not Teszt_vagy_dev then begin
    ShowMessage('Csak TESZT vagy DEV adatb�zisba lehet adatot bet�lteni!');
    exit;
  end;
  Edit1.Text:=EgyebDlg.v_koz_db;
  Edit2.Text:=EgyebDlg.v_alap_db;

  ADOQuery2.ExecSQL;
  ADOQuery2.Open;
  ADOQuery2.First;
  ADOQuery2.RecordCount;
  SetLength(forras,ADOQuery2.RecordCount);
  i:=0;
  while not ADOQuery2.Eof do begin
    DBName:= LowerCase(ADOQuery2DATABASE_NAME.Value);
    if  (pos('201', DBName)>0) and (pos('dat',DBName)>0) and (pos('teszt',DBName)=0) and (LowerCase(EgyebDlg.v_koz_db)<>LowerCase(ADOQuery2DATABASE_NAME.Name)) then
    begin
      //ComboBox1.Items.add(ADOQuery2DATABASE_NAME.Value);
      adatb:=ADOQuery2DATABASE_NAME.Value;
      if copy(adatb,1,3)='DAT' then adatb:='JS  '+StringReplace(adatb,'DAT','',[]);
      // if copy(adatb,1,4)='AQUA' then adatb:='AQUA '+StringReplace(adatb,'FRIGOKOZ','',[]);
      if copy(adatb,1,2)='DK' then adatb:='DK '+StringReplace(adatb,'DKDAT','',[]);
      // if copy(adatb,1,8)='FUVAR_01' then adatb:='L�NA '+StringReplace(adatb,'FUVAR_01KOZ','',[]);
      if (copy(adatb,1,3)='JS ') or (copy(adatb,1,3)='DK ') then begin  // nem akarunk m�st import�lni
          ComboBox1.Items.add(adatb);
          forras[i]:=ADOQuery2DATABASE_NAME.Value;
          inc(i);
          end;
    end;
    ADOQuery2.Next;
  end;
  ComboBox1.ItemIndex:=0;
  if ComboBox1.Items.IndexOf('JS  '+'2013')>-1 then
    ComboBox1.ItemIndex:= ComboBox1.Items.IndexOf('JS  '+'2013');
//  if ComboBox1.Items.IndexOf('JS  '+EgyebDlg.EvSzam)>-1 then
//    ComboBox1.ItemIndex:= ComboBox1.Items.IndexOf('JS  '+EgyebDlg.EvSzam);

end;

procedure TAdatimportDlg.FormActivate(Sender: TObject);
begin
  FomenuDlg.Timer2.Enabled:=False;
end;

procedure TAdatimportDlg.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
     FRestart:=True;
     ShowMessage('A program �jraindul!');
     FomenuDlg.Close;
end;

function TAdatimportDlg.TablaMezok(tabla: string): String;
var
  sql:string;
  oid:integer;
begin
   Result  := '';
   sql		:= 'SELECT object_id FROM sys.objects WHERE name = '''+tabla+''' ' ;
   if Query_Run(QueryA1, sql) then begin
       oid := QueryA1.FieldByName('object_id').AsInteger;
       sql	:= 'SELECT * FROM sys.[columns] where OBJECT_ID='+IntToStr(oid);
       if Query_Run(QueryA1, sql) then begin
           QueryA1.First;
           while not QueryA1.Eof do begin
               if (not QueryA1.FieldByName('is_computed').AsBoolean)and(not QueryA1.FieldByName('is_identity').AsBoolean) then begin
                   Result:=Result+QueryA1.FieldByName('name').AsString+',';
               end;
               QueryA1.Next;
           end;
           Result:=copy(Result,1,length(Result)-1);
       end;
   end;
end;

function TAdatimportDlg.TablaMezokListaba(db, tabla: string; var Lista: TStringList): boolean;
var
  sql:string;
  oid:integer;
begin
   Result := False;
   sql		:= 'SELECT object_id FROM '+db+'.sys.objects WHERE name = '''+tabla+''' ' ;
   if Query_Run(QueryA1, sql) then begin
       oid := QueryA1.FieldByName('object_id').AsInteger;
       if oid>0 then begin  // l�tez� t�bla
         sql	:= 'SELECT * FROM '+db+'.sys.[columns] where OBJECT_ID='+IntToStr(oid);
         if Query_Run(QueryA1, sql) then begin
           QueryA1.First;
           while not QueryA1.Eof do begin
              // if (not QueryA1.FieldByName('is_computed').AsBoolean)and(not QueryA1.FieldByName('is_identity').AsBoolean) then begin
               if (not QueryA1.FieldByName('is_computed').AsBoolean) then begin  // identity j�het!
                   Lista.Add(QueryA1.FieldByName('name').AsString);
                   end;  // if
               QueryA1.Next;
               end; // while
           end;  // if
           Result := True;
         end  // if oid>0
        else begin
          Result := False;  // nem l�tez� t�bla
          end;  // else
        end  // if
end;

function TAdatimportDlg.TablaMezokMindketto(forrasdb, forrastabla, celdb, celtabla: string): TDuplaString;
var
  ForrasLista, Cellista: TStringList;
  i1, i2: integer; // az aktu�lis forr�s �s c�lindex
begin
   // if Pos('SZFEJ', forrastabla)> 0 then
   //   NoticeKi('SZFEJ DEBUG');
   ForrasLista:= TStringList.Create;
   Cellista:= TStringList.Create;
   ForrasLista.Sorted:=True;
   CelLista.Sorted:=True;
   if TablaMezokListaba(forrasdb, forrastabla, ForrasLista) and TablaMezokListaba(celdb, celtabla, Cellista) then begin
       // Egyel�re annyit kezel, hogy ha a c�l t�bl�ban �j mez� jelent meg, NULL-lal felt�lti.
       // K�s�bb esetleg a t�r�lt mez�ket is kezelheti.
       i1:=0;
       for i2:=0 to Cellista.Count-1 do begin
          if (i1 <= ForrasLista.Count-1) and (ForrasLista[i1] = Cellista[i2]) then begin  // egyezik: mindk�t list�hoz hozz�adjuk
              Result.Egyik:=Result.Egyik+Cellista[i2]+',';
              Result.Masik:=Result.Masik+Cellista[i2]+',';
              Inc(i1); // tov�bbl�p�nk a ForrasList�ban is
              end  // if
          else begin
              Result.Egyik:=Result.Egyik+'NULL'+',';  // ez a forr�s oldal: NULL-t tesz�nk bele
              Result.Masik:=Result.Masik+Cellista[i2]+',';
              // nem l�p�nk tov�bb a ForrasList�ban
              end;  // else
          end; // for

       Result.Egyik:=copy(Result.Egyik,1,length(Result.Egyik)-1);
       Result.Masik:=copy(Result.Masik,1,length(Result.Masik)-1);
       end
    else begin
       Result.Egyik:='';
       Result.Masik:='';
       end;
end;

procedure TAdatimportDlg.TriggerKiBe(tablanev: string; kibe: integer);
var
  tnev,sql,triggernev:string;
  hossz: integer;
begin
 if kibe=0 then  // kikapcsol�s
 begin
  tnev:=tablanev+'_';
  hossz:=length(tnev);
  Try
    // SQL1.CommandText:='SET IDENTITY_INSERT '+tablanev+' ON GO';
    // SQL1.CommandText:='SET IDENTITY_INSERT '+tablanev+' ON';
    // SQL1.Execute;
    Command_Run (SQL1, 'SET IDENTITY_INSERT '+tablanev+' ON');
  Except
  End;

  sql:='select * from sys.triggers where is_disabled=0 and substring(name,1,'+IntToStr(hossz)+')='''+tnev+''' ' ;
  TTrigger.Close;
  TTrigger.SQL.Clear;
  TTrigger.SQL.Add(sql);
  TTrigger.Open;
 end;
 TTrigger.First;
 while not TTrigger.Eof do begin
    triggernev:=TTriggername.Value;
    if kibe=0 then
      // SQL1.CommandText:='ALTER TABLE '+tablanev+' DISABLE TRIGGER '+triggernev  ;
       Command_Run (SQL1, 'ALTER TABLE '+tablanev+' DISABLE TRIGGER '+triggernev);
    if kibe=1 then
      // SQL1.CommandText:='ALTER TABLE '+tablanev+' ENABLE TRIGGER '+triggernev  ;
      Command_Run (SQL1, 'ALTER TABLE '+tablanev+' ENABLE TRIGGER '+triggernev);

    // SQL1.Execute;
    TTrigger.Next;
 end;
 if kibe=1 then  // bekapcsol�s ut�n
 begin
   Try
    // SQL1.CommandText:='SET IDENTITY_INSERT '+tablanev+' OFF';
    // SQL1.Execute;
    Command_Run (SQL1, 'SET IDENTITY_INSERT '+tablanev+' OFF');
   Except
   End;
   TTrigger.Close;
 end;
end;

end.
