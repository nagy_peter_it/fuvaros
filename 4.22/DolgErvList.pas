unit DolgErvLIst;

interface

uses
  WinTypes, WinProcs, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, QuickRpt, QRExpr, Qrctrls, ExtCtrls, printers, DB, DBTables, Egyeb, Forgalom, J_SQL,
  Grids, Kozos, ADODB ;

type
  TDolgErvListDlg = class(TForm)
    Rep: TQuickRep;
    QRBand3: TQRBand;
    QRLSzamla: TQRLabel;
    QRLabel15: TQRLabel;
    QRLabel6: TQRLabel;
    QRSysData1: TQRSysData;
    QRLabel7: TQRLabel;
    QRSysData4: TQRSysData;
    QRLabel1: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel12: TQRLabel;
    QRLabel13: TQRLabel;
	 QRLabel14: TQRLabel;
	 QRLabel10: TQRLabel;
	 Query1: TADOQuery;
	 QDolgozo: TADOQuery;
	 QRShape1: TQRShape;
	 QRBand2: TQRBand;
	 QL3: TQRLabel;
	 QL1: TQRLabel;
	 QL2: TQRLabel;
	 QGepkocsi: TADOQuery;
	 QJarat: TADOQuery;
	 QL0: TQRLabel;
	 QRLabel2: TQRLabel;
	 QRLabel3: TQRLabel;
    QRLabel5: TQRLabel;
    QL4: TQRLabel;
	 procedure FormCreate(Sender: TObject);
	 procedure QRBand1BeforePrint(Sender: TQRCustomBand;
	   var PrintBand: Boolean);
	 procedure Tolto(nev, alkod : string; ordnr : integer);
	 procedure RepBeforePrint(Sender: TCustomQuickRep;
      var PrintReport: Boolean);
  private
		dolgozodir		: string;
		gepkocsidir		: string;
		jaratdir		: string;
		rekszam			: integer;
  public
  end;

var
  DolgErvListDlg: TDolgErvListDlg;

implementation

{$R *.DFM}

procedure TDolgErvListDlg.FormCreate(Sender: TObject);
begin
	EgyebDlg.SetADOQueryDatabase(Query1);
	EgyebDlg.SetADOQueryDatabase(QDolgozo);
	EgyebDlg.SetADOQueryDatabase(QGepkocsi);
	EgyebDlg.SetADOQueryDatabase(QJarat);
	dolgozodir	:= EgyebDlg.Read_SZGrid('999', '720');
	gepkocsidir	:= EgyebDlg.Read_SZGrid('999', '721');
	jaratdir	:= EgyebDlg.Read_SZGrid('999', '722');
	if ( ( copy(dolgozodir, Length(dolgozodir), 0) <> '\' ) and ( copy(dolgozodir, Length(dolgozodir), 0) <> '/' ) ) then begin
		dolgozodir	:= dolgozodir + '\';
	end;
	if ( ( copy(gepkocsidir, Length(gepkocsidir), 0) <> '\' ) and ( copy(gepkocsidir, Length(gepkocsidir), 0) <> '/' ) ) then begin
		gepkocsidir	:= gepkocsidir + '\';
	end;
	if ( ( copy(jaratdir, Length(jaratdir), 0) <> '\' ) and ( copy(jaratdir, Length(jaratdir), 0) <> '/' ) ) then begin
		jaratdir	:= jaratdir + '\';
	end;
	Query_Run(QDolgozo, 	'Select do_kod, do_kulso, do_arhiv, do_name from dolgozo ');
	Query_Run(QGepkocsi, 	'Select gk_kod, gk_arhiv, gk_kulso, gk_resz from gepkocsi');
	Query_Run(QJarat, 		'Select ja_kod, +ja_orsz, ja_alkod, ja_fazis from jarat');
end;

procedure TDolgErvListDlg.Tolto(nev, alkod : string; ordnr : integer);
var
	sqlstr	: string;
begin
	QrLabel3.Caption	:= nev;
	sqlstr	:= 'SELECT * FROM DOLGOZOERV, DOLGOZO WHERE DE_DOKOD = DO_KOD AND DE_ALKOD = '''+alkod+''' ';
	if ordnr = 0 then begin
		sqlstr	:= sqlstr + ' ORDER BY DE_DATUM' ;
	end else begin
		sqlstr	:= sqlstr + ' ORDER BY DO_NAME' ;
	end;
	Query_Run(Query1, sqlstr);
end;

procedure TDolgErvListDlg.QRBand1BeforePrint(Sender: TQRCustomBand;
	   var PrintBand: Boolean);
begin
	QL0.Caption	:= IntToStr(rekszam);
	Inc(rekszam);
	QL1.Caption	:= Query1.FieldByName('DE_DOKOD').AsString;
	QL2.Caption	:= Query1.FieldByName('DO_NAME').AsString;
	QL3.Caption	:= Query1.FieldByName('DE_MEGJE').AsString;
	QL4.Caption	:= Query1.FieldByName('DE_DATUM').AsString;
end;

procedure TDolgErvListDlg.RepBeforePrint(Sender: TCustomQuickRep;
  var PrintReport: Boolean);
begin
	rekszam	:= 1;
end;

end.
