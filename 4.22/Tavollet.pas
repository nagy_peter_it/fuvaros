unit Tavollet;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ADODB, Grids, DBGrids, ComCtrls, ExtCtrls, DBCtrls, StdCtrls,
  Buttons, Mask,  jpeg,Shellapi;

type TMyGrid=class(TDBGrid) end;

type
  TFTavollet = class(TForm)
    TDolgozo: TADODataSet;
    TTavollet: TADODataSet;
    TDolgozoDO_KOD: TStringField;
    TDolgozoDO_NAME: TStringField;
    TDolgozoDO_KULSO: TIntegerField;
    TDolgozoDO_ARHIV: TIntegerField;
    TDolgozoHO: TStringField;
    TDolgozoN01: TStringField;
    TDolgozoN02: TStringField;
    TDolgozoN03: TStringField;
    DSDolgozo: TDataSource;
    T_Tavollet: TADOQuery;
    TTavolletTA_DOKOD: TStringField;
    TTavolletTA_DONEV: TStringField;
    TTavolletTA_HONAP: TStringField;
    TTavolletTA_NAP01: TStringField;
    TTavolletTA_NAP02: TStringField;
    TTavolletTA_NAP03: TStringField;
    TTavolletTA_NAP04: TStringField;
    TTavolletTA_NAP05: TStringField;
    TTavolletTA_NAP06: TStringField;
    TTavolletTA_NAP07: TStringField;
    TTavolletTA_NAP08: TStringField;
    TTavolletTA_NAP09: TStringField;
    TTavolletTA_NAP10: TStringField;
    TTavolletTA_NAP11: TStringField;
    TTavolletTA_NAP12: TStringField;
    TTavolletTA_NAP13: TStringField;
    TTavolletTA_NAP14: TStringField;
    TTavolletTA_NAP15: TStringField;
    TTavolletTA_NAP16: TStringField;
    TTavolletTA_NAP17: TStringField;
    TTavolletTA_NAP18: TStringField;
    TTavolletTA_NAP19: TStringField;
    TTavolletTA_NAP20: TStringField;
    TTavolletTA_NAP21: TStringField;
    TTavolletTA_NAP22: TStringField;
    TTavolletTA_NAP23: TStringField;
    TTavolletTA_NAP24: TStringField;
    TTavolletTA_NAP25: TStringField;
    TTavolletTA_NAP26: TStringField;
    TTavolletTA_NAP27: TStringField;
    TTavolletTA_NAP28: TStringField;
    TTavolletTA_NAP29: TStringField;
    TTavolletTA_NAP30: TStringField;
    TTavolletTA_NAP31: TStringField;
    TTavolletTA_AKTIV: TIntegerField;
    TDolgozoNAP04: TStringField;
    TDolgozoNAP05: TStringField;
    TDolgozoNAP06: TStringField;
    TDolgozoNAP07: TStringField;
    TDolgozoNAP08: TStringField;
    TDolgozoNAP09: TStringField;
    TDolgozoNAP10: TStringField;
    TDolgozoNAP11: TStringField;
    TDolgozoNAP12: TStringField;
    TDolgozoNAP13: TStringField;
    TDolgozoNAP14: TStringField;
    TDolgozoNAP15: TStringField;
    TDolgozoNAP16: TStringField;
    TDolgozoNAP17: TStringField;
    TDolgozoNAP18: TStringField;
    TDolgozoNAP19: TStringField;
    TDolgozoNAP20: TStringField;
    TDolgozoNAP21: TStringField;
    TDolgozoNAP22: TStringField;
    TDolgozoNAP23: TStringField;
    TDolgozoNAP24: TStringField;
    TDolgozoNAP25: TStringField;
    TDolgozoNAP26: TStringField;
    TDolgozoNAP27: TStringField;
    TDolgozoNAP28: TStringField;
    TDolgozoNAP29: TStringField;
    TDolgozoNAP30: TStringField;
    TDolgozoNAP31: TStringField;
    Timer1: TTimer;
    Timer2: TTimer;
    TTavolletTA_O_X: TIntegerField;
    TDolgozoO_X: TIntegerField;
    TTavolletTA_O_SZ: TIntegerField;
    TTavolletTA_O_B: TIntegerField;
    TTavolletTA_O_EI: TIntegerField;
    TTavolletTA_O_EL: TIntegerField;
    TDolgozoO_SZ: TIntegerField;
    TDolgozoO_B: TIntegerField;
    TDolgozoO_EI: TIntegerField;
    TDolgozoO_EL: TIntegerField;
    TDolgozoDO_SZABI: TIntegerField;
    T_Dolgozo: TADODataSet;
    DSTavollet: TDataSource;
    T_DolgozoDO_KOD: TStringField;
    T_DolgozoDO_NAME: TStringField;
    T_DolgozoDO_KULSO: TIntegerField;
    T_DolgozoDO_ARHIV: TIntegerField;
    T_DolgozoDO_KSZAB: TIntegerField;
    T_DolgozoDO_SZABI: TIntegerField;
    TTavolletKSZAB: TIntegerField;
    TDolgozoKSZAB: TIntegerField;
    TDolgozoMSZAB: TIntegerField;
    Panel3: TPanel;
    TabControl3: TTabControl;
    TabControl1: TTabControl;
    DBText2: TDBText;
    Panel1: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    DBText1: TDBText;
    Label3: TLabel;
    Label4: TLabel;
    DBNavigator1: TDBNavigator;
    RadioButton1: TRadioButton;
    RadioButton2: TRadioButton;
    RadioButton3: TRadioButton;
    RadioButton4: TRadioButton;
    RadioButton5: TRadioButton;
    RadioButton6: TRadioButton;
    kereso: TEdit;
    BitKilep: TBitBtn;
    CheckBox1: TCheckBox;
    DBEdit1: TDBEdit;
    Panel2: TPanel;
    DBGrid2: TDBGrid;
    T_DolgozoDO_BERKOD: TStringField;
    TTavolletTA_BERKOD: TStringField;
    TTavolletTA_CSKOD: TStringField;
    TTavolletTA_CSNEV: TStringField;
    TDolgozoBERKOD: TStringField;
    TDolgozoDO_BERKOD: TStringField;
    Label5: TLabel;
    DBEdit2: TDBEdit;
    TDCsop: TADOQuery;
    TDolgozoDO_KSZAB: TIntegerField;
    TDolgozoDO_CSKOD: TStringField;
    TDolgozoDO_CSNEV: TStringField;
    TDCsopDC_DOKOD: TStringField;
    TDCsopDC_DONEV: TStringField;
    TDCsopDC_DATTOL: TStringField;
    TDCsopDC_CSKOD: TStringField;
    TDCsopDC_CSNEV: TStringField;
    BTBerel: TBitBtn;
    TDolgozoDO_ADOSZ: TStringField;
    Panel4: TPanel;
    Panel5: TPanel;
    DBGrid1: TDBGrid;
    TJOGOK: TADOQuery;
    GroupBox1: TGroupBox;
    Edit7: TEdit;
    Edit8: TEdit;
    Edit9: TEdit;
    Edit10: TEdit;
    Edit11: TEdit;
    Edit12: TEdit;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    GroupBox2: TGroupBox;
    Edit1: TEdit;
    Edit2: TEdit;
    Edit3: TEdit;
    Edit4: TEdit;
    Edit5: TEdit;
    Edit6: TEdit;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    TDolgozoSZABIARAMY: TIntegerField;
    TDFOTO: TADOQuery;
    TDFOTODO_KOD: TStringField;
    TDFOTODO_FOTO: TBlobField;
    Button1: TButton;
    Image2: TImage;
    TNapok: TADOQuery;
    TNapokNA_ORSZA: TStringField;
    TNapokNA_DATUM: TDateTimeField;
    TNapokNA_UNNEP: TIntegerField;
    TNapokNA_MEGJE: TStringField;
    TNapokNA_EV: TIntegerField;
    TNapokNA_HO: TIntegerField;
    TTavolletTA_EV: TStringField;
    Edit_EV: TEdit;
    UpDown11: TUpDown;
    SZABI: TADODataSet;
    SZABISB_DOKOD: TStringField;
    SZABISB_EV: TStringField;
    SZABISB_SZABI: TIntegerField;
    SZABISB_ATHOZ: TIntegerField;
    SZABISB_KSZAB: TIntegerField;
    SZABISB_MSZAB: TIntegerField;
    SZABISB_OSZAB: TIntegerField;
    TDolgozoOSZAB2: TIntegerField;
    TDolgozoKSZAB2: TIntegerField;
    TDolgozoMSZAB2: TIntegerField;
    Label6: TLabel;
    kiszabi: TADOQuery;
    SZABISB_ESZAB: TIntegerField;
    SZABISB_SATHO: TIntegerField;
    TDolgozoESZAB2: TIntegerField;
    TDolgozoASZAB2: TIntegerField;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    TDolgozoSZABI2: TIntegerField;
    Button2: TButton;
    ADOCommand1: TADOCommand;
    UpDown1: TUpDown;
    procedure FormCreate(Sender: TObject);
    procedure TabControl1Change(Sender: TObject);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure DBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure RadioButton1Click(Sender: TObject);
    procedure RadioButton2Click(Sender: TObject);
    procedure RadioButton3Click(Sender: TObject);
    procedure RadioButton4Click(Sender: TObject);
    procedure RadioButton5Click(Sender: TObject);
    procedure RadioButton6Click(Sender: TObject);
    procedure keresoEnter(Sender: TObject);
    procedure TDolgozoAfterScroll(DataSet: TDataSet);
    procedure Timer1Timer(Sender: TObject);
    procedure DBGrid1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBGrid1ColEnter(Sender: TObject);
    procedure DBGrid1KeyPress(Sender: TObject; var Key: Char);
    procedure Timer2Timer(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure DSDolgozoDataChange(Sender: TObject; Field: TField);
    procedure BitKilepClick(Sender: TObject);
    procedure CheckBox1Click(Sender: TObject);
    procedure TTavolletBeforePost(DataSet: TDataSet);
    procedure DBGrid2Enter(Sender: TObject);
    procedure DBGrid2DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure TDolgozoAfterOpen(DataSet: TDataSet);
    procedure DBEdit1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure TDolgozoCalcFields(DataSet: TDataSet);
    procedure DBEdit1Exit(Sender: TObject);
    procedure BTBerelClick(Sender: TObject);
    procedure TDolgozoDO_ADOSZValidate(Sender: TField);
    procedure TabControl3Change(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Image2Click(Sender: TObject);
    procedure TabControl1Changing(Sender: TObject;
      var AllowChange: Boolean);
    procedure UpDown11ChangingEx(Sender: TObject; var AllowChange: Boolean;
      NewValue: Smallint; Direction: TUpDownDirection);
    procedure SZABIBeforeOpen(DataSet: TDataSet);
    procedure TTavolletAfterPost(DataSet: TDataSet);
    procedure Button2Click(Sender: TObject);
    procedure DolgozoFissit;
    procedure UpDown1ChangingEx(Sender: TObject; var AllowChange: Boolean;
      NewValue: Integer; Direction: TUpDownDirection);
    procedure TabControl1DrawTab(Control: TCustomTabControl; TabIndex: Integer;
      const Rect: TRect; Active: Boolean);
    procedure TabControl3DrawTab(Control: TCustomTabControl; TabIndex: Integer;
      const Rect: TRect; Active: Boolean);
  private
    { Private declarations }
    DolgozoMutat: boolean;
    Procedure Beir(jel:string);
    function DateToDName(datum:TDateTime):string ;
    procedure Rend_szures;
    function WriteLogFile(FName:string; Text:string):Boolean;
    function KivettSzabi(DOkod, Ev : string): integer;
  public
    vanadat    : boolean;
  end;

var
  FTavollet: TFTavollet;
  AKTHO,AKTEV, TJEL,MOSTANIEV :string;
  GridHint:string;
  szov_hossz:integer;
  szov,oldrecord:string;
  RSZAZ:integer;
  kep2:string;
  dokod: string;
  dorec: TBookmark;

implementation

uses Egyeb, Fomenu, DateUtils, Kozos, Math,J_SQL;

{$R *.dfm}

procedure TFTavollet.FormCreate(Sender: TObject);
var
  i:integer;
  sqlstr   : string;
begin
  DolgozoMutat:=True;
  AKTEV:=EgyebDlg.v_evszam;
  MOSTANIEV:=IntToStr( YearOf(date));
  AKTEV:=MOSTANIEV;
  UpDown1.Max:=YearOf(date)+1;
  UpDown1.Position:=StrToInt( AKTEV);
  RSZAZ:=StrToIntDef(EgyebDlg.Read_SZGrid( '999', '921' ),50);
  if RSZAZ=0 then RSZAZ:=100;
  //RSZAZ:=100;
  TJEL:='SZ';
  TMyGrid(DBGrid2).ScrollBars:=ssNone;
   TabControl3.Tabs.Clear;
   // if (EgyebDlg.user_super)or(Query_Select2('JOGOS','JO_FEKOD','JO_DIKOD',EgyebDlg.user_code,'567','JO_SUPER')='1') then begin
   if (EgyebDlg.user_super) or (GetRightTag(567) <> RG_NORIGHT) then begin
       // Csin�lunk egy olyan csoportot amiben minden dolgoz� benne van
       TabControl3.Tabs.Add('*Minden csoport*');
   end;
   sqlstr  := 'SELECT SZ_MENEV FROM SZOTAR WHERE SZ_FOKOD = ''400'' AND SZ_MENEV IN (select DISTINCT DO_CSNEV from DOLGOZO where DO_KULSO=0 and DO_ARHIV=0)';
   if not EgyebDlg.user_super then begin
       sqlstr  := sqlstr + ' AND SZ_LEIRO LIKE ''%'+EgyebDlg.user_code+'%'' ';
   end;
   Query_Run(TJOGOK, sqlstr);
   while not TJOGOK.Eof do begin
       TabControl3.Tabs.Add( TJOGOK.FieldByName('SZ_MENEV').AsString );
       TJOGOK.Next;
   end;
   if Tabcontrol3.Tabs.Count < 1 then begin
       NoticeKi('A felhaszn�l�nak egyik dolgoz�i csoporthoz sincs hozz�f�r�se! Sz�t�r, 400-as f�csoport! ');
       vanadat                 := false;
   end else begin
       vanadat                 := true;
       TabControl3.Tabindex    := 0;
       TabControl3.OnChange(self);
       TabControl1.TabIndex:=MonthOf(now)-1;
       TabControl1.OnChange(self);
       DolgozoFissit;  // az OnChange nem h�vja a Scroll-t, ha m�r eleve az els� adatsoron �ll.
   end;
end;

procedure TFTavollet.TabControl1Change(Sender: TObject);
var
  i,n:integer;
  rec:TBookmark;
  dat:string;
  OSZ,OKSZ,OMSZ:integer;
  kszaz:double;
  nap: Tdatetime;
begin
  Caption:=AKTEV+'  '+TabControl1.Tabs[TabControl1.tabindex];
  Label6.Caption:=TabControl1.Tabs[TabControl1.tabindex];
  AKTHO:='';
  if TabControl1.TabIndex+1<10 then
    AKTHO:='0';
  AKTHO:=AKTHO+IntToStr(TabControl1.TabIndex+1);
  TTavollet.Close;
  TTavollet.Parameters[0].Value:=AKTEV;
  TTavollet.Parameters[1].Value:=AKTHO;
  TTavollet.Open;

  TNapok.Close;
//  TNapok.Parameters[0].Value:=StrToInt( EgyebDlg.v_evszam);
//  TNapok.Parameters[1].Value:=StrToInt(AKTHO);
  TNapok.Open;

  SZABI.Close;

  if TDolgozo.Active then begin
    rec:=TDolgozo.GetBookmark;
    TDolgozo.Close;
  end;
  dat:=TabControl3.Tabs[TabControl3.TabIndex];
  if copy(TabControl3.Tabs[TabControl3.TabIndex],1,1)<>'*' then
    TDolgozo.Parameters[0].Value:=TabControl3.Tabs[TabControl3.TabIndex];

  GroupBox2.Caption:=TabControl3.Tabs[TabControl3.TabIndex];
  TDolgozo.DisableControls;
  DolgozoMutat:=False;  // a TDolgozoAfterScroll-ban vizsg�ljuk

  TDolgozo.Open;
  ///////////////////
  OSZ:=0;
  OKSZ:=0;
  OMSZ:=0;

  TDolgozo.First;
  while not TDolgozo.Eof do begin
    OSZ:=OSZ+TDolgozoOSZAB2.Value;
    OKSZ:=OKSZ+TDolgozoKSZAB2.Value;
    OMSZ:=OMSZ+TDolgozoMSZAB2.Value;
    TDolgozo.Next;
    end;  // while
  TDolgozo.EnableControls;
  DolgozoMutat:=True;
  Try
      TDolgozo.GotoBookmark(dorec);
  Except
    TDolgozo.First;
  End;

  Edit1.Text:=IntToStr(OSZ);
  Edit2.Text:=IntToStr(OKSZ);
  Edit3.Text:=IntToStr(OMSZ);
  kszaz:=0;
  if OSZ<>0 then
    kszaz:=RoundTo(OKSZ/OSZ*100,0);

  Edit5.Text:=CurrToStr(kszaz)+' %';
  Edit6.Text:=CurrToStr(100-kszaz)+' %';
 // DBGrid1.Refresh;

  n:=0;
  for i:=1 to 31 do
  begin
    DBGrid1.Columns[i].Title.Caption:=inttostr(i);
    try
        nap:= EncodeDate(StrToInt(AKTEV), StrToInt(AKTHO),i);

        if DayOfWeek(nap) in [1,7] then                     // h�tv�ge
          DBGrid1.Columns[i].Title.Font.Color:=clRed
        else
          DBGrid1.Columns[i].Title.Font.Color:=clWindowText;

        if TNapok.Locate('NA_DATUM',nap,[loCaseInsensitive]) then begin
          if TNapokNA_UNNEP.Value=1 then  // �nnep
            DBGrid1.Columns[i].Title.Font.Color:=clRed
          else
            DBGrid1.Columns[i].Title.Font.Color:=clWindowText;
        end;
        DBGrid1.Columns[i].Visible:=True;
        n:=n+1;
    except
      on E: EConvertError do
        DBGrid1.Columns[i].Visible:=False;
    end;
  end;

end;

procedure TFTavollet.Beir(jel:string);
var
  Year,Month,Day: Word;
  rec:TBookMark;
  datum,cskod,csnev:string;
begin
//  if DBGrid1.SelectedField.FieldName='DO_NAME' then abort;
  if DBGrid1.SelectedIndex=0 then abort;
  if TDolgozoDO_ARHIV.Value>0 then abort;
  rec:=TDolgozo.GetBookmark;

  if not SZABI.Active then SZABI.Open;
  //if not SZABI.Locate('SB_DOKOD',TTavolletTA_DOKOD.Value,[loCaseInsensitive]) then
  if not SZABI.Locate('SB_DOKOD',TDolgozoDO_KOD.Value,[loCaseInsensitive]) then
  begin
      SZABI.Append;
      SZABISB_EV.Value:=AKTEV;
      SZABISB_DOKOD.Value:=TDolgozoDO_KOD.Value;
      SZABISB_SZABI.Value:=TDolgozoDO_SZABI.Value;
      SZABISB_KSZAB.Value:=TDolgozoDO_KSZAB.Value;
      SZABI.Post;
      SZABI.Close;
      SZABI.Open;
      TDolgozo.GotoBookmark(TDolgozo.GetBookmark);
      //DBGrid2.Refresh;
  end;


  Year:=StrToInt(AKTEV);
  Month:=strtoint(AKTHO);
  Day:=strtoint(copy(DBGrid1.SelectedField.FieldName,4,2));
  datum:= DateToStr(EncodeDate(Year,Month,Day));
  if (Month < MonthOf(date))and(Year <= YearOf(date)) then
  begin
    if EgyebDlg.user_super then
    begin
      if MessageBox(0, 'El�z� id�szak!'+#13+#10+'M�dos�tja?', '', MB_ICONQUESTION or MB_YESNO or MB_TASKMODAL or MB_DEFBUTTON2)=IDNO then
        exit;
    end
    else
    begin
      MessageBox(0, 'El�z� id�szakot csak Rendszergazda m�dos�that!', '', MB_ICONSTOP or MB_OK or MB_TASKMODAL);
      exit;
    end;
  end;

  if (jel='SZ')and(DBGrid1.Columns[DBGrid1.SelectedIndex].Title.Font.Color=clRed) then       // csak nunkanapra lehet Szabit �rni.
  begin
      MessageBox(0, 'Csak munkanapra lehet szabads�got ki�rni!', '', MB_ICONSTOP or MB_OK or MB_TASKMODAL);
      exit;
  end;

  TDCsop.Close;
  TDCsop.Parameters[0].Value:=TDolgozoDO_KOD.Value;
  TDCsop.Parameters[1].Value:=datum;
  TDCsop.Open;
  TDCsop.First;
  cskod:=TDCsopDC_CSKOD.Value;
  csnev:=TDCsopDC_CSNEV.Value;

  T_Tavollet.Close;
  T_Tavollet.Parameters[0].Value:=TDolgozoDO_KOD.Value;
  T_Tavollet.Parameters[1].Value:=AKTEV;
  T_Tavollet.Parameters[2].Value:=AKTHO;
  T_Tavollet.Open;
  T_Tavollet.First;
  if T_Tavollet.Eof then
    begin      // nincs m�g rekord a TTAVOLLETben
      TTavollet.Append;
      TTavolletTA_DOKOD.Value:=TDolgozoDO_KOD.Value;
      TTavolletTA_DONEV.value:=TDolgozoDO_NAME.Value;
      TTavolletTA_CSKOD.Value:=cskod;
      TTavolletTA_CSNEV.Value:=csnev;
      TTavolletTA_HONAP.Value:=AKTHO;
      TTavolletTA_EV.Value   :=AKTEV;
      TTavollet.FieldByName('TA_'+DBGrid1.SelectedField.FieldName).value:=jel;
    end
    else
    begin
      TTavollet.Locate('TA_DOKOD',TDolgozoDO_KOD.Value,[]);
      TTavolletTA_DONEV.Value;
      TTavollet.edit;
      TTavolletTA_CSKOD.Value:=cskod;
      TTavolletTA_CSNEV.Value:=csnev;
      if TTavollet.FieldByName('TA_'+DBGrid1.SelectedField.FieldName).value<>jel then
        TTavollet.FieldByName('TA_'+DBGrid1.SelectedField.FieldName).value:=jel
      else
        TTavollet.FieldByName('TA_'+DBGrid1.SelectedField.FieldName).value:='';
    end;
    TTavollet.Post;

    //if not SZABI.Active then SZABI.Open;
    //SZABI.Locate('SB_DOKOD',TTavolletTA_DOKOD.Value,[loCaseInsensitive]);
    SZABI.Edit;
    SZABISB_KSZAB.Value:=KivettSzabi(TDolgozoDO_KOD.Value,AKTEV) ;
  {  if SZABISB_KSZAB.Value- e_SZ+o_SZ<0 then
      SZABISB_KSZAB.Value:=0
    else
      SZABISB_KSZAB.Value:=SZABISB_KSZAB.Value- e_SZ+o_SZ;      }
    SZABI.Post;
    SZABI.Close;
    SZABI.Open;

    TDolgozo.GotoBookmark(rec);
  //ShowMessage(IntToStr (KivettSzabi(TDolgozoDO_KOD.Value,AKTEV) ));
end;

procedure TFTavollet.DBGrid1DblClick(Sender: TObject);
begin
  Beir(TJEL);
//  ShowMessage(IntToStr (KivettSzabi(TDolgozoDO_KOD.Value,AKTEV) ));
end;

procedure TFTavollet.DBGrid1DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
  if (Odd(TDolgozo.RecNo)) then
   dbGrid1.Canvas.Brush.Color :=$0080FFFF;

  Try
    if Column.Title.Font.Color=clRed then
      DBGrid1.Canvas.Brush.Color:=$00FFF4BB;             //clAqua;
  Except
  End;
  Try
   if (datacol>0) and (EncodeDate(StrToInt(AKTEV),StrToInt(AKTHO),strtoint(DBGrid1.Columns[datacol].Title.Caption))=date) then     // ma
    DBGrid1.Canvas.Brush.Color:=clLime;
  Except
  End;
  if TDolgozoDO_ARHIV.Value<>0 then
    DBGrid1.Canvas.Font.Color := clRed;

  if gdFocused in state	then
  begin
    DBGrid1.Canvas.Brush.Color:=clHighlight;
    DBGrid1.Canvas.Font.Color := clWhite;
  end;

  DBGrid1.DefaultDrawColumnCell(Rect,DataCol,Column , State);

	if oldrecord = TDolgozoDO_KOD.AsString then begin
		// Egy fekete keretet rajzol az aktu�lis sor k�r�!!!
    DBGrid1.Canvas.Pen.Color	:= clBlack;
		DBGrid1.Canvas.Pen.Width	:= 3;
		DBGrid1.Canvas.MoveTo(0, Rect.Top-1);
		DBGrid1.Canvas.LineTo(DBGrid1.Width, Rect.Top-1);
		DBGrid1.Canvas.LineTo(DBGrid1.Width, Rect.Bottom-1);
		DBGrid1.Canvas.LineTo(0, Rect.Bottom-1);
	//	DBGrid1.Canvas.LineTo(0, Rect.Top-0);

    DBGrid2.Canvas.Pen.Color	:= clBlack;
		DBGrid2.Canvas.Pen.Width	:= 3;
		DBGrid2.Canvas.MoveTo(0, Rect.Top-1);
		DBGrid2.Canvas.LineTo(DBGrid2.Width, Rect.Top-1);
		DBGrid2.Canvas.LineTo(DBGrid2.Width, Rect.Bottom-1);
		DBGrid2.Canvas.LineTo(0, Rect.Bottom-1);
	end;

end;

procedure TFTavollet.RadioButton1Click(Sender: TObject);
begin
  if RadioButton1.Checked then TJEL:='�';
  Label1.Caption:=TJEL;
  DBGrid1.SetFocus;
end;

procedure TFTavollet.RadioButton2Click(Sender: TObject);
begin
  if RadioButton2.Checked then TJEL:='SZ';
  Label1.Caption:=TJEL;
  DBGrid1.SetFocus;
end;

procedure TFTavollet.RadioButton3Click(Sender: TObject);
begin
  if RadioButton3.Checked then TJEL:='B';
  Label1.Caption:=TJEL;
  DBGrid1.SetFocus;
end;

procedure TFTavollet.RadioButton4Click(Sender: TObject);
begin
  if RadioButton4.Checked then TJEL:='EI';
  Label1.Caption:=TJEL;
  DBGrid1.SetFocus;
end;

procedure TFTavollet.RadioButton5Click(Sender: TObject);
begin
  if RadioButton5.Checked then TJEL:='EL';
  Label1.Caption:=TJEL;
  DBGrid1.SetFocus;
end;

procedure TFTavollet.RadioButton6Click(Sender: TObject);
begin
  if RadioButton6.Checked then TJEL:='';
  Label1.Caption:=TJEL;
  DBGrid1.SetFocus;
end;

procedure TFTavollet.keresoEnter(Sender: TObject);
begin
  DBGrid1.SetFocus;
end;

procedure TFTavollet.TDolgozoAfterScroll(DataSet: TDataSet);
var
  kszaz:double;
begin
//  Caption:=EgyebDlg.v_v_evszam+'  '+TabControl1.Tabs[TabControl1.tabindex]+'     '+TDolgozoDO_NAME.Value;
  if DolgozoMutat then begin
    DolgozoFissit;
    end;  // if
end;

procedure TFTavollet.DolgozoFissit;
var
  kszaz:double;
begin
//  Caption:=EgyebDlg.v_v_evszam+'  '+TabControl1.Tabs[TabControl1.tabindex]+'     '+TDolgozoDO_NAME.Value;
    GroupBox1.Caption:=TDolgozoDO_NAME.Value;
    Edit7.Text :=IntToStr(TDolgozoOSZAB2.Value);
    Edit8.Text :=IntToStr(TDolgozoASZAB2.Value);
    Edit9.Text :=IntToStr(TDolgozoSZABI2.Value);
    Edit10.Text:=IntToStr(TDolgozoESZAB2.Value);
    Edit11.Text:=IntToStr(TDolgozoKSZAB2.Value);
    Edit12.Text:=IntToStr(TDolgozoMSZAB2.Value);
    kszaz:=0;
    Button1.OnClick(self);
end;


procedure TFTavollet.Timer1Timer(Sender: TObject);
begin
  DBGrid1.ShowHint:=False;
  DBGrid1.Hint:=GridHint;
  Timer1.Enabled:=False;

end;

function TFTavollet.DateToDName(datum: TDateTime): string;
var
  days: array[1..7] of string;
begin
  days[1]  := 'Vas�rnap';
  days[2]  := 'H�tf�';
  days[3]  := 'Kedd';
  days[4]  := 'Szerda';
  days[5]  := 'Cs�t�rt�k';
  days[6]  := 'P�ntek';
  days[7]  := 'Szombat';
  Result   := days[DayOfWeek(datum)];
end;

procedure TFTavollet.DBGrid1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key=VK_SPACE then
    Beir(TJEL);
end;

procedure TFTavollet.DBGrid1ColEnter(Sender: TObject);
var
  datum:TDateTime;
begin
  if DBGrid1.SelectedIndex <>0 then
  begin
    datum:=EncodeDate(StrToInt(AKTEV), StrToInt(AKTHO),strtoint(DBGrid1.Columns[DBGrid1.SelectedIndex].Title.Caption));
    GridHint:=DBGrid1.Hint;
    DBGrid1.Hint:=   AKTEV+'.'+AKTHO+'.'+DBGrid1.Columns[DBGrid1.SelectedIndex].Title.Caption+'. '+DateToDName(datum) ;
    Label2.Caption:= AKTEV+'.'+AKTHO+'.'+DBGrid1.Columns[DBGrid1.SelectedIndex].Title.Caption+'. '+DateToDName(datum) ;
    DBGrid1.ShowHint:=True;
    Timer1.Enabled:=true;
  end;

end;

procedure TFTavollet.DBGrid1KeyPress(Sender: TObject; var Key: Char);
var
  C:string;
begin
        C:=Key;
        if (key<>#8) and (key<>#27)and (key<>#13) and (key<>#9) and (key<>#32)then
        begin
         szov_hossz:=Length(szov);
         szov:=szov+C;
         if Timer2.Interval<>1 then
         begin
           Timer2.Enabled:=False;
           Timer2.Enabled:=True;
         end
         else
         begin
           Rend_szures;
         end;
         kereso.Text:=szov;
        end;
end;

procedure TFTavollet.Timer2Timer(Sender: TObject);
begin
  Timer2.Enabled:=False;
  Rend_szures;

end;

procedure TFTavollet.Rend_szures;
begin
  TDolgozo.Locate('DO_NAME',szov,[loPartialKey]);
  szov:='';
  kereso.Text:=szov;
end;

procedure TFTavollet.FormActivate(Sender: TObject);
begin
  DBGrid1.SetFocus;
end;

procedure TFTavollet.DSDolgozoDataChange(Sender: TObject; Field: TField);
begin
	// Ez a procedura h�v�dik meg, ha v�ltozik a rekord
	oldrecord			:= TDolgozoDO_KOD.AsString;
	DBGrid1.Refresh;
  DBGrid2.Refresh;
end;

procedure TFTavollet.BitKilepClick(Sender: TObject);
begin
  close;
end;

procedure TFTavollet.CheckBox1Click(Sender: TObject);
begin
  TDolgozo.Close;

  if CheckBox1.Checked then
    TDolgozo.CommandText:='select * from DOLGOZO where DO_KULSO=0 and DO_ARHIV=0 order by DO_NAME'
  else
    TDolgozo.CommandText:='select * from DOLGOZO where DO_KULSO=0 order by DO_NAME';

  TDolgozo.Open;
  DBGrid1.SetFocus;
end;

procedure TFTavollet.TTavolletBeforePost(DataSet: TDataSet);
var
  o_X,o_SZ,o_B,o_EI,o_EL,i, e_SZ:integer;
  nap:string;
begin
  TTavolletTA_BERKOD.Value:=TDolgozoDO_BERKOD.Value;

  o_X:=0;
  o_SZ:=0;
  o_B:=0;
  o_EI:=0;
  o_EL:=0;
  e_SZ:=TTavolletTA_O_SZ.Value;
  for i:=1 to 31 do
  begin
    nap:='';
    if i<10 then nap:='0';
    nap:=nap+IntToStr(i);
    if TTavollet.FieldByName('TA_NAP'+nap).AsString='X' then inc(o_X);
    if TTavollet.FieldByName('TA_NAP'+nap).AsString='SZ' then inc(o_SZ);
    if TTavollet.FieldByName('TA_NAP'+nap).AsString='B' then inc(o_B);
    if TTavollet.FieldByName('TA_NAP'+nap).AsString='EI' then inc(o_EI);
    if TTavollet.FieldByName('TA_NAP'+nap).AsString='EL' then inc(o_EL);
  end;
  TTavolletTA_O_X.Value:=o_X;
  TTavolletTA_O_SZ.Value:=o_SZ;
  TTavolletTA_O_B.Value:=o_B;
  TTavolletTA_O_EI.Value:=o_EI;
  TTavolletTA_O_EL.Value:=o_EL;
  if e_SZ<>o_SZ then
  begin
    if not T_Dolgozo.Active then T_Dolgozo.Open;
    T_Dolgozo.Locate('DO_KOD',TTavolletTA_DOKOD.Value,[loCaseInsensitive]);
    T_Dolgozo.Edit;
    if T_DolgozoDO_KSZAB.Value- e_SZ+o_SZ<0 then
      T_DolgozoDO_KSZAB.Value:=0
    else
      T_DolgozoDO_KSZAB.Value:=T_DolgozoDO_KSZAB.Value- e_SZ+o_SZ;
    T_Dolgozo.Post;
  end;
end;

procedure TFTavollet.DBGrid2Enter(Sender: TObject);
begin
  DBGrid1.SetFocus;
end;

procedure TFTavollet.DBGrid2DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
var
  szabszaz:double;
begin
  ShowScrollBar(DBGrid2.Handle, SB_VERT, False);
  TMyGrid(DBGrid2).ScrollBars:=ssNone;

  if (Odd(TDolgozo.RecNo)) then
   dbGrid2.Canvas.Brush.Color :=$0080FFFF;

  if (DataCol<3) then
    DBGrid2.Canvas.Brush.Color:=$00FFF4BB;

  if TDolgozo.FieldByName(Column.FieldName).AsInteger=0 then
    DBGrid2.Canvas.Font.Color:=DBGrid2.Canvas.Brush.Color;

  szabszaz:=100;
  if TDolgozoSZABIARAMY.Value<>0 then
    szabszaz:=TDolgozoKSZAB.Value / TDolgozoSZABIARAMY.Value * 100;
//  if (DataCol=1)or(DataCol=2) and ((szabszaz<75) or (szabszaz>125)) then
  if (DataCol=2) and (szabszaz < 100-RSZAZ)  then
  begin
    DBGrid2.Canvas.Font.Color:= clRed;
  end;
  if (DataCol=2) and (szabszaz > 100+RSZAZ) then
  begin
    DBGrid2.Canvas.Font.Color:= clGreen;
  end;

  DBGrid2.DefaultDrawColumnCell(Rect,DataCol,Column , State);

	if oldrecord = TDolgozoDO_KOD.AsString then begin
    DBGrid2.Canvas.Pen.Color	:= clBlack;
		DBGrid2.Canvas.Pen.Width	:= 3;
		DBGrid2.Canvas.MoveTo(0, Rect.Top-1);
		DBGrid2.Canvas.LineTo(DBGrid2.Width, Rect.Top-1);
		DBGrid2.Canvas.LineTo(DBGrid2.Width, Rect.Bottom-1);
		DBGrid2.Canvas.LineTo(0, Rect.Bottom-1);
	end;
end;

procedure TFTavollet.TDolgozoAfterOpen(DataSet: TDataSet);
begin
  Label3.Caption:=IntToStr(TDolgozo.RecordCount);
end;

procedure TFTavollet.DBEdit1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (key=VK_RETURN)and(TDolgozo.State=dsEdit) then
  begin
     TDolgozo.Post;
     DBGrid1.SetFocus;
  end;
end;

procedure TFTavollet.TDolgozoCalcFields(DataSet: TDataSet);
begin
  TDolgozoSZABIARAMY.Value:=Trunc(RoundTo( DayOfTheYear(date) / 365 * TDolgozoOSZAB2.Value,0));
end;

procedure TFTavollet.DBEdit1Exit(Sender: TObject);
begin
  if TDolgozo.Modified then TDolgozo.Post;
end;

function TFTavollet.WriteLogFile(FName, Text: string): Boolean;
var
  LogFile: TextFile;
begin
  AssignFile (LogFile, Fname);
  if FileExists (FName) then
  begin
    Append (LogFile); // open existing file
  end
  else
  begin
    Rewrite (LogFile); // create a new one
  end;
  // write to the file and show error
  Writeln (LogFile,Text);

  // close the file
  CloseFile (LogFile);
  Result:=True;
end;

procedure TFTavollet.BTBerelClick(Sender: TObject);
var
  fn,sor,dkod,nap:string;
  i:integer;
  SZkod,honap:string;
begin
  if MessageBox(0, 'K�ri a felad�si f�jl elk�sz�t�s�t?', 'Felad�s a B�rnek', MB_ICONQUESTION or MB_YESNO or MB_DEFBUTTON2)=IDNO then
    exit;

	SZkod:=EgyebDlg.Read_SZGrid( '999', '920' );
  if SZkod='' then
  begin
    ShowMessage('Nincs B�rfelad�si k�d!');
    exit;
  end;
  TabControl3.TabIndex:=0;
  TabControl3.OnChange(self);
  if TabControl3.Tabs[TabControl3.TabIndex]<>'*Minden csoport*' then
  begin

    exit;
  end;
  // fn:=ExtractFilePath(Application.ExeName)+'T'+AKTEV+AKTHO+'.txt';
  fn:=EgyebDlg.TempList+'T'+AKTEV+AKTHO+'.txt';
  if FileExists(fn) then
    DeleteFile(fn);

  TDolgozo.First;
  while not TDolgozo.Eof do
  begin
//    dkod:=TDolgozoDO_BERKOD.Value+stringofchar(' ',10-length(TDolgozoDO_BERKOD.Value));
    TDolgozoDO_NAME.Value;
    dkod:=TDolgozoDO_ADOSZ.Value+stringofchar(' ',10-length(TDolgozoDO_ADOSZ.Value));
    if (Trim(dkod)='')and(Trim(dkod)<>'*') then
    begin
      ShowMessage('Nincs dolgoz�i k�d!  '+TDolgozoDO_NAME.Value);
      exit;
    end;
    if Trim(dkod)<>'*' then
    begin
     honap:=IntToStr(TabControl1.TabIndex+1);
     if TabControl1.TabIndex+1 <10 then
      honap:='0'+honap;
    if TTavollet.Locate('TA_DOKOD;TA_HONAP', VarArrayOf([TDolgozoDO_KOD.Value,honap]),[loCaseInsensitive]) then
    begin
     for i:=1 to 31 do
     begin
      nap:='';
      if i<10 then nap:='0';
      nap:=nap+IntToStr(i);
      if (TTavollet.FieldByName('TA_NAP'+nap).AsString='SZ') then
      begin
         sor:=dkod+SZkod+AKTEV+'.'+AKTHO+'.'+nap+'    ';
         WriteLogFile(fn,sor);
      end;
     end;
    end;
    end;
    TDolgozo.Next;
  end;
  ShowMessage('A f�jl elk�sz�lt.'+#13+fn);
end;

procedure TFTavollet.TDolgozoDO_ADOSZValidate(Sender: TField);
begin
{  if EgyebDlg.CheckAdoazonosito(TDolgozoDO_ADOSZ.Value)<>0 then
  begin
    ShowMessage('Rossz ad�azonos�t�!');
    DBEdit2.SetFocus;
  end; }
end;

procedure TFTavollet.TabControl3Change(Sender: TObject);
begin
  TDolgozo.Close;
//  if TabControl3.TabIndex=0 then
  if copy(TabControl3.Tabs[TabControl3.TabIndex],1,1)='*' then
  begin
    TDolgozo.CommandText:='select DO_KOD, DO_NAME, DO_KULSO, DO_ARHIV , DO_KSZAB, DO_SZABI, DO_BERKOD, DO_CSKOD, DO_CSNEV, DO_ADOSZ from DOLGOZO where DO_KULSO=0 and DO_ARHIV=0 order by DO_NAME';
  end
  else
  begin
   TDolgozo.CommandText:='select DO_KOD, DO_NAME, DO_KULSO, DO_ARHIV , DO_KSZAB, DO_SZABI, DO_BERKOD, DO_CSKOD, DO_CSNEV, DO_ADOSZ from DOLGOZO '+
    'where DO_KULSO=0 and DO_ARHIV=0 and DO_CSNEV= :CSOP  order by DO_NAME';
  end;
  TabControl1.OnChange(self);
end;

procedure TFTavollet.TabControl3DrawTab(Control: TCustomTabControl;
  TabIndex: Integer; const Rect: TRect; Active: Boolean);
var
  tabName: String;
  TmpRect: TRect;
begin
  inherited;
  with (Control as TTabControl).Canvas do begin
      tabName := String(TTabControl(Control).Tabs[TabIndex]);
      if Active then begin  // az akt�v f�let sz�nezz�k �t
        Font.Color := clRed;
        // brush.Color := clYellow;
        Fillrect(Rect);
        end;  // if
      TmpRect:=   Rect;
      OffsetRect(TmpRect,   4,   4);
      DrawText(Handle, PChar(tabName), -1, TmpRect, DT_TOP);
    end;  // with
end;

procedure TFTavollet.Button1Click(Sender: TObject);
var
  stream: TMemoryStream;
  jpeg: TJPEGImage;
begin
  TDFOTO.Close;
  TDFOTO.Parameters[0].Value:=TDolgozoDO_KOD.Value;
  TDFOTO.Open;
  TDFOTO.First;

   Image2.Picture.Graphic := nil;
   if not TDFOTO.Eof then begin
       try
           jpeg:=TJPEGImage.Create;
           stream:=TMemoryStream.Create;
           TBlobField(TDFOTODO_FOTO).SaveToStream(stream);
           stream.Position:=0;
           try
               jpeg.LoadFromStream(stream);
               jpeg.Scale:=jsFullSize;
               jpeg.Performance:=jpBestQuality;
               jpeg.ProgressiveDisplay:=true;
               Image2.Picture.Graphic:= nil;
               if not jpeg.Empty then begin
                   Image2.Picture.Graphic:=jpeg;
                   Image2.Update;
               end;
               stream.Free;
           finally
               jpeg.Free;
           end;
       except
           on EInvalidGraphic do begin
               Image2.Picture.Graphic := nil;
           end;
       end;
   end;
end;

procedure TFTavollet.Image2Click(Sender: TObject);
begin
  if TDFOTODO_FOTO.BlobSize>0 then begin
    kep2:=EgyebDlg.TempPath+'tmp_2.jpg';
    //kep2:=LMDSysInfo1.TempPath+'tmp_2.jpg';
    TDFOTODO_FOTO.SaveToFile(kep2);
    Application.ProcessMessages;
    ShellExecute(Handle, 'open', PChar(kep2), nil, PChar(ExtractFilePath(kep2)), SW_SHOWDEFAULT)
  end;
end;

procedure TFTavollet.TabControl1Changing(Sender: TObject;
  var AllowChange: Boolean);
begin
  dokod:=TDolgozoDO_KOD.Value;
  dorec:=TDolgozo.GetBookmark;
end;

procedure TFTavollet.TabControl1DrawTab(Control: TCustomTabControl;
  TabIndex: Integer; const Rect: TRect; Active: Boolean);
var
  tabName: String;
  TmpRect: TRect;
begin
  inherited;
  with (Control as TTabControl).Canvas do begin
      tabName := String(TTabControl(Control).Tabs[TabIndex]);
      // if tabName = 'Janu�r' then begin
      if Active then begin  // az akt�v f�let sz�nezz�k �t
        Font.Color := clRed;
        // brush.Color := clYellow;
        Fillrect(Rect);
        end;  // if
      TmpRect:=   Rect;
      OffsetRect(TmpRect,   4,   4);
      DrawText(Handle, PChar(tabName), -1, TmpRect, DT_TOP);
    end;  // with
end;

procedure TFTavollet.UpDown11ChangingEx(Sender: TObject;
  var AllowChange: Boolean; NewValue: Smallint;
  Direction: TUpDownDirection);
begin
 if UpDown1.Position>2000 then
 begin
  if Direction=updUp then
    TabControl1.TabIndex:=0;
  if Direction=updDown then
    TabControl1.TabIndex:=11;

  dokod:=TDolgozoDO_KOD.Value;
  dorec:=TDolgozo.GetBookmark;
  AKTEV:=IntToStr(NewValue);
  if NewValue=YearOf(date) then
  begin
    TabControl1.TabIndex:= MonthOf(date)-1
  end;
  SZABI.Close;
  SZABI.Parameters[0].Value:=AKTEV;
  SZABI.Open;
  TabControl1.OnChange(self);
  //DBGrid2.Enabled:=AKTEV=MOSTANIEV;
  Try
      TDolgozo.GotoBookmark(dorec);
  Except
    TDolgozo.First;
  End;

 end;

end;

procedure TFTavollet.UpDown1ChangingEx(Sender: TObject;
  var AllowChange: Boolean; NewValue: Integer; Direction: TUpDownDirection);
begin
 if UpDown1.Position>2000 then
 begin
  if Direction=updUp then
    TabControl1.TabIndex:=0;
  if Direction=updDown then
    TabControl1.TabIndex:=11;

  dokod:=TDolgozoDO_KOD.Value;
  dorec:=TDolgozo.GetBookmark;
  AKTEV:=IntToStr(NewValue);
  if NewValue=YearOf(date) then
  begin
    TabControl1.TabIndex:= MonthOf(date)-1
  end;
  SZABI.Close;
  SZABI.Parameters[0].Value:=AKTEV;
  SZABI.Open;
  TabControl1.OnChange(self);
  //DBGrid2.Enabled:=AKTEV=MOSTANIEV;
  Try
      TDolgozo.GotoBookmark(dorec);
  Except
    TDolgozo.First;
  End;

 end;

end;

procedure TFTavollet.SZABIBeforeOpen(DataSet: TDataSet);
begin
  SZABI.Parameters[0].Value:=AKTEV;
end;

procedure TFTavollet.TTavolletAfterPost(DataSet: TDataSet);
begin
  TDolgozo.GotoBookmark(TDolgozo.GetBookmark);
  DBGrid2.Refresh;

end;

function TFTavollet.KivettSzabi(DOkod, Ev: string): integer;
var
  i: integer;
  nap: string;
begin
  Result:= -1;
  if (DOkod='') or (Ev='') then exit;

  kiszabi.Close;
  kiszabi.Parameters[0].Value:=DOkod;
  kiszabi.Parameters[1].Value:=Ev;
  kiszabi.Open;
  kiszabi.First;
  Result:=0;
  while not kiszabi.Eof do
  begin
    for i:=1 to 31 do
    begin
      nap:=IntToStr(i);
      if i<10 then
        nap:='0'+nap;
      if kiszabi.fieldbyname('TA_NAP'+nap).Value='SZ' then
        inc(Result);
        
    end;
    kiszabi.Next;
  end;
end;

procedure TFTavollet.Button2Click(Sender: TObject);
var
  szabi,satho,eszab: integer;
  S: string;
begin
  if not EgyebDlg.user_super then
  begin
    MessageBox(0, 'Csak Rendszergazdai joggal lehet menteni!', '', MB_ICONWARNING or MB_OK);
    exit;
  end;
  if (MessageBox(0, 'K�ri az adatok ment�s�t?', '', MB_ICONQUESTION or MB_YESNO or MB_DEFBUTTON2) in [idNo]) then
    exit;

  szabi:=StrToIntDef(Edit9.Text ,0);
  satho:=StrToIntDef(Edit8.Text ,0);
  eszab:=StrToIntDef(Edit10.Text,0);
  // ADOCommand1.CommandText:='update szabadsag set sb_szabi='+IntToStr(szabi)+',sb_satho='+IntToStr(satho)+',sb_eszab='+IntToStr(eszab)+
  //  ' where sb_ev='''+Edit_EV.Text+''' and sb_dokod='''+TDolgozoDO_KOD.Value+'''';
  // ADOCommand1.Execute;

  S:='update szabadsag set sb_szabi='+IntToStr(szabi)+',sb_satho='+IntToStr(satho)+',sb_eszab='+IntToStr(eszab)+
        ' where sb_ev='''+Edit_EV.Text+''' and sb_dokod='''+TDolgozoDO_KOD.Value+'''';
  Command_Run(ADOCommand1, S, true);

  dorec:=TDolgozo.GetBookmark;
  TabControl1.OnChange(self);
  TDolgozo.GotoBookmark(dorec);

end;

end.
