unit Szamsorbe;

interface

uses
	SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
	Forms, Dialogs, StdCtrls, Buttons, Mask, DB, DBTables, ADODB;
	
type
	TSzamSorbeDlg = class(TForm)
	BitElkuld: TBitBtn;
	BitKilep: TBitBtn;
	Label1: TLabel;
	Label2: TLabel;
	Label3: TLabel;
	Label4: TLabel;
	Label6: TLabel;
	Label7: TLabel;
	MaskEdit1: TMaskEdit;
	MaskEdit2: TMaskEdit;
	MaskEdit3: TMaskEdit;
	MaskEdit4: TMaskEdit;
	MaskEdit6: TMaskEdit;
	Label9: TLabel;
	ButValVevo: TBitBtn;
  MaskEdit8: TMaskEdit;
  AFACombo: TComboBox;
    Label8: TLabel;
    Label10: TLabel;
    MaskEdit9: TMaskEdit;
    Label11: TLabel;
    MaskEdit10: TMaskEdit;
    MaskEdit11: TMaskEdit;
    MaskEdit12: TMaskEdit;
	 Label5: TLabel;
    MaskEdit7: TMaskEdit;
    Label13: TLabel;
    MaskEdit13: TMaskEdit;
    Label14: TLabel;
    Label12: TLabel;
    Label15: TLabel;
    MaskEdit14: TMaskEdit;
    CVnem: TComboBox;
    Label16: TLabel;
    Query1: TADOQuery;
    CheckBox1: TCheckBox;
	procedure BitKilepClick(Sender: TObject);
	procedure BitElkuldClick(Sender: TObject);
  procedure MaskEditClick(Sender: TObject);
  procedure MaskEditExit(Sender: TObject);
  procedure MaskEditKeyPress(Sender: TObject; var Key: Char);
  procedure MaskEdit7KeyPress(Sender: TObject; var Key: Char);
  procedure Tolt(stri : TStringList);
  procedure ButValVevoClick(Sender: TObject);
  procedure FormCreate(Sender: TObject);
  procedure FormDestroy(Sender: TObject);
  procedure Modosit(Sender: TObject);
  procedure MaskEdit2Change(Sender: TObject);
	procedure ValutaSzam;
    procedure MaskEdit6Exit(Sender: TObject);
    procedure MaskEdit8KeyPress(Sender: TObject; var Key: Char);
    procedure MaskEdit9Exit(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit13Exit(Sender: TObject);
    procedure CVnemChange(Sender: TObject);
    procedure MaskEdit2Exit(Sender: TObject);
    procedure Termekolvaso(tekod : string);
	private
		afalist 		: TStringList;
		modosult		: boolean;
		termekkod 		: string;
		vnemlist		: TStringList;
	public
		locstri 		: TStringList;
		datum			: string;
		vevokod			: string;
end;

var
	SzamSorbeDlg: TSzamSorbeDlg;

implementation

uses
	Egyeb , TermekFm, J_SQL, Kozos;
{$R *.DFM}

procedure TSzamSorbeDlg.FormCreate(Sender: TObject);
var
  sorsz	: integer;
begin
	EgyebDlg.SetADOQueryDatabase(Query1);
	SetMaskEdits([MaskEdit1]);
	afalist 	:= TStringList.Create;
	SzotarTolt(AFACombo, '101' , afalist, false, false );
	modosult 	:= false;
	{A valutanem t�bla felt�lt�se a k�dokkal}
	vnemlist 	:= TStringList.Create;
	SzotarTolt(CVnem, '110' , vnemlist, false, false );
	CVnem.Items.Clear;
	for sorsz := 0 to vnemlist.Count - 1 do begin
		CVnem.Items.Add(vnemlist[sorsz]);
	end;
	CVnem.ItemIndex	:= CVNem.Items.IndexOf('EUR');
	vevokod		:= '';
end;

procedure TSzamSorbeDlg.BitKilepClick(Sender: TObject);
begin
	if modosult then begin
		if NoticeKi('M�dosultak az adatok . Ki akar l�pni ment�s n�lk�l?', NOT_QUESTION) = 0 then begin
			locstri.Clear;
			Close;
		end;
	end else begin
		locstri.Clear;
		Close;
	end;
end;

procedure TSzamSorbeDlg.BitElkuldClick(Sender: TObject);
begin
{
	if MaskEdit3.Text = '' then begin
	NoticeKi('A megnevez�s m�g nincs kit�ltve!');
     MaskEdit3.SetFocus;
  end;
}
{	ValutaSzam;}
	locstri.Clear;
	locstri.Add(MaskEdit1.Text);
	locstri.Add(MaskEdit2.Text);
	locstri.Add(MaskEdit3.Text);
	locstri.Add(MaskEdit4.Text);
	locstri.Add(CVnem.Text);
	locstri.Add(MaskEdit6.Text);
	locstri.Add(AFACombo.Text);
	locstri.Add(MaskEdit8.Text);
	locstri.Add(MaskEdit7.Text);
	locstri.Add(MaskEdit9.Text);
	locstri.Add(termekkod);
	locstri.Add(MaskEdit10.Text);
	locstri.Add(afalist[AfaCombo.ItemIndex]);
	locstri.Add(Maskedit11.Text);
	locstri.Add(Maskedit12.Text);
	locstri.Add(Maskedit13.Text);
	locstri.Add(Maskedit7.Text);
	locstri.Add(Maskedit9.Text); {Egys�g�r}
	locstri.Add(Maskedit14.Text); {K�lf�ldi mennyis�gi egys�g}
	locstri.Add(datum); {�rfolyam d�tuma}
	locstri.Add(Maskedit8.Text); {�rfolyam}
	if ( CheckBox1.Enabled and CheckBox1.Checked ) then begin
		locstri.Add('1'); {NEM EU-s flag}
	end else begin
		locstri.Add('0'); {NEM EU-s flag}
	end;
	{Valutanem �s �rfolyam t�rol�sa}
	EgyebDlg.Valutanem 		:= CVnem.Text;
	EgyebDlg.Arfolyam		:= StringSzam(MaskEdit8.Text);

  modosult:=True;
	Close;
end;

procedure TSzamSorbeDlg.MaskEditClick(Sender: TObject);
begin
	with Sender as TMaskEdit do begin
  		SelectAll;
  	end;
end;

procedure TSzamSorbeDlg.MaskEditExit(Sender: TObject);
begin
 	MaskEdit8.Text := StrSzamStr( MaskEdit8.Text, 12, 4);
	ValutaSzam;
end;

procedure TSzamSorbeDlg.MaskEditKeyPress(Sender: TObject; var Key: Char);
begin
  	with Sender as TMaskEdit do begin
  		Key := EgyebDlg.Jochar(Text,Key,12,2);
  	end;
end;

procedure TSzamSorbeDlg.MaskEdit7KeyPress(Sender: TObject; var Key: Char);
begin
  with Sender as TMaskEdit do begin
  	Key := EgyebDlg.Jochar(Text,Key,6,2);
  end;
end;

procedure TSzamSorbeDlg.Tolt(stri : TStringList);
var
	afasor		: integer;
	orszag		: string;
begin
	locstri := stri;
	Maskedit1.Text 		:= stri[0];
	CVnem.Text			:= EgyebDlg.Valutanem;
	label16.Caption		:= '( '+datum+' )';
	label16.Update;
	MaskEdit8.Text		:= SzamString(EgyebDlg.Arfolyam,12,4);
	MaskEdit13.Text 	:= '1';
	MaskEdit7.Text 		:= 'db';
	Maskedit2.Text 		:= stri[1];
	Maskedit3.Text 		:= stri[2];
	Maskedit4.Text := stri[3];
	CVnem.Text		:= stri[4];
	if stri[4] = '' then begin
		CVnem.Text	:= EgyebDlg.Valutanem;
	end;
	if CVnem.Text = '' then begin
		CVnem.Text	:= 'EUR';
	end;
	Maskedit6.Text := stri[5];
	afasor := afalist.IndexOf(stri[12]);
	if afasor < 0 then begin
			afasor := 0;
	end;
	AfaCombo.ItemIndex  := afasor;
	Maskedit8.Text := stri[7];
	if stri[7] = '' then begin
		CVnemChange(self);
		{MaskEdit8.Text		:= EgyebDlg.SzamString(EgyebDlg.Arfolyam,12,4);}
	end;
	Maskedit7.Text 	:= stri[8];
	Maskedit9.Text 	:= stri[17];	{Egys�g�r}
	termekkod 		:= stri[10];
	Maskedit10.Text := stri[11];
	Maskedit11.Text := stri[13];	{Idegen megnevez�s}
	Maskedit12.Text := stri[14];	{Idegen le�r�s}
	Maskedit13.Text := stri[15];	{Mennyis�g}
	if stri[15] = '' then begin
		MaskEdit13.Text 	:= '1';
	end;
	Maskedit7.Text := stri[16];	{Mennyis�gi egys�g}
	if stri[16] = '' then begin
		MaskEdit7.Text 	:= 'db';
	end;
	Maskedit14.Text := stri[18];	{K�lf�ldi mennyis�gi egys�g}
	modosult := false;
	// A NEM EU flag be�ll�t�sa
	CheckBox1.Enabled	:= false;
	orszag		:= Query_Select('VEVO', 'V_KOD', vevokod, 'V_ORSZ' );
   CheckBox1.Enabled	:= true;
   CheckBox1.Checked	:= StrToIntDef(stri[21], 0) > 0;
end;

procedure TSzamSorbeDlg.ButValVevoClick(Sender: TObject);
begin
	Application.CreateForm(TTermekFmDlg, TermekFmDlg);
  Application.ProcessMessages;
	TermekFmDlg.valaszt := true;
	TermekFmDlg.ShowModal;
	if TermekFmDlg.ret_vkod <> '' then begin
  	Termekolvaso(TermekFmDlg.ret_vkod);
	end;
	TermekFmDlg.Destroy;
	ValutaSzam;
  MaskEdit6.SelectAll;
end;

procedure TSzamSorbeDlg.FormDestroy(Sender: TObject);
begin
  afalist.Free;
	vnemlist.Free;
end;

procedure TSzamSorbeDlg.Modosit(Sender: TObject);
begin
	modosult := true;
end;

procedure TSzamSorbeDlg.MaskEdit2Change(Sender: TObject);
begin
	modosult := true;
	termekkod := '';
end;

procedure TSzamSorbeDlg.MaskEdit6Exit(Sender: TObject);
var
	valert	: double;
  valarf	: double;
begin
	MaskEdit6.Text := StrSzamStr( MaskEdit6.Text, 12, 3);
  valert := StringSzam(MaskEdit6.Text);
	valarf := StringSzam(MaskEdit8.Text);
  if valarf <> 0 then begin
		MaskEdit9.Text := SzamString( valert * valarf, 12, 3);
  end else begin
		MaskEdit9.Text := SzamString( 0, 12, 3);
  end;
end;

procedure TSzamSorbeDlg.MaskEdit8KeyPress(Sender: TObject; var Key: Char);
begin
 	Key := EgyebDlg.Jochar(MaskEdit8.Text,Key,12,4);
end;

procedure TSzamSorbeDlg.MaskEdit9Exit(Sender: TObject);
begin
 	MaskEdit9.Text := StrSzamStr( MaskEdit9.Text, 12, 2);
	ValutaSzam;
end;

procedure TSzamSorbeDlg.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
	EgyebDlg.FormReturn(Key);
end;

procedure TSzamSorbeDlg.ValutaSzam;
var
	  valert	: double;
  	valarf	: double;
begin
  	valert := StringSzam(MaskEdit9.Text) {* StringSzam(MaskEdit13.Text)};
	  valarf := StringSzam(MaskEdit8.Text);
  	if valarf <> 0 then begin
		MaskEdit6.Text := SzamString( valert / valarf, 12, 2);
  	end else begin
		MaskEdit6.Text := SzamString( 0, 12, 2);
  	end;
end;

procedure TSzamSorbeDlg.MaskEdit13Exit(Sender: TObject);
begin
	ValutaSzam;
end;

procedure TSzamSorbeDlg.CVnemChange(Sender: TObject);
begin
	modosult := true;
  MaskEdit8.Text	:= Format('%10.4f', [EgyebDlg.ArfolyamErtek( CVnem.Text, datum, true)] ) ;
end;

procedure TSzamSorbeDlg.MaskEdit2Exit(Sender: TObject);
begin
	// A k�d alapj�n beolvassuk a megfelel� �rucikket
  if MaskEdit2.Text <> '' then begin
 			Termekolvaso(MaskEdit2.Text);
  end;
end;

procedure TSzamSorbeDlg.Termekolvaso(tekod : string);
var
	afasor	: integer;
begin
   if Query_Run(Query1, 'SELECT * FROM TERMEK WHERE T_TERKOD = '''+tekod+''' ') then begin
      MaskEdit2.Text 	:= Query1.FieldByName('T_TERKOD').AsString;
      MaskEdit3.Text 	:= Query1.FieldByName('T_TERNEV').AsString;
      MaskEdit4.Text 	:= Query1.FieldByName('T_ITJSZJ').AsString;
      MaskEdit10.Text 	:= Query1.FieldByName('T_LEIRAS').AsString;
      MaskEdit11.Text 	:= Query1.FieldByName('T_MEGJ').AsString;
      afasor := afalist.IndexOf(Query1.FieldByName('T_AFA').AsString);
      if afasor < 0 then begin
         afasor := 0;
      end;
      AfaCombo.ItemIndex  := afasor;
      MaskEdit9.Text 	:= SzamString(Query1.FieldByName('T_EGYAR').AsFloat,12,0);
      CVnem.Text			:= EgyebDlg.Valutanem;
      MaskEdit6.SetFocus;
      MaskEdit6.SelectAll;
      termekkod 	:= tekod;
   end else begin
	   NoticeKi('Hib�s term�kk�d!');
      MaskEdit2.Text 	:= '';
      MaskEdit3.Text 	:= '';
      MaskEdit4.Text 	:= '';
      CVnem.Text		:= '';
      MaskEdit6.Text 	:= '';
      MaskEdit8.Text 	:= '';
      MaskEdit10.Text 	:= '';
      MaskEdit11.Text 	:= '';
      MaskEdit12.Text 	:= '';
      MaskEdit13.Text 	:= '';
      MaskEdit7.Text 	:= '';
   end;
end;

end.
