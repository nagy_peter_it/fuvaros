unit Palettabe;

interface

uses
	SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
	Forms, Dialogs, StdCtrls, Buttons, Mask, DB, DBTables, J_ALFORM, ADODB,
  ExtCtrls;

type
	TPalettabeDlg = class(TJ_AlformDlg)
    Panel1: TPanel;
    BitElkuld: TBitBtn;
    BitKilep: TBitBtn;
    Panel2: TPanel;
    Label2: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label12: TLabel;
    Label1: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label10: TLabel;
    Label13: TLabel;
    Label11: TLabel;
    Label14: TLabel;
    M3: TMaskEdit;
    M2: TMaskEdit;
    M1: TMaskEdit;
    CB1: TComboBox;
    BitBtn7: TBitBtn;
    M5: TMaskEdit;
    CB3: TComboBox;
    M4: TMaskEdit;
    BitBtn1: TBitBtn;
    E6: TEdit;
    BitBtn2: TBitBtn;
    M6: TMaskEdit;
    M7: TMaskEdit;
    BitBtn3: TBitBtn;
    BitBtn4: TBitBtn;
    Edit3: TEdit;
    BitBtn5: TBitBtn;
    E7: TMaskEdit;
    BitBtn6: TBitBtn;
    BitBtn8: TBitBtn;
    BitBtn9: TBitBtn;
    BitBtn10: TBitBtn;
    CB5: TComboBox;
    CB4: TComboBox;
    Query1: TADOQuery;
    TSZOTAR: TADOQuery;
    TSZOTARSZ_FOKOD: TStringField;
    TSZOTARSZ_ALKOD: TStringField;
    TSZOTARSZ_MENEV: TStringField;
    TSZOTARSZ_LEIRO: TStringField;
    TSZOTARSZ_KODHO: TBCDField;
    TSZOTARSZ_NEVHO: TBCDField;
    TSZOTARSZ_ERVNY: TIntegerField;
    SQL: TADOCommand;
    Query2: TADOQuery;
    Query2FF_FELNEV: TStringField;
    Query2FF_FELTEL: TStringField;
    QFel: TADOQuery;
    QFelms_felnev: TStringField;
    QLe: TADOQuery;
    QLems_lernev: TStringField;
    BitBtn11: TBitBtn;
    BitBtn12: TBitBtn;
    CheckBox1: TCheckBox;
    Panel3: TPanel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    Label22: TLabel;
    Label23: TLabel;
    Label24: TLabel;
    Label25: TLabel;
    Label26: TLabel;
    Label27: TLabel;
    Label28: TLabel;
    M3_: TMaskEdit;
    M2_: TMaskEdit;
    M1_: TMaskEdit;
    CB1_: TComboBox;
    BitBtn7_: TBitBtn;
    M5_: TMaskEdit;
    CB3_: TComboBox;
    M4_: TMaskEdit;
    BitBtn14: TBitBtn;
    E6_: TEdit;
    BitBtn15: TBitBtn;
    M6_: TMaskEdit;
    M7_: TMaskEdit;
    BitBtn16: TBitBtn;
    BitBtn17: TBitBtn;
    Edit2: TEdit;
    BitBtn18: TBitBtn;
    E7_: TMaskEdit;
    BitBtn19: TBitBtn;
    BitBtn20: TBitBtn;
    BitBtn21: TBitBtn;
    BitBtn22: TBitBtn;
    CB5_: TComboBox;
    CB4_: TComboBox;
    BitBtn23: TBitBtn;
    BitBtn24: TBitBtn;
    CheckBox1_: TCheckBox;
    CheckBox2: TCheckBox;
    QID: TADOQuery;
    QIDMAXID: TIntegerField;
    QFelms_feltel: TStringField;
    QFelms_felcim: TStringField;
    QLems_lertel: TStringField;
    QLems_lercim: TStringField;
    Label29: TLabel;
    M8: TMaskEdit;
    Label30: TLabel;
    Label31: TLabel;
    M8_: TMaskEdit;
    Label32: TLabel;
    BitBtn13: TBitBtn;
    BitBtn25: TBitBtn;
    BitBtn26: TBitBtn;
    BitBtn27: TBitBtn;
	procedure BitKilepClick(Sender: TObject);
	procedure FormCreate(Sender: TObject);
	procedure Tolto(cim : string; teko : string);override;
	procedure BitElkuldClick(Sender: TObject);
  procedure MaskEditClick(Sender: TObject);
  procedure MaskEditExit(Sender: TObject);
	procedure MaskEditKeyPress(Sender: TObject; var Key: Char);
    procedure Modosit(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure M1Exit(Sender: TObject);
    procedure BitBtn7Click(Sender: TObject);
    procedure CB1Change(Sender: TObject);
    procedure CB3Change(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure M2Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn4Click(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure BitBtn5Click(Sender: TObject);
    procedure BitBtn6Click(Sender: TObject);
    procedure BitBtn8Click(Sender: TObject);
    procedure BitBtn9Click(Sender: TObject);
    procedure M3Click(Sender: TObject);
    procedure M6Click(Sender: TObject);
    procedure M7Click(Sender: TObject);
    procedure BitBtn10Click(Sender: TObject);
    procedure BitBtn11Click(Sender: TObject);
    procedure BitBtn12Click(Sender: TObject);
    procedure BitBtn22Click(Sender: TObject);
    procedure BitBtn16Click(Sender: TObject);
    procedure BitBtn20Click(Sender: TObject);
    procedure BitBtn23Click(Sender: TObject);
    procedure BitBtn24Click(Sender: TObject);
    procedure BitBtn21Click(Sender: TObject);
    procedure BitBtn17Click(Sender: TObject);
    procedure BitBtn18Click(Sender: TObject);
    procedure BitBtn14Click(Sender: TObject);
    procedure BitBtn19Click(Sender: TObject);
    procedure BitBtn15Click(Sender: TObject);
    procedure CB3_Change(Sender: TObject);
    procedure M1_Exit(Sender: TObject);
    procedure BitBtn7_Click(Sender: TObject);
    procedure M6_Click(Sender: TObject);
    procedure M7_Click(Sender: TObject);
    procedure M2_Click(Sender: TObject);
    procedure M3_Click(Sender: TObject);
    procedure CheckBox2Click(Sender: TObject);
    procedure M4Change(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure CheckBox2Enter(Sender: TObject);
    procedure BitBtn13Click(Sender: TObject);
    procedure BitBtn26Click(Sender: TObject);
    procedure BitBtn25Click(Sender: TObject);
    procedure BitBtn27Click(Sender: TObject);
    procedure M1Enter(Sender: TObject);
    procedure M1_Enter(Sender: TObject);
	private
     modosult 	: boolean;
     ID1,ID2: integer;
    procedure keszletki;

	public
		afalist		: TStringList;
    ret_arfoly	: string;
    csakmegjegyzes: boolean;

	end;

var
	PalettabeDlg: TPalettabeDlg;
  _rkod, _rnev, _mkod, _mnev: TStringList;

implementation

uses
	Egyeb, J_SQL, Kozos, StrUtils,J_VALASZTO,VevoUjFm,Felrakofm,PalettaFm;
{$R *.DFM}


procedure TPalettabeDlg.BitKilepClick(Sender: TObject);
begin
	if modosult then begin
  		if NoticeKi('M�dosultak az adatok . Ki akar l�pni ment�s n�lk�l?', NOT_QUESTION ) = 0 then begin
			ret_kod := '';
			Close;
     	end;
  	end else begin
		ret_kod := '';
		Close;
  	end;
end;

procedure TPalettabeDlg.FormCreate(Sender: TObject);
begin
// 	EgyebDlg.SetADOQueryDatabase(Query1);
	ret_kod 	:= '';
  modosult 	:= false;
  M1.Text:=EgyebDlg.MaiDatum;
  //afalist 	:= TStringList.Create;
  ///////////////////////
  TSZOTAR.Close;
  TSZOTAR.Parameters[0].Value:='140';  //rakt�rak
  TSZOTAR.Open;
  TSZOTAR.last;
  TSZOTAR.First;
  CB1.Items.Clear;
  _rkod:=TStringList.Create;
  _rnev:=TStringList.Create;

  _rkod.Add('');
  _rnev.add('');
  CB1.Items.Add('') ;
  CB1_.Items.Add('') ;
  while not TSZOTAR.Eof do
  begin
    _rkod.Add(TSZOTARSZ_ALKOD.Value);
    _rnev.add(TSZOTARSZ_MENEV.Value);
    CB1.Items.Add(TSZOTARSZ_MENEV.Value) ;
    CB1_.Items.Add(TSZOTARSZ_MENEV.Value) ;
    TSZOTAR.Next;
  end;
{  if CB1.Items.Count=1 then
  begin
    CB1.ItemIndex:=0;
    CB1.OnChange(self);
  end;      }
  /////////////////
  TSZOTAR.Close;
  TSZOTAR.Parameters[0].Value:='141';  //mozg�snem
  TSZOTAR.Open;
  TSZOTAR.last;
  _mkod:=TStringList.Create;
  _mnev:=TStringList.Create;
  TSZOTAR.First;
  while not TSZOTAR.Eof do
  begin
    _mkod.Add(TSZOTARSZ_ALKOD.Value);
    _mnev.Add(TSZOTARSZ_MENEV.Value);
    CB3.Items.Add(TSZOTARSZ_MENEV.Value);
    CB3_.Items.Add(TSZOTARSZ_MENEV.Value);
    TSZOTAR.Next;
  end;
  //CB3.ItemIndex:=0;
  //CB3.OnChange(self);
  TSZOTAR.Close;
  ////////////////
  CB4.Items.Clear;
  CB5.Items.Clear;
  CB4_.Items.Clear;
  CB5_.Items.Clear;
  QFel.Open;
  QFel.First;
  while not QFel.Eof do
  begin
    CB4.Items.Add( QFelms_felnev.Value+' '+QFelms_feltel.Value+' '+QFelms_felcim.Value);  //+#254+Query2FF_FELTEL.Value);
    CB4_.Items.Add(QFelms_felnev.Value+' '+QFelms_feltel.Value+' '+QFelms_felcim.Value);  //+#254+Query2FF_FELTEL.Value);
    QFel.Next;
  end;
  QFel.Close;
  /////////////////////////
  QLe.Open;
  QLe.First;
  while not QLe.Eof do
  begin
    CB5.Items.Add( QLems_lernev.Value+' '+QLems_lertel.Value+' '+QLems_lercim.Value);  //+#254+Query2FF_FELTEL.Value);
    CB5_.Items.Add(QLems_lernev.Value+' '+QLems_lertel.Value+' '+QLems_lercim.Value);  //+#254+Query2FF_FELTEL.Value);
    QLe.Next;
  end;
  QLe.Close;
  ////////////////

  modosult:=False;
end;

procedure TPalettabeDlg.Tolto(cim : string; teko:string);
begin
	ret_kod 	:= teko;
	Caption := cim;
 //	M1.Text := EgyebDlg.MaiDatum;
	if ret_kod <> '' then begin		{M�dos�t�s}
		if Query_Run (Query1, 'SELECT * FROM PALETTA WHERE PA_ID = '+teko,true) then begin
			ID1 	:= Query1.FieldByName('PA_ID' ).AsInteger;
			ID2 	:= Query1.FieldByName('PA_ID2').AsInteger;

			if ID2 = 0 then         // NEM �tad�s - �tv�tel
      begin
        CheckBox2.Checked:=False;
        CheckBox2.OnClick(self);
      end;
			M1.Text 	:= Query1.FieldByName('PA_DATUM').AsString;
			M2.Text 	:= Query1.FieldByName('PA_RENSZ').AsString;
 			M3.Text 	:= Query1.FieldByName('PA_POTSZ').AsString;
			E6.Text 	:= Query1.FieldByName('PA_VEKOD').AsString;
			M7.Text 	:= Query1.FieldByName('PA_SNEV1').AsString;
			E7.Text 	:= Query1.FieldByName('PA_SKOD1').AsString;
      if Query1.FieldByName('PA_FELDB').Value>0 then
          M4.Text:=Query1.FieldByName('PA_FELDB').AsString
      else
          M4.Text:=Query1.FieldByName('PA_LERDB').AsString;

			M5.Text 	:= Query1.FieldByName('PA_MEGJ').AsString;
      ComboSetSzoveg(CB3,Query1.FieldByName('PA_MNEM').AsString);
      CB3Change(self);
      CheckBox1.Checked:= Query1.FieldByName('PA_OK').Value=1;
      if Length( Query1.FieldByName('PA_VEKOD').AsString)<4 then  // Rakt�r
        ComboSetSzoveg(CB1,Query1.FieldByName('PA_VENEV').AsString)
      else
  			M6.Text 	:= Query1.FieldByName('PA_VENEV').AsString;

      /////////////////////////////////////////////
			if ID2 > 0 then         // �tad�s - �tv�tel
      begin
        CheckBox2.Checked:=True;
        CheckBox2.OnClick(self);

    		if Query_Run (Query1, 'SELECT * FROM PALETTA WHERE PA_ID = '+IntToStr(ID2) ,true) then begin
    			M1_.Text 	:= Query1.FieldByName('PA_DATUM').AsString;
		    	M2_.Text 	:= Query1.FieldByName('PA_RENSZ').AsString;
     			M3_.Text 	:= Query1.FieldByName('PA_POTSZ').AsString;
		    	E6_.Text 	:= Query1.FieldByName('PA_VEKOD').AsString;
    			M7_.Text 	:= Query1.FieldByName('PA_SNEV1').AsString;
		    	E7_.Text 	:= Query1.FieldByName('PA_SKOD1').AsString;
          if Query1.FieldByName('PA_FELDB').Value>0 then
            M4_.Text:=Query1.FieldByName('PA_FELDB').AsString
          else
            M4_.Text:=Query1.FieldByName('PA_LERDB').AsString;

    			M5_.Text 	:= Query1.FieldByName('PA_MEGJ').AsString;
          ComboSetSzoveg(CB3_,Query1.FieldByName('PA_MNEM').AsString);
          CB3_Change(self);
          CheckBox1_.Checked:= Query1.FieldByName('PA_OK').Value=1;
          if Length( Query1.FieldByName('PA_VEKOD').AsString)<4 then  // Rakt�r
            ComboSetSzoveg(CB1_,Query1.FieldByName('PA_VENEV').AsString)
          else
  	    		M6_.Text 	:= Query1.FieldByName('PA_VENEV').AsString;

        end;
      end;
      {			Label10.Caption 	:= Query1.FieldByName('KM_KIBE').AsString;
			M6.Text 	:= SzamString(Query1.FieldByName('KM_MENNY').AsFloat,12,2);
			Label6.Caption 	:= Query1.FieldByName('KM_ME').AsString;
			Edit1.Text 	:= Query1.FieldByName('KM_RAKKOD').AsString;
			Edit2.Text 	:= Query1.FieldByName('KM_TKOD').AsString;
      ComboSetSzoveg(CB1,Query1.FieldByName('KM_RAKNEV').AsString);
      ComboSetSzoveg(CB2,Query1.FieldByName('KM_TNEV').AsString);
      ComboSetSzoveg(CB3,Query1.FieldByName('KM_MNEM').AsString);
      keszletki;    }
		end;
	end
  else
  begin
        CheckBox2.Checked:=False;
        CheckBox2.OnClick(self);
  end;
  //CB1.Enabled:=ret_kod='';
  //CB3.Enabled:=ret_kod='';
  modosult 	:= false;
end;

procedure TPalettabeDlg.BitElkuldClick(Sender: TObject);
var
  ujkodszam 	: integer;
  gkkat,gkeurob, elojel, vekod,venev,vekod_,venev_, dble,dbfel: string;
  S: string;
begin
	if not Jodatum2(M1 ) then begin
		NoticeKi('Hib�s d�tum megad�sa!');
		M1.Text := '';
		M1.Setfocus;
		Exit;
	end;
  if (M2.Text<>'') and   (M3.Text='') then
  begin
    gkkat:=Query_Select('GEPKOCSI','GK_RESZ',M2.Text,'GK_GKKAT');
    if copy(gkkat,1,2)='24' then       // val�sz�n�leg vontat�
    begin
  		NoticeKi('Meg kell adni p�rkocsit!');
	  	M3.Setfocus;
		  Exit;
    end;
  end;
	if CB3.Text='' then begin
		NoticeKi('A mozg�snemet meg kell adni!');
		CB3.Setfocus;
		Exit;
	end;
	if (CB1.Text<>'')and(M6.Text<>'') then begin
		NoticeKi('Rakt�rat �s Vev�t nem lehet egyszerre megadni!');
		M6.Setfocus;
		Exit;
	end;
	if M4.Text='' then begin
		NoticeKi('A mennyis�get meg kell adni!');
		M4.Setfocus;
		Exit;
	end;
  if (CB1.Text='') and (M6.Text='')  and (M7.Text='')  and (M2.Text='')  and (M3.Text='') and (CB4.Text='') and (CB5.Text='') then
  begin

    exit;
  end;
  //////////////
 if CheckBox2.Checked then
 begin
  if (M2_.Text<>'') and   (M3_.Text='') then
  begin
    gkkat:=Query_Select('GEPKOCSI','GK_RESZ',M2_.Text,'GK_GKKAT');
    if copy(gkkat,1,2)='24' then       // val�sz�n�leg vontat�
    begin
  		NoticeKi('Meg kell adni p�rkocsit!');
	  	M3_.Setfocus;
		  Exit;
    end;
  end;
	if CB3_.Text='' then begin
		NoticeKi('A mozg�snemet meg kell adni!');
		CB3_.Setfocus;
		Exit;
	end;
  if copy(Label10.Caption,1,2)=copy(Label25.Caption,1,2) then
  begin
		NoticeKi('A k�t mozg�snemnek ellenkez� ir�ny�nak kell lenni!');
		CB3_.Setfocus;
		Exit;
  end;
	if (CB1_.Text<>'')and(M6_.Text<>'') then begin
		NoticeKi('Rakt�rat �s Vev�t nem lehet egyszerre megadni!');
		M6_.Setfocus;
		Exit;
	end;
  if (CB1_.Text='') and (M6_.Text='')  and (M7_.Text='')  and (M2_.Text='')  and (M3_.Text='') and (CB4_.Text='') and (CB5_.Text='') then
  begin

    exit;
  end;
 end;
 //////////////////////////////////
{
  gkkat:='';
  gkeurob:='';
  if M2.Text<>'' then
  begin
    gkkat:=Query_Select('GEPKOCSI','GK_RESZ',M2.Text,'GK_GKKAT');
    gkeurob:=Query_Select('GEPKOCSI','GK_RESZ',M2.Text,'GK_EUROB');
  end;
  }
  elojel:='1*';
  if copy(Label10.Caption,1,2)='KI' then
  begin
    dble:=M4.Text;
    dbfel:='0';
  end
  else
  begin
    dbfel:=M4.Text;
    dble:='0';
  end;
   elojel:='-1*';
  if  M6.Text<>'' then
  begin
    venev:=M6.Text;
    vekod:=E6.Text;
  end;
  if  CB1.Text<>'' then
  begin
    venev:=CB1.Text;
    vekod:= _rkod[CB1.ItemIndex];
  end;

	if ret_kod = '' then begin
		//�j rekord felvitele
		// SQL.CommandText:='INSERT INTO PALETTA (PA_DATUM,PA_SKOD1,PA_SNEV1,PA_RENSZ,PA_POTSZ,PA_VEKOD,PA_VENEV,PA_FELDB,PA_LERDB,PA_MEGJ,PA_MNEM,PA_MTIP,PA_OK,PA_IGAZOLO) VALUES (' +
    S:='INSERT INTO PALETTA (PA_DATUM,PA_SKOD1,PA_SNEV1,PA_RENSZ,PA_POTSZ,PA_VEKOD,PA_VENEV,PA_FELDB,PA_LERDB,PA_MEGJ,PA_MNEM,PA_MTIP,PA_OK,PA_IGAZOLO) VALUES (' +
    ''''+M1.Text+''','''+E7.Text+''','''+M7.text+''','''+M2.Text+''','''+M3.Text+''','''+vekod+''','''+venev+''','+Trim(SqlSzamString(StringSzam(dbfel),3,0))+','+Trim(SqlSzamString(StringSzam(dble),3,0))+','+
    ''''+M5.Text+''','''+CB3.Text+''','''+'0'''+','''+IfThen(CheckBox1.Checked,'1','0')+''','''+EgyebDlg.user_name+'''' +' )';
    // SQL.Execute;
    Command_Run(SQL, S, true);

    QID.Close;
    QID.Open;
    ID1:=QIDMAXID.Value;
    QID.Close;
  end
  else
  begin
    // SQL.CommandText:='UPDATE PALETTA set '+
    S:='UPDATE PALETTA set '+
    'PA_DATUM='''+M1.Text+''','+
    'PA_SKOD1='''+E7.Text+''','+
    'PA_SNEV1='''+M7.Text+''','+
    'PA_RENSZ='''+M2.Text+''','+
    'PA_POTSZ='''+M3.Text+''','+
    'PA_VEKOD='''+vekod+''','+
    'PA_VENEV='''+venev+''','+
    'PA_FELDB='+SqlSzamString(StringSzam(dbfel), 3, 0)+','+
    'PA_LERDB='+SqlSzamString(StringSzam(dble), 3, 0)+','+
    'PA_MEGJ='''+M5.Text+''','+
    'PA_MNEM='''+CB3.Text+''','+
    'PA_MTIP='''+'0'''+','+
    'PA_OK='''+IfThen(CheckBox1.Checked,'1','0') +''','+
    'PA_IGAZOLO='''+EgyebDlg.user_name+''''+

    ' WHERE PA_ID = '+ret_kod;
    // SQL.Execute;
    Command_Run(SQL, S, true);
  end;
  ///////////////////////////////////////
  if CheckBox2.Checked then   // �tad�s-�tv�tel
  begin
   if  M6_.Text<>'' then
   begin
    venev_:=M6_.Text;
    vekod_:=E6_.Text;
   end;
   if  CB1_.Text<>'' then
   begin
    venev_:=CB1_.Text;
    vekod_:= _rkod[CB1_.ItemIndex];
   end;
 	 if ID2 = 0 then begin
		//�j rekord felvitele
		// SQL.CommandText:='INSERT INTO PALETTA (PA_DATUM,PA_SKOD1,PA_SNEV1,PA_RENSZ,PA_POTSZ,PA_VEKOD,PA_VENEV,PA_FELDB,PA_LERDB,PA_MEGJ,PA_MNEM,PA_MTIP,PA_OK,PA_IGAZOLO,PA_ID2) VALUES (' +
    S:='INSERT INTO PALETTA (PA_DATUM,PA_SKOD1,PA_SNEV1,PA_RENSZ,PA_POTSZ,PA_VEKOD,PA_VENEV,PA_FELDB,PA_LERDB,PA_MEGJ,PA_MNEM,PA_MTIP,PA_OK,PA_IGAZOLO,PA_ID2) VALUES (' +
    ''''+M1.Text+''','''+E7_.Text+''','''+M7_.text+''','''+M2_.Text+''','''+M3_.Text+''','''+vekod_+''','''+venev_+''','+Trim(SqlSzamString(StringSzam(dble),3,0))+','+Trim(SqlSzamString(StringSzam(dbfel),3,0))+','+
    ''''+M5_.Text+''','''+CB3_.Text+''','''+'0'''+','''+IfThen(CheckBox1_.Checked,'1','0')+''','''+EgyebDlg.user_name+''','''+IntToStr(ID1)+'''' +' )';
    // SQL.Execute;
    Command_Run(SQL, S, true);

    QID.Close;
    QID.Open;
    ID2:=QIDMAXID.Value;
    QID.Close;

    // SQL.CommandText:='UPDATE PALETTA set PA_ID2='+IntToStr(ID2)+' where PA_ID='+IntToStr(ID1);
    S:='UPDATE PALETTA set PA_ID2='+IntToStr(ID2)+' where PA_ID='+IntToStr(ID1);
    // SQL.Execute;
    Command_Run(SQL, S, true);
   end
   else
   begin
    // SQL.CommandText:='UPDATE PALETTA set '+
    S:='UPDATE PALETTA set '+
    'PA_DATUM='''+M1.Text+''','+
    'PA_SKOD1='''+E7_.Text+''','+
    'PA_SNEV1='''+M7_.Text+''','+
    'PA_RENSZ='''+M2_.Text+''','+
    'PA_POTSZ='''+M3_.Text+''','+
    'PA_VEKOD='''+vekod_+''','+
    'PA_VENEV='''+venev_+''','+
    'PA_FELDB='+SqlSzamString(StringSzam(dble), 3, 0)+','+
    'PA_LERDB='+SqlSzamString(StringSzam(dbfel), 3, 0)+','+
    'PA_MEGJ='''+M5_.Text+''','+
    'PA_MNEM='''+CB3_.Text+''','+
    'PA_MTIP='''+'0'''+','+
    'PA_OK='''+IfThen(CheckBox1_.Checked,'1','0') +''','+
    'PA_IGAZOLO='''+EgyebDlg.user_name+''''+

    ' WHERE PA_ID = '+IntToStr(ID2);
    // SQL.Execute;
    Command_Run(SQL, S, true);
   end;
  end;
  modosult:=False;
	Query1.Close;
	Close;
end;

procedure TPalettabeDlg.MaskEditClick(Sender: TObject);
begin
	with Sender as TMaskEdit do begin
	SelectAll;
  end;
end;

procedure TPalettabeDlg.MaskEditExit(Sender: TObject);
begin
	with Sender as TMaskEdit do begin
		Text := StrSzamStr(Text,3,0);
	end;
end;

procedure TPalettabeDlg.MaskEditKeyPress(Sender: TObject; var Key: Char);
begin
  with Sender as TMaskEdit do begin
		Key := EgyebDlg.Jochar(Text,Key,3,0);
  end;
end;

procedure TPalettabeDlg.Modosit(Sender: TObject);
begin
	modosult := true;
end;

procedure TPalettabeDlg.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
	EgyebDlg.FormReturn(Key);
end;

procedure TPalettabeDlg.M1Exit(Sender: TObject);
begin
	if ( ( M1.Text <> '' ) and ( not Jodatum2(M1 ) ) ) then begin
		NoticeKi('Hib�s d�tum megad�sa!');
		M1.Text := '';
		M1.Setfocus;
	end;
end;

procedure TPalettabeDlg.BitBtn7Click(Sender: TObject);
begin
	EgyebDlg.Calendarbe(M1);
end;

procedure TPalettabeDlg.CB1Change(Sender: TObject);
begin
	modosult := true;
end;

procedure TPalettabeDlg.CB3Change(Sender: TObject);
begin
	modosult := true;
  if (StrToInt( (copy(_mkod[ _mnev.IndexOf(CB3.Text)],1,1)) ) mod 2)<>0 then
    Label10.Caption:='KI (LE)'
  else
    Label10.Caption:='BE (FEL)'  ;

end;

procedure TPalettabeDlg.BitBtn1Click(Sender: TObject);
begin
	GepkocsiValaszto(M2, GK_TRAKTOR);
end;

procedure TPalettabeDlg.keszletki;
var
  sql: string;
begin
end;

procedure TPalettabeDlg.M2Click(Sender: TObject);
begin
  if not PalettaFmDlg.csakmegjegyzes   then
  BitBtn1.OnClick(self);
end;

procedure TPalettabeDlg.BitBtn2Click(Sender: TObject);
begin
	GepkocsiValaszto(M3, GK_POTKOCSI);
end;

procedure TPalettabeDlg.BitBtn4Click(Sender: TObject);
var
  rendsz:string;
begin
	DolgozoValaszto(E7, M7);
  rendsz:= Query_Select('DOLGOZO','DO_KOD',E7.Text,'DO_RENDSZ');
  if Length(rendsz)>2 then
     M2.Text:=rendsz;
end;

procedure TPalettabeDlg.BitBtn3Click(Sender: TObject);
begin
	{A megb�z� beolvas�sa}
	Application.CreateForm(TVevoUjFmDlg, VevoUjFmDlg);
	VevoUjFmDlg.valaszt := true;
	VevoUjFmDlg.ShowModal;
	if VevoUjFmDlg.ret_vkod <> '' then begin
		E6.Text 	:= VevoUjFmDlg.ret_vkod;
		M6.Text 	:= VevoUjFmDlg.ret_vnev;
	end;
	VevoUjFmDlg.Destroy;
end;

procedure TPalettabeDlg.BitBtn5Click(Sender: TObject);
begin
  M2.Text:='';
end;

procedure TPalettabeDlg.BitBtn6Click(Sender: TObject);
begin
  M3.Text:='';

end;

procedure TPalettabeDlg.BitBtn8Click(Sender: TObject);
begin
  M6.Text:='';

end;

procedure TPalettabeDlg.BitBtn9Click(Sender: TObject);
begin
  M7.Text:='';

end;

procedure TPalettabeDlg.M3Click(Sender: TObject);
begin
  if not PalettaFmDlg.csakmegjegyzes   then
  BitBtn2.OnClick(self);
end;

procedure TPalettabeDlg.M6Click(Sender: TObject);
begin
  if not PalettaFmDlg.csakmegjegyzes   then
  BitBtn3.OnClick(self);
end;

procedure TPalettabeDlg.M7Click(Sender: TObject);
begin
  if not PalettaFmDlg.csakmegjegyzes   then
  BitBtn4.OnClick(self);
end;

procedure TPalettabeDlg.BitBtn10Click(Sender: TObject);
begin
  CB1.ItemIndex:=0;
end;

procedure TPalettabeDlg.BitBtn11Click(Sender: TObject);
begin
  CB4.ItemIndex:=-1;
end;

procedure TPalettabeDlg.BitBtn12Click(Sender: TObject);
begin
  CB5.ItemIndex:=-1;

end;

procedure TPalettabeDlg.BitBtn22Click(Sender: TObject);
begin
  CB1_.ItemIndex:=0;

end;

procedure TPalettabeDlg.BitBtn16Click(Sender: TObject);
begin
	Application.CreateForm(TVevoUjFmDlg, VevoUjFmDlg);
	VevoUjFmDlg.valaszt := true;
	VevoUjFmDlg.ShowModal;
	if VevoUjFmDlg.ret_vkod <> '' then begin
		E6_.Text 	:= VevoUjFmDlg.ret_vkod;
		M6_.Text 	:= VevoUjFmDlg.ret_vnev;
	end;
	VevoUjFmDlg.Destroy;

end;

procedure TPalettabeDlg.BitBtn20Click(Sender: TObject);
begin
  M6_.Text:='';

end;

procedure TPalettabeDlg.BitBtn23Click(Sender: TObject);
begin
  CB4_.ItemIndex:=-1;

end;

procedure TPalettabeDlg.BitBtn24Click(Sender: TObject);
begin
  CB5_.ItemIndex:=-1;

end;

procedure TPalettabeDlg.BitBtn21Click(Sender: TObject);
begin
  M7_.Text:='';

end;

procedure TPalettabeDlg.BitBtn17Click(Sender: TObject);
var
  rendsz:string;
begin
	DolgozoValaszto(E7_, M7_);
  rendsz:= Query_Select('DOLGOZO','DO_KOD',E7_.Text,'DO_RENDSZ');
  if Length(rendsz)>2 then
     M2_.Text:=rendsz;

end;

procedure TPalettabeDlg.BitBtn18Click(Sender: TObject);
begin
  M2_.Text:='';

end;

procedure TPalettabeDlg.BitBtn14Click(Sender: TObject);
begin
	GepkocsiValaszto(M2_, GK_TRAKTOR);

end;

procedure TPalettabeDlg.BitBtn19Click(Sender: TObject);
begin
  M3_.Text:='';

end;

procedure TPalettabeDlg.BitBtn15Click(Sender: TObject);
begin
	GepkocsiValaszto(M3_, GK_POTKOCSI);

end;

procedure TPalettabeDlg.CB3_Change(Sender: TObject);
begin
	modosult := true;
  if (StrToInt( (copy(_mkod[ _mnev.IndexOf(CB3_.Text)],1,1)) ) mod 2)<>0 then
    Label25.Caption:='KI (LE)'
  else
    Label25.Caption:='BE (FEL)'  ;

end;

procedure TPalettabeDlg.M1_Exit(Sender: TObject);
begin
	if ( ( M1_.Text <> '' ) and ( not Jodatum2(M1_ ) ) ) then begin
		NoticeKi('Hib�s d�tum megad�sa!');
		M1_.Text := '';
		M1_.Setfocus;
	end;

end;

procedure TPalettabeDlg.BitBtn7_Click(Sender: TObject);
begin
	EgyebDlg.Calendarbe(M1_);

end;

procedure TPalettabeDlg.M6_Click(Sender: TObject);
begin
  BitBtn16.OnClick(self);

end;

procedure TPalettabeDlg.M7_Click(Sender: TObject);
begin
  BitBtn17.OnClick(self);

end;

procedure TPalettabeDlg.M2_Click(Sender: TObject);
begin
  BitBtn14.OnClick(self);

end;

procedure TPalettabeDlg.M3_Click(Sender: TObject);
begin
  if not PalettaFmDlg.csakmegjegyzes   then
  BitBtn15.OnClick(self);

end;

procedure TPalettabeDlg.CheckBox2Click(Sender: TObject);
begin
  if CheckBox2.Checked then
    Width:=1024
  else
    Width:=512;

  Left:= Trunc((Screen.Width-width)/2) ;
end;

procedure TPalettabeDlg.M4Change(Sender: TObject);
begin
	modosult := true;
  M4_.Text:=M4.Text;
end;

procedure TPalettabeDlg.FormResize(Sender: TObject);
begin
  CheckBox2.Left:=Trunc((width-150)/2);
end;

procedure TPalettabeDlg.CheckBox2Enter(Sender: TObject);
begin
  if PalettaFmDlg.csakmegjegyzes   then   M5.SetFocus;;
  if id2>0 then
    BitKilep.SetFocus;

end;

procedure TPalettabeDlg.BitBtn13Click(Sender: TObject);
var
  felrako: string;
begin
	Application.CreateForm(TFelrakoFmDlg, FelrakoFmDlg);
	FelrakoFmDlg.valaszt	:= true;
	FelrakoFmDlg.ShowModal;
	if FelrakoFmDlg.ret_vkod <> '' then begin
    felrako:=Query_Select('FELRAKO','FF_FEKOD',FelrakoFmDlg.ret_vkod,'FF_FELNEV')+' '+ Query_Select('FELRAKO','FF_FEKOD',FelrakoFmDlg.ret_vkod,'FF_FELTEL')+' '+ Query_Select('FELRAKO','FF_FEKOD',FelrakoFmDlg.ret_vkod,'FF_FELCIM');
    CB4.Text:=felrako;
//    CB4.ItemIndex:= CB4.Items.IndexOf(felrako);
	end;
	FelrakoFmDlg.Destroy;
end;

procedure TPalettabeDlg.BitBtn26Click(Sender: TObject);
var
  felrako: string;
begin
	Application.CreateForm(TFelrakoFmDlg, FelrakoFmDlg);
	FelrakoFmDlg.valaszt	:= true;
	FelrakoFmDlg.ShowModal;
	if FelrakoFmDlg.ret_vkod <> '' then begin
    felrako:=Query_Select('FELRAKO','FF_FEKOD',FelrakoFmDlg.ret_vkod,'FF_FELNEV')+' '+ Query_Select('FELRAKO','FF_FEKOD',FelrakoFmDlg.ret_vkod,'FF_FELTEL')+' '+ Query_Select('FELRAKO','FF_FEKOD',FelrakoFmDlg.ret_vkod,'FF_FELCIM');
    CB4_.Text:=felrako;
//    CB4.ItemIndex:= CB4.Items.IndexOf(felrako);
	end;
	FelrakoFmDlg.Destroy;

end;

procedure TPalettabeDlg.BitBtn25Click(Sender: TObject);
var
  felrako: string;
begin
	Application.CreateForm(TFelrakoFmDlg, FelrakoFmDlg);
	FelrakoFmDlg.valaszt	:= true;
	FelrakoFmDlg.ShowModal;
	if FelrakoFmDlg.ret_vkod <> '' then begin
    felrako:=Query_Select('FELRAKO','FF_FEKOD',FelrakoFmDlg.ret_vkod,'FF_FELNEV')+' '+ Query_Select('FELRAKO','FF_FEKOD',FelrakoFmDlg.ret_vkod,'FF_FELTEL')+' '+ Query_Select('FELRAKO','FF_FEKOD',FelrakoFmDlg.ret_vkod,'FF_FELCIM');
    CB5.Text:=felrako;
//    CB4.ItemIndex:= CB4.Items.IndexOf(felrako);
	end;
	FelrakoFmDlg.Destroy;

end;

procedure TPalettabeDlg.BitBtn27Click(Sender: TObject);
var
  felrako: string;
begin
	Application.CreateForm(TFelrakoFmDlg, FelrakoFmDlg);
	FelrakoFmDlg.valaszt	:= true;
	FelrakoFmDlg.ShowModal;
	if FelrakoFmDlg.ret_vkod <> '' then begin
    felrako:=Query_Select('FELRAKO','FF_FEKOD',FelrakoFmDlg.ret_vkod,'FF_FELNEV')+' '+ Query_Select('FELRAKO','FF_FEKOD',FelrakoFmDlg.ret_vkod,'FF_FELTEL')+' '+ Query_Select('FELRAKO','FF_FEKOD',FelrakoFmDlg.ret_vkod,'FF_FELCIM');
    CB5_.Text:=felrako;
//    CB4.ItemIndex:= CB4.Items.IndexOf(felrako);
	end;
	FelrakoFmDlg.Destroy;

end;

procedure TPalettabeDlg.M1Enter(Sender: TObject);
begin
 // if csakmegjegyzes then exit;
  if PalettaFmDlg.csakmegjegyzes   then   M5.SetFocus;;
end;

procedure TPalettabeDlg.M1_Enter(Sender: TObject);
begin
  if PalettaFmDlg.csakmegjegyzes   then   M5_.SetFocus;;

end;

end.
