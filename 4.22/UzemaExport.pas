unit UzemaExport;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, Data.Win.ADODB, Vcl.Grids, Strutils,
  Vcl.StdCtrls, Vcl.Mask, T_DictCsv, J_SQL, Kozos, Vcl.ExtCtrls,
  Vcl.Samples.Gauges;

type

  TFogyasztRecord = record
       Ervenyes: boolean;
       Datum: string;
       Kilometer: integer;
       Tonna: double;
       Liter: double;
       Jarat: string;
       Sofor: string;
    end; // record

  TUzemaExportDlg = class(TForm)
    ListaGrid: TStringGrid;
    Query3: TADOQuery;
    Query2: TADOQuery;
    Query5: TADOQuery;
    Query1: TADOQuery;
    QueryGk: TADOQuery;
    ebFrom: TMaskEdit;
    ebTo: TMaskEdit;
    Label1: TLabel;
    Button1: TButton;
    pnlInfo: TPanel;
    Gauge1: TGauge;
    procedure FormCreate(Sender: TObject);
    procedure	ExportAll;
    function Gepkocs_tolt(rsz, datumtol, datumig, telep: string): boolean;
    function AdatExport_mag(i: integer): TFogyasztRecord;
    procedure Button1Click(Sender: TObject);
  private
      rendszam: string;
     	reszkm			: double;
     	reszliter		: double;
     	osszkm			: double;
     	elozokm			: double;
     	elsorekord		: boolean;
     	kezdokm			: longint;
     	zarokm			: longint;
     	apehnorma		: boolean;
     	dtol	   		: string;
       kezdotkm        : integer;
  public
	  vanadat			: boolean;
  end;

var
  UzemaExportDlg: TUzemaExportDlg;

implementation

{$R *.dfm}

uses Egyeb;

procedure TUzemaExportDlg.Button1Click(Sender: TObject);
begin
   ExportAll;
end;

procedure	TUzemaExportDlg.ExportAll;
var
  S, RSZ, Datumtol, Datumig, ExportFN, ExportFN_SQL: string;
  i, gknum: integer;
  FogyRec: TFogyasztRecord;
  ExportList: TNP_CSV;
  ExportLine: TDictCSVLine;
  F: textfile;
begin
  Datumtol:= ebFrom.Text;
  Datumig:= ebTo.Text;
  S:= 'select GK_RESZ from gepkocsi where GK_GKKAT like ''24%'' ';
  // debug
  // S:= S + 'and GK_RESZ = ''LSG-841''';
  Query_Run(QueryGk, S, true);

  Gauge1.MaxValue:= QueryGk.RecordCount;
  gknum:=0;
  ExportList:= TNP_CSV.Create;
  if not DirecToryExists(EgyebDlg.DocumentPath+'LIST') then begin
   	ForceDirectories(EgyebDlg.DocumentPath+'LIST');
    end;
  ExportFN:=EgyebDlg.DocumentPath+'LIST'+'\FOGYIEXPORT_CSV.TXT';
  ExportFN_SQL:=EgyebDlg.DocumentPath+'LIST'+'\FOGYIEXPORT_SQL.TXT';
  assignfile(F, ExportFN_SQL);
  rewrite(F);
  pnlInfo.Caption:='';
  while not QueryGk.EOF do begin
     RSZ:= QueryGk.FieldByName('GK_RESZ').AsString;
     Gauge1.Progress:= gknum;
     Gauge1.Refresh;
     Inc(gknum);
     // if RSZ= 'KPZ-529' then
     //    NoticeKi('Debug');
		 if Gepkocs_tolt (RSZ, Datumtol, Datumig, '0') then begin   // csak ha van adat
       elsorekord:= true;
        for i := 0 to ListaGrid.RowCount-1 do begin
          FogyRec:= AdatExport_mag(i);
          if FogyRec.Ervenyes then begin
            ExportLine:= TDictCSVLine.Create;
            ExportLine.Clear;
            ExportLine.Add('01_Rendszam', RSZ);
            ExportLine.Add('02_Datum', FogyRec.Datum);
            ExportLine.Add('03_Kilometer', FormatFloat('0', FogyRec.Kilometer));
            ExportLine.Add('04_Tonna', FormatFloat('0.00', FogyRec.Tonna));
            ExportLine.Add('05_Liter', FormatFloat('0.00', FogyRec.Liter));
            ExportLine.Add('06_Jarat', FogyRec.Jarat);
            ExportLine.Add('07_Sofor', FogyRec.Sofor);
            ExportList.Add(ExportLine);
            // ----- SQL ----- //
            S:= 'insert into _fogystat (rendszam, datum, kilometer, tonna, liter, jarat, sofor) values (';
            S:= S+ ''''+RSZ+''', ';
            S:= S+ ''''+FogyRec.Datum+''', ';
            S:= S+ FormatFloat('0', FogyRec.Kilometer)+', ';
            S:= S+ StringReplace(FormatFloat('0.00', FogyRec.Tonna), ',', '.', [rfReplaceAll])+', ';
            S:= S+ StringReplace(FormatFloat('0.00', FogyRec.Liter), ',', '.', [rfReplaceAll])+', ';
            S:= S+ ''''+FogyRec.Jarat+''', ';
            S:= S+ ''''+FogyRec.Sofor+''')';
            writeln(F, S);
            pnlInfo.Caption:= 'Feldolgoz�s alatt: '+RSZ + ' / '+FogyRec.Jarat;
            pnlInfo.Refresh;
            end;  // if
          end;  // for
        end;  // if
		QueryGk.Next;
	end;
  pnlInfo.Caption:= 'Feldolgoz�s k�sz!';
  Gauge1.Progress:= Gauge1.MaxValue;
  Gauge1.Refresh;
  closefile(F);
  ExportList.SaveToCSV(ExportFN, True);
end;

function	TUzemaExportDlg.Gepkocs_tolt(rsz, datumtol, datumig, telep: string): boolean;
var
  sqlstr	: string;
  d1, d2	: string;
  str, S		: string;
  i: integer;
begin
  rendszam:= rsz;
  // A megadott intervallumba es� els� �s utols� teli tankol�s adatai
  S:='SELECT min(KS_DATUM) FROM KOLTSEG WHERE KS_RENDSZ = '''+rsz+''' AND KS_TELE > 0 AND KS_TIPUS = 0 and KS_DATUM >='''+datumtol+'''';
  d1:= Query_SelectString('KOLTSEG', S);
  if d1='' then begin  // nincs az intervallumban teli tank
      Result:= False;
      Exit;
      end;
  S:='SELECT KS_KMORA FROM KOLTSEG WHERE KS_RENDSZ = '''+rsz+''' AND KS_TELE > 0 AND KS_TIPUS = 0 and KS_DATUM ='''+d1+'''';
  kezdokm:= StrToInt(Query_SelectString('KOLTSEG', S));

  S:='SELECT max(KS_DATUM) FROM KOLTSEG WHERE KS_RENDSZ = '''+rsz+''' AND KS_TELE > 0 AND KS_TIPUS = 0 and KS_DATUM <='''+datumig+'''';
  d2:= Query_SelectString('KOLTSEG', S);
  if d2='' then begin  // nincs az intervallumban teli tank
      Result:= False;
      Exit;
      end;
  S:='SELECT KS_KMORA FROM KOLTSEG WHERE KS_RENDSZ = '''+rsz+''' AND KS_TELE > 0 AND KS_TIPUS = 0 and KS_DATUM ='''+d2+'''';
  zarokm:= StrToInt(Query_SelectString('KOLTSEG', S));

 	kezdotkm    := 0;  // ism�telt gener�l�sn�l (nyomtat�s) kezdje el�lr��l

	Query3.Close;
	// Az aktu�lis �v
	sqlstr	:= 'SELECT * FROM KOLTSEG WHERE KS_RENDSZ = '''+rsz+''' AND KS_DATUM >= '''+d1+''' '+
		' AND KS_DATUM <= '''+d2+''' AND KS_UZMENY > 0 AND KS_KMORA >= '+IntToStr(kezdokm) +
		' AND KS_TIPUS = 0 '+IfThen(telep='1',' and KS_FIMOD='''+'TELEP''')+IfThen(telep='2',' and KS_FIMOD<>'''+'Telep''');
  sqlstr := sqlstr + ' AND KS_TETAN = 0 ';
	// sqlstr	:= sqlstr + ' ORDER BY KS_DATUM, KS_KMORA, KS_IDO, KS_TELE '; // egyszerre tankol�sn�l a tele legyen az utols�
  sqlstr	:= sqlstr + ' ORDER BY KS_DATUM, KS_KMORA, KS_TELE ';  // a KS_IDO-t kivettem, mert van hogy dupla tankol�sn�l 1 perc elt�r�s van a KOLTSEG-ben.
	Query_Run(Query1, sqlstr, true);
	// A LISTAGRID felt�lt�se
	ListaGrid.RowCount := Query1.RecordCount;
	Query1.First;
  if not Query1.EOF then result := true else result:= false;
  i:=0;
	while not Query1.EOF do begin
		Listagrid.Cells[0, i] 	:= Query1.FieldByname('KS_DATUM').AsString;
		Listagrid.Cells[1, i]	 	:= Query1.FieldByname('KS_JAKOD').AsString;
		Listagrid.Cells[2, i] 	:= Query1.FieldByname('KS_KMORA').AsString;
		Listagrid.Cells[3, i] 	:= Query1.FieldByname('KS_UZMENY').AsString;
		Listagrid.Cells[4, i] 	:= Query1.FieldByname('KS_TELE').AsString;
		Listagrid.Cells[10, i] 	:= Query1.FieldByname('KS_ORSZA').AsString;
		Listagrid.Cells[11, i] 	:= Query1.FieldByname('KS_FIMOD').AsString;
		Listagrid.Cells[12, i]  := Query1.FieldByname('KS_IDO').AsString;
		Inc(i);
		Query1.Next;
  	end; // while
end;


function TUzemaExportDlg.AdatExport_mag(i: integer): TFogyasztRecord;
var
   tkmatlag    : double;
   megtliter   : double;
   megt100km   : double;
   LiterS, Sofor: string;
   Ervenyes_tonna, Ervenyes_liter: boolean;
begin
	{KM �ra �ll�s�nak ellen�rz�se}
	if ( 	( kezdokm > StrToIntDef(Listagrid.Cells[2, i],0) ) or
		( zarokm < StrToIntDef(Listagrid.Cells[2, i],0) ) ) then begin
		Result.Ervenyes:= False;  // �rv�nytelen
    Exit;
  	end;

  Result.Datum:= Listagrid.Cells[0, i];
  Ervenyes_liter:= False;
  Ervenyes_tonna:= False;
  if kezdotkm <> 0 then begin
    if Listagrid.Cells[4, i] = '1' then begin    // KS_TELE
       // Csak a tele tankol�sokn�l sz�molunk
       tkmatlag    := GetTonnaAtlag(rendszam, kezdotkm, StrToIntDef(Listagrid.Cells[2, i], 0), false);
       Result.Tonna	:= tkmatlag/1000;
       Ervenyes_tonna:= True;
       end;  // if
    end;  // if
   // ha ez a tankol�s tele volt, akkor az �j kezd�tkm ennek a km�ra �rt�ke
   if Listagrid.Cells[4, i] = '1' then begin  // KS_TELE
       kezdotkm := StrToIntDef(Listagrid.Cells[2, i], 0);   // KS_KMORA
      end;

	if elsorekord then begin
		elsorekord := false;
	end else begin
		reszliter		:= reszliter + StringSzam(Listagrid.Cells[3, i]);
   	end;
	if StrToIntDef(Listagrid.Cells[4, i],0) = 1 then begin 	// Tele tankn�l a megtett km ill. a fogyaszt�s ki�r�sa
		reszkm			:= StrToIntDef(Listagrid.Cells[2, i],0) - elozokm;
   	osszkm			:= osszkm + reszkm;
		elozokm			:= StrToIntDef(Listagrid.Cells[2, i],0);
		Result.Kilometer:= Round(reszkm);
    Result.Liter:= reszliter;
    Ervenyes_liter:= True;
   	if reszkm = 0 then begin
     		reszkm 		:= 1;
       	reszliter	:= 0;
       	end;  // if
    reszliter	:= 0;
       // A fogyaszt�si adatok meghat�roz�sa
       // Kell ez nek�nk?
       {
       Application.CreateForm(TFogyasztasDlg, FogyasztasDlg);
       FogyasztasDlg.ClearAllList;
       FogyasztasDlg.GeneralBetoltes(Rendszam, Round(elozokm-reszkm), StrToIntDef(Listagrid.Cells[2, sorszam],0) );
       FogyasztasDlg.CreateTankLista;
       FogyasztasDlg.CreateAdBlueLista;
       FogyasztasDlg.Szakaszolo;
       megtliter   :=FogyasztasDlg.GetMegtakaritas;
       if ( ( reszkm = 0 ) or (rekordszam = 2) ) then begin
           megt100km   := 0;
       end else begin
           megt100km   :=  megtliter*100/reszkm;
       end;
       FogyasztasDlg.Destroy;
       }
  	end;
  // A j�ratsz�m ki�r�sa
  if Listagrid.Cells[1, i] <> '' then begin
  if Query_Run(Query2, 'SELECT * FROM JARAT WHERE JA_KOD = '''+Listagrid.Cells[1, i]+''' ', true) then begin
    if Query2.RecordCount > 0 then begin
      JaratEllenor( Query2.FieldByname('JA_KOD').AsString );
      Result.Jarat:= Trim(Query2.FieldByName('JA_ORSZ').AsString) + '-' + Format('%5.5d',[Query2.FieldByName('JA_ALKOD').AsInteger]);
      // A dolgoz�k beolvas�sa
      Result.Sofor:= '';
      Query_Run(Query5, 'SELECT * FROM JARSOFOR WHERE JS_JAKOD = '''+Query2.FieldByname('JA_KOD').AsString+''' ORDER BY JS_SORSZ ');
      while not  Query5.Eof do begin
        Sofor:= Query_Select('DOLGOZO', 'DO_KOD', Query5.FieldByname('JS_SOFOR').AsString , 'DO_NAME');
        if Result.Sofor='' then
          Result.Sofor:= Sofor
        else Result.Sofor:= Result.Sofor	+ ', '+Sofor;
        Query5.Next;
        end;  // while
      end; // if RecordCount
    end; // if listagrid.cells
	end;
	Query2.Close;
  Result.Ervenyes:= Ervenyes_tonna and Ervenyes_liter;
end;



procedure TUzemaExportDlg.FormCreate(Sender: TObject);
begin
	EgyebDlg.SetADOQueryDatabase(QueryGk);
	EgyebDlg.SetADOQueryDatabase(Query1);
	EgyebDlg.SetADOQueryDatabase(Query2);
	EgyebDlg.SetADOQueryDatabase(Query3);
 	EgyebDlg.SetADOQueryDatabase(Query5);
end;

end.
