unit Szamla_DK_HUF;

interface

uses
  WinTypes, WinProcs, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, QuickRpt, QRExpr, Qrctrls, ExtCtrls, printers, DB, DBTables,
  ADODB, J_DataModule, System.UITypes, System.Contnrs ;

type
  TSzamla_DK_HUFForm = class(TForm)
    Rep: TQuickRep;
    QRBand3: TQRBand;
    QRLNEV: TQRLabel;
    QRLCIMB: TQRLabel;
	 QRLSzamla: TQRLabel;
	 QRBand1: TQRBand;
    QRBand2: TQRBand;
    QRLabel2: TQRLabel;
	 QRLabel3: TQRLabel;
	 QRLabel4: TQRLabel;
    QRLabel5: TQRLabel;
    QRShape1: TQRShape;
    QRShape2: TQRShape;
    QRLabel6: TQRLabel;
    QRLabel7: TQRLabel;
    QRLIRSZ: TQRLabel;
	 QRLabel8: TQRLabel;
	 QRLabel9: TQRLabel;
	 QRLabel10: TQRLabel;
    QRLabel11: TQRLabel;
    QRLabel12: TQRLabel;
	 QRLabel13: TQRLabel;
    QRLabel14: TQRLabel;
    QRShape3: TQRShape;
	 QRLabel15: TQRLabel;
    QRShape4: TQRShape;
    QRLabel16: TQRLabel;
	 QRShape5: TQRShape;
    QRLabel17: TQRLabel;
    QRShape6: TQRShape;
    QRLabel18: TQRLabel;
    QRShape7: TQRShape;
    QRLabel19: TQRLabel;
    QRShape8: TQRShape;
    QRShape9: TQRShape;
    QRShape10: TQRShape;
    QRShape11: TQRShape;
    QRShape12: TQRShape;
    QRLabel20: TQRLabel;
    QRLabel21: TQRLabel;
    QRLabel22: TQRLabel;
    QRLabel23: TQRLabel;
    QRLabel24: TQRLabel;
	 QRShape13: TQRShape;
    QRLabel25: TQRLabel;
	 QRLabel26: TQRLabel;
	 QRShape14: TQRShape;
	 QRLabel27: TQRLabel;
    QRLabel28: TQRLabel;
    QRShape15: TQRShape;
    QRShape16: TQRShape;
    QRLabel29: TQRLabel;
	 QRShape17: TQRShape;
	 QRShape18: TQRShape;
    QRShape19: TQRShape;
    QRShape20: TQRShape;
    QRShape21: TQRShape;
    QRShape22: TQRShape;
    QRShape23: TQRShape;
    QRShape24: TQRShape;
    QRShape25: TQRShape;
    QRLabel1: TQRLabel;
    QRLabel34: TQRLabel;
    QRLabel35: TQRLabel;
    QRLabel36: TQRLabel;
	 QRLabel37: TQRLabel;
    QRLabel38: TQRLabel;
    QRLabel39: TQRLabel;
    QRLabel40: TQRLabel;
    QRLabel41: TQRLabel;
	 QRLabel42: TQRLabel;
    QRLabel43: TQRLabel;
    QRLabel44: TQRLabel;
	 QRLabel45: TQRLabel;
    QRLabel46: TQRLabel;
    QRLabel48: TQRLabel;
	 LabelO1: TQRLabel;
    QRLabel47: TQRLabel;
    QRLabel49: TQRLabel;
    QRLabel50: TQRLabel;
	 QRLabel51: TQRLabel;
    QRLabel52: TQRLabel;
    QRLabel53: TQRLabel;
    QRLabel54: TQRLabel;
	 QRLabel55: TQRLabel;
	 QRShape26: TQRShape;
    QRLabel56: TQRLabel;
    QRLabel57: TQRLabel;
    QRLabel58: TQRLabel;
    QRShape27: TQRShape;
    QRLabel59: TQRLabel;
    QRLabel60: TQRLabel;
    QRLabel61: TQRLabel;
    QRLabel62: TQRLabel;
    QRLabel63: TQRLabel;
    QRLabel66: TQRLabel;
	 QRLabel67: TQRLabel;
    QRLabel68: TQRLabel;
    QRLabel70: TQRLabel;
    QRLabel30: TQRLabel;
	 QRLabel31: TQRLabel;
    QRLabel69: TQRLabel;
	 QRLabel71: TQRLabel;
    QRLabel74: TQRLabel;
	 QRLabel75: TQRLabel;
    QRLabel76: TQRLabel;
    QRLabel77: TQRLabel;
	 QRLabel32: TQRLabel;
    QRShape33: TQRShape;
    QRShape34: TQRShape;
    QRLabel81: TQRLabel;
    QRLabel72: TQRLabel;
    QRLabel73: TQRLabel;
    QRLabel80: TQRLabel;
    QRBand4: TQRBand;
    QRLabel82: TQRLabel;
	 QRLabel83: TQRLabel;
    QRLabel84: TQRLabel;
    QRLabel85: TQRLabel;
    QRLabel86: TQRLabel;
    QRLabel88: TQRLabel;
	 QRLabel89: TQRLabel;
    QRLabel90: TQRLabel;
	 QRShape29: TQRShape;
    QRShape30: TQRShape;
    QRShape31: TQRShape;
    QRLabel33: TQRLabel;
    QRLabel87: TQRLabel;
	 QRShape32: TQRShape;
    QRLabel91: TQRLabel;
    QRShape35: TQRShape;
    QRShape36: TQRShape;
    QRLabel92: TQRLabel;
	 QRLabel93: TQRLabel;
    QRLabel94: TQRLabel;
	 QRLabel95: TQRLabel;
    QRLabel96: TQRLabel;
    QRLabel97: TQRLabel;
    QRLabel98: TQRLabel;
    QRLabel99: TQRLabel;
	 QRLabel100: TQRLabel;
    QRLabel78: TQRLabel;
    QRLabel79: TQRLabel;
    QE1: TQRLabel;
    QE2: TQRLabel;
    QE3: TQRLabel;
    QE4: TQRLabel;
    QE5: TQRLabel;
	 QE6: TQRLabel;
    QE7: TQRLabel;
	 QRLabel101: TQRLabel;
    QRLabel65: TQRLabel;
    QRLabel102: TQRLabel;
    QueryKoz: TADOQuery;
    QueryAl: TADOQuery;
    QueryFej: TADOQuery;
	 QuerySor: TADOQuery;
	 QRLabel8A: TQRLabel;
	 SZ_BOLD1: TQRLabel;
	 SZ_NORM1: TQRLabel;
    QRLabel103: TQRLabel;
    QRLabel104: TQRLabel;
    QRLabel106: TQRLabel;
    QRLabel107: TQRLabel;
    QRLabel141: TQRLabel;
	 procedure FormCreate(Sender: TObject);
	 procedure QRBand3BeforePrint(Sender: TQRCustomBand;
	   var PrintBand: Boolean);
	 function	Tolt(szkod	: string) : boolean;
	 procedure QRBand1BeforePrint(Sender: TQRCustomBand;
	   var PrintBand: Boolean);
	 procedure QRBand2BeforePrint(Sender: TQRCustomBand;
	   var PrintBand: Boolean);
	 procedure RepBeforePrint(Sender: TCustomQuickRep;
	   var PrintReport: Boolean);
	 procedure FormDestroy(Sender: TObject);
  private
		oldalszam		: integer;
		kellvnem		: boolean;	// Jelz�, hogy kell-e haszn�lni valutanemet a sz�ml�n
		tenyvnem		: string;	// A sz�mla t�nyleges valutaneme
		JSzamla     	: TJSzamla;
  public
  end;

var
  Szamla_DK_HUFForm: TSzamla_DK_HUFForm;

implementation

uses
	Egyeb, J_SQL, Kozos;

{$R *.DFM}

procedure TSzamla_DK_HUFForm.FormCreate(Sender: TObject);
begin
	EgyebDlg.SetADOQueryDatabase(QueryKoz);
	EgyebDlg.SetADOQueryDatabase(QueryAl);
	EgyebDlg.SetADOQueryDatabase(QueryFej);
	EgyebDlg.SetADOQueryDatabase(QuerySor);
	oldalszam	:= 1;
end;

procedure TSzamla_DK_HUFForm.QRBand3BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
var
  str      : string;
  str2     : string;
  kodstr	: string;
  orsz		: string;
  csopaz  : string;
begin
 	if QueryFej.FieldByName('SA_PELDANY').AsInteger < EgyebDlg.PeldanySzam then begin
 		QrLabel66.Caption := EgyebDlg.Read_SZGrid('210','102')+
		Format('%5.0d',[QueryFej.FieldByName('SA_PELDANY').AsInteger + 1])+' / '+
			IntToStr(EgyebDlg.Peldanyszam)+'  ';
	end else begin
		QrLabel66.Caption := EgyebDlg.Read_SZGrid('210','137');
	end;
  QrLabel81.Caption := EgyebDlg.Read_SZGrid('210','103')+Format('%5d',[oldalszam]) + '  ';
  Inc(oldalszam);
	{A fejl�c adatainak ki�r�sa}
	QRLSzamla.Caption	:= EgyebDlg.Read_SZGrid('210','101');
	if Trim(QueryFej.FieldByName('SA_HELYE').AsString) = '1' then begin
		QRLSzamla.Caption	:= EgyebDlg.Read_SZGrid('210','201');
	end;
	if StrToIntDef(QueryFej.FieldByName('SA_RESZE').AsString, 0) > 0 then begin
		QRLSzamla.Caption	:= 'FIGYELEM!!! R�SZSZ�MLA!!!';
	end;
  QrLabel6.Caption  	:= EgyebDlg.Read_SZGrid('210','104');
  QrLabel7.Caption  	:= EgyebDlg.Read_SZGrid('210','105');
  QrLabel86.Caption 	:= EgyebDlg.Read_SZGrid('210','106');
  QrLabel88.Caption 	:= EgyebDlg.Read_SZGrid('210','107');
  QrLabel89.Caption 	:= EgyebDlg.Read_SZGrid('210','108');
  QrLabel90.Caption 	:= EgyebDlg.Read_SZGrid('210','109');
  QrLabel85.Caption 	:= EgyebDlg.Read_SZGrid('210','110');
  QrLabel26.Caption 	:= EgyebDlg.Read_SZGrid('210','111');
  QrLabel55.Caption 	:= EgyebDlg.Read_SZGrid('210','112');
  QrLabel58.Caption 	:= EgyebDlg.Read_SZGrid('210','113');
  QrLabel33.Caption 	:= EgyebDlg.Read_SZGrid('210','114');
  QrLabel87.Caption 	:= EgyebDlg.Read_SZGrid('210','115');
  QrLabel92.Caption 	:= EgyebDlg.Read_SZGrid('210','116');
  QrLabel93.Caption 	:= EgyebDlg.Read_SZGrid('210','117');
  QrLabel91.Caption 	:= EgyebDlg.Read_SZGrid('210','118');
  QrLabel94.Caption 	:= EgyebDlg.Read_SZGrid('210','119');
  QrLabel95.Caption 	:= EgyebDlg.Read_SZGrid('210','120');
  QrLabel96.Caption 	:= EgyebDlg.Read_SZGrid('210','121');
  QrLabel97.Caption 	:= EgyebDlg.Read_SZGrid('210','122');
  QrLabel98.Caption 	:= EgyebDlg.Read_SZGrid('210','123');
  QrLabel99.Caption 	:= EgyebDlg.Read_SZGrid('210','124');
  QrLNEV.Caption 		:= EgyebDlg.Read_SZGrid('CEG', '100');
  QrLIRSZ.Caption 		:= EgyebDlg.Read_SZGrid('CEG', '102');
  QrLabel8.Caption 	:= EgyebDlg.Read_SZGrid('CEG', '104');
  QrLabel9.Caption 	:= EgyebDlg.Read_SZGrid('CEG', '106');
  QrLabel10.Caption 	:= EgyebDlg.Read_SZGrid('CEG', '107');
  QrLabel8A.Caption 	:= EgyebDlg.Read_SZGrid('CEG', '108');
  QrLabel80.Caption 	:= EgyebDlg.Read_SZGrid('CEG', '109');
  if QueryFej.FieldByName('SA_FEJL1').AsString <> '' then begin
	{L�tezik fejl�c adat, azt kell betenni a sz�ml�ba}
     QrLNEV.Caption 	:= QueryFej.FieldByName('SA_FEJL1').AsString;
     QrLIRSZ.Caption 	:= QueryFej.FieldByName('SA_FEJL2').AsString;
     QrLabel8.Caption 	:= QueryFej.FieldByName('SA_FEJL3').AsString;
     QrLabel9.Caption 	:= QueryFej.FieldByName('SA_FEJL4').AsString;
     QrLabel10.Caption := QueryFej.FieldByName('SA_FEJL5').AsString;
     QrLabel8A.Caption := QueryFej.FieldByName('SA_FEJL6').AsString;
	  QrLabel80.Caption := QueryFej.FieldByName('SA_FEJL7').AsString;
	end;
	QrLabel11.Caption := QueryFej.FieldByName('SA_VEVONEV').AsString;
	orsz				:= Query_Select('VEVO', 'V_NEV', QueryFej.FieldByName('SA_VEVONEV').AsString, 'V_ORSZ');
	QrLabel12.Caption := orsz + '-' + QueryFej.FieldByName('SA_VEIRSZ').AsString;
	QrLabel13.Caption := QueryFej.FieldByName('SA_VEVAROS').AsString;
	QrLabel14.Caption := QueryFej.FieldByName('SA_VECIM').AsString;
	if Pos('.', QrLabel14.Caption) < 1 then begin
		QrLabel14.Caption := QrLabel14.Caption + '.';
	end;
	QrLabel101.Caption 		:= JSzamla.szamlaadoszam;
	csopaz:= Query_Select('VEVO', 'V_KOD', QueryFej.FieldByName('SA_VEVOKOD').AsString, 'VE_CSOPAZ');
  if csopaz<>'' then
    QRLabel141.Caption:='Csoport az.: '+csopaz
  else
    QRLabel141.Caption:='';
(*
	QrLabel101.Caption := QueryFej.FieldByName('SA_VEADO').AsString;
	if QrLabel101.Caption = '' then begin
		QrLabel101.Caption := QueryFej.FieldByName('SA_EUADO').AsString;
	end;
*)
	{A k�lf�ldi fizet�si m�d megkeres�se}
	str	:= QueryFej.FieldByName('SA_FIMOD').AsString;
  if Query_Run(QueryKoz, 'SELECT * FROM SZOTAR WHERE SZ_FOKOD = ''100'' AND SZ_MENEV = '''+str+''' ',true) then begin
  	if QueryKoz.RecordCount > 0 then begin
			kodstr 	:= QueryKoz.FieldByName('SZ_ALKOD').AsString;
		 str2		:= EgyebDlg.Read_SZGrid('200',kodstr);
        if str2 <> '' then begin
			str := str + '/'+str2;
        end;
	  end;
  end;
  QrLabel20.Caption := str;
  QrLabel21.Caption := QueryFej.FieldByName('SA_TEDAT').AsString;
  QrLabel22.Caption := QueryFej.FieldByName('SA_KIDAT').AsString;
  QrLabel23.Caption := QueryFej.FieldByName('SA_ESDAT').AsString;
  QrLabel24.Caption := QueryFej.FieldByName('SA_KOD').AsString;
  QrLabel25.Caption := QueryFej.FieldByName('SA_MEGJ').AsString;
  QrLabel56.Caption := QueryFej.FieldByName('SA_JARAT').AsString;
  { A j�ratsz�m alapj�n megkeress�k a bet�jelet is }
  if Query_Run(QueryAl, 'SELECT * FROM JARAT WHERE JA_ALKOD = '+
  	IntToStr(Round(StringSzam(QueryFej.FieldByName('SA_JARAT').AsString)))+' and substring(JA_JKEZD,1,4)='''+copy(QueryFej.FieldByName('SA_TEDAT').AsString,1,4)+''' ' ,true) then begin
  	QrLabel56.Caption := QueryAl.FieldByName('JA_ORSZ').AsString + ' - ' +
     	QueryFej.FieldByName('SA_JARAT').AsString;
  end;
  QrLabel57.Caption := QueryFej.FieldByName('SA_RSZ').AsString;
  QrLabel63.Caption := QueryFej.FieldByName('SA_POZICIO').AsString;
end;

function	TSzamla_DK_HUFForm.Tolt(szkod	: string) : boolean;
begin
	Result	:= false;
	Query_Run(QueryFej, 'SELECT * FROM SZFEJ WHERE SA_UJKOD = '+szkod);
	if QueryFej.RecordCount < 1 then begin
		NoticeKi('Hi�nyz� sz�mlarekord : ('+szkod+')');
		Exit;
	end;
	JSzamla	:= TJSzamla.Create(Self);
	JSzamla.FillSzamla(QueryFej.FieldByName('SA_UJKOD').AsString);
	Result		:= true;
	Query_Run(QuerySor, 'SELECT * FROM SZSOR WHERE SS_UJKOD = '+szkod+' ORDER BY SS_SOR');
	tenyvnem	:= GetSzamlaValuta(szkod);
	if ( ( tenyvnem	= 'HUF' ) or ( tenyvnem	= '' ) ) then begin
		kellvnem	:= false;
	end else begin
		kellvnem	:= true;
	end;
	QrLabel82.Caption := EgyebDlg.Read_SZGrid('210','140')+' DMK COMP Kft., Tatab�nya';
end;

procedure TSzamla_DK_HUFForm.QRBand1BeforePrint(Sender: TQRCustomBand;
	   var PrintBand: Boolean);
var
	sor_szama	: integer;
	JSzamlaSor	: TJSzamlaSor;
begin
	sor_szama	:= StrToIntDef(QuerySor.FieldByName('SS_SOR').AsString,-1) -1;
	if sor_szama >= 0 then begin
		JSzamlasor	:= (JSzamla.SzamlaSorok[sor_szama] as TJszamlaSor);
	end else begin
		NoticeKi('�rv�nytelen sorsz�m : '+QuerySor.FieldByName('SS_SOR').AsString);
		Exit;
	end;
	if QuerySor.FieldByName('SS_ITJSZJ').AsString = '' then begin
		QrLabel1.Caption := QuerySor.FieldByName('SS_TERNEV').AsString;
	end else begin
		QrLabel1.Caption := QuerySor.FieldByName('SS_ITJSZJ').AsString+', '+QuerySor.FieldByName('SS_TERNEV').AsString;
	end;
	{A k�lf�ldi le�r�s megjelen�t�se}
	QrLabel100.Caption := QuerySor.FieldByName('SS_LEIRAS').AsString;
	if QuerySor.FieldByName('SS_EGYAR').AsFloat = 0 then begin
		QrLabel32.Caption := '';
		QrLabel37.Caption := '';
		QrLabel68.Caption := '';
		QrLabel61.Caption := '';
		QrLabel34.Caption := '';
		QrLabel36.Caption := '';
		QrLabel35.Caption := '';
		QrLabel72.Caption := '';
		QrLabel73.Caption := '';
	end else begin
		QrLabel32.Caption := EgyebDlg.Read_SZGrid('101',QuerySor.FieldByName('SS_AFAKOD').AsString);
		QrLabel37.Caption := Format('%.2f',[StringSzam(QuerySor.FieldByName('SS_DARAB').AsString)]) ;
		QrLabel68.Caption := QuerySor.FieldByName('SS_MEGY').AsString;
		if QuerySor.FieldByName('SS_KULME').AsString <> '' then begin
			QrLabel68.Caption := QrLabel68.Caption + '/'+QuerySor.FieldByName('SS_KULME').AsString;
		end;
		QrLabel61.Caption 	:= Format('%.0f',[QuerySor.FieldByName('SS_EGYAR').AsFloat]);
		QrLabel34.Caption 	:= Format('%.0f',[jszamlasor.sornetto]);
		QrLabel36.Caption 	:= Format('%.0f',[JSzamlaSor.sorafa]);
		QrLabel32.Caption 	:= EgyebDlg.Read_SZGrid('101', QuerySor.FieldByName('SS_AFAKOD').AsString );
		QrLabel35.Caption 	:= Format('%.0f',[JSzamlaSor.sornetto+JSzamlaSor.sorafa]);

		{A valut�s �rt�kek megad�sa}
		if ( ( not kellvnem ) and ( tenyvnem <> 'HUF' ) ) then begin
			QrLabel72.Caption := '';
			QrLabel73.Caption := '';
		end else begin
			QrLabel72.Caption := Format('%f %3s',[JSzamlaSor.sorbruteur, JSzamlaSor.valutanem]);
			QrLabel73.Caption := Format('%9.4f ',[JSzamlaSor.eurarfertek]);
		end;
	end;
end;

procedure TSzamla_DK_HUFForm.QRBand2BeforePrint(Sender: TQRCustomBand;
	   var PrintBand: Boolean);
begin
	QrLabel62.Caption 	:= EgyebDlg.Read_SZGrid('210','125');
	QrLabel38.Caption 	:= EgyebDlg.Read_SZGrid('210','131');
	QrLabel39.Caption 	:= EgyebDlg.Read_SZGrid('210','126');
	QrLabel44.Caption 	:= EgyebDlg.Read_SZGrid('210','127');
	QrLabel83.Caption 	:= EgyebDlg.Read_SZGrid('210','128');
	QrLabel43.Caption 	:= EgyebDlg.Read_SZGrid('210','129');
	QrLabel42.Caption 	:= EgyebDlg.Read_SZGrid('210','130');
	QrLabel40.Caption 	:= EgyebDlg.Read_SZGrid('210','132');
	QrLabel41.Caption 	:= EgyebDlg.Read_SZGrid('210','133');
	QrLabel45.Caption 	:= EgyebDlg.Read_SZGrid('210','134');
	QrLabel3.Caption   	:= EgyebDlg.Read_SZGrid('210','135');
	if ( JSzamla.GetAfaSorszam('101') > 0 ) then begin
		QrLabel42.Caption := '25'+copy(QrLabel42.Caption,3,99);
		QrLabel41.Caption := '25'+copy(QrLabel41.Caption,3,99);
	end;
//	if QueryFej.FieldByName('SA_KIDAT').AsString >= '2009.07.01.' then begin
//		QrLabel42.Caption := '25'+copy(QrLabel42.Caption,3,99);
//		QrLabel41.Caption := '25'+copy(QrLabel41.Caption,3,99);
//	end;
	if QueryFej.FieldByName('SA_KIDAT').AsString >= '2012.01.01.' then begin
		QrLabel42.Caption := '27'+copy(QrLabel42.Caption,3,99);
		QrLabel41.Caption := '27'+copy(QrLabel41.Caption,3,99);
	end;
	if ( JSzamla.GetAfaSorszam('110') > 0 ) then begin	// Van az �FA T�bl�zatban 20 %-os �FA
		QrLabel42.Caption := '20'+copy(QrLabel42.Caption,3,99);
		QrLabel41.Caption := '20'+copy(QrLabel41.Caption,3,99);
	end;
	if ( JSzamla.GetAfaSorszam(JSzamla.afakod27) > 0 ) then begin
		QrLabel42.Caption := '27'+copy(QrLabel42.Caption,3,99);
		QrLabel41.Caption := '27'+copy(QrLabel41.Caption,3,99);
	end;

	LabelO1.Caption   	:= Format('%.0f Ft',[JSzamla.ossz_netto]);
	QrLabel47.Caption 	:= Format('%.0f Ft',[( JSzamla.Afasorok[JSzamla.GetAfaSorszam('102')] as TJAfasor ).afaertek]);
	QrLabel49.Caption 	:= Format('%.0f Ft',[( JSzamla.Afasorok[JSzamla.GetAfaSorszam('110')] as TJAfasor ).afaertek ]);
	QrLabel50.Caption 	:= Format('%.0f Ft',[ JSzamla.ossz_brutto]);
	QrLabel51.Caption 	:= Format('%.0f Ft',[( JSzamla.Afasorok[JSzamla.GetAfaSorszam('102')] as TJAfasor ).afaalap]);
	QrLabel52.Caption 	:= Format('%.0f Ft',[( JSzamla.Afasorok[JSzamla.GetAfaSorszam('110')] as TJAfasor ).afaalap]);
	QrLabel53.Caption 	:= Format('%.0f Ft',[( JSzamla.Afasorok[JSzamla.GetAfaSorszam('103')] as TJAfasor ).afaalap]);
	QrLabel54.Caption 	:= Format('%.0f Ft',[( JSzamla.Afasorok[JSzamla.GetAfaSorszam('104')] as TJAfasor ).afaalap]);
	QrLabel84.Caption 	:= Format('%.0f Ft',[( JSzamla.Afasorok[JSzamla.GetAfaSorszam('105')] as TJAfasor ).afaalap]);
	QrLabel48.Caption 	:= SzamText(Format('%.0f',[JSzamla.ossz_brutto]))+' forint / 00';
	if ( JSzamla.GetAfaSorszam('101') > 0 ) then begin    // Van 25% -os �FA
		QrLabel49.Caption := Format('%.0f Ft',[( JSzamla.Afasorok[JSzamla.GetAfaSorszam('101')] as TJAfasor ).afaertek]);
		QrLabel52.Caption := Format('%.0f Ft',[( JSzamla.Afasorok[JSzamla.GetAfaSorszam('101')] as TJAfasor ).afaalap]);
	end;
	if ( JSzamla.GetAfaSorszam(JSzamla.afakod27) > 0 ) then begin    // Van 25% -os �FA
		QrLabel49.Caption := Format('%.0f Ft',[( JSzamla.Afasorok[JSzamla.GetAfaSorszam(JSzamla.afakod27)] as TJAfasor ).afaertek]);
		QrLabel52.Caption := Format('%.0f Ft',[( JSzamla.Afasorok[JSzamla.GetAfaSorszam(JSzamla.afakod27)] as TJAfasor ).afaalap]);
	end;
	QrLabel4.Caption  	:= QueryFej.FieldByName('SA_MELL1').AsString;
	QrLabel5.Caption  	:= QueryFej.FieldByName('SA_MELL2').AsString;
	QrLabel59.Caption 	:= QueryFej.FieldByName('SA_MELL3').AsString;
	QrLabel60.Caption 	:= QueryFej.FieldByName('SA_MELL4').AsString;
	QrLabel65.Caption 	:= QueryFej.FieldByName('SA_MELL5').AsString;
	QrLabel103.Caption 	:= QueryFej.FieldByName('SA_MELL6').AsString;
	QrLabel104.Caption 	:= QueryFej.FieldByName('SA_MELL7').AsString;
	QE1.Caption 		:= Format('%.2f',[( JSzamla.Afasorok[JSzamla.GetAfaSorszam('103')] as TJAfasor ).afaaleur]);
	QE2.Caption 		:= Format('%.2f',[( JSzamla.Afasorok[JSzamla.GetAfaSorszam('104')] as TJAfasor ).afaaleur]);
	QE3.Caption 		:= Format('%.2f',[( JSzamla.Afasorok[JSzamla.GetAfaSorszam('105')] as TJAfasor ).afaaleur]);
	QE4.Caption 		:= Format('%.2f',[( JSzamla.Afasorok[JSzamla.GetAfaSorszam('102')] as TJAfasor ).afaaleur]);
	QE5.Caption 		:= Format('%.2f',[( JSzamla.Afasorok[JSzamla.GetAfaSorszam('110')] as TJAfasor ).afaaleur]);
	QE6.Caption 		:= Format('%.2f',[( JSzamla.Afasorok[JSzamla.GetAfaSorszam('102')] as TJAfasor ).afaerteur]);
	QE7.Caption 		:= Format('%.2f',[( JSzamla.Afasorok[JSzamla.GetAfaSorszam('110')] as TJAfasor ).afaerteur]);
	if ( JSzamla.GetAfaSorszam('101') > 0 ) then begin
		QE5.Caption := Format('%.2f',[( JSzamla.Afasorok[JSzamla.GetAfaSorszam('101')] as TJAfasor ).afaaleur]);
		QE7.Caption := Format('%.2f',[( JSzamla.Afasorok[JSzamla.GetAfaSorszam('101')] as TJAfasor ).afaerteur]);
	end;
	if ( JSzamla.GetAfaSorszam(JSzamla.afakod27) > 0 ) then begin
		QE5.Caption := Format('%.2f',[( JSzamla.Afasorok[JSzamla.GetAfaSorszam(JSzamla.afakod27)] as TJAfasor ).afaaleur]);
		QE7.Caption := Format('%.2f',[( JSzamla.Afasorok[JSzamla.GetAfaSorszam(JSzamla.afakod27)] as TJAfasor ).afaerteur]);
	end;

	{A valut�s �rt�k ki�r�sa}
	if not kellvnem then begin
		QrLabel78.Caption 	:= '';
		QrLabel79.Caption 	:= '';
		QrLabel102.Caption 	:= '';
		QE1.Caption 		:= '';
		QE2.Caption 		:= '';
		QE3.Caption 		:= '';
		QE4.Caption 		:= '';
		QE5.Caption 		:= '';
		QE6.Caption 		:= '';
		QE7.Caption 		:= '';
	end else begin
		QrLabel78.Caption 	:= EgyebDlg.Read_SZGrid('210','136')+' '+tenyvnem+' : ';
		if QueryFej.FieldByName('SA_NEMET').AsString = '1' then begin
			QrLabel78.Caption 	:= EgyebDlg.Read_SZGrid('210','150')+' '+tenyvnem+' : ';
		end;
		QrLabel79.Caption 	:= Format('%.2f %3s',[JSzamla.ossz_bruttoeur, tenyvnem]);
		if tenyvnem = 'EUR' then begin
			QrLabel102.Caption 	:= SzamText(Format('%.0f',[Int(JSzamla.ossz_bruttoeur)]))+' eur� / '+
				Format('%2.2d',[Round(JSzamla.ossz_bruttoeur*100) mod 100]);
		end else begin
			QrLabel102.Caption 	:= SzamText(Format('%.0f',[Int(JSzamla.ossz_bruttoeur)]))+' '+tenyvnem+' / '+
				Format('%2.2d',[Round(JSzamla.ossz_bruttoeur*100) mod 100]);
		end;
		QE1.Caption 		:= '('+QE1.Caption+')';
		QE2.Caption 		:= '('+QE2.Caption+')';
		QE3.Caption 		:= '('+QE3.Caption+')';
		QE4.Caption 		:= '('+QE4.Caption+')';
		QE5.Caption 		:= '('+QE5.Caption+')';
		QE6.Caption 		:= '('+QE6.Caption+')';
		QE7.Caption 		:= '('+QE7.Caption+')';
	end;
	if tenyvnem	= 'HUF' then begin
		QrLabel78.Caption 	:= EgyebDlg.Read_SZGrid('210','136')+' EUR : ';
		if QueryFej.FieldByName('SA_NEMET').AsString = '1' then begin
			QrLabel78.Caption 	:= EgyebDlg.Read_SZGrid('210','150')+' EUR : ';
		end;
		QrLabel79.Caption 	:= Format('%.2f %3s',[JSzamla.ossz_bruttoeur, 'EUR']);
		QrLabel102.Caption 	:= SzamText(Format('%.0f',[JSzamla.ossz_bruttoeur]))+' eur� / '+
			Format('%2.2d',[Round(JSzamla.ossz_bruttoeur*100) mod 100]);
		QE1.Caption 		:= '('+Format('%.2f',[( JSzamla.Afasorok[JSzamla.GetAfaSorszam('103')] as TJAfasor ).afaaleur])+')';
		QE2.Caption 		:= '('+Format('%.2f',[( JSzamla.Afasorok[JSzamla.GetAfaSorszam('104')] as TJAfasor ).afaaleur])+')';
		QE3.Caption 		:= '('+Format('%.2f',[( JSzamla.Afasorok[JSzamla.GetAfaSorszam('105')] as TJAfasor ).afaaleur])+')';
		QE4.Caption 		:= '('+Format('%.2f',[( JSzamla.Afasorok[JSzamla.GetAfaSorszam('102')] as TJAfasor ).afaaleur])+')';
		QE5.Caption 		:= '('+Format('%.2f',[( JSzamla.Afasorok[JSzamla.GetAfaSorszam('110')] as TJAfasor ).afaaleur])+')';
		QE6.Caption 		:= '('+Format('%.2f',[( JSzamla.Afasorok[JSzamla.GetAfaSorszam('102')] as TJAfasor ).afaerteur])+')';
		QE7.Caption 		:= '('+Format('%.2f',[( JSzamla.Afasorok[JSzamla.GetAfaSorszam('110')] as TJAfasor ).afaerteur])+')';
		if ( JSzamla.GetAfaSorszam('101') > 0 ) then begin
			QE5.Caption := '('+Format('%.2f',[( JSzamla.Afasorok[JSzamla.GetAfaSorszam('101')] as TJAfasor ).afaaleur])+')';
			QE7.Caption := '('+Format('%.2f',[( JSzamla.Afasorok[JSzamla.GetAfaSorszam('101')] as TJAfasor ).afaerteur])+')';
		end;
		if ( JSzamla.GetAfaSorszam(JSzamla.afakod27) > 0 ) then begin
			QE5.Caption := '('+Format('%.2f',[( JSzamla.Afasorok[JSzamla.GetAfaSorszam(JSzamla.afakod27)] as TJAfasor ).afaaleur])+')';
			QE7.Caption := '('+Format('%.2f',[( JSzamla.Afasorok[JSzamla.GetAfaSorszam(JSzamla.afakod27)] as TJAfasor ).afaerteur])+')';
		end;
	end;

	{A k�lf�ldi cimk�k beolvas�sa}
	if QueryFej.FieldByName('SA_NEMET').AsString = '1' then begin
		 // N�met fejl�c eset�n a valut�s �rt�k van bold-al
		 QrLabel45.Font.Style   := QrLabel51.Font.Style;  // Normal
		 QrLabel50.Font.Style   := QrLabel51.Font.Style;
		 QrLabel78.Font.Style   := QrLabel40.Font.Style;  // Bold
		 QrLabel79.Font.Style   := QrLabel40.Font.Style;
		 QrLabel45.Font.Size    := 10;
		 QrLabel50.Font.Size    := 10;
		 QrLabel78.Font.Size    := 12;
//        QrLabel79.Font.Size    := 12;
	end else begin
        QrLabel45.Font.Style   := QrLabel40.Font.Style;
        QrLabel50.Font.Style   := QrLabel40.Font.Style;
        QrLabel78.Font.Style   := QrLabel51.Font.Style;
        QrLabel79.Font.Style   := QrLabel51.Font.Style;
        QrLabel45.Font.Size    := 12;
        QrLabel50.Font.Size    := 12;
        QrLabel78.Font.Size    := 10;
//        QrLabel79.Font.Size    := 10;
	end;

end;

procedure TSzamla_DK_HUFForm.RepBeforePrint(Sender: TCustomQuickRep;
      var PrintReport: Boolean);
begin
	oldalszam		:= 1;
end;

procedure TSzamla_DK_HUFForm.FormDestroy(Sender: TObject);
begin
	JSzamla.Destroy
end;

end.
