unit Telefonbe2;

interface

uses
	SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
	Forms, Dialogs, StdCtrls, Buttons, Mask, DB, DBTables, J_ALFORM, ADODB,
  Vcl.Grids, Vcl.ExtCtrls, Vcl.ComCtrls, Vcl.TabNotBk;

type
	TTelefonbe2Dlg = class(TJ_AlformDlg)
	BitElkuld: TBitBtn;
	BitKilep: TBitBtn;
    Label3: TLabel;
    Label6: TLabel;
    M5: TMaskEdit;
    M6: TMaskEdit;
    BitBtn1: TBitBtn;
    Label1: TLabel;
    Label2: TLabel;
    M1: TMaskEdit;
	 Query1: TADOQuery;
	 Query2: TADOQuery;
	 BitBtn37: TBitBtn;
	 Label9: TLabel;
    meKiadas: TMaskEdit;
    BitBtn8: TBitBtn;
    meMegjegyzes: TMemo;
    Label7: TLabel;
    meIMEI: TMaskEdit;
    cbTipus: TComboBox;
    Label10: TLabel;
    cbStatusz: TComboBox;
    chkTech: TCheckBox;
    meELOFIZKOD: TMaskEdit;
    TabbedNotebook1: TTabbedNotebook;
    Panel6: TPanel;
    Panel5: TPanel;
    BitBtn10: TBitBtn;
    BitBtn12: TBitBtn;
    SGElofizetes: TStringGrid;
    BitBtnTelKieg: TBitBtn;
    Label4: TLabel;
    meMDM: TMaskEdit;
	 procedure BitKilepClick(Sender: TObject);
	 procedure FormCreate(Sender: TObject);
	 procedure BitElkuldClick(Sender: TObject);
	 procedure MaskEditClick(Sender: TObject);
	 procedure FormKeyDown(Sender: TObject; var Key: Word;
	   Shift: TShiftState);
	 procedure Tolto(cim, kod : string); override;
	 procedure BitBtn1Click(Sender: TObject);
	 procedure BitBtn37Click(Sender: TObject);
    procedure BitBtn8Click(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure BitBtn10Click(Sender: TObject);
    procedure BitBtn12Click(Sender: TObject);
    procedure M6Click(Sender: TObject);
    procedure BitBtnTelKiegClick(Sender: TObject);
    procedure M5Change(Sender: TObject);

	private
    lista_tipus, lista_statusz: TStringList;
    procedure ElofizetesTablaToltes;
    procedure Mentes;
	public
	end;

const
  SG_Elofizeteskod_oszlop = 5;
var
	Telefonbe2Dlg: TTelefonbe2Dlg;

implementation

uses
 Egyeb, J_SQL, Kozos, J_VALASZTO, TelkiegCsoportosbe;

{$R *.DFM}

procedure TTelefonbe2Dlg.BitKilepClick(Sender: TObject);
begin
	ret_kod := '';
	Close;
end;

procedure TTelefonbe2Dlg.FormCreate(Sender: TObject);
var
  i: integer;
begin
	EgyebDlg.SeTADOQueryDatabase(Query1);
	EgyebDlg.SeTADOQueryDatabase(Query2);
  lista_statusz	:= TStringList.Create;
  lista_tipus	:= TStringList.Create;
  SzotarTolt(CBStatusz, '144' , lista_statusz, false, false, true );
  SzotarTolt(CBTipus, '146' , lista_tipus, false, false, true );
	CBStatusz.ItemIndex  := 1;  // Akt�v
  CBTipus.ItemIndex  := lista_tipus.IndexOf(LegutobbFelvittTelefonTipus);

  SGElofizetes.Cells[0,0]	:= 'Telefonsz�m';
  // SGElofizetes.Cells[1,0]	:= 'Mobilnet';
  // SGElofizetes.Cells[2,0]	:= 'Roaming';
  SGElofizetes.Cells[3,0]	:= 'H�v�ssz�r�s';
  SGElofizetes.Cells[4,0]	:= 'Sz�mlacsoport';
  SGElofizetes.Cells[5,0]	:= 'TEID'; // el�fizet�s k�d
  SGElofizetes.ColWidths[0]	:= 100;
  SGElofizetes.ColWidths[1]	:= 0;
  SGElofizetes.ColWidths[2]	:= 0;
  SGElofizetes.ColWidths[3]	:= 80;
  SGElofizetes.ColWidths[4]	:= 120;
  SGElofizetes.ColWidths[5]	:= 0;  // elrejt

	ret_kod:= '';
end;

procedure TTelefonbe2Dlg.FormDestroy(Sender: TObject);
begin
  if lista_statusz <> nil then lista_statusz.Free;
  if lista_tipus <> nil then lista_tipus.Free;
end;

procedure TTelefonbe2Dlg.Tolto(cim, kod : string);
var
  S: string;
begin
	SetMaskEdits([M1, M5, M6]);
	Caption 		:= cim;
	ret_kod  := kod;
	M1.Text		:= ret_kod;
	if ret_kod <> '' then begin
		if Query_Run (Query1, 'SELECT * FROM TELKESZULEK WHERE TK_ID = '+ret_kod ,true) then begin
      ComboSet (cbStatusz, Trim(Query1.FieldByName('TK_STATUSKOD').AsString), lista_statusz);
      ComboSet (cbTipus, Trim(Query1.FieldByName('TK_TIPUSKOD').AsString), lista_tipus);
			meIMEI.Text		:= Query1.FieldByname('TK_IMEI').AsString;
      meMDM.Text		:= Query1.FieldByname('TK_MDM').AsString;
			M5.Text	:= Query1.FieldByname('TK_DOKOD').AsString;
			// meAppleID.Text		:= Query1.FieldByname('TK_APPLEID').AsString;
			// meAppleJelszo.Text		:= Query1.FieldByname('TK_APPLEJELSZO').AsString;
			meKiadas.Text		:= Query1.FieldByname('TK_KIADDAT').AsString;
			meMegjegyzes.Text	:= Query1.FieldByname('TK_MEGJE').AsString;
			M6.Text		:= Query_Select('DOLGOZO', 'DO_KOD', M5.Text, 'DO_NAME');
			chkTech.Checked	:= Query1.FieldByName('TK_TECH').AsInteger > 0;
  		end;
    // --- az extra t�bl�zatok felt�lt�se
    ElofizetesTablaToltes;
	end;
end;

procedure TTelefonbe2Dlg.ElofizetesTablaToltes;
const
  IgenString = 'Igen';
  NemString = '-';
var
	i : integer;
  S, Hivasszures, Roaming: string;
begin
   // A telefonsz�mok felt�lt�se
   SGElofizetes.RowCount	:= 2;
   SGElofizetes.Rows[1].Clear;
   BitBtn12.Enabled	:= false;

   S:= 'select TE_TELSZAM, sz.SZ_MENEV TEL_SZAMLACSOP, TE_HIVSZUR, TE_ID '+
      ' from SZOTAR sz, TELELOFIZETES e '+
      ' where sz.SZ_ALKOD = TE_SZAMLACSOPKOD and sz.SZ_FOKOD=145 '+
      ' and TE_STATUSKOD = 1 '+
      ' and TE_TKID= '+ret_kod;
   S:=S+' ORDER BY TE_TELSZAM';

   if Query_Run (Query1, S,true) then begin
       if Query1.RecordCount > 0 then with Query1 do begin
           BitBtn12.Enabled		:= true;
           SGElofizetes.RowCount	:= RecordCount + 1;
           i := 1;
           while not Eof do begin
               // if FieldByName('TE_ROAMING').AsInteger=1 then Roaming:=IgenString else Roaming:= NemString;
               if FieldByName('TE_HIVSZUR').AsInteger=1 then Hivasszures:=IgenString else Hivasszures:= NemString;
               SGElofizetes.Cells[0, i]	:= FieldByName('TE_TELSZAM').AsString;
               SGElofizetes.Cells[1, i]	:= ''; // FieldByName('MOBILNET').AsString;
	      			 SGElofizetes.Cells[2, i]	:= ''; // Roaming;
	      			 SGElofizetes.Cells[3, i]	:= Hivasszures;
               SGElofizetes.Cells[4, i]	:= FieldByName('TEL_SZAMLACSOP').AsString;
               SGElofizetes.Cells[5, i]	:= FieldByName('TE_ID').AsString;
               Next;
               Inc(i);
           end;
       end;
   end;
end;

procedure TTelefonbe2Dlg.BitElkuldClick(Sender: TObject);
begin
  Mentes;
	Close;
end;

procedure TTelefonbe2Dlg.Mentes;
var
  UjTkID, i: integer;
  DarabS: string;
begin

	if cbTipus.ItemIndex = -1 then begin
		NoticeKi('Telefon t�pus megad�sa k�telez�!');
		cbTipus.Setfocus;
		Exit;
	end;

	if ret_kod = '' then begin  // �j telefon
    UjTkID:= Query_Insert_Identity (Query1, 'TELKESZULEK',
      [
      'TK_STATUSKOD',  ''''+lista_statusz[cbStatusz.ItemIndex]+'''',
      'TK_TIPUSKOD', ''''+lista_tipus[cbTipus.ItemIndex]+'''',
      'TK_IMEI', ''''+meIMEI.Text+'''',
      'TK_MDM', ''''+meMDM.Text+'''',
      // ezt majd k�l�n �ll�tjuk
      // 'TK_DOKOD', ''''+M5.Text+'''',
      'TK_TECH', BoolToIntStr(chkTech.Checked),
      // 'TK_APPLEID', ''''+meAppleID.Text+'''',
      // 'TK_APPLEJELSZO', ''''+meAppleJelszo.Text+'''',
      'TK_KIADDAT', ''''+meKiadas.Text+'''',
      'TK_MEGJE', ''''+meMegjegyzes.Text+''''
      ], 'TK_ID' );
     if UjTkID>=0 then begin  // ha nem volt sikeres, �gyis kap SQL hiba ablakot
      ret_kod:= IntToStr(UjTkID);
      M1.Text:= ret_kod;
      end;
     end
  else begin
     Query_Update (Query1, 'TELKESZULEK',
      [
      'TK_STATUSKOD',  ''''+lista_statusz[cbStatusz.ItemIndex]+'''',
      'TK_TIPUSKOD', ''''+lista_tipus[cbTipus.ItemIndex]+'''',
      'TK_IMEI', ''''+meIMEI.Text+'''',
      'TK_MDM', ''''+meMDM.Text+'''',
      // ezt majd k�l�n �ll�tjuk
      // 'TK_DOKOD', ''''+M5.Text+'''',
      'TK_TECH', BoolToIntStr(chkTech.Checked),
      // 'TK_APPLEID', ''''+meAppleID.Text+'''',
      // 'TK_APPLEJELSZO', ''''+meAppleJelszo.Text+'''',
      'TK_KIADDAT', ''''+meKiadas.Text+'''',
      'TK_MEGJE', ''''+meMegjegyzes.Text+''''
      ], ' WHERE TK_ID='+ret_kod);
      end;  // else
  SetTelefonDolgozo(ret_kod, M5.Text);
end;

procedure TTelefonbe2Dlg.M5Change(Sender: TObject);
begin
  if M5.text= '' then BitBtnTelKieg.Enabled:= False
  else BitBtnTelKieg.Enabled:= True;
end;

procedure TTelefonbe2Dlg.M6Click(Sender: TObject);
begin
   EgyebDlg.DolgozoFotoMutato(M5.Text, M6);
end;

procedure TTelefonbe2Dlg.MaskEditClick(Sender: TObject);
begin
	with Sender as TMaskEdit do begin
  	SelectAll;
  end;
end;

procedure TTelefonbe2Dlg.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 // Nagyp 2016-08-03
 // EgyebDlg.FormReturn(Key);
end;

procedure TTelefonbe2Dlg.BitBtn10Click(Sender: TObject);
var
  ElofizetesTelefonja: string;
begin
  if M1.text = '' then
    Mentes;    // hogy legyen azonos�t�ja
  meELOFIZKOD.text:= '';
  TelElofizetValaszto2(meELOFIZKOD, True);
  ElofizetesTelefonja:=Query_Select('TELELOFIZETES', 'TE_ID', meELOFIZKOD.Text, 'TE_TKID');
  if (ElofizetesTelefonja <> '') and (ElofizetesTelefonja <> '0') then begin
    if NoticeKi('A kiv�lasztott el�fizet�s egy m�sik telefonk�sz�l�khez tartozik. Val�ban t�r�lni k�v�nja a m�sik �sszerendel�st?', NOT_QUESTION) <> 0 then begin
     	Exit;
      end;
    end;
  SetElofizetesTelefon(meELOFIZKOD.Text, M1.text);  // a kiv�lasztott el�fizet�s
  ElofizetesTablaToltes;  // a t�bl�zat friss�t�s
end;

procedure TTelefonbe2Dlg.BitBtn12Click(Sender: TObject);
var
	i : integer;
  TEID: string;
begin
   if NoticeKi('Val�ban t�r�lni k�v�nja a kijel�lt �sszerendel�st?', NOT_QUESTION) <> 0 then begin
     	Exit;
      end;
   TEID:= SGElofizetes.Cells[SG_Elofizeteskod_oszlop, SGElofizetes.Row];
   if TEID <> '' then begin  	// Ha ki van t�ltve az el�fizet�s ID
  		SetElofizetesTelefon (TEID, '');
      end;

   	// A sor t�rl�se
    GridTorles(SGElofizetes, SGElofizetes.Row);
   	if (SGElofizetes.Cells[SG_Elofizeteskod_oszlop ,SGElofizetes.Row] = '') then begin
       	BitBtn12.Enabled	:= false;
       end;
end;

procedure TTelefonbe2Dlg.BitBtn1Click(Sender: TObject);
var
	sofkod	: string;
begin
	{Dolgoz� v�laszt�s}
	sofkod	:= M5.Text;
	DolgozoValaszto(M5, M6 );
  meMegjegyzes.SetFocus;
end;

procedure TTelefonbe2Dlg.BitBtnTelKiegClick(Sender: TObject);
begin
  if M5.Text = '' then begin
		NoticeKi('Nincs dolgoz� megadva! Tartoz�kot csak dolgoz�hoz tudunk hozz�rendelni.');
		Exit;
    end; // if
  Application.CreateForm(TTelkiegCsoportosbeDlg, TelkiegCsoportosbeDlg);
  TelkiegCsoportosbeDlg.Tolto(M5.Text);  // DOKOD
  TelkiegCsoportosbeDlg.ShowModal;
  TelkiegCsoportosbeDlg.Free;

end;

procedure TTelefonbe2Dlg.BitBtn37Click(Sender: TObject);
begin
	M5.Text	:= '';
	M6.Text	:= '';
end;

procedure TTelefonbe2Dlg.BitBtn8Click(Sender: TObject);
begin
	EgyebDlg.Calendarbe(meKiadas);
end;

end.







