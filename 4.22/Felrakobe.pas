unit Felrakobe;

interface

uses
	SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
	Forms, Dialogs, StdCtrls, Buttons, Mask, DB, DBTables, Egyeb, Kozos, J_SQL,
  	ADODB, J_ALFORM;
	
type
	TFelrakobeDlg = class(TJ_AlformDlg)
	BitElkuld: TBitBtn;
	BitKilep: TBitBtn;
	Label1: TLabel;
    M1: TMaskEdit;
    M2: TMaskEdit;
    QueryKoz: TADOQuery;
    Label4: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    Label22: TLabel;
    M2AA: TComboBox;
    M2A: TMaskEdit;
    M3: TMaskEdit;
    M4: TMaskEdit;
    M11: TMaskEdit;
    M12: TMaskEdit;
    M13: TMaskEdit;
    M14: TMaskEdit;
    M15: TMaskEdit;
    GroupBox1: TGroupBox;
    Label8: TLabel;
    Label15: TLabel;
    Label7: TLabel;
    Label14: TLabel;
    Label50: TLabel;
    AdoOrszagkodCombo: TComboBox;
    meAdoszam: TMaskEdit;
    meKapcsolat: TMaskEdit;
    meTelefon: TMaskEdit;
    meEmail: TMaskEdit;
    meMobil: TMaskEdit;
	procedure BitKilepClick(Sender: TObject);
	procedure FormCreate(Sender: TObject);
	procedure Tolto(cim : string; teko : string); override;
	procedure BitElkuldClick(Sender: TObject);
	 procedure FormKeyDown(Sender: TObject; var Key: Word;
	   Shift: TShiftState);
	 procedure FormDestroy(Sender: TObject);
	private
		listaorsz	: TStringList;
    orszagkodlist	: TStringList;
	public
	end;

var
	FelrakobeDlg: TFelrakobeDlg;

implementation

uses
	Felrakofm;
{$R *.DFM}

procedure TFelrakobeDlg.BitKilepClick(Sender: TObject);
begin
	ret_kod := '';
	Close;
end;

procedure TFelrakobeDlg.FormCreate(Sender: TObject);
begin
	EgyebDlg.SetADOQueryDatabase(QueryKoz);
	listaorsz	:= TStringList.Create;
  orszagkodlist := TStringList.Create;
	SzotarTolt(M2AA, '300' , listaorsz, true, true );
 	SzotarTolt(AdoOrszagkodCombo,  '300' , orszagkodlist, true );  // a k�dokat tessz�k bele
	SetMaskEdits([M1]);
end;

procedure TFelrakobeDlg.Tolto(cim : string; teko : string);
begin
	ret_kod 	:= teko;
	Caption 	:= cim;
  	M1.Text		:= teko;
  	if ret_kod <> '' then begin
   	  Query_Run(QueryKoz, 'SELECT * FROM FELRAKO WHERE FF_FEKOD = '+ret_kod);
      if QueryKoz.RecordCount > 0 then begin
         	M2.Text 	:= QueryKoz.FieldByName('FF_FELNEV').AsString;
          M2A.Text 	:= QueryKoz.FieldByName('FF_FELIR').AsString;
          M3.Text 	:= QueryKoz.FieldByName('FF_FELTEL').AsString;
          M4.Text 	:= QueryKoz.FieldByName('FF_FELCIM').AsString;
          M11.Text 	:= QueryKoz.FieldByName('FF_FELSE1').AsString;
          M12.Text 	:= QueryKoz.FieldByName('FF_FELSE2').AsString;
          M13.Text 	:= QueryKoz.FieldByName('FF_FELSE3').AsString;
          M14.Text 	:= QueryKoz.FieldByName('FF_FELSE4').AsString;
          M15.Text 	:= QueryKoz.FieldByName('FF_FELSE5').AsString;
          ComboSet (M2AA, Querykoz.FieldByName('FF_ORSZA').AsString, listaorsz);

          // f�lre�rt�s volt
          // meAdoszam.Text 	:= QueryKoz.FieldByName('FF_ADOSZ').AsString;
          // meKapcsolat.Text 	:= QueryKoz.FieldByName('FF_UGYINT').AsString;
          // meTelefon.Text 	:= QueryKoz.FieldByName('FF_TEL').AsString;
          // meMobil.Text 	:= QueryKoz.FieldByName('FF_MOBIL').AsString;
          // meEmail.Text 	:= QueryKoz.FieldByName('FF_EMAIL').AsString;
          // ComboSet (AdoOrszagkodCombo, QueryKoz.FieldByName('FF_AORSZ').AsString, orszagkodlist);
          end;  // if
	    end;  // if
end;

procedure TFelrakobeDlg.BitElkuldClick(Sender: TObject);
var
  FelrakoInfo, S: string;
  Ujkod: integer;
begin
  S:='rtrim(FF_ORSZA)+'' ''+rtrim(FF_FELIR)+'' ''+rtrim(FF_FELTEL)+'' ''+rtrim(FF_FELCIM)+'' '''+
      '+rtrim(FF_FELSE1)+'' ''+rtrim(FF_FELSE2)+'' ''+rtrim(FF_FELSE3)+'' ''+rtrim(FF_FELSE4)+'' ''+rtrim(FF_FELSE5)';
  FelrakoInfo:= trim(Query_select('FELRAKO', 'FF_FELNEV', M2.Text, S));
	if (ret_kod = '') and (FelrakoInfo <> '') then begin
      NoticeKi('Ilyen n�vvel m�r l�tezik felrak� ('+FelrakoInfo+')! Ha elt�r� c�mmel szeretn�l �j felrak�t r�gz�teni, k�rlek adj meg m�sik nevet!');
      Exit;
      end;
  Ujkod:= FelrakoElment(M2.Text, M2AA.Text, M2A.Text, M3.Text, M4.Text, M11.Text, M12.Text, M13.Text, M14.Text, M15.Text, ret_kod, True);

  {Query_Update (EgyebDlg.QueryAlap, 'FELRAKO',
          [
          'FF_ADOSZ', 	''''+meAdoszam.Text+'''',
          'FF_UGYINT', 	''''+meKapcsolat.Text+'''',
          'FF_TEL', 	''''+meTelefon.Text+'''',
          'FF_MOBIL', 	''''+meMobil.Text+'''',
          'FF_EMAIL', 	''''+meEmail.Text+'''',
          'FF_AORSZ', 	''''+orszagkodlist[AdoOrszagkodCombo.ItemIndex]+''''
          ], '  WHERE FF_FEKOD = '+ IntToStr(Ujkod) );
          }
  Close;
end;

procedure TFelrakobeDlg.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
	EgyebDlg.FormReturn(Key);
end;

procedure TFelrakobeDlg.FormDestroy(Sender: TObject);
begin
	if listaorsz <> nil then listaorsz.Free;
  if orszagkodlist <> nil then orszagkodlist.Free;
end;

end.
