unit Notice;

interface

uses WinTypes, WinProcs, Classes, Graphics, Forms, Controls, Buttons,
  StdCtrls, ExtCtrls, messages, Sysutils;

const
  WM_MYMEMO_ENTER = WM_USER + 500;

type
  TNoticeDlg = class(TForm)
    BtnOk: TBitBtn;
    BtnYes: TBitBtn;
    BtnNo: TBitBtn;
    Image1: TImage;
    Image2: TImage;
    Image3: TImage;
    ScrollBox1: TScrollBox;
    Label1: TLabel;
    procedure BtnOkClick(Sender: TObject);
    procedure Init(szoveg: string; tipus: integer);
    procedure BtnYesClick(Sender: TObject);
    procedure BtnNoClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
  private
    shapek: array [1 .. 32] of TShape;
  public
    ret_code: integer;
  end;

var
  NoticeDlg: TNoticeDlg;

implementation

uses
  Kozos;
{$R *.DFM}

procedure TNoticeDlg.BtnOkClick(Sender: TObject);
begin
  ret_code := 1;
  Close;
end;

procedure TNoticeDlg.Init(szoveg: string; tipus: integer);
begin
  Image1.Hide;
  Image2.Hide;
  Image3.Hide;
  // Az ablak c�m�nek meghat�roz�sa + k�p megjelen�t�sa
  case tipus of
    NOT_QUESTION:
      begin
        Caption := 'K�rd�s';
        BtnOk.Hide;
        Image1.Show;
      end;
    NOT_MESSAGE:
      begin
        Caption := '�zenet';
        BtnYes.Hide;
        BtnNo.Hide;
        Image2.Show;
      end;
    NOT_ERROR:
      begin
        Caption := 'Hiba';
        BtnYes.Hide;
        BtnNo.Hide;
        Image3.Show;
      end;
    NOT_DIALOG:
      begin
        Caption := 'Figyelem';
        BtnYes.Hide;
        BtnNo.Hide;
        BtnOk.Hide;
        Image3.Hide;
        // Height := 2 * Panel1.Top + Panel1.Height;
        Height := 156;
      end;
  else
    begin
      Caption := 'Notice';
      BtnYes.Hide;
      BtnNo.Hide;
    end;
  end;

  // A sz�veg t�rdel�se ha '/' van benne + megjelen�t�s
  if Pos('/', szoveg) > 0 then
  begin
    szoveg := copy(szoveg, 1, Pos('/', szoveg) - 1) + #13 + #10 +
      copy(szoveg, Pos('/', szoveg) + 1, 999);
  end;
  Label1.Caption := szoveg;
  Update;
end;

procedure TNoticeDlg.BtnYesClick(Sender: TObject);
begin
  ret_code := 0;
  Close;
end;

procedure TNoticeDlg.BtnNoClick(Sender: TObject);
begin
  ret_code := -1;
  Close;
end;

procedure TNoticeDlg.FormCreate(Sender: TObject);
begin
  ret_code := 0;
  Caption := '';
end;

procedure TNoticeDlg.Timer1Timer(Sender: TObject);
begin
  // �tsz�nez�s
  shapek[1 + Random(31)].Brush.Color := Random(1000000);
end;

end.
