unit Telkiegtomegesbe;

interface

uses
	SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
	Forms, Dialogs, StdCtrls, Buttons, Mask, DB, DBTables, J_ALFORM, ADODB,
  Vcl.Grids, Vcl.ExtCtrls, Vcl.ComCtrls, Vcl.TabNotBk;

type
	TTelKiegTomegesBeDlg = class(TJ_AlformDlg)
	BitElkuld: TBitBtn;
	BitKilep: TBitBtn;
    Label3: TLabel;
    Label1: TLabel;
	 Query1: TADOQuery;
	 Query2: TADOQuery;
    meMegjegyzes: TMemo;
    cbTipus: TComboBox;
    Label10: TLabel;
    cbStatusz: TComboBox;
    Panel1: TPanel;
    meDarabszam: TMaskEdit;
    Label8: TLabel;
    pnlInfo: TPanel;
	 procedure BitKilepClick(Sender: TObject);
	 procedure FormCreate(Sender: TObject);
	 procedure BitElkuldClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure meDarabszamChange(Sender: TObject);
    procedure meDarabszamExit(Sender: TObject);

	private
    lista_tipus, lista_statusz: TStringList;
    procedure Mentes;
	public
	end;

var
	TelKiegTomegesBeDlg: TTelKiegTomegesBeDlg;

implementation

uses
 Egyeb, J_SQL, Kozos,  J_VALASZTO;

{$R *.DFM}

procedure TTelKiegTomegesBeDlg.BitKilepClick(Sender: TObject);
begin
	ret_kod := '';
	Close;
end;

procedure TTelKiegTomegesBeDlg.FormCreate(Sender: TObject);
var
  i: integer;
begin
	EgyebDlg.SeTADOQueryDatabase(Query1);
	EgyebDlg.SeTADOQueryDatabase(Query2);
  lista_statusz	:= TStringList.Create;
  lista_tipus	:= TStringList.Create;
  SzotarTolt(CBStatusz, '144' , lista_statusz, false, false, true );
  SzotarTolt(CBTipus, '153' , lista_tipus, false, false, true );
	CBStatusz.ItemIndex  := 1;  // Akt�v
  // CBTipus.ItemIndex  := lista_tipus.IndexOf(LegutobbFelvittTelefonTipus);
end;

procedure TTelKiegTomegesBeDlg.FormDestroy(Sender: TObject);
begin
  if lista_statusz <> nil then lista_statusz.Free;
  if lista_tipus <> nil then lista_tipus.Free;
end;


procedure TTelKiegTomegesBeDlg.BitElkuldClick(Sender: TObject);
begin
  Mentes;
end;

procedure TTelKiegTomegesBeDlg.Mentes;
var
  UjTIID, i, darab: integer;
  DarabS: string;
  Kilep: boolean;
begin

	if cbTipus.ItemIndex = -1 then begin
		NoticeKi('Kieg�sz�t� t�pus megad�sa k�telez�!');
		cbTipus.Setfocus;
		Exit;
	end;
  darab:= StrToInt(meDarabSzam.text);
  pnlInfo.Caption:= '';
  i:=1;
  Kilep:= False;
  while (i<= darab) and not Kilep do begin
    UjTIID:= Query_Insert_Identity (Query1, 'TELKIEG',
      [
      'TI_STATUSKOD',  ''''+lista_statusz[cbStatusz.ItemIndex]+'''',
      'TI_TIPUSKOD', ''''+lista_tipus[cbTipus.ItemIndex]+'''',
      'TI_MEGJE', ''''+meMegjegyzes.Text+''''
      ], 'TI_ID' );
     if UjTIID>=0 then begin  // ha nem volt sikeres, �gyis kap SQL hiba ablakot
        pnlInfo.Caption:='Tartoz�k r�gz�tve '+IntToStr(UjTIID)+' k�ddal ('+IntToStr(i)+'/'+IntToStr(darab)+')';
        pnlInfo.Refresh;
        end  // if
     else begin
        Kilep:= True;  // hiba volt, ne folytassuk a r�gz�t�st
        end;
     Inc(i);
     end;  // while
  pnlInfo.Caption:='Tartoz�kok r�gz�t�se befejez�d�tt.';
end;

procedure TTelKiegTomegesBeDlg.meDarabszamChange(Sender: TObject);
var
  S: string;
begin
  S:= meDarabszam.Text;
  if (S <> '') and JoEgesz(S) then
    BitElkuld.Caption:= meDarabszam.Text+ ' darab tartoz�k r�gz�t�se';
end;

procedure TTelKiegTomegesBeDlg.meDarabszamExit(Sender: TObject);
var
  S: string;
begin
  S:= Trim(meDarabszam.text);
  if not ((S='') or JoEgesz(S)) then begin
    NoticeKi('Csak eg�sz sz�m adhat� meg!');
    meDarabszam.SetFocus;
    end;
end;


end.







