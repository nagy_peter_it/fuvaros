unit Valszuro2;

interface

uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, DBTables, DB, StdCtrls, Buttons, Mask, DBCtrls,
  Printers, ExtCtrls, CheckLst, ADODB , J_FOFORMUJ;

type
  TValszuro2Dlg = class(TForm)
	 Query1: TADOQuery;
	 Label1: TLabel;
	 CheckBox1: TCheckBox;
	 CheckBox2: TCheckBox;
	 CheckBox3: TCheckBox;
	 CheckBox4: TCheckBox;
    Panel2: TPanel;
    Panel1: TPanel;
    BitElkuld: TBitBtn;
    BitKilep: TBitBtn;
	 procedure BitBtn6Click(Sender: TObject);
	 procedure BitElkuldClick(Sender: TObject);
	 procedure BitKilepClick(Sender: TObject);
	 procedure FormDestroy(Sender: TObject);
	 procedure FormCreate(Sender: TObject);
	 procedure Tolt(melist, melist2 : TStringList);
	 procedure FormResize(Sender: TObject);
	 private
		foselected		: boolean;
  public
		kodkilist   	: TStringList;
		voltenter		: boolean;
		fodial			: TJ_FOFORMUJDLG;
		mezo_sorsz		: integer;
		kelluresszures 	: boolean;
		mezolista		: TStringList;
		mezok			: array[1..10] of TLabel;
		negyzetek		: array[1..40] of TCheckBox;
  end;

var
  Valszuro2Dlg: TValszuro2Dlg;

implementation

uses
	Egyeb, Kozos, J_SQL, GepkocsiBe;
{$R *.DFM}

procedure TValszuro2Dlg.BitBtn6Click(Sender: TObject);
begin
	Close;
end;

procedure TValszuro2Dlg.BitElkuldClick(Sender: TObject);
begin
	// Itt �ll�tjuk �ssze a visszaadand� stringeket
	kodkilist.Clear;
//	fodial.VoltSzuroEnter(mezo_sorsz);
	Close;
end;

procedure TValszuro2Dlg.BitKilepClick(Sender: TObject);
begin
	kodkilist.Clear;
	voltenter	:= false;
	Close;
end;

procedure TValszuro2Dlg.Tolt(melist, melist2 : TStringList);
var
	i 				: integer;
begin
	Query1.Close;
	
	EgyebDlg.SetADOQueryDatabase(Query1);
//	Query_Run(Query1, sqlstr);
	// A sz�r�si felt�telek megjelen�t�se
	i := 1;
	while ( ( i <= melist.Count ) and (i < 11) ) do begin
		mezok[i]				:= TLabel.Create(Self);
		mezok[i].Parent     	:= Panel2;
		mezok[i].Name			:= 'MEZO'+IntToStr(i);
		mezok[i].Caption		:= melist2[i-1];
		mezok[i].Top			:= Label1.Top + (i-1) * ( Label1.Height + 5);
		mezok[i].Left       	:= Label1.Left;
		// A jel�l�n�gyzetek l�trehoz�sa
		negyzetek[(i-1)*4+1]    		:= TCheckBox.Create(Self);
		negyzetek[(i-1)*4+1].Parent    	:= Panel2;
		negyzetek[(i-1)*4+1].Name	   	:= 'NEGYZET'+IntToStr(i*10+1);
		negyzetek[(i-1)*4+1].Caption   	:= 'minden';
		negyzetek[(i-1)*4+1].Top	   	:= Label1.Top + (i-1) * ( Label1.Height + 5);
		negyzetek[(i-1)*4+1].Left      	:= CheckBox1.Left;
		negyzetek[(i-1)*4+2]    		:= TCheckBox.Create(Self);
		negyzetek[(i-1)*4+2].Parent    	:= Panel2;
		negyzetek[(i-1)*4+2].Name	   	:= 'NEGYZET'+IntToStr(i*10+2);
		negyzetek[(i-1)*4+2].Caption   	:= '�res';
		negyzetek[(i-1)*4+2].Top	   	:= Label1.Top + (i-1) * ( Label1.Height + 5);
		negyzetek[(i-1)*4+2].Left      	:= CheckBox2.Left;
		negyzetek[(i-1)*4+3]    		:= TCheckBox.Create(Self);
		negyzetek[(i-1)*4+3].Parent    	:= Panel2;
		negyzetek[(i-1)*4+3].Name	   	:= 'NEGYZET'+IntToStr(i*10+3);
		negyzetek[(i-1)*4+3].Caption   	:= '0';
		negyzetek[(i-1)*4+3].Top	   	:= Label1.Top + (i-1) * ( Label1.Height + 5);
		negyzetek[(i-1)*4+3].Left      	:= CheckBox3.Left;
		negyzetek[(i-1)*4+4]    		:= TCheckBox.Create(Self);
		negyzetek[(i-1)*4+4].Parent    	:= Panel2;
		negyzetek[(i-1)*4+4].Name	   	:= 'NEGYZET'+IntToStr(i*10+4);
		negyzetek[(i-1)*4+4].Caption   	:= '1';
		negyzetek[(i-1)*4+4].Top	   	:= Label1.Top + (i-1) * ( Label1.Height + 5);
		negyzetek[(i-1)*4+4].Left      	:= CheckBox4.Left;
		Inc(i);
	end;
end;

procedure TValszuro2Dlg.FormDestroy(Sender: TObject);
var
	i : integer;
begin
	kodkilist.Free;
	try
		for i := 1 to 40 do begin
			negyzetek[i].Free;
		end;
	except
	end;
end;

procedure TValszuro2Dlg.FormCreate(Sender: TObject);
begin
	kodkilist		:= TStringList.Create;
	foselected		:= false;
	kelluresszures  := true;
end;

procedure TValszuro2Dlg.FormResize(Sender: TObject);
begin
	BitElkuld.Top		:= 0;
	BitElkuld.Left		:= 0;
	BitElkuld.Height	:= Panel1.Height;
	BitElkuld.Width		:= Panel1.Width DIV 2;
	BitKilep.Top		:= 0;
	BitKilep.Left		:= BitElkuld.Width;
	BitKilep.Height		:= Panel1.Height;
	BitKilep.Width		:= Panel1.Width - BitElkuld.Width;
end;

end.


