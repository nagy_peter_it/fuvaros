object FNapok: TFNapok
  Left = 1313
  Top = 430
  BorderStyle = bsSingle
  Caption = #220'nnepnapok, szabadnapok'
  ClientHeight = 546
  ClientWidth = 552
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 16
  object Panel1: TPanel
    Left = 0
    Top = 505
    Width = 552
    Height = 41
    Align = alBottom
    Color = 8454143
    ParentBackground = False
    TabOrder = 1
    DesignSize = (
      552
      41)
    object BitKilep: TBitBtn
      Left = 447
      Top = 7
      Width = 89
      Height = 27
      HelpContext = 10012
      Anchors = [akTop, akRight]
      Cancel = True
      Caption = 'Kil'#233'p'#233's'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      OnClick = BitKilepClick
      Glyph.Data = {
        CE070000424DCE07000000000000360000002800000024000000120000000100
        1800000000009807000000000000000000000000000000000000008080008080
        0080800080800080800080800080800080800080800080800080800080800080
        8000808000808000808000808000808000808000808000808000808000808000
        8080008080008080008080008080008080008080008080008080008080008080
        0080800080800080800080800080800080808080808080800080800080800080
        8000808000808000808000808000808000808000808000808000808000808000
        8080008080008080008080FFFFFF008080008080008080008080008080008080
        0080800080800080800080800080800080800080800080800080800000FF0000
        800000808080800080800080800080800080800080800000FF80808000808000
        8080008080008080008080008080008080008080808080808080FFFFFF008080
        008080008080008080008080008080FFFFFF0080800080800080800080800080
        800080800080800000FF00008000008000008080808000808000808000808000
        00FF000080000080808080008080008080008080008080008080008080808080
        FFFFFF008080808080FFFFFF008080008080008080FFFFFF808080808080FFFF
        FF0080800080800080800080800080800080800000FF00008000008000008000
        00808080800080800000FF000080000080000080000080808080008080008080
        008080008080008080808080FFFFFF008080008080808080FFFFFF008080FFFF
        FF808080008080008080808080FFFFFF00808000808000808000808000808000
        80800000FF000080000080000080000080808080000080000080000080000080
        000080808080008080008080008080008080008080808080FFFFFF0080800080
        80008080808080FFFFFF808080008080008080008080008080808080FFFFFF00
        80800080800080800080800080800080800000FF000080000080000080000080
        0000800000800000800000808080800080800080800080800080800080800080
        80008080808080FFFFFF00808000808000808080808000808000808000808000
        8080FFFFFF808080008080008080008080008080008080008080008080008080
        0000FF0000800000800000800000800000800000808080800080800080800080
        80008080008080008080008080008080008080808080FFFFFF00808000808000
        8080008080008080008080FFFFFF808080008080008080008080008080008080
        0080800080800080800080800080800000800000800000800000800000808080
        8000808000808000808000808000808000808000808000808000808000808000
        8080808080FFFFFF008080008080008080008080008080808080008080008080
        0080800080800080800080800080800080800080800080800080800000FF0000
        8000008000008000008080808000808000808000808000808000808000808000
        8080008080008080008080008080008080808080FFFFFF008080008080008080
        8080800080800080800080800080800080800080800080800080800080800080
        800080800000FF00008000008000008000008000008080808000808000808000
        8080008080008080008080008080008080008080008080008080008080808080
        008080008080008080008080808080FFFFFF0080800080800080800080800080
        800080800080800080800080800000FF00008000008000008080808000008000
        0080000080808080008080008080008080008080008080008080008080008080
        008080008080808080008080008080008080008080008080808080FFFFFF0080
        800080800080800080800080800080800080800080800000FF00008000008000
        00808080800080800000FF000080000080000080808080008080008080008080
        008080008080008080008080008080808080008080008080008080808080FFFF
        FF008080008080808080FFFFFF00808000808000808000808000808000808000
        80800000FF0000800000808080800080800080800080800000FF000080000080
        000080808080008080008080008080008080008080008080808080FFFFFF0080
        80008080808080008080808080FFFFFF008080008080808080FFFFFF00808000
        80800080800080800080800080800080800000FF000080008080008080008080
        0080800080800000FF0000800000800000800080800080800080800080800080
        80008080808080FFFFFFFFFFFF808080008080008080008080808080FFFFFF00
        8080008080808080FFFFFF008080008080008080008080008080008080008080
        0080800080800080800080800080800080800080800000FF0000800000FF0080
        8000808000808000808000808000808000808080808080808000808000808000
        8080008080008080808080FFFFFFFFFFFFFFFFFF808080008080008080008080
        0080800080800080800080800080800080800080800080800080800080800080
        8000808000808000808000808000808000808000808000808000808000808000
        8080008080008080008080008080008080008080008080808080808080808080
        0080800080800080800080800080800080800080800080800080800080800080
        8000808000808000808000808000808000808000808000808000808000808000
        8080008080008080008080008080008080008080008080008080008080008080
        008080008080008080008080008080008080}
      NumGlyphs = 2
    end
    object DBNavigator1: TDBNavigator
      Left = 8
      Top = 7
      Width = 232
      Height = 27
      DataSource = DSNapok
      VisibleButtons = [nbFirst, nbPrior, nbNext, nbLast, nbInsert, nbDelete, nbPost, nbCancel]
      TabOrder = 1
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 552
    Height = 505
    Align = alClient
    Color = 8454143
    ParentBackground = False
    TabOrder = 0
    object TabControl1: TTabControl
      Left = 1
      Top = 1
      Width = 550
      Height = 503
      Align = alClient
      TabOrder = 0
      Tabs.Strings = (
        'HU')
      TabIndex = 0
      OnChange = TabControl1Change
      object Panel3: TPanel
        Left = 4
        Top = 432
        Width = 542
        Height = 67
        Align = alBottom
        Color = 8454143
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentBackground = False
        ParentFont = False
        TabOrder = 0
        object Label1: TLabel
          Left = 208
          Top = 11
          Width = 42
          Height = 16
          Caption = 'D'#225'tum:'
        end
        object Label2: TLabel
          Left = 8
          Top = 40
          Width = 77
          Height = 16
          Caption = 'Megjegyz'#233's:'
        end
        object Label3: TLabel
          Left = 8
          Top = 11
          Width = 46
          Height = 16
          Caption = 'Orsz'#225'g:'
        end
        object DBCheckBox1: TDBCheckBox
          Left = 400
          Top = 10
          Width = 129
          Height = 17
          Caption = 'Munkasz'#252'neti nap'
          DataField = 'NA_UNNEP'
          DataSource = DSNapok
          TabOrder = 0
          ValueChecked = '1'
          ValueUnchecked = '0'
        end
        object DBEdit1: TDBEdit
          Left = 88
          Top = 37
          Width = 433
          Height = 24
          DataField = 'NA_MEGJE'
          DataSource = DSNapok
          TabOrder = 1
        end
        object CBOrsz: TDBComboBox
          Left = 88
          Top = 8
          Width = 57
          Height = 24
          Style = csDropDownList
          DataField = 'NA_ORSZA'
          DataSource = DSNapok
          ItemHeight = 16
          TabOrder = 2
          TabStop = False
        end
        object DateTimePicker1: TDateTimePicker
          Left = 256
          Top = 8
          Width = 97
          Height = 24
          Date = 41610.420399236110000000
          Time = 41610.420399236110000000
          TabOrder = 3
        end
      end
      object DBGrid1: TDBGrid
        Left = 4
        Top = 27
        Width = 542
        Height = 405
        Align = alClient
        DataSource = DSNapok
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -13
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'NA_ORSZA'
            Title.Alignment = taCenter
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NA_DATUM'
            Title.Alignment = taCenter
            Width = 70
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'napnev'
            Title.Alignment = taCenter
            Title.Caption = 'Nap'
            Width = 70
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NA_UNNEP'
            Title.Alignment = taCenter
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NA_MEGJE'
            Visible = True
          end>
      end
    end
  end
  object DSNapok: TDataSource
    DataSet = TNapok
    Left = 353
    Top = 177
  end
  object TNapok: TADODataSet
    Connection = EgyebDlg.ADOConnectionKozos
    CursorType = ctStatic
    AfterInsert = ADODataSet1AfterInsert
    BeforePost = TNapokBeforePost
    AfterPost = TNapokAfterPost
    AfterScroll = TNapokAfterScroll
    OnCalcFields = TNapokCalcFields
    CommandText = 'select  * from NAPOK where (NA_ORSZA= :ORSZ) order by NA_DATUM '
    Parameters = <
      item
        Name = 'ORSZ'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 3
        Value = Null
      end>
    Left = 313
    Top = 177
    object TNapokNA_ORSZA: TStringField
      DisplayLabel = 'Orsz.'
      FieldName = 'NA_ORSZA'
      Size = 3
    end
    object TNapokNA_DATUM: TDateTimeField
      Alignment = taCenter
      DisplayLabel = 'D'#225'tum'
      DisplayWidth = 12
      FieldName = 'NA_DATUM'
    end
    object TNapokNA_UNNEP: TIntegerField
      Alignment = taCenter
      DisplayLabel = #220'nnep'
      DisplayWidth = 5
      FieldName = 'NA_UNNEP'
    end
    object TNapokNA_MEGJE: TStringField
      DisplayLabel = 'Megjegyz'#233's'
      DisplayWidth = 40
      FieldName = 'NA_MEGJE'
      Size = 100
    end
    object TNapoknapnev: TStringField
      FieldKind = fkCalculated
      FieldName = 'napnev'
      Calculated = True
    end
  end
  object TOrsz: TADOQuery
    Connection = EgyebDlg.ADOConnectionKozos
    Parameters = <>
    SQL.Strings = (
      'select distinct na_orsza from napok order by na_orsza')
    Left = 201
    Top = 233
    object TOrszna_orsza: TStringField
      FieldName = 'na_orsza'
      Size = 3
    end
  end
end
