unit SoforBe;

interface

uses
	SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
	Forms, Dialogs, StdCtrls, Buttons, Mask, DB, DBTables, J_ALFORM, ADODB,
  ExtCtrls;

type
	TSoforbeDlg = class(TJ_AlformDlg)
	BitElkuld: TBitBtn;
	BitKilep: TBitBtn;

    Query1: TADOQuery;
    Label5: TLabel;
    Label6: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label7: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label23: TLabel;
    Label4: TLabel;
    Label36: TLabel;
    M7: TMaskEdit;
    M9: TMaskEdit;
    M8: TMaskEdit;
    M10: TMaskEdit;
    Panel1: TPanel;
    BitBtn9: TBitBtn;
    BitBtn10: TBitBtn;
    M6: TMaskEdit;
	 BitBtn2: TBitBtn;
    M23: TMaskEdit;
    M24: TMaskEdit;
    M24A: TMaskEdit;
    M13A: TMaskEdit;
    M5: TMaskEdit;
    M11: TMaskEdit;
    Label1: TLabel;
    Label2: TLabel;
    M3: TMaskEdit;
    Label3: TLabel;
    M4: TMaskEdit;
    Query2: TADOQuery;
    Label8: TLabel;
    cbCSEHTRAN: TComboBox;
	procedure BitKilepClick(Sender: TObject);
	procedure FormCreate(Sender: TObject);
	procedure Tolt(cim : string; jaratkod, sorsz : string);
	procedure BitElkuldClick(Sender: TObject);
  procedure MaskEditClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure M3KeyPress(Sender: TObject; var Key: Char);
    procedure BitBtn2Click(Sender: TObject);
    procedure M24AExit(Sender: TObject);
    procedure BitBtn9Click(Sender: TObject);
    procedure BitBtn10Click(Sender: TObject);
    procedure M9Exit(Sender: TObject);
    procedure M8Exit(Sender: TObject);
	 procedure KintiIdo;
	 procedure KmHozzaadas(rendsz : string; Mkm1, Mkm2 : TMaskEdit );
    procedure M3Change(Sender: TObject);
	private
  		sorszam		: string;
	public
   	ret_sofor	: string;
		kmdij		: string;
		rendszam	: string;
		eujarat, elszamoltjarat,kellellenorzes		: boolean;
	end;

var
	SoforbeDlg: TSoforbeDlg;
  jkdat,jvdat:string;      // j�rat kezdete,v�ge KG

implementation

uses
	J_VALASZTO, Egyeb, J_SQL, Kozos, Jaratbe;
{$R *.DFM}

procedure TSoforbeDlg.BitKilepClick(Sender: TObject);
begin
	ret_kod := '';
	Close;
end;

procedure TSoforbeDlg.FormCreate(Sender: TObject);
begin
	EgyebDlg.SetADOQueryDatabase(Query1);
	EgyebDlg.SetADOQueryDatabase(Query2);
	SetMaskEdits([M6]);
	ret_kod 	:= '';
	kmdij		:= '';
	eujarat		:= false;
end;

procedure TSoforbeDlg.Tolt(cim : string; jaratkod, sorsz : string);
begin
	ret_kod  	:= jaratkod;
	Caption  	:= cim;
   sorszam		:= sorsz;
   M24.Text	:= kmdij;
   if sorszam <> '' then begin
   	// Az adatok beolvas�sa
     	Query_Run (Query1, 'SELECT * FROM JARSOFOR WHERE JS_JAKOD = '''+ret_kod+''' '+' AND JS_SORSZ = '''+sorszam+''' ',true);
		if Query1.RecordCount > 0 then begin
   		M3.Text 	:= Query1.FieldByname('JS_KMOR1').AsString;
   		M4.Text 	:= Query1.FieldByname('JS_KMOR2').AsString;
   		M5.Text 	:= Query1.FieldByname('JS_SOFOR').AsString;
      M6.Text 	:= Query_Select('DOLGOZO', 'DO_KOD', M5.Text, 'DO_NAME' );
   		M7.Text 	:= Query1.FieldByname('JS_KIDAT').AsString;
   		M8.Text 	:= Query1.FieldByname('JS_KIIDO').AsString;
   		M9.Text 	:= Query1.FieldByname('JS_BEDAT').AsString;
   		M10.Text 	:= Query1.FieldByname('JS_BEIDO').AsString;
   		M23.Text 	:= Query1.FieldByname('JS_SZAKM').AsString;
   		M24.Text 	:= Query1.FieldByname('JS_JADIJ').AsString;
   		M24A.Text 	:= Query1.FieldByname('JS_EXDIJ').AsString;
   		M13A.Text 	:= Query1.FieldByname('JS_FELRA').AsString;
      cbCSEHTRAN.ItemIndex:=Query1.FieldByname('JS_CSEHTRAN').AsInteger;   // a list�ban 0, 1, stb. van
   		M11.Text 	:= Query1.FieldByname('JS_MEGJE').AsString;
           sorszam		:= Query1.FieldByname('JS_SORSZ').AsString;
           KintiIdo;
//           BitBtn2.Enabled := false;
		end;
   end;
end;

procedure TSoforbeDlg.BitElkuldClick(Sender: TObject);
var
	returnstr	: string;
  jkezd,jveg,sbe,ski:Tdatetime;
begin
  // KG 20111011
  // kil�p�s, bel�p�s ideje a j�rat kezdete, v�ge intervallumba esik e.
 if (M7.Text<>EmptyStr) and (M9.Text<>EmptyStr) then
 begin
  jkezd:=StrToDateTime(jkdat);
  jveg:=StrToDateTime(jvdat);
  ski:=StrToDateTime(M7.Text+' '+M8.Text+':00');
  sbe:=StrToDateTime(M9.Text+' '+M10.Text+':00');

  if (ski<jkezd)or(ski>jveg) then
  begin
    NoticeKi('Hib�s kil�p�si id�pont!');
    M7.SetFocus;
    exit;
  end;
  if (sbe<jkezd)or(sbe>jveg) then
  begin
    NoticeKi('Hib�s bel�p�si id�pont!');
    M9.SetFocus;
    exit;
  end;
  if (sbe<ski) then
  begin
    NoticeKi('Hib�s id�pont!');
    M9.SetFocus;
    exit;
  end;
 end;
  ///////////////////////////////////////////

	KmHozzaadas(rendszam, M3, M4 );
	if M6.Text = '' then begin
		NoticeKi('Sof�r megad�sa k�telez�!');
       BitBtn2.SetFocus;
     	Exit;
  	end;
   if (StrToIntDef(M3.Text,-1) <= 0)and(kellellenorzes) then begin
		NoticeKi('�rv�nytelen kezd� km!');
       M3.SetFocus;
       Exit;
   end;
   if elszamoltjarat and (StrToIntDef(M4.Text,-1) <= 0) then begin
		NoticeKi('�rv�nytelen befejez� km!');
       M4.SetFocus;
       Exit;
   end;
   if elszamoltjarat and (StringSzam(M3.Text) > StringSzam(M4.Text)) then begin
		NoticeKi('Rossz km-ek megad�sa! A befejez�nek nagyobbnak kell lennie a kezd�n�l!');
       M4.Setfocus;
       Exit;
   end;
   if StrToIntDef(M23.Text, 0) = 0 then begin
   	if StringSzam(M4.Text) - StringSzam(M3.Text) > 0 then begin
       	if NoticeKi('Az elsz�molhat� km nulla. Korrig�ljam?' , NOT_QUESTION ) = 0 then begin
           	M23.Text	:= Format('%.0f', [StringSzam(M4.Text) - StringSzam(M3.Text)]);
        end;
    end;
   end;
   ret_sofor	:= M5.Text;
	if sorszam = '' then begin
		{�j rekord felvitele}
		returnstr	:= SoforUtkozes(ret_kod, M5.text, M3.Text, M4.Text);
       if returnstr	<> '' then begin
       	NoticeKi(returnstr);
       	Exit;
		end;
(*
		// A sof�r napid�j �tk�z�s�nek vizsg�lata
		Query_Run (Query1, 'SELECT * FROM JARSOFOR WHERE JS_SOFOR = '''+M5.Text+''' AND ( '+
			'('''+M7.Text+M8.Text+'''  >= JS_KIDAT + JS_KIIDO AND '''+M7.Text+M8.Text+'''  <= JS_BEDAT + JS_BEIDO ) OR '+
			'('''+M9.Text+M10.Text+''' >= JS_KIDAT + JS_KIIDO AND '''+M9.Text+M10.Text+''' <= JS_BEDAT + JS_BEIDO ) OR '+
			'('''+M7.Text+M8.Text+'''  <= JS_KIDAT + JS_KIIDO AND '''+M9.Text+M10.Text+''' >= JS_BEDAT + JS_BEIDO ) )', true);
		if Query1.RecordCount > 0 then begin
			if Query1.FieldByName('JS_KIDAT').AsString <> '' then begin
				NoticeKi('�tk�z� id�pontok! A m�r l�tez� : '+Query1.FieldByName('JS_KIDAT').AsString +' '+
					Query1.FieldByName('JS_KIIDO').AsString+' - '+Query1.FieldByName('JS_BEDAT').AsString+' '+
					Query1.FieldByName('JS_BEIDO').AsString );
				Exit;
			end;
		end;
*)
		// A maxim�lis sorsz�m kiv�laszt�sa
		Query_Run (Query1, 'SELECT MAX(JS_SORSZ) MAXKOD FROM JARSOFOR WHERE JS_JAKOD = '''+ret_kod+'''  ',true);
		sorszam	:= IntToStr(StrToIntDef(Query1.FieldByName('MAXKOD').AsString, 0) + 1);
		Query_Run ( Query1, 'INSERT INTO JARSOFOR (JS_JAKOD, JS_SOFOR, JS_SORSZ) VALUES ('''+ret_kod+''','''+M5.Text+''', '+sorszam+' )',true);
	end else begin
(*
		// M�dos�t�sn�l az aktu�lis rekord nem sz�m�t
		Query_Run (Query1, 'SELECT * FROM JARSOFOR WHERE JS_SOFOR = '''+M5.Text+''' AND ( '+
			'('''+M7.Text+M8.Text+''' >= JS_KIDAT + JS_KIIDO AND  '''+M7.Text+M8.Text+''' <= JS_BEDAT + JS_BEIDO ) OR '+
			'('''+M9.Text+M10.Text+''' >= JS_KIDAT + JS_KIIDO AND '''+M9.Text+M10.Text+''' <= JS_BEDAT + JS_BEIDO ) OR '+
			'('''+M7.Text+M8.Text+''' <= JS_KIDAT + JS_KIIDO AND  '''+M9.Text+M10.Text+''' >= JS_BEDAT + JS_BEIDO ) ) '+
			' AND (( JS_JAKOD <> '''+ret_kod+''' ) OR ( JS_SORSZ <> '+sorszam+'))', true);
		if Query1.RecordCount > 0 then begin
			if Query1.FieldByName('JS_KIDAT').AsString <> '' then begin
				NoticeKi('�tk�z� id�pontok! A m�r l�tez�: '+Query1.FieldByName('JS_KIDAT').AsString +' '+
					Query1.FieldByName('JS_KIIDO').AsString+' - '+Query1.FieldByName('JS_BEDAT').AsString+' '+
					Query1.FieldByName('JS_BEIDO').AsString );
				Exit;
			end;
		end;
*)		
	end;

	{A r�gi rekord m�dos�t�sa}
	Query_Update (Query1, 'JARSOFOR',
		[
		'JS_SOFOR', ''''+M5.Text+'''',
		'JS_KIDAT', ''''+M7.Text+'''',
		'JS_KIIDO', ''''+M8.Text+'''',
		'JS_BEDAT', ''''+M9.Text+'''',
		'JS_BEIDO', ''''+M10.Text+'''',
		'JS_SZAKM', IntToStr(StrToIntDef(M23.Text,0)),
		'JS_JADIJ', SqlSzamString(StringSzam(M24.Text), 12, 2),
		'JS_EXDIJ', SqlSzamString(StringSzam(M24A.Text), 12, 2),
		'JS_FELRA', SqlSzamString(StringSzam(M13A.Text), 6, 2),
		'JS_KMOR1', SqlSzamString(StringSzam(M3.Text), 12, 0),
		'JS_KMOR2', SqlSzamString(StringSzam(M4.Text), 12, 0),
    'JS_CSEHTRAN', IntToStr(cbCSEHTRAN.ItemIndex),
		'JS_MEGJE', ''''+M11.Text+''''
		],
		' WHERE JS_JAKOD = '''+ret_kod+''' AND JS_SORSZ = '''+sorszam+''' ');
	ret_kod := sorszam;
	Close;
end;

procedure TSoforbeDlg.MaskEditClick(Sender: TObject);
begin
	with Sender as TMaskEdit do begin
  		SelectAll;
  	end;
end;

procedure TSoforbeDlg.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
	EgyebDlg.FormReturn(Key);
end;

procedure TSoforbeDlg.M3KeyPress(Sender: TObject; var Key: Char);
begin
  with Sender as TMaskEdit do begin
  		Key := EgyebDlg.Jochar(Text,Key,(Sender as TMaskEdit).MaxLength,(Sender as TMaskEdit).Tag);
  end;
end;

procedure TSoforbeDlg.BitBtn2Click(Sender: TObject);
var
	kategkod	: string;
	szorzo		: double;
	arfolyam	: double;
	datum		: string;
begin
	DolgozoValaszto(M5, M6);
(*
	Query_Run (Query1, 'SELECT * FROM JARSOFOR WHERE JS_JAKOD = '''+ret_kod+''' '+' AND JS_SOFOR = '''+M5.Text+''' ',true);
	if Query1.RecordCount > 0 then begin
		NoticeKi('L�tez� sof�r!');
		BitBtn2.SetFocus;
		Exit;
	end;
*)
	// A d�j/km eld�nt�se a sof�r alapj�n
	if eujarat then begin
		// Az el�z� h�nap 15.-ei �rfolyammal szorozzuk az EUR-ban a sz�t�rban megadott �sszeget
		kategkod	:= Query_Select('DOLGOZO', 'DO_KOD', M5.Text, 'DO_MBKAT');
		Query_Run(Query2, 'SELECT SZ_LEIRO FROM SZOTAR WHERE SZ_FOKOD = ''120'' AND SZ_ALKOD = '''+kategkod+''' ');
		szorzo	:= Stringszam(Query2.FieldByName('SZ_LEIRO').AsString);
		if szorzo = 0 then begin
			NoticeKi('Figyelem! A b�zissz�t�rban a dolgoz� kateg�ri�j�hoz 0 szorz� tartozik! K�rem jav�tsa ki az �rt�ket a le�r�s mez�ben!');
			Exit;
		end;
		datum		:= EgyebDlg.MaiDatum;
		datum		:= copy(datum,1,8)+'01.';
		datum		:= DatumHoznap(datum,-1, true);
		datum		:= copy(datum,1,8)+'15.';
		arfolyam	:= EgyebDlg.ArfolyamErtek( 'EUR', datum, false);
		if arfolyam = 0 then begin
			NoticeKi('Figyelem! A '+datum+' napon az EUR �rfolyama 0!');
			Exit;
		end;
		M24.Text	:= Format('%.2f', [arfolyam*szorzo]);
	end;
	M23.SetFocus;

end;

procedure TSoforbeDlg.M24AExit(Sender: TObject);
begin
	SzamExit(Sender);
end;

procedure TSoforbeDlg.BitBtn9Click(Sender: TObject);
begin
	EgyebDlg.Calendarbe(M7);
end;

procedure TSoforbeDlg.BitBtn10Click(Sender: TObject);
begin
	EgyebDlg.Calendarbe(M9);
end;

procedure TSoforbeDlg.M9Exit(Sender: TObject);
begin
	DatumExit(Sender, false);
  	KintiIdo;
end;

procedure TSoforbeDlg.KintiIdo;
var
 	ora		: integer;
  	nap		: integer;
begin
	if ( (M7.Text <> '' ) and (M9.Text <> '') ) then begin
  		EgyebDlg.DateTimeDifference(M7.Text, M8.Text, M9.Text, M10.Text, nap, ora);
  		Panel1.Caption := Format('%2.2d nap %2.2d �ra', [nap, ora]);
  	end;
end;

procedure TSoforbeDlg.M8Exit(Sender: TObject);
begin
	if not Idoexit( Sender, false ) then begin
		Exit;
	end;
	KintiIdo;
end;

procedure TSoforbeDlg.KmHozzaadas(rendsz : string; Mkm1, Mkm2 : TMaskEdit );
var
	hozza	: double;
begin
   // A hozz�adott km. ellen�rz�se
   if rendsz <> '' then begin
   	hozza	:= StringSzam(Query_Select('GEPKOCSI', 'GK_RESZ', rendsz, 'GK_HOZZA'));
       if hozza > 0 then begin
       	if StringSzam(Mkm1.Text) <> 0 then begin
               if StringSzam(Mkm1.Text) < hozza then begin
               	Mkm1.Text	:= Format('%.0f', [hozza + StringSZam(Mkm1.Text)]);
               end;
           end;
       	if StringSzam(Mkm2.Text) <> 0 then begin
               if StringSzam(Mkm2.Text) < hozza then begin
               	Mkm2.Text	:= Format('%.0f', [hozza + StringSZam(Mkm2.Text)]);
               end;
           end;
       end;
   end;
end;

procedure TSoforbeDlg.M3Change(Sender: TObject);
begin
//  	M23.Text	:= Format('%.0f', [StringSzam(M4.Text) - StringSzam(M3.Text)]);
end;

end.
