unit RB_Export;

interface

uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls, Egyeb,
  Forms, Dialogs, DBTables, DB, StdCtrls, Buttons, Mask, DBCtrls, Grids,
  Printers, Shellapi, ExtCtrls, ADODB, J_Excel;

type

  TRB_ExportDlg = class(TForm)
	 Label1: TLabel;
    BitBtn4: TBitBtn;
    BitBtn6: TBitBtn;
    M1: TMaskEdit;
	 M2: TMaskEdit;
	 Label2: TLabel;
	 Label3: TLabel;
	 BitBtn10: TBitBtn;
	 BitBtn1: TBitBtn;
	 Query1: TADOQuery;
    Label4: TLabel;
	 Query2: TADOQuery;
	 Query3: TADOQuery;
    M50: TMaskEdit;
    M5: TMaskEdit;
    BitBtn5: TBitBtn;
	 Label6: TLabel;
	 M4: TMaskEdit;
    QKoz: TADOQuery;
	 procedure FormCreate(Sender: TObject);
	 procedure BitBtn4Click(Sender: TObject);
	 procedure BitBtn6Click(Sender: TObject);
	 procedure M1Click(Sender: TObject);
	 procedure BitBtn10Click(Sender: TObject);
	 procedure BitBtn1Click(Sender: TObject);
	 procedure M1Exit(Sender: TObject);
	 function XLS_Generalas : boolean;
	 procedure BitBtn5Click(Sender: TObject);
  private
		kilepes		: boolean;
  public
		voltkilepes	: boolean;
		kellshell	: boolean;
		kelluzenet	: boolean;
  end;

var
  RB_ExportDlg: TRB_ExportDlg;

implementation

uses
	j_sql, Kozos, ValVevo, J_DataModule;
{$R *.DFM}

procedure TRB_ExportDlg.FormCreate(Sender: TObject);
begin
	M4.Text		:= 'Izs� L�szl�';
	kilepes     := true;
	kellshell	:= true;
	voltkilepes	:= false;
	kelluzenet	:= true;
	EgyebDlg.SetADOQueryDatabase(Query1);
	EgyebDlg.SetADOQueryDatabase(Query2);
	EgyebDlg.SetADOQueryDatabase(Query3);
	EgyebDlg.SetADOQueryDatabase(QKoz);
	// A RB...   vev� kiv�laszt�sa : 4012 / 3736 / 3576
	m50.Text 	:= '3576,3736,4012';
	m5.Text		:= '';
   Query_Run(QKoz, 'SELECT V_NEV FROM VEVO WHERE V_KOD IN ('+M50.Text+')');
   while not QKoz.Eof do begin
       m5.Text := m5.Text + QKoz.FieldByName('V_NEV').AsString+',';
       QKoz.Next;
   end;
	m2.Text		:= DatumHozNap(copy(EgyebDlg.MaiDatum, 1, 8)+'01.', -1, true);
	m1.Text		:= copy(M2.Text, 1, 8)+'01.'
end;

procedure TRB_ExportDlg.BitBtn4Click(Sender: TObject);
begin
	if not DatumExit(M1, false) then begin
		Exit;
	end;
	if not DatumExit(M2, false) then begin
		Exit;
	end;
	if M50.Text = '' then begin
		NoticeKi('A megb�z� kiv�laszt�sa k�telez�!');
		M5.SetFocus;
		Exit;
	end;
	// Az XLS �llom�ny gener�l�sa
	Screen.Cursor	:= crHourGlass;
	XLS_Generalas;
	Screen.Cursor	:= crDefault;
end;

procedure TRB_ExportDlg.BitBtn6Click(Sender: TObject);
begin
	if not kilepes then begin
		kilepes	:= true;
	end else begin
		Close;
	end;
end;

procedure TRB_ExportDlg.M1Click(Sender: TObject);
begin
	with Sender as TMaskEdit do begin
		SelectAll;
	end;
end;

procedure TRB_ExportDlg.BitBtn10Click(Sender: TObject);
begin
	// EgyebDlg.Calendarbe(M1);
	// EgyebDlg.ElsoNapEllenor(M1, M2);
  EgyebDlg.CalendarBe_idoszak(M1, M2);
end;

procedure TRB_ExportDlg.BitBtn1Click(Sender: TObject);
begin
	EgyebDlg.Calendarbe(M2, True);
end;

procedure TRB_ExportDlg.M1Exit(Sender: TObject);
begin
	DatumExit(M1,false);
	EgyebDlg.ElsoNapEllenor(M1, M2);
end;

function TRB_ExportDlg.XLS_Generalas : boolean;
var
	fn			: String;
	sorszam 	: integer;
	EX	  		: TJExcel;
   dat1        : string;
   ido1        : string;
   idokul      : integer;
   db          : integer;
   dbfel       : integer;
   dble        : integer;
   idofel      : double;
   idole       : double;
   Col1_toltve: boolean;
begin

	EX 	:= TJexcel.Create(Self);
	EllenListTorol;
	result		:= true;
	fn		:= 'RB_'+M1.Text+'_'+M2.Text;
	while Pos('.', fn) > 0 do begin
		fn		:= copy(fn, 1, Pos('.', fn) - 1 ) + copy(fn, Pos('.', fn) + 1, 999 );
	end;
	fn			:= EgyebDlg.DocumentPath + fn + '.' + EX.GetExtension;
	if FileExists(fn) then begin
		DeleteFile(PChar(fn));
	end;

   // vkod = 3736
	Query_Run(Query1, 'SELECT MB_MBKOD, MB_RENSZ, MB_VENEV, MB_POZIC, MB_FUDIJ, MB_JASZA, MB_KISCS, MB_SAKO2 FROM MEGBIZAS '+
			' WHERE MB_MBKOD IN ( SELECT MS_MBKOD FROM MEGSEGED WHERE MS_FETDAT >= '''+M1.Text+''' AND MS_FETDAT <= '''+M2.Text+''' AND MS_FAJKO = 0 ) '+
			' AND MB_VEKOD IN ('+M50.Text+') '+
			' ORDER BY MB_DATUM, MB_MBKOD ' );
	if Query1.RecordCount < 1 then begin
		if kelluzenet then begin
			NoticeKi('Nincs a felt�teleknek megfelel� elem!');
		end;
		result		:= false;
		Exit;
	end;
	BitBtn4.Hide;
	// Az XLS megnyit�sa
	if not EX.OpenXlsFile(fn) then begin
       NoticeKi('Nem siker�lt megnyitnom az Excel �llom�nyt!');
       EX.Free;
       Exit;
   end;
   EX.ColNumber	:= 40;
   EX.InitRow;
   EX.SetActiveSheet(1);
   sorszam			:= 1;

   EX.SetRowcell(1, 'Delivery period from-to');
   EX.SetRowcell(2, M1.Text + ' - ' + M2.Text);
   EX.SaveRow(sorszam);
   Inc(sorszam);
   Inc(sorszam);

   Ex.EmptyRow;
   EX.SetRowcell( 1, 'J&S loa dref.');
   EX.SetRowcell( 2, 'File nr');
   EX.SetRowcell( 3, 'Invoice nr.');
   EX.SetRowcell( 4, 'Service');
   EX.SetRowcell( 5, 'Planned loading date');
   EX.SetRowcell( 6, 'Planned loading time slot');
   EX.SetRowcell( 7, 'Arrival time to loading point');
   EX.SetRowcell( 8, 'Arrival time to loading point');
   EX.SetRowcell( 9, 'Delay at arrival at Loading point in hrs');
   EX.SetRowcell(10, 'Date of Loading');
   EX.SetRowcell(11, 'Real time of loading');
   EX.SetRowcell(12, 'duration of loading (hrs)');
   EX.SetRowcell(13, 'Client');
   EX.SetRowcell(14, 'Loading ref');
   EX.SetRowcell(15, 'Consignor');
   EX.SetRowcell(16, 'Country origin');
   EX.SetRowcell(17, 'Postal code -loading place');
   EX.SetRowcell(18, 'City of origin');
   EX.SetRowcell(19, 'Planned unloading date');
   EX.SetRowcell(20, 'Planned unloading time slot');
   EX.SetRowcell(21, 'Arrival time to unloading point');
   EX.SetRowcell(22, 'Arrival time to unloading point');
   EX.SetRowcell(23, 'Delay at arrival at unloading point in hrs');
   EX.SetRowcell(24, 'Date of unloading');
   EX.SetRowcell(25, 'Real time of unloading');
   EX.SetRowcell(26, 'duration of unloading (hrs)');
   EX.SetRowcell(27, 'Consignee');
   EX.SetRowcell(28, 'Country of destin.');
   EX.SetRowcell(29, 'postal code destin.');
   EX.SetRowcell(30, 'destination city');
   EX.SetRowcell(31, 'Truck nr');
   EX.SetRowcell(32, 'Trailer');
   EX.SetRowcell(33, 'qantity');
   EX.SetRowcell(34, 'wight');
   EX.SetRowcell(35, 'Freight cost');
   EX.SetRowcell(36, 'Reason for delay');
   EX.SaveRow(sorszam);
   Inc(sorszam);
   Ex.EmptyRow;
   Col1_toltve:= False;
   kilepes		:= false;
   Query1.DisableControls;
   db          := Query1.RecordCount;
   dbfel       := 0;
   dble        := 0;
   idofel      := 0;
   idole       := 0;
   while ( ( not Query1.Eof ) and (not kilepes) ) do begin
       Query_Run(Query2, 'SELECT * FROM MEGSEGED WHERE MS_MBKOD = '+Query1.FieldByName('MB_MBKOD').AsString+' AND MS_FETDAT >= '''+M1.Text+''' AND MS_FETDAT <= '''+M2.Text+''' AND MS_FAJKO = 0 ORDER BY MS_SORSZ, MS_TIPUS ');
       while not Query2.Eof do begin
           if StrToIntDef(Query2.FieldByName('MS_TIPUS').AsString, -1) = 0 then begin
               // Felrak�sr�l van sz�
               if Col1_toltve then begin
                   // A sor m�r f�lig t�lt�tt, le kell �rni
                   EX.SaveRow(sorszam);
                   Inc(sorszam);
                   Ex.EmptyRow;
                   Col1_toltve:= False;
               end;
               // Bet�ltj�k a felrak�si adatokat
               EX.SetRowcell( 1, Query1.FieldByName('MB_MBKOD').AsString );
               Col1_toltve:= True;
               EX.SetRowcell( 2, Query1.FieldByName('MB_JASZA').AsString );
               EX.SetRowcell( 3, Query1.FieldByName('MB_SAKO2').AsString );
               if StrToIntDef(Query1.FieldByName('MB_KISCS').AsString, 0) > 0 then begin
                   EX.SetRowcell( 4, 'express');
               end else begin
                   EX.SetRowcell( 4, 'standard');
               end;
               dat1    := Query2.FieldByName('MS_FELDAT2').AsString;
               ido1    := Query2.FieldByName('MS_FELIDO2').AsString;
               if dat1 = '' then begin
                   dat1    := Query2.FieldByName('MS_FELDAT').AsString;
                   ido1    := Query2.FieldByName('MS_FELIDO').AsString;
               end;
               EX.SetRowcell( 5, dat1 );
               EX.SetRowcell( 6, ido1 );
               EX.SetRowcell( 7, Query2.FieldByName('MS_FERKDAT').AsString );
               EX.SetRowcell( 8, Query2.FieldByName('MS_FERKIDO').AsString );
               idokul  := DateTimeDifferenceSec( Query2.FieldByName('MS_FERKDAT').AsString, Query2.FieldByName('MS_FERKIDO').AsString, dat1, ido1);
               if idokul > 0 then begin
                   // K�selemes felrak�s
                   Inc(dbfel);
               end;
               EX.SetRowcell( 9, idokul / 3600 );
               EX.SetRowcell(10, Query2.FieldByName('MS_FETDAT').AsString );
               EX.SetRowcell(11, Query2.FieldByName('MS_FETIDO').AsString );
               idokul  := DateTimeDifferenceSec( Query2.FieldByName('MS_FETDAT').AsString, Query2.FieldByName('MS_FETIDO').AsString,Query2.FieldByName('MS_FERKDAT').AsString, Query2.FieldByName('MS_FERKIDO').AsString);
               idofel  := idofel + idokul / 3600;
               EX.SetRowcell(12, idokul / 3600 );
               EX.SetRowcell(13, Query1.FieldByName('MB_VENEV').AsString );
               EX.SetRowcell(14, Query1.FieldByName('MB_POZIC').AsString );
               EX.SetRowcell(15, Query2.FieldByName('MS_FELNEV').AsString );
               EX.SetRowcell(16, Query2.FieldByName('MS_FORSZ').AsString );
               EX.SetRowcell(17, Query2.FieldByName('MS_FELIR').AsString );
               EX.SetRowcell(18, Query2.FieldByName('MS_FELTEL').AsString );
               EX.SetRowcell(33, Query2.FieldByName('MS_FEPAL').AsString );
               EX.SetRowcell(34, Query2.FieldByName('MS_FSULY').AsString );

           end else begin
               // Lerak�sr�l van sz�
               // Bet�ltj�k a lerak�si adatokat
               dat1    := Query2.FieldByName('MS_LERDAT2').AsString;
               ido1    := Query2.FieldByName('MS_LERIDO2').AsString;
               if dat1 = '' then begin
                   dat1    := Query2.FieldByName('MS_LERDAT').AsString;
                   ido1    := Query2.FieldByName('MS_LERIDO').AsString;
               end;
               EX.SetRowcell(19, dat1 );
               EX.SetRowcell(20, ido1 );
               EX.SetRowcell(21, Query2.FieldByName('MS_LERKDAT').AsString );
               EX.SetRowcell(22, Query2.FieldByName('MS_LERKIDO').AsString );
               idokul  := DateTimeDifferenceSec( Query2.FieldByName('MS_LERKDAT').AsString, Query2.FieldByName('MS_LERKIDO').AsString, dat1, ido1);
               if idokul > 0 then begin
                   // K�selemes lerak�s
                   Inc(dble);
               end;
               EX.SetRowcell(23, idokul / 3600);
               EX.SetRowcell(24, Query2.FieldByName('MS_LETDAT').AsString );
               EX.SetRowcell(25, Query2.FieldByName('MS_LETIDO').AsString );
               idokul  := DateTimeDifferenceSec( Query2.FieldByName('MS_LETDAT').AsString, Query2.FieldByName('MS_LETIDO').AsString,Query2.FieldByName('MS_LERKDAT').AsString, Query2.FieldByName('MS_LERKIDO').AsString);
               idole   := idole + idokul / 3600;
               EX.SetRowcell(26, idokul / 3600);
               EX.SetRowcell(27, Query2.FieldByName('MS_LERNEV').AsString );
               EX.SetRowcell(28, Query2.FieldByName('MS_ORSZA').AsString );
               EX.SetRowcell(29, Query2.FieldByName('MS_LELIR').AsString );
               EX.SetRowcell(30, Query2.FieldByName('MS_LERTEL').AsString );
               EX.SetRowcell(31, Query2.FieldByName('MS_RENSZ').AsString );
               EX.SetRowcell(32, Query2.FieldByName('MS_POTSZ').AsString );
               EX.SetRowcell(33, Query2.FieldByName('MS_LEPAL').AsString );
               EX.SetRowcell(34, Query2.FieldByName('MS_LSULY').AsString );
               EX.SetRowcell(35, Query1.FieldByName('MB_FUDIJ').AsString );
               EX.SetRowcell(36, Query2.FieldByName('MS_KESES').AsString );
               // A sor m�r f�lig t�lt�tt, le kell �rni
               EX.SaveRow(sorszam);
               Inc(sorszam);
               Ex.EmptyRow;
               Col1_toltve:= False;
           end;
           Query2.Next;
       end;
       Query1.Next;
   end;
   // A statisztik�k ki�r�sa
   EX.SetActiveSheet(2);
   sorszam := 1;
   EX.SetRowcell(1, 'Nr of transports');
   EX.SetRowcell(2, IntToStr(db));
   EX.SaveRow(sorszam);
   Inc(sorszam);
   Ex.EmptyRow;
   EX.SetRowcell(1, 'Nr of delays at loadings');
   EX.SetRowcell(2, IntToStr(dbfel));
   EX.SaveRow(sorszam);
   Inc(sorszam);
   Ex.EmptyRow;
   EX.SetRowcell(1, '% of delays at loading');
   EX.SetRowcell(2, dbfel*100/db);
   EX.SetRangeFormat(sorszam, 2, sorszam, 2, '0,0');
   EX.SaveRow(sorszam);
   Inc(sorszam);
   Ex.EmptyRow;
   EX.SetRowcell(1, 'nr of delays at unloading');
   EX.SetRowcell(2, IntToStr(dble));
   EX.SaveRow(sorszam);
   Inc(sorszam);
   Ex.EmptyRow;
   EX.SetRowcell(1, '% of delays at unloading');
   EX.SetRowcell(2, dble*100/db);
   EX.SetRangeFormat(sorszam, 2, sorszam, 2, '0,0');
   EX.SaveRow(sorszam);
   Inc(sorszam);
   Ex.EmptyRow;
   EX.SetRowcell(1, 'Average Duration of loading');
   EX.SetRowcell(2, idofel/db);
   EX.SetRangeFormat(sorszam, 2, sorszam, 2, '0,00');
   EX.SaveRow(sorszam);
   Inc(sorszam);
   Ex.EmptyRow;
   EX.SetRowcell(1, 'Average Duration of unloading');
   EX.SetRowcell(2, idole/db);
   EX.SetRangeFormat(sorszam, 2, sorszam, 2, '0,00');
   EX.SaveRow(sorszam);
   Ex.EmptyRow;

   ///////////////////////////////
	EX.SaveFileAsXls(fn);
	EX.Free;
	voltkilepes	:= kilepes;
	if ( ( EgyebDlg.ellenlista.Count > 0 ) and kelluzenet ) then begin
		EllenListMutat('Az RB riport eredm�nye', false);
	end;
	BitBtn4.Show;
	if kellshell then begin
		ShellExecute(Application.Handle,'open',PChar(fn), '', '', SW_SHOWNORMAL );
	end;
	Label4.Caption	:= '';
	Label4.Update;
	kilepes			:= true;
end;

procedure TRB_ExportDlg.BitBtn5Click(Sender: TObject);
begin
	// T�bb vev� kiv�laszt�sa
	Application.CreateForm(TValvevoDlg, ValvevoDlg);
	ValvevoDlg.Tolt(M50.Text);
	ValvevoDlg.ShowModal;
	if ValvevoDlg.kodkilist.Count > 0 then begin
		M5.Text		:= ValvevoDlg.nevlista;
		M50.Text	:= ValvevoDlg.kodlista;
	end;
	ValvevoDlg.Destroy;
end;

end.
