unit GkKorKm;

interface

uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls, Egyeb,
  Forms, Dialogs, DBTables, DB, StdCtrls, Buttons, Mask, DBCtrls,
  ExtCtrls;

type
  TGkKorKmDlg = class(TForm)
    BitBtn4: TBitBtn;
    BitBtn6: TBitBtn;
    CheckBox1: TCheckBox;
    RadioButton1: TRadioButton;
    RadioButton2: TRadioButton;
    RadioButton3: TRadioButton;
    procedure BitBtn4Click(Sender: TObject);
    procedure BitBtn6Click(Sender: TObject);
  private
  public
  end;

var
  GkKorKmDlg: TGkKorKmDlg;

implementation

uses
	Kozos, Kozos_Local, GkKorKmList;
{$R *.DFM}

procedure TGkKorKmDlg.BitBtn4Click(Sender: TObject);
begin
	{A lista �ssze�ll�t�sa a sz�r�si elemek alapj�n}
	Screen.Cursor	:= crHourGlass;
	Application.CreateForm(TGkKorKmlisDlg, GkKorKmlisDlg);
	GkKorKmlisDlg.Tolt(CheckBox1.Checked);
	Screen.Cursor	:= crDefault;
	if GkKorKmlisDlg.vanadat	then begin
		GkKorKmlisDlg.Rep.Preview;
	end else begin
		NoticeKi('Nincs a megadott felt�teleknek megfelel� t�tel!');
	end;
	GkKorKmlisDlg.Destroy;
end;

procedure TGkKorKmDlg.BitBtn6Click(Sender: TObject);
begin
	Close;
end;

end.


