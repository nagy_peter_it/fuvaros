unit Kozos_Export;

interface

uses
	SysUtils, Graphics, Mask, IniFiles, Forms, Classes, StdCtrls, Grids, DateUtils, Shellapi,
	Comobj, Variants , Comctrls, WinTypes, ADODB, QuickRpt, QRPDFFilt;

	function	GetKobSuly( mbkod : string) : double;
	function	GetSuly( mbkod : string) : double;
	function	GetPaletta( mbkod : string) : double;
	procedure SorLogolas(grid : TStringGrid; kod: string; suly, ossz, potlek : double; tipus : integer);
	procedure KezdoErtekek(var m1, m2, m3, m4 : TMaskEdit);
  function DatumAngolosra(S: string): string;
  function CalendarWeek(S: string): integer;
  procedure QuickReportExport(aReport: TQuickRep; const aFileName: TFileName);

implementation

uses
	Notice, Info, Egyeb, J_SQL, J_EXPORT, KOZOS;


function	GetSuly( mbkod : string) : double;
var
	Query11		: TADOQuery;
begin
	Query11		:= TADOQuery.Create(nil);
	Query11.Tag 	:= 1;
	EgyebDlg.SeTADOQueryDatabase(Query11);
	Query_Run(Query11, 'SELECT SUM(MS_FSULY) SULY FROM MEGSEGED WHERE MS_MBKOD = '+mbkod+' AND MS_TIPUS = 0 AND MS_FAJKO = 0 ');
	Result		:= StringSzam(Query11.FieldByName('SULY').AsString);
end;

function	GetKobSuly( mbkod : string) : double;
var
	Query11		: TADOQuery;
   suly        : double;
   kobos_megbizas: boolean;
begin
	Query11		:= TADOQuery.Create(nil);
	Query11.Tag 	:= 1;
	EgyebDlg.SeTADOQueryDatabase(Query11);
	Query_Run(Query11, 'SELECT MB_KOBOS FROM MEGBIZAS where MB_MBKOD= '+mbkod);
  if Query11.FieldByName('MB_KOBOS').AsString = '1' then
    kobos_megbizas:= True
  else kobos_megbizas:= False;
	Query_Run(Query11, 'SELECT MS_FEKOB, MS_FSULY FROM MEGSEGED WHERE MS_MBKOD = '+mbkod+' AND MS_TIPUS = 0 AND MS_FAJKO = 0 ');
   suly    := 0;
   while not Query11.Eof do begin
       if kobos_megbizas and (StringSZam(Query11.FieldByName('MS_FEKOB').AsString) > 0) then begin
          suly := suly + StringSZam(Query11.FieldByName('MS_FEKOB').AsString)
          end
       else begin
          suly := suly + StringSZam(Query11.FieldByName('MS_FSULY').AsString)
          end;
       Query11.Next;
   end;
	Result		:= suly;
end;

{function	GetKobSuly( mbkod : string) : double;
var
	Query11		: TADOQuery;
   suly        : double;
begin
	Query11		:= TADOQuery.Create(nil);
	Query11.Tag 	:= 1;
	EgyebDlg.SeTADOQueryDatabase(Query11);
	Query_Run(Query11, 'SELECT * FROM MEGSEGED WHERE MS_MBKOD = '+mbkod+' AND MS_TIPUS = 0 AND MS_FAJKO = 0 ');
   suly    := 0;
   while not Query11.Eof do begin
       if StringSZam(Query11.FieldByName('MS_FEKOB').AsString) = 0 then begin
           suly := suly + StringSZam(Query11.FieldByName('MS_FSULY').AsString)
       end else begin
           suly := suly + StringSZam(Query11.FieldByName('MS_FEKOB').AsString)
       end;
       Query11.Next;
   end;
	Result		:= suly;
end;
}

function	GetPaletta( mbkod : string) : double;
var
	Query11		: TADOQuery;
begin
	Query11		:= TADOQuery.Create(nil);
	Query11.Tag 	:= 1;
	EgyebDlg.SeTADOQueryDatabase(Query11);
	Query_Run(Query11, 'SELECT SUM(MS_FEPAL) PALETTA FROM MEGSEGED WHERE MS_MBKOD = '+mbkod+' AND MS_TIPUS = 0 AND MS_FAJKO = 0 ');
	Result		:= StringSzam(Query11.FieldByName('PALETTA').AsString);
end;

procedure SorLogolas(grid : TStringGrid; kod: string; suly, ossz, potlek : double; tipus : integer);
var
	sorsz	: integer;
begin
	// Ide j�n a sorok berak�sa a t�bl�zatba
	if grid = nil then begin
		Exit;
	end;
	sorsz	:= grid.Cols[0].IndexOf(kod);
	if sorsz < 0 then begin
		// �j sor besz�r�sa
		if grid.Cells[0,0] <> '' then begin
			grid.RowCount := grid.RowCount + 1;
		end;
		sorsz							:= grid.RowCount - 1;
	end;
	grid.Cells[0,sorsz]				:= kod;
	grid.Cells[1+(tipus-1)*3,sorsz] := Format('%.2f', [suly]);
	grid.Cells[2+(tipus-1)*3,sorsz] := Format('%.2f', [ossz]);
	grid.Cells[3+(tipus-1)*3,sorsz] := Format('%.2f', [potlek]);
end;

procedure   KezdoErtekek(var m1, m2, m3, m4 : TMaskEdit);
begin
	// A "Tyco Electronics Logistics AG" vev� kiv�laszt�sa
	m3.Text		:= 'Tyco Electronics Logistics AG';
	m4.Text		:= Query_select('VEVO', 'V_NEV', M3.Text, 'V_KOD');
	m2.Text		:= DatumHozNap(copy(EgyebDlg.MaiDatum, 1, 8)+'01.', -1, true);
	m1.Text		:= copy(M2.Text, 1, 8)+'01.'
end;

// bemenet: "Fuvaros" d�tum, "2015.08.31." form�ban, esetleg a z�r� pont n�lk�l
function DatumAngolosra(S: string): string;
begin
  if ((length(S)=10) or (length(S)=11))
    and (copy(S,5,1)='.') and (copy(S,8,1)='.') then  // ha "d�tumszer�"
      Result:= copy(S,9,2)+'/'+copy(S,6,2)+'/'+copy(S,1,4)
  else Result:= S;
end;

function CalendarWeek(S: string): integer;
var
  Y,M,D: Word;
begin
 if ((length(S)=10) or (length(S)=11)) and (copy(S,5,1)='.') and (copy(S,8,1)='.') then begin // ha "d�tumszer�"
   Y:=StrToInt(copy(S,1,4));
   M:=StrToInt(copy(S,6,2));
   D:=StrToInt(copy(S,9,2));
   Result:=WeekOfTheYear(EncodeDate(Y,M,D));
   end
 else begin
   Result:= -1;
   end;
end;

procedure QuickReportExport(aReport: TQuickRep; const aFileName: TFileName);
var
  aPDF : TQRPDFDocumentFilter;
begin
  // QuickReportExport(Rep, 'W:\LIST\szla.pdf');
  // Rep.PrintToPDF ('W:\LIST\szla.pdf');
  aPDF := TQRPDFDocumentFilter.Create(aFileName);
  // aPDF.FontHandling := fhAutoEmbed;
  aReport.ExportToFilter( aPDF);
  aPDF.Free;
end;

end.
