unit Kombi_Export;

interface

uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls, Egyeb,
  Forms, Dialogs, DBTables, DB, StdCtrls, Buttons, Mask, DBCtrls,
  Printers, Shellapi, ExtCtrls, ADODB, J_Excel, Grids;

type
  TKombi_ExportDlg = class(TForm)
    Label1: TLABEL;
    BitBtn4: TBitBtn;
    BitBtn6: TBitBtn;
    M1: TMaskEdit;
	 M2: TMaskEdit;
	 Label2: TLabel;
	 Label3: TLabel;
	 BitBtn10: TBitBtn;
	 BitBtn1: TBitBtn;
	 Query1: TADOQuery;
    Label4: TLabel;
	 Query2: TADOQuery;
	 Query3: TADOQuery;
    M50: TMaskEdit;
    M5: TMaskEdit;
    BitBtn5: TBitBtn;
	 Label6: TLabel;
    M3: TMaskEdit;
    CheckBox1: TCheckBox;
    CheckBox2: TCheckBox;
    CheckBox3: TCheckBox;
    CheckBox4: TCheckBox;
    Label5: TLabel;
	 M4: TMaskEdit;
	 CheckBox5: TCheckBox;
    SG1: TStringGrid;
    CheckBox6: TCheckBox;
	 procedure FormCreate(Sender: TObject);
	 procedure BitBtn4Click(Sender: TObject);
	 procedure BitBtn6Click(Sender: TObject);
	 procedure M1Click(Sender: TObject);
	 procedure BitBtn10Click(Sender: TObject);
	 procedure BitBtn1Click(Sender: TObject);
	 procedure M1Exit(Sender: TObject);
	 procedure BitBtn5Click(Sender: TObject);
	 procedure CheckBox1Click(Sender: TObject);
	 procedure CheckBox4Click(Sender: TObject);
  private
		kilepes		: boolean;
		kellshell	: boolean;
  public
    belf_alval, kellaszamla :string;
  end;

var
  Kombi_ExportDlg: TKombi_ExportDlg;

implementation

uses
	J_sql, Kozos, ValVevo, J_DataModule, Kozos_Export, MSR_Export, KPI_Export, ADR30_Export, XML_Export;
{$R *.DFM}

procedure TKombi_ExportDlg.FormCreate(Sender: TObject);
begin
 // belf_alval:='0607';
  belf_alval	:= EgyebDlg.Read_SZGrid( '999', '748' );
	kilepes     := true;
	kellshell	:= true;
	EgyebDlg.SetADOQueryDatabase(Query1);
	EgyebDlg.SetADOQueryDatabase(Query2);
	EgyebDlg.SetADOQueryDatabase(Query3);
	// A "Tyco Electronics Logistics AG" vev� kiv�laszt�sa
	M2.Text 	:= EgyebDlg.MaiDatum;
	M3.Text		:= 'Izs� L�szl�';
	//M5.Text	 	:= 'Tyco Electronics Logistics AG';
	M50.Text 	:= Query_select('VEVO', 'V_NEV', M5.Text, 'V_KOD');
	M2.Text	 	:= DatumHozNap(copy(EgyebDlg.MaiDatum, 1, 8)+'01.', -1, true);
	M1.Text	 	:= copy(M2.Text, 1, 8)+'01.';
	CheckBox1Click(Sender);
	CheckBox4Click(Sender);
end;

procedure TKombi_ExportDlg.BitBtn4Click(Sender: TObject);
var
	kilepett	: boolean;
	fn			: String;
	i			: integer;
	j			: integer;
  o1,o2,o3:string;
	osszok		: array [1..12] of double;
begin
	if not DatumExit(M1, false) then begin
		Exit;
	end;
	if not DatumExit(M2, false) then begin
		Exit;
	end;
	if M50.Text = '' then begin
		NoticeKi('A megb�z� kiv�laszt�sa k�telez�!');
		M5.SetFocus;
		Exit;
	end;

	// Az export�l�sok v�grehajt�sa
	SG1.RowCount    := 1;
	SG1.Rows[0].Clear;
	SG1.Cells[0,0]	:= 'Megb�z�sk�d';
	SG1.Cells[1,0]	:= 'MSR s�ly';
	SG1.Cells[2,0]	:= 'MSR fuvar';
	SG1.Cells[3,0]	:= 'MSR p�tl�k';
	SG1.Cells[4,0]	:= 'KPI s�ly';
	SG1.Cells[5,0]	:= 'KPI fuvar';
	SG1.Cells[6,0]	:= 'KPI p�tl�k';
	SG1.Cells[7,0]	:= 'ADR s�ly';
	SG1.Cells[8,0]	:= 'ADR fuvar';
	SG1.Cells[9,0]	:= 'ADR p�tl�k';
	SG1.Cells[10,0]	:= 'XML s�ly';
	SG1.Cells[11,0]	:= 'XML fuvar';
	SG1.Cells[12,0]	:= 'XML p�tl�k';
	SG1.Cells[13,0]	:= 'Eredm�ny';

	// A dial�gus elt�ntet�se
	Kombi_ExportDlg.Hide;

	kilepett		:= false;
	if CheckBox1.Checked then begin
		//Az MS riport v�grehajt�sa
		Application.CreateForm(TMSR_ExportDlg, MSR_ExportDlg);
       MSR_ExportDlg.kelluzpotlek:= CheckBox6.Checked;
		MSR_ExportDlg.kombinalt			:= true;
		MSR_ExportDlg.kellshell			:= false;
		MSR_ExportDlg.kelluzenet		:= CheckBox5.Checked;
		MSR_ExportDlg.gr				:= SG1;
		MSR_ExportDlg.M5.Text			:= M5.Text;
		MSR_ExportDlg.M50.Text			:= M50.Text;
		MSR_ExportDlg.M1.Text			:= M1.Text;
		MSR_ExportDlg.M2.Text			:= M2.Text;
		MSR_ExportDlg.M4.Text			:= M3.Text;
		SetMaskEdits([MSR_ExportDlg.M1, MSR_ExportDlg.M2, MSR_ExportDlg.M4]);
		MSR_ExportDlg.BitBtn1.Enabled	:= false;
		MSR_ExportDlg.BitBtn5.Enabled	:= false;
		MSR_ExportDlg.BitBtn10.Enabled	:= false;
		MSR_ExportDlg.Show;
		MSR_ExportDlg.XLS_Generalas;
		kilepett	:= MSR_ExportDlg.voltkilepes;
		MSR_ExportDlg.Destroy;
	end;
	if kilepett then begin
		Kombi_ExportDlg.Show;
		Exit;
	end;
	if CheckBox2.Checked then begin
		//A KPI export v�grehajt�sa
		Application.CreateForm(TKPI_ExportDlg, KPI_ExportDlg);
		KPI_ExportDlg.kombinalt			:= true;
		KPI_ExportDlg.kellshell			:= false;
		KPI_ExportDlg.kelluzenet		:= CheckBox5.Checked;
		KPI_ExportDlg.gr				:= SG1;
		KPI_ExportDlg.M5.Text			:= M5.Text;
		KPI_ExportDlg.M50.Text			:= M50.Text;
		KPI_ExportDlg.M1.Text			:= M1.Text;
		KPI_ExportDlg.M2.Text			:= M2.Text;
		SetMaskEdits([KPI_ExportDlg.M1, KPI_ExportDlg.M2]);
		KPI_ExportDlg.BitBtn1.Enabled	:= false;
		KPI_ExportDlg.BitBtn5.Enabled	:= false;
		KPI_ExportDlg.BitBtn10.Enabled	:= false;
		KPI_ExportDlg.Show;
		KPI_ExportDlg.XLS_Generalas;
		kilepett	:= KPI_ExportDlg.voltkilepes;
		KPI_ExportDlg.Destroy;
	end;
	if kilepett then begin
		Kombi_ExportDlg.Show;
		Exit;
	end;
	if CheckBox3.Checked then begin
		//A KPI export v�grehajt�sa
		Application.CreateForm(TADR30_ExportDlg, ADR30_ExportDlg);
		ADR30_ExportDlg.kombinalt			:= true;
		ADR30_ExportDlg.kelluzenet			:= CheckBox5.Checked;
		ADR30_ExportDlg.gr					:= SG1;
		ADR30_ExportDlg.M5.Text				:= M5.Text;
		ADR30_ExportDlg.M50.Text			:= M50.Text;
		ADR30_ExportDlg.M1.Text				:= M1.Text;
		ADR30_ExportDlg.M2.Text				:= M2.Text;
		SetMaskEdits([ADR30_ExportDlg.M1, ADR30_ExportDlg.M2]);
		ADR30_ExportDlg.BitBtn1.Enabled		:= false;
		ADR30_ExportDlg.BitBtn5.Enabled		:= false;
		ADR30_ExportDlg.BitBtn10.Enabled	:= false;
		ADR30_ExportDlg.Show;
		ADR30_ExportDlg.ADR_Generalas;
		kilepett	:= ADR30_ExportDlg.voltkilepes;
		ADR30_ExportDlg.Destroy;
	end;
	if kilepett then begin
		Kombi_ExportDlg.Show;
		Exit;
	end;
	if CheckBox4.Checked then begin
		//Az XML export v�grehajt�sa
		Application.CreateForm(TXML_ExportDlg, XML_ExportDlg);
		XML_ExportDlg.kombinalt			:= true;
		XML_ExportDlg.kellshell			:= false;
		XML_ExportDlg.kelluzenet		:= CheckBox5.Checked;
		XML_ExportDlg.gr				:= SG1;
		XML_ExportDlg.M5.Text			:= M5.Text;
		XML_ExportDlg.M50.Text			:= M50.Text;
		XML_ExportDlg.M1.Text			:= M1.Text;
		XML_ExportDlg.M2.Text			:= M2.Text;
		XML_ExportDlg.M3.Text			:= M4.Text;
		SetMaskEdits([XML_ExportDlg.M1, XML_ExportDlg.M2, XML_ExportDlg.M3]);
		XML_ExportDlg.BitBtn1.Enabled	:= false;
		XML_ExportDlg.BitBtn5.Enabled	:= false;
		XML_ExportDlg.BitBtn10.Enabled	:= false;
		XML_ExportDlg.Show;
		XML_ExportDlg.XML_Generalas;
		kilepett	:= XML_ExportDlg.voltkilepes;
		XML_ExportDlg.Destroy;
	end;
	if kilepett then begin
		Kombi_ExportDlg.Show;
		Exit;
	end;
	// V�gigmegy�nk az eredm�nyt�bl�zaton �s megn�zz�k az elt�r�seket
	for i := 1 to SG1.RowCount - 1 do begin
		for j := 1 to 3 do begin
//			if ( ( SG1.Cells[j, i] <> SG1.Cells[j+3, i] ) or ( SG1.Cells[j, i] <> SG1.Cells[j+6, i] )  or ( SG1.Cells[j, i] <> SG1.Cells[j+9, i] )) then begin
			if ( ( SG1.Cells[j, i] <> SG1.Cells[j+3, i] ) or ( SG1.Cells[j, i] <> SG1.Cells[j+6, i] )   ) then begin
        o1:=SG1.Cells[j, i];
        o2:=SG1.Cells[j+3, i];
        o3:=SG1.Cells[j+6, i];
        o3:=SG1.Cells[j+4, i];
        o3:=SG1.Cells[j+5, i];
        o3:=SG1.Cells[j+7, i];
				SG1.Cells[13, i] := 'HIBA!!!';
			end
      else
				SG1.Cells[13, i] := 'OK';
		end;
	end;
	for i := 1 to 12 do begin
		osszok[i] := 0;
	end;
	for i := 1 to SG1.RowCount - 1 do begin
		for j := 1 to 12 do begin
			osszok[j] := osszok[j] + StringSzam(SG1.Cells[j, i]);
		end;
	end;
	SG1.RowCount := SG1.RowCount + 1;
	SG1.Cells[0,SG1.RowCount-1] := '�sszesen :';
	for i := 1 to 12 do begin
		SG1.Cells[i,SG1.RowCount-1] := Format('%.2f', [osszok[i]]);
	end;
	// Az eredm�nygrid megjelen�t�se
	fn			:= EgyebDlg.DocumentPath + 'EXPORT_EREDMENY.XLS';;
	GridExportToFile (SG1, fn);
	ShellExecute(Application.Handle,'open',PChar(fn), '', '', SW_SHOWNORMAL );
	Close;
end;

procedure TKombi_ExportDlg.BitBtn6Click(Sender: TObject);
begin
	Close;
end;

procedure TKombi_ExportDlg.M1Click(Sender: TObject);
begin
	with Sender as TMaskEdit do begin
		SelectAll;
	end;
end;

procedure TKombi_ExportDlg.BitBtn10Click(Sender: TObject);
begin
	// EgyebDlg.Calendarbe(M1);
	// EgyebDlg.ElsoNapEllenor(M1, M2);
  EgyebDlg.CalendarBe_idoszak(M1, M2);
end;

procedure TKombi_ExportDlg.BitBtn1Click(Sender: TObject);
begin
	EgyebDlg.Calendarbe(M2, True);
end;

procedure TKombi_ExportDlg.M1Exit(Sender: TObject);
begin
	DatumExit(M1,false);
	EgyebDlg.ElsoNapEllenor(M1, M2);
end;

procedure TKombi_ExportDlg.BitBtn5Click(Sender: TObject);
begin
	// T�bb vev� kiv�laszt�sa
	Application.CreateForm(TValvevoDlg, ValvevoDlg);
	ValvevoDlg.Tolt(M50.Text);
	ValvevoDlg.ShowModal;
	if ValvevoDlg.kodkilist.Count > 0 then begin
		M5.Text		:= ValvevoDlg.nevlista;
		M50.Text	:= ValvevoDlg.kodlista;
	end;
	ValvevoDlg.Destroy;
end;

procedure TKombi_ExportDlg.CheckBox1Click(Sender: TObject);
begin
	if CheckBox1.Checked then begin
		SetMaskEdits([M3], 1);
	end else begin
		SetMaskEdits([M3]);
	end;
end;

procedure TKombi_ExportDlg.CheckBox4Click(Sender: TObject);
begin
	if CheckBox4.Checked then begin
		SetMaskEdits([M4], 1);
	end else begin
		SetMaskEdits([M4]);
	end;
end;

end.
