unit GooglePoints;
{******************************************************************************
   Aloszt�ly a t�vols�g meghat�roz�s�hoz
*******************************************************************************}

interface

uses
   Classes, SysUtils, Variants,  Vcl.ExtCtrls;
const
   MaxPoints = 10;  // free API limit: 100 elements per query.
type
   TPositions = array[1..MaxPoints, 1..3] of string;  // az �tvonalpontok helye: sz�veges megad�s eset�n 1: orsz�g-v�ros, 2: utca-h�zsz�m  3:Felismert utca-h�zsz�m (y/n) (az els� k�rben t�ltj�k ki)
   TLabels = array[1..MaxPoints] of string;  // az �tvonalpontok megnevez�se

type
   TPoints = Class (TComponent)
      public
         // constructor Create(AOwner: TComponent); override;
         // destructor Destroy; override;
         LabelList: TLabels;
         PositionList: TPositions;
         PointCount: integer;
         function AddPointAddr(AddressS, UtcaS, Nev: string): integer;
         function AddPointCoord(Lat, Long: double; Nev: string): integer;
         procedure ClearPoints;
      end;
implementation

procedure TPoints.ClearPoints;
var
   i, j: integer;
begin
   PointCount:=0;
   // hogy ne maradjon benne szem�t
   for i:=1 to MaxPoints do begin
      LabelList[i]:= '';
      for j:=1 to 3 do
         PositionList[i,j]:= '';
      end;  // for
end;

function TPoints.AddPointAddr(AddressS, UtcaS, Nev: string): integer;
begin
   AddressS:=StringReplace(AddressS, ' ', '+', [rfReplaceAll]);  // lehet hogy orsz�g k�d->n�v �talak�t�s is kell
   UtcaS:=StringReplace(UtcaS, ' ', '+', [rfReplaceAll]);
   Inc(PointCount);
   if PointCount <= MaxPoints then begin
     PositionList[PointCount,1]:=AddressS;
     PositionList[PointCount,2]:=UtcaS;
     PositionList[PointCount,3]:='y';  // default good address
     LabelList[PointCount]:=Nev;
     Result:=0;  // OK
     end
   else begin
     Result:=-1  // t�l sok pont
     end;
end;

function TPoints.AddPointCoord(Lat, Long: double; Nev: string): integer;
var
   AddressS: string;
   MyFormatSetings: TFormatSettings;
begin
   MyFormatSetings:= TFormatSettings.Create;
   MyFormatSetings.DecimalSeparator:='.';
   AddressS:=FormatFloat('000.000000000000', Lat, MyFormatSetings);
   AddressS:=AddressS + ','+ FormatFloat('000.000000000000', Long, MyFormatSetings);
   Inc(PointCount);
   if PointCount <= MaxPoints then begin
     PositionList[PointCount,1]:=AddressS;
     PositionList[PointCount,2]:='';  // itt nincs tov�bbi info
     PositionList[PointCount,3]:='y';  // default good address
     LabelList[PointCount]:=Nev;
     Result:=0;  // OK
     end
   else begin
     Result:=-1  // t�l sok pont
     end;
end;

end.

