unit UniGraphInfoForm;

interface

uses
	SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
	Forms, Dialogs, StdCtrls, Buttons, Mask, DB, DBTables, J_ALFORM, ADODB,
  Vcl.ImgList, acPNG, Vcl.ExtCtrls;

type
	TUniGraphInfoFormDlg = class(TJ_AlformDlg)
    Image1: TImage;
    Image2: TImage;
    procedure Image1Click(Sender: TObject);
	private
	public
    procedure SetPicture(Fejlec: string; PicIndex: integer);
	end;

var
	UniGraphInfoFormDlg: TUniGraphInfoFormDlg;

implementation

uses
 Egyeb, J_SQL, Kozos, Clipbrd;

{$R *.DFM}



procedure TUniGraphInfoFormDlg.Image1Click(Sender: TObject);
begin
  Close;
end;

procedure TUniGraphInfoFormDlg.SetPicture(Fejlec: string; PicIndex: integer);
const
  WidthOffset = 4;
  HeightOffset = 29;  // a fejl�c miatt nagyobb
var
  MyPic: TBitmap;
begin
  caption:= Fejlec;
  case PicIndex of
    1: begin
      Image1.Left:= 0;
      Image1.Top:= 0;
      Image1.Visible:= True;
      Width:= Image1.Width + WidthOffset;
      Height:= Image1.Height + HeightOffset;
      end;
    2: begin
      Image2.Left:= 0;
      Image2.Top:= 0;
      Image2.Visible:= True;
      Width:= Image2.Width + WidthOffset;
      Height:= Image2.Height + HeightOffset;
      end;
    end;  // case
end;

end.




