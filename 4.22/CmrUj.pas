	unit Cmruj;

interface

uses
	SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
	Forms, Dialogs, StdCtrls, Buttons, Mask, DB, DBTables, J_ALFORM, ADODB,
  ExtCtrls;

type
	TCmrUjDlg = class(TJ_AlformDlg)
	BitElkuld: TBitBtn;
	BitKilep: TBitBtn;
    Query1: TADOQuery;
    Label17: TLabel;
    Label18: TLabel;
    M1: TMaskEdit;
    M2: TMaskEdit;
    CB1: TCheckBox;
	procedure BitKilepClick(Sender: TObject);
	procedure FormCreate(Sender: TObject);
	procedure Tolto(cim : string; teko : string); override;
	procedure BitElkuldClick(Sender: TObject);
  	procedure MaskEditClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure M3KeyPress(Sender: TObject; var Key: Char);
    procedure M24AExit(Sender: TObject);
	private
	public
	end;

var
	CmrUjDlg: TCmrUjDlg;

implementation

uses
	J_VALASZTO, Egyeb, J_SQL, Kozos;
{$R *.DFM}

procedure TCmrUjDlg.BitKilepClick(Sender: TObject);
begin
	ret_kod := '';
	Close;
end;

procedure TCmrUjDlg.FormCreate(Sender: TObject);
begin
	EgyebDlg.SetADOQueryDatabase(Query1);
	ret_kod 	:= '';
end;

procedure TCmrUjDlg.Tolto(cim : string; teko : string); 
begin
	ret_kod  	:= '';
	Caption  	:= cim;
end;

procedure TCmrUjDlg.BitElkuldClick(Sender: TObject);
var
	returnstr	: string;
	kezdostr	: string;
	vegzostr	: string;
	sorsz		: integer;
	ujkod		: integer;
	masjel		: string;
	vegestr		: string;
	vankozte	: boolean;
	str			: string;
	i			: integer;
	letezocmr	: string;
begin
	if M1.Text = '' then begin
		NoticeKi('A kezd� sorsz�m megad�sa k�telez�!');
		M1.SetFocus;
		Exit;
	end;
	if M2.Text = '' then begin
		NoticeKi('A befejez� sorsz�m megad�sa k�telez�!');
		M2.SetFocus;
		Exit;
	end;
	if StringSzam(M1.Text) > StringSzam(M2.Text) then begin
		NoticeKi('Rossz sorsz�mok megad�sa! A befejez�nek nagyobbnak kell lennie a kezd�n�l!');
		M1.Setfocus;
		Exit;
	end;
	if CB1.Checked then begin
(*
		if StringSzam(M2.Text) > 999 then begin
			NoticeKi('T�l nagy sorsz�m!');
			M2.Setfocus;
			Exit;
		end;
*)
	end;
	vegestr	:= '/'+copy(EgyebDlg.EvSzam, 3, 2);
	if CB1.Checked then begin
		// Idegen CMR k�dj�nak gener�l�sa
		kezdostr	:= Format('%3d'+vegestr, [StrToIntDef(M1.Text, 0)]);
		vegzostr	:= Format('%3d'+vegestr, [StrToIntDef(M2.Text, 0)]);
	end else begin
		// Saj�t CMR k�dj�nak gener�l�sa
		kezdostr	:= Format('%7.7d', [StrToIntDef(M1.Text, 0)]);
		vegzostr	:= Format('%7.7d', [StrToIntDef(M2.Text, 0)]);
	end;
	Screen.Cursor	:= crHourGlass;
	i := StrToIntDef(M1.Text, 0);
	while i <= StrToIntDef(M2.Text, 0) do begin
		if CB1.Checked then begin
			//Idegen CMR-n�l az �vsz�mok is fontosak
			str	:= Format('%3d'+vegestr, [i]);
		end else begin
			// Saj�t CMR-n�l csak a sz�m sz�m�t
			str	:= Format('%7.7d', [i]);
		end;
		Query_Run (Query1, 'SELECT CM_CSZAM FROM CMRDAT WHERE CM_CSZAM = '''+str+''' ');
		if Query1.RecordCount > 0 then begin
			vankozte	:= true;
			letezocmr	:= str;
			i	:= StrToIntDef(M2.Text, 0);
		end;
		Inc(i);
	end;
	Screen.Cursor	:= crDefault;
	if vankozte then begin
		NoticeKi('A megadott intervallumban m�r l�tezik CMR sz�m! ('+letezocmr+')');
		M1.Setfocus;
		Exit;
	end;
	if StrToIntDef(M2.Text, 0) - StrToIntDef(M1.Text, 0) > 100 then begin
		if NoticeKi('�n 100-n�l t�bb CMR-t k�v�n l�trehozni. Folytatja?', NOT_QUESTION) <> 0 then begin
			Exit;
		end;
	end;
	{�j rekordok l�trehoz�sa}
	ujkod		:= GetNextCode('CMRDAT', 'CM_CMKOD');
	for sorsz 	:= StrToIntDef(M1.Text, 0) to StrToIntDef(M2.Text, 0) do begin
		if CB1.Checked then begin
			// Idegen CMR k�dj�nak gener�l�sa
			returnstr	:= Format('%3d'+vegestr, [sorsz]);
		end else begin
			// Saj�t CMR k�dj�nak gener�l�sa
			returnstr	:= Format('%7.7d', [sorsz]);
		end;
		masjel	:= '0';
		if CB1.Checked then begin
			masjel	:= '1';
		end;
		Query_Insert (Query1, 'CMRDAT',
			[
			'CM_CMKOD', IntToStr(ujkod),
			'CM_CSZAM', ''''+returnstr+'''',
			'CM_CRDAT', ''''+EgyebDlg.MaiDatum+'''',
			'CM_DOKOD', ''''+''+'''',
			'CM_KIDAT', ''''+''+'''',
			'CM_VIDAT', ''''+''+'''',
			'CM_JAKOD', ''''+''+'''',
			'CM_JARAT', ''''+''+'''',
			'CM_MEGJE', ''''+''+'''',
			'CM_VIKOD', ''''+''+'''',
			'CM_DONEV', ''''+''+'''',
			'CM_MASIK', masjel
			]);
		Inc(ujkod);
	end;
	// ret_kod := returnstr;
  ret_kod := IntToStr(ujkod);  // ezzel keresi a "ModLocate (Query1, FOMEZO, AlForm.ret_kod );"
	Close;
end;

procedure TCmrUjDlg.MaskEditClick(Sender: TObject);
begin
	with Sender as TMaskEdit do begin
  		SelectAll;
  	end;
end;

procedure TCmrUjDlg.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
	EgyebDlg.FormReturn(Key);
end;

procedure TCmrUjDlg.M3KeyPress(Sender: TObject; var Key: Char);
begin
  with Sender as TMaskEdit do begin
  		Key := EgyebDlg.Jochar(Text,Key,(Sender as TMaskEdit).MaxLength,(Sender as TMaskEdit).Tag);
  end;
end;

procedure TCmrUjDlg.M24AExit(Sender: TObject);
begin
	SzamExit(Sender);
end;

end.
