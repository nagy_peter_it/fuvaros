	unit Oszamlabe;

interface

uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, StdCtrls, Buttons, Mask, Grids, DB, DBTables, J_ALFORM,
  ADODB, ExtCtrls,OSzamSorBe;

type
  TOSzamlaBeDlg = class(TJ_AlformDlg)
    GroupBox4: TGroupBox;
    Panel1: TPanel;
    ButtonKuld: TBitBtn;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    MaskEdit1: TMaskEdit;
    MaskEdit2: TMaskEdit;
    MaskEdit3: TMaskEdit;
    MaskEdit4: TMaskEdit;
    ButValVevo: TBitBtn;
    MaskEdit5: TMaskEdit;
    GroupBox2: TGroupBox;
    Label2: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label13: TLabel;
    Label12: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    MaskEdit6: TMaskEdit;
    MaskEdit7: TMaskEdit;
    MaskEdit8: TMaskEdit;
    ComboBox1: TComboBox;
    MaskEdit13: TMaskEdit;
    MaskEdit12: TMaskEdit;
    MaskEdit14: TMaskEdit;
    MaskEdit15: TMaskEdit;
    BitBtn4: TBitBtn;
    BitBtn10: TBitBtn;
    BitBtn8: TBitBtn;
    BitBtn9: TBitBtn;
    BitBtn6: TBitBtn;
    BitBtn5: TBitBtn;
    QSor: TADOQuery;
    QFej: TADOQuery;
    Query2: TADOQuery;
    Query1: TADOQuery;
    Query3: TADOQuery;
    Query4: TADOQuery;
    Panel2: TPanel;
    ButtonKilep: TBitBtn;
    GroupBox3: TGroupBox;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    MaskEdit10: TMaskEdit;
    MaskEdit11: TMaskEdit;
    MaskEdit9: TMaskEdit;
    Panel3: TPanel;
    BitBtn1: TBitBtn;
    BitBtn3: TBitBtn;
    Panel4: TPanel;
    SorGrid: TStringGrid;
    Label11: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    MaskEdit100: TMaskEdit;
    MaskEdit110: TMaskEdit;
    MaskEdit90: TMaskEdit;
    Label18: TLabel;
    Label19: TLabel;
    ButtonMod: TBitBtn;
    CheckBox7: TCheckBox;
    Label20: TLabel;
    MaskEdit16: TMaskEdit;
    Label21: TLabel;
    SpeedButton3: TSpeedButton;
    SpeedButton4: TSpeedButton;
    SpeedButton5: TSpeedButton;
    SpeedButton6: TSpeedButton;
    Button1: TButton;
    Query22: TADOQuery;
    ComboBox2: TComboBox;
    cbNyelv: TComboBox;
    Label40: TLabel;
    CheckBox3: TCheckBox;
    ebVEKOD: TEdit;
    Label24: TLabel;
    OrszagCombo: TComboBox;
    procedure ButtonKilepClick(Sender: TObject);
    procedure Tolto(cim : string; teko : string); override;
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure ButValVevoClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure ButtonKuldClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
	  procedure Osszesit;
	 procedure Modosit(Sender: TObject);
	 procedure MaskEdit7Exit(Sender: TObject);
	 procedure BitBtn4Click(Sender: TObject);
	 procedure BitBtn5Click(Sender: TObject);
	 function 	Elkuldes : boolean;
	 procedure FormKeyDown(Sender: TObject; var Key: Word;
	   Shift: TShiftState);
	 procedure BitBtn10Click(Sender: TObject);
	 procedure BitBtn8Click(Sender: TObject);
	 procedure BitBtn9Click(Sender: TObject);
	 procedure GridTolto(arf_ujraszamol:boolean);
	 procedure BitBtn6Click(Sender: TObject);
	 procedure MaskEdit16Click(Sender: TObject);
	 procedure Tiltas;
    procedure ButtonModClick(Sender: TObject);
    procedure MaskEdit8Change(Sender: TObject);
    procedure MaskEdit7Change(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure SpeedButton5Click(Sender: TObject);
    procedure MaskEdit8Exit(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure SorGridSelectCell(Sender: TObject; ACol, ARow: Integer;
      var CanSelect: Boolean);
    procedure ComboBox2Change(Sender: TObject);
    procedure cbNyelvChange(Sender: TObject);
    procedure NyelvFrissit;
  private
		modlist 	: TStringList;
    nyelvlist: TStringList;
    orszaglist : TStringLIst;
		modosult 	: Boolean;
		fej1, fej2, fej3, fej4, fej5, fej6, fej7 	: string;
		mell1, mell2, mell3, mell4, mell5 	: string;
		lejarat		: integer;
		fejuj13, fejuj14, fejuj15, fejuj23, fejuj24, fejuj25, fejuj26, TDATUM : string;
    vevobank    : boolean;
	public
		vevokod		: string;
  end;

var
  OSzamlaBeDlg: TOSzamlaBeDlg;

const
	GRIDOSZLOP	= 13 ;
implementation

uses
  Mellek, J_SQL, Egyeb, VevoUjfm, Kozos, SzamlaNezo, Szamlabe, Windows;
//  SzamPe02, Szmutat,

{$R *.DFM}

procedure TOSzamlaBeDlg.ButtonKilepClick(Sender: TObject);
begin
	ret_kod := '';
	if modosult then begin
		if NoticeKi('M�dosultak az adatok . Ki akar l�pni ment�s n�lk�l?', NOT_QUESTION) = 0 then begin
			Close;
		end;
	end else begin
		Close;
	end;
end;

procedure TOSzamlaBeDlg.Tolto(cim : string; teko:string);
begin
	ret_kod 					:= teko;
	Caption 					:= cim;
	MaskEdit1.Text 		:= '';
	MaskEdit2.Text 		:= '';
	MaskEdit3.Text 		:= '';
	MaskEdit4.Text 		:= '';
	MaskEdit5.Text 		:= '';
	MaskEdit6.Text 		:= EgyebDlg.MaiDatum;
  if ret_kod='' then
  begin
  	MaskEdit7.Text 		:= EgyebDlg.MaiDatum;
  	MaskEdit8.Text 		:= DatumHoznap(MaskEdit7.Text, lejarat, true);
  end;
	MaskEdit9.Text 		:= '';
	MaskEdit10.Text 		:= '';
	MaskEdit11.Text 		:= '';
	MaskEdit12.Text 		:= '';
	MaskEdit13.Text 		:= '';
	MaskEdit14.Text 		:= '';
	MaskEdit15.Text 		:= '';
  if ret_kod <> '' then begin
		if Query_Run (QFej, 'SELECT * FROM SZFEJ2 WHERE SA_UJKOD = '+ret_kod ,true) then begin
			if QFej.RecordCount > 0 then begin
				vevokod				:= QFej.FieldByName('SA_VEVOKOD').AsString;
        //vevobank:= Query_Select('VEVO','V_KOD',vevokod,'VE_SBANK1')<>'';
        vevobank:= Query_Select('VEVO','V_KOD',vevokod,'VE_VBANK')<>'';
        ///////////////////
				if Query_Run (Query3, 'SELECT * FROM VEVO WHERE V_KOD = '''+vevokod+''' ',true) then begin
				  if StrToIntDef(Query3.FieldByName('V_LEJAR').AsString,0) > 0 then begin
						lejarat	:= StrToIntDef(Query3.FieldByName('V_LEJAR').AsString,0);
						//MaskEdit8.Text := DatumHoznap(MaskEdit7.Text, lejarat, true);
				  end;
        end;
        /////////////////////
      	CheckBox7.Checked	:=StrToIntDef( Query_Select('VEVO', 'V_KOD', vevokod, 'VE_FTELJ'),-1)>0;
				MaskEdit1.Text 		:= QFej.FieldByName('SA_VEVONEV').AsString;
        ebVEKOD.text      := vevokod;
				MaskEdit2.Text 		:= QFej.FieldByName('SA_VEIRSZ').AsString;
				MaskEdit3.Text 		:= QFej.FieldByName('SA_VEVAROS').AsString;
				MaskEdit4.Text 		:= QFej.FieldByName('SA_VECIM').AsString;
				MaskEdit5.Text 		:= QFej.FieldByName('SA_VEADO').AsString;
				Combobox1.Text		:= QFej.FieldByName('SA_FIMOD').AsString;
				MaskEdit6.Text 		:= QFej.FieldByName('SA_TEDAT').AsString;
				MaskEdit7.Text 		:= QFej.FieldByName('SA_KIDAT').AsString;
				MaskEdit8.Text 		:= QFej.FieldByName('SA_ESDAT').AsString;
				MaskEdit12.Text		:= QFej.FieldByName('SA_JARAT').AsString;
				MaskEdit13.Text 	:= QFej.FieldByName('SA_MEGJ').AsString;
				MaskEdit14.Text 	:= QFej.FieldByName('SA_POZICIO').AsString;
				MaskEdit15.Text 	:= QFej.FieldByName('SA_RSZ').AsString;
				mell1  				:= QFej.FieldByName('SA_MELL1').AsString;
				mell2  				:= QFej.FieldByName('SA_MELL2').AsString;
				mell3  				:= QFej.FieldByName('SA_MELL3').AsString;
				mell4  				:= QFej.FieldByName('SA_MELL4').AsString;
				mell5  				:= QFej.FieldByName('SA_MELL5').AsString;

               ComboSet(OrszagCombo, QFej.FieldByName('SA_ORSZ2').AsString, orszaglist);
               ComboSet (cbNyelv, 	QFej.FieldByName('SA_NYELV').AsString, nyelvlist);
				CheckBox3.Checked	:= ( QFej.FieldByName('SA_SAEUR').AsString = '1' );

				if QFej.FieldByName('SA_FEJL1').AsString <> '' then begin
				   {Van a fejl�cben valami, t�lts�k be}
				   fej1	:= QFej.FieldByName('SA_FEJL1').AsString;
				   fej2	:= QFej.FieldByName('SA_FEJL2').AsString;
				   fej3	:= QFej.FieldByName('SA_FEJL3').AsString;
				   fej4	:= QFej.FieldByName('SA_FEJL4').AsString;
				   fej5	:= QFej.FieldByName('SA_FEJL5').AsString;
				   fej6	:= QFej.FieldByName('SA_FEJL6').AsString;
				   fej7	:= QFej.FieldByName('SA_FEJL7').AsString;
				end;
        {
				// A vev�n�v alapj�n megkeress�k a vev�t
				Query_Run (Query3, 'SELECT * FROM VEVO WHERE V_NEV = '''+QFej.FieldByName('SA_VEVONEV').AsString+''' ',true);
				if Query3.RecordCount > 0 then begin
          // A vev�h�z tartoz� bank bet�lt�se
          bakod  			:= Trim(Query3.FieldByName('VE_VBANK').AsString);
					fejuj13			:= EgyebDlg.Read_SZGrid('320', bakod);
					fejuj14			:= EgyebDlg.Read_SZGrid('320', IntToStr(StrToIntDef(bakod,0) + 1));
					fejuj15			:= EgyebDlg.Read_SZGrid('320', IntToStr(StrToIntDef(bakod,0) + 2));
					fejuj23			:= EgyebDlg.Read_SZGrid('320', IntToStr(StrToIntDef(bakod,0) + 4));
					fejuj24			:= EgyebDlg.Read_SZGrid('320', IntToStr(StrToIntDef(bakod,0) + 5));
					fejuj25			:= EgyebDlg.Read_SZGrid('320', IntToStr(StrToIntDef(bakod,0) + 6));
					fejuj26			:= EgyebDlg.Read_SZGrid('320', IntToStr(StrToIntDef(bakod,0) + 7));

				end;
         }
				GridTolto(false);
				{ Enged�lyezi a m�dos�t�s �s t�rl�s buttonokat }
				if SorGrid.Cells[1,1] <> '' then begin
					BitBtn3.Enabled := true;
				end;
				Osszesit;
			end;
		end;
	end;
	modosult := false;
	if megtekint then begin
		Tiltas;
	end;
end;

procedure TOSzamlaBeDlg.BitBtn1Click(Sender: TObject);
begin
	{�j elem felvitele}
	if CheckBox7.Checked and  (MaskEdit16.Text = '') then begin
		NoticeKi('Nincs �rfolyam!');
		Exit;
	end;
	if ret_kod = '' then begin
		if not Elkuldes then begin
			Exit;
		end;
	end;
 	Application.CreateForm(TOSzamSorbeDlg, OSzamSorbeDlg);
	// OSzamSorbeDlg.Tolto(ret_kod, MaskEdit1.Text);
  OSzamSorbeDlg.Tolto2(ret_kod, MaskEdit1.Text, ebVEKOD.Text);
	OSzamSorbeDlg.ShowModal;
	if not OSzamSorbeDlg.kilep then begin
		GridTolto(CheckBox7.Checked);
		Osszesit;
	end;
  SorGrid.Row:=SorGrid.RowCount-1;
	OSzamSorbeDlg.Destroy;
end;

procedure TOSzamlaBeDlg.BitBtn3Click(Sender: TObject);
var
	szkod	: string;
  JAKOD: string;
begin
	if (  (SorGrid.Cells[1,SorGrid.Row] = '' ) or (ret_kod = '') )then begin
		Exit;
	end;
	{A j�ratsz�m ellen�rz�se}
	Query_Run (QSor, 'SELECT SA_JARAT, SA_DATUM FROM SZFEJ WHERE SA_UJKOD = '+SorGrid.Cells[1,SorGrid.Row], true);
	Query_Run (QSor, 'SELECT JA_KOD FROM JARAT WHERE JA_ALKOD = '+IntToStr(StrToIntDef(Qsor.FieldByname('SA_JARAT').AsString,0))+
       ' AND JA_JKEZD LIKE '''+copy( Qsor.FieldByname('SA_DATUM').AsString, 1, 4)+'%'' ', true);
  JAKOD := QSor.FieldByName('JA_KOD').AsString;
  if EllenorzottJarat3(JAKOD) > 0 then begin
	// if EllenorzottJarat2(SorGrid.Cells[2,SorGrid.Row],copy( SorGrid.Cells[3,SorGrid.Row],1,4)) > 0 then begin
	//if EllenorzottJarat(Qsor.FieldByname('JA_KOD').AsString) > 0 then begin
		Exit;
	end;
	Query_Run (QSor, 'SELECT SA_KOD FROM SZFEJ2 WHERE SA_UJKOD = '+ret_kod, true);
	szkod := QSor.FieldByName('SA_KOD').AsString;
	if NoticeKi('Val�ban t�r�lni szeretn� a kijel�lt t�telt?', NOT_QUESTION ) = 0 then begin
		{A bels� t�bla karbantart�sa a sor kit�rl�s�vel}
		Query_Run(QSor,'UPDATE SZFEJ SET SA_RESZE = 0, SA_KOD = '''' WHERE SA_UJKOD = '+ SorGrid.Cells[1,SorGrid.Row]+
			' AND SA_KOD = '''+szkod+''' ' , true);
		Query_Run(QSor,'UPDATE SZSOR SET SS_KOD='''' WHERE SS_UJKOD='+ SorGrid.Cells[1,SorGrid.Row]+
			' AND SS_KOD = '''+szkod+''' ' , true);
		Query_Run(QSor,'UPDATE VISZONY SET VI_SAKOD='''' WHERE VI_UJKOD='+ SorGrid.Cells[1,SorGrid.Row]+
			' AND VI_SAKOD = '''+szkod+''' ' , true);
		// Csak ebb�l az �sszes�tett sz�ml�b�l t�r�lj�nk!!!
		Query_Run (QSor, 'DELETE FROM SZSOR2 WHERE S2_S2KOD = '+ret_kod+' AND S2_UJKOD = '+ SorGrid.Cells[1,SorGrid.Row],true);
		// Minden �sszes�tett sz�ml�b�l kit�r�lj�k ezt a t�telt!
//		Query_Run (QSor, 'DELETE FROM SZSOR2 WHERE S2_UJKOD = '+ SorGrid.Cells[1,SorGrid.Row],true);
		{Csak a kijel�lt sort kell t�r�lni}
		if Sorgrid.Row = 1 then begin
			SorGrid.Rows[1].Clear;
		end else begin
			GridTorles(SorGrid, SorGrid.Row);
		end;
	end;
	Osszesit;
end;

procedure TOSzamlaBeDlg.ButValVevoClick(Sender: TObject);
var
  VNEMET: integer;
begin
	Application.CreateForm(TVevoUjFmDlg, VevoUjFmDlg);
	VevoUjFmDlg.valaszt := true;
	VevoUjFmDlg.ShowModal;
	if VevoUjFmDlg.ret_vkod <> '' then begin
		if VevoUjFmDlg.felista > 0 then begin
			if NoticeKi('Feketelist�s vev�! Folytatja?', NOT_QUESTION) <> 0 then begin
				VevoUjFmDlg.Destroy;
				Exit;
			end;
		end;
		if Query_Run (Query3, 'SELECT * FROM VEVO WHERE V_KOD = '''+VevoUjFmDlg.ret_vkod+''' ',true) then begin
			vevokod			:= VevoUjFmDlg.ret_vkod;
			MaskEdit1.Text 	:= Query3.FieldByName('V_NEV').AsString;
      ebVEKOD.Text    := Query3.FieldByName('V_KOD').AsString;
			MaskEdit2.Text 	:= Query3.FieldByName('V_IRSZ').AsString;
			MaskEdit3.Text 	:= Query3.FieldByName('V_VAROS').AsString;
			MaskEdit4.Text 	:= Query3.FieldByName('V_CIM').AsString;
			MaskEdit5.Text 	:= Query3.FieldByName('V_ADO').AsString;
      // ---- 2018.09.12. ---
      VNEMET:= Query3.FieldByName('V_NEMET').AsInteger;
      CheckBox3.Checked := ( VNEMET > 0);
      // --------------------
      ComboSet(OrszagCombo, Query3.FieldByName('V_ORSZ').AsString, orszaglist);
			if MaskEdit5.Text = '' then begin
				MaskEdit5.Text 	:= Query3.FieldByName('VE_EUADO').AsString;
			end;
			CheckBox7.Checked 		:= StrToIntDef(Query3.FieldByName('VE_FTELJ').AsString, -1) > 0;
      if CheckBox7.Checked and (MaskEdit16.Text='') then
        MaskEdit7.OnChange(self);
{			// A vev�h�z tartoz� bank bet�lt�se
			bakod  			:= Trim(Query3.FieldByName('VE_VBANK').AsString);
			fejuj13			:= EgyebDlg.Read_SZGrid('320', bakod);
			fejuj14			:= EgyebDlg.Read_SZGrid('320', IntToStr(StrToIntDef(bakod,0) + 1));
			fejuj15			:= EgyebDlg.Read_SZGrid('320', IntToStr(StrToIntDef(bakod,0) + 2));
			fejuj23			:= EgyebDlg.Read_SZGrid('320', IntToStr(StrToIntDef(bakod,0) + 4));
      fejuj24			:= EgyebDlg.Read_SZGrid('320', IntToStr(StrToIntDef(bakod,0) + 5));
      fejuj25			:= EgyebDlg.Read_SZGrid('320', IntToStr(StrToIntDef(bakod,0) + 6));
      fejuj26			:= EgyebDlg.Read_SZGrid('320', IntToStr(StrToIntDef(bakod,0) + 7));
      }

			// CheckBox1.Checked	:= false;
      // if Query3.FieldByName('V_NEMET').AsInteger > 0 then begin
			// 	CheckBox1.Checked	:= true;
      // end;
      ComboSet (cbNyelv, 	Query3.FieldByName('VE_NYELV').AsString, nyelvlist);
      NyelvFrissit;

      // CheckBox1Click(Sender);
      if StrToIntDef(Query3.FieldByName('V_LEJAR').AsString,0) > 0 then begin
				lejarat			:= StrToIntDef(Query3.FieldByName('V_LEJAR').AsString,0);
        MaskEdit8.Text 	:= DatumHoznap(MaskEdit7.Text, lejarat, true);
      end;
      Combobox1.SetFocus;
		end else begin
			NoticeKi('Hib�s vev�k�d!');
			MaskEdit1.Text := '';
			MaskEdit2.Text := '';
			MaskEdit3.Text := '';
			MaskEdit4.Text := '';
			MaskEdit5.Text := '';
		end;
	end;
	VevoUjFmDlg.Destroy;
end;

procedure TOSzamlaBeDlg.cbNyelvChange(Sender: TObject);
begin
  NyelvFrissit;
end;

procedure TOSzamlaBeDlg.FormDestroy(Sender: TObject);
begin
  modlist.Free;
  nyelvlist.Free;
end;

procedure TOSzamlaBeDlg.ButtonKuldClick(Sender: TObject);
begin
	if Elkuldes then begin
		Close;
	end;
end;

procedure TOSzamlaBeDlg.FormCreate(Sender: TObject);
var
  i: integer;
begin
  modosult := false;
	EgyebDlg.SetADOQueryDatabase(Query1);
	EgyebDlg.SetADOQueryDatabase(Query2);
	EgyebDlg.SetADOQueryDatabase(Query22);
	EgyebDlg.SetADOQueryDatabase(QFej);
	EgyebDlg.SetADOQueryDatabase(QSor);
	EgyebDlg.SetADOQueryDatabase(Query3);
	EgyebDlg.SetADOQueryDatabase(Query4);
  SorGrid.ColCount 	 	:= GRIDOSZLOP;
	SorGrid.Cells[0,0] 		:= 'Ssz.';
	SorGrid.Cells[1,0] 		:= 'K�d';
	SorGrid.Cells[2,0] 		:= 'J�rat';
	SorGrid.Cells[3,0] 		:= 'Telj.d�t.';
	SorGrid.Cells[4,0] 		:= 'Megjegyz�s';
	SorGrid.Cells[5,0] 		:= 'Nett�';
	SorGrid.Cells[6,0] 		:= '�FA';
	SorGrid.Cells[7,0] 		:= 'V.nem';
	SorGrid.Cells[8,0] 		:= 'V.nett�';
	SorGrid.Cells[9,0] 		:= 'V.�FA';
	SorGrid.Cells[10,0] 		:= 'R.';
	SorGrid.Cells[11,0] 		:= '�a.p�tl.';
  SorGrid.Cells[12,0] 		:= 'Poz�ci�';
  ComboBox2.Clear;
  for i:=0 to 12 do
  begin
    ComboBox2.Items.Add(SorGrid.Cells[i,0] ) ;
  end;
  ComboBox2.ItemIndex:=0;
  	SorGrid.ColWidths[0] 	:= 30;
  	SorGrid.ColWidths[1] 	:= 40;
  	SorGrid.ColWidths[2] 	:= 40;
  	SorGrid.ColWidths[3] 	:= 70;
  	SorGrid.ColWidths[4] 	:= 300;
  	SorGrid.ColWidths[5] 	:= 60;
  	SorGrid.ColWidths[6] 	:= 60;
  	SorGrid.ColWidths[7] 	:= 40;
  	SorGrid.ColWidths[8] 	:= 60;
  	SorGrid.ColWidths[9] 	:= 50;
  	SorGrid.ColWidths[10] := 15;
  	SorGrid.ColWidths[11] := 40;
  	SorGrid.ColWidths[12] := 180;
	SorGrid.RowCount 		:= 2;
	modlist 				:= TStringList.Create;
   orszaglist := TStringList.Create;
	Combobox1.Clear;
	SzotarTolt(Combobox1, '100' , modlist, false, false );
	Combobox1.ItemIndex 	:= 0;
  nyelvlist := TStringList.Create;
  SzotarTolt(cbNyelv, '430', nyelvlist, false );
  ComboSet (cbNyelv, 	DefaultSzamlaNyelv, nyelvlist);
  // cbNyelv.ItemIndex 	:= 0;
 	mell1 	:= EgyebDlg.Read_SZGrid('999','401');
 	mell2 	:= EgyebDlg.Read_SZGrid('999','402');
 	mell3 	:= EgyebDlg.Read_SZGrid('999','403');
 	mell4 	:= EgyebDlg.Read_SZGrid('999','404');
  mell5  	:= '';
  lejarat	:= Round(StringSzam(EgyebDlg.Read_SZGrid('999','602')));
	MaskEdit6.Text 			:= EgyebDlg.MaiDatum;
//	MaskEdit7.Text 			:= EgyebDlg.MaiDatum;
//	MaskEdit8.Text 			:= DatumHoznap(MaskEdit7.Text, lejarat, true);
 //	CheckBox1Click(Sender);

  NyelvFrissit;
   fejuj13			:= '';
   fejuj14			:= '';
   fejuj15			:= '';
   fejuj23			:= '';
   fejuj24			:= '';
   fejuj25			:= '';
   fejuj26			:= '';
	SetMaskEdits([MaskEdit9, MaskEdit10, MaskEdit11]);
	SetMaskEdits([MaskEdit90, MaskEdit100, MaskEdit110]);
	SzotarTolt(OrszagCombo,  '300' , orszaglist, false );

end;

procedure TOSzamlaBeDlg.Osszesit;
var
	netto, afa 	: double;
	vnetto,vafa : double;
  ii 			: integer;
  vnem:string;
begin
	netto 		:= 0;
  afa 			:= 0;
	vnetto 		:= 0;
  vafa 			:= 0;
  vnem:='';
  TDATUM:='';
	for ii	:= 1 to SorGrid.RowCount - 1 do begin
  	netto := netto  + StringSzam(SorGrid.Cells[5,ii]);
    afa   := afa  	+ StringSzam(SorGrid.Cells[6,ii]);
  	vnetto := vnetto+ StringSzam(SorGrid.Cells[8,ii]);
    vafa   := vafa 	+ StringSzam(SorGrid.Cells[9,ii]);
    if SorGrid.Cells[3,ii] > TDATUM then
      TDATUM:=SorGrid.Cells[3,ii];
    if SorGrid.Cells[7,ii]<>'' then vnem:=SorGrid.Cells[7,ii];
  end;
  if (not megtekint)and(not CheckBox7.Checked) then
    MaskEdit6.Text:=TDATUM;
  MaskEdit9.Text 	:= SzamString(netto,12,0);
  MaskEdit10.Text 	:= SzamString(afa,12,0);
  MaskEdit11.Text 	:= SzamString(netto+afa,12,0);
  MaskEdit90.Text 	:= SzamString(vnetto,12,2);
  MaskEdit100.Text 	:= SzamString(vafa,12,2);
  MaskEdit110.Text 	:= SzamString(vnetto+vafa,12,2);
  Label19.Caption:=vnem;
end;

procedure TOSzamlaBeDlg.Modosit(Sender: TObject);
begin
  modosult := true;
end;

procedure TOSzamlaBeDlg.MaskEdit7Exit(Sender: TObject);
begin
	DatumExit(Sender, false);
	MaskEdit8.Text 		:= DatumHoznap(MaskEdit7.Text, lejarat, true);
end;

procedure TOSzamlaBeDlg.BitBtn4Click(Sender: TObject);
begin
	Application.CreateForm(TMellekForm, MellekForm);
	MellekForm.Tolt('Mell�kletek', mell1, mell2, mell3, mell4, mell5, '', '', 5);
	MellekForm.ToltUj;
	MellekForm.ShowModal;
	if not MellekForm.Kilepes then begin
		mell1 := MellekForm.mell1;
		mell2 := MellekForm.mell2;
		mell3 := MellekForm.mell3;
		mell4 := MellekForm.mell4;
		mell5 := MellekForm.mell5;
	end;
	MellekForm.Destroy;
end;

procedure TOSzamlaBeDlg.BitBtn5Click(Sender: TObject);
var
	torlendo	: boolean;
begin
	torlendo	:= false;
	if ret_kod = '' then begin
		torlendo	:= true;		{Megtekint�s ut�n t�r�lni kell a rekordokat}
	end;
	if Elkuldes then begin
		// A sz�mla megjelen�t�se nem nyomtathat� form�ban
		Application.CreateForm(TSzamlaNezoDlg, SzamlaNezoDlg);
		SzamlaNezoDlg.OsszesitettNyomtatas(StrToIntDef(ret_kod,0), 0);
		SzamlaNezoDlg.Destroy;
	end;
	if torlendo then begin
		// Ide j�n a t�rl�s
	end;
end;

function TOSzamlaBeDlg.Elkuldes : boolean;
var
	st, S		: string;
	ii 			: integer;
	extrafej	: string;
	vnetto,vafa : double;
  vnem:string;
begin
	if megtekint then begin
		Result	:= true;
		Exit;
	end;
	Result := false;
	if CheckBox7.Checked and  (MaskEdit16.Text = '') then begin
		NoticeKi('Nincs �rfolyam!');
		//MaskEdit1.SetFocus;
		Exit;
	end;

 	if MaskEdit1.Text = '' then begin
		NoticeKi('A megrendel� neve m�g �res!');
		MaskEdit1.SetFocus;
		Exit;
	end;

	if MaskEdit6.Text = '' then begin
		NoticeKi('A teljes�t�s d�tuma m�g nincs megadva!');
		MaskEdit6.SetFocus;
		Exit;
	end;

	if MaskEdit5.Text = '' then begin
		NoticeKi('Az ad�sz�m kit�lt�se k�telez�!');
		MaskEdit5.SetFocus;
		Exit;
	end;

  if (MaskEdit7.Text<>'') and (StrToDate(MaskEdit7.Text)<date) then
		NoticeKi('FIGYELEM! A sz�mla kelte a mai nap el�tti!');
  ///////////////////////
	vnetto 		:= 0;
  vafa 			:= 0;
  vnem:='';
	for ii	:= 1 to SorGrid.RowCount - 1 do begin
  	vnetto := vnetto+ StringSzam(SorGrid.Cells[8,ii]);
    vafa   := vafa 	+ StringSzam(SorGrid.Cells[9,ii]);
    if SorGrid.Cells[7,ii]<>'' then
    begin
      if (vnem<>'')and(vnem<>SorGrid.Cells[7,ii]) then
      begin
    		NoticeKi('T�bbf�le valutanem!');
    		Exit;
      end;
      vnem:=SorGrid.Cells[7,ii];
    end;
  end;


	{
	if ret_kod = '' then begin
		if NoticeKi(' Egy �J �SSZES�TETT SZ�MLA ker�l l�trehoz�sra. Folytatja?', NOT_QUESTION) <> 0 then begin
     	Exit;
     end;
  end else begin
		if NoticeKi(' Egy r�gebbi �sszes�tett sz�mla ker�l m�dos�t�sra. Folytatja?', NOT_QUESTION) <> 0 then begin
		Exit;
	  end;
  end;
}
	if ret_kod = '' then begin
		{�j rekord felv�tele}
		// Query_Run(QFej, 'SELECT MAX(SA_UJKOD) MAXKOD FROM SZFEJ2 ', true);
    // ret_kod 	:= InttoStr(QFej.FieldByName('MAXKOD').AsInteger + 1);
    ret_kod 	:= InttoStr(GetNewIDLoop(modeGetNextCode, 'SZFEJ2', 'SA_UJKOD', ''));
    if (ret_kod='0') or (ret_kod='') then begin
     S:= 'Sikertelen k�d gener�l�s, k�rem pr�b�lja �jra! (OsszesitettSzamlaBevitel)';
     HibaKiiro(S);
     NoticeKi(S);
     Exit;
     end;
		Query_Run(QFej, 'INSERT INTO SZFEJ2 (SA_UJKOD, SA_FLAG ) '+
			'VALUES ('+ret_kod+', ''1'' ) ', true);
	end;
	// st	:= '0';
	// if CheckBox1.Checked then begin
	//	st	:= '1';
	// end;
	// Az extra vev�i sor meghat�roz�sa
	extrafej	:= Query_Select('VEVO', 'V_KOD', vevokod, 'VE_VEFEJ');
	Query_Update( QFej, 'SZFEJ2', [
		'SA_DATUM', 	''''+EgyebDlg.MaiDatum+'''',
		'SA_TEDAT',		''''+MaskEdit6.Text+'''',
		'SA_KIDAT',	 	''''+MaskEdit7.Text+'''',
		'SA_ESDAT',		''''+MaskEdit8.Text+'''',
		'SA_VEVOKOD',	''''+vevokod+'''',
		'SA_VEVONEV',	''''+MaskEdit1.Text+'''',
		'SA_VEIRSZ',	''''+MaskEdit2.Text+'''',
		'SA_VEVAROS', 	''''+MaskEdit3.Text+'''',
		'SA_VECIM',		''''+MaskEdit4.Text+'''',
		'SA_VEADO',		''''+MaskEdit5.Text+'''',
		'SA_VEFEJ',		''''+extrafej+'''',
		'SA_VEKEDV',	'0',
		'SA_ATAD', 		'0',
		'SA_FIMOD',		''''+Combobox1.Text+'''',
		'SA_OSSZEG',	SqlSzamString(StringSzam(MaskEdit9.Text),12,2),
		'SA_AFA',		SqlSzamString(StringSzam(MaskEdit10.Text),12,2),
		'SA_VALNEM',		''''+vnem+'''',
		'SA_VALOSS',	SqlSzamString(vnetto,12,2),
		'SA_VALAFA',	SqlSzamString(vafa,12,2),
		'SA_SAEUR',		BoolToStr01(Checkbox3.Checked),
		'SA_MEGJ',		''''+MaskEdit13.Text+'''',
		'SA_JARAT',		''''+MaskEdit12.Text+'''',
		'SA_POZICIO',	''''+MaskEdit14.Text+'''',
		'SA_RSZ',		''''+MaskEdit15.Text+'''',
		'SA_MELL1',		''''+mell1+'''',
		'SA_MELL2',		''''+mell2+'''',
		'SA_MELL3',		''''+mell3+'''',
		'SA_MELL4',		''''+mell4+'''',
		'SA_MELL5',		''''+mell5+'''',
		'SA_FEJL1',		''''+fej1+'''',
		'SA_FEJL2',		''''+fej2+'''',
		'SA_FEJL3',		''''+fej3+'''',
		'SA_FEJL4',		''''+fej4+'''',
		'SA_FEJL5',		''''+fej5+'''',
		'SA_FEJL6',		''''+fej6+'''',
		'SA_FEJL7',		''''+fej7+'''',
       'SA_NYELV',		''''+nyelvlist[cbNyelv.ItemIndex]+'''',
       'SA_ORSZ2',     ''''+orszaglist[OrszagCombo.ItemIndex]+'''',
		'SA_TIPUS',		'100'
	  ], ' WHERE SA_UJKOD = '+ret_kod);

	 {SZSOR2 karbantart�sa}
	 Query_Run (QSor, 'DELETE FROM SZSOR2 WHERE S2_S2KOD = '+ret_kod ,true);
	 for ii := 1 to SorGrid.RowCount - 1 do begin
		if SorGrid.Cells[1,ii] <> '' then begin
			Query_Run(QSor, 'INSERT INTO SZSOR2 (S2_S2KOD, S2_S2SOR, S2_UJKOD ) '+
			'VALUES ('+ret_kod+', '+SorGrid.Cells[0,ii]+', '+SorGrid.Cells[1,ii]+' ) ', true);
		end;
	 end;
	 Result := true;
end;


procedure TOSzamlaBeDlg.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
	EgyebDlg.FormReturn(Key);
end;

procedure TOSzamlaBeDlg.BitBtn10Click(Sender: TObject);
begin
	EgyebDlg.Calendarbe(MaskEdit6);
end;

procedure TOSzamlaBeDlg.BitBtn8Click(Sender: TObject);
begin
	EgyebDlg.Calendarbe(MaskEdit7);
  MaskEdit7.OnExit(MaskEdit7);
end;

procedure TOSzamlaBeDlg.BitBtn9Click(Sender: TObject);
begin
	EgyebDlg.Calendarbe(MaskEdit8);
end;

procedure TOSzamlaBeDlg.GridTolto(arf_ujraszamol:boolean);
var
  jaratsz,edij, vikod: string;
begin
	if ret_kod <> '' then begin
		SorGrid.RowCount 	:= 2;
		SorGrid.Rows[1].Clear;
{     Query_Run (QSor, 'SELECT DISTINCT S2_UJKOD FROM SZSOR2 WHERE S2_S2KOD = '+ret_kod+' ORDER BY S2_S2SOR' ,true);
}
//		Query_Run (QSor, 'SELECT DISTINCT S2_UJKOD FROM SZSOR2 WHERE S2_S2KOD = '+ret_kod ,true);
		Query_Run (QSor, 'SELECT DISTINCT S2_UJKOD,S2_S2SOR FROM SZSOR2 WHERE S2_S2KOD = '+ret_kod+' order by S2_S2SOR' ,true);
		while not QSor.EOF do begin
			if Query_Run (Query2, 'SELECT * FROM SZFEJ WHERE SA_UJKOD = '+QSor.FieldByName('S2_UJKOD').AsString ,true) then begin
        if Query2.FieldByName('SA_RESZE').AsInteger<>1 then
        begin
          // NoticeKi(Query2.FieldByName('SA_UJKOD').AsString+' t�tel nem r�ssz�ml�nak van jel�lve!');
  			   Query_Run(Query22, 'UPDATE SZFEJ SET SA_RESZE = 1 WHERE SA_UJKOD = '+ Query2.FieldByName('SA_UJKOD').AsString, true);
        end;
        if arf_ujraszamol then
        begin
          EgyebDlg.Szamla_Arf_szamol(QSor.FieldByName('S2_UJKOD').Value,StringSzam(MaskEdit16.Text));
  		  	Query_Run (Query2, 'SELECT * FROM SZFEJ WHERE SA_UJKOD = '+QSor.FieldByName('S2_UJKOD').AsString ,true);
        end;
				if SorGrid.Cells[1,1] <> '' then begin
					SorGrid.RowCount := SorGrid.RowCount + 1;
				end;
				SorGrid.Cells[0,SorGrid.RowCount-1] := IntToStr(SorGrid.RowCount-1);
				//SorGrid.Cells[1,SorGrid.RowCount-1] := QSor.FieldByName('S2_UJKOD').AsString;
				SorGrid.Cells[1,SorGrid.RowCount-1] := Stringofchar('0',5-length(QSor.FieldByName('S2_UJKOD').AsString))+ QSor.FieldByName('S2_UJKOD').AsString;
				SorGrid.Cells[2,SorGrid.RowCount-1] := Query2.FieldByName('SA_JARAT').AsString;
				SorGrid.Cells[3,SorGrid.RowCount-1] := Query2.FieldByName('SA_TEDAT').AsString;
				SorGrid.Cells[4,SorGrid.RowCount-1] := Query2.FieldByName('SA_MEGJ').AsString;
 				SorGrid.Cells[5,SorGrid.RowCount-1] := Query2.FieldByName('SA_OSSZEG').AsString;
				SorGrid.Cells[6,SorGrid.RowCount-1] := Query2.FieldByName('SA_AFA').AsString;
				SorGrid.Cells[7,SorGrid.RowCount-1] := Query2.FieldByName('SA_VALNEM').AsString;
				SorGrid.Cells[8,SorGrid.RowCount-1] := Query2.FieldByName('SA_VALOSS').AsString;
				SorGrid.Cells[9,SorGrid.RowCount-1] := Query2.FieldByName('SA_VALAFA').AsString;
        jaratsz:=  Query_Select('VISZONY','VI_UJKOD',QSor.FieldByName('S2_UJKOD').AsString,'VI_JAKOD');
        vikod:=  Query_Select('VISZONY','VI_UJKOD',QSor.FieldByName('S2_UJKOD').AsString,'VI_VIKOD');
        edij:=Query_Select2('MEGBIZAS','MB_JAKOD','MB_VIKOD', jaratsz,vikod,'MB_EDIJ');
        //edij:=Query_Select('MEGBIZAS','MB_JAKOD',jaratsz,'MB_EDIJ');
        if edij='1' then
  				SorGrid.Cells[10,SorGrid.RowCount-1] :='X';
				SorGrid.Cells[11,SorGrid.RowCount-1] := Query2.FieldByName('SA_UZPOT').AsString;
        SorGrid.Cells[12,SorGrid.RowCount-1] := Query2.FieldByName('SA_POZICIO').AsString;
			end;
			QSor.Next;
		end;
  	end;
end;

procedure TOSzamlaBeDlg.BitBtn6Click(Sender: TObject);
begin
	{Fejl�c beolvas�sa}
  	Application.CreateForm(TMellekForm, MellekForm);
  	MellekForm.Tolt('A fejl�c adatai', fej1, fej2, fej3, fej4, fej5, fej6, fej7, 7);
  	MellekForm.ShowModal;
  	if not MellekForm.Kilepes then begin
		fej1 := MellekForm.mell1;
		fej2 := MellekForm.mell2;
		fej3 := MellekForm.mell3;
		fej4 := MellekForm.mell4;
		fej5 := MellekForm.mell5;
		fej6 := MellekForm.mell6;
		fej7 := MellekForm.mell6;
  	end;
	MellekForm.Destroy;
end;

procedure TOSzamlaBeDlg.NyelvFrissit;
var
  vbank: integer;
  NyelvKod: string;
begin
   NyelvKod:= nyelvlist[cbNyelv.ItemIndex];
   // 2018-09-12: itt is V_NEMET alapj�n vessz�k az alap�rtelmez�st
   {if NyelvKod='HUN' then begin
       CheckBox3.Checked   := False    // nem EUR sz�mla
       // EgyebDlg.nemetfejlec:=False; // az �j form�tumn�l nem fogjuk haszn�lni [2015-01-19]
   end else begin
      CheckBox3.Checked    := True;    // EUR sz�mla
      // EgyebDlg.nemetfejlec:=True; // az �j form�tumn�l nem fogjuk haszn�lni [2015-01-19]
   end;
   }

  fej1	:= EgyebDlg.Read_SZGrid('440', NyelvKod+'01');    // pl. 'GER01'
  fej2	:= EgyebDlg.Read_SZGrid('440', NyelvKod+'02');
  fej3	:= EgyebDlg.Read_SZGrid('440', NyelvKod+'03');
  fej4	:= EgyebDlg.Read_SZGrid('440', NyelvKod+'04');
  fej5	:= EgyebDlg.Read_SZGrid('440', NyelvKod+'05');
  fej6	:= EgyebDlg.Read_SZGrid('440', NyelvKod+'06');
  fej7	:= EgyebDlg.Read_SZGrid('440', NyelvKod+'07');

  mell1 := EgyebDlg.Read_SZGrid('450', NyelvKod+'01');
  mell2 := EgyebDlg.Read_SZGrid('450', NyelvKod+'02');
  mell3 := EgyebDlg.Read_SZGrid('450', NyelvKod+'03');
  mell4 := EgyebDlg.Read_SZGrid('450', NyelvKod+'04');

  if (NyelvKod<>'HUN') and (fejuj23<>'') then begin
			fej3	:= fejuj23;
			fej4	:= fejuj24;
			fej5	:= fejuj25;
			fej6	:= fejuj26;
      end; // if

  if vevobank then begin
    vbank:= StrToIntDef(Query_Select('VEVO','V_KOD',vevokod,'VE_VBANK'),0);
    if NyelvKod='HUN' then begin
        fej3:= EgyebDlg.Read_SZGrid('320',inttostr(vbank  ));
        fej4:= EgyebDlg.Read_SZGrid('320',inttostr(vbank+1));
	      fej5:= EgyebDlg.Read_SZGrid('320',inttostr(vbank+2));
        end;  // HUN
    if NyelvKod='GER' then begin
        fej3:= EgyebDlg.Read_SZGrid('320',inttostr(vbank+4));
        fej4:= EgyebDlg.Read_SZGrid('320',inttostr(vbank+5));
	      fej5:= EgyebDlg.Read_SZGrid('320',inttostr(vbank+6));
        end;  // GER
      // **********************
      // 'ENG' m�g nem megoldott
      // **********************
    end;  // if
end;

procedure TOSzamlaBeDlg.MaskEdit16Click(Sender: TObject);
begin
	with Sender as TMaskEdit do begin
  		SelectAll;
  	end;
end;

procedure TOSzamlaBeDlg.Tiltas;
begin
	GroupBox1.Enabled		:= false;
	GroupBox2.Enabled		:= false;
 //	GroupBox4.Enabled		:= false;
  BitBtn1.Enabled:=False;
  BitBtn3.Enabled:=False;
	ButtonKuld.Enabled		:= false;
  ButtonMod.Enabled:=False;
end;

procedure TOSzamlaBeDlg.ButtonModClick(Sender: TObject);
var
  sor:integer;
begin
	if CheckBox7.Checked and  (MaskEdit16.Text = '') then begin
		NoticeKi('Nincs �rfolyam!');
		Exit;
	end;
  sor:= SorGrid.Row ;
  Application.CreateForm(TSzamlaBeDlg, SzamlaBeDlg) ;
  SzamlaBeDlg.Tolto('',SorGrid.Cells[1,SorGrid.Row]);
  SzamlaBeDlg.ShowModal;

  SzamlaBeDlg.Free;

//  GridTolto(CheckBox7.Checked);
  GridTolto(False);
	Osszesit;
  SorGrid.Row:=sor;
end;

procedure TOSzamlaBeDlg.MaskEdit8Change(Sender: TObject);
begin
  if megtekint then exit;
  modosult := true;
  if CheckBox7.Checked then  // Folyamatos teljes�t�s
  begin
     MaskEdit6.Text:=MaskEdit8.Text;
  end;
end;

procedure TOSzamlaBeDlg.MaskEdit7Change(Sender: TObject);
var
  arf:double;
begin
  if megtekint then exit;
  modosult := true;
  if CheckBox7.Checked then        // Folyamatos teljes�t�s
  begin
    arf:= EgyebDlg.ArfolyamErtek('EUR',MaskEdit7.Text,False);
    if arf = 0 then
    begin
      NoticeKi('Nincs �rfolyam az adott napra!');
      exit;
    end;
    MaskEdit16.Text:=SzamString(arf,10,4) ;
    Label21.Caption:='('+MaskEdit7.Text+')';
  end;
end;

procedure TOSzamlaBeDlg.SpeedButton4Click(Sender: TObject);
var
	ii 	: integer;
   str	: string;
begin
	// A kiv�lasztott elemet beteszi a lista elej�re
   if SorGrid.Cells [0,1] <> '' then begin
		if SorGrid.Row > 1 then begin
           for ii := 1 to GRIDOSZLOP - 1 do begin
           	str									:= SorGrid.Cells [ii,SorGrid.Row];
               SorGrid.Cells [ii,SorGrid.Row] 		:= SorGrid.Cells [ii,SorGrid.Row-1];
               SorGrid.Cells [ii,SorGrid.Row-1] 	:= str;
           end;
           SorGrid.Row	:= SorGrid.Row - 1;
       end;
   end;

end;

procedure TOSzamlaBeDlg.SpeedButton5Click(Sender: TObject);
var
	ii 	: integer;
   str	: string;
begin
	// A kiv�lasztott elemet beteszi a lista elej�re
	if SorGrid.Cells [0,1] <> '' then begin
   	if SorGrid.Row < SorGrid.RowCount - 1 then begin
           for ii := 1 to GRIDOSZLOP - 1 do begin
           	str									:= SorGrid.Cells [ii,SorGrid.Row];
               SorGrid.Cells [ii,SorGrid.Row] 		:= SorGrid.Cells [ii,SorGrid.Row+1];
				SorGrid.Cells [ii,SorGrid.Row+1] 	:= str;
           end;
           SorGrid.Row	:= SorGrid.Row + 1;
		end;
   end;

end;

procedure TOSzamlaBeDlg.MaskEdit8Exit(Sender: TObject);
begin
	DatumExit(Sender, false);
end;

procedure TOSzamlaBeDlg.Button1Click(Sender: TObject);
begin
 // GridTorles();
 if MaskEdit16.Text='' then
 begin
  ShowMessage('Nincs �rfolyam!');
  exit;
 end;
 if MessageBox(0, 'K�ri az �jrasz�mol�st?', '', MB_ICONQUESTION or MB_YESNO or MB_DEFBUTTON1)= IDNO then exit;
 GridTolto(True);
 ShowMessage('K�sz!');
end;

procedure TOSzamlaBeDlg.SorGridSelectCell(Sender: TObject; ACol,
  ARow: Integer; var CanSelect: Boolean);
begin
  if ARow>0 then exit;
end;

procedure TOSzamlaBeDlg.ComboBox2Change(Sender: TObject);
begin
  TablaRendezo( SorGrid, [ComboBox2.ItemIndex,0], [0,0], 1, true );
end;

end.
