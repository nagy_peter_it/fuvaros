unit ArfolyamTolto;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, StdCtrls, OleCtrls, SHDocVw, HTTPApp, HTTPProd, mshtml,
  Buttons, ExtCtrls, IdBaseComponent, IdComponent, IdTCPConnection,
  IdTCPClient, IdHTTP;

const
	WEBADDRESS	: string = 'http://arfolyam.iridium.hu/';


type
  TArfolyamToltoDlg = class(TForm)
	 SG2: TStringGrid;
	 Panel1: TPanel;
	 BitElkuld: TBitBtn;
	 Panel2: TPanel;
	 SG1: TStringGrid;
	 Panel3: TPanel;
	 BitKilep: TBitBtn;
	 Memo1: TMemo;
	 IdHTTP1: TIdHTTP;
    SG3: TStringGrid;
//	 function  Tolto : boolean;
	 function  Tolto2 : boolean;
	 function GetDatum(sorsz : integer) : string;
	 function GetValutanem(sorsz : integer) : string;
	 function GetArfolyam(vnem, datum : string; var egyseg : integer) : double;
	 procedure FormCanResize(Sender: TObject; var NewWidth,
	   NewHeight: Integer; var Resize: Boolean);
	 procedure FormResize(Sender: TObject);
	 procedure BitElkuldClick(Sender: TObject);
	 procedure BitKilepClick(Sender: TObject);
	 procedure Tarolas;
  private
  public
		valutaszam	: integer;
		datumszam	: integer;
  end;

var
  ArfolyamToltoDlg: TArfolyamToltoDlg;

implementation

uses
		Kozos, J_SQL, Egyeb;

{$R *.dfm}
(*
function TArfolyamToltoDlg.Tolto : boolean;
var
	 i			: integer;
	 j			: integer;
	 josor		: integer;
	 sor		: string;
	 str		: string;
	 sorsza		: integer;
	 temposz	: integer;
	 maxosz		: integer;
	 vnem		: string;
	 Tabla		: TStringGrid;
	 proba		: integer;
begin
	Screen.Cursor	:= crHourGlass;
	Result			:= false;
	// El�sz�r ellen�rizz�k, hogy volt-e m�r ma let�lt�s
	Query_Run(EgyebDlg.QueryKozos, 'SELECT AR_KOD FROM ARFOLYAM WHERE AR_DATUM = '''+EgyebDlg.MaiDatum+''' '+
		' AND AR_MEGJE = '''+WEBADDRESS+''' ', FALSE);
	if EgyebDlg.QueryKozos.RecordCount > 0 then begin
		Exit;
	end;
	Memo1.Width		:= 2000;
	Tabla			:= SG1;
	Tabla.RowCount	:= 1;
	Tabla.ColCount	:= 50;
	Tabla			:= SG2;
	Tabla.RowCount	:= 1;
	Tabla.ColCount	:= 50;
	Tabla			:= SG1;

//	Hibakiiro('    �rfolyam beolvas�s 4. ');
	proba	:= 1;
	while proba > 0 do begin
		try
			str	:= IDHttp1.Get('http://arfolyam.iridium.hu/');
			while Pos(chr(10), str ) > 0 do begin
				Memo1.Lines.Add(copy(str, 1, Pos(chr(10), str )-1));
				str	:= copy(str, Pos(chr(10), str ) +1, 999999);
			end;
			Memo1.Lines.Add(str);
			proba		:= -1;
	 //	NoticeKi('Az �rfolyam beolvas�sa siker�lt');
		except
			Inc(proba);
	 //	NoticeKi('Az �rfolyam beolvas�sa NEM siker�lt');
		end;
		if proba > 5 then begin
			proba 	:= -2;
		end;
	end;

	if Memo1.Lines.Count < 1 then begin
		Memo1.Lines.Add('Nem siker�lt a beolvas�s!');
		Hibakiiro('    �rfolyam beolvas�s 5. ');
		Screen.Cursor	:= crDefault;
		Exit;
	end;
//	Hibakiiro('    �rfolyam beolvas�s 6. ');
//	Memo1.Lines.SaveToFile(EgyebDlg.TempList +'arfmemo.txt');
	// A memo feldolgoz�sa
	josor	:= 0;
	sorsza	:= 0;
	maxosz	:= 0;
	temposz	:= 0;
	for i := 0 to Memo1.Lines.Count - 1 do begin
		sor := Memo1.Lines[i];
		if Pos('<TR>', sor) > 0 then begin
			josor	:= 1;
			temposz	:= 0;
		end;
		if josor > 0 then begin
			// Ez egy j� sor,, itt van az �rt�k t�rolva
			if copy(sor, 1, 10 ) = '<TD WIDTH=' then begin
//           if ( ( Pos('<TD', sor) > 0 ) and (( Pos('</TD>', sor) > 0 )) )  then begin
				if temposz = 0 then begin
					Inc(sorsza);
					Tabla.RowCount := sorsza;
				end;
				// A j� sz�veg kiv�tele a sorb�l
				str	:= copy(sor, 11, 999);
				if Pos('>', str) > 0 then begin
					str := copy(str, Pos('>', str) + 1, 999);
					if Pos('<', str) > 0 then begin
						str := copy(str, 1, Pos('<', str) - 1);
					end else begin
						str	:= '';
					end;
				end else begin
					str	:= '';
				end;
				if str <> '' then begin
					if str = 'D�tum' then begin
						if Tabla.Cells[0,0] = str then begin
							// Ez m�r a m�sodik t�bl�zat
							sorsza			:= 1;       // A m�sodik t�bl�zat megint az els� sort�l �r
							str				:= '';  	// A m�sodik d�tum helyett �res karaktert �runk
							Tabla			:= SG2;
						end else begin
							// Ez az els� t�bl�zat -> betessz�k a bal fels� sarokba a d�tum sz�veget
						end;
					end;
//               	Memo1.Lines.Add('   -> '+str);
				end;
				Tabla.Cells[temposz, sorsza-1]	:= str;
				Inc(temposz);
				if maxosz < temposz then begin
					maxosz := temposz;
				end;
			end;
		end;

		if Pos('</TR>', sor) > 0 then begin
			josor	:= 0;
		end;
	end;

	// A m�sodik t�bla bem�sol�sa az els� mell�
	for i := 1 to SG1.RowCount -1 do begin
		if SG1.Cells[0, i] <> SG2.Cells[0, i] then begin
			Screen.Cursor	:= crDefault;
			Exit;
		end;
	end;
	i	:= 1;
	while SG1.Cells[i, 0] <> '' do begin
		Inc(i);
	end;
	SG1.ColCount	:= i;
	i	:= 1;
	while SG2.Cells[i, 0] <> '' do begin
		Inc(i);
	end;
	SG2.ColCount	:= i;
	maxosz			:= SG1.ColCount;
	SG1.ColCount	:= SG1.ColCount + SG2.ColCount - 1;
	for i := 1 to SG2.ColCount -1 do begin
		for j := 0 to SG2.RowCount - 1 do begin
			SG1.Cells[maxosz+i-1, j] := SG2.Cells[i, j];
		end;
	end;
	Tabla	:= SG1;
	for i := 1 to Tabla.ColCount - 1 do begin
		if Tabla.Cells[i, 0] <> '' then begin
			Tabla.Cells[i, 1]		:= '1';
			vnem				:= Tabla.Cells[i, 0];
			if Pos('**', vnem) > 0 then begin
				Tabla.Cells[i, 0]		:= copy(vnem, 1, Pos('**', vnem) - 1);
				Tabla.Cells[i, 1]		:= '1000';
			end;
			if Pos('*', vnem) > 0 then begin
				Tabla.Cells[i, 0]		:= copy(vnem, 1, Pos('*', vnem) - 1);
				Tabla.Cells[i, 1]		:= '100';
			end;
		end;
	end;
	if SG1.Cells[0,SG1.RowCount - 1] = '' then begin
		SG1.RowCount := SG1.RowCount - 1;
	end;
//	Hibakiiro('�rfolyam beolvas�s 8. '+FormatDateTime('nn.ss.zzz', now - t1 ));
	valutaszam		:= SG1.ColCount - 1;
	datumszam		:= SG1.RowCount - 2;
	Screen.Cursor	:= crDefault;
	Result			:= true;
end;
*)
function TArfolyamToltoDlg.Tolto2 : boolean;
var
	 i			: integer;
	 j			: integer;
	 josor		: integer;
	 sor		: string;
	 str		: string;
	 sorsza		: integer;
	 temposz	: integer;
	 maxosz		: integer;
	 vnem		: string;
	 Tabla		: TStringGrid;
	 proba		: integer;
	 kezdodik	: boolean;
begin
	Screen.Cursor	:= crHourGlass;
	Result			:= false;
	// El�sz�r ellen�rizz�k, hogy volt-e m�r ma let�lt�s
	Query_Run(EgyebDlg.QueryKozos, 'SELECT AR_KOD FROM ARFOLYAM WHERE AR_DATUM = '''+EgyebDlg.MaiDatum+''' '+
		' AND AR_MEGJE = '''+WEBADDRESS+''' ', FALSE);
	if EgyebDlg.QueryKozos.RecordCount > 0 then begin
		Exit;
	end;
	Memo1.Width		:= 2000;
	Tabla			:= SG1;
	Tabla.RowCount	:= 1;
	Tabla.ColCount	:= 50;
	Tabla			:= SG2;
	Tabla.RowCount	:= 1;
	Tabla.ColCount	:= 50;
	Tabla			:= SG1;

	proba	:= 1;
	while proba > 0 do begin
		try
			str	:= IDHttp1.Get('http://arfolyam.iridium.hu/');
			while Pos(chr(10), str ) > 0 do begin
				Memo1.Lines.Add(copy(str, 1, Pos(chr(10), str )-1));
				str	:= copy(str, Pos(chr(10), str ) +1, 999999);
			end;
			Memo1.Lines.Add(str);
			proba		:= -1;
		except
			Inc(proba);
		end;
		if proba > 5 then begin
			proba 	:= -2;
		end;
	end;
	if proba = -2 then begin
		// Nem siker�lt a bet�lt�s
		Screen.Cursor	:= crDefault;
		Exit;
	end;
	if Memo1.Lines.Count < 1 then begin
		Screen.Cursor	:= crDefault;
		Exit;
	end;
	// A memo feldolgoz�sa
	josor	:= 0;
	sorsza	:= 0;
	while josor < Memo1.Lines.Count do begin
		sor := Memo1.Lines[josor];
		if ( ( Pos('<TD', Uppercase(sor)) > 0 ) and ( Pos('D�tum', sor) > 0) ) then begin
			sorsza	:= josor;
			josor	:= Memo1.Lines.Count;
		end;
		Inc(josor);
	end;
	if sorsza = 0 then begin
		Screen.Cursor	:= crDefault;
		Exit;
	end;
	// Elkezdj�k az els� t�bla t�lt�s�t
	Tabla.Cells[0, 0]	:= 'D�tum';
	Inc(sorsza);
	temposz	:= 1;
	maxosz	:= 1;
	josor	:= 0;
	kezdodik	:= true;
	while sorsza < Memo1.Lines.Count do begin
		sor := Memo1.Lines[sorsza];
		if copy(Uppercase(sor), 1, 10 ) = '<TD WIDTH=' then begin
			str	:= sor;
			if Pos('>', str) > 0 then begin
				str := copy(str, Pos('>', str) + 1, 999);
				if Pos('<', str) > 0 then begin
					str := copy(str, 1, Pos('<', str) - 1);
				end else begin
					str	:= '';
				end;
			end else begin
				str	:= '';
			end;
			if str <> '' then begin
				if ( ( Length(str) = 10 ) and ( Jodatum(str) ) ) then begin
					Inc(josor);
					if kezdodik then begin
						Inc(josor);
						kezdodik	:= false;
					end;
					Tabla.RowCount	:= josor + 1;
				end;
				if str = 'D�tum' then begin
                  if Tabla.Name<>'SG2' then begin
                           Tabla			:= SG2
                  end else begin
                           Tabla			:= SG3;
                  end;
					// Kezd�dik a m�sodik t�bla
					josor			:= 0;       // A m�sodik t�bl�zat megint az els� sort�l �r
//					str				:= '';  	// A m�sodik d�tum helyett �res karaktert �runk
					Tabla.ColCount	:= maxosz;
					maxosz			:= 1;
					kezdodik		:= true;
				end;
			end;
			if str <> '' then begin
				Tabla.Cells[temposz, josor]	:= str;
			end;
			Inc(temposz);
			if temposz > maxosz then begin
				maxosz	:= temposz;
			end;
		end else begin
			temposz	:= 0;
		end;
		Inc(sorsza);
	end;
	Tabla.ColCount	:= maxosz;
	temposz			:= SG1.ColCount;
	SG1.ColCount	:= SG1.ColCount  + SG2.ColCount;
	for i := temposz to temposz + SG2.ColCount -1 do begin
		for j := 0 to SG1.RowCount - 1 do begin
			SG1.Cells[i, j] := SG2.Cells[i-temposz+1, j];
		end;
	end;
	Tabla	:= SG1;
	for i := 1 to Tabla.ColCount - 1 do begin
		if Tabla.Cells[i, 0] <> '' then begin
			Tabla.Cells[i, 1]		:= '1';
			vnem				:= Tabla.Cells[i, 0];
			if Pos('**', vnem) > 0 then begin
				Tabla.Cells[i, 0]		:= copy(vnem, 1, Pos('**', vnem) - 1);
				Tabla.Cells[i, 1]		:= '1000';
			end;
			if Pos('*', vnem) > 0 then begin
				Tabla.Cells[i, 0]		:= copy(vnem, 1, Pos('*', vnem) - 1);
				Tabla.Cells[i, 1]		:= '100';
			end;
		end;
	end;
	if SG1.Cells[0,SG1.RowCount - 1] = '' then begin
		SG1.RowCount := SG1.RowCount - 1;
	end;

//	Hibakiiro('�rfolyam beolvas�s 8. '+FormatDateTime('nn.ss.zzz', now - t1 ));
	valutaszam		:= SG1.ColCount - 1;
	datumszam		:= SG1.RowCount - 2;
	Screen.Cursor	:= crDefault;
	Result			:= true;
end;

function TArfolyamToltoDlg.GetDatum(sorsz : integer) : string;
begin
	Result	:= '';
	if ( ( sorsz > ( SG1.RowCount - 2 ) ) or (sorsz < 1) ) then begin
		Exit;
	end;
	Result	:= SG1.Cells[0, 1+sorsz];
end;


function TArfolyamToltoDlg.GetValutanem(sorsz : integer) : string;
begin
	Result	:= '';
	if ( ( sorsz > ( SG1.ColCount - 1 ) ) or (sorsz < 1) ) then begin
		Exit;
	end;
	Result	:= SG1.Cells[sorsz,0];
end;

procedure TArfolyamToltoDlg.FormCanResize(Sender: TObject; var NewWidth,
  NewHeight: Integer; var Resize: Boolean);
begin
	if ( ( NewWidth < 800 ) or ( NewHeight < 600 ) ) then begin
		Resize := false;
  	end;
end;

function TArfolyamToltoDlg.GetArfolyam(vnem, datum : string; var egyseg : integer) : double;
var
	sorsz	: integer;
	oszsz	: integer;
begin
	Result	:= 0;
	egyseg 	:= 1;
	sorsz	:= SG1.Cols[0].IndexOf(copy(datum, 1, 11));
	if sorsz < 0 then begin
		Exit;
	end;
	oszsz	:= SG1.Rows[0].IndexOf(vnem);
	if oszsz < 0 then begin
		Exit;
	end;
	egyseg	:= StrToIntDef(SG1.Cells[oszsz, 1],0);
	Result	:= StringSzam(SG1.Cells[oszsz, sorsz]);
end;

procedure TArfolyamToltoDlg.FormResize(Sender: TObject);
begin
	BitElkuld.Left	:= ( Panel1.Width - BitElkuld.Width ) DIV 3;
	BitKilep.Left	:= 2 * ( ( Panel1.Width - BitElkuld.Width ) DIV 3);
end;

procedure TArfolyamToltoDlg.BitElkuldClick(Sender: TObject);
begin
//	Tarolas;
Tolto2;
end;

procedure TArfolyamToltoDlg.BitKilepClick(Sender: TObject);
begin
	Close;
end;

procedure TArfolyamToltoDlg.Tarolas;
var
	vszam		: integer;
	datsz		: integer;
	vnem		: string;
	datum		: string;
	egys		: integer;
	arf			: double;
	arf2		: double;
	ujkodszam 	: integer;
begin
	Screen.Cursor	:= crHourGlass;
	// A t�bl�zat adatainak elt�rol�sa az adatb�zisban
	for vszam := 1 to valutaszam do begin
		vnem	:= GetValutanem(vszam);
		// A valutanem ellen�rz�se a sz�t�rban
		Query_Run(EgyebDlg.QueryKozos, 'SELECT * FROM SZOTAR WHERE SZ_FOKOD = ''110'' AND SZ_ALKOD = '''+vnem+''' ');
		if EgyebDlg.QueryKozos.RecordCount < 1 then begin
			// Hi�nyz� valutanem, be kell jegyezni
			Query_Run(EgyebDlg.QueryKozos,  'INSERT INTO SZOTAR ( SZ_FOKOD, SZ_ALKOD, SZ_MENEV, SZ_ERVNY ) VALUES '+
				'( ''110'', '''+vnem+''', '''+vnem+''', 1 ) ', FALSE);
		end;
		for datsz	:= 1 to datumszam do begin
			datum	:= GetDatum(datsz);
			if ( ( Jodatum(datum) ) and (vnem <> '') ) then begin
				arf		:= GetArfolyam(vnem, datum, egys);
				arf2	:= EgyebDlg.ArfolyamErtek( vnem, datum, false);
				if arf2 = 0 then begin
					ujkodszam	:= GetNextCode('ARFOLYAM', 'AR_KOD', 1, 5);
					Query_Insert (EgyebDlg.QueryKozos, 'ARFOLYAM',
						[
						'AR_KOD', 	 IntToStr(ujkodszam),
						'AR_EGYSEG', IntToStr(egys),
						'AR_VALNEM', ''''+vnem+'''',
						'AR_ERTEK', SqlSzamString(arf,12,4),
						'AR_DATUM', ''''+datum+'''',
						'AR_MEGJE', ''''+WEBADDRESS+''''
						] );
				end;
			end;
		end;
	end;
	Screen.Cursor	:= crDefault;
end;

end.
