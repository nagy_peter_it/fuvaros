unit URLCache;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ImgList, Vcl.StdCtrls, Data.DB,
  Data.Win.ADODB, KozosTipusok;


type
  TURLCacheDlg = class(TForm)
    qryGetPermanentCache: TADOQuery;
    qryGetPermanentCacheUC_OBJECT: TBlobField;
    qryPutPermanentCache: TADOQuery;
    BlobField1: TBlobField;
    qryGetPermanentCacheUC_CACHEID: TStringField;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    FURLCache: TUrlcache;
    function PermanentCacheGetObject(var ACacheItem: TUrlcacheItem): boolean;
    procedure PermanentCachePutObject(var ACacheItem: TUrlcacheItem);
    procedure PermanentCacheGetAll(const AIDlista: string);
    procedure MemoryCachePutObject(ACacheItem: TUrlcacheItem);

    function MemoryCacheGetAccessedKeys: string;
  public
    function MemoryCacheGetObject(var ACacheItem: TUrlcacheItem): boolean;
    function GetCacheIDFromURL(const URL: string): string;
    function MemoryCachePersistAccessedKeys: boolean;
  end;

const
  cacheAgingDays = '7';

var
  URLCacheDlg: TURLCacheDlg;

implementation

{$R *.dfm}

uses
  Kozos, Egyeb, J_SQL, HTTPRutinok;

procedure TURLCacheDlg.FormClose(Sender: TObject; var Action: TCloseAction);
begin
 //  MemoryCachePersistAccessedKeys;
end;

procedure TURLCacheDlg.FormCreate(Sender: TObject);
var
  i: integer;
  MyKeys: string;
begin
  SetLength(FURLCache, 0);
  MyKeys:= MemoryCacheGetAccessedKeys;
  PermanentCacheGetAll(MyKeys);
end;


procedure TURLCacheDlg.MemoryCachePutObject(ACacheItem: TUrlcacheItem);
begin
  SetLength(FURLCache, Length(FURLCache)+1);
  FURLCache[Length(FURLCache)-1]:= ACacheItem;
end;

// function TURLCacheDlg.CacheGetObject(const AImageCacheID: string; var AMimeType: string; var AStream: TMemoryStream): boolean;
function TURLCacheDlg.MemoryCacheGetObject(var ACacheItem: TUrlcacheItem): boolean;
var
  i: integer;
  found, isPermanentOK: boolean;
  responseStream: TMemoryStream;
  Res: string;
begin
  // EgyebDlg.AppendLog('CACHE', FormatDatetIme('HH:mm:ss:zzz', now)+' Getting memory '+ACacheItem.cacheid);
  Result:= False;
  found:= False;
  for i:=0 to Length(FURLCache)-1 do begin
     if FURLCache[i].cacheid = ACacheItem.cacheid then begin
        ACacheItem:= FURLCache[i];
        FURLCache[i].isaccessed:= True;
        Result:= True;
        found:= true;
        break;
        end; // if
     end; // for
  if not found then begin  // not in the memory
      // EgyebDlg.AppendLog('CACHE', FormatDatetIme('HH:mm:ss:zzz', now)+' Getting permanent '+ACacheItem.cacheid);
      isPermanentOK:= PermanentCacheGetObject(ACacheItem);
      if isPermanentOK then begin
          // EgyebDlg.AppendLog('CACHE', FormatDatetIme('HH:mm:ss:zzz', now)+' Permanent OK '+ACacheItem.cacheid);
          Result:= True;
          end
      else begin  // download it now
        responseStream := TMemoryStream.Create;
        Res:= HttpGetBinary(ACacheItem.url, responseStream);
        if Res = '' then begin
          ACacheItem.stream:= responseStream;
          ACacheItem.isaccessed:= True;
          MemoryCachePutObject(ACacheItem);
          PermanentCachePutObject(ACacheItem); // store in permanent cache
          // EgyebDlg.AppendLog('CACHE', FormatDatetIme('HH:mm:ss:zzz', now)+' Download OK '+ACacheItem.cacheid);
          Result:= True;
          end
        else begin
          Result:= False;
          end;
        end; //  not in cache
      end; // not in the memory before
end;

function TURLCacheDlg.PermanentCacheGetObject(var ACacheItem: TUrlcacheItem): boolean;
var
  i: integer;
  responseStream: TMemoryStream;
  S: string;
begin
  S:= 'select UC_OBJECT, UC_CACHEID from URLCACHE where UC_CACHEID='''+ACacheItem.cacheid+''''+
      ' and datediff(dd, UC_TIMESTAMP, SYSDATETIME())<='+cacheAgingDays;
  if Query_run(qryGetPermanentCache, S) and (qryGetPermanentCache.RecordCount = 1) then begin  // found item
     responseStream:= TMemoryStream.Create;
     qryGetPermanentCacheUC_OBJECT.SaveToStream(responseStream);
     ACacheItem.stream:= responseStream;
     ACacheItem.isaccessed:= True;
     MemoryCachePutObject(ACacheItem); // if permanent cache requested, it does not exists in memory cache
     Result:= True;
     end
  else begin
    Result:= False;  // query failed
    end;
end;

procedure TURLCacheDlg.PermanentCacheGetAll(const AIDlista: string);
var
  i: integer;
  responseStream: TMemoryStream;
  S: string;
  UrlcacheItem: TUrlcacheItem;
begin
  if (AIDlista = '') then exit;  // nothing to do
  EgyebDlg.AppendLog('CACHE', 'Getting all to memory...');
  S:= 'select UC_OBJECT, UC_CACHEID from URLCACHE where UC_CACHEID in ('+AIDlista+')'+
      ' and datediff(dd, UC_TIMESTAMP, SYSDATETIME())<='+cacheAgingDays;
  if Query_run(qryGetPermanentCache, S) then begin
    while not qryGetPermanentCache.eof do begin
       responseStream:= TMemoryStream.Create;
       qryGetPermanentCacheUC_OBJECT.SaveToStream(responseStream);
       UrlcacheItem.stream:= responseStream;
       UrlcacheItem.cacheid:= qryGetPermanentCacheUC_CACHEID.AsString;
       UrlcacheItem.url:= '';
       UrlcacheItem.isaccessed:= False; // just loaded
       MemoryCachePutObject(UrlcacheItem);
       qryGetPermanentCache.Next;
       end; // while
    EgyebDlg.AppendLog('CACHE', 'Getting all to memory FINISHED.');
    end // if
  else begin
    EgyebDlg.AppendLog('CACHE', 'Query_run failed: '+S);
    end;
end;

procedure TURLCacheDlg.PermanentCachePutObject(var ACacheItem: TUrlcacheItem);
var
  S: string;
begin
  S:= 'if exists(select UC_CACHEID from URLCACHE where UC_CACHEID= :CacheID1) begin '+
      'delete from URLCACHE where UC_CACHEID= :CacheID2 end '+
      'insert into URLCACHE (UC_CACHEID, UC_OBJECT, UC_TIMESTAMP) values (:CacheID3, :CacheObject, SYSDATETIME()) ';
  // m�r benne van, csak ide is kitettem, hogy kereshet� legyen.
  // qryPutPermanentCache.SQL:= S;
  qryPutPermanentCache.Parameters.ParamByName('CacheID1').Value:= ACacheItem.cacheid;
  qryPutPermanentCache.Parameters.ParamByName('CacheID2').Value:= ACacheItem.cacheid;
  qryPutPermanentCache.Parameters.ParamByName('CacheID3').Value:= ACacheItem.cacheid;
  qryPutPermanentCache.Parameters.ParamByName('CacheObject').LoadFromStream(ACacheItem.stream, ftBlob);
  qryPutPermanentCache.ExecSQL;
end;

function TURLCacheDlg.MemoryCachePersistAccessedKeys: Boolean;
var
  i: integer;
  lista, S: string;
begin
  lista:= '';
  for i:=0 to Length(FURLCache)-1 do begin
    if FURLCache[i].isaccessed then begin
      lista:= Felsorolashoz(lista, ''''''+FURLCache[i].cacheid+'''''', ',', True);
      end;
    end;
  S:= 'update JELSZO set JE_CACHELIST='''+lista+''' where JE_FEKOD='''+EgyebDlg.user_code+'''';
  Result:= Query_run(EgyebDlg.Query3, S);
end;

function TURLCacheDlg.MemoryCacheGetAccessedKeys: string;
var
  S: string;
begin
  S:= 'select JE_CACHELIST from JELSZO where JE_FEKOD='''+EgyebDlg.user_code+'''';
  Query_run(EgyebDlg.Query3, S);
  if not EgyebDlg.Query3.Eof then
    Result:= EgyebDlg.Query3.Fields[0].AsString
  else Result:= '';
end;

function TURLCacheDlg.GetCacheIDFromURL(const URL: string): string;
// stage1: https://truckpit-staging.s3.eu-central-1.amazonaws.com/production/avatars/groups/23.jpg?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAIIJC2K
// stage2: https://truckpit-staging.s3.eu-central-1.amazonaws.com/production/avatars/groups/23.jpg
// stage3: production/avatars/groups/23.jpg

var
  Stage2, Stage3: string;
begin
  Stage2:= EgyebDlg.SepFrontToken('?', URL);
  Stage3:= EgyebDlg.SepRest(EgyebDlg.S3_BASE_URL, Stage2);
  Result:= Stage3;
end;


end.
