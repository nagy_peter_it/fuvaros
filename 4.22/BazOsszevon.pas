unit BazOsszevon;

interface

uses
	SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
	Forms, Dialogs, StdCtrls, Buttons, Mask, DB, DBTables, ADODB;

type
	TBazOsszevonbeDlg = class(TForm)
	BitElkuld: TBitBtn;
	BitKilep: TBitBtn;
	Label2: TLabel;
	Label3: TLabel;
    M1: TMaskEdit;
    M2: TMaskEdit;
    Query1: TADOQuery;
    CB1: TComboBox;
	procedure FormCreate(Sender: TObject);
	procedure Tolto(fokod, alkod, nev : string );
	procedure BitElkuldClick(Sender: TObject);
	procedure BitKilepClick(Sender: TObject);
	private
		fokodstr	: string;
		kodlista	: TStringList;
	public
	end;

var
	BazOsszevonbeDlg: TBazOsszevonbeDlg;

implementation

uses
	J_SQL, Egyeb, Kozos;
{$R *.DFM}

procedure TBazOsszevonbeDlg.FormCreate(Sender: TObject);
begin
	EgyebDlg.SeTADOQueryDatabase(Query1);
	fokodstr		:= '';
end;

procedure TBazOsszevonbeDlg.Tolto(fokod, alkod, nev : string );
begin
	fokodstr 	:= fokod;
   M1.Text		:= alkod;
   M2.Text		:= nev;
	Query_Run(Query1,'SELECT * FROM SZOTAR WHERE SZ_FOKOD='''+fokod+''' AND SZ_ALKOD <> '''+alkod+''' ');
   kodlista.Clear;
   CB1.Items.Clear;
   while not Query1.Eof do begin
   	kodlista.Add(Query1.FieldByName('SZ_ALKOD').AsString);
       CB1.Items.Add(Query1.FieldByName('SZ_ALKOD').AsString+' - '+Query1.FieldByName('SZ_MENEV').AsString);
   	Query1.Next;
   end;
end;

procedure TBazOsszevonbeDlg.BitElkuldClick(Sender: TObject);
var
	ujkod	: string;
begin
	if NoticeKi('Val�ban �ssze k�v�nja vonni a k�t elemet? FIGYELEM! A r�gi k�d t�rl�sre ker�l!', NOT_QUESTION) = 0 then begin
		if M1.Text <> '' then begin
   		ujkod := kodlista[CB1.ItemIndex];
			case StrToIntdef(fokodstr,0) of
            	100:
               	begin

						Query_Run(Query1, 'DELETE FROM SZOTAR WHERE SZ_FOKOD='''+fokodstr+''' AND SZ_ALKOD <> '''+M1.Text+''' ');
                   end;
            	101:
               	begin

						Query_Run(Query1, 'DELETE FROM SZOTAR WHERE SZ_FOKOD='''+fokodstr+''' AND SZ_ALKOD <> '''+M1.Text+''' ');
                   end;
            	110:
               	begin

						Query_Run(Query1, 'DELETE FROM SZOTAR WHERE SZ_FOKOD='''+fokodstr+''' AND SZ_ALKOD <> '''+M1.Text+''' ');
                   end;
            	112:
               	begin
						Query_Run(Query1, 'DELETE FROM SZOTAR WHERE SZ_FOKOD='''+fokodstr+''' AND SZ_ALKOD <> '''+M1.Text+''' ');
                   end;
            	113:
               	begin

						Query_Run(Query1, 'DELETE FROM SZOTAR WHERE SZ_FOKOD='''+fokodstr+''' AND SZ_ALKOD <> '''+M1.Text+''' ');
                   end;
            	120:
               	begin

						Query_Run(Query1, 'DELETE FROM SZOTAR WHERE SZ_FOKOD='''+fokodstr+''' AND SZ_ALKOD <> '''+M1.Text+''' ');
                   end;
            	125:
               	begin

						Query_Run(Query1, 'DELETE FROM SZOTAR WHERE SZ_FOKOD='''+fokodstr+''' AND SZ_ALKOD <> '''+M1.Text+''' ');
                   end;
            	127:
               	begin

						Query_Run(Query1, 'DELETE FROM SZOTAR WHERE SZ_FOKOD='''+fokodstr+''' AND SZ_ALKOD <> '''+M1.Text+''' ');
                   end;
            	130:
               	begin

						Query_Run(Query1, 'DELETE FROM SZOTAR WHERE SZ_FOKOD='''+fokodstr+''' AND SZ_ALKOD <> '''+M1.Text+''' ');
                   end;
            	200:
               	begin

						Query_Run(Query1, 'DELETE FROM SZOTAR WHERE SZ_FOKOD='''+fokodstr+''' AND SZ_ALKOD <> '''+M1.Text+''' ');
                   end;
            	210:
               	begin

						Query_Run(Query1, 'DELETE FROM SZOTAR WHERE SZ_FOKOD='''+fokodstr+''' AND SZ_ALKOD <> '''+M1.Text+''' ');
                   end;
            	300:
               	begin

						Query_Run(Query1, 'DELETE FROM SZOTAR WHERE SZ_FOKOD='''+fokodstr+''' AND SZ_ALKOD <> '''+M1.Text+''' ');
                   end;
            	310:
               	begin

						Query_Run(Query1, 'DELETE FROM SZOTAR WHERE SZ_FOKOD='''+fokodstr+''' AND SZ_ALKOD <> '''+M1.Text+''' ');
                   end;
            	320:
               	begin

						Query_Run(Query1, 'DELETE FROM SZOTAR WHERE SZ_FOKOD='''+fokodstr+''' AND SZ_ALKOD <> '''+M1.Text+''' ');
                   end;
            	330:
               	begin

						Query_Run(Query1, 'DELETE FROM SZOTAR WHERE SZ_FOKOD='''+fokodstr+''' AND SZ_ALKOD <> '''+M1.Text+''' ');
                   end;
            	340:
               	begin

						Query_Run(Query1, 'DELETE FROM SZOTAR WHERE SZ_FOKOD='''+fokodstr+''' AND SZ_ALKOD <> '''+M1.Text+''' ');
                   end;
            	350:
               	begin

						Query_Run(Query1, 'DELETE FROM SZOTAR WHERE SZ_FOKOD='''+fokodstr+''' AND SZ_ALKOD <> '''+M1.Text+''' ');
                   end;
            	360:
               	begin

						Query_Run(Query1, 'DELETE FROM SZOTAR WHERE SZ_FOKOD='''+fokodstr+''' AND SZ_ALKOD <> '''+M1.Text+''' ');
                   end;
            	950:
               	begin

						Query_Run(Query1, 'DELETE FROM SZOTAR WHERE SZ_FOKOD='''+fokodstr+''' AND SZ_ALKOD <> '''+M1.Text+''' ');
                   end;
            	960:
           end;
       end;
	end;
end;

procedure TBazOsszevonbeDlg.BitKilepClick(Sender: TObject);
begin
	Close;
end;

end.
