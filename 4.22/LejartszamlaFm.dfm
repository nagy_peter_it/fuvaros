�
 TLEJARTSZAMLAFMDLG 0   TPF0TLejartszamlaFmDlgLejartszamlaFmDlgTag� Left*Top� Caption   Lejárt számlákClientHeightyClientWidthSColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameArial
Font.Style 
KeyPreview	OldCreateOrder	PositionpoScreenCenterOnCreate
FormCreateOnResize
FormResizePixelsPerInch`
TextHeight TPanelPanelLejaratLeft Top0Width�HeightTabOrder  TRadioButtonRadioButton1LeftTopWidth1HeightCaptionMindChecked	TabOrder TabStop	OnClickRadioButton1Click  TRadioButtonRadioButton2LeftHTopWidth� HeightCaption   Több mint 30 napja lejártTabOrderOnClickRadioButton2Click  TRadioButtonRadioButton3Left� TopWidth� HeightCaption   Több mint 60 napja lejártTabOrderOnClickRadioButton3Click   TPanel	PanelInfoLefthTop� Width�HeightSTabOrder TLabelLLabel1LeftTopWidthZHeight	AlignmenttaRightJustifyCaption   Fizetési morál :  TLabelLLabel2Left� TopWidth	HeightCaption()Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelLLabel3Left� TopWidth� HeightCaption<   (100% alatt késedelmes fizető, 100% felett előre fizető)Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLLabel4Left!TopWidth>Height	AlignmenttaRightJustifyCaptionH / F nap :  TLabelLLabel5Left� TopWidthHHeightCaption7   (Átlagos fizetési határidő / Tényleges kifizetés)Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelLLabel6LeftTop9Width[Height	AlignmenttaRightJustifyCaption   Lejárt tartozás :  TLabelLLabel7Left� Top9WidthHeightCaptionFtFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TEditEditL3LeftaTopWidth9HeightReadOnly	TabOrder Text100  TEditEditL4LeftaTopWidth9HeightReadOnly	TabOrderText100  TDBEditDBEdit1LeftaTop7WidthYHeight	DataFieldosszeg
DataSourceDataSourceL3TabOrder   TPanelPanelOsszesenLeft@Top\Width�Height'TabOrder TLabelLLabel11LeftTop	WidthRHeightCaption
   Összesen:Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFont  TLabelLLabel12Left� Top	Width"HeightCaptionHUFFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFont  TLabelLLabel13Left�Top	Width#HeightCaptionEURFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFont  TEditEditL1LeftiTopWidth� HeightFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontReadOnly	TabOrder   TEditebSumEurLeft)TopWidthhHeightFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontReadOnly	TabOrder   TBitBtn	BitSzamlaTagLeft Top`WidthdHeightHint   Vagy dupla klikk a számlánHelpContextr9Caption   Számla
Glyph.Data
z  v  BMv      v   (                                       �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 333     333wwwww333����?���??� 0  � �w�ww?sw7������ws3?33�࿿ ���w�3ws��7�������w�3?��7࿿  �w�3wwss7��������w�?���37�   ��w�wwws?� � �� �ws�w73w730 ���37wss3?�330���  33773�ww33��33s7s730���37�33s3	�� 33ws��w3303   3373wwws3	NumGlyphsParentShowHintShowHint	TabOrderTabStopOnClickBitSzamlaClick  TBitBtnButton1TagLeft(Top� WidthdHeightHint   Vagy dupla klikk a számlánHelpContextr9Caption   Számla	NumGlyphsParentShowHintShowHint	TabOrderTabStop  TBitBtnBitBtn1Tag	Left� Top`WidthdHeightHelpContextr9Caption   Fiz.felszólításEnabled
Glyph.Data
z  v  BMv      v   (                                       �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 333     333wwwww333����?���??� 0  � �w�ww?sw7������ws3?33�࿿ ���w�3ws��7�������w�3?��7࿿  �w�3wwss7��������w�?���37�   ��w�wwws?� � �� �ws�w73w730 ���37wss3?�330���  33773�ww33��33s7s730���37�33s3	�� 33ws��w3303   3373wwws3	NumGlyphsParentShowHintShowHint	TabOrderTabStop  TBitBtnBitBtn2Tag	Left@Top8WidthdHeightHint-   Kifizetési adatok átvétele a ServantesbőlHelpContextr9Caption   Adatok átvétele	NumGlyphsParentShowHintShowHint	TabOrderTabStopOnClickBitBtn2Click  	TADOQueryQueryL3
ConnectionEgyebDlg.ADOConnectionAlap
ParametersNameVKOD
Attributes
paNullable DataTypeftStringNumericScale� 	Precision� SizeValue   SQL.Strings.select sum(sa_osszeg) osszeg from lejartszamlawhere sa_vevokod= :VKOD     Left�Topq 	TBCDFieldQueryL3osszeg	FieldNameosszegReadOnly	DisplayFormat0,	Precision    	TADOQueryQueryL2
ConnectionEgyebDlg.ADOConnectionAlap
ParametersNameVKOD
Attributes
paNullable DataTypeftStringNumericScale� 	Precision� SizeValue   SQL.Stringsselect * from fizetesimoralwhere sa_vevokod= :VKOD     Left�Topq TStringFieldQueryL2sa_vevokod	FieldName
sa_vevokodSize  TStringFieldQueryL2sa_vevonev	FieldName
sa_vevonevSizeP  TIntegerField	QueryL2db	FieldNamedbReadOnly	  TIntegerFieldQueryL2hnap	FieldNamehnapReadOnly	  	TBCDFieldQueryL2lnap	FieldNamelnapReadOnly		Precision Size  TIntegerFieldQueryL2moral	FieldNamemoralReadOnly	   TDataSourceDataSourceL3DataSetQueryL3Left�Top�   	TADOQuery	SERVANTES
ConnectionEgyebDlg.ADOConnectionServantes
CursorTypectStatic
Parameters SQL.Stringsselect * from VEVO_SZLA_EGYENwhere year(dkiegy)>1900order by bizonylat LeftTopx TStringFieldSERVANTESBIZONYLAT	FieldName	BIZONYLATReadOnly		FixedChar	Size  TDateTimeFieldSERVANTESDTELJESIT	FieldName	DTELJESITReadOnly	  TDateTimeFieldSERVANTESDBIZONY	FieldNameDBIZONYReadOnly	  TDateTimeFieldSERVANTESDESEDEK	FieldNameDESEDEKReadOnly	  TStringFieldSERVANTESSZOVEG	FieldNameSZOVEGReadOnly	Size(  TDateTimeFieldSERVANTESDKIEGY	FieldNameDKIEGYReadOnly	  TFloatFieldSERVANTESPART_FO	FieldNamePART_FOReadOnly	  TStringFieldSERVANTESPA_NEVROV	FieldName	PA_NEVROVReadOnly		FixedChar	Sizex  TStringFieldSERVANTESREGIKOD	FieldNameREGIKODReadOnly		FixedChar	Size<  TFloatFieldSERVANTESSELOIR	FieldNameSELOIRReadOnly	  TFloatFieldSERVANTESSKIEGY	FieldNameSKIEGYReadOnly	  TStringFieldSERVANTESVALUTAJEL	FieldName	VALUTAJELReadOnly		FixedChar	Size   TADODataSetSZFEJ
ConnectionEgyebDlg.ADOConnectionAlapCommandTextwselect SA_KOD, SA_KIEDAT, SA_KIEOSS, SA_KIEVAL, SA_VEVOKOD from SZFEJ
where sa_resze=0 and sa_kod<>''
order by sa_kod
Parameters Left�Top�  TStringFieldSZFEJSA_KOD	FieldNameSA_KODSize  TStringFieldSZFEJSA_KIEDAT	FieldName	SA_KIEDATSize  	TBCDFieldSZFEJSA_KIEOSS	FieldName	SA_KIEOSS	Precision  TStringFieldSZFEJSA_KIEVAL	FieldName	SA_KIEVALSize  TStringFieldSZFEJSA_VEVOKOD	FieldName
SA_VEVOKODSize   TADODataSetSZFEJ2
ConnectionEgyebDlg.ADOConnectionAlapCommandTextiselect SA_KOD, SA_KIEDAT, SA_KIEOSS, SA_KIEVAL,SA_VEVOKOD  from SZFEJ2
where sa_kod<>''
order by sa_kod
Parameters Left(Top�  TStringFieldSZFEJ2SA_KOD	FieldNameSA_KODSize  TStringFieldSZFEJ2SA_KIEDAT	FieldName	SA_KIEDATSize  	TBCDFieldSZFEJ2SA_KIEOSS	FieldName	SA_KIEOSS	Precision  TStringFieldSZFEJ2SA_KIEVAL	FieldName	SA_KIEVALSize  TStringFieldSZFEJ2SA_VEVOKOD	FieldName
SA_VEVOKODSize   
TPopupMenu
PopupMenu1Left�Topx 	TMenuItemmiKiegyenlitesMegjegyzesCaption   Kiegyenlítés megjegyzésHint(   Kiegyenlítés megjegyzés mósosításaOnClickmiKiegyenlitesMegjegyzesClick  	TMenuItemMorlmegjelentse1Caption   Morál megjelenítéseOnClickMorlmegjelentse1Click    