unit LejartszamlaFm;

interface

uses
	SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
	StdCtrls, Forms, DBCtrls, DB, DBGrids, DBTables, Grids, ExtCtrls, Buttons,
	ShellApi, Dialogs, J_FOFORMUJ, ADODB, Mask, Menus, Variants,Math;

type
	TLejartszamlaFmDlg = class(TJ_FOFORMUJDLG)
    PanelLejarat: TPanel;
    RadioButton1: TRadioButton;
    RadioButton2: TRadioButton;
    RadioButton3: TRadioButton;
    QueryL3: TADOQuery;
    QueryL3osszeg: TBCDField;
    QueryL2: TADOQuery;
    QueryL2sa_vevokod: TStringField;
    QueryL2sa_vevonev: TStringField;
    QueryL2db: TIntegerField;
    QueryL2hnap: TIntegerField;
    QueryL2lnap: TBCDField;
    QueryL2moral: TIntegerField;
    DataSourceL3: TDataSource;
    PanelInfo: TPanel;
    LLabel1: TLabel;
    EditL3: TEdit;
    LLabel2: TLabel;
    LLabel3: TLabel;
    EditL4: TEdit;
    LLabel4: TLabel;
    LLabel5: TLabel;
    LLabel6: TLabel;
    LLabel7: TLabel;
    DBEdit1: TDBEdit;
    PanelOsszesen: TPanel;
    EditL1: TEdit;
    LLabel11: TLabel;
    BitSzamla: TBitBtn;
    Button1: TBitBtn;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    SERVANTES: TADOQuery;
    SERVANTESBIZONYLAT: TStringField;
    SERVANTESDTELJESIT: TDateTimeField;
    SERVANTESDBIZONY: TDateTimeField;
    SERVANTESDESEDEK: TDateTimeField;
    SERVANTESSZOVEG: TStringField;
    SERVANTESDKIEGY: TDateTimeField;
    SERVANTESPART_FO: TFloatField;
    SERVANTESPA_NEVROV: TStringField;
    SERVANTESREGIKOD: TStringField;
    SERVANTESSELOIR: TFloatField;
    SERVANTESSKIEGY: TFloatField;
    SERVANTESVALUTAJEL: TStringField;
    SZFEJ: TADODataSet;
    SZFEJSA_KOD: TStringField;
    SZFEJSA_KIEDAT: TStringField;
    SZFEJSA_KIEOSS: TBCDField;
    SZFEJSA_KIEVAL: TStringField;
    SZFEJSA_VEVOKOD: TStringField;
    SZFEJ2: TADODataSet;
    SZFEJ2SA_KOD: TStringField;
    SZFEJ2SA_KIEDAT: TStringField;
    SZFEJ2SA_KIEOSS: TBCDField;
    SZFEJ2SA_KIEVAL: TStringField;
    SZFEJ2SA_VEVOKOD: TStringField;
    PopupMenu1: TPopupMenu;
    miKiegyenlitesMegjegyzes: TMenuItem;
    LLabel12: TLabel;
    ebSumEur: TEdit;
    LLabel13: TLabel;
    Morlmegjelentse1: TMenuItem;
	  procedure FormCreate(Sender: TObject);override;
    procedure TablazatFrissit;
    procedure RadioButton2Click(Sender: TObject);
    procedure RadioButton3Click(Sender: TObject);
    procedure RadioButton1Click(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure Query1AfterScroll(DataSet: TDataSet);
    procedure Query1AfterOpen(DataSet: TDataSet);
    procedure BitSzamlaClick(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure Adatlap(cim, kod : string);override;
    procedure miKiegyenlitesMegjegyzesClick(Sender: TObject);
    procedure KiegyenlitesMegjegyzesMutat(TableName: string; UJKOD: string);
    procedure StatMutat;
    procedure Morlmegjelentse1Click(Sender: TObject);
	 private
       szur140     :string;
       kellSzamoltmezoFrissites: boolean;
	 public
	end;

var
	LejartszamlaFmDlg : TLejartszamlaFmDlg;

implementation

uses
	Egyeb, J_SQL, Kozos, SzamlaNezo, Windows, KulcsAnalitikaOlvaso, UniSzovegForm;

{$R *.DFM}

procedure TLejartszamlaFmDlg.FormCreate(Sender: TObject);
var
  SALAPSTR : string;
begin
	Inherited FormCreate(Sender);
	kellujures	:= false;
  kellSzamoltmezoFrissites:= False;

	AddSzuromezoRovid( 'SA_UJKOD', 'LEJARTSZAMLA' );
	AddSzuromezoRovid( 'SA_VEVONEV', 'LEJARTSZAMLA' );
	AddSzuromezoRovid( 'SA_GYUJTO', 'LEJARTSZAMLA' );
//	AddSzuromezoRovid( 'DO_GKKAT', 'DOLGOZO','1' );

	Button1.Parent		:= PanelBottom;
	BitSzamla.Parent		:= PanelBottom;
	BitBtn1.Parent		:= PanelBottom;
	BitBtn2.Parent		:= PanelBottom;
	PanelLejarat.Parent		:= PanelBottom;
	PanelInfo.Parent		:= PanelBottom;
	PanelOsszesen.Parent		:= PanelBottom;

	alapstr		:= 'select * from ('+
                'select sa_kod, sa_ujkod, sa_vevokod, sa_vevonev, sa_kidat, sa_esdat, sa_tedat, sa_valoss, sa_valafa, sa_valnem, '+
                'sa_osszeg, sa_afa, sa_gyujto, sa_lnap, sa_kiemj, a1.AR_ERTEK EURArfolyam, isnull(a2.AR_ERTEK, 1.00) SzamlaValArfolyam '+
                ' from LEJARTSZAMLA '+
                ' left join ARFOLYAM a1 on a1.AR_DATUM = sa_tedat and a1.AR_VALNEM = ''EUR'' '+   // SA_ARFDAT-tal jobb lenne...
                ' left join ARFOLYAM a2 on a2.AR_DATUM = sa_tedat and a2.AR_VALNEM = sa_valnem '+
                ' where sa_osszeg<>0 ) x1 where 1=1'; // 2018-10-19
	FelTolto('Lej�rt sz�ml�k ', 'Lej�rt sz�ml�k ',
			'', 'SA_KOD', alapstr, 'LEJARTSZAMLA', QUERY_ADAT_TAG );
	kulcsmezo			:= 'SA_KOD';
	nevmezo				:= 'SA_VEVONEV';
	width				:= DLG_WIDTH;
	height				:= DLG_HEIGHT;
  DBGrid2.PopupMenu	:= PopupMenu1;

  ButtonUj.Visible:=False;
  ButtonKuld.Visible:=False;
  // ButtonMod.Visible:=False;
  ButtonTor.Visible:=False;
  ButtonPrint.Visible:=False;
  kellSzamoltmezoFrissites:= True;
end;

procedure TLejartszamlaFmDlg.RadioButton2Click(Sender: TObject);
begin
 if RadioButton2.Checked then
 begin
    ALAPSTR:=StringReplace(ALAPSTR,szur140,'',[rfReplaceAll]);
    // szur140:=' where SA_LNAP>30 ';
    szur140:=' and SA_LNAP>30 ';
    ALAPSTR:=ALAPSTR+szur140;
	  TablazatFrissit;
 end;
end;

procedure TLejartszamlaFmDlg.RadioButton3Click(Sender: TObject);
begin
 if RadioButton3.Checked then
 begin
    ALAPSTR:=StringReplace(ALAPSTR,szur140,'',[rfReplaceAll]);
    // szur140:=' where SA_LNAP>60 ';
    szur140:=' and SA_LNAP>60 ';
    ALAPSTR:=ALAPSTR+szur140;
	  TablazatFrissit;
 end;

end;

procedure TLejartszamlaFmDlg.RadioButton1Click(Sender: TObject);
begin
 if RadioButton1.Checked then
 begin
    ALAPSTR:=StringReplace(ALAPSTR,szur140,'',[rfReplaceAll]);
	  TablazatFrissit;
 end;

end;

procedure TLejartszamlaFmDlg.FormResize(Sender: TObject);
begin
	Inherited FormResize(Sender);
  //PanelInfo.Left:=ButtonKilep.Left+ButtonKilep.Width+20;  // PanelBottom.Width-Panel1.Width-100;
  PanelInfo.Left:=Button1.Left;  // PanelBottom.Width-Panel1.Width-100;
  PanelInfo.Top:=ButtonKilep.Top;

  PanelLejarat.Left:=PanelInfo.Left+PanelInfo.Width+20;  // PanelBottom.Width-Panel1.Width-100;
  PanelLejarat.Top:=ButtonKilep.Top;

  PanelOsszesen.Top:=PanelInfo.Top+44;
  PanelOsszesen.Left:=PanelLejarat.Left;

 // Button1.Top:=PanelInfo.Top;
 // Button1.Left:=PanelInfo.Left;
end;

procedure TLejartszamlaFmDlg.KiegyenlitesMegjegyzesMutat(TableName: string; UJKOD: string);
var
  Szoveg, KOD: string;
begin
    Szoveg:= Query1.FieldByName('sa_kiemj').AsString;
    KOD:= Query1.FieldByName(FOMEZO).AsString;
		Application.CreateForm(TUniSzovegFormDlg, UniSzovegFormDlg);
    with UniSzovegFormDlg do begin
      Caption:='Kiegyenl�t�s megjegyz�s m�dos�t�sa';
      lblSzovegelemNeve.Caption:='Kiegy.megj.:';
      meSzoveg.Text:= Szoveg;
      M1.Text:= UJKOD;
      UpdateSQLString:= 'update '+TableName+' set SA_KIEMJ=''%s'' where SA_UJKOD='+Ujkod;
      ShowModal;
      Release;
      end; // with
    TablazatFrissit;
end;


procedure TLejartszamlaFmDlg.TablazatFrissit;
var
  KOD: string;
begin
  kellSzamoltmezoFrissites:= False;
  KOD:= Query1.FieldByName(FOMEZO).AsString;
  SQL_ToltoFo(GetOrderBy(true));
  ModLocate (Query1, FOMEZO, KOD);
  kellSzamoltmezoFrissites:= True;
end;

procedure TLejartszamlaFmDlg.miKiegyenlitesMegjegyzesClick(Sender: TObject);
var
  TableName: string;
begin
  if Query1.FieldByName('sa_gyujto').AsInteger = 1 then TableName:='SZFEJ2'
      else TableName:='SZFEJ';
  KiegyenlitesMegjegyzesMutat(TableName, Query1.FieldByName('sa_ujkod').AsString);
end;

procedure TLejartszamlaFmDlg.Morlmegjelentse1Click(Sender: TObject);
begin
   StatMutat;
end;

procedure TLejartszamlaFmDlg.Adatlap(cim, kod : string);
var
  TableName: string;
begin
  if kod <> '' then begin
    if Query1.FieldByName('sa_gyujto').AsInteger = 1 then TableName:='SZFEJ2'
    else TableName:='SZFEJ';
    KiegyenlitesMegjegyzesMutat(TableName, Query1.FieldByName('sa_ujkod').AsString);
    end; // if
end;

procedure TLejartszamlaFmDlg.StatMutat;
var
  vevokod: string;
  hnap,lnap: integer;
begin
  vevokod:=Query1.fieldbyname('sa_vevokod').AsString;
  QueryL3.Close;
  QueryL3.Parameters[0].Value:=vevokod;
  QueryL3.Open;
  QueryL3.First;
  QueryL2.Close;
  QueryL2.Parameters[0].Value:=vevokod;
  QueryL2.Open;
  QueryL2.First;

  EditL3.Text:=QueryL2moral.AsString+' %';
  LLabel2.Caption:='('+QueryL2db.AsString+' db sz�mla alapj�n)';
  hnap:=0;
  lnap:=0;
  if QueryL2db.AsInteger<>0 then
  begin
    hnap:= Trunc(QueryL2hnap.Value/QueryL2db.Value);
    lnap:= Trunc(QueryL2lnap.Value/QueryL2db.Value);
  end;
  EditL4.Text:= IntToStr(hnap)+' / '+IntToStr(hnap+lnap);

  QueryL2.Close;
end;

procedure TLejartszamlaFmDlg.Query1AfterScroll(DataSet: TDataSet);
var
  vevokod: string;
  hnap,lnap: integer;
begin
	Inherited Query1AfterScroll(Query1);
  {
  if not kellSzamoltmezoFrissites then exit;
  vevokod:=Query1.fieldbyname('sa_vevokod').AsString;
  QueryL3.Close;
  QueryL3.Parameters[0].Value:=vevokod;
  QueryL3.Open;
  QueryL3.First;
  QueryL2.Close;
  QueryL2.Parameters[0].Value:=vevokod;
  QueryL2.Open;
  QueryL2.First;

  EditL3.Text:=QueryL2moral.AsString+' %';
  LLabel2.Caption:='('+QueryL2db.AsString+' db sz�mla alapj�n)';
  hnap:=0;
  lnap:=0;
  if QueryL2db.AsInteger<>0 then
  begin
    hnap:= Trunc(QueryL2hnap.Value/QueryL2db.Value);
    lnap:= Trunc(QueryL2lnap.Value/QueryL2db.Value);
  end;
  EditL4.Text:= IntToStr(hnap)+' / '+IntToStr(hnap+lnap);

  QueryL2.Close;
  }

end;



procedure TLejartszamlaFmDlg.Query1AfterOpen(DataSet: TDataSet);
var
  osszesenHUF, osszesenEUR, EUROsszeg: double;
  //recn: TBookmark;
begin
//	Inherited Query1AfterOpen(Query1);
  osszesenHUF:=0;
  osszesenEUR:=0;
  Query1.DisableControls;
  // kellinfo:=False;
  Query1.First;
  while not Query1.Eof do
  begin
    osszesenHUF:= osszesenHUF+Query1.fieldbyname('SA_OSSZEG').Value;
    if Query1.fieldbyname('SA_VALNEM').AsString = 'EUR' then begin
      osszesenEUR:= osszesenEUR + Query1.fieldbyname('SA_VALOSS').Value;
      end
    else begin
      EUROsszeg:= Query1.fieldbyname('SA_VALOSS').Value * Query1.fieldbyname('SzamlaValArfolyam').Value / Query1.fieldbyname('EURArfolyam').Value;
      osszesenEUR:= osszesenEUR + EUROsszeg;
      end;
    Query1.Next;
  end;
  // kellinfo:=True;
  Query1.First;
  Query1.EnableControls;
  EditL1.Text:=FormatFloat(',0',osszesenHUF);
  ebSumEur.Text:=FormatFloat(',0',osszesenEUR);
end;

procedure TLejartszamlaFmDlg.BitSzamlaClick(Sender: TObject);
begin
		Application.CreateForm(TSzamlaNezoDlg, SzamlaNezoDlg);

    if Query1.FieldByName('sa_gyujto').Value='0' then
    begin
  		SzamlaNezoDlg.SzamlaNyomtatas(Query1.FieldByName('sa_ujkod').AsInteger, 0);
    end
    else
    begin
      SzamlaNezoDlg.OsszesitettNyomtatas(Query1.FieldByName('sa_ujkod').AsInteger, 0);
    end;
		SzamlaNezoDlg.Destroy;
end;



procedure TLejartszamlaFmDlg.BitBtn2Click(Sender: TObject);
var
  db,hdb: integer;
  szamla,fdat,sdat: string;
  sszamla: string;
begin

  if EgyebDlg.KONYVELES_ATAD_FORMA = 'SERVANTES' then begin

      if (MessageBox(0, 'Mehet az adatok �tv�tele?', '', MB_ICONQUESTION or MB_YESNO or MB_DEFBUTTON2) in [ idNo]) then  exit;
      Query_Log_Str('Servantes sz�mla kiegyenl�t�s �tv�tel elindult.', 0, true);
      SERVANTES.Open;

      SZFEJ.Open;
      SZFEJ.First;
      while not SZFEJ.Eof do
      begin
        szamla:=SZFEJSA_KOD.AsString;
        BitBtn2.Caption:=szamla;
        BitBtn2.Update;
        if SERVANTES.Locate('BIZONYLAT',szamla,[loCaseInsensitive])  then
        begin
          //fvkod:=IntToStr(StrToIntDef(SZFEJSA_VEVOKOD.AsString,0));
          //svkod:=IntToStr(StrToIntDef(Trim(SERVANTESREGIKOD.AsString),0));
          sszamla:=Trim(SERVANTESBIZONYLAT.AsString);
          //if (sszamla<>szamla)or(fvkod<>svkod) then   // nem egyezik a sz�mlasz�m, vagy a vev�k�d
          if (sszamla<>szamla) then   // nem egyezik a sz�mlasz�m, vagy a vev�k�d
          begin
            SZFEJ.Next;
            Continue;
          end;
          fdat:=SZFEJSA_KIEDAT.Value;
          sdat:=DateToStr(SERVANTESDKIEGY.Value);
          if Length(sdat)=10 then
            sdat:=sdat+'.';
          if (fdat<>sdat)or(RoundTo(SZFEJSA_KIEOSS.AsFloat,-2) <> RoundTo(SERVANTESSKIEGY.AsFloat,-2))   then     // ment�s
          begin
           Try
            SZFEJ.Edit;
            SZFEJSA_KIEDAT.Value:=sdat;
            SZFEJSA_KIEOSS.Value:=SERVANTESSKIEGY.Value;
            SZFEJSA_KIEVAL.Value:=TRIM(SERVANTESVALUTAJEL.Value);
            SZFEJ.Post;
            inc(db);
           Except
            SZFEJ.Cancel;
           End;
          end;
        end;
        Label1.Caption:=SZFEJSA_KOD.Value;
        Label2.Caption:=IntToStr(db);
        Application.ProcessMessages;

        SZFEJ.Next;
      end;
      SZFEJ.Close;
      ///////////////////////////////////
      SZFEJ2.Open;
      SZFEJ2.First;
      while not SZFEJ2.Eof do
      begin
        szamla:=SZFEJ2SA_KOD.AsString;
        if SERVANTES.Locate('BIZONYLAT',szamla,[loCaseInsensitive])  then
        begin
          //fvkod:=IntToStr(StrToInt(SZFEJSA_VEVOKOD.AsString));
          //svkod:=IntToStr(StrToInt(Trim(SERVANTESREGIKOD.AsString)));
          sszamla:=Trim(SERVANTESBIZONYLAT.AsString);
          if (sszamla<>szamla) then   // nem egyezik a sz�mlasz�m
          begin
            SZFEJ.Next;
            Continue;
          end;
          fdat:=SZFEJ2SA_KIEDAT.Value;
          sdat:=DateToStr(SERVANTESDKIEGY.Value);
          if Length(sdat)=10 then
            sdat:=sdat+'.';
          if (fdat<>sdat)or(RoundTo(SZFEJ2SA_KIEOSS.AsFloat,-2) <> RoundTo(SERVANTESSKIEGY.AsFloat,-2))   then     // ment�s
          begin
           Try
            SZFEJ2.Edit;
            SZFEJ2SA_KIEDAT.Value:=sdat;
            SZFEJ2SA_KIEOSS.Value:=SERVANTESSKIEGY.Value;
            SZFEJ2SA_KIEVAL.Value:=TRIM(SERVANTESVALUTAJEL.Value);
            SZFEJ2.Post;
            inc(db);
           Except
            SZFEJ2.Cancel;
           End;
          end;
        end;
        Label1.Caption:=SZFEJ2SA_KOD.Value;
        Label2.Caption:=IntToStr(db);
        Application.ProcessMessages;

        SZFEJ2.Next;
      end;
      SZFEJ2.Close;
      ////////////////////////////////
      SERVANTES.Close;
      ModLocate(Query1, FOMEZO, Query1.FieldByName(FOMEZO).AsString );
      BitBtn2.Caption:='Adatok �tv�tele';
      BitBtn2.Update;
      ShowMessage('K�sz!');
      end;
  {else begin
      NoticeKi('Nincs m�r Servantes-�nk, ez nem fog menni...');
      // NoticeKi('A Kulcs B�r programb�l a dolgoz�k adatait a "Dolgoz�k karbantart�sa" ablakon a "B�rprogram adatok �tv�tele" gombbal lehet �tvenni!');
      end;}
  if EgyebDlg.KONYVELES_ATAD_FORMA = 'KULCS' then begin
      // Beolvassuk �s feldolgozzuk az XLS t�bl�t
	    Application.CreateForm(TKulcsAnalitikaOlvasoDlg, KulcsAnalitikaOlvasoDlg);
	    KulcsAnalitikaOlvasoDlg.ShowModal;
	    KulcsAnalitikaOlvasoDlg.Destroy;
      Query_Log_Str('Kulcs sz�mla kiegyenl�t�s �tv�tel megt�rt�nt.', 0, true);

      // ModLocate(Query1, FOMEZO, Query1.FieldByName(FOMEZO).AsString );
      TablazatFrissit;
      // ---- helyette
      end;
end;

end.


