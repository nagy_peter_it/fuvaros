unit SzamlaExport;

interface

uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, DBTables, DB, StdCtrls, Buttons, Mask, DBCtrls,
  ExtCtrls, ADODB, FileCtrl, StrUtils, Grids, J_Datamodule;

type
  TSzamlaExportDlg = class(TForm)
    BitBtn6: TBitBtn;
    Label4: TLabel;
    M1: TMaskEdit;
    BitBtn5: TBitBtn;
	 BtnExport: TBitBtn;
    Query1: TADOQuery;
    Query2: TADOQuery;
    Label1: TLabel;
    OpenDialog1: TOpenDialog;
    SG1: TStringGrid;
    Label2: TLabel;
    Label3: TLabel;
    Label5: TLabel;
    M2: TMaskEdit;
    Query3: TADOQuery;
    procedure FormCreate(Sender: TObject);
	 procedure BitBtn6Click(Sender: TObject);
	 procedure BitBtn5Click(Sender: TObject);
    procedure BtnExportClick(Sender: TObject);
	 procedure Feldolgozas;
  private
		kilepes		: boolean;
		JSzamla    	: TJSzamla;
  public
  end;

var
  SzamlaExportDlg: TSzamlaExportDlg;

implementation

uses
	Egyeb, Kozos, J_SQL, J_Excel, HianyVevobe;
{$R *.DFM}

procedure TSzamlaExportDlg.FormCreate(Sender: TObject);
begin
	M1.Text 		:= '';
	EgyebDlg.SetADOQueryDatabase(Query1);
	EgyebDlg.SetADOQueryDatabase(Query2);
	EgyebDlg.SetADOQueryDatabase(Query3);
	Label1.Caption	:= '';
	Label1.Update;
	JSzamla	:= TJSzamla.Create(Self);
end;

procedure TSzamlaExportDlg.BitBtn6Click(Sender: TObject);
begin
   if not kilepes then begin
   	kilepes	:= true;
	end else begin
		Close;
   end;
end;

procedure TSzamlaExportDlg.BitBtn5Click(Sender: TObject);
begin
	// Az �llom�ny kiv�laszt�sa
	OpenDialog1.Filter		:= 'XLS �llom�nyok (*.xls)|*.XLS';
  	OpenDialog1.DefaultExt  := 'XLS';
   OpenDialog1.InitialDir	:= EgyebDlg.DocumentPath;
	if OpenDialog1.Execute then begin
 		M1.Text := OpenDialog1.FileName;
 	end;
end;

procedure TSzamlaExportDlg.BtnExportClick(Sender: TObject);
begin
	BtnExport.Visible	:= false;
	Feldolgozas;
	BtnExport.Visible	:= true;
end;

procedure TSzamlaExportDlg.Feldolgozas;
var
	fn		   	: string;
	EX	  		: TJExcel;
	sorszam		: integer;
	str			: string;
	szkod		: string;
begin
	if M1.Text	= '' then begin
		NoticeKi('A kimeneti �lom�ny nem lehet �res!');
		M1.SetFocus;
		Exit;
	end;
	if FileExists(M1.Text) then begin
		if NoticeKi('L�tez� �llom�ny! Fel�l�rjuk?', NOT_QUESTION) <> 0 then begin
			Exit;
		end;
		DeleteFile(PChar(M1.Text));
	end;
	szkod	:= Format('%5.5d', [StrToIntDef(M2.text,0)]);
	Query_Run(Query1,'Select SA_UJKOD from SZFEJ2 WHERE SA_KOD LIKE ''%'+szkod+'%'' ');
	if Query1.RecordCount < 1 then begin
		NoticeKi('Hi�nyz� sz�mla!');
		M2.SetFocus;
		Exit;
	end;
	if Query1.RecordCount > 1 then begin
		NoticeKi('T�l sok sz�mla!');
		M2.SetFocus;
		Exit;
	end;
	fn	:= m1.Text;
	EX := TJexcel.Create(Self);
	if not EX.OpenXlsFile(fn) then begin
		NoticeKi('Nem siker�lt megnyitnom az Excel �llom�nyt!');
		Exit;
	end;
	EX.ColNumber	:= 10;
	EX.InitRow;
	szkod	:= Query1.FieldByName('SA_UJKOD').AsString;
	Query_Run(Query1, 'Select S2_UJKOD from SZSOR2 WHERE S2_S2KOD ='+szkod+' ORDER BY S2_S2SOR');
	if Query1.RecordCount < 1 then begin
		NoticeKi('A sz�ml�hoz nem tartoznak r�sz-sz�ml�k!');
		M2.SetFocus;
		EX.Free;
		Exit;
	end;
	Kilepes	:= false;
	sorszam	:= 1;
	while not Query1.Eof do begin
		Label1.Caption	:= Query1.FieldByName('S2_UJKOD').AsString;
		Label1.Update;
		str		:= '';
		EX.SetRowcell( 8,  '');
		JSzamla.FillSzamla(Query1.FieldByName('S2_UJKOD').AsString);
		Query_Run(Query2, 'Select SA_KOD, SA_ORSZA, SA_JARAT, SA_MEGJ from SZFEJ WHERE SA_UJKOD ='+Query1.FieldByName('S2_UJKOD').AsString);
		if Query2.RecordCount > 0 then begin
			str	:= Query2.FieldByName('SA_KOD').AsString;
			EX.SetRowcell( 8,  Query2.FieldByName('SA_ORSZA').AsString+'-'+Query2.FieldByName('SA_JARAT').AsString);		// J�ratsz�m
		end;
		EX.SetRowcell( 1,  str);
		str		:= '';
		if Query2.RecordCount > 0 then begin
			str	:= Query2.FieldByName('SA_MEGJ').AsString;
		end;
		EX.SetRowcell( 2,  str);
		str	:= '';
		Query_Run(Query2, 'SELECT MB_MBKOD FROM SZFEJ, JARAT, MEGBIZAS WHERE SA_UJKOD = '+Query1.FieldByName('S2_UJKOD').AsString+
			' AND SA_JARAT = JA_ALKOD AND JA_KOD = MB_JAKOD AND MB_DATUM LIKE ''%'+EgyebDlg.EvSzam+'%'' ');
		if Query2.RecordCount > 0 then begin
//			str	:= Query2.FieldByName('MB_RENSZ').AsString;
		end;
//		EX.SetRowcell( 4,  str);		// Rendsz�m
		EX.SetRowcell( 4,  jszamla.rendszam);		// Rendsz�m
		EX.SetRowcell( 3,  '');
		EX.SetRowcell( 5,  '');
		EX.SetRowcell( 6,  SqlSzamString(jszamla.ossz_bruttoeur, 12,2));
		EX.SetRowcell( 7,  'EUR');
		if StrToIntDef(Query2.FieldByName('MB_MBKOD').AsString, 0) > 0 then begin
			Query_Run(Query3, 'SELECT MIN(MS_FETDAT) FETDAT FROM MEGSEGED WHERE MS_MBKOD = '+Query2.FieldByName('MB_MBKOD').AsString+' AND MS_TIPUS = 0');
			EX.SetRowcell( 3,  Query3.FieldByName('FETDAT').AsString);
			Query_Run(Query3, 'SELECT SUM(MS_FSULY) OSSZSULY FROM MEGSEGED WHERE MS_MBKOD = '+Query2.FieldByName('MB_MBKOD').AsString+' AND MS_TIPUS = 0');
			EX.SetRowcell( 5,  SqlSzamString(StringSzam(Query3.FieldByName('OSSZSULY').AsString), 12,2));
		end;
		EX.SaveRow(sorszam);
		Inc(sorszam);
		Query1.Next;
	end;
	EX.SaveFileAsXls(fn);
	EX.Free;
	Label1.Caption	:= '';
	Label1.Update;
	NoticeKi('A ment�s befejez�d�tt!');
	kilepes	:= true;
end;

end.



