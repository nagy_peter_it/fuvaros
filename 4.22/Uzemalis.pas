unit uzemalis;

interface

uses
  WinTypes, WinProcs, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, QuickRpt, QRExpr, Qrctrls, ExtCtrls, printers, DB, DBTables, Egyeb, J_SQL,
  Grids, Kozos, ADODB ;

(*
		A LISTAGRID tartalma

     Oszlop	Mez�bn�v		Tartalom
     ------	--------		----------------------------------------------------------------
     	 0.		KS_DATUM		d�tum
        1.		KS_JAKOD		j�ratsz�m
        2.		KS_KMORA		km �ra �ll�sa
        3.		KS_UZMENY	    tankolt mennyis�g /liter/
        4.		KS_TELE 		teletank (1 - igen; 0 - nem )
*)

type
  TUzemaLisDlg = class(TForm)
    QRLabel1: TQRLabel;
	 QRLabel13: TQRLabel;
    QRLabel14: TQRLabel;
    Query1: TADOQuery;
    Query2: TADOQuery;
    Query3: TADOQuery;
    QueryAl2: TADOQuery;
    RepUzem: TQuickRep;
    QRBand3: TQRBand;
    QRShape8: TQRShape;
    QRShape7: TQRShape;
    QRShape6: TQRShape;
	 QRShape5: TQRShape;
    QRShape3: TQRShape;
    QRShape1: TQRShape;
    QRLSzamla: TQRLabel;
    QRLabel15: TQRLabel;
    QRLabel16: TQRLabel;
    QRLabel17: TQRLabel;
    QRLabel6: TQRLabel;
    QRSysData1: TQRSysData;
    QRLabel7: TQRLabel;
    QRSysData4: TQRSysData;
    QRLabel8: TQRLabel;
    QRLabel3: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel12: TQRLabel;
    QRLabel22: TQRLabel;
    QRLabel24: TQRLabel;
    QRLabel2: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel11: TQRLabel;
    QRLabel18: TQRLabel;
    QRLabel9: TQRLabel;
    QRLabel10: TQRLabel;
    QRLabel20: TQRLabel;
    QRLabel23: TQRLabel;
    QRShape18: TQRShape;
    QRLabel26: TQRLabel;
    QRShape19: TQRShape;
    QRLabel27: TQRLabel;
	 QRShape21: TQRShape;
    QRLabel28: TQRLabel;
    ListaGrid: TStringGrid;
    QRLabel19: TQRLabel;
    QRBand1: TQRBand;
    QRShape20: TQRShape;
    QRShape17: TQRShape;
    QRShape14: TQRShape;
    QRShape13: TQRShape;
    QRShape12: TQRShape;
    QRShape11: TQRShape;
    QRShape9: TQRShape;
    QRShape2: TQRShape;
    QL1: TQRLabel;
    QL2: TQRLabel;
    QL28: TQRLabel;
    QRShape22: TQRShape;
    QL21: TQRLabel;
    QRLabel29: TQRLabel;
    QRLabel30: TQRLabel;
    QRLabel31: TQRLabel;
    QRLabel32: TQRLabel;
    QRLabel33: TQRLabel;
    QRBand2: TQRBand;
    QRShape10: TQRShape;
    QRShape4: TQRShape;
    QRLabel21: TQRLabel;
    QRShape15: TQRShape;
    QL3: TQRLabel;
	 QRShape16: TQRShape;
    QL4: TQRLabel;
    QL5: TQRLabel;
    QRLabel25: TQRLabel;
    QL6: TQRLabel;
    Query4: TADOQuery;
    Query5: TADOQuery;
    QRShape23: TQRShape;
    QRLabel34: TQRLabel;
    QRLabel35: TQRLabel;
    QRLabel36: TQRLabel;
    QRLabel37: TQRLabel;
    QRShape25: TQRShape;
    QRShape24: TQRShape;
    QL7: TQRLabel;
    QRLabel38: TQRLabel;
    QRShape26: TQRShape;
    QRLabel39: TQRLabel;
    QRLabel40: TQRLabel;
    QRShape27: TQRShape;
    QRShape28: TQRShape;
    QRLabel41: TQRLabel;
    QRShape29: TQRShape;
    QRLabel42: TQRLabel;
    QRLabel43: TQRLabel;
    QRShape30: TQRShape;
    QL20: TQRLabel;
    procedure FormCreate(Sender: TObject);
    procedure	Tolt(rsz, datumtol, datumig, apnorma, telep : string );
    procedure QRBand1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure RepUzemBeforePrint(Sender: TCustomQuickRep;
      var PrintReport: Boolean);
    procedure QRBand2BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure RepUzemNeedData(Sender: TObject; var MoreData: Boolean);
  private
     	reszkm			: double;
     	reszliter		: double;
     	osszkm			: double;
     	osszliter		: double;
     	elozokm			: double;
     	elsorekord		: boolean;
     	kezdokm			: longint;
     	zarokm			: longint;
     	sorszam			: integer;
     	rekordszam		: integer;
     	apehnorma		: boolean;
     	dtol	   		: string;
		elozoev_str		: string;
       kezdotkm        : integer;
  public
	  vanadat			: boolean;
  end;

var
  UzemaLisDlg: TUzemaLisDlg;

implementation

uses
	J_Fogyaszt, StrUtils;
{$R *.DFM}

procedure TUzemaLisDlg.FormCreate(Sender: TObject);
begin
	EgyebDlg.SetADOQueryDatabase(Query1);
	EgyebDlg.SetADOQueryDatabase(Query2);
	EgyebDlg.SetADOQueryDatabase(Query3);
	EgyebDlg.SetADOQueryDatabase(Query4);
	EgyebDlg.SetADOQueryDatabase(Query5);
	EgyebDlg.SetADOQueryDatabase(QueryAl2);
	kezdokm			:= 0;
   kezdotkm        := 0;
	zarokm			:= 0;
	apehnorma		:= false;
	dtol			:= '';
	elozoev_str     := copy(EgyebDlg.v_alap_db, 1, Length(EgyebDlg.v_alap_db) - 4 ) + IntToStr(StrToIntDef(EgyebDlg.Evszam,1000)-1);
end;

procedure	TUzemaLisDlg.Tolt(rsz, datumtol, datumig, apnorma, telep  : string );
var
  sqlstr	: string;
  d1, d2	: string;
  str		: string;
begin
  	EllenListTorol;
   dtol	:= datumtol;
	d1		:= copy(datumtol,1,11);
	d2		:= copy(datumig,1,11);
  {APEH szerinti norm�n�l nem kell megtakar�t�s}
  	apehnorma	:= false;
  	if StrToIntDef(apnorma,0) > 0 then begin
  		apehnorma			:= true;
  	end;

  	{ A kezd� ill. befejez� km -ek kisz�m�t�sa}
  	str		:= datumtol;
  	if Pos( '( ',str) > 0 then begin
  	str	:= copy(str, Pos( '( ',str) + 2, 255);
  	if Pos( ' km',str) > 0 then begin
     	str	:= copy(str, 1, Pos( ' km',str) - 1)
     end;
  end;
	kezdokm	:= StrToIntDef(str, 0);
	str		:= datumig;
	if Pos( '( ',str) > 0 then begin
		str	:= copy(str, Pos( '( ',str) + 2, 255);
		if Pos( ' km',str) > 0 then begin
			str	:= copy(str, 1, Pos( ' km',str) - 1)
		end;
	end;
	zarokm	:= StrToIntDef(str, 0);
 	kezdotkm    := 0;  // ism�telt gener�l�sn�l (nyomtat�s) kezdje el�lr��l

	vanadat := false;
  	if Query_Run(Query3, 'SELECT * FROM GEPKOCSI WHERE GK_RESZ = '''+rsz+''' ', true) then begin
 		if not Query3.EOF then begin
 			vanadat := true;
     	{A fejl�c felt�lt�se}
 			QRLabel16.Caption 	:= rsz;
 			QRLabel5.Caption 	:= Query3.FieldByname('GK_TIPUS').AsString;
        if Query3.FieldByname('GK_KLIMA').AsInteger > 0 then begin
 				QRLabel5.Caption 		:= QRLabel5.Caption 	+ ' (kl�m�s)';
		 end;
        {t�li ill. ny�ri norma kijelz�se}
        if not apehnorma then begin
				QRLabel18.Caption 	:= SzamString(Query3.FieldByname('GK_NORMA').AsFloat,8,2)+
				' / ' +SzamString(Query3.FieldByname('GK_NORNY').AsFloat,8,2);
		 end else begin
				QRLabel18.Caption 	:= SzamString(Query3.FieldByname('GK_APETE').AsFloat,8,2)+
				' / ' +SzamString(Query3.FieldByname('GK_APENY').AsFloat,8,2);
		 end;
			QRLabel17.Caption 	:= datumtol + ' - ' + datumig;
		end;
	end;
	Query3.Close;
	// Az aktu�lis �v
	sqlstr	:= 'SELECT * FROM KOLTSEG WHERE KS_RENDSZ = '''+rsz+''' AND KS_DATUM >= '''+d1+''' '+
		' AND KS_DATUM <= '''+d2+''' AND KS_UZMENY > 0 AND KS_KMORA >= '+IntToStr(kezdokm) +
		' AND KS_TIPUS = 0 '+IfThen(telep='1',' and KS_FIMOD='''+'TELEP''')+IfThen(telep='2',' and KS_FIMOD<>'''+'Telep''');
	if not apehnorma then begin
		sqlstr := sqlstr + ' AND KS_TETAN = 0 ';
	end;
	// sqlstr	:= sqlstr + ' ORDER BY KS_DATUM, KS_KMORA ';
  // sqlstr	:= sqlstr + ' ORDER BY KS_DATUM, KS_KMORA, KS_IDO, KS_TELE ';  // ha egyszerre volt, akkor a TELE legyen az ut�bbi
  sqlstr	:= sqlstr + ' ORDER BY KS_DATUM, KS_KMORA, KS_TELE ';  // a KS_IDO-t kivettem, mert van hogy dupla tankol�sn�l 1 perc elt�r�s van a KOLTSEG-ben.

	Query_Run(Query1, sqlstr, true);
	{A LISTAGRID felt�lt�se}
	ListaGrid.RowCount := ListaGrid.RowCount + Query1.RecordCount;
	Query1.First;
	while not Query1.EOF do begin
		vanadat := true;
		Listagrid.Cells[0, sorszam] 	:= Query1.FieldByname('KS_DATUM').AsString;
		Listagrid.Cells[1, sorszam]	 	:= Query1.FieldByname('KS_JAKOD').AsString;
		Listagrid.Cells[2, sorszam] 	:= Query1.FieldByname('KS_KMORA').AsString;
		Listagrid.Cells[3, sorszam] 	:= Query1.FieldByname('KS_UZMENY').AsString;
		Listagrid.Cells[4, sorszam] 	:= Query1.FieldByname('KS_TELE').AsString;
		Listagrid.Cells[10, sorszam] 	:= Query1.FieldByname('KS_ORSZA').AsString;
		Listagrid.Cells[11, sorszam] 	:= Query1.FieldByname('KS_FIMOD').AsString;
		Listagrid.Cells[12, sorszam]  := Query1.FieldByname('KS_IDO').AsString;
		Inc(sorszam);
		Query1.Next;
	end;
end;

procedure TUzemaLisDlg.QRBand1BeforePrint(Sender: TQRCustomBand;
	   var PrintBand: Boolean);
var
   tkmatlag    : double;
   megtliter   : double;
   megt100km   : double;
begin
	{KM �ra �ll�s�nak ellen�rz�se}
	if ( 	( kezdokm > StrToIntDef(Listagrid.Cells[2, sorszam],0) ) or
		( zarokm < StrToIntDef(Listagrid.Cells[2, sorszam],0) ) ) then begin
		PrintBand	:= false;
 		Inc(sorszam);
     	Exit;
  	end;
	QrLabel29.Caption	:= IntToStr(rekordszam);
	QrLabel30.Caption	:= Listagrid.Cells[0, sorszam];
	QrLabel31.Caption	:= Listagrid.Cells[2, sorszam];
	QrLabel32.Caption	:= Listagrid.Cells[3, sorszam];
	QrLabel33.Caption	:= '-';

   if kezdotkm <> 0 then begin
       if Listagrid.Cells[4, sorszam] = '1' then begin    // KS_TELE
           // Csak a tele tankol�sokn�l sz�molunk
           tkmatlag    := GetTonnaAtlag(QRLabel16.Caption, kezdotkm, StrToIntDef(Listagrid.Cells[2, sorszam], 0), true);
	        QrLabel33.Caption	:= Format('%.1f',[tkmatlag/1000]);
       end;
   end;
   // ha ez a tankol�s tele volt, akkor az �j kezd�tkm ennek a km�ra �rt�ke
   if Listagrid.Cells[4, sorszam] = '1' then begin  // KS_TELE
       kezdotkm            := StrToIntDef(Listagrid.Cells[2, sorszam], 0);   // KS_KMORA
      end;

	QrLabel36.Caption	:= Listagrid.Cells[10, sorszam] + ' ' +  EgyebDlg.Read_SZGrid( '300' , Listagrid.Cells[10, sorszam]);
	QrLabel37.Caption	:= Listagrid.Cells[11, sorszam];
	QrLabel40.Caption	:= Listagrid.Cells[12, sorszam];

	Inc(rekordszam);
	QL1.Caption     := '';
	QL2.Caption     := '';
	QL20.Caption    := '';
	if elsorekord then begin
    if Listagrid.Cells[4, sorszam] = '1' then begin    // kezelj�k le azt, ha az els� adatsor nem teletank (dupla tankol�s ugyanahhoz a km-hez)
  		elsorekord := false;
      end
	end else begin
		reszliter		:= reszliter + StringSzam(Listagrid.Cells[3, sorszam]);
		osszliter		:= osszliter + StringSzam(Listagrid.Cells[3, sorszam]);
	end;
	if StrToIntDef(Listagrid.Cells[4, sorszam],0) = 1 then begin
		{Tele tankn�l a megtett km ill. a fogyaszt�s ki�r�sa}
		reszkm			:= StrToIntDef(Listagrid.Cells[2, sorszam],0) - elozokm;
     	osszkm			:= osszkm + reszkm;
  		elozokm			:= StrToIntDef(Listagrid.Cells[2, sorszam],0);
		QL1.Caption 	:= Format('%.0f',[reszkm]);
     	if reszkm = 0 then begin
     		reszkm 		:= 1;
        	reszliter	:= 0;
     	end;
		QL2.Caption 	:= SzamString(reszliter*100/reszkm, 8, 2);
     	reszliter	:= 0;
       // Az �zemanyag megtakar�t�s ki�r�sa
       // A fogyaszt�si adatok meghat�roz�sa
       Application.CreateForm(TFogyasztasDlg, FogyasztasDlg);
       FogyasztasDlg.ClearAllList;
       FogyasztasDlg.GeneralBetoltes(QRLabel16.Caption, Round(elozokm-reszkm), StrToIntDef(Listagrid.Cells[2, sorszam],0) );
       FogyasztasDlg.CreateTankLista;
       FogyasztasDlg.CreateAdBlueLista;
       FogyasztasDlg.Szakaszolo;
       megtliter   :=FogyasztasDlg.GetMegtakaritas;
       if ( ( reszkm = 0 ) or (rekordszam = 2) ) then begin
           megt100km   := 0;
       end else begin
           megt100km   :=  megtliter*100/reszkm;
       end;                                        
       QL20.Caption   := Format('%.2f', [megt100km]);
       FogyasztasDlg.Destroy;
  	end;
  	{A j�ratsz�m ki�r�sa}
	QL28.Caption 	:= '';
	QL21.Caption	:= '';
//  EllenListTorol;
  	if Listagrid.Cells[1, sorszam] <> '' then begin
		if Query_Run(Query2, 'SELECT * FROM JARAT WHERE JA_KOD = '''+Listagrid.Cells[1, sorszam]+''' ', true) then begin
			if Query2.RecordCount > 0 then begin
				JaratEllenor( Query2.FieldByname('JA_KOD').AsString );
				QL28.Caption 		:= copy(Query2.FieldByName('JA_ORSZ').AsString+URESSTRING,1,3) + '-' + Format('%5.5d',[Query2.FieldByName('JA_ALKOD').AsInteger]);
				{A dolgoz�k beolvas�sa}
				QL21.Caption	:= '';
				Query_Run(Query5, 'SELECT * FROM JARSOFOR WHERE JS_JAKOD = '''+Query2.FieldByname('JA_KOD').AsString+''' ORDER BY JS_SORSZ ');
				while not  Query5.Eof do begin
					QL21.Caption	:= QL21.Caption	+ Query_Select('DOLGOZO', 'DO_KOD', Query5.FieldByname('JS_SOFOR').AsString , 'DO_NAME') + '; ';
					Query5.Next;
				end;
			end;
		end;
	end;
//	EllenListMutat('Nem ellen�rz�tt j�ratok', true);
	Query2.Close;
	Inc(sorszam);
end;

procedure TUzemaLisDlg.RepUzemBeforePrint(Sender: TCustomQuickRep;
      var PrintReport: Boolean);
begin
 	reszkm				:= 0;
  	reszliter			:= 0;
  	osszkm				:= 0;
  	osszliter			:= 0;
  	sorszam				:= 0;
  	elozokm				:= StrToIntDef(Listagrid.Cells[2, sorszam],0);
  	elsorekord			:= true;
  	rekordszam			:= 1;
end;

procedure TUzemaLisDlg.QRBand2BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
var
  megt100km        : double;
  megtliter        : double;
  tkmatlag         : double;
begin
	EllenListMutat('Nem ellen�rz�tt j�ratok', true);
  	QL5.Caption := Format('%.2f',[osszliter]);
  	QL3.Caption := Format('%.0f',[osszkm]);
  	if osszkm = 0 then begin
		QL4.Caption 	:= '0';
  	end else begin
  		QL4.Caption 	:= Format('%.2f',[osszliter*100/osszkm]);
  	end;
   try
       tkmatlag            := GetTonnaAtlag(QRLabel16.Caption, kezdotkm - Round(osszkm), kezdotkm, true);
       QrLabel41.Caption	:= Format('%.1f',[tkmatlag/1000]);
   except
   end;
	{A norma meghat�roz�sa a d�tum alapj�n}

   // A fogyaszt�si adatok meghat�roz�sa
   Application.CreateForm(TFogyasztasDlg, FogyasztasDlg);
   FogyasztasDlg.ClearAllList;
   FogyasztasDlg.GeneralBetoltes(QRLabel16.Caption, kezdokm, zarokm );
   FogyasztasDlg.CreateTankLista;
	FogyasztasDlg.CreateAdBlueLista;
	FogyasztasDlg.Szakaszolo;
   QL6.Caption   := Format('%.2f', [FogyasztasDlg.GetMegtakaritas]);
   megtliter:=FogyasztasDlg.GetMegtakaritas;
   megt100km:=megtliter*100/osszkm;
   QL7.Caption   := Format('%.2f', [megt100km]);
   FogyasztasDlg.TeljesLista;
   FogyasztasDlg.Memo1.Lines.SaveToFile(EgyebDlg.TempLIst + 'UZMEGT.TXT');
   FogyasztasDlg.Destroy;
end;

procedure TUzemaLisDlg.RepUzemNeedData(Sender: TObject; var MoreData: Boolean);
begin
	MoreData	:= sorszam < ListaGrid.RowCount;
end;

end.

