unit Koltimpujbe;

interface

uses
	SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
	Forms, Dialogs, StdCtrls, Buttons, Mask, DB, DBTables, J_ALFORM, ADODB,
  ComCtrls, ExtCtrls;

type
	TKoltImpUjBeDlg = class(TJ_AlformDlg)
	BitElkuld: TBitBtn;
	BitKilep: TBitBtn;
    Label3: TLabel;
    M7: TMaskEdit;
    CValuta: TComboBox;
    Label4: TLabel;
    Label5: TLabel;
    M3: TMaskEdit;
    Label6: TLabel;
    M4: TMaskEdit;
    M5: TMaskEdit;
    Label14: TLabel;
	 M6: TMaskEdit;
    Label15: TLabel;
    Label17: TLabel;
    QueryKoz: TADOQuery;
    QueryAl2: TADOQuery;
    QueryAl: TADOQuery;
    QueryAl3: TADOQuery;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    MaskEdit1: TMaskEdit;
    BitBtn2: TBitBtn;
    MaskEdit2: TMaskEdit;
    MaskEdit3: TMaskEdit;
    CheckBox1: TCheckBox;
    CheckBox2: TCheckBox;
    Label2: TLabel;
    Label8: TLabel;
    Label21: TLabel;
    Label1: TLabel;
    Label26: TLabel;
    M1: TMaskEdit;
    M2: TMaskEdit;
    BitBtn10: TBitBtn;
    M0: TMaskEdit;
    M1B: TMaskEdit;
    OrszagCombo: TComboBox;
    M21: TMaskEdit;
    BitBtn3: TBitBtn;
    M10: TMaskEdit;
    Label12: TLabel;
    M8: TMaskEdit;
    Label7: TLabel;
    M9: TMaskEdit;
    M6A: TMaskEdit;
	procedure BitKilepClick(Sender: TObject);
	procedure FormCreate(Sender: TObject);
	procedure Tolto(cim : string; teko:string); override;
	procedure BitElkuldClick(Sender: TObject);
  procedure MaskEditClick(Sender: TObject);
    procedure Modosit(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskKeyPress(Sender: TObject; var Key: Char);
	 procedure BitBtn2Click(Sender: TObject);
    procedure M2Exit(Sender: TObject);
	 procedure BitBtn10Click(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure AFAComboChange(Sender: TObject);
    procedure M21Exit(Sender: TObject);
    procedure OrszagComboChange(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
	private
	  modosult 		: boolean;
    telemod     : boolean;
	  listavnem		: TStringList;
	  jaratkod		: string;
	  rendszam		: string;
	  afalist 		: TStringList;
	  listafimod	: TStringList;
	  orszaglist	: TStringList;
    HUTOTIP, K_TIPUS: string;
    KTIPUS:integer;
    HUTOS, ADBLUES: boolean;
    MAXPOTTANKL, MAXTANKL : integer;
	public
    JARATBOL: Boolean;
	end;

var
	KoltImpUjBeDlg: TKoltImpUjBeDlg;

implementation

uses
	  Egyeb, J_SQL, J_VALASZTO, Kozos, Windows, Math;

{$R *.DFM}

procedure TKoltImpUjBeDlg.BitKilepClick(Sender: TObject);
begin
	if modosult and BitElkuld.Enabled then begin
       if NoticeKi('M�dosultak az adatok . Ki akar l�pni ment�s n�lk�l?', NOT_QUESTION) = 0 then begin
           ret_kod := '';
           Close;
       end;
  	end else begin
		ret_kod := '';
		Close;
  end;
end;

procedure TKoltImpUjBeDlg.FormCreate(Sender: TObject);
var
	str	: string;
  	sorsz : integer;
begin
	EgyebDlg.SetADOQueryDatabase(QueryAl);
	EgyebDlg.SetADOQueryDatabase(QueryAl2);
	EgyebDlg.SetADOQueryDatabase(QueryAl3);
	EgyebDlg.SetADOQueryDatabase(QueryKoz);
	SetMaskEdits([M1, M1B, M3, M5, M6, M6A, M7, M8, M9]);
	ret_kod 						:= '';
  	jaratkod						:= '';
  	modosult 						:= false;
  	listavnem						:= TStringList.Create;
	SzotarTolt(CValuta, '110' , listavnem, false );
  	CValuta.ItemIndex				:= 0;
	// Az orsz�gok bet�lt�se �s HU be�ll�t�sa
	orszaglist := TStringList.Create;
	SzotarTolt(OrszagCombo,  '300' , orszaglist, false );
	for sorsz	:= 0 to OrszagCombo.Items.Count - 1 do begin
		OrszagCombo.Items[sorsz] := orszaglist[sorsz];
	end;
	OrszagCombo.ItemIndex  	:= orszaglist.IndexOf('HU');
end;

procedure TKoltImpUjBeDlg.Tolto(cim : string; teko:string);
var
	tipus	: integer;
	sorsz	: integer;
begin
	ret_kod 		:= teko;
	Caption 		:= cim;
   jaratkod        := '';
	if ret_kod <> '' then begin		{M�dos�t�s}
		if Query_Run (QueryAl, 'SELECT * FROM KOLTSEG_GYUJTO WHERE KS_KTKOD = '+teko,true) then begin
			jaratkod	  	:= QueryAl.FieldByName('KS_JAKOD').AsString;
			M1.Text 	  	:= QueryAl.FieldByName('KS_JARAT').AsString;
			M10.Text 	  	:= QueryAl.FieldByName('KS_SOKOD').AsString;
			M1B.Text 	  	:= QueryAl.FieldByName('KS_SONEV').AsString;
			M2.Text 	  	:= QueryAl.FieldByName('KS_DATUM').AsString;
			M21.Text 	  	:= QueryAl.FieldByName('KS_IDO').AsString;
           MaskEdit1.Text		:= QueryAl.FieldByName('KS_RENDSZ').AsString;
           MaskEdit2.Text		:= QueryAl.FieldByName('KS_KMORA').AsString;
           MaskEdit3.Text		:= QueryAl.FieldByName('KS_UZMENY').AsString;
           CheckBox1.Checked 	:= QueryAl.FieldByName('KS_TELE').AsInteger > 0;
           CheckBox2.Checked 	:= QueryAl.FieldByName('KS_TETAN').AsInteger > 0;

			M3.Text 		:= SzamString(QueryAl.FieldByName('KS_ARFOLY').AsFloat,12,4);
			M4.Text 		:= SzamString(QueryAl.FieldByName('KS_OSSZEG').AsFloat,12,2);
			M5.Text 		:= QueryAl.FieldByName('KS_FIMOD').AsString;
			M7.Text 		:= QueryAl.FieldByName('KS_MEGJ').AsString;
			M6A.Text 		:= SzamString(QueryAl.FieldByName('KS_AFASZ').AsFloat,12,2);
			M6.Text 		:= SzamString(QueryAl.FieldByName('KS_AFAERT').AsFloat,12,2);
			M8.Text 		:= QueryAl.FieldByName('KS_LEIRAS').AsString;
			M9.Text 		:= QueryAl.FieldByName('KS_IMPFN').AsString;

			ComboSet (CValuta, QueryAl.FieldByName('KS_VALNEM').AsString, listavnem);
			if QueryAl.FieldByName('KS_ORSZA').AsString <> '' then begin
				ComboSet (OrszagCombo,QueryAl.FieldByName('KS_ORSZA').AsString, orszaglist);
			end else begin
				OrszagCombo.ItemIndex  	:= orszaglist.IndexOf('HU');
			end;
		end;
   end;
   if Trim(jaratkod) <> ''  then begin
       // Ha van j�rat, mindent tiltunk
	    SetMaskEdits([M1, M2, M21, MaskEdit1, MaskEdit2, MaskEdit3, M3, M4]);
       BitBtn3.Enabled := false;
       BitBtn2.Enabled := false;
       BitBtn10.Enabled := false;
       CValuta.Enabled := false;
       OrszagCombo.Enabled := false;
       CheckBox1.Enabled := false;
       CheckBox2.Enabled := false;
       BitElkuld.Hide;
   end;

	modosult 	:= false;
end;

procedure TKoltImpUjBeDlg.BitElkuldClick(Sender: TObject);
begin

    Query_Update( QueryAl, 'KOLTSEG_GYUJTO',
    [
    'KS_DATUM', ''''+ M2.Text +'''',
    'KS_ERTEK', SqlSzamString( StringSzam(M4.Text) * StringSzam(M3.Text), 12, 2),
    'KS_IDO', ''''+ M21.Text +'''',
    'KS_KMORA', SqlSzamString( StringSzam(MaskEdit2.Text), 12, 2),
    'KS_MEGJ', ''''+ M7.Text +'''',
    'KS_ORSZA', ''''+ orszaglist[OrszagCombo.ItemIndex] +'''',
    'KS_VALNEM', ''''+ listavnem[CValuta.ItemIndex] +'''',
    'KS_OSSZEG', SqlSzamString( StringSzam(M4.Text) , 12, 2),
    'KS_RENDSZ', ''''+ MaskEdit1.Text +'''',
    'KS_SOKOD', ''''+ M10.Text +'''',
    'KS_SONEV', ''''+ M1B.Text +'''',
    'KS_TELE',     BoolToStr01(CheckBox1.Checked),
    'KS_TETAN',    BoolToStr01(CheckBox2.Checked),
    'KS_UZMENY', SqlSzamString( StringSzam(MaskEdit3.Text), 12, 2)
    ], ' WHERE KS_KTKOD = '+ret_kod );

    Close;
end;

procedure TKoltImpUjBeDlg.MaskEditClick(Sender: TObject);
begin
	with Sender as TMaskEdit do begin
		SelectAll;
  	end;
end;

procedure TKoltImpUjBeDlg.Modosit(Sender: TObject);
begin
	modosult := true;
end;

procedure TKoltImpUjBeDlg.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
	EgyebDlg.FormReturn(Key);
end;

procedure TKoltImpUjBeDlg.MaskKeyPress(Sender: TObject; var Key: Char);
begin
	with Sender as TMaskEdit do begin
		Key := EgyebDlg.Jochar(Text,Key,12,Tag);
  	end;
end;

procedure TKoltImpUjBeDlg.BitBtn2Click(Sender: TObject);
begin
	GepkocsiValaszto(MaskEdit1);
end;

procedure TKoltImpUjBeDlg.BitBtn3Click(Sender: TObject);
begin
	DolgozoValaszto(M10, M1B);
end;

procedure TKoltImpUjBeDlg.M2Exit(Sender: TObject);
begin
	DatumExit(Sender, false);
end;

procedure TKoltImpUjBeDlg.BitBtn10Click(Sender: TObject);
begin
	EgyebDlg.Calendarbe(M2);
end;

procedure TKoltImpUjBeDlg.FormDestroy(Sender: TObject);
begin
	listavnem.Free;
	afalist.Free;
end;

procedure TKoltImpUjBeDlg.AFAComboChange(Sender: TObject);
begin
	modosult := true;
end;

procedure TKoltImpUjBeDlg.M21Exit(Sender: TObject);
begin
	IdoExit(Sender, false);
end;

procedure TKoltImpUjBeDlg.OrszagComboChange(Sender: TObject);
begin
	modosult := true;
end;

end.

