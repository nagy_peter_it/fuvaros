unit GyorshajtReszlet;

interface

uses
	SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
	Forms, Dialogs, StdCtrls, Buttons, Mask, DB, DBTables, J_ALFORM,
	TabNotBk, ComCtrls, Grids, ADODB, ExtCtrls, DBGrids, Shellapi, FileCtrl,
  DBCtrls;

type
	TGyorshajtReszletDlg = class(TJ_AlformDlg)
    Panel1: TPanel;
    BitKilep: TBitBtn;
	 DataSource1: TDataSource;
    Query1: TADOQuery;
    DBGrid1: TDBGrid;
	procedure Tolto(cim : string; teko : string); override;
    procedure BitKilepClick(Sender: TObject);

	private
	public
	end;

var
	GyorshajtReszletDlg: TGyorshajtReszletDlg;

implementation

uses
	Kozos, Egyeb, J_SQL, Kozos_Local, Windows;

{$R *.DFM}


procedure TGyorshajtReszletDlg.BitKilepClick(Sender: TObject);
begin
   Close;
end;


procedure TGyorshajtReszletDlg.Tolto(cim : string; teko:string);
var
  RENSZ, HONAP, S: string;
begin
  RENSZ := EgyebDlg.SepFrontToken('^', teko);
  HONAP := EgyebDlg.SepRest('^', teko);
  S:= 'SELECT GY_RENSZ Rendsz�m, GY_AUTIP T�pus, GY_SEBES Sebess�g, GY_DATUM D�tum, GY_IDOPT Id�, GY_LOCAT Hely ';
  S:= S+ 'FROM NVC_GYORS_SZURT WHERE GY_RENSZ = '''+RENSZ+'''  and GY_HONAP = '''+HONAP+'''   ';;
  S:= S+ ' ORDER BY D�tum, Id�';
  Query_Run (Query1, S ,true);
end;


end.
