unit Kilizing;

interface

uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls, Egyeb,
  Forms, Dialogs, DBTables, DB, StdCtrls, Buttons, Mask, DBCtrls,
  ExtCtrls, ADODB, QuickRpt;

type
  TKilizingDlg = class(TForm)
    BitBtn4: TBitBtn;
    BitBtn6: TBitBtn;
    Label3: TLabel;
    M0: TMaskEdit;
    BitBtn1: TBitBtn;
    M1: TMaskEdit;
    Label1: TLabel;
    BitBtn2: TBitBtn;
	 Label2: TLabel;
    M2: TMaskEdit;
    BitBtn3: TBitBtn;
	 CB1: TCheckBox;
    Label4: TLabel;
    CBSorrend: TComboBox;
    CBValuta: TComboBox;
    Label5: TLabel;
    Query1: TADOQuery;
    CB2: TCheckBox;
    BitBtn32: TBitBtn;
    QRCompositeReport1: TQRCompositeReport;
    Label6: TLabel;
    CBSorrend2: TComboBox;
    procedure FormCreate(Sender: TObject);
    procedure BitBtn4Click(Sender: TObject);
    procedure BitBtn6Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure M1Exit(Sender: TObject);
	 procedure M2Exit(Sender: TObject);
	 procedure BitBtn32Click(Sender: TObject);
	 procedure QRCompositeReport1AddReports(Sender: TObject);
  private
  public
  end;

var
  KilizingDlg: TKilizingDlg;

implementation

uses
	J_VALASZTO, KilizingList, KilizingList2, KilizingList3, J_Sql, Kozos,
  DateUtils;
{$R *.DFM}

procedure TKilizingDlg.FormCreate(Sender: TObject);
var
	vnem	: string;
begin
	EgyebDlg.SetADOQueryDatabase(Query1);
	M0.Text := '';
	M1.Text	:= copy(EgyebDlg.MaiDatum,1,8)+'01.';
	M2.Text	:= EgyebDlg.MaiDatum;
	// A valutanemek felt�lt�se
	Query_Run(Query1, 'SELECT DISTINCT GK_LINEM FROM GEPKOCSI');
	CBValuta.Clear;
	CBValuta.Items.Add('HUF');
	while not Query1.Eof do begin
		vnem	:= UpperCase(Query1.FieldByName('GK_LINEM').AsString);
		if ( (vnem <> 'HUF') and (vnem <> '') ) then begin
			CBValuta.Items.Add(vnem);
		end;
		Query1.Next;
	end;
	CBValuta.ItemIndex	:= CBValuta.Items.Count - 1;
end;

procedure TKilizingDlg.BitBtn4Click(Sender: TObject);
begin
	{A lista �ssze�ll�t�sa a sz�r�si elemek alapj�n}
	if M1.Text	= '' then begin
		NoticeKi('A kezd� d�tum megad�sa k�telez�!');
		Exit;
	end;
	Screen.Cursor	:= crHourGlass;
	Application.CreateForm(TKilizingListDlg, KilizingListDlg);
	KilizingListDlg.kellgrafikon	:= CB2.Checked;
	KilizingListDlg.Tolt(M1.Text, M2.Text, M0.Text, CBValuta.Text, CB1.Checked, CBSorrend.Itemindex);
	Screen.Cursor	:= crDefault;
	if KilizingListDlg.vanadat	then begin
		Application.CreateForm(TKilizingList2Dlg, KilizingList2Dlg);
		KilizingList2Dlg.Tolt(CBSorrend2.ItemIndex);
		QRCompositeReport1.Reports.Clear;
		QRCompositeReport1.Preview;
		KilizingList2Dlg.Destroy;
	end else begin
		NoticeKi('Nincs a felt�teleknek megfelel� t�tel!');
	end;
	KilizingListDlg.Destroy;
end;

procedure TKilizingDlg.BitBtn6Click(Sender: TObject);
begin
	Close;
end;

procedure TKilizingDlg.BitBtn1Click(Sender: TObject);
begin
	GepkocsiValaszto(M0);
end;

procedure TKilizingDlg.BitBtn2Click(Sender: TObject);
begin
	// EgyebDlg.Calendarbe(M1);
 	// EgyebDlg.ElsoNapEllenor(M1, M2);
  EgyebDlg.CalendarBe_idoszak(M1, M2);
end;

procedure TKilizingDlg.BitBtn3Click(Sender: TObject);
var
  dat:Tdate;
begin
	EgyebDlg.Calendarbe(M2);
  dat:=StrToDate(M2.Text);
  M2.Text:=DateToStr(EndOfTheMonth(dat));
end;

procedure TKilizingDlg.M1Exit(Sender: TObject);
begin
	if ( ( M1.Text <> '' ) and ( not Jodatum2(M1 ) ) ) then begin
		NoticeKi('Hib�s d�tum megad�sa!');
		M1.Text := '';
		M1.Setfocus;
	end;
  EgyebDlg.ElsoNapEllenor(M1, M2);
end;

procedure TKilizingDlg.M2Exit(Sender: TObject);
begin
	if ( ( M2.Text <> '' ) and ( not Jodatum2(M2 ) ) ) then begin
		NoticeKi('Hib�s d�tum megad�sa!');
		M2.Text := '';
		M2.Setfocus;
	end;
end;

procedure TKilizingDlg.BitBtn32Click(Sender: TObject);
begin
	M0.Text	:= '';
end;

procedure TKilizingDlg.QRCompositeReport1AddReports(Sender: TObject);
begin
	with QRCompositeReport1.Reports do begin
		Add(KilizingListDlg.Rep);
		Add(KilizingList2Dlg.Rep);
	end;
end;

end.


