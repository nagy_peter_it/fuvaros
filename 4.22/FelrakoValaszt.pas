unit FelrakoValaszt;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Buttons, Vcl.Mask,
  Vcl.ExtCtrls, Vcl.Grids;

type
  TFelrakoValasztDlg = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    BitMentMasNeven: TBitBtn;
    meUjNev: TMaskEdit;
    BitElkuld: TBitBtn;
    SG1: TStringGrid;
    Panel3: TPanel;
    Label19: TLabel;
    Label20: TLabel;
    Label22: TLabel;
    Label21: TLabel;
    M3: TMaskEdit;
    M4: TMaskEdit;
    M5: TMaskEdit;
    M11: TMaskEdit;
    M13: TMaskEdit;
    M12: TMaskEdit;
    M14: TMaskEdit;
    M15: TMaskEdit;
    M2: TMaskEdit;
    procedure BitMentMasNevenClick(Sender: TObject);
  private
    { Private declarations }
  public
    procedure Tolto(RakoNeve, Orszag, ZIP, Varos, UtcaHazszam, Tovabbi1, Tovabbi2, Tovabbi3, Tovabbi4, Tovabbi5: string);
    procedure GridFormazas;
  end;

var
  FelrakoValasztDlg: TFelrakoValasztDlg;

implementation

{$R *.dfm}

uses Kozos, J_SQL;



procedure TFelrakoValasztDlg.BitMentMasNevenClick(Sender: TObject);
begin
  if trim(meUjNev.Text) = '' then begin
    NoticeKi('Az �j felrak� nev�nek kit�lt�se k�telez�!');
		meUjNev.SetFocus;
  	Exit;
	  end;
  if Query_Select('FELRAKO', 'FF_FELNEV', trim(meUjNev.Text), 'FF_FEKOD') <> '' then begin
    NoticeKi('M�r l�tezik felrak� ilyen n�vvel!');
		meUjNev.SetFocus;
  	Exit;
	  end;
  FelrakoElment (trim(meUjNev.Text), M2.Text, M3.Text, M4.Text, M5.Text, M11.Text, M12.Text, M13.Text, M14.Text, M15.Text, '', False);
  // Close;  -- ModalResult miatt en�lk�l is becsukja
end;

procedure TFelrakoValasztDlg.Tolto(RakoNeve, Orszag, ZIP, Varos, UtcaHazszam, Tovabbi1, Tovabbi2, Tovabbi3, Tovabbi4, Tovabbi5: string);
begin
    // M1.Text:= RakoNeve;
    M2.Text:= Orszag;
    M3.Text:= ZIP;
    M4.Text:= Varos;
    M5.Text:= UtcaHazszam;
    M11.Text:= Tovabbi1;
    M12.Text:= Tovabbi2;
    M13.Text:= Tovabbi3;
    M14.Text:= Tovabbi4;
    M15.Text:= Tovabbi5;
    // meUjNev.Text:= GetUjFelrakoNev(RakoNeve);
end;

procedure TFelrakoValasztDlg.GridFormazas;
var
    i: integer;
begin
      i:=0;
      SG1.Cells[0,i]:= 'Fel/Lerak�';
      SG1.Cells[1,i]:= 'Orsz�g';
      SG1.Cells[2,i]:= 'Irsz.';
      SG1.Cells[3,i]:= 'V�ros';
      SG1.Cells[4,i]:= 'C�m';
      SG1.Cells[5,i]:= 'Tov�bbi1';
      SG1.Cells[6,i]:= 'Tov�bbi2';
      SG1.Cells[7,i]:= 'Tov�bbi3';
      SG1.Cells[8,i]:= 'Tov�bbi4';
      SG1.Cells[9,i]:= 'Tov�bbi5';

      SG1.ColWidths[0]	:= 180;
	    SG1.ColWidths[1]	:= 50;
	    SG1.ColWidths[2]	:= 50;
	    SG1.ColWidths[3]	:= 90;
	    SG1.ColWidths[4]	:= 160;
	    SG1.ColWidths[5]	:= 100;
      SG1.ColWidths[6]	:= 100;
      SG1.ColWidths[7]	:= 100;
      SG1.ColWidths[8]	:= 100;
      SG1.ColWidths[9]	:= 100;

end;
end.
