unit PotStatFm;

interface

uses
	SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
	StdCtrls, Forms, DBCtrls, DB, DBGrids, DBTables, Grids, ExtCtrls, Buttons,
	ShellApi, Dialogs, J_FOFORMUJ, ADODB, Mask, Menus, Variants;

type
	TPotStatFmDlg = class(TJ_FOFORMUJDLG)
	 QueryUj1: TADOQuery;
	 QueryUj2: TADOQuery;
	 BitBtn7: TBitBtn;
	 BitBtn8: TBitBtn;
	 QueryUj3: TADOQuery;
    Timer1: TTimer;
    QueryUj4: TADOQuery;
	  procedure FormCreate(Sender: TObject);override;
	 procedure Adatlap(cim, kod : string);override;
	 procedure DBGrid2DrawColumnCell(Sender: TObject; const Rect: TRect;
	   DataCol: Integer; Column: TColumn; State: TGridDrawState);override;
    procedure BitBtn8Click(Sender: TObject);
    procedure BitBtn7Click(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
	 private
	 public
		ret_mskod	: string;
	end;

var
	PotStatFmDlg : TPotStatFmDlg;

implementation

uses
	Egyeb, J_SQL, MegbizasBe, J_VALASZTO, Viszonybe, Kozos, Segedbe2, 
	Idobe, GKErvenyesseg;

{$R *.DFM}
const
sh : string[16] = '0123456789ABCDEF';


procedure TPotStatFmDlg.FormCreate(Sender: TObject);
var
	rszlista	: TStringLIst;
	i			: integer;
	rsz			: string;
begin
//  Application.CreateForm(TFGKErvenyesseg, FGKErvenyesseg);
	EgyebDlg.SeTADOQueryDatabase(QueryUj1);
	EgyebDlg.SeTADOQueryDatabase(QueryUj2);
	EgyebDlg.SeTADOQueryDatabase(QueryUj3);
	EgyebDlg.SeTADOQueryDatabase(QueryUj4);
	// Ellen�rizz�k, hogy minden g�pkocsi benne van-e a list�ban
	rszlista	:= TStringList.Create;
	GetRszLista(rszlista, 1);
	Query_Run(QueryUj2, 'SELECT TP_POTSZ FROM T_POTK ');
	i := 0;
	while i < rszlista.Count do begin
		rsz	:= rszlista[i];
		if not QueryUj2.Locate('TP_POTSZ', rsz , []) then begin
			if Length(rsz) = 7 then begin
				if copy(rsz, 4, 3) <> '000' then begin
					// L�trehozzuk a g�pkocsit
					PotStatTolto(rsz);
				end;
			end;
		end;
		Inc(i);
	end;
	QueryUj2.First;
	while not QueryUj2.Eof do begin
		if rszlista.IndexOf( QueryUj2.FieldByName('TP_POTSZ').AsString ) < 0 then begin
			// T�r�lni kellene a g�pkocsit, mert m�r nem �l
			Query_Run(QueryUj3, 'DELETE FROM T_POTK WHERE TP_POTSZ = '''+QueryUj2.FieldByName('TP_POTSZ').AsString+''' ');
		end;
		QueryUj2.Next;
	end;
	ret_mskod	:= '';
	Inherited FormCreate(Sender);
	kellujures	:= false;
	AddSzuromezoRovid('TP_TINEV', 'T_POTK');
	AddSzuromezoRovid('TP_RENSZ', 'T_POTK');
	AddSzuromezoRovid('TP_GKKAT', 'T_POTK');
	AddSzuromezoRovid('TP_TORSZ', 'T_POTK');
	AddSzuromezoRovid('TP_TENYO', 'T_POTK');
	AddSzuromezoRovid('TP_POTSZ', 'T_POTK');
	AddSzuromezoRovid('TP_SONEV', 'T_POTK');

	alapstr		:= 'SELECT * FROM T_POTK ';
	FelTolto('�j p�tkocsi statisztika felvitele ', 'P�tkocsi statisztika adatok m�dos�t�sa ',
			'P�tkocsi statisztika list�ja', 'TP_POTSZ', alapstr, 'T_POTK', QUERY_ADAT_TAG );
	ButtonUj.Hide;
	ButtonMod.Hide;
	ButtonTor.Hide;
  ButtonKuld.Hide;
	BitBtn7.Parent		:= PanelBottom;
	BitBtn8.Parent		:= PanelBottom;
	width				:= DLG_WIDTH;
	height				:= DLG_HEIGHT;
	Timer1Timer(Sender);
end;

procedure TPotStatFmDlg.Adatlap(cim, kod : string);
begin
//	Application.CreateForm(TMegbizasbeDlg, AlForm);
//	Inherited Adatlap(cim, kod );
//   if kod <> '' then begin
//   	ModLocate (Query1, 'MS_MSKOD', oldmezo1 );
//   end;
end;

procedure TPotStatFmDlg.DBGrid2DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
var
	szin	: integer;
begin
//	oldszin := DBGrid2.Canvas.Brush.Color;
	// A felrak�s/lerak�s sz�n�nek meghat�roz�sa
	if Query1.FieldByName('TP_TEIDO').AsString <> '' then begin
		szin		:= clYellow;
	end else begin
		szin		:= clWindow;
	end;
	DBGrid2.Canvas.Brush.Color		:= szin;
	DBGrid2.DefaultDrawDataCell(Rect, Column.Field, State);
	inherited DBGrid2DrawColumnCell(Sender, Rect, DataCol, Column, State);
//  	DBGrid2.Canvas.Brush.Color		:= oldszin;
end;

procedure TPotStatFmDlg.BitBtn8Click(Sender: TObject);
var
	rendsz	: string;
begin
	// Ellen�rizz�k, hogy �res soron futunk-4
	Timer1.Enabled	:= false;
	if STrToIntDef(Query1.FieldByName('TP_MSKOD').AsString, -1) <= 1 then begin
		Timer1.Enabled	:= true;
		Exit;
	end;
	// Ellen�rizz�k, hogy ki dolgozik m�g ezen a megb�z�son
	Query_Run(QueryUj3, 'SELECT * FROM MEGSEGED WHERE MS_MSKOD = '+Query1.FieldByName('TP_MSKOD').AsString);
	if QueryUj3.RecordCount = 0 then begin
		Timer1.Enabled	:= true;
		Exit;
	end;
	Query_Run(QueryUj2, 'SELECT * FROM MEGBIZAS WHERE MB_MBKOD = '+QueryUj3.FieldByName('MS_MBKOD').AsString);
	if QueryUj2.FieldByName('MB_AFKOD').AsString <> '' then begin
		if QueryUj2.FieldByName('MB_AFKOD').AsString <> EgyebDlg.user_code then begin
			NoticeKi('A megb�z�st �ppen m�s m�dos�tja!');
			Timer1.Enabled	:= true;
			Exit;
		end;
	 end;
	// A kijel�lt felrak� megjelenit�se
	rendsz	:= Query1.FieldByName('TP_RENSZ').AsString;
	Query_Update(QueryUj2, 'MEGBIZAS',
		[
		'MB_AFKOD', ''''+EgyebDlg.user_code+'''',
		'MB_AFNEV', ''''+EgyebDlg.user_name+'''',
		'MB_AFLAG', IntToStr(StrToINtDef(FormatDateTime('nnss', now),0))
		], ' WHERE MB_MBKOD = '+QueryUj3.FieldByName('MS_MBKOD').AsString );
	Application.CreateForm(TMegbizasbeDlg,MegbizasbeDlg);
	MegbizasbeDlg.Tolto('Megb�z�s m�dos�t�sa', QueryUj3.FieldByName('MS_MBKOD').AsString);
	MegbizasbeDlg.ShowModal;
	if MegbizasbeDlg.ret_kod  <> '' then begin
		PotStatTolto(rendsz);
		ModLocate (Query1, 'TP_RENSZ', rendsz );
	end;
	MegbizasbeDlg.Destroy;
	Query_Update(QueryUj3, 'MEGBIZAS',
		[
		'MB_AFKOD', ''''+''+'''',
		'MB_AFNEV', ''''+''+'''',
		'MB_AFLAG', IntToStr(StrToINtDef(FormatDateTime('nnss', now),0))
		], ' WHERE MB_AFKOD = '''+EgyebDlg.user_code+'''' );
	Timer1.Enabled	:= true;
end;

procedure TPotStatFmDlg.BitBtn7Click(Sender: TObject);
var
	rendsz	: string;
begin
	// Ellen�rizz�k, hogy �res soron futunk-4
	Timer1.Enabled	:= false;
	if STrToIntDef(Query1.FieldByName('TP_MSKOD').AsString, -1) <= 1 then begin
		Timer1.Enabled	:= true;
		Exit;
	end;
	// Ellen�rizz�k, hogy ki dolgozik m�g ezen a megb�z�son
	Query_Run(QueryUj3, 'SELECT * FROM MEGSEGED WHERE MS_MSKOD = '+Query1.FieldByName('TP_MSKOD').AsString);
	if QueryUj3.RecordCount = 0 then begin
		Timer1.Enabled	:= true;
		Exit;
	end;
	Query_Run(QueryUj2, 'SELECT * FROM MEGBIZAS WHERE MB_MBKOD = '+QueryUj3.FieldByName('MS_MBKOD').AsString);
	if QueryUj2.FieldByName('MB_AFKOD').AsString <> '' then begin
		if QueryUj2.FieldByName('MB_AFKOD').AsString <> EgyebDlg.user_code then begin
			NoticeKi('A megb�z�st �ppen m�s m�dos�tja!');
			Timer1.Enabled	:= true;
			Exit;
		end;
	 end;
	// A kijel�lt felrak� megjelenit�se
	rendsz	:= Query1.FieldByName('TP_RENSZ').AsString;
	Query_Update(QueryUj2, 'MEGBIZAS',
		[
		'MB_AFKOD', ''''+EgyebDlg.user_code+'''',
		'MB_AFNEV', ''''+EgyebDlg.user_name+'''',
		'MB_AFLAG', IntToStr(StrToINtDef(FormatDateTime('nnss', now),0))
		], ' WHERE MB_MBKOD = '+QueryUj3.FieldByName('MS_MBKOD').AsString );
	Application.CreateForm(TSegedbe2Dlg,Segedbe2Dlg);
	Segedbe2Dlg.tolt('Felrak�s/lerak�s m�dos�t�sa', QueryUj3.FieldByName('MS_MBKOD').AsString,
		QueryUj3.FieldByname('MS_SORSZ').AsString, QueryUj3.FieldByName('MS_MSKOD').AsString);
	Segedbe2Dlg.ShowModal;
	if Segedbe2Dlg.voltenter then begin
		ModLocate (Query1, 'TP_RENSZ', rendsz );
	end;
	Segedbe2Dlg.Destroy;
	Query_Update(QueryUj3, 'MEGBIZAS',
		[
		'MB_AFKOD', ''''+''+'''',
		'MB_AFNEV', ''''+''+'''',
		'MB_AFLAG', IntToStr(StrToINtDef(FormatDateTime('nnss', now),0))
		], ' WHERE MB_AFKOD = '''+EgyebDlg.user_code+'''' );
	Timer1.Enabled	:= true;
end;

function konvert(x : word;alap : integer) : string;
begin
    Result := '';
    if not(alap in [2..16]) then exit;
    if x=0 then exit;
    if (x div alap)<>0 then Result := konvert(x div alap,alap) + sh[(x mod alap)+1]
                       else Result := sh[x+1]
end;

procedure TPotStatFmDlg.Timer1Timer(Sender: TObject);
var
	str		: string;
  eszk,gypoz :integer;
begin
	// Percenk�nt ellen�rizz�k a poz�ci�t
	Timer1.Enabled	:= false;
	Query_Run(QueryUj4, 'SELECT * FROM POZICIOK ORDER BY PO_RENSZ ');
	Query_Run(QueryUj3, 'SELECT * FROM T_POTK ');
	while not QueryUj3.Eof do begin
		if QueryUj4.Locate('PO_RENSZ', QueryUj3.FieldByName('TP_RENSZ').AsString , []) then begin
			if (	(QueryUj4.FieldByName('PO_GEKOD').AsString <> QueryUj3.FieldByName('TP_LOCAT').AsString) or
					(QueryUj4.FieldByName('PO_DATUM').AsString <> QueryUj3.FieldByName('TP_LODAT').AsString) or
					(QueryUj4.FieldByName('PO_DIGIT').AsString <> QueryUj3.FieldByName('TP_DIGIT').AsString) ) then begin
				//str		:= QueryUj4.FieldByName('PO_DIGIT').AsString;
        str:= konvert(QueryUj4.FieldByName('PO_DIGIT2').Value,2);
        str:=stringofchar('0',8-length(str))+str;

        eszk:=QueryUj4.FieldByName('PO_ETIPUS').Value;
        if eszk=0 then
          gypoz:=1
        else
          gypoz:=5;

        if copy(str, gypoz, 1) = '0' then
			  	str	:= 'GYUJT�S BE'
        else
			  	str	:= 'GYUJT�S KI';
        if QueryUj4.FieldByName('PO_VALID').Value=0 then
			  	str	:= 'HIB�S ADAT';
{
				if copy(str, 5, 1) = '0' then begin
					str	:= 'GYUJT�S BE';
				end else begin
					str	:= 'GYUJT�S KI';
				end;        }
				Query_Update( QueryUj2, 'T_POTK',
				[
				'TP_LOCAT', ''''+copy(StringReplace(QueryUj4.FieldByName('PO_GEKOD').AsString,'''','',[rfReplaceAll	]),1,60)+'''',
				'TP_DIGIT', ''''+str+'''',
				'TP_SEBES', IntToStr(StrToIntdef(QueryUj4.FieldByName('PO_SEBES').AsString,0)),
				'TP_LODAT', ''''+QueryUj4.FieldByName('PO_DATUM').AsString+''''
				], ' WHERE TP_RENSZ = '''+QueryUj3.FieldByName('TP_RENSZ').AsString+''' ' );
			end;
		end;
		QueryUj3.Next;
	end;
	Timer1.Enabled	:= true;

end;

procedure TPotStatFmDlg.FormShow(Sender: TObject);
begin
	Query_Run(QueryUj3, 'SELECT TP_POTSZ FROM T_POTK, MEGSEGED WHERE TP_MSKOD = MS_MSKOD AND ( TP_POTSZ <> MS_POTSZ OR TP_RENSZ <> MS_RENSZ)' );
	if QueryUj3.RecordCount > 0 then begin
		Screen.Cursor	:= crHourGlass;
		while not QueryUj3.Eof do begin
			PotStatTolto(QueryUj3.FieldByName('TP_POTSZ').AsString);
			QueryUj3.Next;
		end;
		Query1.Close;
		Query1.Open;
		Screen.Cursor	:= crDefault;
	end;
end;

procedure TPotStatFmDlg.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
//  FGKErvenyesseg.Free;
end;

end.

