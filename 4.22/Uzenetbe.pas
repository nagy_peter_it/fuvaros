unit Uzenetbe;

interface

uses
	SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
	Forms, Dialogs, StdCtrls, Buttons, Mask, DB, DBTables, J_ALFORM, ADODB,
  	ExtCtrls, FelForm;
	
type
	TUzenetbeDlg = class(TJ_AlformDlg)
	BitElkuld: TBitBtn;
	BitKilep: TBitBtn;
	Label1: TLabel;
	Label2: TLabel;
    M1: TMaskEdit;
    M4: TMaskEdit;
    Query1: TADOQuery;
    Label3: TLabel;
    Memo1: TMemo;
    Label4: TLabel;
    M2: TMaskEdit;
    M3: TMaskEdit;
    Label5: TLabel;
    M5: TMaskEdit;
    M6: TMaskEdit;
    Label6: TLabel;
    BitBtn7: TBitBtn;
    M40: TMaskEdit;
	procedure BitKilepClick(Sender: TObject);
	procedure FormCreate(Sender: TObject);
	procedure Tolto(cim : string; teko : string);override;
	procedure BitElkuldClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure BitBtn7Click(Sender: TObject);
	private
	public
	end;

var
	UzenetbeDlg: TUzenetbeDlg;

implementation

uses
	Egyeb, J_SQL, Kozos;
{$R *.DFM}

procedure TUzenetbeDlg.BitKilepClick(Sender: TObject);
begin
	ret_kod := '';
	Close;
end;

procedure TUzenetbeDlg.FormCreate(Sender: TObject);
begin
	ret_kod := '';
  	EgyebDlg.SetADOQueryDatabase(Query1);
	SetMaskEdits([M1, M2, M3, M4, M5, M6]);
end;

procedure TUzenetbeDlg.Tolto(cim : string; teko:string);
begin
	ret_kod 	:= teko;
	Caption 	:= cim;
	M1.Text 	:= teko;
	M2.Text 	:= '';
	M3.Text 	:= '';
	M4.Text 	:= '';
	if ret_kod <> '' then begin		{M�dos�t�s}
     	if Query_Run(Query1, 'SELECT * FROM UZENET WHERE UZ_UZKOD = '+ret_kod) then begin
			if Query1.RecordCount > 0 then begin
				M2.Text  	:= Query1.FieldByName('UZ_KITOL').AsString;
				M3.Text		:= Query1.FieldByName('UZ_KULST').AsString;
				M5.Text		:= Query1.FieldByName('UZ_DATUM').AsString;
				M6.Text		:= Query1.FieldByName('UZ_IDOPT').AsString;
				Memo1.Lines.Add(Query1.FieldByName('UZ_UTART').AsString);
				M40.Text	:= Query1.FieldByName('UZ_CIMEK').AsString;
			end;
			if Query1.FieldByName('UZ_TIPUS').AsString = '0' then begin
				// K�ld�tt lev�l
				M4.Text		:= NevKiiro(M40.Text, ',');
			end else begin
				// Kapott lev�l
				M4.Text		:= NevKiiro(Query1.FieldByName('UZ_KINEK').AsString, ',');
			end;
		end;
		memo1.Enabled	:= false;
	end else begin
		// Az �j levelet �n k�ld�m
		M2.Text		:= EgyebDlg.user_code;
		M3.Text		:= Query_Select('JELSZO', 'JE_FEKOD', M2.Text, 'JE_FENEV');
		M5.Text		:= EgyebDlg.MaiDatum;
		M6.Text		:= FormatDateTime('hh:nn',SysUtils.Time);
		M4.Text		:= NevKiiro(M40.Text, ',');
	end;
end;

procedure TUzenetbeDlg.BitElkuldClick(Sender: TObject);
var
	st 			: string;
	i			: integer;
	cimlista	: TStringList;
	cim			: string;
	ujkod		: integer;
begin
	{�j �zenet felvitele}
	if ret_kod = '' then begin
		if M40.Text = '' then begin
			NoticeKi('Hi�nyz� c�mzett(ek)!');
			Exit;
		end;
		{�j k�d gener�l�sa}
		ujkod		:= GetNextCode('UZENET', 'UZ_UZKOD');
		st			:= Format('%4.4d',[ujkod]);
		Query_Run(Query1, 'INSERT INTO UZENET (UZ_UZKOD) VALUES ('''+st+''') ');
		M1.Text 	:= st;
		ret_kod	  	:= st;
		st	:= '';
		for i := 0 to Memo1.Lines.Count - 1 do begin
			st := st + Memo1.Lines[i] + Chr(13)+Chr(10);
		end;
		Query_Update( Query1, 'UZENET', [
			'UZ_STATU', '0',
			'UZ_STAST', ''''+'�j �zenet'+'''',
			'UZ_KITOL', ''''+M2.Text+'''',
			'UZ_KULST', ''''+copy(Query_Select('JELSZO', 'JE_FEKOD', M2.Text , 'JE_FENEV'),1,40)+'''',
			'UZ_KINEK', ''''+''+'''',
			'UZ_UTART', ''''+copy(st,1,250)+'''',
			'UZ_CIMEK', ''''+copy(M40.Text,1,250)+'''',
			'UZ_TIPUS', '0', 	// Ez egy k�ld�tt �zenet
			'UZ_TIPST', ''''+'elk�ld�tt'+'''',
			'UZ_ORKOD', ret_kod,
			'UZ_DATUM', ''''+M5.Text+'''',
			'UZ_IDOPT', ''''+M6.Text+''''
			], ' WHERE UZ_UZKOD = '+ret_kod );
		// Az �zenet felbont�sa a c�mek szerint
		cimlista	:= TStringLIst.Create;
		StringParse(M40.Text, ',', cimlista);
		for i := 0 to cimlista.Count - 1 do begin
			cim	:= cimlista[i];
			if cim <> '' then begin
				Inc(ujkod);
				Query_Insert( Query1, 'UZENET', [
					'UZ_UZKOD', IntToStr(ujkod),
					'UZ_STATU', '0',
					'UZ_STAST', ''''+'�j �zenet'+'''',
					'UZ_KITOL', ''''+M2.Text+'''',
					'UZ_KULST', ''''+copy(Query_Select('JELSZO', 'JE_FEKOD', M2.Text , 'JE_FENEV'),1,40)+'''',
					'UZ_KINEK', ''''+cim+'''',
					'UZ_UTART', ''''+copy(st,1,250)+'''',
					'UZ_CIMEK', ''''+copy(M40.Text,1,250)+'''',
					'UZ_TIPUS', '1', 	// Ez egy �rkezett �zenet
					'UZ_TIPST', ''''+'�rkezett'+'''',
					'UZ_ORKOD', ret_kod,
					'UZ_DATUM', ''''+M5.Text+'''',
					'UZ_IDOPT', ''''+M6.Text+''''
					]);
			end;
		end;
		cimlista.Free;
	end else begin
		// M�dos�t�sn�l csak az olvasotts�got �ll�tjuk
		Query_Update( Query1, 'UZENET', [
			'UZ_STATU', '1',
			'UZ_STAST', ''''+'olvasott'+''''
			], ' WHERE UZ_UZKOD = '''+ret_kod+''' ' );
	end;
	Close;
end;

procedure TUzenetbeDlg.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
	EgyebDlg.FormReturn(Key);
end;

procedure TUzenetbeDlg.BitBtn7Click(Sender: TObject);
var
	i : integer;
begin
	// T�bb felhaszn�l� kiv�laszt�sa
	Application.CreateForm(TFelFormDlg, FelFormDlg);
	FelFormDlg.valaszt		:= true;
	FelFormDlg.multiselect	:= true;
	FelFormDlg.ShowModal;
	if FelFormDlg.selectlist.Count > 0 then begin
		M4.Text		:= '';
		M40.Text	:= '';
		for i := 0 to FelFormDlg.selectlist.Count - 1 do begin
			M4.Text		:= M4.Text + FelFormDlg.SelectNevlist[i]+',';
			M40.Text	:= M40.Text + FelFormDlg.Selectlist[i]+',';
		end;
	end;
	FelFormDlg.Destroy;
end;

end.
