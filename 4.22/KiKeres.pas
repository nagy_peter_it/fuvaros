unit KiKeres;

interface

uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls, Egyeb,
  Forms, Dialogs, DBTables, DB, StdCtrls, Buttons, Mask, DBCtrls,Shellapi,
  ExtCtrls,DateUtils, Data.Win.ADODB, Vcl.CheckLst, IniFiles;

type
  TKiKeresDlg = class(TForm)
    BitBtn4: TBitBtn;
    BitBtn6: TBitBtn;
    M1: TMaskEdit;
    Label1: TLabel;
    BitBtn2: TBitBtn;
    Label2: TLabel;
    M2: TMaskEdit;
    BitBtn3: TBitBtn;
    Query1: TADOQuery;
    CL1: TCheckListBox;
    Label3: TLabel;
    Query1TA_DATUM: TStringField;
    Query1TA_GKNEV: TStringField;
    Query1TA_IDO: TStringField;
    Query1TA_KM: TIntegerField;
    Query1TA_MENNY: TBCDField;
    Query1TA_PAN: TStringField;
    Query1TA_RENSZ: TStringField;
    Query1TA_TENEV: TStringField;
    QueryKoz: TADOQuery;
    BtnExport: TBitBtn;
    Label4: TLabel;
    ADOCTankolas: TADOConnection;
    Label5: TLabel;
    M3: TMaskEdit;
    Query2: TADOQuery;
    procedure FormCreate(Sender: TObject);
    procedure BitBtn4Click(Sender: TObject);
    procedure BitBtn6Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
	 procedure M1Exit(Sender: TObject);
    procedure M2Exit(Sender: TObject);
    procedure BtnExportClick(Sender: TObject);
    function  Betoltes : boolean;
  private
       katstr  : string;
  public
  end;

var
  KiKeresDlg: TKiKeresDlg;

implementation

uses
	Kozos, J_SQL, SqlList, J_Excel;
{$R *.DFM}

procedure TKiKeresDlg.FormCreate(Sender: TObject);
var
   i   : integer;
   str : string;
begin
	Label4.Caption	:= '';
	Label4.Update;
   katstr          := '';
   QueryKoz.Tag    := QUERY_KOZOS_TAG;
   EgyebDlg.SetADOQueryDatabase(QueryKoz);
   // A d�tumok legyenek az el�z� h�napra be�ll�tva
   M1.Text := copy(Datumhoznap(copy(EgyebDlg.MaiDatum, 1, 8)+'01.', -1, true), 1, 8)+'01.';
   M1Exit(Sender);
   // A listabox felt�lt�se a g�pkocsi kategori�kkal
   CL1.Clear;
   Query_Run(QueryKoz, 'SELECT DISTINCT GK_GKKAT FROM GEPKOCSI ');
   while not QueryKoz.Eof  do begin
       if Trim(QueryKoz.FieldByName('GK_GKKAT').AsString) <> '' then begin
           CL1.Items.Add(QueryKoz.FieldByName('GK_GKKAT').AsString);
       end;
       QueryKoz.Next;
   end;
   // Bejel�lj�k a 6 ill 24 tonn�sakat
   for i := 0 to CL1.Count - 1 do begin
       str := CL1.Items[i];
       if ( (copy(str, 1, 1) = '6') or (copy(str, 1, 2) = '24') )  then begin
           // Be kell jel�lni a tagot
           CL1.Checked[i]  := true;
           CL1.ItemIndex   := i;
       end;
   end;
end;

procedure TKiKeresDlg.BitBtn4Click(Sender: TObject);
var
   fn      : string;
   F       : TextFile;
   sor     : string;
   ossz    : double;
begin
   if not Betoltes then begin
       Exit;
   end;
   // A lista elk�sz�t�se
	fn	:= EgyebDlg.Temppath + 'KERUZ.TXT';
	if FileExists(fn) then begin
		DeleteFile(PChar(fn));
	end;
   {$I-}
	AssignFile(F, fn);
	Rewrite(F);
	{$I+}
	if IOResult <> 0 then begin
       NoticeKi('Nem siker�lt az �llom�ny megnyit�sa!');
		Exit;
   end;
   Query1.First;
   ossz    := 0;
	while not Query1.Eof do begin
       sor     :=  copy( Query1.FieldByName('TA_RENSZ').AsString + URESSTRING, 1, 10 )+' '+
                   copy( Query1.FieldByName('TA_DATUM').AsString + URESSTRING, 1, 11 )+' '+
                   copy( Query1.FieldByName('TA_IDO'  ).AsString + URESSTRING, 1,  8 )+' '+
                   copy( Query1.FieldByName('TA_GKNEV').AsString + URESSTRING, 1, 25 )+' '+
                   copy( Query1.FieldByName('TA_KM'   ).AsString + URESSTRING, 1, 10 )+' '+
                   Format('%10.2f', [ StringSZam(Query1.FieldByName('TA_MENNY').AsString)])+' '+
                   copy( Query1.FieldByName('TA_PAN'  ).AsString + URESSTRING, 1, 19 );
       ossz    := ossz + StringSZam(Query1.FieldByName('TA_MENNY').AsString);
       Writeln(F, sor);
		Query1.Next;
	end;
   sor     := '                                                                     ----------';
   Writeln(F, sor);
   sor     :=  '�sszesen'+copy( URESSTRING, 1, 61 ) + Format('%10.2f', [ ossz]);
   Writeln(F, sor);
   // A rendsz�monk�nti �sszesen ki�r�sa
   Writeln(F, '');
   Writeln(F, '');
   sor     := '�sszes�tett lista rendsz�monk�nt';
   Writeln(F, sor);
   Writeln(F, '');
   sor     := 'Rendsz�m    Mennyis�g   ';
   Writeln(F, sor);
   sor     := '----------  ----------';
   Writeln(F, sor);
   Query2.First;
   ossz    := 0;
	while not Query2.Eof do begin
       sor     :=  copy( Query2.FieldByName('TA_RENSZ').AsString + URESSTRING, 1, 10 )+'  '+
                   Format('%10.2f', [ StringSZam(Query2.FieldByName('LITER').AsString)]) ;
       ossz    := ossz + StringSZam(Query2.FieldByName('LITER').AsString);
       Writeln(F, sor);
		Query2.Next;
	end;
   sor     := '            ----------';
   Writeln(F, sor);
   sor     :=  '�sszesen    '+ Format('%10.2f', [ ossz]);
   Writeln(F, sor);

   CloseFile(F);
   EgyebDlg.vanvonal   := false;
	EgyebDlg.FileMutato( fn, 'Kimutat�s a kereskedelmi �zemanyagokr�l', 'G�pkocsi kateg�ri�k = '+katstr+'    Id�szak = '+M1.Text+'-'+M2.Text,
           'Kihagyott rendsz�mok : '+M3.Text,
           'Rendsz�m   D�tum       Id�pont  Sof�r neve                KM �ll�s   Mennyis�g  PAN azonos�t�      ',
           '---------- ----------- -------- ------------------------- ---------- ---------- -------------------', 0 );
   DeleteFile(PChar(fn));

(*
	SQLListDlg := TSQLListDlg.Create(nil);
  	SQLListDlg.Tolto(Query1, EgyebDlg.Read_SZGrid('999','100'),'Kimutat�s a kereskedelmi �zemanyagokr�l',
       'G�pkocsi kateg�ri�k = '+katstr+'    Id�szak = '+M1.Text+'-'+M2.Text);
	SQLListDlg.Rep.Preview;
  	SQLListDlg.Destroy;
*)
end;

procedure TKiKeresDlg.BitBtn6Click(Sender: TObject);
begin
	Close;
end;

procedure TKiKeresDlg.BtnExportClick(Sender: TObject);
var
	fn		   	: string;
	EX	  		: TJExcel;
	sorszam		: integer;
   i           : integer;
   kezdoszam   : integer;
begin
   if not Betoltes then begin
       Exit;
   end;
	EX := TJexcel.Create(Self);
//   NoticeKi('Dec. sep : '+EX.dec_sep);
   // Ment�s XLS �llom�nyba
	fn	:= EgyebDlg.tempList+ 'Kereskedelmi_'+copy(M1.Text, 6, 2)+copy(M1.Text, 9, 2)+
       '_'+copy(M2.Text, 6, 2)+copy(M2.Text, 9, 2)+'.'+ EX.GetExtension;
   if FileExists(fn) then begin
		DeleteFile(PChar(fn));
   end;
	if not EX.OpenXlsFile(fn) then begin
		NoticeKi('Nem siker�lt megnyitnom az Excel �llom�nyt!');
		Exit;
	end;
   Screen.Cursor   := crHourGlass;
	EX.ColNumber	:= Query1.FieldCount;
	EX.InitRow;
   EX.SetRowcell( 1,  'Kimutat�s a kereskedelmi �zemanyagokr�l');
   EX.SaveRow(1);
   EX.SetRowcell( 1,  'G�pkocsi kateg�ri�k : '+katstr);
   EX.SaveRow(3);
   EX.SetRowcell( 1,  'Id�szak : '+M1.Text+'-'+M2.Text);
   EX.SaveRow(4);
   EX.SetRowcell( 1,  'Kihagyottak : '+M3.Text);
   EX.SaveRow(5);

   for i := 0 to Query1.FieldCount - 1 do begin
		EX.SetRowcell( i+1,  Query1.Fields[i].DisplayLabel);
   end;
   EX.SaveRow(7);
	sorszam	:= 8;
	while not Query1.Eof do begin
		Label4.Caption	:= Query1.FieldByName('TA_DATUM').AsString+' '+Query1.FieldByName('TA_IDO').AsString;
		Label4.Update;
       for i := 0 to Query1.FieldCount - 1 do begin
           if Query1.Fields[i].FieldName = 'TA_MENNY' then begin
               EX.SetRowcell( i+1,  StringSzam(Query1.Fields[i].AsString));
           end else begin
               EX.SetRowcell( i+1,  Query1.Fields[i].AsString);
           end;
       end;
		EX.SaveRow(sorszam);
       EX.EmptyRow;
		Inc(sorszam);
		Query1.Next;
	end;
   EX.SetRowcell( 7,  '=SUM(G8:G'+IntToStr(sorszam-1)+')');
   EX.SaveRow(sorszam);
   EX.EmptyRow;
   Inc(sorszam);
   Inc(sorszam);
   EX.SetRowcell( 1,  '�sszes�tett lista rendsz�monk�nt');
   EX.SaveRow(sorszam);
   Inc(sorszam);
   Inc(sorszam);
   EX.SetRowcell( 1,  'Rendsz�m');
   EX.SetRowcell( 2,  'Mennyis�g');
   EX.SaveRow(sorszam);
   Inc(sorszam);
   kezdoszam := sorszam;
	while not Query2.Eof do begin
		Label4.Caption	:= Query2.FieldByName('TA_RENSZ').AsString;
		Label4.Update;
       EX.SetRowcell( 1,  Query2.FieldByName('TA_RENSZ').AsString);
       EX.SetRowcell( 2,  StringSzam( Query2.FieldByName('LITER').AsString ));
		EX.SaveRow(sorszam);
		Inc(sorszam);
		Query2.Next;
	end;
   // Az �sszesenek ki�r�sa
   EX.SetRowcell( 1,  'Mind�sszesen');
   EX.SetRowcell( 2,  '=SUM(B'+IntToStr(kezdoszam)+':B'+IntToStr(sorszam-1)+')');
   EX.SaveRow(sorszam);
	EX.SaveFileAsXls(fn);
	EX.Free;
	Label4.Caption	:= '';
	Label4.Update;
   Screen.Cursor   := crDefault;
   // Megnyitjuk az XLS-t
   ShellExecute(Application.Handle,'open',PChar(fn), '', '', SW_SHOWNORMAL );
end;

procedure TKiKeresDlg.BitBtn2Click(Sender: TObject);
begin
	// EgyebDlg.Calendarbe(M1);
  // EgyebDlg.ElsoNapEllenor(M1, M2);
  EgyebDlg.CalendarBe_idoszak(M1, M2);
end;

procedure TKiKeresDlg.BitBtn3Click(Sender: TObject);
begin
	EgyebDlg.Calendarbe(M2);
  EgyebDlg.UtolsoNap(M2);
  // M2.Text:=DateToStr(EndOfTheMonth(StrToDate(M2.Text)));
end;

procedure TKiKeresDlg.M1Exit(Sender: TObject);
begin
	if ( ( M1.Text <> '' ) and ( not Jodatum2(M1 ) ) ) then begin
		NoticeKi('Hib�s d�tum megad�sa!');
		M1.Text := '';
		M1.Setfocus;
	end;
   EgyebDlg.ElsoNapEllenor(M1, M2);
end;

procedure TKiKeresDlg.M2Exit(Sender: TObject);
begin
	if ( ( M2.Text <> '' ) and ( not Jodatum2(M2 ) ) ) then begin
		NoticeKi('Hib�s d�tum megad�sa!');
		M2.Text := '';
		M2.Setfocus;
	end;
end;

function TKiKeresDlg.Betoltes : boolean;
var
   sqlstr      : string;
   i           : integer;
	Inifn  	    : TIniFile;
   tank_conn   : string;
   tank_user   : string;
   tank_pass   : string;
   tank_db     : string;
   rendszamok  : string;
   kihagyok    : string;
   kihagylist  : TStringLIst;
   kihagysep   : string;
begin
   Result  := false;
	if ( ( M1.Text <> '' ) and ( not Jodatum2(M1 ) ) ) then begin
		NoticeKi('Hib�s d�tum megad�sa!');
		M1.Text := '';
		M1.Setfocus;
       Exit;
	end;
	if ( M2.Text = '' ) then begin
       M2.Text := EgyebDlg.MaiDatum;
   end;
	if ( ( M2.Text <> '' ) and ( not Jodatum2(M2) ) ) then begin
		NoticeKi('Hib�s d�tum megad�sa!');
		M2.Text := '';
		M2.Setfocus;
       Exit;
	end;
   // A g�pkocsi kateg�ri�k �sszerak�sa
   katstr  := '';
   for i := 0 to CL1.Count - 1 do begin
       if CL1.Checked[i] then begin
           katstr  := katstr + '''' + CL1.Items[i] + ''',';
       end;
   end;
   if Length(katstr) > 1 then begin
       katstr  := copy(katstr, 1, Length(katstr)-1);
   end;

	Inifn 		:= TIniFile.Create( EgyebDlg.globalini );
   tank_conn   := Inifn.ReadString( 'GENERAL', 'TANK_CONNECTION','');
   tank_db     := Inifn.ReadString( 'GENERAL', 'TANK_DB','TANKOLAS');
   tank_user   := DecodeString(Inifn.ReadString( 'GENERAL', 'TANK_USER',''));
   tank_pass   := DecodeString(Inifn.ReadString( 'GENERAL', 'TANK_PASSWORD',''));
	Inifn.Free;
   if tank_conn = '' then begin
       NoticeKi('Az ini-ben ellen�rizze a TANK_CONNECTION; TANK_USER; TANK_PASSWORD be�ll�t�sokat !');
       Exit;
   end;
   try
		ADOCTankolas.Close;
		if tank_user <> '' then begin
			tank_conn 	:= tank_conn + ';User ID= ' + tank_user;
		end;
		if tank_pass <> '' then begin
			tank_conn 	:= tank_conn + ';Password=' + tank_pass;
		end;
		ADOCTankolas.ConnectionString		:= tank_conn;
		ADOCTankolas.DefaultDatabase		:= tank_db;
		ADOCTankolas.LoginPrompt			:= false;
		ADOCTankolas.Open;
	except
		NoticeKi('Nem siker�lt az adatb�zis kapcsol�d�s!');
		Exit;
	end;
   if not ADOCTankolas.Connected then begin
		NoticeKi('Nem siker�lt az adatb�zis kapcsol�d�s!');
		Exit;
   end;
   Query1.Connection  := ADOCTankolas;
   Query2.Connection  := ADOCTankolas;
   rendszamok := '';
   // Query_Run(QueryKoz, 'SELECT GK_RESZ FROM GEPKOCSI WHERE GK_GKKAT IN ('+katstr+')');
   Query_Run(QueryKoz, 'SELECT GK_RESZ FROM GEPKOCSI WHERE GK_GKKAT IN ('+katstr+') and GK_ARHIV = 0');
   while not QueryKoz.Eof  do begin
       if QueryKoz.FieldByName('GK_RESZ').AsString <> '' then begin
           rendszamok := rendszamok + '''' + QueryKoz.FieldByName('GK_RESZ').AsString + ''',';
       end;
       QueryKoz.Next;
   end;
   rendszamok := rendszamok + '''ABCDEFG1234567''';
   // A kihagy�sok �sszegy�jt�se
   kihagyok    := '';
   if M3.Text <> '' then begin
       kihagysep   := ',';
       if Pos (kihagysep, M3.Text) = 0 then begin
           kihagysep   := ';';
       end;
       kihagylist  := TStringLIst.Create;
       StringParse(M3.Text, kihagysep, kihagylist);
       for i := 0 to kihagylist.Count -1 do begin
           kihagyok := kihagyok + '''' + kihagylist[i]+ ''',';
       end;
       kihagyok := ' AND TA_RENSZ NOT IN ('+kihagyok + '''ABCD1234'' )';
   end;
   sqlstr  := 'SELECT * FROM TANKOLAS WHERE TA_RENSZ IN ( '+rendszamok+' ) '+
       ' AND TA_TENEV = ''Diesel'' '+
       kihagyok +
       ' AND TA_DATUM >= '''+M1.Text+''' AND TA_DATUM <= '''+M2.Text+''' ORDER BY TA_RENSZ, TA_DATUM, TA_IDO';
   if not Query_Run(Query1, sqlstr) then begin
       NoticeKi('A lek�rdez�s v�grehajt�sa nem siker�lt!');
       Exit;
   end;
   sqlstr  := 'SELECT TA_RENSZ, SUM(TA_MENNY) LITER FROM TANKOLAS WHERE TA_RENSZ IN ( '+rendszamok+' ) '+
       ' AND TA_TENEV = ''Diesel'' '+
       kihagyok +
       ' AND TA_DATUM >= '''+M1.Text+''' AND TA_DATUM <= '''+M2.Text+''' '+
       ' GROUP BY TA_RENSZ ORDER BY TA_RENSZ ';
   if not Query_Run(Query2, sqlstr) then begin
       NoticeKi('A lek�rdez�s v�grehajt�sa nem siker�lt!');
       Exit;
   end;

   if Query1.RecordCount = 0 then begin
       NoticeKi('Nincs megfelel� elem!');
       Exit;
   end;
   Result  := true;
end;

end.


