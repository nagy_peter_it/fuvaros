unit FelrakoMentes;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Buttons, Vcl.Mask,
  Vcl.ExtCtrls;

type
  TFelrakoMentesDlg = class(TForm)
    Panel1: TPanel;
    M3: TMaskEdit;
    M4: TMaskEdit;
    M5: TMaskEdit;
    M11: TMaskEdit;
    M13: TMaskEdit;
    M12: TMaskEdit;
    M14: TMaskEdit;
    M15: TMaskEdit;
    M1: TMaskEdit;
    M2: TMaskEdit;
    Label2: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    Label22: TLabel;
    Label21: TLabel;
    BitElkuld: TBitBtn;
    BitMentMasNeven: TBitBtn;
    meUjNev: TMaskEdit;
    Panel2: TPanel;
    Label1: TLabel;
    Label3: TLabel;
    procedure BitElkuldClick(Sender: TObject);
    procedure BitMentMasNevenClick(Sender: TObject);
  private
    { Private declarations }
  public
    procedure Tolto(RakoNeve, Orszag, ZIP, Varos, UtcaHazszam, Tovabbi1, Tovabbi2, Tovabbi3, Tovabbi4, Tovabbi5: string);
  end;

var
  FelrakoMentesDlg: TFelrakoMentesDlg;

implementation

{$R *.dfm}

uses Kozos, J_SQL;

procedure TFelrakoMentesDlg.BitElkuldClick(Sender: TObject);
begin
   FelrakoElment (M1.Text, M2.Text, M3.Text, M4.Text, M5.Text, M11.Text, M12.Text, M13.Text, M14.Text, M15.Text, '', False);
  // Close;  // ModalResult elint�zi
end;

procedure TFelrakoMentesDlg.BitMentMasNevenClick(Sender: TObject);
begin
  if trim(meUjNev.Text) = '' then begin
    NoticeKi('Az �j felrak� nev�nek kit�lt�se k�telez�!');
		meUjNev.SetFocus;
  	Exit;
	  end;
  if Query_Select('FELRAKO', 'FF_FELNEV', trim(meUjNev.Text), 'FF_FEKOD') <> '' then begin
    NoticeKi('M�r l�tezik felrak� ilyen n�vvel!');
		meUjNev.SetFocus;
  	Exit;
	  end;
  FelrakoElment (trim(meUjNev.Text), M2.Text, M3.Text, M4.Text, M5.Text, M11.Text, M12.Text, M13.Text, M14.Text, M15.Text, '', False);
  // Close;  // ModalResult elint�zi
end;

procedure TFelrakoMentesDlg.Tolto(RakoNeve, Orszag, ZIP, Varos, UtcaHazszam, Tovabbi1, Tovabbi2, Tovabbi3, Tovabbi4, Tovabbi5: string);
begin
    M1.Text:= RakoNeve;
    M2.Text:= Orszag;
    M3.Text:= ZIP;
    M4.Text:= Varos;
    M5.Text:= UtcaHazszam;
    M11.Text:= Tovabbi1;
    M12.Text:= Tovabbi2;
    M13.Text:= Tovabbi3;
    M14.Text:= Tovabbi4;
    M15.Text:= Tovabbi5;
    meUjNev.Text:= GetUjFelrakoNev(RakoNeve);
end;

end.
