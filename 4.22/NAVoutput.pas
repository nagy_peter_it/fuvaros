unit NAVoutput;

interface

uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls, Egyeb,
  Forms, Dialogs, Lgauge, DBTables, DB, StdCtrls, Buttons, QuickRpt, Printers,
  Mask, Data.Win.ADODB;

type
  TNAVOutputDlg = class(TForm)
    BitBtn4: TBitBtn;
    BitBtn6: TBitBtn;
    M1: TMaskEdit;
    M2: TMaskEdit;
    Label2: TLabel;
    RB1: TRadioButton;
    RB2: TRadioButton;
    M3: TMaskEdit;
    Label1: TLabel;
    M4: TMaskEdit;
    Q1: TADOQuery;
    BitBtn10: TBitBtn;
    BitBtn1: TBitBtn;
    procedure FormCreate(Sender: TObject);
    procedure BitBtn4Click(Sender: TObject);
    procedure BitBtn6Click(Sender: TObject);
    procedure M1Click(Sender: TObject);
    procedure M1Exit(Sender: TObject);
    procedure RB1Click(Sender: TObject);
    procedure BitBtn10Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
  private
		fileazon 	: TextFile;
		report		: TQuickRep;
  public
  end;

var
  NAVOutputDlg: TNAVOutputDlg;

implementation

uses
  J_SQL, J_NAV, IniFiles, Kozos;
{$R *.DFM}

procedure TNAVOutputDlg.FormCreate(Sender: TObject);
begin
	EgyebDlg.SetADOQueryDatabase(Q1);
   RB1Click(Sender);
end;

procedure TNAVOutputDlg.BitBtn10Click(Sender: TObject);
begin
	// EgyebDlg.Calendarbe(M1);
  EgyebDlg.CalendarBe_idoszak(M1, M2);
end;

procedure TNAVOutputDlg.BitBtn1Click(Sender: TObject);
begin
	EgyebDlg.Calendarbe(M2, True);
end;

procedure TNAVOutputDlg.BitBtn4Click(Sender: TObject);
var
  sorszam 	: integer;
  rekszam 	: integer;
  kilep     : boolean;
  navout    : TJNAV;
  fn        : string;
  fn2       : string;
   Ini			: TIniFile;
  fnini     : string;
  ADOSZAM      : string;
  KOZADOSZAM   : string;
  KISADOZO   : string;
  NEV   : string;
  CIM_IRANYITOSZAM   : string;
  CIM_TELEPULES   : string;
  CIM_KERULET   : string;
  CIM_KOZTERULET_NEVE   : string;
  CIM_KOZTERULET_JELLEGE   : string;
  CIM_HAZSZAM   : string;
  CIM_EPULET   : string;
  CIM_LEPCSOHAZ   : string;
  CIM_SZINT   : string;
  CIM_AJTO   : string;
  veado       : string;
  netto        : double;
  afa          : double;
begin
  navout  := TJNAV.Create(Self);
  if RB1.Checked then begin
    // A d�tumok ellen�rz�se
    if M1.Text = '' then begin
      M1.Text     := FormatDateTime('YYYY.01.01.', now);
    end;
    if M2.Text = '' then begin
      M2.Text     := FormatDateTime('YYYY.MM.DD.', now);
    end;
    if (not Jodatum2(M1))then begin
        NoticeKi('Rossz d�tum megad�sa !');
        M1.SetFocus;
        Exit;
    end;
    if (not Jodatum2(M2)) then begin
        NoticeKi('Rossz d�tum megad�sa !');
        M2.SetFocus;
        Exit;
    end;
    if M2.Text > FormatDateTime('YYYY.MM.DD.', now) then begin
        NoticeKi('Rossz z�r� d�tum megad�sa !');
        M2.Text := FormatDateTime('YYYY.MM.DD.', now);
        M2.SetFocus;
        Exit;
    end;
  end else begin
    // A sz�mlasz�mok ellen�rz�se
    Query_Run(Q1, 'SELECT SA_KOD FROM SZFEJ WHERE SA_KOD = '''+M3.Text+''' ', true);
    if Q1.RecordCount = 0 then begin
        NoticeKi('Nem l�tez� sz�mlasz�m !');
        M3.SetFocus;
    end;
    Query_Run(Q1, 'SELECT SA_KOD FROM SZFEJ WHERE SA_KOD = '''+M4.Text+''' ', true);
    if Q1.RecordCount = 0 then begin
        NoticeKi('Nem l�tez� sz�mlasz�m !');
        M4.SetFocus;
    end;
  end;

  // Az adatok lek�rdez�se
   Screen.Cursor   := crHourGlass;
  if RB1.Checked then begin
    // A d�tumok alapj�n
    Query_Run(Q1, 'SELECT * FROM SZFEJ, SZSOR WHERE SA_KOD = SS_KOD AND SA_KIDAT >= '''+M1.Text+''' AND SA_KIDAT <= '''+M2.Text+'''  ORDER BY SA_KOD, SS_SOR', true);
  end else begin
    // A sorsz�mok alapj�n
    Query_Run(Q1, 'SELECT * FROM SZFEJ, SZSOR WHERE SA_KOD = SS_KOD AND SA_KOD >= '''+M3.Text+''' AND SA_KOD <= '''+M4.Text+'''  ORDER BY SA_KOD, SS_SOR', true);
  end;
   Screen.Cursor   := crDefault;
  if Q1.RecordCount = 0 then begin
    	NoticeKi('Nincs megfelel� elem !');
      Exit;
  end;
  Application.CreateForm(TFormGaugeDlg, FormGaugeDlg);
  FormGaugeDlg.GaugeNyit('NAV output list�ja','Egy kis t�relmet k�rek, dolgozom ...');
  sorszam 	:= 1;
  rekszam 	:= 1;

  ADOSZAM      := '';
  KOZADOSZAM   := '';
  KISADOZO      := '';
  NEV           := '';
  CIM_IRANYITOSZAM   := '';
  CIM_TELEPULES   := '';
  CIM_KERULET   := '';
  CIM_KOZTERULET_NEVE   := '';
  CIM_KOZTERULET_JELLEGE   := '';
  CIM_HAZSZAM   := '';
  CIM_EPULET   := '';
  CIM_LEPCSOHAZ   := '';
  CIM_SZINT   := '';
  CIM_AJTO   := '';
  // AZ ini beolvas�sa
  fnini     := ExtractFilePath(ExtractFilePath(ParamStr(0)))+'LOCAL.INI';
  if FileExists(fnini) then begin
       Ini := TIniFile.Create( fnini );
       ADOSZAM	:= Ini.ReadString( 'GENERAL', 'ADOSZAM', '' );
       KOZADOSZAM	:= Ini.ReadString( 'GENERAL', 'KOZADOSZAM', '' );
       KISADOZO	:= Ini.ReadString( 'GENERAL', 'KISADOZO', '' );
       NEV	:= Ini.ReadString( 'GENERAL', 'NEV', '' );
       CIM_IRANYITOSZAM	:= Ini.ReadString( 'GENERAL', 'CIM_IRANYITOSZAM', '' );
       CIM_TELEPULES:= Ini.ReadString( 'GENERAL', 'CIM_TELEPULES', '' );
       CIM_KERULET:= Ini.ReadString( 'GENERAL', 'CIM_KERULET', '' );
       CIM_KOZTERULET_NEVE	:= Ini.ReadString( 'GENERAL', 'CIM_KOZTERULET_NEVE', '' );
       CIM_KOZTERULET_JELLEGE	:= Ini.ReadString( 'GENERAL', 'CIM_KOZTERULET_JELLEGE', '' );
       CIM_HAZSZAM	:= Ini.ReadString( 'GENERAL', 'CIM_HAZSZAM', '' );
       CIM_EPULET:= Ini.ReadString( 'GENERAL', 'CIM_EPULET', '' );
       CIM_LEPCSOHAZ	:= Ini.ReadString( 'GENERAL', 'CIM_LEPCSOHAZ', '' );
       CIM_SZINT	:= Ini.ReadString( 'GENERAL', 'CIM_SZINT', '' );
       CIM_AJTO:= Ini.ReadString( 'GENERAL', 'CIM_AJTO', '' );
       Ini.Free;
  end;
  kilep     := false;
  while ( ( not Q1.EOF ) and (not FormGaugeDlg.Kilepes ) ) do begin
      FormGaugeDlg.pozicio( Round(rekszam / Q1.RecordCount * 100));
      Inc(rekszam);
      netto    := StringSzam(Q1.FieldByName('SS_EGYAR').AsString);
      afa      := netto * StringSzam(Q1.FieldByName('SS_AFA').AsString) / 100;
      navout.FillGrid([
        Q1.FieldByName('SA_KOD').AsString,
        '1',
        Q1.FieldByName('SA_KIDAT').AsString,
        Q1.FieldByName('SA_TEDAT').AsString,
  ADOSZAM,                                          //'szamlakibocsato_adoszam';
  KOZADOSZAM,                                       //'szamlakibocsato_kozadoszam';
  KISADOZO,                                         //'szamlakibocsato_kisadozo';
  NEV,                                              //'szamlakibocsato_nev';
  CIM_IRANYITOSZAM,                                 //'szamlakibocsato_cim_iranyitoszam';
  CIM_TELEPULES,                                    //'szamlakibocsato_cim_telepules';
  CIM_KERULET,                                      //'szamlakibocsato_cim_kerulet';
  CIM_KOZTERULET_NEVE,                              //'szamlakibocsato_cim_kozterulet_neve';
  CIM_KOZTERULET_JELLEGE,                           //'szamlakibocsato_cim_kozterulet_jellege';
  CIM_HAZSZAM,                                      //'szamlakibocsato_cim_hazszam';
  CIM_EPULET,                                       //'szamlakibocsato_cim_epulet';
  CIM_LEPCSOHAZ,                                    //'szamlakibocsato_cim_lepcsohaz';
  CIM_SZINT,                                        //'szamlakibocsato_cim_szint';
  CIM_AJTO,                                         //'szamlakibocsato_cim_ajto';
  Q1.FieldByName('SA_VEADO').AsString,              //'vevo_adoszam';
  Q1.FieldByName('SA_EUADO').AsString,              //'vevo_kozadoszam';
  Q1.FieldByName('SA_VEVONEV').AsString,            //'vevo_nev';
  Q1.FieldByName('SA_VEIRSZ').AsString,             //'vevo_cim_iranyitoszam';
  Q1.FieldByName('SA_VEVAROS').AsString,            //'vevo_cim_telepules';
  Q1.FieldByName('SA_KERULET').AsString,            //'vevo_cim_kerulet';
  Q1.FieldByName('SA_KOZTER').AsString,             //'vevo_cim_kozterulet_neve';
  Q1.FieldByName('SA_JELLEG').AsString,             //'vevo_cim_kozterulet_jellege';
  Q1.FieldByName('SA_HAZSZAM').AsString,            //'vevo_cim_hazszam';
  Q1.FieldByName('SA_EPULET').AsString,             //'vevo_cim_epulet';
  Q1.FieldByName('SA_LEPCSO').AsString,             //'vevo_cim_lepcsohaz';
  Q1.FieldByName('SA_SZINT').AsString,              //'vevo_cim_szint';
  Q1.FieldByName('SA_AJTO').AsString,               //'vevo_cim_ajto';
  Q1.FieldByName('SS_TERNEV').AsString,             //'termek_termeknev';
  Q1.FieldByName('SS_ITJSZJ').AsString,             //'termek_besorszam';
  Format('%.2f', [StringSzam(Q1.FieldByName('SS_DARAB').AsString)]),              //'termek_menny';
  Q1.FieldByName('SS_MEGY').AsString,               //'termek_mertekegys';
  Format('%.2f', [StringSzam(Q1.FieldByName('SS_DARAB').AsString)*StringSZam(Q1.FieldByName('SS_EGYAR').AsString)]),    //'termek_nettoar';
  Format('%.2f', [StringSzam(Q1.FieldByName('SS_EGYAR').AsString)]),              //'termek_nettoegysar';
  Format('%.2f', [StringSzam(Q1.FieldByName('SS_AFA').AsString)]),              //'termek_adokulcs';
  Format('%.2f', [afa]),                                                        //'termek_adoertek';
  '',                                                                           //'termek_szazalekertek';
  Format('%.2f', [netto + afa]),                                                //'termek_bruttoar';
  Q1.FieldByName('SA_ESDAT').AsString,                                          //'nem_kotelezo_tipus_fiz_hatarido';
  '', //'nem_kotelezo_tipus_fiz_mod';
  Q1.FieldByName('SS_VALNEM').AsString                                          //'nem_kotelezo_tipus_penznem';

        ]);
  	  Q1.Next;
  end;
  kilep := FormGaugeDlg.Kilepes;
	FormGaugeDlg.Destroy;
  if kilep then begin
    // Feljhaszn�l�i megszak�t�s
    NoticeKi('Felhaszn�l�i megszak�t�s !');
    Exit;
  end;
  // Elk�sz�tj�k az output �llom�nyt!!!
   Screen.Cursor   := crHourGlass;
  fn    := 'NAVOUTPUT_'+FormatDateTime('YYMMDD', now);
  fn2   := navout.SaveGrid(fn);
   Screen.Cursor   := crDefault;
  if fn2 = '' then begin
    NoticeKi('�llom�ny gener�l�si hiba !');
  end else begin
    NoticeKi('Az output �llom�ny elk�sz�lt ('+fn2+')');
  end;
  //navout.Free;
end;

procedure TNAVOutputDlg.BitBtn6Click(Sender: TObject);
begin
	Close;
end;

procedure TNAVOutputDlg.M1Click(Sender: TObject);
begin
	with Sender as TMaskEdit do begin
  	SelectAll;
  end;
end;

procedure TNAVOutputDlg.M1Exit(Sender: TObject);
begin
	with Sender as TMAskEdit do begin
  	if Text <> '' then begin
     	if not Jodatum2(TMaskEdit(Sender)) then begin
        	NoticeKi('Rossz d�tum megad�sa !');
           TMaskEdit(Sender).SetFocus;
        end;
     end;
  end;
end;

procedure TNAVOutputDlg.RB1Click(Sender: TObject);
var
  evszam  : string;
begin
  // A mez�k enged�lyez�se
  M1.Enabled  := false;
  M2.Enabled  := false;
  M3.Enabled  := false;
  M4.Enabled  := false;
  M1.Color    := CLAqua;
  M2.Color    := CLAqua;
  M3.Color    := CLAqua;
  M4.Color    := CLAqua;
  if RB1.Checked then begin
    M1.Enabled  := true;
    M2.Enabled  := true;
    M1.Color    := CLWindow;
    M2.Color    := CLWindow;
    M1.Text     := FormatDateTime('YYYY.01.01.', now);
    M2.Text     := FormatDateTime('YYYY.MM.DD.', now);
    M3.Text     := '';
    M4.Text     := '';
  end else begin
    M3.Enabled  := true;
    M4.Enabled  := true;
    M3.Color    := CLWindow;
    M4.Color    := CLWindow;
    evszam      := FormatDateTime('YYYY', now);
    Query_Run(Q1, 'SELECT SA_KOD FROM SZFEJ WHERE SA_KOD LIKE '''+copy(evszam, 3, 2)+'%'' ORDER BY SA_KOD', true);
    M1.Text     := '';
    M2.Text     := '';
    Q1.First;
    M3.Text     := Q1.FieldByName('SA_KOD').AsString;
    Q1.Last;
    M4.Text     := Q1.FieldByName('SA_KOD').AsString;
  end;
end;

end.
