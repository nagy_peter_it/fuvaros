unit Bazuj;

interface

uses
	SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
	StdCtrls, Forms, DBCtrls, DB, DBGrids, DBTables, Grids, ExtCtrls, Buttons,
   ShellApi, Dialogs, Variants, ADODB, Mask;

type
	TBazisUjDlg = class(TForm)
    Panel3: TPanel;
    DBGrid2: TDBGrid;
    DataSource1: TDataSource;
    Panel2: TPanel;
    Panel4: TPanel;
    DBGrid1: TDBGrid;
    Panel5: TPanel;
    DataSource2: TDataSource;
    Query1: TADOQuery;
    Query2: TADOQuery;
    Query3: TADOQuery;
    Query1SZ_FOKOD: TStringField;
    Query1SZ_ALKOD: TStringField;
    Query1SZ_MENEV: TStringField;
    Query1SZ_LEIRO: TStringField;
    Query1SZ_KODHO: TBCDField;
    Query1SZ_NEVHO: TBCDField;
    Query1SZ_ERVNY: TIntegerField;
    Query3SZ_FOKOD: TStringField;
    Query3SZ_ALKOD: TStringField;
    Query3SZ_MENEV: TStringField;
    Query3SZ_LEIRO: TStringField;
    Query3SZ_KODHO: TBCDField;
    Query3SZ_NEVHO: TBCDField;
    Query3SZ_ERVNY: TIntegerField;
    Panel1: TPanel;
    ButtonUj: TBitBtn;
    ButtonMod: TBitBtn;
    ButtonTor: TBitBtn;
    ButtonPrint: TBitBtn;
    ButtonKilep: TBitBtn;
    DBNavigator1: TDBNavigator;
    BitBtn1: TBitBtn;
    Splitter1: TSplitter;
    Query3SZ_EGYEB1: TIntegerField;
    ebInkrementalis: TEdit;
    Label1: TLabel;
	procedure FormCreate(Sender: TObject);
	procedure ButtonKilepClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  procedure FormKeyUp(Sender: TObject; var Key: Word;
     Shift: TShiftState);
  procedure ButtonTorClick(Sender: TObject);
    procedure ButtonPrintClick(Sender: TObject);
    procedure ButtonUjClick(Sender: TObject);
    procedure ButtonModClick(Sender: TObject);
    procedure Adatlap(cim, kod : string);
    procedure DataSource1DataChange(Sender: TObject; Field: TField);
    procedure Elemki;
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormShow(Sender: TObject);
    procedure DBGrid1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure BitBtn1Click(Sender: TObject);
    procedure ebInkrementalisClick(Sender: TObject);
    procedure ebInkrementalisChange(Sender: TObject);
    procedure Query1FilterRecord(DataSet: TDataSet; var Accept: Boolean);
    procedure DBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
	private
  	mindenki		: boolean;
     mezonevek	: TStringList;
     dispnevek	: TStringList;
     voltalt		: boolean;
     vancreate	: boolean;
     ADATNEV		: String;
	public
		vanrek 			: boolean;
		ret_vkod		: string;
		valaszto		: boolean;
		voltmodositas	: boolean;
	end;

var
	BazisUjDlg: TBazisUjDlg;

implementation

uses
	Egyeb, LiBazis, Bazisbe, BazFobe, Kozos, J_SQL, BazOsszevon ;

{$R *.DFM}

procedure TBazisUjDlg.FormCreate(Sender: TObject);
begin
	EgyebDlg.SeTADOQueryDatabase(Query1);
	EgyebDlg.SeTADOQueryDatabase(Query2);
	EgyebDlg.SeTADOQueryDatabase(Query3);
	adatnev		:= 'SZOTAR';
	vancreate 	:= true;
 	FormatSettings.ShortDateFormat := 'yyyy.mm.dd';
	mezonevek 	:= TStringList.Create;
	dispnevek 	:= TStringList.Create;
	mindenki 	:= false;
	vanrek 		:= false;
	voltmodositas	:= false;

  if not Query_Run(Query1, 'Select * from '+ADATNEV+' where SZ_FOKOD=''000'' Order by SZ_ALKOD') then begin
    	Exit;
  end;
	vanrek := ( Query1.RecordCount > 0 );

  if not vanrek then begin
  	// Az adatb�zis m�g �res, �j felvitel
		ButtonUjClick(Sender);
  	if not Query_Run(Query1, 'Select * from '+ADATNEV+' where SZ_FOKOD=''000'' Order by SZ_ALKOD') then begin
    		Exit;
  	end;
 		vanrek := ( Query1.RecordCount > 0 );
  end;

  // A combobox felt�lt�se
	valaszto 	:= false;
  voltalt 		:= true;
	vancreate 	:= false;
  Elemki;
end;

procedure TBazisUjDlg.ButtonKilepClick(Sender: TObject);
begin
	if not voltalt then begin
  	voltalt 	:= true;
  	Exit;
  end;

	ret_vkod	:= '';
	Close;
end;

procedure TBazisUjDlg.FormDestroy(Sender: TObject);
begin
  mezonevek.Free;
  dispnevek.Free;
end;

procedure TBazisUjDlg.FormKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
	voltalt  := true;
end;

procedure TBazisUjDlg.ButtonTorClick(Sender: TObject);
begin
	if not voltalt then begin
		voltalt 	:= true;
		Exit;
	end;

	if NoticeKi( TORLOTEXT, NOT_QUESTION ) = 0 then begin
		// Rekord t�rl�se
		voltmodositas	:= true;
		if not Query_Run(Query2, 'DELETE from '+ADATNEV+
			' where SZ_FOKOD = ''' + Query1.FieldByName('SZ_ALKOD').AsString +
			''' and SZ_ALKOD = ''' + Query3.FieldByName('SZ_ALKOD').AsString +''''
			) then begin
			Exit;
     	end;
     	Elemki;
   end;
end;

procedure TBazisUjDlg.ButtonPrintClick(Sender: TObject);
begin
	if not voltalt then begin
 		voltalt 	:= true;
 		Exit;
 	end;
  	Application.CreateForm(TLiBazisDlg, LiBazisDlg);
	LiBazisDlg.QuickRep1.Preview;
 	LiBazisDlg.Destroy;
end;

procedure TBazisUjDlg.ButtonUjClick(Sender: TObject);
begin
  Adatlap('�j elem felvitele','');
end;

procedure TBazisUjDlg.ButtonModClick(Sender: TObject);
begin
	if ButtonMod.Enabled then begin
	  Adatlap('Elem m�dos�t�sa',Query3.FieldByName('SZ_ALKOD').AsString);
  end;
end;

procedure TBazisUjDlg.Adatlap(cim, kod : string);
begin
	if not voltalt then begin
		voltalt 	:= true;
		Exit;
	end;

	// Ez a r�szletes adatlap bek�r�si ablaka
	Application.CreateForm(TBazisbeDlg, BazisbeDlg);
	BazisbeDlg.Tolto(cim, kod, '', Query1.FieldByName('SZ_ALKOD').AsString,
	    Query1.FieldByName('SZ_KODHO').AsInteger, Query1.FieldByName('SZ_NEVHO').AsInteger);
	BazisbeDlg.ShowModal;
	if BazisbeDlg.ret_kod <> '' then begin
		Elemki;
		Query3.Locate('SZ_ALKOD', BazisbeDlg.ret_kod, [] );
		voltmodositas	:= true;
	end;
	BazisbeDlg.Destroy;
	DBGrid2.Refresh;
end;

procedure TBazisUjDlg.Elemki;
var
	fokod	: string;
begin
	fokod	:= Trim(Query1.FieldByName('SZ_ALKOD').AsString);
  	Query_Run(Query3, 'Select * from '+ADATNEV +' where SZ_FOKOD= '''+fokod+''' order by SZ_ALKOD ');
   ButtonUj.Enabled 	:= true;
   ButtonTor.Enabled 	:= true;
   ButtonMod.Enabled 	:= true;
  	if Query3.RecordCount < 1 then begin
  		ButtonTor.Enabled := false;
  		ButtonMod.Enabled := false;
  	end;
   if StrToIntDef(Query1.FieldByName('SZ_ERVNY').AsString,0) = 0 then begin
   	// Ha nem �rv�nyes a f�k�d, akkor csak m�dos�tani lehet az elemeket
  		ButtonUj.Enabled 	:= false;
  		ButtonTor.Enabled 	:= false;
   end;
end;


procedure TBazisUjDlg.DataSource1DataChange(Sender: TObject;
  Field: TField);
begin
  Elemki;
end;


procedure TBazisUjDlg.DBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
  DataCol: Integer; Column: TColumn; State: TGridDrawState);
var
  r0,r1,r2: TRect;
  s0,s1,s2: string;
  keresettmintahely: integer;
begin
  // DBGrid2.DefaultDrawDataCell(Rect, Column.Field, State);
 // ------ inkrement�lis keres�s minta-sz�nez�se ------------- //
 { keresettmintahely:= Pos(Uppercase_HUN(ebInkrementalis.Text), Uppercase_HUN(Column.Field.AsString));
  if (ebInkrementalis.Text = '') or (keresettmintahely = 0) then exit;  // nincs benne, nem sz�nez�nk

  r0 := Rect; r1 := Rect; r2 := Rect;
  r0.Top:= r0.Top +2;    // margin
  r1.Top:= r1.Top +2;    // margin
  r2.Top:= r2.Top +2;    // margin
  r0.Left:= r0.Left +2;  // margin
  DBGrid1.Canvas.FillRect(Rect);  // init
  // -- az eleje, ha van -- //
  if (keresettmintahely > 1) then begin
    s0 := copy(Column.Field.AsString,1,keresettmintahely-1);
    DBGrid1.Canvas.Font.Assign(DBGrid1.Font);
    DrawText(DBGrid1.Canvas.Handle,pchar(s0),length(s0), r0,DT_CALCRECT);
    DBGrid1.Canvas.TextOut(r0.Left,r0.Top,s0);
    end;  // if
  // -- a megtal�lt minta -- //
  s1 := copy(Column.Field.AsString,keresettmintahely,length(ebInkrementalis.Text));
  if (keresettmintahely > 1) then
    r1.Left := r0.Right + 1 // el kell jobbra tolni
  else r1.Left := r0.Left; // ugyanott kezd�dhet
  DBGrid1.Canvas.Font.Color := clRed;
  DBGrid1.Canvas.Font.Style := [fsbold];
  DrawText(DBGrid1.Canvas.Handle,PChar(s1),Length(s1), r1,DT_CALCRECT); // "... DrawText modifies the right side of the rectangle..."
  DBGrid1.Canvas.TextOut(r1.Left,r2.Top,s1);
  // -- a v�ge, ha van -- //
  if keresettmintahely + length(ebInkrementalis.Text) <= length(Column.Field.AsString) then begin
    DBGrid1.Canvas.Font.Assign(DBGrid1.Font);
    s2 := copy(Column.Field.AsString,keresettmintahely+length(ebInkrementalis.Text), 9999999);  // a minta v�g�t�l eg�sz v�gig
    r2.Left := r1.Right + 1;
    DrawText(DBGrid1.Canvas.Handle,pchar(s2),length(s2), r2,0);
    end;  // if
     }
  // ------ inkrement�lis keres�s minta-sz�nez�se ------------- //
end;

procedure TBazisUjDlg.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if ssAlt in Shift then begin
  	voltalt := true;
    Exit;
    end;
 	if ssCtrl in Shift then begin
    if Chr(Key) in ['S','s'] then begin
       ebInkrementalis.SetFocus;
       ebInkrementalis.SelectAll;
       Exit;
       end;  // Ctrl-S
    end;  // Ctrl-valami
	voltalt  := false;
	DBGrid1KeyDown(Sender, Key, Shift);

end;

procedure TBazisUjDlg.FormShow(Sender: TObject);
begin
  // Jogosults�gok lekezel�se
  case GetRightTag(Tag) of
  	RG_NORIGHT:
     	begin
        	ButtonPrint.Enabled := false;
        	ButtonUj.Enabled := false;
        	ButtonMod.Enabled := false;
        	ButtonTor.Enabled := false;
        end;
  	RG_READONLY:
     	begin
        	ButtonUj.Enabled := false;
        	ButtonMod.Enabled := false;
        	ButtonTor.Enabled := false;
        end;
  	RG_MODIFY:
     	begin
        	ButtonUj.Enabled := false;
        	ButtonTor.Enabled := false;
        end;
  end;
  ebInkrementalis.SetFocus;
end;

procedure TBazisUjDlg.Query1FilterRecord(DataSet: TDataSet;
  var Accept: Boolean);
var
  i: integer;
  Keresett, Mezotartalom: string;
begin
  Keresett:= Uppercase_HUN(ebInkrementalis.Text);
  for i := 0 to DataSet.FieldCount - 1 do begin
    Mezotartalom:= Uppercase_HUN(DataSet.Fields[i].AsString);
    Accept := Pos(Keresett, Mezotartalom) > 0;
    if Accept then exit;
    end; // for
end;

procedure TBazisUjDlg.DBGrid1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
	if ssCtrl in Shift then begin
   	if Chr(Key) in ['M','m'] then begin
			// F�k�dok m�dos�t�sa
			Application.CreateForm(TBazFobeDlg, BazFobeDlg);
			BazFobeDlg.Tolt('F�csoport m�dos�t�sa', Query1.FieldByName('SZ_ALKOD').AsString);
			BazFobeDlg.ShowModal;
			if BazFobeDlg.ret_kod <> '' then begin
 				Query1.Close;
 				Query1.Open;
 				Query1.Locate('SZ_ALKOD; SZ_FOKOD', VarArrayOf([BazFobeDlg.ret_kod,'000']), [] );
			end;
			BazFobeDlg.Destroy;
			DBGrid1.Refresh;
         Exit;
      end;
   	//if Key = Key_N then begin
   	if Chr(Key) in ['N','n'] then begin
			// F�k�dok l�trehoz�sa
			Application.CreateForm(TBazFobeDlg, BazFobeDlg);
			BazFobeDlg.Tolt('F�csoport l�trehoz�sa', '');
			BazFobeDlg.ShowModal;
			if BazFobeDlg.ret_kod <> '' then begin
 				Query1.Close;
 				Query1.Open;
 				Query1.Locate('SZ_ALKOD; SZ_FOKOD', VarArrayOf([BazFobeDlg.ret_kod,'000']), [] );
			end;
			BazFobeDlg.Destroy;
			DBGrid1.Refresh;
      end;
   	if Chr(Key) in ['D','d'] then begin
   	//if Key = Key_D then begin
			// F�k�dok t�rl�se
         if  NoticeKi('Figyelem, val�ban t�r�lni k�v�nja a kijel�lt f�csoportot?',NOT_QUESTION) = 0 then begin
            Query_Run(Query2, 'DELETE from '+ADATNEV+' where SZ_FOKOD = ''' + Query1.FieldByName('SZ_ALKOD').AsString + ''' ');
            Query_Run(Query2, 'DELETE from '+ADATNEV+' where SZ_FOKOD = ''000'' '+
  					' AND SZ_ALKOD = ''' + Query1.FieldByName('SZ_ALKOD').AsString +''' ');
				Query1.Close;
 				Query1.Open;
				DBGrid1.Refresh;
         end;
      end;
   end;
end;

procedure TBazisUjDlg.ebInkrementalisChange(Sender: TObject);
begin
  Query1.Filtered := false;
  Query1.Filtered := (ebInkrementalis.Text <> '');
end;

procedure TBazisUjDlg.ebInkrementalisClick(Sender: TObject);
begin
   ebInkrementalis.SelectAll;
end;

procedure TBazisUjDlg.BitBtn1Click(Sender: TObject);
begin
	// �sszevonjuk a kijel�lt elemet egy m�sikkal
	Application.CreateForm(TBazOsszevonbeDlg, BazOsszevonbeDlg);
	BazOsszevonbeDlg.Tolto(Query1.FieldByName('SZ_FOKOD').AsString,
   	Query1.FieldByName('SZ_ALKOD').AsString,Query1.FieldByName('SZ_MENEV').AsString);
	BazOsszevonbeDlg.ShowModal;
	BazOsszevonbeDlg.Destroy;
end;

end.

