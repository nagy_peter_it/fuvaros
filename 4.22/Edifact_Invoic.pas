unit Edifact_Invoic;

interface
uses SysUtils, Classes;
const
   MezoSzep = '+';
type
   TNevEsCim = record
      kod, nev, cim, varos, irsz, orszag: string;
      end;
   TGepkocsiAdatok = record
      rendszam, kategoria: string;
      end;
   TReferenciak = record
      pozicio, loadid, sajatAdoszam, vevoAdoszam, szamlaszam, lanid: string;
      end;
   TRakomanyAdatok = record
      suly: double;
      paletta: integer;
      end;
   TSzamlaTetel = record
      termekkod, leiras: string;
      netto, afa: double;
      end;
   TSzamlaTetelArray = array of TSzamlaTetel;

type
  TEdifactInvoic = class(TObject)
    private
       function ConvertSzam(str: string): string;
       function UNOAText(str: string): string;
    protected
    public
       FSzamlaKibocsato, FVevo, FFelrako, FLerako: TNevEsCim;
       FMessageReference, FSzamlaDeviza, FControlReference: string;
       FSzamladatum, FFelrakoDatum, FLerakoDatum: string;
       FOsszNetto, FOsszAfa: double;
       FRakomanyAdatok: TRakomanyAdatok;
       FGepkocsiAdatok: TGepkocsiAdatok;
       FReferenciak: TReferenciak;
       FSzamlaTetelek: TSzamlaTetelArray;
       FKuldoID, FFogadoID, FPlantNumber: string;
        // constructor Create(AOwner: TComponent);
        // destructor Destroy; override;
       function getDataOutput(IsTestMessage: boolean):string;
    published
    end;



implementation


function TEdifactInvoic.ConvertSzam(str: string): string;
begin
   Result := StringReplace(str,',','.', [rfReplaceAll]);;
end;

function TEdifactInvoic.UNOAText(str: string): string;
var
  res: string;
  i: Integer;
  mibol: string;
  mibe: string;
begin
  // Konvert�lni kell UNOA karakterk�szlet szerint (nagybet�, sz�m, sz�k�z, .,-()/=
  res := UpperCase(str);
  mibol := '��������';
  mibe := 'AUUIEOUO';
  for i := 1 to Length(mibol) do
  begin
    while Pos(copy(mibol, i, 1), res) > 0 do
    begin
      res := copy(res, 1, Pos(copy(mibol, i, 1), res) - 1) + copy(mibe, i, 1) +
        copy(res, Pos(copy(mibol, i, 1), res) + 1, 999);
    end;
  end;
  Result := res;
end;

function TEdifactInvoic.getDataOutput(IsTestMessage: boolean):string;
const
  EDIFACT_FieldSep = '+';
  EDIFACT_SubFieldSep = ':';
  EDIFACT_LineSep = chr(13)+chr(10);
  ALC_VAT_CODE = '750';
  ALC_VAT_DESCRIPTION = 'VAT CHARGES';

var
  Sor: TStringList;
  SegmentCount, i, Paletta: integer;
  TestIndicator: string;
  Suly: double;

  procedure Sor_Add(Elem: string);
    var
      E: string;
    begin
       E:= StringReplace(Elem, EDIFACT_FieldSep,' ',[rfReplaceAll]); // ne okozzon gondot
       // E:= StringReplace(E, EDIFACT_SubFieldSep,' ',[rfReplaceAll]); // ezt nem lehet
       Sor.Add(E);
    end;

  procedure SorKuld;
    begin
       Result:= Result + UNOAText(Sor.DelimitedText) + ''''+ EDIFACT_LineSep;
       if Sor[0]='UNH' then SegmentCount := 0;  // ne sz�molja az "Interchange header"-t
       Inc(SegmentCount);
    end;

  procedure SorbaNevCim(MelyikNAD: string; NevCimRekord: TNevEsCim);
    begin
      Sor.Clear;
      Sor_Add('NAD');
      Sor_Add(MelyikNAD);   // NAD010
      Sor_Add(NevCimRekord.Kod);  // NAD020
      Sor_Add('');  // NAD030
      Sor_Add(NevCimRekord.nev);  // NAD040
      Sor_Add(NevCimRekord.cim);
      Sor_Add(NevCimRekord.varos);
      Sor_Add('');
      Sor_Add(NevCimRekord.irsz);
      Sor_Add(NevCimRekord.orszag);
      SorKuld;
    end;

   procedure SorbaOsszeg(const Kateg: string; const Osszeg: double);
    begin
        // ---  MOA � MONETARY AMOUNT
        Sor.Clear;
        Sor_Add('MOA');
        Sor_Add(Kateg + EDIFACT_SubFieldSep + ConvertSzam(Format('%.2f', [Osszeg])));
        SorKuld;
     end;

   procedure SorbaSzamlasor(const Termekkod, Leiras: string; Osszeg: double);
    begin
     Sor.Clear;
        Sor_Add('ALC');
        Sor_Add('C');  // ALC010
        Sor_Add('');  // ALC020
        Sor_Add('');  // ALC030
        Sor_Add('');  // ALC040
        Sor_Add(Termekkod + EDIFACT_SubFieldSep + EDIFACT_SubFieldSep + EDIFACT_SubFieldSep + Leiras);
        SorKuld;
        // ---  MOA � MONETARY AMOUNT
        SorbaOsszeg('23', Osszeg);
     end;

  procedure SorbaReferencia(const MelyikRef, RefErtek: string);
    begin
      if RefErtek <> '' then begin
        Sor.Clear;
        Sor_Add('RFF');
        Sor_Add(MelyikRef+EDIFACT_SubFieldSep+RefErtek);
        SorKuld;
        end;  // if
    end;

begin
  Sor:= TStringList.Create;
  Sor.Delimiter:= EDIFACT_FieldSep;
  Sor.QuoteChar := chr(0);
  Sor.StrictDelimiter := True;
  try
    // --- UNB � INTERCHANGE HEADER
    Sor.Clear;
    Sor_Add('UNB');
    Sor_Add('UNOA'+EDIFACT_SubFieldSep+'1'); // 1 = version number
    Sor_Add(FKuldoID);  // Sender Identification
    Sor_Add(FFogadoID+EDIFACT_SubFieldSep+'');  // Recipient Identification + Partner Identification Code Qualifier
    Sor_Add(FormatDateTime('yymmdd', now)+EDIFACT_SubFieldSep+FormatDateTime('HHmmss', now)); // Date/Time Preparation
    Sor_Add(FControlReference);
    Sor_Add('');   // Acknowledge request
    if IsTestMessage then TestIndicator:= 'T'
    else TestIndicator:= '';
    Sor_Add(TestIndicator);
    SorKuld;
    // ---- UNH � MESSAGE HEADER
    Sor.Clear;
    Sor_Add('UNH');
    Sor_Add(FMessageReference);
    Sor_Add('INVOIC'+EDIFACT_SubFieldSep+'D'+EDIFACT_SubFieldSep+'96A'+EDIFACT_SubFieldSep+'UN');
    SorKuld;
    // ---- BGM � BEGINNING OF MESSAGE
    Sor.Clear;
    Sor_Add('BGM');
    Sor_Add('380');
    Sor_Add(FReferenciak.szamlaszam);
    Sor_Add('9');
    SorKuld;
    // ---- DTM � DATE/TIME/PERIOD
    Sor.Clear;
    Sor_Add('DTM');
    Sor_Add('3'+EDIFACT_SubFieldSep+FSzamladatum+EDIFACT_SubFieldSep+'102');
    SorKuld;
    // ---- NAD II - NAME AND ADDRESS
    SorbaNevCim('II', FSzamlaKibocsato);
    // ---- NAD IV - NAME AND ADDRESS
    SorbaNevCim('IV', FVevo);
    // ---- CUX - CURRENCIES
    Sor.Clear;
    Sor_Add('CUX');
    Sor_Add('1'+EDIFACT_SubFieldSep+FSzamlaDeviza+EDIFACT_SubFieldSep+'4');
    SorKuld;
    // ---- LIN � LINE ITEM
    Sor.Clear;
    Sor_Add('LIN');
    Sor_Add('1');
    SorKuld;
    // ---- MEA - MEASUREMENTS
    Sor.Clear;
    Sor_Add('MEA');
    Sor_Add('WT');
    Sor_Add('AAD');
    Suly:= FRakomanyAdatok.suly;
    if Suly=0 then Suly:= 1; //  CTSI: "If there are no pieces and weight to provide please default to 1"
    Sor_Add('KGM'+EDIFACT_SubFieldSep+ConvertSzam(Format('%.0f', [Suly]))); // MEA030
    SorKuld;
    // ---  DTM � DATE/TIME/PERIOD
    Sor.Clear;
    Sor_Add('DTM');
    Sor_Add('110'+EDIFACT_SubFieldSep+FFelrakoDatum+EDIFACT_SubFieldSep+'102');
    SorKuld;

    Sor.Clear;
    Sor_Add('DTM');
    Sor_Add('35'+EDIFACT_SubFieldSep+FLerakoDatum+EDIFACT_SubFieldSep+'102');
    SorKuld;
    // ---  EQD � EQUIPMENT DETAILS
    Sor.Clear;
    Sor_Add('EQD');
    Sor_Add('TE');
    Sor_Add(FGepkocsiAdatok.rendszam);
    Sor_Add(FGepkocsiAdatok.kategoria);
    SorKuld;
    // ---  RFF � REFERENCE
    SorbaReferencia('HWB', FReferenciak.szamlaszam);
    SorbaReferencia('AHP', FReferenciak.vevoAdoszam);
    SorbaReferencia('VX', FReferenciak.sajatAdoszam);
    SorbaReferencia('ADE', FPlantNumber);
    SorbaReferencia('PO', FReferenciak.pozicio);
    // ---  PAC - PACKAGE
    Sor.Clear;
    Sor_Add('PAC');
    Paletta:= FRakomanyAdatok.paletta;
    if Paletta= 0 then Paletta:= 1;  //  CTSI: "If there are no pieces and weight to provide please default to 1"
    Sor_Add(Format('%d', [Paletta]));
    Sor_Add('201'); // PAC030-010
    SorKuld;
     // ---- NAD SF - NAME AND ADDRESS
    SorbaNevCim('SF', FFelrako);
     // ---- NAD SF - NAME AND ADDRESS
    SorbaNevCim('ST', FLerako);

    for i:= 0 to Length(FSzamlaTetelek)-1 do begin
        // ---------------------  N E T T � ------------------------ //
        // SorbaSzamlasor(FSzamlaTetelek[i].termekkod, FSzamlaTetelek[i].leiras, FSzamlaTetelek[i].netto + FSzamlaTetelek[i].afa);
        SorbaSzamlasor(FSzamlaTetelek[i].termekkod, FSzamlaTetelek[i].leiras, FSzamlaTetelek[i].netto);
        // ---------------------  � F A  ------------------------ //
        SorbaSzamlasor(ALC_VAT_CODE, ALC_VAT_DESCRIPTION, FSzamlaTetelek[i].afa);
        end;  // for
  // ---  TOD � TERMS OF DELIVERY OR TRANSPORT
    Sor.Clear;
    Sor_Add('TOD');
    Sor_Add('6');
    Sor_Add('CC');
    Sor_Add(''); // TOD030, FAP: 001::INCOTERMS 1
    SorKuld;
  // ---  UNS � SECTION CONTROL
    Sor.Clear;
    Sor_Add('UNS');
    Sor_Add('S');
    SorKuld;
  // ---  MOA � MONETARY AMOUNT
    SorbaOsszeg('128', FOsszNetto+FOsszAfa);
    SorbaOsszeg('125', FOsszNetto);
    SorbaOsszeg('150', FOsszAfa);
    // ---  UNT � MESSAGE TRAILER
    Sor.Clear;
    Sor_Add('UNT');
    Sor_Add(IntToStr(SegmentCount+1)); // ezzel egy�tt
    Sor_Add(FMessageReference);
    SorKuld;

    // ---  UNZ � INTERCHANGE TRAILER
    Sor.Clear;
    Sor_Add('UNZ');
    Sor_Add('1'); // Interchange Control Count
    Sor_Add(FControlReference); // Interchange Control Reference
    SorKuld;
  finally
    if Sor <> nil then Sor.Free;
    end;  // try - finally

end;
    {

Kimaradt:
   Soriro('FTX+ACB++' + mbkod + '''');
   Soriro('PYT+2''');
   Soriro('PAI+::2''');
   Soriro('TDT+21++1''');
      }
end.
