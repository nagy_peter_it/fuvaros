unit Jelcsere;

interface

uses
	SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
	Forms, Dialogs, StdCtrls, Buttons, DB, DBTables, ADODB;

type
	TJelszoCsereDlg = class(TForm)
	BitBtn1: TBitBtn;
	BitBtn2: TBitBtn;
	Edit1: TEdit;
	Edit2: TEdit;
	Edit3: TEdit;
	Label1: TLabel;
	Label2: TLabel;
	Label3: TLabel;
    Query1: TADOQuery;
	procedure BitBtn2Click(Sender: TObject);
	procedure BitBtn1Click(Sender: TObject);
	procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
	private
	end;

var
	JelszoCsereDlg: TJelszoCsereDlg;

implementation

uses
	Egyeb, J_SQL, Kozos;

{$R *.DFM}

procedure TJelszoCsereDlg.BitBtn2Click(Sender: TObject);
begin
	Close;
end;

procedure TJelszoCsereDlg.BitBtn1Click(Sender: TObject);
begin
	if Edit2.Text = '' then begin
	NoticeKi( 'A jelsz� nem lehet �res!', NOT_ERROR );
  	Edit2.Setfocus;
  	Exit;
  end;
  if Edit2.Text <> Edit3.Text then begin
	NoticeKi( 'A jelsz�nak �s az ism�tl�s�nek egyeznie kell!', NOT_ERROR );
		Edit3.Text := '';
  	Edit3.Setfocus;
  	Exit;
  end;
	if not Query_Run(Query1, 'SELECT JE_JESZO, JE_FEKOD FROM JELSZO WHERE JE_JESZO = '''+Edit1.Text+'''') then begin
  	Exit;
  end;
	if Query1.RecordCount < 1 then begin
	NoticeKi( 'Hi�nyz� jelsz�!', NOT_ERROR );
		Edit1.Setfocus;
	end else begin
  	if Query1.FieldByName('JE_FEKOD').AsString <> EgyebDlg.user_code then begin
  		NoticeKi( 'Hib�s jelsz�!', NOT_ERROR );
        Edit1.Text := '';
        Edit1.Setfocus;
     end else begin
     	{A jelsz� kicser�l�se}
			Query_Run(Query1, 'UPDATE JELSZO SET JE_JESZO ='''+Edit2.Text+''' WHERE JE_JESZO = '''+Edit1.Text+'''');
        Close;
     end;
	end;
end;

procedure TJelszoCsereDlg.FormCreate(Sender: TObject);
begin
  EgyebDlg.SeTADOQueryDatabase(Query1);
	Edit1.Text := '';
	Edit2.Text := '';
	Edit3.Text := '';
end;

procedure TJelszoCsereDlg.FormDestroy(Sender: TObject);
begin
	Query1.Close;
end;

end.
