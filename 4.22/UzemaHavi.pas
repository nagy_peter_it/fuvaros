unit UzemaHavi;

interface

uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls, Forms, Dialogs,
  DBTables, DB, StdCtrls, Buttons, Mask, DBCtrls, ExtCtrls, J_SQL,
  ADODB, QuickRpt, CheckLst;

type
  TUzemaHaviDlg = class(TForm)
    BitBtn4: TBitBtn;
    BitBtn6: TBitBtn;
    Query1: TADOQuery;
    Label46: TLabel;
    CBRendszam: TComboBox;
    CB2: TCheckBox;
    Label1: TLabel;
    M2: TMaskEdit;
    BitBtn2: TBitBtn;
    QueryKoz: TADOQuery;
    CL1: TCheckListBox;
    CheckBox1: TCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure BitBtn4Click(Sender: TObject);
    procedure BitBtn6Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure M2Exit(Sender: TObject);
    procedure CheckBox1Click(Sender: TObject);
    procedure CBRendszamClick(Sender: TObject);
    procedure CL1ClickCheck(Sender: TObject);
  private
		listagkat		: TStringList;
  public
		vanadat	: boolean;
  end;

var
  UzemaHaviDlg: TUzemaHaviDlg;

implementation

uses
	J_VALASZTO, UzemalisHavi, UzemalisHavi2, Egyeb, Kozos, Kozos_Local;
{$R *.DFM}

procedure TUzemaHaviDlg.FormCreate(Sender: TObject);
var
   i: integer;
   S: string;
begin
  	EgyebDlg.SetADOQueryDatabase(Query1);
  	EgyebDlg.SetADOQueryDatabase(QueryKoz);
   // A g�pkocsi bet�lt�se
   cbRendszam.Items.Add('Minden g�pkocsi');
   // Query_Run(QueryKoz, 'SELECT GK_RESZ FROM GEPKOCSI WHERE  ( GK_POTOS = 2 OR GK_POTOS = 0) AND GK_ARHIV = 0 AND GK_GKKAT NOT IN (''MG'', ''SZGK.'', '''') ORDER BY GK_RESZ');
   S:='SELECT GK_RESZ FROM GEPKOCSI WHERE  ( GK_POTOS = 2 OR GK_POTOS = 0) AND GK_ARHIV = 0 AND GK_GKKAT NOT IN (''MG'', ''SZGK.'', '''') ';
   S:=S+' AND GK_NOKIM = 0 ';  // amiket nem z�rtunk ki a kimutat�sb�l
   S:=S+' ORDER BY GK_RESZ';
   Query_Run(QueryKoz, S);
   while not QueryKoz.Eof do begin
       cbRendszam.Items.Add(QueryKoz.FieldByName('GK_RESZ').AsString);
       QueryKoz.Next;
   end;
   cbRendszam.ItemIndex   := 0;
	listagkat	:= TStringList.Create;
//   Query_Run(QueryKoz, 'SELECT DISTINCT GK_GKKAT FROM GEPKOCSI WHERE ( GK_POTOS = 2 OR GK_POTOS = 0) AND GK_ARHIV = 0 AND GK_GKKAT NOT IN (''MG'', ''SZGK.'', '''') AND GK_KIVON = 0 ORDER BY GK_GKKAT ');
//    Query_Run(QueryKoz, 'SELECT DISTINCT GK_GKKAT FROM GEPKOCSI WHERE ( GK_POTOS = 2 OR GK_POTOS = 0) AND GK_ARHIV = 0 AND GK_KIVON = 0 ORDER BY GK_GKKAT ');
   Query_Run(QueryKoz, 'SELECT DISTINCT GK_GKKAT FROM GEPKOCSI WHERE ( GK_POTOS = 2 OR GK_POTOS = 0) AND GK_ARHIV = 0 AND GK_KIVON = 0 AND GK_NOKIM = 0 ORDER BY GK_GKKAT ');
   CL1.Clear;
   listagkat.Add('');
   CL1.Items.Add('Minden kat.');
   while not QueryKoz.Eof do begin
       listagkat.Add(QueryKoz.FieldByName('GK_GKKAT').AsString);
       CL1.Items.Add(QueryKoz.FieldByName('GK_GKKAT').AsString);
       Querykoz.Next;
   end;
	CL1.ItemIndex		:= 0;
   CheckBox1Click(Sender);
end;

procedure TUzemaHaviDlg.BitBtn4Click(Sender: TObject);
var
   kdat        : string;
   vdat        : string;
   rsz         : string;
   kategstr    : string;
   i           : integer;
begin
	Screen.Cursor	:= crHourGlass;
   kdat    := M2.Text+'01.';
   if not jodatum(kdat) then begin
       NoticeKi('�rv�nytelen h�nap!');
       M2.SetFocus;
       Exit;
   end;
   vdat    := DatumHozNap(kdat, 40, true);
   vdat    := copy(vdat, 1, 8)+'01.';
   vdat    := DatumHozNap(vdat, -1, true);
   rsz     := '';

   // A csoportok �sszegy�jt�se
   kategstr	:= '';
   for i := 1 to CL1.Items.Count - 1 do begin
       if CL1.Checked[i] then begin
           kategstr	:= kategstr + ''''+ listagkat[i] + ''', ';
       end;
   end;
   if kategstr <> '' then begin
       kategstr := copy(kategstr, 1, Length(kategstr)-2);
   end;

   if CBRendszam.ItemIndex > 0 then begin
       rsz := CBRendszam.Text;
   end;
   if CB2.Checked then begin
       // R�szletes lista
       Application.CreateForm(TUzemalisHaviDlg, UzemalisHaviDlg);
       UzemalisHaviDlg.kategoriak  := kategstr;
       UzemalisHaviDlg.Tolt(rsz, kdat, vdat, '1','0');
       if UzemalisHaviDlg.vanadat then begin
           UzemalisHaviDlg.RepUzem.Preview;
       end else begin
           NoticeKi('Nincs megfelel� adat!');
       end;
       UzemalisHaviDlg.Destroy;
   end else begin
       // Nem r�szletes lista
       Application.CreateForm(TUzemalisHavi2Dlg, UzemalisHavi2Dlg);
       UzemalisHavi2Dlg.kategoriak  := kategstr;
       UzemalisHavi2Dlg.Tolt(rsz, kdat, vdat, '1','0');
       if UzemalisHavi2Dlg.vanadat then begin
           UzemalisHavi2Dlg.RepUzemHavi2.Preview;
       end else begin
           NoticeKi('Nincs megfelel� adat!');
       end;
       UzemalisHavi2Dlg.Destroy;
   end;

end;

procedure TUzemaHaviDlg.BitBtn6Click(Sender: TObject);
begin
	Close;
end;

procedure TUzemaHaviDlg.BitBtn2Click(Sender: TObject);
begin
	EgyebDlg.Calendarbe(M2);
   M2.Text := copy(M2.Text,1,8);
end;

procedure TUzemaHaviDlg.M2Exit(Sender: TObject);
begin
   DatumExit(Sender, false);
   M2.Text := copy(M2.Text,1,8);
end;

procedure TUzemaHaviDlg.CheckBox1Click(Sender: TObject);
begin
   if CheckBox1.Checked then begin
       CL1.Enabled := true;
   end else begin
       CL1.Enabled := false;
   end;
end;

procedure TUzemaHaviDlg.CBRendszamClick(Sender: TObject);
begin
   if CBRendszam.ItemIndex = 0 then begin
       CheckBox1.Enabled   := true;
   end else begin
       // Le kell tiltani a kkateg�ria v�laszt�st
       CheckBox1.Checked   := false;
       CheckBox1Click(Sender);
       CheckBox1.Enabled   := false;
   end;
end;

procedure TUzemaHaviDlg.CL1ClickCheck(Sender: TObject);
var
	i : integer;
begin
   if CL1.ItemIndex = 0 then begin
       // Mindent be�ll�tunk az els�nek megfelel�en
       for i := 1 to CL1.Items.Count - 1 do begin
           CL1.Checked[i]	:= CL1.Checked[0];
       end;
   end;
end;

end.


