unit GkIdoKmList;

interface

uses
  WinTypes, WinProcs, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, QuickRpt, QRExpr, Qrctrls, ExtCtrls, printers, DB, DBTables, Egyeb, Forgalom, J_SQL,
  Grids, Kozos, ADODB ;

type
  TGkIdoKmlisDlg = class(TForm)
    Rep: TQuickRep;
    QRBand3: TQRBand;
    QRLSzamla: TQRLabel;
    QRBand1: TQRBand;
    QRLabel6: TQRLabel;
    QRSysData1: TQRSysData;
    QRLabel7: TQRLabel;
    QRSysData4: TQRSysData;
    QRLabel1: TQRLabel;
    QRLabel13: TQRLabel;
    QRLabel14: TQRLabel;
    Query1: TADOQuery;
    QRGroup1: TQRGroup;
    QRBand2: TQRBand;
    QRLabel2: TQRLabel;
    QRLabel21: TQRLabel;
    QRLabel18: TQRLabel;
    QRShape1: TQRShape;
    QRLabel3: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel8: TQRLabel;
    QRShape2: TQRShape;
    QRLabel9: TQRLabel;
    QRBand4: TQRBand;
    QRLabel10: TQRLabel;
    QRShape3: TQRShape;
    QRLabel11: TQRLabel;
	 procedure FormCreate(Sender: TObject);
	 procedure	Tolt(resz,nevsorrend : boolean);
	 procedure QRBand1BeforePrint(Sender: TQRCustomBand;
	   var PrintBand: Boolean);
	 procedure RepBeforePrint(Sender: TCustomQuickRep;
	   var PrintReport: Boolean);
	 procedure QRGroup1BeforePrint(Sender: TQRCustomBand;
	   var PrintBand: Boolean);
	 procedure QRBand2BeforePrint(Sender: TQRCustomBand;
	   var PrintBand: Boolean);
    procedure QRBand4BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
  private
		reszletes		: boolean;
		atlag,oatlag			: double;
		letszam,no			: integer;
  public
	  vanadat			: boolean;
  end;

var
  GkIdoKmlisDlg: TGkIdoKmlisDlg;

implementation

{$R *.DFM}

procedure TGkIdoKmlisDlg.FormCreate(Sender: TObject);
begin
	EgyebDlg.SetADOQueryDatabase(Query1);
end;

procedure	TGkIdoKmlisDlg.Tolt(resz,nevsorrend : boolean);
begin
	reszletes 	:= resz;
//	Query_Run(Query1, 'SELECT * FROM DOLGOZO WHERE DO_ARHIV = 0 ORDER BY DO_GKKAT, DO_NAME');
  if reszletes then
   if nevsorrend then
    	Query_Run(Query1, 'SELECT * FROM DOLGOZO WHERE DO_ARHIV = 0 ORDER BY DO_CSNEV, DO_NAME')
   else
  	  Query_Run(Query1, 'SELECT * FROM DOLGOZO WHERE DO_ARHIV = 0 ORDER BY DO_CSNEV,CAST (DO_MBKAT as integer), DO_NAME')
  else
  	Query_Run(Query1, 'SELECT * FROM DOLGOZO WHERE DO_ARHIV = 0 ORDER BY DO_CSNEV, DO_NAME');
	vanadat 			:= Query1.RecordCount > 0;
//	QrGroup1.Expression	:= 'Query1.DO_GKKAT';
	QrGroup1.Expression	:= 'Query1.DO_CSNEV';
end;

procedure TGkIdoKmlisDlg.QRBand1BeforePrint(Sender: TQRCustomBand;
	   var PrintBand: Boolean);
var
  honap:integer;
begin
	QrLabel3.Caption	:= Query1.FieldByName('DO_NAME').AsString;
	honap:=Query1.FieldByName('DO_MBKAT').AsInteger;
	QrLabel4.Caption	:=  IntToStr(honap div 12)+' �v '+IntToStr(honap mod 12)+' h�';
//	QrLabel4.Caption	:= Query1.FieldByName('DO_MBKAT').AsString+' �v';
	Inc(letszam);
//	atlag	:= atlag + StringSzam(Query1.FieldByName('DO_MBKAT').AsString);
//  oatlag:= oatlag+ StringSzam(Query1.FieldByName('DO_MBKAT').AsString);
	atlag	:= atlag + honap;
  oatlag:= oatlag+ honap;
  inc(no);
  QRLabel9.Caption:=IntToStr(no)+'.';
	PrintBand			:= reszletes;
end;

procedure TGkIdoKmlisDlg.RepBeforePrint(Sender: TCustomQuickRep;
  var PrintReport: Boolean);
begin
 // Caption:='Dolgoz�k ideje kateg�ri�nk�nt';
	letszam		:= 0;
	atlag		:= 0;
	oatlag		:= 0;
  no:=0;
end;

procedure TGkIdoKmlisDlg.QRGroup1BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
//	QrLabel2.Caption	:= 'Gk. kateg�ria : '+Query1.FieldByName('DO_GKKAT').AsString;
	QrLabel2.Caption	:= 'Dolgoz�i csoport : '+Query1.FieldByName('DO_CSNEV').AsString;
	PrintBand			:= reszletes;
end;

procedure TGkIdoKmlisDlg.QRBand2BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
  honap: integer;
begin
//	QrLabel5.Caption	:= '�tlag a gk. kateg�ri�ra : '+Query1.FieldByName('DO_GKKAT').AsString;
	QrLabel5.Caption	:= '�tlag a dolgoz�i csoportra : '+Query1.FieldByName('DO_CSNEV').AsString;
	if letszam = 0 then begin
		QrLabel8.Caption	:= '0';
	end else begin
		//QrLabel8.Caption	:= Format('%.1f �v', [atlag/letszam]);
    honap:= Trunc(atlag/letszam);
  	QrLabel8.Caption	:=  IntToStr(honap div 12)+' �v '+IntToStr(honap mod 12)+' h�';
	end;
	atlag				:= 0;
	letszam				:= 0;
	if not reszletes then begin
		QrShape2.Width		:= 0;
	end;
end;

procedure TGkIdoKmlisDlg.QRBand4BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
  honap: integer;
begin
//		QrLabel11.Caption	:= Format('%.1f �v', [oatlag/no]);
    honap:= Trunc(oatlag/no);
  	QrLabel11.Caption	:=  IntToStr(honap div 12)+' �v '+IntToStr(honap mod 12)+' h�';

end;

end.
