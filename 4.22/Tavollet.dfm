object FTavollet: TFTavollet
  Left = 419
  Top = 176
  Caption = 'FTavollet'
  ClientHeight = 704
  ClientWidth = 1264
  Color = 8454143
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -16
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 20
  object Panel3: TPanel
    Left = 0
    Top = 663
    Width = 1264
    Height = 41
    Align = alBottom
    TabOrder = 0
    Visible = False
  end
  object TabControl3: TTabControl
    Left = 0
    Top = 0
    Width = 1264
    Height = 663
    Align = alClient
    MultiLine = True
    OwnerDraw = True
    TabOrder = 1
    Tabs.Strings = (
      'Mind')
    TabIndex = 0
    OnChange = TabControl3Change
    OnDrawTab = TabControl3DrawTab
    object TabControl1: TTabControl
      Left = 4
      Top = 31
      Width = 1256
      Height = 628
      Align = alClient
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      OwnerDraw = True
      ParentFont = False
      TabOrder = 0
      Tabs.Strings = (
        'Janu'#225'r'
        'Febru'#225'r'
        'M'#225'rcius'
        #193'prilis'
        'M'#225'jus'
        'J'#250'nius'
        'J'#250'lius'
        'Augusztus'
        'Szeptember'
        'Okt'#243'ber'
        'November'
        'December')
      TabIndex = 0
      OnChange = TabControl1Change
      OnChanging = TabControl1Changing
      OnDrawTab = TabControl1DrawTab
      DesignSize = (
        1256
        628)
      object DBText2: TDBText
        Left = 988
        Top = 3
        Width = 268
        Height = 24
        Alignment = taRightJustify
        Anchors = [akTop, akRight]
        DataField = 'DO_NAME'
        DataSource = DSDolgozo
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -19
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Panel1: TPanel
        Left = 4
        Top = 555
        Width = 1248
        Height = 69
        Align = alBottom
        Color = 8454143
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentBackground = False
        ParentFont = False
        TabOrder = 0
        DesignSize = (
          1248
          69)
        object Label1: TLabel
          Left = 312
          Top = 25
          Width = 65
          Height = 40
          Alignment = taCenter
          AutoSize = False
          Caption = 'SZ'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -37
          Font.Name = 'Verdana'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label2: TLabel
          Left = 247
          Top = 11
          Width = 194
          Height = 22
          Alignment = taCenter
          AutoSize = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object DBText1: TDBText
          Left = 8
          Top = 8
          Width = 80
          Height = 24
          AutoSize = True
          DataField = 'DO_NAME'
          DataSource = DSDolgozo
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -19
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label3: TLabel
          Left = 240
          Top = 44
          Width = 57
          Height = 20
          Caption = 'Label3'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label4: TLabel
          Left = 1057
          Top = 8
          Width = 137
          Height = 16
          Caption = #214'sszes szabads'#225'g:'
          Visible = False
        end
        object Label5: TLabel
          Left = 792
          Top = 47
          Width = 91
          Height = 16
          Caption = 'B'#233'relsz. k'#243'd:'
        end
        object DBNavigator1: TDBNavigator
          Left = 8
          Top = 40
          Width = 228
          Height = 25
          DataSource = DSDolgozo
          VisibleButtons = [nbFirst, nbPrior, nbNext, nbLast]
          TabOrder = 0
        end
        object RadioButton1: TRadioButton
          Left = 448
          Top = 8
          Width = 161
          Height = 17
          Caption = 'H'#233'tk'#246'znapi '#225'll'#225's ('#193')'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 1
          OnClick = RadioButton1Click
        end
        object RadioButton2: TRadioButton
          Left = 448
          Top = 28
          Width = 137
          Height = 17
          Caption = 'Szabads'#225'g (SZ)'
          Checked = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 2
          TabStop = True
          OnClick = RadioButton2Click
        end
        object RadioButton3: TRadioButton
          Left = 448
          Top = 48
          Width = 153
          Height = 17
          Caption = 'Beteg'#225'llom'#225'ny (B)'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 3
          OnClick = RadioButton3Click
        end
        object RadioButton4: TRadioButton
          Left = 616
          Top = 8
          Width = 209
          Height = 17
          Caption = 'Egy'#233'b igazolt t'#225'voll'#233't (EI)'
          TabOrder = 4
          OnClick = RadioButton4Click
        end
        object RadioButton5: TRadioButton
          Left = 616
          Top = 28
          Width = 249
          Height = 17
          Caption = 'Egy'#233'b igazolatlan t'#225'voll'#233't (EL)'
          TabOrder = 5
          OnClick = RadioButton5Click
        end
        object RadioButton6: TRadioButton
          Left = 616
          Top = 48
          Width = 113
          Height = 17
          Caption = 'T'#246'rl'#233's'
          TabOrder = 6
          OnClick = RadioButton6Click
        end
        object kereso: TEdit
          Left = 376
          Top = 40
          Width = 57
          Height = 24
          TabOrder = 7
          Visible = False
          OnEnter = keresoEnter
        end
        object BitKilep: TBitBtn
          Left = 1124
          Top = 33
          Width = 117
          Height = 32
          HelpContext = 10012
          Anchors = [akTop, akRight]
          Cancel = True
          Caption = 'Kil'#233'p'#233's'
          Glyph.Data = {
            CE070000424DCE07000000000000360000002800000024000000120000000100
            1800000000009807000000000000000000000000000000000000008080008080
            0080800080800080800080800080800080800080800080800080800080800080
            8000808000808000808000808000808000808000808000808000808000808000
            8080008080008080008080008080008080008080008080008080008080008080
            0080800080800080800080800080800080808080808080800080800080800080
            8000808000808000808000808000808000808000808000808000808000808000
            8080008080008080008080FFFFFF008080008080008080008080008080008080
            0080800080800080800080800080800080800080800080800080800000FF0000
            800000808080800080800080800080800080800080800000FF80808000808000
            8080008080008080008080008080008080008080808080808080FFFFFF008080
            008080008080008080008080008080FFFFFF0080800080800080800080800080
            800080800080800000FF00008000008000008080808000808000808000808000
            00FF000080000080808080008080008080008080008080008080008080808080
            FFFFFF008080808080FFFFFF008080008080008080FFFFFF808080808080FFFF
            FF0080800080800080800080800080800080800000FF00008000008000008000
            00808080800080800000FF000080000080000080000080808080008080008080
            008080008080008080808080FFFFFF008080008080808080FFFFFF008080FFFF
            FF808080008080008080808080FFFFFF00808000808000808000808000808000
            80800000FF000080000080000080000080808080000080000080000080000080
            000080808080008080008080008080008080008080808080FFFFFF0080800080
            80008080808080FFFFFF808080008080008080008080008080808080FFFFFF00
            80800080800080800080800080800080800000FF000080000080000080000080
            0000800000800000800000808080800080800080800080800080800080800080
            80008080808080FFFFFF00808000808000808080808000808000808000808000
            8080FFFFFF808080008080008080008080008080008080008080008080008080
            0000FF0000800000800000800000800000800000808080800080800080800080
            80008080008080008080008080008080008080808080FFFFFF00808000808000
            8080008080008080008080FFFFFF808080008080008080008080008080008080
            0080800080800080800080800080800000800000800000800000800000808080
            8000808000808000808000808000808000808000808000808000808000808000
            8080808080FFFFFF008080008080008080008080008080808080008080008080
            0080800080800080800080800080800080800080800080800080800000FF0000
            8000008000008000008080808000808000808000808000808000808000808000
            8080008080008080008080008080008080808080FFFFFF008080008080008080
            8080800080800080800080800080800080800080800080800080800080800080
            800080800000FF00008000008000008000008000008080808000808000808000
            8080008080008080008080008080008080008080008080008080008080808080
            008080008080008080008080808080FFFFFF0080800080800080800080800080
            800080800080800080800080800000FF00008000008000008080808000008000
            0080000080808080008080008080008080008080008080008080008080008080
            008080008080808080008080008080008080008080008080808080FFFFFF0080
            800080800080800080800080800080800080800080800000FF00008000008000
            00808080800080800000FF000080000080000080808080008080008080008080
            008080008080008080008080008080808080008080008080008080808080FFFF
            FF008080008080808080FFFFFF00808000808000808000808000808000808000
            80800000FF0000800000808080800080800080800080800000FF000080000080
            000080808080008080008080008080008080008080008080808080FFFFFF0080
            80008080808080008080808080FFFFFF008080008080808080FFFFFF00808000
            80800080800080800080800080800080800000FF000080008080008080008080
            0080800080800000FF0000800000800000800080800080800080800080800080
            80008080808080FFFFFFFFFFFF808080008080008080008080808080FFFFFF00
            8080008080808080FFFFFF008080008080008080008080008080008080008080
            0080800080800080800080800080800080800080800000FF0000800000FF0080
            8000808000808000808000808000808000808080808080808000808000808000
            8080008080008080808080FFFFFFFFFFFFFFFFFF808080008080008080008080
            0080800080800080800080800080800080800080800080800080800080800080
            8000808000808000808000808000808000808000808000808000808000808000
            8080008080008080008080008080008080008080008080808080808080808080
            0080800080800080800080800080800080800080800080800080800080800080
            8000808000808000808000808000808000808000808000808000808000808000
            8080008080008080008080008080008080008080008080008080008080008080
            008080008080008080008080008080008080}
          NumGlyphs = 2
          TabOrder = 8
          OnClick = BitKilepClick
        end
        object CheckBox1: TCheckBox
          Left = 872
          Top = 8
          Width = 185
          Height = 17
          Caption = 'Csak az akt'#237'v dolgoz'#243'k'
          Checked = True
          State = cbChecked
          TabOrder = 9
          OnClick = CheckBox1Click
        end
        object DBEdit1: TDBEdit
          Left = 1192
          Top = 5
          Width = 49
          Height = 23
          AutoSize = False
          DataField = 'DO_SZABI'
          DataSource = DSDolgozo
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 10
          Visible = False
          OnExit = DBEdit1Exit
          OnKeyDown = DBEdit1KeyDown
        end
        object DBEdit2: TDBEdit
          Left = 887
          Top = 42
          Width = 114
          Height = 23
          AutoSize = False
          DataField = 'DO_ADOSZ'
          DataSource = DSDolgozo
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          ReadOnly = True
          TabOrder = 11
          OnExit = DBEdit1Exit
          OnKeyDown = DBEdit1KeyDown
        end
        object BTBerel: TBitBtn
          Left = 1005
          Top = 42
          Width = 108
          Height = 23
          HelpContext = 14706
          Caption = 'SZ felad'#225's'
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000010000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333000000
            000033333377777777773333330FFFFFFFF03FF3FF7FF33F3FF700300000FF0F
            00F077F777773F737737E00BFBFB0FFFFFF07773333F7F3333F7E0BFBF000FFF
            F0F077F3337773F3F737E0FBFBFBF0F00FF077F3333FF7F77F37E0BFBF00000B
            0FF077F3337777737337E0FBFBFBFBF0FFF077F33FFFFFF73337E0BF0000000F
            FFF077FF777777733FF7000BFB00B0FF00F07773FF77373377373330000B0FFF
            FFF03337777373333FF7333330B0FFFF00003333373733FF777733330B0FF00F
            0FF03333737F37737F373330B00FFFFF0F033337F77F33337F733309030FFFFF
            00333377737FFFFF773333303300000003333337337777777333}
          NumGlyphs = 2
          TabOrder = 12
          TabStop = False
          OnClick = BTBerelClick
        end
      end
      object Panel2: TPanel
        Left = 1044
        Top = 31
        Width = 208
        Height = 420
        Align = alRight
        Color = 8454143
        ParentBackground = False
        TabOrder = 1
        object DBGrid2: TDBGrid
          Left = 1
          Top = 1
          Width = 206
          Height = 418
          Align = alClient
          DataSource = DSDolgozo
          Options = [dgTitles, dgIndicator, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
          ReadOnly = True
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -16
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = [fsBold]
          OnDrawColumnCell = DBGrid2DrawColumnCell
          OnEnter = DBGrid2Enter
          Columns = <
            item
              Expanded = False
              FieldName = 'OSZAB2'
              Title.Caption = #214
              Width = 30
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'KSZAB2'
              Title.Caption = 'K'
              Width = 30
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'MSZAB2'
              Title.Caption = 'M'
              Width = 30
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'O_B'
              Title.Alignment = taCenter
              Title.Caption = 'B'
              Width = 30
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'O_EI'
              Title.Alignment = taCenter
              Title.Caption = 'EI'
              Width = 30
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'O_EL'
              Title.Alignment = taCenter
              Title.Caption = 'EL'
              Width = 30
              Visible = True
            end>
        end
      end
      object Panel4: TPanel
        Left = 4
        Top = 451
        Width = 1248
        Height = 104
        Align = alBottom
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 2
        DesignSize = (
          1248
          104)
        object Image2: TImage
          Left = 301
          Top = 9
          Width = 79
          Height = 93
          Hint = 'Klikkre nagy'#237't'#225's'
          Center = True
          ParentShowHint = False
          Proportional = True
          ShowHint = True
          OnClick = Image2Click
        end
        object Label6: TLabel
          Left = 1056
          Top = 54
          Width = 177
          Height = 24
          Alignment = taCenter
          AutoSize = False
          Caption = 'H'#243'nap'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -21
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object GroupBox1: TGroupBox
          Left = 1
          Top = 1
          Width = 300
          Height = 102
          Caption = 'GroupBox1'
          Color = 8454143
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
          TabOrder = 0
          object Label10: TLabel
            Left = 14
            Top = 22
            Width = 41
            Height = 13
            Caption = #214'sszes'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label11: TLabel
            Left = 7
            Top = 42
            Width = 48
            Height = 13
            Caption = #193'thozott'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label12: TLabel
            Left = 35
            Top = 82
            Width = 20
            Height = 13
            Caption = 'P'#243't'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label13: TLabel
            Left = 29
            Top = 62
            Width = 26
            Height = 13
            Caption = 'Alap'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label14: TLabel
            Left = 183
            Top = 22
            Width = 34
            Height = 13
            Caption = 'Kivett'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label15: TLabel
            Left = 167
            Top = 42
            Width = 50
            Height = 13
            Caption = 'Marad'#233'k'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Edit7: TEdit
            Left = 67
            Top = 20
            Width = 65
            Height = 18
            AutoSize = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -15
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            ReadOnly = True
            TabOrder = 0
            Text = 'Edit1'
          end
          object Edit8: TEdit
            Left = 67
            Top = 40
            Width = 65
            Height = 18
            AutoSize = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 1
            Text = 'Edit1'
          end
          object Edit9: TEdit
            Left = 67
            Top = 60
            Width = 65
            Height = 18
            AutoSize = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 2
            Text = 'Edit1'
          end
          object Edit10: TEdit
            Left = 67
            Top = 80
            Width = 65
            Height = 18
            Hint = 'Id'#337'ar'#225'nyos szabads'#225'g'
            AutoSize = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 3
            Text = 'Edit1'
          end
          object Edit11: TEdit
            Left = 227
            Top = 20
            Width = 65
            Height = 18
            AutoSize = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -15
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            ReadOnly = True
            TabOrder = 4
          end
          object Edit12: TEdit
            Left = 227
            Top = 40
            Width = 65
            Height = 18
            AutoSize = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -15
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            ReadOnly = True
            TabOrder = 5
            Text = 'Edit1'
          end
          object Button2: TButton
            Left = 227
            Top = 80
            Width = 65
            Height = 17
            Caption = 'Ment'#233's'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 6
            OnClick = Button2Click
          end
        end
        object GroupBox2: TGroupBox
          Left = 750
          Top = 1
          Width = 300
          Height = 102
          Anchors = [akTop, akRight]
          Caption = 'GroupBox2'
          Color = 8454143
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
          TabOrder = 1
          object Label7: TLabel
            Left = 19
            Top = 21
            Width = 133
            Height = 16
            Caption = #214'sszes szabads'#225'g'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label8: TLabel
            Left = 32
            Top = 41
            Width = 120
            Height = 16
            Caption = 'Kivett szabads'#225'g'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label9: TLabel
            Left = 9
            Top = 61
            Width = 143
            Height = 16
            Caption = 'Marad'#233'k szabads'#225'g'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Edit1: TEdit
            Left = 156
            Top = 20
            Width = 65
            Height = 18
            AutoSize = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            ReadOnly = True
            TabOrder = 0
            Text = 'Edit1'
          end
          object Edit2: TEdit
            Left = 156
            Top = 40
            Width = 65
            Height = 18
            AutoSize = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            ReadOnly = True
            TabOrder = 1
            Text = 'Edit1'
          end
          object Edit3: TEdit
            Left = 156
            Top = 60
            Width = 65
            Height = 18
            AutoSize = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            ReadOnly = True
            TabOrder = 2
            Text = 'Edit1'
          end
          object Edit4: TEdit
            Left = 228
            Top = 20
            Width = 65
            Height = 18
            AutoSize = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            ReadOnly = True
            TabOrder = 3
            Text = 'Edit1'
            Visible = False
          end
          object Edit5: TEdit
            Left = 228
            Top = 40
            Width = 65
            Height = 18
            AutoSize = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            ReadOnly = True
            TabOrder = 4
            Text = 'Edit1'
          end
          object Edit6: TEdit
            Left = 228
            Top = 60
            Width = 65
            Height = 18
            AutoSize = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            ReadOnly = True
            TabOrder = 5
            Text = 'Edit1'
          end
        end
        object Button1: TButton
          Left = 386
          Top = 6
          Width = 75
          Height = 25
          Caption = 'Button1'
          TabOrder = 2
          Visible = False
          OnClick = Button1Click
        end
        object Edit_EV: TEdit
          Left = 1113
          Top = 13
          Width = 73
          Height = 33
          AutoSize = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -27
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
          ReadOnly = True
          TabOrder = 3
          Text = '2000'
        end
        object UpDown11: TUpDown
          Left = 1064
          Top = 15
          Width = 17
          Height = 33
          Min = 2000
          Max = 2050
          Position = 2000
          TabOrder = 4
          Thousands = False
          Visible = False
        end
        object UpDown1: TUpDown
          Left = 1186
          Top = 13
          Width = 16
          Height = 33
          Associate = Edit_EV
          Min = 2000
          Max = 2050
          Position = 2000
          TabOrder = 5
          Thousands = False
          OnChangingEx = UpDown1ChangingEx
        end
      end
      object Panel5: TPanel
        Left = 4
        Top = 31
        Width = 1040
        Height = 420
        Align = alClient
        Caption = 'Panel5'
        TabOrder = 3
        object DBGrid1: TDBGrid
          Left = 1
          Top = 1
          Width = 1038
          Height = 418
          Align = alClient
          DataSource = DSDolgozo
          DrawingStyle = gdsGradient
          Options = [dgTitles, dgIndicator, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -16
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = [fsBold]
          OnColEnter = DBGrid1ColEnter
          OnDrawColumnCell = DBGrid1DrawColumnCell
          OnDblClick = DBGrid1DblClick
          OnKeyDown = DBGrid1KeyDown
          OnKeyPress = DBGrid1KeyPress
          Columns = <
            item
              Expanded = False
              FieldName = 'DO_NAME'
              ReadOnly = True
              Title.Caption = 'Dolgoz'#243' neve'
              Width = 195
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'NAP01'
              ReadOnly = True
              Width = 25
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'NAP02'
              ReadOnly = True
              Width = 25
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'NAP03'
              ReadOnly = True
              Width = 25
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'NAP04'
              ReadOnly = True
              Width = 25
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'NAP05'
              ReadOnly = True
              Width = 25
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'NAP06'
              ReadOnly = True
              Width = 25
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'NAP07'
              ReadOnly = True
              Width = 25
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'NAP08'
              ReadOnly = True
              Width = 25
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'NAP09'
              ReadOnly = True
              Width = 25
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'NAP10'
              ReadOnly = True
              Width = 25
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'NAP11'
              ReadOnly = True
              Width = 25
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'NAP12'
              ReadOnly = True
              Width = 25
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'NAP13'
              ReadOnly = True
              Width = 25
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'NAP14'
              ReadOnly = True
              Width = 25
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'NAP15'
              ReadOnly = True
              Width = 25
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'NAP16'
              ReadOnly = True
              Width = 25
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'NAP17'
              ReadOnly = True
              Width = 25
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'NAP18'
              ReadOnly = True
              Width = 25
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'NAP19'
              ReadOnly = True
              Width = 25
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'NAP20'
              ReadOnly = True
              Width = 25
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'NAP21'
              ReadOnly = True
              Width = 25
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'NAP22'
              ReadOnly = True
              Width = 25
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'NAP23'
              ReadOnly = True
              Width = 25
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'NAP24'
              ReadOnly = True
              Width = 25
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'NAP25'
              ReadOnly = True
              Width = 25
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'NAP26'
              ReadOnly = True
              Width = 25
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'NAP27'
              ReadOnly = True
              Width = 25
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'NAP28'
              ReadOnly = True
              Width = 25
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'NAP29'
              ReadOnly = True
              Width = 25
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'NAP30'
              ReadOnly = True
              Width = 25
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'NAP31'
              ReadOnly = True
              Width = 25
              Visible = True
            end>
        end
      end
    end
  end
  object TDolgozo: TADODataSet
    Connection = EgyebDlg.ADOConnectionKozos
    CursorType = ctStatic
    AfterOpen = TDolgozoAfterOpen
    AfterScroll = TDolgozoAfterScroll
    OnCalcFields = TDolgozoCalcFields
    CommandText = 
      'select DO_KOD, DO_NAME, DO_KULSO, DO_ARHIV , DO_KSZAB, DO_SZABI,' +
      ' DO_BERKOD, DO_CSKOD, DO_CSNEV, DO_ADOSZ, s.*'#13#10'from DOLGOZO, SZA' +
      'BADSAG s'#13#10'where DO_KULSO=0 and DO_ARHIV=0 and DO_KOD=SB_DOKOD'#13#10'o' +
      'rder by DO_NAME'
    Parameters = <>
    Left = 136
    Top = 24
    object TDolgozoDO_KOD: TStringField
      FieldName = 'DO_KOD'
      Size = 5
    end
    object TDolgozoDO_NAME: TStringField
      FieldName = 'DO_NAME'
      Size = 45
    end
    object TDolgozoDO_KULSO: TIntegerField
      FieldName = 'DO_KULSO'
    end
    object TDolgozoDO_ARHIV: TIntegerField
      FieldName = 'DO_ARHIV'
    end
    object TDolgozoDO_SZABI: TIntegerField
      FieldName = 'DO_SZABI'
    end
    object TDolgozoDO_BERKOD: TStringField
      FieldName = 'DO_BERKOD'
      Size = 8
    end
    object TDolgozoHO: TStringField
      FieldKind = fkLookup
      FieldName = 'HO'
      LookupDataSet = TTavollet
      LookupKeyFields = 'TA_DOKOD'
      LookupResultField = 'TA_HONAP'
      KeyFields = 'DO_KOD'
      Lookup = True
    end
    object TDolgozoN01: TStringField
      FieldKind = fkLookup
      FieldName = 'NAP01'
      LookupDataSet = TTavollet
      LookupKeyFields = 'TA_DOKOD'
      LookupResultField = 'TA_NAP01'
      KeyFields = 'DO_KOD'
      Lookup = True
    end
    object TDolgozoN02: TStringField
      FieldKind = fkLookup
      FieldName = 'NAP02'
      LookupDataSet = TTavollet
      LookupKeyFields = 'TA_DOKOD'
      LookupResultField = 'TA_NAP02'
      KeyFields = 'DO_KOD'
      Lookup = True
    end
    object TDolgozoN03: TStringField
      FieldKind = fkLookup
      FieldName = 'NAP03'
      LookupDataSet = TTavollet
      LookupKeyFields = 'TA_DOKOD'
      LookupResultField = 'TA_NAP03'
      KeyFields = 'DO_KOD'
      Lookup = True
    end
    object TDolgozoNAP04: TStringField
      FieldKind = fkLookup
      FieldName = 'NAP04'
      LookupDataSet = TTavollet
      LookupKeyFields = 'TA_DOKOD'
      LookupResultField = 'TA_NAP04'
      KeyFields = 'DO_KOD'
      Lookup = True
    end
    object TDolgozoNAP05: TStringField
      FieldKind = fkLookup
      FieldName = 'NAP05'
      LookupDataSet = TTavollet
      LookupKeyFields = 'TA_DOKOD'
      LookupResultField = 'TA_NAP05'
      KeyFields = 'DO_KOD'
      Lookup = True
    end
    object TDolgozoNAP06: TStringField
      FieldKind = fkLookup
      FieldName = 'NAP06'
      LookupDataSet = TTavollet
      LookupKeyFields = 'TA_DOKOD'
      LookupResultField = 'TA_NAP06'
      KeyFields = 'DO_KOD'
      Lookup = True
    end
    object TDolgozoNAP07: TStringField
      FieldKind = fkLookup
      FieldName = 'NAP07'
      LookupDataSet = TTavollet
      LookupKeyFields = 'TA_DOKOD'
      LookupResultField = 'TA_NAP07'
      KeyFields = 'DO_KOD'
      Lookup = True
    end
    object TDolgozoNAP08: TStringField
      FieldKind = fkLookup
      FieldName = 'NAP08'
      LookupDataSet = TTavollet
      LookupKeyFields = 'TA_DOKOD'
      LookupResultField = 'TA_NAP08'
      KeyFields = 'DO_KOD'
      Lookup = True
    end
    object TDolgozoNAP09: TStringField
      FieldKind = fkLookup
      FieldName = 'NAP09'
      LookupDataSet = TTavollet
      LookupKeyFields = 'TA_DOKOD'
      LookupResultField = 'TA_NAP09'
      KeyFields = 'DO_KOD'
      Lookup = True
    end
    object TDolgozoNAP10: TStringField
      FieldKind = fkLookup
      FieldName = 'NAP10'
      LookupDataSet = TTavollet
      LookupKeyFields = 'TA_DOKOD'
      LookupResultField = 'TA_NAP10'
      KeyFields = 'DO_KOD'
      Lookup = True
    end
    object TDolgozoNAP11: TStringField
      FieldKind = fkLookup
      FieldName = 'NAP11'
      LookupDataSet = TTavollet
      LookupKeyFields = 'TA_DOKOD'
      LookupResultField = 'TA_NAP11'
      KeyFields = 'DO_KOD'
      Lookup = True
    end
    object TDolgozoNAP12: TStringField
      FieldKind = fkLookup
      FieldName = 'NAP12'
      LookupDataSet = TTavollet
      LookupKeyFields = 'TA_DOKOD'
      LookupResultField = 'TA_NAP12'
      KeyFields = 'DO_KOD'
      Lookup = True
    end
    object TDolgozoNAP13: TStringField
      FieldKind = fkLookup
      FieldName = 'NAP13'
      LookupDataSet = TTavollet
      LookupKeyFields = 'TA_DOKOD'
      LookupResultField = 'TA_NAP13'
      KeyFields = 'DO_KOD'
      Lookup = True
    end
    object TDolgozoNAP14: TStringField
      FieldKind = fkLookup
      FieldName = 'NAP14'
      LookupDataSet = TTavollet
      LookupKeyFields = 'TA_DOKOD'
      LookupResultField = 'TA_NAP14'
      KeyFields = 'DO_KOD'
      Lookup = True
    end
    object TDolgozoNAP15: TStringField
      FieldKind = fkLookup
      FieldName = 'NAP15'
      LookupDataSet = TTavollet
      LookupKeyFields = 'TA_DOKOD'
      LookupResultField = 'TA_NAP15'
      KeyFields = 'DO_KOD'
      Lookup = True
    end
    object TDolgozoNAP16: TStringField
      FieldKind = fkLookup
      FieldName = 'NAP16'
      LookupDataSet = TTavollet
      LookupKeyFields = 'TA_DOKOD'
      LookupResultField = 'TA_NAP16'
      KeyFields = 'DO_KOD'
      Lookup = True
    end
    object TDolgozoNAP17: TStringField
      FieldKind = fkLookup
      FieldName = 'NAP17'
      LookupDataSet = TTavollet
      LookupKeyFields = 'TA_DOKOD'
      LookupResultField = 'TA_NAP17'
      KeyFields = 'DO_KOD'
      Lookup = True
    end
    object TDolgozoNAP18: TStringField
      FieldKind = fkLookup
      FieldName = 'NAP18'
      LookupDataSet = TTavollet
      LookupKeyFields = 'TA_DOKOD'
      LookupResultField = 'TA_NAP18'
      KeyFields = 'DO_KOD'
      Lookup = True
    end
    object TDolgozoNAP19: TStringField
      FieldKind = fkLookup
      FieldName = 'NAP19'
      LookupDataSet = TTavollet
      LookupKeyFields = 'TA_DOKOD'
      LookupResultField = 'TA_NAP19'
      KeyFields = 'DO_KOD'
      Lookup = True
    end
    object TDolgozoNAP20: TStringField
      FieldKind = fkLookup
      FieldName = 'NAP20'
      LookupDataSet = TTavollet
      LookupKeyFields = 'TA_DOKOD'
      LookupResultField = 'TA_NAP20'
      KeyFields = 'DO_KOD'
      Lookup = True
    end
    object TDolgozoNAP21: TStringField
      FieldKind = fkLookup
      FieldName = 'NAP21'
      LookupDataSet = TTavollet
      LookupKeyFields = 'TA_DOKOD'
      LookupResultField = 'TA_NAP21'
      KeyFields = 'DO_KOD'
      Lookup = True
    end
    object TDolgozoNAP22: TStringField
      FieldKind = fkLookup
      FieldName = 'NAP22'
      LookupDataSet = TTavollet
      LookupKeyFields = 'TA_DOKOD'
      LookupResultField = 'TA_NAP22'
      KeyFields = 'DO_KOD'
      Lookup = True
    end
    object TDolgozoNAP23: TStringField
      FieldKind = fkLookup
      FieldName = 'NAP23'
      LookupDataSet = TTavollet
      LookupKeyFields = 'TA_DOKOD'
      LookupResultField = 'TA_NAP23'
      KeyFields = 'DO_KOD'
      Lookup = True
    end
    object TDolgozoNAP24: TStringField
      FieldKind = fkLookup
      FieldName = 'NAP24'
      LookupDataSet = TTavollet
      LookupKeyFields = 'TA_DOKOD'
      LookupResultField = 'TA_NAP24'
      KeyFields = 'DO_KOD'
      Lookup = True
    end
    object TDolgozoNAP25: TStringField
      FieldKind = fkLookup
      FieldName = 'NAP25'
      LookupDataSet = TTavollet
      LookupKeyFields = 'TA_DOKOD'
      LookupResultField = 'TA_NAP25'
      KeyFields = 'DO_KOD'
      Lookup = True
    end
    object TDolgozoNAP26: TStringField
      FieldKind = fkLookup
      FieldName = 'NAP26'
      LookupDataSet = TTavollet
      LookupKeyFields = 'TA_DOKOD'
      LookupResultField = 'TA_NAP26'
      KeyFields = 'DO_KOD'
      Lookup = True
    end
    object TDolgozoNAP27: TStringField
      FieldKind = fkLookup
      FieldName = 'NAP27'
      LookupDataSet = TTavollet
      LookupKeyFields = 'TA_DOKOD'
      LookupResultField = 'TA_NAP27'
      KeyFields = 'DO_KOD'
      Lookup = True
    end
    object TDolgozoNAP28: TStringField
      FieldKind = fkLookup
      FieldName = 'NAP28'
      LookupDataSet = TTavollet
      LookupKeyFields = 'TA_DOKOD'
      LookupResultField = 'TA_NAP28'
      KeyFields = 'DO_KOD'
      Lookup = True
    end
    object TDolgozoNAP29: TStringField
      FieldKind = fkLookup
      FieldName = 'NAP29'
      LookupDataSet = TTavollet
      LookupKeyFields = 'TA_DOKOD'
      LookupResultField = 'TA_NAP29'
      KeyFields = 'DO_KOD'
      Lookup = True
    end
    object TDolgozoNAP30: TStringField
      FieldKind = fkLookup
      FieldName = 'NAP30'
      LookupDataSet = TTavollet
      LookupKeyFields = 'TA_DOKOD'
      LookupResultField = 'TA_NAP30'
      KeyFields = 'DO_KOD'
      Lookup = True
    end
    object TDolgozoNAP31: TStringField
      FieldKind = fkLookup
      FieldName = 'NAP31'
      LookupDataSet = TTavollet
      LookupKeyFields = 'TA_DOKOD'
      LookupResultField = 'TA_NAP31'
      KeyFields = 'DO_KOD'
      Lookup = True
    end
    object TDolgozoO_X: TIntegerField
      FieldKind = fkLookup
      FieldName = 'O_X'
      LookupDataSet = TTavollet
      LookupKeyFields = 'TA_DOKOD'
      LookupResultField = 'TA_O_X'
      KeyFields = 'DO_KOD'
      Lookup = True
    end
    object TDolgozoO_SZ: TIntegerField
      FieldKind = fkLookup
      FieldName = 'O_SZ'
      LookupDataSet = TTavollet
      LookupKeyFields = 'TA_DOKOD'
      LookupResultField = 'TA_O_SZ'
      KeyFields = 'DO_KOD'
      Lookup = True
    end
    object TDolgozoO_B: TIntegerField
      FieldKind = fkLookup
      FieldName = 'O_B'
      LookupDataSet = TTavollet
      LookupKeyFields = 'TA_DOKOD'
      LookupResultField = 'TA_O_B'
      KeyFields = 'DO_KOD'
      Lookup = True
    end
    object TDolgozoO_EI: TIntegerField
      FieldKind = fkLookup
      FieldName = 'O_EI'
      LookupDataSet = TTavollet
      LookupKeyFields = 'TA_DOKOD'
      LookupResultField = 'TA_O_EI'
      KeyFields = 'DO_KOD'
      Lookup = True
    end
    object TDolgozoO_EL: TIntegerField
      FieldKind = fkLookup
      FieldName = 'O_EL'
      LookupDataSet = TTavollet
      LookupKeyFields = 'TA_DOKOD'
      LookupResultField = 'TA_O_EL'
      KeyFields = 'DO_KOD'
      Lookup = True
    end
    object TDolgozoKSZAB: TIntegerField
      FieldKind = fkLookup
      FieldName = 'KSZAB'
      LookupDataSet = T_Dolgozo
      LookupKeyFields = 'DO_KOD'
      LookupResultField = 'DO_KSZAB'
      KeyFields = 'DO_KOD'
      Lookup = True
    end
    object TDolgozoMSZAB: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'MSZAB'
      Calculated = True
    end
    object TDolgozoBERKOD: TStringField
      FieldKind = fkLookup
      FieldName = 'BERKOD'
      LookupDataSet = TTavollet
      LookupKeyFields = 'TA_DOKOD'
      LookupResultField = 'TA_BERKOD'
      KeyFields = 'DO_KOD'
      Lookup = True
    end
    object TDolgozoDO_KSZAB: TIntegerField
      FieldName = 'DO_KSZAB'
    end
    object TDolgozoDO_CSKOD: TStringField
      FieldName = 'DO_CSKOD'
      Size = 3
    end
    object TDolgozoDO_CSNEV: TStringField
      FieldName = 'DO_CSNEV'
      Size = 40
    end
    object TDolgozoDO_ADOSZ: TStringField
      FieldName = 'DO_ADOSZ'
      OnValidate = TDolgozoDO_ADOSZValidate
      Size = 10
    end
    object TDolgozoSZABIARAMY: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'SZABIARAMY'
      Calculated = True
    end
    object TDolgozoOSZAB2: TIntegerField
      FieldKind = fkLookup
      FieldName = 'OSZAB2'
      LookupDataSet = SZABI
      LookupKeyFields = 'SB_DOKOD'
      LookupResultField = 'SB_OSZAB'
      KeyFields = 'DO_KOD'
      Lookup = True
    end
    object TDolgozoKSZAB2: TIntegerField
      FieldKind = fkLookup
      FieldName = 'KSZAB2'
      LookupDataSet = SZABI
      LookupKeyFields = 'SB_DOKOD'
      LookupResultField = 'SB_KSZAB'
      KeyFields = 'DO_KOD'
      Lookup = True
    end
    object TDolgozoMSZAB2: TIntegerField
      FieldKind = fkLookup
      FieldName = 'MSZAB2'
      LookupDataSet = SZABI
      LookupKeyFields = 'SB_DOKOD'
      LookupResultField = 'SB_MSZAB'
      KeyFields = 'DO_KOD'
      Lookup = True
    end
    object TDolgozoESZAB2: TIntegerField
      FieldKind = fkLookup
      FieldName = 'ESZAB2'
      LookupDataSet = SZABI
      LookupKeyFields = 'SB_DOKOD'
      LookupResultField = 'SB_ESZAB'
      KeyFields = 'DO_KOD'
      Lookup = True
    end
    object TDolgozoASZAB2: TIntegerField
      FieldKind = fkLookup
      FieldName = 'ASZAB2'
      LookupDataSet = SZABI
      LookupKeyFields = 'SB_DOKOD'
      LookupResultField = 'SB_SATHO'
      KeyFields = 'DO_KOD'
      Lookup = True
    end
    object TDolgozoSZABI2: TIntegerField
      FieldKind = fkLookup
      FieldName = 'SZABI2'
      LookupDataSet = SZABI
      LookupKeyFields = 'SB_DOKOD'
      LookupResultField = 'SB_SZABI'
      KeyFields = 'DO_KOD'
      Lookup = True
    end
  end
  object TTavollet: TADODataSet
    Tag = 1
    Connection = EgyebDlg.ADOConnectionAlap
    CursorType = ctStatic
    BeforePost = TTavolletBeforePost
    AfterPost = TTavolletAfterPost
    CommandText = 'select * from TAVOLLET'#13#10'where TA_EV= :EV and TA_HONAP= :HO'
    Parameters = <
      item
        Name = 'EV'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 4
        Value = Null
      end
      item
        Name = 'HO'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end>
    Left = 192
    Top = 32
    object TTavolletTA_DOKOD: TStringField
      FieldName = 'TA_DOKOD'
      Size = 5
    end
    object TTavolletTA_DONEV: TStringField
      FieldName = 'TA_DONEV'
      Size = 45
    end
    object TTavolletTA_HONAP: TStringField
      FieldName = 'TA_HONAP'
      FixedChar = True
      Size = 2
    end
    object TTavolletTA_NAP01: TStringField
      FieldName = 'TA_NAP01'
      Size = 10
    end
    object TTavolletTA_NAP02: TStringField
      FieldName = 'TA_NAP02'
      Size = 10
    end
    object TTavolletTA_NAP03: TStringField
      FieldName = 'TA_NAP03'
      Size = 10
    end
    object TTavolletTA_NAP04: TStringField
      FieldName = 'TA_NAP04'
      Size = 10
    end
    object TTavolletTA_NAP05: TStringField
      FieldName = 'TA_NAP05'
      Size = 10
    end
    object TTavolletTA_NAP06: TStringField
      FieldName = 'TA_NAP06'
      Size = 10
    end
    object TTavolletTA_NAP07: TStringField
      FieldName = 'TA_NAP07'
      Size = 10
    end
    object TTavolletTA_NAP08: TStringField
      FieldName = 'TA_NAP08'
      Size = 10
    end
    object TTavolletTA_NAP09: TStringField
      FieldName = 'TA_NAP09'
      Size = 10
    end
    object TTavolletTA_NAP10: TStringField
      FieldName = 'TA_NAP10'
      Size = 10
    end
    object TTavolletTA_NAP11: TStringField
      FieldName = 'TA_NAP11'
      Size = 10
    end
    object TTavolletTA_NAP12: TStringField
      FieldName = 'TA_NAP12'
      Size = 10
    end
    object TTavolletTA_NAP13: TStringField
      FieldName = 'TA_NAP13'
      Size = 10
    end
    object TTavolletTA_NAP14: TStringField
      FieldName = 'TA_NAP14'
      Size = 10
    end
    object TTavolletTA_NAP15: TStringField
      FieldName = 'TA_NAP15'
      Size = 10
    end
    object TTavolletTA_NAP16: TStringField
      FieldName = 'TA_NAP16'
      Size = 10
    end
    object TTavolletTA_NAP17: TStringField
      FieldName = 'TA_NAP17'
      Size = 10
    end
    object TTavolletTA_NAP18: TStringField
      FieldName = 'TA_NAP18'
      Size = 10
    end
    object TTavolletTA_NAP19: TStringField
      FieldName = 'TA_NAP19'
      Size = 10
    end
    object TTavolletTA_NAP20: TStringField
      FieldName = 'TA_NAP20'
      Size = 10
    end
    object TTavolletTA_NAP21: TStringField
      FieldName = 'TA_NAP21'
      Size = 10
    end
    object TTavolletTA_NAP22: TStringField
      FieldName = 'TA_NAP22'
      Size = 10
    end
    object TTavolletTA_NAP23: TStringField
      FieldName = 'TA_NAP23'
      Size = 10
    end
    object TTavolletTA_NAP24: TStringField
      FieldName = 'TA_NAP24'
      Size = 10
    end
    object TTavolletTA_NAP25: TStringField
      FieldName = 'TA_NAP25'
      Size = 10
    end
    object TTavolletTA_NAP26: TStringField
      FieldName = 'TA_NAP26'
      Size = 10
    end
    object TTavolletTA_NAP27: TStringField
      FieldName = 'TA_NAP27'
      Size = 10
    end
    object TTavolletTA_NAP28: TStringField
      FieldName = 'TA_NAP28'
      Size = 10
    end
    object TTavolletTA_NAP29: TStringField
      FieldName = 'TA_NAP29'
      Size = 10
    end
    object TTavolletTA_NAP30: TStringField
      FieldName = 'TA_NAP30'
      Size = 10
    end
    object TTavolletTA_NAP31: TStringField
      FieldName = 'TA_NAP31'
      Size = 10
    end
    object TTavolletTA_AKTIV: TIntegerField
      FieldName = 'TA_AKTIV'
    end
    object TTavolletTA_O_X: TIntegerField
      FieldName = 'TA_O_X'
    end
    object TTavolletTA_O_SZ: TIntegerField
      FieldName = 'TA_O_SZ'
    end
    object TTavolletTA_O_B: TIntegerField
      FieldName = 'TA_O_B'
    end
    object TTavolletTA_O_EI: TIntegerField
      FieldName = 'TA_O_EI'
    end
    object TTavolletTA_O_EL: TIntegerField
      FieldName = 'TA_O_EL'
    end
    object TTavolletKSZAB: TIntegerField
      FieldKind = fkLookup
      FieldName = 'KSZAB'
      LookupDataSet = T_Dolgozo
      LookupKeyFields = 'DO_KOD'
      LookupResultField = 'DO_KSZAB'
      KeyFields = 'TA_DOKOD'
      Lookup = True
    end
    object TTavolletTA_BERKOD: TStringField
      FieldName = 'TA_BERKOD'
      Size = 8
    end
    object TTavolletTA_CSKOD: TStringField
      FieldName = 'TA_CSKOD'
      Size = 3
    end
    object TTavolletTA_CSNEV: TStringField
      FieldName = 'TA_CSNEV'
      Size = 40
    end
    object TTavolletTA_EV: TStringField
      FieldName = 'TA_EV'
      FixedChar = True
      Size = 4
    end
  end
  object DSDolgozo: TDataSource
    DataSet = TDolgozo
    OnDataChange = DSDolgozoDataChange
    Left = 152
    Top = 64
  end
  object T_Tavollet: TADOQuery
    Connection = EgyebDlg.ADOConnectionAlap
    Parameters = <
      item
        Name = 'KOD'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 5
        Value = Null
      end
      item
        Name = 'EV'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 4
        Value = Null
      end
      item
        Name = 'HO'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end>
    SQL.Strings = (
      'select TA_HONAP from TAVOLLET'
      'where  (TA_DOKOD= :KOD) and (TA_EV= :EV) and (TA_HONAP= :HO)')
    Left = 200
    Top = 144
  end
  object Timer1: TTimer
    Enabled = False
    Interval = 2000
    OnTimer = Timer1Timer
    Left = 148
    Top = 107
  end
  object Timer2: TTimer
    Enabled = False
    Interval = 400
    OnTimer = Timer2Timer
    Left = 196
    Top = 107
  end
  object T_Dolgozo: TADODataSet
    Connection = EgyebDlg.ADOConnectionKozos
    CursorType = ctStatic
    CommandText = 
      'select DO_KOD, DO_NAME, DO_KULSO, DO_ARHIV , DO_KSZAB, DO_SZABI,' +
      ' DO_BERKOD'#13#10'from DOLGOZO'#13#10'where DO_KULSO=0 and DO_ARHIV=0'#13#10
    Parameters = <>
    Left = 152
    Top = 144
    object T_DolgozoDO_KOD: TStringField
      FieldName = 'DO_KOD'
      Size = 5
    end
    object T_DolgozoDO_NAME: TStringField
      FieldName = 'DO_NAME'
      Size = 45
    end
    object T_DolgozoDO_KULSO: TIntegerField
      FieldName = 'DO_KULSO'
    end
    object T_DolgozoDO_ARHIV: TIntegerField
      FieldName = 'DO_ARHIV'
    end
    object T_DolgozoDO_KSZAB: TIntegerField
      FieldName = 'DO_KSZAB'
    end
    object T_DolgozoDO_SZABI: TIntegerField
      FieldName = 'DO_SZABI'
    end
    object T_DolgozoDO_BERKOD: TStringField
      FieldName = 'DO_BERKOD'
      Size = 8
    end
  end
  object DSTavollet: TDataSource
    DataSet = TTavollet
    Left = 200
    Top = 64
  end
  object TDCsop: TADOQuery
    Connection = EgyebDlg.ADOConnectionKozos
    Parameters = <
      item
        Name = 'ID'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 5
        Value = Null
      end
      item
        Name = 'DAT'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 11
        Value = Null
      end>
    SQL.Strings = (
      'select * from dolgozocsop'
      'where  (dc_dokod= :ID) and (dc_dattol<= :DAT)'
      'order by dc_dattol desc')
    Left = 260
    Top = 143
    object TDCsopDC_DOKOD: TStringField
      FieldName = 'DC_DOKOD'
      Size = 5
    end
    object TDCsopDC_DONEV: TStringField
      FieldName = 'DC_DONEV'
      Size = 45
    end
    object TDCsopDC_DATTOL: TStringField
      FieldName = 'DC_DATTOL'
      Size = 11
    end
    object TDCsopDC_CSKOD: TStringField
      FieldName = 'DC_CSKOD'
      Size = 3
    end
    object TDCsopDC_CSNEV: TStringField
      FieldName = 'DC_CSNEV'
      Size = 40
    end
  end
  object TJOGOK: TADOQuery
    Connection = EgyebDlg.ADOConnectionKozos
    Parameters = <>
    SQL.Strings = (
      'select * from szotar')
    Left = 88
    Top = 190
  end
  object TDFOTO: TADOQuery
    Connection = EgyebDlg.ADOConnectionKozos
    Parameters = <
      item
        Name = 'ID'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 5
        Value = Null
      end>
    SQL.Strings = (
      'select * from dolgozofoto'
      'where  (do_kod= :ID)')
    Left = 324
    Top = 159
    object TDFOTODO_KOD: TStringField
      FieldName = 'DO_KOD'
      Size = 5
    end
    object TDFOTODO_FOTO: TBlobField
      FieldName = 'DO_FOTO'
    end
  end
  object TNapok: TADOQuery
    Connection = EgyebDlg.ADOConnectionKozos
    Parameters = <>
    SQL.Strings = (
      'select * from napok'
      'order by na_datum')
    Left = 280
    Top = 222
    object TNapokNA_ORSZA: TStringField
      FieldName = 'NA_ORSZA'
      Size = 3
    end
    object TNapokNA_DATUM: TDateTimeField
      FieldName = 'NA_DATUM'
    end
    object TNapokNA_UNNEP: TIntegerField
      FieldName = 'NA_UNNEP'
    end
    object TNapokNA_MEGJE: TStringField
      FieldName = 'NA_MEGJE'
      Size = 100
    end
    object TNapokNA_EV: TIntegerField
      FieldName = 'NA_EV'
    end
    object TNapokNA_HO: TIntegerField
      FieldName = 'NA_HO'
    end
  end
  object SZABI: TADODataSet
    Connection = EgyebDlg.ADOConnectionKozos
    CursorType = ctStatic
    BeforeOpen = SZABIBeforeOpen
    CommandText = 'select * from SZABADSAG'#13#10'where SB_EV= :EV'
    Parameters = <
      item
        Name = 'EV'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 4
        Value = Null
      end>
    Left = 96
    Top = 110
    object SZABISB_DOKOD: TStringField
      FieldName = 'SB_DOKOD'
      Size = 5
    end
    object SZABISB_EV: TStringField
      FieldName = 'SB_EV'
      FixedChar = True
      Size = 4
    end
    object SZABISB_SZABI: TIntegerField
      FieldName = 'SB_SZABI'
    end
    object SZABISB_ATHOZ: TIntegerField
      FieldName = 'SB_ATHOZ'
    end
    object SZABISB_KSZAB: TIntegerField
      FieldName = 'SB_KSZAB'
    end
    object SZABISB_MSZAB: TIntegerField
      FieldName = 'SB_MSZAB'
      ReadOnly = True
    end
    object SZABISB_OSZAB: TIntegerField
      FieldName = 'SB_OSZAB'
      ReadOnly = True
    end
    object SZABISB_ESZAB: TIntegerField
      FieldName = 'SB_ESZAB'
    end
    object SZABISB_SATHO: TIntegerField
      FieldName = 'SB_SATHO'
    end
  end
  object kiszabi: TADOQuery
    Connection = EgyebDlg.ADOConnectionAlap
    Parameters = <
      item
        Name = 'KOD'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 5
        Value = Null
      end
      item
        Name = 'EV'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'select * from tavollet '
      'where ta_dokod= :KOD and ta_ev= :EV')
    Left = 80
    Top = 262
  end
  object ADOCommand1: TADOCommand
    CommandText = 'select  from SZABADSAG'
    Connection = EgyebDlg.ADOConnectionKozos
    Parameters = <>
    Left = 169
    Top = 551
  end
end
