unit J_ALFORM;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs;

type
  TJ_AlformDlg = class(TForm)
	procedure Tolto(cim : string; kod : string);virtual;abstract;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
		ret_kod		: string;
		megtekint	: boolean;
  end;

var
  J_AlformDlg: TJ_AlformDlg;

implementation

{$R *.dfm}

procedure TJ_AlformDlg.FormCreate(Sender: TObject);
begin
	megtekint	:= false;
end;

end.
