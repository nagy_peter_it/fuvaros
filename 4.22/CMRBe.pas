unit Cmrbe;

interface

uses
	SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
	Forms, Dialogs, StdCtrls, Buttons, Mask, DB, DBTables, J_ALFORM, ADODB,
	ExtCtrls;

type
	TCmrBeDlg = class(TJ_AlformDlg)
	BitElkuld: TBitBtn;
	BitKilep: TBitBtn;
    Query1: TADOQuery;
    Label5: TLabel;
    Label6: TLabel;
    Label4: TLabel;
    M7: TMaskEdit;
    M9: TMaskEdit;
    BitBtn9: TBitBtn;
    BitBtn10: TBitBtn;
    M6: TMaskEdit;
    BitBtn2: TBitBtn;
    M5: TMaskEdit;
	 M11: TMaskEdit;
	 Label1: TLabel;
	 MaskEdit1: TMaskEdit;
	 Label8: TLabel;
	 Label9: TLabel;
	 MaskEdit2: TMaskEdit;
	 Label3: TLabel;
	 MaskEdit3: TMaskEdit;
	 Label10: TLabel;
	 MaskEdit4: TMaskEdit;
	 MaskEdit5: TMaskEdit;
	 BitBtn1: TBitBtn;
    CB1: TCheckBox;
    Label2: TLabel;
    MaskEdit6: TMaskEdit;
    BitBtn3: TBitBtn;
    BitBtn32: TBitBtn;
    BitBtn4: TBitBtn;
	procedure BitKilepClick(Sender: TObject);
	procedure FormCreate(Sender: TObject);
	procedure Tolto(cim : string; teko : string); override;
	procedure BitElkuldClick(Sender: TObject);
  procedure MaskEditClick(Sender: TObject);
	 procedure FormKeyDown(Sender: TObject; var Key: Word;
	   Shift: TShiftState);
	 procedure M3KeyPress(Sender: TObject; var Key: Char);
	 procedure BitBtn2Click(Sender: TObject);
	 procedure M24AExit(Sender: TObject);
	 procedure BitBtn9Click(Sender: TObject);
	 procedure BitBtn10Click(Sender: TObject);
	 procedure M9Exit(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure BitBtn32Click(Sender: TObject);
    procedure BitBtn4Click(Sender: TObject);
	private
	public
	end;

var
	CmrBeDlg: TCmrBeDlg;

implementation

uses
	J_VALASZTO, Egyeb, J_SQL, Kozos,VevoUjfm;
{$R *.DFM}

procedure TCmrBeDlg.BitKilepClick(Sender: TObject);
begin
	ret_kod := '';
	Close;
end;

procedure TCmrBeDlg.FormCreate(Sender: TObject);
begin
	EgyebDlg.SetADOQueryDatabase(Query1);
	SetMaskEdits([M5, M6, MaskEdit1, MaskEdit3, MaskEdit4, Maskedit6]);
	ret_kod 	:= '';
end;

procedure TCmrBeDlg.Tolto(cim : string; teko : string);
begin
	ret_kod		:= teko;
	Caption  	:= cim;
	M5.Text		:= teko;
	// Az adatok beolvas�sa
	Query_Run (Query1, 'SELECT * FROM CMRDAT WHERE CM_CMKOD = '+IntToStr(StrToIntDef(ret_kod, 0)),true);
	if Query1.RecordCount > 0 then begin
		CB1.Checked 	:= false;
		if StrToIntdef(Query1.FieldByName('CM_MASIK').AsString, 0) > 0 then begin
			CB1.Checked 	:= true;
		end;
		MaskEdit1.Text	:= Query1.FieldByName('CM_CSZAM').AsString;
		MaskEdit3.Text	:= Query1.FieldByName('CM_CRDAT').AsString;
		MaskEdit2.Text	:= Query1.FieldByName('CM_DOKOD').AsString;
		M7.Text 		:= Query1.FieldByname('CM_KIDAT').AsString;
		M9.Text 		:= Query1.FieldByname('CM_VIDAT').AsString;
		MaskEdit5.Text	:= Query1.FieldByName('CM_JAKOD').AsString;
		M11.Text 		:= Query1.FieldByname('CM_MEGJE').AsString;
    if MaskEdit2.Text='' then
  		MaskEdit6.Text			:= Query1.FieldByName('CM_DONEV').AsString
    else
  		M6.Text			:= Query1.FieldByName('CM_DONEV').AsString;
		MaskEdit4.Text	:= GetJaratszam(Query1.FieldByName('CM_JAKOD').AsString);
	end;
end;

procedure TCmrBeDlg.BitElkuldClick(Sender: TObject);
var
  nev:string;
begin
	{A r�gi rekord m�dos�t�sa}

  nev:=M6.Text;
  if MaskEdit6.Text<>'' then
  begin
    nev:=MaskEdit6.Text;
    MaskEdit2.Text:='';
  end;
	Query_Update (Query1, 'CMRDAT',
		[
		'CM_DOKOD', ''''+MaskEdit2.Text+'''',
		'CM_DONEV', ''''+M6.Text+'''',
		'CM_KIDAT', ''''+M7.Text+'''',
		'CM_VIDAT', ''''+M9.Text+'''',
		'CM_JAKOD', ''''+MaskEdit5.Text+'''',
		'CM_JARAT', ''''+MaskEdit4.Text+'''',
		'CM_MEGJE', ''''+M11.Text+''''
		],' WHERE CM_CMKOD = '+ret_kod);
	Close;
end;

procedure TCmrBeDlg.MaskEditClick(Sender: TObject);
begin
	with Sender as TMaskEdit do begin
		SelectAll;
	end;
end;

procedure TCmrBeDlg.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
	EgyebDlg.FormReturn(Key);
end;

procedure TCmrBeDlg.M3KeyPress(Sender: TObject; var Key: Char);
begin
  with Sender as TMaskEdit do begin
		Key := EgyebDlg.Jochar(Text,Key,(Sender as TMaskEdit).MaxLength,(Sender as TMaskEdit).Tag);
  end;
end;

procedure TCmrBeDlg.BitBtn2Click(Sender: TObject);
begin
	DolgozoValaszto(MaskEdit2, M6);
	M7.SetFocus;
end;

procedure TCmrBeDlg.M24AExit(Sender: TObject);
begin
	SzamExit(Sender);
end;

procedure TCmrBeDlg.BitBtn9Click(Sender: TObject);
begin
	EgyebDlg.Calendarbe(M7);
end;

procedure TCmrBeDlg.BitBtn10Click(Sender: TObject);
begin
	EgyebDlg.Calendarbe(M9);
end;

procedure TCmrBeDlg.M9Exit(Sender: TObject);
begin
	DatumExit(Sender, false);
end;

procedure TCmrBeDlg.BitBtn1Click(Sender: TObject);
begin
	// J�rat v�laszt�s
	JaratValaszto(MaskEdit5, MaskEdit4 );
end;

procedure TCmrBeDlg.BitBtn3Click(Sender: TObject);
begin
	{A megb�z� beolvas�sa}
	Application.CreateForm(TVevoUjFmDlg, VevoUjFmDlg);
	VevoUjFmDlg.valaszt := true;
	VevoUjFmDlg.ShowModal;
	if VevoUjFmDlg.ret_vkod <> '' then begin
		if VevoUjFmDlg.felista > 0 then begin
			if NoticeKi('Feketelist�s vev�! Folytatja?', NOT_QUESTION) <> 0 then begin
				VevoUjFmDlg.Destroy;
				Exit;
			end;
		end;
		MaskEdit6.Text 	:= VevoUjFmDlg.ret_vnev;
		//M12.Text 	:= VevoUjFmDlg.ret_vnev;
	end;
	VevoUjFmDlg.Destroy;

end;

procedure TCmrBeDlg.BitBtn32Click(Sender: TObject);
begin
	MaskEdit2.Text	:= '';
	M6.Text			:= '';
end;

procedure TCmrBeDlg.BitBtn4Click(Sender: TObject);
begin
	MaskEdit6.Text	:= '';
end;

end.
