unit HianyDokList;

interface

uses
  WinTypes, WinProcs, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, QuickRpt, QRExpr, Qrctrls, ExtCtrls, printers, DB, DBTables, Egyeb, Forgalom, J_SQL,
  Grids, Kozos, ADODB ;

type
  THianyDoklistDlg = class(TForm)
    Rep: TQuickRep;
    QRBand3: TQRBand;
    QRLSzamla: TQRLabel;
    QRLabel15: TQRLabel;
    QRLabel6: TQRLabel;
    QRSysData1: TQRSysData;
    QRLabel7: TQRLabel;
    QRSysData4: TQRSysData;
    QRLabel1: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel12: TQRLabel;
    QRLabel13: TQRLabel;
    QRLabel14: TQRLabel;
    QRLabel10: TQRLabel;
	 Query1: TADOQuery;
	 QDolgozo: TADOQuery;
	 QRShape1: TQRShape;
	 QRBand2: TQRBand;
	 QL3: TQRLabel;
	 QL1: TQRLabel;
	 QL2: TQRLabel;
	 QGepkocsi: TADOQuery;
	 QJarat: TADOQuery;
	 QL0: TQRLabel;
	 procedure FormCreate(Sender: TObject);
	 procedure QRBand1BeforePrint(Sender: TQRCustomBand;
	   var PrintBand: Boolean);
	 procedure Tolto(b1, b2, b3 : boolean; jt : integer);
    procedure RepBeforePrint(Sender: TCustomQuickRep;
      var PrintReport: Boolean);
  private
		kellgepkocsi    : boolean;
		kelldolgozo		: boolean;
		kelljarat		: boolean;
		jarattipus		: integer;
		dolgozodir		: string;
		gepkocsidir		: string;
		jaratdir		: string;
		rekszam			: integer;
  public
  end;

var
  HianyDoklistDlg: THianyDoklistDlg;

implementation

{$R *.DFM}

procedure THianyDoklistDlg.FormCreate(Sender: TObject);
begin
	EgyebDlg.SetADOQueryDatabase(Query1);
	EgyebDlg.SetADOQueryDatabase(QDolgozo);
	EgyebDlg.SetADOQueryDatabase(QGepkocsi);
	EgyebDlg.SetADOQueryDatabase(QJarat);
	dolgozodir	:= EgyebDlg.Read_SZGrid('999', '720');
	gepkocsidir	:= EgyebDlg.Read_SZGrid('999', '721');
	jaratdir	:= EgyebDlg.Read_SZGrid('999', '722');
	if ( ( copy(dolgozodir, Length(dolgozodir), 0) <> '\' ) and ( copy(dolgozodir, Length(dolgozodir), 0) <> '/' ) ) then begin
		dolgozodir	:= dolgozodir + '\';
	end;
	if ( ( copy(gepkocsidir, Length(gepkocsidir), 0) <> '\' ) and ( copy(gepkocsidir, Length(gepkocsidir), 0) <> '/' ) ) then begin
		gepkocsidir	:= gepkocsidir + '\';
	end;
	if ( ( copy(jaratdir, Length(jaratdir), 0) <> '\' ) and ( copy(jaratdir, Length(jaratdir), 0) <> '/' ) ) then begin
		jaratdir	:= jaratdir + '\';
	end;
	Query_Run(QDolgozo, 	'Select do_kod, do_kulso, do_arhiv, do_name from dolgozo ');
	Query_Run(QGepkocsi, 	'Select gk_kod, gk_arhiv, gk_kulso, gk_resz from gepkocsi');
	Query_Run(QJarat, 		'Select ja_kod, ja_orsz, ja_alkod, ja_fazis, aj_alnev from jarat, aljarat where ja_kod = aj_jakod');
end;

procedure THianyDoklistDlg.Tolto(b1, b2, b3 : boolean; jt : integer);
begin
	kellgepkocsi    := b1;
	kelldolgozo		:= b2;
	kelljarat		:= b3;
	jarattipus		:= jt;
	Query_Run(Query1, 'SELECT ''1'' TIPUS, GK_RESZ NEV, GK_KOD KOD FROM GEPKOCSI UNION SELECT ''2'' TIPUS, DO_NAME NEV, DO_KOD KOD FROM DOLGOZO '+
		' UNION SELECT ''3'' TIPUS, JA_KOD NEV, JA_KOD KOD FROM '+GetTableFullName(EgyebDlg.v_alap_db, 'JARAT')+' ORDER BY TIPUS, NEV');
end;

procedure THianyDoklistDlg.QRBand1BeforePrint(Sender: TQRCustomBand;
	   var PrintBand: Boolean);
var
	dirstr		: string;
	nevstr		: string;
	tipusstr    : string;
	kereses		: TSearchRec;
	talalat		: integer;
begin
	if ( ( Query1.FieldByName('TIPUS').AsString = '1' ) and (not kellgepkocsi) ) then begin
		PrintBand	:= false;
		Exit;
	end;
	if ( ( Query1.FieldByName('TIPUS').AsString = '2' ) and (not kelldolgozo) ) then begin
		PrintBand	:= false;
		Exit;
	end;
	if ( ( Query1.FieldByName('TIPUS').AsString = '3' ) and (not kelljarat) ) then begin
		PrintBand	:= false;
		Exit;
	end;
	if Query1.FieldByName('TIPUS').AsString = 'JARAT' then begin
		// Ellen�rizni kell a j�rat t�pus�t
//		if jt = 1 then begin
//		end;
//		Exit;
	end;
	dirstr	:= '';
	nevstr	:= Query1.FieldByName('NEV').AsString;
	case StrToIntDef(Query1.FieldByName('TIPUS').AsString, 0) of
		1 : // G�pkocsikr�l van sz�
			begin
				tipusstr	:= 'G';
				if not QGepkocsi.Locate('GK_KOD', Query1.FieldByName('KOD').AsString, []) then begin
					PrintBand	:= false;
					Exit;
				end;
				// L�tezik a GEPKOCSI -> arh�v vizsg�lata
				if StrToIntDef(QGepkocsi.FieldByName('GK_KULSO').AsString, 0) > 0 then begin
					PrintBand	:= false;
					Exit;
				end;
				if StrToIntDef(QGepkocsi.FieldByName('GK_ARHIV').AsString, 0) > 0 then begin
					PrintBand	:= false;
					Exit;
				end;
				// A k�nyvt�r megl�t�nek vizsg�lata
				dirstr	:= gepkocsidir + QGepkocsi.FieldByName('GK_RESZ').AsString;
				if DirectoryExists(dirstr) then begin
					PrintBand	:= false;
					Exit;
				end;
			end;
		2 : // Dolgoz�kr�l van sz�
			begin
				tipusstr	:= 'D';
				if not QDolgozo.Locate('DO_KOD', Query1.FieldByName('KOD').AsString, []) then begin
					PrintBand	:= false;
					Exit;
				end;
				// L�tezik a dolgoz�, arh�v vizsg�lata
				if StrToIntDef(QDolgozo.FieldByName('DO_KULSO').AsString, 0) > 0 then begin
					PrintBand	:= false;
					Exit;
				end;
				if StrToIntDef(QDolgozo.FieldByName('DO_ARHIV').AsString, 0) > 0 then begin
					PrintBand	:= false;
					Exit;
				end;
				// A k�nyvt�r megl�t�nek vizsg�lata
				dirstr	:= dolgozodir + QDolgozo.FieldByName('DO_NAME').AsString;
				if DirectoryExists(dirstr) then begin
					PrintBand	:= false;
					Exit;
				end;
			end;
		3 : // J�ratr�l van sz�
			begin
				tipusstr	:= 'J';
				if not QJarat.Locate('JA_KOD', Query1.FieldByName('KOD').AsString, []) then begin
					PrintBand	:= false;
					Exit;
				end;
				// L�tezik a j�rat -> ellen�rz�tts�g/elsz�molts�g vizsg�lata
				if StrToIntDef(QJarat.FieldByName('JA_FAZIS').AsString, 0) = 0 then begin
					// A j�rat nem elsz�molt �s nem ellen�rz�tt
					PrintBand	:= false;
					Exit;
				end;
				if ( ( StrToIntDef(QJarat.FieldByName('JA_FAZIS').AsString, 0) >= 0 ) and (jarattipus = 1) ) then begin
					// A j�rat nem elsz�molt
					PrintBand	:= false;
					Exit;
				end;
				if ( ( StrToIntDef(QJarat.FieldByName('JA_FAZIS').AsString, 0) <= 0 ) and (jarattipus = 0) ) then begin
					// A j�rat nem ellen�rz�tt
					PrintBand	:= false;
					Exit;
				end;
				if QJarat.FieldByName('AJ_ALNEV').AsString <> '' then begin
					// Alv�llalkoz�i j�rat
					PrintBand	:= false;
					Exit;
				end;
				// A k�nyvt�r megl�t�nek vizsg�lata
				dirstr		:= IntToStr(StrToIntDef(QJarat.FieldByName('JA_ALKOD').AsString,0));
				if dirstr = '0' then begin
					PrintBand	:= false;
					Exit;
				end;
				talalat		:= FindFirst(jaratdir+'*'+QJarat.FieldByName('JA_ORSZ').AsString+dirstr+'.*', faAnyFile, kereses );
				if talalat = 0 then begin
					PrintBand	:= false;
					Exit;
				end;
				dirstr	:= jaratdir+'*'+QJarat.FieldByName('JA_ORSZ').AsString+dirstr+'.*';
				nevstr	:= GetJaratszamFromDb(QJarat);
			end;
		0 :
			begin
			PrintBand	:= false;
			Exit;
			end;
	end;

	QL0.Caption	:= IntToStr(rekszam);
	Inc(rekszam);
	QL1.Caption	:= tipusstr;
	QL2.Caption	:= nevstr;
	QL3.Caption	:= dirstr;
end;

procedure THianyDoklistDlg.RepBeforePrint(Sender: TCustomQuickRep;
  var PrintReport: Boolean);
begin
	rekszam	:= 1;
end;

end.
