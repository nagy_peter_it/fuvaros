unit J_EXCEL;
{******************************************************************************
   Excel �llom�ny kezel�se.
*******************************************************************************}

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls,  OleServer, COMobj, ActiveX, Excel2000, Grids;


const
  xlVAlignBottom 					= $FFFFEFF5;
  xlVAlignCenter 					= $FFFFEFF4;
  xlVAlignDistributed 				= $FFFFEFEB;
  xlVAlignJustify 					= $FFFFEFDE;
  xlVAlignTop 						= $FFFFEFC0;

  xlHAlignCenter 					= $FFFFEFF4;
  xlHAlignCenterAcrossSelection 	= $00000007;
  xlHAlignDistributed 				= $FFFFEFEB;
  xlHAlignFill 					= $00000005;
  xlHAlignGeneral 					= $00000001;
  xlHAlignJustify 					= $FFFFEFDE;
  xlHAlignLeft 					= $FFFFEFDD;
  xlHAlignRight 					= $FFFFEFC8;

  xlPortrait = 1;
  xlLandscape = 2;

type
	TJExcel = class(TComponent)
  	private
		EXAPP		: OleVariant;
		ExcelName	: string;
		FColNumber	: integer;
		RowArr		: Variant;
		TableArr	: Variant;
		last_row 	: integer;
		last_col	: integer;
       version     : string;
	public
		constructor Create(AOwner: TComponent); override;
		destructor 	Destroy; override;
		function  	OpenXlsFile(fn : string) : boolean;
		function	SaveXlsFile : boolean;
		function  	SaveFileAsXls(fn : string) : boolean;
		function  	SaveFileAsHtml(fn : string) : boolean;
		function	FillRow(rnum : integer) : boolean;
		function	FillTable(rnum : integer) : boolean;
		function  	GetRowcell(cnum : integer)	: string;
		function	GetTablecell(rnum, cnum : integer)	: string;
		function  	ReadCell(rnum, cnum : integer)	: string;
		function  	WriteCell(rnum, cnum : integer ; valu : string)	: boolean;
		function  	WriteCellNew(rnum, cnum : integer ; valu : variant)	: boolean;
		function  	Hor_AlignmentCell(rnum, cnum, align : integer)	: boolean;
		function  	Hor_AlignmentRow(rnum, align : integer)	: boolean;
		function	SetRangeBold(rnum1, cnum1, rnum2, cnum2 : integer ; bold_flag : boolean = true  ) : boolean;
		function  	MergeCell(rnum1, cnum1, rnum2, cnum2 : integer ) : boolean;
		function	InitRow : boolean;
		function	InitTable : boolean;
		function	SaveRow(rnum : integer) : boolean;
		function	SetRowcell(cnum : integer; val : variant ) : boolean;
		function	GetSheetsNumber : Integer;
		function 	GetSheetName( shnr : integer) : string;
		function 	SetActiveSheet( shnr : integer) : boolean;
		function 	SetActiveSheetName( newname : string) : boolean;
		function 	SetSheetName( shnr : integer; newname : string) : boolean;
		function 	AddSheet( newname : string) : boolean;
		function  	DeleteSheet( nr : integer) : boolean;
		function	SetColumnWidth(col_nr, width_nr : integer) : boolean;
		function 	GetLastRow : Integer;
		procedure 	SetLastValues;
		function 	GetLastCol : Integer;
		function  	ReadFullXls( sg1 : TStringGrid) : boolean;
       function    SaveFullXls( sg1 : TStringGrid) : boolean;
		function	SetRangeFormat(rnum1, cnum1, rnum2, cnum2 : integer ; form : string  ) : boolean;
		function	SetRangeStyle(rnum1, cnum1, rnum2, cnum2 : integer ; sty : string  ) : boolean;
		function  	ValidateCell(rnum1, cnum1, rnum2, cnum2 : integer; valstr : string ) : boolean;
		function  	SetBorder(rnum1, cnum1, rnum2, cnum2 : integer) : boolean;
		function  	SetCellColor(rnum1, cnum1, rnum2, cnum2, colo : integer) : boolean;
		procedure 	EmptyRow;
       function    GetExtension : string;
    procedure SetOrientation(xlOrientation: integer);
    procedure SetMarginsCentimeters(LeftMargin, RightMargin, TopMargin, BottomMargin: double);
	private
		function 	GetColumnName( colnum : integer) : string;
		procedure 	SetColNumber( colnum : integer) ;
	published
		property 	ColNumber : integer read FColNumber write SetColnumber;
	    property	FileName : string read ExcelName write ExcelName;
   public
       dec_sep     : string;
  end;

var
  JExcel: TJExcel;

implementation

{******************************************************************************
Elj�r�s neve  :  Create
Le�r�s        :  Objektum l�trehoz�sa
Param�terek   :  AOwner - a sz�l� objektum
*******************************************************************************}
constructor TJExcel.Create(AOwner: TComponent);
begin
  	inherited Create(AOwner);
  	Coinitialize(nil);
  	EXAPP			:=CreateOLEObject('Excel.Application');     // Ole object creation
  	EXAPP.visible	:=false;
 	ExcelName		:= '';
  	FColNumber		:= 256;
   dec_sep         := EXAPP.DecimalSeparator;
   version         := EXAPP.Version;
(*
    "15.0"  "Excel 2013."
    "14.0"  "Excel 2010."
    "12.0"  "Excel 2007."
    "11.0"  "Excel 2003."
    "10.0"  "Excel 2002."
    " 9.0"  "Excel 2000."
    " 8.0"  "Excel 97."
    " 7.0"  "Excel 95."
*)
end;

{******************************************************************************
Elj�r�s neve  :  Destroy
Le�r�s        :  Objektum megsz�ntet�se
Param�terek   :  -
*******************************************************************************}
destructor TJExcel.Destroy;
begin
	// Itt j�nnek a megsz�ntet�sek
  	EXAPP.Quit;
	EXAPP 	:= Unassigned;
  	RowArr 	:= Unassigned;
  	inherited Destroy;
end;

{******************************************************************************
F�ggv�ny neve :  GetColumnName
Le�r�s        :  Oszlop sz�veges nev�nek visszaad�sa
Param�terek   :  colnum - az oszlop sorsz�ma
Visszaadott �.:  string - az oszlop neve
*******************************************************************************}
function TJExcel.GetColumnName( colnum : integer) : string;
//******************************************************//
// Creates the column name from the number (eg. 27 = 'AA')
//******************************************************//
begin
	Result	:= '';
	if ( ( colnum < 1 ) or ( colnum > 256 ) ) then begin
		Exit;
	end;
	if colnum < 27 then begin
		Result	:= Chr(64+colnum);
	end else begin
		Result	:= Chr(64+((colnum - 1) DIV 26))+Chr(64+((colnum - 1) MOD 26)+1);
	end;
end;

{******************************************************************************
Elj�r�s neve  :  SetColNumber
Le�r�s        :  Az oszlopok sz�m�nak megad�sa
Param�terek   :  colnum - az oszlopok sz�ma
*******************************************************************************}
procedure TJExcel.SetColNumber( colnum : integer);
begin
	if ( ( colnum < 1 ) or ( colnum > 256 ) ) then begin
   	Exit;
  	end;
  	FColNumber	:= colnum;
end;

{******************************************************************************
F�ggv�ny neve :  OpenXlsFile
Le�r�s        :  XLS �llom�ny megnyit�sa
Param�terek   :  fn        - az �llom�ny neve
Visszaadott �.:  boolean   - sikeress�g jelz�
*******************************************************************************}
function	TJExcel.OpenXlsFile(fn : string) : boolean;
begin
	Result		:= false;
	ExcelName 	:= fn;
  	try
     	if not FileExists(fn) then begin
			// �j workbook hozz�ad�sa
        	EXAPP.workBooks.Add;
     	end else begin
			EXAPP.workBooks.Open(fn);
     	end;
  		Result	:= true;
  	except
  	end;
end;

{******************************************************************************
F�ggv�ny neve :  SaveXlsFile
Le�r�s        :  �llom�ny ment�se
Param�terek   :  -
Visszaadott �.:  boolean   - a ment�s sikeress�ge
*******************************************************************************}
function	TJExcel.SaveXlsFile : boolean;
begin
	Result		:= false;
   try
       EXAPP.DisplayAlerts := False;
//   	EXAPP.ActiveWorkBook.SaveAs(ExcelName,xlWorkbookNormal);
  		EXAPP.ActiveWorkBook.SaveAs(ExcelName);
     	Result	:= true;
   except
   end;
end;

{******************************************************************************
F�ggv�ny neve :  SaveFileAsXls
Le�r�s        :  Ment�s m�sk�nt
Param�terek   :  fn        - a ment�si �llom�ny neve
Visszaadott �.:  boolean   - a ment�s sikeress�ge
*******************************************************************************}
function	TJExcel.SaveFileAsXls(fn : string) : boolean;
begin
	Result	:= false;
   try
       EXAPP.DisplayAlerts := False;
//  		EXAPP.ActiveWorkBook.SaveAs(fn,xlWorkbookNormal);
  		EXAPP.ActiveWorkBook.SaveAs(fn);
     	Result	:= true;
   except
   end;
end;

{******************************************************************************
F�ggv�ny neve :  SaveFileAsHtml
Le�r�s        :  Ment�s html �llom�nyk�nt
Param�terek   :  fn        - �llom�ny neve
Visszaadott �.:  boolean   - a ment�s sikeress�ge
*******************************************************************************}
function	TJExcel.SaveFileAsHtml(fn : string) : boolean;
begin
	Result	:= false;
   try
   	EXAPP.ActiveWorkBook.SaveAs(fn,xlHtml);
     	Result	:= true;
   except
	end;
end;

{******************************************************************************
F�ggv�ny neve :  FillRow
Le�r�s        :  A sor objektum felt�lt�se az XLS tartalommal
Param�terek   :  rnum      - a sor sz�ma
Visszaadott �.:  boolean   - sikeress�g
*******************************************************************************}
function	TJExcel.FillRow(rnum : integer) : boolean;
begin
	Result	:= false;
  	try
		RowArr 	:= EXAPP.Range['A'+IntToStr(rnum), GetColumnName(ColNumber)+IntToStr(rnum)].Value;
     	Result	:= true;
  	except
	end;
end;

{******************************************************************************
F�ggv�ny neve :  InitRow
Le�r�s        :  Sor objektum inicializ�l�sa
Param�terek   :  -
Visszaadott �.:  boolean   - sikeress�g
*******************************************************************************}
function	TJExcel.InitRow : boolean;
begin
	Result	:= false;
  	try
    	RowArr	:= VarArrayCreate([1,ColNumber],varVariant);
     	Result	:= true;
  	except
  	end;
end;

{******************************************************************************
F�ggv�ny neve :  InitTable
Le�r�s        :  T�bl�zat objektum inicializ�l�sa
Param�terek   :  -
Visszaadott �.:  boolean   - sikeress�g
*******************************************************************************}
function	TJExcel.InitTable : boolean;
begin
end;

{******************************************************************************
F�ggv�ny neve :  FillTable
Le�r�s        :  T�bl�zat objektum felt�lt�se az XLS-b�l
Param�terek   :  rnum      - a felt�ltend� sorok sz�ma
Visszaadott �.:  boolean   - sikeress�g
*******************************************************************************}
function	TJExcel.FillTable(rnum : integer) : boolean;
begin
	Result	:= false;
  	try
		TableArr 	:= EXAPP.Range['A1', GetColumnName(ColNumber)+IntToStr(rnum)].Value;
     	Result		:= true;
	except
	end;
end;

{******************************************************************************
F�ggv�ny neve :  GetTablecell
Le�r�s        :  T�bl�zat cell�j�nak visszaad�sa
Param�terek   :  rnum      - sor sz�ma
                 cnum      - oszlop sz�ma
Visszaadott �.:  string    - a cella tartalma
*******************************************************************************}
function	TJExcel.GetTablecell(rnum, cnum : integer)	: string;
begin
	Result	:= '';
	if ( ( cnum < 1 ) or ( cnum > ColNumber) ) then begin
   	Exit;
	end;
   Result	:= TableArr[rnum,cnum];
end;

{******************************************************************************
F�ggv�ny neve :  SaveRow
Le�r�s        :  Sor objektum be�r�sa az XLS-be
Param�terek   :  rnum      - a sor sz�ma
Visszaadott �.:  boolean   - sikeress�g
*******************************************************************************}
function	TJExcel.SaveRow(rnum : integer) : boolean;
begin
	Result	:= false;
	try
		EXAPP.Range['A'+IntToStr(rnum), GetColumnName(ColNumber)+IntToStr(rnum)].Value := RowArr;
		Result	:= true;
	except
	end;
end;

{******************************************************************************
F�ggv�ny neve :  GetRowcell
Le�r�s        :  Sor objektum cella tartalm�nak visszaad�sa
Param�terek   :  cnum      - az oszlop sz�ma
Visszaadott �.:  string    - a cella tartalma
*******************************************************************************}
function	TJExcel.GetRowcell(cnum : integer)	: string;
begin
	Result	:= '';
	if ( ( cnum < 1 ) or ( cnum > ColNumber) ) then begin
		Exit;
	end;
	Result	:= RowArr[1, cnum];  // NagyP 20160406 - az�rt k�tdimenzi�s a hivatkoz�s, mert
                              // a FillRow egy Range �rt�ket ad neki, ami k�tdimenzi�s
  // Result	:= RowArr[cnum];
  // Result	:= VarArrayGet(RowArr, [cnum]);
end;

{******************************************************************************
F�ggv�ny neve :  SetRowcell
Le�r�s        :  Sor objektum cella tartalm�nak be�ll�t�sa
Param�terek   :  cnum      - az oszlop sz�ma
                 val       -  a cella �rt�ke
Visszaadott �.:  boolean   - sikeress�g
*******************************************************************************}
function	TJExcel.SetRowcell(cnum : integer; val : variant ) : boolean;
begin
	Result	:= false;
	if ( ( cnum < 1 ) or ( cnum > ColNumber) ) then begin
		Exit;
	end;
	RowArr[cnum]	:= val;
  	Result          := true;
end;

{******************************************************************************
F�ggv�ny neve :  ReadCell
Le�r�s        :  Cella tartalm�nak olvas�sa az XLS-b�l
Param�terek   :  rnum      - a sor sz�ma
                 cnum      - az oszlop sz�ma
Visszaadott �.:  string    - a cella �rt�ke
*******************************************************************************}
function	TJExcel.ReadCell(rnum, cnum : integer)	: string;
begin
	Result	:= '';
	if ( ( cnum < 1 ) or ( cnum > 256) ) then begin
   	Exit;
	end;
  	Result	:= EXAPP.Range[GetColumnName(cnum)+IntToStr(rnum)].Value;
end;

{******************************************************************************
F�ggv�ny neve :  WriteCell
Le�r�s        :  Cella be�r�sa az XLS-be
Param�terek   :  rnum      - a sor sz�ma
                 cnum      - az oszlop sz�ma
                 valu      - a cella �rt�ke
Visszaadott �.:  boolean   - sikeress�g
*******************************************************************************}
function	TJExcel.WriteCell(rnum, cnum : integer ; valu : string)	: boolean;
begin
	Result				:= true;
	EXAPP.Range[GetColumnName(cnum)+IntToStr(rnum)].select;
	EXAPP.ActiveCell	:= valu;
end;

function	TJExcel.WriteCellNew(rnum, cnum : integer ; valu : variant)	: boolean;
begin
	Result				:= false;
   try
	    EXAPP.Cells[rnum, cnum].Value := valu;
	    Result				:= true;
   except

   end;
end;



{******************************************************************************
F�ggv�ny neve :  GetSheetsNumber
Le�r�s        :  Az adatlapok sz�m�nak meghat�roz�sa
Param�terek   :  -
Visszaadott �.:  integer - az adatlapok sz�ma
*******************************************************************************}
function TJExcel.GetSheetsNumber : Integer;
begin
	Result	:= EXAPP.ActiveWorkBook.Sheets.Count;
end;

{******************************************************************************
F�ggv�ny neve :  GetSheetName
Le�r�s        :  Az adatlap nev�nek visszaad�sa
Param�terek   :  shnr      - az adatlap sz�ma
Visszaadott �.:  string    - az adatlap neve
*******************************************************************************}
function TJExcel.GetSheetName( shnr : integer) : string;
begin
	Result	:= EXAPP.ActiveWorkBook.Sheets[shnr].Name;
end;


{******************************************************************************
F�ggv�ny neve :  SetActiveSheet
Le�r�s        :  Az akt�v adatlap be�ll�t�sa sorsz�m alapj�n
Param�terek   :  shnr      - az adatlap sz�ma
Visszaadott �.:  boolean   - sikeress�g
*******************************************************************************}
function 	TJExcel.SetActiveSheet( shnr : integer) : boolean;
begin
	Result	:= false;
	try
		EXAPP.ActiveWorkBook.Sheets[shnr].Select;
  		Result	:= true;
  	except
	end;
end;

function TJExcel.SetSheetName( shnr : integer; newname : string) : boolean;
begin
	EXAPP.ActiveWorkBook.Sheets[shnr].Name := newname;
	Result	:= true;
end;

function TJExcel.SetActiveSheetName( newname : string) : boolean;
begin
	EXAPP.ActiveSheet.Name := newname;
	Result	:= true;
end;

function  TJExcel.AddSheet( newname : string) : boolean;
begin
	EXAPP.ActiveWorkBook.Sheets.Add;
	SetActiveSheet(0);
   SetActiveSheetName(newname);
	Result	:= true;
end;

function  TJExcel.DeleteSheet( nr : integer) : boolean;
begin
	if ( ( nr < 1 ) or (nr > GetSheetsNumber) ) then begin
	   Result	:= false;
	   Exit;
   end;
   SetActiveSheet( nr );
	EXAPP.ActiveSheet.Delete;
   Result	:= true;
end;

function  	TJExcel.MergeCell(rnum1, cnum1, rnum2, cnum2 : integer ) : boolean;
begin
	EXAPP.Range[GetColumnName(cnum1)+IntToStr(rnum1), GetColumnName(cnum2)+IntToStr(rnum2)].MergeCells	:= true;
	Result	:= true;
end;

function TJExcel.Hor_AlignmentCell(rnum, cnum, align : integer)	: boolean;
begin
	EXAPP.Range[GetColumnName(cnum)+IntToStr(rnum)].HorizontalAlignment := align;
	Result	:= true;
end;

function TJExcel.Hor_AlignmentRow(rnum, align : integer)	: boolean;
begin
	EXAPP.Range['A'+IntToStr(rnum)+':'+GetColumnName(ColNumber)+IntToStr(rnum)].HorizontalAlignment := align;
	Result	:= true;
end;

function TJExcel.SetColumnWidth(col_nr, width_nr : integer) : boolean;
begin
	EXAPP.ActiveSheet.Columns[col_nr].ColumnWidth := width_nr;
	Result	:= true;
end;

function TJExcel.SetRangeBold(rnum1, cnum1, rnum2, cnum2 : integer ; bold_flag : boolean = true  ) : boolean;
begin
	EXAPP.Range[GetColumnName(cnum1)+IntToStr(rnum1)+':'+GetColumnName(cnum2)+IntToStr(rnum2)].Font.Bold := bold_flag;
	Result	:= true;
end;

{******************************************************************************
Elj�r�s neve  :  SetLastValues;
Le�r�s        :  A maxim�lis �rt�kek be�ll�t�sa
Param�terek   :  -
*******************************************************************************}
procedure TJExcel.SetLastValues;
begin
	EXAPP.ActiveSheet.Cells.SpecialCells(xlCellTypeLastCell,EmptyParam).Activate;
	last_row	:= EXAPP.ActiveCell.Row;
	last_col	:= EXAPP.ActiveCell.Column;
end;

{******************************************************************************
F�ggv�ny neve :  GetLastRow
Le�r�s        :  A max sor visszaad�sa
Param�terek   :  -
Visszaadott �.:  integer - a max sor sz�ma
*******************************************************************************}
function TJExcel.GetLastRow : Integer;
begin
	SetLastValues;
   Result	:= last_row;
end;

{******************************************************************************
F�ggv�ny neve :  GetLastCol
Le�r�s        :  A max oszlop visszaad�sa
Param�terek   :  -
Visszaadott �.:  integer - a max oszlop
*******************************************************************************}
function TJExcel.GetLastCol : Integer;
begin
	SetLastValues;
   Result	:= last_col;
end;

{******************************************************************************
F�ggv�ny neve :  ReadFullXls
Le�r�s        :  A teljes XLS beolvas�sa egy StringGrid-be
Param�terek   :  sg1       - a grid neve
Visszaadott �.:  boolean   - sikeress�g
*******************************************************************************}
function  TJExcel.ReadFullXls( sg1 : TStringGrid) : boolean;
var
	i, j 	: integer;
  S: string;
begin
	sg1.RowCount 	:= GetLastRow;
	sg1.ColCount    := GetLastCol;
   InitRow;
	for i := 1 to sg1.RowCount do begin
		FillRow(i);
		for j := 1 to sg1.ColCount do begin
      S:= GetRowcell(j);
			sg1.Cells[j-1,i-1] := S;
		end;
	end;
	Result	:= true;
end;

function	TJExcel.SetRangeFormat(rnum1, cnum1, rnum2, cnum2 : integer ; form : string  ) : boolean;
begin
	EXAPP.Range[GetColumnName(cnum1)+IntToStr(rnum1)+':'+GetColumnName(cnum2)+IntToStr(rnum2)].NumberFormat := form;
	Result	:= true;
end;

function	TJExcel.SetRangeStyle(rnum1, cnum1, rnum2, cnum2 : integer ; sty : string  ) : boolean;
begin
	EXAPP.Range[GetColumnName(cnum1)+IntToStr(rnum1)+':'+GetColumnName(cnum2)+IntToStr(rnum2)].Style := sty;
	Result	:= true;
end;

{******************************************************************************
F�ggv�ny neve :  SaveFullXls
Le�r�s        :  A teljes grid ki�r�sa xls-be
Param�terek   :  sg1       - a grid neve
Visszaadott �.:  boolean   - sikeress�g
*******************************************************************************}
function  TJExcel.SaveFullXls( sg1 : TStringGrid) : boolean;
var
  i		   	: integer;
  j		   	: integer;
begin
   // T�bl�zat ment�se xls-be
   ColNumber	:= sg1.ColCount;
   InitRow;
   for j := 0 to SG1.RowCount - 1 do begin
       for i := 0 to sg1.ColCount - 1 do begin
           SetRowcell(i+1, sg1.Cells[i, j]);
       end;
       SaveRow(j+1);
   end;
   Result  := true;
end;

{******************************************************************************
Elj�r�s neve  :  EmptyRow;
Le�r�s        :  Sor objektum �r�t�sa
Param�terek   :  -
*******************************************************************************}
procedure TJExcel.EmptyRow;
var
	cnum	: integer;
begin
	for cnum := 1 to FColNumber do begin
		RowArr[cnum]	:= '';
	end;
end;

function  	TJExcel.ValidateCell(rnum1, cnum1, rnum2, cnum2 : integer; valstr : string ) : boolean;
begin
	EXAPP.Range[GetColumnName(cnum1)+IntToStr(rnum1), GetColumnName(cnum2)+IntToStr(rnum2)].Validation.Add(xlValidateLIst, xlValidAlertStop, xlBetween, valstr);
	Result	:= true;
end;

function TJExcel.GetExtension : string;
begin
    Result := 'XLS';
    if version  >= '12.0' then begin
        Result := 'XLSX';
    end;
end;

procedure TJExcel.SetOrientation(xlOrientation: integer);
begin
   EXAPP.ActiveSheet.PageSetup.Orientation := xlOrientation;
end;

procedure TJExcel.SetMarginsCentimeters(LeftMargin, RightMargin, TopMargin, BottomMargin: double);
begin
   if LeftMargin >= 0 then EXAPP.ActiveSheet.PageSetup.LeftMargin := EXAPP.CentimetersToPoints(LeftMargin);
   if RightMargin >= 0 then EXAPP.ActiveSheet.PageSetup.RightMargin := EXAPP.CentimetersToPoints(RightMargin);
   if TopMargin >= 0 then EXAPP.ActiveSheet.PageSetup.TopMargin := EXAPP.CentimetersToPoints(TopMargin);
   if BottomMargin >= 0 then EXAPP.ActiveSheet.PageSetup.BottomMargin := EXAPP.CentimetersToPoints(BottomMargin);
end;

function  	TJExcel.SetBorder(rnum1, cnum1, rnum2, cnum2 : integer) : boolean;
begin
	EXAPP.Range[GetColumnName(cnum1)+IntToStr(rnum1), GetColumnName(cnum2)+IntToStr(rnum2)].Borders.LineStyle := xlContinuous;
	Result	:= true;
end;

function  	TJExcel.SetCellColor(rnum1, cnum1, rnum2, cnum2, colo : integer) : boolean;
begin
	EXAPP.Range[GetColumnName(cnum1)+IntToStr(rnum1), GetColumnName(cnum2)+IntToStr(rnum2)].Interior.Color  := colo;
	Result	:= true;
end;



(*
XLApplication := CreateOleObject('Excel.Application');
  XLApplication.Visible := True;
  XLApplication.Workbooks.Add;
  XLApplication.Workbooks.Add(xlWBatChart);
  XLApplication.Workbooks.Add(xlWBatWorkSheet);
  XLApplication.Workbooks[2].Sheets.Add(,,1,xlChart);
  XLApplication.Workbooks[3].Sheets.Add(,,1,xlWorkSheet);
  for i := 1 to XLApplication.Workbooks.Count do begin
    ListBox1.Items.Add('Workbook: ' + XLApplication.Workbooks[i].Name);
	 for j := 1 to XLApplication.Workbooks[i].Sheets.Count do
	   ListBox1.Items.Add('  Sheet: ' + XLApplication.Workbooks[i].Sheets[j].Name);
  end;
end;


 XLApp.DisplayAlerts := False;  // Discard unsaved files....
    XLApp.Quit;

var
  ColumnRange: Variant;
begin
  ColumnRange := XLApp.Workbooks[1].WorkSheets['Delphi Data'].Columns;
  ColumnRange.Columns[1].ColumnWidth := 5;
  ColumnRange.Columns.Item[1].Font.Bold := True;
  ColumnRange.Columns[1].Font.Color := clBlue;


procedure TForm1.ChartData;
var
  ARange: Variant;
  Sheets: Variant;
begin
  XLApp.Workbooks[1].Sheets.Add(,,1,xlChart);
  Sheets := XLApp.Sheets;
  ARange := Sheets.Item['Delphi Data'].Range['A1:A10'];
  Sheets.Item['Chart1'].SeriesCollection.Item[1].Values := ARange;
  Sheets.Item['Chart1'].ChartType := xl3DPie;
  Sheets.Item['Chart1'].SeriesCollection.Item[1].HasDataLabels := True;

  XLApp.Workbooks[1].Sheets.Add(,,1,xlChart);
  Sheets.Item['Chart2'].SeriesCollection.Item[1].Values := ARange;
  Sheets.Item['Chart2'].SeriesCollection.Add(ARange);
  Sheets.Item['Chart2'].SeriesCollection.NewSeries;
  Sheets.Item['Chart2'].SeriesCollection.Item[3].Values :=
    VarArrayOf([1,2,3,4,5, 6,7,8,9,10]);
  Sheets.Item['Chart2'].ChartType := xl3DColumn;
end;


var
  Sheets: Variant;
begin
  SetFocus;
  
  Sheets := XLApp.Sheets;

  Sheets.Item['Delphi Data'].Activate;
  Sheets.Item['Delphi Data'].Range['A1:A10'].Select;
  Sheets.Item['Delphi Data'].UsedRange.Copy;

  CopyCellsToWord;

  Sheets.Item['Chart1'].Select;
  XLApp.Selection.Copy;

  CopyChartToWord;


procedure TForm1.CopyChartToWord;
var
  Range: Variant;
  i, NumPars: Integer;
begin
  NumPars := WordApp.Documents.Item(1).Paragraphs.Count;

  Range := WordApp.Documents.Item(1).Range(
    WordApp.Documents.Item(1).Paragraphs.Item(NumPars).Range.Start,
    WordApp.Documents.Item(1).Paragraphs.Item(NumPars).Range.End);
  Range.Text := 'This is graph: ';

  for i := 1 to 3 do WordApp.Documents.Item(1).Paragraphs.Add;

  Range := WordApp.Documents.Item(1).Range(
    WordApp.Documents.Item(1).Paragraphs.Item(NumPars + 1).Range.Start,
    WordApp.Documents.Item(1).Paragraphs.Item(NumPars + 1).Range.End);

  Range.Paste; //Special(,,,,wdPasteOleObject);
end;

procedure TForm1.CopyCellsToWord;
var
  Range: Variant;
  i: Integer;
begin
  WordApp := CreateOleObject('Word.Application');
  WordApp.Visible := True;
  WordApp.Documents.Add;
  Range := WordApp.Documents.Item(1).Range;
  Range.Text := 'This is a column from a spreadsheet: ';
  for i := 1 to 3 do WordApp.Documents.Item(1).Paragraphs.Add;
  Range := WordApp.Documents.Item(1).Range(
    WordApp.Documents.Item(1).Paragraphs.Item(3).Range.Start);
  Range.Paste;
  for i := 1 to 3 do WordApp.Documents.Item(1).Paragraphs.Add;
end;


procedure TForm1.CopyChartToWord;
var
  Range: Variant;
  i, NumPars: Integer;
begin
  NumPars := WordApp.Documents.Item(1).Paragraphs.Count;
  Range := WordApp.Documents.Item(1).Range(
  WordApp.Documents.Item(1).Paragraphs.Item(NumPars).Range.Start,
  WordApp.Documents.Item(1).Paragraphs.Item(NumPars).Range.End);
  Range.Text := 'This is graph: ';

  for i := 1 to 3 do WordApp.Documents.Item(1).Paragraphs.Add;
  Range := WordApp.Documents.Item(1).Range(
    WordApp.Documents.Item(1).Paragraphs.Item(NumPars + 2).Range.Start,
    WordApp.Documents.Item(1).Paragraphs.Item(NumPars + 2).Range.End);

  Range.PasteSpecial(,,,,wdPasteOleObject);
end;


I return the paragraph count in the variable NumPars. I then create a domain that ranges over the last paragraph of the document. In other words, I count the paragraphs in the document, and then say I want to establish a Range on the last paragraph. Once again, this is one way to position yourself in the document:

Range := WordApp.Documents.Item(1).Range(
  WordApp.Documents.Item(1).Paragraphs.Item(NumPars).Range.Start,
  WordApp.Documents.Item(1).Paragraphs.Item(NumPars).Range.End);

Once I�ve located myself in the proper position, the next step is to enter a single descriptive line of text, followed by a few additional paragraphs:

  Range.Text := 'This is graph: ';
  for i := 1 to 3 do
    WordApp.Documents.Item(1).Paragraphs.Add;

I then once again position myself on the last paragraph in the document:

Range := WordApp.Documents.Item(1).Range(
  WordApp.Documents.Item(1).Paragraphs.Item(NumPars + 1).Range.Start,
  WordApp.Documents.Item(1).Paragraphs.Item(NumPars + 1).Range.End);


WordApp.Documents.Item(1).SaveAs('c:\foo.doc');

The following code allows you to send a mail message:

procedure TForm1.MailDocument;
begin
  WordApp.Documents.Item(1).SaveAs('c:\foo.doc');
  WordApp.Options.SendMailAttach := True;
  WordApp.Documents.Item(1).SendMail;
end;

// SIMPLE WORD USAGE

uses
   ComObj;

procedure TForm1.Button1Click(Sender: TObject) ;
var
   WordApplication, WordDocument: Variant;
begin
   WordApplication := CreateOleObject('Word.Application') ;
   WordDocument := WordApplication.Documents.Add;
   WordApplication.Selection.TypeText('Hello world') ;
   WordDocument.SaveAs(FileName := 'C:\Doc.Doc',
                       AddToRecentFiles := False) ;
   WordApplication.Quit(False)
end;


*)

end.
