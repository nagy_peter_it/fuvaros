unit NyitoExport;

interface

uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, DBTables, DB, StdCtrls, Buttons, Mask, DBCtrls,
  ExtCtrls, ADODB, FileCtrl, StrUtils, Grids, J_DataModule, T_DictCsv;

type
  TKulcsAFA = record  // KULCS �FA k�d �s megnevez�s
    KulcsKod: string;
    KulcsNev: string;
    end;  // record

  TNyitoExportDlg = class(TForm)
    Panel1: TPanel;
    Label4: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    BitBtn6: TBitBtn;
    M1: TMaskEdit;
    BitBtn5: TBitBtn;
    BtnExport: TBitBtn;
    SG1: TStringGrid;
    M2: TMaskEdit;
    MaskEdit3: TMaskEdit;
    Query1: TADOQuery;
    Query2: TADOQuery;
    OpenDialog1: TOpenDialog;
    Query3: TADOQuery;
    ADOQuery1: TADOQuery;
    ADOQuery2: TADOQuery;
    Query4: TADOQuery;
    QueryVevo: TADOQuery;
    Panel2: TPanel;
    Listbox1: TMemo;
    cbEv: TComboBox;
	 procedure FormCreate(Sender: TObject);
	 procedure BitBtn6Click(Sender: TObject);
	 procedure BitBtn5Click(Sender: TObject);
	 procedure BtnExportClick(Sender: TObject);
	 procedure Feldolgozas_Servantes;
 	 procedure Feldolgozas_Kulcs;

  private
		kilepes		: boolean;
		JSzamla    	: TJSzamla;
    // --> T_DictCsv function SaveToCSV(Dict: TObjectList; FN: string; IncludeHeader: boolean): boolean;
    procedure GetFokonyviSzamok(v_eus, EURszamla, s_3orsz, v_3orsz, v_belf, adoalany: boolean; szamla_allapot: integer; var str9, str11, str12, AFAFokonyviKod: string);
    function GetKulcsAFA(AFAFokonyviKod, afakulcs: string): TKulcsAFA;
    function GetFoszamla(CCY: string; Belfold_e: boolean): string;
  public
  end;

var
  NyitoExportDlg: TNyitoExportDlg;

implementation

uses
	Egyeb, Kozos, J_SQL, J_Excel, HianyVevobe, Windows;
{$R *.DFM}

procedure TNyitoExportDlg.FormCreate(Sender: TObject);
const
  FirstDBYear =  2013;
var
  i, ThisYear: integer;
begin
	// M1.Text:= EgyebDlg.DocumentPath+'\KONYVELES';
  if EgyebDlg.KONYVELES_ATAD_FORMA = 'SERVANTES' then begin
	   M1.Text:= EgyebDlg.Read_SZGrid( '999', '752' );
     // MaskEdit3.Enabled:= True;
     end;
  if EgyebDlg.KONYVELES_ATAD_FORMA = 'KULCS' then begin
	   M1.Text:= EgyebDlg.Read_SZGrid( '999', '754' );
     // MaskEdit3.Enabled:= False; // nincs k�l�n �llom�ny n�v
     end;
  if  EgyebDlg.TESZTPRG then
	  M1.Text:=  EgyebDlg.DocumentPath+'\KONYVELES'  ;

  // nem mindenhol m�k�dik a relat�v hivatkoz�s, ez�rt behelyettes�tem a konkr�t el�r�si utat
  if copy(M1.Text,1,2)='.\' then begin
    M1.Text:= EgyebDlg.ExePath+copy(M1.Text, 3, 9999);
    end;

  //M1.Text:= StringReplace(M1.Text,'\\','\',[rfReplaceAll]);
	EgyebDlg.SetADOQueryDatabase(Query1);
	EgyebDlg.SetADOQueryDatabase(Query2);
	EgyebDlg.SetADOQueryDatabase(Query3);
	EgyebDlg.SetADOQueryDatabase(Query4);
	EgyebDlg.SetADOQueryDatabase(QueryVevo);
	Label1.Caption	:= '';
	Label1.Update;
	Query_Run(Query2, 'UPDATE SZFEJ SET SA_ATAD = 0 WHERE SA_ATAD IS NULL OR SA_ATAD = '''' ');
  with cbEv do begin
     Items.Clear;
     Items.Add('mind');
     ThisYear := StrToInt(FormatDateTime('yyyy', Now));
     for i := FirstDBYear to ThisYear do begin
         Items.Add(IntToStr(i));
         end;  // for
     ItemIndex := Items.Count -1;
     end; // with
end;

procedure TNyitoExportDlg.BitBtn6Click(Sender: TObject);
begin
	if not kilepes then begin
		kilepes	:= true;
	end else begin
		Close;
	end;
end;

procedure TNyitoExportDlg.BitBtn5Click(Sender: TObject);
begin
	// Az �llom�ny kiv�laszt�sa
{	OpenDialog1.Filter		:= 'XLS �llom�nyok (*.xls)|*.XLS';
	OpenDialog1.DefaultExt  := 'XLS';
   OpenDialog1.InitialDir	:= EgyebDlg.DocumentPath;
	if OpenDialog1.Execute then begin
 		M1.Text := OpenDialog1.FileName;
	end;
 }
//  JvSelectDirectory1.InitialDir:=EgyebDlg.DocumentPath;
//  JvSelectDirectory1.InitialDir:=M1.Text;
//  if JvSelectDirectory1.Execute then
//    M1.Text:=JvSelectDirectory1.Directory;
end;

procedure TNyitoExportDlg.BtnExportClick(Sender: TObject);
var
  fnev,elotag: string;
begin

  elotag:='';
  fnev:= DateTimeToStr(now);
  fnev:= StringReplace(fnev,'.','',[rfReplaceAll]);
  fnev:= StringReplace(fnev,':','',[rfReplaceAll]);
  fnev:= StringReplace(fnev,' ','_',[rfReplaceAll]);
  if copy(EgyebDlg.v_alap_db,1,5)='DKDAT' then elotag:='DK_';
  if copy(EgyebDlg.v_alap_db,1,3)='DAT' then elotag:='JS_';


	BtnExport.Visible	:= false;
  if EgyebDlg.KONYVELES_ATAD_FORMA = 'SERVANTES' then begin
      fnev:= 'feladas_'+fnev+'.xls';
      MaskEdit3.Text:=elotag+fnev;
    	Feldolgozas_Servantes;
      end
  else if EgyebDlg.KONYVELES_ATAD_FORMA = 'KULCS' then begin
      fnev:= 'feladas_'+fnev; // 3 file k�sz�l majd
      MaskEdit3.Text:=elotag+fnev;
    	Feldolgozas_Kulcs;
      end
  else NoticeKi('Ismeretlen f�k�nyvi rendszer: '+EgyebDlg.KONYVELES_ATAD_FORMA);
	BtnExport.Visible	:= true;
end;

procedure TNyitoExportDlg.Feldolgozas_Kulcs;
var
	fn_fej, fn_tetel, fn_ugyfel, mappa, str: string;
	sorszam, i, j, hianysz, pozic, szamlakod, belfoldi, eus, code, hiba, atadva, tetelsorszam: integer;
	valutanem, str8,	str9, str10, str11,	str12, vegsokod, kodstr: string;
	szamlaszam, tedat, esdat, kidat, kodkiegstr, kodelostr, orszag, regiszamla, sql, sev : string;
  storno_string, AFAs, vevocim, afakulcs, AFAFokonyviKod, szamla_devizanem, ArfFormat: string;
	JSzamlaSor	: TJSzamlaSor;
	t	: TDateTime;
	szorzo: double;
  v_belf,v_eus,v_3orsz,s_3orsz, EURszamla, adoalany, osszesitett, found: boolean;

  // KulcsFejList, KulcsTetelList, KulcsUgyfelList: TObjectList;
  KulcsFejList, KulcsTetelList, KulcsUgyfelList: TNP_CSV;
  KulcsFej, KulcsTetel, KulcsUgyfel: TDictCSVLine;
  PartnerKontakt: TPartnerKontakt;
  KulcsAFA: TKulcsAFA;

  function GetElszamolasiDatum(const i_vekod: string; i_osszesitett: boolean): string;
     // a sz�mlasorok teljes�t�si d�tumainak maximuma
     var
        VevokodLista, teljdatum, maxdatum: string;
        i: integer;
     begin
        VevokodLista:= EgyebDlg.Read_SZGrid( '382', 'ELSZ');
        if ListabanBenne(i_vekod, VevokodLista, ',') and i_osszesitett then begin
          maxdatum:= '1900.01.01.';
          for i:= 0 to JSzamla.SzamlaSorok.Count-1 do begin
            teljdatum:= (JSzamla.SzamlaSorok[i] as TJSzamlaSor).sorteljesites;
            if teljdatum > maxdatum then
               maxdatum:= teljdatum;
            end;
          Result:= maxdatum;
          end  // if
        else Result:= '';
      end;

begin

  hiba:=0;
  atadva:=0;
	if M1.Text	= '' then begin
		NoticeKi('A kimeneti �llom�ny nem lehet �res!');
		M1.SetFocus;
		Exit;
	end;

	mappa	:= m1.Text;

  fn_fej:=mappa+'\'+MaskEdit3.Text+'.csv';
  fn_tetel:=mappa+'\'+MaskEdit3.Text+'.001';
  fn_ugyfel:=mappa+'\'+MaskEdit3.Text+'.002';

  if FileExists(fn_fej) then begin
		if NoticeKi('L�tez� �llom�ny ('+fn_fej+')! Fel�l�rjuk?', NOT_QUESTION) <> 0 then begin
			Exit;
		end;
		DeleteFile(PChar(fn_fej));
    DeleteFile(PChar(fn_tetel));
    DeleteFile(PChar(fn_ugyfel));
	end;

  // KulcsFejList:= TObjectList.Create;
  // KulcsTetelList:= TObjectList.Create;
  // KulcsUgyfelList:= TObjectList.Create;

  KulcsFejList:= TNP_CSV.Create;
  KulcsTetelList:= TNP_CSV.Create;
  KulcsUgyfelList:= TNP_CSV.Create;

  tetelsorszam:=0;  // "Tetel" �llom�ny egyedi sorsz�ma
  try

  // ---------- SZ�ML�K FELGY�JT�SE ---------- //
	if M2.Text = '' then begin
		Query_Run(Query2, 'SELECT SA_KOD, SA_UJKOD,SA_SAEUR,SA_VEADO,SA_EUADO, sa_kidat,SA_FIMOD FROM SZFEJ '+
		' WHERE ( SA_TEDAT >= ''2016.01.01.'' ) AND SA_RESZE = 0  AND SA_FLAG  <> ''1'' AND SA_ATAD = 0 '+
    ' and SA_TEDAT LIKE '''+cbEV.Text+'%'' '+
    // a p�rhuzamos Servantes-Kulcs feldolgoz�shoz: csak addig adjuk �t, ameddig a m�sik is �t van adva.
    // ' AND SA_KOD<= ''16107707'' '+
		' UNION SELECT SA_KOD, SA_UJKOD,SA_SAEUR,SA_VEADO,SA_VEADO,sa_kidat,SA_FIMOD FROM SZFEJ2 '+
    ' WHERE ( SA_TEDAT >= ''2016.01.01.'' ) AND SA_FLAG  <> ''1''  AND SA_ATAD = 0 '+
    ' and SA_TEDAT LIKE '''+cbEV.Text+'%'' '+
    // a p�rhuzamos Servantes-Kulcs feldolgoz�shoz: csak addig adjuk �t, ameddig a m�sik is �t van adva.
    // ' AND SA_KOD<= ''16107707'' '+
		' ORDER BY SA_KOD');
	end else begin
		// V�gigmegy�nk a megadott sz�mlasz�mokon �s kigy�jtj�k a megfelel� sz�ml�kat
		vegsokod	:= '';
		kodstr		:= M2.Text;
		pozic 		:= Pos(',', kodstr);
		while pozic > 0 do begin
			szamlakod	:= StrToIntdef(copy(kodstr,1,pozic-1),0);
			szamlaszam	:= Format('%5.5d', [szamlakod]);
			Query_Run(Query2, 'SELECT SA_KOD FROM SZFEJ WHERE SA_KOD LIKE ''%'+szamlaszam+'%'' ');
			if Query2.RecordCount > 0 then begin
				vegsokod	:= vegsokod + ''''+Query2.FieldByName('SA_KOD').AsString+''',';
			end;
			kodstr	:= copy(kodstr, pozic+1, 99999);
			pozic 	:= Pos(',', kodstr);
		end;
		szamlakod	:= StrToIntdef(kodstr,0);
		szamlaszam	:= Format('%5.5d', [szamlakod]);
		Query_Run(Query2, 'SELECT SA_KOD FROM SZFEJ WHERE SA_KOD LIKE ''%'+szamlaszam+'%'' ');
		if Query2.RecordCount > 0 then begin
			vegsokod	:= vegsokod + ''''+Query2.FieldByName('SA_KOD').AsString+''',';
		end;
		vegsokod	:= vegsokod +'''aaa''';
		Query_Run(Query2, 'SELECT SA_KOD, SA_UJKOD,SA_SAEUR,SA_VEADO,SA_EUADO,SA_KIDAT,SA_FIMOD FROM SZFEJ '+
		' WHERE ( SA_TEDAT >= ''2016.01.01.'' ) AND SA_RESZE = 0  AND SA_FLAG  <> ''1'' /*AND SA_ATAD = 0 */   AND SA_KOD IN ('+vegsokod+') ' +
		' UNION SELECT SA_KOD, SA_UJKOD,SA_SAEUR,SA_VEADO,SA_VEADO,sa_kidat,SA_FIMOD FROM SZFEJ2 WHERE ( SA_TEDAT >= ''2016.01.01.'' ) '+
    ' AND SA_FLAG  <> ''1''  /* AND SA_ATAD = 0*/  AND SA_KOD IN ('+vegsokod+')'+
		' ORDER BY SA_KOD ');
	  end;  // else

  // ---------- SZ�ML�K FELDOLGOZ�SA ---------- //
	sorszam			:= 2;
	kilepes			:= false;
	SG1.RowCount	:= 1;
	SG1.Rows[0].Clear;
	regiszamla	:= '';  // az el�z�leg feldolgozott sz�mla
	Query_Run(Query4, 		'SELECT SA_KOD FROM SZFEJ2');
	Query_Run(QueryVevo, 	'SELECT *, case when isnull(VE_AORSZ,'''')='''' then V_ORSZ else VE_AORSZ end ADO_ORSZ FROM VEVO');

	Query2.DisableControls;
	while ( ( not Query2.Eof ) and (not kilepes) ) do begin
		Application.ProcessMessages;
		// Ellen�rizz�k, hogy feldolgoz�sra ker�lt-e m�r
		if regiszamla = Query2.FieldByName('SA_KOD').AsString then begin
			Query2.Next;
			Continue;
		end;
		JSzamla	:= TJSzamla.Create(Self);
		osszesitett	:= Query4.Locate('SA_KOD', Query2.FieldByName('SA_KOD').AsString, []);
		if not osszesitett then begin
			JSzamla.FillSzamla(Query2.FieldByName('SA_UJKOD').AsString);
		end else begin
			JSzamla.FillOsszSzamla(Query2.FieldByName('SA_UJKOD').AsString);
		end;
		if Query2.FieldByName('SA_KOD').AsString = '' then begin
			Query2.Next;
			Continue;
		end;
		Label1.Caption	:= Query2.FieldByName('SA_KOD').AsString;
		Label1.Update;
		str	:= JSzamla.vevokod;
		if str = '' then begin
			if QueryVevo.Locate('V_NEV', Jszamla.vevonev, []) then begin
				str	:= QueryVevo.FieldByName('V_KOD').AsString;
			end else begin
				hianysz := SG1.Cols[1].IndexOf(Jszamla.vevonev);
				if 0 > hianysz then begin
					Application.CreateForm(THianyVevobeDlg, HianyVevobeDlg);
					HianyVevobeDlg.Tolt(Jszamla.vevonev, Jszamla.vevoirsz+' '+Jszamla.vevovaros+' '+ JSzamla.vevocim);
					HianyVevobeDlg.ShowModal;
					if HianyVevobeDlg.vevokod <> '' then begin
						str	:= HianyVevobeDlg.vevokod;
						SG1.RowCount	:= SG1.RowCount + 1;
						SG1.Cells[0,SG1.RowCount-1]	:= str;
						SG1.Cells[1,SG1.RowCount-1]	:= Jszamla.vevonev;
					  end; // if
					HianyVevobeDlg.Destroy;
				  end
        else begin
					// Megtal�ltuk a nevet
					str	:= SG1.Cells[0,hianysz];
				  end;  // else
			  end; // else
  		end;  // if  str = ''
		orszag		:= 'HU';
		if QueryVevo.Locate('V_KOD', str, []) then begin
      // if QueryVevo.FieldByName('V_NEV').AsString = JSzamla.vevonev then
      if (QueryVevo.FieldByName('V_NEV').AsString = JSzamla.vevonev) or (JSzamla.allapot = 3) then  // sztorn� sz�ml�n�l n�vv�ltoz�s eset�n lehet elt�r�s
  			// orszag		:= QueryVevo.FieldByName('V_ORSZ').AsString
           orszag:= QueryVevo.FieldByName('ADO_ORSZ').AsString
      else begin
//    		Hibakiiro(Query2.FieldByName('SA_KOD').AsString + ' Rossz vev�k�d!  ' +FormatDateTime('nn.ss.zzz', now - t));

    		ListBox1.Lines.Add(Query2.FieldByName('SA_KOD').AsString + ' Elt�r� vev�n�v! (T�rzs-Sz�mla) ' );
    		ListBox1.Lines.Add('__'+QueryVevo.FieldByName('V_NEV').AsString+' - '+JSzamla.vevonev );
        inc(hiba);
  			// Query2.Next;
	  		// Continue;


        end;  // else
  		end;   // if QueryVevo.Locate

    v_belf:=(orszag	= 'HU');
		v_eus:=(Pos(','+orszag+',', ','+EgyebDlg.Read_SZGrid( '999', '740' )+',') > 0 );
    v_3orsz:= (not v_belf)and(not v_eus);
    EURszamla:= (Query2.FieldByName('SA_SAEUR').AsString='1' );
    // Ez egy csapda elker�l�se, ugyanis a JSzamla.szamla_valutanem a sz�mlasorokb�l veszi a devizanemet, viszont a
    // nyomtat�skor SA_SAEUR alapj�n dolgozunk, vagyis a felhaszn�l� ett�l f�gg�en l�t HUF vagy EUR sz�ml�t.
    if EURszamla then szamla_devizanem := 'EUR'
    else szamla_devizanem := 'HUF';
    // --------------------------------------------------------------------------------------------------
    adoalany:=  (Query2.FieldByName('SA_VEADO').AsString<>'' )or(Query2.FieldByName('SA_EUADO').AsString<>'' );
    if JSzamla.allapot = 3 then begin // sztorno
        szorzo	:= -1;
        storno_string:='1';
        end
    else begin
        szorzo	:= 1;
        storno_string:='0';
        end;
    sev:= EgyebDlg.EvSzam;
    // ---------- SZ�MLA FEJ T�ROL�SA -----------//
     KulcsFej:= TDictCSVLine.Create;
     KulcsFej.Clear;
     KulcsFej.Add('01_tipus', '1'); // vev� sz�mla
     KulcsFej.Add('02_nev', JSzamla.vevonev);
     // KulcsFej.Add('03_foszamla', JSzamla.vevokod);
     // �gy t�nik ide a sz�mlat�k�rb�l kell az idev�g� f�k�nyvi sz�mot tenni
     KulcsFej.Add('03_foszamla', GetFoszamla(szamla_devizanem, v_belf));

     KulcsFej.Add('04_szamlaid', Query2.FieldByName('SA_KOD').AsString);
     KulcsFej.Add('05_iktatoszam', '');
     KulcsFej.Add('06_szamlaszam', Query2.FieldByName('SA_KOD').AsString);
     // NagyP 20160407 - A k�nyvel�snek gondot okozott, hogy a k�szp�nzes t�telek beker�lnek a p�nzt�r napl�ba,
     // ugyanakkor a Kulcs H�zip�nzt�r is �tadja a f�k�nyvnek, �gy dupl�n vannak.
     // �gy (�tutal�sos t�telk�nt) csak a sz�ll�t� napl�ba ker�l.
     // KulcsFej.Add('07_fizmodnev', Query2.FieldByName('SA_FIMOD').AsString);
     KulcsFej.Add('07_fizmodnev', '�tutal�s');

     KulcsFej.Add('08_teljesites', FuvarosDatum_to_Kulcs(JSzamla.teljesites));
     KulcsFej.Add('09_kelt', FuvarosDatum_to_Kulcs(JSzamla.kiadas));
     KulcsFej.Add('10_esedekesseg', FuvarosDatum_to_Kulcs(JSzamla.esedekesseg));

     // KulcsFej.Add('11_elsz_id_zaro_datum', '');
     KulcsFej.Add('11_elsz_id_zaro_datum', GetElszamolasiDatum(JSzamla.vevokod, osszesitett));
     if szamla_devizanem = 'HUF' then begin
         // KulcsFej.Add('12_valutabrutto', Format('%.2f',[szorzo * JSzamla.ossz_brutto]));
         KulcsFej.Add('12_valutabrutto', Format('%.2f',[JSzamla.ossz_brutto]));  // Sztorn� sz�ml�n�l eleve negat�v
         end;
     if szamla_devizanem = 'EUR' then begin
         // KulcsFej.Add('12_valutabrutto', Format('%.2f',[szorzo * JSzamla.ossz_bruttoeur]));
         KulcsFej.Add('12_valutabrutto', Format('%.2f',[JSzamla.ossz_bruttoeur])); // Sztorn� sz�ml�n�l eleve negat�v
         end;
     KulcsFej.Add('13_valutanem', szamla_devizanem);
     // KulcsFej.Add('14_arfolyam', Format('%.2f',[JSzamla.szamla_valuta_arfolyam]));
     // mivel �gy sz�moljuk vissza az �rfolyamot, kev�s a 2 tizedes: a Kulcs beolvas�s visszaszorz�s�n�l elt�r�s ad�dik
     if szamla_devizanem='HUF' then
        KulcsFej.Add('14_arfolyam', '1')
     else begin
        // KulcsFej.Add('14_arfolyam', Format('%.5f',[JSzamla.szamla_valuta_arfolyam]));
        if osszesitett then ArfFormat:= '%.5f' // hogy az "�tlag�rfolyam"-mal visszaszorozva megkapjuk a sz�ml�n szerepl� HUF �rt�ket
        else ArfFormat:=  '%.2f';  // hogy egyezzen az MNB �rfolyammal
        KulcsFej.Add('14_arfolyam', Format(ArfFormat,[JSzamla.szamla_valuta_arfolyam]));
        end;
     KulcsFej.Add('15_reszlegszam', '');
     KulcsFej.Add('16_munkaszam', '');
     KulcsFej.Add('17_irany', 'K'); // kimen�
     KulcsFej.Add('18_esedekesseg_napokban', IntToStr(DatumKul(JSzamla.esedekesseg, JSzamla.kiadas)));
     if JSzamla.Afasorok.Count>0 then AFAs:='1'
     else AFAs:='0';
     KulcsFej.Add('19_afas', AFAs);
     // KulcsFej.Add('20_netto', Format('%.0f',[szorzo * JSzamla.ossz_netto]));  // ez mindenk�pp HUF-ban van
     // KulcsFej.Add('21_brutto', Format('%.0f',[szorzo * JSzamla.ossz_brutto]));
     // Sztorn� sz�ml�n�l a JSzamlasor.ossz_netto, ossz_brutto m�r eleve negat�v
     KulcsFej.Add('20_netto', Format('%.0f',[JSzamla.ossz_netto]));  // ez mindenk�pp HUF-ban van
     KulcsFej.Add('21_brutto', Format('%.0f',[JSzamla.ossz_brutto]));

     KulcsFej.Add('22_rontott', '0');
     KulcsFej.Add('23_storno', storno_string);
     KulcsFej.Add('24_stornoszam', '');
     KulcsFej.Add('25_stornomode', '0'); // k�telez� �rt�k
     KulcsFej.Add('26_ugyfelkod', JSzamla.vevokod);
     KulcsFej.Add('27_helyesbito_osszeg', '');
     KulcsFej.Add('28_megjegyzes', '');
     KulcsFej.Add('29_eu_ados', '0'); // mindig 0
     KulcsFej.Add('30_penzforgalmi', ''); // ??
     KulcsFej.Add('31_kezhezvetel', '');
     KulcsFejList.Add(KulcsFej);
     // ---------- �GYF�L ADATOK T�ROL�SA -----------//
     KulcsUgyfel:= TDictCSVLine.Create;
     KulcsUgyfel.Clear;
     KulcsUgyfel.Add('01_ugyfelkod', JSzamla.vevokod);
     KulcsUgyfel.Add('02_adoszam', JSzamla.vevoadoszam);
     vevocim:=JSzamla.vevoirsz + ' '+ JSzamla.vevovaros + ' '+ JSzamla.vevocim;
     KulcsUgyfel.Add('03_cim', vevocim);
     PartnerKontakt:= GetPartnerKontakt(JSzamla.vevokod);
     KulcsUgyfel.Add('04_ugyintezo', PartnerKontakt.ugyintezo);
     KulcsUgyfel.Add('05_telefon', PartnerKontakt.telefon);
     KulcsUgyfel.Add('06_email', PartnerKontakt.email);
     KulcsUgyfel.Add('07_bankszamla', JSzamla.vevobank);
     // KulcsUgyfel.Add('08_eu_adoszam', JSzamla.vevoEUadoszam);
     KulcsUgyfel.Add('08_eu_adoszam', JSzamla.adoorszag+JSzamla.vevoEUadoszam);
     KulcsUgyfel.Add('09_orszag_iso', JSzamla.vevoorszag);
     KulcsUgyfel.Add('10_orszag', JSzamla.vevoorszag);
     // megn�zz�k hogy szerepel-e m�r benne
     i:=0;
     found:= False;
     while (i <= KulcsUgyfelList.Count-1) and not found do begin
        if (KulcsUgyfelList[i] as TDictCSVLine)['01_ugyfelkod'] = KulcsUgyfel['01_ugyfelkod'] then
           Found:= True;
        Inc(i);
        end;  // while
     if not found then
        KulcsUgyfelList.Add(KulcsUgyfel);
    // ------------------------------------------//
		// V�gigmegy�nk a sz�mla sorain �s ki�rjuk az adatokat

		if JSzamla.vannaksorok then begin
			for i := 0 to Jszamla.Szamlasorok.Count - 1 do begin
				JSzamlasor	:= (JSzamla.SzamlaSorok[i] as TJszamlaSor);
				if JSzamlaSor.mennyiseg <> 0 then begin
					// A belf�ldi �s az EU tags�g eld�nt�se
          s_3orsz:=(JSzamlaSor.nemeus=1);
          GetFokonyviSzamok(v_eus, EURszamla, s_3orsz, v_3orsz, v_belf, adoalany, JSzamla.allapot, str9, str11, str12, AFAFokonyviKod);
          // ---------- SZ�MLASOR ADATOK T�ROL�SA -----------//
          KulcsTetel:= TDictCSVLine.Create;
          KulcsTetel.Clear;
          inc(tetelsorszam);
          KulcsTetel.Add('01_id', IntToStr(tetelsorszam));
          KulcsTetel.Add('02_fokszamnetto', str9);
          // KulcsTetel.Add('03_valutaertek', Format('%.2f',[szorzo * JSzamlasor.sorneteur]));
          // Sztorn� sz�ml�n�l a JSzamlasor.sorneteur m�r eleve negat�v

          // 2016-02-21: f�lre�rt�s, a sz�m�rt�k a sz�mla devizanem�ben kell.
          // KulcsTetel.Add('03_valutaertek', Format('%.2f',[JSzamlasor.sorneteur]));
          // if JSzamla.szamla_valutanem = 'HUF' then begin
          if szamla_devizanem = 'HUF' then begin
              KulcsTetel.Add('03_valutaertek', Format('%.2f',[JSzamlasor.sornetto]));
              end;
           if szamla_devizanem= 'EUR' then begin
              KulcsTetel.Add('03_valutaertek', Format('%.2f',[JSzamlasor.sorneteur]));
              end;
          afakulcs:= Format('%.0f',[Abs(JSzamlasor.afaszaz)]);
          KulcsTetel.Add('04_afakulcs', afakulcs);
          KulcsTetel.Add('05_fejid', Query2.FieldByName('SA_KOD').AsString);
          KulcsTetel.Add('06_fokszam_afa', str11);
          // KulcsAFA:= GetKulcsAFA(JSzamlasor.afakod);   //  sz�t�rb�l, SZ_FOKOD=102
          // Az �fak�d alapja a f�k�nyvi sz�m lesz
          KulcsAFA:= GetKulcsAFA(AFAFokonyviKod, afakulcs);   //  sz�t�rb�l, SZ_FOKOD=102
          KulcsTetel.Add('07_afakod', KulcsAFA.KulcsKod);
          KulcsTetel.Add('08_afanev', KulcsAFA.KulcsNev);
          KulcsTetel.Add('09_helyesbito', '0');
          KulcsTetel.Add('10_eloleg_szamla_szama', '');
          KulcsTetel.Add('11_megjegyzes', JSzamlasor.termeknev);
          KulcsTetel.Add('12_reszlegszam', '');
          KulcsTetel.Add('13_munkaszam', '');
          KulcsTetel.Add('14_masodlagos', '');
          KulcsTetel.Add('15_forditott', '0');
          KulcsTetelList.Add(KulcsTetel);
					// Query_Log_Str('Sz�mlaexport : '+Query2.FieldByName('SA_KOD').AsString+','+Query2.FieldByName('SA_UJKOD').AsString+','+ExtractFileName(fn), 0, true);
					Inc(sorszam);
				end;
			end;
	 	end;
		JSzamla.Destroy;
		// A sz�mla bejegyz�se �tadottra
    // 2016-02-21
	 		Query_Run(Query1, 'UPDATE SZFEJ  SET SA_ATAD = 1, SA_ATDAT = '''+EgyebDlg.MaiDatum+''', SA_ATNEV = '''+copy(ExtractFileName(fn_fej),1,30)+''' '+
				' WHERE SA_KOD = '''+Query2.FieldByName('SA_KOD').AsString+''' ');
		 if osszesitett then begin
			Query_Run(Query1, 'UPDATE SZFEJ2 SET SA_ATAD = 1 WHERE SA_KOD = '''+Query2.FieldByName('SA_KOD').AsString+''' ');
		  end;
    ListBox1.Lines.Add(Query2.FieldByName('SA_KOD').AsString + ' OK');
    Inc(atadva);
		Query2.Next;
	end;
  ListBox1.Lines.Add('');
  ListBox1.Lines.Add('�tadva: '+IntToStr(atadva) );
  ListBox1.Lines.Add('Hiba  : '+IntToStr(hiba) );
	Query2.EnableControls;

  // SaveToCSV(KulcsFejList, fn_fej, True);
  // SaveToCSV(KulcsTetelList, fn_tetel, True);
  // SaveToCSV(KulcsUgyfelList, fn_ugyfel, True);

  KulcsFejList.SaveToCSV(fn_fej, True);
  KulcsTetelList.SaveToCSV(fn_tetel, True);
  KulcsUgyfelList.SaveToCSV(fn_ugyfel, True);

	Label1.Caption	:= '';
	Label1.Update;
  if FileExists(fn_fej) and not kilepes then begin
    EgyebDlg.WriteLogFile(EgyebDlg.NaploPath+'\k�nyvel�s_�tad�s.log',fn_fej+' '+IntToStr(sorszam)+' db '+EgyebDlg.user_name+' '+DateTimeToStr(now)+' File:'+fn_fej+' �s t�rsai');
	  NoticeKi('Az export�l�s befejez�d�tt!');
    end
  else begin
    EgyebDlg.WriteLogFile(EgyebDlg.NaploPath+'\k�nyvel�s_�tad�s.log','Hib�s �tad�s! '+EgyebDlg.user_name+' '+DateTimeToStr(now)+' File:'+fn_fej+' �s t�rsai');
	  NoticeKi('Az export�l�s nem siker�lt!');
    end;
	kilepes	:= true;
 finally
    KulcsFejList.Free;
    KulcsTetelList.Free;
    KulcsUgyfelList.Free;
    // ezeket a t�mbbel egy�tt felszabad�tjuk, azt hiszem.
    // KulcsFej.Free;
    // KulcsTetel.Free;
    // KulcsUgyfel.Free;
    end;  // try-finally
end;

function TNyitoExportDlg.GetFoszamla(CCY: string; Belfold_e: boolean): string;
begin
  if CCY='HUF' then Result:='311'
  else Result:='312';

  // R�gi:
  { if CCY='HUF' then begin
     if Belfold_e then Result:='311'
     else Result:='316';
     end
   else begin
     if Belfold_e then Result:='312'
     else Result:='317';
     end;  // else
     }
end;

{
function TNyitoExportDlg.SaveToCSV(Dict: TObjectList; FN: string; IncludeHeader: boolean): boolean;
var
  F	: TextFile;
  i: integer;
begin
	if FileExists(FN) then begin
		DeleteFile(PChar(FN));
	  end;
	// $I-
	AssignFile(F, FN);
	Rewrite(F);
	// $I+
	if IOResult <> 0 then begin
    Result:= False;
		Exit;
  	end;
  if (Dict.Count>0) and IncludeHeader then begin
    Writeln(F, TDictCSV(Dict[0]).GetHeaderCSV);
    end;  // if
  for i:=0 to Dict.Count -1 do begin
  	Writeln(F, TDictCSV(Dict[i]).GetDataCSV);
    end;  // for
	CloseFile(F);
end;
}

function TNyitoExportDlg.GetKulcsAFA(AFAFokonyviKod, afakulcs: string): TKulcsAFA;
var
   OsszetettKulcs: string;
begin
   OsszetettKulcs:= Trim(AFAFokonyviKod)+'/'+Trim(afakulcs);  // pl. 4671/27
   Result.KulcsKod := EgyebDlg.Read_SZGrid('102', OsszetettKulcs);
   Result.KulcsNev := Query_select2('SZOTAR', 'SZ_FOKOD', 'SZ_ALKOD', '102', OsszetettKulcs, 'SZ_LEIRO')
end;

procedure TNyitoExportDlg.GetFokonyviSzamok(v_eus, EURszamla, s_3orsz, v_3orsz, v_belf, adoalany: boolean; szamla_allapot: integer; var str9, str11, str12, AFAFokonyviKod: string);
var
  str: string;
  str8, str10: string; // ezek csak bel�l kellenek, ha storno a sz�mla
begin
    str12:='';
    if v_eus then            // EU-s
    begin
      if EURszamla then       // EUR
      begin
        str8	:= '312';
        str9	:= '94' ;
        str10	:= '312';
        // str11	:= '4653';
        str11	:= '4673';
        str12 := '5';
        if s_3orsz then
        begin
          // str11	:= '4658';
          str11	:= '4678';
          str12 := '3';
        end ;
      end
      else                    // HUF
      begin
        str8	:= '311';
        str9	:= '94' ;
        str10	:= '311';
        // str11	:= '4653';
        str11	:= '4673';
        str12 := '5';
        if s_3orsz then
        begin
          // str11	:= '4658';
          str11	:= '4678';
          str12 := '3';
        end ;
      end;
    end;
    /////////////////////
    if v_belf then            // belf�ldi
    begin
      if EURszamla then       // EUR
      begin
        str8	:= '312';
        str9	:= '91' ;
        str10	:= '312';
        if s_3orsz then
        begin
          // str11	:= '4658';
          str11	:= '4678';
          str12 := '3';
        end
        else
        begin
          // str11	:= '467';
          str11	:= '4671';
        end;
      end
      else                    // HUF
      begin
        str8	:= '311';
        str9	:= '91' ;
        str10	:= '311';
        // str11	:= '467';
        str11	:= '4671';
        if s_3orsz then
        begin
          // str11	:= '4658';
          str11	:= '4678';
          str12 := '3';
        end ;
      end;
    end;
    /////////////////////
    if v_3orsz then            // harmadik orsz�g
    begin
      if EURszamla then       // EUR
      begin
        str8	:= '312';
        str9	:= '94' ;
        str10	:= '312';
        // str11	:= '4659';
        str11	:= '4679';
        str12 := '5';
      end
      else                    // HUF
      begin
        str8	:= '311';
        str9	:= '94' ;
        str10	:= '311';
        // str11	:= '467';
        str11	:= '4671';
      end;
    end;
    /////////////////////
    if not adoalany then            // nem ad�alany
    begin
      if EURszamla then       // EUR
      begin
        str8	:= '312';
        str9	:= '91' ;
        str10	:= '312';
        // str11	:= '467';
        str11	:= '4671';
      end
      else                    // HUF
      begin
        str8	:= '311';
        str9	:= '91' ;
        str10	:= '311';
        // str11	:= '467';
        str11	:= '4671';
      end;
    end;
    AFAFokonyviKod:= str11; // sztorn�n�l is
    // szerintem a KULCS-n�l nem kell f�k�nyvi sz�mokat cser�lni, am�gy is csak az egyiket kapja meg

    {if JSzamla.allapot = 3 then begin
      // Storn�n�l
      str		:= str8;
      str8	:= str9;
      str9	:= str;
      str		:= str10;
      str10	:= str11;
      str11	:= str;
      end;
    }
end;


procedure TNyitoExportDlg.Feldolgozas_Servantes;
var
	fn		   	: string;
	EX	  		: TJExcel;
	sorszam		: integer;
	i			: integer;
	j			: integer;
	str			: string;
	hianysz		: integer;
	filterstr	: string;
	valutanem	: string;
	str8		: string;
	str9		: string;
	str10	   	: string;
	str11	   	: string;
	str12	   	: string;
	vegsokod	: string;
	kodstr		: string;
	pozic		: integer;
	szamlakod	: integer;
	szamlaszam	: string;
	tedat		: string;
	esdat		: string;
	kidat		: string;
	kodkiegstr	: string;
	kodelostr	: string;
	belfoldi	: integer;
	eus			: integer;
	orszag		: string;
	str2		: string;
	regiszamla	: string;
	osszesitett	: boolean;
	JSzamlaSor	: TJSzamlaSor;
	t			: TDateTime;
	szorzo		: double;
  v_belf,v_eus,v_3orsz,s_3orsz, EURszamla, adoalany : boolean;
  sql, sev: string;
  code, hiba,atadva: integer;
begin
  hiba:=0;
  atadva:=0;
	if M1.Text	= '' then begin
		NoticeKi('A kimeneti �lom�ny nem lehet �res!');
		M1.SetFocus;
		Exit;
	end;
	if FileExists(M1.Text) then begin
		if NoticeKi('L�tez� �llom�ny! Fel�l�rjuk?', NOT_QUESTION) <> 0 then begin
			Exit;
		end;
		DeleteFile(PChar(M1.Text));
	end;

	// Duplik�lt sz�ml�k keres�se (FT + EUR is van benne)
	filterstr	:= '''JS09/02169''';
  if  EgyebDlg.TESZTPRG then
	  M1.Text 		:=  EgyebDlg.DocumentPath+'\KONYVELES'  ;

	fn	:= m1.Text;
  code:=GetFileAttributes(PChar(fn));
  if code = -1 then
  begin
      NoticeKi('A k�nyvt�r nem l�tezik!');
      Exit;
  end;
{  if not DirectoryExists(fn) then
    if not CreateDir(fn) then
    begin
      NoticeKi('A k�nyvt�r nem l�tezik, �s nem siker�lt l�rtehozni!');
      Exit;
    end;    }
  fn:=M1.Text+'\'+MaskEdit3.Text;
	EX := TJexcel.Create(Self);
	if not EX.OpenXlsFile(fn) then begin
		NoticeKi('Nem siker�lt megnyitnom az Excel �llom�nyt!');
		Exit;
	end;
	EX.ColNumber	:= 30;
	EX.InitRow;

	EX.SetRowcell( 1,  'KOD');
	EX.SetRowcell( 2,  'KORZET');
	EX.SetRowcell( 3,  'BIZONYLAT');
	EX.SetRowcell( 4,  'KIEGYBIZ');
	EX.SetRowcell( 5,  'TELJKELT');
	EX.SetRowcell( 6,  'ESEDKELT');
	EX.SetRowcell( 7,  'SZOVEG');
	EX.SetRowcell( 8,  'TARTFSZ');
	EX.SetRowcell( 9,  'KOVFSZ');
	EX.SetRowcell( 10, 'TARTAFAFSZ');
	EX.SetRowcell( 11, 'KOVAFAFSZ');
	EX.SetRowcell( 12, 'NETTO');
	EX.SetRowcell( 13, 'AFA_FT');
	EX.SetRowcell( 14, 'AFAKULCS');
	EX.SetRowcell( 15, 'BRUTTO');
	EX.SetRowcell( 16, 'VALUTA');
	EX.SetRowcell( 17, 'VALUAFA');
	EX.SetRowcell( 18, 'VALUTANEM');
	EX.SetRowcell( 19, 'PART_NEV');
	EX.SetRowcell( 20, 'PART_CIM');
	EX.SetRowcell( 21, 'BIZKELT');
	EX.SetRowcell( 22, 'BIZBELSO');
	EX.SetRowcell( 23, 'AFANUL');
	EX.SetRowcell( 24, 'AFAKELT');
	EX.SetRowcell( 25, 'TECHKELT');
	EX.SetRowcell( 26, 'BEKELT');
	EX.SaveRow(1);

	if M2.Text = '' then begin
		Query_Run(Query2, 'SELECT SA_KOD, SA_UJKOD,SA_SAEUR,SA_VEADO,SA_EUADO, sa_kidat FROM SZFEJ '+
		' WHERE ( SA_TEDAT >= ''2009.01.01.'' ) AND SA_RESZE = 0  AND SA_FLAG  <> ''1'' AND SA_ATAD = 0 '+
		' UNION SELECT SA_KOD, SA_UJKOD,SA_SAEUR,SA_VEADO,SA_VEADO,sa_kidat FROM SZFEJ2 WHERE ( SA_TEDAT >= ''2009.01.01.'' ) AND SA_FLAG  <> ''1'' AND SA_ATAD = 0  '+
		' ORDER BY SA_KOD');
	end else begin
		// V�gigmegy�nk a megadott sz�mlasz�mokon �s kigy�jtj�k a megfelel� sz�ml�kat
		vegsokod	:= '';
		kodstr		:= M2.Text;
		pozic 		:= Pos(',', kodstr);
		while pozic > 0 do begin
			szamlakod	:= StrToIntdef(copy(kodstr,1,pozic-1),0);
			szamlaszam	:= Format('%5.5d', [szamlakod]);
			Query_Run(Query2, 'SELECT SA_KOD FROM SZFEJ WHERE SA_KOD LIKE ''%'+szamlaszam+'%'' ');
			if Query2.RecordCount > 0 then begin
				vegsokod	:= vegsokod + ''''+Query2.FieldByName('SA_KOD').AsString+''',';
			end;
			kodstr	:= copy(kodstr, pozic+1, 99999);
			pozic 	:= Pos(',', kodstr);
		end;
		szamlakod	:= StrToIntdef(kodstr,0);
		szamlaszam	:= Format('%5.5d', [szamlakod]);
		Query_Run(Query2, 'SELECT SA_KOD FROM SZFEJ WHERE SA_KOD LIKE ''%'+szamlaszam+'%'' ');
		if Query2.RecordCount > 0 then begin
			vegsokod	:= vegsokod + ''''+Query2.FieldByName('SA_KOD').AsString+''',';
		end;
		vegsokod	:= vegsokod +'''aaa''';
		Query_Run(Query2, 'SELECT SA_KOD, SA_UJKOD,SA_SAEUR,SA_VEADO,SA_EUADO,SA_KIDAT FROM SZFEJ '+
		' WHERE ( SA_TEDAT >= ''2009.01.01.'' ) AND SA_RESZE = 0  AND SA_FLAG  <> ''1'' AND SA_ATAD = 0 AND SA_KOD IN ('+vegsokod+') ' +
		' UNION SELECT SA_KOD, SA_UJKOD,SA_SAEUR,SA_VEADO,SA_VEADO,sa_kidat FROM SZFEJ2 WHERE ( SA_TEDAT >= ''2009.01.01.'' ) AND SA_FLAG  <> ''1'' AND SA_ATAD = 0 AND SA_KOD IN ('+vegsokod+')'+
		' ORDER BY SA_KOD ');
	  end;  // else
	sorszam			:= 2;
	kilepes			:= false;
	SG1.RowCount	:= 1;
	SG1.Rows[0].Clear;
	regiszamla	:= '';  // az el�z�leg feldolgozott sz�mla
	Query_Run(Query4, 		'SELECT SA_KOD FROM SZFEJ2');
	Query_Run(QueryVevo, 	'SELECT * FROM VEVO');

	Query2.DisableControls;
	while ( ( not Query2.Eof ) and (not kilepes) ) do begin
		Application.ProcessMessages;
		// Ellen�rizz�k, hogy feldolgoz�sra ker�lt-e m�r
		if regiszamla = Query2.FieldByName('SA_KOD').AsString then begin
			Query2.Next;
			Continue;
		end;
		JSzamla	:= TJSzamla.Create(Self);
		osszesitett	:= Query4.Locate('SA_KOD', Query2.FieldByName('SA_KOD').AsString, []);
		if not osszesitett then begin
			JSzamla.FillSzamla(Query2.FieldByName('SA_UJKOD').AsString);
		end else begin
			JSzamla.FillOsszSzamla(Query2.FieldByName('SA_UJKOD').AsString);
		end;
		if Query2.FieldByName('SA_KOD').AsString = '' then begin
			Query2.Next;
			Continue;
		end;
		Label1.Caption	:= Query2.FieldByName('SA_KOD').AsString;
		Label1.Update;
//		Hibakiiro(Query2.FieldByName('SA_KOD').AsString + '  ' +FormatDateTime('nn.ss.zzz', now - t));
		//ListBox1.Items.Add(Query2.FieldByName('SA_KOD').AsString + '  ' +FormatDateTime('nn.ss.zzz', now - t));
		str	:= JSzamla.vevokod;
		if str = '' then begin
			if QueryVevo.Locate('V_NEV', Jszamla.vevonev, []) then begin
				str	:= QueryVevo.FieldByName('V_KOD').AsString;
			end else begin
				hianysz := SG1.Cols[1].IndexOf(Jszamla.vevonev);
				if 0 > hianysz then begin
					Application.CreateForm(THianyVevobeDlg, HianyVevobeDlg);
					HianyVevobeDlg.Tolt(Jszamla.vevonev, Jszamla.vevoirsz+' '+Jszamla.vevovaros+' '+ JSzamla.vevocim);
					HianyVevobeDlg.ShowModal;
					if HianyVevobeDlg.vevokod <> '' then begin
						str	:= HianyVevobeDlg.vevokod;
						SG1.RowCount	:= SG1.RowCount + 1;
						SG1.Cells[0,SG1.RowCount-1]	:= str;
						SG1.Cells[1,SG1.RowCount-1]	:= Jszamla.vevonev;
					  end; // if
					HianyVevobeDlg.Destroy;
				  end
        else begin
					// Megtal�ltuk a nevet
					str	:= SG1.Cells[0,hianysz];
				  end;  // else
			end; // else
		end;  // if  str = ''
		orszag		:= 'HU';
		if QueryVevo.Locate('V_KOD', str, []) then begin
      // if QueryVevo.FieldByName('V_NEV').AsString = JSzamla.vevonev then
      if (QueryVevo.FieldByName('V_NEV').AsString = JSzamla.vevonev) or (JSzamla.allapot = 3) then  // sztorn� sz�ml�n�l n�vv�ltoz�s eset�n lehet elt�r�s
  			orszag		:= QueryVevo.FieldByName('V_ORSZ').AsString
      else
      begin
//    		Hibakiiro(Query2.FieldByName('SA_KOD').AsString + ' Rossz vev�k�d!  ' +FormatDateTime('nn.ss.zzz', now - t));
    		ListBox1.Lines.Add(Query2.FieldByName('SA_KOD').AsString + ' Rossz vev�n�v! (T�rzs-Sz�mla) ' );
    		ListBox1.Lines.Add('__'+QueryVevo.FieldByName('V_NEV').AsString+' - '+JSzamla.vevonev );
        inc(hiba);
  			Query2.Next;
	  		Continue;
      end;
		end;   // if QueryVevo.Locate

    v_belf:=(orszag	= 'HU');
		v_eus:=(Pos(','+orszag+',', ','+EgyebDlg.Read_SZGrid( '999', '740' )+',') > 0 );
    v_3orsz:= (not v_belf)and(not v_eus);
    EURszamla:= (Query2.FieldByName('SA_SAEUR').AsString='1' );
    adoalany:=  (Query2.FieldByName('SA_VEADO').AsString<>'' )or(Query2.FieldByName('SA_EUADO').AsString<>'' );

		str2		:= EgyebDlg.Read_SZGrid( '999', '740' );
    //sev:= copy( Query2.FieldByName('SA_KIDAT').AsString,1,4);
    sev:= EgyebDlg.EvSzam;
		// V�gigmegy�nk a sz�mla sorain �s ki�rjuk az adatokat
		if JSzamla.vannaksorok then begin
			for i := 0 to Jszamla.Szamlasorok.Count - 1 do begin
				for j := 1 to 26 do begin
					EX.SetRowcell(j, '');
				end;
				JSzamlasor	:= (JSzamla.SzamlaSorok[i] as TJszamlaSor);
				if JSzamlaSor.mennyiseg <> 0 then begin
					// A belf�ldi �s az EU tags�g eld�nt�se
          s_3orsz:=(JSzamlaSor.nemeus=1);
//					EX.SetRowcell( 1, IntToStr(StrToIntDef(str,0)));
					EX.SetRowcell( 1, JSzamla.vevokod);
					EX.SetRowcell( 2, '');
					kodkiegstr	:= '';
					kodelostr	:= '';

					{if CheckBox2.Checked and (sev<'2013') then begin         // "/�vsz�m" hozz�f�z�se a sz�mlasz�mhoz
						kodkiegstr	:= '/'+copy(EgyebDlg.EvSzam, 3,2);
						kodelostr	:= '''';
					end;
					if CheckBox1.Checked and (sev<'2013')  then begin        //  Sz�mlasz�m JS... n�lk�l
						EX.SetRowcell( 3, kodelostr + IntToStr(StrToIntdef(copy(Query2.FieldByName('SA_KOD').AsString,6,12),0))+kodkiegstr);
					end else begin
						EX.SetRowcell( 3, Query2.FieldByName('SA_KOD').AsString);
					end;}

          EX.SetRowcell( 3, Query2.FieldByName('SA_KOD').AsString);

					EX.SetRowcell( 4, '');
					// �sszes�tett sz�mla eset�n az �sszes�tett d�tumokat kell venni
					tedat	:= JSzamla.teljesites;
					esdat	:= JSzamla.esedekesseg;
					kidat	:= JSzamla.kiadas;

					EX.SetRowcell( 5, copy(tedat,1,10));
					EX.SetRowcell( 6, copy(esdat,1,10));
					EX.SetRowcell( 7, JSzamlasor.termeknev);
          ///////////////////////////////////
          if EURszamla then
			    		valutanem	:= 'EUR'
          else
			    		valutanem	:= 'HUF';
          /////////////////////////////////////
          str12:='';
          if v_eus then            // EU-s
          begin
            if EURszamla then       // EUR
            begin
    					str8	:= '312';
		    			str9	:= '94' ;
				    	str10	:= '312';
  	    			str11	:= '4653';
              str12 := '5';
              if s_3orsz then
              begin
  	    				str11	:= '4658';
                str12 := '3';
              end ;
            end
            else                    // HUF
            begin
    					str8	:= '311';
		    			str9	:= '94' ;
				    	str10	:= '311';
	    				str11	:= '4653';
              str12 := '5';
              if s_3orsz then
              begin
  	    				str11	:= '4658';
                str12 := '3';
              end ;
            end;
          end;
          /////////////////////
          if v_belf then            // belf�ldi
          begin
            if EURszamla then       // EUR
            begin
    					str8	:= '312';
		    			str9	:= '91' ;
				    	str10	:= '312';
              if s_3orsz then
              begin
  	    				str11	:= '4658';
                str12 := '3';
              end
              else
              begin
  	    				str11	:= '467';
              end;
            end
            else                    // HUF
            begin
    					str8	:= '311';
		    			str9	:= '91' ;
				    	str10	:= '311';
	    				str11	:= '467';
              if s_3orsz then
              begin
  	    				str11	:= '4658';
                str12 := '3';
              end ;
            end;
          end;
          /////////////////////
          if v_3orsz then            // harmadik orsz�g
          begin
            if EURszamla then       // EUR
            begin
    					str8	:= '312';
		    			str9	:= '94' ;
				    	str10	:= '312';
  	    			str11	:= '4659';
              str12 := '5';
            end
            else                    // HUF
            begin
    					str8	:= '311';
		    			str9	:= '94' ;
				    	str10	:= '311';
	    				str11	:= '467';
            end;
          end;
          /////////////////////
          if not adoalany then            // nem ad�alany
          begin
            if EURszamla then       // EUR
            begin
    					str8	:= '312';
		    			str9	:= '91' ;
				    	str10	:= '312';
	    				str11	:= '467';
            end
            else                    // HUF
            begin
    					str8	:= '311';
		    			str9	:= '91' ;
				    	str10	:= '311';
	    				str11	:= '467';
            end;
          end;
					szorzo	:= 1;
					if JSzamla.allapot = 3 then begin
						// Storn�n�l
						str		:= str8;
						str8	:= str9;
						str9	:= str;
						str		:= str10;
						str10	:= str11;
						str11	:= str;
						szorzo	:= -1;
					end;
					EX.SetRowcell( 8,  str8);
					EX.SetRowcell( 9,  str9 );
					EX.SetRowcell( 10, str10);
					EX.SetRowcell( 11, str11);

					EX.SetRowcell( 28, '');
					if  valutanem = 'HUF' then begin
						// HUF eset�n nincs eur� �s a forint kerek�tett
						EX.SetRowcell( 12, Format('%.0f',[szorzo * JSzamlasor.sornetto]));
						EX.SetRowcell( 13, Format('%.0f',[szorzo * JSzamlasor.sorafa]));
						EX.SetRowcell( 14, Format('%.0f',[Abs(szorzo * JSzamlasor.afaszaz)]));
						EX.SetRowcell( 15, Format('%.0f',[szorzo * JSzamlasor.sorbrutto]));
						EX.SetRowcell( 16, '');
						EX.SetRowcell( 17, '');
  				 	EX.SetRowcell( 18, '');
					end else begin
						// EUR eset�n 2 tizedessel jelenik meg
						EX.SetRowcell( 12, Format('%.2f',[szorzo * JSzamlasor.sornetto]));
						EX.SetRowcell( 13, Format('%.2f',[szorzo * JSzamlasor.sorafa]));
						EX.SetRowcell( 14, Format('%.0f',[Abs(szorzo * JSzamlasor.afaszaz)]));
						EX.SetRowcell( 15, Format('%.2f',[szorzo * JSzamlasor.sorbrutto]));
						EX.SetRowcell( 16, Format('%.2f',[szorzo * JSzamlasor.sorneteur]));
						EX.SetRowcell( 17, Format('%.2f',[szorzo * JSzamlasor.sorafaeur]));
	  				EX.SetRowcell( 18, valutanem);
					end;
		//			EX.SetRowcell( 18, valutanem);
					EX.SetRowcell( 19, JSzamla.vevonev);
					EX.SetRowcell( 20, JSzamla.vevoirsz+' '+JSzamla.vevovaros+' '+JSzamla.vevocim);
					EX.SetRowcell( 21, copy(kidat,1,10));
					EX.SetRowcell( 22, '');
					EX.SetRowcell( 23, str12);
					EX.SetRowcell( 24, copy(tedat,1,10));
					EX.SetRowcell( 25, copy(tedat,1,10));
					EX.SetRowcell( 26, copy(kidat,1,10));
					EX.SaveRow(sorszam);
					Query_Log_Str('Sz�mlaexport : '+Query2.FieldByName('SA_KOD').AsString+','+Query2.FieldByName('SA_UJKOD').AsString+','+ExtractFileName(fn), 0, true);
					Inc(sorszam);
				end;
			end;
	 	end;
		JSzamla.Destroy;
		// A sz�mla bejegyz�se �tadottra
//		if not osszesitett then begin
			Query_Run(Query1, 'UPDATE SZFEJ  SET SA_ATAD = 1, SA_ATDAT = '''+EgyebDlg.MaiDatum+''', SA_ATNEV = '''+copy(ExtractFileName(fn),1,30)+''' '+
				' WHERE SA_KOD = '''+Query2.FieldByName('SA_KOD').AsString+''' ');
//		end else begin
		if osszesitett then begin
			Query_Run(Query1, 'UPDATE SZFEJ2 SET SA_ATAD = 1 WHERE SA_KOD = '''+Query2.FieldByName('SA_KOD').AsString+''' ');
		end;
    ListBox1.Lines.Add(Query2.FieldByName('SA_KOD').AsString + ' OK');
    Inc(atadva);
		Query2.Next;
	end;
  ListBox1.Lines.Add('');
  ListBox1.Lines.Add('�tadva: '+IntToStr(atadva) );
  ListBox1.Lines.Add('Hiba  : '+IntToStr(hiba) );
	Query2.EnableControls;
	EX.SaveFileAsXls(fn);
	EX.Free;
	Label1.Caption	:= '';
	Label1.Update;
  if FileExists(fn) and not kilepes then
  begin
    EgyebDlg.WriteLogFile(EgyebDlg.DocumentPath+'\k�nyvel�s_�tad�s.log',fn+' '+IntToStr(sorszam)+' db '+EgyebDlg.user_name+' '+DateTimeToStr(now));
	  NoticeKi('Az export�l�s befejez�d�tt!');
  end
  else
  begin
    EgyebDlg.WriteLogFile(EgyebDlg.DocumentPath+'\k�nyvel�s_�tad�s.log','Hib�s �tad�s! '+EgyebDlg.user_name+' '+DateTimeToStr(now));
	  NoticeKi('Az export�l�s nem siker�lt!');
  end;
	kilepes	:= true;
end;


end.



