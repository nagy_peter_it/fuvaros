unit VCARDParams;

interface

uses
	SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
	Forms, Dialogs, StdCtrls, Buttons, Mask, DB, DBTables, J_ALFORM, ADODB,
  	ExtCtrls, FelForm;
	
type
	TVCARDParamsDlg = class(TJ_AlformDlg)
	BitElkuld: TBitBtn;
	BitKilep: TBitBtn;
    Query1: TADOQuery;
    Label6: TLabel;
    cbBeosztas: TComboBox;
    GroupBox1: TGroupBox;
    CheckBox1: TCheckBox;
    CheckBox2: TCheckBox;
    GroupBox2: TGroupBox;
    CheckBox3: TCheckBox;
    CheckBox4: TCheckBox;
    GroupBox3: TGroupBox;
    CheckBox5: TCheckBox;
    CheckBox6: TCheckBox;
    ebTABLA: TMaskEdit;
    ebSZEMELYKOD: TMaskEdit;
    ebNEV: TMaskEdit;
	procedure BitKilepClick(Sender: TObject);
	procedure FormCreate(Sender: TObject);
  procedure VCARDTolto(const Tabla, SzemelyKod, SzemelyNev: string);
  // procedure Tolto(cim : string; kod : string); override;
	procedure BitElkuldClick(Sender: TObject);
	private

	public
	end;

var
	VCARDParamsDlg: TVCARDParamsDlg;

implementation

uses
	Egyeb, J_SQL, Kozos;
{$R *.DFM}

procedure TVCARDParamsDlg.BitKilepClick(Sender: TObject);
begin
	Close;
end;

procedure TVCARDParamsDlg.FormCreate(Sender: TObject);
var
  cegnev: string;
begin
	EgyebDlg.SetADOQueryDatabase(Query1);
  SzotarTolt_CsakLista( cbBeosztas, '471');
  GroupBox1.Caption:= EgyebDlg.Read_SZgrid('470', 'CEG1');
  GroupBox2.Caption:= EgyebDlg.Read_SZgrid('470', 'CEG2');
  GroupBox3.Caption:= EgyebDlg.Read_SZgrid('470', 'CEG3');
  if GroupBox2.Caption = '' then GroupBox2.Enabled:= False;
  if GroupBox3.Caption = '' then GroupBox3.Enabled:= False;
end;

{procedure TVCARDParamsDlg.Tolto(cim : string; kod : string);
var
  Tabla, SzemelyKod: string;
begin
  Tabla:= EgyebDlg.SepFrontToken('|', kod);  // a "Tolto" kompatibilit�sa miatt �gy csin�ltam
  SzemelyKod:= EgyebDlg.SepRest('|', kod);
	ebTABLA.Text:= Tabla;
	ebSZEMELYKOD.Text:= SzemelyKod;
  ebNEV.Text:= cim;
  Query_Run(Query1, 'select * from VCARDPARAMS where VC_TABLA= '''+ebTABLA.Text+''' and VC_SZEMELYKOD='''+ ebSZEMELYKOD.text+'''');
  CheckBox1.Checked:= (Query1.FieldByname('VC_LISTA1').AsInteger = 1);
  CheckBox2.Checked:= (Query1.FieldByname('VC_LISTA2').AsInteger = 1);
  CheckBox3.Checked:= (Query1.FieldByname('VC_LISTA3').AsInteger = 1);
  CheckBox4.Checked:= (Query1.FieldByname('VC_LISTA4').AsInteger = 1);
  CheckBox5.Checked:= (Query1.FieldByname('VC_LISTA5').AsInteger = 1);
  CheckBox6.Checked:= (Query1.FieldByname('VC_LISTA6').AsInteger = 1);
  cbBeosztas.Text:= Query1.FieldByname('VC_BEOSZTAS').AsString;
end;}

procedure TVCARDParamsDlg.VCARDTolto(const Tabla, SzemelyKod, SzemelyNev: string);
begin
	ebTABLA.Text:= Tabla;
	ebSZEMELYKOD.Text:= SzemelyKod;
  ebNEV.Text:= SzemelyNev;
  Query_Run(Query1, 'select * from VCARDPARAMS where VC_TABLA= '''+ebTABLA.Text+''' and VC_SZEMELYKOD='''+ ebSZEMELYKOD.text+'''');
  CheckBox1.Checked:= (Query1.FieldByname('VC_LISTA1').AsInteger = 1);
  CheckBox2.Checked:= (Query1.FieldByname('VC_LISTA2').AsInteger = 1);
  CheckBox3.Checked:= (Query1.FieldByname('VC_LISTA3').AsInteger = 1);
  CheckBox4.Checked:= (Query1.FieldByname('VC_LISTA4').AsInteger = 1);
  CheckBox5.Checked:= (Query1.FieldByname('VC_LISTA5').AsInteger = 1);
  CheckBox6.Checked:= (Query1.FieldByname('VC_LISTA6').AsInteger = 1);
  cbBeosztas.Text:= Query1.FieldByname('VC_BEOSZTAS').AsString;
end;

procedure TVCARDParamsDlg.BitElkuldClick(Sender: TObject);
var
	st 			: string;
	i			: integer;
	cimlista	: TStringList;
	cim			: string;
	ujkod		: integer;
begin
		if cbBeosztas.Text = '' then begin
			NoticeKi('Hi�nyz� beoszt�s adat!');
  		end;
    SzotarBovito('471', cbBeosztas.Text); // felveszi a sz�t�rba, ha m�g nem l�tezik
		Query_Run(Query1, 'select VC_TABLA from VCARDPARAMS where VC_TABLA= '''+ebTABLA.Text+''' and VC_SZEMELYKOD='''+ ebSZEMELYKOD.text+'''');
    if not Query1.Eof then begin
      Query_Update( Query1, 'VCARDPARAMS', [
        'VC_BEOSZTAS', ''''+cbBeosztas.Text+'''',
        'VC_LISTA1', BoolToIntStr(CheckBox1.Checked),
        'VC_LISTA2', BoolToIntStr(CheckBox2.Checked),
        'VC_LISTA3', BoolToIntStr(CheckBox3.Checked),
        'VC_LISTA4', BoolToIntStr(CheckBox4.Checked),
        'VC_LISTA5', BoolToIntStr(CheckBox5.Checked),
        'VC_LISTA6', BoolToIntStr(CheckBox6.Checked)
        ], ' WHERE VC_TABLA= '''+ebTABLA.Text+''' and VC_SZEMELYKOD='''+ ebSZEMELYKOD.text+'''');
      end
    else begin
      Query_Insert( Query1, 'VCARDPARAMS', [
        'VC_TABLA', ''''+ebTABLA.Text+'''',
        'VC_SZEMELYKOD', ''''+ ebSZEMELYKOD.text+'''',
        'VC_BEOSZTAS', ''''+cbBeosztas.Text+'''',
        'VC_LISTA1', BoolToIntStr(CheckBox1.Checked),
        'VC_LISTA2', BoolToIntStr(CheckBox2.Checked),
        'VC_LISTA3', BoolToIntStr(CheckBox3.Checked),
        'VC_LISTA4', BoolToIntStr(CheckBox4.Checked),
        'VC_LISTA5', BoolToIntStr(CheckBox5.Checked),
        'VC_LISTA6', BoolToIntStr(CheckBox6.Checked)
        ]);
      end;  // else
	Close;
end;

end.
