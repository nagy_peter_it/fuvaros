object FArfolyamtolto2: TFArfolyamtolto2
  Left = 652
  Top = 387
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = 'MNB WEB Service '#225'rfolyamt'#246'lt'#337
  ClientHeight = 577
  ClientWidth = 207
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -16
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 20
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 207
    Height = 89
    Align = alTop
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 0
    object Label1: TLabel
      Left = 16
      Top = 10
      Width = 45
      Height = 16
      Caption = 'D'#225'tum'
    end
    object Label2: TLabel
      Left = 6
      Top = 40
      Width = 195
      Height = 16
      Caption = 'Erre a napra nincs '#225'rfolyam!'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object Button1: TButton
      Left = 8
      Top = 24
      Width = 81
      Height = 25
      Caption = #193'rfolyam lek'#233'r'#233's'
      TabOrder = 0
      Visible = False
      OnClick = Button1Click
    end
    object Button2: TButton
      Left = 88
      Top = 24
      Width = 75
      Height = 25
      Caption = 'Button2'
      TabOrder = 1
      Visible = False
      OnClick = Button2Click
    end
    object DateTimePicker1: TDateTimePicker
      Left = 80
      Top = 8
      Width = 105
      Height = 24
      Date = 41297.659053344910000000
      Time = 41297.659053344910000000
      TabOrder = 2
      OnChange = DateTimePicker1Change
    end
    object Button3: TButton
      Left = 63
      Top = 60
      Width = 81
      Height = 25
      Caption = 'T'#225'rol'#225's'
      TabOrder = 3
      OnClick = Button3Click
    end
  end
  object RateMemo: TMemo
    Left = 0
    Top = 89
    Width = 207
    Height = 488
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = 8404992
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
  end
  object Button4: TButton
    Left = 128
    Top = 112
    Width = 75
    Height = 25
    Caption = 'Button4'
    TabOrder = 2
    Visible = False
    OnClick = Button4Click
  end
  object Rio: THTTPRIO
    HTTPWebNode.Agent = 'Borland SOAP 1.2'
    HTTPWebNode.UseUTF8InHeader = False
    HTTPWebNode.InvokeOptions = [soIgnoreInvalidCerts, soAutoCheckAccessPointViaUDDI]
    Converter.Options = [soSendMultiRefObj, soTryAllSchema, soRootRefNodesToBody, soCacheMimeResponse, soUTF8EncodeXML]
    Left = 16
    Top = 112
  end
  object Query1: TADOQuery
    Connection = EgyebDlg.ADOConnectionKozos
    Parameters = <>
    Left = 64
    Top = 112
  end
end
