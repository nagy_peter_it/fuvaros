unit Bazisbe;

interface

uses
	SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
	Forms, Dialogs, StdCtrls, Buttons, Mask, DB, DBTables, ADODB;

type
	TBazisbeDlg = class(TForm)
	BitElkuld: TBitBtn;
	BitKilep: TBitBtn;
	Label2: TLabel;
	Label3: TLabel;
	MaskEdit1: TMaskEdit;
	MaskEdit2: TMaskEdit;
    CheckBox1: TCheckBox;
    Label1: TLabel;
    MaskEdit3: TMaskEdit;
    Query1: TADOQuery;
    Label4: TLabel;
    meEgyeb1: TMaskEdit;
	procedure FormCreate(Sender: TObject);
	procedure Tolto(cim : string; kod, szov, fokod : string ; ho, meho : integer );
	procedure BitElkuldClick(Sender: TObject);
	procedure BitKilepClick(Sender: TObject);
	procedure FormShow(Sender: TObject);
	procedure Kiiro;
	private
		fokodstr		: string;
	public
		ret_szoveg 	: string;
		ret_kod		: string;
	end;

var
	BazisbeDlg: TBazisbeDlg;

implementation

uses
	J_SQL, Egyeb, Kozos;
{$R *.DFM}

procedure TBazisbeDlg.FormCreate(Sender: TObject);
begin
	EgyebDlg.SeTADOQueryDatabase(Query1);
	MaskEdit1.Text 	:= '';
	MaskEdit2.Text 	:= '';
	MaskEdit3.Text 	:= '';
	fokodstr				:= '';
end;

procedure TBazisbeDlg.Tolto(cim : string; kod, szov, fokod : string ; ho, meho : integer);
const
  Alkod_hossz = 50;
begin
	ret_kod					:= kod;
	ret_szoveg 				:= szov;
	fokodstr					:= fokod;

  MaskEdit1.MaxLength	:= ho;
  if ( ( ho < 1 ) or (ho > Alkod_hossz) ) then begin
  	MaskEdit1.MaxLength	:= Alkod_hossz;
  end;
  MaskEdit2.MaxLength	:= meho;
  if ( ( ho < 1 ) or (ho > 120) ) then begin
  	MaskEdit2.MaxLength	:= 120;
  end;

	Caption 					:= cim;
	MaskEdit1.Text 		:= kod;
  if kod <> '' then begin
  	if Query_Run(Query1,'SELECT * FROM SZOTAR WHERE SZ_FOKOD='''+fokod+''' AND SZ_ALKOD='''+kod+''' ') then begin
			if Query1.RecordCount > 0 then begin
    		CheckBox1.Checked := Query1.FieldByName('SZ_ERVNY').AsInteger > 0;
    		// CheckBox2.Checked := Query1.FieldByName('SZ_EGYEB1').AsInteger > 0;
        meEgyeb1.Text 	:= Query1.FieldByName('SZ_EGYEB1').AsString;
				MaskEdit2.Text 	:= Query1.FieldByName('SZ_MENEV').AsString;
				MaskEdit3.Text 	:= Query1.FieldByName('SZ_LEIRO').AsString;
        end;
     end;
  end;
end;

procedure TBazisbeDlg.BitElkuldClick(Sender: TObject);
begin
	if MaskEdit1.Text = '' then begin
		NoticeKi('A k�d nem lehet �res!', NOT_ERROR);
		MaskEdit1.SetFocus;
	end else begin
		if ret_kod = '' then begin
			{Alk�d felvitele}
     	if Query_Run(Query1,'SELECT * FROM SZOTAR WHERE SZ_FOKOD='''+fokodstr+''' AND SZ_ALKOD='''+UpperCase(MaskEdit1.Text)+''' ') then begin
				if Query1.RecordCount > 0 then begin
					NoticeKi( 'Ez a k�d m�r l�tezik!', NOT_ERROR);
					MaskEdit1.Text := '';
					MaskEdit1.SetFocus;
              Exit;
				end else begin
     			if not Query_Run(Query1,'INSERT INTO SZOTAR (SZ_FOKOD, SZ_ALKOD) '+
				     	'VALUES('''+fokodstr+''', '''+UpperCase(MaskEdit1.Text)+''') ') then begin
     				Exit;
              end;
           end;
        end else begin
        	Exit;
     	end;
			ret_kod		:= UpperCase(MaskEdit1.Text);
        Kiiro;
		end else begin
			Kiiro;
		end;
	end;
end;

procedure TBazisbeDlg.BitKilepClick(Sender: TObject);
begin
	ret_szoveg 		:= '';
	ret_kod			:= '';
	Close;
end;

procedure TBazisbeDlg.FormShow(Sender: TObject);
begin
	if ret_kod <> '' then begin
		MaskEdit1.Enabled := false;
		MaskEdit2.SetFocus;
	end;
end;

procedure TBazisbeDlg.Kiiro;
var
	logszov, egyeb1	: string;
begin
	logszov	:= '0';
	if CheckBox1.Checked then begin
		logszov	:= '1';
	end;
	// if CheckBox2.Checked then egyeb1	:= '1';
  if trim(meEgyeb1.Text) <> '' then
    egyeb1:=meEgyeb1.Text
  else egyeb1	:= '0';
	Query_Update(Query1, 'SZOTAR',
		[
		'SZ_MENEV' , '''' + MaskEdit2.text + '''',
		'SZ_LEIRO' , '''' + MaskEdit3.text + '''',
		'SZ_ERVNY' , logszov ,
		'SZ_EGYEB1' , egyeb1
		]
		, ' WHERE SZ_FOKOD='''+fokodstr+''' AND SZ_ALKOD='''+UpperCase(MaskEdit1.Text)+''' ');
	ret_szoveg	:= MaskEdit2.Text;
	BazisbeDlg.Close;
end;

end.
