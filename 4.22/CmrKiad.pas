unit Cmrkiad;

interface

uses
	SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
	Forms, Dialogs, StdCtrls, Buttons, Mask, DB, DBTables, J_ALFORM, ADODB,
  ExtCtrls;

type
	TCmrKiadDlg = class(TJ_AlformDlg)
	BitElkuld: TBitBtn;
	BitKilep: TBitBtn;
    Query1: TADOQuery;
    Label17: TLabel;
    Label18: TLabel;
    M1: TMaskEdit;
    M2: TMaskEdit;
    Label4: TLabel;
    M6: TMaskEdit;
    MaskEdit2: TMaskEdit;
    BitBtn2: TBitBtn;
    Label5: TLabel;
    M7: TMaskEdit;
	 BitBtn9: TBitBtn;
    Query2: TADOQuery;
    BitBtn32: TBitBtn;
    Label1: TLabel;
    MaskEdit1: TMaskEdit;
    BitBtn1: TBitBtn;
    BitBtn3: TBitBtn;
	procedure BitKilepClick(Sender: TObject);
	procedure FormCreate(Sender: TObject);
	procedure Tolto(cim : string; teko : string); override;
	procedure BitElkuldClick(Sender: TObject);
	procedure MaskEditClick(Sender: TObject);
	 procedure FormKeyDown(Sender: TObject; var Key: Word;
	   Shift: TShiftState);
	 procedure M3KeyPress(Sender: TObject; var Key: Char);
	 procedure M24AExit(Sender: TObject);
	 procedure BitBtn9Click(Sender: TObject);
	 procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn32Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
	private
	public
	end;

var
	CmrKiadDlg: TCmrKiadDlg;

implementation

uses
	J_VALASZTO, Egyeb, J_SQL, Kozos, VevoUjfm;
{$R *.DFM}

procedure TCmrKiadDlg.BitKilepClick(Sender: TObject);
begin
	ret_kod := '';
	Close;
end;

procedure TCmrKiadDlg.FormCreate(Sender: TObject);
begin
	EgyebDlg.SetADOQueryDatabase(Query1);
	EgyebDlg.SetADOQueryDatabase(Query2);
	SetMaskEdits([M6]);
	SetMaskEdits([MaskEdit1]);
end;

procedure TCmrKiadDlg.Tolto(cim : string; teko : string);
begin
	ret_kod  	:= teko;
	Caption  	:= cim;
	M7.Text		:= EgyebDlg.MaiDatum;
end;

procedure TCmrKiadDlg.BitElkuldClick(Sender: TObject);
var
	kezdostr	: string;
	vegzostr	: string;
	vegestr		: string;
	idegen		: boolean;
	szamstr,nev		: string;
begin
	if (MaskEdit2.Text <> '')and(MaskEdit1.Text<>'') then begin
		NoticeKi('Csak egy adat adhat� meg!');
		BitBtn2.SetFocus;
		Exit;
	end;
	if (MaskEdit2.Text = '')and(MaskEdit1.Text='') then begin
		NoticeKi('A sof�r megad�sa k�telez�!');
		BitBtn2.SetFocus;
		Exit;
	end;
	if M1.Text = '' then begin
		NoticeKi('A kezd� sorsz�m megad�sa k�telez�!');
		M1.SetFocus;
		Exit;
	end;
	if M2.Text = '' then begin
		NoticeKi('A befejez� sorsz�m megad�sa k�telez�!');
		M2.SetFocus;
		Exit;
	end;
	idegen	:= false;
	if Pos('/', M1.Text) > 0 then begin
		idegen	:= true;
		if  Pos('/', M1.Text) < 1 then begin
			NoticeKi('A m�sodik sorsz�mnak is idegennek kell lennie!');
			M2.Setfocus;
			Exit;
		end;
	end;
	if idegen then begin
		szamstr		:= copy(M1.Text, 1, Pos('/', M1.Text) - 1);
		vegestr		:= copy(M1.Text, Pos('/', M1.Text), 999);
		kezdostr	:= Format('%3d', [StrToIntDef(szamstr, 0)])+vegestr;
		szamstr		:= copy(M2.Text, 1, Pos('/', M2.Text) - 1);
		vegzostr	:= Format('%3d', [StrToIntDef(szamstr, 0)])+vegestr;
	end else begin
		kezdostr	:= Format('%7.7d', [StrToIntDef(M1.Text, 0)]);
		vegzostr	:= Format('%7.7d', [StrToIntDef(M2.Text, 0)]);
	end;
	Query_Run (Query1, 'SELECT * FROM CMRDAT WHERE CM_CSZAM >= '''+kezdostr+''' AND CM_CSZAM <= '''+vegzostr+''' ');
	if  Query1.RecordCount = 0 then begin
		NoticeKi('Nincs megfelel� ele!');
		M1.Setfocus;
		Exit;
	end;
	Query_Run (Query1, 'SELECT * FROM CMRDAT WHERE CM_CSZAM >= '''+kezdostr+''' AND CM_CSZAM <= '''+vegzostr+''' '+
		'AND CM_DOKOD <> '''' AND CM_DOKOD <> '''+MaskEdit2.Text+''' ');
	if Query1.RecordCount > 0 then begin
		NoticeKi('A megadott intervallumban m�r l�tezik m�sik sof�rh�z kiadott CMR : '+Query1.FieldByName('CM_CSZAM').AsString);
		M1.Setfocus;
		Exit;
	end;

	// AZ adatok friss�t�se
	Query_Run (Query1, 'SELECT * FROM CMRDAT WHERE CM_CSZAM >= '''+kezdostr+''' AND CM_CSZAM <= '''+vegzostr+''' '+
		'AND ( CM_DOKOD = '''' OR CM_DOKOD IS NULL ) ');
	if Query1.RecordCount = 0 then begin
		NoticeKi('Nincs megfelel� elem!');
		M1.Setfocus;
		Exit;
	end;
	Query1.First;
  nev:=M6.Text;
  if MaskEdit1.Text<>'' then
    nev:=MaskEdit1.Text;
	while not Query1.Eof do begin
		Query_Update (Query2, 'CMRDAT',
			[
			'CM_DOKOD', ''''+MaskEdit2.Text+'''',
			'CM_DONEV', ''''+Nev+'''',
			'CM_KIDAT', ''''+M7.Text+''''
			],' WHERE CM_CMKOD = '+Query1.FieldByName('CM_CMKOD').AsString);
		Query1.Next;
	end;
	Close;
end;

procedure TCmrKiadDlg.MaskEditClick(Sender: TObject);
begin
	with Sender as TMaskEdit do begin
		SelectAll;
	end;
end;

procedure TCmrKiadDlg.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
	EgyebDlg.FormReturn(Key);
end;

procedure TCmrKiadDlg.M3KeyPress(Sender: TObject; var Key: Char);
begin
  with Sender as TMaskEdit do begin
  		Key := EgyebDlg.Jochar(Text,Key,(Sender as TMaskEdit).MaxLength,(Sender as TMaskEdit).Tag);
  end;
end;

procedure TCmrKiadDlg.M24AExit(Sender: TObject);
begin
	SzamExit(Sender);
end;

procedure TCmrKiadDlg.BitBtn9Click(Sender: TObject);
begin
	EgyebDlg.Calendarbe(M7);
end;

procedure TCmrKiadDlg.BitBtn2Click(Sender: TObject);
begin
	DolgozoValaszto(MaskEdit2, M6);
	M7.SetFocus;
end;

procedure TCmrKiadDlg.BitBtn32Click(Sender: TObject);
begin
	MaskEdit2.Text	:= '';
	M6.Text			:= '';
end;

procedure TCmrKiadDlg.BitBtn1Click(Sender: TObject);
begin
	{A megb�z� beolvas�sa}
	Application.CreateForm(TVevoUjFmDlg, VevoUjFmDlg);
	VevoUjFmDlg.valaszt := true;
	VevoUjFmDlg.ShowModal;
	if VevoUjFmDlg.ret_vkod <> '' then begin
		if VevoUjFmDlg.felista > 0 then begin
			if NoticeKi('Feketelist�s vev�! Folytatja?', NOT_QUESTION) <> 0 then begin
				VevoUjFmDlg.Destroy;
				Exit;
			end;
		end;
		MaskEdit1.Text 	:= VevoUjFmDlg.ret_vnev;
		//M12.Text 	:= VevoUjFmDlg.ret_vnev;
	end;
	VevoUjFmDlg.Destroy;
end;

procedure TCmrKiadDlg.BitBtn3Click(Sender: TObject);
begin
	MaskEdit1.Text			:= '';
end;

end.
