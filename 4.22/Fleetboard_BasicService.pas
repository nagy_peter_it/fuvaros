// ************************************************************************ //
// The types declared in this file were generated from data read from the
// WSDL File described below:
// WSDL     : P:\SRC\FUVAR_ADO\4.20\WSDL\BasicService.wsdl
//  >Import : P:\SRC\FUVAR_ADO\4.20\WSDL\BasicService.wsdl>0
// Encoding : UTF-8
// Version  : 1.0
// (2017.10.03. 10:36:39 - - $Rev: 56641 $)
// ************************************************************************ //

unit Fleetboard_BasicService;

interface

uses InvokeRegistry, SOAPHTTPClient, Types, XSBuiltIns;

const
  IS_OPTN = $0001;
  IS_UNBD = $0002;
  IS_NLBL = $0004;
  IS_ATTR = $0010;
  IS_REF  = $0080;


type

  // ************************************************************************ //
  // The following types, referred to in the WSDL document are not being represented
  // in this file. They are either aliases[@] of other types represented or were referred
  // to but never[!] declared in the document. The types from the latter category
  // typically map to predefined/known XML or Embarcadero types; however, they could also 
  // indicate incorrect WSDL documents that failed to declare or import a schema type.
  // ************************************************************************ //
  // !:long            - "http://www.w3.org/2001/XMLSchema"[Gbl]
  // !:integer         - "http://www.w3.org/2001/XMLSchema"[Gbl]
  // !:string          - "http://www.w3.org/2001/XMLSchema"[Gbl]
  // !:short           - "http://www.w3.org/2001/XMLSchema"[Gbl]
  // !:boolean         - "http://www.w3.org/2001/XMLSchema"[Gbl]

  VhcStatusLayout      = class;                 { "http://www.fleetboard.com/data"[GblElm] }
  Hidden               = class;                 { "http://www.fleetboard.com/data"[GblElm] }
  PropertyType         = class;                 { "http://www.fleetboard.com/data"[GblCplx] }
  DriverToken          = class;                 { "http://www.fleetboard.com/data"[Cplx] }
  StatusDef            = class;                 { "http://www.fleetboard.com/data"[GblElm] }
  TmSoapUserType       = class;                 { "http://www.fleetboard.com/data"[GblCplx] }
  UserSettings         = class;                 { "http://www.fleetboard.com/data"[Cplx] }
  LoginRequest         = class;                 { "http://www.fleetboard.com/data"[GblElm] }
  Semantics            = class;                 { "http://www.fleetboard.com/data"[Cplx] }
  Type_                = class;                 { "http://www.fleetboard.com/data"[Cplx] }
  TXMessage            = class;                 { "http://www.fleetboard.com/data"[GblElm] }
  RXMessage            = class;                 { "http://www.fleetboard.com/data"[GblElm] }
  StatusLayout         = class;                 { "http://www.fleetboard.com/data"[GblElm] }
  Action               = class;                 { "http://www.fleetboard.com/data"[GblElm] }
  DriverGroup          = class;                 { "http://www.fleetboard.com/data"[GblElm] }
  dtcoCapabilityType   = class;                 { "http://www.fleetboard.com/data"[GblCplx] }
  GetStatusDefRequestType = class;              { "http://www.fleetboard.com/data"[GblCplx] }
  GetStatusDefResponseType = class;             { "http://www.fleetboard.com/data"[GblCplx] }
  GetStatusDefResponse = class;                 { "http://www.fleetboard.com/data"[GblElm] }
  GetDriverRequestType = class;                 { "http://www.fleetboard.com/data"[GblCplx] }
  GetDriverRequest     = class;                 { "http://www.fleetboard.com/data"[GblElm] }
  GetDriverResponseType = class;                { "http://www.fleetboard.com/data"[GblCplx] }
  GetDriverResponse    = class;                 { "http://www.fleetboard.com/data"[GblElm] }
  GetUserResponseType  = class;                 { "http://www.fleetboard.com/data"[GblCplx] }
  GetUserResponse      = class;                 { "http://www.fleetboard.com/data"[GblElm] }
  GetDriverGroupRequest = class;                { "http://www.fleetboard.com/data"[GblElm] }
  GetCurrentUserRequest = class;                { "http://www.fleetboard.com/data"[GblElm] }
  GetDriverGroupResponse = class;               { "http://www.fleetboard.com/data"[GblElm] }
  DriverInfo           = class;                 { "http://www.fleetboard.com/data"[Cplx] }
  FLEETVEHICLE         = class;                 { "http://www.fleetboard.com/data"[Cplx] }
  GetVehicleResponse   = class;                 { "http://www.fleetboard.com/data"[GblElm] }
  GetFleetResponse     = class;                 { "http://www.fleetboard.com/data"[GblElm] }
  VEHICLEHARDWARE      = class;                 { "http://www.fleetboard.com/data"[Cplx] }
  GetStatusDefRequest  = class;                 { "http://www.fleetboard.com/data"[GblElm] }
  GetVehicleGroupResponse = class;              { "http://www.fleetboard.com/data"[GblElm] }
  VEHICLEGROUP         = class;                 { "http://www.fleetboard.com/data"[GblElm] }
  GetServerPropertiesRequest = class;           { "http://www.fleetboard.com/data"[GblElm] }
  User                 = class;                 { "http://www.fleetboard.com/data"[GblElm] }
  GetUserRequest       = class;                 { "http://www.fleetboard.com/data"[GblElm] }
  GetVehicleGroupRequest = class;               { "http://www.fleetboard.com/data"[GblElm] }
  GetCurrentUserResponseType = class;           { "http://www.fleetboard.com/data"[GblCplx] }
  GetCurrentUserResponse = class;               { "http://www.fleetboard.com/data"[GblElm] }
  GetVehicleRequest    = class;                 { "http://www.fleetboard.com/data"[GblElm] }
  VEHICLE              = class;                 { "http://www.fleetboard.com/data"[GblElm] }
  LoginResponse        = class;                 { "http://www.fleetboard.com/data"[GblElm] }
  FLEETINFORMATION     = class;                 { "http://www.fleetboard.com/data"[Cplx] }
  GetServerPropertiesResponse = class;          { "http://www.fleetboard.com/data"[GblElm] }
  FLEET                = class;                 { "http://www.fleetboard.com/data"[GblElm] }
  LogoutResponse       = class;                 { "http://www.fleetboard.com/data"[GblElm] }
  LogoutRequest        = class;                 { "http://www.fleetboard.com/data"[GblElm] }
  RIGHT                = class;                 { "http://www.fleetboard.com/data"[GblElm] }
  GetFleetRequest      = class;                 { "http://www.fleetboard.com/data"[GblElm] }

  {$SCOPEDENUMS ON}
  { "http://www.fleetboard.com/data"[Smpl] }
  SettingsType = (UserSettings, RightSettings);

  { "http://www.fleetboard.com/data"[Smpl] }
  DriverTokenKind = (SIM, FB, KEY, DTCO);

  { "http://www.fleetboard.com/data"[GblSmpl] }
  driverStatusType = (ACTIVE, INACTIVE);

  { "http://www.fleetboard.com/data"[Smpl] }
  Kind = (VEHICLE, ORDER, TOUR, CLIENT, TIMEMGMT, EVENT);

  { "http://www.fleetboard.com/data"[Smpl] }
  Property_ = (TIME, VERSION, QUERYLIMITS, FIELDLIMITS, MAPPING, UPDATESERVICE);

  { "http://www.fleetboard.com/data"[GblSmpl] }
  tmmodeType = (MASTERFLEETONLY, PARTNERFLEETONLY, MASTERFLEETISOWNER, PARTNERFLEETISOWNER);

  { "http://www.fleetboard.com/data"[GblSmpl] }
  leasingStatusType = (LENT, RENTED, OWNED);

  { "http://www.fleetboard.com/data"[GblSmpl] }
  serviceType = (VM, TM, ALL);

  { "http://www.fleetboard.com/data"[GblSmpl] }
  telematicGroupIDType = (MB, AM, DISPOSITION);

  {$SCOPEDENUMS OFF}

  History    = array of User;                   { "http://www.fleetboard.com/data"[GblElm] }


  // ************************************************************************ //
  // XML       : VhcStatusLayout, global, <element>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  VhcStatusLayout = class(TRemotable)
  private
    FHidden: Hidden;
    FHidden_Specified: boolean;
    procedure SetHidden(Index: Integer; const AHidden: Hidden);
    function  Hidden_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property Hidden: Hidden  Index (IS_OPTN or IS_REF) read FHidden write SetHidden stored Hidden_Specified;
  end;

  SemanticsDef = array of Semantics;            { "http://www.fleetboard.com/data"[GblElm] }
  Array_Of_DriverToken = array of DriverToken;   { "http://www.fleetboard.com/data"[Ubnd] }
  Array_Of_DriverInfo = array of DriverInfo;    { "http://www.fleetboard.com/data"[Ubnd] }
  Array_Of_dtcoCapabilityType = array of dtcoCapabilityType;   { "http://www.fleetboard.com/data"[GblUbnd] }
  Array_Of_TmSoapUserType = array of TmSoapUserType;   { "http://www.fleetboard.com/data"[GblUbnd] }
  Array_Of_StatusDef = array of StatusDef;      { "http://www.fleetboard.com/data"[GblUbnd] }
  Array_Of_DriverGroup = array of DriverGroup;   { "http://www.fleetboard.com/data"[GblUbnd] }
  Array_Of_VEHICLE = array of VEHICLE;          { "http://www.fleetboard.com/data"[GblUbnd] }
  Array_Of_VEHICLEHARDWARE = array of VEHICLEHARDWARE;   { "http://www.fleetboard.com/data"[Ubnd] }
  Array_Of_VEHICLEGROUP = array of VEHICLEGROUP;   { "http://www.fleetboard.com/data"[GblUbnd] }
  Array_Of_SettingsType = array of SettingsType;   { "http://www.fleetboard.com/data"[Ubnd] }
  fleetidType     =  type Int64;      { "http://www.fleetboard.com/data"[GblSmpl] }
  driverNameIDType =  type Int64;      { "http://www.fleetboard.com/data"[GblSmpl] }
  Array_Of_driverNameIDType = array of driverNameIDType;   { "http://www.fleetboard.com/data"[GblUbnd] }
  driverGroupIdType =  type Int64;      { "http://www.fleetboard.com/data"[GblSmpl] }
  resultSizeType  =  type Int64;      { "http://www.fleetboard.com/data"[GblSmpl] }
  responseSizeType =  type Int64;      { "http://www.fleetboard.com/data"[GblSmpl] }
  Array_Of_integer = array of Int64;            { "http://www.w3.org/2001/XMLSchema"[GblUbnd] }


  // ************************************************************************ //
  // XML       : Hidden, global, <element>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  Hidden = class(TRemotable)
  private
    FDCFAP: Int64;
    FDCFAP_Specified: boolean;
    procedure SetDCFAP(Index: Integer; const AInt64: Int64);
    function  DCFAP_Specified(Index: Integer): boolean;
  published
    property DCFAP: Int64  Index (IS_OPTN) read FDCFAP write SetDCFAP stored DCFAP_Specified;
  end;

  idType          =  type Int64;      { "http://www.fleetboard.com/data"[GblSmpl] }
  Array_Of_idType = array of idType;            { "http://www.fleetboard.com/data"[GblUbnd] }
  inoidType       =  type string;      { "http://www.fleetboard.com/data"[GblSmpl] }
  Array_Of_inoidType = array of inoidType;      { "http://www.fleetboard.com/data"[GblUbnd] }
  MsgID           =  type inoidType;      { "http://www.fleetboard.com/data"[Smpl] }
  timestampType   =  type string;      { "http://www.fleetboard.com/data"[GblSmpl] }


  // ************************************************************************ //
  // XML       : PropertyType, global, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  PropertyType = class(TRemotable)
  private
    FName_: string;
    FValue: string;
  published
    property Name_: string  read FName_ write FName_;
    property Value: string  Index (IS_NLBL) read FValue write FValue;
  end;

  fbidType        =  type string;      { "http://www.fleetboard.com/data"[GblSmpl] }
  Array_Of_fbidType = array of fbidType;        { "http://www.fleetboard.com/data"[GblUbnd] }


  // ************************************************************************ //
  // XML       : DriverToken, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  DriverToken = class(TRemotable)
  private
    FDriverTokenKind: DriverTokenKind;
    FToken: string;
  published
    property DriverTokenKind: DriverTokenKind  read FDriverTokenKind write FDriverTokenKind;
    property Token:           string           read FToken write FToken;
  end;



  // ************************************************************************ //
  // XML       : StatusDef, global, <element>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  StatusDef = class(TRemotable)
  private
    Finoid: inoidType;
    Finoid_Specified: boolean;
    Ffbid: fbidType;
    Ffbid_Specified: boolean;
    FFLEETID: fleetidType;
    FType_: Type_;
    FStatusLayout: StatusLayout;
    FAction: Action;
    FHistory: History;
    FHistory_Specified: boolean;
    FVisible: string;
    FVisible_Specified: boolean;
    FShowable: Int64;
    FShowable_Specified: boolean;
    FStatusGroup: string;
    FStatusGroup_Specified: boolean;
    FVhcStatusLayout: VhcStatusLayout;
    FVhcStatusLayout_Specified: boolean;
    FSemanticsDef: SemanticsDef;
    FSemanticsDef_Specified: boolean;
    FFormDefInoID: inoidType;
    FFormDefInoID_Specified: boolean;
    procedure Setinoid(Index: Integer; const AinoidType: inoidType);
    function  inoid_Specified(Index: Integer): boolean;
    procedure Setfbid(Index: Integer; const AfbidType: fbidType);
    function  fbid_Specified(Index: Integer): boolean;
    procedure SetHistory(Index: Integer; const AHistory: History);
    function  History_Specified(Index: Integer): boolean;
    procedure SetVisible(Index: Integer; const Astring: string);
    function  Visible_Specified(Index: Integer): boolean;
    procedure SetShowable(Index: Integer; const AInt64: Int64);
    function  Showable_Specified(Index: Integer): boolean;
    procedure SetStatusGroup(Index: Integer; const Astring: string);
    function  StatusGroup_Specified(Index: Integer): boolean;
    procedure SetVhcStatusLayout(Index: Integer; const AVhcStatusLayout: VhcStatusLayout);
    function  VhcStatusLayout_Specified(Index: Integer): boolean;
    procedure SetSemanticsDef(Index: Integer; const ASemanticsDef: SemanticsDef);
    function  SemanticsDef_Specified(Index: Integer): boolean;
    procedure SetFormDefInoID(Index: Integer; const AinoidType: inoidType);
    function  FormDefInoID_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property inoid:           inoidType        Index (IS_ATTR or IS_OPTN) read Finoid write Setinoid stored inoid_Specified;
    property fbid:            fbidType         Index (IS_ATTR or IS_OPTN) read Ffbid write Setfbid stored fbid_Specified;
    property FLEETID:         fleetidType      read FFLEETID write FFLEETID;
    property Type_:           Type_            read FType_ write FType_;
    property StatusLayout:    StatusLayout     Index (IS_REF) read FStatusLayout write FStatusLayout;
    property Action:          Action           Index (IS_REF) read FAction write FAction;
    property History:         History          Index (IS_OPTN or IS_REF) read FHistory write SetHistory stored History_Specified;
    property Visible:         string           Index (IS_OPTN) read FVisible write SetVisible stored Visible_Specified;
    property Showable:        Int64            Index (IS_OPTN) read FShowable write SetShowable stored Showable_Specified;
    property StatusGroup:     string           Index (IS_OPTN) read FStatusGroup write SetStatusGroup stored StatusGroup_Specified;
    property VhcStatusLayout: VhcStatusLayout  Index (IS_OPTN or IS_REF) read FVhcStatusLayout write SetVhcStatusLayout stored VhcStatusLayout_Specified;
    property SemanticsDef:    SemanticsDef     Index (IS_OPTN or IS_REF) read FSemanticsDef write SetSemanticsDef stored SemanticsDef_Specified;
    property FormDefInoID:    inoidType        Index (IS_OPTN) read FFormDefInoID write SetFormDefInoID stored FormDefInoID_Specified;
  end;

  RightSettings = array of string;              { "http://www.fleetboard.com/data"[Cplx] }


  // ************************************************************************ //
  // XML       : TmSoapUserType, global, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  TmSoapUserType = class(TRemotable)
  private
    FID: Int64;
    FUserSettings: UserSettings;
    FUserSettings_Specified: boolean;
    FRightSettings: RightSettings;
    FRightSettings_Specified: boolean;
    procedure SetUserSettings(Index: Integer; const AUserSettings: UserSettings);
    function  UserSettings_Specified(Index: Integer): boolean;
    procedure SetRightSettings(Index: Integer; const ARightSettings: RightSettings);
    function  RightSettings_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property ID:            Int64          read FID write FID;
    property UserSettings:  UserSettings   Index (IS_OPTN) read FUserSettings write SetUserSettings stored UserSettings_Specified;
    property RightSettings: RightSettings  Index (IS_OPTN) read FRightSettings write SetRightSettings stored RightSettings_Specified;
  end;



  // ************************************************************************ //
  // XML       : UserSettings, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  UserSettings = class(TRemotable)
  private
    FFleetID: fleetidType;
    FName_: string;
    FDescription: string;
    FDescription_Specified: boolean;
    FLocale: string;
    FLogin: string;
    FStatus: Int64;
    FStatus_Specified: boolean;
    FUserGroup: string;
    FUserGroup_Specified: boolean;
    FProfileId: idType;
    FProfileId_Specified: boolean;
    FEndDate: timestampType;
    FEndDate_Specified: boolean;
    FMobilePhone: string;
    FMobilePhone_Specified: boolean;
    FEmail: string;
    FEmail_Specified: boolean;
    FCSVSeparator: string;
    FCSVSeparator_Specified: boolean;
    FLastAmended: string;
    FLastAmended_Specified: boolean;
    FAmendedBy: string;
    FAmendedBy_Specified: boolean;
    procedure SetDescription(Index: Integer; const Astring: string);
    function  Description_Specified(Index: Integer): boolean;
    procedure SetStatus(Index: Integer; const AInt64: Int64);
    function  Status_Specified(Index: Integer): boolean;
    procedure SetUserGroup(Index: Integer; const Astring: string);
    function  UserGroup_Specified(Index: Integer): boolean;
    procedure SetProfileId(Index: Integer; const AidType: idType);
    function  ProfileId_Specified(Index: Integer): boolean;
    procedure SetEndDate(Index: Integer; const AtimestampType: timestampType);
    function  EndDate_Specified(Index: Integer): boolean;
    procedure SetMobilePhone(Index: Integer; const Astring: string);
    function  MobilePhone_Specified(Index: Integer): boolean;
    procedure SetEmail(Index: Integer; const Astring: string);
    function  Email_Specified(Index: Integer): boolean;
    procedure SetCSVSeparator(Index: Integer; const Astring: string);
    function  CSVSeparator_Specified(Index: Integer): boolean;
    procedure SetLastAmended(Index: Integer; const Astring: string);
    function  LastAmended_Specified(Index: Integer): boolean;
    procedure SetAmendedBy(Index: Integer; const Astring: string);
    function  AmendedBy_Specified(Index: Integer): boolean;
  published
    property FleetID:      fleetidType    read FFleetID write FFleetID;
    property Name_:        string         read FName_ write FName_;
    property Description:  string         Index (IS_OPTN) read FDescription write SetDescription stored Description_Specified;
    property Locale:       string         read FLocale write FLocale;
    property Login:        string         read FLogin write FLogin;
    property Status:       Int64          Index (IS_OPTN) read FStatus write SetStatus stored Status_Specified;
    property UserGroup:    string         Index (IS_OPTN) read FUserGroup write SetUserGroup stored UserGroup_Specified;
    property ProfileId:    idType         Index (IS_OPTN) read FProfileId write SetProfileId stored ProfileId_Specified;
    property EndDate:      timestampType  Index (IS_OPTN) read FEndDate write SetEndDate stored EndDate_Specified;
    property MobilePhone:  string         Index (IS_OPTN) read FMobilePhone write SetMobilePhone stored MobilePhone_Specified;
    property Email:        string         Index (IS_OPTN) read FEmail write SetEmail stored Email_Specified;
    property CSVSeparator: string         Index (IS_OPTN) read FCSVSeparator write SetCSVSeparator stored CSVSeparator_Specified;
    property LastAmended:  string         Index (IS_OPTN) read FLastAmended write SetLastAmended stored LastAmended_Specified;
    property AmendedBy:    string         Index (IS_OPTN) read FAmendedBy write SetAmendedBy stored AmendedBy_Specified;
  end;

  User2           =  type string;      { "http://www.fleetboard.com/data"[Smpl] }
  Fleetname       =  type string;      { "http://www.fleetboard.com/data"[Smpl] }
  Password        =  type string;      { "http://www.fleetboard.com/data"[Smpl] }


  // ************************************************************************ //
  // XML       : LoginRequest, global, <element>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  LoginRequest = class(TRemotable)
  private
    FFleetname: Fleetname;
    FFleetname_Specified: boolean;
    FUser: User2;
    FPassword: Password;
    procedure SetFleetname(Index: Integer; const AFleetname: Fleetname);
    function  Fleetname_Specified(Index: Integer): boolean;
  published
    property Fleetname: Fleetname  Index (IS_OPTN) read FFleetname write SetFleetname stored Fleetname_Specified;
    property User:      User2      read FUser write FUser;
    property Password:  Password   read FPassword write FPassword;
  end;



  // ************************************************************************ //
  // XML       : Semantics, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  Semantics = class(TRemotable)
  private
    FDenotation: string;
    FSemanticId: Int64;
    FSemanticId_Specified: boolean;
    FScope: RightSettings;
    FScope_Specified: boolean;
    FValue: string;
    FValue_Specified: boolean;
    procedure SetSemanticId(Index: Integer; const AInt64: Int64);
    function  SemanticId_Specified(Index: Integer): boolean;
    procedure SetScope(Index: Integer; const ARightSettings: RightSettings);
    function  Scope_Specified(Index: Integer): boolean;
    procedure SetValue(Index: Integer; const Astring: string);
    function  Value_Specified(Index: Integer): boolean;
  published
    property Denotation: string         read FDenotation write FDenotation;
    property SemanticId: Int64          Index (IS_OPTN) read FSemanticId write SetSemanticId stored SemanticId_Specified;
    property Scope:      RightSettings  Index (IS_OPTN or IS_UNBD) read FScope write SetScope stored Scope_Specified;
    property Value:      string         Index (IS_OPTN) read FValue write SetValue stored Value_Specified;
  end;



  // ************************************************************************ //
  // XML       : Type, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  Type_ = class(TRemotable)
  private
    FKind: string;
    FATSRegister: string;
  published
    property Kind:        string  read FKind write FKind;
    property ATSRegister: string  read FATSRegister write FATSRegister;
  end;



  // ************************************************************************ //
  // XML       : TXMessage, global, <element>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  TXMessage = class(TRemotable)
  private
    FReceiveRQ: string;
    FReceiveRQ_Specified: boolean;
    FReadRQ: string;
    FReadRQ_Specified: boolean;
    procedure SetReceiveRQ(Index: Integer; const Astring: string);
    function  ReceiveRQ_Specified(Index: Integer): boolean;
    procedure SetReadRQ(Index: Integer; const Astring: string);
    function  ReadRQ_Specified(Index: Integer): boolean;
  published
    property ReceiveRQ: string  Index (IS_OPTN) read FReceiveRQ write SetReceiveRQ stored ReceiveRQ_Specified;
    property ReadRQ:    string  Index (IS_OPTN) read FReadRQ write SetReadRQ stored ReadRQ_Specified;
  end;

  Email           =  type string;      { "http://www.fleetboard.com/data"[Smpl] }


  // ************************************************************************ //
  // XML       : RXMessage, global, <element>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  RXMessage = class(TRemotable)
  private
    FPopup: string;
    FPopup_Specified: boolean;
    FSound: string;
    FSound_Specified: boolean;
    FEmail: Email;
    FEmail_Specified: boolean;
    FSMS: string;
    FSMS_Specified: boolean;
    FOrderdone: string;
    FOrderdone_Specified: boolean;
    procedure SetPopup(Index: Integer; const Astring: string);
    function  Popup_Specified(Index: Integer): boolean;
    procedure SetSound(Index: Integer; const Astring: string);
    function  Sound_Specified(Index: Integer): boolean;
    procedure SetEmail(Index: Integer; const AEmail: Email);
    function  Email_Specified(Index: Integer): boolean;
    procedure SetSMS(Index: Integer; const Astring: string);
    function  SMS_Specified(Index: Integer): boolean;
    procedure SetOrderdone(Index: Integer; const Astring: string);
    function  Orderdone_Specified(Index: Integer): boolean;
  published
    property Popup:     string  Index (IS_OPTN) read FPopup write SetPopup stored Popup_Specified;
    property Sound:     string  Index (IS_OPTN) read FSound write SetSound stored Sound_Specified;
    property Email:     Email   Index (IS_OPTN) read FEmail write SetEmail stored Email_Specified;
    property SMS:       string  Index (IS_OPTN) read FSMS write SetSMS stored SMS_Specified;
    property Orderdone: string  Index (IS_OPTN) read FOrderdone write SetOrderdone stored Orderdone_Specified;
  end;



  // ************************************************************************ //
  // XML       : StatusLayout, global, <element>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  StatusLayout = class(TRemotable)
  private
    FText: string;
    FBGColor: string;
    FFontColor: string;
  published
    property Text:      string  read FText write FText;
    property BGColor:   string  read FBGColor write FBGColor;
    property FontColor: string  read FFontColor write FFontColor;
  end;

  Request    = array of string;                 { "http://www.fleetboard.com/data"[Cplx] }


  // ************************************************************************ //
  // XML       : Action, global, <element>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  Action = class(TRemotable)
  private
    FTXMessage: TXMessage;
    FTXMessage_Specified: boolean;
    FRXMessage: RXMessage;
    FRXMessage_Specified: boolean;
    FRequest: Request;
    FRequest_Specified: boolean;
    procedure SetTXMessage(Index: Integer; const ATXMessage: TXMessage);
    function  TXMessage_Specified(Index: Integer): boolean;
    procedure SetRXMessage(Index: Integer; const ARXMessage: RXMessage);
    function  RXMessage_Specified(Index: Integer): boolean;
    procedure SetRequest(Index: Integer; const ARequest: Request);
    function  Request_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property TXMessage: TXMessage  Index (IS_OPTN or IS_REF) read FTXMessage write SetTXMessage stored TXMessage_Specified;
    property RXMessage: RXMessage  Index (IS_OPTN or IS_REF) read FRXMessage write SetRXMessage stored RXMessage_Specified;
    property Request:   Request    Index (IS_OPTN) read FRequest write SetRequest stored Request_Specified;
  end;



  // ************************************************************************ //
  // XML       : DriverGroup, global, <element>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  DriverGroup = class(TRemotable)
  private
    FDriverGroupId: driverGroupIdType;
    FFleetId: fleetidType;
    FName_: string;
    FDescription: string;
  published
    property DriverGroupId: driverGroupIdType  read FDriverGroupId write FDriverGroupId;
    property FleetId:       fleetidType        read FFleetId write FFleetId;
    property Name_:         string             read FName_ write FName_;
    property Description:   string             read FDescription write FDescription;
  end;



  // ************************************************************************ //
  // XML       : dtcoCapabilityType, global, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  dtcoCapabilityType = class(TRemotable)
  private
    FType_: SmallInt;
    FValue: SmallInt;
  published
    property Type_: SmallInt  read FType_ write FType_;
    property Value: SmallInt  read FValue write FValue;
  end;

  Array_Of_Kind = array of Kind;                { "http://www.fleetboard.com/data"[Ubnd] }
  tmConfigIdType  =  type Int64;      { "http://www.fleetboard.com/data"[GblSmpl] }
  limitType       =  type Int64;      { "http://www.fleetboard.com/data"[GblSmpl] }
  versionType     =  type Int64;      { "http://www.fleetboard.com/data"[GblSmpl] }
  offsetType      =  type Int64;      { "http://www.fleetboard.com/data"[GblSmpl] }


  // ************************************************************************ //
  // XML       : GetStatusDefRequestType, global, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  GetStatusDefRequestType = class(TRemotable)
  private
    Flimit: limitType;
    Flimit_Specified: boolean;
    Foffset: offsetType;
    Foffset_Specified: boolean;
    Fversion: versionType;
    Fversion_Specified: boolean;
    FKind: Array_Of_Kind;
    FKind_Specified: boolean;
    FInoid: Array_Of_inoidType;
    FInoid_Specified: boolean;
    FFbid: Array_Of_fbidType;
    FFbid_Specified: boolean;
    FMultiFleet: Boolean;
    FMultiFleet_Specified: boolean;
    FTmConfigId: tmConfigIdType;
    FTmConfigId_Specified: boolean;
    procedure Setlimit(Index: Integer; const AlimitType: limitType);
    function  limit_Specified(Index: Integer): boolean;
    procedure Setoffset(Index: Integer; const AoffsetType: offsetType);
    function  offset_Specified(Index: Integer): boolean;
    procedure Setversion(Index: Integer; const AversionType: versionType);
    function  version_Specified(Index: Integer): boolean;
    procedure SetKind(Index: Integer; const AArray_Of_Kind: Array_Of_Kind);
    function  Kind_Specified(Index: Integer): boolean;
    procedure SetInoid(Index: Integer; const AArray_Of_inoidType: Array_Of_inoidType);
    function  Inoid_Specified(Index: Integer): boolean;
    procedure SetFbid(Index: Integer; const AArray_Of_fbidType: Array_Of_fbidType);
    function  Fbid_Specified(Index: Integer): boolean;
    procedure SetMultiFleet(Index: Integer; const ABoolean: Boolean);
    function  MultiFleet_Specified(Index: Integer): boolean;
    procedure SetTmConfigId(Index: Integer; const AtmConfigIdType: tmConfigIdType);
    function  TmConfigId_Specified(Index: Integer): boolean;
  published
    property limit:      limitType           Index (IS_ATTR or IS_OPTN) read Flimit write Setlimit stored limit_Specified;
    property offset:     offsetType          Index (IS_ATTR or IS_OPTN) read Foffset write Setoffset stored offset_Specified;
    property version:    versionType         Index (IS_ATTR or IS_OPTN) read Fversion write Setversion stored version_Specified;
    property Kind:       Array_Of_Kind       Index (IS_OPTN or IS_UNBD) read FKind write SetKind stored Kind_Specified;
    property Inoid:      Array_Of_inoidType  Index (IS_OPTN or IS_UNBD) read FInoid write SetInoid stored Inoid_Specified;
    property Fbid:       Array_Of_fbidType   Index (IS_OPTN or IS_UNBD) read FFbid write SetFbid stored Fbid_Specified;
    property MultiFleet: Boolean             Index (IS_OPTN) read FMultiFleet write SetMultiFleet stored MultiFleet_Specified;
    property TmConfigId: tmConfigIdType      Index (IS_OPTN) read FTmConfigId write SetTmConfigId stored TmConfigId_Specified;
  end;



  // ************************************************************************ //
  // XML       : GetStatusDefResponseType, global, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  GetStatusDefResponseType = class(TRemotable)
  private
    Flimit: limitType;
    Flimit_Specified: boolean;
    Foffset: offsetType;
    Foffset_Specified: boolean;
    FresultSize: resultSizeType;
    FresultSize_Specified: boolean;
    FresponseSize: responseSizeType;
    FresponseSize_Specified: boolean;
    FStatusDef: Array_Of_StatusDef;
    FStatusDef_Specified: boolean;
    procedure Setlimit(Index: Integer; const AlimitType: limitType);
    function  limit_Specified(Index: Integer): boolean;
    procedure Setoffset(Index: Integer; const AoffsetType: offsetType);
    function  offset_Specified(Index: Integer): boolean;
    procedure SetresultSize(Index: Integer; const AresultSizeType: resultSizeType);
    function  resultSize_Specified(Index: Integer): boolean;
    procedure SetresponseSize(Index: Integer; const AresponseSizeType: responseSizeType);
    function  responseSize_Specified(Index: Integer): boolean;
    procedure SetStatusDef(Index: Integer; const AArray_Of_StatusDef: Array_Of_StatusDef);
    function  StatusDef_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property limit:        limitType           Index (IS_ATTR or IS_OPTN) read Flimit write Setlimit stored limit_Specified;
    property offset:       offsetType          Index (IS_ATTR or IS_OPTN) read Foffset write Setoffset stored offset_Specified;
    property resultSize:   resultSizeType      Index (IS_ATTR or IS_OPTN) read FresultSize write SetresultSize stored resultSize_Specified;
    property responseSize: responseSizeType    Index (IS_ATTR or IS_OPTN) read FresponseSize write SetresponseSize stored responseSize_Specified;
    property StatusDef:    Array_Of_StatusDef  Index (IS_OPTN or IS_UNBD or IS_REF) read FStatusDef write SetStatusDef stored StatusDef_Specified;
  end;



  // ************************************************************************ //
  // XML       : GetStatusDefResponse, global, <element>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  GetStatusDefResponse = class(GetStatusDefResponseType)
  private
  published
  end;



  // ************************************************************************ //
  // XML       : GetDriverRequestType, global, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  GetDriverRequestType = class(TRemotable)
  private
    Flimit: limitType;
    Flimit_Specified: boolean;
    Foffset: offsetType;
    Foffset_Specified: boolean;
    Fversion: versionType;
    Fversion_Specified: boolean;
    FDriverNameID: Array_Of_driverNameIDType;
    FDriverNameID_Specified: boolean;
    FDriverGroupID: driverGroupIdType;
    FDriverGroupID_Specified: boolean;
    FStatus: driverStatusType;
    FStatus_Specified: boolean;
    procedure Setlimit(Index: Integer; const AlimitType: limitType);
    function  limit_Specified(Index: Integer): boolean;
    procedure Setoffset(Index: Integer; const AoffsetType: offsetType);
    function  offset_Specified(Index: Integer): boolean;
    procedure Setversion(Index: Integer; const AversionType: versionType);
    function  version_Specified(Index: Integer): boolean;
    procedure SetDriverNameID(Index: Integer; const AArray_Of_driverNameIDType: Array_Of_driverNameIDType);
    function  DriverNameID_Specified(Index: Integer): boolean;
    procedure SetDriverGroupID(Index: Integer; const AdriverGroupIdType: driverGroupIdType);
    function  DriverGroupID_Specified(Index: Integer): boolean;
    procedure SetStatus(Index: Integer; const AdriverStatusType: driverStatusType);
    function  Status_Specified(Index: Integer): boolean;
  published
    property limit:         limitType                  Index (IS_ATTR or IS_OPTN) read Flimit write Setlimit stored limit_Specified;
    property offset:        offsetType                 Index (IS_ATTR or IS_OPTN) read Foffset write Setoffset stored offset_Specified;
    property version:       versionType                Index (IS_ATTR or IS_OPTN) read Fversion write Setversion stored version_Specified;
    property DriverNameID:  Array_Of_driverNameIDType  Index (IS_OPTN or IS_UNBD) read FDriverNameID write SetDriverNameID stored DriverNameID_Specified;
    property DriverGroupID: driverGroupIdType          Index (IS_OPTN) read FDriverGroupID write SetDriverGroupID stored DriverGroupID_Specified;
    property Status:        driverStatusType           Index (IS_OPTN) read FStatus write SetStatus stored Status_Specified;
  end;



  // ************************************************************************ //
  // XML       : GetDriverRequest, global, <element>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  GetDriverRequest = class(GetDriverRequestType)
  private
  published
  end;



  // ************************************************************************ //
  // XML       : GetDriverResponseType, global, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  GetDriverResponseType = class(TRemotable)
  private
    Flimit: limitType;
    Flimit_Specified: boolean;
    Foffset: offsetType;
    Foffset_Specified: boolean;
    FresultSize: resultSizeType;
    FresultSize_Specified: boolean;
    FresponseSize: responseSizeType;
    FresponseSize_Specified: boolean;
    FDriverInfo: Array_Of_DriverInfo;
    FDriverInfo_Specified: boolean;
    procedure Setlimit(Index: Integer; const AlimitType: limitType);
    function  limit_Specified(Index: Integer): boolean;
    procedure Setoffset(Index: Integer; const AoffsetType: offsetType);
    function  offset_Specified(Index: Integer): boolean;
    procedure SetresultSize(Index: Integer; const AresultSizeType: resultSizeType);
    function  resultSize_Specified(Index: Integer): boolean;
    procedure SetresponseSize(Index: Integer; const AresponseSizeType: responseSizeType);
    function  responseSize_Specified(Index: Integer): boolean;
    procedure SetDriverInfo(Index: Integer; const AArray_Of_DriverInfo: Array_Of_DriverInfo);
    function  DriverInfo_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property limit:        limitType            Index (IS_ATTR or IS_OPTN) read Flimit write Setlimit stored limit_Specified;
    property offset:       offsetType           Index (IS_ATTR or IS_OPTN) read Foffset write Setoffset stored offset_Specified;
    property resultSize:   resultSizeType       Index (IS_ATTR or IS_OPTN) read FresultSize write SetresultSize stored resultSize_Specified;
    property responseSize: responseSizeType     Index (IS_ATTR or IS_OPTN) read FresponseSize write SetresponseSize stored responseSize_Specified;
    property DriverInfo:   Array_Of_DriverInfo  Index (IS_OPTN or IS_UNBD) read FDriverInfo write SetDriverInfo stored DriverInfo_Specified;
  end;



  // ************************************************************************ //
  // XML       : GetDriverResponse, global, <element>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  GetDriverResponse = class(GetDriverResponseType)
  private
  published
  end;



  // ************************************************************************ //
  // XML       : GetUserResponseType, global, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  GetUserResponseType = class(TRemotable)
  private
    Flimit: limitType;
    Flimit_Specified: boolean;
    Foffset: offsetType;
    Foffset_Specified: boolean;
    FresultSize: resultSizeType;
    FresultSize_Specified: boolean;
    FresponseSize: responseSizeType;
    FresponseSize_Specified: boolean;
    FUser: Array_Of_TmSoapUserType;
    FUser_Specified: boolean;
    procedure Setlimit(Index: Integer; const AlimitType: limitType);
    function  limit_Specified(Index: Integer): boolean;
    procedure Setoffset(Index: Integer; const AoffsetType: offsetType);
    function  offset_Specified(Index: Integer): boolean;
    procedure SetresultSize(Index: Integer; const AresultSizeType: resultSizeType);
    function  resultSize_Specified(Index: Integer): boolean;
    procedure SetresponseSize(Index: Integer; const AresponseSizeType: responseSizeType);
    function  responseSize_Specified(Index: Integer): boolean;
    procedure SetUser(Index: Integer; const AArray_Of_TmSoapUserType: Array_Of_TmSoapUserType);
    function  User_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property limit:        limitType                Index (IS_ATTR or IS_OPTN) read Flimit write Setlimit stored limit_Specified;
    property offset:       offsetType               Index (IS_ATTR or IS_OPTN) read Foffset write Setoffset stored offset_Specified;
    property resultSize:   resultSizeType           Index (IS_ATTR or IS_OPTN) read FresultSize write SetresultSize stored resultSize_Specified;
    property responseSize: responseSizeType         Index (IS_ATTR or IS_OPTN) read FresponseSize write SetresponseSize stored responseSize_Specified;
    property User:         Array_Of_TmSoapUserType  Index (IS_OPTN or IS_UNBD) read FUser write SetUser stored User_Specified;
  end;



  // ************************************************************************ //
  // XML       : GetUserResponse, global, <element>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  GetUserResponse = class(GetUserResponseType)
  private
  published
  end;



  // ************************************************************************ //
  // XML       : GetDriverGroupRequest, global, <element>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  GetDriverGroupRequest = class(TRemotable)
  private
    Flimit: limitType;
    Flimit_Specified: boolean;
    Foffset: offsetType;
    Foffset_Specified: boolean;
    FFleetId: fleetidType;
    FFleetId_Specified: boolean;
    FDriverGroupId: driverGroupIdType;
    FDriverGroupId_Specified: boolean;
    procedure Setlimit(Index: Integer; const AlimitType: limitType);
    function  limit_Specified(Index: Integer): boolean;
    procedure Setoffset(Index: Integer; const AoffsetType: offsetType);
    function  offset_Specified(Index: Integer): boolean;
    procedure SetFleetId(Index: Integer; const AfleetidType: fleetidType);
    function  FleetId_Specified(Index: Integer): boolean;
    procedure SetDriverGroupId(Index: Integer; const AdriverGroupIdType: driverGroupIdType);
    function  DriverGroupId_Specified(Index: Integer): boolean;
  published
    property limit:         limitType          Index (IS_ATTR or IS_OPTN) read Flimit write Setlimit stored limit_Specified;
    property offset:        offsetType         Index (IS_ATTR or IS_OPTN) read Foffset write Setoffset stored offset_Specified;
    property FleetId:       fleetidType        Index (IS_OPTN) read FFleetId write SetFleetId stored FleetId_Specified;
    property DriverGroupId: driverGroupIdType  Index (IS_OPTN) read FDriverGroupId write SetDriverGroupId stored DriverGroupId_Specified;
  end;



  // ************************************************************************ //
  // XML       : GetCurrentUserRequest, global, <element>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  GetCurrentUserRequest = class(TRemotable)
  private
    Flimit: limitType;
    Flimit_Specified: boolean;
    Foffset: offsetType;
    Foffset_Specified: boolean;
    Fversion: versionType;
    Fversion_Specified: boolean;
    procedure Setlimit(Index: Integer; const AlimitType: limitType);
    function  limit_Specified(Index: Integer): boolean;
    procedure Setoffset(Index: Integer; const AoffsetType: offsetType);
    function  offset_Specified(Index: Integer): boolean;
    procedure Setversion(Index: Integer; const AversionType: versionType);
    function  version_Specified(Index: Integer): boolean;
  published
    property limit:   limitType    Index (IS_ATTR or IS_OPTN) read Flimit write Setlimit stored limit_Specified;
    property offset:  offsetType   Index (IS_ATTR or IS_OPTN) read Foffset write Setoffset stored offset_Specified;
    property version: versionType  Index (IS_ATTR or IS_OPTN) read Fversion write Setversion stored version_Specified;
  end;



  // ************************************************************************ //
  // XML       : GetDriverGroupResponse, global, <element>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  GetDriverGroupResponse = class(TRemotable)
  private
    Flimit: limitType;
    Flimit_Specified: boolean;
    Foffset: offsetType;
    Foffset_Specified: boolean;
    FresultSize: resultSizeType;
    FresultSize_Specified: boolean;
    FresponseSize: responseSizeType;
    FresponseSize_Specified: boolean;
    FDriverGroup: Array_Of_DriverGroup;
    FDriverGroup_Specified: boolean;
    procedure Setlimit(Index: Integer; const AlimitType: limitType);
    function  limit_Specified(Index: Integer): boolean;
    procedure Setoffset(Index: Integer; const AoffsetType: offsetType);
    function  offset_Specified(Index: Integer): boolean;
    procedure SetresultSize(Index: Integer; const AresultSizeType: resultSizeType);
    function  resultSize_Specified(Index: Integer): boolean;
    procedure SetresponseSize(Index: Integer; const AresponseSizeType: responseSizeType);
    function  responseSize_Specified(Index: Integer): boolean;
    procedure SetDriverGroup(Index: Integer; const AArray_Of_DriverGroup: Array_Of_DriverGroup);
    function  DriverGroup_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property limit:        limitType             Index (IS_ATTR or IS_OPTN) read Flimit write Setlimit stored limit_Specified;
    property offset:       offsetType            Index (IS_ATTR or IS_OPTN) read Foffset write Setoffset stored offset_Specified;
    property resultSize:   resultSizeType        Index (IS_ATTR or IS_OPTN) read FresultSize write SetresultSize stored resultSize_Specified;
    property responseSize: responseSizeType      Index (IS_ATTR or IS_OPTN) read FresponseSize write SetresponseSize stored responseSize_Specified;
    property DriverGroup:  Array_Of_DriverGroup  Index (IS_OPTN or IS_UNBD or IS_REF) read FDriverGroup write SetDriverGroup stored DriverGroup_Specified;
  end;

  DriverGroup2 = array of driverGroupIdType;    { "http://www.fleetboard.com/data"[Cplx] }


  // ************************************************************************ //
  // XML       : DriverInfo, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  DriverInfo = class(TRemotable)
  private
    FDriverNameID: driverNameIDType;
    FDriverToken: Array_Of_DriverToken;
    FDriverToken_Specified: boolean;
    FFirstName: string;
    FLastName: string;
    FLocale: string;
    FComment: string;
    FStatus: driverStatusType;
    FStatus_Specified: boolean;
    FDriverGroup: DriverGroup2;
    FDriverGroup_Specified: boolean;
    procedure SetDriverToken(Index: Integer; const AArray_Of_DriverToken: Array_Of_DriverToken);
    function  DriverToken_Specified(Index: Integer): boolean;
    procedure SetStatus(Index: Integer; const AdriverStatusType: driverStatusType);
    function  Status_Specified(Index: Integer): boolean;
    procedure SetDriverGroup(Index: Integer; const ADriverGroup2: DriverGroup2);
    function  DriverGroup_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property DriverNameID: driverNameIDType      read FDriverNameID write FDriverNameID;
    property DriverToken:  Array_Of_DriverToken  Index (IS_OPTN or IS_UNBD) read FDriverToken write SetDriverToken stored DriverToken_Specified;
    property FirstName:    string                read FFirstName write FFirstName;
    property LastName:     string                read FLastName write FLastName;
    property Locale:       string                read FLocale write FLocale;
    property Comment:      string                read FComment write FComment;
    property Status:       driverStatusType      Index (IS_OPTN) read FStatus write SetStatus stored Status_Specified;
    property DriverGroup:  DriverGroup2          Index (IS_OPTN) read FDriverGroup write SetDriverGroup stored DriverGroup_Specified;
  end;

  GROUPVEHICLE = array of string;               { "http://www.fleetboard.com/data"[Cplx] }


  // ************************************************************************ //
  // XML       : FLEETVEHICLE, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  FLEETVEHICLE = class(TRemotable)
  private
    FFLEETID: fleetidType;
    FREGISTRATION: string;
    FREGISTRATION_Specified: boolean;
    FVEHICLENUMBER: string;
    FINSMSISDN: string;
    FINSMSISDN_Specified: boolean;
    FSTARTTIME: string;
    FSTARTTIME_Specified: boolean;
    FENDTIME: string;
    FENDTIME_Specified: boolean;
    FVEHICLETYPE: string;
    FVEHICLETYPE_Specified: boolean;
    FSTATUS: Int64;
    FSTATUS_Specified: boolean;
    procedure SetREGISTRATION(Index: Integer; const Astring: string);
    function  REGISTRATION_Specified(Index: Integer): boolean;
    procedure SetINSMSISDN(Index: Integer; const Astring: string);
    function  INSMSISDN_Specified(Index: Integer): boolean;
    procedure SetSTARTTIME(Index: Integer; const Astring: string);
    function  STARTTIME_Specified(Index: Integer): boolean;
    procedure SetENDTIME(Index: Integer; const Astring: string);
    function  ENDTIME_Specified(Index: Integer): boolean;
    procedure SetVEHICLETYPE(Index: Integer; const Astring: string);
    function  VEHICLETYPE_Specified(Index: Integer): boolean;
    procedure SetSTATUS(Index: Integer; const AInt64: Int64);
    function  STATUS_Specified(Index: Integer): boolean;
  published
    property FLEETID:       fleetidType  read FFLEETID write FFLEETID;
    property REGISTRATION:  string       Index (IS_OPTN) read FREGISTRATION write SetREGISTRATION stored REGISTRATION_Specified;
    property VEHICLENUMBER: string       read FVEHICLENUMBER write FVEHICLENUMBER;
    property INSMSISDN:     string       Index (IS_OPTN) read FINSMSISDN write SetINSMSISDN stored INSMSISDN_Specified;
    property STARTTIME:     string       Index (IS_OPTN) read FSTARTTIME write SetSTARTTIME stored STARTTIME_Specified;
    property ENDTIME:       string       Index (IS_OPTN) read FENDTIME write SetENDTIME stored ENDTIME_Specified;
    property VEHICLETYPE:   string       Index (IS_OPTN) read FVEHICLETYPE write SetVEHICLETYPE stored VEHICLETYPE_Specified;
    property STATUS:        Int64        Index (IS_OPTN) read FSTATUS write SetSTATUS stored STATUS_Specified;
  end;



  // ************************************************************************ //
  // XML       : GetVehicleResponse, global, <element>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  GetVehicleResponse = class(TRemotable)
  private
    Flimit: limitType;
    Flimit_Specified: boolean;
    Foffset: offsetType;
    Foffset_Specified: boolean;
    FresultSize: resultSizeType;
    FresultSize_Specified: boolean;
    FresponseSize: responseSizeType;
    FresponseSize_Specified: boolean;
    FVEHICLE: Array_Of_VEHICLE;
    FVEHICLE_Specified: boolean;
    procedure Setlimit(Index: Integer; const AlimitType: limitType);
    function  limit_Specified(Index: Integer): boolean;
    procedure Setoffset(Index: Integer; const AoffsetType: offsetType);
    function  offset_Specified(Index: Integer): boolean;
    procedure SetresultSize(Index: Integer; const AresultSizeType: resultSizeType);
    function  resultSize_Specified(Index: Integer): boolean;
    procedure SetresponseSize(Index: Integer; const AresponseSizeType: responseSizeType);
    function  responseSize_Specified(Index: Integer): boolean;
    procedure SetVEHICLE(Index: Integer; const AArray_Of_VEHICLE: Array_Of_VEHICLE);
    function  VEHICLE_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property limit:        limitType         Index (IS_ATTR or IS_OPTN) read Flimit write Setlimit stored limit_Specified;
    property offset:       offsetType        Index (IS_ATTR or IS_OPTN) read Foffset write Setoffset stored offset_Specified;
    property resultSize:   resultSizeType    Index (IS_ATTR or IS_OPTN) read FresultSize write SetresultSize stored resultSize_Specified;
    property responseSize: responseSizeType  Index (IS_ATTR or IS_OPTN) read FresponseSize write SetresponseSize stored responseSize_Specified;
    property VEHICLE:      Array_Of_VEHICLE  Index (IS_OPTN or IS_UNBD or IS_REF) read FVEHICLE write SetVEHICLE stored VEHICLE_Specified;
  end;

  VEHICLEPROPERTIES = array of PropertyType;    { "http://www.fleetboard.com/data"[Cplx] }


  // ************************************************************************ //
  // XML       : GetFleetResponse, global, <element>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  GetFleetResponse = class(TRemotable)
  private
    Flimit: limitType;
    Flimit_Specified: boolean;
    Foffset: offsetType;
    Foffset_Specified: boolean;
    FresultSize: resultSizeType;
    FresultSize_Specified: boolean;
    FresponseSize: responseSizeType;
    FresponseSize_Specified: boolean;
    FFLEET: FLEET;
    procedure Setlimit(Index: Integer; const AlimitType: limitType);
    function  limit_Specified(Index: Integer): boolean;
    procedure Setoffset(Index: Integer; const AoffsetType: offsetType);
    function  offset_Specified(Index: Integer): boolean;
    procedure SetresultSize(Index: Integer; const AresultSizeType: resultSizeType);
    function  resultSize_Specified(Index: Integer): boolean;
    procedure SetresponseSize(Index: Integer; const AresponseSizeType: responseSizeType);
    function  responseSize_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property limit:        limitType         Index (IS_ATTR or IS_OPTN) read Flimit write Setlimit stored limit_Specified;
    property offset:       offsetType        Index (IS_ATTR or IS_OPTN) read Foffset write Setoffset stored offset_Specified;
    property resultSize:   resultSizeType    Index (IS_ATTR or IS_OPTN) read FresultSize write SetresultSize stored resultSize_Specified;
    property responseSize: responseSizeType  Index (IS_ATTR or IS_OPTN) read FresponseSize write SetresponseSize stored responseSize_Specified;
    property FLEET:        FLEET             Index (IS_REF) read FFLEET write FFLEET;
  end;



  // ************************************************************************ //
  // XML       : VEHICLEHARDWARE, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  VEHICLEHARDWARE = class(TRemotable)
  private
    FHARDWARE: string;
    FHARDWAREID: Int64;
  published
    property HARDWARE:   string  read FHARDWARE write FHARDWARE;
    property HARDWAREID: Int64   read FHARDWAREID write FHARDWAREID;
  end;



  // ************************************************************************ //
  // XML       : GetStatusDefRequest, global, <element>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  GetStatusDefRequest = class(GetStatusDefRequestType)
  private
  published
  end;



  // ************************************************************************ //
  // XML       : GetVehicleGroupResponse, global, <element>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  GetVehicleGroupResponse = class(TRemotable)
  private
    Flimit: limitType;
    Flimit_Specified: boolean;
    Foffset: offsetType;
    Foffset_Specified: boolean;
    FresultSize: resultSizeType;
    FresultSize_Specified: boolean;
    FresponseSize: responseSizeType;
    FresponseSize_Specified: boolean;
    FVEHICLEGROUP: Array_Of_VEHICLEGROUP;
    FVEHICLEGROUP_Specified: boolean;
    procedure Setlimit(Index: Integer; const AlimitType: limitType);
    function  limit_Specified(Index: Integer): boolean;
    procedure Setoffset(Index: Integer; const AoffsetType: offsetType);
    function  offset_Specified(Index: Integer): boolean;
    procedure SetresultSize(Index: Integer; const AresultSizeType: resultSizeType);
    function  resultSize_Specified(Index: Integer): boolean;
    procedure SetresponseSize(Index: Integer; const AresponseSizeType: responseSizeType);
    function  responseSize_Specified(Index: Integer): boolean;
    procedure SetVEHICLEGROUP(Index: Integer; const AArray_Of_VEHICLEGROUP: Array_Of_VEHICLEGROUP);
    function  VEHICLEGROUP_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property limit:        limitType              Index (IS_ATTR or IS_OPTN) read Flimit write Setlimit stored limit_Specified;
    property offset:       offsetType             Index (IS_ATTR or IS_OPTN) read Foffset write Setoffset stored offset_Specified;
    property resultSize:   resultSizeType         Index (IS_ATTR or IS_OPTN) read FresultSize write SetresultSize stored resultSize_Specified;
    property responseSize: responseSizeType       Index (IS_ATTR or IS_OPTN) read FresponseSize write SetresponseSize stored responseSize_Specified;
    property VEHICLEGROUP: Array_Of_VEHICLEGROUP  Index (IS_OPTN or IS_UNBD or IS_REF) read FVEHICLEGROUP write SetVEHICLEGROUP stored VEHICLEGROUP_Specified;
  end;



  // ************************************************************************ //
  // XML       : VEHICLEGROUP, global, <element>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  VEHICLEGROUP = class(TRemotable)
  private
    Finoid: inoidType;
    FGROUPID: string;
    FFLEETID: fleetidType;
    FGROUPNAME: string;
    FDescription: string;
    FDescription_Specified: boolean;
    procedure SetDescription(Index: Integer; const Astring: string);
    function  Description_Specified(Index: Integer): boolean;
  published
    property inoid:       inoidType    Index (IS_ATTR) read Finoid write Finoid;
    property GROUPID:     string       read FGROUPID write FGROUPID;
    property FLEETID:     fleetidType  read FFLEETID write FFLEETID;
    property GROUPNAME:   string       read FGROUPNAME write FGROUPNAME;
    property Description: string       Index (IS_OPTN) read FDescription write SetDescription stored Description_Specified;
  end;

  Array_Of_Property = array of Property_;       { "http://www.fleetboard.com/data"[Ubnd] }


  // ************************************************************************ //
  // XML       : GetServerPropertiesRequest, global, <element>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  GetServerPropertiesRequest = class(TRemotable)
  private
    Flimit: limitType;
    Flimit_Specified: boolean;
    Foffset: offsetType;
    Foffset_Specified: boolean;
    FProperty_: Array_Of_Property;
    FProperty__Specified: boolean;
    procedure Setlimit(Index: Integer; const AlimitType: limitType);
    function  limit_Specified(Index: Integer): boolean;
    procedure Setoffset(Index: Integer; const AoffsetType: offsetType);
    function  offset_Specified(Index: Integer): boolean;
    procedure SetProperty_(Index: Integer; const AArray_Of_Property: Array_Of_Property);
    function  Property__Specified(Index: Integer): boolean;
  published
    property limit:     limitType          Index (IS_ATTR or IS_OPTN) read Flimit write Setlimit stored limit_Specified;
    property offset:    offsetType         Index (IS_ATTR or IS_OPTN) read Foffset write Setoffset stored offset_Specified;
    property Property_: Array_Of_Property  Index (IS_OPTN or IS_UNBD) read FProperty_ write SetProperty_ stored Property__Specified;
  end;

  sessionidType   =  type string;      { "http://www.fleetboard.com/data"[GblSmpl] }
  useridType      =  type Int64;      { "http://www.fleetboard.com/data"[GblSmpl] }
  USERID          =  type useridType;      { "http://www.fleetboard.com/data"[Smpl] }


  // ************************************************************************ //
  // XML       : User, global, <element>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  User = class(TRemotable)
  private
    FUsertype: string;
    FUsertype_Specified: boolean;
    FUSERID: USERID;
    FTimestamp: timestampType;
    FMsgID: MsgID;
    FMsgID_Specified: boolean;
    procedure SetUsertype(Index: Integer; const Astring: string);
    function  Usertype_Specified(Index: Integer): boolean;
    procedure SetMsgID(Index: Integer; const AMsgID: MsgID);
    function  MsgID_Specified(Index: Integer): boolean;
  published
    property Usertype:  string         Index (IS_ATTR or IS_OPTN) read FUsertype write SetUsertype stored Usertype_Specified;
    property USERID:    USERID         read FUSERID write FUSERID;
    property Timestamp: timestampType  read FTimestamp write FTimestamp;
    property MsgID:     MsgID          Index (IS_OPTN) read FMsgID write SetMsgID stored MsgID_Specified;
  end;

  Array_Of_useridType = array of useridType;    { "http://www.fleetboard.com/data"[GblUbnd] }


  // ************************************************************************ //
  // XML       : GetUserRequest, global, <element>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  GetUserRequest = class(TRemotable)
  private
    Flimit: limitType;
    Flimit_Specified: boolean;
    Foffset: offsetType;
    Foffset_Specified: boolean;
    Fversion: versionType;
    Fversion_Specified: boolean;
    FSettingsType: Array_Of_SettingsType;
    FSettingsType_Specified: boolean;
    FUserID: Array_Of_useridType;
    FUserID_Specified: boolean;
    FProfileId: Array_Of_idType;
    FProfileId_Specified: boolean;
    procedure Setlimit(Index: Integer; const AlimitType: limitType);
    function  limit_Specified(Index: Integer): boolean;
    procedure Setoffset(Index: Integer; const AoffsetType: offsetType);
    function  offset_Specified(Index: Integer): boolean;
    procedure Setversion(Index: Integer; const AversionType: versionType);
    function  version_Specified(Index: Integer): boolean;
    procedure SetSettingsType(Index: Integer; const AArray_Of_SettingsType: Array_Of_SettingsType);
    function  SettingsType_Specified(Index: Integer): boolean;
    procedure SetUserID(Index: Integer; const AArray_Of_useridType: Array_Of_useridType);
    function  UserID_Specified(Index: Integer): boolean;
    procedure SetProfileId(Index: Integer; const AArray_Of_idType: Array_Of_idType);
    function  ProfileId_Specified(Index: Integer): boolean;
  published
    property limit:        limitType              Index (IS_ATTR or IS_OPTN) read Flimit write Setlimit stored limit_Specified;
    property offset:       offsetType             Index (IS_ATTR or IS_OPTN) read Foffset write Setoffset stored offset_Specified;
    property version:      versionType            Index (IS_ATTR or IS_OPTN) read Fversion write Setversion stored version_Specified;
    property SettingsType: Array_Of_SettingsType  Index (IS_OPTN or IS_UNBD) read FSettingsType write SetSettingsType stored SettingsType_Specified;
    property UserID:       Array_Of_useridType    Index (IS_OPTN or IS_UNBD) read FUserID write SetUserID stored UserID_Specified;
    property ProfileId:    Array_Of_idType        Index (IS_OPTN or IS_UNBD) read FProfileId write SetProfileId stored ProfileId_Specified;
  end;

  Array_Of_tmmodeType = array of tmmodeType;    { "http://www.fleetboard.com/data"[GblUbnd] }
  Array_Of_leasingStatusType = array of leasingStatusType;   { "http://www.fleetboard.com/data"[GblUbnd] }
  vehicleGroupIdType =  type Int64;      { "http://www.fleetboard.com/data"[GblSmpl] }


  // ************************************************************************ //
  // XML       : GetVehicleGroupRequest, global, <element>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  GetVehicleGroupRequest = class(TRemotable)
  private
    Flimit: limitType;
    Flimit_Specified: boolean;
    Foffset: offsetType;
    Foffset_Specified: boolean;
    Fversion: versionType;
    Fversion_Specified: boolean;
    FFleetId: fleetidType;
    FFleetId_Specified: boolean;
    FGroupId: vehicleGroupIdType;
    FGroupId_Specified: boolean;
    procedure Setlimit(Index: Integer; const AlimitType: limitType);
    function  limit_Specified(Index: Integer): boolean;
    procedure Setoffset(Index: Integer; const AoffsetType: offsetType);
    function  offset_Specified(Index: Integer): boolean;
    procedure Setversion(Index: Integer; const AversionType: versionType);
    function  version_Specified(Index: Integer): boolean;
    procedure SetFleetId(Index: Integer; const AfleetidType: fleetidType);
    function  FleetId_Specified(Index: Integer): boolean;
    procedure SetGroupId(Index: Integer; const AvehicleGroupIdType: vehicleGroupIdType);
    function  GroupId_Specified(Index: Integer): boolean;
  published
    property limit:   limitType           Index (IS_ATTR or IS_OPTN) read Flimit write Setlimit stored limit_Specified;
    property offset:  offsetType          Index (IS_ATTR or IS_OPTN) read Foffset write Setoffset stored offset_Specified;
    property version: versionType         Index (IS_ATTR or IS_OPTN) read Fversion write Setversion stored version_Specified;
    property FleetId: fleetidType         Index (IS_OPTN) read FFleetId write SetFleetId stored FleetId_Specified;
    property GroupId: vehicleGroupIdType  Index (IS_OPTN) read FGroupId write SetGroupId stored GroupId_Specified;
  end;



  // ************************************************************************ //
  // XML       : GetCurrentUserResponseType, global, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  GetCurrentUserResponseType = class(TRemotable)
  private
    Flimit: limitType;
    Flimit_Specified: boolean;
    Foffset: offsetType;
    Foffset_Specified: boolean;
    FresultSize: resultSizeType;
    FresultSize_Specified: boolean;
    FresponseSize: responseSizeType;
    FresponseSize_Specified: boolean;
    FUser: TmSoapUserType;
    FUser_Specified: boolean;
    procedure Setlimit(Index: Integer; const AlimitType: limitType);
    function  limit_Specified(Index: Integer): boolean;
    procedure Setoffset(Index: Integer; const AoffsetType: offsetType);
    function  offset_Specified(Index: Integer): boolean;
    procedure SetresultSize(Index: Integer; const AresultSizeType: resultSizeType);
    function  resultSize_Specified(Index: Integer): boolean;
    procedure SetresponseSize(Index: Integer; const AresponseSizeType: responseSizeType);
    function  responseSize_Specified(Index: Integer): boolean;
    procedure SetUser(Index: Integer; const ATmSoapUserType: TmSoapUserType);
    function  User_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property limit:        limitType         Index (IS_ATTR or IS_OPTN) read Flimit write Setlimit stored limit_Specified;
    property offset:       offsetType        Index (IS_ATTR or IS_OPTN) read Foffset write Setoffset stored offset_Specified;
    property resultSize:   resultSizeType    Index (IS_ATTR or IS_OPTN) read FresultSize write SetresultSize stored resultSize_Specified;
    property responseSize: responseSizeType  Index (IS_ATTR or IS_OPTN) read FresponseSize write SetresponseSize stored responseSize_Specified;
    property User:         TmSoapUserType    Index (IS_OPTN) read FUser write SetUser stored User_Specified;
  end;



  // ************************************************************************ //
  // XML       : GetCurrentUserResponse, global, <element>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  GetCurrentUserResponse = class(GetCurrentUserResponseType)
  private
  published
  end;



  // ************************************************************************ //
  // XML       : GetVehicleRequest, global, <element>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  GetVehicleRequest = class(TRemotable)
  private
    Flimit: limitType;
    Flimit_Specified: boolean;
    Foffset: offsetType;
    Foffset_Specified: boolean;
    Fversion: versionType;
    Fversion_Specified: boolean;
    FVehicleGroup: vehicleGroupIdType;
    FVehicleGroup_Specified: boolean;
    FServiceType: serviceType;
    FServiceType_Specified: boolean;
    FVehicleID: Array_Of_inoidType;
    FVehicleID_Specified: boolean;
    FStatus: Array_Of_integer;
    FStatus_Specified: boolean;
    FCapability: Array_Of_dtcoCapabilityType;
    FCapability_Specified: boolean;
    FLeasingStatus: Array_Of_leasingStatusType;
    FLeasingStatus_Specified: boolean;
    FTMMode: Array_Of_tmmodeType;
    FTMMode_Specified: boolean;
    procedure Setlimit(Index: Integer; const AlimitType: limitType);
    function  limit_Specified(Index: Integer): boolean;
    procedure Setoffset(Index: Integer; const AoffsetType: offsetType);
    function  offset_Specified(Index: Integer): boolean;
    procedure Setversion(Index: Integer; const AversionType: versionType);
    function  version_Specified(Index: Integer): boolean;
    procedure SetVehicleGroup(Index: Integer; const AvehicleGroupIdType: vehicleGroupIdType);
    function  VehicleGroup_Specified(Index: Integer): boolean;
    procedure SetServiceType(Index: Integer; const AserviceType: serviceType);
    function  ServiceType_Specified(Index: Integer): boolean;
    procedure SetVehicleID(Index: Integer; const AArray_Of_inoidType: Array_Of_inoidType);
    function  VehicleID_Specified(Index: Integer): boolean;
    procedure SetStatus(Index: Integer; const AArray_Of_integer: Array_Of_integer);
    function  Status_Specified(Index: Integer): boolean;
    procedure SetCapability(Index: Integer; const AArray_Of_dtcoCapabilityType: Array_Of_dtcoCapabilityType);
    function  Capability_Specified(Index: Integer): boolean;
    procedure SetLeasingStatus(Index: Integer; const AArray_Of_leasingStatusType: Array_Of_leasingStatusType);
    function  LeasingStatus_Specified(Index: Integer): boolean;
    procedure SetTMMode(Index: Integer; const AArray_Of_tmmodeType: Array_Of_tmmodeType);
    function  TMMode_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property limit:         limitType                    Index (IS_ATTR or IS_OPTN) read Flimit write Setlimit stored limit_Specified;
    property offset:        offsetType                   Index (IS_ATTR or IS_OPTN) read Foffset write Setoffset stored offset_Specified;
    property version:       versionType                  Index (IS_ATTR or IS_OPTN) read Fversion write Setversion stored version_Specified;
    property VehicleGroup:  vehicleGroupIdType           Index (IS_OPTN) read FVehicleGroup write SetVehicleGroup stored VehicleGroup_Specified;
    property ServiceType:   serviceType                  Index (IS_OPTN) read FServiceType write SetServiceType stored ServiceType_Specified;
    property VehicleID:     Array_Of_inoidType           Index (IS_OPTN or IS_UNBD) read FVehicleID write SetVehicleID stored VehicleID_Specified;
    property Status:        Array_Of_integer             Index (IS_OPTN or IS_UNBD) read FStatus write SetStatus stored Status_Specified;
    property Capability:    Array_Of_dtcoCapabilityType  Index (IS_OPTN or IS_UNBD) read FCapability write SetCapability stored Capability_Specified;
    property LeasingStatus: Array_Of_leasingStatusType   Index (IS_OPTN or IS_UNBD) read FLeasingStatus write SetLeasingStatus stored LeasingStatus_Specified;
    property TMMode:        Array_Of_tmmodeType          Index (IS_OPTN or IS_UNBD) read FTMMode write SetTMMode stored TMMode_Specified;
  end;



  // ************************************************************************ //
  // XML       : VEHICLE, global, <element>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  VEHICLE = class(TRemotable)
  private
    Finoid: inoidType;
    FCHASSIS: string;
    FVEHICLEHARDWARE: Array_Of_VEHICLEHARDWARE;
    FVEHICLEHARDWARE_Specified: boolean;
    FFLEETVEHICLE: FLEETVEHICLE;
    FGROUPVEHICLE: GROUPVEHICLE;
    FGROUPVEHICLE_Specified: boolean;
    FREGISTRATIONDATE: string;
    FREGISTRATIONDATE_Specified: boolean;
    FTELEMATICGROUP: telematicGroupIDType;
    FTELEMATICGROUP_Specified: boolean;
    FVEHICLEPROPERTIES: VEHICLEPROPERTIES;
    FVEHICLEPROPERTIES_Specified: boolean;
    procedure SetVEHICLEHARDWARE(Index: Integer; const AArray_Of_VEHICLEHARDWARE: Array_Of_VEHICLEHARDWARE);
    function  VEHICLEHARDWARE_Specified(Index: Integer): boolean;
    procedure SetGROUPVEHICLE(Index: Integer; const AGROUPVEHICLE: GROUPVEHICLE);
    function  GROUPVEHICLE_Specified(Index: Integer): boolean;
    procedure SetREGISTRATIONDATE(Index: Integer; const Astring: string);
    function  REGISTRATIONDATE_Specified(Index: Integer): boolean;
    procedure SetTELEMATICGROUP(Index: Integer; const AtelematicGroupIDType: telematicGroupIDType);
    function  TELEMATICGROUP_Specified(Index: Integer): boolean;
    procedure SetVEHICLEPROPERTIES(Index: Integer; const AVEHICLEPROPERTIES: VEHICLEPROPERTIES);
    function  VEHICLEPROPERTIES_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property inoid:             inoidType                 Index (IS_ATTR) read Finoid write Finoid;
    property CHASSIS:           string                    read FCHASSIS write FCHASSIS;
    property VEHICLEHARDWARE:   Array_Of_VEHICLEHARDWARE  Index (IS_OPTN or IS_UNBD) read FVEHICLEHARDWARE write SetVEHICLEHARDWARE stored VEHICLEHARDWARE_Specified;
    property FLEETVEHICLE:      FLEETVEHICLE              read FFLEETVEHICLE write FFLEETVEHICLE;
    property GROUPVEHICLE:      GROUPVEHICLE              Index (IS_OPTN) read FGROUPVEHICLE write SetGROUPVEHICLE stored GROUPVEHICLE_Specified;
    property REGISTRATIONDATE:  string                    Index (IS_OPTN) read FREGISTRATIONDATE write SetREGISTRATIONDATE stored REGISTRATIONDATE_Specified;
    property TELEMATICGROUP:    telematicGroupIDType      Index (IS_OPTN) read FTELEMATICGROUP write SetTELEMATICGROUP stored TELEMATICGROUP_Specified;
    property VEHICLEPROPERTIES: VEHICLEPROPERTIES         Index (IS_OPTN) read FVEHICLEPROPERTIES write SetVEHICLEPROPERTIES stored VEHICLEPROPERTIES_Specified;
  end;



  // ************************************************************************ //
  // XML       : LoginResponse, global, <element>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  LoginResponse = class(TRemotable)
  private
    Fsessionid: sessionidType;
    Fsessionid_Specified: boolean;
    procedure Setsessionid(Index: Integer; const AsessionidType: sessionidType);
    function  sessionid_Specified(Index: Integer): boolean;
  published
    property sessionid: sessionidType  Index (IS_ATTR or IS_OPTN) read Fsessionid write Setsessionid stored sessionid_Specified;
  end;



  // ************************************************************************ //
  // XML       : FLEETINFORMATION, <complexType>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  FLEETINFORMATION = class(TRemotable)
  private
    FSTREET: string;
    FSTREET_Specified: boolean;
    FPOSTALCODE: string;
    FPOSTALCODE_Specified: boolean;
    FCITY: string;
    FCITY_Specified: boolean;
    FCOUNTRY: string;
    FCOUNTRY_Specified: boolean;
    FCOUNTRYCODE: string;
    FCOUNTRYCODE_Specified: boolean;
    FREGION: string;
    FREGION_Specified: boolean;
    FREGIONCODE: string;
    FREGIONCODE_Specified: boolean;
    FPAGEHEADING: string;
    FPAGEHEADING_Specified: boolean;
    FFLEETTIMEZONE: Int64;
    FFLEETTIMEZONE_Specified: boolean;
    FFLEETTIMEZONEID: string;
    FFLEETTIMEZONEID_Specified: boolean;
    FDLSAVING: Int64;
    FDLSAVING_Specified: boolean;
    FDELTAETAMON: Int64;
    FDELTAETAMON_Specified: boolean;
    FDELTAAREAMON: Int64;
    FDELTAAREAMON_Specified: boolean;
    procedure SetSTREET(Index: Integer; const Astring: string);
    function  STREET_Specified(Index: Integer): boolean;
    procedure SetPOSTALCODE(Index: Integer; const Astring: string);
    function  POSTALCODE_Specified(Index: Integer): boolean;
    procedure SetCITY(Index: Integer; const Astring: string);
    function  CITY_Specified(Index: Integer): boolean;
    procedure SetCOUNTRY(Index: Integer; const Astring: string);
    function  COUNTRY_Specified(Index: Integer): boolean;
    procedure SetCOUNTRYCODE(Index: Integer; const Astring: string);
    function  COUNTRYCODE_Specified(Index: Integer): boolean;
    procedure SetREGION(Index: Integer; const Astring: string);
    function  REGION_Specified(Index: Integer): boolean;
    procedure SetREGIONCODE(Index: Integer; const Astring: string);
    function  REGIONCODE_Specified(Index: Integer): boolean;
    procedure SetPAGEHEADING(Index: Integer; const Astring: string);
    function  PAGEHEADING_Specified(Index: Integer): boolean;
    procedure SetFLEETTIMEZONE(Index: Integer; const AInt64: Int64);
    function  FLEETTIMEZONE_Specified(Index: Integer): boolean;
    procedure SetFLEETTIMEZONEID(Index: Integer; const Astring: string);
    function  FLEETTIMEZONEID_Specified(Index: Integer): boolean;
    procedure SetDLSAVING(Index: Integer; const AInt64: Int64);
    function  DLSAVING_Specified(Index: Integer): boolean;
    procedure SetDELTAETAMON(Index: Integer; const AInt64: Int64);
    function  DELTAETAMON_Specified(Index: Integer): boolean;
    procedure SetDELTAAREAMON(Index: Integer; const AInt64: Int64);
    function  DELTAAREAMON_Specified(Index: Integer): boolean;
  published
    property STREET:          string  Index (IS_OPTN) read FSTREET write SetSTREET stored STREET_Specified;
    property POSTALCODE:      string  Index (IS_OPTN) read FPOSTALCODE write SetPOSTALCODE stored POSTALCODE_Specified;
    property CITY:            string  Index (IS_OPTN) read FCITY write SetCITY stored CITY_Specified;
    property COUNTRY:         string  Index (IS_OPTN) read FCOUNTRY write SetCOUNTRY stored COUNTRY_Specified;
    property COUNTRYCODE:     string  Index (IS_OPTN) read FCOUNTRYCODE write SetCOUNTRYCODE stored COUNTRYCODE_Specified;
    property REGION:          string  Index (IS_OPTN) read FREGION write SetREGION stored REGION_Specified;
    property REGIONCODE:      string  Index (IS_OPTN) read FREGIONCODE write SetREGIONCODE stored REGIONCODE_Specified;
    property PAGEHEADING:     string  Index (IS_OPTN) read FPAGEHEADING write SetPAGEHEADING stored PAGEHEADING_Specified;
    property FLEETTIMEZONE:   Int64   Index (IS_OPTN) read FFLEETTIMEZONE write SetFLEETTIMEZONE stored FLEETTIMEZONE_Specified;
    property FLEETTIMEZONEID: string  Index (IS_OPTN) read FFLEETTIMEZONEID write SetFLEETTIMEZONEID stored FLEETTIMEZONEID_Specified;
    property DLSAVING:        Int64   Index (IS_OPTN) read FDLSAVING write SetDLSAVING stored DLSAVING_Specified;
    property DELTAETAMON:     Int64   Index (IS_OPTN) read FDELTAETAMON write SetDELTAETAMON stored DELTAETAMON_Specified;
    property DELTAAREAMON:    Int64   Index (IS_OPTN) read FDELTAAREAMON write SetDELTAAREAMON stored DELTAAREAMON_Specified;
  end;

  FLEETPROPERTIES = array of PropertyType;      { "http://www.fleetboard.com/data"[Cplx] }


  // ************************************************************************ //
  // XML       : GetServerPropertiesResponse, global, <element>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  GetServerPropertiesResponse = class(TRemotable)
  private
    Flimit: limitType;
    Flimit_Specified: boolean;
    Foffset: offsetType;
    Foffset_Specified: boolean;
    FresultSize: resultSizeType;
    FresultSize_Specified: boolean;
    FresponseSize: responseSizeType;
    FresponseSize_Specified: boolean;
    FProperty_: FLEETPROPERTIES;
    FProperty__Specified: boolean;
    procedure Setlimit(Index: Integer; const AlimitType: limitType);
    function  limit_Specified(Index: Integer): boolean;
    procedure Setoffset(Index: Integer; const AoffsetType: offsetType);
    function  offset_Specified(Index: Integer): boolean;
    procedure SetresultSize(Index: Integer; const AresultSizeType: resultSizeType);
    function  resultSize_Specified(Index: Integer): boolean;
    procedure SetresponseSize(Index: Integer; const AresponseSizeType: responseSizeType);
    function  responseSize_Specified(Index: Integer): boolean;
    procedure SetProperty_(Index: Integer; const AFLEETPROPERTIES: FLEETPROPERTIES);
    function  Property__Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property limit:        limitType         Index (IS_ATTR or IS_OPTN) read Flimit write Setlimit stored limit_Specified;
    property offset:       offsetType        Index (IS_ATTR or IS_OPTN) read Foffset write Setoffset stored offset_Specified;
    property resultSize:   resultSizeType    Index (IS_ATTR or IS_OPTN) read FresultSize write SetresultSize stored resultSize_Specified;
    property responseSize: responseSizeType  Index (IS_ATTR or IS_OPTN) read FresponseSize write SetresponseSize stored responseSize_Specified;
    property Property_:    FLEETPROPERTIES   Index (IS_OPTN or IS_UNBD) read FProperty_ write SetProperty_ stored Property__Specified;
  end;

  FLEETRIGHTS = array of RIGHT;                 { "http://www.fleetboard.com/data"[Cplx] }


  // ************************************************************************ //
  // XML       : FLEET, global, <element>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  FLEET = class(TRemotable)
  private
    Finoid: inoidType;
    Finoid_Specified: boolean;
    FFLEETID: fleetidType;
    FFLEETNAME: string;
    FFLEETINFORMATION: FLEETINFORMATION;
    FFLEETRIGHTS: FLEETRIGHTS;
    FFLEETPROPERTIES: FLEETPROPERTIES;
    procedure Setinoid(Index: Integer; const AinoidType: inoidType);
    function  inoid_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property inoid:            inoidType         Index (IS_ATTR or IS_OPTN) read Finoid write Setinoid stored inoid_Specified;
    property FLEETID:          fleetidType       read FFLEETID write FFLEETID;
    property FLEETNAME:        string            read FFLEETNAME write FFLEETNAME;
    property FLEETINFORMATION: FLEETINFORMATION  Index (IS_NLBL) read FFLEETINFORMATION write FFLEETINFORMATION;
    property FLEETRIGHTS:      FLEETRIGHTS       Index (IS_NLBL) read FFLEETRIGHTS write FFLEETRIGHTS;
    property FLEETPROPERTIES:  FLEETPROPERTIES   Index (IS_NLBL) read FFLEETPROPERTIES write FFLEETPROPERTIES;
  end;



  // ************************************************************************ //
  // XML       : LogoutResponse, global, <element>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  LogoutResponse = class(TRemotable)
  private
    FOK: string;
  published
    property OK: string  read FOK write FOK;
  end;



  // ************************************************************************ //
  // XML       : LogoutRequest, global, <element>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  LogoutRequest = class(TRemotable)
  private
  published
  end;



  // ************************************************************************ //
  // XML       : RIGHT, global, <element>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  RIGHT = class(TRemotable)
  private
    FNAME_: string;
    FTYPE_: Int64;
    FSERVICE: Int64;
  published
    property NAME_:   string  read FNAME_ write FNAME_;
    property TYPE_:   Int64   read FTYPE_ write FTYPE_;
    property SERVICE: Int64   read FSERVICE write FSERVICE;
  end;



  // ************************************************************************ //
  // XML       : GetFleetRequest, global, <element>
  // Namespace : http://www.fleetboard.com/data
  // ************************************************************************ //
  GetFleetRequest = class(TRemotable)
  private
    Flimit: limitType;
    Flimit_Specified: boolean;
    Foffset: offsetType;
    Foffset_Specified: boolean;
    Fversion: versionType;
    Fversion_Specified: boolean;
    FFleetId: fleetidType;
    FFleetId_Specified: boolean;
    procedure Setlimit(Index: Integer; const AlimitType: limitType);
    function  limit_Specified(Index: Integer): boolean;
    procedure Setoffset(Index: Integer; const AoffsetType: offsetType);
    function  offset_Specified(Index: Integer): boolean;
    procedure Setversion(Index: Integer; const AversionType: versionType);
    function  version_Specified(Index: Integer): boolean;
    procedure SetFleetId(Index: Integer; const AfleetidType: fleetidType);
    function  FleetId_Specified(Index: Integer): boolean;
  published
    property limit:   limitType    Index (IS_ATTR or IS_OPTN) read Flimit write Setlimit stored limit_Specified;
    property offset:  offsetType   Index (IS_ATTR or IS_OPTN) read Foffset write Setoffset stored offset_Specified;
    property version: versionType  Index (IS_ATTR or IS_OPTN) read Fversion write Setversion stored version_Specified;
    property FleetId: fleetidType  Index (IS_OPTN) read FFleetId write SetFleetId stored FleetId_Specified;
  end;


  // ************************************************************************ //
  // Namespace : http://ws.fleetboard.com/BasicService
  // soapAction: %operationName%
  // transport : http://schemas.xmlsoap.org/soap/http
  // style     : document
  // use       : literal
  // binding   : BasicServiceBinding
  // service   : BasicService
  // port      : BasicService
  // URL       : http://www.fleetboard.com/soap_v1_1/services/BasicService
  // ************************************************************************ //
  BasicService = interface(IInvokable)
  ['{395CE28E-C667-DF3C-33CB-B99F88667E08}']
    function  login(const LoginRequest: LoginRequest): LoginResponse; stdcall;
    function  logout(const LogoutRequest: LogoutRequest): LogoutResponse; stdcall;
    function  getFleet(const GetFleetRequest: GetFleetRequest): GetFleetResponse; stdcall;
    function  getVehicle(const GetVehicleRequest: GetVehicleRequest): GetVehicleResponse; stdcall;
    function  getVehicleGroup(const GetVehicleGroupRequest: GetVehicleGroupRequest): GetVehicleGroupResponse; stdcall;
    function  getServerProperties(const GetServerPropertiesRequest: GetServerPropertiesRequest): GetServerPropertiesResponse; stdcall;
    function  getStatusDef(const GetStatusDefRequest: GetStatusDefRequest): GetStatusDefResponse; stdcall;
    function  getUser(const GetUserRequest: GetUserRequest): GetUserResponse; stdcall;
    function  getDriver(const GetDriverRequest: GetDriverRequest): GetDriverResponse; stdcall;
    function  getCurrentUser(const GetCurrentUserRequest: GetCurrentUserRequest): GetCurrentUserResponse; stdcall;
    function  getDriverGroup(const GetDriverGroupRequest: GetDriverGroupRequest): GetDriverGroupResponse; stdcall;
  end;

function GetBasicService(UseWSDL: Boolean=System.False; Addr: string=''; HTTPRIO: THTTPRIO = nil): BasicService;


implementation
  uses SysUtils;

function GetBasicService(UseWSDL: Boolean; Addr: string; HTTPRIO: THTTPRIO): BasicService;
const
  defWSDL = 'P:\SRC\FUVAR_ADO\4.20\WSDL\BasicService.wsdl';
  defURL  = 'http://www.fleetboard.com/soap_v1_1/services/BasicService';
  defSvc  = 'BasicService';
  defPrt  = 'BasicService';
var
  RIO: THTTPRIO;
begin
  Result := nil;
  if (Addr = '') then
  begin
    if UseWSDL then
      Addr := defWSDL
    else
      Addr := defURL;
  end;
  if HTTPRIO = nil then
    RIO := THTTPRIO.Create(nil)
  else
    RIO := HTTPRIO;
  try
    Result := (RIO as BasicService);
    if UseWSDL then
    begin
      RIO.WSDLLocation := Addr;
      RIO.Service := defSvc;
      RIO.Port := defPrt;
    end else
      RIO.URL := Addr;
  finally
    if (Result = nil) and (HTTPRIO = nil) then
      RIO.Free;
  end;
end;


destructor VhcStatusLayout.Destroy;
begin
  SysUtils.FreeAndNil(FHidden);
  inherited Destroy;
end;

procedure VhcStatusLayout.SetHidden(Index: Integer; const AHidden: Hidden);
begin
  FHidden := AHidden;
  FHidden_Specified := True;
end;

function VhcStatusLayout.Hidden_Specified(Index: Integer): boolean;
begin
  Result := FHidden_Specified;
end;

procedure Hidden.SetDCFAP(Index: Integer; const AInt64: Int64);
begin
  FDCFAP := AInt64;
  FDCFAP_Specified := True;
end;

function Hidden.DCFAP_Specified(Index: Integer): boolean;
begin
  Result := FDCFAP_Specified;
end;

destructor StatusDef.Destroy;
var
  I: Integer;
begin
  for I := 0 to System.Length(FHistory)-1 do
    SysUtils.FreeAndNil(FHistory[I]);
  System.SetLength(FHistory, 0);
  for I := 0 to System.Length(FSemanticsDef)-1 do
    SysUtils.FreeAndNil(FSemanticsDef[I]);
  System.SetLength(FSemanticsDef, 0);
  SysUtils.FreeAndNil(FType_);
  SysUtils.FreeAndNil(FStatusLayout);
  SysUtils.FreeAndNil(FAction);
  SysUtils.FreeAndNil(FVhcStatusLayout);
  inherited Destroy;
end;

procedure StatusDef.Setinoid(Index: Integer; const AinoidType: inoidType);
begin
  Finoid := AinoidType;
  Finoid_Specified := True;
end;

function StatusDef.inoid_Specified(Index: Integer): boolean;
begin
  Result := Finoid_Specified;
end;

procedure StatusDef.Setfbid(Index: Integer; const AfbidType: fbidType);
begin
  Ffbid := AfbidType;
  Ffbid_Specified := True;
end;

function StatusDef.fbid_Specified(Index: Integer): boolean;
begin
  Result := Ffbid_Specified;
end;

procedure StatusDef.SetHistory(Index: Integer; const AHistory: History);
begin
  FHistory := AHistory;
  FHistory_Specified := True;
end;

function StatusDef.History_Specified(Index: Integer): boolean;
begin
  Result := FHistory_Specified;
end;

procedure StatusDef.SetVisible(Index: Integer; const Astring: string);
begin
  FVisible := Astring;
  FVisible_Specified := True;
end;

function StatusDef.Visible_Specified(Index: Integer): boolean;
begin
  Result := FVisible_Specified;
end;

procedure StatusDef.SetShowable(Index: Integer; const AInt64: Int64);
begin
  FShowable := AInt64;
  FShowable_Specified := True;
end;

function StatusDef.Showable_Specified(Index: Integer): boolean;
begin
  Result := FShowable_Specified;
end;

procedure StatusDef.SetStatusGroup(Index: Integer; const Astring: string);
begin
  FStatusGroup := Astring;
  FStatusGroup_Specified := True;
end;

function StatusDef.StatusGroup_Specified(Index: Integer): boolean;
begin
  Result := FStatusGroup_Specified;
end;

procedure StatusDef.SetVhcStatusLayout(Index: Integer; const AVhcStatusLayout: VhcStatusLayout);
begin
  FVhcStatusLayout := AVhcStatusLayout;
  FVhcStatusLayout_Specified := True;
end;

function StatusDef.VhcStatusLayout_Specified(Index: Integer): boolean;
begin
  Result := FVhcStatusLayout_Specified;
end;

procedure StatusDef.SetSemanticsDef(Index: Integer; const ASemanticsDef: SemanticsDef);
begin
  FSemanticsDef := ASemanticsDef;
  FSemanticsDef_Specified := True;
end;

function StatusDef.SemanticsDef_Specified(Index: Integer): boolean;
begin
  Result := FSemanticsDef_Specified;
end;

procedure StatusDef.SetFormDefInoID(Index: Integer; const AinoidType: inoidType);
begin
  FFormDefInoID := AinoidType;
  FFormDefInoID_Specified := True;
end;

function StatusDef.FormDefInoID_Specified(Index: Integer): boolean;
begin
  Result := FFormDefInoID_Specified;
end;

destructor TmSoapUserType.Destroy;
begin
  SysUtils.FreeAndNil(FUserSettings);
  inherited Destroy;
end;

procedure TmSoapUserType.SetUserSettings(Index: Integer; const AUserSettings: UserSettings);
begin
  FUserSettings := AUserSettings;
  FUserSettings_Specified := True;
end;

function TmSoapUserType.UserSettings_Specified(Index: Integer): boolean;
begin
  Result := FUserSettings_Specified;
end;

procedure TmSoapUserType.SetRightSettings(Index: Integer; const ARightSettings: RightSettings);
begin
  FRightSettings := ARightSettings;
  FRightSettings_Specified := True;
end;

function TmSoapUserType.RightSettings_Specified(Index: Integer): boolean;
begin
  Result := FRightSettings_Specified;
end;

procedure UserSettings.SetDescription(Index: Integer; const Astring: string);
begin
  FDescription := Astring;
  FDescription_Specified := True;
end;

function UserSettings.Description_Specified(Index: Integer): boolean;
begin
  Result := FDescription_Specified;
end;

procedure UserSettings.SetStatus(Index: Integer; const AInt64: Int64);
begin
  FStatus := AInt64;
  FStatus_Specified := True;
end;

function UserSettings.Status_Specified(Index: Integer): boolean;
begin
  Result := FStatus_Specified;
end;

procedure UserSettings.SetUserGroup(Index: Integer; const Astring: string);
begin
  FUserGroup := Astring;
  FUserGroup_Specified := True;
end;

function UserSettings.UserGroup_Specified(Index: Integer): boolean;
begin
  Result := FUserGroup_Specified;
end;

procedure UserSettings.SetProfileId(Index: Integer; const AidType: idType);
begin
  FProfileId := AidType;
  FProfileId_Specified := True;
end;

function UserSettings.ProfileId_Specified(Index: Integer): boolean;
begin
  Result := FProfileId_Specified;
end;

procedure UserSettings.SetEndDate(Index: Integer; const AtimestampType: timestampType);
begin
  FEndDate := AtimestampType;
  FEndDate_Specified := True;
end;

function UserSettings.EndDate_Specified(Index: Integer): boolean;
begin
  Result := FEndDate_Specified;
end;

procedure UserSettings.SetMobilePhone(Index: Integer; const Astring: string);
begin
  FMobilePhone := Astring;
  FMobilePhone_Specified := True;
end;

function UserSettings.MobilePhone_Specified(Index: Integer): boolean;
begin
  Result := FMobilePhone_Specified;
end;

procedure UserSettings.SetEmail(Index: Integer; const Astring: string);
begin
  FEmail := Astring;
  FEmail_Specified := True;
end;

function UserSettings.Email_Specified(Index: Integer): boolean;
begin
  Result := FEmail_Specified;
end;

procedure UserSettings.SetCSVSeparator(Index: Integer; const Astring: string);
begin
  FCSVSeparator := Astring;
  FCSVSeparator_Specified := True;
end;

function UserSettings.CSVSeparator_Specified(Index: Integer): boolean;
begin
  Result := FCSVSeparator_Specified;
end;

procedure UserSettings.SetLastAmended(Index: Integer; const Astring: string);
begin
  FLastAmended := Astring;
  FLastAmended_Specified := True;
end;

function UserSettings.LastAmended_Specified(Index: Integer): boolean;
begin
  Result := FLastAmended_Specified;
end;

procedure UserSettings.SetAmendedBy(Index: Integer; const Astring: string);
begin
  FAmendedBy := Astring;
  FAmendedBy_Specified := True;
end;

function UserSettings.AmendedBy_Specified(Index: Integer): boolean;
begin
  Result := FAmendedBy_Specified;
end;

procedure LoginRequest.SetFleetname(Index: Integer; const AFleetname: Fleetname);
begin
  FFleetname := AFleetname;
  FFleetname_Specified := True;
end;

function LoginRequest.Fleetname_Specified(Index: Integer): boolean;
begin
  Result := FFleetname_Specified;
end;

procedure Semantics.SetSemanticId(Index: Integer; const AInt64: Int64);
begin
  FSemanticId := AInt64;
  FSemanticId_Specified := True;
end;

function Semantics.SemanticId_Specified(Index: Integer): boolean;
begin
  Result := FSemanticId_Specified;
end;

procedure Semantics.SetScope(Index: Integer; const ARightSettings: RightSettings);
begin
  FScope := ARightSettings;
  FScope_Specified := True;
end;

function Semantics.Scope_Specified(Index: Integer): boolean;
begin
  Result := FScope_Specified;
end;

procedure Semantics.SetValue(Index: Integer; const Astring: string);
begin
  FValue := Astring;
  FValue_Specified := True;
end;

function Semantics.Value_Specified(Index: Integer): boolean;
begin
  Result := FValue_Specified;
end;

procedure TXMessage.SetReceiveRQ(Index: Integer; const Astring: string);
begin
  FReceiveRQ := Astring;
  FReceiveRQ_Specified := True;
end;

function TXMessage.ReceiveRQ_Specified(Index: Integer): boolean;
begin
  Result := FReceiveRQ_Specified;
end;

procedure TXMessage.SetReadRQ(Index: Integer; const Astring: string);
begin
  FReadRQ := Astring;
  FReadRQ_Specified := True;
end;

function TXMessage.ReadRQ_Specified(Index: Integer): boolean;
begin
  Result := FReadRQ_Specified;
end;

procedure RXMessage.SetPopup(Index: Integer; const Astring: string);
begin
  FPopup := Astring;
  FPopup_Specified := True;
end;

function RXMessage.Popup_Specified(Index: Integer): boolean;
begin
  Result := FPopup_Specified;
end;

procedure RXMessage.SetSound(Index: Integer; const Astring: string);
begin
  FSound := Astring;
  FSound_Specified := True;
end;

function RXMessage.Sound_Specified(Index: Integer): boolean;
begin
  Result := FSound_Specified;
end;

procedure RXMessage.SetEmail(Index: Integer; const AEmail: Email);
begin
  FEmail := AEmail;
  FEmail_Specified := True;
end;

function RXMessage.Email_Specified(Index: Integer): boolean;
begin
  Result := FEmail_Specified;
end;

procedure RXMessage.SetSMS(Index: Integer; const Astring: string);
begin
  FSMS := Astring;
  FSMS_Specified := True;
end;

function RXMessage.SMS_Specified(Index: Integer): boolean;
begin
  Result := FSMS_Specified;
end;

procedure RXMessage.SetOrderdone(Index: Integer; const Astring: string);
begin
  FOrderdone := Astring;
  FOrderdone_Specified := True;
end;

function RXMessage.Orderdone_Specified(Index: Integer): boolean;
begin
  Result := FOrderdone_Specified;
end;

destructor Action.Destroy;
begin
  SysUtils.FreeAndNil(FTXMessage);
  SysUtils.FreeAndNil(FRXMessage);
  inherited Destroy;
end;

procedure Action.SetTXMessage(Index: Integer; const ATXMessage: TXMessage);
begin
  FTXMessage := ATXMessage;
  FTXMessage_Specified := True;
end;

function Action.TXMessage_Specified(Index: Integer): boolean;
begin
  Result := FTXMessage_Specified;
end;

procedure Action.SetRXMessage(Index: Integer; const ARXMessage: RXMessage);
begin
  FRXMessage := ARXMessage;
  FRXMessage_Specified := True;
end;

function Action.RXMessage_Specified(Index: Integer): boolean;
begin
  Result := FRXMessage_Specified;
end;

procedure Action.SetRequest(Index: Integer; const ARequest: Request);
begin
  FRequest := ARequest;
  FRequest_Specified := True;
end;

function Action.Request_Specified(Index: Integer): boolean;
begin
  Result := FRequest_Specified;
end;

procedure GetStatusDefRequestType.Setlimit(Index: Integer; const AlimitType: limitType);
begin
  Flimit := AlimitType;
  Flimit_Specified := True;
end;

function GetStatusDefRequestType.limit_Specified(Index: Integer): boolean;
begin
  Result := Flimit_Specified;
end;

procedure GetStatusDefRequestType.Setoffset(Index: Integer; const AoffsetType: offsetType);
begin
  Foffset := AoffsetType;
  Foffset_Specified := True;
end;

function GetStatusDefRequestType.offset_Specified(Index: Integer): boolean;
begin
  Result := Foffset_Specified;
end;

procedure GetStatusDefRequestType.Setversion(Index: Integer; const AversionType: versionType);
begin
  Fversion := AversionType;
  Fversion_Specified := True;
end;

function GetStatusDefRequestType.version_Specified(Index: Integer): boolean;
begin
  Result := Fversion_Specified;
end;

procedure GetStatusDefRequestType.SetKind(Index: Integer; const AArray_Of_Kind: Array_Of_Kind);
begin
  FKind := AArray_Of_Kind;
  FKind_Specified := True;
end;

function GetStatusDefRequestType.Kind_Specified(Index: Integer): boolean;
begin
  Result := FKind_Specified;
end;

procedure GetStatusDefRequestType.SetInoid(Index: Integer; const AArray_Of_inoidType: Array_Of_inoidType);
begin
  FInoid := AArray_Of_inoidType;
  FInoid_Specified := True;
end;

function GetStatusDefRequestType.Inoid_Specified(Index: Integer): boolean;
begin
  Result := FInoid_Specified;
end;

procedure GetStatusDefRequestType.SetFbid(Index: Integer; const AArray_Of_fbidType: Array_Of_fbidType);
begin
  FFbid := AArray_Of_fbidType;
  FFbid_Specified := True;
end;

function GetStatusDefRequestType.Fbid_Specified(Index: Integer): boolean;
begin
  Result := FFbid_Specified;
end;

procedure GetStatusDefRequestType.SetMultiFleet(Index: Integer; const ABoolean: Boolean);
begin
  FMultiFleet := ABoolean;
  FMultiFleet_Specified := True;
end;

function GetStatusDefRequestType.MultiFleet_Specified(Index: Integer): boolean;
begin
  Result := FMultiFleet_Specified;
end;

procedure GetStatusDefRequestType.SetTmConfigId(Index: Integer; const AtmConfigIdType: tmConfigIdType);
begin
  FTmConfigId := AtmConfigIdType;
  FTmConfigId_Specified := True;
end;

function GetStatusDefRequestType.TmConfigId_Specified(Index: Integer): boolean;
begin
  Result := FTmConfigId_Specified;
end;

destructor GetStatusDefResponseType.Destroy;
var
  I: Integer;
begin
  for I := 0 to System.Length(FStatusDef)-1 do
    SysUtils.FreeAndNil(FStatusDef[I]);
  System.SetLength(FStatusDef, 0);
  inherited Destroy;
end;

procedure GetStatusDefResponseType.Setlimit(Index: Integer; const AlimitType: limitType);
begin
  Flimit := AlimitType;
  Flimit_Specified := True;
end;

function GetStatusDefResponseType.limit_Specified(Index: Integer): boolean;
begin
  Result := Flimit_Specified;
end;

procedure GetStatusDefResponseType.Setoffset(Index: Integer; const AoffsetType: offsetType);
begin
  Foffset := AoffsetType;
  Foffset_Specified := True;
end;

function GetStatusDefResponseType.offset_Specified(Index: Integer): boolean;
begin
  Result := Foffset_Specified;
end;

procedure GetStatusDefResponseType.SetresultSize(Index: Integer; const AresultSizeType: resultSizeType);
begin
  FresultSize := AresultSizeType;
  FresultSize_Specified := True;
end;

function GetStatusDefResponseType.resultSize_Specified(Index: Integer): boolean;
begin
  Result := FresultSize_Specified;
end;

procedure GetStatusDefResponseType.SetresponseSize(Index: Integer; const AresponseSizeType: responseSizeType);
begin
  FresponseSize := AresponseSizeType;
  FresponseSize_Specified := True;
end;

function GetStatusDefResponseType.responseSize_Specified(Index: Integer): boolean;
begin
  Result := FresponseSize_Specified;
end;

procedure GetStatusDefResponseType.SetStatusDef(Index: Integer; const AArray_Of_StatusDef: Array_Of_StatusDef);
begin
  FStatusDef := AArray_Of_StatusDef;
  FStatusDef_Specified := True;
end;

function GetStatusDefResponseType.StatusDef_Specified(Index: Integer): boolean;
begin
  Result := FStatusDef_Specified;
end;

procedure GetDriverRequestType.Setlimit(Index: Integer; const AlimitType: limitType);
begin
  Flimit := AlimitType;
  Flimit_Specified := True;
end;

function GetDriverRequestType.limit_Specified(Index: Integer): boolean;
begin
  Result := Flimit_Specified;
end;

procedure GetDriverRequestType.Setoffset(Index: Integer; const AoffsetType: offsetType);
begin
  Foffset := AoffsetType;
  Foffset_Specified := True;
end;

function GetDriverRequestType.offset_Specified(Index: Integer): boolean;
begin
  Result := Foffset_Specified;
end;

procedure GetDriverRequestType.Setversion(Index: Integer; const AversionType: versionType);
begin
  Fversion := AversionType;
  Fversion_Specified := True;
end;

function GetDriverRequestType.version_Specified(Index: Integer): boolean;
begin
  Result := Fversion_Specified;
end;

procedure GetDriverRequestType.SetDriverNameID(Index: Integer; const AArray_Of_driverNameIDType: Array_Of_driverNameIDType);
begin
  FDriverNameID := AArray_Of_driverNameIDType;
  FDriverNameID_Specified := True;
end;

function GetDriverRequestType.DriverNameID_Specified(Index: Integer): boolean;
begin
  Result := FDriverNameID_Specified;
end;

procedure GetDriverRequestType.SetDriverGroupID(Index: Integer; const AdriverGroupIdType: driverGroupIdType);
begin
  FDriverGroupID := AdriverGroupIdType;
  FDriverGroupID_Specified := True;
end;

function GetDriverRequestType.DriverGroupID_Specified(Index: Integer): boolean;
begin
  Result := FDriverGroupID_Specified;
end;

procedure GetDriverRequestType.SetStatus(Index: Integer; const AdriverStatusType: driverStatusType);
begin
  FStatus := AdriverStatusType;
  FStatus_Specified := True;
end;

function GetDriverRequestType.Status_Specified(Index: Integer): boolean;
begin
  Result := FStatus_Specified;
end;

destructor GetDriverResponseType.Destroy;
var
  I: Integer;
begin
  for I := 0 to System.Length(FDriverInfo)-1 do
    SysUtils.FreeAndNil(FDriverInfo[I]);
  System.SetLength(FDriverInfo, 0);
  inherited Destroy;
end;

procedure GetDriverResponseType.Setlimit(Index: Integer; const AlimitType: limitType);
begin
  Flimit := AlimitType;
  Flimit_Specified := True;
end;

function GetDriverResponseType.limit_Specified(Index: Integer): boolean;
begin
  Result := Flimit_Specified;
end;

procedure GetDriverResponseType.Setoffset(Index: Integer; const AoffsetType: offsetType);
begin
  Foffset := AoffsetType;
  Foffset_Specified := True;
end;

function GetDriverResponseType.offset_Specified(Index: Integer): boolean;
begin
  Result := Foffset_Specified;
end;

procedure GetDriverResponseType.SetresultSize(Index: Integer; const AresultSizeType: resultSizeType);
begin
  FresultSize := AresultSizeType;
  FresultSize_Specified := True;
end;

function GetDriverResponseType.resultSize_Specified(Index: Integer): boolean;
begin
  Result := FresultSize_Specified;
end;

procedure GetDriverResponseType.SetresponseSize(Index: Integer; const AresponseSizeType: responseSizeType);
begin
  FresponseSize := AresponseSizeType;
  FresponseSize_Specified := True;
end;

function GetDriverResponseType.responseSize_Specified(Index: Integer): boolean;
begin
  Result := FresponseSize_Specified;
end;

procedure GetDriverResponseType.SetDriverInfo(Index: Integer; const AArray_Of_DriverInfo: Array_Of_DriverInfo);
begin
  FDriverInfo := AArray_Of_DriverInfo;
  FDriverInfo_Specified := True;
end;

function GetDriverResponseType.DriverInfo_Specified(Index: Integer): boolean;
begin
  Result := FDriverInfo_Specified;
end;

destructor GetUserResponseType.Destroy;
var
  I: Integer;
begin
  for I := 0 to System.Length(FUser)-1 do
    SysUtils.FreeAndNil(FUser[I]);
  System.SetLength(FUser, 0);
  inherited Destroy;
end;

procedure GetUserResponseType.Setlimit(Index: Integer; const AlimitType: limitType);
begin
  Flimit := AlimitType;
  Flimit_Specified := True;
end;

function GetUserResponseType.limit_Specified(Index: Integer): boolean;
begin
  Result := Flimit_Specified;
end;

procedure GetUserResponseType.Setoffset(Index: Integer; const AoffsetType: offsetType);
begin
  Foffset := AoffsetType;
  Foffset_Specified := True;
end;

function GetUserResponseType.offset_Specified(Index: Integer): boolean;
begin
  Result := Foffset_Specified;
end;

procedure GetUserResponseType.SetresultSize(Index: Integer; const AresultSizeType: resultSizeType);
begin
  FresultSize := AresultSizeType;
  FresultSize_Specified := True;
end;

function GetUserResponseType.resultSize_Specified(Index: Integer): boolean;
begin
  Result := FresultSize_Specified;
end;

procedure GetUserResponseType.SetresponseSize(Index: Integer; const AresponseSizeType: responseSizeType);
begin
  FresponseSize := AresponseSizeType;
  FresponseSize_Specified := True;
end;

function GetUserResponseType.responseSize_Specified(Index: Integer): boolean;
begin
  Result := FresponseSize_Specified;
end;

procedure GetUserResponseType.SetUser(Index: Integer; const AArray_Of_TmSoapUserType: Array_Of_TmSoapUserType);
begin
  FUser := AArray_Of_TmSoapUserType;
  FUser_Specified := True;
end;

function GetUserResponseType.User_Specified(Index: Integer): boolean;
begin
  Result := FUser_Specified;
end;

procedure GetDriverGroupRequest.Setlimit(Index: Integer; const AlimitType: limitType);
begin
  Flimit := AlimitType;
  Flimit_Specified := True;
end;

function GetDriverGroupRequest.limit_Specified(Index: Integer): boolean;
begin
  Result := Flimit_Specified;
end;

procedure GetDriverGroupRequest.Setoffset(Index: Integer; const AoffsetType: offsetType);
begin
  Foffset := AoffsetType;
  Foffset_Specified := True;
end;

function GetDriverGroupRequest.offset_Specified(Index: Integer): boolean;
begin
  Result := Foffset_Specified;
end;

procedure GetDriverGroupRequest.SetFleetId(Index: Integer; const AfleetidType: fleetidType);
begin
  FFleetId := AfleetidType;
  FFleetId_Specified := True;
end;

function GetDriverGroupRequest.FleetId_Specified(Index: Integer): boolean;
begin
  Result := FFleetId_Specified;
end;

procedure GetDriverGroupRequest.SetDriverGroupId(Index: Integer; const AdriverGroupIdType: driverGroupIdType);
begin
  FDriverGroupId := AdriverGroupIdType;
  FDriverGroupId_Specified := True;
end;

function GetDriverGroupRequest.DriverGroupId_Specified(Index: Integer): boolean;
begin
  Result := FDriverGroupId_Specified;
end;

procedure GetCurrentUserRequest.Setlimit(Index: Integer; const AlimitType: limitType);
begin
  Flimit := AlimitType;
  Flimit_Specified := True;
end;

function GetCurrentUserRequest.limit_Specified(Index: Integer): boolean;
begin
  Result := Flimit_Specified;
end;

procedure GetCurrentUserRequest.Setoffset(Index: Integer; const AoffsetType: offsetType);
begin
  Foffset := AoffsetType;
  Foffset_Specified := True;
end;

function GetCurrentUserRequest.offset_Specified(Index: Integer): boolean;
begin
  Result := Foffset_Specified;
end;

procedure GetCurrentUserRequest.Setversion(Index: Integer; const AversionType: versionType);
begin
  Fversion := AversionType;
  Fversion_Specified := True;
end;

function GetCurrentUserRequest.version_Specified(Index: Integer): boolean;
begin
  Result := Fversion_Specified;
end;

destructor GetDriverGroupResponse.Destroy;
var
  I: Integer;
begin
  for I := 0 to System.Length(FDriverGroup)-1 do
    SysUtils.FreeAndNil(FDriverGroup[I]);
  System.SetLength(FDriverGroup, 0);
  inherited Destroy;
end;

procedure GetDriverGroupResponse.Setlimit(Index: Integer; const AlimitType: limitType);
begin
  Flimit := AlimitType;
  Flimit_Specified := True;
end;

function GetDriverGroupResponse.limit_Specified(Index: Integer): boolean;
begin
  Result := Flimit_Specified;
end;

procedure GetDriverGroupResponse.Setoffset(Index: Integer; const AoffsetType: offsetType);
begin
  Foffset := AoffsetType;
  Foffset_Specified := True;
end;

function GetDriverGroupResponse.offset_Specified(Index: Integer): boolean;
begin
  Result := Foffset_Specified;
end;

procedure GetDriverGroupResponse.SetresultSize(Index: Integer; const AresultSizeType: resultSizeType);
begin
  FresultSize := AresultSizeType;
  FresultSize_Specified := True;
end;

function GetDriverGroupResponse.resultSize_Specified(Index: Integer): boolean;
begin
  Result := FresultSize_Specified;
end;

procedure GetDriverGroupResponse.SetresponseSize(Index: Integer; const AresponseSizeType: responseSizeType);
begin
  FresponseSize := AresponseSizeType;
  FresponseSize_Specified := True;
end;

function GetDriverGroupResponse.responseSize_Specified(Index: Integer): boolean;
begin
  Result := FresponseSize_Specified;
end;

procedure GetDriverGroupResponse.SetDriverGroup(Index: Integer; const AArray_Of_DriverGroup: Array_Of_DriverGroup);
begin
  FDriverGroup := AArray_Of_DriverGroup;
  FDriverGroup_Specified := True;
end;

function GetDriverGroupResponse.DriverGroup_Specified(Index: Integer): boolean;
begin
  Result := FDriverGroup_Specified;
end;

destructor DriverInfo.Destroy;
var
  I: Integer;
begin
  for I := 0 to System.Length(FDriverToken)-1 do
    SysUtils.FreeAndNil(FDriverToken[I]);
  System.SetLength(FDriverToken, 0);
  inherited Destroy;
end;

procedure DriverInfo.SetDriverToken(Index: Integer; const AArray_Of_DriverToken: Array_Of_DriverToken);
begin
  FDriverToken := AArray_Of_DriverToken;
  FDriverToken_Specified := True;
end;

function DriverInfo.DriverToken_Specified(Index: Integer): boolean;
begin
  Result := FDriverToken_Specified;
end;

procedure DriverInfo.SetStatus(Index: Integer; const AdriverStatusType: driverStatusType);
begin
  FStatus := AdriverStatusType;
  FStatus_Specified := True;
end;

function DriverInfo.Status_Specified(Index: Integer): boolean;
begin
  Result := FStatus_Specified;
end;

procedure DriverInfo.SetDriverGroup(Index: Integer; const ADriverGroup2: DriverGroup2);
begin
  FDriverGroup := ADriverGroup2;
  FDriverGroup_Specified := True;
end;

function DriverInfo.DriverGroup_Specified(Index: Integer): boolean;
begin
  Result := FDriverGroup_Specified;
end;

procedure FLEETVEHICLE.SetREGISTRATION(Index: Integer; const Astring: string);
begin
  FREGISTRATION := Astring;
  FREGISTRATION_Specified := True;
end;

function FLEETVEHICLE.REGISTRATION_Specified(Index: Integer): boolean;
begin
  Result := FREGISTRATION_Specified;
end;

procedure FLEETVEHICLE.SetINSMSISDN(Index: Integer; const Astring: string);
begin
  FINSMSISDN := Astring;
  FINSMSISDN_Specified := True;
end;

function FLEETVEHICLE.INSMSISDN_Specified(Index: Integer): boolean;
begin
  Result := FINSMSISDN_Specified;
end;

procedure FLEETVEHICLE.SetSTARTTIME(Index: Integer; const Astring: string);
begin
  FSTARTTIME := Astring;
  FSTARTTIME_Specified := True;
end;

function FLEETVEHICLE.STARTTIME_Specified(Index: Integer): boolean;
begin
  Result := FSTARTTIME_Specified;
end;

procedure FLEETVEHICLE.SetENDTIME(Index: Integer; const Astring: string);
begin
  FENDTIME := Astring;
  FENDTIME_Specified := True;
end;

function FLEETVEHICLE.ENDTIME_Specified(Index: Integer): boolean;
begin
  Result := FENDTIME_Specified;
end;

procedure FLEETVEHICLE.SetVEHICLETYPE(Index: Integer; const Astring: string);
begin
  FVEHICLETYPE := Astring;
  FVEHICLETYPE_Specified := True;
end;

function FLEETVEHICLE.VEHICLETYPE_Specified(Index: Integer): boolean;
begin
  Result := FVEHICLETYPE_Specified;
end;

procedure FLEETVEHICLE.SetSTATUS(Index: Integer; const AInt64: Int64);
begin
  FSTATUS := AInt64;
  FSTATUS_Specified := True;
end;

function FLEETVEHICLE.STATUS_Specified(Index: Integer): boolean;
begin
  Result := FSTATUS_Specified;
end;

destructor GetVehicleResponse.Destroy;
var
  I: Integer;
begin
  for I := 0 to System.Length(FVEHICLE)-1 do
    SysUtils.FreeAndNil(FVEHICLE[I]);
  System.SetLength(FVEHICLE, 0);
  inherited Destroy;
end;

procedure GetVehicleResponse.Setlimit(Index: Integer; const AlimitType: limitType);
begin
  Flimit := AlimitType;
  Flimit_Specified := True;
end;

function GetVehicleResponse.limit_Specified(Index: Integer): boolean;
begin
  Result := Flimit_Specified;
end;

procedure GetVehicleResponse.Setoffset(Index: Integer; const AoffsetType: offsetType);
begin
  Foffset := AoffsetType;
  Foffset_Specified := True;
end;

function GetVehicleResponse.offset_Specified(Index: Integer): boolean;
begin
  Result := Foffset_Specified;
end;

procedure GetVehicleResponse.SetresultSize(Index: Integer; const AresultSizeType: resultSizeType);
begin
  FresultSize := AresultSizeType;
  FresultSize_Specified := True;
end;

function GetVehicleResponse.resultSize_Specified(Index: Integer): boolean;
begin
  Result := FresultSize_Specified;
end;

procedure GetVehicleResponse.SetresponseSize(Index: Integer; const AresponseSizeType: responseSizeType);
begin
  FresponseSize := AresponseSizeType;
  FresponseSize_Specified := True;
end;

function GetVehicleResponse.responseSize_Specified(Index: Integer): boolean;
begin
  Result := FresponseSize_Specified;
end;

procedure GetVehicleResponse.SetVEHICLE(Index: Integer; const AArray_Of_VEHICLE: Array_Of_VEHICLE);
begin
  FVEHICLE := AArray_Of_VEHICLE;
  FVEHICLE_Specified := True;
end;

function GetVehicleResponse.VEHICLE_Specified(Index: Integer): boolean;
begin
  Result := FVEHICLE_Specified;
end;

destructor GetFleetResponse.Destroy;
begin
  SysUtils.FreeAndNil(FFLEET);
  inherited Destroy;
end;

procedure GetFleetResponse.Setlimit(Index: Integer; const AlimitType: limitType);
begin
  Flimit := AlimitType;
  Flimit_Specified := True;
end;

function GetFleetResponse.limit_Specified(Index: Integer): boolean;
begin
  Result := Flimit_Specified;
end;

procedure GetFleetResponse.Setoffset(Index: Integer; const AoffsetType: offsetType);
begin
  Foffset := AoffsetType;
  Foffset_Specified := True;
end;

function GetFleetResponse.offset_Specified(Index: Integer): boolean;
begin
  Result := Foffset_Specified;
end;

procedure GetFleetResponse.SetresultSize(Index: Integer; const AresultSizeType: resultSizeType);
begin
  FresultSize := AresultSizeType;
  FresultSize_Specified := True;
end;

function GetFleetResponse.resultSize_Specified(Index: Integer): boolean;
begin
  Result := FresultSize_Specified;
end;

procedure GetFleetResponse.SetresponseSize(Index: Integer; const AresponseSizeType: responseSizeType);
begin
  FresponseSize := AresponseSizeType;
  FresponseSize_Specified := True;
end;

function GetFleetResponse.responseSize_Specified(Index: Integer): boolean;
begin
  Result := FresponseSize_Specified;
end;

destructor GetVehicleGroupResponse.Destroy;
var
  I: Integer;
begin
  for I := 0 to System.Length(FVEHICLEGROUP)-1 do
    SysUtils.FreeAndNil(FVEHICLEGROUP[I]);
  System.SetLength(FVEHICLEGROUP, 0);
  inherited Destroy;
end;

procedure GetVehicleGroupResponse.Setlimit(Index: Integer; const AlimitType: limitType);
begin
  Flimit := AlimitType;
  Flimit_Specified := True;
end;

function GetVehicleGroupResponse.limit_Specified(Index: Integer): boolean;
begin
  Result := Flimit_Specified;
end;

procedure GetVehicleGroupResponse.Setoffset(Index: Integer; const AoffsetType: offsetType);
begin
  Foffset := AoffsetType;
  Foffset_Specified := True;
end;

function GetVehicleGroupResponse.offset_Specified(Index: Integer): boolean;
begin
  Result := Foffset_Specified;
end;

procedure GetVehicleGroupResponse.SetresultSize(Index: Integer; const AresultSizeType: resultSizeType);
begin
  FresultSize := AresultSizeType;
  FresultSize_Specified := True;
end;

function GetVehicleGroupResponse.resultSize_Specified(Index: Integer): boolean;
begin
  Result := FresultSize_Specified;
end;

procedure GetVehicleGroupResponse.SetresponseSize(Index: Integer; const AresponseSizeType: responseSizeType);
begin
  FresponseSize := AresponseSizeType;
  FresponseSize_Specified := True;
end;

function GetVehicleGroupResponse.responseSize_Specified(Index: Integer): boolean;
begin
  Result := FresponseSize_Specified;
end;

procedure GetVehicleGroupResponse.SetVEHICLEGROUP(Index: Integer; const AArray_Of_VEHICLEGROUP: Array_Of_VEHICLEGROUP);
begin
  FVEHICLEGROUP := AArray_Of_VEHICLEGROUP;
  FVEHICLEGROUP_Specified := True;
end;

function GetVehicleGroupResponse.VEHICLEGROUP_Specified(Index: Integer): boolean;
begin
  Result := FVEHICLEGROUP_Specified;
end;

procedure VEHICLEGROUP.SetDescription(Index: Integer; const Astring: string);
begin
  FDescription := Astring;
  FDescription_Specified := True;
end;

function VEHICLEGROUP.Description_Specified(Index: Integer): boolean;
begin
  Result := FDescription_Specified;
end;

procedure GetServerPropertiesRequest.Setlimit(Index: Integer; const AlimitType: limitType);
begin
  Flimit := AlimitType;
  Flimit_Specified := True;
end;

function GetServerPropertiesRequest.limit_Specified(Index: Integer): boolean;
begin
  Result := Flimit_Specified;
end;

procedure GetServerPropertiesRequest.Setoffset(Index: Integer; const AoffsetType: offsetType);
begin
  Foffset := AoffsetType;
  Foffset_Specified := True;
end;

function GetServerPropertiesRequest.offset_Specified(Index: Integer): boolean;
begin
  Result := Foffset_Specified;
end;

procedure GetServerPropertiesRequest.SetProperty_(Index: Integer; const AArray_Of_Property: Array_Of_Property);
begin
  FProperty_ := AArray_Of_Property;
  FProperty__Specified := True;
end;

function GetServerPropertiesRequest.Property__Specified(Index: Integer): boolean;
begin
  Result := FProperty__Specified;
end;

procedure User.SetUsertype(Index: Integer; const Astring: string);
begin
  FUsertype := Astring;
  FUsertype_Specified := True;
end;

function User.Usertype_Specified(Index: Integer): boolean;
begin
  Result := FUsertype_Specified;
end;

procedure User.SetMsgID(Index: Integer; const AMsgID: MsgID);
begin
  FMsgID := AMsgID;
  FMsgID_Specified := True;
end;

function User.MsgID_Specified(Index: Integer): boolean;
begin
  Result := FMsgID_Specified;
end;

procedure GetUserRequest.Setlimit(Index: Integer; const AlimitType: limitType);
begin
  Flimit := AlimitType;
  Flimit_Specified := True;
end;

function GetUserRequest.limit_Specified(Index: Integer): boolean;
begin
  Result := Flimit_Specified;
end;

procedure GetUserRequest.Setoffset(Index: Integer; const AoffsetType: offsetType);
begin
  Foffset := AoffsetType;
  Foffset_Specified := True;
end;

function GetUserRequest.offset_Specified(Index: Integer): boolean;
begin
  Result := Foffset_Specified;
end;

procedure GetUserRequest.Setversion(Index: Integer; const AversionType: versionType);
begin
  Fversion := AversionType;
  Fversion_Specified := True;
end;

function GetUserRequest.version_Specified(Index: Integer): boolean;
begin
  Result := Fversion_Specified;
end;

procedure GetUserRequest.SetSettingsType(Index: Integer; const AArray_Of_SettingsType: Array_Of_SettingsType);
begin
  FSettingsType := AArray_Of_SettingsType;
  FSettingsType_Specified := True;
end;

function GetUserRequest.SettingsType_Specified(Index: Integer): boolean;
begin
  Result := FSettingsType_Specified;
end;

procedure GetUserRequest.SetUserID(Index: Integer; const AArray_Of_useridType: Array_Of_useridType);
begin
  FUserID := AArray_Of_useridType;
  FUserID_Specified := True;
end;

function GetUserRequest.UserID_Specified(Index: Integer): boolean;
begin
  Result := FUserID_Specified;
end;

procedure GetUserRequest.SetProfileId(Index: Integer; const AArray_Of_idType: Array_Of_idType);
begin
  FProfileId := AArray_Of_idType;
  FProfileId_Specified := True;
end;

function GetUserRequest.ProfileId_Specified(Index: Integer): boolean;
begin
  Result := FProfileId_Specified;
end;

procedure GetVehicleGroupRequest.Setlimit(Index: Integer; const AlimitType: limitType);
begin
  Flimit := AlimitType;
  Flimit_Specified := True;
end;

function GetVehicleGroupRequest.limit_Specified(Index: Integer): boolean;
begin
  Result := Flimit_Specified;
end;

procedure GetVehicleGroupRequest.Setoffset(Index: Integer; const AoffsetType: offsetType);
begin
  Foffset := AoffsetType;
  Foffset_Specified := True;
end;

function GetVehicleGroupRequest.offset_Specified(Index: Integer): boolean;
begin
  Result := Foffset_Specified;
end;

procedure GetVehicleGroupRequest.Setversion(Index: Integer; const AversionType: versionType);
begin
  Fversion := AversionType;
  Fversion_Specified := True;
end;

function GetVehicleGroupRequest.version_Specified(Index: Integer): boolean;
begin
  Result := Fversion_Specified;
end;

procedure GetVehicleGroupRequest.SetFleetId(Index: Integer; const AfleetidType: fleetidType);
begin
  FFleetId := AfleetidType;
  FFleetId_Specified := True;
end;

function GetVehicleGroupRequest.FleetId_Specified(Index: Integer): boolean;
begin
  Result := FFleetId_Specified;
end;

procedure GetVehicleGroupRequest.SetGroupId(Index: Integer; const AvehicleGroupIdType: vehicleGroupIdType);
begin
  FGroupId := AvehicleGroupIdType;
  FGroupId_Specified := True;
end;

function GetVehicleGroupRequest.GroupId_Specified(Index: Integer): boolean;
begin
  Result := FGroupId_Specified;
end;

destructor GetCurrentUserResponseType.Destroy;
begin
  SysUtils.FreeAndNil(FUser);
  inherited Destroy;
end;

procedure GetCurrentUserResponseType.Setlimit(Index: Integer; const AlimitType: limitType);
begin
  Flimit := AlimitType;
  Flimit_Specified := True;
end;

function GetCurrentUserResponseType.limit_Specified(Index: Integer): boolean;
begin
  Result := Flimit_Specified;
end;

procedure GetCurrentUserResponseType.Setoffset(Index: Integer; const AoffsetType: offsetType);
begin
  Foffset := AoffsetType;
  Foffset_Specified := True;
end;

function GetCurrentUserResponseType.offset_Specified(Index: Integer): boolean;
begin
  Result := Foffset_Specified;
end;

procedure GetCurrentUserResponseType.SetresultSize(Index: Integer; const AresultSizeType: resultSizeType);
begin
  FresultSize := AresultSizeType;
  FresultSize_Specified := True;
end;

function GetCurrentUserResponseType.resultSize_Specified(Index: Integer): boolean;
begin
  Result := FresultSize_Specified;
end;

procedure GetCurrentUserResponseType.SetresponseSize(Index: Integer; const AresponseSizeType: responseSizeType);
begin
  FresponseSize := AresponseSizeType;
  FresponseSize_Specified := True;
end;

function GetCurrentUserResponseType.responseSize_Specified(Index: Integer): boolean;
begin
  Result := FresponseSize_Specified;
end;

procedure GetCurrentUserResponseType.SetUser(Index: Integer; const ATmSoapUserType: TmSoapUserType);
begin
  FUser := ATmSoapUserType;
  FUser_Specified := True;
end;

function GetCurrentUserResponseType.User_Specified(Index: Integer): boolean;
begin
  Result := FUser_Specified;
end;

destructor GetVehicleRequest.Destroy;
var
  I: Integer;
begin
  for I := 0 to System.Length(FCapability)-1 do
    SysUtils.FreeAndNil(FCapability[I]);
  System.SetLength(FCapability, 0);
  inherited Destroy;
end;

procedure GetVehicleRequest.Setlimit(Index: Integer; const AlimitType: limitType);
begin
  Flimit := AlimitType;
  Flimit_Specified := True;
end;

function GetVehicleRequest.limit_Specified(Index: Integer): boolean;
begin
  Result := Flimit_Specified;
end;

procedure GetVehicleRequest.Setoffset(Index: Integer; const AoffsetType: offsetType);
begin
  Foffset := AoffsetType;
  Foffset_Specified := True;
end;

function GetVehicleRequest.offset_Specified(Index: Integer): boolean;
begin
  Result := Foffset_Specified;
end;

procedure GetVehicleRequest.Setversion(Index: Integer; const AversionType: versionType);
begin
  Fversion := AversionType;
  Fversion_Specified := True;
end;

function GetVehicleRequest.version_Specified(Index: Integer): boolean;
begin
  Result := Fversion_Specified;
end;

procedure GetVehicleRequest.SetVehicleGroup(Index: Integer; const AvehicleGroupIdType: vehicleGroupIdType);
begin
  FVehicleGroup := AvehicleGroupIdType;
  FVehicleGroup_Specified := True;
end;

function GetVehicleRequest.VehicleGroup_Specified(Index: Integer): boolean;
begin
  Result := FVehicleGroup_Specified;
end;

procedure GetVehicleRequest.SetServiceType(Index: Integer; const AserviceType: serviceType);
begin
  FServiceType := AserviceType;
  FServiceType_Specified := True;
end;

function GetVehicleRequest.ServiceType_Specified(Index: Integer): boolean;
begin
  Result := FServiceType_Specified;
end;

procedure GetVehicleRequest.SetVehicleID(Index: Integer; const AArray_Of_inoidType: Array_Of_inoidType);
begin
  FVehicleID := AArray_Of_inoidType;
  FVehicleID_Specified := True;
end;

function GetVehicleRequest.VehicleID_Specified(Index: Integer): boolean;
begin
  Result := FVehicleID_Specified;
end;

procedure GetVehicleRequest.SetStatus(Index: Integer; const AArray_Of_integer: Array_Of_integer);
begin
  FStatus := AArray_Of_integer;
  FStatus_Specified := True;
end;

function GetVehicleRequest.Status_Specified(Index: Integer): boolean;
begin
  Result := FStatus_Specified;
end;

procedure GetVehicleRequest.SetCapability(Index: Integer; const AArray_Of_dtcoCapabilityType: Array_Of_dtcoCapabilityType);
begin
  FCapability := AArray_Of_dtcoCapabilityType;
  FCapability_Specified := True;
end;

function GetVehicleRequest.Capability_Specified(Index: Integer): boolean;
begin
  Result := FCapability_Specified;
end;

procedure GetVehicleRequest.SetLeasingStatus(Index: Integer; const AArray_Of_leasingStatusType: Array_Of_leasingStatusType);
begin
  FLeasingStatus := AArray_Of_leasingStatusType;
  FLeasingStatus_Specified := True;
end;

function GetVehicleRequest.LeasingStatus_Specified(Index: Integer): boolean;
begin
  Result := FLeasingStatus_Specified;
end;

procedure GetVehicleRequest.SetTMMode(Index: Integer; const AArray_Of_tmmodeType: Array_Of_tmmodeType);
begin
  FTMMode := AArray_Of_tmmodeType;
  FTMMode_Specified := True;
end;

function GetVehicleRequest.TMMode_Specified(Index: Integer): boolean;
begin
  Result := FTMMode_Specified;
end;

destructor VEHICLE.Destroy;
var
  I: Integer;
begin
  for I := 0 to System.Length(FVEHICLEHARDWARE)-1 do
    SysUtils.FreeAndNil(FVEHICLEHARDWARE[I]);
  System.SetLength(FVEHICLEHARDWARE, 0);
  for I := 0 to System.Length(FVEHICLEPROPERTIES)-1 do
    SysUtils.FreeAndNil(FVEHICLEPROPERTIES[I]);
  System.SetLength(FVEHICLEPROPERTIES, 0);
  SysUtils.FreeAndNil(FFLEETVEHICLE);
  inherited Destroy;
end;

procedure VEHICLE.SetVEHICLEHARDWARE(Index: Integer; const AArray_Of_VEHICLEHARDWARE: Array_Of_VEHICLEHARDWARE);
begin
  FVEHICLEHARDWARE := AArray_Of_VEHICLEHARDWARE;
  FVEHICLEHARDWARE_Specified := True;
end;

function VEHICLE.VEHICLEHARDWARE_Specified(Index: Integer): boolean;
begin
  Result := FVEHICLEHARDWARE_Specified;
end;

procedure VEHICLE.SetGROUPVEHICLE(Index: Integer; const AGROUPVEHICLE: GROUPVEHICLE);
begin
  FGROUPVEHICLE := AGROUPVEHICLE;
  FGROUPVEHICLE_Specified := True;
end;

function VEHICLE.GROUPVEHICLE_Specified(Index: Integer): boolean;
begin
  Result := FGROUPVEHICLE_Specified;
end;

procedure VEHICLE.SetREGISTRATIONDATE(Index: Integer; const Astring: string);
begin
  FREGISTRATIONDATE := Astring;
  FREGISTRATIONDATE_Specified := True;
end;

function VEHICLE.REGISTRATIONDATE_Specified(Index: Integer): boolean;
begin
  Result := FREGISTRATIONDATE_Specified;
end;

procedure VEHICLE.SetTELEMATICGROUP(Index: Integer; const AtelematicGroupIDType: telematicGroupIDType);
begin
  FTELEMATICGROUP := AtelematicGroupIDType;
  FTELEMATICGROUP_Specified := True;
end;

function VEHICLE.TELEMATICGROUP_Specified(Index: Integer): boolean;
begin
  Result := FTELEMATICGROUP_Specified;
end;

procedure VEHICLE.SetVEHICLEPROPERTIES(Index: Integer; const AVEHICLEPROPERTIES: VEHICLEPROPERTIES);
begin
  FVEHICLEPROPERTIES := AVEHICLEPROPERTIES;
  FVEHICLEPROPERTIES_Specified := True;
end;

function VEHICLE.VEHICLEPROPERTIES_Specified(Index: Integer): boolean;
begin
  Result := FVEHICLEPROPERTIES_Specified;
end;

procedure LoginResponse.Setsessionid(Index: Integer; const AsessionidType: sessionidType);
begin
  Fsessionid := AsessionidType;
  Fsessionid_Specified := True;
end;

function LoginResponse.sessionid_Specified(Index: Integer): boolean;
begin
  Result := Fsessionid_Specified;
end;

procedure FLEETINFORMATION.SetSTREET(Index: Integer; const Astring: string);
begin
  FSTREET := Astring;
  FSTREET_Specified := True;
end;

function FLEETINFORMATION.STREET_Specified(Index: Integer): boolean;
begin
  Result := FSTREET_Specified;
end;

procedure FLEETINFORMATION.SetPOSTALCODE(Index: Integer; const Astring: string);
begin
  FPOSTALCODE := Astring;
  FPOSTALCODE_Specified := True;
end;

function FLEETINFORMATION.POSTALCODE_Specified(Index: Integer): boolean;
begin
  Result := FPOSTALCODE_Specified;
end;

procedure FLEETINFORMATION.SetCITY(Index: Integer; const Astring: string);
begin
  FCITY := Astring;
  FCITY_Specified := True;
end;

function FLEETINFORMATION.CITY_Specified(Index: Integer): boolean;
begin
  Result := FCITY_Specified;
end;

procedure FLEETINFORMATION.SetCOUNTRY(Index: Integer; const Astring: string);
begin
  FCOUNTRY := Astring;
  FCOUNTRY_Specified := True;
end;

function FLEETINFORMATION.COUNTRY_Specified(Index: Integer): boolean;
begin
  Result := FCOUNTRY_Specified;
end;

procedure FLEETINFORMATION.SetCOUNTRYCODE(Index: Integer; const Astring: string);
begin
  FCOUNTRYCODE := Astring;
  FCOUNTRYCODE_Specified := True;
end;

function FLEETINFORMATION.COUNTRYCODE_Specified(Index: Integer): boolean;
begin
  Result := FCOUNTRYCODE_Specified;
end;

procedure FLEETINFORMATION.SetREGION(Index: Integer; const Astring: string);
begin
  FREGION := Astring;
  FREGION_Specified := True;
end;

function FLEETINFORMATION.REGION_Specified(Index: Integer): boolean;
begin
  Result := FREGION_Specified;
end;

procedure FLEETINFORMATION.SetREGIONCODE(Index: Integer; const Astring: string);
begin
  FREGIONCODE := Astring;
  FREGIONCODE_Specified := True;
end;

function FLEETINFORMATION.REGIONCODE_Specified(Index: Integer): boolean;
begin
  Result := FREGIONCODE_Specified;
end;

procedure FLEETINFORMATION.SetPAGEHEADING(Index: Integer; const Astring: string);
begin
  FPAGEHEADING := Astring;
  FPAGEHEADING_Specified := True;
end;

function FLEETINFORMATION.PAGEHEADING_Specified(Index: Integer): boolean;
begin
  Result := FPAGEHEADING_Specified;
end;

procedure FLEETINFORMATION.SetFLEETTIMEZONE(Index: Integer; const AInt64: Int64);
begin
  FFLEETTIMEZONE := AInt64;
  FFLEETTIMEZONE_Specified := True;
end;

function FLEETINFORMATION.FLEETTIMEZONE_Specified(Index: Integer): boolean;
begin
  Result := FFLEETTIMEZONE_Specified;
end;

procedure FLEETINFORMATION.SetFLEETTIMEZONEID(Index: Integer; const Astring: string);
begin
  FFLEETTIMEZONEID := Astring;
  FFLEETTIMEZONEID_Specified := True;
end;

function FLEETINFORMATION.FLEETTIMEZONEID_Specified(Index: Integer): boolean;
begin
  Result := FFLEETTIMEZONEID_Specified;
end;

procedure FLEETINFORMATION.SetDLSAVING(Index: Integer; const AInt64: Int64);
begin
  FDLSAVING := AInt64;
  FDLSAVING_Specified := True;
end;

function FLEETINFORMATION.DLSAVING_Specified(Index: Integer): boolean;
begin
  Result := FDLSAVING_Specified;
end;

procedure FLEETINFORMATION.SetDELTAETAMON(Index: Integer; const AInt64: Int64);
begin
  FDELTAETAMON := AInt64;
  FDELTAETAMON_Specified := True;
end;

function FLEETINFORMATION.DELTAETAMON_Specified(Index: Integer): boolean;
begin
  Result := FDELTAETAMON_Specified;
end;

procedure FLEETINFORMATION.SetDELTAAREAMON(Index: Integer; const AInt64: Int64);
begin
  FDELTAAREAMON := AInt64;
  FDELTAAREAMON_Specified := True;
end;

function FLEETINFORMATION.DELTAAREAMON_Specified(Index: Integer): boolean;
begin
  Result := FDELTAAREAMON_Specified;
end;

destructor GetServerPropertiesResponse.Destroy;
var
  I: Integer;
begin
  for I := 0 to System.Length(FProperty_)-1 do
    SysUtils.FreeAndNil(FProperty_[I]);
  System.SetLength(FProperty_, 0);
  inherited Destroy;
end;

procedure GetServerPropertiesResponse.Setlimit(Index: Integer; const AlimitType: limitType);
begin
  Flimit := AlimitType;
  Flimit_Specified := True;
end;

function GetServerPropertiesResponse.limit_Specified(Index: Integer): boolean;
begin
  Result := Flimit_Specified;
end;

procedure GetServerPropertiesResponse.Setoffset(Index: Integer; const AoffsetType: offsetType);
begin
  Foffset := AoffsetType;
  Foffset_Specified := True;
end;

function GetServerPropertiesResponse.offset_Specified(Index: Integer): boolean;
begin
  Result := Foffset_Specified;
end;

procedure GetServerPropertiesResponse.SetresultSize(Index: Integer; const AresultSizeType: resultSizeType);
begin
  FresultSize := AresultSizeType;
  FresultSize_Specified := True;
end;

function GetServerPropertiesResponse.resultSize_Specified(Index: Integer): boolean;
begin
  Result := FresultSize_Specified;
end;

procedure GetServerPropertiesResponse.SetresponseSize(Index: Integer; const AresponseSizeType: responseSizeType);
begin
  FresponseSize := AresponseSizeType;
  FresponseSize_Specified := True;
end;

function GetServerPropertiesResponse.responseSize_Specified(Index: Integer): boolean;
begin
  Result := FresponseSize_Specified;
end;

procedure GetServerPropertiesResponse.SetProperty_(Index: Integer; const AFLEETPROPERTIES: FLEETPROPERTIES);
begin
  FProperty_ := AFLEETPROPERTIES;
  FProperty__Specified := True;
end;

function GetServerPropertiesResponse.Property__Specified(Index: Integer): boolean;
begin
  Result := FProperty__Specified;
end;

destructor FLEET.Destroy;
var
  I: Integer;
begin
  for I := 0 to System.Length(FFLEETRIGHTS)-1 do
    SysUtils.FreeAndNil(FFLEETRIGHTS[I]);
  System.SetLength(FFLEETRIGHTS, 0);
  for I := 0 to System.Length(FFLEETPROPERTIES)-1 do
    SysUtils.FreeAndNil(FFLEETPROPERTIES[I]);
  System.SetLength(FFLEETPROPERTIES, 0);
  SysUtils.FreeAndNil(FFLEETINFORMATION);
  inherited Destroy;
end;

procedure FLEET.Setinoid(Index: Integer; const AinoidType: inoidType);
begin
  Finoid := AinoidType;
  Finoid_Specified := True;
end;

function FLEET.inoid_Specified(Index: Integer): boolean;
begin
  Result := Finoid_Specified;
end;

procedure GetFleetRequest.Setlimit(Index: Integer; const AlimitType: limitType);
begin
  Flimit := AlimitType;
  Flimit_Specified := True;
end;

function GetFleetRequest.limit_Specified(Index: Integer): boolean;
begin
  Result := Flimit_Specified;
end;

procedure GetFleetRequest.Setoffset(Index: Integer; const AoffsetType: offsetType);
begin
  Foffset := AoffsetType;
  Foffset_Specified := True;
end;

function GetFleetRequest.offset_Specified(Index: Integer): boolean;
begin
  Result := Foffset_Specified;
end;

procedure GetFleetRequest.Setversion(Index: Integer; const AversionType: versionType);
begin
  Fversion := AversionType;
  Fversion_Specified := True;
end;

function GetFleetRequest.version_Specified(Index: Integer): boolean;
begin
  Result := Fversion_Specified;
end;

procedure GetFleetRequest.SetFleetId(Index: Integer; const AfleetidType: fleetidType);
begin
  FFleetId := AfleetidType;
  FFleetId_Specified := True;
end;

function GetFleetRequest.FleetId_Specified(Index: Integer): boolean;
begin
  Result := FFleetId_Specified;
end;

initialization
  { BasicService }
  InvRegistry.RegisterInterface(TypeInfo(BasicService), 'http://ws.fleetboard.com/BasicService', 'UTF-8');
  InvRegistry.RegisterDefaultSOAPAction(TypeInfo(BasicService), '%operationName%');
  InvRegistry.RegisterInvokeOptions(TypeInfo(BasicService), ioDocument);
  { BasicService.login }
  InvRegistry.RegisterMethodInfo(TypeInfo(BasicService), 'login', '',
                                 '[ReturnName="LoginResponse", RequestNS="http://www.fleetboard.com/data", ResponseNS="http://www.fleetboard.com/data"]', IS_REF);
  InvRegistry.RegisterParamInfo(TypeInfo(BasicService), 'login', 'LoginRequest', '',
                                '', IS_REF);
  InvRegistry.RegisterParamInfo(TypeInfo(BasicService), 'login', 'LoginResponse', '',
                                '', IS_REF);
  { BasicService.logout }
  InvRegistry.RegisterMethodInfo(TypeInfo(BasicService), 'logout', '',
                                 '[ReturnName="LogoutResponse", RequestNS="http://www.fleetboard.com/data", ResponseNS="http://www.fleetboard.com/data"]', IS_REF);
  InvRegistry.RegisterParamInfo(TypeInfo(BasicService), 'logout', 'LogoutRequest', '',
                                '', IS_REF);
  InvRegistry.RegisterParamInfo(TypeInfo(BasicService), 'logout', 'LogoutResponse', '',
                                '', IS_REF);
  { BasicService.getFleet }
  InvRegistry.RegisterMethodInfo(TypeInfo(BasicService), 'getFleet', '',
                                 '[ReturnName="GetFleetResponse", RequestNS="http://www.fleetboard.com/data", ResponseNS="http://www.fleetboard.com/data"]', IS_REF);
  InvRegistry.RegisterParamInfo(TypeInfo(BasicService), 'getFleet', 'GetFleetRequest', '',
                                '', IS_REF);
  InvRegistry.RegisterParamInfo(TypeInfo(BasicService), 'getFleet', 'GetFleetResponse', '',
                                '', IS_REF);
  { BasicService.getVehicle }
  InvRegistry.RegisterMethodInfo(TypeInfo(BasicService), 'getVehicle', '',
                                 '[ReturnName="GetVehicleResponse", RequestNS="http://www.fleetboard.com/data", ResponseNS="http://www.fleetboard.com/data"]', IS_REF);
  InvRegistry.RegisterParamInfo(TypeInfo(BasicService), 'getVehicle', 'GetVehicleRequest', '',
                                '', IS_REF);
  InvRegistry.RegisterParamInfo(TypeInfo(BasicService), 'getVehicle', 'GetVehicleResponse', '',
                                '', IS_REF);
  { BasicService.getVehicleGroup }
  InvRegistry.RegisterMethodInfo(TypeInfo(BasicService), 'getVehicleGroup', '',
                                 '[ReturnName="GetVehicleGroupResponse", RequestNS="http://www.fleetboard.com/data", ResponseNS="http://www.fleetboard.com/data"]', IS_REF);
  InvRegistry.RegisterParamInfo(TypeInfo(BasicService), 'getVehicleGroup', 'GetVehicleGroupRequest', '',
                                '', IS_REF);
  InvRegistry.RegisterParamInfo(TypeInfo(BasicService), 'getVehicleGroup', 'GetVehicleGroupResponse', '',
                                '', IS_REF);
  { BasicService.getServerProperties }
  InvRegistry.RegisterMethodInfo(TypeInfo(BasicService), 'getServerProperties', '',
                                 '[ReturnName="GetServerPropertiesResponse", RequestNS="http://www.fleetboard.com/data", ResponseNS="http://www.fleetboard.com/data"]', IS_REF);
  InvRegistry.RegisterParamInfo(TypeInfo(BasicService), 'getServerProperties', 'GetServerPropertiesRequest', '',
                                '', IS_REF);
  InvRegistry.RegisterParamInfo(TypeInfo(BasicService), 'getServerProperties', 'GetServerPropertiesResponse', '',
                                '', IS_REF);
  { BasicService.getStatusDef }
  InvRegistry.RegisterMethodInfo(TypeInfo(BasicService), 'getStatusDef', '',
                                 '[ReturnName="GetStatusDefResponse", RequestNS="http://www.fleetboard.com/data", ResponseNS="http://www.fleetboard.com/data"]', IS_REF);
  InvRegistry.RegisterParamInfo(TypeInfo(BasicService), 'getStatusDef', 'GetStatusDefRequest', '',
                                '', IS_REF);
  InvRegistry.RegisterParamInfo(TypeInfo(BasicService), 'getStatusDef', 'GetStatusDefResponse', '',
                                '', IS_REF);
  { BasicService.getUser }
  InvRegistry.RegisterMethodInfo(TypeInfo(BasicService), 'getUser', '',
                                 '[ReturnName="GetUserResponse", RequestNS="http://www.fleetboard.com/data", ResponseNS="http://www.fleetboard.com/data"]', IS_REF);
  InvRegistry.RegisterParamInfo(TypeInfo(BasicService), 'getUser', 'GetUserRequest', '',
                                '', IS_REF);
  InvRegistry.RegisterParamInfo(TypeInfo(BasicService), 'getUser', 'GetUserResponse', '',
                                '', IS_REF);
  { BasicService.getDriver }
  InvRegistry.RegisterMethodInfo(TypeInfo(BasicService), 'getDriver', '',
                                 '[ReturnName="GetDriverResponse", RequestNS="http://www.fleetboard.com/data", ResponseNS="http://www.fleetboard.com/data"]', IS_REF);
  InvRegistry.RegisterParamInfo(TypeInfo(BasicService), 'getDriver', 'GetDriverRequest', '',
                                '', IS_REF);
  InvRegistry.RegisterParamInfo(TypeInfo(BasicService), 'getDriver', 'GetDriverResponse', '',
                                '', IS_REF);
  { BasicService.getCurrentUser }
  InvRegistry.RegisterMethodInfo(TypeInfo(BasicService), 'getCurrentUser', '',
                                 '[ReturnName="GetCurrentUserResponse", RequestNS="http://www.fleetboard.com/data", ResponseNS="http://www.fleetboard.com/data"]', IS_REF);
  InvRegistry.RegisterParamInfo(TypeInfo(BasicService), 'getCurrentUser', 'GetCurrentUserRequest', '',
                                '', IS_REF);
  InvRegistry.RegisterParamInfo(TypeInfo(BasicService), 'getCurrentUser', 'GetCurrentUserResponse', '',
                                '', IS_REF);
  { BasicService.getDriverGroup }
  InvRegistry.RegisterMethodInfo(TypeInfo(BasicService), 'getDriverGroup', '',
                                 '[ReturnName="GetDriverGroupResponse", RequestNS="http://www.fleetboard.com/data", ResponseNS="http://www.fleetboard.com/data"]', IS_REF);
  InvRegistry.RegisterParamInfo(TypeInfo(BasicService), 'getDriverGroup', 'GetDriverGroupRequest', '',
                                '', IS_REF);
  InvRegistry.RegisterParamInfo(TypeInfo(BasicService), 'getDriverGroup', 'GetDriverGroupResponse', '',
                                '', IS_REF);
  RemClassRegistry.RegisterXSInfo(TypeInfo(SettingsType), 'http://www.fleetboard.com/data', 'SettingsType');
  RemClassRegistry.RegisterXSInfo(TypeInfo(History), 'http://www.fleetboard.com/data', 'History');
  RemClassRegistry.RegisterXSClass(VhcStatusLayout, 'http://www.fleetboard.com/data', 'VhcStatusLayout');
  RemClassRegistry.RegisterXSInfo(TypeInfo(SemanticsDef), 'http://www.fleetboard.com/data', 'SemanticsDef');
  RemClassRegistry.RegisterXSInfo(TypeInfo(Array_Of_DriverToken), 'http://www.fleetboard.com/data', 'Array_Of_DriverToken');
  RemClassRegistry.RegisterXSInfo(TypeInfo(Array_Of_DriverInfo), 'http://www.fleetboard.com/data', 'Array_Of_DriverInfo');
  RemClassRegistry.RegisterXSInfo(TypeInfo(Array_Of_dtcoCapabilityType), 'http://www.fleetboard.com/data', 'Array_Of_dtcoCapabilityType');
  RemClassRegistry.RegisterXSInfo(TypeInfo(Array_Of_TmSoapUserType), 'http://www.fleetboard.com/data', 'Array_Of_TmSoapUserType');
  RemClassRegistry.RegisterXSInfo(TypeInfo(Array_Of_StatusDef), 'http://www.fleetboard.com/data', 'Array_Of_StatusDef');
  RemClassRegistry.RegisterXSInfo(TypeInfo(Array_Of_DriverGroup), 'http://www.fleetboard.com/data', 'Array_Of_DriverGroup');
  RemClassRegistry.RegisterXSInfo(TypeInfo(Array_Of_VEHICLE), 'http://www.fleetboard.com/data', 'Array_Of_VEHICLE');
  RemClassRegistry.RegisterXSInfo(TypeInfo(Array_Of_VEHICLEHARDWARE), 'http://www.fleetboard.com/data', 'Array_Of_VEHICLEHARDWARE');
  RemClassRegistry.RegisterXSInfo(TypeInfo(Array_Of_VEHICLEGROUP), 'http://www.fleetboard.com/data', 'Array_Of_VEHICLEGROUP');
  RemClassRegistry.RegisterXSInfo(TypeInfo(Array_Of_SettingsType), 'http://www.fleetboard.com/data', 'Array_Of_SettingsType');
  RemClassRegistry.RegisterXSInfo(TypeInfo(fleetidType), 'http://www.fleetboard.com/data', 'fleetidType');
  RemClassRegistry.RegisterXSInfo(TypeInfo(DriverTokenKind), 'http://www.fleetboard.com/data', 'DriverTokenKind');
  RemClassRegistry.RegisterXSInfo(TypeInfo(driverNameIDType), 'http://www.fleetboard.com/data', 'driverNameIDType');
  RemClassRegistry.RegisterXSInfo(TypeInfo(Array_Of_driverNameIDType), 'http://www.fleetboard.com/data', 'Array_Of_driverNameIDType');
  RemClassRegistry.RegisterXSInfo(TypeInfo(driverStatusType), 'http://www.fleetboard.com/data', 'driverStatusType');
  RemClassRegistry.RegisterXSInfo(TypeInfo(driverGroupIdType), 'http://www.fleetboard.com/data', 'driverGroupIdType');
  RemClassRegistry.RegisterXSInfo(TypeInfo(resultSizeType), 'http://www.fleetboard.com/data', 'resultSizeType');
  RemClassRegistry.RegisterXSInfo(TypeInfo(responseSizeType), 'http://www.fleetboard.com/data', 'responseSizeType');
  RemClassRegistry.RegisterXSInfo(TypeInfo(Array_Of_integer), 'http://www.w3.org/2001/XMLSchema', 'Array_Of_integer');
  RemClassRegistry.RegisterXSClass(Hidden, 'http://www.fleetboard.com/data', 'Hidden');
  RemClassRegistry.RegisterXSInfo(TypeInfo(idType), 'http://www.fleetboard.com/data', 'idType');
  RemClassRegistry.RegisterXSInfo(TypeInfo(Array_Of_idType), 'http://www.fleetboard.com/data', 'Array_Of_idType');
  RemClassRegistry.RegisterXSInfo(TypeInfo(inoidType), 'http://www.fleetboard.com/data', 'inoidType');
  RemClassRegistry.RegisterXSInfo(TypeInfo(Array_Of_inoidType), 'http://www.fleetboard.com/data', 'Array_Of_inoidType');
  RemClassRegistry.RegisterXSInfo(TypeInfo(MsgID), 'http://www.fleetboard.com/data', 'MsgID');
  RemClassRegistry.RegisterXSInfo(TypeInfo(timestampType), 'http://www.fleetboard.com/data', 'timestampType');
  RemClassRegistry.RegisterXSClass(PropertyType, 'http://www.fleetboard.com/data', 'PropertyType');
  RemClassRegistry.RegisterExternalPropName(TypeInfo(PropertyType), 'Name_', '[ExtName="Name"]');
  RemClassRegistry.RegisterXSInfo(TypeInfo(fbidType), 'http://www.fleetboard.com/data', 'fbidType');
  RemClassRegistry.RegisterXSInfo(TypeInfo(Array_Of_fbidType), 'http://www.fleetboard.com/data', 'Array_Of_fbidType');
  RemClassRegistry.RegisterXSClass(DriverToken, 'http://www.fleetboard.com/data', 'DriverToken');
  RemClassRegistry.RegisterXSClass(StatusDef, 'http://www.fleetboard.com/data', 'StatusDef');
  RemClassRegistry.RegisterExternalPropName(TypeInfo(StatusDef), 'Type_', '[ExtName="Type"]');
  RemClassRegistry.RegisterExternalPropName(TypeInfo(StatusDef), 'History', '[ArrayItemName="User"]');
  RemClassRegistry.RegisterExternalPropName(TypeInfo(StatusDef), 'SemanticsDef', '[ArrayItemName="Semantics"]');
  RemClassRegistry.RegisterXSInfo(TypeInfo(RightSettings), 'http://www.fleetboard.com/data', 'RightSettings');
  RemClassRegistry.RegisterXSClass(TmSoapUserType, 'http://www.fleetboard.com/data', 'TmSoapUserType');
  RemClassRegistry.RegisterExternalPropName(TypeInfo(TmSoapUserType), 'RightSettings', '[ArrayItemName="Right"]');
  RemClassRegistry.RegisterXSClass(UserSettings, 'http://www.fleetboard.com/data', 'UserSettings');
  RemClassRegistry.RegisterExternalPropName(TypeInfo(UserSettings), 'Name_', '[ExtName="Name"]');
  RemClassRegistry.RegisterXSInfo(TypeInfo(User2), 'http://www.fleetboard.com/data', 'User2', 'User');
  RemClassRegistry.RegisterXSInfo(TypeInfo(Fleetname), 'http://www.fleetboard.com/data', 'Fleetname');
  RemClassRegistry.RegisterXSInfo(TypeInfo(Password), 'http://www.fleetboard.com/data', 'Password');
  RemClassRegistry.RegisterXSClass(LoginRequest, 'http://www.fleetboard.com/data', 'LoginRequest');
  RemClassRegistry.RegisterXSClass(Semantics, 'http://www.fleetboard.com/data', 'Semantics');
  RemClassRegistry.RegisterExternalPropName(TypeInfo(Semantics), 'Scope', '[ArrayItemName="Right"]');
  RemClassRegistry.RegisterXSClass(Type_, 'http://www.fleetboard.com/data', 'Type_', 'Type');
  RemClassRegistry.RegisterXSClass(TXMessage, 'http://www.fleetboard.com/data', 'TXMessage');
  RemClassRegistry.RegisterXSInfo(TypeInfo(Email), 'http://www.fleetboard.com/data', 'Email');
  RemClassRegistry.RegisterXSClass(RXMessage, 'http://www.fleetboard.com/data', 'RXMessage');
  RemClassRegistry.RegisterXSClass(StatusLayout, 'http://www.fleetboard.com/data', 'StatusLayout');
  RemClassRegistry.RegisterXSInfo(TypeInfo(Request), 'http://www.fleetboard.com/data', 'Request');
  RemClassRegistry.RegisterXSClass(Action, 'http://www.fleetboard.com/data', 'Action');
  RemClassRegistry.RegisterExternalPropName(TypeInfo(Action), 'Request', '[ArrayItemName="Service"]');
  RemClassRegistry.RegisterXSClass(DriverGroup, 'http://www.fleetboard.com/data', 'DriverGroup');
  RemClassRegistry.RegisterExternalPropName(TypeInfo(DriverGroup), 'Name_', '[ExtName="Name"]');
  RemClassRegistry.RegisterXSClass(dtcoCapabilityType, 'http://www.fleetboard.com/data', 'dtcoCapabilityType');
  RemClassRegistry.RegisterExternalPropName(TypeInfo(dtcoCapabilityType), 'Type_', '[ExtName="Type"]');
  RemClassRegistry.RegisterXSInfo(TypeInfo(Kind), 'http://www.fleetboard.com/data', 'Kind');
  RemClassRegistry.RegisterXSInfo(TypeInfo(Array_Of_Kind), 'http://www.fleetboard.com/data', 'Array_Of_Kind');
  RemClassRegistry.RegisterXSInfo(TypeInfo(tmConfigIdType), 'http://www.fleetboard.com/data', 'tmConfigIdType');
  RemClassRegistry.RegisterXSInfo(TypeInfo(limitType), 'http://www.fleetboard.com/data', 'limitType');
  RemClassRegistry.RegisterXSInfo(TypeInfo(versionType), 'http://www.fleetboard.com/data', 'versionType');
  RemClassRegistry.RegisterXSInfo(TypeInfo(offsetType), 'http://www.fleetboard.com/data', 'offsetType');
  RemClassRegistry.RegisterXSClass(GetStatusDefRequestType, 'http://www.fleetboard.com/data', 'GetStatusDefRequestType');
  RemClassRegistry.RegisterXSClass(GetStatusDefResponseType, 'http://www.fleetboard.com/data', 'GetStatusDefResponseType');
  RemClassRegistry.RegisterXSClass(GetStatusDefResponse, 'http://www.fleetboard.com/data', 'GetStatusDefResponse');
  RemClassRegistry.RegisterXSClass(GetDriverRequestType, 'http://www.fleetboard.com/data', 'GetDriverRequestType');
  RemClassRegistry.RegisterXSClass(GetDriverRequest, 'http://www.fleetboard.com/data', 'GetDriverRequest');
  RemClassRegistry.RegisterXSClass(GetDriverResponseType, 'http://www.fleetboard.com/data', 'GetDriverResponseType');
  RemClassRegistry.RegisterXSClass(GetDriverResponse, 'http://www.fleetboard.com/data', 'GetDriverResponse');
  RemClassRegistry.RegisterXSClass(GetUserResponseType, 'http://www.fleetboard.com/data', 'GetUserResponseType');
  RemClassRegistry.RegisterXSClass(GetUserResponse, 'http://www.fleetboard.com/data', 'GetUserResponse');
  RemClassRegistry.RegisterXSClass(GetDriverGroupRequest, 'http://www.fleetboard.com/data', 'GetDriverGroupRequest');
  RemClassRegistry.RegisterXSClass(GetCurrentUserRequest, 'http://www.fleetboard.com/data', 'GetCurrentUserRequest');
  RemClassRegistry.RegisterXSClass(GetDriverGroupResponse, 'http://www.fleetboard.com/data', 'GetDriverGroupResponse');
  RemClassRegistry.RegisterXSInfo(TypeInfo(DriverGroup2), 'http://www.fleetboard.com/data', 'DriverGroup2', 'DriverGroup');
  RemClassRegistry.RegisterXSClass(DriverInfo, 'http://www.fleetboard.com/data', 'DriverInfo');
  RemClassRegistry.RegisterExternalPropName(TypeInfo(DriverInfo), 'DriverGroup', '[ArrayItemName="DriverGroupId"]');
  RemClassRegistry.RegisterXSInfo(TypeInfo(GROUPVEHICLE), 'http://www.fleetboard.com/data', 'GROUPVEHICLE');
  RemClassRegistry.RegisterXSClass(FLEETVEHICLE, 'http://www.fleetboard.com/data', 'FLEETVEHICLE');
  RemClassRegistry.RegisterXSClass(GetVehicleResponse, 'http://www.fleetboard.com/data', 'GetVehicleResponse');
  RemClassRegistry.RegisterXSInfo(TypeInfo(VEHICLEPROPERTIES), 'http://www.fleetboard.com/data', 'VEHICLEPROPERTIES');
  RemClassRegistry.RegisterXSClass(GetFleetResponse, 'http://www.fleetboard.com/data', 'GetFleetResponse');
  RemClassRegistry.RegisterXSClass(VEHICLEHARDWARE, 'http://www.fleetboard.com/data', 'VEHICLEHARDWARE');
  RemClassRegistry.RegisterXSClass(GetStatusDefRequest, 'http://www.fleetboard.com/data', 'GetStatusDefRequest');
  RemClassRegistry.RegisterXSClass(GetVehicleGroupResponse, 'http://www.fleetboard.com/data', 'GetVehicleGroupResponse');
  RemClassRegistry.RegisterXSClass(VEHICLEGROUP, 'http://www.fleetboard.com/data', 'VEHICLEGROUP');
  RemClassRegistry.RegisterXSInfo(TypeInfo(Property_), 'http://www.fleetboard.com/data', 'Property_', 'Property');
  RemClassRegistry.RegisterXSInfo(TypeInfo(Array_Of_Property), 'http://www.fleetboard.com/data', 'Array_Of_Property');
  RemClassRegistry.RegisterXSClass(GetServerPropertiesRequest, 'http://www.fleetboard.com/data', 'GetServerPropertiesRequest');
  RemClassRegistry.RegisterExternalPropName(TypeInfo(GetServerPropertiesRequest), 'Property_', '[ExtName="Property"]');
  RemClassRegistry.RegisterXSInfo(TypeInfo(sessionidType), 'http://www.fleetboard.com/data', 'sessionidType');
  RemClassRegistry.RegisterXSInfo(TypeInfo(useridType), 'http://www.fleetboard.com/data', 'useridType');
  RemClassRegistry.RegisterXSInfo(TypeInfo(USERID), 'http://www.fleetboard.com/data', 'USERID');
  RemClassRegistry.RegisterXSClass(User, 'http://www.fleetboard.com/data', 'User');
  RemClassRegistry.RegisterXSInfo(TypeInfo(Array_Of_useridType), 'http://www.fleetboard.com/data', 'Array_Of_useridType');
  RemClassRegistry.RegisterXSClass(GetUserRequest, 'http://www.fleetboard.com/data', 'GetUserRequest');
  RemClassRegistry.RegisterXSInfo(TypeInfo(tmmodeType), 'http://www.fleetboard.com/data', 'tmmodeType');
  RemClassRegistry.RegisterXSInfo(TypeInfo(Array_Of_tmmodeType), 'http://www.fleetboard.com/data', 'Array_Of_tmmodeType');
  RemClassRegistry.RegisterXSInfo(TypeInfo(leasingStatusType), 'http://www.fleetboard.com/data', 'leasingStatusType');
  RemClassRegistry.RegisterXSInfo(TypeInfo(Array_Of_leasingStatusType), 'http://www.fleetboard.com/data', 'Array_Of_leasingStatusType');
  RemClassRegistry.RegisterXSInfo(TypeInfo(vehicleGroupIdType), 'http://www.fleetboard.com/data', 'vehicleGroupIdType');
  RemClassRegistry.RegisterXSClass(GetVehicleGroupRequest, 'http://www.fleetboard.com/data', 'GetVehicleGroupRequest');
  RemClassRegistry.RegisterXSClass(GetCurrentUserResponseType, 'http://www.fleetboard.com/data', 'GetCurrentUserResponseType');
  RemClassRegistry.RegisterXSClass(GetCurrentUserResponse, 'http://www.fleetboard.com/data', 'GetCurrentUserResponse');
  RemClassRegistry.RegisterXSInfo(TypeInfo(serviceType), 'http://www.fleetboard.com/data', 'serviceType');
  RemClassRegistry.RegisterXSClass(GetVehicleRequest, 'http://www.fleetboard.com/data', 'GetVehicleRequest');
  RemClassRegistry.RegisterXSInfo(TypeInfo(telematicGroupIDType), 'http://www.fleetboard.com/data', 'telematicGroupIDType');
  RemClassRegistry.RegisterXSClass(VEHICLE, 'http://www.fleetboard.com/data', 'VEHICLE');
  RemClassRegistry.RegisterExternalPropName(TypeInfo(VEHICLE), 'GROUPVEHICLE', '[ArrayItemName="GROUPID"]');
  RemClassRegistry.RegisterExternalPropName(TypeInfo(VEHICLE), 'VEHICLEPROPERTIES', '[ArrayItemName="Property"]');
  RemClassRegistry.RegisterXSClass(LoginResponse, 'http://www.fleetboard.com/data', 'LoginResponse');
  RemClassRegistry.RegisterXSClass(FLEETINFORMATION, 'http://www.fleetboard.com/data', 'FLEETINFORMATION');
  RemClassRegistry.RegisterXSInfo(TypeInfo(FLEETPROPERTIES), 'http://www.fleetboard.com/data', 'FLEETPROPERTIES');
  RemClassRegistry.RegisterXSClass(GetServerPropertiesResponse, 'http://www.fleetboard.com/data', 'GetServerPropertiesResponse');
  RemClassRegistry.RegisterExternalPropName(TypeInfo(GetServerPropertiesResponse), 'Property_', '[ArrayItemName="Property", ExtName="Property"]');
  RemClassRegistry.RegisterXSInfo(TypeInfo(FLEETRIGHTS), 'http://www.fleetboard.com/data', 'FLEETRIGHTS');
  RemClassRegistry.RegisterXSClass(FLEET, 'http://www.fleetboard.com/data', 'FLEET');
  RemClassRegistry.RegisterExternalPropName(TypeInfo(FLEET), 'FLEETRIGHTS', '[ArrayItemName="RIGHT"]');
  RemClassRegistry.RegisterExternalPropName(TypeInfo(FLEET), 'FLEETPROPERTIES', '[ArrayItemName="Property"]');
  RemClassRegistry.RegisterXSClass(LogoutResponse, 'http://www.fleetboard.com/data', 'LogoutResponse');
  RemClassRegistry.RegisterXSClass(LogoutRequest, 'http://www.fleetboard.com/data', 'LogoutRequest');
  RemClassRegistry.RegisterXSClass(RIGHT, 'http://www.fleetboard.com/data', 'RIGHT');
  RemClassRegistry.RegisterExternalPropName(TypeInfo(RIGHT), 'NAME_', '[ExtName="NAME"]');
  RemClassRegistry.RegisterExternalPropName(TypeInfo(RIGHT), 'TYPE_', '[ExtName="TYPE"]');
  RemClassRegistry.RegisterXSClass(GetFleetRequest, 'http://www.fleetboard.com/data', 'GetFleetRequest');

end.