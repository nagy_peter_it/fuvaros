unit Tavolletidij;

interface

uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, DBTables, DB, StdCtrls, Buttons, Mask, DBCtrls, ExtCtrls,DateUtils;

type
  TTavolletidijDlg = class(TForm)
    Label1: TLabel;
    BitBtn4: TBitBtn;
    BitBtn6: TBitBtn;
    MaskEdit1: TMaskEdit;
    BitBtn10: TBitBtn;
    procedure FormCreate(Sender: TObject);
    procedure BitBtn4Click(Sender: TObject);
    procedure BitBtn6Click(Sender: TObject);
    procedure MaskEdit1Click(Sender: TObject);
    procedure BitBtn10Click(Sender: TObject);
    procedure MaskEdit1Exit(Sender: TObject);
  private
  public
  end;

var
  TavolletidijDlg: TTavolletidijDlg;

implementation

uses
	TavolletiList, Egyeb, Kozos;
{$R *.DFM}

procedure TTavolletidijDlg.FormCreate(Sender: TObject);
var
  moddat: string;
begin
  moddat := copy(EgyebDlg.MaiDatum, 1, 8)+'01.'; // e h�nap elseje
  moddat := DatumHoznap(moddat, -1, true);  // el�z� h�nap utols� napja
  moddat := copy(moddat, 1, 8)+'01.';  // el�z� h�nap elseje
 	MaskEdit1.Text := moddat;
end;

procedure TTavolletidijDlg.BitBtn4Click(Sender: TObject);
begin

  if not Jodatum2(MaskEdit1) then begin
	NoticeKi('Hib�s d�tum megad�sa!');
   	MaskEdit1.SetFocus;
   	Exit;
    end;

   Screen.Cursor	:= crHourGlass;
   Application.CreateForm(TTavolletiListDlg, TavolletiListDlg);
   TavolletiListDlg.Tolt(MaskEdit1.Text);
   Screen.Cursor	:= crDefault;
   if TavolletiListDlg.vanadat then begin
     	TavolletiListDlg.Rep.Preview;
  	  end
   else begin
    	NoticeKi('Nincs a megadott felt�teleknek megfelel� t�tel!');
  	  end;
  	TavolletiListDlg.Destroy;
end;

procedure TTavolletidijDlg.BitBtn6Click(Sender: TObject);
begin
	Close;
end;

procedure TTavolletidijDlg.MaskEdit1Click(Sender: TObject);
begin
	with Sender as TMaskEdit do begin
  	SelectAll;
  end;
end;

procedure TTavolletidijDlg.BitBtn10Click(Sender: TObject);
begin
	EgyebDlg.Calendarbe(MaskEdit1);
end;


procedure TTavolletidijDlg.MaskEdit1Exit(Sender: TObject);
begin
	DatumExit(MaskEdit1, false);
end;

end.


