unit RESTAPI_grid;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, IPPeerClient, Vcl.Grids,
  REST.Response.Adapter, REST.Client, REST.Types, Data.Bind.Components,
  Data.Bind.ObjectScope, Data.DB, Vcl.StdCtrls,
  json, Vcl.ExtCtrls, RESTAPI_elem, Vcl.Menus, Data.DBXJSON, Data.Win.ADODB;

type
  TMoveSG = class(TCustomGrid);
  TSortInfo = record
    col : integer;
    asc : boolean;
   end;

type
  TRESTAPIGridDlg = class(TForm)
    SG: TStringGrid;
    pnlMessage: TPanel;
    GridPanel2: TGridPanel;
    Button2: TButton;
    Button4: TButton;
    Panel1: TPanel;
    PopupMenu1: TPopupMenu;
    UzenetTop1: TMenuItem;
    ppmKapcsolatok: TPopupMenu;
    btnKapcsolatok: TButton;
    btnArchive: TButton;
    Label1: TLabel;
    ebInkrementalis: TEdit;
    Panel2: TPanel;
    ebPhone: TEdit;
    ebPassword: TEdit;
    ebXuid: TEdit;
    Button7: TButton;
    Button1: TButton;
    btnFoto: TButton;
    Button3: TButton;
    pnlProgress: TPanel;
    DOLGFOTO: TADODataSet;
    DOLGFOTODO_KOD: TStringField;
    DOLGFOTODO_FOTO: TBlobField;
    chkArchivIs: TCheckBox;
    Label5: TLabel;
    pnlInfo: TPanel;
    procedure Button1Click(Sender: TObject);
    procedure RealtimeMessage(const S: string);
    procedure Button2Click(Sender: TObject);
    procedure SGDblClick(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure btnKapcsolatokClick(Sender: TObject);
    procedure KapcsolatokElemClick(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure Button6Click(Sender: TObject);
    procedure UzenetTop1Click(Sender: TObject);
    procedure btnArchiveClick(Sender: TObject);
    procedure ebInkrementalisClick(Sender: TObject);
    procedure ebInkrementalisChange(Sender: TObject);
    procedure Button7Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btnFotoClick(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure chkArchivIsClick(Sender: TObject);
  private
    FRequestResource: string;
    FRequestMethod: string;
    FResourceDisplayName: string;
    FKeyColumn: integer;
    FOrderByColumn: integer;
    FLastGridContent: TJSONValue;
    StructArray: TJSON;
    cMenuClosed: Cardinal;
    SortArray: array[0..0] of TSortInfo;
    function GetSelectedKod: string;
    function GetSelectedSor: integer;
    procedure ElemMutat(const DisplayMode: TRESTAPIDisplayMode; const Sor: integer; const Kod: string);
    procedure FillElemData(const DisplayMode: TRESTAPIDisplayMode; const Sor: integer; SGE: TStringGrid; ReadOnlyCells: TListBox);
    procedure ArchiveItem(const Sor: integer; const Kod: string);
    procedure DropMenuDown(Control: TControl; PopupMenu: TPopupMenu);
    procedure InitKapcsolatok;
    function CompareNumber(i1, i2: Double): Integer;
    function CompareValues(const S1, S2 : String;asc:Boolean): Integer;
    procedure SortGridByCols(Grid: TStringGrid; ColOrder: array of TSortInfo; Fixed: Boolean);
    procedure SetUser;
    procedure UploadAllAvatars;
  public
    procedure Load_groups;
    procedure Load_users;
    procedure Load_domains;
    procedure SetRequestResource(Res: string);
    procedure SetRequestMethod(AMethod: string);
    procedure RequestAll;
    procedure RefreshGrid(const jsGridData: TJSONValue);
    procedure SetResourceDisplayName(const DisplayName: string);
    function FillDataStructure (StructJSON: string): string;
  end;

const
  RESTAPI_true = 'Igen';
  RESTAPI_false = 'Nem';

var
  RESTAPIGridDlg: TRESTAPIGridDlg;

implementation

{$R *.dfm}

uses RESTAPI_kapcs, UzenetTop, RESTAPI_Kozos, Kozos, J_SQL, Egyeb;


procedure TRESTAPIGridDlg.SetRequestResource(Res: string);
begin
    FRequestResource:= Res;
end;

procedure TRESTAPIGridDlg.SetRequestMethod(AMethod: string);
begin
   FRequestMethod:= AMethod;
end;

procedure TRESTAPIGridDlg.SetResourceDisplayName(const DisplayName: string);
begin
   FResourceDisplayName:= DisplayName;
end;

procedure TRESTAPIGridDlg.SGDblClick(Sender: TObject);
begin
   ElemMutat(dmModify, GetSelectedSor, GetSelectedKod);
end;

procedure TRESTAPIGridDlg.UzenetTop1Click(Sender: TObject);
begin
   UzenetTopDlg.Show;
end;

procedure TRESTAPIGridDlg.RequestAll;
var
  Res, S: string;
  JSONResult: TJSONResult;
begin
    Caption:= FResourceDisplayName;
    JSONResult:= RESTAPIKozosDlg.ExecRequest(Format('%s', [FRequestResource]), 'get', RESTAPIKozosDlg.EmptyParamsList);
    if JSONResult.ErrorString <> '' then begin
        MessageDlg(Res, mtError,[mbOK],0);
        // NoticeKi(Res);
        Exit;  // function
        end // if
    else begin
        // RESTResponseDataSetAdapter1.Response:= RESTAPIKozosDlg.RESTResponse1;
        // RESTResponseDataSetAdapter1.Active:= True;
        end;
    FLastGridContent:= JSONResult.JSONContent;
    RefreshGrid(JSONResult.JSONContent);
   //  RESTResponseDataSetAdapter1.Active:= False;  // sz�tkapcsoljuk

end;

procedure TRESTAPIGridDlg.RefreshGrid(const jsGridData: TJSONValue);
const
  SzelessegSzorzo = 8;
var
  i, oszlop, sor, FieldNumber: integer;
  row: TJSON;
  CellValue, KeresMinta, TeljesSor: string;
  jsRecordArray: TJSONArray;
  jsRecord: TJSONValue;
  SzuresKell, SzuresOK, ElemArchive: boolean;
begin
  for i := 0 to SG.ColCount - 1 do
      SG.Cols[i].Clear;
  SG.RowCount := 1;
  // RealtimeMessage('Adatok lek�r�se...');
  SG.ColCount:=2;
  oszlop:= 0;
  // -- fejl�cek --
  for row in StructArray do begin
    if (row['ENABLED'].AsString = 'true') and (Pos('newonly', row['OPTION'].AsString) = 0) then begin
      if oszlop > SG.ColCount-1 then
          SG.ColCount:= SG.ColCount+1;
      SG.Cells[oszlop,0]:= row['DISPLAY'].AsString;
      SG.ColWidths[oszlop]:= row['DISPLAYWIDTH'].AsInteger * SzelessegSzorzo;
      if row['KEY'].AsString = 'true' then begin
          FKeyColumn:= oszlop;  // csak egy lehet
          end;
      if row['ORDERBY'].AsString = 'true' then begin
          FOrderByColumn:= oszlop;  // csak egy lehet
          end;
      inc(oszlop);
     end;  // if
    end; // for

  SortArray[0].col:= FOrderByColumn;
  SortArray[0].asc:= true;
  sor:=1;

  if (ebInkrementalis.Text = KeresAlapStr) or (ebInkrementalis.Text = '') then begin
       KeresMinta := '';
       SzuresKell:= False;
       end
   else begin
       KeresMinta := Lowercase(ebInkrementalis.Text);
       SzuresKell:= True;
       end;

  jsRecordArray := jsGridData as TJSONArray;
  for jsRecord in jsRecordArray do begin
     // if (row['archived'].isNull) then
     // if (row as TJSONObject).Get('archived').JsonValue is TJSONNull then
     // if not(row.Items.ContainsKey('archived')) then
     if (jsRecord as TJSONObject).Get('archived').JsonValue = nil then
        ElemArchive:= False
     // else ElemArchive:= (((jsRecord as TJSONObject).Get('archived').JsonValue as TJSONString).Value = 'true');
     else ElemArchive:= ((jsRecord as TJSONObject).Get('archived').JsonValue is TJSONTrue);
     if (not chkArchivIs.Checked) and ElemArchive then begin
        continue;  // next "for" iteration
        end;
    oszlop:= 0;
    if sor > SG.RowCount-1 then
          SG.RowCount:= SG.RowCount+1;
    for row in StructArray do begin
     if (row['ENABLED'].AsString = 'true') and (Pos('newonly', row['OPTION'].AsString) = 0) then begin
        if row['DATATYPE'].AsString = 'boolean' then begin
          if (jsRecord as TJSONObject).Get(row['FIELD'].AsString).JsonValue is TJSONTrue then
            CellValue:= RESTAPI_true
          else CellValue:= RESTAPI_false;
          end  // if boolean
        else begin
          CellValue:= ((jsRecord as TJSONObject).Get(row['FIELD'].AsString).JsonValue as TJSONString).Value;
          if row['DATATYPE'].AsString = 'timestamp' then begin
            CellValue:= RESTAPIKozosDlg.RESTTimeStampFormat(CellValue);
            end;
          end; // else = nem boolean
        SG.Cells[oszlop, sor]:= CellValue;
        inc(oszlop);
        end; // if
      end; // for Fields
      // ------------- sz�r�s -----------------
      TeljesSor:= '';
      for i:= 0 to oszlop-1 do
         TeljesSor := TeljesSor + '|' +SG.Cells[i, sor];
      SzuresOK:= not(SzuresKell) or (Pos(KeresMinta, Lowercase(TeljesSor)) > 0);
      if not SzuresOK then begin
        for i:= 0 to oszlop-1 do  // sor t�rl�se
           SG.Cells[i, sor] := '';
        sor:= sor-1;
        if sor < SG.RowCount-1 then
          SG.RowCount:= SG.RowCount-1;
        end;  // if SzuresOK
    Inc(sor);
    end;  // for Dataset

  if SG.RowCount>1 then
      SG.FixedRows:= 1;
  // --- �s v�g�l sorrendez�s ----
  SortGridByCols(SG, SortArray, True);
  // --- sorok sz�ma ----
  pnlInfo.Caption:= IntToStr(SG.RowCount-1) + ' adatsor';
end;


procedure TRESTAPIGridDlg.Button2Click(Sender: TObject);
begin
   ElemMutat(dmModify, GetSelectedSor, GetSelectedKod);
end;

procedure TRESTAPIGridDlg.Button3Click(Sender: TObject);
begin
   // UploadAllAvatars;
   // RESTAPIKozosDlg.csoportos_process;
   RESTAPIKozosDlg.Show;
end;

procedure TRESTAPIGridDlg.UploadAllAvatars;
var
  ms: TMemoryStream;
  // JPG: TJPEGImage;
  S, DOKOD, JPGBase64, thumbbase64, TotalRecCount, attachmime: string;
  i: integer;
  aOpenDialog : TOpenDialog;
begin
  S:= 'select f.DO_KOD, DO_FOTO '+
      ' from dolgozofoto f '+
      ' left join dolgozo d on d.DO_KOD = f.DO_KOD '+
      ' where d.DO_ARHIV = 0 ';
      // ' and f.DO_KOD = 839';
  if DolgFoto.Active then DolgFoto.Close;
  DolgFoto.CommandText:= S;
  DolgFoto.Open;
  // Query_Run(EgyebDlg.Query3, S);
  i:=1;
  // TotalRecCount:= IntToStr(EgyebDlg.Query3.RecordCount);
  TotalRecCount:= IntToStr(DolgFoto.RecordCount);

  aOpenDialog:=TopenDialog.Create(self);
  with aOpenDialog do begin
    Filter:='K�p (*.JPG)|*.JPG|';
    Options:=[ofFileMustExist];
		if Execute then begin
        attachmime:= RESTAPIKozosDlg.GetAttachmentMime(Filename);
				RESTAPIKozosDlg.ConvertFileToBase64(Filename, attachmime, JPGBase64, thumbbase64);
        ms:=TMemoryStream.Create;
        try
          while not DolgFoto.Eof do begin
              pnlProgress.Caption:= IntToStr(i)+ ' / '+ TotalRecCount;
              // DOKOD:= EgyebDlg.Query3.FieldByName('DO_KOD').AsString;
              // (EgyebDlg.Query3.FieldByName('DO_KOD') as TBlobField).SaveToStream(ms);
              // EgyebDlg.Query3.Fields[1].SaveToStream(ms);
              DOKOD:= DOLGFOTODO_KOD.AsString;
              // ----- Fuvaros k�pek ------ //
              // DOLGFOTODO_FOTO.SaveToStream(ms);
              // JPGBase64:= RESTAPIKozosDlg.ConvertStreamToBase64(ms);
              // ----- univerz�lis k�p megy fel! ------ //
              RESTAPIKozosDlg.SendAvatar(cUserAvatarResource, DOKOD, JPGBase64, True);
              DolgFoto.Next;
              Inc(i);
              end; // while
        finally
           ms.Free;
          end; // try-finally
        end;  // if execute
    end;  // with
end;

procedure TRESTAPIGridDlg.btnFotoClick(Sender: TObject);
var
  aOpenDialog : TOpenDialog;
  attachbase64, thumbbase64, attachmime, ActAvatarResource: string;
begin
  aOpenDialog:=TopenDialog.Create(self);
  with aOpenDialog do begin
    Filter:='K�p (*.JPG)|*.JPG|';
    Options:=[ofFileMustExist];
		if Execute then begin
        attachmime:= RESTAPIKozosDlg.GetAttachmentMime(Filename);
				RESTAPIKozosDlg.ConvertFileToBase64(Filename, attachmime, attachbase64, thumbbase64);
        if FRequestResource= cGroupResource then begin
           ActAvatarResource:= cGroupAvatarResource;
           end;
        if FRequestResource= cUserResource then begin
           ActAvatarResource:= cUserAvatarResource;
           end;
        RESTAPIKozosDlg.SendAvatar(ActAvatarResource, GetSelectedKod, attachbase64, False);
        end;  // if
    end;  // with
end;


procedure TRESTAPIGridDlg.btnArchiveClick(Sender: TObject);
begin
  // majd betenni
  if NoticeKi('Biztosan archiv�lni szeretn� ezt az elemet?', NOT_QUESTION) <> 0 then
     exit ;
  ArchiveItem(GetSelectedSor, GetSelectedKod);
  RequestAll;  // a t�bl�zat friss�t�se
end;

procedure TRESTAPIGridDlg.Button4Click(Sender: TObject);
begin
   ElemMutat(dmNew, 0, '');
end;

procedure TRESTAPIGridDlg.Button5Click(Sender: TObject);
begin
   Load_users;
end;

procedure TRESTAPIGridDlg.Button6Click(Sender: TObject);
begin
  //  UzenetTopDlg.Show;
  RESTAPIKozosDlg.Show;
end;

procedure TRESTAPIGridDlg.Button7Click(Sender: TObject);
begin
   SetUser;
end;

procedure TRESTAPIGridDlg.chkArchivIsClick(Sender: TObject);
begin
   RefreshGrid(FLastGridContent);
end;

procedure TRESTAPIGridDlg.SetUser;
var
  ErrorText: string;
  ResponseCode: integer;
begin
   RESTAPIKozosDlg.InitTokenRequest(ebPhone.Text, ebPassword.Text);
   if RESTAPIKozosDlg.GetNewToken(ResponseCode, ErrorText)  then begin
     UzenetTopDlg.Actxuid:= ebXuid.Text;
     UzenetTopDlg.SetAlias;
     end
   else begin
     MessageDlg('Hiba a bejelentkez�s sor�n! '+ErrorText, mtError,[mbOK],0);
     end;
end;

procedure TRESTAPIGridDlg.btnKapcsolatokClick(Sender: TObject);
begin
  DropMenuDown(btnKapcsolatok, ppmKapcsolatok);
  cMenuClosed := GetTickCount;
end;

procedure TRESTAPIGridDlg.DropMenuDown(Control: TControl; PopupMenu: TPopupMenu);
var
  APoint: TPoint;
begin
  APoint := Control.ClientToScreen(Point(0, Control.ClientHeight));
  PopupMenu.Popup(APoint.X, APoint.Y);
end;

procedure TRESTAPIGridDlg.ebInkrementalisChange(Sender: TObject);
begin
    RefreshGrid(FLastGridContent);
end;

procedure TRESTAPIGridDlg.ebInkrementalisClick(Sender: TObject);
begin
 ebInkrementalis.SelectAll;
end;

procedure TRESTAPIGridDlg.InitKapcsolatok;
begin
  ppmKapcsolatok.Items.Clear;
  if FRequestResource= cGroupResource then begin
       ppmKapcsolatok.Items.Add(TMenuItem.Create(ppmKapcsolatok));
       with ppmKapcsolatok.Items[ppmKapcsolatok.Items.Count-1] do begin
          Caption:= 'Felhaszn�l�k';
          Tag:=1;  // k�s�bb az OnClick-hez
          OnClick:= KapcsolatokElemClick;
          end;  // with
       cMenuClosed := 0;
       btnKapcsolatok.Visible:= True;
       end // if
  else if FRequestResource= cUserResource then begin
       ppmKapcsolatok.Items.Add(TMenuItem.Create(ppmKapcsolatok));
       with ppmKapcsolatok.Items[ppmKapcsolatok.Items.Count-1] do begin
          Caption:= 'Csoportok';
          Tag:=2;  // k�s�bb az OnClick-hez
          OnClick:= KapcsolatokElemClick;
          end;  // with
       // egy user egy c�g, teh�t ilyen funkci� nincs
       {ppmKapcsolatok.Items.Add(TMenuItem.Create(ppmKapcsolatok));
       with ppmKapcsolatok.Items[ppmKapcsolatok.Items.Count-1] do begin
          Caption:= 'C�gek';
          Tag:=3;  // k�s�bb az OnClick-hez
          OnClick:= KapcsolatokElemClick;
          end;  // with
          }
       cMenuClosed := 0;
       btnKapcsolatok.Visible:= True;
       end // if
  else if FRequestResource= 'admin/domains' then begin
       ppmKapcsolatok.Items.Add(TMenuItem.Create(ppmKapcsolatok));
       with ppmKapcsolatok.Items[ppmKapcsolatok.Items.Count-1] do begin
          Caption:= 'Felhaszn�l�k';
          Tag:=1;  // k�s�bb az OnClick-hez
          OnClick:= KapcsolatokElemClick;
          end;  // with
       cMenuClosed := 0;
       btnKapcsolatok.Visible:= True;
       end // if
  else begin
       btnKapcsolatok.Visible:= False;  // m�shol ne legyen
       end;  // else
end;

procedure TRESTAPIGridDlg.KapcsolatokElemClick(Sender: TObject);
var
   i: integer;
   ElemID, ListaURL, KapcsolatElem, URLKapcsolatElem, IDKapcsolatElem, IDElem: string;
   RESTAPIKapcs: TRESTAPIKapcsolatDlg;
begin
   ElemID:= GetSelectedKod;  // a StringGrid kiv�lasztott sor�b�l
   i:= (Sender as TMenuItem).Tag;
   case i of
     1: begin
        ListaURL:= '/admin/users';
        KapcsolatElem:= 'Users';
        IDKapcsolatElem:='xuid';
        URLKapcsolatElem:='user';
        IDElem:='xgid';
        end;
     2: begin
        ListaURL:= '/admin/groups';
        KapcsolatElem:= 'Groups';
        IDKapcsolatElem:='xgid';
        URLKapcsolatElem:='groups';
        IDElem:='xuid';
        end;
     3: begin
        ListaURL:= '/admin/domains';
        KapcsolatElem:= 'Domains';
        IDKapcsolatElem:='slug';
        URLKapcsolatElem:='user';
        IDElem:='xuid';
        end;
     {3: begin
        ListaURL:= '/admin/profiles';
        KapcsolatElem:= 'Profiles';
        IDKapcsolatElem:='xpid';
        URLKapcsolatElem:='profile';
        IDElem:='xuid';
        end;}
     end;  // case
   // MessageDlg('Kapcsolatok : '+IntToStr(i), mtInformation,[mbOK],0);
   Screen.Cursor:= crHourGlass;
   Application.CreateForm(TRESTAPIKapcsolatDlg, RESTAPIKapcs);
   RESTAPIKapcs.SetResourceDisplayName(FResourceDisplayName); // tov�bbadjuk
   RESTAPIKapcs.SetRequestResource(FRequestResource, ElemID); // tov�bbadjuk
   RESTAPIKapcs.SetKapcsolatElem(KapcsolatElem);
   RESTAPIKapcs.SetListaURL(ListaURL);
   RESTAPIKapcs.SetIDElem(IDElem);
   RESTAPIKapcs.SetIDKapcsolatElem(IDKapcsolatElem);
   RESTAPIKapcs.SetURLKapcsolatElem(URLKapcsolatElem);
   RESTAPIKapcs.FillKapcsolatData(chkArchivIs.Checked);
   Screen.Cursor	:= crDefault;
   RESTAPIKapcs.ShowModal;
   if RESTAPIKapcs.ModalResult = mrOK then begin
      RequestAll;  // a t�bl�zat friss�t�se
      end;
   RESTAPIKapcs.Free;
end;


function TRESTAPIGridDlg.GetSelectedKod: string;
begin
   Result:= SG.Cells[FKeyColumn, GetSelectedSor];
end;

function TRESTAPIGridDlg.GetSelectedSor: integer;
begin
   Result:= SG.Selection.Top;
end;

procedure TRESTAPIGridDlg.ElemMutat(const DisplayMode: TRESTAPIDisplayMode; const Sor: integer; const Kod: string);
var
   APIElemDlg: TRESTAPIElemDlg;
   ElemID: string;
begin
   Screen.Cursor:= crHourGlass;
   Application.CreateForm(TRESTAPIElemDlg, APIElemDlg);
   APIElemDlg.SetResourceDisplayName(FResourceDisplayName); // tov�bbadjuk
   APIElemDlg.SetDisplayMode(DisplayMode);
   if DisplayMode= dmNew then begin
      APIElemDlg.SetRequestMethod('post');
      APIElemDlg.SetRequestResource(FRequestResource); // tov�bbadjuk
      FillElemData(DisplayMode, Sor, APIElemDlg.SGE, APIElemDlg.lbReadOnlyCells);
      end;  // if
   if DisplayMode= dmModify then begin
      APIElemDlg.SetRequestMethod('put');
      ElemID:= Kod;
      APIElemDlg.SetRequestResource(FRequestResource+'/'+ElemID); // tov�bbadjuk
      FillElemData(DisplayMode, Sor, APIElemDlg.SGE, APIElemDlg.lbReadOnlyCells);
      end;  // if

   Screen.Cursor	:= crDefault;
   APIElemDlg.ShowModal;
   if APIElemDlg.ModalResult = mrOK then begin
      RequestAll;  // a t�bl�zat friss�t�se
      end;
   APIElemDlg.Free;
end;

procedure TRESTAPIGridDlg.ArchiveItem(const Sor: integer; const Kod: string);
var
  Res, RequestRes, BodyKey, BodyValue, S: string;
  row: TJSON;
  oszlop: integer;
  JSONResult: TJSONResult;
begin
  if FRequestResource <> cGroupResource then begin
    RequestRes:= FRequestResource+'/'+Kod;  // elem m�dos�t�sa
    RESTAPIKozosDlg.RESTAPIParamsList.Clear;
    RESTAPIKozosDlg.RESTAPIParamsList.Add('archived', 'true');
    JSONResult:= RESTAPIKozosDlg.ExecRequest(RequestRes, 'put', RESTAPIKozosDlg.RESTAPIParamsList);
    end  // if
  else begin // Group archiv�l�sra van k�l�n h�v�s
    RequestRes:= Format(cGroupArchiveResource, [Kod]);
    JSONResult:= RESTAPIKozosDlg.ExecRequest(RequestRes, 'delete', RESTAPIKozosDlg.EmptyParamsList);
    end;  // else
  if JSONResult.ErrorString <> '' then begin
      S:='Hiba a ment�s sor�n! K�d: '+ JSONResult.ErrorString;
      MessageDlg( S, mtError,[mbOK],0);
      // NoticeKi(S);
      end;
end;

procedure TRESTAPIGridDlg.FillElemData(const DisplayMode: TRESTAPIDisplayMode; const Sor: integer; SGE: TStringGrid; ReadOnlyCells: TListBox);
var
   i, oszlop: integer;
   row: TJSON;
   NewOnly: boolean;
   S1, S2, S3, S4: string;
begin
   SGE.Cells[0, 0]:= 'Adatmez�';
   SGE.Cells[1, 0]:= '�rt�k';
   i:=1;
   oszlop:=0;
   ReadOnlyCells.Items.Clear;
   for row in StructArray do begin
    // S1:= row['FIELD'].AsString;
    // S2:= row['ENABLED'].AsString;
    // S3:= row['OPTION'].AsString;
    // S4:= row['DISPLAY'].AsString;
    NewOnly:= (Pos('newonly', row['OPTION'].AsString) > 0);
    if (row['ENABLED'].AsString = 'true') and ((DisplayMode = dmNew) or not NewOnly) then begin
      SGE.Cells[0, i]:= row['DISPLAY'].AsString;  // mez� display string
      if DisplayMode = dmModify then begin
        SGE.Cells[1, i]:= SG.Cells[oszlop, Sor];  // mez� �rt�k
        if row['EDITABLE'].AsString = 'false' then
          ReadOnlyCells.Items.Add(IntToStr(i)+'|'+IntToStr(1));
        end;
      SGE.Cells[2, i]:= row['FIELD'].AsString;  // mez�n�v
      SGE.Cells[3, i]:= row['DATATYPE'].AsString;  // adatt�pus
      SGE.Cells[4, i]:= row['KEY'].AsString;  // kulcs-e
      inc(i);
      inc(oszlop);  // ezt mindenk�pp l�ptetni kell
      end;  // if
     end;  // for
   SGE.RowCount:= i;
end;


procedure TRESTAPIGridDlg.FormCreate(Sender: TObject);
begin
  // ebInkrementalis.SetFocus;
end;

function TRESTAPIGridDlg.FillDataStructure (StructJSON: string): string;
var
  S, retvalue: string;
begin
  try
    StructArray := TJSON.Parse(StructJSON);
    retvalue:= '';
  except
     on E: exception do begin
        S:= 'JSON Parse error ('+E.Message+'), text='+StructJSON;
        // Hibakiiro(S);
        retvalue:= S;
        end; // on E
    end;  // try-esxept
  Result:= retvalue;
  InitKapcsolatok;
end;

procedure TRESTAPIGridDlg.Button1Click(Sender: TObject);
var
  ErrorText: string;
  ResponseCode: integer;
begin
   // Balogh Ferenc
   RESTAPIKozosDlg.InitTokenRequest('36209588549', '123456');
   if RESTAPIKozosDlg.GetNewToken(ResponseCode, ErrorText)  then begin
     UzenetTopDlg.Actxuid:= '079';
     end
   else begin
     MessageDlg('Hiba a bejelentkez�s sor�n! '+ErrorText, mtError,[mbOK],0);
     end;
end;

procedure TRESTAPIGridDlg.Load_groups;
var
  Struct: string;
begin
   Struct :=
    '['+
      // '{"FIELD":"id","KEY":"false","DISPLAY":"Sorsz�m","DATATYPE":"integer","DISPLAYWIDTH":"8","ORDERBY":"false","EDITABLE":"false","ENABLED":"false"},'+
        '{"FIELD":"xgid","KEY":"true","DISPLAY":"Azonos�t�","DATATYPE":"integer","DISPLAYWIDTH":"12","ORDERBY":"false","EDITABLE":"true","ENABLED":"true","OPTION":""},'+
        '{"FIELD":"name","KEY":"false","DISPLAY":"Csoport n�v","DATATYPE":"string","DISPLAYWIDTH":"25","ORDERBY":"true","EDITABLE":"true","ENABLED":"true","OPTION":""},'+
        '{"FIELD":"archived","KEY":"false","DISPLAY":"Arch�v","DATATYPE":"boolean","DISPLAYWIDTH":"10","ORDERBY":"false","EDITABLE":"false","ENABLED":"true","OPTION":""},'+
        '{"FIELD":"created_at","KEY":"false","DISPLAY":"L�trehozva","DATATYPE":"timestamp","DISPLAYWIDTH":"16","ORDERBY":"false","EDITABLE":"false","ENABLED":"true","OPTION":""},'+
        '{"FIELD":"updated_at","KEY":"false","DISPLAY":"M�dos�tva","DATATYPE":"timestamp","DISPLAYWIDTH":"16","ORDERBY":"false","EDITABLE":"false","ENABLED":"true","OPTION":""},'+
      ']';
     SetRequestResource(cGroupResource);
     SetRequestMethod('get');
     SetResourceDisplayName('Levelez�si csoport');
     btnArchive.Enabled:= True;
     btnFoto.Enabled:= True;
     FillDataStructure(Struct);
     RequestAll;
end;

procedure TRESTAPIGridDlg.Load_users;
var
  Struct: string;
begin
  Struct :=
    '['+
      // '{"FIELD":"id","KEY":"false","DISPLAY":"Sorsz�m","DATATYPE":"integer","DISPLAYWIDTH":"8","ORDERBY":"false","EDITABLE":"false","ENABLED":"false"},'+
      '{"FIELD":"xuid","KEY":"true","DISPLAY":"Azonos�t�","DATATYPE":"integer","DISPLAYWIDTH":"12","ORDERBY":"false","EDITABLE":"true","ENABLED":"true","OPTION":""},'+
      '{"FIELD":"name","KEY":"false","DISPLAY":"Felhaszn�l� n�v","DATATYPE":"string","DISPLAYWIDTH":"25","ORDERBY":"true","EDITABLE":"true","ENABLED":"true","OPTION":""},'+
      '{"FIELD":"phone","KEY":"false","DISPLAY":"Telefon","DATATYPE":"string","DISPLAYWIDTH":"15","ORDERBY":"false","EDITABLE":"true","ENABLED":"true","OPTION":""},'+
      '{"FIELD":"email","KEY":"false","DISPLAY":"Email","DATATYPE":"string","DISPLAYWIDTH":"20","ORDERBY":"false","EDITABLE":"true","ENABLED":"true","OPTION":""},'+
      '{"FIELD":"admin","KEY":"false","DISPLAY":"Admin","DATATYPE":"boolean","DISPLAYWIDTH":"10","ORDERBY":"false","EDITABLE":"true","ENABLED":"true","OPTION":""},'+
      '{"FIELD":"overkill","KEY":"false","DISPLAY":"Irodai","DATATYPE":"boolean","DISPLAYWIDTH":"10","ORDERBY":"false","EDITABLE":"true","ENABLED":"true","OPTION":""},'+
      '{"FIELD":"archived","KEY":"false","DISPLAY":"Arch�v","DATATYPE":"boolean","DISPLAYWIDTH":"10","ORDERBY":"false","EDITABLE":"true","ENABLED":"true","OPTION":""},'+
      '{"FIELD":"password","KEY":"false","DISPLAY":"Jelsz�","DATATYPE":"string","DISPLAYWIDTH":"14","ORDERBY":"false","EDITABLE":"false","ENABLED":"true","OPTION":"newonly"},'+
      '{"FIELD":"password_confirmation","KEY":"false","DISPLAY":"Jelsz� �jra","DATATYPE":"string","DISPLAYWIDTH":"14","ORDERBY":"false","EDITABLE":"false","ENABLED":"true","OPTION":"newonly"}'+

      // '{"FIELD":"created_at","KEY":"false","DISPLAY":"L�trehozva","DATATYPE":"timestamp","DISPLAYWIDTH":"16","ORDERBY":"false","EDITABLE":"false","ENABLED":"true"},'+
      // '{"FIELD":"updated_at","KEY":"false","DISPLAY":"M�dos�tva","DATATYPE":"timestamp","DISPLAYWIDTH":"16","ORDERBY":"false","EDITABLE":"false","ENABLED":"true"},'+
    ']';
   SetRequestResource(cUserResource);
   SetRequestMethod('get');
   SetResourceDisplayName('Felhaszn�l�');
   btnArchive.Enabled:= True;
   btnFoto.Enabled:= True;
   FillDataStructure(Struct);
   RequestAll;
end;

procedure TRESTAPIGridDlg.Load_domains;
var
  Struct: string;
begin
   Struct :=
    '['+
      // '{"FIELD":"id","KEY":"false","DISPLAY":"Sorsz�m","DATATYPE":"integer","DISPLAYWIDTH":"8","ORDERBY":"false","EDITABLE":"false","ENABLED":"false"},'+
        '{"FIELD":"slug","KEY":"true","DISPLAY":"Azonos�t�","DATATYPE":"integer","DISPLAYWIDTH":"12","ORDERBY":"false","EDITABLE":"true","ENABLED":"true","OPTION":""},'+
        '{"FIELD":"name","KEY":"false","DISPLAY":"C�g n�v","DATATYPE":"string","DISPLAYWIDTH":"25","ORDERBY":"true","EDITABLE":"true","ENABLED":"true","OPTION":""},'+
        '{"FIELD":"created_at","KEY":"false","DISPLAY":"L�trehozva","DATATYPE":"timestamp","DISPLAYWIDTH":"16","ORDERBY":"false","EDITABLE":"false","ENABLED":"true","OPTION":""},'+
        '{"FIELD":"updated_at","KEY":"false","DISPLAY":"M�dos�tva","DATATYPE":"timestamp","DISPLAYWIDTH":"16","ORDERBY":"false","EDITABLE":"false","ENABLED":"true","OPTION":""},'+
      ']';
     SetRequestResource('admin/domains');
     SetRequestMethod('get');
     SetResourceDisplayName('C�g');
     btnArchive.Enabled:= False;
     btnFoto.Enabled:= False;
     FillDataStructure(Struct);
     RequestAll;
end;

procedure TRESTAPIGridDlg.RealtimeMessage(const S: string);
begin
   pnlMessage.Caption:= S;
   pnlMessage.Refresh;
end;

function TRESTAPIGridDlg.CompareNumber(i1, i2: Double): Integer;
// Result: -1 if i1 < i2, 1 if i1 > i2, 0 if i1 = i2
begin
  if i1 < i2 then
    Result := -1
  else if i1 > i2 then
    Result := 1
  else
    Result := 0;
end;

// Compare Strings if possible try to interpret as numbers
function TRESTAPIGridDlg.CompareValues(const S1, S2 : String;asc:Boolean): Integer;
var
  V1, V2 : Double;
  C1, C2 : Integer;
begin
  Val(S1, V1, C1);
  Val(S2, V2, C2);
  if (C1 = 0) and (C2 = 0) then  // both as numbers
     Result := CompareNumber(V1, V2)
  else  // not both as nubers
     Result := AnsiCompareStr(S1, S2);
  if not Asc then Result := Result * -1;

end;

procedure TRESTAPIGridDlg.SortGridByCols(Grid: TStringGrid; ColOrder: array of TSortInfo; Fixed: Boolean);
var
  I, J, FirstRow: Integer;
  Sorted: Boolean;

  function Sort(Row1, Row2: Integer): Integer;
  var
    C: Integer;
  begin
    C := 0;
    Result := CompareValues(Grid.Cols[ColOrder[C].col][Row1], Grid.Cols[ColOrder[C].col][Row2],ColOrder[C].asc);
    if Result = 0 then
    begin
      Inc(C);
      while (C <= High(ColOrder)) and (Result = 0) do
      begin
        Result := CompareValues(Grid.Cols[ColOrder[C].col][Row1], Grid.Cols[ColOrder[C].col][Row2],ColOrder[C].asc);
        Inc(C);
      end;
    end;
  end;

begin
  for I := 0 to High(ColOrder) do
    if (ColOrder[I].col < 0) or (ColOrder[I].col >= Grid.ColCount) then
      Exit;

  if not Fixed then
    FirstRow := 0
  else
    FirstRow := Grid.FixedRows;

  J := FirstRow;
  Sorted := True;
  repeat
    Inc(J);
    for I := FirstRow to Grid.RowCount - 2 do
      if Sort(I, I + 1) > 0 then
      begin
        TMoveSG(Grid).MoveRow(i + 1, i);
        Sorted := False;
      end;
  until Sorted or (J >= Grid.RowCount + 1000);
  Grid.Repaint;
end;

{procedure TForm1.Button1Click(Sender: TObject);
const // we want to use only 4 columns
  MyArray: array[0..3] of TSortInfo =
    ((col: 1; asc: true),
     (col: 2; asc: true),
     (col: 3; asc: true),
     (col: 4; asc: false)
     );
begin
  SortGridByCols(StringGrid1,MyArray,true);
end;}


end.
