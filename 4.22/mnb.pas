// ************************************************************************ //
// The types declared in this file were generated from data read from the
// WSDL File described below:
// WSDL     : E:\Projects\common\mnb.wsdl
// Encoding : UTF-8
// Version  : 1.0
// (2008.09.20. 12:51:47 - 1.33.2.5)
// ************************************************************************ //

unit mnb;

interface

uses SysUtils, InvokeRegistry, SOAPHTTPClient, Types, XSBuiltIns;

type

  // ************************************************************************ //
  // The following types, referred to in the WSDL document are not being represented
  // in this file. They are either aliases[@] of other types represented or were referred
  // to but never[!] declared in the document. The types from the latter category
  // typically map to predefined/known XML or Borland types; however, they could also 
  // indicate incorrect WSDL documents that failed to declare or import a schema type.
  // ************************************************************************ //
  // !:string          - "http://www.w3.org/2001/XMLSchema"



  // ************************************************************************ //
  // Namespace : http://www.mnb.hu/webservices/
  // soapAction: http://www.mnb.hu/webservices/%operationName%
  // transport : http://schemas.xmlsoap.org/soap/http
  // style     : document
  // binding   : MNBArfolyamServiceSoap
  // service   : MNBArfolyamService
  // port      : MNBArfolyamServiceSoap
  // URL       : http://www.mnb.hu/arfolyamok.asmx
  // ************************************************************************ //
  MNBArfolyamServiceSoap = interface(IInvokable)
  ['{30E01F17-E848-C080-5C70-0B40FFB41294}']
    function  GetInfo: WideString; stdcall;
    function  GetCurrentExchangeRates: WideString; stdcall;
    function  GetExchangeRates(const startDate: WideString; const endDate: WideString; const currencyNames: WideString): WideString; stdcall;
  end;

function GetMNBArfolyamServiceSoap(var ErrorString: string; LogFn: string; UseWSDL: Boolean=System.False; Addr: string=''; HTTPRIO: THTTPRIO = nil): MNBArfolyamServiceSoap;


implementation

 uses aki1;  // WriteLogFile miatt

function GetMNBArfolyamServiceSoap(var ErrorString: string; LogFn: string; UseWSDL: Boolean; Addr: string; HTTPRIO: THTTPRIO): MNBArfolyamServiceSoap;
const
  defWSDL = 'E:\Projects\common\mnb.wsdl';
  defURL  = 'http://www.mnb.hu/arfolyamok.asmx';
  defSvc  = 'MNBArfolyamService';
  defPrt  = 'MNBArfolyamServiceSoap';
var
  RIO: THTTPRIO;
begin
  Result := nil;
  ErrorString:= '';
    if (Addr = '') then begin
      if UseWSDL then
        Addr := defWSDL
      else
        Addr := defURL;
    end;
    if LogFn<>'' then Form1.WriteLogFile(LogFn,'-- GetMNBArfolyamServiceSoap 1 '+DateTimeToStr(now));
    if HTTPRIO = nil then
      RIO := THTTPRIO.Create(nil)
    else
      RIO := HTTPRIO;
    if LogFn<>'' then Form1.WriteLogFile(LogFn,'-- GetMNBArfolyamServiceSoap 2 '+DateTimeToStr(now));
    try
      try
        Result := (RIO as MNBArfolyamServiceSoap);
        if LogFn<>'' then Form1.WriteLogFile(LogFn,'-- GetMNBArfolyamServiceSoap 3 '+DateTimeToStr(now));
        if UseWSDL then begin
          RIO.WSDLLocation := Addr;
          RIO.Service := defSvc;
          RIO.Port := defPrt;
        end else
          RIO.URL := Addr;
       if LogFn<>'' then Form1.WriteLogFile(LogFn,'-- GetMNBArfolyamServiceSoap 4 '+DateTimeToStr(now));
    except
      on E: Exception do begin
        if LogFn<>'' then Form1.WriteLogFile(LogFn,'-- GetMNBArfolyamServiceSoap EXCEPT '+DateTimeToStr(now));
        ErrorString:= E.Message;
        end;
      end;  // except
    finally
      if LogFn<>'' then Form1.WriteLogFile(LogFn,'-- GetMNBArfolyamServiceSoap 5 '+DateTimeToStr(now));
      if (Result = nil) and (HTTPRIO = nil) then
        RIO.Free;
    end; // finally
end;


initialization
  InvRegistry.RegisterInterface(TypeInfo(MNBArfolyamServiceSoap), 'http://www.mnb.hu/webservices/', 'UTF-8');
  InvRegistry.RegisterDefaultSOAPAction(TypeInfo(MNBArfolyamServiceSoap), 'http://www.mnb.hu/webservices/%operationName%');
  InvRegistry.RegisterInvokeOptions(TypeInfo(MNBArfolyamServiceSoap), ioDocument);

end. 