unit KipalettaList;

interface

uses
  WinTypes, WinProcs, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, QuickRpt, QRExpr, Qrctrls, ExtCtrls, printers, DB, DBTables, Grids, IniFiles,
  ADODB ;

type
  TKiPalettaListDlg = class(TForm)
    Rep: TQuickRep;
    QRBand3: TQRBand;
	 QRLSzamla: TQRLabel;
	 QRBand1: TQRBand;
    QRBand2: TQRBand;
    QRLabel38: TQRLabel;
    QRLabel6: TQRLabel;
    QRSysData1: TQRSysData;
    QRLabel7: TQRLabel;
    QRSysData4: TQRSysData;
    QRLabel1: TQRLabel;
    Q3: TQRLabel;
    Q2: TQRLabel;
    Q5: TQRLabel;
    Q4: TQRLabel;
    QRLabel21: TQRLabel;
    Q1: TQRLabel;
    ArfolyamGrid: TStringGrid;
    Osszesito: TStringGrid;
    QV3: TQRLabel;
    QRLabel13: TQRLabel;
    QRLabel17: TQRLabel;
    Query1: TADOQuery;
    QRGroup1: TQRGroup;
    QRBand5: TQRBand;
    O1: TQRLabel;
    O3: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel15: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRLabel18: TQRLabel;
    QRShape1: TQRShape;
    QRLabel9: TQRLabel;
    Q6: TQRLabel;
    QRLabel10: TQRLabel;
    Q7: TQRLabel;
    QRShape2: TQRShape;
    ChildBand1: TQRChildBand;
    QRLabel12: TQRLabel;
    QRLabel16: TQRLabel;
    QRShape3: TQRShape;
    QRLabel22: TQRLabel;
    Q8: TQRLabel;
    QRLabel24: TQRLabel;
    Q50: TQRLabel;
    QRShape4: TQRShape;
    procedure FormCreate(Sender: TObject);
    procedure	Tolt(dat1, helykod	: string; reszletes : boolean );
    procedure QRBand1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBand2BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure RepBeforePrint(Sender: TCustomQuickRep;
      var PrintReport: Boolean);
    procedure QRGroup1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBand5BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
  private
    	sorszam			: integer;
     	reszlet			: boolean;
       omenny          : integer;
       oomenny         : integer;
  public
     vanadat			: boolean;
  end;

var
  KiPalettaListDlg: TKiPalettaListDlg;

implementation

uses
	J_SQL, Egyeb, KOzos;

{$R *.DFM}

procedure TKiPalettaListDlg.FormCreate(Sender: TObject);
begin
	EgyebDlg.SetADOQueryDatabase(Query1);
  	sorszam		:= 1;
end;

procedure	TKiPalettaListDlg.Tolt(dat1, helykod  : string; reszletes : boolean );
var
  	sqlstr		: string;
   dattol	  	: string;
   datig 		: string;
begin
   vanadat 	:= false;
   datig 		:= dat1;
   reszlet		:= reszletes;
   if dat1 = '' then begin
   	datig	:= EgyebDlg.MaiDatum;
   end;
   QrLabel21.Caption	:= datig;
   if Trim(helykod) = ''  then begin
      Query_Run (Query1, 'SELECT  ''1'' ELSO, PV_HETI1 AS HETIP, PV_HEKO1 AS HEKOD, PV_HETI2 AS HETI2, PV_HEKO2 AS HEKO2, '+
          ' PV_CSERE, PV_DATUM, PV_IDOPT, PV_MEGJE, PV_MENNY, PV_MSFEL, PV_MSKOD, '+
          ' PV_MSLER, PV_MSTIP, PV_TIPUS  FROM PALVALT  WHERE PV_HEKO1 <> '''' AND PV_DATUM+PV_IDOPT <= '''+datig+'24:00'''+
          ' UNION SELECT  ''2'' ELSO, PV_HETI2 AS HETIP, PV_HEKO2 AS HEKOD, PV_HETI1 AS HETI2, PV_HEKO1 AS HEKO2, PV_CSERE, PV_DATUM, '+
          ' PV_IDOPT, PV_MEGJE, PV_MENNY, PV_MSFEL, PV_MSKOD, PV_MSLER, PV_MSTIP, PV_TIPUS '+
          ' FROM PALVALT  WHERE PV_HEKO2 <> '''' AND PV_DATUM+PV_IDOPT <= '''+datig+'24:00'''+
          ' ORDER BY 2, 3, PV_DATUM, PV_IDOPT');
   end else begin
       Query_Run (Query1, 'SELECT  ''1'' ELSO, PV_HETI1 AS HETIP, PV_HEKO1 AS HEKOD, PV_HETI2 AS HETI2, PV_HEKO2 AS HEKO2, '+
          ' PV_CSERE, PV_DATUM, PV_IDOPT, PV_MEGJE, PV_MENNY, PV_MSFEL, PV_MSKOD, '+
          ' PV_MSLER, PV_MSTIP, PV_TIPUS  FROM PALVALT  WHERE PV_HEKO1 = '''+copy(helykod, 2, 999)+''' AND PV_DATUM+PV_IDOPT <= '''+datig+'24:00'''+
          ' UNION SELECT  ''2'' ELSO, PV_HETI2 AS HETIP, PV_HEKO2 AS HEKOD, PV_HETI1 AS HETI2, PV_HEKO1 AS HEKO2, PV_CSERE, PV_DATUM, '+
          ' PV_IDOPT, PV_MEGJE, PV_MENNY, PV_MSFEL, PV_MSKOD, PV_MSLER, PV_MSTIP, PV_TIPUS '+
          ' FROM PALVALT  WHERE PV_HEKO2 = '''+copy(helykod, 2, 999)+''' AND PV_DATUM+PV_IDOPT <= '''+datig+'24:00'''+
          ' ORDER BY 2, 3, PV_DATUM, PV_IDOPT');
       QRBand2.Enabled := false;
   end;

   vanadat := Query1.RecordCount > 0;
   if not reszlet then begin
   	   // Az �sszesen sorokat kisebbre venni
//       QRBand5.Height      := O1.Top + O1.Height + 5;
       QRShape2.Enabled 	:= false;
   end;
end;

procedure TKiPalettaListDlg.QRBand1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
var
   menny	: integer;
begin
   menny		:= StrToIntDef(Query1.FieldByName('PV_MENNY').AsString, 0);
   if StrToIntDef(Query1.FieldByName('PV_TIPUS').AsString, -1) = 0 then begin
       if StrToIntDef(Query1.FieldByName('ELSO').AsString, 0) < 2 then begin
           menny  := -1 * menny;
       end;
   end;
   Q1.Caption 	:= IntToStr(sorszam);
   Inc(sorszam);
  	Q2.Caption 	:= Query1.FieldByName('PV_DATUM').AsString+Query1.FieldByName('PV_IDOPT').AsString;
   case StrToIntDef(Query1.FieldByName('PV_TIPUS').AsString, 0) of
       0 :
       begin
  	        Q3.Caption 	:= 'V';
           if StrToIntDef(Query1.FieldByName('PV_CSERE').AsString, 0) > 0 then begin
  	            Q3.Caption 	:= 'CS';
           end;
       end;
       1 :
       begin
  	        Q3.Caption 	:= 'L';
       end;
       2 :
       begin
  	        Q3.Caption 	:= 'MF';
           if StrToIntDef(Query1.FieldByName('PV_MSTIP').AsString, 0) > 0 then begin
  	            Q3.Caption 	:= 'ML';
           end;
       end;
   end;

  	Q4.Caption	:= IntToStr(menny);
   if StrToIntDef(Query1.FieldByName('PV_CSERE').AsString, 0) > 0 then begin
  	    Q4.Caption	:= '('+IntToStr(menny)+')';
   end;
  	Q5.Caption 	:= GetHelynev(Query1.FieldByName('HETI2').AsString+Query1.FieldByName('HEKO2').AsString);
  	Q50.Caption := Query1.FieldByName('PV_MSKOD').AsString;
  	Q6.Caption 	:= Query1.FieldByName('PV_MSFEL').AsString;
  	Q8.Caption 	:= Query1.FieldByName('PV_MSLER').AsString;
  	Q7.Caption 	:= Query1.FieldByName('PV_MEGJE').AsString;
   case StrToIntDef(Query1.FieldByName('PV_TIPUS').AsString, 0) of
       0 :     // v�ltoz�s
       begin
           // Csak akkor v�ltozik a dolog, ha nem cser�lt�nk
           if StrToIntDef(Query1.FieldByName('PV_CSERE').AsString, 0) = 0 then begin
               omenny  := omenny + menny;
           end;
       end;
       1 :     // lelt�r
       begin
           omenny  := menny;
       end;
       2 :     // megb�z�s
       begin
           omenny  := omenny + menny;
       end;
   end;
   PrintBand	:= reszlet;
end;

procedure TKiPalettaListDlg.QRBand2BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
begin
	// A v�g�sszesenek ki�r�sa
  	QV3.Caption 	:= Format('%d', [oomenny]);
end;

procedure TKiPalettaListDlg.RepBeforePrint(Sender: TCustomQuickRep;
      var PrintReport: Boolean);
begin
   omenny      := 0;
   oomenny     := 0;
end;

procedure TKiPalettaListDlg.QRGroup1BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
	// A rendsz�m be�r�sa
   QrLabel5.Caption	:= GetHelynev(Query1.FieldByName('HETIP').AsString+Query1.FieldByName('HEKOD').AsString);
   sorszam             := 1;
   PrintBand			:= reszlet;
end;

procedure TKiPalettaListDlg.QRBand5BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
   O1.Caption	:= GetHelynev(Query1.FieldByName('HETIP').AsString+Query1.FieldByName('HEKOD').AsString) + ' �sszes :';
   O3.Caption	:= Format('%d', [omenny]);
   oomenny     := oomenny + omenny;
   omenny      := 0;
end;

procedure TKiPalettaListDlg.ChildBand1BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
	PrintBand	:= not reszlet;
end;

end.
