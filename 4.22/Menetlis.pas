unit menetlis;

interface

uses
  WinTypes, WinProcs, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, QuickRpt, QRExpr, Qrctrls, ExtCtrls, printers, DB, DBTables, Egyeb, Forgalom, J_SQL,
  Grids, Kozos, ADODB ;

type
  TMenetlisDlg = class(TForm)
    Rep: TQuickRep;
    QRBand3: TQRBand;
    QRLSzamla: TQRLabel;
    QRBand1: TQRBand;
    QRLabel15: TQRLabel;
    QRLabel16: TQRLabel;
    QRLabel17: TQRLabel;
    QRLabel6: TQRLabel;
    QRSysData1: TQRSysData;
    QRLabel7: TQRLabel;
    QRSysData4: TQRSysData;
    QRLabel1: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel3: TQRLabel;
    QRShape1: TQRShape;
    QRLabel2: TQRLabel;
    QRShape3: TQRShape;
    QRShape4: TQRShape;
    QRShape5: TQRShape;
    QRShape6: TQRShape;
    QRShape7: TQRShape;
    QRShape8: TQRShape;
    QRLabel4: TQRLabel;
    QRLabel9: TQRLabel;
    QRLabel10: TQRLabel;
    QRLabel11: TQRLabel;
    QRLabel12: TQRLabel;
    QRLabel13: TQRLabel;
    QRLabel14: TQRLabel;
    QRLabel19: TQRLabel;
    QRLabel20: TQRLabel;
    QRLabel22: TQRLabel;
    QRLabel23: TQRLabel;
    QRLabel24: TQRLabel;
    QRLabel25: TQRLabel;
    QRDBText1: TQRDBText;
    QRShape2: TQRShape;
    QRShape9: TQRShape;
    QRShape10: TQRShape;
    QRShape11: TQRShape;
    QRShape12: TQRShape;
    QRShape13: TQRShape;
    QRShape14: TQRShape;
    QRDBText3: TQRDBText;
    QRDBText4: TQRDBText;
    QRDBText5: TQRDBText;
    QRDBText6: TQRDBText;
    QRLabel5: TQRLabel;
    QRLabel18: TQRLabel;
    QRLabel21: TQRLabel;
    QRLabel26: TQRLabel;
    QRLabel27: TQRLabel;
    Query1: TADOQuery;
    Query1JA_KOD: TStringField;
    Query1JA_JARAT: TStringField;
    Query1JA_JKEZD: TStringField;
    Query1JA_JVEGE: TStringField;
    Query1JA_RENDSZ: TStringField;
    Query1JA_MENSZ: TStringField;
    Query1JA_ORSZ: TStringField;
    Query1JA_ALKOD: TFloatField;
    Query1DOKOD: TStringField;
    Query1DONAME: TStringField;
    Query2: TADOQuery;
    procedure FormCreate(Sender: TObject);
    procedure	Tolt(rsz, dat1, dat2 : string);
    procedure QRBand3BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBand1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure Query1CalcFields(DataSet: TDataset);
    procedure QRBand2BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
  private
  public
     vanadat			: boolean;
  end;

var
  MenetlisDlg: TMenetlisDlg;

implementation

{$R *.DFM}

procedure TMenetlisDlg.FormCreate(Sender: TObject);
begin
	EgyebDlg.SetADOQueryDatabase(Query1);
	EgyebDlg.SetADOQueryDatabase(Query2);
  	EllenListTorol;
end;

procedure	TMenetlisDlg.Tolt(rsz, dat1, dat2 : string);
var
  sqlstr	: string;
  kezdat	: string;
  vegdat	: string;
begin
  	if rsz = '' then begin
  		QRLabel26.Caption := '';
  		sqlstr	:= 'SELECT * FROM JARAT WHERE JA_RENDSZ LIKE ''%'' ';
  	end else begin
  		QRLabel26.Caption := rsz;
  		sqlstr	:= 'SELECT * FROM JARAT WHERE JA_RENDSZ = '''+	rsz +''' ';
  	end;

 	QRLabel27.Caption := dat1+' - '+dat2;
  	kezdat := dat1;
  	if kezdat = '' then begin
  		kezdat := '1990.01.01';
  	end;
  	vegdat := dat2;
  	if vegdat = '' then begin
  		vegdat := EgyebDlg.MaiDatum;
  	end;
  	sqlstr := sqlstr + ' AND JA_JKEZD >= '''+kezdat+''' AND JA_JKEZD <= '''+vegdat+''' ';
  	sqlstr := sqlstr + ' ORDER BY JA_MENSZ,JA_JKEZD';
	{A kapott sql alapján végrehajtja a lekérdezést}
  	Query_Run(Query1, sqlstr );
  	vanadat := Query1.RecordCount > 0;

end;

procedure TMenetlisDlg.QRBand3BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
begin
  	QrLabel16.Caption 	:= EgyebDlg.Read_SZGrid('CEG', '100');
  	QrLabel17.Caption 	:= EgyebDlg.Read_SZGrid('CEG', '102');
end;

procedure TMenetlisDlg.QRBand1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
begin
 	JaratEllenor( Query1.FieldByname('JA_KOD').AsString );
	QRLabel5.Caption := Query1.FieldByname('JA_ORSZ').AsString + '-' + Query1.FieldByname('JA_ALKOD').AsString;
  	if QRLabel5.Caption = '-' then begin
  		QRLabel5.Caption := Query1.FieldByname('JA_JARAT').AsString;
  	end;
end;

procedure TMenetlisDlg.Query1CalcFields(DataSet: TDataset);
begin
	Query1DOKOD.AsString	:= '';
	Query_Run(Query2, 'SELECT * FROM JARSOFOR WHERE JS_JAKOD = '''+Query1.FieldByname('JA_KOD').AsString+''' ORDER BY JS_SORSZ ');
   if Query2.RecordCount > 0 then begin
		Query1DOKOD.AsString	:= Query2.FieldByname('JS_SOFOR').AsString;
   end;
   Query1DONAME.AsString	:= '';
   if Query1DOKOD.AsString <> '' then begin
  		Query1DONAME.AsString	:= Query_Select('DOLGOZO', 'DO_KOD', Query1DOKOD.AsString , 'DO_NAME');
   end;
end;

procedure TMenetlisDlg.QRBand2BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
begin
	EllenListMutat('Nem ellenőrzött járatok', true);
end;

end.
