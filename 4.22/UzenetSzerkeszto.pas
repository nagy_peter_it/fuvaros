﻿unit UzenetSzerkeszto;

interface

uses WinTypes, WinProcs, Classes, Graphics, Forms, Controls, Buttons, Messages,
	StdCtrls, ExtCtrls, Sysutils, Vcl.Mask, Vcl.Grids, Vcl.ValEdit, Generics.Collections,
  Vcl.Menus, Data.DBXJSON, KozosTipusok;

type
  TCimzettVagyCC = (cimzett, cc);
  TMyPopupAction= procedure(Sender: TObject);

type
	TUzenetSzerkesztoDlg = class(TForm)
	Panel1: TPanel;
    Memo1: TMemo;
    Label8: TLabel;
    GridPanel2: TGridPanel;
    BitBtn1: TBitBtn;
    BitBtn3: TBitBtn;
    M33: TMaskEdit;
    M34: TMaskEdit;
    BitBtn4: TBitBtn;
    pnlMessageInfo: TPanel;
    BitBtn2: TBitBtn;
    ppmCimzettek: TPopupMenu;
    ppmCsoportok: TPopupMenu;
    BitBtn6: TBitBtn;
    Panel2: TPanel;
    Label1: TLabel;
    BitBtn7: TBitBtn;
    BitBtn8: TBitBtn;
    BitBtn10: TBitBtn;
    ppmCCk: TPopupMenu;
    ppmCsoportokCC: TPopupMenu;
    chkForceSMS: TCheckBox;
    ScrollBox1: TScrollBox;
    fpCimzett: TFlowPanel;
    ScrollBox2: TScrollBox;
    fpCC: TFlowPanel;
    pnlTargy: TPanel;
    Label2: TLabel;
    meTargy: TMaskEdit;
    pnlValaszSzam: TPanel;
    Label3: TLabel;
    cbValaszSzam: TComboBox;
    chkValaszDefault: TCheckBox;
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure CimzettLista_Frissites;
    procedure CCLista_Frissites;
    // procedure CimzettLista_Frissites(CimzettVagyCC: TCimzettVagyCC);
    // procedure CimzettLista_Frissit(EditBox: TMemo; PopupM: TPopupMenu; MyPopupAction: TMyPopupAction;
    //    MyDict: TDictionary<string,string>; MyArray: TArray<string>);
    function GetCimzettek(Forma: string; CimzettVagyCC: TCimzettVagyCC): string;
    procedure RefreshUzenetMeret;
    procedure Memo1KeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure DropMenuDown(Control: TControl; PopupMenu: TPopupMenu);
    procedure BitBtn5MouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure CimzettTorlesClick(Sender: TObject);
    procedure CCTorlesClick(Sender: TObject);
    procedure CsoportHozzaadClick(Sender: TObject);
    // procedure CsoportCCHozzaadClick(Sender: TObject);
    procedure SelectPartner(CimzettVagyCC: TCimzettVagyCC);
    procedure SelectDolgozo(CimzettVagyCC: TCimzettVagyCC);
    procedure BitBtn4Click(Sender: TObject);
    procedure BitBtn6Click(Sender: TObject);
    procedure StoreSentSMS(CimzettLista, Szoveg: string);
    procedure BitBtn7Click(Sender: TObject);
    procedure BitBtn10Click(Sender: TObject);
    procedure BitBtn8Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ValaszSzamToltes;
    procedure Button1Click(Sender: TObject);
    procedure cbValaszSzamChange(Sender: TObject);
	private
    Cimzettek, CCk, Cimzett_Nevek, Cc_Nevek, Csoportok, KifejtettCsoportokKontakt, KifejtettCsoportokEagleKontakt: TDictionary<string,string>;
    KifejtettCsoportokCimke: TDictionary<string,string>;
    CimzettekArray, CsoportokArray : TArray<string>;  // a sorrendezéshez
    ValaszSzamLista: TStringList;
    cMenuClosed: Cardinal;
    CsoportListaToltve: boolean;
    function Kuldes: boolean;
    procedure CsoportKifejt(GKKAT: string; LegyenAlert: boolean);
	public
    VoltKuldes: boolean;  // történt-e végül küldés, vagy "Mégse" volt (kell-e tárolni)
    KuldottSzoveg: string; // a végleges, elküldött szöveg
    KuldottCimzettek: string;  // a címzettek végleges listája
    EagleSMSTipus: TEagleSMSTipus;
    procedure AddDolgozoCimzett(DOKOD: string; CimzettVagyCC: TCimzettVagyCC; ForceSMS: boolean);
    procedure AddPartnerCimzett(VEKOD: string; CimzettVagyCC: TCimzettVagyCC);
    procedure AddCimzett(Kontakt, Nev: string; CimzettVagyCC: TCimzettVagyCC);
    procedure AddCimzettCsoport(Megjelenitett_nev: string; CimzettVagyCC: TCimzettVagyCC);
    procedure SetSzoveg(Szoveg: string);
    procedure AddCimzettPanelElem(Sorszam: integer; MelyikPanel: TFlowPanel; DisplayName: string);
    procedure CsoportListaTolt;
    procedure Reset;
	end;

const
  C_Csoportkod = 'Csoport:';
  C_MindenSoforCsoport = 'Mindenki';
  ValaszMinta = '[Válasz: %s] ';

var
	UzenetSzerkesztoDlg: TUzenetSzerkesztoDlg;

implementation
uses
	Egyeb, J_VALASZTO, Math, SMSKuld, J_SQL, Kozos, LGauge;

{$R *.DFM}

procedure TUzenetSzerkesztoDlg.BitBtn10Click(Sender: TObject);
begin
   SelectPartner(cc);
   chkForceSMS.Checked:= False;
end;

procedure TUzenetSzerkesztoDlg.BitBtn1Click(Sender: TObject);
begin
   if Kuldes then begin
      ModalResult:= mrOK;
      Close;
      end;
end;

function TUzenetSzerkesztoDlg.Kuldes: boolean;
var
  CimzettekJSON, CimzettekLista, CCLista, Szoveg: string;
  APIKEY, APIPassword, SendingPhone, KuldesHibaKod, emailcim, cccim, targy, ValaszSzam: string;
  Siker: boolean;
  CimzettekSL, CCSL, UresSL: TStringList;
begin
   Szoveg:= trim(Memo1.Text);
   if EgyebDlg.UZENET_VALASZ_SZAM then begin
     ValaszSzam:= ValaszSzamLista[cbValaszSzam.ItemIndex];
     if chkValaszDefault.Checked then begin
        EgyebDlg.Write_Szotar('156', EgyebDlg.user_code, ValaszSzam);
        end;
     if (cbValaszSzam.ItemIndex >0) then  // ha van kiválasztva valami
       Szoveg:= Format(ValaszMinta, [ValaszSzam]) + Szoveg;
     end;
   chkValaszDefault.Checked:= False;
   Szoveg:= EgyebDlg.SzovegFilter(Szoveg, 'szám_per_szám');
   if Szoveg = '' then begin
      NoticeKi ('Üres üzenetet nem küldünk!');
      Result:= False;
      Exit;
      end;  // if
   if fpCimzett.ControlCount = 0 then begin
      NoticeKi ('Nincs címzett megadva!');
      Result:= False;
      Exit;
      end;  // if

   if EgyebDlg.SMS_KULDES_FORMA='MYSMS' then begin
       CimzettekJSON:= GetCimzettek('MYSMS', cimzett);
       CimzettekJSON:= EgyebDlg.JSONArray_DuplikacioTorol(CimzettekJSON); // kiveszi, pl. ha csoporttal is benne van egy szám és egyéniként is
       Screen.Cursor	:= crHourGlass;
       KuldesHibaKod:= SMSKuldDlg.SendMySms(CimzettekJSON, Szoveg);
       Screen.Cursor	:= crDefault;
       if (KuldesHibaKod = '' ) then begin
         VoltKuldes:= True;
         KuldottSzoveg:= Szoveg;
         // KuldottCimzettek:= ebCimzett.Text;
         KuldottCimzettek:= GetCimzettek('MYSMS', cimzett);
         NoticeKi('Üzenet elküldve.');
         Result:= True;
         end
       else begin
         NoticeKi('Sikertelen küldés! Hibakód:'+KuldesHibaKod);
         HibaKiiro('Sikertelen SMS küldés! Címzett: '+CimzettekJSON+' Hibakód: '+KuldesHibaKod+' Szöveg: '+Szoveg);
         Result:= False;
         end;
       end;
   if EgyebDlg.SMS_KULDES_FORMA='SMSEAGLE' then begin
      emailcim:= GetCimzettek('SMSEAGLE', cimzett);
      emailcim:= EgyebDlg.StringLista_DuplikacioTorol(emailcim, ';'); // kiveszi, pl. ha csoporttal is benne van egy szám és egyéniként is
      cccim:= GetCimzettek('SMSEAGLE', cc);
      cccim:= EgyebDlg.StringLista_DuplikacioTorol(cccim, ';');
      targy:= meTargy.Text;

      if (Length(emailcim)+Length(cccim)+Length(Szoveg))> EgyebDlg.SMS_DIRECT_SENDING_LIMIT then begin
          CimzettekSL:=TStringList.Create;
          CCSL:=TStringList.Create;
          UresSL:=TStringList.Create;
          try
            Seplist2TStringList(emailcim, ';', CimzettekSL);
            Seplist2TStringList(cccim, ';', CCSL);
            if EgyebDlg.SMS_DIRECT_SENDING_IMMEDIATE then begin
               if NoticeKi('Biztosan elküldi az üzenetet?', NOT_QUESTION) <> 0 then
                  exit; // procedure
                end;
            Siker:= SendOutlookHTMLMmail(CimzettekSL, CCSL, UresSL, targy, Szoveg, EgyebDlg.SMS_DIRECT_SENDING_IMMEDIATE, EagleSMSTipus);  // először csak jelenítse meg
            if Siker then begin
              if EgyebDlg.SMS_DIRECT_SENDING_IMMEDIATE then
                  NoticeKi('Üzenet elküldve.');
              VoltKuldes:= True;
              Result:= True;
              end
            else begin   // nincs siker
            if EgyebDlg.SMS_DIRECT_SENDING_IMMEDIATE then begin
                  NoticeKi('Sikertelen üzenetküldés!!!');
                  HibaKiiro('Sikertelen üzenetküldés!!! Címzettek: '+emailcim+' Tárgy: '+targy+' Szöveg: '+Szoveg);
                  end;
              VoltKuldes:= False;
              Result:= False;
              end;
          finally
            if CimzettekSL <> nil then CimzettekSL.Free;
            if CCSL <> nil then CCSL.Free;
            if UresSL <> nil then UresSL.Free;
            end; // try-finally
          end
      else begin  // nem túl hosszú
        cccim:= StringReplace(cccim, '"', '%22', [rfReplaceAll]); // a cc részt a MailTo-nál már URL-enkódolni kell
        if cccim='' then SendDirectEmail(emailcim, 'SMS', Szoveg)
        else SendDirectEmailCC(emailcim, cccim, 'SMS', Szoveg);
        VoltKuldes:= True;
        Result:= True;
        end;

      if VoltKuldes then begin
          Screen.Cursor	:= crHourGlass;
          // tárolás az adatbázisban: címzettek
          CimzettekLista:= GetCimzettek('MYSMS', cimzett);  // csak telefonszám listát szeretnék. Ez itt nem JSON, mert nincsenek idézőjelek az elemek körül.
          CimzettekLista:= StringReplace(CimzettekLista, '[', '', [rfReplaceAll]);
          CimzettekLista:= StringReplace(CimzettekLista, ']', '', [rfReplaceAll]);
          CimzettekLista:= EgyebDlg.StringLista_DuplikacioTorol(CimzettekLista, ','); // kiveszi, pl. ha csoporttal is benne van egy szám és egyéniként is
          StoreSentSMS(CimzettekLista, Szoveg);

          KuldottCimzettek:= CimzettekLista;
          KuldottSzoveg:= Szoveg;

          // tárolás az adatbázisban: cc
          CCLista:= GetCimzettek('MYSMS', cc);  // csak telefonszám listát szeretnék. Ez itt nem JSON, mert nincsenek idézőjelek az elemek körül.
          CCLista:= StringReplace(CCLista, '[', '', [rfReplaceAll]);
          CCLista:= StringReplace(CCLista, ']', '', [rfReplaceAll]);
          CCLista:= EgyebDlg.StringLista_DuplikacioTorol(CCLista, ','); // kiveszi, pl. ha csoporttal is benne van egy szám és egyéniként is
          StoreSentSMS(CCLista, Szoveg);
          Screen.Cursor	:= crDefault;
          end;  // if voltkuldes
      end;
end;

procedure TUzenetSzerkesztoDlg.StoreSentSMS(CimzettLista, Szoveg: string);
var
   Telefon, Lista, Elem, S: string;
   i: integer;
begin
   Lista:= CimzettLista;
   while Lista <> '' do begin
      Telefon:= Trim(EgyebDlg.SepFrontToken(',', Lista));
      Lista:= Trim(EgyebDlg.SepRest(',', Lista));
      StoreOneSMS(Telefon, Szoveg);
      end; // while
end;

procedure TUzenetSzerkesztoDlg.BitBtn3Click(Sender: TObject);
begin
 SelectDolgozo(cimzett);
 chkForceSMS.Checked:= False;
end;

procedure TUzenetSzerkesztoDlg.BitBtn4Click(Sender: TObject);
begin
  if not CsoportListaToltve then
     // CsoportListaTolt; -- elvileg nincs csoport küldés itt
  DropMenuDown(BitBtn4, ppmCsoportok);
  cMenuClosed := GetTickCount;
end;

procedure TUzenetSzerkesztoDlg.ValaszSzamToltes;
var
  DOKOD, TEID, S, Telszam, Megje, ValaszSzam: string;
  i: integer;
begin
  ValaszSzamLista:= TStringList.Create;
  DOKOD:= EgyebDlg.user_dokod;  // bejelentkezéskor kitöltjük, JE_DOKOD
  cbValaszSzam.Items.Clear;
  cbValaszSzam.Items.Add('');  // 0. elem: nincs kiválasztva semmi
  ValaszSzamLista.Add('');
  Telszam:= EgyebDlg.GetDolgozoSMSKontakt(DOKOD);
  cbValaszSzam.Items.Add(Telszam+ ' (Saját szám)');
  ValaszSzamLista.Add(Telszam);
  S:= 'select DT_TEID from DOVALTEL where convert(int, DT_DOKOD)='+ DOKOD;
  Query_Run(EgyebDlg.Query1, S, True);
  while not EgyebDlg.Query1.Eof do begin
    TEID:= trim(EgyebDlg.Query1.FieldByName('DT_TEID').AsString);
    S:= 'select TE_TELSZAM, TE_MEGJE from TELELOFIZETES where TE_ID='+ TEID;
    Query_Run(EgyebDlg.Query2, S, True);
    if not EgyebDlg.Query2.Eof then begin
      Telszam:= EgyebDlg.Query2.FieldByName('TE_TELSZAM').AsString;
      Megje:= EgyebDlg.Query2.FieldByName('TE_MEGJE').AsString;
      cbValaszSzam.Items.Add(Telszam+ ' ('+ Megje+')');
      ValaszSzamLista.Add(Telszam);
      end;  // if
    EgyebDlg.Query1.Next;
    end;  // while
  if cbValaszSzam.Items.Count>=2 then begin
      // cbValaszSzam.ItemIndex:= 1;  // ez a "Saját szám"
      ValaszSzam:= EgyebDlg.Read_Szotar('156', EgyebDlg.user_code);
      if ValaszSzam <>'' then begin
        for i:=0 to ValaszSzamLista.Count-1 do begin
          if ValaszSzamLista[i]=ValaszSzam then begin
            cbValaszSzam.ItemIndex:=i;
            Break; // found, exit loop
            end;  // if
          end;  // for
        end  // if
      else begin
          cbValaszSzam.ItemIndex:= 1;  // ez a "Saját szám"
          end;
      end;
end;

procedure TUzenetSzerkesztoDlg.AddDolgozoCimzett(DOKOD: string; CimzettVagyCC: TCimzettVagyCC; ForceSMS: boolean);
var
  DONAME, Megjelenitett_nev, kontakt, icloudkontakt: string;
begin
  DONAME:= Query_Select('DOLGOZO', 'DO_KOD', DOKOD, 'DO_NAME');
  kontakt:= EgyebDlg.GetDolgozoKontakt(DOKOD, ForceSMS);
  AddCimzett(kontakt, DONAME, CimzettVagyCC);
  if EgyebDlg.SMS_MASOLAT_ICLOUD_HASZNALATA then begin
      icloudkontakt:= EgyebDlg.GetDolgozoICloudKontakt(DOKOD);
      if icloudkontakt <> '' then
          AddCimzett(icloudkontakt, DONAME, CimzettVagyCC);
      end;
end;


procedure TUzenetSzerkesztoDlg.AddPartnerCimzett(VEKOD: string; CimzettVagyCC: TCimzettVagyCC);
var
  V_NEV, Megjelenitett_nev, emailkontakt, smskontakt: string;
begin
  V_NEV:= Query_Select('VEVO', 'V_KOD', VEKOD, 'V_NEV');
  if not chkForceSMS.Checked then begin
     emailkontakt:= trim(EgyebDlg.GetPartnerEmailKontakt(VEKOD));
     end
  else begin
     emailkontakt:= '';
     end;
  if EgyebDlg.ErvenyesEmailKontakt(emailkontakt) then begin  // az email prioritást élvez (ingyenes)
      AddCimzett(emailkontakt, V_NEV, CimzettVagyCC);
      end
  else begin
      smskontakt:= trim(EgyebDlg.GetPartnerSMSKontakt(VEKOD));  // DOKOD-ból
      AddCimzett(smskontakt, V_NEV, CimzettVagyCC);
      end;  // else

end;

procedure TUzenetSzerkesztoDlg.AddCimzett(Kontakt, Nev: string; CimzettVagyCC: TCimzettVagyCC);
var
  Megjelenitett_nev: string;
begin
  if EgyebDlg.SMS_KULDES_FORMA='SMSEAGLE' then begin
      kontakt:=StringReplace(kontakt, '"','', [rfReplaceAll]);
      end;
  if ((EgyebDlg.SMS_KULDES_FORMA='MYSMS') and (Length(kontakt) >= 14) and (copy(kontakt,1,2) = '"+'))
   or ((EgyebDlg.SMS_KULDES_FORMA='SMSEAGLE') and (EgyebDlg.ErvenyesSMSEagleKontakt(kontakt) or EgyebDlg.ErvenyesEmailKontakt(kontakt))) then begin
    Megjelenitett_nev:= Nev+ ' <'+kontakt+'>';
    if Cimzettek.ContainsKey(Megjelenitett_nev) then begin
       NoticeKi('Már szerepel a címzettek között!');
       Exit;
       end;
    if CCk.ContainsKey(Megjelenitett_nev) then begin
       NoticeKi('Már szerepel a másolatot kapók között!');
       Exit;
       end;
    if CimzettVagyCC = cimzett then begin
       Cimzettek.Add(Megjelenitett_nev, kontakt);
       Cimzett_Nevek.Add(Megjelenitett_nev, Nev);  // ez is kell később
       CimzettLista_Frissites;
       end;
    if CimzettVagyCC = cc then begin
       CCk.Add(Megjelenitett_nev, kontakt);
       Cc_Nevek.Add(Megjelenitett_nev, Nev);  // ez is kell később
       CCLista_Frissites;
       end;
    end
  else begin
    NoticeKi('A kiválasztott címzetthez ('+Nev+') nincs érvényes elérhetőség beállítva! Jelenlegi beállítás: '+kontakt);
    end;
end;

procedure TUzenetSzerkesztoDlg.AddCimzettCsoport(Megjelenitett_nev: string; CimzettVagyCC: TCimzettVagyCC);
begin
  if Cimzettek.ContainsKey(Megjelenitett_nev) then begin
     NoticeKi('Már szerepel a címzettek között!');
     Exit;
     end;
  if CCk.ContainsKey(Megjelenitett_nev) then begin
     NoticeKi('Már szerepel a másolatot kapók között!');
     Exit;
     end;
  if CimzettVagyCC = cimzett then begin
     Cimzettek.Add(Megjelenitett_nev, Csoportok[Megjelenitett_nev]);
     CimzettLista_Frissites;
     end;
  if CimzettVagyCC = cc then begin
     CCk.Add(Megjelenitett_nev, Csoportok[Megjelenitett_nev]);
     CCLista_Frissites;
     end;
end;

procedure TUzenetSzerkesztoDlg.SetSzoveg(Szoveg: string);
var
  Felado: string;
begin
  Felado := '['+EgyebDlg.user_name+']: ';    // pl. [Ágh Viktor]:
  Memo1.Text:= Felado + Szoveg;
  RefreshUzenetMeret;
end;


procedure TUzenetSzerkesztoDlg.DropMenuDown(Control: TControl; PopupMenu: TPopupMenu);
var
  APoint: TPoint;
begin
  APoint := Control.ClientToScreen(Point(0, Control.ClientHeight));
  PopupMenu.Popup(APoint.X, APoint.Y);
end;

procedure TUzenetSzerkesztoDlg.BitBtn5MouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if (Button = mbLeft) and not ((cMenuClosed + 100) < GetTickCount) then begin
    ReleaseCapture;
    end; // if
end;

procedure TUzenetSzerkesztoDlg.BitBtn6Click(Sender: TObject);
begin
   SelectPartner(cimzett);
   chkForceSMS.Checked:= False;
end;

procedure TUzenetSzerkesztoDlg.BitBtn7Click(Sender: TObject);
begin
  SelectDolgozo(cc);
  chkForceSMS.Checked:= False;
end;

procedure TUzenetSzerkesztoDlg.BitBtn8Click(Sender: TObject);
begin
  if not CsoportListaToltve then
     // CsoportListaTolt;   -- elvileg nincs csoport küldés itt
  DropMenuDown(BitBtn8, ppmCsoportokCC);
  cMenuClosed := GetTickCount;
end;

procedure TUzenetSzerkesztoDlg.Button1Click(Sender: TObject);
begin
    fpCimzett.Refresh;  //
end;

procedure TUzenetSzerkesztoDlg.cbValaszSzamChange(Sender: TObject);
begin
   RefreshUzenetMeret;
end;

procedure TUzenetSzerkesztoDlg.SelectDolgozo(CimzettVagyCC: TCimzettVagyCC);
var
  DOKOD, Lista: string;
  ForceSMS: boolean;
begin
	TobbDolgozoValaszto2(M33, M34, True); // archív dolgozók nem kellenek
  ForceSMS:= chkForceSMS.Checked;
  if (M33.Text<>'') then begin
    Lista:= M33.Text;
    while Lista <> '' do begin
      DOKOD:= Trim(EgyebDlg.SepFrontToken(',', Lista));
      Lista:= Trim(EgyebDlg.SepRest(',', Lista));
      AddDolgozoCimzett(DOKOD, CimzettVagyCC, ForceSMS);
      end; // while
    end;  // if
end;

procedure TUzenetSzerkesztoDlg.SelectPartner(CimzettVagyCC: TCimzettVagyCC);
var
  VEKOD, Lista: string;
begin
	TobbPartnerValaszto(M33, M34);
  if (M33.Text<>'') then begin
    Lista:= M33.Text;
    while Lista <> '' do begin
      VEKOD:= Trim(EgyebDlg.SepFrontToken(',', Lista));
      Lista:= Trim(EgyebDlg.SepRest(',', Lista));
      AddPartnerCimzett(VEKOD, CimzettVagyCC);
      end; // while
    end;  // if
end;

procedure TUzenetSzerkesztoDlg.CimzettLista_Frissites;
var
   KeyName, S: string;
   i: integer;
begin
  while fpCimzett.ControlCount > 0 do fpCimzett.Controls[0].Free;  // az összes al-panel törlése
  i:=0;
  CimzettekArray := Cimzettek.Keys.ToArray;
  TArray.Sort<string>(CimzettekArray);
  // try
    // fpCimzett.Visible:= False; // hogy ne villogjon, ha sok elemet adunk hozzá
    for KeyName in CimzettekArray do begin
       AddCimzettPanelElem(i, fpCimzett, KeyName);
       Inc(i);
       end;  // for
  // finally
    // fpCimzett.Visible:= True;
   // ScrollBox1.Repaint;
   // fpCimzett.Visible:= True;
   // fpCimzett.Repaint;
  //  end;  // try-finally
  if (fpCimzett.ControlCount > 0) and (fpCimzett.Height=0) then
     fpCimzett.Height:=30;  // workaround: ha pl. a dolgozó adatlapról adjuk hozzá a címzettet, 0 lesz a magassága, ezért nem látszik.

end;

procedure TUzenetSzerkesztoDlg.CCLista_Frissites;
var
   KeyName, S: string;
   i: integer;
begin
  while fpCC.ControlCount > 0 do fpCC.Controls[0].Free;  // az összes al-panel törlése
  i:=1000;  // a CC elemek 1001-től kezdődnek
  CimzettekArray := CCk.Keys.ToArray;
  TArray.Sort<string>(CimzettekArray);
  for KeyName in CimzettekArray do begin
     AddCimzettPanelElem(i, fpCC, KeyName);
     Inc(i);
     end;  // for
  // ScrollBox2.Repaint;
  fpCC.Repaint;
end;


procedure TUzenetSzerkesztoDlg.AddCimzettPanelElem(Sorszam: integer; MelyikPanel: TFlowPanel; DisplayName: string);
var
  Elem: TFlowPanel;
  MyLabel: TLabel;
begin
   Elem:= TFlowPanel.Create(MelyikPanel);
   with Elem do begin
      Parent:= MelyikPanel;
      Visible:= True;
      AutoSize:= True;
      AutoWrap:= False;
      BevelInner:= bvNone;
      BevelKind:= bkNone;
      BevelOuter:= bvNone;
      BorderStyle:= bsNone;
      end;
   MyLabel:= TLabel.Create(Elem);
   with MyLabel do begin
      Parent:= Elem;
      Visible:= True;
      Caption:= ' '+DisplayName; // egy kis margó kell
      Font.Size:=10;
      end;
   with TButton.Create(Elem) do begin
      Parent:= Elem;
      Visible:= True;
      Font.Size:= 8;
      Caption:= 'x';
      Height:=15;
      Width:=15;
      Tag:=Sorszam;  // később az OnClick-hez
      OnClick:= CimzettTorlesClick;
      end;
   with TLabel.Create(Elem) do begin
      Parent:= Elem;
      Caption:= ' '; // egy kis margó ide is
      Font.Size:=10;
      end;
end;

procedure TUzenetSzerkesztoDlg.CsoportListaTolt;
var
   KeyName, S, GKKAT, CsoportKod, Megjelenitett_nev: string;
   cimkelista, Dolgozo_cimke: string;
   i, GaugePos: integer;
begin
  if CsoportListaToltve then begin
      Exit;  // procedure
      end;
  Application.CreateForm(TFormGaugeDlg, FormGaugeDlg);
  try
      FormGaugeDlg.GaugeNyit('Csoportok gyűjtése... ' , '', false);
      S:= 'select distinct GK_GKKAT from dolgozo, gepkocsi where DO_RENDSZ = GK_RESZ '+
           'and DO_RENDSZ<> '''' order by 1';
      Query_Run(EgyebDlg.Query1, S, True);
      FormGaugeDlg.SetMax(EgyebDlg.Query1.RecordCount * 3); // 2 menet lesz, de arányosítunk, az első menet dupla lépésben megy
      GaugePos:=0;
      while not EgyebDlg.Query1.Eof do begin
        GKKAT:= trim(EgyebDlg.Query1.FieldByName('GK_GKKAT').AsString);
        if GKKAT <> '' then begin
          Megjelenitett_nev:= '[Tgk_'+GKKAT+']';
          CsoportKod:= C_Csoportkod+GKKAT;
          Csoportok.Add(Megjelenitett_nev, CsoportKod);
          Query_Log_Str('CsoportKifejt '+GKKAT, 0, false, false);
          CsoportKifejt(GKKAT, false);
          Query_Log_Str('CsoportKifejt '+GKKAT+' OK', 0, false, false);
          end;  // if
         GaugePos:= GaugePos+2; // mivel ez lassabb, így arányosabb lesz a csíkhúzás
         FormGaugeDlg.Pozicio (GaugePos);
         EgyebDlg.Query1.Next;
         end;  // while
      // az "Összes gépkocsivezető" csoport
      GKKAT:= C_MindenSoforCsoport;
      Megjelenitett_nev:= '[Tgk_'+GKKAT+']';
      CsoportKod:= C_Csoportkod+GKKAT;
      Csoportok.Add(Megjelenitett_nev, CsoportKod);
      CsoportKifejt(GKKAT, false);


      // Azért kell külön ciklusban, mert a TDict nincs sorrendezve, és így lesz szinkronban
      // a TDict és a Popupmenu.
      ppmCsoportok.Items.Clear;
      ppmCsoportokCC.Items.Clear;
      i:=0;
      CsoportokArray := Csoportok.Keys.ToArray;
      TArray.Sort<string>(CsoportokArray);
      for KeyName in CsoportokArray do begin
         // popumenu a hozzáadáshoz
          Query_Log_Str('KeyName feldolgozás: '+KeyName, 0, false, false);
          ppmCsoportok.Items.Add(TMenuItem.Create(ppmCsoportok));
          with ppmCsoportok.Items[ppmCsoportok.Items.Count-1] do begin
              Caption:= KeyName;
              Tag:=i;  // később az OnClick-hez
              OnClick:= CsoportHozzaadClick;
              Hint:= KifejtettCsoportokCimke[KeyName];
              end;  // with

          ppmCsoportokCC.Items.Add(TMenuItem.Create(ppmCsoportokCC));
          with ppmCsoportokCC.Items[ppmCsoportokCC.Items.Count-1] do begin
              Caption:= KeyName;
              Tag:=i;  // később az OnClick-hez
              // OnClick:= CsoportCCHozzaadClick;
              OnClick:= CsoportHozzaadClick; // majd az Owner-ből tudni fogja hogy melyik menüből hívták
              Hint:= KifejtettCsoportokCimke[KeyName];
              end;  // with
         Inc(GaugePos);
         FormGaugeDlg.Pozicio (GaugePos);
         Inc(i);
         end;  // for
      cMenuClosed := 0;
      CsoportListaToltve:= True;
  finally
     if FormGaugeDlg <> nil then FormGaugeDlg.Destroy;
     end;  // try-finally

end;

procedure TUzenetSzerkesztoDlg.CsoportKifejt(GKKAT: string; LegyenAlert: boolean);
var
  S, kontaktlista, eaglekontaktlista, cimkelista, Megjelenitett_nev, CsoportKod: string;
  DONAME, DOKOD, kontakt, eaglekontakt, Dolgozo_cimke, NincsKontaktja: string;
  sofor_FEOR, sofor_FEOR_kivetelek, icloudkontakt, CsoportCCLista: string;
  ForceSMS: boolean;
begin
    Megjelenitett_nev:= '[Tgk_'+GKKAT+']';
    CsoportKod:= C_Csoportkod+GKKAT;
    if GKKAT = C_MindenSoforCsoport then begin  // az összes gépkocsivezető
      sofor_FEOR:= EgyebDlg.Read_SZGrid('147', '1');
      sofor_FEOR_kivetelek:= EgyebDlg.Read_SZGrid('147', '2');
      if sofor_FEOR='' then exit;  // ha nincs beállítva, egyáltalán ne legyen ilyen csoport
      S:= 'select distinct DO_NAME, DO_KOD from dolgozo where DO_DFEOR like ''%'+sofor_FEOR+'%'''+
       ' and DO_KOD not in ('+sofor_FEOR_kivetelek+') '+
       ' and DO_ARHIV=0  ';
      end
    else begin  // tényleg gépkocsi kategória
      S:= 'select distinct DO_NAME, DO_KOD from dolgozo, gepkocsi where DO_RENDSZ = GK_RESZ '+
       'and DO_RENDSZ<> '''' and GK_GKKAT = '''+GKKAT+''' and DO_ARHIV=0 ';
      // 2018-02-02: csoportonkénti "CC" is lehet
      CsoportCCLista:= EgyebDlg.Read_SZGrid('341', GKKAT);
      if trim(CsoportCCLista) <> '' then
         S:= S+' union select distinct DO_NAME, DO_KOD from dolgozo where DO_KOD in ('+CsoportCCLista+ ') ';
      end;
    Query_Run(EgyebDlg.Query2, S, True);
    kontaktlista:='';
    eaglekontaktlista:='';
    cimkelista:='';
    NincsKontaktja:= '';
    ForceSMS:= chkForceSMS.Checked;
    while not EgyebDlg.Query2.Eof do begin
      DOKOD:= trim(EgyebDlg.Query2.FieldByName('DO_KOD').AsString);
      DONAME:= trim(EgyebDlg.Query2.FieldByName('DO_NAME').AsString);
      // Query_Log_Str('..GetDolgozoKontakt '+DOKOD, 0, false, false);
      kontakt:= trim(EgyebDlg.GetDolgozoKontakt(DOKOD, ForceSMS));  // DOKOD-ból
      if kontakt <> '' then begin
        // Query_Log_Str('..GetDolgozoSMSEagleCimzett '+DOKOD, 0, false, false);
        eaglekontakt:= EgyebDlg.GetDolgozoSMSEagleCimzett(DOKOD, ForceSMS);
        eaglekontaktlista:= Felsorolashoz(eaglekontaktlista, eaglekontakt, ' ; ', False);

        icloudkontakt:= '';
        if EgyebDlg.SMS_MASOLAT_ICLOUD_HASZNALATA then begin  // iCloud mail másolat
          // Query_Log_Str('..GetDolgozoICloudKontakt '+DOKOD, 0, false, false);
          icloudkontakt:= EgyebDlg.GetDolgozoICloudKontakt(DOKOD);
          if icloudkontakt <> '' then
            eaglekontaktlista:= Felsorolashoz(eaglekontaktlista, icloudkontakt, ' ; ', False);
        end;
        // Query_Log_Str('..Rest... '+DOKOD, 0, false, false);
        if ((EgyebDlg.SMS_KULDES_FORMA='MYSMS') and (Length(kontakt) >= 14) and (copy(kontakt,1,2) = '"+'))
         or ((EgyebDlg.SMS_KULDES_FORMA='SMSEAGLE') and (EgyebDlg.ErvenyesSMSEagleKontakt(kontakt) or EgyebDlg.ErvenyesEmailKontakt(kontakt))) then begin
          Dolgozo_cimke:= DONAME+ ' <'+kontakt+'>';
          cimkelista:= Felsorolashoz(cimkelista, Dolgozo_cimke, ' ; ', False);
          kontaktlista:= Felsorolashoz(kontaktlista, kontakt, ' , ', False);
          if icloudkontakt <> '' then
            kontaktlista:= Felsorolashoz(kontaktlista, icloudkontakt, ' , ', False);
          end;  // if
        end  // if kontakt nem üres
      else begin
        if LegyenAlert then begin
           NincsKontaktja:= Felsorolashoz(NincsKontaktja, DONAME, ', ', False);
           end;
        end; // else
      EgyebDlg.Query2.Next;
      end;  // while
    if not KifejtettCsoportokKontakt.ContainsKey(CsoportKod) then
        KifejtettCsoportokKontakt.Add(CsoportKod, kontaktlista)
    else KifejtettCsoportokKontakt[CsoportKod]:= kontaktlista;
    if not KifejtettCsoportokEagleKontakt.ContainsKey(CsoportKod) then
        KifejtettCsoportokEagleKontakt.Add(CsoportKod, eaglekontaktlista)
    else KifejtettCsoportokEagleKontakt[CsoportKod]:= eaglekontaktlista;
    if not KifejtettCsoportokCimke.ContainsKey(Megjelenitett_nev) then
        KifejtettCsoportokCimke.Add(Megjelenitett_nev, cimkelista)
    else KifejtettCsoportokCimke[Megjelenitett_nev]:= cimkelista;
    if LegyenAlert and (NincsKontaktja <> '') then begin
        NoticeKi('Nincs érvényes elérhetőségük, nem kerültek a címzettek közé: '+NincsKontaktja);
        HibaKiiro('Üzenet küldésekor nincs érvényes elérhetőségük, nem kerültek a címzettek közé: '+NincsKontaktja);
        end;
end;

procedure TUzenetSzerkesztoDlg.CimzettTorlesClick(Sender: TObject);
var
   i, keresett: integer;
   KeyName, KeresettKey: string;
   CimzettVagyCC: TCimzettVagyCC;
begin
   keresett:= (Sender as TButton).Tag; // 1..999: cimzett 1000..1999: cc
   if keresett <=999 then begin
      CimzettVagyCC:= cimzett;
      end
   else begin
      keresett:= keresett - 1000;
      CimzettVagyCC:= cc;
      end;
   i:=0;
   if CimzettVagyCC = cimzett then
       CimzettekArray := Cimzettek.Keys.ToArray
   else CimzettekArray := CCk.Keys.ToArray;
   TArray.Sort<string>(CimzettekArray);
   for KeyName in CimzettekArray do begin
     if i = keresett then
        KeresettKey := KeyName;
     Inc(i);
     end;  // for
   if CimzettVagyCC = cimzett then begin
     Cimzettek.Remove(KeresettKey);
     Cimzett_Nevek.Remove(KeresettKey);
     CimzettLista_Frissites;
     end
   else begin
     CCk.Remove(KeresettKey);
     Cc_Nevek.Remove(KeresettKey);
     CCLista_Frissites;
     end;
end;

procedure TUzenetSzerkesztoDlg.CCTorlesClick(Sender: TObject);
var
   i, keresett: integer;
   KeyName, KeresettKey: string;
begin
   keresett:= (Sender as TMenuItem).Tag;
   i:=0;
   CimzettekArray := CCk.Keys.ToArray;
   TArray.Sort<string>(CimzettekArray);
   for KeyName in CimzettekArray do begin
     if i = keresett then
        KeresettKey := KeyName;
     Inc(i);
     end;  // for
   CCk.Remove(KeresettKey);
   CCLista_Frissites;
end;

procedure TUzenetSzerkesztoDlg.CsoportHozzaadClick(Sender: TObject);
var
   i, keresett: integer;
   KeyName, KeresettKey, Megjelenitett_nev, GKKAT, Csoportkod: string;
begin
   keresett:= (Sender as TMenuItem).Tag;
   i:=0;
   CsoportokArray := Csoportok.Keys.ToArray;
   TArray.Sort<string>(CsoportokArray);
   for KeyName in CsoportokArray do begin
     if i = keresett then
        KeresettKey := KeyName;
     Inc(i);
     end;  // for

   Csoportkod:= Csoportok[KeresettKey];  // C_Csoportkod+GKKAT;
   GKKAT:= EgyebDlg.SepRest(C_Csoportkod, Csoportkod);
   CsoportKifejt(GKKAT, True);  // frissíti a kifejtett listákat a "ForceSMS" függvényében
   if (Sender as TMenuItem).Owner = ppmCsoportok then begin
     AddCimzettCsoport(KeresettKey, cimzett);
     CimzettLista_Frissites;
     end;
   if (Sender as TMenuItem).Owner = ppmCsoportokCC then begin
     AddCimzettCsoport(KeresettKey, cc);
     CCLista_Frissites;
     end;
   chkForceSMS.Checked:= False;  // alaphelyzetbe vissza
end;


function TUzenetSzerkesztoDlg.GetCimzettek(Forma: string; CimzettVagyCC: TCimzettVagyCC): string;
var
   KeyName, S, Listaba, DisplayString: string;

   procedure Add_MySMS;
   begin
      if copy(DisplayString,1, Length(C_Csoportkod)) = C_Csoportkod then begin // ki kell fejteni
        DisplayString:= KifejtettCsoportokKontakt[DisplayString];  // hiszen a kulcs a kulcs :-)
        end;  // if
     if S = '' then S:= '['+DisplayString
     else S:= S + ' , '+DisplayString;
   end;

   procedure Add_SMSEagle(CimzettVagyCC: TCimzettVagyCC);
   begin
     if copy(DisplayString,1, Length(C_Csoportkod)) = C_Csoportkod then begin // ki kell fejteni
        Listaba:= KifejtettCsoportokEagleKontakt[DisplayString];  // DisplayString: pl. 'Csoport:1,5P'
        end  // if
     else begin
        if CimzettVagyCC = cimzett then
          if EgyebDlg.ErvenyesSMSEagleKontakt(Cimzettek[KeyName]) then
            Listaba:= '"'+Cimzett_Nevek[KeyName] + '" <'+Cimzettek[KeyName]+'@'+EgyebDlg.SMSEAGLE_DOMAIN+'>'
          else
            Listaba:= '"'+Cimzett_Nevek[KeyName] + '" <'+Cimzettek[KeyName]+'>';  // eleve email cím
        if CimzettVagyCC = cc then
          if EgyebDlg.ErvenyesSMSEagleKontakt(CCk[KeyName]) then
            Listaba:= '"'+CC_Nevek[KeyName] + '" <'+CCk[KeyName]+'@'+EgyebDlg.SMSEAGLE_DOMAIN+'>'
          else
            Listaba:= '"'+CC_Nevek[KeyName] + '" <'+CCk[KeyName]+'>';
        end;
     if S = '' then S:= Listaba
     else S:= S + ' ; '+Listaba;
   end;

begin
 if CimzettVagyCC = cimzett then begin
   for KeyName in Cimzettek.Keys do begin
     DisplayString:= Cimzettek[KeyName];
     if Forma='MYSMS' then Add_MySMS;
     if Forma='SMSEAGLE' then Add_SMSEagle(CimzettVagyCC);
     end;  // for
   if (Forma='MYSMS') then S:= S+']';
   end;  // if cimzett

 if CimzettVagyCC = cc then begin
   for KeyName in CCk.Keys do begin
     DisplayString:= CCk[KeyName];
     if Forma='MYSMS' then Add_MySMS;
     if Forma='SMSEAGLE' then Add_SMSEagle(CimzettVagyCC);
     end;  // for
   if (Forma='MYSMS') then S:= S+']';
   end;  // if cimzett
 Result:= S;
end;

procedure TUzenetSzerkesztoDlg.FormCreate(Sender: TObject);
begin
  Cimzettek:=TDictionary<string,string>.Create;
  CCk:=TDictionary<string,string>.Create;
  Cimzett_Nevek:=TDictionary<string,string>.Create;
  Cc_Nevek:=TDictionary<string,string>.Create;
  Csoportok:=TDictionary<string,string>.Create;
  KifejtettCsoportokCimke:=TDictionary<string,string>.Create;
  KifejtettCsoportokKontakt:=TDictionary<string,string>.Create;
  KifejtettCsoportokEagleKontakt:=TDictionary<string,string>.Create;
  cMenuClosed := 0;
  VoltKuldes:= False;  // mrCancel esetére
  KuldottSzoveg:= '';
  SetSzoveg('');  // beállítja a küldőt is
  KuldottCimzettek:= '';
  if EgyebDlg.UZENET_VALASZ_SZAM then begin
    pnlValaszSzam.Visible:= True;
    ValaszSzamToltes;
    end
  else begin
    pnlValaszSzam.Visible:= False;
    end;
  CsoportListaToltve:= False;
  chkValaszDefault.Checked:= False;
  // CsoportListaTolt;  kivettem innen, sok idő, és nem kell pl. a "Levél küldése sofőrnek" funkcióhoz. Ahol kell, ott külön meghívjuk.
end;

procedure TUzenetSzerkesztoDlg.FormDestroy(Sender: TObject);
begin
  if Cimzettek <> nil then Cimzettek.Free;
  if CCk <> nil then CCk.Free;
  if Cimzett_Nevek <> nil then Cimzett_Nevek.Free;
  if Cc_Nevek <> nil then Cc_Nevek.Free;
  if Csoportok <> nil then Csoportok.Free;
  if KifejtettCsoportokCimke <> nil then KifejtettCsoportokCimke.Free;
  if KifejtettCsoportokKontakt <> nil then KifejtettCsoportokKontakt.Free;
  if KifejtettCsoportokEagleKontakt <> nil then KifejtettCsoportokEagleKontakt.Free;
  if ValaszSzamLista <> nil then ValaszSzamLista.Free;
  inherited;
end;

procedure TUzenetSzerkesztoDlg.FormShow(Sender: TObject);
var
  Uzenettipus: string;
begin
  if EagleSMSTipus = uzleti then Uzenettipus := 'üzleti üzenet';
  if EagleSMSTipus = technikai then Uzenettipus := 'technikai üzenet';
   Caption:= 'SMS / email szerkesztő  ['+Uzenettipus+']';
end;

procedure TUzenetSzerkesztoDlg.RefreshUzenetMeret;
var
  CharDarab, SMSMeret, SMSDarab: integer;
begin
  CharDarab:= Length(Memo1.Text);
  if cbValaszSzam.ItemIndex >0 then
     CharDarab:= CharDarab + length(Format(ValaszMinta, [ValaszSzamLista[cbValaszSzam.ItemIndex]]));
  if EgyebDlg.SMS_KULDES_CHARSET = 'ASCII' then SMSMeret:= 160;
  if EgyebDlg.SMS_KULDES_CHARSET = 'UNICODE' then SMSMeret:= 70;
  SMSDarab:= Floor((CharDarab-1) / SMSMeret)+1;
  pnlMessageInfo.Caption:= 'Betűk száma: '+IntToStr(CharDarab)+', SMS-ek száma: '+IntToStr(SMSDarab)+' ('+IntToStr(SMSMeret)+' betű / SMS)';
end;

procedure TUzenetSzerkesztoDlg.Memo1KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
   RefreshUzenetMeret;
end;

procedure TUzenetSzerkesztoDlg.Reset;
begin
  Cimzettek.Clear;
  Cimzett_Nevek.Clear;
  CimzettLista_Frissites;
  CCk.Clear;
  Cc_Nevek.Clear;
  CCLista_Frissites;
  pnlMessageInfo.Caption:= '';
  KuldottCimzettek:= '';
  cMenuClosed := 0;
  VoltKuldes:= False;  // mrCancel esetére
  meTargy.Text:= '';
  KuldottSzoveg:= '';
  SetSzoveg('');  // beállítja a küldőt is
end;

end.








