unit Kozos_Local;

interface

uses
	SysUtils, Graphics, Mask, IniFiles, Forms, Classes, StdCtrls, Grids, DateUtils, Shellapi,
	Comobj, Variants , Comctrls, WinTypes;

	function	GetLastLizingDate( lidat, perdarab : string; periodus: integer) : string;
	function	GetRestLizingMonth( lidat, kezddat : string  ) : integer;
  function	GetRestLizingPeriod( lidat, kezddat : string; periodus: integer) : integer;
	function	GetSzamlaAllapot( allap : integer) : string;
	function	GetJaratFazis( fazkod : integer) : string;
	function 	Jopozicio(posza: string) : boolean;
	function 	GetValueInOtherCur(invalue : double; dat, incur, outcur : string ) : double;
   procedure   CalendarRead(var date1, date2 : string );

var
	Ini			: TIniFile  = nil;
	IniLocal	: TIniFile  = nil;

const

	// G�pkocsi fajt�k v�laszt�sa
	GK_TRAKTOR		= 0;
	GK_POTKOCSI		= 1;
	GK_MINDENGK		= 2;

	// Sz�mla form�tumok
	SZAMLA_FORMA_JS		= 0;
	SZAMLA_FORMA_DK 	= 3;

implementation

uses
	Notice, Info, Egyeb, J_SQL, J_EXPORT, KOZOS, Calend1, Math;


function	GetLastLizingDate( lidat, perdarab : string; periodus: integer) : string;
var
	datstr	: string;
	aktho2	: integer;
begin
	Result	:= '';
	datstr	:= 	lidat;
	// Az utols� befizet�s d�tum�nak meghat�roz�sa
	aktho2	:= 	StrToIntDef(copy(datstr,1,4),0) * 12 + StrToIntDef(copy(datstr,6,2),0)+
				(StrToIntDef(perdarab,0)-1)* periodus;
	if ( aktho2 mod 12 ) = 0 then begin
		datstr	:= IntToStr(( aktho2 div 12 ) - 1)+'12';
	end else begin
		datstr	:= IntToStr(aktho2 div 12)+Format('%2.2d', [aktho2 mod 12]);
	end;
	Result	:= datstr;
end;


function	GetRestLizingPeriod( lidat, kezddat : string; periodus: integer) : integer;
begin
	// Visszaadjuk a h�tral�v� lizinges peri�dusok sz�m�t
	Result	:= Floor(GetRestLizingMonth( lidat, kezddat)/ periodus);
end;

//function	GetRestLizingMonth( lidat : string) : integer;
function	GetRestLizingMonth( lidat, kezddat : string ) : integer;
begin
	// Visszaadjuk a h�tral�v� lizinges h�napok sz�m�t
	Result	:= 0;
  if kezddat<DateToStr(date) then
     kezddat:= DateToStr(date) ;
	if lidat < EgyebDlg.MaiDatum then begin
		Exit;
	end;
	Result	:=  StrToIntDef(copy(lidat,1,4),0)*12+StrToIntDef(copy(lidat,6,2),0) -
				StrToIntDef(copy(kezddat,1,4),0)*12-StrToIntDef(copy(kezddat,6,2),0) + 1;
//			StrToIntDef(copy(EgyebDlg.MaiDatum,1,4),0)*12-StrToIntDef(copy(EgyebDlg.MaiDatum,6,2),0) + 1;
end;

function	GetSzamlaAllapot( allap : integer) : string;
begin
  case allap  of
	0:	Result := 'befejezett';
	1:	Result := 'k�sz�t�s alatt';
	2:	Result := 'storn�zott';
	else
		Result := 'storn� sz�mla';
  end;
end;

function	GetJaratFazis( fazkod : integer) : string;
begin
	case fazkod of
		-2:	Result := 'Feloldott';
		-1:	Result := 'Elsz�molt';
		 0:	Result := 'Norm�l';
		 1:	Result := 'Ellen�rz�tt';
		 2:	Result := 'Storn�zott';
		else
		Result := 'Ismeretlen';
	end;
end;

function 	Jopozicio(posza: string) : boolean;
begin
	Result	:= true;
	if Length(posza) > 8 then begin
		if ( ( Pos(',', posza) < 1 ) or ( Pos('/', posza) > 0 ) ) then begin
			Result	:= false;
		end;
	end;
end;

function GetValueInOtherCur(invalue : double; dat, incur, outcur : string ) : double;
var
	arf		: double;
	huert   : double;
begin
	// Kisz�m�tjuk egy valut�s �sszeg �rt�k�t egy m�sik valutanemben
	if incur = outcur then begin
		Result	:= invalue;
		Exit;
	end;
	arf 	:= EgyebDlg.ArfolyamErtek( incur, dat, true);
	huert	:= arf * invalue;
	if Uppercase(Trim(outcur)) = 'HUF' then begin
		Result	:= huert;
		Exit;
	end;
	arf 	:= EgyebDlg.ArfolyamErtek( outcur, dat, true);
	if arf = 0 then begin
		Result	:= 0;
	end else begin
		Result	:= huert / arf;
	end;
end;

procedure CalendarRead(var date1, date2 : string );
{*************************************************}
begin
   // D�tum beolvas�sa
 	if date1 = '' then begin
  		date1 := EgyebDlg.MaiDatum;
  	end;
   date2       := '';
  	Application.CreateForm(TCalendarDlg, CalendarDlg);
  	CalendarDlg.Tolt(date1);
  	CalendarDlg.ShowModal;
  	if CalendarDlg.ret_date <> '' then begin
  		date1 	:= CalendarDlg.ret_date;
  		date2 	:= CalendarDlg.ret_date2;
  	end;
  	CalendarDlg.Destroy;
end;

end.
