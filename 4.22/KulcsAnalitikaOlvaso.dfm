ÿ
 TKULCSANALITIKAOLVASODLG 0æ  TPF0TKulcsAnalitikaOlvasoDlgKulcsAnalitikaOlvasoDlgLeft|ToprBorderIconsbiSystemMenu BorderStylebsSingleCaption"   Kulcs analitika adatok beolvasÃ¡saClientHeightZClientWidth¢Color	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Heightó	Font.NameArial
Font.Style OldCreateOrder	PositionpoScreenCenterOnCreate
FormCreatePixelsPerInch`
TextHeight TLabelLabel4Left	TopQWidth>HeightCaption   ÃllomÃ¡ny :Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Heightó	Font.NameArial
Font.Style 
ParentFont  TBitBtnBitBtn6Left°Top,WidthjHeight$Hint   KilÃ©pÃ©s a fÅmenÃ¼beHelpContextã'Cancel	Caption
   &KilÃ©pÃ©s
Glyph.Data
Ò  Î  BMÎ      6   (   $                                                                                       ÿÿÿ                 ÿ           ÿ        ÿÿÿ      ÿÿÿ         ÿ           ÿ          ÿÿÿ ÿÿÿ   ÿÿÿÿÿÿ        ÿ           ÿ             ÿÿÿ  ÿÿÿ ÿÿÿ  ÿÿÿ        ÿ                       ÿÿÿ   ÿÿÿ    ÿÿÿ        ÿ                       ÿÿÿ       ÿÿÿ          ÿ                     ÿÿÿ      ÿÿÿ                               ÿÿÿ                  ÿ                    ÿÿÿ                ÿ                          ÿÿÿ           ÿ                           ÿÿÿ          ÿ         ÿ                 ÿÿÿ  ÿÿÿ         ÿ         ÿ            ÿÿÿ   ÿÿÿ  ÿÿÿ         ÿ         ÿ            ÿÿÿÿÿÿ   ÿÿÿ  ÿÿÿ                ÿ    ÿ            ÿÿÿÿÿÿÿÿÿ                                                                      	NumGlyphsTabOrderOnClickBitBtn6Click  	TMaskEditM1LeftNTopMWidthHeightColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Heightó	Font.NameArial
Font.Style 	MaxLengthÈ 
ParentFontTabOrder Text      TBitBtnBitBtn5Left]TopMWidth(Height
Glyph.Data
F  B  BMB      v   (               Ì                       ¿  ¿   ¿¿ ¿   ¿ ¿ ¿¿  ÀÀÀ    ÿ  ÿ   ÿÿ ÿ   ÿ ÿ ÿÿ  ÿÿÿ wwwwwwwwp   p    wwwp   pÿÿÿðwwp    p÷wwðww     p÷wwðwp p   p÷w  p   p÷pÿ  wp   p÷ÿÿwp   pø wp   pðïïïpwp   pðð pwp   p ïïÿpwp   wx   wp   ww wwxwp   wwpw wwp   www wwp   wwwwwwwwp   TabOrderTabStopOnClickBitBtn5Click  TBitBtn	BtnImportLeft Top.WidthkHeight$Hint   Import|Adatok importÃ¡lÃ¡saHelpContextr9Caption
   BeolvasÃ¡sFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Heightó	Font.NameArial
Font.Style 
Glyph.Data
z  v  BMv      v   (                                                  ¿¿¿   ÿ  ÿ   ÿÿ ÿ   ÿ ÿ ÿÿ  ÿÿÿ 333ÿÿ°333w33w333ÿÿ°333w33w333ÿÿ°333w33w333ÿÿ°?ÿÿwÿÿw     w pwwwwwwwwÿÿÿð w°3337ÿÿwÿÿÿð {°?óÿ÷÷w ð ð w7w7wwÿÿÿð93?ÿó÷÷wÿ  ððww77wwÿÿÿù?óÿ÷www ð  93w7ww7wóÿðÿ93?÷ós7wóðð3y3w7÷?ww3ÿð3ÿ÷swws3   33333www33333	NumGlyphs
ParentFontTabOrderTabStopOnClickBtnImportClick  TStringGridSG1LeftTop!WidthTHeight/ColCount
	FixedCols 	FixedRows TabOrderVisible  TMemoMemo1LeftTopkWidthHeight» Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Heightó	Font.NameCourier New
Font.Style 
ParentFontReadOnly	
ScrollBarsssBothTabOrderWordWrap  TRadioButtonRadioButton1LeftTopWidth HeightCaption   Direkt adatÃ¡tvitelChecked	TabOrderTabStop	OnClickRadioButton1Click  TRadioButtonRadioButton2LeftTopWidthqHeightCaption   CSV beolvasÃ¡sTabOrderOnClickRadioButton2Click  TPanelPanel1LeftÐ TopWidthµHeight5
BevelOuter	bvLoweredTabOrder TLabelLabel1LeftTopWidth³Height3AlignalClientAutoSizeCaptionÏ   BeolvasÃ¡s elÅtt kÃ©rem lÃ©pjen be a Kulcs KÃ¶nyvelÃ©s programba, Ã©s mentsen le Excel Ã¡llomÃ¡nykÃ©nt egy tÃ©telenkÃ©nti folyÃ³szÃ¡mla analitikÃ¡t! Ha ez mÃ¡r kÃ©sz, kattintson a lenti "BeolvasÃ¡s" gombra.Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Heightó	Font.NameArial
Font.Style 
ParentFontWordWrap	ExplicitLeftExplicitTop'ExplicitWidthÅExplicitHeight@   TPanelPanel2LeftÑ TopWidth³Height5
BevelOuter	bvLoweredTabOrder	  	TMaskEditmeEvLeft TopWidth<HeightColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Heightó	Font.NameArial
Font.Style 	MaxLengthÈ 
ParentFontTabOrder
Text      	TADOQueryQuery1Tag
CursorTypectStatic
Parameters LeftÙ Top  	TADOQueryQuery2Tag
CursorTypectStatic
Parameters LeftTop  TOpenDialogOpenDialog1Left Top  	TADOQuery
KulcsQueryTag
CursorTypectStatic
Parameters LeftYTop  	TADOQuery	QueryLongTag
CursorTypectStaticCommandTimeout,
Parameters LeftÙ Top¿    