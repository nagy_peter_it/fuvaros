unit KilizingList2;

interface

uses
  WinTypes, WinProcs, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, QuickRpt, QRExpr, Qrctrls, ExtCtrls, printers, DB, DBTables, Grids, IniFiles,
  ADODB, ClipBrd;

type
  TKilizingList2Dlg = class(TForm)
    Rep: TQuickRep;
    QRBand3: TQRBand;
    QRLSzamla: TQRLabel;
    QRBand1: TQRBand;
    QRLabel6: TQRLabel;
    QRSysData1: TQRSysData;
    QRLabel7: TQRLabel;
    QRSysData4: TQRSysData;
    QRLabel1: TQRLabel;
    Q3: TQRLabel;
    Q2: TQRLabel;
    Q5: TQRLabel;
    Q4: TQRLabel;
    Q1: TQRLabel;
	 ArfolyamGrid: TStringGrid;
	 Osszesito: TStringGrid;
	 QRLabel13: TQRLabel;
    Query1: TADOQuery;
    Query2: TADOQuery;
	 Q0: TQRLabel;
	 QRLabel2: TQRLabel;
	 QRLabel3: TQRLabel;
	 QRLabel4: TQRLabel;
	 QRLabel5: TQRLabel;
	 QRLabel8: TQRLabel;
	 QRLabel9: TQRLabel;
	 QRLabel10: TQRLabel;
	 Q6: TQRLabel;
    SG1: TStringGrid;
    QRShape1: TQRShape;
    QRGroup1: TQRGroup;
    QRLabel11: TQRLabel;
    Q5B: TQRLabel;
    QRBand2: TQRBand;
    QRLabel38: TQRLabel;
    QV6: TQRLabel;
    QRShape3: TQRShape;
    Q5C: TQRLabel;
    Q7: TQRLabel;
    QRLabel12: TQRLabel;
	 procedure FormCreate(Sender: TObject);
	 procedure	Tolt(sorrend: integer);
	 procedure RepBeforePrint(Sender: TCustomQuickRep;
	   var PrintReport: Boolean);
    procedure RepNeedData(Sender: TObject; var MoreData: Boolean);
    procedure QRBand1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBand2BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
  private
		sorszam			: integer;
		vegertek		: double;
  public
	  vanadat			: boolean;
  end;

var
  KilizingList2Dlg: TKilizingList2Dlg;

implementation

uses
	J_SQL, Egyeb, KOzos, Kozos_Local;

{$R *.DFM}

procedure TKilizingList2Dlg.FormCreate(Sender: TObject);
begin
	EgyebDlg.SetADOQueryDatabase(Query1);
	EgyebDlg.SetADOQueryDatabase(Query2);
end;

procedure	TKilizingList2Dlg.Tolt(sorrend: integer);
var
	  i  ,hpervissza		: integer;
	  str	, rensz, dat, liceg	: string;
    arf: double;
begin
	vanadat 	:= false;
 // 	Query_Run(Query1, 'SELECT * FROM GEPKOCSI WHERE GK_LIVAN = 1 ');
 	Query_Run(Query1, 'SELECT * FROM GEPKOCSI left outer join SZOTAR on SZ_FOKOD=''240'' AND GK_LIPER = SZ_ALKOD WHERE GK_LIVAN = 1 ');
	if Query1.RecordCount = 0 then begin
		Exit;
	end;
	vanadat			:= true;
  arf:=EgyebDlg.ArfolyamErtek('EUR',EgyebDlg.MaiDatum, True, True);
	SG1.RowCount    := Query1.RecordCount;
	i				:= 0;
	while not Query1.Eof do begin
		SG1.Cells[0, i]	:= Query1.FieldByName('GK_RESZ').AsString;
		SG1.Cells[1, i]	:= Query1.FieldByName('GK_TIPUS').AsString;
		SG1.Cells[2, i]	:= Query1.FieldByName('GK_LIDAT').AsString;
//		SG1.Cells[3, i]	:= Query1.FieldByName('GK_LIOSZ').AsString + ' ' + Query1.FieldByName('GK_LINEM').AsString;
		SG1.Cells[3, i]	:= Format('%.2f',[StringSzam(Query1.FieldByName('GK_LIOSZ').AsString)])+ ' ' + Query1.FieldByName('GK_LINEM').AsString +
          '/'+Query1.FieldByName('SZ_LEIRO').AsString; // a peri�dus megnevez�se, r�vid�tve
		SG1.Cells[4, i]	:= Query1.FieldByName('GK_LIHON').AsString;
		liceg		:= Query1.FieldByName('GK_LICEG').AsString;
		SG1.Cells[8, i]	:= EgyebDlg.Read_SZGrid( '380', liceg );
		// A lej�rat d�tum�nak meghat�roz�sa
		// str				:= 	GetLastLizingDate( Query1.FieldByName('GK_LIDAT').AsString, Query1.FieldByName('GK_LIHON').AsString);
    str := GetLastLizingDate( Query1.FieldByName('GK_LIDAT').AsString, Query1.FieldByName('GK_LIHON').AsString, Query1.FieldByName('GK_LIPER').AsInteger);
		str := copy(str,1,4)+'.'+copy(str,5,2)+'.';
		SG1.Cells[5, i]	:= str;
    // hho:=GetRestLizingMonth(SG1.Cells[5, i],Query1.FieldByName('GK_LIDAT').AsString);
    hpervissza:=GetRestLizingPeriod(SG1.Cells[5, i],Query1.FieldByName('GK_LIDAT').AsString, Query1.FieldByName('GK_LIPER').AsInteger);

    rensz:=Query1.FieldByName('GK_RESZ').AsString;
    str:=copy(EgyebDlg.MaiDatum,1,8);
    dat:=Query_Select2('LIZING','LZ_RENSZ','SUBSTRING(LZ_DATUM,1,8)',rensz,str,'LZ_DATUM');
    if (hpervissza>0) and (dat<>EmptyStr) and (dat<>'0') then
      hpervissza:=hpervissza-1;
    if hpervissza=0 then hpervissza:=1;

		SG1.Cells[6, i]	:= Format('%.2f',[hpervissza*StringSzam(Query1.FieldByName('GK_LIOSZ').AsString)])+
     ' ' +Query1.FieldByName('GK_LINEM').AsString;

    if Query1.FieldByName('GK_LINEM').AsString<>'EUR' then
  		SG1.Cells[7, i]	:= Format('%.2f',[hpervissza*StringSzam(Query1.FieldByName('GK_LIOSZ').AsString)/arf])+
       ' EUR'
    else
  		SG1.Cells[7, i]	:= Format('%.2f',[hpervissza*StringSzam(Query1.FieldByName('GK_LIOSZ').AsString)])+
       ' ' +Query1.FieldByName('GK_LINEM').AsString;
		Inc(i);
		Query1.Next;
	end;
	// A t�bla rendez�se lej�rati d�tum szerint
//	TablaRendezo( SG1, [5], [0], 0, true );

	case sorrend of
		0 : // Rendsz�m
    	TablaRendezo( SG1, [0], [0], 0, true );
		1 : // T�pus 		+ rendsz�m
    	TablaRendezo( SG1, [1,0], [0,0], 0, true );
		2 : // L�zing c�g  	+ rendsz�m
    	TablaRendezo( SG1, [8,0], [0,0], 0, true );
		3 : // LI.kezd  	+ rendsz�m
    	TablaRendezo( SG1, [2,0], [0,0], 0, true );
		4 : // Havid�j  	+ rendsz�m
    	TablaRendezo( SG1, [3,0], [0,0], 0, true );
		5 : // id�tart.  	+ rendsz�m
    	TablaRendezo( SG1, [4,0], [1,0], 0, true );
		6 : // H�tral�k  	+ rendsz�m
    	TablaRendezo( SG1, [6,0], [0,0], 0, true );
		7 : // Lej�rat  	+ rendsz�m
    	TablaRendezo( SG1, [5,0], [0,0], 0, true );
  end;
//	TablaRendezo( SG1, [1,2,5], [0,0,0], 0, true );
end;

procedure TKilizingList2Dlg.RepBeforePrint(Sender: TCustomQuickRep;
	   var PrintReport: Boolean);
begin
	sorszam			:= 0;
	vegertek	:= 0;
end;

procedure TKilizingList2Dlg.RepNeedData(Sender: TObject;
  var MoreData: Boolean);
begin
	MoreData	:= sorszam <= SG1.RowCount -1;
end;

procedure TKilizingList2Dlg.QRBand1BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
  ossz:string;
begin
	Q0.Caption	:= IntToStr(sorszam + 1);
	Q1.Caption	:= SG1.Cells[0, sorszam];
	Q2.Caption	:= SG1.Cells[1, sorszam];
	Q7.Caption	:= SG1.Cells[8, sorszam];
	Q3.Caption	:= SG1.Cells[2, sorszam];
	Q4.Caption	:= SG1.Cells[3, sorszam];
	Q5.Caption	:= SG1.Cells[4, sorszam];
	Q5B.Caption	:= SG1.Cells[6, sorszam];
	Q5C.Caption	:= SG1.Cells[7, sorszam];
	Q6.Caption	:= SG1.Cells[5, sorszam];
  Q5C.Enabled:= Q5B.Caption<>Q5C.Caption ;
  if Q5B.Caption=Q5C.Caption then
    QRBand1.Height:=21
  else
    QRBand1.Height:=35;
    
  ossz:=Q5C.Caption;
  if ossz='' then ossz:='0';
  ossz:= StringReplace(ossz,'EUR','',[]);
  ossz:= StringReplace(ossz,'HUF','',[]);
  ossz:= StringReplace(ossz,'.',EgyebDlg.TIZEDESJEL,[]);
  ossz:= StringReplace(ossz,',',EgyebDlg.TIZEDESJEL,[]);
	vegertek	:= vegertek + StrToFloat(ossz);
	Inc(sorszam);
end;

procedure TKilizingList2Dlg.QRBand2BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
	QV6.Caption 	:= Format('%.2f', [vegertek])+' EUR';
end;

end.
