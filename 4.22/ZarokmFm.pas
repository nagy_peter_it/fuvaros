unit ZarokmFm;

interface

uses
	SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
	StdCtrls, Forms, ExtCtrls, Dialogs, J_FOFORMUJ, Buttons;

type
	TZarokmFmDlg = class(TJ_FOFORMUJDLG)
    BitBtn5: TBitBtn;
	  procedure FormCreate(Sender: TObject);override;
    procedure Adatlap(cim, kod : string);override;
  	procedure ButtonUjClick(Sender: TObject);override;
    procedure BitBtn5Click(Sender: TObject);

  public
    idopont: string;
    ret_kod: string;
	end;

var
	ZarokmFmDlg : TZarokmFmDlg;

implementation

uses
	Egyeb, J_SQL, Zarokmbe, DB, UkmFriss;

{$R *.DFM}

procedure TZarokmFmDlg.FormCreate(Sender: TObject);
begin
	Inherited FormCreate(Sender);
	BitBtn5.Parent		:= PanelBottom;

	AddSzuromezoRovid('ZK_RENSZ',  'ZAROKM','1');
	AddSzuromezoRovid('ZK_GKKAT',  'ZAROKM','1');
	AddSzuromezoRovid('ZK_EV1',  'ZAROKM','1');
	AddSzuromezoRovid('ZK_HO1',  'ZAROKM','1');

	FelTolto('�j z�r� km felvitele ', 'Z�r� km m�dos�t�sa ',
			'Z�r� km-ek list�ja', 'ZK_ZKKOD', 'Select * from ZAROKM ','ZAROKM', QUERY_ADAT_TAG );
	width	:= DLG_WIDTH;
	height	:= DLG_HEIGHT;
end;

procedure TZarokmFmDlg.Adatlap(cim, kod : string);
begin
	Application.CreateForm(TZarokmbeDlg, AlForm);
	Inherited Adatlap(cim, kod );
end;

procedure TZarokmFmDlg.ButtonUjClick(Sender: TObject);
begin
  inherited;
  //ModLocate(Query1, 'ZK_ZKKOD', Query1.FieldByName('ZK_ZKKOD').AsString );
  ModLocate(Query1, 'ZK_ZKKOD', ret_kod );

end;

procedure TZarokmFmDlg.BitBtn5Click(Sender: TObject);
begin
  Application.CreateForm(TFUkmFriss, FUkmFriss);
  FUkmFriss.ComboBoxU1.Text:= Query1.fieldbyname('ZK_EV1').AsString;
  FUkmFriss.ComboBoxU2.Text:= Query1.fieldbyname('ZK_HO1').AsString;
  FUkmFriss.ComboBoxU3.Text:= Query1.fieldbyname('ZK_HO1').AsString;

  if  FUkmFriss.ShowModal=mrOk then
    ModLocate(Query1, 'ZK_ZKKOD', ret_kod );


  FUkmFriss.Free;
end;

end.

