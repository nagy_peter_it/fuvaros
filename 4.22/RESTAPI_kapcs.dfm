object RESTAPIKapcsolatDlg: TRESTAPIKapcsolatDlg
  Left = 0
  Top = 0
  Caption = 'RESTAPIKapcsolatDlg'
  ClientHeight = 558
  ClientWidth = 363
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PopupMenu = PopupMenu1
  PixelsPerInch = 96
  TextHeight = 13
  object GridPanel2: TGridPanel
    Left = 0
    Top = 517
    Width = 363
    Height = 41
    Align = alBottom
    ColumnCollection = <
      item
        Value = 100.000000000000000000
      end>
    ControlCollection = <
      item
        Column = 0
        Control = Button4
        Row = 0
      end>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    RowCollection = <
      item
        Value = 100.000000000000000000
      end
      item
        SizeStyle = ssAuto
      end>
    TabOrder = 0
    DesignSize = (
      363
      41)
    object Button4: TButton
      Left = 75
      Top = 4
      Width = 212
      Height = 33
      Anchors = []
      Caption = 'Mentem a  m'#243'dos'#237't'#225'sokat'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      OnClick = Button4Click
    end
  end
  object ListView1: TListView
    Left = 0
    Top = 0
    Width = 363
    Height = 517
    Align = alClient
    Checkboxes = True
    Columns = <
      item
        Caption = 'N'#233'v'
        MaxWidth = 350
        MinWidth = 100
        Width = 350
      end
      item
        Caption = 'ID'
        MaxWidth = 100
        Width = 0
      end
      item
        Caption = 'TaroltCheck'
        MaxWidth = 100
        Width = 0
      end>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    Items.ItemData = {
      05890000000300000000000000FFFFFFFFFFFFFFFF01000000FFFFFFFF000000
      000641007400740069006C0061000139004057921A00000000FFFFFFFFFFFFFF
      FF01000000FFFFFFFF00000000054200650072006300690001310068A8921A00
      000000FFFFFFFFFFFFFFFF01000000FFFFFFFF0000000005530061006E007900
      6900013300D88C921AFFFFFFFFFFFF}
    ParentFont = False
    SortType = stText
    TabOrder = 1
    ViewStyle = vsReport
    ExplicitTop = -2
  end
  object lbSelectedID: TListBox
    Left = 144
    Top = 181
    Width = 73
    Height = 33
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ItemHeight = 13
    ParentFont = False
    TabOrder = 2
    Visible = False
  end
  object PopupMenu1: TPopupMenu
    Left = 263
    Top = 136
    object UzenetTop1: TMenuItem
      Caption = 'UzenetTop'
      ShortCut = 121
      OnClick = UzenetTop1Click
    end
  end
end
