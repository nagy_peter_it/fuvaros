unit Osszlis2Uj;

interface

uses
  WinTypes, WinProcs, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, QuickRpt, QRExpr, Qrctrls, ExtCtrls, printers, DB, DBTables, Egyeb, Forgalom, J_SQL,
  Grids, Kozos, ADODB,Ossznyil;

const
	MEZOSZAM	= 17;

type
  TJaratKoltsegFogyasztas = record
      OsszHUFKoltseg: double;
      OsszEurKoltseg: double;
      OsszHUFNemkellKoltseg: double;
      OsszEurNemkellKoltseg: double;
      TenyFogyasztas: double;
      TervFogyasztas: double;
      Megtakaritas: double;
      OsszKm: double;
      ErvenyesTenyFogyasztas: boolean;
      end;  // record
  // E_res= array[1..3] of string;
  E_res = record
      okm: integer;
      oktg: integer;
      oeurktg: double;
      jaratok: string;
      ervenyes_fogyasztas: boolean;
      end;

  TOsszlis2DlgUj = class(TForm)
    Rep: TQuickRep;
    QRBand3: TQRBand;
    QRLSzamla: TQRLabel;
    QRBand1: TQRBand;
    QRLabel16: TQRLabel;
    QRLabel17: TQRLabel;
    QRLabel6: TQRLabel;
    QRSysData1: TQRSysData;
    QRLabel7: TQRLabel;
    QRSysData4: TQRSysData;
    QRLabel1: TQRLabel;
    QRLabel8: TQRLabel;
	 QRLabel3: TQRLabel;
    QRLabel13: TQRLabel;
    QRLabel14: TQRLabel;
    QRLabel2: TQRLabel;
    QRLabel5: TQRLabel;
    QS2: TQRLabel;
    SG1: TStringGrid;
    QGK06: TQRLabel;
    Query1: TADOQuery;
    Query2: TADOQuery;
    Query3: TADOQuery;
    Query4: TADOQuery;
    Query5: TADOQuery;
    QueryAlap: TADOQuery;
    QRGroup1: TQRGroup;
    QRLabel27: TQRLabel;
    QRBand4: TQRBand;
    QRLabel28: TQRLabel;
    QRShape3: TQRShape;
    QRLabel4: TQRLabel;
    QRShape2: TQRShape;
    QGK05: TQRLabel;
    QGK01: TQRLabel;
    QGK02: TQRLabel;
    QGK04: TQRLabel;
	 QGK07: TQRLabel;
    QGK08: TQRLabel;
    QGK09: TQRLabel;
    QGK10: TQRLabel;
    QK01: TQRLabel;
    QK02: TQRLabel;
    QK04: TQRLabel;
    QK05: TQRLabel;
    QK06: TQRLabel;
    QK07: TQRLabel;
    QK08: TQRLabel;
    QK09: TQRLabel;
    QK10: TQRLabel;
    QRLabel29: TQRLabel;
    QRLabel31: TQRLabel;
    QRLabel33: TQRLabel;
    QRLabel34: TQRLabel;
    QRLabel35: TQRLabel;
    QRLabel36: TQRLabel;
    QRLabel37: TQRLabel;
    QRLabel9: TQRLabel;
    QRLabel12: TQRLabel;
    QRLabel38: TQRLabel;
    QRLabel40: TQRLabel;
    QRLabel42: TQRLabel;
    QGK12: TQRLabel;
    QGK13: TQRLabel;
    QGK14: TQRLabel;
    QK12: TQRLabel;
    QK13: TQRLabel;
    QK14: TQRLabel;
    QRBand5: TQRBand;
    QV01: TQRLabel;
    QV09: TQRLabel;
    QV10: TQRLabel;
    QV02: TQRLabel;
    QV12: TQRLabel;
    QV04: TQRLabel;
    QV05: TQRLabel;
    QV06: TQRLabel;
    QV13: TQRLabel;
    QV07: TQRLabel;
    QV08: TQRLabel;
    QV14: TQRLabel;
    QRShape1: TQRShape;
	 QRShape4: TQRShape;
    QRShape5: TQRShape;
    QRLabel10: TQRLabel;
    QRLabel11: TQRLabel;
    QRLabel15: TQRLabel;
    QRLabel18: TQRLabel;
    QRLabel19: TQRLabel;
    QRLabel20: TQRLabel;
    QRLabel21: TQRLabel;
    QRLabel22: TQRLabel;
    QRLabel23: TQRLabel;
    QRLabel25: TQRLabel;
    QRLabel26: TQRLabel;
    QRLabel30: TQRLabel;
    Query6: TADOQuery;
    QRLabel39: TQRLabel;
    QRLabel41: TQRLabel;
    QRLabel43: TQRLabel;
    QRLabel44: TQRLabel;
    QRLabel45: TQRLabel;
    QK15: TQRLabel;
    QV15: TQRLabel;
    QRLabel46: TQRLabel;
    QRLabel47: TQRLabel;
    SGRSZ: TStringGrid;
    SGEllen: TStringGrid;
    Queryal2: TADOQuery;
    Query1_: TADOQuery;
    Q_Szotar: TADOQuery;
    QueryAlap1: TADOQuery;
    QRLabel48: TQRLabel;
    QRLabel49: TQRLabel;
    QGK15: TQRLabel;
    QRLabel24: TQRLabel;
    QGK16: TQRLabel;
    QV16: TQRLabel;
    QK16: TQRLabel;
    QGK100: TQRLabel;
    QGKKomp1: TQRImage;
    QKKomp1: TQRImage;
    QVKomp1: TQRImage;
    QRLabel32: TQRLabel;
    QRShape6: TQRShape;
    QRLabel50: TQRLabel;
    QGK17: TQRLabel;
    QK17: TQRLabel;
    QV17: TQRLabel;
    QK18: TQRLabel;
    QK19: TQRLabel;
    QK20: TQRLabel;
    QK21: TQRLabel;
    QV18: TQRLabel;
    QV19: TQRLabel;
    QV20: TQRLabel;
    QV21: TQRLabel;
	 procedure FormCreate(Sender: TObject);
    procedure	Tolt(jarats, rendsz, datum1, datum2, vevokod, viszonysql : string; csakSajatAuto: boolean);
    procedure QRBand1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure RepBeforePrint(Sender: TCustomQuickRep;
      var PrintReport: Boolean);
    procedure FormDestroy(Sender: TObject);
    procedure QRGroup1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBand4BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
	 procedure GkSzamolo(rsz: string);
	 function  Joertek(ertek : double; tip : integer) : boolean;
    procedure QRBand5BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
	 procedure Ellenorzes;

	private
  rekszam			: integer;
     jaratsz			: string;
     ertekek			: array [1..6] of double;
     gkertek			: array [1..MEZOSZAM] of double;
     katerte			: array [1..MEZOSZAM] of double;
     okatert			: array [1..MEZOSZAM] of double;
     gkdarabok		: array [1..6] of integer;
     osszeur			: double;
     dat1				: string;
	  dat2				: string;
	  jarat				: string;
	  ertekszin			: integer;
    rendszamok: string;
    l_vevokod, l_viszonysql: string;

    ervenytelen_fogyasztas_auto: boolean;
    ervenytelen_fogyasztas_csoport: boolean;
    ervenytelen_fogyasztas_grandtotal: boolean;

    ervenytelen_km_auto: boolean;
    ervenytelen_km_csoport: boolean;
    ervenytelen_km_grandtotal: boolean;


    function ktsg_nemkell(nev: string): boolean;
    function Jarat_Egyeb_km(jakod: string): E_res;
	  // function Jarat_Egyeb_ktg(jarat : string ): integer;
    function GetJaratKoltsegFogyasztas(JAKOD: string; KellKoltseg, KellFogyasztas: boolean): TJaratKoltsegFogyasztas;
  public
	  vanadat			: boolean;
    ossz,ossz2,eurossz2	: double;
	  kateg				: string;
    csakaproblemas, csakatermelo: boolean;
  end;

var
  Osszlis2DlgUj: TOsszlis2DlgUj;

implementation

uses
	J_Fogyaszt, Valellenor,StrUtils;

{$R *.DFM}

procedure TOsszlis2DlgUj.FormCreate(Sender: TObject);
begin
  	EgyebDlg.SetADOQueryDatabase(Query1);
  	EgyebDlg.SetADOQueryDatabase(Query2);
  	EgyebDlg.SetADOQueryDatabase(Query3);
  	EgyebDlg.SetADOQueryDatabase(Query4);
  	EgyebDlg.SetADOQueryDatabase(Query5);
	  EgyebDlg.SetADOQueryDatabase(QueryAlap);
  	EgyebDlg.SetADOQueryDatabase(Query6);
	  EgyebDlg.SetADOQueryDatabase(QueryAl2);
  	EgyebDlg.SetADOQueryDatabase(Query1_);
	  EgyebDlg.SetADOQueryDatabase(QueryAlap1);
  	rekszam	 := 1;
   kateg    := '';
   Application.CreateForm(TFogyasztasDlg, FogyasztasDlg);
end;

procedure	TOsszlis2DlgUj.Tolt(jarats, rendsz, datum1, datum2, vevokod, viszonysql : string; csakSajatAuto: boolean);
var
  	sqlstr, str, S: string;
	  i			: integer;
begin
	vanadat 	:= false;
	for i := 1 to 6 do begin
		ertekek[i]		:= 0;
		gkdarabok[i]	:= 0;
	end;
  l_vevokod:= vevokod;
  l_viszonysql:= viszonysql;
	{A fejl�c kit�lt�se}
	SG1.RowCount 	:= 0;
	jarat			:= jarats;
	dat1			:= datum1;
	dat2			:= datum2;
	if dat1 = '' then begin
		dat1	:= '2000.01.01.';
	end;
	if dat2 = '' then begin
		dat2	:= EgyebDlg.MaiDatum;
	end;
	QRLabel17.Caption	:= datum1 + ' - ' + datum2;
	QRLabel16.Caption	:= rendsz;
	if (jarat <> '') and (Pos(',', jarat) = 0) then begin  // van megadva j�rat, �s nem lista
    S:= jarat;
    if (copy(S,1,1)='''') and (copy(S,length(S),1)='''') then begin // ha aposztr�fok k�z�tt van
        S:= copy(S,2,Length(S)-2);
        end;
		QRLabel5.Caption	:= GetJaratszam(S);
	end else begin
		QRLabel5.Caption	:= 'T�bb j�rat';
	end;

	// �ssze�ll�tjuk a sz�ks�ges rendsz�mokb�l �ll� list�t + RSZ, JARAT t�bla t�lt�se
	//sqlstr := 'SELECT * FROM GEPKOCSI WHERE GK_KOD > '''' AND GK_ARHIV < 1 AND GK_KIVON = 0 AND GK_POTOS = 0 AND GK_KULSO = 0 ';
	sqlstr := 'SELECT * FROM GEPKOCSI WHERE GK_KOD > '''' AND GK_POTOS <> 1 AND GK_KULSO = 0 ';
  if csakatermelo then begin
    sqlstr := sqlstr + ' AND isnull(GK_NEMTERM, 0) = 0 ';
    end;
	if kateg <> '' then begin
		sqlstr := sqlstr + ' AND GK_GKKAT IN ( '+kateg+' ) ';
	end;
	if rendsz <> '' then begin
		sqlstr	:= sqlstr + ' AND GK_RESZ IN ( ';
		str		:= rendsz;
		while Pos(',',str) > 0 do begin
			sqlstr	:= sqlstr + '''' + copy(str,1,Pos(',',str)-1) + ''',';
			str		:= copy(str, Pos(',',str)+1,999);
		end;
		if str <> '' then begin
			sqlstr	:= sqlstr + '''' + str + ''',';
		end;
		sqlstr	:= copy(sqlstr, 1, Length(sqlstr)-1) +' ) ';
	end;
	sqlstr := sqlstr + ' ORDER BY GK_GKKAT, GK_RESZ';
	Query_Run(QueryAlap, sqlstr);
	vanadat	:= QueryAlap.RecordCount > 0;
	if not vanadat then begin
		Exit;
	end;
	// �sszerakjuk a t�nyleges rendsz�mokb�l �ll� stringet
	QueryAlap.First;
	str		:= '';
	while not QueryAlap.Eof do begin
    if (QueryAlap.FieldByName('GK_SFORH').AsString <= datum2)and
    ((QueryAlap.FieldByName('GK_FORKI').AsString >= datum1)or(QueryAlap.FieldByName('GK_FORKI').AsString = '')) and
    ((QueryAlap.FieldByName('GK_IFORK').AsString >= datum1)or(QueryAlap.FieldByName('GK_IFORK').AsString = '')) then

    //((QueryAlap.FieldByName('GK_FORKI').AsString >= datum1)or(QueryAlap.FieldByName('GK_IFORK').AsString >= datum1)or(QueryAlap.FieldByName('GK_FORKI').AsString + QueryAlap.FieldByName('GK_IFORK').AsString = '')) then
    begin
		  str	:= str + '''' + QueryAlap.FieldByName('GK_RESZ').AsString + ''',';
    end;
		QueryAlap.Next;
	end;
  rendszamok:=str;
	str	:= str + '''' + 'ABC-000' + '''';
	QueryAlap.First;

	{El�sz�r lesz�rj�k a j�ratokat a megadott felt�telek alapj�n}
	sqlstr	:= 'SELECT * FROM JARAT WHERE JA_JKEZD >= '''+dat1+''' AND JA_JKEZD <= '''+dat2+''' '+' AND JA_ZAROKM > 0'+
		' AND JA_RENDSZ IN ('+str+')' ;
    // ide lehetne betenni, hogy csak a norm�l j�ratokat vegye figyelembe, mert a NEM norm�l �gyis szepel mint alj�rat
	if jarat <> '' then begin
    sqlstr	:= sqlstr + ' AND JA_KOD IN ('+jarat+')';   // eleve aposztr�fos list�t kapunk
    end  // if
	else begin
		sqlstr	:= sqlstr + ' AND JA_FAZIS <> 2 ';	// Storn�zott j�ratok nem kellenek
	end;

  // 2017-08-29: csak azok a j�ratok, amelyekhez kapcsolt aut� a j�rat sor�n a mi�nk volt (a DK-hoz �tadott aut�k t�vesen voltak itt)
  if csakSajatAuto then
    sqlstr	:= sqlstr + ' and JA_RENDSZ in (select GK_RESZ from GEPKOCSI where (GK_FORKI='''' or GK_FORKI>JA_JVEGE)) ';

  // vev�, viszonylat sz�r�s 2017-07-28
  if vevokod<> '' then begin
     sqlstr	:= sqlstr + ' and JA_KOD in (select VI_JAKOD from viszony where VI_VEKOD = '+vevokod+ ' '+viszonysql+' ) '
     end;
	sqlstr	:= sqlstr + ' ORDER BY JA_ALKOD ';
	Query_Run (Query1,sqlstr ,true);
	vanadat	:= Query1.RecordCount > 0;
	if not vanadat then begin
		Exit;
	end;
	// Eltessz�k a j�rat adatokat az ellen�rz�shez
	Query1.First;
	i	:= 0;
	while not Query1.Eof do begin
		if Query1.FieldByname('JA_FAZIS').AsString <> '1' then begin
			// Nem ellen�rz�tt j�rat eset�n
			SGEllen.RowCount	:= i+1;
			SGEllen.Cells[0,i]	:= Query1.FieldByName('JA_KOD').AsString;
			SGEllen.Cells[1,i]	:= copy(Query1.FieldByName('JA_ORSZ').AsString + '-' +
				Format('%5.5d',[Query1.FieldByName('JA_ALKOD').AsInteger])+URESSTRING, 1, 9)+'  Rsz.: '+Query1.FieldByName('JA_RENDSZ').AsString;
			SGEllen.Cells[2,i]	:= Query1.FieldByName('JA_RENDSZ').AsString;
			Inc(i);
		end;
		Query1.Next;
	end;
	TablaRendezo( SGEllen, [2], [0], 0, true );
	Query1.First;

	QrGroup1.Expression	:= 'GK_GKKAT';

  // ha b�rmelyik aut�n�l �rv�nytelen, az arra �p�l� �sszegz�sek is �rv�nytelenek
  ervenytelen_fogyasztas_auto:= false;
  ervenytelen_fogyasztas_csoport:= false;
  ervenytelen_fogyasztas_grandtotal:= false;
  ervenytelen_km_auto:= false;
  ervenytelen_km_csoport:= false;
  ervenytelen_km_grandtotal:= false;
end;

procedure TOsszlis2DlgUj.QRBand1BeforePrint(Sender: TQRCustomBand;
	   var PrintBand: Boolean);
var
	i : integer;
  vanrossz: boolean;
  Fojarat_km, Plusz_aljarat_km, Sajat_aljarat_km, Teljes_km: double;
  HolTart, S: string;
begin
 try
  vanrossz:=False;
  HolTart:= '1';
  // ervenytelen_fogyasztas_auto:= false;
  PrintBand:=  pos(QueryAlap.FieldByName('GK_RESZ').AsString,rendszamok)>0;
  if not PrintBand then exit;
	QS2.Caption		:= QueryAlap.FieldByName('GK_RESZ').AsString;
   for i := 1 to MEZOSZAM do begin
     gkertek[i] := 0;
   end;
  HolTart:= '2';
  // Query_Log_Str('BAND1/GKszamolo el�tt', 0, true);
	GkSzamolo(QueryAlap.FieldByName('GK_RESZ').AsString);
  HolTart:= '3';
  // Query_Log_Str('BAND1/GKszamolo ut�n', 0, true);
  QGK15.Enabled:= OssznyilDlg.CheckBox3.Checked;    // alj�ratok figyelembe v�tele be van-e jel�lve
  QGK16.Enabled:= OssznyilDlg.CheckBox3.Checked;    // alj�ratok figyelembe v�tele be van-e jel�lve
  QGK17.Enabled:= OssznyilDlg.CheckBox3.Checked;    // alj�ratok figyelembe v�tele be van-e jel�lve
  Teljes_km:= gkertek[5]+gkertek[12];
  Plusz_aljarat_km:= gkertek[12];
  Fojarat_km:= gkertek[5];
  Sajat_aljarat_km:= gkertek[15];
  // QGK01.Caption	:= IfThen(gkertek[14]=0,'','*')+ Format('%.2f', [gkertek[11]]);  // EUR-ban, nem kell levonni a kompot!
  QGK01.Caption	:= Format('%.2f', [gkertek[11]]);  // EUR-ban, nem kell levonni a kompot!
  if gkertek[14]=0 then QGKKomp1.Enabled:=False else  QGKKomp1.Enabled:=True;

	// QGK02.Caption	:= IfThen(gkertek[14]=0,'','*')+ Format('%.2f', [gkertek[2]]);
  QGK02.Caption	:= Format('%.2f', [gkertek[2]]);
  // nincs k�l�n jel -- if gkertek[14]=0 then QGKKomp2.Enabled:=False else QGKKomp2.Enabled:=True;

	// QGK03.Caption	:= IfThen(gkertek[13]=0,'','*')+ Format('%.0f', [gkertek[3]]);
	// QGK04.Caption	:= Format('%.0f', [gkertek[4]]);
  QGK04.Caption	:= Format('%.2f', [gkertek[2]-gkertek[11]]); // EUR fuvard�j - EUR k�lts�gek
	// QGK05.Caption	:= Format('%.0f', [gkertek[5]]);
  QGK05.Caption	:= Format('%.0f', [Fojarat_km]);
  QGK15.Caption	:= Format('%.0f', [Plusz_aljarat_km]);  // alj�rat km
  QGK16.Caption	:= Format('%.0f', [Sajat_aljarat_km]);  // alj�rat km
  QGK17.Caption	:= Format('%.0f', [Fojarat_km+Sajat_aljarat_km]);
  HolTart:= '4';
  if gkertek[16]=1 then begin
  	QGK06.Caption	:= Format('%.1f', [gkertek[6]]);
  	QGK08.Caption	:= Format('%.1f', [gkertek[8]]);
    end
  else begin
    ervenytelen_fogyasztas_csoport:= true;
    ervenytelen_fogyasztas_grandtotal:= true;
    QGK06.Caption	:= nem_szamolhato_cimke_kozepes;
    QGK08.Caption	:= nem_szamolhato_cimke_kozepes;
    end; // else
  HolTart:= '5';
	QGK07.Caption	:= Format('%.1f', [gkertek[7]]);


  // csak debug c�lokra
  // QGK100.Caption	:= Format('%.0f', [gkertek[1]])+' / '+Format('%.0f', [gkertek[4]]);
  QGK100.Caption	:= Format('%.0f', [gkertek[14]])+' / '+Format('%.0f', [Fojarat_km + Plusz_aljarat_km]);

   // Az intervallumok elen�rz�se
	QGk05.Color	:= clWindow;
   if not Joertek(gkertek[5], 1) then begin
		QGk05.Color	:= ertekszin;
    // vanrossz:= ertekszin=clRed;  csak a Fuvard�j EUR/km kell!
   end;
	QGk09.Color	:= clWindow;
	QGk10.Color	:= clWindow;
	QGk12.Color	:= clWindow;
	QGk13.Color	:= clWindow;
	QGk14.Color	:= clWindow;
  HolTart:= '6';
	// A sz�m�tott �rt�kek
   if Fojarat_km + Plusz_aljarat_km = 0 then begin
       QGK09.Caption	:= '0';
       QGK10.Caption	:= '0';
       // QGK11.Caption	:= '0';
       QGK12.Caption	:= '0';
      end  // if
   else begin
       QGK09.Caption	:= Format('%.3f',  [(gkertek[11]-gkertek[14]) / (Fojarat_km + Plusz_aljarat_km)]);   // EUR-ban
       QGK10.Caption	:= Format('%.3f',  [(gkertek[2]) / (Fojarat_km + Plusz_aljarat_km)]);  // az alj�rat km-eket is belesz�moljuk
       QGK12.Caption	:= Format('%.2f',  [(gkertek[2] - gkertek[11]) / (Fojarat_km + Plusz_aljarat_km)]);  // EUR fuvard�j - EUR k�lts�gek
       if not Joertek((gkertek[11]-gkertek[14]) / (Fojarat_km + Plusz_aljarat_km), 5) then begin
           QGk09.Color	:= ertekszin;
           // vanrossz:= ertekszin=clRed;  csak a Fuvard�j EUR/km kell!
          end;
       //if not Joertek(gkertek[2] / gkertek[5], 6) then begin
       if not Joertek((gkertek[2]) / (Fojarat_km + Plusz_aljarat_km), 6) then begin
           QGk10.Color	:= ertekszin;
           vanrossz:= (ertekszin=clRed) or (ertekszin=clLime);
          end;
       if not Joertek((gkertek[2] - gkertek[11]) / (Fojarat_km + Plusz_aljarat_km), 2) then begin
           QGk12.Color	:= ertekszin;
           // vanrossz:= ertekszin=clRed;  csak a Fuvard�j EUR/km kell!
          end;
      end;  // else
   HolTart:= '7';
   if gkertek[16]=1 then begin  // van �rv�nyes fogyaszt�s
     if Fojarat_km + Sajat_aljarat_km = 0 then begin
       QGK13.Caption	:= '0';
       QGK14.Caption	:= '0';
       end
     else begin
       QGK13.Caption	:= Format('%.2f',  [gkertek[6] * 100 / (Fojarat_km + Sajat_aljarat_km)]);
       QGK14.Caption	:= Format('%.2f',  [gkertek[8] * 100 / (Fojarat_km + Sajat_aljarat_km)]);
       if not Joertek(gkertek[6] * 100 / (Fojarat_km + Sajat_aljarat_km), 3) then begin
          QGk13.Color	:= ertekszin;
           // vanrossz:= ertekszin=clRed;  csak a Fuvard�j EUR/km kell!
          end; // if
       if not Joertek(gkertek[8] * 100 / (Fojarat_km + Sajat_aljarat_km), 4) then begin
           QGk14.Color	:= ertekszin;
           // vanrossz:= ertekszin=clRed;  csak a Fuvard�j EUR/km kell!
           end;
       end // else
     end // if gkertek[16]
    else begin    // nincs �rv�nyes fogyaszt�s
       QGK13.Caption:= nem_szamolhato_cimke_kozepes;
       QGK14.Caption:= nem_szamolhato_cimke_kozepes;
       end; // else

   if gkertek[17]=0 then begin  // ha �rv�nytelen, akkor ezeket fel�l�rjuk
      ervenytelen_km_csoport:= true;
      ervenytelen_km_grandtotal:= true;
      QGK09.Caption	:= nem_szamolhato_cimke_kozepes;
      QGK10.Caption	:= nem_szamolhato_cimke_kozepes;
      QGK12.Caption	:= nem_szamolhato_cimke_kozepes;
      QGK13.Caption	:= nem_szamolhato_cimke_kozepes;
      QGK05.Caption	:= nem_szamolhato_cimke_kozepes;
      QGK15.Caption	:= nem_szamolhato_cimke_kozepes;
      QGK16.Caption	:= nem_szamolhato_cimke_kozepes;
      QGK17.Caption	:= nem_szamolhato_cimke_kozepes;
      end; // if gkertek[17]
  if not ((gkertek[5]=0) and (gkertek[15]=0) and (gkertek[12]=0)) then  // ha nem igaz az, hogy minden km = 0 (nem ment az aut�)
      gkdarabok[1]	:= gkdarabok[1] + 1;  // csak a t�nylegesen ment aut�kra fogunk �tlagokat sz�m�tani
  HolTart:= '8';
   if csakaproblemas and not vanrossz then begin
      PrintBand:=False;
      exit;
   end;
	for i := 1 to MEZOSZAM do begin
    	katerte[i] 	:= katerte[i] + gkertek[i];
      gkertek[i]	:= 0;
      end;
  HolTart:= '9';
 except
   on E: Exception do begin
     S:= 'Hiba az Osszlis2DlgUj.QRBand1BeforePrint-ben, helye: '+HolTart;
     NoticeKi(S);
     HibaKiiro(S);
     end; // on E
    end;  // try-except

end;

procedure TOsszlis2DlgUj.RepBeforePrint(Sender: TCustomQuickRep;
      var PrintReport: Boolean);
var
	i 	: integer;
begin
  	rekszam	:= 1;
   osszeur	:= 0;
   for i := 1 to MEZOSZAM do begin
     	gkertek[i] := 0;
     	katerte[i] := 0;
     	okatert[i] := 0;
   end;
   for i := 1 to 6 do begin
       gkdarabok[i]	:= 0;
   end;
end;

procedure TOsszlis2DlgUj.FormDestroy(Sender: TObject);
begin
	FogyasztasDlg.Destroy;
end;

procedure TOsszlis2DlgUj.QRGroup1BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
   QRLabel48.Enabled:=csakaproblemas;
   QrLabel27.Caption	:= QueryAlap.FieldByName('GK_GKKAT').AsString +' ('+ EgyebDlg.Read_Szgrid('340', QueryAlap.FieldByName('GK_GKKAT').AsString)+')';
   QrLabel15.Caption	:= '';
   QrLabel19.Caption	:= '';
   QrLabel21.Caption	:= '';
   QrLabel26.Caption	:= '';
   QrLabel23.Caption	:= '';
   QrLabel43.Caption	:= '';
   QrLabel45.Caption	:= '';
   QRLabel49.Enabled:= OssznyilDlg.CheckBox3.Checked;    // alj�ratok figyelembe v�tele be van-e jel�lve
   QRLabel24.Enabled:= OssznyilDlg.CheckBox3.Checked;    // alj�ratok figyelembe v�tele be van-e jel�lve
   Query_Run(Query6, 'SELECT * FROM KATINTER WHERE KI_GKKAT = '''+QueryAlap.FieldByName('GK_GKKAT').AsString+''' ');
   if Query6.RecordCount > 0 then begin
       QrLabel19.Caption	:= Query6.FieldByName('KI_KMTOL').AsString + ' - ' + Query6.FieldByName('KI_KMEIG').AsString;
       // QrLabel21.Caption	:= Query6.FieldByName('KI_NYTOL').AsString + ' - ' + Query6.FieldByName('KI_NYEIG').AsString;
       QrLabel21.Caption	:= Query6.FieldByName('KI_NYTOL_EUR').AsString + ' - ' + Query6.FieldByName('KI_NYEIG_EUR').AsString;
       QrLabel26.Caption	:= Query6.FieldByName('KI_ATTOL').AsString + ' - ' + Query6.FieldByName('KI_ATLIG').AsString;
       QrLabel23.Caption	:= Query6.FieldByName('KI_MTTOL').AsString + ' - ' + Query6.FieldByName('KI_MTAIG').AsString;
       QrLabel43.Caption	:= Query6.FieldByName('KI_EUTOL').AsString + ' - ' + Query6.FieldByName('KI_EURIG').AsString;
       // QrLabel45.Caption	:= Query6.FieldByName('KI_KTTOL').AsString + ' - ' + Query6.FieldByName('KI_KTGIG').AsString; -- ez m�g HUF
       QrLabel45.Caption	:= Query6.FieldByName('KI_KTTOL_EUR').AsString + ' - ' + Query6.FieldByName('KI_KTGIG_EUR').AsString;
       QrLabel15.Caption	:= Query6.FieldByName('KI_MEGJE').AsString;
   end;
end;


procedure TOsszlis2DlgUj.QRBand4BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
	i : integer;
  Fojarat_km, Plusz_aljarat_km, Sajat_aljarat_km, Teljes_km: double;
begin

  Teljes_km:= katerte[5]+katerte[12];
  Plusz_aljarat_km:= katerte[12];
  Fojarat_km:= katerte[5];
  Sajat_aljarat_km:= katerte[15];

	QrLabel28.Caption	:= QueryAlap.FieldByName('GK_GKKAT').AsString + ' �ssz:';

  // QK01.Caption	:= IfThen(katerte[14]=0,'','*')+ Format('%.2f', [katerte[11]]);  // nem kell levonni a kompot
  QK01.Caption	:= Format('%.2f', [katerte[11]]);  // nem kell levonni a kompot
  if katerte[14]=0 then QKKomp1.Enabled:=False else QKKomp1.Enabled:=True;
	// QK02.Caption	:= IfThen(katerte[14]=0,'','*')+ Format('%.2f', [katerte[2]]);
	QK02.Caption	:= Format('%.2f', [katerte[2]]);
  // nincs k�l�n jel -- if katerte[14]=0 then QKKomp2.Enabled:=False else QKKomp2.Enabled:=True;

	// QK03.Caption	:= IfThen(katerte[13]=0,'','*')+ Format('%.0f', [katerte[3]]);
	// QK04.Caption	:= Format('%.4f', [katerte[4]]);
  QK04.Caption	:= Format('%.2f', [katerte[2]-katerte[11]]);
	QK05.Caption	:= Format('%.0f', [Fojarat_km]);
  QK15.Caption		:= Format('%.0f', [Plusz_aljarat_km]);
  QK16.Caption		:= Format('%.0f', [Sajat_aljarat_km]);
  QK17.Caption		:= Format('%.0f', [Fojarat_km+Sajat_aljarat_km]);

	QK18.Caption	:= Format('%.0f', [Fojarat_km / gkdarabok[1]]);
  QK19.Caption	:= Format('%.0f', [Plusz_aljarat_km / gkdarabok[1]]);
  QK20.Caption	:= Format('%.0f', [Sajat_aljarat_km / gkdarabok[1]]);
  QK21.Caption	:= Format('%.0f', [(Fojarat_km + Sajat_aljarat_km) / gkdarabok[1]]);

	QK07.Caption	:= Format('%.0f', [katerte[7]]);
  if ervenytelen_fogyasztas_csoport then begin
  	QK06.Caption	:= nem_szamolhato_cimke_kozepes;
  	QK08.Caption	:= nem_szamolhato_cimke_kozepes;
    end
  else begin
  	QK06.Caption	:= Format('%.0f', [katerte[6]]);
  	QK08.Caption	:= Format('%.1f', [katerte[8]]);
    end;

	// A sz�m�tott �rt�kek
   if Teljes_km = 0 then begin
       QK09.Caption	:= '0';
       QK10.Caption	:= '0';
       // QK11.Caption	:= '0';
       QK12.Caption	:= '0';
   end else begin
       // QK09.Caption	:= Format('%.2f', [-1 * (katerte[1]-katerte[13]) / katerte[5]]);
       QK09.Caption	:= Format('%.3f', [(katerte[11]-katerte[14]) / Teljes_km]);
       QK10.Caption	:= Format('%.3f', [(katerte[2]) / Teljes_km]);
       QK12.Caption	:= Format('%.2f', [(katerte[2]-katerte[11]) / Teljes_km]);
   end;
   if Fojarat_km + Sajat_aljarat_km = 0 then begin
       QK13.Caption	:= '0';
       QK14.Caption	:= '0';
   end else begin
      if ervenytelen_fogyasztas_csoport then begin
      	QK13.Caption	:= nem_szamolhato_cimke_kozepes;
      	QK14.Caption	:= nem_szamolhato_cimke_kozepes;
        end
      else begin
       QK13.Caption	:= Format('%.2f', [katerte[6] * 100 / (Fojarat_km + Sajat_aljarat_km)]);
       QK14.Caption	:= Format('%.2f', [katerte[8] * 100 / (Fojarat_km + Sajat_aljarat_km)]);
       end;  // else
   end;

   if ervenytelen_km_csoport then begin  // ha �rv�nytelen, akkor ezeket fel�l�rjuk
      QK09.Caption	:= nem_szamolhato_cimke_kozepes;
      QK10.Caption	:= nem_szamolhato_cimke_kozepes;
      QK12.Caption	:= nem_szamolhato_cimke_kozepes;
      QK13.Caption	:= nem_szamolhato_cimke_kozepes;
      QK05.Caption	:= nem_szamolhato_cimke_kozepes;
      QK15.Caption	:= nem_szamolhato_cimke_kozepes;
      QK16.Caption	:= nem_szamolhato_cimke_kozepes;
      QK17.Caption	:= nem_szamolhato_cimke_kozepes;
      end; // if ervenytelen_km_csoport

   // A g�pkocsidarabok lekezel�se
   QrLabel46.Caption	:= IntToStr(gkdarabok[1])+' db, �tlag:';
   // Ha �tlagot jelen�ten�nk meg
   // QK12A.Caption		:= Format('%.0f km/db', [katerte[5] / gkdarabok[1]]);

   gkdarabok[2]		:= gkdarabok[2] + gkdarabok[1];
   gkdarabok[1]		:= 0;
	for i := 1 to MEZOSZAM do begin
   	okatert[i] 	:= okatert[i] + katerte[i];
       katerte[i]	:= 0;
   end;
   ervenytelen_fogyasztas_csoport:= false;  // a k�vetkez� csoport m�g lehet �rv�nyes
   ervenytelen_km_csoport:= false;  // a k�vetkez� csoport m�g lehet �rv�nyes
end;

function TOsszlis2DlgUj.GetJaratKoltsegFogyasztas(JAKOD: string; KellKoltseg, KellFogyasztas: boolean): TJaratKoltsegFogyasztas;
// Elv: a k�lts�geket is pozit�v sz�mk�nt kezelj�k
var
   EURDat, sof1, fogyi, voltsofor, Hibauzenet, HibaHely: string;
   Q1, Q2: TADOQuery;  // a rekurz�v h�v�s miatt kell saj�t TADOQuery (j�, v�g�l nem lett rekurz�v)
   feldij, EURKoltseg, HUFKoltseg, Jarat_EUR_arfolyam: double;
   uzkolt, uzmegtak	: double;
begin
   // Query_Log_Str('GetJaratKoltsegFogyasztas / 1', 0, true);
   Result.OsszHUFKoltseg:= 0;
   Result.OsszEurKoltseg:= 0;
   Result.OsszHUFNemkellKoltseg:= 0;
   Result.OsszEurNemkellKoltseg:= 0;
   Result.TenyFogyasztas:= 0;
   Result.TervFogyasztas:= 0;
   Result.Megtakaritas:= 0;
   Result.OsszKm:= 0;

 	 Q1:= TADOQuery.Create(nil);
	 Q1.Tag := QUERY_ADAT_TAG;
	 EgyebDlg.SetADOQueryDatabase(Q1);
 	 Q2:= TADOQuery.Create(nil);
	 Q2.Tag := QUERY_ADAT_TAG;
	 EgyebDlg.SetADOQueryDatabase(Q2);
   HibaHely:='01';
   try
     try
      // Query_Log_Str('GetJaratKoltsegFogyasztas / 2', 0, true);
      Query_Run(Q1, 'SELECT JA_JKEZD, JA_OSSZKM FROM JARAT WHERE JA_KOD='+ JAKOD);

       feldij	:= StringSzam(EgyebDlg.Read_SZGrid('CEG', '301'));
       // A j�rathoz tartoz� fogyaszt�si adatok kisz�m�t�sa
       FogyasztasDlg.ClearAllList;
       // Query_Log_Str('GetJaratKoltsegFogyasztas / 3', 0, true);
       FogyasztasDlg.JaratBetoltes(JAKOD);
       // Query_Log_Str('GetJaratKoltsegFogyasztas / 4', 0, true);
       FogyasztasDlg.CreateTankLista;
       // Query_Log_Str('GetJaratKoltsegFogyasztas / 5', 0, true);
       FogyasztasDlg.CreateAdBlueLista;
       // Query_Log_Str('GetJaratKoltsegFogyasztas / 6', 0, true);
       FogyasztasDlg.Szakaszolo;
       // Query_Log_Str('GetJaratKoltsegFogyasztas / 7', 0, true);
       EURDat:= Q1.FieldByName('JA_JKEZD').AsString;
       Jarat_EUR_arfolyam:= EgyebDlg.ArfolyamErtek('EUR', EURDat, true);

       if KellKoltseg then begin  // *****************************************************
         {A dolgoz�i adatok bet�tele}
         HibaHely:='02';
         voltsofor	:= '';
         Query_Run(Q2, 'SELECT JS_SOFOR, JS_SZAKM, JS_JADIJ, JS_EXDIJ, JS_FELRA FROM JARSOFOR WHERE JS_JAKOD = '''+JAKOD+''' ORDER BY JS_SORSZ ');
         if Q2.RecordCount > 0 then begin
          while not Q2.Eof do begin
               sof1		:= Query_Select('DOLGOZO', 'DO_KOD', Q2.FieldByname('JS_SOFOR').AsString , 'DO_NAME');
               if sof1 <> '' then begin
                   // Sof�r fogyaszt�si adatokat
                   fogyi	:= '';
                   if Pos (Q2.FieldByname('JS_SOFOR').AsString, voltsofor) < 1 then begin
                   	  voltsofor	:= voltsofor + Q2.FieldByname('JS_SOFOR').AsString + ';';
                      if  Round(Q1.FieldByname('JA_OSSZKM').AsFloat ) > 0  then begin
							          uzmegtak:= FogyasztasDlg.GetSoforMegtakaritas(Q2.FieldByname('JS_SOFOR').AsString);
							          // Az �zemanyag megtakar�t�s k�lts�g�nek kisz�m�t�sa �s megjelen�t�se
							          uzkolt			 := 0;
							          if uzmegtak <> 0 then begin
								          uzkolt	 	 := uzmegtak * GetApehUzemanyag(Q1.FieldByName('JA_JKEZD').AsString, 1);
                          end;
                        Result.OsszHUFKoltseg:= Result.OsszHUFKoltseg + uzkolt;
                        end;
                      end;
                   Result.OsszHUFKoltseg:= Result.OsszHUFKoltseg + 	StringSzam(Q2.FieldByname('JS_SZAKM').AsString) * StringSzam(Q2.FieldByname('JS_JADIJ').AsString);
                   Result.OsszHUFKoltseg:= Result.OsszHUFKoltseg +  StringSzam(Q2.FieldByname('JS_EXDIJ').AsString);
                   Result.OsszHUFKoltseg:= Result.OsszHUFKoltseg +  StringSzam(Q2.FieldByname('JS_FELRA').AsString)*feldij;
               end; // if
               EURKoltseg:= Result.OsszHUFKoltseg / Jarat_EUR_arfolyam; // ossz-ben m�g csak ez van, j�ratonk�nt null�zzuk
               Result.OsszEurKoltseg:= Result.OsszEurKoltseg + EURKoltseg;
               Q2.Next;
           end;  // while
          end; // Q2.RecordCount

      	// Bet�ltj�k a k�lts�geket
        // Query_Log_Str('GetJaratKoltsegFogyasztas / 8', 0, true);
        HibaHely:='03';
        if Query_Run (Q2,'SELECT KS_VALNEM, KS_OSSZEG, KS_ARFOLY, KS_LEIRAS FROM KOLTSEG WHERE KS_JAKOD = '''+JAKOD+''' '+' AND KS_TETAN = 0 '+
  			 ' AND KS_TIPUS <> 0 AND KS_TIPUS <> 3  AND KS_TIPUS <> 2 ' ,true) then begin		// Az �zemanyag k�lts�get + h�t�t nem vonjuk le, mert azt ut�na sz�moljuk
  			  while not Q2.EOF do begin
           if Q2.FieldByname('KS_VALNEM').AsString='HUF' then begin
             HUFKoltseg:= Q2.FieldByname('KS_OSSZEG').AsFloat;     // KS_AFAERT mindig 0 az adatb�zisban...
             EURKoltseg:= HUFKoltseg / Jarat_EUR_arfolyam; //   + Query3.FieldByname('KS_AFAERT').AsFloat;  KS_AFAERT mindig 0 az adatb�zisban...
             end
           else begin // deviz�s sor
             HUFKoltseg:= Q2.FieldByname('KS_OSSZEG').AsFloat * Q2.FieldByname('KS_ARFOLY').AsFloat; // Query3.FieldByname('KS_AFAERT').AsFloat;
             EURKoltseg:= Q2.FieldByname('KS_OSSZEG').AsFloat * Q2.FieldByname('KS_ARFOLY').AsFloat / Jarat_EUR_arfolyam; // EUR-ba konvert�ljuk, b�rmilyen deviza volt
             end;
           if OssznyilDlg.CheckBox2.Checked and ktsg_nemkell(Q2.FieldByname('KS_LEIRAS').AsString) then begin   // Bizonyos k�lts�geket nem kell figyelembe venni.
  			  	Result.OsszHUFNemkellKoltseg	:= Result.OsszHUFNemkellKoltseg + HUFKoltseg;
  		      Result.OsszEurNemkellKoltseg	:= Result.OsszEurNemkellKoltseg + EURKoltseg;
            end;
           Result.OsszHUFKoltseg:= Result.OsszHUFKoltseg + HUFKoltseg;
           Result.OsszEurKoltseg:= Result.OsszEurKoltseg + EURKoltseg;
  				 Q2.Next;
  			  end;
  		  end;
        // Query_Log_Str('GetJaratKoltsegFogyasztas / 9', 0, true);
        HibaHely:='04';
        HUFKoltseg:= FogyasztasDlg.GetKoltseg;
    		Result.OsszHUFKoltseg:= Result.OsszHUFKoltseg + HUFKoltseg;
        Result.OsszEurKoltseg:= Result.OsszEurKoltseg + HUFKoltseg/Jarat_EUR_arfolyam;

        // Sz�m�tott AD Blue
        // Query_Log_Str('GetJaratKoltsegFogyasztas / 10', 0, true);
        HibaHely:='05';
    		HUFKoltseg 	:= FogyasztasDlg.GetBlueKoltseg;
    		Result.OsszHUFKoltseg:= Result.OsszHUFKoltseg + HUFKoltseg;
        Result.OsszEurKoltseg:= Result.OsszEurKoltseg + HUFKoltseg/Jarat_EUR_arfolyam;

    		// A h�t� tankol�sok berak�sa
        // Query_Log_Str('GetJaratKoltsegFogyasztas / 11', 0, true);
        HibaHely:='06';
    		Query_Run (Q2,'SELECT JP_POREN, JP_UZEM1, JP_UZEM2 FROM JARPOTOS WHERE JP_JAKOD = '''+JAKOD+''' ',true);
    		while not Q2.EOF do begin
    			HUFKoltseg	:= GetHutoOsszeg(Q2.FieldByname('JP_POREN').AsString, Q2.FieldByname('JP_UZEM1').AsString, Q2.FieldByname('JP_UZEM2').AsString);
    			Result.OsszHUFKoltseg:= Result.OsszHUFKoltseg + HUFKoltseg;
          Result.OsszEurKoltseg:= Result.OsszEurKoltseg + HUFKoltseg/Jarat_EUR_arfolyam;
    			Q2.Next;
    		  end;  // while
    		Q2.Close;
      end;  // if KellKoltseg *******************************************

    // fogyaszt�s �s km adatok
    // Query_Log_Str('GetJaratKoltsegFogyasztas / 12', 0, true);
    if KellFogyasztas then begin
      Result.OsszKm:= FogyasztasDlg.GetOsszKm;
      Result.TenyFogyasztas:= FogyasztasDlg.GetTenyFogyasztas;
      Result.ErvenyesTenyFogyasztas := FogyasztasDlg.ErvenyesTenyFogyasztas;
      Result.TervFogyasztas:= FogyasztasDlg.GetTervFogyasztas;
      // Result.Megtakaritas:= FogyasztasDlg.GetMegtakaritas;
      Result.Megtakaritas:= Result.TervFogyasztas - Result.TenyFogyasztas;  // gyorsabb �gy
      end;
    // Query_Log_Str('GetJaratKoltsegFogyasztas / 13', 0, true);
    except
       on E: exception do begin
          Hibauzenet:= 'Hiba a "GetJaratKoltsegFogyasztas('+JAKOD+')" elj�r�sban ('+HibaHely+'): '+E.Message;
          NoticeKi(Hibauzenet);
          Hibakiiro(Hibauzenet);
          end; // on E
       end;  // try-except
    finally
      Q1.Free;
      Q2.Free;
      end;  // try-finally
end;

procedure TOsszlis2DlgUj.GkSzamolo(rsz	: string);
var
  	ertek, szossz, oertek, okolts, okolts2, eurokolts2, ofuvar, euralap, jarateurdij: double;
  	EURDij, EURKoltseg, OsszEurKoltseg, Jarat_EUR_arfolyam, eurarf, arfoly, ertek2: double;
    sajat_fojarat_km, sajat_aljarat_km: double;
    OsszTenyFogyasztas, OsszTervFogyasztas, OsszMegtakaritas, OsszPluszAljaratKm: double;
   	sqlstr, ardat, vevonev, szamlaszam, EURDat, Devizanem, FAJKO, S, HolTart: string;
    JaratkoltsegRec: TJaratKoltsegFogyasztas;
    res: E_res;
    Elojel, i: integer;
    Ervenytelen_fogyasztas, Ervenytelen_km: boolean;
begin
  // if rsz = 'MUJ-165' then begin
  //     NoticeKi('MUJ-165)');
  //    end;

	// A g�pkocsihoz tartoz� �rt�kek kisz�m�t�sa
   // Query_Log_Str('GkSzamolo / 1', 0, true);

 try


	 sqlstr	:= 'SELECT JA_KOD, JA_JKEZD, JA_FAJKO, JA_ALKOD FROM JARAT WHERE JA_JKEZD >= '''+dat1+''' AND JA_JKEZD <= '''+dat2+''' '+
		' /* AND JA_ZAROKM > 0*/ AND JA_RENDSZ = '''+rsz+'''';
   // az alj�ratokat is sz�molnunk kell a fogyaszt�s �s a km miatt, de ezekn�l a k�lts�get nem fogjuk sz�molni
   //  sqlstr	:= sqlstr + ' AND JA_FAJKO = 0 ' ;  // csak a f�j�ratokat n�zz�k, majd hozz�sz�moljuk a m�sok �ltal hozz�tette alj�ratokat

	 if jarat <> '' then begin
      sqlstr	:= sqlstr + ' AND JA_KOD IN ('+jarat+')';
	    end
   else begin
		sqlstr	:= sqlstr + ' AND JA_FAZIS <> 2 ';	// Storn�zott j�ratok nem kellenek
    sqlstr	:= sqlstr + ' AND JA_FAJKO in (0,1) ';	// Rezsi j�ratok nem kellenek
	  end;
   // vev�, viszonylat sz�r�s 2017-07-28
  if l_vevokod<> '' then begin
     sqlstr	:= sqlstr + ' and JA_KOD in (select VI_JAKOD from viszony where VI_VEKOD = '+l_vevokod+ ' '+l_viszonysql+' ) '
     end;
   sqlstr	:= sqlstr + ' ORDER BY JA_ALKOD ';
   HolTart:= '1';
   Query_Run (Query1,sqlstr ,true);
   if Query1.RecordCount < 1 then begin  // ha nincs egyetlen j�rat sem, akkor minden 0 lesz
      for i:= 1 to 15 do
          gkertek[i]:= 0;
      gkertek[16]:= 1;   // �rv�nyes a fogyazt�s
      gkertek[17]:= 1;  // �rv�nyes a km
   	  Exit;
      end;
   okolts	:= 0;
   okolts2:=0;
   eurokolts2:=0;
   ofuvar	:= 0;
   oertek	:= 0;
   ossz	:= 0;
   ossz2:=0;    // a figyelembe nem veend� k�lts�gek (OssznyilDlg.CheckBox2)
   eurossz2:=0;
   osszeur	:= 0;
   OsszEurKoltseg:= 0;
   // ertekek[1]	:= 0;
   sajat_fojarat_km:=0;
   sajat_aljarat_km:=0;
   OsszTenyFogyasztas:= 0;
   OsszTervFogyasztas:= 0;
   OsszMegtakaritas:= 0;
   OsszPluszAljaratKm:= 0;
   Ervenytelen_fogyasztas:= False;
   Ervenytelen_km:= False;
   // DEBUG
   Query_Log_Str('Sz�m�t�s kezdete: '+rsz, 0, true, false);
   HolTart:= '2';
   while not Query1.EOF do begin
       // Query_Log_Str('GkSzamolo / 2', 0, true);
       jaratsz:= Query1.FieldByName('JA_KOD').AsString;
       Query_Log_Str('=== J�ratk�d: '+jaratsz, 0, true, false);
       FAJKO:= Query1.FieldByName('JA_FAJKO').AsString;  // j�rat fajta, 0 = f�j�rat, a t�bbi alj�rat
       HolTart:= '3';
       if FAJKO = '0' then
          JaratkoltsegRec:= GetJaratKoltsegFogyasztas(jaratsz, True, True);  // f�j�rat: kell k�lts�get is sz�molni
       // else
       if FAJKO = '1' then  // alj�rat
          JaratkoltsegRec:= GetJaratKoltsegFogyasztas(jaratsz, False, True); // alj�rat: csak a km �s fogyaszt�s kell
       if JaratkoltsegRec.OsszKm < 0  then begin // pl. nincs kit�ltve m�g a z�r� km
          Ervenytelen_km:= True;
          Query_Log_Str('*** Ervenytelen_km', 0, true, false);
          end;
       // elvileg ilyen nem lehet benne, az SQL-ben kisz�rt�k
       {if FAJKO = '2' then begin // rezsi j�rat
          JaratkoltsegRec.OsszHUFKoltseg:=0;
          JaratkoltsegRec.OsszEurKoltseg:=0;
          JaratkoltsegRec.OsszHUFNemkellKoltseg:=0;
          JaratkoltsegRec.OsszEurNemkellKoltseg:=0;
          JaratkoltsegRec.TenyFogyasztas:=0;
          JaratkoltsegRec.TervFogyasztas:=0;
          JaratkoltsegRec.Megtakaritas:=0;
          JaratkoltsegRec.OsszKm:=0;
          JaratkoltsegRec.ErvenyesTenyFogyasztas:=True;
          end;}
       HolTart:= '4';
       {if rsz = 'MUJ-165' then begin
            NoticeKi('Jarat'+jaratsz+ '='+FormatFloat('0.00', JaratkoltsegRec.OsszEURKoltseg)+' OsszEUR='+FormatFloat('0.00', OsszEurKoltseg));
            end;}
       // ========== Debug ============
       // S:='J�rat:'+Query1.FieldByName('JA_ALKOD').AsString;
       // S:=S+' HUF k�lts�g: '+FormatFloat('0', JaratkoltsegRec.OsszHUFKoltseg);
       // S:=S+' HUF NEMKELLk�lts�g: '+FormatFloat('0', JaratkoltsegRec.OsszHUFNemkellKoltseg);
       // S:=S+' EUR k�lts�g: '+FormatFloat('0', JaratkoltsegRec.OsszEURKoltseg);
       // S:=S+' EUR NEMKELLk�lts�g: '+FormatFloat('0', JaratkoltsegRec.OsszEurNemkellKoltseg);
       // Excel-hez:
       S:=Query1.FieldByName('JA_ALKOD').AsString+';';
       S:=S+FormatFloat('0', JaratkoltsegRec.OsszHUFKoltseg)+';';
       S:=S+FormatFloat('0', JaratkoltsegRec.OsszHUFNemkellKoltseg)+';';
       S:=S+FormatFloat('0', JaratkoltsegRec.OsszEURKoltseg)+';';
       S:=S+FormatFloat('0', JaratkoltsegRec.OsszEurNemkellKoltseg)+';';
       if FAJKO = '0' then
          S:=S+FormatFloat('0', JaratkoltsegRec.OsszKm)+';;;'; // els� mez� a f�j�rat km. oszlop
       // else
       if FAJKO = '1' then
          S:=S+';'+FormatFloat('0', JaratkoltsegRec.OsszKm)+';;';  // m�sodik mez� az alj�rat km. oszlop
       // DEBUG
       Query_Log_Str(S, 0, true, false);
       HolTart:= '5';
       // =============================
       okolts	:= okolts + JaratkoltsegRec.OsszHUFKoltseg;
       OsszEurKoltseg:= OsszEurKoltseg + JaratkoltsegRec.OsszEURKoltseg;
       okolts2	:= okolts2 + JaratkoltsegRec.OsszHUFNemkellKoltseg;
       eurokolts2	:= eurokolts2 + JaratkoltsegRec.OsszEurNemkellKoltseg;
       if FAJKO = '0' then
          sajat_fojarat_km := sajat_fojarat_km + JaratkoltsegRec.OsszKm;
       // else
       if FAJKO = '1' then
          sajat_aljarat_km := sajat_aljarat_km + JaratkoltsegRec.OsszKm;
       OsszTervFogyasztas	:= OsszTervFogyasztas + JaratkoltsegRec.TervFogyasztas;
       if JaratkoltsegRec.ErvenyesTenyFogyasztas then begin
         OsszTenyFogyasztas	:= OsszTenyFogyasztas + JaratkoltsegRec.TenyFogyasztas;
         OsszMegtakaritas	:= OsszMegtakaritas + JaratkoltsegRec.Megtakaritas;
         end
       else begin
         Ervenytelen_fogyasztas:= true;
         end;
       S:=Query1.FieldByName('JA_ALKOD').AsString+'_FOGY;;;;;;;;;;;'+FormatFloat('0.00', JaratkoltsegRec.TenyFogyasztas)+';'+FormatFloat('0.00', JaratkoltsegRec.Megtakaritas)+';';
       // DEBUG
       Query_Log_Str(S, 0, true, false);
       HolTart:= '6';
       EURDat:= Query1.FieldByName('JA_JKEZD').AsString;
       Jarat_EUR_arfolyam:= EgyebDlg.ArfolyamErtek('EUR', EURDat, true);
       //if Jarat_EUR_arfolyam=0 then
       //   NoticeKi('EUR �rfolyam 0! D�tum:'+ EURDat);

    	// Fuvard�jak sz�m�t�sa -----------------------------------
      // Query_Log_Str('GkSzamolo / 3', 0, true);
    	ertek			:= 0;
      jarateurdij:=0;
    	vevonev  	:= '';
    	szamlaszam  := '';
		  Query_Run (Query2,'SELECT * FROM VISZONY WHERE VI_JAKOD = '''+jaratsz+''' ',true);
      HolTart:= '7';
    	while not Query2.EOF do begin
          //szossz	:= Query2.FieldByname('VI_FDEXP').AsFloat * Query2.FieldByname('VI_ARFOLY').AsFloat;
          {Megkeress�k a sz�ml�t a viszonyhoz}
          szossz:=0;
          if Query2.FieldByname('VI_SAKOD').AsString <> '' then begin
             {Van hozz� sz�mla}
             Query_Run (Query3,'SELECT * FROM SZFEJ WHERE SA_UJKOD = '+Query2.FieldByname('VI_UJKOD').AsString,true);
             if Query3.RecordCount > 0 then begin
                   szossz		:= Query3.FieldByname('SA_OSSZEG').AsFloat;
                   ardat		:= Query3.FieldByName('SA_TEDAT').AsString;
                   euralap 		:= 0;
                   {A valut�s �rt�k meghat�roz�sa}
                   if Query_Run (Query3,'SELECT * FROM SZSOR WHERE SS_UJKOD = '+Query2.FieldByname('VI_UJKOD').AsString,true) then begin
                      	while not Query3.EOF do begin
                         if Query3.FieldByName('SS_VALERT').AsFloat <> 0 then begin
                            if Query3.FieldByName('SS_EGYAR').AsFloat<0 then
                                Elojel:= -1  // sztorn� sz�mla
                            else Elojel:= 1;
                            Devizanem:= Query3.FieldByName('SS_VALNEM').AsString;
                             if Devizanem = 'EUR' then begin
                                 EURDij:= Elojel * Query3.FieldByName('SS_VALERT').AsFloat;
                                 end
                             else begin   // HUF-on �t konvert�ljuk
                                 ardat:= Query3.FieldByName('SS_ARDAT').AsString;
                                 if trim(ardat)='' then ardat:= Query3.FieldByname('SA_TEDAT').AsString;  // ha esetleg nincs kit�ltve az SS_ARDAT
                                 EURDij:= Elojel * Query3.FieldByName('SS_VALERT').AsFloat * EgyebDlg.ArfolyamErtek(Devizanem, ardat, true) / EgyebDlg.ArfolyamErtek('EUR', ardat, true);
                                 end;
                            euralap	:= euralap  + EURDij;
                            end;  // if  SS_VALERT <> 0
                         Query3.Next;
                         end;  // while
                      end;  // if Query_run
                   jarateurdij	:= jarateurdij + euralap;
                end;
          end;
          ertek	:= ertek +  szossz;
          Query2.Next;
       end;
       HolTart:= '8';
       Query2.Close;
       S:=Query1.FieldByName('JA_ALKOD').AsString+'_DIJ;;;;;;;'+FormatFloat('0.00', jarateurdij)+';';
       // debug
       Query_Log_Str(S, 0, true, false);
       ofuvar		:= ofuvar + ertek;
       oertek		:= oertek + ertek + ossz;
       osszeur	:= osszeur + jarateurdij;
       ossz		:= 0;
       ossz2	:= 0;
       eurossz2	:= 0;
    	// Plusz alj�ratok sz�m�t�sa -----------------------------------
       // Query_Log_Str('GkSzamolo / 4', 0, true);
       if (FAJKO = '0') and OssznyilDlg.CheckBox3.Checked then begin      // alj�ratok figyelembe v�tele, csak f�j�ratn�l
         // debug if jaratsz = '44804' then
         //   NoticeKi('44804');
         HolTart:= '9';
         res:= Jarat_Egyeb_km(jaratsz);
         HolTart:= '10';
         if not(res.ervenyes_fogyasztas) then
            Ervenytelen_fogyasztas:=True;
         S:=Query1.FieldByName('JA_ALKOD').AsString+'+ALJ;';
         S:=S+FormatFloat('0', res.oktg)+';';
         S:=S+'0'+';';
         S:=S+FormatFloat('0', res.oeurktg)+';';
         S:=S+'0'+';';
         // S:=S+';;'+FormatFloat('0', res.okm)+';'; // 3. oszlop a OsszPluszAljaratKm
         S:=S+';;;'+FormatFloat('0', res.okm)+';'; // 3. oszlop a OsszPluszAljaratKm
         // DEBUG
         Query_Log_Str(S, 0, true, false);
         OsszPluszAljaratKm	:= OsszPluszAljaratKm + res.okm;
         okolts:=okolts + res.oktg;
         {if rsz = 'MUJ-165' then begin
            NoticeKi('BEFORE ALJaratok'+jaratsz+ '='+FormatFloat('0.00', res.oeurktg)+', Arf='+FormatFloat('0.00', Jarat_EUR_arfolyam)+' OsszEUR='+FormatFloat('0.00', OsszEurKoltseg));
            end;}
         // OsszEurKoltseg:= OsszEurKoltseg + res.oeurktg / Jarat_EUR_arfolyam;
         OsszEurKoltseg:= OsszEurKoltseg + res.oeurktg;
         {if rsz = 'MUJ-165' then begin
            NoticeKi('AFTER ALJaratok'+jaratsz+ '='+FormatFloat('0.00', res.oeurktg)+', Arf='+FormatFloat('0.00', Jarat_EUR_arfolyam)+' OsszEUR='+FormatFloat('0.00', OsszEurKoltseg));
            end;}
         HolTart:= '11';
         end;
       // Query_Log_Str('GkSzamolo / 5', 0, true);
       Query1.Next;
   end;
   // DEBUG
   Query_Log_Str('Sz�m�t�s v�ge: '+rsz, 0, true, false);
   gkertek[1]  := okolts;    // minden k�lts�g
   gkertek[2]  := osszeur;  // �sszes fuvard�j EUR
   gkertek[3]  := ofuvar;
   gkertek[4]  := ofuvar+okolts;
   gkertek[5]  := sajat_fojarat_km;  // csak a f�j�rat km megy bele
   gkertek[6]  := OsszTenyFogyasztas;
   if Ervenytelen_fogyasztas then
     gkertek[16]  := 0
   else gkertek[16]  := 1;  // �rv�nyes a fogyaszt�s
   if Ervenytelen_km then
     gkertek[17]  := 0
   else gkertek[17]  := 1; // �rv�nyes a km
   gkertek[7]  := OsszTervFogyasztas;
   if sajat_fojarat_km + sajat_aljarat_km = 0 then begin
   	gkertek[8]  	:= 0;
    end
   else begin
   	if Ervenytelen_fogyasztas then
     gkertek[8]  := -1
    else gkertek[8]:= OsszMegtakaritas;
    end;
   gkertek[11]  := OsszEurKoltseg;    // minden k�lts�g EUR-ban
   gkertek[13] := okolts2;   // a figyelembe nem veend� k�lts�gek (OssznyilDlg.CheckBox2)
   gkertek[14] := eurokolts2;
   gkertek[12] := OsszPluszAljaratKm;      //a plusz alj�ratok km-ei
   gkertek[15] := sajat_aljarat_km;
 except
   on E: Exception do begin
     S:= 'Hiba az Osszlis2DlgUj.GkSzamolo-ban, helye: '+HolTart;
     NoticeKi(S);
     HibaKiiro(S);
     end; // on E
    end;  // try-except

end;

procedure TOsszlis2DlgUj.QRBand5BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
  Fojarat_km, Plusz_aljarat_km, Sajat_aljarat_km, Teljes_km: double;
begin
  Teljes_km:= okatert[5]+okatert[12];
  Plusz_aljarat_km:= okatert[12];
  Fojarat_km:= okatert[5];
  Sajat_aljarat_km:= okatert[15];

	// QV01.Caption	:= IfThen(okatert[14]=0,'','*')+  Format('%.0f', [okatert[11]]);
  QV01.Caption	:= Format('%.0f', [okatert[11]]);
  if okatert[14]=0 then QVKomp1.Enabled:=False else QVKomp1.Enabled:=True;

	// QV02.Caption	:= IfThen(okatert[14]=0,'','*')+  Format('%.2f', [okatert[2]]);
  QV02.Caption	:= Format('%.2f', [okatert[2]]);
  // nincs k�l�n jel -- if okatert[14]=0 then QVKomp2.Enabled:=False else QVKomp2.Enabled:=True;

	// QV03.Caption	:= IfThen(okatert[13]=0,'','*')+  Format('%.0f', [okatert[3]]);
	QV04.Caption	:= Format('%.0f', [(okatert[2]-okatert[11])]);

	QV05.Caption	:= Format('%.0f', [Fojarat_km ]);
  QV15.Caption		:= Format('%.0f', [Plusz_aljarat_km ]);
  QV16.Caption		:= Format('%.0f', [Sajat_aljarat_km ]);
  QV17.Caption		:= Format('%.0f', [(Fojarat_km + Sajat_aljarat_km)]);

	QV18.Caption	:= Format('%.0f', [Fojarat_km / gkdarabok[2]]);
  QV19.Caption		:= Format('%.0f', [Plusz_aljarat_km / gkdarabok[2]]);
  QV20.Caption		:= Format('%.0f', [Sajat_aljarat_km / gkdarabok[2]]);
  QV21.Caption		:= Format('%.0f', [(Fojarat_km + Sajat_aljarat_km) /gkdarabok[2]]);

	QV07.Caption	:= Format('%.0f', [okatert[7]]);
  if ervenytelen_fogyasztas_grandtotal then begin
  	QV06.Caption	:= nem_szamolhato_cimke_kozepes;
  	QV08.Caption	:= nem_szamolhato_cimke_kozepes;
    end
  else begin
  	QV06.Caption	:= Format('%.0f', [okatert[6]]);
  	QV08.Caption	:= Format('%.1f', [okatert[8]]);
    end;
	// A sz�m�tott �rt�kek
   if Teljes_km = 0 then begin
       QV09.Caption	:= '0';
       QV10.Caption	:= '0';
       // QV11.Caption	:= '0';
       QV12.Caption	:= '0';
   end else begin
       QV09.Caption	:= Format('%.3f', [(okatert[11]-okatert[14]) / Teljes_km]);
       Qv10.Caption	:= Format('%.3f', [(okatert[2]) / Teljes_km]);
       QV12.Caption	:= Format('%.2f', [(okatert[2]-okatert[11]) / Teljes_km]);
   end;
   if Fojarat_km + Sajat_aljarat_km = 0 then begin
       QV13.Caption	:= '0';
       QV14.Caption	:= '0';
   end else begin
        if ervenytelen_fogyasztas_grandtotal then begin
  	      QV13.Caption := nem_szamolhato_cimke_kozepes;
        	QV14.Caption := nem_szamolhato_cimke_kozepes;
          end
        else begin
          QV13.Caption := Format('%.2f', [okatert[6] * 100 / (Fojarat_km + Sajat_aljarat_km)]);
          QV14.Caption := Format('%.2f', [okatert[8] * 100 / (Fojarat_km + Sajat_aljarat_km)]);
          end;
   end;

   if ervenytelen_km_grandtotal then begin  // ha �rv�nytelen, akkor ezeket fel�l�rjuk
      QV09.Caption	:= nem_szamolhato_cimke_kozepes;
      QV10.Caption	:= nem_szamolhato_cimke_kozepes;
      QV12.Caption	:= nem_szamolhato_cimke_kozepes;
      QV13.Caption	:= nem_szamolhato_cimke_kozepes;
      QV05.Caption	:= nem_szamolhato_cimke_kozepes;
      QV15.Caption	:= nem_szamolhato_cimke_kozepes;
      QV16.Caption	:= nem_szamolhato_cimke_kozepes;
      QV17.Caption	:= nem_szamolhato_cimke_kozepes;
      end; // if ervenytelen_km_grandtotal

   // A g�pkocsidarabok lekezel�se
   QrLabel47.Caption	:= IntToStr(gkdarabok[2])+' db, �tlag:';
   // QV12A.Caption		:= Format('%.0f km/db', [okatert[5] / gkdarabok[2]]);

end;

function TOsszlis2DlgUj.Joertek(ertek : double; tip : integer) : boolean;
var
	kezdoert	: double;
	vegzoert	: double;
begin
   Result		:= true;
   ertekszin	:= clWindow;
   kezdoert	:= 0;
   vegzoert	:= 0;
   case tip of
   	1 : // KM
       begin
       	kezdoert	:= StringSzam(Query6.FieldByName('KI_KMTOL').AsString);
       	vegzoert	:= StringSzam(Query6.FieldByName('KI_KMEIG').AsString);
		end;
   	2 : // NYERES�G
       begin
       	kezdoert	:= StringSzam(Query6.FieldByName('KI_NYTOL_EUR').AsString);
       	vegzoert	:= StringSzam(Query6.FieldByName('KI_NYEIG_EUR').AsString);
       end;
   	3 : // �TLAG
       begin
       	kezdoert	:= StringSzam(Query6.FieldByName('KI_ATTOL').AsString);
       	vegzoert	:= StringSzam(Query6.FieldByName('KI_ATLIG').AsString);
       end;
   	4 : // MEGTAKAR�T�S
       begin
       	kezdoert	:= StringSzam(Query6.FieldByName('KI_MTTOL').AsString);
       	vegzoert	:= StringSzam(Query6.FieldByName('KI_MTAIG').AsString);
		end;
   	5 : // K�lts�g / km
       begin
       	kezdoert	:= StringSzam(Query6.FieldByName('KI_KTTOL_EUR').AsString);
       	vegzoert	:= StringSzam(Query6.FieldByName('KI_KTGIG_EUR').AsString);
       end;
   	6 : // EUR / km
       begin
       	kezdoert	:= StringSzam(Query6.FieldByName('KI_EUTOL').AsString);
       	vegzoert	:= StringSzam(Query6.FieldByName('KI_EURIG').AsString);
       end;
	end;
   if vegzoert = 0 then begin
   	vegzoert := 999999999999;
   end;
   if kezdoert > ertek then begin
   	ertekszin	:= clLime;
   	Result		:= false;
	end;

	if ertek > vegzoert then begin
		ertekszin	:= clRed;
		Result		:= false;
	end;
end;

procedure TOsszlis2DlgUj.Ellenorzes;
var
	i : integer;
begin
	// Ellen�rz�sek v�grehajt�sa
	Application.CreateForm(TValellenorDlg, ValellenorDlg);
	ValellenorDlg.Tolt(SGEllen);
	ValellenorDlg.ShowModal;
	if ValellenorDlg.kodkilist.Count > 0 then begin
		EllenListTorol;
		if NoticeKi('Val�ban ellen�rz�tt� k�v�nja tenni a kijel�lt j�ratokat?', NOT_QUESTION) = 0 then begin
			for i := 0 to ValellenorDlg.kodkilist.Count - 1 do begin
				Query_Run(Query1,'SELECT * FROM SZFEJ WHERE SA_UJKOD IN (SELECT VI_UJKOD FROM VISZONY WHERE VI_JAKOD = '''+
					ValellenorDlg.kodkilist[i]+''' AND SA_FLAG = 1 '+')',true);
				if Query1.RecordCount > 0 then begin
					// Van nem v�gleges�tett sz�mla a j�ratban
					EllenListAppend( 'Nem v�gleges�tett sz�mla : '+Query1.FieldByName('SA_ORSZA').AsString+Query1.FieldByName('SA_JARAT').AsString, false );
				end else begin
					// Lehet ellen�rz�tt� tenni
					Query_Run(Query1, 'UPDATE JARAT SET JA_FAZIS = 1, JA_FAZST = ''Ellen�rz�tt''  WHERE JA_KOD = '+ValellenorDlg.kodkilist[i],true);
				end;
			end;
		end;
		EllenListMutat('Nem v�gleges�tett sz�ml�khoz tartoz� j�ratok', false);
	end;
	ValellenorDlg.Destroy;
end;

function TOsszlis2DlgUj.Jarat_Egyeb_km(jakod: string): E_res;
var
  jaratdb, mbdb, okm, oktg, jaratkm: integer;
  OsszEURKoltseg: double;
  ejakod,ejasza, ejaev: string;
  jaratok,fajta, S: string;
  JaratkoltsegRec: TJaratKoltsegFogyasztas;
  res: E_res;
begin
  okm:=0;
  oktg:=0;
  OsszEURKoltseg:=0;
  jaratok:='';
  Result.ervenyes_fogyasztas:= True;
  // S:='SELECT DISTINCT ms_jakod,ms_jasza,ms_fajta FROM MEGSEGED, MEGBIZAS where ms_mbkod=mb_mbkod and mb_jakod='+jakod +' and ms_fajko<>0';
  S:='SELECT DISTINCT ms_jakod,ms_jasza,ms_fajta, substring(MS_DATUM,1,4) ev FROM MEGSEGED, MEGBIZAS where ms_mbkod=mb_mbkod and mb_jakod='+jakod +' and ms_fajko <> 0';  //  alighanem hib�s volt: and ms_fajko=1';
  S:=S+' union ';
  S:=S+' select DISTINCT ja_kod, JA_ORSZ+''-''+convert(varchar,convert(int, ja_alkod)), ''alj�rat'', substring(JA_JKEZD,1,4) from JARAT WHERE JA_FOJAR='+jakod;
	if Query_Run(QueryAlap1, S) then begin
    while not QueryAlap1.EOF do begin // MEGSEGED
      fajta:=QueryAlap1.Fieldbyname('MS_FAJTA').AsString ;
      ejakod:=QueryAlap1.Fieldbyname('MS_JAKOD').AsString ;
      ejasza:=QueryAlap1.Fieldbyname('MS_JASZA').AsString ;
      ejaev:=QueryAlap1.Fieldbyname('ev').AsString ;
      if ejakod<>'' then begin
       if fajta= 'alj�rat' then begin
          mbdb:= 1;
          end
        else begin
          Query_Run(Queryal2, 'select DISTINCT mb_jasza from MEGSEGED, megbizas where ms_mbkod=mb_mbkod and ms_jasza='''+ejasza+'''' +
             ' and substring(MS_DATUM,1,4)='''+ejaev+'''');
          mbdb:= Queryal2.RecordCount;
          end;
        if mbdb > 0 then begin
        	Query_Run(Queryal2, 'SELECT JA_OSSZKM FROM JARAT where ja_kod='''+ejakod+'''');
          jaratkm:=QueryAl2.Fieldbyname('JA_OSSZKM').AsInteger;
          if jaratkm > 0 then begin  // ne dolgozzunk nem l�tez� j�rattal
            okm:= okm+ Trunc(jaratkm / mbdb);
            // debug
            S:='alj�rat: '+ejakod+', km='+IntToStr(jaratkm)+', /'+IntToStr(mbdb);
            Query_Log_Str(S, 0, true);

            // oktg:= oktg+ Trunc(Jarat_Egyeb_ktg(ejakod) / mbdb);
            JaratkoltsegRec:= GetJaratKoltsegFogyasztas(ejakod, True, False); // csak a k�lts�g kell, fogyaszt�s nem
            // Nem haszn�lunk fogyaszt�s adatot!
            // if not(JaratkoltsegRec.ErvenyesTenyFogyasztas) then
            //    Result.ervenyes_fogyasztas:= False;
            oktg:= oktg+ Trunc(JaratkoltsegRec.OsszHUFKoltseg / mbdb);
            OsszEURKoltseg:=OsszEURKoltseg + (JaratkoltsegRec.OsszEURKoltseg / mbdb);
            jaratok:=jaratok + ejasza + IfThen(mbdb>1,'/'+IntToStr(mbdb))+' '+fajta+',' ;
            end;
          end;  // if
        end;  // if
      QueryAlap1.Next;
      end;  // while
    end;  // if
  Result.okm:= okm;
  Result.oktg:= oktg;
  Result.oeurktg:= OsszEURKoltseg;
  Result.jaratok:= copy(jaratok,1,length(jaratok)-1);
end;

{function TOsszlis2DlgUj.Jarat_Egyeb_ktg(jarat: string): integer;
var
	ertek    	: double;
	sqlstr		: string;
	sof1		: string;
	szossz		: double;
	jojarat		: string;
	vevonev		: string;
	szamlaszam	: string;
	str			: string;
	feldij		: double;
	felra1		: double;

//	fogyi		: string;
	voltsofor	: string;
	i			: integer;
	uzkolt		: double;
	tipus		: integer;
	osszkm		: double;
	uzmegtak	: double;
	ksatlag		: double;
	rekordszam	: integer;
	osszam		: integer;
	jaratatlag	: double;
  kjel, jaratsz: string;
  ertekek			: array [1..5] of double;
	okolts		: double;
	ofuvar		: double;
	oertek		: double;
	arfoly		: double;
  ossz,ossz2: double;
begin
	selkm1		:= 0;
	selkm2		:= 0;
	sedij1		:= 0;
	sedij2		:= 0;
	oertek		:= 0;
	okolts		:= 0;
	ofuvar		:= 0;
	vanadat 	:= false;
	feldij		:= StringSzam(EgyebDlg.Read_SZGrid('CEG', '301'));
	minimumkt	:= StringSzam(EgyebDlg.Read_SZGrid('999', '742'));
	maximumkt	:= StringSzam(EgyebDlg.Read_SZGrid('999', '743'));
	for i := 1 to 5 do begin
		ertekek[i]	:= 0;
	end;
	// El�sz�r lesz�rj�k a j�ratokat a megadott felt�telek alapj�n
	sqlstr	:= 'SELECT * FROM JARAT WHERE JA_KOD = '''+jarat+''' ';
	if Query_Run (Query1_,sqlstr ,true) then begin
		while ( ( not Query1_.EOF ) and (not EgyebDlg.kilepes) ) do begin
			jaratsz			:= Query1_.FieldByName('JA_KOD').AsString;
			jojarat	:= GetJaratszamFromDb(Query1_);
			vanadat 	:= true;
			// A j�rathoz tartoz� fogyaszt�si adatok kisz�m�t�sa

			FogyasztasDlg.ClearAllList;
			FogyasztasDlg.JaratBetoltes(jarat);
			FogyasztasDlg.CreateTankLista;
			FogyasztasDlg.CreateAdBlueLista;
			FogyasztasDlg.Szakaszolo;

			// A dolgoz�i adatok bet�tele
			voltsofor	:= '';
			Query_Run(Query5, 'SELECT * FROM JARSOFOR WHERE JS_JAKOD = '''+jaratsz+''' ORDER BY JS_SORSZ ');
			if Query5.RecordCount > 0 then begin
				while not Query5.Eof do begin
					sof1		:= Query_Select('DOLGOZO', 'DO_KOD', Query5.FieldByname('JS_SOFOR').AsString , 'DO_NAME');
					if sof1 <> '' then begin
						// Betessz�k a sof�r fogyaszt�si adatokat k�l�n sorba
						uzkolt 	:= 0;
						if Pos (Query5.FieldByname('JS_SOFOR').AsString, voltsofor) < 1 then begin
							voltsofor	:= voltsofor + Query5.FieldByname('JS_SOFOR').AsString + ';';
							if  Round(Query1_.FieldByname('JA_OSSZKM').AsFloat ) > 0  then begin
								uzmegtak                        := FogyasztasDlg.GetSoforMegtakaritas(Query5.FieldByname('JS_SOFOR').AsString);
								// Az �zemanyag megtakar�t�s k�lts�g�nek kisz�m�t�sa �s megjelen�t�se
								if uzmegtak <> 0 then begin
									uzkolt	 				   	:= -1 * uzmegtak * GetApehUzemanyag(Query1_.FieldByName('JA_JKEZD').AsString, 1);
								end;
								ossz  		:= ossz + uzkolt;
								ossz2  		:= ossz2+ uzkolt;
							end;
						end;
						selkm1	:= StringSzam(Query5.FieldByname('JS_SZAKM').AsString);
						sedij1	:= StringSzam(Query5.FieldByname('JS_JADIJ').AsString);
						sextra1	:= StringSzam(Query5.FieldByname('JS_EXDIJ').AsString);
						felra1  := StringSzam(Query5.FieldByname('JS_FELRA').AsString);
						ossz  								:= ossz - selkm1 * sedij1 - sextra1 - felra1*feldij;
						ossz2  								:= ossz2- selkm1 * sedij1 - sextra1 - felra1*feldij;
					end;
					Query5.Next;
				end;
			end;


		 // Bet�ltj�k a k�lts�geket a Query1_-be
		 if Query_Run (Query3,'SELECT * FROM KOLTSEG WHERE KS_JAKOD = '''+jaratsz+''' '+
			' AND KS_TETAN = 0 '+
			' ORDER BY KS_RENDSZ DESC, KS_DATUM' ,true) then begin
			while not Query3.EOF do begin
				tipus 	:= StrToIntdef(Query3.FieldByName('KS_TIPUS').AsString,0);
				// Nett� �rt�ket sz�molunk
  			ertek	:= -1 * Query3.FieldByname('KS_OSSZEG').AsFloat * Query3.FieldByname('KS_ARFOLY').AsFloat + Query3.FieldByname('KS_AFAERT').AsFloat;
				if ( ( tipus <> 0 ) and ( tipus <> 3 ) and ( tipus <> 2 ) ) then begin		// Az �zemanyag k�lts�get + Ad blue -t + h�t� tankol�st nem vonjuk le, mert azt ut�na sz�moljuk
					ossz	:= ossz + ertek;
 					ossz2	:= ossz2 + ertek;
				end;
				Query3.Next;
			end;
		 end;

		// A g�pkocsi �zemanyag k�lts�g�nek berak�sa
		// Ha nincs ilyen adat, ezt ki kellene hagyni!!!
		ossz  								:= ossz - FogyasztasDlg.GetKoltseg;
		ossz2  								:= ossz2- FogyasztasDlg.GetKoltseg;

		// A g�pkocsi Ad-blue k�lts�g�nek berak�sa -> Ha nincs ilyen adat, ezt ki kell hagyni!!!
		Query_Run (Query5, 'SELECT * FROM KOLTSEG WHERE KS_RENDSZ = '''+Query1_.FieldByname('JA_RENDSZ').AsString+''' AND KS_TIPUS = 2 ' );
		if Query5.RecordCount > 0 then begin
			ossz  								:= ossz - FogyasztasDlg.GetBlueKoltseg;
			ossz2  								:= ossz2- FogyasztasDlg.GetBlueKoltseg;
		end;

		// A h�t� tankol�sok berak�sa
		Query_Run (Query2,'SELECT * FROM JARPOTOS WHERE JP_JAKOD = '''+jaratsz+''' ',true);
		while not Query2.EOF do begin
			if HutoPotkocsi(Query2.FieldByname('JP_POREN').AsString) then begin
				szossz	:= GetHutoOsszeg(Query2.FieldByname('JP_POREN').AsString, Query2.FieldByname('JP_UZEM1').AsString, Query2.FieldByname('JP_UZEM2').AsString);
				ossz  								:= ossz - szossz;
				ossz2  								:= ossz2- szossz;
			end;
			Query2.Next;
		end;
		Query2.Close;

		 // K�lts�g �sszesen bet�lt�se
     if OssznyilDlg.CheckBox2.Checked then
        ossz:=ossz2;
		 okolts	:= okolts + ossz;

		 ofuvar	:= ofuvar + ertek;

			ertekek[1]		:= ertekek[1] + FogyasztasDlg.GetOsszKm;
			ertekek[2]		:= ertekek[2] + FogyasztasDlg.GetTenyFogyasztas;
			ertekek[3]		:= ertekek[3] + FogyasztasDlg.GetTervFogyasztas;
			ertekek[4]		:= ertekek[4] + FogyasztasDlg.GetMegtakaritas;
			ertekek[5]		:= ertekek[5] + FogyasztasDlg.GetSulyAtlag * FogyasztasDlg.GetOsszKm ;
			oertek			:= oertek + ertek + ossz;
			// Az EUR k�lts�g kisz�m�t�sa a j�rat indul� d�tum alapj�n
			ossz			:= 0;
			Query1_.Next;
		end;
		Query1_.EnableControls;
	end;
  Result:=Trunc(okolts);

end;
}

function TOsszlis2DlgUj.ktsg_nemkell(nev: string): boolean;
begin
  Result:=False;
  Q_Szotar.Close;
  Q_Szotar.Parameters[0].Value:=nev;
  Q_Szotar.Open;
  Q_Szotar.First;
  if not Q_Szotar.Eof then
     Result:=True;
  Q_Szotar.Close;   
end;

end.
