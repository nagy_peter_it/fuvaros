unit MegbizasFm;

interface

uses
	SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
	StdCtrls, Forms, DBCtrls, DB, DBGrids, DBTables, Grids, ExtCtrls, Buttons,
	ShellApi, Dialogs, ADODB, J_FOFORMUJ, Mask, LGauge, Generics.Collections;

{type
    TNormaRecord = record
      KATEG: string;
      TOL, IG: double;
      end;  // record
      }
type
	TMegbizasFmDlg = class(TJ_FOFORMUJDLG)
    Query3: TADOQuery;
    BitBtn7: TBitBtn;
    BitBtn8: TBitBtn;
    M2: TMaskEdit;
    BitBtn5: TBitBtn;
    M3: TMaskEdit;
    BitBtn4: TBitBtn;
    BitBtn3: TBitBtn;
    BitBtn6: TBitBtn;
    BitBtn10: TBitBtn;
    Query4: TADOQuery;
    BitBtn1: TBitBtn;
    MDij: TMaskEdit;
    BitBtn2: TBitBtn;
    MEur: TMaskEdit;
    BitBtn9: TBitBtn;
    QueryKozos: TADOQuery;
	  procedure FormCreate(Sender: TObject);override;
	 procedure Adatlap(cim, kod : string);override;
    procedure BitBtn8Click(Sender: TObject);
    procedure BitBtn7Click(Sender: TObject);
    procedure BitBtn10Click(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BitBtn5Click(Sender: TObject);
    procedure BitBtn4Click(Sender: TObject);
	 procedure ReadCalcFields ; override;
	 procedure DataSource1DataChange(Sender: TObject; Field: TField);override;
    procedure Mezoiro;override;
    procedure UjSqlEllenor;override;
    procedure Osszegzes;
    procedure FormShow(Sender: TObject);
    procedure AdatTolto;
    procedure BitBtn2Click(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBGrid2DrawColumnCell(Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);override;
    procedure normalista_toltes;
    procedure FormDestroy(Sender: TObject);
  private
		// normalista: array of array [1..2] of double;
    NormaTol: TDictionary<string,double>;
    NormaIg: TDictionary<string,double>;
	end;

const
  // STATU_kizaras = ' MB_STATU<>300 ';  // A "Nem telj." st�tusz� megb�z�sokat nem l�tjuk itt
  STATU_kizaras = ' 1 = 1 ';  // nem sz�rj�k ki �ket eleve, mert egyes esetekben kell hogy l�ss�k.

  // az�rt, hogy tudjunk ISNULL-t haszn�lni. NagyP 2016-07-01
  megbizas_mezolista = 'MB_ADR, MB_AFKOD, MB_AFLAG, MB_AFNEV, MB_ALDIJ, MB_ALEUR, MB_ALEURKM, MB_ALNEM, ISNULL(MB_ALNEV,'''') MB_ALNEV, MB_ALV, MB_ALVAL, MB_ATAKM, MB_BORDNO1, MB_BORDNO2, '+
      'MB_BORDNO3, MB_CSPAL, MB_DATUM, MB_DOKO2, MB_DOKOD, MB_EDIJ, MB_EUDIJ, MB_EURKM, MB_EURKMU, MB_EXPOR, MB_EXSTR, ISNULL(MB_FECEG,'''') MB_FECEG, MB_FEKOD, MB_FELFUG, '+
      ' ISNULL(MB_FENEV,'''') MB_FENEV, MB_FERAK, ISNULL(MB_FORSZ,'''') MB_FORSZ, MB_FUDIJ, MB_FUNEM, MB_FUTID, ISNULL(MB_GKKAT,'''') MB_GKKAT, MB_HUTOS, MB_JAKOD, MB_JASZA, MB_KISCS, MB_KOBOS, '+
      ' ISNULL(MB_LECEG,'''') MB_LECEG, MB_LERAK, ISNULL(MB_LORSZ,'''') MB_LORSZ, MB_MBKOD, MB_MEGJE, MB_MODDAT, MB_NOPOZ, MB_NUZPOT, ISNULL(MB_POTSZ,'''') MB_POTSZ, '+
      ' MB_POZIC, ISNULL(MB_RENSZ,'''') MB_RENSZ, MB_SABLO, MB_SAKO2, MB_SAKOD, MB_SAOSS, MB_SAVAL, MB_SEDIJ, MB_SENEM, MB_SNEV1, MB_SNEV2, MB_SOSFL, MB_SOSKM, ISNULL(MB_STATU,'''') MB_STATU, '+
      ' MB_SZDIJ, MB_TAVKM, MB_TAVMI, MB_TEDAT, MB_VAM, ISNULL(MB_VEKOD,'''') MB_VEKOD, ISNULL(MB_VENEV,'''') MB_VENEV, MB_VIKOD, MB_VISZO, MB_XMLSZ, '+
      ' VI_ARFDAT, VI_ARFOLY, VI_BEDAT, VI_EVIKOD, VI_FDEXP, VI_FDIMP, VI_FDKOV, ISNULL(VI_HONNAN,'''') VI_HONNAN, ISNULL(VI_HOVA,'''') VI_HOVA, VI_JAKOD, VI_JARAT, VI_KIDAT, VI_KULF, VI_MEGJ, '+
      ' VI_POZIC, VI_SAKOD, VI_TIPUS, VI_UJKOD, VI_VALNEM, VI_VEKOD, ISNULL(VI_VENEV,'''') VI_VENEV, VI_VIKOD, MB_MSMOD, MB_KEDAT, MB_INTREF, VI_SZLAVAL, MB_NOCSOP ';
var
	MegbizasFmDlg : TMegbizasFmDlg;

implementation

uses
	Egyeb, J_SQL, Kozos, Jaratbe, MegbizasBe, SzamlaNezo;

{$R *.DFM}

procedure TMegbizasFmDlg.FormCreate(Sender: TObject);
var
   sorsz   : integer;
begin
   // A hi�nyz� d�tumok felt�lt�se
	Inherited FormCreate(Sender);
	EgyebDLg.SetADOQueryDatabase(Query3);
	EgyebDLg.SetADOQueryDatabase(Query4);
	EgyebDLg.SetADOQueryDatabase(QueryKozos);
   // Az egy�b d�j + valutanem r�gz�t�se
	{if not Query_Run(Query3, 'SELECT MB_SEDIJ FROM MEGBIZAS ', FALSE) then begin
  		Query_Run(Query3,'ALTER TABLE MEGBIZAS   ADD MB_SEDIJ numeric(12,2)', FALSE);
  		Query_Run(Query3,'ALTER TABLE MEGBIZAS   ADD MB_SENEM varchar(3)', FALSE);
  		Query_Run(Query3,'ALTER TABLE MEGBIZAS   ADD MB_VISZO varchar(30)', FALSE);
  		Query_Run(Query3,'ALTER TABLE MEGBIZAS   ADD MB_SZDIJ numeric(12,2)', FALSE);
  		Query_Run(Query3,'ALTER TABLE MEGBIZAS2  ADD MB_SEDIJ numeric(12,2)', FALSE);
  		Query_Run(Query3,'ALTER TABLE MEGBIZAS2  ADD MB_SENEM varchar(3)', FALSE);
  		Query_Run(Query3,'ALTER TABLE MEGBIZAS2  ADD MB_VISZO varchar(30)', FALSE);
  		Query_Run(Query3,'ALTER TABLE MEGBIZAS2  ADD MB_SZDIJ numeric(12,2)', FALSE);
       // Kisz�moljuk az �sszeset egyszer
       Query_Run(Query3, 'SELECT MB_MBKOD FROM MEGBIZAS ');
       Screen.Cursor   := crHourGlass;
       sorsz   := 1;
       FormGaugeDlg    := TFormGaugeDlg.Create(Self);
       FormGaugeDlg.GaugeNyit('Seg�dadatok �sszegy�jt�se... ' , '', false);
       while not Query3.Eof do begin
           FormGaugeDlg.Pozicio ( ( sorsz * 100 ) div Query3.RecordCount ) ;
           Inc(sorsz);
           MegbizasKieg(Query3.FieldByName('MB_MBKOD').AsString);
           Query3.Next;
       end;
       Screen.Cursor   := crDefault;
       FormGaugeDlg.Destroy;
   end;
   }

   // Seg�dmez�k defini�l�sa
  {
	if not Query_Run(Query3, 'SELECT MB_FORSZ FROM MEGBIZAS ', FALSE) then begin
  		Query_Run(Query3,'ALTER TABLE MEGBIZAS   ADD MB_FECEG VARCHAR(30)', FALSE);
  		Query_Run(Query3,'ALTER TABLE MEGBIZAS   ADD MB_FERAK VARCHAR(100)', FALSE);
  		Query_Run(Query3,'ALTER TABLE MEGBIZAS   ADD MB_LECEG VARCHAR(30)', FALSE);
  		Query_Run(Query3,'ALTER TABLE MEGBIZAS   ADD MB_LERAK VARCHAR(100)', FALSE);
  		Query_Run(Query3,'ALTER TABLE MEGBIZAS   ADD MB_FORSZ VARCHAR(5)', FALSE);
  		Query_Run(Query3,'ALTER TABLE MEGBIZAS   ADD MB_LORSZ VARCHAR(5)', FALSE);

       // Kisz�moljuk az �sszeset egyszer
       Query_Run(Query3, 'SELECT MB_MBKOD FROM MEGBIZAS ');
       Screen.Cursor   := crHourGlass;
       sorsz   := 1;
       FormGaugeDlg    := TFormGaugeDlg.Create(Self);
       FormGaugeDlg.GaugeNyit('Seg�dadatok �sszegy�jt�se... ' , '', false);
       while not Query3.Eof do begin
           FormGaugeDlg.Pozicio ( ( sorsz * 100 ) div Query3.RecordCount ) ;
           Inc(sorsz);
           MegbizasKieg(Query3.FieldByName('MB_MBKOD').AsString);
           Query3.Next;
       end;
       Screen.Cursor   := crDefault;
       FormGaugeDlg.Destroy;
   end;
   }

//	Query_Run(Query3, 'UPDATE MEGBIZAS SET MB_SAOSS = 0, MB_SZDIJ = 0 WHERE ( ( MB_VIKOD IS NULL ) OR (MB_VIKOD = '') ) ', FALSE);

	AddSzuromezoRovid('VI_HONNAN', 'VISZONY', '1');
	AddSzuromezoRovid('VI_HOVA',   'VISZONY', '1');
	AddSzuromezoRovid('MB_VEKOD',  'MEGBIZAS', '1');
	AddSzuromezoRovid('MB_VENEV',  'MEGBIZAS', '1');
	AddSzuromezoRovid('MB_RENSZ',  'MEGBIZAS', '1');
	AddSzuromezoRovid('MB_POTSZ',  'MEGBIZAS', '1');
	AddSzuromezoRovid('MB_FECEG',  'MEGBIZAS', '1');
	AddSzuromezoRovid('MB_LECEG',  'MEGBIZAS', '1');
	AddSzuromezoRovid('MB_FORSZ',  'MEGBIZAS', '1');
	AddSzuromezoRovid('MB_LORSZ',  'MEGBIZAS', '1');
	AddSzuromezoRovid('MB_FENEV',  'MEGBIZAS', '1');
	AddSzuromezoRovid('MB_GKKAT',  'MEGBIZAS', '1');
	AddSzuromezoRovid('MB_ALNEV',  'MEGBIZAS', '1');
	AddSzuromezoRovid('MB_STATU',  'MEGBIZAS', '1');
  // AddSzuromezoRovid('MB_ALNEV',  'MEGBIZAS', '1'); // lehessen �resre sz�rni
	AddSzuromezoRovid('MB_VENEV',  'MEGBIZAS', '1');
 	AddSzuromezoRovid('MB_EDIJ',  '');
 	AddSzuromezoRovid('MB_KOBOS',  '');
  AddSzuromezoRovid('MB_INTREF',  '');
  AddSzummamezo('MB_EUDIJ');
  AddSzummamezo('MB_SZDIJ');
  AddSzuromezoRovid('MB_NOCSOP', '', '1');

   M2.Text     := EgyebDlg.MaiDatum;
   M3.Text     := EgyebDlg.MaiDatum;
   //	ALAPSTR		:= 'SELECT * FROM MEGBIZAS LEFT OUTER JOIN VISZONY ON VI_VIKOD = MB_VIKOD WHERE MB_TEDAT = '''+M2.Text+''' ';
   //ALAPSTR		:= 'SELECT * FROM MEGBIZAS LEFT OUTER JOIN VISZONY ON VI_VIKOD = MB_VIKOD WHERE '+STATU_kizaras+' AND MB_TEDAT = '''+M2.Text+''' ';

   ALAPSTR		:= 'SELECT '+ megbizas_mezolista +
      ' FROM MEGBIZAS LEFT OUTER JOIN VISZONY ON VI_VIKOD = MB_VIKOD WHERE '+STATU_kizaras+' AND MB_TEDAT = '''+M2.Text+''' ';
   ALAPSTR		:= 'select * from ('+ALAPSTR+') x9 where 1=1 ';  // TJ_FOFORMUJDLG.ReadFields-ben eldobja a mez�list�t, �s "SELECT TOP 1 *" lesz. Emiatt kell itt becsomagolni

	FelTolto('�j megb�z�s felvitele ', 'Megb�z�s adatok m�dos�t�sa ',
			'Megb�z�sok list�ja', 'MB_MBKOD',
           ALAPSTR,
           'MEGBIZAS',
            QUERY_ADAT_TAG);
	width	:= DLG_WIDTH;
	height	:= DLG_HEIGHT;
   ButtonUj.Hide;
   ButtonMod.Hide;
   ButtonTor.Hide;
   ButtonKuld.Hide;
	BitBtn7.Parent		:= PanelBottom;
	BitBtn2.Parent		:= PanelBottom;
	BitBtn8.Parent		:= PanelBottom;
	M2.Parent			:= PanelBottom;
	M3.Parent			:= PanelBottom;
	MDij.Parent			:= PanelBottom;
   MEur.Parent			:= PanelBottom;
	BitBtn1.Parent		:= PanelBottom;
	BitBtn3.Parent		:= PanelBottom;
	BitBtn6.Parent		:= PanelBottom;
	BitBtn5.Parent		:= PanelBottom;
	BitBtn4.Parent		:= PanelBottom;
	BitBtn9.Parent		:= PanelBottom;
	BitBtn10.Parent		:= PanelBottom;

  normalista_toltes;  // az MB_EURKM oszlop sz�nez�s�hez

end;

procedure TMegbizasFmDlg.FormDestroy(Sender: TObject);
begin
  NormaTol.Free;
  NormaIg.Free;
  inherited;
end;

procedure TMegbizasFmDlg.Adatlap(cim, kod : string);
begin
//	Application.CreateForm(TKoltsegbeDlg, AlForm);
//	Inherited Adatlap(cim, kod );
end;

procedure TMegbizasFmDlg.BitBtn8Click(Sender: TObject);
begin
	// J�rat megjelen�t�se
	if Query1.FieldByName('MB_JAKOD').AsString <> '' then begin
		Query_Run (Query3, 'SELECT * FROM JARAT WHERE JA_KOD = '''+Query1.FieldByName('MB_JAKOD').AsString+''' ',true);
		if Query3.RecordCount < 1 then begin
      // NoticeKi('El�z� �vi j�rat!');
			NoticeKi('A j�rat nem tal�lhat�! J�ratk�d: '+Query1.FieldByName('MB_JAKOD').AsString);
			Exit;
		end;
		Application.CreateForm(TJaratbeDlg, JaratbeDlg);
       JaratbeDlg.megtekint:= true;
		JaratbeDlg.Tolto('J�rat megtekint�se',Query1.FieldByName('MB_JAKOD').AsString);
		JaratbeDlg.ShowModal;
		JaratbeDlg.Destroy;
   end;
end;

procedure TMegbizasFmDlg.BitBtn7Click(Sender: TObject);
var
   meguj   : TMegbizasbeDlg;
begin
   if Query1.FieldByName('MB_MBKOD').AsString <> '' then begin
       Application.CreateForm(TMegbizasbeDlg, meguj);
       meguj.Tolto('Megbizas megjelen�t�se', Query1.FieldByName('MB_MBKOD').AsString);
       meguj.ShowModal;
       if meguj.ret_kod <> '' then begin
           MegbizasKieg(meguj.ret_kod);
		    ModLocate (Query1, FOMEZO, meguj.ret_kod );
       end;
       meguj.Destroy;
   end;
end;

procedure TMegbizasFmDlg.BitBtn10Click(Sender: TObject);
begin
	// Let�r�lj�k a felt�teleket
	// alapstr	:= 'SELECT * FROM MEGBIZAS LEFT OUTER JOIN VISZONY ON VI_VIKOD = MB_VIKOD ';
  alapstr	:= 'SELECT '+megbizas_mezolista+' FROM MEGBIZAS LEFT OUTER JOIN VISZONY ON VI_VIKOD = MB_VIKOD WHERE '+STATU_kizaras;
  alapstr		:= 'select * from ('+alapstr+') x9 where 1=1 ';  // TJ_FOFORMUJDLG.ReadFields-ben eldobja a mez�list�t, �s "SELECT TOP 1 *" lesz. Emiatt kell itt becsomagolni
	SQL_ToltoFo(GetOrderBy(true));
end;

procedure TMegbizasFmDlg.FormResize(Sender: TObject);
begin
	Inherited FormResize(Sender);
	M2.Top			:= BitBtn3.Top+2;
	M2.Left			:= BitBtn3.Left;
	BitBtn5.Top		:= BitBtn3.Top+2;
	BitBtn5.Left	:= BitBtn3.Left + M2.Width;
	BitBtn5.Width	:= BitBtn5.Height;

	M3.Top			:= BitBtn6.Top+2;
	M3.Left			:= BitBtn5.Left + BitBtn5.Width + 10;
	BitBtn4.Top		:= M3.Top;
	BitBtn4.Left	:= M3.Left + M3.Width;
	BitBtn4.Width	:= BitBtn4.Height;

	MDij.Top		:= BitBtn1.Top+2;
	MDij.Left		:= BitBtn1.Left;

	MEur.Top		:= BitBtn9.Top+2;
	MEur.Left		:= BitBtn9.Left;

end;

procedure TMegbizasFmDlg.BitBtn5Click(Sender: TObject);
begin
	// EgyebDlg.Calendarbe(M2);
	// M3.Text	:= M2.Text;
  // EgyebDlg.ElsoNapEllenor(M2, M3);
  EgyebDlg.CalendarBe_idoszak(M2, M3);

	alapstr	:= 'SELECT '+megbizas_mezolista+' FROM MEGBIZAS LEFT OUTER JOIN VISZONY ON VI_VIKOD = MB_VIKOD WHERE '+STATU_kizaras+' AND MB_TEDAT >= '''+M2.Text+''' '+
		'AND MB_TEDAT <= '''+M3.Text+''' ';
  alapstr:= 'select * from ('+alapstr+') x9 where 1=1 ';  // TJ_FOFORMUJDLG.ReadFields-ben eldobja a mez�list�t, �s "SELECT TOP 1 *" lesz. Emiatt kell itt becsomagolni
	SQL_ToltoFo(GetOrderBy(true));
end;

procedure TMegbizasFmDlg.BitBtn4Click(Sender: TObject);
begin
	EgyebDlg.Calendarbe(M3, True);
	// Az intervallum megad�sa
	alapstr	:= 'SELECT '+megbizas_mezolista+' FROM MEGBIZAS LEFT OUTER JOIN VISZONY ON VI_VIKOD = MB_VIKOD WHERE '+STATU_kizaras+' AND MB_TEDAT >= '''+M2.Text+''' '+
		'AND MB_TEDAT <= '''+M3.Text+''' ';
  alapstr:= 'select * from ('+alapstr+') x9 where 1=1 ';  // TJ_FOFORMUJDLG.ReadFields-ben eldobja a mez�list�t, �s "SELECT TOP 1 *" lesz. Emiatt kell itt becsomagolni
	SQL_ToltoFo(GetOrderBy(true));
end;

procedure TMegbizasFmDlg.ReadCalcFields ;
begin
(*
   AddCalcField('FELCEG',  'Felrak� c�g',   '1', '0', '100', '0', '100', '0') ;
*)
end;

procedure TMegbizasFmDlg.Mezoiro;
begin
(*
   Query1.FieldByName('FELCEG').AsString   := '';
   Query_Run(Query3, 'SELECT MS_FELNEV, MS_FORSZ, MS_FELIR, MS_FELTEL, MS_FELCIM, MS_LERNEV, MS_ORSZA, MS_LELIR, MS_LERTEL, MS_LERCIM FROM MEGSEGED WHERE MS_MBKOD = '+Query1.FieldByName('MB_MBKOD').AsString+' AND MS_FAJKO = 0 ORDER BY MS_MBKOD, MS_SORSZ, MS_TIPUS');
   if Query3.RecordCount > 0 then begin
       Query3.First;
       Query1.FieldByName('FELCEG').AsString   := Query3.FieldByName('MS_FELNEV').AsString;
   end;
*)   
end;

procedure TMegbizasFmDlg.Osszegzes;
var
   regikod : string;
   osszeur : double;
   str     : string;
   stringvalue: string;
   doublevalue: double;
begin
   // Fuvard�jak �sszegz�se
   str := Query1.SQL[0];

   {if Pos('WHERE', UpperCase(str)) < 1 then begin
       str := '';
   end else begin
       str := copy(str, Pos('WHERE', UpperCase(str)), 9999 );
       if Pos('ORDER', UpperCase(str)) > 0 then begin
           str := copy(str, 1, Pos('ORDER', UpperCase(str))-1 );
       end;
   end;}

   if Pos('ORDER', UpperCase(str)) > 0 then begin
       str := copy(str, 1, Pos('ORDER', UpperCase(str))-1 );
       end;

   // Query_Run(Query3, 'SELECT SUM(MB_EUDIJ) AS OSSZDIJ FROM MEGBIZAS LEFT OUTER JOIN VISZONY ON VI_VIKOD = MB_VIKOD '+str );
   Query_Run(Query3, 'SELECT SUM(MB_EUDIJ) AS OSSZDIJ1, SUM(MB_SZDIJ) AS OSSZDIJ2 FROM ( '+str+' ) x8' );

   stringvalue:=Query3.FieldByName('OSSZDIJ1').AsString;
   doublevalue:= StringSzam(stringvalue);
   Mdij.Text   := 'D�j:'+Format('%10.2n EUR', [doublevalue]);  // ide-oda konvert�l�s vsz. a tizedes szepar�tor miatt
   MEur.Text   := 'Szla.:'+Format('%10.2n EUR', [StringSzam(Query3.FieldByName('OSSZDIJ2').AsString)]);
end;

procedure TMegbizasFmDlg.FormShow(Sender: TObject);
begin
   Query_Run(Query3, 'DELETE FROM MEGBIZAS WHERE MB_VENEV = '''' AND MB_ALNEV = '''' AND MB_FUDIJ = 0 AND MB_MBKOD NOT IN (SELECT MS_MBKOD FROM MEGSEGED) ');
end;

procedure TMegbizasFmDlg.AdatTolto;
begin
   // Adatok t�lt�se
end;

procedure TMegbizasFmDlg.UjSqlEllenor;
begin
   Osszegzes;
end;

procedure TMegbizasFmDlg.BitBtn2Click(Sender: TObject);
begin
   // A megb�z�shoz tartoz� sz�mla megjelen�t�se
   Query_Run(Query3, 'SELECT VI_UJKOD FROM VISZONY, MEGBIZAS WHERE MB_VIKOD = VI_VIKOD AND MB_MBKOD = '+Query1.FieldByName('MB_MBKOD').AsString);
   if Query3.FieldByName('VI_UJKOD').AsString <> '' then begin
       Application.CreateForm(TSzamlaNezoDlg, SzamlaNezoDlg);
       SzamlaNezoDlg.SzamlaNyomtatas(StrToIntDef(Query3.FieldByName('VI_UJKOD').AsString,0), 1);
       SzamlaNezoDlg.Destroy;
   end;
end;

procedure TMegbizasFmDlg.DataSource1DataChange(Sender: TObject; Field: TField);
begin
   Inherited DataSource1DataChange(Sender, Field);
   BitBtn2.Enabled := true;
   if Query1.FieldByName('MB_VIKOD').AsString = '' then begin
       BitBtn2.Enabled := false;
   end;
end;

procedure TMegbizasFmDlg.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
   sorsz   : integer;
begin
   if key = VK_F5 then begin
       if NoticeKi('Val�ban �jrasz�moltatja az �sszes elemre a kalkul�lt �rt�keket?', NOT_QUESTION) <> 0 then begin
           Exit;
       end;
       // Kisz�moljuk az �sszeset egyszer
       Query_Run(Query3, 'SELECT MB_MBKOD FROM MEGBIZAS ORDER BY MB_MBKOD');
       Screen.Cursor   := crHourGlass;
       sorsz   := 1;
       FormGaugeDlg    := TFormGaugeDlg.Create(Self);
       FormGaugeDlg.GaugeNyit('Seg�dadatok �sszegy�jt�se... ' , '', true);
       while ( ( not Query3.Eof ) and (not FormGaugeDlg.kilepes) ) do begin
           FormGaugeDlg.Label1.Caption := Query3.FieldByName('MB_MBKOD').AsString;
           FormGaugeDlg.Label1.Update;
           Application.ProcessMessages;
           FormGaugeDlg.Pozicio ( ( sorsz * 100 ) div Query3.RecordCount ) ;
           Inc(sorsz);
           MegbizasKieg(Query3.FieldByName('MB_MBKOD').AsString);
           Query3.Next;
       end;
       Screen.Cursor   := crDefault;     
       FormGaugeDlg.Destroy;
   end;
   inherited FormKeyDown(Sender,Key,Shift);
end;

procedure TMegbizasFmDlg.normalista_toltes;
var
  i: integer;
begin
  NormaTol:= TDictionary<string,double>.Create;
  NormaIg:= TDictionary<string,double>.Create;
  Query_Run(QueryKozos, 'SELECT KI_GKKAT, KI_EUTOL, KI_EURIG FROM KATINTER ORDER BY KI_GKKAT');
  with QueryKozos do begin
    // SetLength(normalista, RecordCount);
    // i:=0;
    while not eof do begin    // TDictionary-vel
       NormaTol.Add(FieldByName('KI_GKKAT').AsString, FieldByName('KI_EUTOL').AsFloat);
       NormaIg.Add(FieldByName('KI_GKKAT').AsString, FieldByName('KI_EURIG').AsFloat);
       Next;
       // normalista[i].KATEG:=FieldsByName('KI_GKKAT').AsString;
       // normalista[i].TOL:=FieldsByName('KI_EUTOL').AsFloat;
       // normalista[i].IG:=FieldsByName('KI_EURIG').AsFloat;
       // inc(i);
       end;  // while
    end;  // with
end;

procedure TMegbizasFmDlg.DBGrid2DrawColumnCell(Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
var
   kateg: string;
   i: integer;
   found: boolean;
   KategNormaAlso, KategNormaFelso, EURKm: double;
   KellSzin: boolean;
   SZDIJ, EUDIJ: currency;
begin
   if (Column.FieldName = 'MB_EURKM') or (Column.FieldName = 'MB_EURKMU') then begin
      kateg	:= Query1.FieldByName('MB_GKKAT').AsString;
      EURKm	:= Query1.FieldByName(Column.FieldName).AsFloat;    // MB_EURKM or MB_EURKMU
      if Trim(kateg) <> '' then begin
        if EURKm = 0 then begin
           KellSzin:=False;  // ne szinezze az �res mez�ket
           end  // if
        else begin
           KellSzin:=True;
           end;  // if
        try
           KategNormaAlso:= NormaTol[kateg];
           KategNormaFelso:= NormaIg[kateg];
        except
           KellSzin:=False;
        end;
        if KellSzin then begin
          if EURKm < KategNormaAlso then begin  // kisebb az als� hat�rn�l: v�r�s
            DBGrid2.Canvas.Brush.Color:= clRed;
            end;
          if EURKm > KategNormaFelso then begin  // nagyobb a fels� hat�rn�l: z�ld
            DBGrid2.Canvas.Brush.Color:= clLime;
            end;
          end;  // if KellSzin
        end;  // if <>''
      end; // if 'MB_EURKM'

   if (Column.FieldName = 'MB_SZDIJ') or (Column.FieldName = 'MB_EUDIJ') then begin
     //  if (abs(Query1.FieldByName('MB_SZDIJ').AsFloat - Query1.FieldByName('MB_EUDIJ').AsFloat)>=0.001)  // 1 centet sem toler�lunk
      SZDIJ:= Query1.FieldByName('MB_SZDIJ').AsFloat;  // 2 tizedesre kerek�tj�k
      EUDIJ:= Query1.FieldByName('MB_EUDIJ').AsFloat;  // 2 tizedesre kerek�tj�k
      if (SZDIJ <> EUDIJ)  // 1 centet sem toler�lunk
      or ((SZDIJ=0.00) and (EUDIJ=0.00))  // ha mindkett� 0
       then begin
          DBGrid2.Canvas.Brush.Color:= clRed;
          end;  // if elt�r
      end; // if 'MB_SZDIJ', 'MB_EUDIJ'

   DBGrid2.DefaultDrawDataCell(Rect, Column.Field, State);
end;
end.

