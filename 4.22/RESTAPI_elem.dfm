object RESTAPIElemDlg: TRESTAPIElemDlg
  Left = 0
  Top = 0
  ClientHeight = 431
  ClientWidth = 472
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PopupMenu = PopupMenu1
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object GridPanel2: TGridPanel
    Left = 0
    Top = 390
    Width = 472
    Height = 41
    Align = alBottom
    ColumnCollection = <
      item
        Value = 100.000000000000000000
      end>
    ControlCollection = <
      item
        Column = 0
        Control = Button4
        Row = 0
      end>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    RowCollection = <
      item
        Value = 100.000000000000000000
      end
      item
        SizeStyle = ssAuto
      end>
    TabOrder = 0
    DesignSize = (
      472
      41)
    object Button4: TButton
      Left = 130
      Top = 4
      Width = 212
      Height = 33
      Anchors = []
      Caption = 'Mentem a  m'#243'dos'#237't'#225'sokat'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      OnClick = Button4Click
    end
  end
  object SGE: TStringGrid
    Left = 0
    Top = 0
    Width = 472
    Height = 390
    Align = alClient
    RowCount = 2
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goEditing, goTabs]
    ParentFont = False
    TabOrder = 1
    OnSelectCell = SGESelectCell
  end
  object lbReadOnlyCells: TListBox
    Left = 376
    Top = 120
    Width = 73
    Height = 81
    ItemHeight = 13
    TabOrder = 2
    Visible = False
  end
  object PopupMenu1: TPopupMenu
    Left = 276
    Top = 120
    object UzenetTop1: TMenuItem
      Caption = 'UzenetTop'
      ShortCut = 121
      OnClick = UzenetTop1Click
    end
  end
end
