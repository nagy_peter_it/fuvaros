unit Adatell;

interface

uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, Lgauge, DBTables, DB, StdCtrls, Buttons, Mask, DBCtrls, ExtCtrls,
  QuickRpt, ADODB;

type
  TAdatellDlg = class(TForm)
    BitBtn4: TBitBtn;
    BitBtn6: TBitBtn;
    GroupBox1: TGroupBox;
    CB11: TCheckBox;
    Label4: TLabel;
    M10: TMaskEdit;
    Label5: TLabel;
    M12: TMaskEdit;
    M13: TMaskEdit;
    Label6: TLabel;
    Label8: TLabel;
    GroupBox2: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    M20: TMaskEdit;
    M21: TMaskEdit;
    BitBtn2: TBitBtn;
    BitBtn3: TBitBtn;
    Label3: TLabel;
    M22: TMaskEdit;
    Label11: TLabel;
    M23: TMaskEdit;
    Label12: TLabel;
    Label13: TLabel;
    M24: TMaskEdit;
    Label15: TLabel;
    M25: TMaskEdit;
    Label16: TLabel;
    Label17: TLabel;
    M26: TMaskEdit;
    Label19: TLabel;
    M27: TMaskEdit;
    Label20: TLabel;
    Memo1: TMemo;
    Query1: TADOQuery;
    CB12: TCheckBox;
    CB22: TCheckBox;
    CB23: TCheckBox;
    CB24: TCheckBox;
    CB13: TCheckBox;
    CB14: TCheckBox;
    M11: TMaskEdit;
    Label10: TLabel;
    Label21: TLabel;
    Query3: TADOQuery;
    Query2: TADOQuery;
    procedure BitBtn4Click(Sender: TObject);
    procedure BitBtn6Click(Sender: TObject);
    procedure M20Exit(Sender: TObject);
    procedure M21Exit(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure NumExit(Sender: TObject);
    procedure NumKeyPress(Sender: TObject; var Key: Char);
    procedure FormCreate(Sender: TObject);
    procedure M10Click(Sender: TObject);
    procedure MezoEllenor(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
  		kilep	: boolean;
  public
  end;

var
  AdatellDlg: TAdatellDlg;

implementation

uses

	J_SQL, Egyeb, Kozos, J_Fogyaszt;
{$R *.DFM}

procedure TAdatellDlg.BitBtn4Click(Sender: TObject);
var
	filename	: string;
   sorsz		: integer;
   tipus		: string;
   megjegyzes	: string;
   dat1		: string;
   dat2		: string;
   indok		: string;
   kellsor		: boolean;
   uzemar		: double;
   uameg		: double;
   kolts		: double;
   bevet		: double;
   szossz		: double;
   recno		: integer;
   voltsofor	: string;
   uzmegtak	: double;
begin
   if ( not cb11.Checked and not cb12.Checked and not cb13.Checked and not cb14.Checked
   	and not cb22.Checked and not cb23.Checked and not cb24.Checked ) then begin
		NoticeKi('Legal�bb egy felt�tel megad�sa k�telez�!');
       Exit;
   end;

	Screen.Cursor	:= crHourGlass;
	Memo1.Clear;

	kilep	:= false;

   if ( cb11.Checked or cb12.Checked or cb13.Checked or cb14.Checked ) then begin
       // A K�LTS�GEK ELLEN�RZ�SE
       Memo1.Lines.Add('K�lts�gek ellen�rz�se');
       Memo1.Lines.Add('*********************');
       // A sz�r�si felt�telek ki�r�sa
       if CB11.Checked then begin
           Memo1.Lines.Add('   K�lts�g hat�r : '+ Format('%.0f - %.0f Ft', [StringSzam(M10.Text), StringSzam(M11.Text)]));
       end;
       if CB12.Checked then begin
           Memo1.Lines.Add('   �zemanyag �r  : '+ Format('%.0f - %.0f Ft', [StringSzam(M12.Text), StringSzam(M13.Text)]));
       end;
       if CB13.Checked then begin
           Memo1.Lines.Add('   Null�s �rfolyam vizsg�lata');
       end;
       if CB14.Checked then begin
           Memo1.Lines.Add('   Null�s �zemanyag �r');
       end;
       Memo1.Lines.Add('');


       Query_Run(Query1, 'SELECT * FROM KOLTSEG ORDER BY KS_DATUM, KS_KTKOD ');
       if ( ( Query1.RecordCount > 0 ) and (not kilep) ) then begin
           Application.CreateForm(TFormGaugeDlg, FormGaugeDlg);
           FormGaugeDlg.GaugeNyit('K�lts�g adatok �sszegy�jt�se... ' , '', false);
           Memo1.Lines.Add( '   Ssz.   K�d   T�pus J�rat D�tum       �sszeg(Ft) Km. �ra  Rendsz. �z. me. �rfolyam �a. �r   Megjegyz�s           Probl�ma');
           Memo1.Lines.Add( '   ------ ----- ----- ----- ----------- ---------- -------- ------- ------- -------- -------- -------------------- ---------------');
           sorsz	:= 1;
           recno	:= 1;
           while ( ( not Query1.Eof ) and (not kilep) )  do begin
               Application.ProcessMessages;
               FormGaugeDlg.Pozicio ( ( recno * 100 ) div Query1.RecordCount ) ;
               Inc(recno);
               // Itt vannak a felt�tel eld�nt�sek
               kellsor	:= false;
               indok	:= '';
               if CB11.Checked then begin
                   if ( ( StringSzam(Query1.FieldByName('KS_ERTEK').AsString) >= StringSzam(M10.Text) ) and
                        ( ( StringSzam(Query1.FieldByName('KS_ERTEK').AsString) <= StringSzam(M11.Text) ) or (StringSzam(M11.Text) = 0 ) ) ) then begin
                       indok	:= 'K�LTS�G HAT�R';
                       kellsor	:= true;
                   end;
               end;
               uzemar	:= 0;
               if Query1.FieldByName('KS_TIPUS').AsString = '0' then begin
                   if StringSzam(Query1.FieldByName('KS_UZMENY').AsString) > 0 then begin
                       uzemar := 	StringSzam(Query1.FieldByName('KS_ERTEK').AsString) /
                                   StringSzam(Query1.FieldByName('KS_UZMENY').AsString);
                   end;
               end;
               if CB12.Checked then begin
                   // Az �zemanyag �r�nak vizsg�lata
                   if Query1.FieldByName('KS_TIPUS').AsString = '0' then begin
                       // Csak a tankol�sokat kell megn�zni
                       if ( ( uzemar >= StringSzam(M12.Text) ) and ( ( uzemar <= StringSzam(M13.Text) ) or (StringSzam(M13.Text) = 0 ) ) ) then begin
                           indok	:= '�ZEMA. �RT�K';
                           kellsor	:= true;
                       end;
                   end;
               end;
               if CB13.Checked then begin
                   if StringSzam(Query1.FieldByName('KS_ARFOLY').AsString) = 0 then begin
                       indok	:= '0 �RFOLYAM';
                       kellsor	:= true;
                   end;
               end;
               if CB14.Checked then begin
                   if ( ( StringSzam(Query1.FieldByName('KS_UZMENY').AsString) = 0 ) AND (Query1.FieldByName('KS_TIPUS').AsString = '0') ) then begin
                       indok	:= '0 �ZEMA. FT/L';
                       kellsor	:= true;
                   end;
               end;
               if not kellsor then begin
                   Query1.Next;
                   Continue;
               end;
               case StrToIntdef(Query1.FieldByName('KS_TIPUS').AsString,0) of
                   0 : tipus	:= '�a.k.';
                   1 : tipus	:= 'egy�b';
                   2 : tipus	:= 'ad b.';
                   3 : tipus	:= 'h�t� ';
               end;
               megjegyzes	:= Query1.FieldByName('KS_LEIRAS').AsString;
               Memo1.Lines.Add( Format('   %5d. ', [sorsz])+
                   copy(Query1.FieldByName('KS_KTKOD').AsString+URESSTRING,1,5)+' '+
                   copy(tipus+URESSTRING,1,5)+' '+
                   copy(Query1.FieldByName('KS_JAKOD').AsString+URESSTRING,1,6)+' '+
                   copy(Query1.FieldByName('KS_DATUM').AsString+URESSTRING,1,11)+' '+
                   Format('%10.0f', [StringSzam(Query1.FieldByName('KS_ERTEK').AsString)])+' '+
                   Format('%8.0d', [StrToIntDef(Query1.FieldByName('KS_KMORA').AsString, 0)])+' '+
                   copy(Query1.FieldByName('KS_RENDSZ').AsString+URESSTRING,1,7)+' '+
                   Format('%7.2f', [StringSzam(Query1.FieldByName('KS_UZMENY').AsString)])+' '+
                   Format('%8.3f', [StringSzam(Query1.FieldByName('KS_ARFOLY').AsString)])+' '+
                   Format('%8.2f', [uzemar])+' '+
                   copy(megjegyzes+URESSTRING,1,20) + ' ' +
                   copy(indok+URESSTRING,1,15)
                   );
               Inc(sorsz);
               Query1.Next;
           end;
           FormGaugeDlg.Destroy;
       end;
       if sorsz	= 1 then begin
			Memo1.Lines.Add('     Nincs a felt�teleknek megfelel� k�lts�g rekord!');
           Memo1.Lines.Add('');
       end;
   end;

   if ( cb22.Checked or cb23.Checked or cb24.Checked ) then begin
       // A J�RATOK ELLEN�RZ�SE
       Memo1.Lines.Add('');
       Memo1.Lines.Add('');
       Memo1.Lines.Add('J�ratok ellen�rz�se');
       Memo1.Lines.Add('*******************');
       // A sz�r�si felt�telek ki�r�sa
       if CB22.Checked then begin
           Memo1.Lines.Add('   �a. megtakar. : '+ Format('%.0f - %.0f Ft', [StringSzam(M22.Text), StringSzam(M23.Text)]));
       end;
       if CB23.Checked then begin
           Memo1.Lines.Add('   J�rat k�lts�g : '+ Format('%.0f - %.0f Ft', [StringSzam(M24.Text), StringSzam(M25.Text)]));
       end;
       if CB24.Checked then begin
           Memo1.Lines.Add('   J�rat bev�tel : '+ Format('%.0f - %.0f Ft', [StringSzam(M26.Text), StringSzam(M27.Text)]));
       end;
       Memo1.Lines.Add('');

       dat1	:= M20.Text;
       if dat1 = '' then begin
           dat1	:= '1000.01.01.';
       end;
       dat2	:= M21.Text;
       if dat2 = '' then begin
           dat2	:= EgyebDlg.MaiDatum;
       end;
       Application.CreateForm(TFogyasztasDlg, FogyasztasDlg);

       Query_Run(Query1, 'SELECT * FROM JARAT WHERE JA_JKEZD >= '''+dat1+''' AND JA_JKEZD <= '''+dat2+''' ORDER BY JA_ALKOD ');
       if ( ( Query1.RecordCount > 0 ) and (not kilep) ) then begin
           Application.CreateForm(TFormGaugeDlg, FormGaugeDlg);
           FormGaugeDlg.GaugeNyit('J�rat adatok �sszegy�jt�se... ' , '', false);
           Memo1.Lines.Add( '   Ssz.   J�ratsz�m Rendsz. J�rat kezd. J�rat bef.  Men. Sof�r(�k)                      �a.megt. K�lts�g    Bev�tel    Probl�ma');
           Memo1.Lines.Add( '   ------ --------- ------- ----------- ----------- ---- ------------------------------ -------- ---------- ---------- ---------------');
           sorsz	:= 1;
           recno	:= 1;
           while ( ( not Query1.Eof ) and (not kilep) ) do begin
               Application.ProcessMessages;
               FormGaugeDlg.Pozicio ( ( recno * 100 ) div Query1.RecordCount ) ;
               Inc(recno);
               // Az �rt�kek kisz�m�t�sa
               kellsor	:= false;
               indok	:= '';

               // A fogyaszt�s kisz�mol�sa
               FogyasztasDlg.ClearAllList;
               FogyasztasDlg.JaratBetoltes(Query1.FieldByname('JA_KOD').AsString);
               FogyasztasDlg.CreateTankLista;
				FogyasztasDlg.CreateAdBlueLista;
				FogyasztasDlg.Szakaszolo;

               // A dolgoz�i k�lts�gek meghat�roz�sa
               kolts		:= 0;
               voltsofor	:= '';
               Query_Run(Query3, 'SELECT * FROM JARSOFOR WHERE JS_JAKOD = '''+Query1.FieldByName('JA_KOD').AsString+''' ORDER BY JS_SORSZ ');
               while not Query3.Eof do begin
                   if Query3.FieldByname('JS_SOFOR').AsString <> '' then begin
                       // Betessz�k a sof�r fogyaszt�si adatokat k�l�n sorba
                       if Pos (Query3.FieldByname('JS_SOFOR').AsString, voltsofor) < 1 then begin
                           voltsofor	:= voltsofor + Query3.FieldByname('JS_SOFOR').AsString + ';';
                           if  Round(Query1.FieldByname('JA_OSSZKM').AsFloat ) > 0  then begin
                               uzmegtak	:= FogyasztasDlg.GetSoforMegtakaritas(Query3.FieldByname('JS_SOFOR').AsString);
                               if uzmegtak <> 0 then begin
                                   kolts   := kolts + uzmegtak * GetApehUzemanyag(Query1.FieldByName('JA_JKEZD').AsString, 1);
                               end;
                           end;
                       end;
                       kolts 	:= kolts + StringSzam(Query3.FieldByname('JS_SZAKM').AsString) * StringSzam(Query3.FieldByname('JS_JADIJ').AsString)
                           + StringSzam(Query3.FieldByname('JS_EXDIJ').AsString)
                           + StringSzam(Query3.FieldByname('JS_FELRA').AsString)*StringSzam(EgyebDlg.Read_SZGrid('CEG', '301'));
                   end;
                   Query3.Next;
               end;
               // A t�nyleges k�lts�gek kikeres�se
               Query_Run (Query3,'SELECT * FROM KOLTSEG WHERE KS_JAKOD = '''+Query1.FieldByName('JA_KOD').AsString+''' '+
                   ' AND KS_TETAN = 0 ORDER BY KS_RENDSZ DESC, KS_DATUM' ,true);
               while not Query3.EOF do begin
                   {Nett� �rt�ket sz�molunk}
                   if StrToIntdef(Query3.FieldByName('KS_TIPUS').AsString,0) <> 0 then begin		// Az �zemanyag k�lts�get nem vonjuk le, mert azt ut�na sz�moljuk
                       kolts	:= kolts + Query3.FieldByname('KS_OSSZEG').AsFloat * Query3.FieldByname('KS_ARFOLY').AsFloat + Query3.FieldByname('KS_AFAERT').AsFloat;
                   end;
                  Query3.Next;
               end;
               kolts  	:= kolts + FogyasztasDlg.GetKoltseg;

               {A fuvard�jak bet�lt�se}
               bevet	:= 0;
               Query_Run (Query2,'SELECT * FROM VISZONY WHERE VI_JAKOD = '''+Query1.FieldByName('JA_KOD').AsString+''' ',true);
               while not Query2.EOF do begin
                  szossz	:= Query2.FieldByname('VI_FDEXP').AsFloat * Query2.FieldByname('VI_ARFOLY').AsFloat;
                  {Megkeress�k a sz�ml�t a viszonyhoz}
                  if Query2.FieldByname('VI_SAKOD').AsString <> '' then begin
                     if Query_Run (Query3,'SELECT * FROM SZFEJ WHERE SA_UJKOD = '+Query2.FieldByname('VI_UJKOD').AsString,true) then begin
                        if Query3.RecordCount > 0 then begin
                           szossz		:= Query3.FieldByname('SA_OSSZEG').AsFloat;
                        end;
                     end;
                  end;
                  bevet	:= bevet +  szossz;
                  Query2.Next;
               end;

               // Az �zemanyag megtakar�t�s sz�m�t�sa

               if FogyasztasDlg.GetOsszKm = 0 then begin
                   uameg	:= 0;
               end else begin
					uameg 	:=  FogyasztasDlg.GetMegtakaritas *100 / FogyasztasDlg.GetOsszKm;
               end;
               if CB22.Checked then begin
                   if ( ( uameg >= StringSzam(M22.Text) ) and ( ( uameg <= StringSzam(M23.Text) ) or (StringSzam(M23.Text) = 0 ) ) ) then begin
                       indok	:= 'MEGTAKAR�T�S H.';
                       kellsor	:= true;
                   end;
               end;

               if CB23.Checked then begin
                   if ( ( kolts >= StringSzam(M24.Text) ) and ( ( kolts <= StringSzam(M25.Text) ) or (StringSzam(M25.Text) = 0 ) ) ) then begin
                       indok	:= 'K�LTS�G HAT�R  ';
                       kellsor	:= true;
                   end;
               end;

               if CB24.Checked then begin
                   if ( ( bevet >= StringSzam(M26.Text) ) and ( ( bevet <= StringSzam(M27.Text) ) or (StringSzam(M27.Text) = 0 ) ) ) then begin
                       indok	:= 'BEV�TEL HAT�R  ';
                       kellsor	:= true;
                   end;
               end;
               if not kellsor then begin
                   Query1.Next;
                   Continue;
               end;
               Memo1.Lines.Add( Format('   %5d. ', [sorsz])+
                   GetJaratszam(Query1.FieldByName('JA_KOD').AsString) + ' ' +
                   copy(Query1.FieldByName('JA_RENDSZ').AsString+URESSTRING,1,7)+' '+
                   copy(Query1.FieldByName('JA_JKEZD').AsString+URESSTRING,1,11)+' '+
                   copy(Query1.FieldByName('JA_JVEGE').AsString+URESSTRING,1,11)+' '+
                   copy(Query1.FieldByName('JA_MENSZ').AsString+URESSTRING,1,4)+' '+
                   copy(Query1.FieldByName('JA_SNEV1').AsString+'; '+Query1.FieldByName('JA_SNEV2').AsString+URESSTRING,1,30)+' '+
                   Format('%8.2f', [uameg])+' '+
                   Format('%10.0f', [kolts])+' '+
                   Format('%10.0f', [bevet])+' '+
                   copy(indok+URESSTRING,1,15)
                   );
               Inc(sorsz);
               Query1.Next;
           end;
           FormGaugeDlg.Destroy;
       end;
		FogyasztasDlg.Destroy;
       if sorsz	= 1 then begin
           Memo1.Lines.Add('     Nincs a felt�teleknek megfelel� j�rat rekord!');
           Memo1.Lines.Add('');
       end;
   end;

	Screen.Cursor	:= crDefault;
   filename		:= EgyebDlg.GetTempFileName;
   Memo1.Lines.SaveToFile(filename);
   EgyebDlg.FileMutato(
       filename,
       'Adatellen�rz�s',
       '',
       '',
       '',
       '',
       1);
   kilep	:= true;
end;

procedure TAdatellDlg.BitBtn6Click(Sender: TObject);
begin
	if not kilep then begin
   	kilep	:= true;
   end else begin
		Close;
   end;
end;

procedure TAdatellDlg.M20Exit(Sender: TObject);
begin
	DatumExit(Sender, false);
  	EgyebDlg.ElsoNapEllenor(M20, M21);
end;

procedure TAdatellDlg.M21Exit(Sender: TObject);
begin
	DatumExit(Sender, false);
end;

procedure TAdatellDlg.BitBtn2Click(Sender: TObject);
begin
	// EgyebDlg.Calendarbe(M20);
  // M20.Text := copy(M20.Text,1,8)+'01.';
  // EgyebDlg.ElsoNapEllenor(M20, M21);
  EgyebDlg.CalendarBe_idoszak(M20, M21);

end;

procedure TAdatellDlg.BitBtn3Click(Sender: TObject);
begin
	EgyebDlg.Calendarbe(M21, True);
end;

procedure TAdatellDlg.NumExit(Sender: TObject);
begin
	SzamExit(Sender);
end;

procedure TAdatellDlg.NumKeyPress(Sender: TObject; var Key: Char);
begin
	SzamKeyPress(Sender, Key);
end;

procedure TAdatellDlg.FormCreate(Sender: TObject);
begin
	EgyebDlg.SetADOQueryDatabase(Query1);
	EgyebDlg.SetADOQueryDatabase(Query2);
	EgyebDlg.SetADOQueryDatabase(Query3);
   kilep	:= true;
end;

procedure TAdatellDlg.M10Click(Sender: TObject);
begin
	(Sender as TMaskEdit).SelectAll;
end;

procedure TAdatellDlg.MezoEllenor(Sender: TObject);
begin
	if CB11.Checked then begin
		SetMaskEdits([M10, M11], 1);
	end else begin
		M10.Text	:= '';
		M11.Text	:= '';
		SetMaskEdits([M10, M11]);
	end;
	if CB12.Checked then begin
		SetMaskEdits([M12, M13], 1);
	end else begin
		M12.Text	:= '';
		M13.Text	:= '';
		SetMaskEdits([M12, M13]);
	end;
	if CB22.Checked then begin
		SetMaskEdits([M22, M23], 1);
	end else begin
		M22.Text	:= '';
		M23.Text	:= '';
		SetMaskEdits([M22, M23]);
	end;
	if CB23.Checked then begin
		SetMaskEdits([M24, M25], 1);
	end else begin
		M24.Text	:= '';
		M25.Text	:= '';
		SetMaskEdits([M24, M25]);
	end;
	if CB24.Checked then begin
		SetMaskEdits([M26, M27], 1);
	end else begin
		M26.Text	:= '';
		M27.Text	:= '';
		SetMaskEdits([M26, M27]);
	end;
end;

procedure TAdatellDlg.FormShow(Sender: TObject);
begin
	MezoEllenor(Sender);
end;

end.


