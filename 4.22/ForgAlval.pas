unit ForgAlval;

interface

uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls, Egyeb,
  Forms, Dialogs, DBTables, DB, StdCtrls, Buttons, Mask, DBCtrls,
  Printers, ExtCtrls, ADODB,DateUtils;

type
  TForgAlvalDlg = class(TForm)
    Label1: TLabel;
    BitBtn4: TBitBtn;
    BitBtn6: TBitBtn;
    M1: TMaskEdit;
	 M2: TMaskEdit;
	 Label2: TLabel;
	 CB1: TComboBox;
	 BitBtn10: TBitBtn;
	 BitBtn1: TBitBtn;
	 CheckBox4: TCheckBox;
	 Query1: TADOQuery;
    CB2: TComboBox;
    RadioButton1: TRadioButton;
    RadioButton2: TRadioButton;
    CheckBox1: TCheckBox;
    Edit1: TEdit;
    Label3: TLabel;
    CheckBox2: TCheckBox;
    CheckBox3: TCheckBox;
    Edit2: TEdit;
    Label4: TLabel;
	 procedure FormCreate(Sender: TObject);
	 procedure BitBtn4Click(Sender: TObject);
	 procedure BitBtn6Click(Sender: TObject);
	 procedure M1Click(Sender: TObject);
	 procedure BitBtn10Click(Sender: TObject);
	 procedure BitBtn1Click(Sender: TObject);
	 procedure M1Exit(Sender: TObject);
	 procedure RadioButton1Click(Sender: TObject);
    procedure CheckBox2Click(Sender: TObject);
    procedure CheckBox3Click(Sender: TObject);
  private
  public
  end;

var
  ForgAlvalDlg: TForgAlvalDlg;

implementation

uses
	Kialval, Kialval2, j_sql, Kozos;
{$R *.DFM}

procedure TForgAlvalDlg.FormCreate(Sender: TObject);
begin
	M2.Text := EgyebDlg.MaiDatum;
	EgyebDlg.SetADOQueryDatabase(Query1);
	// Az alv�llalkoz�k felt�lt�se
	Query_Run(Query1, 'SELECT DISTINCT AJ_ALNEV FROM ALJARAT WHERE AJ_ALNEV <> '''' ORDER BY AJ_ALNEV');
	CB1.Clear;
	CB1.Items.Add('');
	while not Query1.Eof do begin
		CB1.Items.Add(Query1.FieldByName('AJ_ALNEV').AsString);
		Query1.Next;
	end;
	CB1.Itemindex	:= 0;
	// A megb�z�k felt�lt�se
	Query_Run(Query1, 'SELECT DISTINCT VI_VENEV FROM VISZONY WHERE VI_JAKOD IN (SELECT AJ_JAKOD FROM ALJARAT WHERE AJ_ALNEV <> '''' ) ORDER BY VI_VENEV');
	CB2.Clear;
	CB2.Items.Add('');
	while not Query1.Eof do begin
		CB2.Items.Add(Query1.FieldByName('VI_VENEV').AsString);
		Query1.Next;
	end;
	CB2.Itemindex	:= 0;
	RadioButton1Click(Sender);
end;

procedure TForgAlvalDlg.BitBtn4Click(Sender: TObject);
begin
	if not DatumExit(M1, false) then begin
		Exit;
	end;
	if not DatumExit(M2, false) then begin
		Exit;
	end;
	Screen.Cursor	:= crHourGlass;
	if RadioButton1.Checked then begin
		Application.CreateForm(TKiAlvalDlg, KiAlvalDlg);
		if KiAlvalDlg.Tolt(M1.Text, M2.Text, CB1.Text) then begin
			Screen.Cursor			:= crDefault;
			KiAlvalDlg.Reszletes	:= CheckBox4.Checked;
			KiAlvalDlg.Rep.Preview;
		end else begin
			Screen.Cursor			:= crDefault;
			NoticeKi('Nincs a megadott felt�teleknek megfelel� t�tel!');
		end;
		KiAlvalDlg.Destroy;
	end else begin
		Application.CreateForm(TKiAlval2Dlg, KiAlval2Dlg);
		if KiAlval2Dlg.Tolt(M1.Text, M2.Text, CB2.Text) then begin
			Screen.Cursor			:= crDefault;
			KiAlval2Dlg.Reszletes	:= CheckBox4.Checked;
			KiAlval2Dlg.Rep.Preview;
		end else begin
			Screen.Cursor			:= crDefault;
			NoticeKi('Nincs a megadott felt�teleknek megfelel� t�tel!');
		end;
		KiAlval2Dlg.Destroy;
	end;
end;

procedure TForgAlvalDlg.BitBtn6Click(Sender: TObject);
begin
	Close;
end;

procedure TForgAlvalDlg.M1Click(Sender: TObject);
begin
	with Sender as TMaskEdit do begin
		SelectAll;
	end;
end;

procedure TForgAlvalDlg.BitBtn10Click(Sender: TObject);
begin
	// EgyebDlg.Calendarbe(M1);
	// EgyebDlg.ElsoNapEllenor(M1, M2);
  EgyebDlg.CalendarBe_idoszak(M1, M2);
end;

procedure TForgAlvalDlg.BitBtn1Click(Sender: TObject);
begin
	EgyebDlg.Calendarbe(M2);
  EgyebDlg.UtolsoNap(M2);
  // M2.Text:=DateToStr(EndOfTheMonth(StrToDate(M2.Text)));
end;

procedure TForgAlvalDlg.M1Exit(Sender: TObject);
begin
	DatumExit(M1,false);
	EgyebDlg.ElsoNapEllenor(M1, M2);
end;

procedure TForgAlvalDlg.RadioButton1Click(Sender: TObject);
begin
	// Az alv�llalkoz�/megb�z� v�lt�sa
	if RadioButton1.Checked then begin
		CB1.Enabled	:= true;
		CB2.Enabled	:= false;
	end else begin
		CB1.Enabled	:= false;
		CB2.Enabled	:= true;
	end;
end;

procedure TForgAlvalDlg.CheckBox2Click(Sender: TObject);
begin
  CheckBox3.Checked:=not CheckBox2.Checked;
end;

procedure TForgAlvalDlg.CheckBox3Click(Sender: TObject);
begin
  CheckBox2.Checked:=not CheckBox3.Checked;
end;

end.


