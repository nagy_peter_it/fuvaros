unit Ellenorbe;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Grids, Vcl.Buttons,
  Vcl.Mask, Vcl.ExtCtrls, StrUtils, Data.DB, Data.Win.ADODB;

type
  TEllenorbeDlg = class(TForm)
    GroupBox2: TGroupBox;
    Label1: TLabel;
    CB1: TComboBox;
    Label2: TLabel;
    M0: TMaskEdit;
    BitBtn1: TBitBtn;
    SG1: TStringGrid;
    Memo1: TMemo;
    BitBtn2: TBitBtn;
    BitKilep: TBitBtn;
    OpenDialog1: TOpenDialog;
    Label3: TLabel;
    QueryAl: TADOQuery;
    QueryAl2: TADOQuery;
    QRsz: TADOQuery;
    Splitter1: TSplitter;
    BitBtn3: TBitBtn;
    QFo: TADOQuery;
    procedure BitKilepClick(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    function  GetMezoSorszam( meznev : string ) : integer;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure OlvasasEtoll;
    procedure OlvasasHugo;
    procedure OlvasasAutoDe;
    procedure OlvasasShell;
    procedure OlvasasAutoAT;
    procedure Elokeszito;
    procedure KoltsegGyujtoBeiro( fimod, megje, ktipstr : string );
    function  GetDatum(dstr : string) : string;
    function  GetBeodat( fimod : string ) : string;
    procedure MezonevKorrigalo;
    function  VanRendszam( rsz : string) : boolean;
    procedure BitBtn3Click(Sender: TObject);
  private
    dir        : string;
    rec_szam   : integer;
    mezolista	: TStringList;
    mezonevek	: TStringList;
    leiras     : string;
    valnem     : string;
    ret_kod    : integer;
    fname      : string;
    rsz        : integer;
    F          : Textfile;
    rendsz     : string;
    datum      : string;
    ido        : string;
    osszeg     : double;
    km         : double;
    menny      : double;
    sor        : string;
    orsz       : string;
    tipus      : string;
    sorsz      : integer;
  public

  end;

var
  EllenorbeDlg: TEllenorbeDlg;

implementation

{$R *.dfm}

uses
   Kozos, Egyeb, J_Sql, LGauge, J_Excel, J_Export;

procedure TEllenorbeDlg.FormCreate(Sender: TObject);
begin
	EgyebDlg.SeTADOQueryDatabase(QueryAl);
	EgyebDlg.SeTADOQueryDatabase(QRsz);
	EgyebDlg.SeTADOQueryDatabase(QueryAl2);
	EgyebDlg.SeTADOQueryDatabase(QFo);
   mezolista	    := TStringList.Create;
   mezonevek	    := TStringList.Create;
   CB1.ItemIndex   := 5;
   SetMaskEdits([M0]);
   Query_Run(QRsz, 'SELECT GK_RESZ FROM GEPKOCSI ORDER BY 1 ');
end;

procedure TEllenorbeDlg.BitBtn1Click(Sender: TObject);
begin
   // A k�nyvt�r beolvas�sa
   OpenLocalIni(1);
   case CB1.Itemindex of
       0:  dir	:= ReadLocalIni('DIRECTORIES', 'DIR_AUTODE', '');       // Aut�p�lya DE
       1:  dir	:= ReadLocalIni('DIRECTORIES', 'DIR_SHELL', '');        // SHELL
       2:  dir	:= ReadLocalIni('DIRECTORIES', 'DIR_EUROTOLL', '');     // EUROTOLL
       3:  dir	:= ReadLocalIni('DIRECTORIES', 'DIR_HUGO', '');         // HU-GO
       4:  dir	:= ReadLocalIni('DIRECTORIES', 'DIR_AUTOAT', '');       // AT aut�p�lya
   end;
   CloseLocalIni;
   OpenDialog1.InitialDir  := dir;
   if OpenDialog1.Execute then begin
       dir     := ExtractFilePath( OpenDialog1.FileName);
       M0.Text := ExtractFileName( OpenDialog1.FileName);
       // A k�nyvt�r elment�se
       OpenLocalIni(1);
       case CB1.Itemindex of
           0: WriteLocalIni('DIRECTORIES', 'DIR_AUTODE', dir);         // Aut�p�lya DE
           1: WriteLocalIni('DIRECTORIES', 'DIR_SHELL', dir);          // SHELL
           2: WriteLocalIni('DIRECTORIES', 'DIR_EUROTOLL', dir);       // EUROTOLL
           3: WriteLocalIni('DIRECTORIES', 'DIR_HUGO', dir);           // HU-GO
           4: WriteLocalIni('DIRECTORIES', 'DIR_AUTOAT', dir);         // AT aut�p�lya
       end;
       CloseLocalIni;
   end else begin
       dir     := '';
       M0.Text := '';
   end;
end;

procedure TEllenorbeDlg.BitBtn2Click(Sender: TObject);
var
   ii : integer;
begin
   if M0.Text = '' then begin
       NoticeKi('Hi�nyz� bemeneti �llom�ny!');
       Exit;
   end;
   for ii:=0 to opendialog1.Files.count-1 do begin
       dir     := ExtractFilePath( opendialog1.files.strings[ii]);
       M0.Text := ExtractFileName( opendialog1.files.strings[ii]);
       // Ellen�rizz�k, hogy az �llom�ny beolvas�sra ker�lt-e m�r.
       Query_Run(QueryAl, 'SELECT KS_IMPFN FROM KOLTSEG_GYUJTO WHERE KS_IMPFN = '''+M0.Text+''' ');
       if QueryAl.Recordcount =0 then begin
           Memo1.Lines.Add('');
           Memo1.Lines.Add('Ez az �llom�ny m�g nem ker�lt import�l�sra.');
       end else begin
           rec_szam        := Sorszamolo(dir+M0.Text);
           if rec_szam > 0 then begin
               Screen.Cursor       := crHourGlass;
               BitBtn2.Hide;
               BitBtn1.Enabled     := false;
               BitKilep.Enabled    := false;
               CB1.Enabled         := false;
               case CB1.Itemindex of
                   0:  OlvasasAutoDE;      // Aut�p�lya DE
                   1:  OlvasasShell;       // SHELL
                   2:  OlvasasEtoll;       // EUROTOLL
                   3:  OlvasasHugo;        // HU_GO
                   4:  OlvasasAutoAT;      // AT aut�p�lya
               end;
               BitBtn1.Enabled     := true;
               BitKilep.Enabled    := true;
               BitBtn2.Show;
               Screen.Cursor:=crDefault;
           end else begin
               Memo1.Lines.Add('');
               Memo1.Lines.Add('Az �llom�ny megnyit�sa nem siker�lt! Lehet hogy haszn�latban van!');
           end;
       end;
   end;
end;

procedure TEllenorbeDlg.BitBtn3Click(Sender: TObject);
var
   ii : integer;
   ktip    : string;
begin
   // Az adott t�pus minden �llom�ny�t ellen�rizz�k
   case CB1.Itemindex of
       0:  ktip    := '0';      // Aut�p�lya DE
       1:  ktip    := '3';       // SHELL
       2:  ktip    := '5';       // EUROTOLL
       3:  ktip    := '7';      // HU_GO
       4:  ktip    := '2';      // AT aut�p�lya
   end;
   Query_Run(QFo, 'SELECT DISTINCT KS_IMPFN FROM KOLTSEG_GYUJTO WHERE KS_KTIP = '+ktip);

   while not QFo.Eof do begin
       dir     := ExtractFilePath( QFo.FieldByName('KS_IMPFN').AsString);
       M0.Text := ExtractFileName( QFo.FieldByName('KS_IMPFN').AsString);
       // Ellen�rizz�k, hogy az �llom�ny beolvas�sra ker�lt-e m�r.
       Query_Run(QueryAl, 'SELECT KS_IMPFN FROM KOLTSEG_GYUJTO WHERE KS_IMPFN = '''+M0.Text+''' ');
       if QueryAl.Recordcount =0 then begin
           Memo1.Lines.Add('');
           Memo1.Lines.Add('Az �llom�ny m�g nem ker�lt import�l�sra :'+dir+M0.Text);
       end else begin
           rec_szam        := Sorszamolo(dir+M0.Text);
           if rec_szam > 0 then begin
               Screen.Cursor       := crHourGlass;
               BitBtn2.Hide;
               BitBtn1.Enabled     := false;
               BitKilep.Enabled    := false;
               CB1.Enabled         := false;
               case CB1.Itemindex of
                   0:  OlvasasAutoDE;      // Aut�p�lya DE
                   1:  OlvasasShell;       // SHELL
                   2:  OlvasasEtoll;       // EUROTOLL
                   3:  OlvasasHugo;        // HU_GO
                   4:  OlvasasAutoAT;      // AT aut�p�lya
               end;
               BitBtn1.Enabled     := true;
               BitKilep.Enabled    := true;
               BitBtn2.Show;
               Screen.Cursor:=crDefault;
           end else begin
               Memo1.Lines.Add('');
               Memo1.Lines.Add('Az �llom�ny megnyit�sa nem siker�lt : '+dir+M0.Text);
           end;
       end;
       QFo.Next;
   end;
end;

procedure TEllenorbeDlg.BitKilepClick(Sender: TObject);
begin
   Close;
end;

procedure TEllenorbeDlg.OlvasasEtoll;
var
   ertek   : double;
begin
   Elokeszito;
   // Az els� sor beolvas�sa -> ez lesz a mez�nevek list�ja
   ReadLn(F, sor);
   StringParse(sor, ';', mezonevek);
   MezonevKorrigalo;
   Memo1.Lines.Add('');
   Memo1.Lines.Add('Az EUROTOLL adatok feldolgoz�sa elkezd�d�tt ( '+FormatDateTime('YYYY.MM.DD. HH.NN.SS', now)+' )   '+dir+M0.Text);
   Memo1.Lines.Add('');
   Memo1.Lines.Add('A beolvasott rekordok sz�ma : '+IntToStr(rec_szam));
   if ( ( GetMezoSorszam( 'Immatriculation') < 0 ) or ( GetMezoSorszam( 'Horodatage de sortie' ) < 0 ) or ( GetMezoSorszam( 'Euros HT' ) < 0 ) or ( GetMezoSorszam( 'Pays' ) < 0 ) ) then begin
       NoticeKi('Egy fontos mez� hi�nyzik az �llom�nyb�l : Immatriculation; Horodatage de sortie; Euros HT; Pays');
       Exit;
   end;
   // A t�bl�zat felt�lt�se
   while not Eof(F) do begin
       Application.ProcessMessages;
       ReadLn(F, sor);
       if copy(sor, 1, 10) = ';;;;;;;;;;' then begin
           Continue;
       end;
       mezolista.Clear;
       StringParse(sor, ';', mezolista);
       // Mez�k ellen�rz�se
       rendsz  := mezolista[GetMezoSorszam( 'Immatriculation' )];
       rendsz  := Trim(StringReplace(rendsz, 'VIRT', '', [rfReplaceAll]));
       rendsz  := LeftStr(rendsz,3)+'-'+RightStr(rendsz, 3);
       if rendsz = '-' then begin
           EllenListAppend('A rendsz�m mez� �res ! '+mezolista[GetMezoSorszam( 'Horodatage de sortie')]+';'+mezolista[GetMezoSorszam('Euros HT')]);
           Continue;
       end;
       if not Vanrendszam(rendsz) then begin
           EllenListAppend('Hi�nyz� rendsz�m :'+mezolista[GetMezoSorszam( 'Horodatage de sortie')]+';'+mezolista[GetMezoSorszam('Euros HT')]+';'+rendsz);
           Continue;
       end;
       datum   := copy(mezolista[GetMezoSorszam( 'Horodatage de sortie')],1,10)+'.';
       if not Jodatum(datum) then begin
           EllenListAppend('�rv�nytelen d�tum ! '+mezolista[GetMezoSorszam( 'Horodatage de sortie' )]+';'+mezolista[GetMezoSorszam('Euros HT')]);
           Continue;
       end;
       ido     := Trim(copy(mezolista[GetMezoSorszam( 'Horodatage de sortie')],12,6));
       if not Joido(ido) then begin
           ido := '';
       end;
       orsz  := Trim(mezolista[GetMezoSorszam('Pays')]);
       if orsz='ESPAGNE'      then orsz:='ES' else
       if orsz='FRANCE'       then orsz:='FR' else
       if orsz='ALLEMAGNE'    then orsz:='DE' else
       if orsz='AUTRICHE'     then orsz:='AT' else
       if orsz='PORTUGAL'     then orsz:='PT' else
                                   orsz:='??';
       valnem              := 'EUR';
       tipus               := '1';
       leiras              := 'Autop�lya';
       osszeg              := StringSzam( mezolista[GetMezoSorszam('Euros HT')]);
       SG1.Cells[0,rsz]    := IntToStr(rsz);
       SG1.Cells[1,rsz]    := rendsz;
       SG1.Cells[2,rsz]    := datum;
       SG1.Cells[3,rsz]    := ido;
       SG1.Cells[4,rsz]    := orsz;
       SG1.Cells[5,rsz]    := Format('%.2f', [osszeg]);
       SG1.Cells[7,rsz]    := valnem;
       SG1.Cells[8,rsz]    := tipus;
       SG1.Cells[9,rsz]    := leiras;
       ertek               := 0;
       SG1.Cells[10,rsz]   := Format('%.2f', [ertek]);     // Mennyis�g
       SG1.Cells[11,rsz]   := Format('%.2f', [ertek]);     // �FA %
       rsz                 := rsz + 1;
   end;
   SG1.RowCount    := rsz;
   CloseFile(f);
   KoltsegGyujtoBeiro('ETOLL', 'Aut�p�lya d�j import EUROTOLL', '5');
end;

procedure TEllenorbeDlg.OlvasasHugo;
var
   ertek       : double;
   datumstr    : string;
begin
   Elokeszito;
   // Az els� sor beolvas�sa -> ez lesz a mez�nevek list�ja
   ReadLn(F, sor);
   StringParse(sor, ';', mezonevek);
   MezonevKorrigalo;
   Memo1.Lines.Add('');
   Memo1.Lines.Add('A HU-GO adatok feldolgoz�sa elkezd�d�tt ( '+FormatDateTime('YYYY.MM.DD. HH.NN.SS', now)+' )   '+dir+M0.Text);
   Memo1.Lines.Add('');
   Memo1.Lines.Add('A beolvasott rekordok sz�ma : '+IntToStr(rec_szam));
   if ( ( GetMezoSorszam( 'Rendsz�m') < 0 ) or ( GetMezoSorszam( 'K�lts�g') < 0 ) or ( GetMezoSorszam( 'Haszn�lat ideje') < 0 ) ) then begin
       NoticeKi('Egy fontos mez� hi�nyzik az �llom�nyb�l : Rendsz�m; K�lts�g; Haszn�lat ideje');
       Exit;
   end;
   // A t�bl�zat felt�lt�se
   while not Eof(F) do begin
       Application.ProcessMessages;
       ReadLn(F, sor);
       if copy(sor, 1, 3) <> 'OBU' then begin    // MINDEN SORNAK OBU -val kell kezd�dnie
           Continue;
       end;
       mezolista.Clear;
       StringParse(sor, ';', mezolista);
       // Mez�k ellen�rz�se
       rendsz  := mezolista[GetMezoSorszam( 'Rendsz�m')];
       rendsz  := Trim(StringReplace(rendsz, 'VIRT', '', [rfReplaceAll]));
       rendsz  := LeftStr(rendsz,3)+'-'+RightStr(rendsz, 3);
       if rendsz = '-' then begin
           EllenListAppend('A rendsz�m mez� �res ! '+mezolista[GetMezoSorszam( 'Haszn�lat ideje')]+';'+mezolista[GetMezoSorszam('K�lts�g')]);
           Continue;
       end;
       if not Vanrendszam(rendsz) then begin
           EllenListAppend('Hi�nyz� rendsz�m :'+mezolista[GetMezoSorszam( 'Haszn�lat ideje')]+';'+mezolista[GetMezoSorszam('K�lts�g')]+';'+rendsz);
           Continue;
       end;
       datumstr    := mezolista[GetMezoSorszam( 'Haszn�lat ideje')];
       if copy(datumstr, 1, 1) = '"' then begin
           datumstr    := copy(datumstr, 2, Length(datumstr)-2);
       end;
       datum       := copy(datumstr,1,10)+'.';
       if not Jodatum(datum) then begin
           EllenListAppend('�rv�nytelen d�tum ! '+mezolista[GetMezoSorszam( 'Haszn�lat ideje')]+';'+mezolista[GetMezoSorszam('K�lts�g')]);
           Continue;
       end;
       ido     := Trim(copy(mezolista[GetMezoSorszam( 'Haszn�lat ideje')],12,6));
       if not Joido(ido) then begin
           ido := '';
       end;
       orsz                := 'HU';
       valnem              := 'HUF';
       tipus               := '1';
       leiras              := 'HU-GO';
       osszeg              := StringSzam( mezolista[GetMezoSorszam('K�lts�g')]);
       if osszeg = 0 then begin
           EllenListAppend('�sszeg = 0 !'+mezolista[GetMezoSorszam( 'Haszn�lat ideje')]+';'+rendsz);
           Continue;
       end;
       SG1.Cells[0,rsz]    := IntToStr(rsz);
       SG1.Cells[1,rsz]    := rendsz;
       SG1.Cells[2,rsz]    := datum;
       SG1.Cells[3,rsz]    := ido;
       SG1.Cells[4,rsz]    := orsz;
       SG1.Cells[5,rsz]    := Format('%.2f', [osszeg/1.27]);       // A nett� �sszeget t�roljuk
       SG1.Cells[7,rsz]    := valnem;
       SG1.Cells[8,rsz]    := tipus;
       SG1.Cells[9,rsz]    := leiras;
       ertek               := 0;
       SG1.Cells[10,rsz]   := Format('%.2f', [ertek]);     // Mennyis�g
       SG1.Cells[11,rsz]   := Format('%.2f', [ertek]);     // �FA %
       rsz                 := rsz + 1;
   end;
   SG1.RowCount    := rsz;
   CloseFile(f);
   KoltsegGyujtoBeiro ('HU-GO', 'HU-GO import', '7');
end;

procedure TEllenorbeDlg.OlvasasAutoAT;
var
   ertek           : double;
   datumstr        : string;
   rendszam_str    : string;
   koltseg_str     : string;
   datumido_str    : string;
begin
   Elokeszito;
   // Az els� sor beolvas�sa -> ez lesz a mez�nevek list�ja
   ReadLn(F, sor);
   StringParse(sor, ';', mezonevek);
   MezonevKorrigalo;
   Memo1.Lines.Add('');
   Memo1.Lines.Add('Az AT aut�p�lya adatok feldolgoz�sa elkezd�d�tt ( '+FormatDateTime('YYYY.MM.DD. HH.NN.SS', now)+' )   '+dir+M0.Text);
   Memo1.Lines.Add('');
   rendszam_str    := 'Kfz-Kennzeichen';
   koltseg_str     := 'Netto';
   datumido_str    := 'Datum / Zeit';

   Memo1.Lines.Add('A beolvasott rekordok sz�ma : '+IntToStr(rec_szam));
   if ( ( GetMezoSorszam( rendszam_str ) < 0 ) or ( GetMezoSorszam( koltseg_str ) < 0 ) or ( GetMezoSorszam( datumido_str ) < 0 ) ) then begin
       NoticeKi('Egy fontos mez� hi�nyzik az �llom�nyb�l : '+rendszam_str+'; '+koltseg_str+'; '+datumido_str);
       Exit;
   end;
   // A t�bl�zat felt�lt�se
   while not Eof(F) do begin
       Application.ProcessMessages;
       ReadLn(F, sor);
       if copy(sor, 1, 5) = 'Datum' then begin    // ism�tlik n�ha a fejl�cet !!!!!
           Continue;
       end;
       mezolista.Clear;
       StringParse(sor, ';', mezolista);
       // Mez�k ellen�rz�se
       rendsz  := mezolista[GetMezoSorszam( rendszam_str )];
       rendsz  := Trim(StringReplace(rendsz, 'VIRT', '', [rfReplaceAll]));
       rendsz  := LeftStr(rendsz,3)+'-'+RightStr(rendsz, 3);
       if rendsz = '-' then begin
           EllenListAppend('A rendsz�m mez� �res ! '+mezolista[GetMezoSorszam( datumido_str )]+';'+mezolista[GetMezoSorszam(koltseg_str)]);
           Continue;
       end;
       if not Vanrendszam(rendsz) then begin
           EllenListAppend('Hi�nyz� rendsz�m :'+mezolista[GetMezoSorszam( datumido_str )]+';'+mezolista[GetMezoSorszam(koltseg_str)]+';'+rendsz);
           Continue;
       end;
       datumstr    := mezolista[GetMezoSorszam( datumido_str )];
       datum       := copy(datumstr,7,4)+'.'+copy(datumstr,4,2)+'.'+copy(datumstr,1,2)+'.';
       if not Jodatum(datum) then begin
           EllenListAppend('�rv�nytelen d�tum ! '+mezolista[GetMezoSorszam( datumido_str )]+';'+mezolista[GetMezoSorszam('K�lts�g')]);
           Continue;
       end;
       ido     := Trim(copy(mezolista[GetMezoSorszam( datumido_str )],12,8));
       if not Joido(ido) then begin
           ido := '';
       end;
       ido     := copy(ido, 1, 5);         // Le kell v�gnunk, mert az adatb�zisban csak 5 van
       orsz                := 'AT';
       valnem              := 'EUR';
       tipus               := '1';
       leiras              := 'Aut�p�lya';
       osszeg              := StringSzam( mezolista[GetMezoSorszam(koltseg_str)]);
       if osszeg = 0 then begin
           EllenListAppend('�sszeg = 0 !'+mezolista[GetMezoSorszam( datumido_str )]+';'+rendsz);
           Continue;
       end;
       SG1.Cells[0,rsz]    := IntToStr(rsz);
       SG1.Cells[1,rsz]    := rendsz;
       SG1.Cells[2,rsz]    := datum;
       SG1.Cells[3,rsz]    := ido;
       SG1.Cells[4,rsz]    := orsz;
       SG1.Cells[5,rsz]    := Format('%.2f', [osszeg]);       // A nett� �sszeget t�roljuk
       SG1.Cells[7,rsz]    := valnem;
       SG1.Cells[8,rsz]    := tipus;
       SG1.Cells[9,rsz]    := leiras;
       ertek               := 0;
       SG1.Cells[10,rsz]   := Format('%.2f', [ertek]);     // Mennyis�g
       SG1.Cells[11,rsz]   := Format('%.2f', [ertek]);     // �FA %
       rsz                 := rsz + 1;
   end;
   SG1.RowCount    := rsz;
   CloseFile(f);
   KoltsegGyujtoBeiro ('ASFIN', 'Aut�p�lya d�j import AT', '2');
end;

procedure TEllenorbeDlg.OlvasasAutoDe;
var
   ertek   : double;
begin
   Elokeszito;
   // Az els� sor beolvas�sa -> ez lesz a mez�nevek list�ja
   ReadLn(F, sor);
   StringParse(sor, ';', mezonevek);
   MezonevKorrigalo;
   Memo1.Lines.Add('');
   Memo1.Lines.Add('Az Aut�p�lya DE adatok feldolgoz�sa elkezd�d�tt ( '+FormatDateTime('YYYY.MM.DD. HH.NN.SS', now)+' )   '+dir+M0.Text);
   Memo1.Lines.Add('');
   Memo1.Lines.Add('A beolvasott rekordok sz�ma : '+IntToStr(rec_szam));
   // Az �llom�ny olvas�sa, a t�bl�zat felt�lt�se
   while not Eof(F) do begin
       Application.ProcessMessages;
       ReadLn(F, sor);
       mezolista.Clear;
       StringParse(sor, ';', mezolista);
       if ( ( mezolista.Count < 5 ) or ( Length(sor) < 30  ) ) then begin
           Continue;
       end;
       // Mez�k ellen�rz�se
       rendsz  := mezolista[1];
       rendsz  := Trim(StringReplace(rendsz, 'VIRT', '', [rfReplaceAll]));
       rendsz  := LeftStr(rendsz,3)+'-'+RightStr(rendsz, 3);
       if rendsz = '-' then begin
           EllenListAppend('A rendsz�m mez� �res ! '+mezolista[3]+';'+mezolista[2]);
           Continue;
       end;
       if not Vanrendszam(rendsz) then begin
           EllenListAppend('Hi�nyz� rendsz�m :'+mezolista[3]+';'+mezolista[2]+';'+rendsz);
           Continue;
       end;
       datum   := GetDatum(mezolista[2]);
       if not Jodatum(datum) then begin
           EllenListAppend('�rv�nytelen d�tum ! '+mezolista[3]+';'+mezolista[2]);
           Continue;
       end;
       ido                 := RightStr('0000'+mezolista[3], 5);
       orsz                := 'DE';
       valnem              := 'EUR';
       tipus               := '1';
       osszeg              := StringSzam( mezolista[20]);
       leiras              := 'Aut�p�lya';
       km                  := StringSzam( mezolista[19]);
       SG1.Cells[0,rsz]    := IntToStr(rsz);
       SG1.Cells[1,rsz]    := rendsz;
       SG1.Cells[2,rsz]    := datum;
       SG1.Cells[3,rsz]    := ido;
       SG1.Cells[4,rsz]    := orsz;
       SG1.Cells[5,rsz]    := Format('%.2f', [osszeg]);
       SG1.Cells[6,rsz]    := Format('%.2f', [km]);
       SG1.Cells[7,rsz]    := valnem;
       SG1.Cells[8,rsz]    := tipus;
       SG1.Cells[9,rsz]    := leiras;
       ertek               := 0;
       SG1.Cells[10,rsz]   := Format('%.2f', [ertek]);     // Mennyis�g
       SG1.Cells[11,rsz]   := Format('%.2f', [ertek]);     // �FA %
       rsz                 := rsz + 1;
   end;
   SG1.RowCount    := rsz;
   CloseFile(f);
   KoltsegGyujtoBeiro('AUTDE', 'Aut�p�lya d�j import DE', '0');
end;

procedure TEllenorbeDlg.OlvasasShell;
var
   i           : integer;
   termkod     : string;
	EX		    : TJExcel;
   fn          : string;
   Exdlg       : TExportDlg;
   idoert      : double;
   idostr      : string;
   idosec      : integer;
begin

   // Ha excel t�bl�t kapunk akkor el�sz�r �talak�tjuk txt-re
   if Pos('.xls', M0.Text) > 0 then begin
       // Megnyit�s, beolvas�s, ki�r�s
       EX 	:= TJexcel.Create(Self);
       if not EX.OpenXlsFile(dir + M0.Text) then begin
           NoticeKi('Nem siker�lt megnyitnom az Excel �llom�nyt!');
           EX.Free;  // nem kell m�r
           Exit;
       end;
       SG1.Visible         := false;
       if not EX.ReadFullXls(SG1) then begin
           NoticeKi('Nem siker�lt beolvasnom az Excel �llom�ny 2. lapj�t!');
           EX.Free;  // nem kell m�r
           Exit;
       end;
       // Az �llom�ny ki�r�sa
       Application.CreateForm(TExportDlg, Exdlg);
       fn  := copy(M0.Text, 1, Pos('.xls', M0.Text)-1)+'_T.CSV';
       Exdlg.gridexport    := true;
       Exdlg.grid          := SG1;
       // Az id�pontok �t�r�sa
       for i := 1 to SG1.RowCount - 1 do begin
  	        idoert  := StringSzam(SG1.Cells[6, i]);
           idostr  := '';
           if idoert > 0 then begin
               idosec          := Trunc( idoert * 24 *3600 ) ;
               idostr          := Format('%2.2d:',[ idosec div 3600 ]);
               idosec          := idosec - ( idosec div 3600 )*3600;
               idostr          := idostr + Format('%2.2d',[ idosec div 60]);
           end;
           SG1.Cells[6, i] := idostr;
       end;

       Exdlg.elvalaszto    := ';';
	    Exdlg.SilentFile(dir+fn);
	    Exdlg.Destroy;

       EX.Free;  // nem kell m�r
       M0.Text         := fn;
       SG1.Visible     := true;
       BitBtn2Click(Self);
       Exit;
   end;

   Elokeszito;
   // Az els� sor beolvas�sa -> ez lesz a mez�nevek list�ja
   ReadLn(F, sor);
   StringParse(sor, ';', mezonevek);
   MezonevKorrigalo;
   Memo1.Lines.Add('');
   Memo1.Lines.Add('SHELL adatok feldolgoz�sa elkezd�d�tt ( '+FormatDateTime('YYYY.MM.DD. HH.NN.SS', now)+' )   '+dir+M0.Text);
   Memo1.Lines.Add('');
   Memo1.Lines.Add('A beolvasott rekordok sz�ma : '+IntToStr(rec_szam));
   // Az �llom�ny olvas�sa, a t�bl�zat felt�lt�se
   while not Eof(F) do begin
       Application.ProcessMessages;
       ReadLn(F, sor);
       mezolista.Clear;
       StringParse(sor, ';', mezolista);
       for i := 0 to mezolista.Count - 1 do begin
           if copy(mezolista[i], 1, 1) = '"' then begin
               mezolista[i] := copy(mezolista[i], 2, Length(mezolista[i])-2);
           end;
       end;
       // Mez�k ellen�rz�se
       rendsz  := mezolista[16];
       rendsz  := Trim(StringReplace(rendsz, 'VIRT', '', [rfReplaceAll]));
       rendsz  := LeftStr(rendsz,3)+'-'+RightStr(rendsz, 3);
       if rendsz = '-' then begin
           EllenListAppend('A rendsz�m mez� �res ! '+mezolista[3]+';'+mezolista[2]);
           Continue;
       end;
       if not Vanrendszam(rendsz) then begin
           EllenListAppend('Hi�nyz� rendsz�m :'+mezolista[3]+';'+mezolista[2]+';'+rendsz);
           Continue;
       end;

       datum   := GetDatum(mezolista[5]);
       if not Jodatum(datum) then begin
           EllenListAppend('�rv�nytelen d�tum ! '+mezolista[3]+';'+mezolista[2]);
           Continue;
       end;
//       ido                 := copy(RightStr('00000000'+mezolista[6], 8), 1, 5);
       ido                 := copy(mezolista[6], 1, 5);
       orsz                := Trim(mezolista[1]);
       if orsz='EU' then begin
           if Trim(mezolista[3])<>'' then begin
               orsz:=Trim(mezolista[3]);
           end;
       end;
       valnem              := copy(Trim(mezolista[37]), 1, 3);
       termkod             := Trim(mezolista[19]);
       if Length(termkod)=1 then begin
           termkod         := '00'+termkod;
       end;
       if Length(termkod)=2 then begin
           termkod         := '0'+termkod;
       end;
       tipus               := copy(EgyebDlg.Read_SZGrid( '142', termkod ),1,1);
       leiras              := copy(EgyebDlg.Read_SZGrid( '142', termkod ),3,30);
       if  tipus = '' then begin
           tipus           := '1';
           leiras          := '?'+termkod+'?';
       end;
       osszeg              := StringSzam( mezolista[41]);
       km                  := StringSzam( mezolista[17]);
       menny               := StringSzam( mezolista[20]);
       SG1.Cells[0,rsz]    := IntToStr(rsz);
       SG1.Cells[1,rsz]    := rendsz;
       SG1.Cells[2,rsz]    := datum;
       SG1.Cells[3,rsz]    := ido;
       SG1.Cells[4,rsz]    := orsz;
       SG1.Cells[5,rsz]    := Format('%.2f', [osszeg]);
       SG1.Cells[6,rsz]    := Format('%.2f', [km]);
       SG1.Cells[7,rsz]    := valnem;
       SG1.Cells[8,rsz]    := tipus;
       SG1.Cells[9,rsz]    := leiras;
       SG1.Cells[10,rsz]   := Format('%.2f', [menny]);     // Mennyis�g
       if StringSzam( mezolista[25]) < 1 then begin
           SG1.Cells[11,rsz]   := Format('%.2f', [StringSzam( mezolista[25])*100]);     // �FA %
       end else begin
           SG1.Cells[11,rsz]   := Format('%.2f', [StringSzam( mezolista[25])]);     // �FA %
       end;
       rsz                 := rsz + 1;
   end;
   SG1.RowCount    := rsz;
   CloseFile(f);
   KoltsegGyujtoBeiro('SHELL', 'SHELL import', '3');
end;

procedure TEllenorbeDlg.FormDestroy(Sender: TObject);
begin
   mezolista.Free;
   mezonevek.Free
end;

function TEllenorbeDlg.GetMezoSorszam( meznev : string ) : integer;
var
   msz     : integer;
   felt    : boolean;
begin
   Result  := -1;
   msz     := 0;
   while msz < mezonevek.Count do begin
       felt    := (UpperCase(meznev) = UpperCase(mezonevek[msz]));
       if felt then begin
           Result  := msz;
           msz     := mezonevek.Count;
       end;
       Inc(msz);
   end;
end;

procedure TEllenorbeDlg.MezonevKorrigalo;
var
   msz     : integer;
begin
   for msz := 0 to mezonevek.Count -1 do begin
       if copy(mezonevek[msz], 1, 1) = '"' then begin
           mezonevek[msz] := copy(mezonevek[msz], 2, length(mezonevek[msz])-2)
       end;
       if copy(mezonevek[msz], 1, 1) = '''' then begin
           mezonevek[msz] := copy(mezonevek[msz], 2, length(mezonevek[msz])-2)
       end;
   end;
end;

procedure TEllenorbeDlg.KoltsegGyujtoBeiro( fimod, megje, ktipstr : string );
var
   rsz         : integer;
   sorsz2      : integer;
   regilog     : integer;
   beodat      : string;
begin
   Memo1.Lines.Add('');
   Memo1.Lines.Add('Az �llom�ny sorainak sz�ma  : '+IntToStr(SG1.RowCount-1));
   Query_Run(QueryAl, 'SELECT KS_KTKOD FROM KOLTSEG_GYUJTO WHERE KS_IMPFN = '''+M0.Text+''' ');
   Memo1.Lines.Add('Az adatb�zis rekordok sz�ma : '+IntToStr(QueryAl.RecordCount));
   if IntToStr(SG1.RowCount-1) = IntToStr(QueryAl.RecordCount) then begin
       Memo1.Lines.Add('OK');
   end else begin
       Memo1.Lines.Add('Hiba!');
   end;
   Exit;



   Query_Run(QueryAl, 'CREATE TABLE KOLTSEG_GYUJTO ( KS_AFAERT NUMERIC(14,4), KS_AFAKOD VARCHAR(3), KS_AFASZ NUMERIC(14,4), '+
       'KS_ARFOLY NUMERIC(14,4), KS_ATLAG NUMERIC(14,4), KS_DATUM VARCHAR(11), KS_ERTEK NUMERIC(14,4), KS_FELDAT DATETIME, '+
       'KS_FIMOD VARCHAR(5), KS_GKKAT VARCHAR(5), KS_HMEGJ VARCHAR(100), KS_IDO VARCHAR(5), KS_IMPFN VARCHAR(120), KS_KMORA NUMERIC(14,4), '+
       'KS_KTIP INTEGER, KS_LEIRAS VARCHAR(80), KS_MEGJ VARCHAR(80), KS_NC_DAT VARCHAR(11), KS_NC_IDO VARCHAR(11), KS_NC_KM NUMERIC(12,2), '+
       'KS_NC_UME NUMERIC(12,2), KS_NC_USZ NUMERIC(12,2), KS_ORSZA VARCHAR(3), KS_OSSZEG NUMERIC(14,4), KS_RENDSZ VARCHAR(10), '+
       'KS_SOKOD VARCHAR(5), KS_SONEV VARCHAR(45), KS_SOR VARCHAR(500), KS_SZKOD VARCHAR(11), KS_TELE INTEGER, KS_TELES VARCHAR(3), '+
       'KS_TETAN INTEGER, KS_THELY VARCHAR(80), KS_TIPUS INTEGER, KS_UZMENY NUMERIC(14,4), KS_VALNEM VARCHAR(3) )');


   Memo1.Lines.Add('');
   sorsz2  := Memo1.Lines.Add('A feldolgozott rekordok sz�ma : ');
   ret_kod	:= GetNextCode('KOLTSEG_GYUJTO_TEMP', 'KS_KTKOD', 1, 0);
   FormGaugeDlg    := TFormGaugeDlg.Create(Self);
   FormGaugeDlg.GaugeNyit('Adatok t�rol�sa ... ' , '', true);
   regilog := EgyebDlg.log_level;
   EgyebDlg.log_level  := 0;
   sorsz   := 0;
   beodat  := GetBeodat(fimod);
   SaveGridToFile(SG1, 'tabla01.csv');
   for rsz := 1 to SG1.RowCount - 1 do begin
       Application.ProcessMessages;
       if FormGaugeDlg.kilepes then begin
           if NoticeKi('Val�ban megszak�tja a feldolgoz�si m�veletet?', NOT_QUESTION) = 0 then begin
               // T�r�lj�k az eddig felvitt elemeket
               Query_Run(QueryAl, 'DELETE FROM KOLTSEG_GYUJTO_TEMP WHERE KS_IMPFN = '''+M0.Text+''' ');
               Break;
           end else begin
               FormGaugeDlg.kilepes := false;
           end;
       end;
       if ( ( rsz - 1 ) mod 100 ) = 0 then begin
           Memo1.Lines[sorsz2]  := 'A feldolgozott rekordok sz�ma : '+IntToStr(rsz)+'/'+IntToStr(SG1.RowCount - 1);
              FormGaugeDlg.Pozicio ( ( sorsz * 100 ) div (SG1.RowCount - 1)) ;
       end;
       Inc(sorsz);
       if Query_Insert( QueryAl, 'KOLTSEG_GYUJTO_TEMP', [
           'KS_KTKOD',   IntToStr( ret_kod),
           'KS_RENDSZ',  ''''+SG1.Cells[1,rsz]+'''',
           'KS_DATUM',   ''''+Copy(SG1.Cells[2,rsz], 1, 11)+'''',
           'KS_IDO',     ''''+Copy(SG1.Cells[3,rsz], 1, 5)+'''',
           'KS_ORSZA',   ''''+Copy(SG1.Cells[4,rsz], 1, 2)+'''',
           'KS_LEIRAS',  ''''+SG1.Cells[9,rsz]+'''',
           'KS_VALNEM',  ''''+Copy(SG1.Cells[7,rsz], 1, 3)+'''',
           'KS_FIMOD',   ''''+fimod+'''',
           'KS_KMORA',   SqlSzamString(StringSzam(SG1.Cells[6,rsz]),12,2),
           'KS_OSSZEG',  SqlSzamString(StringSzam(SG1.Cells[5,rsz]),14,4),
           'KS_UZMENY',  SqlSzamString(StringSzam(SG1.Cells[10,rsz]),12,2),
           'KS_ARFOLY',  SqlSzamString(0,14,4),
           'KS_ERTEK',   SqlSzamString(0,14,4),
           'KS_MEGJ',    ''''+megje+'''',
           'KS_TIPUS',   IntToStr(StrToIntDef( SG1.Cells[8,rsz] , 0)),
           'KS_JAKOD',   ''''+''+'''',
           'KS_JARAT',   ''''+''+'''',
           'KS_TELES',   ''''+'N'+'''',
           'KS_AFASZ',   SqlSzamString(StringSzam(SG1.Cells[11,rsz]),12,2),
           'KS_AFAERT',  SqlSzamString(StringSzam(SG1.Cells[5,rsz])*StringSzam(SG1.Cells[11,rsz])/100,12,2),
           'KS_AFAKOD',   ''''+''+'''',
           'KS_KTIP',    ktipstr,
           'KS_IMPFN',   ''''+M0.Text+'''',
           'KS_BEODAT',   ''''+beodat+'''',
           'KS_SOR',     ''''+IntToStr(ret_kod)+SG1.Cells[1,rsz]+SG1.Cells[2,rsz]+SG1.Cells[3,rsz]+'0'+SG1.Cells[5,rsz]+''''                                //
           ]) then begin
           inc(ret_kod);
       end else begin
           // A rossz sql ki�r�sa
           EllenListAppend('A rekord fel�r�s nem siker�lt : '+SG1.Cells[1,rsz]+','+SG1.Cells[2,rsz]+','+SG1.Cells[3,rsz]+','+Format('%.2f', [StringSzam(SG1.Cells[5,rsz])]));
       end;
   end;
   EgyebDlg.log_level  := regilog;
   if not FormGaugeDlg.kilepes then begin
       Memo1.Lines[sorsz2]  := 'A feldolgozott rekordok sz�ma : '+IntToStr(rsz-1)+'/'+IntToStr(SG1.RowCount - 1);
   end else begin
       Memo1.Lines[sorsz2]  := 'A m�veletet a felhaszn�l� megszak�totta!';
       BitBtn2.Show;
       BitBtn1.Enabled     := true;
       BitKilep.Enabled    := true;
       CB1.Enabled         := true;
   end;
   FormGaugeDlg.Free;
   Memo1.Lines.Add('');
   Memo1.Lines.Add('Az adatok feldolgoz�sa befejez�d�tt ( '+FormatDateTime('YYYY.MM.DD. HH.NN.SS', now)+' )');
end;

procedure TEllenorbeDlg.Elokeszito;
begin
    // A m�veletek el�k�sz�t�se
   fname   := dir + M0.Text;
   rsz     := rec_szam;
	AssignFile(F, fname);
	Reset(F);
	mezonevek.Clear;
   // A t�bl�zat kezdet�nek be�ll�t�sa
   SG1.ColCount        := 12;
   SG1.RowCount        := 2;
   SG1.Rows[1].Clear;
   SG1.Cells[0,0]      := 'SSZ';
   SG1.Cells[1,0]      := 'RENDSZAM';
   SG1.Cells[2,0]      := 'DATUM';
   SG1.Cells[3,0]      := 'IDO';
   SG1.Cells[4,0]      := 'ORSZAG';
   SG1.Cells[5,0]      := 'OSSZEG';
   SG1.Cells[6,0]      := 'KM';
   SG1.Cells[7,0]      := 'VNEM';
   SG1.Cells[8,0]      := 'T�pus';
   SG1.Cells[9,0]      := 'Le�r�s';
   SG1.Cells[10,0]     := 'Menny';
   SG1.ColWidths[1]    := 100;
   SG1.ColWidths[2]    := 100;
   SG1.ColWidths[3]    := 100;
   SG1.ColWidths[4]    := 100;
   SG1.ColWidths[5]    := 100;
   SG1.ColWidths[6]    := 80;
   SG1.ColWidths[7]    := 50;
   SG1.ColWidths[8]    := 100;
   SG1.ColWidths[9]    := 200;
   SG1.ColWidths[10]   := 100;
   SG1.RowCount        := 2 + rec_szam;
   rsz                 := 1;
   EgyebDlg.Ellenlista.Clear;
end;

function TEllenorbeDlg.GetDatum(dstr : string) : string;
var
   dat : string;
begin
   Result  := '';
   dat     := dstr;
   if Length(dat) >= 10 then begin
       // Teljes d�tum
       if copy(dat, 3, 1) = '.' then begin
           // NN.HH.���� alak�
           dat := copy(dat, 7, 4)+'.'+copy(dat, 4, 2)+':'+copy(dat, 1, 2);
       end;
   end;
   while Pos(' ', dat) > 0 do begin
       dat := copy(dat, 1, Pos(' ', dat)-1)+copy(dat, Pos(' ', dat)+1, 9999);
   end;
   Result  := dat;
end;

function TEllenorbeDlg.GetBeodat( fimod : string ) : string;
var
   beodat  : string;
   folytat : boolean;
begin
   // A beolvas�s d�tum�nak meghat�roz�sa -> ez egyedi lesz a megadott k�lts�gimport �llom�nyra
   beodat  := EgyebDlg.MaiDatum;
   folytat := true;
   while folytat do begin
       Query_Run( QueryAl, 'SELECT * FROM KOLTSEG_GYUJTO WHERE KS_BEODAT = '''+beodat+''' AND KS_FIMOD = '''+fimod+''' ');
       if QueryAl.RecordCount = 0 then begin
           // J� lesz a megadott d�tum
           folytat := false;
       end else begin
           // Tov�bbl�p�nk a napokkal
           beodat  := Datumhoznap(beodat, 1, true);
       end;
   end;
   Result  := beodat;
end;

function TEllenorbeDlg.VanRendszam( rsz : string) : boolean;
begin
    // A rendsz�m ellen�rz�se
    Result := QRSz.Locate('GK_RESZ', rsz, []);
end;

end.
