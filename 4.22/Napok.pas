unit Napok;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, Grids, DBGrids, ComCtrls, ADODB, ExtCtrls, StdCtrls, Buttons,
  DBCtrls, Mask;

type
  TFNapok = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    TabControl1: TTabControl;
    Panel3: TPanel;
    DBGrid1: TDBGrid;
    DSNapok: TDataSource;
    BitKilep: TBitBtn;
    DBNavigator1: TDBNavigator;
    DBCheckBox1: TDBCheckBox;
    Label1: TLabel;
    Label2: TLabel;
    DBEdit1: TDBEdit;
    TNapok: TADODataSet;
    TNapokNA_ORSZA: TStringField;
    TNapokNA_DATUM: TDateTimeField;
    TNapokNA_UNNEP: TIntegerField;
    TNapokNA_MEGJE: TStringField;
    Label3: TLabel;
    CBOrsz: TDBComboBox;
    TOrsz: TADOQuery;
    TOrszna_orsza: TStringField;
    TNapoknapnev: TStringField;
    DateTimePicker1: TDateTimePicker;
    procedure TabControl1Change(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure TNapokAfterInsert(DataSet: TDataSet);
    procedure BitKilepClick(Sender: TObject);
    procedure ADODataSet1AfterInsert(DataSet: TDataSet);
    procedure FormCreate(Sender: TObject);
    procedure TNapokAfterPost(DataSet: TDataSet);
    procedure TNapokCalcFields(DataSet: TDataSet);
    procedure TNapokAfterScroll(DataSet: TDataSet);
    procedure TNapokBeforePost(DataSet: TDataSet);
  private
    { Private declarations }
		listaorsz			: TStringList;
  public
    { Public declarations }
  end;

var
  FNapok: TFNapok;

implementation

uses Egyeb,J_SQL;

{$R *.dfm}

procedure TFNapok.TabControl1Change(Sender: TObject);
begin
  TNapok.Close;
  TNapok.Parameters[0].Value:=TabControl1.Tabs[TabControl1.TabIndex];
  TNapok.Open;
end;

procedure TFNapok.FormActivate(Sender: TObject);
begin
  TabControl1.OnChange(self);
end;

procedure TFNapok.TNapokAfterInsert(DataSet: TDataSet);
begin
  TNapokNA_ORSZA.Value:=TabControl1.Tabs[TabControl1.TabIndex];
  TNapokNA_UNNEP.Value:=1;
end;

procedure TFNapok.BitKilepClick(Sender: TObject);
begin
  if TNapok.Modified then TNapok.Post;
  close;
end;

procedure TFNapok.ADODataSet1AfterInsert(DataSet: TDataSet);
begin
  TNapokNA_ORSZA.Value:=TabControl1.Tabs[TabControl1.TabIndex];
  TNapokNA_UNNEP.Value:=1;
  //JvDBDateEdit1.SetFocus;
  TNapokNA_DATUM.Value:=date;
  DateTimePicker1.Date:=Date;
  DateTimePicker1.SetFocus;
    keybd_event(VK_F2,0,0,0);
    keybd_event(VK_F2,0,KEYEVENTF_KEYUP,0);
    keybd_event(VK_MENU,0,0,0);
    keybd_event(VK_DOWN,0,0,0);
    keybd_event(VK_DOWN,0,KEYEVENTF_KEYUP,0);
    keybd_event(VK_MENU,0,KEYEVENTF_KEYUP,0);

end;

procedure TFNapok.FormCreate(Sender: TObject);
begin
	listaorsz	:= TStringList.Create;
	SzotarTolt(CBOrsz, '300' , listaorsz, true, true );

     TOrsz.Close;
     TOrsz.Open;
     TOrsz.First;
     TabControl1.Tabs.Clear;
     if TOrsz.Eof then TabControl1.Tabs.Add('HU');
     while not TOrsz.Eof do
     begin
      TabControl1.Tabs.Add(TOrszna_orsza.Value);
      TOrsz.Next;
     end;
     TOrsz.Close;

    TabControl1.TabIndex:=TabControl1.Tabs.IndexOf('HU');
    TabControl1.OnChange(self);

end;

procedure TFNapok.TNapokAfterPost(DataSet: TDataSet);
var
  i: integer;
begin
  if TNapokNA_ORSZA.Value<>TabControl1.Tabs[TabControl1.TabIndex] then
  begin
     TOrsz.Close;
     TOrsz.Open;
     TOrsz.First;
     TabControl1.Tabs.Clear;
     while not TOrsz.Eof do
     begin
      TabControl1.Tabs.Add(TOrszna_orsza.Value);
      TOrsz.Next;
     end;
     TOrsz.Close;

    TabControl1.TabIndex:=TabControl1.Tabs.IndexOf(TNapokNA_ORSZA.Value);
    TabControl1.OnChange(self);
  end;
end;

procedure TFNapok.TNapokCalcFields(DataSet: TDataSet);
var
  i:integer;
  days: array[1..7] of string;
begin
  days[1] := 'Vas�rnap';
  days[2] := 'H�tf�';
  days[3] := 'Kedd';
  days[4] := 'Szerda';
  days[5] := 'Cs�t�rt�k';
  days[6] := 'P�ntek';
  days[7] := 'Szombat';

  i:=DayOfWeek(TNapokNA_DATUM.Value);
  TNapoknapnev.Value:=days[i];

end;

procedure TFNapok.TNapokAfterScroll(DataSet: TDataSet);
begin
  DateTimePicker1.Date:=TNapokNA_DATUM.Value;
end;

procedure TFNapok.TNapokBeforePost(DataSet: TDataSet);
begin
  TNapokNA_DATUM.Value:=StrToDate( datetostr( DateTimePicker1.Date));
end;

end.
