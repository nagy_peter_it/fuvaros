unit Sulybe;

interface

uses
	SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
	Forms, Dialogs, StdCtrls, Buttons, Mask, DB, DBTables, J_ALFORM, ADODB ;

type
	TSulybeDlg = class(TJ_AlformDlg)
	BitElkuld: TBitBtn;
	BitKilep: TBitBtn;
	Label8: TLabel;
    M2: TMaskEdit;
    Label3: TLabel;
    M3: TMaskEdit;
    M0: TMaskEdit;
    Label6: TLabel;
    M10: TMaskEdit;
    Label2: TLabel;
    M11: TMaskEdit;
    Label4: TLabel;
    MaskEdit1: TMaskEdit;
    BitBtn2: TBitBtn;
    Label5: TLabel;
    M00: TMaskEdit;
    Label7: TLabel;
    Query1: TADOQuery;
    Query2: TADOQuery;
	procedure BitKilepClick(Sender: TObject);
	procedure FormCreate(Sender: TObject);
	procedure BitElkuldClick(Sender: TObject);
  procedure MaskEditClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure M1Exit(Sender: TObject);
    procedure MaskKeyPress(Sender: TObject; var Key: Char);
    procedure Tolto(cim, kod : string); override;
    procedure M10Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
	 procedure KmHozzaadas(rendsz : string; Mkm1, Mkm2 : TMaskEdit );
    procedure Tolt2(cim , teko, jakod, rendsz, jarat:string );
	private
  		jaratkod	: string;
	public
		ret_kod 	: string;
	end;

var
	SulybeDlg: TSulybeDlg;

implementation

uses
	Egyeb, J_SQL, J_VALASZTO, Kozos ,Jaratbe ;
{$R *.DFM}

procedure TSulybeDlg.BitKilepClick(Sender: TObject);
begin
	ret_kod := '';
	Close;
end;

procedure TSulybeDlg.FormCreate(Sender: TObject);
begin
	EgyebDlg.SetADOQueryDatabase(Query1);
	EgyebDlg.SetADOQueryDatabase(Query2);
	ret_kod  := '';
end;

procedure TSulybeDlg.Tolto(cim, kod : string);
begin
	SetMaskEdits([MaskEdit1,M0]);
	Caption 	:= cim;
  	ret_kod  	:= kod;
  	M0.Text		:= ret_kod;
  	if ret_kod <> '' then begin
  		if Query_Run (Query1, 'SELECT * FROM SULYOK WHERE SU_SUKOD = '''+ret_kod+''' ' ,true) then begin
     		jaratkod		:= Query1.FieldByname('SU_JAKOD').AsString;
			MaskEdit1.Text 	:= '';
     		if Query_Run (Query2, 'SELECT * FROM JARAT WHERE JA_KOD = '''+jaratkod+''' ',true) then begin
				MaskEdit1.Text := Query2.FieldByName('JA_ORSZ').AsString+'-'+
           						Query2.FieldByName('JA_ALKOD').AsString;
        	end;
     		M10.Text		:= Query1.FieldByname('SU_KMORA').AsString;
     		M11.Text		:= Query1.FieldByname('SU_KMOR2').AsString;
     		M2.Text			:= Query1.FieldByname('SU_ERTEK').AsString;
     		M3.Text			:= Query1.FieldByname('SU_MEGJE').AsString;
        	Query1.Close;
     	end;
  	end;
end;

procedure TSulybeDlg.BitElkuldClick(Sender: TObject);
var
  km1, km2, km3	: longint;
  nyitokm,zarokm, rendszam: string;
begin
	if StringSzam(M2.Text) < 50 then begin
		if NoticeKi('T�l kicsi s�ly! Val�ban ekkora s�lyt k�v�n megadni? ', NOT_QUESTION) <> 0 then begin
			M2.Setfocus;
     		Exit;
       end;
  	end;

	if MaskEdit1.Text = '' then begin
		NoticeKi('J�rat megad�sa k�telez�!');
		BitBtn2.Setfocus;
     	Exit;
  	end;

  	if StringSzam(M11.Text) <= StringSzam(M10.Text) then begin
		NoticeKi('Rossz km. �ra megad�sa!');
		M11.Setfocus;
     	Exit;
  	end;

  	{J�rat ellen�rz�tts�g�nek ellen�rz�se}
	if EllenorzottJarat(jaratkod) > 0 then begin
  		Exit;
  	end;

  	{J�rat km -ek ellen�rz�se}
  	if Query_Run (Query1, 'SELECT * FROM JARAT WHERE JA_KOD = '''+jaratkod+''' ',true) then begin
      if JaratbeDlg = nil  then      // Nem a j�ratb�l viszik fel.
      begin
    		KmHozzaadas(Query1.FieldByName('JA_RENDSZ').AsString, M10, M11 );
    		km1	:= Round(StringSzam(M10.Text));
       	km2	:= StrToIntDef(Query1.FieldByname('JA_NYITKM').AsString,0);
     	  km3	:= StrToIntDef(Query1.FieldByname('JA_ZAROKM').AsString,0);
        nyitokm:=IntToStr(km2);
        zarokm :=IntToStr(km3);
      end
      else              // j�ratb�l viszik fel
      begin
        rendszam:= JaratbeDlg.m4.Text; // itt n�ha AV-t dob
    		KmHozzaadas(rendszam, M10, M11 );
    		km1	:= Round(StringSzam(M10.Text));
       	km2	:= StrToIntDef(JaratbeDlg.M14.Text,0);
     	  km3	:= StrToIntDef(JaratbeDlg.M15.Text,0);
        nyitokm:=IntToStr(km2);
        zarokm :=IntToStr(km3);
      end;
  		if (  ( km1 < km2 ) or ( km1 > km3 ) ) then begin
  			NoticeKi('J�raton k�v�li km. �ra megad�sa! ('+
        //Query1.FieldByname('JA_NYITKM').AsString + ' - ' + Query1.FieldByname('JA_ZAROKM').AsString+')');
        nyitokm + ' - ' + zarokm+')');
	  		M10.Setfocus;
     		Exit;
     	end;
  		km1	:= Round(StringSzam(M11.Text));
  		if (  ( km1 < km2 ) or  ( km1 > km3 ) ) then begin
			  NoticeKi('J�raton k�v�li km. �ra megad�sa! ('+
        nyitokm + ' - ' + zarokm+')');
       // Query1.FieldByname('JA_NYITKM').AsString + ' - ' + Query1.FieldByname('JA_ZAROKM').AsString+')');
			  M11.Setfocus;
     		Exit;
     	end;
  	end;
	  if ret_kod = '' then begin
  		{�j s�ly l�trehoz�sa}
     	ret_kod := Format('%5.5d',[GetNextCode('SULYOK', 'SU_SUKOD', 0, 5)]);
     	Query_Run ( Query1, 'INSERT INTO SULYOK (SU_SUKOD) VALUES (''' +ret_kod+ ''' )',true);
  	end;
  	{Update rekord}
  	Query_Update( Query1, 'SULYOK',
  		[
     	'SU_KMORA',		SqlSzamString(StringSzam(M10.Text),10,0),
     	'SU_KMOR2',  	SqlSzamString(StringSzam(M11.Text),10,0),
     	'SU_ERTEK',  	SqlSzamString(StringSzam(M2.Text),10,0),
     	'SU_JAKOD', 	''''+jaratkod+'''',
     	'SU_MEGJE', 	''''+M3.Text+''''
     	], ' WHERE SU_SUKOD = '''+ret_kod+''' ');
  	Close;
end;

procedure TSulybeDlg.MaskEditClick(Sender: TObject);
begin
	with Sender as TMaskEdit do begin
  		SelectAll;
  	end;
end;

procedure TSulybeDlg.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
	EgyebDlg.FormReturn(Key);
end;

procedure TSulybeDlg.M1Exit(Sender: TObject);
begin
	SzamExit(Sender);
{  if (Sender as TMaskEdit).Name = 'M10' then begin       // Nyit�
    KmHozzaadas(MaskEdit1,M10);
  if (Sender as TMaskEdit).Name = 'M11' then begin       // Z�r�
    KmHozzaadas(MaskEdit1,M11);
    }
end;

procedure TSulybeDlg.MaskKeyPress(Sender: TObject; var Key: Char);
begin
  	with Sender as TMaskEdit do begin
  		Key := EgyebDlg.Jochar(Text,Key,10,Tag);
  	end;
end;

procedure TSulybeDlg.M10Click(Sender: TObject);
begin
	with Sender as TMaskEdit do begin
  	SelectAll;
  end;
end;

procedure TSulybeDlg.BitBtn2Click(Sender: TObject);
begin
	JaratValaszto(M00, MaskEdit1);
	jaratkod		:= M00.Text;
end;

procedure TSulybeDlg.Tolt2(cim , teko, jakod, rendsz, jarat:string );
begin
  	{Kit�ltj�k a j�ratot + rendsz�mot �s nem lehet megv�ltoztatni}
	MaskEdit1.Text 	:= jarat;
  	jaratkod        := jakod;
   Tolto(cim, teko);
  	BitBtn2.Enabled	:= false;
end;

procedure TSulybeDlg.KmHozzaadas(rendsz : string; Mkm1, Mkm2 : TMaskEdit );
var
	hozza	: double;
begin
   // A hozz�adott km. ellen�rz�se
   if rendsz <> '' then begin
   	hozza	:= StringSzam(Query_Select('GEPKOCSI', 'GK_RESZ', rendsz, 'GK_HOZZA'));
       if hozza > 0 then begin
       	if StringSzam(Mkm1.Text) <> 0 then begin
               if StringSzam(Mkm1.Text) < hozza then begin
               	Mkm1.Text	:= Format('%.0f', [hozza + StringSZam(Mkm1.Text)]);
               end;
           end;
       	if StringSzam(Mkm2.Text) <> 0 then begin
               if StringSzam(Mkm2.Text) < hozza then begin
               	Mkm2.Text	:= Format('%.0f', [hozza + StringSZam(Mkm2.Text)]);
               end;
           end;
       end;
   end;
end;

end.
