unit koltlis;

interface

uses
  WinTypes, WinProcs, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, QuickRpt, QRExpr, Qrctrls, ExtCtrls, printers, DB, DBTables, Egyeb, Forgalom, J_SQL,
  Grids, Kozos, ADODB ;

type
  TKoltlisDlg = class(TForm)
    Rep: TQuickRep;
    QRBand3: TQRBand;
    QRLSzamla: TQRLabel;
    QRBand1: TQRBand;
    QRLabel15: TQRLabel;
    QRLabel16: TQRLabel;
    QRLabel17: TQRLabel;
    QRLabel6: TQRLabel;
    QRSysData1: TQRSysData;
    QRLabel7: TQRLabel;
    QRSysData4: TQRSysData;
    QRLabel1: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel3: TQRLabel;
    QRShape1: TQRShape;
    QRShape3: TQRShape;
    QRShape4: TQRShape;
    QRShape5: TQRShape;
    QRShape6: TQRShape;
    QRShape7: TQRShape;
    QRShape8: TQRShape;
    QRLabel4: TQRLabel;
    QRLabel9: TQRLabel;
    QRLabel12: TQRLabel;
    QRLabel13: TQRLabel;
    QRLabel14: TQRLabel;
    QRLabel19: TQRLabel;
    QRLabel22: TQRLabel;
    QRLabel24: TQRLabel;
    QRDBText4: TQRDBText;
    QRDBText5: TQRDBText;
    QRDBText6: TQRDBText;
    QRLabel2: TQRLabel;
    QRLabel5: TQRLabel;
    QRShape2: TQRShape;
    QRShape9: TQRShape;
    QRShape10: TQRShape;
    QRShape11: TQRShape;
    QRShape12: TQRShape;
    QRShape13: TQRShape;
    QRShape14: TQRShape;
    QRSysData2: TQRSysData;
    QL2: TQRLabel;
    QRBand2: TQRBand;
    QRLabel21: TQRLabel;
    QRShape15: TQRShape;
    QL3: TQRLabel;
    QRDBText2: TQRDBText;
    QRDBText3: TQRDBText;
    QRDBText7: TQRDBText;
    QRLabel10: TQRLabel;
    QRLabel18: TQRLabel;
    QRLabel23: TQRLabel;
    QRDBText1: TQRDBText;
    QRDBText8: TQRDBText;
    QRDBText9: TQRDBText;
    QRLabel11: TQRLabel;
    Query1: TADOQuery;
    Query2: TADOQuery;
    procedure FormCreate(Sender: TObject);
    procedure	Tolt(rsz, datumtol, datumig, datol, datig, jarat : string );
    procedure QRBand1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure RepBeforePrint(Sender: TCustomQuickRep;
      var PrintReport: Boolean);
    procedure QRBand2BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
  private
  public
     vanadat			: boolean;
     ossz				: double;
     menlev			: string;
  end;

var
  KoltlisDlg: TKoltlisDlg;

implementation

{$R *.DFM}

procedure TKoltlisDlg.FormCreate(Sender: TObject);
begin
	EgyebDlg.SetADOQueryDatabase(Query1);
	EgyebDlg.SetADOQueryDatabase(Query2);
end;

procedure	TKoltlisDlg.Tolt(rsz, datumtol, datumig, datol, datig, jarat : string );
var
  sqlstr	: string;
begin
	{A fejl�c kit�lt�se}
	QRLabel5.Caption	:= jarat;
  	EllenListTorol;
  	if jarat <> '' then begin
     	if Query_Run(Query2, 'SELECT JA_KOD FROM JARAT WHERE JA_KOD = '''+jarat+''' ' ,true) then begin
     		if Query2.RecordCount > 0 then begin
    			JaratEllenor( Query2.FieldByname('JA_KOD').AsString );
  				QRLabel5.Caption	:= GetJaratszam(Query2.FieldByname('JA_KOD').AsString);
        	end;
		end;
  	end;
	QRLabel16.Caption	:= rsz;
	QRLabel17.Caption	:= datol + ' - ' + datig;
	EllenListMutat('Nem ellen�rz�tt j�ratok', true);

	{A kapott sql alapj�n v�grehajtja a lek�rdez�st}
	sqlstr	:= 'select * from koltseg where KS_DATUM >= '''+datumtol+''' AND KS_DATUM <= '''+datumig+''' ';
  	if rsz <> '' then begin
  		sqlstr	:= sqlstr + ' AND KS_RENDSZ = '''+rsz+''' ';
  	end;
  	if jarat <> '' then begin
  		sqlstr	:= sqlstr + ' AND KS_JAKOD = '''+jarat+''' ';
  	end;
  	Query_Run(Query1, sqlstr);
  	vanadat	:= Query1.RecordCount > 0;
end;

procedure TKoltlisDlg.QRBand1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
var
	ert : double;
begin
	ert := Query1.FieldByname('KS_ARFOLY').AsFloat * Query1.FieldByname('KS_OSSZEG').AsFloat;
  if ert < 0 then begin
  	ert := 0;
  end;
	QL2.Caption := SzamString(ert, 12, 1);
  ossz	:= ossz + ert;
end;

procedure TKoltlisDlg.RepBeforePrint(Sender: TCustomQuickRep;
      var PrintReport: Boolean);
begin
	ossz		:= 0;
  menlev	:= '';
end;

procedure TKoltlisDlg.QRBand2BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
begin
	QL3.Caption := SzamString(ossz, 12, 1);
end;

end.
