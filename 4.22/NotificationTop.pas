unit NotificationTop;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls, Data.DBXJSON, ChatControl,
  {Gradient,} acPNG, Vcl.Buttons, System.Win.ScktComp, cefvcl, ceflib, RESTAPI_Kozos, Kozos;

const
 WM_REENABLE = WM_USER + $100;

type
  TNotificationTopDlg = class(TForm)
    pnlMegnyitom: TPanel;
    pnlKesobb: TPanel;
    Image1: TImage;
    procedure FormShow(Sender: TObject);
    procedure pnlKesobbClick(Sender: TObject);
    procedure pnlMegnyitomClick(Sender: TObject);
    procedure FormHide(Sender: TObject);
  private
    procedure WMEnable(var Message: TWMEnable); message WM_ENABLE;
    procedure WMReenable(var Message: TMessage); message WM_REENABLE;
    function TaskBarHeight: integer;
  public
    XUID: string;
    // WinTop: integer;
  end;
 var
  NotificationTopDlg: TNotificationTopDlg;

implementation

uses UzenetTop;

{$R *.dfm}

procedure TNotificationTopDlg.pnlKesobbClick(Sender: TObject);
begin
    Hide;
end;

procedure TNotificationTopDlg.pnlMegnyitomClick(Sender: TObject);
begin
  if XUID <> '' then begin
    UzenetTopDlg.ChangeToXUID(XUID);
    end;
  UzenetTopDlg.SetActivePage(1);
  // UzenetTopDlg.WindowState:= wsNormal;
  UzenetTopDlg.Show;
  Hide; // hide notification
end;

procedure TNotificationTopDlg.WMEnable(var Message: TWMEnable);
begin
 if not Message.Enabled then
 begin
   PostMessage(Self.Handle, WM_REENABLE, 1, 0);
 end;
end;

procedure TNotificationTopDlg.WMReenable(var Message: TMessage);
begin
 EnableWindow(Self.Handle, True);
end;

procedure TNotificationTopDlg.FormHide(Sender: TObject);
begin
  SetWindowPos(Handle, HWND_NOTOPMOST, 0, 0, 0, 0, SWP_NoMove or SWP_NoSize);
end;

procedure TNotificationTopDlg.FormShow(Sender: TObject);
begin
  Left := (Screen.Width - Width);
  // Top := (Screen.Height - Height) - TaskBarHeight;
  // Top := WinTop;
  SetWindowPos(Handle, HWND_TOPMOST, 0, 0, 0, 0, SWP_NoMove or SWP_NoSize);
end;

function TNotificationTopDlg.TaskBarHeight: integer;
  var
    hTB: HWND; // taskbar handle
    TBRect: TRect; // taskbar rectangle
  begin
    hTB:= FindWindow('Shell_TrayWnd', '');
    if hTB = 0 then
      Result := 0
    else begin
      GetWindowRect(hTB, TBRect);
      Result := TBRect.Bottom - TBRect.Top;
    end;
  end;

end.
