unit J_FOFORMUJ;

interface
uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DBCtrls, StdCtrls, ExtCtrls, Grids, DBGrids, DBTables,
  Buttons, J_ALForm, J_EXPORT, Szures, ADODB, DB, Menus, Contnrs, CheckLst,
  Mask, clipbrd,IniFiles;

type
  THackGrid = class(TDBGrid);  // az automatikus v�g�lapra m�sol�shoz

  TJ_FOFORMUJDLG = class(TForm)
    PanelControl: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    ComboBox1: TComboBox;
    Edit1: TEdit;
    DBNavigator1: TDBNavigator;
    DataSource1: TDataSource;
    PanelGrid: TPanel;
    DBGrid2: TDBGrid;
    ButtonSzures: TBitBtn;
    PanelBottom: TPanel;
    ButtonKilep: TBitBtn;
	 ButtonPrint: TBitBtn;
    ButtonUj: TBitBtn;
	 ButtonMod: TBitBtn;
    ButtonTor: TBitBtn;
	 ButtonKuld: TBitBtn;
    Label3: TLabel;
	 BtnExport: TBitBtn;
    Query1: TADOQuery;
	 Query2: TADOQuery;
	 SB1: TSpeedButton;
	 ColorDialog1: TColorDialog;
    SGFields: TStringGrid;
    SGCalcFields: TStringGrid;
    SGSzures: TStringGrid;
	 SGSzuroMezok: TStringGrid;
    PanelSzuro: TPanel;
	 BSzuresTorles: TBitBtn;
    ButtonNez: TBitBtn;
    ShapeTorles: TShape;
    Timer_Keres: TTimer;
    ComboBox2: TComboBox;
    ComboBox3: TComboBox;
    Panel1: TPanel;
    Edit2: TEdit;
    BitBtn101: TBitBtn;
    BitBtn102: TBitBtn;
    ButtonSzuresek: TBitBtn;
    ppmSzuresek: TPopupMenu;
    Kakukk1: TMenuItem;
    Hapci1: TMenuItem;
    ebInkrementalis: TEdit;
    procedure FormCreate(Sender: TObject);virtual;
    procedure FormDestroy(Sender: TObject);virtual;
    procedure ButtonKilepClick(Sender: TObject);
    procedure ButtonPrintClick(Sender: TObject);virtual;
    procedure ButtonUjClick(Sender: TObject);virtual;
    procedure ButtonModClick(Sender: TObject);virtual;
	 procedure ButtonTorClick(Sender: TObject);virtual;
	 procedure ButtonKuldClick(Sender: TObject);virtual;
    procedure ButtonSzuresClick(Sender: TObject);
    procedure Adatlap(cim, kod : string);virtual;
	 procedure FormResize(Sender: TObject);virtual;
	 procedure FormShow(Sender: TObject);
	 procedure ComboBox1Change(Sender: TObject);virtual;
	 procedure FormKeyDown(Sender: TObject; var Key: Word;
	   Shift: TShiftState);virtual;
   // procedure FormBezaras(); virtual; // az ablakok k�z�tti "ugr�shoz"
	 procedure FormKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
	 procedure SajatFormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
	 procedure DBGrid2TitleClick(Column: TColumn);
	 procedure Query1CalcFields(DataSet: TDataSet);
	 procedure Mezoiro;virtual;abstract;
	 procedure UjSqlEllenor;virtual;
	 procedure FelTolto(s1, s2, s3, s4, s5,s6 : string; v_tag : integer);
	 procedure FormCanResize(Sender: TObject; var NewWidth,
	   NewHeight: Integer; var Resize: Boolean);
	 procedure PlaceButtons;
	 procedure DataSource1DataChange(Sender: TObject; Field: TField);virtual;
	 function  KeresChange(mezo, szoveg : string) : string; virtual;
	 procedure BtnExportClick(Sender: TObject);
	 function  GetOrderBy( ascflag : boolean) : string;
	 procedure SB1Click(Sender: TObject);
	 procedure DBGrid2DrawColumnCell(Sender: TObject; const Rect: TRect;
	   DataCol: Integer; Column: TColumn; State: TGridDrawState);virtual;
	 procedure DBGrid2MouseUp(Sender: TObject; Button: TMouseButton;
	   Shift: TShiftState; X, Y: Integer);
	 procedure Jeloles(kod : string; tipus: Integer);
	 procedure Set_Index;
	 function  Get_Index : integer;
	 function  ReadFields : boolean;
	 procedure ReadCalcFields ; virtual;
	 function  GetFieldStr(ftag : integer; qu : TAdoQuery) : string;
	 procedure SetQueryFields;
	 procedure ComboTolto(start : integer );
	 function  SQL_ToltoFo(orderstr : string) : boolean;
 	 function  SQL_ToltoFoMag(orderstr : string; SzuresNelkul: boolean = false) : boolean;
	 procedure AddCalcField(nev, lab, visib, tipus, width, mztag, msize, align : string) ;
	 procedure DataSource1StateChange(Sender: TObject);
	 procedure 	Szures(Forras : TADOQuery; alapstr, orderstr : string; dlgtag : integer);
	 function  SzuresValaszto(mezonev : string; lx,ly : integer) : boolean;
   function SzummaMutato(mezonev : string; lx,ly : integer; MyField: TField) : boolean;
	 procedure DBGrid2MouseDown(Sender: TObject; Button: TMouseButton;
	   Shift: TShiftState; X, Y: Integer);
	 procedure SzuresTorles(mezonev : string);
	 procedure SzuresToltes(mezonev : string; lista : TStringList);
	 procedure SzuresHozzaadas(mezonev, szures_str : string );
	 procedure AddSzuromezo(mezonev, sqlstr : string);
	 //procedure AddSzuromezoRovid(mezonev, tabla : string);
   procedure AddSzuromezoRovid(mezonev, tabla : string; uresval: string='0');
   procedure AddSzummamezo(mezonev: string);
	 function  GetSzuresPanelStr : string;
	 function  GetSzuresStr : string;
	 procedure FormClose(Sender: TObject; var Action: TCloseAction);
	 procedure SzuresUrites;
	 procedure SzuresKiiras;
	 procedure SzuresBeolvasas;
	 procedure SzuresBeolvasas2;
	 procedure BSzuresTorlesClick(Sender: TObject);
	 procedure ButtonNezClick(Sender: TObject);
	procedure   Keres (Query : TADOQuery; fname, source : string );
    procedure FormActivate(Sender: TObject);
    procedure DBGrid2DblClick(Sender: TObject);
    procedure DBGrid2CellClick(Column: TColumn);
    procedure Timer_KeresTimer(Sender: TObject);
    procedure ComboBox2Change(Sender: TObject);
    procedure BitBtn101Click(Sender: TObject);
    procedure BitBtn102Click(Sender: TObject);
    procedure Edit2Enter(Sender: TObject);
    procedure Edit2Exit(Sender: TObject);
    procedure ComboBox3Change(Sender: TObject);
    procedure Query1AfterOpen(DataSet: TDataSet);
    procedure Query1AfterScroll(DataSet: TDataSet); virtual;
    procedure ButtonSzuresekMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure ButtonSzuresekClick(Sender: TObject);
    procedure DropMenuDown(Control: TControl; PopupMenu: TPopupMenu);
    procedure SzuresekElemClick(Sender: TObject);
    procedure ebInkrementalisChange(Sender: TObject);
    procedure Query1FilterRecord(DataSet: TDataSet; var Accept: Boolean);
    procedure ebInkrementalisClick(Sender: TObject);
    procedure DarabszamMutat(const KellAktRekord: boolean);
    procedure Query1BeforeOpen(DataSet: TDataSet);
    procedure Label3Click(Sender: TObject);
  published
		AlForm			: TJ_AlFormDlg;
		mezonevek  		: TStringList;
    procedure InitSzuresek;
    procedure SzuresElemlista_frissit;
	private
    // sorokszama: integer;  // ha az inkrement�lis sz�r�st haszn�ljuk, a megjelen�tett rekordokat k�l�n sz�molnunk kell.
		ascstr			: string;
		lastfield		: string;
		ascending		: boolean;
		oldselect		: string;
		oldselnum		: integer;
		oldstatus		: integer;
		index_arr		: array[0..2] of integer;
		menuelem		:TPopupmenu;
		CheckList		: TStringList;
		CheckList2		: TStringList;
		KeresoTable		: string;
		KeresoStr		: string;
  	    keres_str	    : string;
       datum_tab       : string;
       datum_mez       : string;
    keresheto: boolean;
    cMenuClosed: Cardinal;
    InkrementalisSzures: boolean;
    OldRestore: TNotifyEvent;
    SzuresElemlista: array of TStringList;
    procedure MyMinimize(var Msg: TWMSysCommand); message WM_SYSCOMMAND;
    procedure DoRestore(Sender: TObject);
	public
		oldrecord		: string;
		mentettfo		: string;
		voltalt			: boolean;
		vanrek 			: boolean;
		ret_vkod		: string;
		ret_vnev		: string;
		valaszt			: boolean;
		vancols			: boolean;
		FELVISZ			: string;
		MODOSIT  		: string;
		LISTCIM			: string;
		FOMEZO			: string;
		NEVMEZO			: string;
		ALAPSTR			: string;
		FOTABLA			: string;
		DLG_WIDTH		: integer;
		DLG_HEIGHT		: integer;
		multiselect		: boolean;
		selectlist		: TStringList;
		// selectlist2		: TStringList;
		selectnevlist 	: TStringList;
		SelString		: string;
		kellujures		: boolean;
		kulcsmezo		: string;
		alapszin		: integer;
		rkod			: string;
    vedettElemek: TStringList;  // ID lista, a nem t�r�lhet� elemek
		megtekintes		: boolean;
		uresszures		: boolean;	// A mez�sz�r�sben szab�lyozzuk az �resek sz�r�s�t, true = enged�lyezett
		SZUR_MEZO   	: TStringList;
		SZUR_URES   	: TStringList;
    FixSzuresNev  : TStringList;  // a "Sz�r�sek" gombon aktiv�lhat� "behegesztett" sz�r�sek neve ...
    FixSzuresSQL  : TStringList;  //  ... �s az SQL
    SZUMMA_MEZO  	: TStringList;  // szumm�zhat� mez�k
    pFontSize: integer;    // SQLList.pFontSize
    pDisplaySzorzo: double;  // SQLList.pDisplaySzorzo
    pFieldMargin: integer;    // SQLList.pFieldMargin
   end;

   TDateFields      = record
       tablename   : string;
       fieldname   : string;
   end;

var
  J_FOFORMUJDLG: TJ_FOFORMUJDLG;

implementation

uses
  Kozos, Egyeb, J_SQL, J_EDITCOLUJ, Valszuro, Valszuro2, SzummaMutat, Math, UzenetTop;

const
	BUTTONWIDTH = 160;		// A nyom�gombok sz�less�ge
	BUTTONCOUNT	= 7;		// Az soronk�nti nyom�gombok sz�ma (minwidth = buttonwidth*buttoncount+10)
   DATUMTABLAK = 14;
   DATUMSZURO  : array [1..DATUMTABLAK] of TDateFields =
       (
           (tablename   : 'JARAT';     fieldname   : 'JA_JKEZD'),
           // (tablename   : 'MEGBIZAS';  fieldname   : 'MB_DATUM'),
           // (tablename   : 'MEGBIZAS';  fieldname   : 'MB_DATUM'),
           (tablename   : 'MEGBIZAS';  fieldname   : 'MB_TEDAT'),
           (tablename   : 'MEGBIZAS';  fieldname   : 'MB_TEDAT'),
           (tablename   : 'ARFOLYAM';  fieldname   : 'AR_DATUM'),
           (tablename   : 'KOLTSEG';   fieldname   : 'KS_DATUM'),
           (tablename   : 'TEHER';     fieldname   : 'TH_DATUM'),
           (tablename   : 'ZAROKM';    fieldname   : 'ZK_DATUM'),
           (tablename   : 'UZEMANYAG'; fieldname   : 'UA_DATUM'),
           (tablename   : 'CMRDAT';    fieldname   : 'CM_CRDAT'),
           (tablename   : 'TETANAR';   fieldname   : 'TT_DATUM'),
           (tablename   : 'PONTOK';    fieldname   : 'PP_DATUM'),
           (tablename   : 'SZFEJ';     fieldname   : 'SA_DATUM'),
           (tablename   : 'JARPOTOS';  fieldname   : 'EV'),   // Query-b�l j�nnek a mez�nevek
           (tablename   : 'SZFEJ2';    fieldname   : 'SA_DATUM')
       );
          // (tablename   : 'SZFEJ';     fieldname   : 'SA_KIDAT'),
  NincsDarabszam = '( Sz�molj! )';
  LeszDarabszam = '(Sz�molok...)';

{$R *.dfm}

procedure TJ_FOFORMUJDLG.FormCreate(Sender: TObject);
begin
//	Application.CreateForm(TValszuroDlg, ValszuroDlg);
	mezonevek 		:= TStringList.Create;
	valaszt			:= false;
	vanrek 			:= false;
	voltalt 		:= true;
	vancols			:= false;
  if EgyebDlg.ALAPERTELMEZETT_KERESESI_MOD = 1 then begin  // r�gi m�dszer
    InkrementalisSzures:= false;
    ebInkrementalis.Visible:= False;
    end;
  if EgyebDlg.ALAPERTELMEZETT_KERESESI_MOD = 2 then begin // inkrement�lis
    InkrementalisSzures:= True;
    ebInkrementalis.Visible:= True;
    end;
	kellujures		:= true;
	index_arr[0]	:= -1;
	index_arr[1]	:= -1;
	index_arr[2]	:= -1;
	DLG_WIDTH  		:= BUTTONCOUNT * BUTTONWIDTH + 10;
	DLG_HEIGHT 		:= 768;
	multiselect		:= false;
	megtekintes		:= false;
	uresszures		:= False;
	oldselect		:= '';
	oldselnum		:= 0;
	oldstatus		:= 0;
	SelectList		:= TStringList.Create;
	// SelectList2		:= TStringList.Create;
	SelectNevList 	:= TStringList.Create;
  SZUR_MEZO:= TStringList.Create;
  SZUR_URES:= TStringList.Create;
  SZUMMA_MEZO:= TStringList.Create;
  FixSzuresNev:= TStringList.Create;
  FixSzuresSQL:= TStringList.Create;
  vedettElemek:= TStringList.Create;
	if not EgyebDlg.Export_Available then begin
		BtnExport.Hide;
	end;
	SGSzuromezok.RowCount	:= 1;
	SGSzuromezok.Rows[0].Clear;
	SzuresUrites;
	SzuresBeolvasas;
	SzuresBeolvasas2;
	kulcsmezo		 		:= '';
	KeresoTable				:= '';
	KeresoStr				:= '';
	alapszin				:= DBGrid2.Canvas.Brush.Color;
	ButtonNez.Visible		:= false;	// Alap�rtelmez�sben nincs megtekint�s
	CheckList				:= TStringList.Create;
	CheckList2				:= TStringList.Create;
	SetMaskEdits([TMaskEdit(Edit1)]);
  ButtonSzuresek.Visible:= False;   // K�l�n kell enged�lyezni az InitSzuresek-kel
end;

procedure TJ_FOFORMUJDLG.FormDestroy(Sender: TObject);
var
  i: integer;
begin
	Set_Index;
	mezonevek.Free;
	while (Query1.FieldCount <> 0) do begin
		Query1.Fields[0].Free;
	end;
	SzuresKiiras;
	CheckList.Free;
	CheckList2.Free;
  if FixSzuresNev<>nil then FixSzuresNev.Free;
  if FixSzuresSQL<>nil then FixSzuresSQL.Free;
  if vedettElemek<>nil then vedettElemek.Free;
  for i:=0 to Length(SzuresElemlista)-1 do
    if SzuresElemlista[i] <> nil then SzuresElemlista[i].Free;
//	ValszuroDlg.Destroy;
end;

procedure TJ_FOFORMUJDLG.InitSzuresek;
var
  i: integer;
begin
  if FixSzuresNev.Count > 0 then begin
    ppmSzuresek.Items.Clear;
    for i:=0 to FixSzuresNev.Count-1 do begin
       ppmSzuresek.Items.Add(TMenuItem.Create(ppmSzuresek));
       with ppmSzuresek.Items[ppmSzuresek.Items.Count-1] do begin
          Caption:=FixSzuresNev[i];
          Tag:=i;  // k�s�bb az OnClick-hez
          OnClick:= SzuresekElemClick;
          end;  // with
       end;  // for
       cMenuClosed := 0;
       ButtonSzuresek.Visible:= True;
       end
  else begin
       ButtonSzuresek.Visible:= False;
        end;  // else
end;

procedure TJ_FOFORMUJDLG.Query1FilterRecord(DataSet: TDataSet;
  var Accept: Boolean);
var
  i: integer;
  Keresett, Mezotartalom: string;
begin
  if InkrementalisSzures then begin
    Keresett:= Uppercase_HUN(ebInkrementalis.Text);
    for i := 0 to DataSet.FieldCount - 1 do begin
      Mezotartalom:= Uppercase_HUN(DataSet.Fields[i].AsString);
      Accept := Pos(Keresett, Mezotartalom) > 0;
      if Accept then begin
        // Inc(sorokszama);
        exit;
        end;  // if
      end; // for
    end  // if
  else begin  // ha nincs inkrement�lis sz�r�s, akkor minden adatsor j�het
    Accept := True;
    // Inc(sorokszama);
    end;
end;

procedure TJ_FOFORMUJDLG.SzuresekElemClick(Sender: TObject);
var
   i: integer;
   SQL, szuro: string;
begin
   i:= (Sender as TMenuItem).Tag;
   SQL:= FixSzuresSQL[i];
   // NoticeKi(SQL);
   szuro:= EgyebDlg.GetSzuroStr(IntToStr(tag));
   if Pos(SQL, szuro)= 0 then begin  // m�g nincs benne
     if Trim(szuro) <> '' then
          szuro:= szuro + ' and ' + SQL
     else
          szuro:= SQL;
     EgyebDlg.SetSzuroStr(IntToStr(tag), szuro);
     SQL_ToltoFoMag(GetOrderBy(true));  // nem kell sz�r�selem-list�kat friss�teni, biztosan nem b�v�ltek.
     end;
end;

procedure TJ_FOFORMUJDLG.ButtonKilepClick(Sender: TObject);
begin
	if not voltalt then begin
		voltalt 	:= true;
		Exit;
	end;
	ret_vkod		:= '';
	ret_vnev		:= '';
	selectlist.Clear;
  // selectlist2.Clear;
   SelectNevList.Clear;
	Close;
end;

procedure TJ_FOFORMUJDLG.ButtonPrintClick(Sender: TObject);
var
	aktrekord	: string;
begin
	if not voltalt then begin
		voltalt 	:= true;
 		Exit;
 	end;
	Edit1.Text	:= '';
	// Megjegyezz�k hogy hol vagyunk
	aktrekord	:= Query1.FieldByName(FOMEZO).AsString;
	Query1.DisableControls;
	SQL_List(Query1,LISTCIM, Tag, '', pFontSize, pDisplaySzorzo, pFieldMargin);
	// Vissza�llunk a rekordra
	Query1.Locate(FOMEZO, aktrekord, [] );
	Query1.EnableControls;
end;

procedure TJ_FOFORMUJDLG.ButtonUjClick(Sender: TObject);
begin
	if not voltalt then begin
 		voltalt 	:= true;
 		Exit;
 	end;
  	Adatlap(FELVISZ,'');
	  {Label3.Caption	:= '('+IntToStr(Query1.RecordCount)+'/'+IntToStr(Query1.RecordCount)+')';
  	Label3.Update;}
    DarabszamMutat(True);
end;

procedure TJ_FOFORMUJDLG.ButtonModClick(Sender: TObject);
begin
	if not voltalt then begin
 		voltalt 	:= true;
 		Exit;
 	end;
	if ButtonMod.Enabled then begin
  		Adatlap(MODOSIT,Query1.FieldByName(FOMEZO).AsString);
  	end;
end;

procedure TJ_FOFORMUJDLG.ButtonKuldClick(Sender: TObject);
begin
	if not voltalt then begin
		voltalt 	:= true;
     	Exit;
	end;
	 if ButtonKuld.Visible then begin
		ret_vkod	:= Query1.FieldByName(FOMEZO).AsString;
     	if multiselect then begin
			if SelectList.Count < 1 then begin
				  SelectList.Add(Query1.FieldByName(KULCSMEZO).AsString);
          // SelectList2.Add(Query1.FieldByName(KULCSMEZO2).AsString);
          SelectNevList.Add(Query1.FieldByName(NEVMEZO).AsString);
           end;
       end;
	  	Close;
   end;
end;

procedure TJ_FOFORMUJDLG.ButtonSzuresClick(Sender: TObject);
begin
	if not voltalt then begin
     	voltalt 	:= true;
		Exit;
  	end;
	Szures(Query1, alapstr, GetOrderBy(false), Tag);
	DbGrid2.SelectedIndex 	:= ComboBox1.ItemIndex;
  	//Label3.Caption			:= '('+IntToStr(Query1.RecordCount)+')';
  	//Label3.Update;
    DarabszamMutat(False);
end;

procedure TJ_FOFORMUJDLG.FormResize(Sender: TObject);
var
	i : integer;
begin
	// A vez�rl� panel be�ll�t�sai
	Label2.Left			 := PanelControl.Width  - Label2.Width - Edit1.Width - ButtonSzures.Width - Combobox2.Width - 40;
	Edit1.Left			 := PanelControl.Width  - Edit1.Width - ButtonSzures.Width - Combobox2.Width - 30;
  if InkrementalisSzures then ebInkrementalis.Left:= Edit1.Left;  // r�teszem
	ButtonSzures.Left	 := PanelControl.Width  - ButtonSzures.Width - Combobox2.Width - 20;
   Combobox2.Left       := PanelControl.Width  - Combobox2.Width - 10;
	BtnExport.Left		 := Label2.Left         - BtnExport.Width - 5;
	SB1.Left			 := BtnExport.Left      - SB1.Width - 5;
	// Az als� panel elhelyez�se
	PanelBottom.Height 	:= 45;
	for i := 0 to PanelBottom.ControlCount - 1 do begin
		if ( ( PanelBottom.Controls[i].Tag > BUTTONCOUNT ) and (PanelBottom.Controls[i].Visible) )then begin
			PanelBottom.Height 	:= 92;
      break;
		end;
	end;
	PlaceButtons;
	Minimalizal(Sender);
end;

procedure TJ_FOFORMUJDLG.FormShow(Sender: TObject);
begin
	ButtonKuld.Visible  	:= valaszt;
	EgyebDlg.SetJogok(Tag, ButtonUj, ButtonMod, ButtonTor, ButtonPrint, ButtonNez);
	menuelem				:= DbGrid2.PopupMenu;
  if EgyebDlg.ALAPERTELMEZETT_KERESESI_MOD = 2 then begin // inkrement�lis
    ebInkrementalis.SetFocus;
    end;
//	J_FOFORMUJDLG.
//	WindowState   := wsMaximized;
//  	SpeedButton1.Visible	:= vancols;
end;


// ----- Ezekkel az elj�r�sokkal az ablak minimaliz�l�sa a teljes alkalmaz�st a t�lc�ra helyezi --- //
// ----- A vissza�ll�t�s sor�n az eredeti akt�v ablak ker�l az el�t�rbe. --   //
procedure TJ_FOFORMUJDLG.MyMinimize(var msg: TWMSysCommand);
begin
  if (msg.cmdtype and $FFF0) = SC_MINIMIZE then
  begin
    // the following line only for D5, not for D3, due to a bug(?) in forms.pas }
    EnableWindow(Application.handle, true);
    Application.Minimize;
    OldRestore := Application.OnRestore;
    Application.OnRestore := DoRestore;
    msg.Result := 0;
  end
  else
    inherited;
end;

procedure TJ_FOFORMUJDLG.DoRestore(Sender: TObject);
begin
  Application.OnRestore := OldRestore;
  SetForegroundWindow(Handle);
end;
// --------------------------------------------------------------------------------------------------- //

procedure TJ_FOFORMUJDLG.ComboBox1Change(Sender: TObject);
var
	aktrekord	: string;
begin
	Edit1.Text	:= '';
	if lastfield = mezonevek[ComboBox1.ItemIndex] then begin
		// �jra ugyanazt a mez�t v�lasztott�k -> meg kell ford�tani a sorrendet
		ascending	:= not ascending;
	end;
	if index_arr[0]	<> ComboBox1.ItemIndex then begin
		ascending	:= true;
		// A r�gebbi mez�t betesz�k a r�gi indexbe
		index_arr[2]	:= index_arr[1];
		index_arr[1]	:= index_arr[0];
		index_arr[0]	:= ComboBox1.ItemIndex;
	end;
	ascstr	:= ' ASC';
	if not ascending then begin
		ascstr	:= ' DESC';
	end;
	aktrekord	:= Query1.FieldByName(FOMEZO).AsString;
	SQL_ToltoFoMag(GetOrderBy(true)); // csak a sorrend v�ltozott; nem kell sz�r�selem-list�kat friss�teni, biztosan nem b�v�ltek.
	// R��llunk a kiv�lasztott oszlopra figyelve a sz�m�tott mezokre
	lastfield := mezonevek[ComboBox1.ItemIndex];
	DbGrid2.SelectedField := Query1.FieldByName(mezonevek[ComboBox1.ItemIndex]);
	if Sender is TComboBox then begin
		// Ha a ComboBox k�rte a m�veletet, akkor m�r lehet f�kusz�lni a gridre
 		DbGrid2.SetFocus;
 	end;
	Query1.Locate(FOMEZO, aktrekord, [] );
end;

procedure TJ_FOFORMUJDLG.ebInkrementalisChange(Sender: TObject);
var
  ekezet: integer;
begin
	// ekezet := Pos(chr(Key),	Chr(186)+chr(222)+chr(192)+chr(191)+chr(219)+chr(187)+Chr(220)+Chr(226)+Chr(221));
	// if ekezet > 0 then begin
	// 	Edit1.Text := Edit1.Text + copy('���������', ekezet,1);

  Query1.Filtered := false;
  Query1.Filtered := (ebInkrementalis.Text <> '');
  Label3.Caption:= NincsDarabszam;
  Label3.Update;
end;

procedure TJ_FOFORMUJDLG.ebInkrementalisClick(Sender: TObject);
begin
   // ebInkrementalis.Text:= '';
   ebInkrementalis.SelectAll;
end;

procedure TJ_FOFORMUJDLG.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
//var
//	keres_str	: string;
begin
 	if ssAlt in Shift then begin
 		voltalt := true;
    if ( ( key = VK_F3 ) and (EgyebDlg.user_super) ) then begin
     	 	if ColorDialog1.Execute then begin
            EgyebDlg.row_color := ColorDialog1.Color;
            EgyebDlg.WriteRowColor;
		        DbGrid2.Repaint;
			      end;  // ColorDialog
        Exit;
        end;  // Alt-F3
 	    end;  // Alt-valami

 	if ssCtrl in Shift then begin
    if Chr(Key) in ['S','s'] then begin
      if InkrementalisSzures then begin
       ebInkrementalis.SetFocus;
       ebInkrementalis.SelectAll;
       Exit;
       end;
     end;  // Ctrl-S
    if Chr(Key) in ['N','n']  then begin
      if (dgEditing in DBGrid2.Options) then
          DBGrid2.Options := DBGrid2.Options - [dgEditing, dgAlwaysShowEditor]  // Removes dbEditing option
      else DBGrid2.Options := DBGrid2.Options + [dgEditing, dgAlwaysShowEditor];  // Adds dbEditing option
      Exit;
     end;  // Ctrl-N
    end;  // Ctrl-valami

  if key = VK_F8 then begin  // F8
        if not InkrementalisSzures then begin // oda-vissza kapcsolja a sz�r�si m�dot
            InkrementalisSzures:= True;
            ebInkrementalis.Left:= Edit1.Left;  // r�teszem
            ebInkrementalis.Visible:= True;
            ebInkrementalis.SetFocus;
            end
        else begin
            InkrementalisSzures:= False;
            ebInkrementalis.Text:='';  // = kikapcsoljuk a sz�r�st
            ebInkrementalis.Visible:= False;
            DarabszamMutat(False);
            end;
        Exit;
        end; // F8

  if key = VK_F9 then begin  // F9: "�zenetek" ablak b�rmelyik t�bl�zatb�l
      if UzenetTopDlg <> nil then begin
        UzenetTopDlg.SetActivePage(UzenetTopDlg.ActivePage);  // friss�ti az aktu�lis lapot
        UzenetTopDlg.Show;
        Exit; // abort key processing
        end;
      end; // F8


  if not InkrementalisSzures then begin
    voltalt  := false;
    SajatFormKeyDown(Sender, Key, Shift);
    if EgyebDlg.keresheto then begin
      keres_str	:= KeresChange(mezonevek[ComboBox1.ItemIndex], Edit1.Text);
      if Timer_Keres.Interval=1 then // azonnal keres
        Keres(Query1, mezonevek[ComboBox1.ItemIndex], Edit1.Text)
      else begin
        Timer_Keres.Enabled:=False;
        Timer_Keres.Enabled:=True;
        end;  // else
      if not Edit2.Focused then
         DbGrid2.SetFocus;
      end;  // if keresheto
     end  // norm�l keres�s
  else begin  // inkrement�lis
     // inherited OnKeyDown(Sender, Key, Shift); -- na itt lett a v�gtelen ciklus
     end;
end;

procedure TJ_FOFORMUJDLG.FormKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
	voltalt  := true;
end;

procedure TJ_FOFORMUJDLG.DBGrid2TitleClick(Column: TColumn);
begin
 	if mezonevek.IndexOf(Column.FieldName) > -1 then begin
		ComboBox1.ItemIndex := mezonevek.IndexOf(Column.FieldName);
		ComboBox1Change(self);
	end;
end;

procedure TJ_FOFORMUJDLG.ButtonTorClick(Sender: TObject);
var
  ID: string;
begin
	if not voltalt then begin
 		voltalt 	:= true;
 		Exit;
	end;
   ID:= Query1.FieldByName(FOMEZO).AsString;
   if ID <> '' then begin
       if vedettElemek.IndexOf(ID) = -1 then begin  // nincs benne
         if not EgyebDlg.RekordTorles(Query1, alapstr, GetOrderBy(false) ,Tag,
           'DELETE from '+FOTABLA+' where '+FOMEZO+'='''+ID+''' ') then begin
           if datum_tab = '' then begin
               // Csak akkor z�rjuk be, ha nincs d�tum szerinti t�rl�s
              Close;
              end;  // if
           end; // if
        end  // if nem v�dett
      else begin  // v�dett elem
        NoticeKi('Az elem t�rl�se nem enged�lyezett!');
        end;
   end;
  	// Label3.Caption	:= '('+IntToStr(Query1.RecordCount)+')';
  	// Label3.Update;
    DarabszamMutat(False);
end;

procedure TJ_FOFORMUJDLG.FelTolto(s1, s2, s3, s4, s5, s6 : string; v_tag : integer);
const
  FirstDBYear =  2013;
var
	oszlopnr		: integer;
   i          : integer;
   ThisYear   : integer;
   S, OB      : string;
begin
	Query1.Close;
	Query2.Close;
	Query1.Tag	:= v_tag;
	Query2.Tag	:= v_tag;
	EgyebDlg.SeTADOQueryDatabase(Query1);
	EgyebDlg.SeTADOQueryDatabase(Query2);
	FELVISZ	:= s1;
	MODOSIT := s2;
	LISTCIM	:= s3;
	FOMEZO	:= s4;
	ALAPSTR	:= s5;
	FOTABLA	:= s6;
	NEVMEZO	:= FOMEZO;

  // �vek list�j�nak automatikus felt�lt�se
  with ComboBox2 do begin
     Items.Clear;
     Items.Add('mind');
     ThisYear := StrToInt(FormatDateTime('yyyy', Now));
     for i := FirstDBYear to ThisYear do begin
         Items.Add(IntToStr(i));
         end;  // for
     ItemIndex := Items.Count -1;
     end; // with

	// A Query felt�lt�se
	if not ReadFields then begin
		vanrek := false;
		Exit;
	end;
//   GetFieldStr(Tag, Query1);
	SetQueryFields;
	// Caption	:= Caption + ' ('+SW_VERZIO+')';
  if EgyebDlg.Alcim <> '' then S:=', '+EgyebDlg.Alcim else S:='';
  Caption	:= Caption + ' ('+SW_VERZIO+S+')';    // jelenjen meg minden fejl�cben, ha teszt verzi�
	mentettfo	:= '';
	Get_Index;
	oszlopnr	:= index_arr[0];
	ascstr		:= ' ASC';
	if not ascending then begin
		ascstr	:= ' DESC';
	end;
	if oszlopnr  = -1 then begin
		oszlopnr  := 0;
	end;

   // A d�tumsz�r�s lekezel�se
   ComboBox2.Hide;
   ComboBox2.Width := 0;
   datum_tab       := '';
   datum_mez       := '';
   for i := 1 to DATUMTABLAK do begin
       if Pos(UpperCase(s6), DATUMSZURO[i].tablename) > 0 then begin
           ComboBox2.Show;
           ComboBox2.Width     := 80;
           ComboBox2.ItemIndex := 0;   // R��llunk a 'mind' -re
           datum_tab           := DATUMSZURO[i].tablename;
           datum_mez           := DATUMSZURO[i].fieldname;
       end;
   end;
   // Megn�zz�k az �vsz�mot
   ComboBox2.ItemIndex   := Max(0, ComboBox2.Items.IndexOf(EgyebDlg.EvSzam));

	ComboTolto(oszlopnr);

	SQL_ToltoFo(GetOrderBy (true)); // friss�tj�k a sz�r� list�kat

	if mentettfo = '' then begin
		Query1.First;
	end else begin
		Query1.Locate(FOMEZO, mentettfo, [] );
	end;
	vanrek := ( Query1.RecordCount > 0 );
	// Ha nincs rekord, el�sz�r pr�b�ljuk meg t�r�lni a sz�r�si felt�teleket
	if not vanrek then begin
		SzuresUrites;
		SQL_ToltoFo(GetOrderBy (true));
		Query1.First;
		vanrek := ( Query1.RecordCount > 0 );
	end;
	if ( ( not vanrek ) and (kellujures) ) then begin
		// Az adatb�zis m�g �res, �j felvitel
		ButtonUjClick(self);
		SQL_ToltoFo(GetOrderBy(false));
		vanrek := ( Query1.RecordCount > 0 );
	end;
	if vanrek then begin
		//  ascending   := not ascending;
		lastfield		:= mezonevek[ComboBox1.ItemIndex];
		//	ComboBox1Change(Sender);
		Edit1.Text 		:= '';
		// Label3.Caption	:= '('+IntToStr(Query1.RecordCount)+')';
		// Label3.Update;
    DarabszamMutat(False);
	end;
	kulcsmezo	:= fomezo;


(*
	// A sz�r�si mez�ket �rjuk ki d�nt�tt bet�kkel
	for i := 0 to Dbgrid2.Columns.Count - 1 do begin
		if SGSzuromezok.Cols[0].IndexOf(Dbgrid2.Columns[i].FieldName) > -1 then begin
			//Dbgrid2.Columns[i].Font.Style := [fsItalic];
			Dbgrid2.Columns[i].Field.DisplayLabel := '&'+Dbgrid2.Columns[i].Field.DisplayLabel;
		end;
	end;
*)
end;

procedure TJ_FOFORMUJDLG.Adatlap(cim, kod : string);
var
   ujdat   : string;
begin
	// Ez a r�szletes adatlap bek�r�si ablaka
	Edit1.Text	:= '';
	AlForm.megtekint	:= megtekintes;
	AlForm.Tolto(cim, kod);
	Alform.ShowModal;
	if AlForm.ret_kod <> '' then begin
       // D�tum megad�s ellen�rz�se
       if ( ( datum_tab <> '' ) and (ComboBox2.ItemIndex <> 0) ) then begin
           ujdat   := copy(Query_Select(datum_tab, FOMEZO, AlForm.ret_kod, datum_mez), 1, 4);
           if ujdat <> ComboBox2.Text then begin
               ComboBox2.ItemIndex   := Max(0, ComboBox2.Items.IndexOf(ujdat));
               ComboBox2Change(Self);
           end;
       end;
		ModLocate (Query1, FOMEZO, AlForm.ret_kod );
	end;
	AlForm.Destroy;
	DBGrid2.Refresh;
end;

procedure TJ_FOFORMUJDLG.Query1CalcFields(DataSet: TDataSet);
begin
 	Mezoiro;
end;

procedure TJ_FOFORMUJDLG.FormCanResize(Sender: TObject; var NewWidth,
  NewHeight: Integer; var Resize: Boolean);
begin
	if ( ( NewWidth < ( BUTTONCOUNT * BUTTONWIDTH + 10 ) ) or ( NewHeight < 600 ) ) then begin
  		Resize := false;
  	end;
end;

procedure TJ_FOFORMUJDLG.PlaceButtons;
var
	i 		: integer;
	space	: integer;
  	felso	: integer;
begin
	// Szimmetrikusan elhelyezi a nyom�gombokat
	ShapeTorles.Visible	:= ButtonTor.Visible;
	space	:= ( PanelBottom.Width - buttoncount * BUTTONWIDTH ) DIV (buttoncount + 1);
  	if space < 0 then begin
  		space := 0;
  	end;
	for i := 0 to PanelBottom.ControlCount -1 do begin
  		felso	:= 6;
  		if PanelBottom.Controls[i].Tag > BUTTONCOUNT then begin
     		felso := 42;     //46
     	end;
		if PanelBottom.Controls[i] is TBitBtn then begin
			if Pos('T�rl�s', (PanelBottom.Controls[i] as TBitBtn).Caption	) > 0 then begin
				ShapeTorles.Width				:= BUTTONWIDTH;
				ShapeTorles.Left				:= space + ( PanelBottom.Controls[i].Tag -1 )* (space + BUTTONWIDTH);
				ShapeTorles.Top					:= felso;
				PanelBottom.Controls[i].Width	:= BUTTONWIDTH - 10;
				PanelBottom.Controls[i].Left 	:= space + 5 + ( PanelBottom.Controls[i].Tag -1 )* (space + BUTTONWIDTH);
				PanelBottom.Controls[i].Top 	:= felso + 5;
			end else begin
				PanelBottom.Controls[i].Width	:= BUTTONWIDTH;
				PanelBottom.Controls[i].Left 	:= space + ( PanelBottom.Controls[i].Tag -1 )* (space + BUTTONWIDTH);
				PanelBottom.Controls[i].Top 	:= felso;
			end;
			if PanelBottom.Controls[i].Tag > BUTTONCOUNT then begin
				PanelBottom.Controls[i].Left 	:= space + ( PanelBottom.Controls[i].Tag - BUTTONCOUNT - 1 )* (space + BUTTONWIDTH);
			end;
		end;
  	end;
end;

procedure TJ_FOFORMUJDLG.DataSource1DataChange(Sender: TObject;
  Field: TField);
begin
	// Ez a procedura h�v�dik meg, ha v�ltozik a rekord
	oldrecord			:= Query1.FieldByName(FOMEZO).AsString;
  // sorokszama:= 0;
	DBGrid2.Refresh;
	// Label3.Caption		:= '('+IntToStr(Query1.RecNo)+'/'+IntToStr(Query1.RecordCount)+')';
  // Label3.Update;
  if not InkrementalisSzures then
    DarabszamMutat(True);
end;

procedure TJ_FOFORMUJDLG.BtnExportClick(Sender: TObject);
begin
	// Export folyamat
	if not voltalt then begin
 		voltalt 	:= true;
 		Exit;
 	end;
	Application.CreateForm(TExportDlg, ExportDlg);
	ExportDlg.Tolto('DEMO EXPORT',Query1);
	ExportDlg.ShowModal;
	ExportDlg.Destroy;
end;

function TJ_FOFORMUJDLG.KeresChange(mezo, szoveg : string) : string;
begin
	// Ebben a funkci�ban lehet megval�s�tani a keres�si sz�veg cser�j�t
  	Result	:= szoveg;
end;

procedure TJ_FOFORMUJDLG.Label3Click(Sender: TObject);
var
  sorokszama: integer;
begin
   Query1.DisableControls;
   Screen.Cursor	:= crHourGlass;
   Label3.Caption:= LeszDarabszam;
   Label3.Update;
   try
      with Query1 do begin
        First;
        sorokszama:= 0;
        while not eof do begin
          Next;
          Inc(sorokszama);
          end;
        First;  // vissza az elej�re a gridben
        end;  // with
      Label3.Caption:= '('+IntToStr(sorokszama)+')';
      Label3.Update;
   finally
      Query1.EnableControls;
      Screen.Cursor	:= crDefault;
      end;  // try-finally
end;

function TJ_FOFORMUJDLG.GetOrderBy ( ascflag : boolean ) : string;
// Az index_arr t�mbben a mez�k sorsz�ma tal�lhat�; -1 = nem haszn�ljuk.
// az el�z�leg kiv�lasztott sorrendez�sek t�ltik fel.
var
	str	: string;
begin
	str	:= '';
  	if index_arr[0] = -1 then begin
   	index_arr[0] := 0;
   end;
  	if ( ( index_arr[0] <> -1 ) and (index_arr[0] < mezonevek.Count) ) then begin
     	str := ' order by '+mezonevek[index_arr[0]];
     	if ascflag then begin
        	str := str + ' ' + ascstr;
		end;
  	end;
  	if ( ( index_arr[1] <> -1 ) and (index_arr[1] < mezonevek.Count) and (index_arr[1] <> index_arr[0])) then begin
		str := str + ', ' +mezonevek[index_arr[1]];
  	end;
  	if ( ( index_arr[2] <> -1 ) and (index_arr[2] < mezonevek.Count) and (index_arr[2] <> index_arr[0]) and (index_arr[2] <> index_arr[1] ) ) then begin
		str := str + ', ' +mezonevek[index_arr[2]];
  	end;
  	Result	:= str;
end;


procedure TJ_FOFORMUJDLG.SB1Click(Sender: TObject);
var
  i		: integer;
begin
	// Megjelen�teni egy t�bl�t az oszlopsorrendekkel + ki-be kapcsolhat� is legyen
	Application.CreateForm(TEditcolUjDlg, EditcolUjDlg);
	EditcolUjDlg.Tolt2(SGFields, Tag);
	EditcolUjDlg.ShowModal;
	if EditcolUjDlg.ret_value then begin
		// Bet�ltj�k az �jabb elrendez�st
		for  i:= 0 to SGFields.RowCount -1 do begin
			SGFields.Rows[i] 	:= EditcolUjDlg.SG1.Rows[i+1];
		end;
		SetQueryFields;
		SQL_ToltoFo(GetOrderBy (true)); // lehet hogy �j oszlop lett l�that�, friss�teni kell a sz�r�selem-list�kat
		ComboTolto(0);
	end;
	EditcolUjDlg.Destroy;
end;

procedure TJ_FOFORMUJDLG.DBGrid2DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
var
  r0,r1,r2: TRect;
  s0,s1,s2: string;
  keresettmintahely: integer;
begin
	if ( gdFocused in state ) then begin
		// A kiemeltekn�l ne t�nj�n el a felirat, legyen a sor sz�ne fekete bet�kkel!!!
		DBGrid2.Canvas.Brush.Color		:= alapszin;
		DBGrid2.Canvas.Font.Color		:= clBlack;
		DBGrid2.DefaultDrawDataCell(Rect, Column.Field, State);
	end;

	if SelectList.IndexOf(Query1.FieldByName(KULCSMEZO).AsString) > -1 then begin
		// A sor ki van jel�lve
		DBGrid2.Canvas.Brush.Color		:= clSilver;
		DBGrid2.DefaultDrawDataCell(Rect, Column.Field, State);
	end;

	if oldrecord = Query1.FieldByName(KULCSMEZO).AsString then begin
		// Egy fekete keretet rajzol az aktu�lis sor k�r�!!!
       DBGrid2.Canvas.Pen.Color	:= clBlack;
		DBGrid2.Canvas.Pen.Width	:= 3;
		DBGrid2.Canvas.MoveTo(0, Rect.Top-1);
		DBGrid2.Canvas.LineTo(DbGrid2.Width, Rect.Top-1);
		DBGrid2.Canvas.LineTo(DbGrid2.Width, Rect.Bottom-1);
		DBGrid2.Canvas.LineTo(0, Rect.Bottom-1);
		DBGrid2.Canvas.LineTo(0, Rect.Top-1);
	end;

  // ------ inkrement�lis keres�s minta-sz�nez�se ------------- //
  keresettmintahely:= Pos(Uppercase_HUN(ebInkrementalis.Text), Uppercase_HUN(Column.Field.AsString));
  if (ebInkrementalis.Text = '') or (keresettmintahely = 0) then exit;  // nincs benne, nem sz�nez�nk

  r0 := Rect; r1 := Rect; r2 := Rect;
  r0.Top:= r0.Top +2;    // margin
  r1.Top:= r1.Top +2;    // margin
  r2.Top:= r2.Top +2;    // margin
  r0.Left:= r0.Left +2;  // margin
  DBGrid2.Canvas.FillRect(Rect);  // init
  // -- az eleje, ha van -- //
  if (keresettmintahely > 1) then begin
    s0 := copy(Column.Field.AsString,1,keresettmintahely-1);
    DBGrid2.Canvas.Font.Assign(DBGrid2.Font);
    DrawText(DBGrid2.Canvas.Handle,pchar(s0),length(s0), r0,DT_CALCRECT);
    DBGrid2.Canvas.TextOut(r0.Left,r0.Top,s0);
    end;  // if
  // -- a megtal�lt minta -- //
  s1 := copy(Column.Field.AsString,keresettmintahely,length(ebInkrementalis.Text));
  if (keresettmintahely > 1) then
    r1.Left := r0.Right + 1 // el kell jobbra tolni
  else r1.Left := r0.Left; // ugyanott kezd�dhet
  DBGrid2.Canvas.Font.Color := clRed;
  DBGrid2.Canvas.Font.Style := [fsbold];
  DrawText(DBGrid2.Canvas.Handle,PChar(s1),Length(s1), r1,DT_CALCRECT); // "... DrawText modifies the right side of the rectangle..."
  DBGrid2.Canvas.TextOut(r1.Left,r2.Top,s1);
  // -- a v�ge, ha van -- //
  if keresettmintahely + length(ebInkrementalis.Text) <= length(Column.Field.AsString) then begin
    DBGrid2.Canvas.Font.Assign(DBGrid2.Font);
    s2 := copy(Column.Field.AsString,keresettmintahely+length(ebInkrementalis.Text), 9999999);  // a minta v�g�t�l eg�sz v�gig
    r2.Left := r1.Right + 1;
    DrawText(DBGrid2.Canvas.Handle,pchar(s2),length(s2), r2,0);
    end;  // if

  // ------ inkrement�lis keres�s minta-sz�nez�se ------------- //
end;

procedure TJ_FOFORMUJDLG.DBGrid2MouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
	kezdo 	: integer;
   befej   : integer;
   itemp	: integer;
begin
  // ha Ctrl-N-nel "kim�sol" m�dba kapcsoltunk, a kijel�lt cella tartalm�t automatikusan a v�g�lapra teszi
  with THackGrid(DBGrid2) do begin
    if (dgEditing in DBGrid2.Options) then begin
      Clipboard.Clear;
      Clipboard.AsText:=InplaceEditor.Text;
      end;
    end;  // with

//	if ( ( Button = mbLeft ) and (ssCtrl in  Shift ) and (multiselect) ) then begin
  	if ( ( Button = mbLeft ) and (multiselect) and ((ssCtrl in  Shift) or (ssShift in  Shift)) ) then begin
   	if ssShift in  Shift then begin
       	// Tartom�ny kijel�l�se
//           Label3.Caption	:= oldselect + '-'+Query1.FieldByName(FOMEZO).AsString+'='+IntToStr(oldstatus);
//           Label3.Caption	:= IntToStr(oldselnum) + '-'+IntToStr(Query1.RecNo)+'='+IntToStr(oldstatus);
//           Label3.Update;
           kezdo	:= oldselnum;
           befej	:= Query1.RecNo;
           if kezdo > befej then begin
           	itemp	:= kezdo;
               kezdo	:= befej;
               befej	:= itemp;
           end;
           if kezdo < befej then begin
           	Query1.DisableControls;
           	// R�l�p�nk az el� elemre
				if kezdo < Query1.RecNo then begin
               	Query1.MoveBy(kezdo - Query1.RecNo);
				end;
				while ( ( Query1.RecNo <= befej ) and (not Query1.Eof) ) do begin
					Jeloles(Query1.FieldByName(FOMEZO).AsString, oldstatus);
               	Query1.Next;
               end;
           	Query1.EnableControls;
           end;
           Exit;
       end;
       // Kiv�lasztja ill. visszateszi a megjel�lt elemet
       Jeloles(Query1.FieldByName(FOMEZO).AsString, 2);
       oldselect 		:= Query1.FieldByName(FOMEZO).AsString;
       oldselnum		:= Query1.RecNo;
		DBGrid2.Refresh;
   end;
   DbGrid2.PopupMenu	:= menuelem;
end;

procedure TJ_FOFORMUJDLG.Jeloles(kod : string; tipus: Integer);
(* A megadott k�d� elem kijelolese :
	Tipus 	:
   	0	- kijel�l�s megsz�ntet�se
       1	- kijel�l�s
       2	- kijel�l�s cser�je
*)
var
   fosor	: integer;
begin
	case tipus of
		0	:
       begin
        	fosor	:= SelectList.IndexOf(kod);
			if fosor > -1 then begin
				SelectList.Delete(fosor);
        SelectNevList.Delete(fosor);
        oldstatus	:= 0;
        end;
       end;
   	1	:
       begin
        	fosor	:= SelectList.IndexOf(kod);
        	if fosor < 0 then begin
           	// M�g nem szerepel a list�ban az elem
           	SelectList.Add(Query1.FieldByName(KULCSMEZO).AsString);
           	SelectNevList.Add(Query1.FieldByName(NEVMEZO).AsString);
           	oldstatus	:= 1;
        	end;
       end;
   	2	:
       begin
        	fosor	:= SelectList.IndexOf(kod);
        	if fosor < 0 then begin
           	// M�g nem szerepel a list�ban az elem
           	SelectList.Add(Query1.FieldByName(KULCSMEZO).AsString);
           	SelectNevList.Add(Query1.FieldByName(NEVMEZO).AsString);
           	oldstatus	:= 1;
			end else begin
           	// M�r szerepel a list�ban az elem
           	SelectList.Delete(fosor);
           	SelectNevList.Delete(fosor);
           	oldstatus	:= 0;
        	end;
       end;
   end;
end;

procedure TJ_FOFORMUJDLG.Set_Index;
var
	recszam	: integer;
begin
 if EgyebDlg <> nil then begin
  	recszam	:= EgyebDlg.IndexGrid.Cols[0].IndexOf(IntToStr(Tag));
  	if recszam < 0 then begin
  		EgyebDlg.IndexGrid.RowCount := EgyebDlg.IndexGrid.RowCount + 1;
		recszam	:= EgyebDlg.IndexGrid.RowCount - 1;
	end;
   EgyebDlg.IndexGrid.Cells[0, recszam] := IntToStr(Tag);
   EgyebDlg.IndexGrid.Cells[1, recszam] := IntToStr(index_arr[0]);
   if EgyebDlg.IndexGrid.Cells[2, recszam] <> '' then begin
       if Caption <> EgyebDlg.IndexGrid.Cells[2, recszam] then begin
//           NoticeKi('Duplik�lt dial�gus : '+IntToStr(Tag)+' / '+Caption+ ' <-> '+EgyebDlg.IndexGrid.Cells[2, recszam]);
       end;
  	end;
	EgyebDlg.IndexGrid.Cells[2, recszam] := Caption;
   EgyebDlg.IndexGrid.Cells[3, recszam] := BoolToStr(ascending);
   EgyebDlg.IndexGrid.Cells[4, recszam] := IntToStr(index_arr[1]);
   EgyebDlg.IndexGrid.Cells[5, recszam] := IntToStr(index_arr[2]);
   if FOMEZO <> '' then
     EgyebDlg.IndexGrid.Cells[6, recszam] := Query1.FieldByName(FOMEZO).AsString
   else EgyebDlg.IndexGrid.Cells[6, recszam] := '';
   end;  // if EgyebDlg
end;

function TJ_FOFORMUJDLG.Get_Index : integer;
var
	recszam	: integer;
begin
	index_arr[0]	:= -1;
	index_arr[1]	:= -1;
	index_arr[2]	:= -1;
	ascending		:= true;
   mentettfo		:= '';
  	recszam	:= EgyebDlg.IndexGrid.Cols[0].IndexOf(IntToStr(Tag));
  	if recszam > -1 then begin
 		index_arr[0]	:= StrToIntDef(EgyebDlg.IndexGrid.Cells[1, recszam],0);
 		index_arr[1]	:= StrToIntDef(EgyebDlg.IndexGrid.Cells[4, recszam],0);
 		index_arr[2]	:= StrToIntDef(EgyebDlg.IndexGrid.Cells[5, recszam],0);
  		ascending		:= StrToBool(EgyebDlg.IndexGrid.Cells[3, recszam]);
      mentettfo		:= EgyebDlg.IndexGrid.Cells[6, recszam];
	end;
	Result 	:= index_arr[0];
end;

function  TJ_FOFORMUJDLG.GetFieldStr(ftag : integer; qu : TAdoQuery) : string;
var
	QueryMezo	: TADOQuery;
	QueryMLabel	: TADOQuery;
	i			: integer;
	tf			: array [0..100] of TField;
	sajatmezok	: boolean;
begin
	Result	            := '';
	QueryMezo		    := TADOQuery.Create(nil);
	QueryMezo.Tag  	    := 0;
	EgyebDlg.SeTADOQueryDatabase(QueryMezo);
	QueryMLabel		    := TADOQuery.Create(nil);
	QueryMLabel.Tag  	:= 0;
	EgyebDlg.SeTADOQueryDatabase(QueryMLabel);
	sajatmezok	:= true;
	Query_Run(QueryMezo, 'SELECT * FROM MEZOK WHERE MZ_DIKOD = '+IntToStr(ftag)+
		' AND MZ_FEKOD = '+EgyebDlg.user_code+' AND MZ_SORSZ = -1 ');
	if QueryMezo.RecordCount > 0 then begin
		// A k�z�s elrendez�st kell haszn�lni
		sajatmezok	:= false;
	end;
	if Query_Run(QueryMezo, 'SELECT * FROM MEZOK WHERE MZ_DIKOD = '+IntToStr(ftag)+
		' AND MZ_FEKOD = '+EgyebDlg.user_code+' AND MZ_SORSZ > -1 '+
		' ORDER BY MZ_SORSZ') then begin
		if ( ( QueryMezo.RecordCount < 1 ) or ( not sajatmezok ) ) then begin
			if Query_Run(QueryMezo, 'SELECT * FROM MEZOK WHERE MZ_DIKOD = '+IntToStr(ftag)+' AND MZ_FEKOD = 0 AND MZ_SORSZ > -1 '+
				' ORDER BY MZ_SORSZ') then begin
				if QueryMezo.RecordCount < 1 then begin
					Exit;
				end;
			end;
		end;
       Query_Run(QueryMLabel, 'SELECT MZ_MZNEV, MZ_LABEL FROM MEZOK WHERE MZ_DIKOD = '+IntToStr(ftag)+' AND MZ_FEKOD = 0 AND MZ_SORSZ > -1 ORDER BY MZ_MZNEV');
		i := 0;
		try
			Qu.Fields.Clear;
		except
		end;
		while not QueryMezo.Eof do begin
//           for i := 0 to QueryMezo.RecordCount - 1 do begin
				if QueryMezo.FieldByName('MZ_TIPUS').AsString = '1' then begin
					tf[i] := TFloatField.Create(Qu);
					if QueryMezo.FieldByName('MZ_FORMA').AsString <> '' then begin
						TFloatField(tf[i]).DisplayFormat 	:= QueryMezo.FieldByName('MZ_FORMA').AsString;
					end;
				end;
				if QueryMezo.FieldByName('MZ_TIPUS').AsString = '0' then begin
					tf[i] := TStringField.Create(Qu);
				end;
				if QueryMezo.FieldByName('MZ_TIPUS').AsString = '5' then begin
					tf[i] := TBooleanField.Create(Qu);
				end;
				if QueryMezo.FieldByName('MZ_TIPUS').AsString = '2'  then begin
					tf[i] := TIntegerField.Create(Qu);
					if QueryMezo.FieldByName('MZ_FORMA').AsString <> '' then begin
						TIntegerField(tf[i]).DisplayFormat 	:= QueryMezo.FieldByName('MZ_FORMA').AsString;
					end;
				end;
				if QueryMezo.FieldByName('MZ_TIPUS').AsString = '4' then begin
					tf[i] := TBCDField.Create(Qu);
					if QueryMezo.FieldByName('MZ_FORMA').AsString <> '' then begin
						TBCDField(tf[i]).DisplayFormat 	:= QueryMezo.FieldByName('MZ_FORMA').AsString;
					end;
				end;
				if QueryMezo.FieldByName('MZ_TIPUS').AsString = '3' then begin
					tf[i] := TDateField.Create(Qu);
				end;
				if QueryMezo.FieldByName('MZ_TIPUS').AsString = '6' then begin
					tf[i] := TDateTimeField.Create(Qu);
				end;
				tf[i].FieldName 		:= QueryMezo.FieldByName('MZ_MZNEV').AsString;
				if QueryMezo.FieldByName('MZ_MKIND').AsString = '0' then begin
					tf[i].FieldKind 		:= fkData;
				end;
				if QueryMezo.FieldByName('MZ_MKIND').AsString = '1' then begin
					tf[i].FieldKind 		:= fkCalculated;
				end;
				tf[i].DataSet 	 		:= Qu;
				tf[i].Name 		 		:= 'TF'+IntToStr(i);
				tf[i].DisplayLabel 		:= QueryMezo.FieldByName('MZ_LABEL').AsString;
               if QueryMLabel.Locate('MZ_MZNEV', QueryMezo.FieldByName('MZ_MZNEV').AsString, [] ) then begin
				    tf[i].DisplayLabel 		:= QueryMLabel.FieldByName('MZ_LABEL').AsString;
               end;

				tf[i].DisplayWidth 		:= StrToIntDef(QueryMezo.FieldByName('MZ_WIDTH').AsString,0);
				tf[i].Size 				:= StrToIntDef(QueryMezo.FieldByName('MZ_MSIZE').AsString,0);
				if QueryMezo.FieldByName('MZ_VISIB').AsString = '0' then begin
					tf[i].Visible 		:= false;
				end else begin
					tf[i].Visible 		:= true;
				end;
				if StrToIntDef(QueryMezo.FieldByName('MZ_HIDEN').AsString, 0 ) = 1 then begin
					tf[i].Visible 		:= false;
				end;
				tf[i].Tag 				:= StrToIntDef(QueryMezo.FieldByName('MZ_MZTAG').AsString,0);
				if QueryMezo.FieldByName('MZ_ALIGN').AsString = '0' then begin
					tf[i].Alignment     := taLeftJustify;
				end;
				if QueryMezo.FieldByName('MZ_ALIGN').AsString = '1' then begin
					tf[i].Alignment     := taCenter;
				end;
				if QueryMezo.FieldByName('MZ_ALIGN').AsString = '2' then begin
					tf[i].Alignment     := taRightJustify;
				end;
			Inc(i);
//           end;
			QueryMezo.Next;
		end;
	end;
	QueryMezo.Free;
end;

procedure TJ_FOFORMUJDLG.ComboTolto(start : integer );
{****************************************************************************************************************}
var
	i : integer;
begin
  // A combobox felt�lt�se
  	ComboBox1.Clear;
   mezonevek.Clear;
	for i := 0 to Query1.FieldCount  - 1 do begin
  // if ( ( Query1.Fields[i].FieldKind = fkData )  and (Query1.Fields[i].Visible) )then begin
		if ( ((Query1.Fields[i].FieldKind = fkData) or (Query1.Fields[i].FieldKind = fkAggregate)) and Query1.Fields[i].Visible) then begin
			ComboBox1.Items.Add(Query1.Fields[i].DisplayLabel);
			mezonevek.Add(Query1.Fields[i].FieldName);
		end;
	end;
	if start < ComboBox1.Items.Count then begin
		ComboBox1.ItemIndex := start;
   end else begin
		ComboBox1.ItemIndex := 0;
   end;
  	// A nem kereshet� mez�k megjel�l�se
	for i := 0 to Query1.FieldCount  - 1 do begin
  		if Query1.Fields[i].FieldKind <> fkData then begin
     		Query1.Fields[i].DisplayLabel	:= '*'+Query1.Fields[i].DisplayLabel;
     	end;
	end;
end;

function TJ_FOFORMUJDLG.ReadFields : boolean;
var
	v_alapstr	: string;
	mezoszam	: integer;
	i			: integer;
	mezonev		: string;
	QueryMezo	: TADOQuery;
	tipus		: string;
	kind		: string;
	sajatmezok	: boolean;
	mezosorszam	: integer;
begin
	// Felt�ltj�k a t�bl�zatot a mez�defin�ci�kkal
	Result				:= false;
	SGFields.RowCount 	:= 1;
	SGFields.Rows[0].Clear;
	QueryMezo		:= TADOQuery.Create(nil);
	QueryMezo.Tag  	:= QUERY_KOZOS_TAG;
	EgyebDlg.SeTADOQueryDatabase(QueryMezo);
	// Megnyitjuk a t�bl�t a mez�kh�z
	v_alapstr	:= 'SELECT TOP 1 * '+copy(alapstr, Pos('FROM', UPPERCASE(alapstr)), 99999 );
	if not Query_Run(Query1, v_alapstr) then begin
		// Az adatb�zis nem MSSQL
		v_alapstr	:= alapstr + ' LIMIT 1' ;    // Pr�b�lkozunk MYSQL-el
		if not Query_Run(Query1, v_alapstr) then begin
			v_alapstr	:= alapstr;				// Lek�rdezz�k a teljes adatt�bl�t
			if not Query_Run(Query1, v_alapstr) then begin
				Exit;
			end;
		end;
	end;
	sajatmezok	:= true;
	Query_Run(QueryMezo, 'SELECT * FROM MEZOK WHERE MZ_DIKOD = '+IntToStr(Tag)+
		' AND MZ_FEKOD = '+EgyebDlg.user_code+' AND MZ_SORSZ = -1 ');
	if QueryMezo.RecordCount > 0 then begin
		// A k�z�s elrendez�st kell haszn�lni
		sajatmezok	:= false;
	end;
	if Query_Run(QueryMezo, 'SELECT * FROM MEZOK WHERE MZ_DIKOD = '+IntToStr(Tag)+' AND MZ_FEKOD = '+EgyebDlg.user_code+' AND MZ_SORSZ > -1 ') then begin
		if ( ( QueryMezo.RecordCount < 1 ) or ( not sajatmezok ) ) then begin
			Query_Run(QueryMezo, 'SELECT * FROM MEZOK WHERE MZ_DIKOD = '+IntToStr(Tag)+' AND MZ_FEKOD = 0 AND MZ_SORSZ > -1 ');
		end;
	end;
	// Beolvassuk az adatokat a Query-b�l
	mezoszam	:= 0;
	CheckList.Clear;
	CheckList2.Clear;
	for i := 0 to Query1.FieldCount - 1 do begin
		SGFields.RowCount	:= mezoszam+1;
		mezonev 			:= Query1.Fields[i].FieldName;
		mezosorszam			:= -1;
		QueryMezo.First;
		while not QueryMezo.Eof do begin
			if mezonev = QueryMezo.FieldByName('MZ_MZNEV').AsString then begin
				// Az adatb�zisb�l olvassuk be a param�tereket
				SGFields.Cells[0,mezoszam] 	:= QueryMezo.FieldByName('MZ_SORSZ').AsString;
				SGFields.Cells[1,mezoszam] 	:= QueryMezo.FieldByName('MZ_MZNEV').AsString;
				SGFields.Cells[2,mezoszam] 	:= QueryMezo.FieldByName('MZ_LABEL').AsString;
				SGFields.Cells[3,mezoszam]	:= QueryMezo.FieldByName('MZ_VISIB').AsString;
				SGFields.Cells[4,mezoszam] 	:= QueryMezo.FieldByName('MZ_TIPUS').AsString;
				SGFields.Cells[5,mezoszam]	:= QueryMezo.FieldByName('MZ_WIDTH').AsString;
				SGFields.Cells[6,mezoszam]	:= QueryMezo.FieldByName('MZ_MZTAG').AsString;
				SGFields.Cells[7,mezoszam]	:= QueryMezo.FieldByName('MZ_MSIZE').AsString;
				SGFields.Cells[8,mezoszam]	:= QueryMezo.FieldByName('MZ_ALIGN').AsString;
				SGFields.Cells[9,mezoszam]	:= QueryMezo.FieldByName('MZ_MKIND').AsString;
				SGFields.Cells[10,mezoszam]	:= QueryMezo.FieldByName('MZ_MEGJE').AsString;
				SGFields.Cells[11,mezoszam]	:= QueryMezo.FieldByName('MZ_HIDEN').AsString;
        SGFields.Cells[12,mezoszam]	:= QueryMezo.FieldByName('MZ_FORMA').AsString;
				if StrToIntDef(QueryMezo.FieldByName('MZ_GROUP').AsString, 0) > 0 then begin
					// A mez� hozz�f�z�se a csoporthoz
					CheckList.Add(QueryMezo.FieldByName('MZ_MZNEV').AsString);
					CheckList2.Add(QueryMezo.FieldByName('MZ_LABEL').AsString);
				end;
				QueryMezo.Last;
				mezosorszam	:= 0;
			end;
			QueryMezo.Next;
		end;
		if mezosorszam < 0 then begin
			SGFields.Cells[0,mezoszam] 	:= IntToStr(mezoszam);
			tipus := '0';
			if Query1.Fields[i] is TFloatField then begin
				tipus := '1';
			end;
			if Query1.Fields[i] is TStringField then begin
				tipus := '0';
			end;
			if Query1.Fields[i] is TBooleanField then begin
				tipus := '5';
			end;
			if Query1.Fields[i] is TIntegerField then begin
				tipus := '2';
			end;
		 // 	if Query1.Fields[i] is TBCDField then begin
     // ... BCD values with more than 4 decimal places or 20 significant digits, you should use TFMTBCDField ...
     	if (Query1.Fields[i] is TBCDField) or (Query1.Fields[i] is TFMTBCDField) then begin
				tipus := '4';
			end;
			if Query1.Fields[i] is TDateField then begin
				tipus := '3';
			end;
			if Query1.Fields[i] is TDateTimeField then begin
				tipus := '6';
			end;
			SGFields.Cells[4,mezoszam] 	:= tipus;
			SGFields.Cells[1,mezoszam] 	:= Query1.Fields[i].FieldName;
			SGFields.Cells[2,mezoszam] 	:= Query1.Fields[i].DisplayLabel;
			SGFields.Cells[5,mezoszam]	:= IntToStr(Query1.Fields[i].DisplayWidth);
			SGFields.Cells[6,mezoszam]	:= IntToStr(Query1.Fields[i].Tag);
			case Query1.Fields[i].Alignment of
				taLeftJustify 	: 	SGFields.Cells[8,mezoszam]	:= '0';
				taCenter      	:	SGFields.Cells[8,mezoszam]	:= '1';
				taRightJustify	: 	SGFields.Cells[8,mezoszam]	:= '2';
			end;
			SGFields.Cells[3,mezoszam]	:= '1';
			SGFields.Cells[7,mezoszam]	:= IntToStr(Query1.Fields[i].Size);
			kind	:= '0';
			if Query1.Fields[i].FieldKind = fkData then begin
				kind	:= '0';
			end;
			if Query1.Fields[i].FieldKind = fkCalculated then begin
				kind	:= '1';
			end;
			SGFields.Cells[9,mezoszam]	:= kind;
		end;
		Inc(mezoszam);
	end;
	// Beolvassuk a kalkul�lt mez�ket
	SGCalcFields.RowCount := 1;
   SGCalcFields.Rows[0].Clear;
	ReadCalcFields;
   for i := 0 to SGCalcFields.RowCount - 1 do begin
       mezonev 			:= SGCalcFields.Cells[1,i];
       if mezonev <> '' then begin
       	SGFields.RowCount	:= mezoszam+1;
           if QueryMezo.Locate('MZ_MZNEV', mezonev, []) then begin
               // Az adatb�zisb�l olvassuk be a param�tereket
				SGFields.Cells[0,mezoszam] 	:= QueryMezo.FieldByName('MZ_SORSZ').AsString;
				SGFields.Cells[1,mezoszam] 	:= QueryMezo.FieldByName('MZ_MZNEV').AsString;
               SGFields.Cells[2,mezoszam] 	:= QueryMezo.FieldByName('MZ_LABEL').AsString;
				SGFields.Cells[3,mezoszam]	:= QueryMezo.FieldByName('MZ_VISIB').AsString;
               SGFields.Cells[4,mezoszam] 	:= QueryMezo.FieldByName('MZ_TIPUS').AsString;
               SGFields.Cells[5,mezoszam]	:= QueryMezo.FieldByName('MZ_WIDTH').AsString;
               SGFields.Cells[6,mezoszam]	:= QueryMezo.FieldByName('MZ_MZTAG').AsString;
               SGFields.Cells[7,mezoszam]	:= QueryMezo.FieldByName('MZ_MSIZE').AsString;
               SGFields.Cells[8,mezoszam]	:= QueryMezo.FieldByName('MZ_ALIGN').AsString;
				SGFields.Cells[9,mezoszam]	:= QueryMezo.FieldByName('MZ_MKIND').AsString;
				SGFields.Cells[10,mezoszam]	:= QueryMezo.FieldByName('MZ_MEGJE').AsString;
				SGFields.Cells[11,mezoszam]	:= QueryMezo.FieldByName('MZ_HIDEN').AsString;
        SGFields.Cells[12,mezoszam]	:= QueryMezo.FieldByName('MZ_FORMA').AsString;
			end else begin
				SGFields.Cells[0,mezoszam] 	:= IntToStr(mezoszam);
				SGFields.Cells[1,mezoszam] 	:= SGCalcFields.Cells[1,i];
				SGFields.Cells[2,mezoszam] 	:= SGCalcFields.Cells[2,i];
				SGFields.Cells[3,mezoszam]	:= SGCalcFields.Cells[3,i];
				SGFields.Cells[4,mezoszam] 	:= SGCalcFields.Cells[4,i];
				SGFields.Cells[5,mezoszam]	:= SGCalcFields.Cells[5,i];
				SGFields.Cells[6,mezoszam]	:= SGCalcFields.Cells[6,i];
				SGFields.Cells[7,mezoszam]	:= SGCalcFields.Cells[7,i];
				SGFields.Cells[8,mezoszam]	:= SGCalcFields.Cells[8,i];
				SGFields.Cells[9,mezoszam]	:= SGCalcFields.Cells[9,i];
			end;
			Inc(mezoszam);
		end;
	 end;
	// Rendezz�k a grid-et a sorsz�mnak megfelel�en
//	SaveGridToFile(SGFields, 'UJMEZOK0.TXT');
	TablaRendezo( SGFields, [0], [1], 0, true );
//	SaveGridToFile(SGFields, 'UJMEZOK.TXT');
	Result	:= true;
	QueryMezo.Destroy;
end;

procedure TJ_FOFORMUJDLG.SetQueryFields;
var
	i			: integer;
	tf			: array [0..100] of TField;
	mezoszam, mezotipus	: integer;

begin
	Query1.Close;
	try
		Query1.Fields.Clear;
	except
	end;
	mezoszam	:= 0;
	while mezoszam  < SGFields.RowCount do begin
		case StrToIntDef(SGFields.Cells[4,mezoszam], -1) of
			0 : tf[i] := TStringField.Create(Query1);
			1 :	tf[i] := TFloatField.Create(Query1);
			2 : tf[i] := TIntegerField.Create(Query1);
			3 : tf[i] := TDateField.Create(Query1);
			4 : tf[i] := TBCDField.Create(Query1);
			5 : tf[i] := TBooleanField.Create(Query1);
			6 : tf[i] := TDateTimeField.Create(Query1);
		end;
		tf[i].FieldName 		:= SGFields.Cells[1,mezoszam];
		if SGFields.Cells[9,mezoszam] = '0' then begin
			tf[i].FieldKind 		:= fkData;
		end;
		if SGFields.Cells[9,mezoszam] = '1' then begin
			tf[i].FieldKind 		:= fkCalculated;
		end;
//                   TFloatField(tf[i]).DisplayFormat 	:= TFloatField(Query1.Fields[i]).DisplayFormat;
//                   TIntegerField(tf[i]).DisplayFormat 	:= TIntegerField(Query1er.Fields[i]).DisplayFormat;
//                   TBCDField(tf[i]).DisplayFormat 	:= TBCDField(Query1er.Fields[i]).DisplayFormat;
    // NagyP modi 20151116
    if SGFields.Cells[12,mezoszam] <> '' then begin
      mezotipus:= StrToIntDef(SGFields.Cells[4,mezoszam], -1);
      if mezotipus=1 then
        TFloatField(tf[i]).DisplayFormat := SGFields.Cells[12,mezoszam];
      if mezotipus=2 then
        TIntegerField(tf[i]).DisplayFormat := SGFields.Cells[12,mezoszam];
      if mezotipus=4 then
        TBCDField(tf[i]).DisplayFormat := SGFields.Cells[12,mezoszam];
      end;
    // end modi
		tf[i].DataSet 	 		:= Query1;
		tf[i].Name 		 		:= 'TF'+IntToStr(mezoszam);
		tf[i].DisplayLabel 		:= SGFields.Cells[2,mezoszam];
		tf[i].DisplayWidth 		:= StrToIntDef(SGFields.Cells[5,mezoszam],0);
		tf[i].Tag 				:= StrToIntDef(SGFields.Cells[6,mezoszam],0);
		if StrToIntDef(SGFields.Cells[4,mezoszam], -1) <> 1 then begin
			// Float eset�n nem lehet megadni m�retet
			tf[i].Size 				:= StrToIntDef(SGFields.Cells[7,mezoszam],0);
		end;
		if SGFields.Cells[3,mezoszam] = '0' then begin
			tf[i].Visible 		:= false;
		end else begin
			tf[i].Visible 		:= true;
		end;
		if StrToIntDef(SGFields.Cells[11,mezoszam],0) = 1 then begin
			tf[i].Visible 		:= false;
		end;
		case StrToIntDef(SGFields.Cells[8,mezoszam], -1)of
			0 : tf[i].Alignment     := taLeftJustify;
			1 : tf[i].Alignment     := taCenter;
			2 : tf[i].Alignment     := taRightJustify;
		end;
		Inc(mezoszam);
	end;
end;

procedure TJ_FOFORMUJDLG.ReadCalcFields ;
begin
	// Itt t�ltj�k be a kalkul�lt mez�ket a t�bl�ba
end;


procedure TJ_FOFORMUJDLG.AddCalcField(nev, lab, visib, tipus, width, mztag, msize, align : string) ;
var
	sorsz	: integer;
begin
   if SGCalcFields.Cells[1,0] <> '' then begin
   	SGCalcFields.RowCount := SGCalcFields.RowCount + 1;
	end;
   if nev <> '' then begin
		sorsz							:= SGCalcFields.RowCount - 1;
       SGCalcFields.Cells[0,sorsz]  	:= IntToStr(sorsz);
       SGCalcFields.Cells[1,sorsz] 	:= UpperCase(nev);
       SGCalcFields.Cells[2,sorsz] 	:= lab;
       SGCalcFields.Cells[3,sorsz]		:= visib;
       SGCalcFields.Cells[4,sorsz] 	:= tipus;
       SGCalcFields.Cells[5,sorsz]		:= width;
       SGCalcFields.Cells[6,sorsz]		:= mztag;
		SGCalcFields.Cells[7,sorsz]		:= msize;
		SGCalcFields.Cells[8,sorsz]		:= align;
		SGCalcFields.Cells[9,sorsz]		:= '1';
		SGCalcFields.Cells[11,sorsz]  	:= '0';
	end;
end;

// Ez az SQL_ToltoFo mindenk�pp friss�ti a Sz�reselem-list�t (a fejl�ces jobbkattint�s list�j�t)
function TJ_FOFORMUJDLG.SQL_ToltoFo(orderstr : string) : boolean;
begin
   // Query1.DisableControls;  2018-01-30
   Datasource1.Enabled:= False;
   // ------------
   SQL_ToltoFoMag(orderstr, True);
   SzuresElemlista_frissit;
   // Query1.EnableControls;
   // Datasource1.Enabled:= True;  // csak a k�vetkez� "SQL_ToltoFoMag" v�g�n kapcsoljuk vissza a t�bl�zatot
   // ------------
   SQL_ToltoFoMag(orderstr, False);
end;

// Ezt a modulon bel�li haszn�latra: ha fejl�ces sz�r�s t�rt�nt, akkor pl. nem kell friss�teni,
// biztosan nem b�v�lt a lista.
function TJ_FOFORMUJDLG.SQL_ToltoFoMag(orderstr : string; SzuresNelkul: boolean = false) : boolean;
{****************************************************************************************************}
var
	regilist	: TStringList;
  ujstr		: string;
  ujstr2		: string;
  v_szurostr,szuro	: string;
begin
	Result 		:= true;
	regilist 	:= TStringList.Create;
	regilist.Assign(Query1.Sql);
  Screen.Cursor	:= crHourGlass;
	try
		ujstr		:= alapstr;
		// Az �j sz�r� be�ll�t�sok bet�tele a keres� stringbe
    if not SzuresNelkul then  // a fejl�c sz�r� list�inak felt�lt�s�hez
  		v_szurostr	:= GetSzuresStr
    else v_szurostr	:= '';
		PanelSzuro.Visible	:= false;
		if v_szurostr <> '' then begin
			PanelSzuro.Visible	:= true;
			PanelSzuro.Caption	:= GetSzuresPanelStr;
			if Pos( 'WHERE', UpperCase(ujstr) ) > 0 then begin
				ujstr := ujstr + ' AND ' +v_szurostr + ' ';
			end else begin
				ujstr := ujstr + ' WHERE ' +v_szurostr + ' ';
			end;
		end;
       // A d�tumsz�r�s hozz�rak�sa
       if ( ( datum_tab <> '' ) and (ComboBox2.ItemIndex <> 0) ) then begin
			if Pos( 'WHERE', UpperCase(ujstr) ) > 0 then begin
				ujstr := ujstr + ' AND '    + datum_mez + ' LIKE '''+ComboBox2.Text+'%''';
			end else begin
				ujstr := ujstr + ' WHERE '  + datum_mez + ' LIKE '''+ComboBox2.Text+'%''';
			end;
       end;
		szuro:= EgyebDlg.GetSzuroStr(IntToStr(tag));
		if EgyebDlg.GetSzuroStr(IntToStr(tag)) <> '' then begin
			ujstr2	:= '';
			if pos ('GROUP BY', UpperCase(ujstr) ) > 0 then begin
				ujstr2	:= ' '+copy(ujstr,pos ('GROUP BY', UpperCase(ujstr) ), 255);
				ujstr		:= copy(ujstr,1, pos ('GROUP BY', UpperCase(ujstr) )-1);
			end;
			if Pos( 'WHERE', UpperCase(ujstr) ) > 0 then begin
				ujstr := ujstr + ' AND ' +EgyebDlg.GetSzuroStr(IntToStr(tag)) + ' ';
			end else begin
               ujstr := ujstr + ' WHERE ' +EgyebDlg.GetSzuroStr(IntToStr(tag)) + ' ';
           end;
           if ujstr2 <>'' then begin
               ujstr := ujstr + ujstr2;
           end;
       end;
  		ujstr2 := ujstr + orderstr;
      //Clipboard.AsText:=ujstr2;
       if not Query_Run(Query1, ujstr2 ) then begin
       	Query_Run(Query1, ujstr );
      end;
     	Query1.First;
  	except
      // 2018-03-02
      on E: Exception do begin
        NoticeKi('A t�bl�zat friss�t�se nem siker�lt: '+E.Message);
        Query1.Sql.Assign(regilist);
        Query1.Open;
        Result := false;
        end;  // on E
  	end;
  Screen.Cursor	:= crDefault;
  // Label3.Caption	:= '('+IntToStr(Query1.RecordCount)+')';
  // Label3.Update;
  DarabszamMutat(False);
 	regilist.Free;
 	Query1.First;
  if not SzuresNelkul then  // csak itt kapcsoljuk vissza a t�bl�zatot
     Datasource1.Enabled:= True;
  UjSqlEllenor;
end;

procedure TJ_FOFORMUJDLG.SzuresElemlista_frissit;
 //-----  elemlista: a t�bl�zatb�l veszi az �rt�kek list�j�t --------
var
  i: integer;
  AdatElem, MyFieldName: string;
begin
   Query1.DisableControls;
   try
     SetLength(SzuresElemlista, SGSzuroMezok.RowCount);
     for i:=0 to SGSzuroMezok.RowCount-1 do begin
        if SzuresElemlista[i]=nil then begin  // ha m�r l�tezik, megtartjuk...
          SzuresElemlista[i]:= TStringList.Create;
          SzuresElemlista[i].Sorted:= True;
          SzuresElemlista[i].duplicates:= dupIgnore;
          end
        else begin
          SzuresElemlista[i].Clear;  // mindig �res list�b�l indulunk, hiszen kikapcsolt sz�r�sek mellett futtatjuk.
          end;
        end;
      with Query1 do begin
          First;
          while not eof do begin
            for i:=0 to SGSzuroMezok.RowCount-1 do
                if (SGSzuroMezok.Cells[0,i] <> '') then begin // ha nem �res
                   MyFieldName:= SGSzuroMezok.Cells[0,i];
                   AdatElem:= FieldByName(MyFieldName).AsString;
                   if SzuresElemlista[i].IndexOf(AdatElem) = -1 then
                       SzuresElemlista[i].Add(AdatElem);
                   end;  // if
            Next;
            end;  // while
         end;  // with

     finally
        Query1.EnableControls;
        end;  // try-finally
end;

{procedure TJ_FOFORMUJDLG.SzuresElemlista_frissit;
 //-----  elemlista: a t�bl�zatb�l veszi az �rt�kek list�j�t --------
var
  i: integer;
begin
   Query1.DisableControls;
   try
     SetLength(SzuresElemlista, SGSzuroMezok.RowCount);
     for i:=0 to SGSzuroMezok.RowCount-1 do begin
        SzuresElemlista[i]:= TStringList.Create;
        SzuresElemlista[i].Sorted:= True;
        SzuresElemlista[i].duplicates:= dupIgnore;
        end;
      with Query1 do begin
          First;
          while not eof do begin
            for i:=0 to SGSzuroMezok.RowCount-1 do
                if (SGSzuroMezok.Cells[0,i] <> '') then  // ha nem �res
                   SzuresElemlista[i].Add(FieldByName(SGSzuroMezok.Cells[0,i]).AsString);
            Next;
            end;  // while
         end;  // with

     finally
        Query1.EnableControls;
        end;  // try-finally
  end;}

procedure TJ_FOFORMUJDLG.DataSource1StateChange(Sender: TObject);
begin
	//	Noticeki('V�ltoz�s!');
end;

procedure TJ_FOFORMUJDLG.Szures(Forras : TADOQuery; alapstr, orderstr : string; dlgtag : integer);
{****************************************************************************************}
begin
	Application.CreateForm(TSzuresDlg, SzuresDlg);
  	SzuresDlg.Tolt(Forras,IntToStr(dlgtag), FixSzuresNev, FixSzuresSQL);
  	SzuresDlg.ShowModal;
  	if not SzuresDlg.kilepes then begin
  		// Van valami sz�r�si felt�tel
		EgyebDlg.SetSzuroStr(IntToStr(dlgtag),SzuresDlg.szuresstr);
		if not SQL_ToltoFoMag(orderstr) then begin    // nem kell sz�r�selem-list�kat friss�teni, biztosan nem b�v�ltek.
			NoticeKi('Hib�s sz�r�si felt�tel!', NOT_MESSAGE);
    	end;
		if (Forras.RecordCount=0) then begin NoticeKi('Nincs megfelel� elem!', NOT_MESSAGE);
   		Szures(Forras, alapstr, orderstr, dlgtag );
    		Exit;
  		end;
  	end;
  	SzuresDlg.Destroy;
end;

procedure TJ_FOFORMUJDLG.DBGrid2MouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  xx	: integer;
	i 	: integer;
  r	: integer;
  Megvolt: boolean;
begin
	if ( ( Button <> mbRight ) or (y > TStringGrid(DbGrid2).RowHeights[0] ) ) then begin
		// Csak jobb klikkel �s fejl�cen
		Exit;
	end;
	DbGrid2.PopupMenu	:= nil;
	xx 		:= x;
	i		:= 0;
	r		:= Dbgrid2.Columns.Count + 1;
	while xx > 0 do begin
		xx 		:= xx - TStringGrid(DbGrid2).ColWidths[i];
		if xx < 0 then begin
			r		:= i;
		end;
		if i = 0 then begin
			i := TStringGrid(DbGrid2).LeftCol;	// Az els� nem fix�lt oszlop
		end else begin
			Inc(i);
		end;
	end;
(*
	if r = 0 then begin
		// A 0. oszlopban nyomtunk jobb klikket, ellen�rizz�k a mindent be�ll�t� dial�gust
		if CheckList.Count > 0 then begin
			// Megmutathatjuk a dial�gust;
			Application.CreateForm(TValszuro2Dlg, Valszuro2Dlg);
			Valszuro2Dlg.Tolt(CheckList, CheckList2);
			Valszuro2Dlg.kelluresszures	:= uresszures;
			Valszuro2Dlg.Left    		:= 10;
			Valszuro2Dlg.Top			:= PanelControl.Height + 10;
			Valszuro2Dlg.fodial			:= Self;
			Valszuro2Dlg.ShowModal;
			Valszuro2Dlg.Destroy;
		end;
	end;
*)
	if  r > 0 then begin
		rkod	:= Query1.FieldByName(FOMEZO).AsString;
		Megvolt:= SzuresValaszto(Dbgrid2.Columns[r-1].FieldName, X, PanelControl.Height + Y);
    if not Megvolt then SzummaMutato(Dbgrid2.Columns[r-1].FieldName, X, PanelControl.Height + Y, Dbgrid2.Fields[r-1]);
	end;
end;

function TJ_FOFORMUJDLG.SzummaMutato(mezonev : string; lx,ly : integer; MyField: TField) : boolean;
var
	sorsz		: integer;
	tizedesek: integer;
	pt1, pt2	: TPoint;
  oszlopszumma: double;
  SikerultASzumma: boolean;
begin
	Result	:= false;
	sorsz	:= SZUMMA_MEZO.IndexOf(UpperCase(mezonev));
	if  sorsz > -1 then begin
		Result	:= true;
      pt1.X	:= lx;
  		pt1.Y	:= ly;
	  	pt2:= ClientToScreen(pt1);
      oszlopszumma:= 0;
      with Query1 do begin
        DisableControls;
        First;
        SikerultASzumma:= True;
        while not eof and SikerultASzumma do begin
          if JoSzam(FieldByName(mezonev).AsString, True) then // lehet �res is
            oszlopszumma:= oszlopszumma + StringSzam(FieldByName(mezonev).AsString)
          else
            SikerultASzumma:= False;
          Next;
          end;  // while
        EnableControls;
        end;  // with
      if (MyField is TFloatField) or (MyField is TBCDField) then tizedesek:=2
      else tizedesek:=0;
      Application.CreateForm(TSzummaMutatDlg, SzummaMutatDlg);
      try
        SzummaMutatDlg.dial_left  	:= pt2.X;
	  	  SzummaMutatDlg.dial_top	:= pt2.y;
        if SikerultASzumma then
          SzummaMutatDlg.Tolt(Trim(SzamString(oszlopszumma, 18, tizedesek, True)))
        else SzummaMutatDlg.Tolt('Nem �sszegezhet�!');
   		  SzummaMutatDlg.ShowModal;
      except
			  SzummaMutatDlg.Destroy;
        end;  // try-except
	end;
end;

function TJ_FOFORMUJDLG.SzuresValaszto(mezonev : string; lx,ly : integer) : boolean;
var
	sorsz		: integer;
	i,si			: integer;
	pt1, pt2	: TPoint;
  elemlista: TStringList;
begin
	Result	:= false;
	sorsz	:= SGSzuroMezok.Cols[0].IndexOf(UpperCase(mezonev));
	if  sorsz > -1 then begin
		Result	:= true;

		Application.CreateForm(TValszuroDlg, ValszuroDlg);
		pt1.X	:= lx;
		pt1.Y	:= ly;
		if PanelSzuro.Visible then begin
			pt1.Y	:= pt1.Y + PanelSzuro.Height;
		end;
		pt2						:= ClientToScreen(pt1);
		ValszuroDlg.dial_left  	:= pt2.X;
		ValszuroDlg.dial_top	:= pt2.y;

    si:=SZUR_MEZO.IndexOf(mezonev);
    if si>=0 then
    begin
      if SZUR_URES[si]='0' then
        uresszures:=False
      else
        uresszures:=True;
    end;

		ValszuroDlg.kelluresszures	:= uresszures;
		ValszuroDlg.kodkilist.Clear;
		for i := 0 to SGSzures.RowCount - 1 do begin
			if SGSzures.Cells[0, i] = UpperCase(mezonev) then begin
				ValszuroDlg.kodkilist.Add(SGSzures.Cells[1, i]);
			end;
		end;
       ValszuroDlg.FillProperties(UpperCase(mezonev), Tag);
    //------------------------------------------------------------------
    // NagyP 2016-06-29
		// ValszuroDlg.Tolt(SGSzuroMezok.Cells[0,sorsz], SGSzuroMezok.Cells[1,sorsz], Query1.Tag);
    //-----  elemlista: a t�bl�zatb�l veszi az �rt�kek list�j�t --------
    {elemlista:= TStringList.Create;
    elemlista.Sorted:= True;
    elemlista.duplicates:= dupIgnore;
    try
      with Query1 do begin
        DisableControls;
        First;
        while not eof do begin
          elemlista.Add(FieldByName(SGSzuroMezok.Cells[0,sorsz]).AsString);
          Next;
          end;  // while
        EnableControls;
        end;  // with}
      // ValszuroDlg.Tolt2(SGSzuroMezok.Cells[0,sorsz], elemlista, 0);
      ValszuroDlg.Tolt2(SGSzuroMezok.Cells[0,sorsz], SzuresElemlista[sorsz], 0);
    {finally
      if elemlista <> nil then elemlista.Free;
      end;  // try-finally
      }
    // ----------------------------------------------------
		ValszuroDlg.mezo_sorsz	:= sorsz;
		ValszuroDlg.ShowModal;
       if ValszuroDlg.voltenter then begin
       	// A sz�r�sek felt�tele a t�bl�zatba
			SzuresToltes(SGSzuroMezok.Cells[0,sorsz],ValszuroDlg.kodkilist);
			SQL_ToltoFo(GetOrderBy(true)); // kell a sz�r�selem-lista friss�t�s, lehet teljesen m�s adathalmaz
			Query1.Locate(FOMEZO, rkod, [] );
       end;
       try
			ValszuroDlg.Destroy;
       except
       end;
	end;
end;

procedure TJ_FOFORMUJDLG.SzuresUrites;
begin
	SGSzures.RowCount  := 1;
	SGSzures.Rows[0].Clear;
end;

procedure TJ_FOFORMUJDLG.SzuresTorles(mezonev : string);
var
	i : integer;
begin
	i := 0;
	while i < SGSzures
  .RowCount  do begin
		if SGSzures.Cells[0, i] = mezonev then begin
			GridTorles(SGSzures, i);
		end else begin
			Inc(i);
		end;
	end;
end;

procedure TJ_FOFORMUJDLG.SzuresToltes(mezonev : string; lista : TStringList);
var
	i 		: integer;
begin
	SzuresTorles(mezonev);
	// A kiv�lasztott elemek berak�sa a sz�r�sbe
	for i := 0 to lista.Count - 1 do begin
		SzuresHozzaadas(mezonev, lista[i]);
	end;
end;

procedure TJ_FOFORMUJDLG.SzuresHozzaadas(mezonev, szures_str : string );
var
	sorsz	: integer;
	vanszur	: boolean;
begin
	// Egy sz�r�si felt�tel hozz�ad�sa a jelenlegi elemekhez
	// El�sz�r ellen�rozz�k a duplik�ci�t
	vanszur	:= false;
	for sorsz	:= 0 to SGSzures.RowCount - 1 do begin
		if ( ( SGSzures.Cells[0, sorsz]	= mezonev ) and ( SGSzures.Cells[1, sorsz]	= szures_str ) ) then begin
			vanszur	:= true;
		end;
	end;
	if vanszur then begin
		Exit;
	end;
	if SGSzures.Cells[0, 0] = '' then begin
		SGSzures.RowCount := 0;
	end else begin
		SGSzures.RowCount := SGSzures.RowCount + 1;
	end;
	SGSzures.Cells[0, SGSzures.RowCount - 1]	:= mezonev;
	SGSzures.Cells[1, SGSzures.RowCount - 1]	:= szures_str;
end;

procedure TJ_FOFORMUJDLG.AddSzuromezo(mezonev, sqlstr : string);
var
	sorsz	: integer;
begin
	sorsz	:= SGSzuromezok.Cols[0].IndexOf(UpperCase(mezonev));
	if sorsz < 0 then begin
		// Nem l�tez� sz�r�mez�, l�tre kell hozni
		sorsz	:= 0;
		if SGSzuromezok.Cells[0, 0] <> '' then begin
			sorsz	:= SGSzuromezok.RowCount;
       end;
		SGSzuromezok.RowCount			:= sorsz + 1;
		SGSzuromezok.Cells[0, sorsz]	:= UpperCase(mezonev);
		SGSzuromezok.Cells[1, sorsz]	:= sqlstr;
	end else begin
		// L�tez� sz�r�mez�, csak lecser�lj�k az sql-t
		SGSzuromezok.Cells[1, sorsz]	:= sqlstr;
	end;
end;

function TJ_FOFORMUJDLG.GetSzuresStr : string;
var
	str		: string;
	menev	: string;
	i		: integer;
	mtipus	: string;
begin
	Result	:= '';
	str		:= '';
	menev	:= '';
	for i 	:= 0 to SGSzures.RowCount - 1 do begin
		if SGSzures.Cells[0, i]  <> '' then begin
			if menev  <> SGSzures.Cells[0, i] then begin
				// Ha az els� elem '' egy sz�mn�l, akkor azt ki kell venni
				menev := SGSzures.Cells[0, i];
				mtipus	:= GetTypename(Query1.FieldByName(menev).DataType);
				if ( ( ( mtipus = 'N' ) or ( mtipus = 'I' ) ) and (SGSzures.Cells[1, i] = '') ) then begin
					menev	:= '';
					Continue;
				end;
				if str <> '' then begin
					str := str + ') AND ';
				end;
				str	:= str + menev + ' IN ( '''+SGSzures.Cells[1, i]+'''';
			end else begin
				str	:= str + ',''' + SGSzures.Cells[1, i] + '''';
           end;
		end;
	end;
	if str  <> '' then begin
		str 	:= str + ') ';
	end;
	Result	:= str;
end;

function TJ_FOFORMUJDLG.GetSzuresPanelStr : string;
var
	str		: string;
	menev	: string;
	i		: integer;
	sorsz	: integer;
	mezonev	: string;
begin
	Result	:= '';
	str		:= '';
	menev	:= '';
	for i 	:= 0 to SGSzures.RowCount - 1 do begin
		if SGSzures.Cells[0, i]  <> '' then begin
			if menev  <> SGSzures.Cells[0, i] then begin
				mezonev	:= '';
				sorsz 	:= SGFields.Cols[1].IndexOf(SGSzures.Cells[0, i]);
				if sorsz > -1 then begin
					mezonev	:= SGFields.Cells[2,sorsz];
				end;
				if str  <> '' then begin
					str 	:= str + ';  ';
				end;
				str		:= str + mezonev + ' = '''+SGSzures.Cells[1, i]+'''';
				menev 	:= SGSzures.Cells[0, i];
			end else begin
				str		:= str + ',''' + SGSzures.Cells[1, i] + '''';
			end;
		end;
	end;
	Result	:= str;
end;

procedure TJ_FOFORMUJDLG.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
	if Action = caMinimize then begin
		Application.Minimize;
	end;
end;

procedure TJ_FOFORMUJDLG.SzuresKiiras;
begin
	// Ki�rjuk a sz�r�si felt�teleket az INI �llom�nyba
	OpenLocalIni(1);
	WriteLocalIni('SZURES', 'SZ_'+IntToStr(TAG)+'SZURES', GridToStr(SGSZures));
	WriteLocalIni('SZURES2', 'SZ_'+IntToStr(TAG)+'SZURES2', GridToStr(EgyebDlg.SzuroGrid));
	CloseLocalIni;
end;

procedure TJ_FOFORMUJDLG.SzuresBeolvasas;
var
	sz_str	: string;
begin
	// Ki�rjuk a sz�r�si felt�teleket az INI �llom�nyba
	OpenLocalIni(1);
	sz_str	:= readLocalIni('SZURES', 'SZ_'+IntToStr(TAG)+'SZURES');
	if sz_str <> '' then begin
		StrToGrid(sz_str, SGSzures);
	end;
{
	sz_str	:= readLocalIni('SZURES2', 'SZ_'+IntToStr(TAG)+'SZURES2');
  sz:=StringReplace(sz_str,'#','',[rfReplaceAll]);
  sz:=StringReplace(sz,'@','',[rfReplaceAll]);
  sz:=StringReplace(sz,IntToStr(TAG),'',[rfReplaceAll]);
//	if sz_str <> '' then begin
	if sz <> '' then begin
		StrToGrid(sz_str, EgyebDlg.SzuroGrid);
  end;
    if sz='' then
      ButtonSzures.Font.Color:=clWindowText
    else
      ButtonSzures.Font.Color:=clRed;
      }
	CloseLocalIni;
end;

procedure TJ_FOFORMUJDLG.BSzuresTorlesClick(Sender: TObject);
begin
	// Let�r�lj�k az �sszes sz�r�si felt�telt
	if NoticeKi('Val�ban t�r�lni k�v�nja az �sszes sz�r�st?', NOT_QUESTION) = 0 then begin
		SzuresUrites;
    if ComboBox3.Items.Count>0 then ComboBox3.ItemIndex:=0;
	end;
	SQL_ToltoFoMag(GetOrderBy(true)); // nem kell sz�r�selem-list�kat friss�teni, az eredeti is sz�r�sek n�lk�l k�sz�lt.
end;

procedure TJ_FOFORMUJDLG.AddSzuromezoRovid(mezonev, tabla : string; uresval: string='0');
begin
	// Sz�r�mez� hozz�ad�sa egyszer�en
	AddSzuromezo(mezonev, 'SELECT DISTINCT '+mezonev+' FROM '+tabla+' ORDER BY '+mezonev);
  SZUR_MEZO.Add(mezonev);
  SZUR_URES.Add(uresval);
end;

procedure TJ_FOFORMUJDLG.AddSzummamezo(mezonev: string);
begin
  SZUMMA_MEZO.Add(mezonev);
end;

procedure TJ_FOFORMUJDLG.SajatFormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
var
  ekezet	: integer;
begin
	EgyebDlg.CtrlGomb   	:= false;
	EgyebDlg.CtrlGomb2	:= false;
	ekezet := Pos(chr(Key),	Chr(186)+chr(222)+chr(192)+chr(191)+chr(219)+chr(187)+Chr(220)+Chr(226)+Chr(221));
	if ekezet > 0 then begin
		Edit1.Text := Edit1.Text + copy('���������', ekezet,1);
		Exit;
	end;
	if ( ( ssCtrl in Shift ) and (key = VK_F11) ) then begin
		EgyebDlg.CtrlGomb := true;
		Exit;
	end;
	if ( ( ssCtrl in Shift ) and (key = VK_F12) ) then begin
		EgyebDlg.CtrlGomb2 := true;
		Exit;
	end;
	EgyebDlg.keresheto	:= true;
	case Key of
	VK_BACK :
		begin
			Edit1.Text := copy(Edit1.Text,1,Length(Edit1.Text)-1);
			Exit;
		end;
	VK_F1 :
		begin
			Exit;
		end;
	VK_DELETE :
		begin
			Edit1.Text := '';
			Exit;
		end;
	32:
     	begin
  			Edit1.Text := Edit1.Text + Chr(Key);
        end;
   48..90:
     	begin
			Edit1.Text := Edit1.Text + Chr(Key);
        end;
   96..105:
     	begin
        	Key := Key - 48;
  			Edit1.Text := Edit1.Text + Chr(Key);
       end;
	111:
     	begin
        	Key := Ord('/');
  			Edit1.Text := Edit1.Text + Chr(Key);
		 end;
	106:
     	begin
        	Key := Ord('*');
  			Edit1.Text := Edit1.Text + Chr(Key);
		 end;
	189,
	109:
		begin
			Key := Ord('-');
  			Edit1.Text := Edit1.Text + Chr(Key);
        end;
	107:
		begin
        	Key := Ord('+');
  			Edit1.Text := Edit1.Text + Chr(Key);
        end;
	110:
     	begin
        	Key := Ord(',');
			Edit1.Text := Edit1.Text + Chr(Key);
		 end;
	190:
		begin
			Key := Ord('.');
			Edit1.Text := Edit1.Text + Chr(Key);
		 end;
	else
		EgyebDlg.keresheto	:= false;		// Nem v�ltozott a keres�s, nem kell keresni!!!
//     	Edit1.Text := Edit1.Text + Chr(Key);
	end;
end;

procedure TJ_FOFORMUJDLG.ButtonNezClick(Sender: TObject);
begin
	if not voltalt then begin
		voltalt 	:= true;
		Exit;
	end;
	if ButtonNez.Enabled then begin
		megtekintes	:= true;
		Adatlap('Adatok megtekint�se',Query1.FieldByName(FOMEZO).AsString);
		megtekintes	:= false;
	end;
end;

procedure TJ_FOFORMUJDLG.Keres (Query : TADOQuery; fname, source : string );
begin
	if ( (KeresoTable <> fname) or (KeresoStr <> source) ) then begin
		try
//			if not Query.Locate(fname,source,[loCaseInsensitive, loPartialKey]) then begin
			if not Query.Locate(fname,KeresChange(mezonevek[ComboBox1.ItemIndex],source ),[loCaseInsensitive, loPartialKey]) then begin
				// Keress�k meg k�zzel
				Query.DisableControls;
				Query.First;
				if Query.FieldByName(fname).DataType in [ ftSmallint, ftInteger, ftWord, ftFloat, ftCurrency, ftBCD ] then begin
					// Sz�m mez�r�l van sz�
					Query.First;
						//while ( ( StringSzam(Query.FieldByName(fname).AsString) > StringSzam(source) ) and (not Query.Eof) ) do begin
					while  (copy( Query.FieldByName(fname).AsString,1,length(source)) <> source ) and (not Query.Eof)  do begin
							Query.Next;
					end;
				{	if copy( Query.FieldByName(fname).AsString,1,length(source)) <> source  then
          begin
              Edit1.Text:=copy(Edit1.Text,1,length(Edit1.Text)-1);
              KeresoTable:='';
              if Edit1.Text<>'' then
                Keres(Query1, mezonevek[ComboBox1.ItemIndex], Edit1.Text) ;
          end
          else
             KeresoStr:=Edit1.Text;
             }
          {

					szam1	:= StringSzam(Query.FieldByName(fname).AsString);
					Query1.Last;
					if StringSzam(Query.FieldByName(fname).AsString) >= szam1 then begin
						// N�vekv� a sorrend
						Query.First;
						//while ( ( StringSzam(Query.FieldByName(fname).AsString) < StringSzam(source) ) and (not Query.Eof) ) do begin
						while ( ( Query.FieldByName(fname).AsString < source ) and (not Query.Eof) ) do begin
							Query.Next;
						end;
						if copy( Query.FieldByName(fname).AsString,1,length(source)) <> source  then
            begin
              Edit1.Text:=copy(Edit1.Text,1,length(Edit1.Text)-1);
              KeresoTable:='';
              if Edit1.Text<>'' then
                Keres(Query1, mezonevek[ComboBox1.ItemIndex], Edit1.Text) ;
            end;
					end else begin
						// Cs�kken� a sorrend
						Query.First;
						//while ( ( StringSzam(Query.FieldByName(fname).AsString) > StringSzam(source) ) and (not Query.Eof) ) do begin
						while  (copy( Query.FieldByName(fname).AsString,1,length(source)) > source ) and (not Query.Eof)  do begin
							Query.Next;
						end;
						if copy( Query.FieldByName(fname).AsString,1,length(source)) <> source  then
            begin
              Edit1.Text:=copy(Edit1.Text,1,length(Edit1.Text)-1);
              KeresoTable:='';
              if Edit1.Text<>'' then
                Keres(Query1, mezonevek[ComboBox1.ItemIndex], Edit1.Text) ;
            end;
					end;     }
				end else begin
				 {	// Sz�veg mez�r�l van sz�
					str1	:= UpperCase(Query.FieldByName(fname).AsString);
					Query1.Last;
					if UpperCase(Query.FieldByName(fname).AsString) >= str1 then begin
						// N�vekv� a sorrend
						Query.First;
						while ( ( UpperCase(Query.FieldByName(fname).AsString) < UpperCase(source)  ) and (not Query.Eof) )do begin
							Query.Next;
						end;
					end else begin
						// Cs�kken� a sorrend
						Query.First;
						while ( ( UpperCase(Query.FieldByName(fname).AsString) > UpperCase(source)  ) and (not Query.Eof) )do begin
							Query.Next;
						end;
					end;  }
              {
					if copy( Query.FieldByName(fname).AsString,1,length(source)) <> source  then
          begin
              Edit1.Text:=copy(Edit1.Text,1,length(Edit1.Text)-1);
              KeresoTable:='';
              if Edit1.Text<>'' then
                Keres(Query1, mezonevek[ComboBox1.ItemIndex], Edit1.Text) ;
          end
          else
             KeresoStr:=Edit1.Text;
             }
   {
        Edit1.Text:=copy(Edit1.Text,1,length(Edit1.Text)-1);
        //keres_str:=Edit1.Text;
        //KeresoStr:=keres_str;
        KeresoTable:='';
        if Edit1.Text<>'' then
        begin
          Keres(Query1, mezonevek[ComboBox1.ItemIndex], Edit1.Text) ;
    			KeresoStr		:= source;
        end;
        }
        //Timer_Keres.OnTimer(self);
				//Query.EnableControls;
        //exit;
				end;
				if copy( Query.FieldByName(fname).AsString,1,length(source)) <> source  then
        begin
         {     Edit1.Text:=copy(Edit1.Text,1,length(Edit1.Text)-1);
              KeresoTable:='';
              if Edit1.Text<>'' then
               // Keres(Query1, mezonevek[ComboBox1.ItemIndex], KeresChange(mezonevek[ComboBox1.ItemIndex], Edit1.Text)) ;
                Keres(Query1, mezonevek[ComboBox1.ItemIndex], Edit1.Text) ;
                }
        end
        else
             KeresoStr:= Edit1.Text;
             //keres_str	:= KeresChange(mezonevek[ComboBox1.ItemIndex], Edit1.Text);

				Query.EnableControls;
        exit;
			end;
			KeresoTable		:= fname;
		 //	KeresoStr		:= source;
		except
			// HA t�l hossz� a sor akkor ne csin�ljon semmit
		end;
				Query.EnableControls;
	end;
end;

procedure TJ_FOFORMUJDLG.FormActivate(Sender: TObject);
begin
(*
	// Megn�zz�k, hogy a Valszuro �l-e?
	if Assigned(ValszuroDlg) then begin
       try
			ValszuroDlg.Destroy;
       except
       end;
   end;
*)
end;

{procedure TJ_FOFORMUJDLG.FormBezaras();
begin
   ButtonKuldClick(ButtonKuld);
end;}

procedure TJ_FOFORMUJDLG.DBGrid2DblClick(Sender: TObject);
begin
	if not voltalt then begin
		voltalt 	:= true;
		Exit;
	end;
  if ButtonKuld.Visible and ButtonKuld.Enabled then begin
    ButtonKuld.OnClick(self);
    exit;
    end
  else begin
    if ButtonMod.Visible and ButtonMod.Enabled then begin
      ButtonMod.OnClick(self);
      end
    else begin
      if ButtonNez.Visible and ButtonNez.Enabled then begin
        ButtonNez.OnClick(self);
        end
      else begin
        keybd_event(VK_MENU,0,0,0);
        keybd_event(ord('M'),0,0,0);
        keybd_event(ord('M'),0,KEYEVENTF_KEYUP,0);
        keybd_event(VK_MENU,0,KEYEVENTF_KEYUP,0);
        end;
     end;
   end;
end;

procedure TJ_FOFORMUJDLG.DBGrid2CellClick(Column: TColumn);
begin
  Edit1.Text:='';
end;

procedure TJ_FOFORMUJDLG.Timer_KeresTimer(Sender: TObject);
begin
  Timer_Keres.Enabled:=False;
  Keres(Query1, mezonevek[ComboBox1.ItemIndex], keres_str)
  //Keres(Query1, mezonevek[ComboBox1.ItemIndex], Edit1.Text)
end;

procedure TJ_FOFORMUJDLG.UjSqlEllenor;
begin
   // Ez itt az SQL ellen�rz�s a F�t�lt� ut�n
end;
procedure TJ_FOFORMUJDLG.ComboBox2Change(Sender: TObject);
begin
	SQL_ToltoFo(GetOrderBy(true)); // �v v�lt�s: kell friss�teni a sz�r�selem-list�kat
end;

procedure TJ_FOFORMUJDLG.BitBtn101Click(Sender: TObject);
var
  szuro: string;
begin
  DBGrid2.SetFocus;
  if Edit2.Text='' then exit;

  szuro:=GridToStr(SGSZures);
  WriteLocalIni('SZURES_'+IntToStr(TAG),Edit2.Text, szuro);
	SzuresBeolvasas2;
  ComboBox3.ItemIndex:= ComboBox3.Items.IndexOf(Edit2.Text)  ;

  ShowMessage('Sz�r�s ment�se k�sz!');
  //Edit2.Text:='';
  DBGrid2.SetFocus;
end;

procedure TJ_FOFORMUJDLG.BitBtn102Click(Sender: TObject);
var
  szoveg: string;
	Ini	: TIniFile  ;
begin
  // T�rl�s az INI-b�l
  DBGrid2.SetFocus;
  if ComboBox3.ItemIndex<1 then exit;
  szoveg:= 'K�ri a sz�r�si sablon t�rl�s�t?'+#13+#10+ComboBox3.Text;
  if (MessageBox(0,PChar(szoveg), '', MB_ICONQUESTION or MB_YESNO or MB_DEFBUTTON2) in [ idNo]) then     exit;

	Ini := TIniFile.Create(EgyebDlg.userini );
  Ini.DeleteKey('SZURES_'+IntToStr(TAG), ComboBox3.Text);
  SzuresUrites;
	SzuresBeolvasas2;
  ComboBox3.ItemIndex:= 0  ;
	SQL_ToltoFoMag(GetOrderBy(true));  // nem kell sz�r�selem-list�kat friss�teni, biztosan nem b�v�ltek.

  ShowMessage('Sz�r�s t�rl�se k�sz!');

  Edit2.Text:='';
  DBGrid2.SetFocus;
end;

procedure TJ_FOFORMUJDLG.ButtonSzuresekClick(Sender: TObject);
begin
  DropMenuDown(ButtonSzuresek, ppmSzuresek);
  cMenuClosed := GetTickCount;
end;

procedure TJ_FOFORMUJDLG.DropMenuDown(Control: TControl; PopupMenu: TPopupMenu);
var
  APoint: TPoint;
begin
  APoint := Control.ClientToScreen(Point(0, Control.ClientHeight));
  PopupMenu.Popup(APoint.X, APoint.Y);
end;

procedure TJ_FOFORMUJDLG.ButtonSzuresekMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if (Button = mbLeft) and not ((cMenuClosed + 100) < GetTickCount) then begin
    ReleaseCapture;
    end; // if
end;

procedure TJ_FOFORMUJDLG.Edit2Enter(Sender: TObject);
begin
  keresheto:=False;
end;

procedure TJ_FOFORMUJDLG.Edit2Exit(Sender: TObject);
begin
  keresheto:=True;
end;

procedure TJ_FOFORMUJDLG.SzuresBeolvasas2;
var
	Ini	: TIniFile  ;
	i		: integer;
  szuronevek: TStrings;
begin
  szuronevek:=TStringList.Create;
  szuronevek.Clear;
	Ini := TIniFile.Create(EgyebDlg.userini );
  Ini.ReadSection('SZURES_'+IntToStr(TAG),szuronevek);
  if szuronevek.Count=0 then
  begin
    ComboBox3.Visible:=False;
    exit;
  end;
  ComboBox3.Clear;
  ComboBox3.Items.Add('*V�lassz sz�r�si sablont*');
  for i:=0 to szuronevek.Count-1 do
  begin
    ComboBox3.Items.Add(szuronevek[i]);
  end;
  ComboBox3.DropDownCount:=szuronevek.Count+1;
  ComboBox3.Visible:=True;
  ComboBox3.ItemIndex:=0;
end;

procedure TJ_FOFORMUJDLG.ComboBox3Change(Sender: TObject);
  var
	sz_str	: string;
begin
  if ComboBox3.ItemIndex<1 then exit;

	SzuresUrites;
	OpenLocalIni(1);
	sz_str	:= readLocalIni('SZURES_'+IntToStr(TAG), ComboBox3.Text);
	if sz_str <> '' then begin
		StrToGrid(sz_str, SGSzures);
		PanelSzuro.Visible	:= true;
		PanelSzuro.Caption	:= GetSzuresPanelStr;
		if not SQL_ToltoFoMag(GetOrderBy(true)) then begin  // nem kell sz�r�selem-list�kat friss�teni, biztosan nem b�v�ltek.
			NoticeKi('Hib�s sz�r�si felt�tel!', NOT_MESSAGE);
    end
    else
      Edit2.Text:=ComboBox3.Text;
	end;
	CloseLocalIni;
  DBGrid2.SetFocus;
end;


procedure TJ_FOFORMUJDLG.DarabszamMutat(const KellAktRekord: boolean);
begin
  if InkrementalisSzures then begin
    Label3.Caption:= NincsDarabszam;
    end
  else begin
    if KellAktRekord then
       Label3.Caption	:= '('+IntToStr(Query1.RecNo)+'/'+IntToStr(Query1.RecordCount)+')'
    else Label3.Caption	:= '('+IntToStr(Query1.RecordCount)+')';
    end;

  // Nem tudjuk, hogy a DBGrid-ben kijel�lt sor a sz�rt recordset-ben h�nyadik...
  // S�t a darabsz�mot is csak akkor tudn�nk, ha v�gigp�rgetn�nk az adatokat, mert az "OnFilterRecord" csak a megjelen�t�skor h�v�dik
  {if KellAktRekord and not(InkrementalisSzures) then
     Label3.Caption	:= '('+IntToStr(Query1.RecNo)+'/'+IntToStr(Query1.RecordCount)+')'
   else
     Label3.Caption	:= '('+IntToStr(sorokszama)+')';
     }


  Label3.Update;
end;


procedure TJ_FOFORMUJDLG.Query1AfterOpen(DataSet: TDataSet);
begin
//
end;

procedure TJ_FOFORMUJDLG.Query1AfterScroll(DataSet: TDataSet);
begin
//
end;

procedure TJ_FOFORMUJDLG.Query1BeforeOpen(DataSet: TDataSet);
begin
   // sorokszama:= 0;
end;

end.

