unit OSzamSorbe2;

interface

uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, J_SQL, DB, DBTables, Grids, StdCtrls, Mask, Buttons,
  Egyeb, ADODB, ExtCtrls, DBGrids;

type
  TOSzamSorbeDlg2 = class(TForm)
    GroupBox1: TGroupBox;
    QFej: TADOQuery;
    QSor: TADOQuery;
    Panel1: TPanel;
    ButtonKuld: TBitBtn;
    ButtonKilep: TBitBtn;
    Panel2: TPanel;
    Label1: TLabel;
    MaskEdit1: TMaskEdit;
    SorGrid: TStringGrid;
    ComboBox1: TComboBox;
    ComboBox2: TComboBox;
    DBGrid1: TDBGrid;
    DSSor: TDataSource;
    procedure FormCreate(Sender: TObject);
    procedure Tolto(kod : string; vevonev : string);
    procedure ButtonKuldClick(Sender: TObject);
    procedure ButtonKilepClick(Sender: TObject);
    procedure SorGridClick(Sender: TObject);
    procedure Kijelol;
    procedure SorGridDrawCell(Sender: TObject; Col, Row: Longint;
      Rect: TRect; State: TGridDrawState);
    procedure ComboBox1Change(Sender: TObject);
  private
		ujkod	: string;
    fejsorrend:string;
  public
		kilep	: boolean;
  end;

var
  OSzamSorbeDlg2: TOSzamSorbeDlg2;

implementation

uses
	Kozos;
{$R *.DFM}

procedure TOSzamSorbeDlg2.FormCreate(Sender: TObject);
begin
  fejsorrend:='SA_UJKOD';
	EgyebDlg.SetADOQueryDatabase(QFej);
	EgyebDlg.SetADOQueryDatabase(QSor);
	SorGrid.RowCount 		:= 2;
	SorGrid.Cells[0,0] 		:= 'Kijel�lt';
  //SorGrid.Canvas.Font.Color:=clBlue;
	SorGrid.Cells[1,0] 		:= 'K�d';
  //SorGrid.Canvas.Font.Color:=clBlack;
	SorGrid.Cells[2,0] 		:= 'J�rat';
	SorGrid.Cells[3,0] 		:= 'Telj.d�t.';
	SorGrid.Cells[4,0] 		:= 'Megjegyz�s';
	SorGrid.Cells[5,0] 		:= 'Nett�';
	SorGrid.Cells[6,0] 		:= '�FA';
	SorGrid.Cells[7,0] 		:= 'V.nem';
	SorGrid.Cells[8,0] 		:= 'V.nett�';
	SorGrid.Cells[9,0] 		:= 'V.�FA';
	SorGrid.ColWidths[0] 	:= 70;
	SorGrid.ColWidths[1] 	:= 60;
	SorGrid.ColWidths[2] 	:= 60;
	SorGrid.ColWidths[3] 	:= 100;
	SorGrid.ColWidths[4] 	:= 300;
	SorGrid.ColWidths[5] 	:= 80;
	SorGrid.ColWidths[6] 	:= 80;
	SorGrid.ColWidths[7] 	:= 80;
	SorGrid.ColWidths[8] 	:= 80;
	SorGrid.ColWidths[9] 	:= 80;
	SetMaskEdits([MaskEdit1]);
end;

procedure TOSzamSorbeDlg2.Tolto(kod : string; vevonev : string);
begin
  SorGrid.RowCount:=2;
	SorGrid.Cells[1,1] := '';
	ujkod	:= kod;
	if ujkod <> '' then begin
		{Bet�ltj�k a megfelel� elemeket}
		MaskEdit1.Text	:= vevonev;
		if Query_Run (QSor, 'SELECT * FROM SZFEJ WHERE SA_VEVONEV = '''+vevonev +
			''' AND SA_FLAG = ''1'' AND ( ( SA_RESZE IS NULL ) OR ( SA_RESZE < 1 ) ) '+
			' AND SA_UJKOD NOT IN (SELECT S2_UJKOD FROM SZSOR2) ' +
			'ORDER BY '+fejsorrend,true) then begin
//			'ORDER BY SA_UJKOD ',true) then begin
			while not QSor.EOF do begin
				if SorGrid.Cells[1, 1] <> '' then begin
				  SorGrid.RowCount := SorGrid.RowCount + 1;
				end;
				SorGrid.Cells[0,SorGrid.RowCount-1] := '';
				SorGrid.Cells[1,SorGrid.RowCount-1] := QSor.FieldByName('SA_UJKOD').AsString;
				SorGrid.Cells[2,SorGrid.RowCount-1] := QSor.FieldByName('SA_JARAT').AsString;
				SorGrid.Cells[3,SorGrid.RowCount-1] := QSor.FieldByName('SA_TEDAT').AsString;
				SorGrid.Cells[4,SorGrid.RowCount-1] := QSor.FieldByName('SA_MEGJ').AsString;
				SorGrid.Cells[5,SorGrid.RowCount-1] := QSor.FieldByName('SA_OSSZEG').AsString;
				SorGrid.Cells[6,SorGrid.RowCount-1] := QSor.FieldByName('SA_AFA').AsString;
				SorGrid.Cells[7,SorGrid.RowCount-1] := QSor.FieldByName('SA_VALNEM').AsString;
				SorGrid.Cells[8,SorGrid.RowCount-1] := QSor.FieldByName('SA_VALOSS').AsString;
				SorGrid.Cells[9,SorGrid.RowCount-1] := QSor.FieldByName('SA_VALAFA').AsString;
				QSor.Next;
			end;
		end;
	end;
	SorGrid.Refresh;
end;

procedure TOSzamSorbeDlg2.ButtonKuldClick(Sender: TObject);
var
	ii 		: integer;
	maxi	: integer;
begin
	{A maxim�lis sor�rt�k meghat�roz�sa}
	Query_Run(QFej, 'SELECT MAX(S2_S2SOR) MAXKOD FROM SZSOR2 WHERE S2_S2KOD = '+ujkod, true);
	if QFej.RecordCount < 1 then begin
		maxi	:= 1;
	end else begin
		maxi := QFej.Fieldbyname('MAXKOD').AsInteger + 1;
	end;
	for ii := 1 to SorGrid.RowCount - 1 do begin
		if ( ( SorGrid.Cells[1,ii] <> '' ) AND ( SorGrid.Cells[0,ii] = '***' ) ) then begin
			// Ellen�rizni kell, hogy a kijel�lt elem szerepel-e m�r m�sik �sszes�tett sz�ml�ban
			Query_Run(QFej, 'SELECT * FROM SZSOR2 WHERE S2_UJKOD = '+SorGrid.Cells[1,ii], true);
			if QFej.RecordCount > 0 then begin
				NoticeKi('A kiv�lasztott sz�mla '+ SorGrid.Cells[1,ii]
					+'m�r szerepel m�s �sszes�tett sz�ml�(k)ban!');
				Continue;
			end;
			Query_Run(QSor, 'INSERT INTO SZSOR2 (S2_S2KOD, S2_S2SOR, S2_UJKOD ) '+
				'VALUES ('+ujkod+', '+IntToStr(maxi)+', '+SorGrid.Cells[1,ii]+' ) ', true);
			Inc(maxi);
			{A SA_RESZE be�ll�t�sa}
			Query_Run(QSor, 'UPDATE SZFEJ SET SA_RESZE = 1 WHERE SA_UJKOD = '+ SorGrid.Cells[1,ii], true);
     	end;
  	end;
	kilep	:= false;
	Close;
end;

procedure TOSzamSorbeDlg2.ButtonKilepClick(Sender: TObject);
begin
	kilep	:= true;
	Close;
end;

procedure TOSzamSorbeDlg2.SorGridClick(Sender: TObject);
begin
	Kijelol;
end;

procedure TOSzamSorbeDlg2.Kijelol;
begin
	if SorGrid.Cells[0, SorGrid.Row] = '' then begin
  		SorGrid.Cells[0, SorGrid.Row] := '***';
  	end else begin
		SorGrid.Cells[0, SorGrid.Row] := '';
  	end;
  	SorGrid.Refresh;
end;

procedure TOSzamSorbeDlg2.SorGridDrawCell(Sender: TObject; Col, Row: Longint;
  Rect: TRect; State: TGridDrawState);
begin
	if Row > 0 then begin
		SorGrid.Canvas.Font.Color	:= clBlack;
     	if SorGrid.Cells[0, Row] = '***' then begin
        	{A rekord ki van jel�lve}
        	SorGrid.Canvas.Brush.Color := clAqua;
     	end else begin
        	SorGrid.Canvas.Brush.Color := clWindow;
     	end;
     	SorGrid.Canvas.Rectangle(Rect.Left,Rect.Top, Rect.Right, Rect.Bottom);
     	SorGrid.Canvas.TextOut(Rect.Left+2,Rect.Top+2,SorGrid.Cells[Col, Row]);
  	end;
end;

procedure TOSzamSorbeDlg2.ComboBox1Change(Sender: TObject);
begin
  fejsorrend:=ComboBox2.Items[ComboBox1.itemindex];
  Tolto(ujkod,MaskEdit1.Text);
end;

end.
