unit TetanarUjfm;

interface

uses
	SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
	StdCtrls, Forms, DBCtrls, DB, DBGrids, DBTables, Grids, ExtCtrls, Buttons,
	ShellApi, Dialogs, J_FOFORMUJ, ADODB, Mask, Menus, Variants;

type
	TTetanarUjFmDlg = class(TJ_FOFORMUJDLG)
	  procedure FormCreate(Sender: TObject);override;
	 procedure Adatlap(cim, kod : string);override;
	 private
	 public
	end;

var
	TetanarUjFmDlg : TTetanarUjFmDlg;

implementation

uses
	Egyeb, J_SQL, Tetanarbe, Kozos;

{$R *.DFM}

procedure TTetanarUjFmDlg.FormCreate(Sender: TObject);
begin
	Inherited FormCreate(Sender);
	AddSzuromezoRovid( 'TT_TIPUS', 'TETANAR' );
	alapstr		:= 'Select * from TETANAR ';
	FelTolto('�j telepi egys�g�r felvitele ', 'Telepi egys�g�r adatok m�dos�t�sa ',
			'Telepi tankol�s egys�g�rak list�ja', 'TT_TTKOD', alapstr,'TETANAR', QUERY_KOZOS_TAG);
	kulcsmezo			:= 'TT_TTKOD';
	nevmezo				:= 'TT_TTKOD';
	width				:= DLG_WIDTH;
	height				:= DLG_HEIGHT;
end;

procedure TTetanarUjFmDlg.Adatlap(cim, kod : string);
begin
	Application.CreateForm(TTetanarbeDlg, AlForm);
	Inherited Adatlap(cim, kod );
end;

end.


