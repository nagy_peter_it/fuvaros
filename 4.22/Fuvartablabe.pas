unit Fuvartablabe;

interface

uses
	SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
	Forms, Dialogs, StdCtrls, Buttons, Mask, DB, DBTables, J_ALFORM,
	TabNotBk, ComCtrls, Grids, ADODB, ExtCtrls, DBGrids, Shellapi, FileCtrl;

type
	TFuvartablabeDlg = class(TJ_AlformDlg)
    Query1: TADOQuery;
    Panel1: TPanel;
    BitElkuld: TBitBtn;
    BitKilep: TBitBtn;
    Label8: TLabel;
    Label1: TLabel;
    Label24: TLabel;
    Label35: TLabel;
    M1: TMaskEdit;
    M2: TMaskEdit;
    M3: TMaskEdit;
    M5: TMaskEdit;
    BitBtn2: TBitBtn;
    CBKateg: TComboBox;
    Label3: TLabel;
    Label31: TLabel;
    M6: TMaskEdit;
    M4: TMaskEdit;
    Label5: TLabel;
	 Label4: TLabel;
    M7: TMaskEdit;
    Label6: TLabel;
    M50: TMaskEdit;
    Label7: TLabel;
    Label9: TLabel;
    cbTOL: TComboBox;
	procedure BitKilepClick(Sender: TObject);
	procedure FormCreate(Sender: TObject);
	procedure Tolto(cim : string; teko : string); override;
	procedure BitElkuldClick(Sender: TObject);
  procedure MaskEditClick(Sender: TObject);
    procedure Modosit(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
	   Shift: TShiftState);
    procedure MaskKeyPress(Sender: TObject; var Key: Char);
    procedure MaskExit(Sender: TObject);
	 procedure BitBtn2Click(Sender: TObject);
    procedure MDatumExit(Sender: TObject);
    procedure cbTOLChange(Sender: TObject);
    procedure cbKategFrissit;
	private
	  modosult 		: boolean;
	  lista_kateg: TStringList;
    lista_tol: TStringList;
	public
	end;

var
	FuvartablabeDlg: TFuvartablabeDlg;


implementation

uses
	Kozos, Egyeb, J_SQL, AlvallalUjFm;

{$R *.DFM}

procedure TFuvartablabeDlg.BitKilepClick(Sender: TObject);
begin
	if modosult then begin
	if NoticeKi('M�dosultak az adatok . Ki akar l�pni ment�s n�lk�l?', NOT_QUESTION ) = 0 then begin
			ret_kod := '';
			Close;
	  end;
  end else begin
		ret_kod := '';
		Close;
  end;
end;

procedure TFuvartablabeDlg.cbTOLChange(Sender: TObject);
begin
   cbKategFrissit;
end;

procedure TFuvartablabeDlg.cbKategFrissit;
begin
   DistinctTolt(CBKateg, 'FUVARTABLA', 'select distinct FT_KATEG from FUVARTABLA where FT_TOL='''+cbTOL.Text+''' order by 1' , lista_kateg, false );
	 CBKateg.ItemIndex  	:= 0;
end;


procedure TFuvartablabeDlg.FormCreate(Sender: TObject);
begin
	EgyebDlg.SetADOQueryDatabase(Query1);
	ret_kod 			:= '';
	modosult 			:= false;
	lista_kateg			:= TStringList.Create;
  lista_tol			:= TStringList.Create;
  DistinctTolt(cbTOL, 'FUVARTABLA', 'select distinct FT_TOL from FUVARTABLA order by 1' , lista_tol, false );
  // felt�lt�se a d�tum kiv�laszt�sa ut�n: m�s-m�s kateg�ri�k vannak az id�szakokban
	// SzotarTolt(CBKateg, '390' , lista_kateg, false, false );
	// CBKateg.ItemIndex  	:= 0;
  cbKategFrissit;
end;

procedure TFuvartablabeDlg.Tolto(cim : string; teko:string);
begin
	// A t�li norm�k nem m�dos�that�ak
	SetMaskEdits([M1]);
	ret_kod 		:= teko;
	Caption 		:= cim;
	M1.Text 		:= teko;
	if ret_kod <> '' then begin		{M�dos�t�s}
		if Query_Run (Query1, 'SELECT * FROM FUVARTABLA WHERE FT_FTKOD = '+IntToStr(StrToIntDef(teko,0)),true) then begin
      ComboSet (cbTOL, 	 Trim(Query1.FieldByName('FT_TOL').AsString), lista_tol);
      cbKategFrissit;  // lista_kateg friss�t�se
			M7.Text    			:= Query1.FieldByName('FT_LANID').AsString;
			M2.Text    			:= Query1.FieldByName('FT_DESCR').AsString;
			M3.Text    			:= Query1.FieldByName('FT_FTPLZ').AsString;
			M4.Text 			:= SzamString(Query1.FieldByName('FT_FTEUR').AsFloat,12,2);
			M5.Text    			:= Query1.FieldByName('FT_VALID').AsString;
			M50.Text    		:= Query1.FieldByName('FT_TRDAY').AsString;
			M6.Text    			:= Query1.FieldByName('FT_MEGJE').AsString;
			ComboSet (CBKateg, 	 Trim(Query1.FieldByName('FT_KATEG').AsString), lista_kateg);
		end;
	end else begin
		{�j felviteln�l gener�ljon egy �j azonos�t�t}
		M1.Text	:= IntToStr(GetNextCode('FUVARTABLA', 'FT_FTKOD'));
	end;
	modosult 	:= false;
end;

procedure TFuvartablabeDlg.BitElkuldClick(Sender: TObject);
begin
	if M2.Text = '' then begin
		NoticeKi('A le�r�s megad�sa k�telez�!');
		M2.Setfocus;
		Exit;
	end;

	if ret_kod = '' then begin
		{�j rekord felvitele}
		M1.Text	:= IntToStr(GetNextCode('FUVARTABLA', 'FT_FTKOD'));
		ret_kod	:= M1.Text;
		Query_Run ( Query1, 'INSERT INTO FUVARTABLA (FT_FTKOD) VALUES (' +M1.Text+ ' )',true);
	end;

	{A r�gi rekord m�dos�t�sa}
	Query_Update (Query1, 'FUVARTABLA',
		[
    'FT_TOL',  ''''+cbTOL.Text+'''',
		'FT_LANID', M7.Text,
		'FT_DESCR', ''''+trim(M2.Text)+'''',
		'FT_FTPLZ', ''''+M3.Text+'''',
		'FT_KATEG', ''''+lista_kateg[CBKateg.ItemIndex]+'''',
		'FT_MEGJE', ''''+M6.Text+'''',
		'FT_TRDAY', IntToStr(StrToIntDef(M50.Text,0)),
		'FT_FTEUR', SqlSzamString(StringSzam(M4.Text),12,2),
		'FT_VALID', ''''+M5.Text+''''
		],' WHERE FT_FTKOD = '+ret_kod);
	Query1.Close;
	FuvarHovaUpdate(ret_kod);
	Close;
end;

procedure TFuvartablabeDlg.MaskEditClick(Sender: TObject);
begin
	with Sender as TMaskEdit do begin
		SelectAll;
	end;
end;

procedure TFuvartablabeDlg.Modosit(Sender: TObject);
begin
	modosult := true;
end;

procedure TFuvartablabeDlg.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
	EgyebDlg.FormReturn(Key);
end;

procedure TFuvartablabeDlg.MaskKeyPress(Sender: TObject; var Key: Char);
begin
	with Sender as TMaskEdit do begin
		Key := EgyebDlg.Jochar(Text,Key,MaxLength,Tag);
	end;
end;

procedure TFuvartablabeDlg.MaskExit(Sender: TObject);
begin
	SzamExit(Sender);
end;

procedure TFuvartablabeDlg.BitBtn2Click(Sender: TObject);
begin
	EgyebDlg.Calendarbe(M5);
end;

procedure TFuvartablabeDlg.MDatumExit(Sender: TObject);
begin
	DatumExit(Sender, false);
end;

end.
