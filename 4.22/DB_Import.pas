unit DB_Import;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DB, ADODB;

type
  TFDB_Import = class(TForm)
    ADOQuery2: TADOQuery;
    ADOQuery2DATABASE_NAME: TWideStringField;
    ADOQuery2DATABASE_SIZE: TIntegerField;
    ADOQuery2REMARKS: TStringField;
    ADOQuery1: TADOQuery;
    Button2: TButton;
    Edit1: TEdit;
    Edit2: TEdit;
    ComboBox1: TComboBox;
    ADOQuery3: TADOQuery;
    ADOQuery3spid: TSmallintField;
    ADOQuery3ecid: TSmallintField;
    ADOQuery3status: TWideStringField;
    ADOQuery3loginame: TWideStringField;
    ADOQuery3hostname: TWideStringField;
    ADOQuery3blk: TStringField;
    ADOQuery3dbname: TWideStringField;
    ADOQuery3cmd: TWideStringField;
    ADOQuery3request_id: TIntegerField;
    ListBox1: TListBox;
    Label1: TLabel;
    Label2: TLabel;
    Button1: TButton;
    ADOConnection1: TADOConnection;
    procedure Button2Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure FormActivate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FDB_Import: TFDB_Import;

implementation

uses Egyeb, Fomenu, DateUtils, Info_;

{$R *.dfm}

procedure TFDB_Import.Button2Click(Sender: TObject);
var
  sql,cel,forras:string;
  tkezd:TDatetime;
  poz,felh:integer;
begin
//  if MessageBox(0, 'K�ri az adatb�zis import�l�s�t?', 'Import', MB_ICONQUESTION or MB_YESNO) = IDNO then
//    exit;
 if MessageBox(0, 'K�ri az adatb�zis import�l�s�t?', 'Import', MB_ICONQUESTION or MB_YESNO or MB_DEFBUTTON2) = IDYES then
 begin

  EgyebDlg.BackUpPath	:= EgyebDlg.Read_SZGrid( '999', '747' );
  EgyebDlg.BackUpPath	:= 'C:\install';
  if EgyebDlg.BackUpPath='' then
  begin
    ListBox1.Items.add('Nincs BackUp c�l k�nyvt�r megadva!');
    ListBox1.Update;
    ShowMessage('Nincs BackUp c�l k�nyvt�r megadva!');
    exit;
  end;

{  EgyebDlg.ADOConnectionAlap.Close;
  EgyebDlg.ADOConnectionKozos.Close;
  EgyebDlg.ADOConnectionKozos.Connected;
  EgyebDlg.ADOConnectionAlap.Connected;
 }
  ADOQuery3.Open;
  ADOQuery3.First;
  felh:=0;
  while not ADOQuery3.Eof do
  begin
    if ADOQuery3dbname.Value=EgyebDlg.v_koz_db then
      inc(felh);
    ADOQuery3.Next;
  end;

  if felh>1 then
  begin
    ListBox1.Items.Add(IntToStr(felh-1)+ 'db felhaszn�l� nyitvatartja a c�l adatb�zist!');
    ListBox1.Update;
    ShowMessage(IntToStr(felh-1)+ 'db felhaszn�l� nyitvatartja a c�l adatb�zist!');
    exit;
  end;

  Button2.Enabled:=False;
  forras:=ComboBox1.Text;
	Screen.Cursor	:= crHourGlass;
  Application.ProcessMessages;
  FomenuDlg.Timer2.Enabled:=False;
 // BackUp
 Try
  tkezd:=now;
  ListBox1.Items.add(forras+' BackUp start: '+timetostr(now));
  ListBox1.Update;
  cel:=EgyebDlg.BackUpPath+'\BackUp_KOZ.bak';
  sql:='BACKUP DATABASE ['+forras+'] TO  DISK = N'''+cel+''' WITH NOFORMAT, INIT,  NAME = N'''+forras+'_backup'+''',COPY_ONLY, SKIP, REWIND, NOUNLOAD, COMPRESSION,  STATS = 10';
//  sql:='BACKUP DATABASE ['+EgyebDlg.import_koz+'] TO  DISK = N'''+cel+''' WITH NOFORMAT, INIT,  NAME = N'''+EgyebDlg.import_dat+'_backup'+''',COPY_ONLY, SKIP, REWIND, NOUNLOAD, COMPRESSION,  STATS = 10';
  ADOQuery1.SQL.Clear;
  ADOQuery1.SQL.Add(sql);
  if forras<>'' then
    ADOQuery1.ExecSQL;
  ListBox1.Items.add(forras+' BackUp end: '+timetostr(now));
  ListBox1.Update;

 // poz:=pos('KOZ',ComboBox1.Text);
  forras:=StringReplace(ComboBox1.Text,'KOZ','DAT',[rfReplaceAll]);
  ListBox1.Items.add(forras+' BackUp start: '+timetostr(now));
  ListBox1.Update;
  cel:=EgyebDlg.BackUpPath+'\BackUp_DAT.bak';
  sql:='BACKUP DATABASE ['+EgyebDlg.import_dat+'] TO  DISK = N'''+cel+''' WITH NOFORMAT, INIT,  NAME = N'''+EgyebDlg.import_dat+'_backup'+''',COPY_ONLY, SKIP, REWIND, NOUNLOAD, COMPRESSION,  STATS = 10';
  ADOQuery1.SQL.Clear;
  ADOQuery1.SQL.Add(sql);
  if forras<>'' then
    ADOQuery1.ExecSQL;
  ListBox1.Items.add(forras+' BackUp end: '+timetostr(now));
  ListBox1.Items.add('BackUp id�tartama: '+IntToStr( SecondsBetween(Now,tkezd))+' sec');
  ListBox1.Items.add('');
  ListBox1.Update;

 Except
  	Screen.Cursor	:= crDefault;
    Application.ProcessMessages;
    ListBox1.Items.Add('Az adatb�zis ment�se nem siker�lt!!!');
    ListBox1.Update;
    ShowMessage('Az adatb�zis ment�se nem siker�lt!');
    Exit;
 end;

 ///////////// Restore
 Try
  ShowMessage('1');
  EgyebDlg.ADOConnectionAlap.Close;
  EgyebDlg.ADOConnectionKozos.Close;
  ShowMessage('2');
  ShowMessage('3');


  tkezd:=now;
  forras:=EgyebDlg.BackUpPath+'\BackUp_KOZ.bak';
  ListBox1.Items.add(EgyebDlg.v_koz_db+' Restore start: '+timetostr(now));
  ListBox1.Update;
  ADOQuery1.SQL.Clear;
  //ADOQuery1.SQL.Add('use master');
  ADOQuery1.SQL.Add('ALTER DATABASE '+EgyebDlg.v_koz_db+' SET SINGLE_USER WITH ROLLBACK IMMEDIATE');
  ADOQuery1.ExecSQL;
//  ADOQuery1.SQL.Add('exec sp_dboption '''+EgyebDlg.v_koz_db+''', 'single user', 'true' ');

  sql:='RESTORE DATABASE '+EgyebDlg.v_koz_db+' FROM  DISK = '''+forras+''' WITH REPLACE';
  ADOQuery1.SQL.Clear;
  ADOQuery1.SQL.Add(sql);
  ADOQuery1.ExecSQL;

  ADOQuery1.SQL.Clear;
  ADOQuery1.SQL.Add('ALTER DATABASE '+EgyebDlg.v_koz_db+' SET MULTI_USER ');
  ADOQuery1.ExecSQL;
  ShowMessage('4');
  ListBox1.Items.add(EgyebDlg.v_koz_db+' Restore end: '+timetostr(now));
  ListBox1.Update;

  forras:=EgyebDlg.BackUpPath+'\BackUp_DAT.bak';
  ListBox1.Items.add(EgyebDlg.v_alap_db+' Restore start: '+timetostr(now));
  ListBox1.Update;
  ADOQuery1.SQL.Clear;
  //ADOQuery1.SQL.Add('use master');
  ADOQuery1.SQL.Add('ALTER DATABASE '+EgyebDlg.v_alap_db+' SET SINGLE_USER WITH ROLLBACK IMMEDIATE');
  sql:='RESTORE DATABASE '+EgyebDlg.v_alap_db+' FROM  DISK = '''+forras+''' WITH REPLACE';
  ADOQuery1.SQL.Add(sql);
  ADOQuery1.SQL.Add('ALTER DATABASE '+EgyebDlg.v_alap_db+' SET MULTI_USER ');
 // ADOQuery1.SQL.Add('use '+EgyebDlg.v_koz_db);
  ADOQuery1.ExecSQL;
  ShowMessage('5');
  ListBox1.Items.add(EgyebDlg.v_alap_db+' Restore end: '+timetostr(now));
  ListBox1.Items.add('Restore id�tartama: '+IntToStr( SecondsBetween(Now,tkezd))+' sec');
  ListBox1.Update;

  ListBox1.Items.add('');
  ListBox1.Items.Add('Az adatb�zis import�l�sa elk�sz�lt.');
  ListBox1.Update;

  Screen.Cursor	:= crDefault;
  Application.ProcessMessages;
 Except
  	Screen.Cursor	:= crDefault;
    Application.ProcessMessages;
    ListBox1.Items.Add('Az adatb�zis visszat�lt�se nem siker�lt!!!');
    ListBox1.Update;
    ShowMessage('Az adatb�zis visszat�lt�se nem siker�lt!'+#13+'A program �jraindul!');
    FRestart:=True;
    close;
    Exit;
 End;
 ShowMessage('A program �jraindul!');
 FRestart:=True;
 close;
 {
 EgyebDlg.ADOConnectionKozos.Open;
 EgyebDlg.ADOConnectionAlap.Open;
 EgyebDlg.SqlInit;
  }
 end;
end;

procedure TFDB_Import.FormCreate(Sender: TObject);
begin
  if pos('teszt',LowerCase( EgyebDlg.v_alap_db))=0 then
  begin
    ShowMessage('Csak TESZT adatb�zisba lehet adatot bet�lteni!');
    exit;
  end;
  Edit1.Text:=EgyebDlg.v_koz_db;
  Edit2.Text:=EgyebDlg.v_alap_db;

  ADOQuery2.ExecSQL;
  ADOQuery2.Open;
  ADOQuery2.First;
  ADOQuery2.RecordCount;
  while not ADOQuery2.Eof do
  begin
    if  (pos('koz',LowerCase(ADOQuery2DATABASE_NAME.Value))>0) and (LowerCase(EgyebDlg.v_koz_db)<>LowerCase(ADOQuery2DATABASE_NAME.Name)) then
    ComboBox1.Items.add(ADOQuery2DATABASE_NAME.Value);
    ADOQuery2.Next;
  end;
  ComboBox1.ItemIndex:=0;
  if ComboBox1.Items.IndexOf(EgyebDlg.import_koz)>-1 then
    ComboBox1.ItemIndex:= ComboBox1.Items.IndexOf(EgyebDlg.import_koz);

end;

procedure TFDB_Import.Button1Click(Sender: TObject);
begin
  close;
end;

procedure TFDB_Import.FormActivate(Sender: TObject);
begin
  Left:=0;
  if FInfo_ <> nil then
    if FInfo_.Visible then FInfo_.Close;
end;

end.
