﻿unit Aki1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls,TLHelp32, DB, ADODB, Math,IniFiles,Clipbrd, ComCtrls,
  IdBaseComponent, IdComponent, IdTCPConnection, IdTCPClient, IdAttachmentFile, IdAttachmentMemory,
  IdMessageClient, IdText, IdSMTP, IdMessage, IdExplicitTLSClientServerBase, IdSMTPBase,
  Rio, SOAPHTTPClient, Mnb, Soap.InvokeRegistry, Nvv.IO.CSV.Delphi.NvvCSVClasses, KozosTipusok,
  System.Win.ScktComp;

type
  TArfolyamRec = record
      ertek: string;
      egyseg: string;
      end;

type
  TForm1 = class(TForm)
    Timer1: TTimer;
    ADOConnectionKozos: TADOConnection;
    ADOConnectionAlap: TADOConnection;
    ADOCommand1: TADOCommand;
    ADOCommand2: TADOCommand;
    ADOCommand3: TADOCommand;
    ADOQuery1: TADOQuery;
    ADOQuery1JA_FAZIS: TBCDField;
    ADOQuery2: TADOQuery;
    ADOQuery2AR_KOD: TIntegerField;
    ADOQuery2AR_EGYSEG: TBCDField;
    ADOQuery2AR_VALNEM: TStringField;
    ADOQuery2AR_ERTEK: TBCDField;
    ADOQuery2AR_DATUM: TStringField;
    ADOQuery2AR_MEGJE: TStringField;
    SQLKozos: TADOCommand;
    ADOQuery3: TADOQuery;
    ADOQuery3max: TIntegerField;
    ADOQuery6: TADOQuery;
    ADOQuery6TT_TTKOD: TIntegerField;
    ADOQuery6TT_EGYAR: TBCDField;
    ADOQuery6TT_DATUM: TStringField;
    ADOQuery6TT_MEGJE: TStringField;
    ADOQuery6TT_TIPUS: TIntegerField;
    Button2: TButton;
    ADOQuery8: TADOQuery;
    ADOQuery8gk_hozza: TIntegerField;
    Szotar: TADOQuery;
    Szotarsz_menev: TStringField;
    Gepkocsi: TADOQuery;
    Gepkocsigk_gkkat: TStringField;
    T_GYUJTOMIND: TADOQuery;
    T_GYUJTOMINDKS_KTKOD: TIntegerField;
    T_GYUJTOMINDKS_JAKOD: TStringField;
    T_GYUJTOMINDKS_DATUM: TStringField;
    T_GYUJTOMINDKS_KMORA: TBCDField;
    T_GYUJTOMINDKS_VALNEM: TStringField;
    T_GYUJTOMINDKS_ARFOLY: TBCDField;
    T_GYUJTOMINDKS_MEGJ: TStringField;
    T_GYUJTOMINDKS_RENDSZ: TStringField;
    T_GYUJTOMINDKS_LEIRAS: TStringField;
    T_GYUJTOMINDKS_UZMENY: TBCDField;
    T_GYUJTOMINDKS_OSSZEG: TBCDField;
    T_GYUJTOMINDKS_TELE: TIntegerField;
    T_GYUJTOMINDKS_SZKOD: TStringField;
    T_GYUJTOMINDKS_AFASZ: TBCDField;
    T_GYUJTOMINDKS_AFAKOD: TStringField;
    T_GYUJTOMINDKS_AFAERT: TBCDField;
    T_GYUJTOMINDKS_ERTEK: TBCDField;
    T_GYUJTOMINDKS_VIKOD: TStringField;
    T_GYUJTOMINDKS_ATLAG: TBCDField;
    T_GYUJTOMINDKS_FIMOD: TStringField;
    T_GYUJTOMINDKS_TETAN: TIntegerField;
    T_GYUJTOMINDKS_TIPUS: TIntegerField;
    T_GYUJTOMINDKS_THELY: TStringField;
    T_GYUJTOMINDKS_TELES: TStringField;
    T_GYUJTOMINDKS_JARAT: TStringField;
    T_GYUJTOMINDKS_SOKOD: TStringField;
    T_GYUJTOMINDKS_SONEV: TStringField;
    T_GYUJTOMINDKS_ORSZA: TStringField;
    T_GYUJTOMINDKS_IDO: TStringField;
    T_GYUJTOMINDKS_DATI: TDateTimeField;
    T_GYUJTOMINDKS_KTIP: TIntegerField;
    T_GYUJTOMINDKS_HMEGJ: TStringField;
    T_GYUJTOMINDKS_FELDAT: TDateTimeField;
    T_JARAT: TADOQuery;
    T_JARATJA_KOD: TStringField;
    T_JARATJA_JARAT: TStringField;
    T_JARATJA_JKEZD: TStringField;
    T_JARATJA_JVEGE: TStringField;
    T_JARATJA_RENDSZ: TStringField;
    T_JARATJA_SAJAT: TIntegerField;
    T_JARATJA_MEGJ: TStringField;
    T_JARATJA_MENSZ: TStringField;
    T_JARATJA_NYITKM: TBCDField;
    T_JARATJA_ZAROKM: TBCDField;
    T_JARATJA_BELKM: TBCDField;
    T_JARATJA_ALKOD: TBCDField;
    T_JARATJA_FAZIS: TBCDField;
    T_JARATJA_OSSZKM: TBCDField;
    T_JARATJA_ALVAL: TIntegerField;
    T_JARATJA_ORSZ: TStringField;
    T_JARATJA_SOFK1: TStringField;
    T_JARATJA_SOFK2: TStringField;
    T_JARATJA_SNEV1: TStringField;
    T_JARATJA_SNEV2: TStringField;
    T_JARATJA_JPOTK: TStringField;
    T_JARATJA_FAZST: TStringField;
    T_JARATJA_KEIDO: TStringField;
    T_JARATJA_VEIDO: TStringField;
    T_JARATJA_LFDAT: TStringField;
    T_JARATJA_SZAK1: TBCDField;
    T_JARATJA_SZAK2: TBCDField;
    T_JARATJA_URESK: TStringField;
    T_JARATJA_EUROS: TStringField;
    T_JARATJA_UBKM: TStringField;
    T_JARATJA_UKKM: TStringField;
    T_JARATJA_JAORA: TIntegerField;
    T_GYUJTO: TADODataSet;
    T_GYUJTOKS_KTKOD: TIntegerField;
    T_GYUJTOKS_JAKOD: TStringField;
    T_GYUJTOKS_DATUM: TStringField;
    T_GYUJTOKS_KMORA: TBCDField;
    T_GYUJTOKS_VALNEM: TStringField;
    T_GYUJTOKS_ARFOLY: TBCDField;
    T_GYUJTOKS_MEGJ: TStringField;
    T_GYUJTOKS_RENDSZ: TStringField;
    T_GYUJTOKS_LEIRAS: TStringField;
    T_GYUJTOKS_UZMENY: TBCDField;
    T_GYUJTOKS_OSSZEG: TBCDField;
    T_GYUJTOKS_TELE: TIntegerField;
    T_GYUJTOKS_SZKOD: TStringField;
    T_GYUJTOKS_AFASZ: TBCDField;
    T_GYUJTOKS_AFAKOD: TStringField;
    T_GYUJTOKS_AFAERT: TBCDField;
    T_GYUJTOKS_ERTEK: TBCDField;
    T_GYUJTOKS_VIKOD: TStringField;
    T_GYUJTOKS_ATLAG: TBCDField;
    T_GYUJTOKS_FIMOD: TStringField;
    T_GYUJTOKS_TETAN: TIntegerField;
    T_GYUJTOKS_TIPUS: TIntegerField;
    T_GYUJTOKS_THELY: TStringField;
    T_GYUJTOKS_TELES: TStringField;
    T_GYUJTOKS_JARAT: TStringField;
    T_GYUJTOKS_SOKOD: TStringField;
    T_GYUJTOKS_SONEV: TStringField;
    T_GYUJTOKS_ORSZA: TStringField;
    T_GYUJTOKS_IDO: TStringField;
    T_GYUJTOKS_DATI: TDateTimeField;
    T_GYUJTOKS_KTIP: TIntegerField;
    T_GYUJTOKS_HMEGJ: TStringField;
    T_GYUJTOKS_FELDAT: TDateTimeField;
    T_JARPOTOS: TADOQuery;
    T_JARPOTOSJP_JAKOD: TStringField;
    T_JARPOTOSJP_POREN: TStringField;
    T_JARPOTOSJP_PORKM: TBCDField;
    T_JARPOTOSJP_PORK2: TBCDField;
    T_JARPOTOSJP_JPKOD: TIntegerField;
    T_JARPOTOSJP_UZEM1: TBCDField;
    T_JARPOTOSJP_UZEM2: TBCDField;
    T_KOLTSEG: TADOQuery;
    Edit1: TEdit;
    Edit2: TEdit;
    NMSMTP1: TIdSMTP;
    Button3: TButton;
    SQLDATE: TADOCommand;
    T_MEGSEGED: TADODataSet;
    T_MEGSEGEDMS_MBKOD: TIntegerField;
    T_MEGSEGEDMS_SORSZ: TIntegerField;
    T_MEGSEGEDMS_FELNEV: TStringField;
    T_MEGSEGEDMS_FELTEL: TStringField;
    T_MEGSEGEDMS_FELCIM: TStringField;
    T_MEGSEGEDMS_FELDAT: TStringField;
    T_MEGSEGEDMS_FELIDO: TStringField;
    T_MEGSEGEDMS_FETDAT: TStringField;
    T_MEGSEGEDMS_FETIDO: TStringField;
    T_MEGSEGEDMS_LERNEV: TStringField;
    T_MEGSEGEDMS_LERTEL: TStringField;
    T_MEGSEGEDMS_LERCIM: TStringField;
    T_MEGSEGEDMS_LERDAT: TStringField;
    T_MEGSEGEDMS_LERIDO: TStringField;
    T_MEGSEGEDMS_LETDAT: TStringField;
    T_MEGSEGEDMS_LETIDO: TStringField;
    T_MEGSEGEDMS_FELARU: TStringField;
    T_MEGSEGEDMS_FSULY: TBCDField;
    T_MEGSEGEDMS_FEPAL: TBCDField;
    T_MEGSEGEDMS_LSULY: TBCDField;
    T_MEGSEGEDMS_LEPAL: TBCDField;
    T_MEGSEGEDMS_FELSE1: TStringField;
    T_MEGSEGEDMS_FELSE2: TStringField;
    T_MEGSEGEDMS_FELSE3: TStringField;
    T_MEGSEGEDMS_FELSE4: TStringField;
    T_MEGSEGEDMS_FELSE5: TStringField;
    T_MEGSEGEDMS_LERSE1: TStringField;
    T_MEGSEGEDMS_LERSE2: TStringField;
    T_MEGSEGEDMS_LERSE3: TStringField;
    T_MEGSEGEDMS_LERSE4: TStringField;
    T_MEGSEGEDMS_LERSE5: TStringField;
    T_MEGSEGEDMS_ORSZA: TStringField;
    T_MEGSEGEDMS_FORSZ: TStringField;
    T_MEGSEGEDMS_TIPUS: TIntegerField;
    T_MEGSEGEDMS_FELIR: TStringField;
    T_MEGSEGEDMS_LELIR: TStringField;
    T_MEGSEGEDMS_DATUM: TStringField;
    T_MEGSEGEDMS_IDOPT: TStringField;
    T_MEGSEGEDMS_MSKOD: TIntegerField;
    T_MEGSEGEDMS_TINEV: TStringField;
    T_MEGSEGEDMS_STATK: TStringField;
    T_MEGSEGEDMS_STATN: TStringField;
    T_MEGSEGEDMS_TEIDO: TStringField;
    T_MEGSEGEDMS_TENNEV: TStringField;
    T_MEGSEGEDMS_TENTEL: TStringField;
    T_MEGSEGEDMS_TENCIM: TStringField;
    T_MEGSEGEDMS_TENYL1: TStringField;
    T_MEGSEGEDMS_TENYL2: TStringField;
    T_MEGSEGEDMS_TENYL3: TStringField;
    T_MEGSEGEDMS_TENYL4: TStringField;
    T_MEGSEGEDMS_TENYL5: TStringField;
    T_MEGSEGEDMS_TENOR: TStringField;
    T_MEGSEGEDMS_TENIR: TStringField;
    T_MEGSEGEDMS_AKTNEV: TStringField;
    T_MEGSEGEDMS_AKTTEL: TStringField;
    T_MEGSEGEDMS_AKTCIM: TStringField;
    T_MEGSEGEDMS_AKTOR: TStringField;
    T_MEGSEGEDMS_AKTIR: TStringField;
    T_MEGSEGEDMS_EXSTR: TStringField;
    T_MEGSEGEDMS_EXPOR: TIntegerField;
    T_MEGSEGEDMS_TEFNEV: TStringField;
    T_MEGSEGEDMS_TEFTEL: TStringField;
    T_MEGSEGEDMS_TEFCIM: TStringField;
    T_MEGSEGEDMS_TEFL1: TStringField;
    T_MEGSEGEDMS_TEFL2: TStringField;
    T_MEGSEGEDMS_TEFL3: TStringField;
    T_MEGSEGEDMS_TEFL4: TStringField;
    T_MEGSEGEDMS_TEFL5: TStringField;
    T_MEGSEGEDMS_TEFOR: TStringField;
    T_MEGSEGEDMS_TEFIR: TStringField;
    T_MEGSEGEDMS_REGIK: TIntegerField;
    T_MEGSEGEDMS_FAJKO: TIntegerField;
    T_MEGSEGEDMS_FAJTA: TStringField;
    T_MEGSEGEDMS_RENSZ: TStringField;
    T_MEGSEGEDMS_POTSZ: TStringField;
    T_MEGSEGEDMS_DOKOD: TStringField;
    T_MEGSEGEDMS_DOKO2: TStringField;
    T_MEGSEGEDMS_GKKAT: TStringField;
    T_MEGSEGEDMS_SNEV1: TStringField;
    T_MEGSEGEDMS_SNEV2: TStringField;
    T_MEGSEGEDMS_KIFON: TIntegerField;
    T_MEGSEGEDMS_FETED: TStringField;
    T_MEGSEGEDMS_FETEI: TStringField;
    T_MEGSEGEDMS_LETED: TStringField;
    T_MEGSEGEDMS_LETEI: TStringField;
    T_MEGSEGEDMS_LDARU: TIntegerField;
    T_MEGSEGEDMS_FDARU: TIntegerField;
    T_MEGSEGEDMS_ID: TAutoIncField;
    T_MEGSEGEDMS_NORID: TIntegerField;
    T_MEGSEGEDMS_FERKDAT: TStringField;
    T_MEGSEGEDMS_LERKDAT: TStringField;
    T_MEGSEGEDMS_FERKIDO: TStringField;
    T_MEGSEGEDMS_LERKIDO: TStringField;
    T_MEGSEGEDMS_FETEI2: TStringField;
    T_MEGSEGEDMS_LETEI2: TStringField;
    T_MEGSEGEDMS_FELIDO2: TStringField;
    T_MEGSEGEDMS_LERIDO2: TStringField;
    T_MEGSEGEDMS_LOADID: TStringField;
    Szotarsz_leiro: TStringField;
    Dolgozo: TADOQuery;
    Dolgozodo_kod: TStringField;
    Dolgozodo_name: TStringField;
    Dolgozodo_email: TStringField;
    T_MEGSEGEDMS_M_MAIL: TDateTimeField;
    T_MEGSEGEDMS_S_MAIL: TDateTimeField;
    Dolgozodo_telef: TStringField;
    T_MEGBIZAS: TADOQuery;
    T_MEGSEGEDALVAL: TStringField;
    T_MEGSEGEDALNEV: TStringField;
    Partner: TADOQuery;
    PartnerV_KOD: TStringField;
    PartnerV_NEV: TStringField;
    PartnerV_IRSZ: TStringField;
    PartnerV_VAROS: TStringField;
    PartnerV_CIM: TStringField;
    PartnerV_TEL: TStringField;
    PartnerV_FAX: TStringField;
    PartnerV_BANK: TStringField;
    PartnerV_ADO: TStringField;
    PartnerV_KEDV: TBCDField;
    PartnerV_UGYINT: TStringField;
    PartnerV_MEGJ: TStringField;
    PartnerV_LEJAR: TIntegerField;
    PartnerV_AFAKO: TStringField;
    PartnerV_NEMET: TIntegerField;
    PartnerV_TENAP: TIntegerField;
    PartnerV_ARNAP: TIntegerField;
    PartnerV_ORSZ: TStringField;
    PartnerVE_VBANK: TStringField;
    PartnerVE_EUADO: TStringField;
    PartnerV_UPTIP: TIntegerField;
    PartnerV_UPERT: TBCDField;
    PartnerVE_ARHIV: TIntegerField;
    PartnerVE_FELIS: TIntegerField;
    PartnerVE_EMAIL: TStringField;
    PartnerVE_POCIM: TStringField;
    PartnerVE_POVAR: TStringField;
    PartnerVE_PORSZ: TStringField;
    PartnerVE_PIRSZ: TStringField;
    PartnerVE_PFLAG: TIntegerField;
    PartnerVE_PNEV1: TStringField;
    PartnerVE_PNEV2: TStringField;
    PartnerVE_PNEV3: TStringField;
    PartnerVE_VEFEJ: TStringField;
    PartnerVE_XMLSZ: TIntegerField;
    PartnerVE_PKELL: TIntegerField;
    PartnerVE_LANID: TIntegerField;
    PartnerVE_FTELJ: TIntegerField;
    PartnerVE_TIMO: TStringField;
    PartnerV_EMAIL: TStringField;
    T_MEGSEGEDALVKOD: TStringField;
    Poziciok: TADOQuery;
    PoziciokPO_RENSZ: TStringField;
    PoziciokPO_GEKOD: TStringField;
    PoziciokPO_DATUM: TStringField;
    PoziciokPO_SEBES: TIntegerField;
    PoziciokPO_DIGIT: TStringField;
    PoziciokPO_SZELE: TBCDField;
    PoziciokPO_HOSZA: TBCDField;
    PoziciokPO_DAT: TDateTimeField;
    PoziciokPO_IRANYSZOG: TSmallintField;
    PoziciokPO_UDAT: TDateTimeField;
    PoziciokPO_DIGIT2: TSmallintField;
    PoziciokPO_ETIPUS: TSmallintField;
    PoziciokPO_VALID: TSmallintField;
    T_MEGSEGEDHUTOS: TIntegerField;
    T_MEGSEGEDFELFUG: TIntegerField;
    Button1: TButton;
    T_MEGSEGEDMS_FETED2: TStringField;
    T_MEGSEGEDMS_LETED2: TStringField;
    T_MEGSEGEDMS_FELDAT2: TStringField;
    T_MEGSEGEDMS_LERDAT2: TStringField;
    T_GYUJTOMINDKS_SOR: TStringField;
    T_GYUJTOMINDKS_GKKAT: TStringField;
    T_GYUJTOMINDKS_NC_KM: TBCDField;
    T_GYUJTOMINDKS_NC_USZ: TBCDField;
    T_GYUJTOMINDKS_NC_UME: TBCDField;
    T_GYUJTOMINDKS_NC_DAT: TStringField;
    T_GYUJTOMINDKS_NC_IDO: TStringField;
    T_MEGSEGEDmb_venev: TStringField;
    btnArfolyamBe: TButton;
    MasikRio: THTTPRIO;
    qUNI: TADOQuery;
    Memo1: TMemo;
    Button4: TButton;
    ADOConnectionArfolyam: TADOConnection;
    Button5: TButton;
    Button6: TButton;
    Query1: TADOQuery;
    qUNIArfolyam: TADOQuery;
    Button7: TButton;
    Query2: TADOQuery;
    QueryMain: TADOQuery;
    Button8: TButton;
    Button9: TButton;
    Button10: TButton;
    Button11: TButton;
    Button12: TButton;
    Button13: TButton;
    Button14: TButton;
    Button15: TButton;
    Button16: TButton;
    ADOConnectionMulti: TADOConnection;
    QueryMulti: TADOQuery;
    QueryMulti2: TADOQuery;
    Button17: TButton;
    Button18: TButton;
    Button19: TButton;
    QueryMain_long: TADOQuery;
    ClientSocket1: TClientSocket;
    procedure Timer1Timer(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure Button3Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure btnArfolyamBeClick(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button6Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Button7Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure Button8Click(Sender: TObject);
    procedure Button9Click(Sender: TObject);
    procedure Button10Click(Sender: TObject);
    procedure Button11Click(Sender: TObject);
    procedure Button12Click(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure Button13Click(Sender: TObject);
    procedure Button14Click(Sender: TObject);
    procedure Button15Click(Sender: TObject);
    procedure Button16Click(Sender: TObject);
    procedure Button17Click(Sender: TObject);
    procedure Button18Click(Sender: TObject);
    procedure Button19Click(Sender: TObject);
  private
    { Private declarations }
    function EllenorzottJarat(kodstr : string; bb: boolean) : integer;
    function ArfolyamErtek( valnem : string; datestr : string; kell : boolean) : double;
    function GetNextCode(Tabla: string): integer;
    function GetNextArfolyamCode: integer;
    function HetvegeVagyUnnep(datum: TDate) : Boolean;
    function GetTelepiAr(dat : string; tipus : integer) : double;
    function Oraatfordulas(rendsz:string): Integer;
    function HutosGk(rendsz:string) : Boolean;
    function DateTimeMinusz(dati: TDateTime; perc: integer) : TDateTime;
    function Szotar_(fo,al,mezo: string): string;
    //function Feltolt(mit,mivel:string;hossz:integer):string;
    function Feltolt(mit: string; mivel: char; hossz: integer): string;
    function Szotar_olvaso(fokod,alkod:string): string;
    procedure ExchangeRatesFromXML(const DBNev: string; const Devizalista: TStringList; const aXML:AnsiString);
    function Query_Select_local( tabla, mezo, keres, output : string) : string;
    function Query_SelectString_local( tabla, SQL: string) : string;
    {function Query_Insert (sourcequery : TADOQuery; maintable : string; mezok : array of string) : boolean;
  	function 	Query_Update (sourcequery : TADOQuery; maintable : string;
						mezok : array of string; wherestr : string) : boolean;
     }
   	function DatumHozNap(dat: string; napok: integer; kellhetvege: boolean): string;
    function LegutobbiArfolyamNap(ActCurr: string): string;  // visszaadja a dátumot
    function LegutobbiArfolyam(ActCurr, ActDate: string): TArfolyamRec;  // visszaadja az árfolyamot
    procedure StoreArfolyam(const DBNev: string; const Devizalista: TStringList; ActDate, ActCurr, ActUnit, ActRate: string);
    procedure StoreArfolyam_core(const DBNev: string; ActDate, ActCurr, ActUnit, ActRate, Megje: string);
    procedure StringParse(forras : string; elvalaszto : string; var mezok : TStringLIst);
    procedure ArfolyamJelent(TimeStamp: string);
    function Munkaido(D: TDateTime): boolean;
    procedure AutoService_GKTorzs_export;
    procedure AutoService_Munkalap_import;
    procedure OpenCSVSor(CSVSor: string);
    function GetCSVElem(i: integer): string;
    function GetSoforSMS_JSONArray(rendszam: string): string;
    function KellMostFuttatni(Feladat: string): boolean;
    procedure Futtatva(Feladat: string);
    procedure KoltsegBeolvas;
    procedure MegbizasFigyel;
    procedure ArfolyamBeolvas;
    procedure GyorshajtasSMS;
    procedure MySMSSzalFrissit;
    function DBConnect : boolean;
    procedure RendkivuliEsemenyErtesites;
    procedure SzakaszFrissites;
    procedure OdaeresBejegyzes;
    procedure IndulasBejegyzes;
    procedure KesesElorejelzes;
    procedure KULCSKiegyenlitesImport;
    procedure SendKesesGyanu(RENDSZAM, CelInfo, KesesInfo, HelyzetInfo, MSID: string);
    function GetMenetiranyitoEmail(RENDSZAM: string): string;
    function GetMegbizasFelelosEmail(MSID: string): string;
    function GetCsoportFelelosEmail(MSID: string): string;
    function GetFelelosEmail(Funkcio: string): TStringList;
    procedure SelectHosszuAtallas;
    procedure MozdulatlanAutok;
    procedure KutasFigyelmeztetes;
    procedure KutasNapiJelentes;
    function GetKutasTartalySzamitottLiter(Tartaly: integer): double;
    function GetKutasTartalySzamitottIdopont(Tartaly: integer): string;
    // function GetKutasTartalyMertLiter(Tartaly: integer): double;
    function GetKutasTartalyMertLiter(const Tartaly: integer; const KutasDat: string): double;
    function GetKutasTartalyMertIdopont(Tartaly: integer): string;
    function GetKozeliMertIdopont(const Tartaly: integer; RefIdopont: string): string;
    procedure Fizetesi_jegyzek_feldolgozas;
    procedure Osztrak_bejelentes_feldolgozas;
    function KuldesSoforMappaba(FN, CelMappa, SoforNeve, login, password: string): string;
    function PDFKuldesSofornekICloud(const FN, LevelFejlec, LevelBody, SoforNeve: string): string;
    procedure Probakuldes;
    procedure ATJELENT_Frissit;
    procedure ATJELENT_Levelek;
    procedure SMSEagle_outbox_check;
    procedure HecPollImport;
    function VCARDExport_All: string;
    procedure EDI_ALL_Export;
    procedure SzamlaszamEllenor;
    procedure MegbizasDebug;
    procedure KilepBelepErtesitoAll;
    procedure KilepBelepErtesito (const SQL, Mode: string);
    // procedure WriteVCARD_OneSet(const CelFolder, FileName, Cegnev, DOKODList, VEVOKODList: string);
    // procedure WriteVCARD_Egyeb(const CelFolder, FileName: string);
    // procedure Futtatando;
    procedure PozicioKuldesFigyelo;
    procedure PozicioKuldesFeltolt;
  public
    { Public declarations }
    function KillTask(ExeFileName: string): Integer;
    function FindProcess(Name: string) : integer;
    function Feldolgozas(KTIP: integer): integer;
    function WriteAppLog(Text:string):Boolean;
    function WriteLogFile(FName:string; Text:string):Boolean;
    procedure EnableTimer;
  end;

const
  sortores = '<br>';  // HTML sortörés

var
  Form1: TForm1;
  ProgramMode: string;
  INI: TIniFile;
  globalini, logfile, koltseglogfile, arflogfn, munkalaplogfn, gyorshajtaslogfn, Arfolyam_DBLista, EXEPath: string;
  rendkivesemlogfn, odaereslogfn, keseslogfn, kulcslogfn, ediexportlogfn,  mozdulatlanlogfn, kutaslogfn, atallaslogfn: string;
  fizetesilogfn, osztraklogfn, icloudlogfn, ICLOUD_NEV, atjelentlogfn, smseaglelogfn, vcardlogfn, megbizasdebuglogfn: string;
  kilepbeleplogfn, szamlakodlogfn, szakaszlogfn, pozikuldeslogfn: string;
  arfolyam_mail_cim, arfolyam_mail_devizak: string;
  kuldendo_devizak, ArfolyamJelentesSzoveg: TStringList;
  tdb, MunkaIdoKezdete, MunkaIdoVege, ODAERES_HATAR_METERBEN: integer;
  Atallas_KMLimit, Atallas_NapLimit: string;
  KILEPBELEP_KEZDONAP, KILEPBELEP_CSATOLTPDFDIR, KILEPBELEP_CSATOLTPDFNEV: string;
  F0,F1,F2,F3,F4,F5,F6,F7: boolean;
  SoforSMS_Enabled, GyorshajtasSMS_Enabled, Odaeres_update_enabled: boolean;
  MaiDatum, PeldanyAzonosito: string;
  CSVReader: TnvvCSVStringReader;
  // LevelMellekletLista: TMemoryStreamPointerArray;  // TStringList;
  // LevelMellekletNevek, LevelMellekletTipusok: TStringList;
  ReszletesNaplo: integer;

implementation

uses DateUtils, StrUtils, IDOMViewU, IOUtils, SMSKuld, J_SQL, Egyeb, Kozos, KulcsAnalitikaOlvaso,
     Kutallapot, GeoRoutines, SMSEagle, TankTranOlvaso, VCardRoutines, ShellAPI, EDIFACT_Export,
     FuvarosDatum, Email, RESTAPI_Kozos;

{$R *.dfm}

function TForm1.Oraatfordulas(rendsz:string): integer;
begin
  Result:=0;
  ADOQuery8.Close;
  ADOQuery8.Parameters[0].Value:=rendsz;
  ADOQuery8.Open;
  ADOQuery8.First;
  Result:=ADOQuery8gk_hozza.Value;
  ADOQuery8.Close;
end;

procedure TForm1.Timer1Timer(Sender: TObject);
begin
 Try
  Timer1.Enabled:= False;


    // három külön időzített folyamatban háromféle módban indulhat a program.
    // 3 percenként futtatjuk a gyors reakciót igénylő, de röviden lekezelhető folyamatokat.
    // 17 percenként futtatjuk a tovább tartó, lassú rekcióidejű folyamatokat.
    // naponta futtatjuk a még lustább dolgokat

     // ------------------  HOSSZÚ ----------------
    WriteLogFile(logfile, formatdatetime('yyyy.mm.dd. HH:mm:ss:zzz', Now)+' Mód: '+ ProgramMode);
    if ((ProgramMode='hosszu') or (ProgramMode='')) then begin
      if INI.ReadString('FUNKCIO-BEKAPCS','KoltsegBeolvas','be')='be' then begin
          KoltsegBeolvas;
          WriteLogFile(logfile, formatdatetime('yyyy.mm.dd. HH:mm:ss:zzz', Now)+' KoltsegBeolvas OK');
          end;
      end;
    if ((ProgramMode='hosszu') or (ProgramMode='')) then begin
      if INI.ReadString('FUNKCIO-BEKAPCS','MegbizasFigyel','be')='be' then begin
        MegbizasFigyel;
        WriteLogFile(logfile, formatdatetime('yyyy.mm.dd. HH:mm:ss:zzz', Now)+' MegbizasFigyel OK');
        end;
      end;
    if ((ProgramMode='hosszu') or (ProgramMode='')) then begin
      if INI.ReadString('FUNKCIO-BEKAPCS','AutoService_GKTorzs_export','be')='be' then begin
          AutoService_GKTorzs_export;
          WriteLogFile(logfile, formatdatetime('yyyy.mm.dd. HH:mm:ss:zzz', Now)+' AutoService_GKTorzs_export OK');
          end;
      end;
    if ((ProgramMode='hosszu') or (ProgramMode='')) then begin
      if INI.ReadString('FUNKCIO-BEKAPCS','AutoService_Munkalap_import','be')='be' then begin
          AutoService_Munkalap_import;
          WriteLogFile(logfile, formatdatetime('yyyy.mm.dd. HH:mm:ss:zzz', Now)+' AutoService_Munkalap_import OK');
          end;
      end;
    if ((ProgramMode='hosszu') or (ProgramMode='')) then begin
      if INI.ReadString('FUNKCIO-BEKAPCS','RendkivuliEsemenyErtesites','be')='be' then begin
          WriteLogFile(logfile, formatdatetime('yyyy.mm.dd. HH:mm:ss:zzz', Now)+' RendkivuliEsemenyErtesites indul');
          RendkivuliEsemenyErtesites;
          WriteLogFile(logfile, formatdatetime('yyyy.mm.dd. HH:mm:ss:zzz', Now)+' RendkivuliEsemenyErtesites OK');
          end;
      end;
    if ((ProgramMode='hosszu') or (ProgramMode='')) then begin
      if INI.ReadString('FUNKCIO-BEKAPCS','HecPollImport','be')='be' then begin
          WriteLogFile(logfile, formatdatetime('yyyy.mm.dd. HH:mm:ss:zzz', Now)+' HecPollImport indul');
          HecPollImport;
          WriteLogFile(logfile, formatdatetime('yyyy.mm.dd. HH:mm:ss:zzz', Now)+' HecPollImport OK');
          end;
      end;
    if ((ProgramMode='hosszu') or (ProgramMode='')) then begin
      if INI.ReadString('FUNKCIO-BEKAPCS','KutasFigyelmeztetes','be')='be' then begin
          WriteLogFile(logfile, formatdatetime('yyyy.mm.dd. HH:mm:ss:zzz', Now)+' KutasFigyelmeztetes indul');
          KutasFigyelmeztetes;
          WriteLogFile(logfile, formatdatetime('yyyy.mm.dd. HH:mm:ss:zzz', Now)+' KutasFigyelmeztetes OK');
          end;
      end;
    if ((ProgramMode='hosszu') or (ProgramMode='')) then begin
      if INI.ReadString('FUNKCIO-BEKAPCS','KutasNapiJelentes','be')='be' then begin
          WriteLogFile(logfile, formatdatetime('yyyy.mm.dd. HH:mm:ss:zzz', Now)+' KutasNapiJelentes indul');
          KutasNapiJelentes;
          WriteLogFile(logfile, formatdatetime('yyyy.mm.dd. HH:mm:ss:zzz', Now)+' KutasNapiJelentes OK');
          end;
      end;
    {if ((ProgramMode='hosszu') or (ProgramMode='')) then begin
      ATJELENT_Frissit;
      WriteLogFile(logfile, formatdatetime('yyyy.mm.dd. HH:mm:ss:zzz', Now)+' ATJELENT_Frissit OK');
      end;}
    if ((ProgramMode='hosszu') or (ProgramMode='')) then begin
      if INI.ReadString('FUNKCIO-BEKAPCS','SMSEagle_outbox_check','be')='be' then begin
          WriteLogFile(logfile, formatdatetime('yyyy.mm.dd. HH:mm:ss:zzz', Now)+' SMSEagle_outbox_check indul');
          SMSEagle_outbox_check;
          WriteLogFile(logfile, formatdatetime('yyyy.mm.dd. HH:mm:ss:zzz', Now)+' SMSEagle_outbox_check OK');
          end;
      end;
     if ((ProgramMode='hosszu') or (ProgramMode='')) then begin
      if INI.ReadString('FUNKCIO-BEKAPCS','VCARDExport_All','be')='be' then begin
        WriteLogFile(logfile, formatdatetime('yyyy.mm.dd. HH:mm:ss:zzz', Now)+' VCARDExport_All indul');
        VCARDExport_All;
        WriteLogFile(logfile, formatdatetime('yyyy.mm.dd. HH:mm:ss:zzz', Now)+' VCARDExport_All OK');
        end;
      end;

     if ((ProgramMode='hosszu') or (ProgramMode='')) then begin
      if INI.ReadString('FUNKCIO-BEKAPCS','ArfolyamBeolvas','be')='be' then begin
        WriteLogFile(logfile, formatdatetime('yyyy.mm.dd. HH:mm:ss:zzz', Now)+' ArfolyamBeolvas indul');
        ArfolyamBeolvas;
        WriteLogFile(logfile, formatdatetime('yyyy.mm.dd. HH:mm:ss:zzz', Now)+' ArfolyamBeolvas OK');
        end;
      end;

     if ((ProgramMode='hosszu') or (ProgramMode='')) then begin
      if INI.ReadString('FUNKCIO-BEKAPCS','MegbizasDebug','be')='be' then begin
        WriteLogFile(logfile, formatdatetime('yyyy.mm.dd. HH:mm:ss:zzz', Now)+' MegbizasDebug indul');
        MegbizasDebug;
        WriteLogFile(logfile, formatdatetime('yyyy.mm.dd. HH:mm:ss:zzz', Now)+' MegbizasDebug OK');
        end;
      end;

     if ((ProgramMode='hosszu') or (ProgramMode='')) then begin
      if INI.ReadString('FUNKCIO-BEKAPCS','KilepBelepErtesitoAll','be')='be' then begin
        WriteLogFile(logfile, formatdatetime('yyyy.mm.dd. HH:mm:ss:zzz', Now)+' KilepBelepErtesitoAll indul');
        KilepBelepErtesitoAll;
        WriteLogFile(logfile, formatdatetime('yyyy.mm.dd. HH:mm:ss:zzz', Now)+' KilepBelepErtesitoAll OK');
        end;
      end;

     if ((ProgramMode='hosszu') or (ProgramMode='')) then begin
      if INI.ReadString('FUNKCIO-BEKAPCS','EDI_ALL_Export','be')='be' then begin
        WriteLogFile(logfile, formatdatetime('yyyy.mm.dd. HH:mm:ss:zzz', Now)+' EDI_ALL_Export indul');
        EDI_ALL_Export;
        WriteLogFile(logfile, formatdatetime('yyyy.mm.dd. HH:mm:ss:zzz', Now)+' EDI_ALL_Export OK');
        end;
      end;

     if ((ProgramMode='hosszu') or (ProgramMode='')) then begin
      if INI.ReadString('FUNKCIO-BEKAPCS','GyorshajtasSMS','be')='be' then begin
        WriteLogFile(logfile, formatdatetime('yyyy.mm.dd. HH:mm:ss:zzz', Now)+' GyorshajtasSMS indul');
        GyorshajtasSMS;
        WriteLogFile(logfile, formatdatetime('yyyy.mm.dd. HH:mm:ss:zzz', Now)+' GyorshajtasSMS OK');
        end;
      end;

     if ((ProgramMode='hosszu') or (ProgramMode='')) then begin
      if INI.ReadString('FUNKCIO-BEKAPCS','KesesElorejelzes','be')='be' then begin
        WriteLogFile(logfile, formatdatetime('yyyy.mm.dd. HH:mm:ss:zzz', Now)+' KesesElorejelzes indul');
        KesesElorejelzes;
        WriteLogFile(logfile, formatdatetime('yyyy.mm.dd. HH:mm:ss:zzz', Now)+' KesesElorejelzes OK');
        end;
      end;

     if ((ProgramMode='hosszu') or (ProgramMode='')) then begin
      if INI.ReadString('FUNKCIO-BEKAPCS','PozicioKuldesFeltolt','be')='be' then begin
        WriteLogFile(logfile, formatdatetime('yyyy.mm.dd. HH:mm:ss:zzz', Now)+' PozicioKuldesFeltolt indul');
        PozicioKuldesFeltolt;
        WriteLogFile(logfile, formatdatetime('yyyy.mm.dd. HH:mm:ss:zzz', Now)+' PozicioKuldesFeltolt OK');
        end;
      end;

    // ------------------  GYORS ----------------
     // Ezeknek így egymás után van értelme: frissítjük a szakaszokat, bejegyezzük ha odaért, és utána nézzük, hogy késik-e
     if ((ProgramMode='gyors') or (ProgramMode='')) then begin
      if INI.ReadString('FUNKCIO-BEKAPCS','SzakaszFrissites','be')='be' then begin
        SzakaszFrissites;
        WriteLogFile(logfile, formatdatetime('yyyy.mm.dd. HH:mm:ss:zzz', Now)+' SzakaszFrissites OK');
        end;
      end;

     if ((ProgramMode='gyors') or (ProgramMode='')) then begin
      if INI.ReadString('FUNKCIO-BEKAPCS','OdaeresBejegyzes','be')='be' then begin
        OdaeresBejegyzes;
        IndulasBejegyzes;  // ugyanabba logol
        WriteLogFile(logfile, formatdatetime('yyyy.mm.dd. HH:mm:ss:zzz', Now)+' OdaeresBejegyzes OK');
        end;
      end;

     if ((ProgramMode='gyors') or (ProgramMode='')) then begin
      if INI.ReadString('FUNKCIO-BEKAPCS','PozicioKuldesFigyelo','be')='be' then begin
        PozicioKuldesFigyelo;
        WriteLogFile(logfile, formatdatetime('yyyy.mm.dd. HH:mm:ss:zzz', Now)+' PozicioKuldesFigyelo OK');
        end;
      end;

    // ------------------  NAPI ----------------
    if ((ProgramMode='napi') or (ProgramMode='')) then begin
      if INI.ReadString('FUNKCIO-BEKAPCS','KULCSKiegyenlitesImport','be')='be' then begin
        KULCSKiegyenlitesImport;
        WriteLogFile(logfile, formatdatetime('yyyy.mm.dd. HH:mm:ss:zzz', Now)+' KULCSKiegyenlitesImport OK');
        end;
      end;
    if ((ProgramMode='napi') or (ProgramMode='')) then begin
      if INI.ReadString('FUNKCIO-BEKAPCS','MozdulatlanAutok','be')='be' then begin
        MozdulatlanAutok;
        WriteLogFile(logfile, formatdatetime('yyyy.mm.dd. HH:mm:ss:zzz', Now)+' MozdulatlanAutok OK');
        end;
      end;
    if ((ProgramMode='napi') or (ProgramMode='')) then begin
      if INI.ReadString('FUNKCIO-BEKAPCS','SelectHosszuAtallas','be')='be' then begin
        SelectHosszuAtallas;
        WriteLogFile(logfile, formatdatetime('yyyy.mm.dd. HH:mm:ss:zzz', Now)+' SelectHosszuAtallas OK');
        end;
      end;
    if ((ProgramMode='napi') or (ProgramMode='')) then begin
      if INI.ReadString('FUNKCIO-BEKAPCS','SzamlaszamEllenor','be')='be' then begin
        SzamlaszamEllenor;
        WriteLogFile(logfile, formatdatetime('yyyy.mm.dd. HH:mm:ss:zzz', Now)+' SzamlaszamEllenor OK');
        end;
      end;

  WriteLogFile(logfile, formatdatetime('yyyy.mm.dd. HH:mm:ss:zzz', Now)+' Program végzett.');
  WriteLogFile(logfile, '----------------------------------------------------');
 Finally
   ADOConnectionKozos.Close;
   ADOConnectionAlap.Close;
   ADOConnectionArfolyam.Close;
   Close;
   End;  // try-finally

end;

// -------- kiiktatva! -------- //
function TForm1.KellMostFuttatni(Feladat: string): boolean;
var
  S: string;
begin
{
 try
  with Query1 do begin
    Close; SQL.Clear;
    S:='select UT_FELADAT from UTEMEZETT where UT_FELADAT= '''+Feladat+''' '+
      ' and ((GETDATE() >= DATEADD(MINUTE, UT_HANYPERC, UT_LEGUTOBB)) or (UT_LEGUTOBB is null))';
    SQL.Add(S);
    Open;
    Result:= not Eof;
    Close;
    end;  // with
  except
    on E: Exception do begin
      WriteLogFile(logfile, formatdatetime('yyyy.mm.dd. HH:mm:ss:zzz', Now)+' "KellMostFuttatni" hiba: '+E.Message);
      Result:= True;
      end;
    end; // try-except
    }
 Result:= True;

end;

// -------- kiiktatva! -------- //
procedure TForm1.Futtatva(Feladat: string);
var
  S: string;
begin
 {
  S:='update UTEMEZETT set UT_LEGUTOBB=GETDATE() where UT_FELADAT= '''+Feladat+'''';
  Query_run(Query1, S, False);
  }
end;

procedure TForm1.FormCreate(Sender: TObject);
var
  Peldanyszam: integer;
  // AppCommandLine: string;
  S: string;
begin

  // AppCommandLine:=GetCommandLine;
  // ProgramMode:=GetListOp(AppCommandLine, 1, ' ');  // lehet "gyors" vagy "hosszu"

  // for i := 0 to ParamCount do
  //  ShowMessage('Parameter '+IntToStr(i)+' = '+paramstr(i));
  ProgramMode:= '';  // alapértelmezett érték
  if ParamCount>=1 then begin  // ha egyáltalán van paraméter
    ProgramMode:= trim(paramstr(1));  //
    if copy(ProgramMode,1,1)='-' then begin  // csak ha a várt formátum, azaz "-"-val kezdődik
      ProgramMode:=copy(ProgramMode,2,99999);
      end;
    end;

  PeldanyAzonosito:=FormatDateTime('HHmmss.zzz', Now);
  SysUtils.FormatSettings.DateSeparator    :='.';
  SysUtils.FormatSettings.ShortDateFormat  := 'yyyy.MM.dd';
  SysUtils.FormatSettings.TimeSeparator    :=':';
  SysUtils.FormatSettings.ShortTimeFormat  :='hh:mm';
  SysUtils.FormatSettings.LongTimeFormat   :='hh:mm:ss';
  EXEPath 		:= ExtractFilePath(Application.ExeName);
	// globalini		:= ExtractFilePath( Application.ExeName)+ChangeFileExt( ExtractFileName(Application.ExeName), '.INI' );
  globalini		:= EXEPath+'INI\'+ChangeFileExt( ExtractFileName(Application.ExeName), '.INI' );
	// logfile		:= ExtractFilePath( Application.ExeName)+ChangeFileExt( ExtractFileName(Application.ExeName), '.log' );
	logfile		:= EXEPath+'LOG\'+ChangeFileExt( ExtractFileName(Application.ExeName), '.log' );
  WriteLogFile(logfile, formatdatetime('yyyy.mm.dd. HH:mm:ss:zzz', Now)+' Program indul, .ini = '+globalini);
  //AUTOMATA    := ParamStr(1)='AUTO';
  DBConnect;

	Ini:= TIniFile.Create( globalini );

  // DocumentPath:= IncludeTrailingPathDelimiter(Ini.ReadString( 'GENERAL', 'DOCUMENT_ROOT',''));

  Arfolyam_DBLista:= INI.ReadString('DATABASE','ARF_DB_LISTA', ADOConnectionKozos.DefaultDatabase);  // pl. DAT_DEV,DKDAT_TESZT
  SoforSMS_Enabled:= (INI.ReadString('MUNKALAP','SoforSMS_Enabled', '0') = '1');

  ReszletesNaplo:= INI.ReadInteger('GENERAL','RESZLETES_NAPLO', 0);

  Odaeres_update_enabled:= (INI.ReadString('ODAERES','Odaeres_update_enabled', '0') = '1');
  ODAERES_HATAR_METERBEN:= INI.ReadInteger('ODAERES','ODAERES_HATAR_METERBEN', 1000);
  // ElkesettSebesseg := StrToInt(INI.ReadString('KESES_ELOREJELZES','ElkesettSebesseg','70'));

  Atallas_KMLimit:= INI.ReadString('ATALLAS_KM','KM_LIMIT', '300');
  Atallas_NapLimit:= INI.ReadString('ATALLAS_KM','NAP_LIMIT', '3');

  KILEPBELEP_KEZDONAP:= INI.ReadString('KILEPBELEP','KEZDONAP', '2017.01.01.');
  KILEPBELEP_CSATOLTPDFDIR:= INI.ReadString('KILEPBELEP','CSATOLTPDFDIR', 'X:\4_Ügyvitel\Min.ir. rend\Dolgozók érvényes okmány másolatai');
  KILEPBELEP_CSATOLTPDFNEV:= INI.ReadString('KILEPBELEP','CSATOLTPDFNEV', 'felmondás.pdf');

  ICLOUD_NEV:= INI.ReadString('KONTAKT','ICLOUD_NEV', '@icloud.com');

  arfolyam_mail_cim:= INI.ReadString('EMAIL','CIM' ,'');
  arfolyam_mail_devizak:= INI.ReadString('EMAIL','DEVIZAK' ,'EUR,USD,GBP');
  kuldendo_devizak:=TStringList.Create;
  kuldendo_devizak.Sorted:= True;
  StringParse(arfolyam_mail_devizak, ',', kuldendo_devizak);
  ArfolyamJelentesSzoveg:=TStringList.Create;

  MunkaIdoKezdete := StrToInt(INI.ReadString('NEMTELJESULT','MUNKAIDOKEZDETE','8')); // a figyelmeztető levelek küldéséhez
  MunkaIdoVege := StrToInt(INI.ReadString('NEMTELJESULT','MUNKAIDOVEGE','17'));
  // WriteAppLog('AKI1 FormCreate 2');
  NMSMTP1.Host:=INI.ReadString('EMAIL','SMTP','sbs.jsspeed.local');
  // nem kell create... LevelMellekletLista:= TMemoryStreamArray.Create(Form1); // TStringList.Create;
  LevelMellekletNevek:= TStringList.Create;
  LevelMellekletTipusok:= TStringList.Create;
  // MaiDatum    := SQLDATE.Execute.Fields[0].Value;
  MaiDatum    := FormatDateTime('yyyy.mm.dd.', Now);

  F0:= INI.ReadString('FELD','F0','I')='I';
  F1:= INI.ReadString('FELD','F1','I')='I';
  F2:= INI.ReadString('FELD','F2','I')='I';
  F3:= INI.ReadString('FELD','F3','I')='I';
  F4:= INI.ReadString('FELD','F4','I')='I';
  F5:= INI.ReadString('FELD','F5','I')='I';
  F6:= INI.ReadString('FELD','F6','I')='I';
  // WriteAppLog('AKI1 FormCreate 3');

  koltseglogfile		:= EXEPath+'LOG\'+'FUVAR_TIMER_KTSG.log';

  arflogfn:= INI.ReadString('ARFOLYAM','LOGFILE','FUVAR_TIMER_ARF.log');
  arflogfn:= EXEPath+'LOG\'+arflogfn ;
  munkalaplogfn:= INI.ReadString('MUNKALAP','LOGFILE','FUVAR_TIMER_MLP.log');
  munkalaplogfn:= EXEPath+'LOG\'+munkalaplogfn;
  rendkivesemlogfn:= INI.ReadString('RENDKIVULI_ESEMENY','LOGFILE','FUVAR_TIMER_ESEM.log');
  rendkivesemlogfn:= EXEPath+'LOG\'+rendkivesemlogfn;
  odaereslogfn:= INI.ReadString('ODAERES','LOGFILE','FUVAR_TIMER_ODA.log');
  odaereslogfn:= EXEPath+'LOG\'+odaereslogfn;
  keseslogfn:= INI.ReadString('KESESELOREJELZES','LOGFILE','FUVAR_TIMER_KESES.log');
  keseslogfn:= EXEPath+'LOG\'+keseslogfn;

  kulcslogfn:= INI.ReadString('KULCS','LOGFILE','FUVAR_TIMER_KULCS.log');
  kulcslogfn:= EXEPath+'LOG\'+kulcslogfn;

  // mozdulatlanlogfn:= INI.ReadString('KULCS','LOGFILE','FUVAR_TIMER_KULCS.log');
  mozdulatlanlogfn:= EXEPath+'LOG\'+'FUVAR_TIMER_ALLO.log';
  atallaslogfn:= EXEPath+'LOG\'+'FUVAR_TIMER_ATALLAS.log';
  szamlakodlogfn:= EXEPath+'LOG\'+'FUVAR_TIMER_SZLAKOD.log';
  kilepbeleplogfn:= EXEPath+'LOG\'+'FUVAR_TIMER_KIBELEP.log';
  smseaglelogfn:= EXEPath+'LOG\'+'FUVAR_TIMER_SMSEAGLE.log';
  kutaslogfn:= EXEPath+'LOG\'+'FUVAR_TIMER_KUTAS.log';
  fizetesilogfn:= EXEPath+'LOG\'+'FUVAR_TIMER_FIp ZJEGYZEK.log';
  osztraklogfn:= EXEPath+'LOG\'+'FUVAR_TIMER_OSZTRAK.log';
  icloudlogfn:= EXEPath+'LOG\'+'FUVAR_TIMER_ICLOUD.log';
  atjelentlogfn:= EXEPath+'LOG\'+'FUVAR_TIMER_ATJELENT.log';
  vcardlogfn:= EXEPath+'LOG\'+'FUVAR_TIMER_VCARD.log';
  szakaszlogfn:= EXEPath+'LOG\'+'FUVAR_TIMER_SZAKASZ.log';
  pozikuldeslogfn:= EXEPath+'LOG\'+'FUVAR_TIMER_POZIKULD.log';
  ediexportlogfn:= EXEPath+'LOG\'+'FUVAR_TIMER_EDI.log';
  megbizasdebuglogfn:= EXEPath+'LOG\'+'FUVAR_TIMER_MBDEBUG.log';
  gyorshajtaslogfn:= INI.ReadString('GYORSHAJTAS','LOGFILE','FUVAR_TIMER_GYORS.log');
  gyorshajtaslogfn:= EXEPath+'LOG\'+gyorshajtaslogfn;
  GyorshajtasSMS_Enabled:= (INI.ReadString('GYORSHAJTAS','GyorshajtasSMS_Enabled', '0') = '1');

  MasikRio.WSDLLocation:=ExtractFilePath(Application.ExeName)+'mnb.wsdl';

  Peldanyszam:= FindProcess(ExtractFileName( Application.ExeName));
  WriteLogFile(logfile,formatdatetime('yyyy.mm.dd. HH:mm:ss:zzz', Now)+'  Program példányszám: '+IntToStr(Peldanyszam)+' Self='+PeldanyAzonosito);

  if  Peldanyszam > 1 then begin
      // if ProgramMode='gyors' then begin // a "lassu"-t nem szakítjuk meg egy "gyors" kedvéért!
      // 2016-11-17: külön .EXE néven fut a "lassu" és a  "gyors", így egymással nem ütközhetnek
      WriteLogFile(logfile,'****'+formatdatetime('yyyy.mm.dd. HH:mm:ss:zzz', Now)+'  EXITUS!!');
      Application.Terminate;
      close;
      // end;  // if "gyors"
    end;
  // teszt idejére kiveszem...
  // Timer1.Enabled:= Application.ShowMainForm=False;

  CSVReader:= TnvvCSVStringReader.Create;
  // külön kell engedélyezni, ha a többi segédform (pl. EgyebDlg) is kész!
  // Timer1.Enabled:= True;
  // sleep(2000);
end;

procedure TForm1.FormDestroy(Sender: TObject);
begin
   // if LevelMellekletLista <> nil then LevelMellekletLista.Free;
  if LevelMellekletNevek <> nil then LevelMellekletNevek.Free;
  if LevelMellekletTipusok <> nil then LevelMellekletTipusok.Free;
end;

function TForm1.DBConnect : boolean;
var
	lgstr   		: string;
  	kellog			: integer;
  	alap_user		: string;
  	alap_pswd		: string;
  	kozos_user		: string;
  	kozos_pswd		: string;
	alap_str2		: string;
	kozos_str2, v_alap_db, v_koz_db, v_evszam, AlapConnS, KozosConnS : string;
	Inifn		   	: TIniFile;
begin
	Result	:= false;
   // Az alap paraméterek beolvasása a fő ini-ből
 	Inifn 					:= TIniFile.Create( globalini );
	lgstr					:= Inifn.ReadString( 'GENERAL', 'LOGIN_STR',  '');
  alap_user				:= Trim(DecodeString(Inifn.ReadString( 'GENERAL', 'ALAP_USER',  '')));
	alap_pswd				:= Trim(DecodeString(Inifn.ReadString( 'GENERAL', 'ALAP_PSWD',  '')));
	kozos_user				:= Trim(DecodeString(Inifn.ReadString( 'GENERAL', 'KOZOS_USER', '')));
	kozos_pswd				:= Trim(DecodeString(Inifn.ReadString( 'GENERAL', 'KOZOS_PSWD', '')));
 	v_alap_db	:= Inifn.ReadString( 'GENERAL', 'ALAP_DB', '');
	v_koz_db	:= Inifn.ReadString( 'GENERAL', 'KOZOS_DB', '');
  v_evszam	:= Inifn.ReadString( 'GENERAL', 'EVSZAM', '');
 	v_alap_db	:= v_alap_db + v_evszam;
	v_koz_db	:= v_koz_db  + v_evszam;


	Inifn.Free;

   try
		AdoConnectionAlap.Close;
		AdoConnectionKozos.Close;
    AdoConnectionArfolyam.Close;
		alap_str2	:= '';
		if alap_user <> '' then begin
			alap_str2 	:= ';User ID= '+alap_user+';Password='+alap_pswd;
		end;
		kozos_str2	:= '';
		if kozos_user <> '' then begin
			kozos_str2 	:= ';User ID= '+kozos_user+';Password='+kozos_pswd;
		end;

    AlapConnS:= lgstr+alap_str2+';Initial Catalog='+v_alap_db+';';
    KozosConnS:= lgstr+kozos_str2+';Initial Catalog='+v_koz_db+';';

		AdoConnectionAlap.ConnectionString		:= AlapConnS;
    AdoConnectionArfolyam.ConnectionString		:= AlapConnS;
		AdoConnectionKozos.ConnectionString		:= KozosConnS;

    // WriteLogFile(logfile, formatdatetime('yyyy.mm.dd. HH:mm:ss:zzz', Now)+' ALAP ConnectionString: '+AlapConnS);
    // WriteLogFile(logfile, formatdatetime('yyyy.mm.dd. HH:mm:ss:zzz', Now)+' AKOZOS ConnectionString: '+KozosConnS);
		AdoConnectionAlap.LoginPrompt			:= false;
		AdoConnectionKozos.LoginPrompt		:= false;
		AdoConnectionArfolyam.LoginPrompt		:= false;
    // WriteLogFile(logfile, formatdatetime('yyyy.mm.dd. HH:mm:ss:zzz', Now)+' Kapcsolódás...');
    ADOConnectionKozos.Connected;
    ADOConnectionAlap.Connected;
		AdoConnectionAlap.Open;
		AdoConnectionKozos.Open;
    // WriteLogFile(logfile, formatdatetime('yyyy.mm.dd. HH:mm:ss:zzz', Now)+' Adatkapcsolatok nyitva.');
	except
   on E: exception do begin
      WriteLogFile(logfile, formatdatetime('yyyy.mm.dd. HH:mm:ss:zzz', Now)+' Nem sikerült az adatbázis kapcsolódás! '+E.Message);
	  	// NoticeKi('Nem sikerült az adatbázis kapcsolódás!');
		  Exit;
      end;  // on E
	end;
end;
/// ********************************************

procedure TForm1.EnableTimer;
begin
   Timer1.Enabled:= True;
   sleep(2000);
end;

function TForm1.FindProcess(Name: string): integer;
var
  FSH: THandle;
  Fpe32: TProcessEntry32;
  cl: BOOL;
  //hp: Thandle;
begin
   FSH := CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS,0);
   Fpe32.dwSize:=Sizeof(Fpe32);
   cl := Process32First(FSH,Fpe32);
   Result:=0;

   While cl do
   begin
     If Name = Fpe32.szExeFile then
     begin
       //Result := Fpe32.th32ProcessID ;
       Inc(Result);
       //CloseHandle(FSH);
       //Exit;
     end;

     cl := Process32Next(FSH,Fpe32);
   end;
   CloseHandle(FSH);

end;

function TForm1.KillTask(ExeFileName: string): Integer;
const PROCESS_TERMINATE = $0001;
var continueloop: BOOL;
  fsnapshothandle: THandle;
  fprocessentry32: TProcessEntry32;
begin
  result:=0;
  fsnapshothandle:=createtoolhelp32snapshot(TH32CS_SNAPPROCESS,0);
  fprocessentry32.dwsize:=sizeof(fprocessentry32);
  continueloop:=process32first(fsnapshothandle,fprocessentry32);
  while integer(continueloop)<>0 do
  begin
    if((uppercase(extractfilename(fprocessentry32.szexefile))=uppercase(exefilename))
    or(uppercase(fprocessentry32.szexefile)=uppercase(exefilename))) then
    begin
      result:=integer(terminateprocess(openprocess(PROCESS_TERMINATE,BOOL(0),fprocessentry32.th32processID),0));
      closehandle(fsnapshothandle);
      abort;
    end;
    continueloop:=process32next(fsnapshothandle,fprocessentry32);
  end;
  closehandle(fsnapshothandle);

end;


function TForm1.EllenorzottJarat(kodstr: string; bb: boolean): integer;
begin
	ADOQuery1.Close;
  ADOQuery1.Parameters[0].Value:=kodstr;
  ADOQuery1.Open;
  ADOQuery1.First;

	Result	:= StrToIntDef(ADOQuery1JA_FAZIS.AsString, 0);
	ADOQuery1.Close;
end;

function TForm1.ArfolyamErtek(valnem, datestr: string;
  kell: boolean): double;
var
  ev, ho, nap : integer;
  tmpDate  	: TDateTime;

begin
	Result	:= 0;
	{Forintra mindig 1 -et adjon vissza}
  if valnem = 'HUF' then begin
		Result	:= 1;
     Exit;
  end;
	{Megkeresi az aznapi árfolyamot}
  ADOQuery2.Close;
  ADOQuery2.Parameters[0].Value:=valnem;
  ADOQuery2.Parameters[1].Value:=datestr;
  ADOQuery2.Open;
  ADOQuery2.First;

  if ADOQuery2.RecordCount > 0 then begin       // van árfolyam
     	Result := ADOQuery2AR_ERTEK.Value;
  end
  else
  begin                                 // NINCS árfolyam
     	{Megnézzük, hogy hétvégére esett-e}
  		ev    := StrToIntDef(copy(datestr,1,4),0);
  		ho    := StrToIntDef(copy(datestr,6,2),0);
  		nap   := StrToIntDef(copy(datestr,9,2),0);
  		if ( ( ev = 0 ) or ( ho = 0 ) or ( nap = 0 ) ) then begin
     		Exit;
  		end;
  		tmpDate := EncodeDate(ev, ho, nap);
      if not HetvegeVagyUnnep(tmpDate) then
      begin
        Result:=0;
        Exit;
      end;
      ///////////////////////////////// új KG
      while True do
      begin
    			ADOQuery2.Close;
          tmpDate:=tmpDate-1;
          if not HetvegeVagyUnnep(tmpDate) then
          begin
            datestr:= DateToStr(tmpDate) ;
          	Result := ArfolyamErtek(valnem, datestr, kell);
            Break;
          end;
      end;
      ///////////////////////////////
  	 end;
     ADOQuery2.Close;
  end;

function TForm1.HetvegeVagyUnnep(datum: TDate): Boolean;
begin
  Result:=False;
  if DayOfWeek(datum) = 1 then
  begin
    Result:=True;      { vasárnap }
    exit;
  end;
  if DayOfWeek(datum) = 7 then
  begin
    Result:=True;      { szombat  }
    Exit;
  end;
  SQLKozos.CommandText:= 'Select COUNT(na_unnep) from NAPOK WHERE na_orsza='+#39+'HU'+#39+' and na_unnep='+#39+'1'+#39+' and na_datum=CAST( '+#39+copy(DateToStr( datum),1,10)+#39+' AS DATETIME)';
  if SQLKozos.Execute.Fields[0].Value>0 then
    Result:=True;

end;

function TForm1.GetNextCode(Tabla: string): integer;
var
  S: string;
begin
  with ADOQuery3 do begin
    Close; SQL.Clear;
    if Tabla='koltseg' then S:='select max(ks_ktkod) max from koltseg';
    if Tabla='arfolyam' then S:='select max(ar_kod) max from arfolyam';
    SQL.Add(S);
    Open;
    Result:=Fields[0].AsInteger + 1;
    Close;
    end;  // with
end;

function TForm1.GetNextArfolyamCode: integer;    // a qUni-n kell menjen!
var
  S: string;
begin
  with qUniArfolyam do begin
    Close; SQL.Clear;
    S:='select max(ar_kod) max from arfolyam';
    SQL.Add(S);
    Open;
    Result:=Fields[0].AsInteger + 1;
    Close;
    end;  // with
end;

function TForm1.GetTelepiAr(dat: string; tipus: integer): double;
begin
	Result		:= 0;
  ADOQuery6.Close;
  ADOQuery6.Parameters[0].Value:=dat;
  ADOQuery6.Parameters[1].Value:=tipus;
  ADOQuery6.Open;
  ADOQuery6.First;

	if ADOQuery6.RecordCount > 0 then begin
		ADOQuery6.Last;
		Result	:= ADOQuery6TT_EGYAR.Value;
	end;
	ADOQuery6.Close;
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
   KoltsegBeolvas;
end;

procedure TForm1.KoltsegBeolvas;
var
  fn: string;
  // koltseglogfile: string;
begin
 // Timer1.Enabled:=False;

 fn:= INI.ReadString('FELD','FILE','AKI_KTSG.log');
 // koltseglogfile		:= EXEPath+'LOG\'+fn ;
 WriteLogFile(koltseglogfile,'START '+DateTimeToStr(now)+'  '+ADOConnectionAlap.DefaultDatabase);

 if F0 then
 Try
   //Feldolgozas0;          DE autópálya
   tdb:=Feldolgozas(0);
   WriteLogFile(koltseglogfile,' DE: '+IntToStr(tdb)+' db '+DateTimeToStr(now));
 Except
  on E: Exception do begin
     WriteLogFile(koltseglogfile,' ERROR '+DateTimeToStr(now)+ ' '+E.Message);
     end;  // on E
 End;

 if F1 then
 Try
   //Feldolgozas1;          Telepi tankolás
   tdb:=Feldolgozas(1);
   WriteLogFile(koltseglogfile,' TT: '+IntToStr(tdb)+' db '+DateTimeToStr(now));
 Except
  on E: Exception do begin
     WriteLogFile(koltseglogfile,' ERROR '+DateTimeToStr(now)+ ' '+E.Message);
     end;  // on E
 End;

 if F2 then
 Try
   //Feldolgozas2;          AT autópálya
   tdb:=Feldolgozas(2);
   WriteLogFile(koltseglogfile,' AT: '+IntToStr(tdb)+' db '+DateTimeToStr(now));
 Except
  on E: Exception do begin
     WriteLogFile(koltseglogfile,' ERROR '+DateTimeToStr(now)+ ' '+E.Message);
     end;  // on E
 End;

 if F3 then
 Try
   //Feldolgozas3;          SHELL
   tdb:=Feldolgozas(3);
   WriteLogFile(koltseglogfile,' SH: '+IntToStr(tdb)+' db '+DateTimeToStr(now));
 Except
  on E: Exception do begin
     WriteLogFile(koltseglogfile,' ERROR '+DateTimeToStr(now)+ ' '+E.Message);
     end;  // on E
 End;

 if F4 then
 Try
   //Feldolgozas4;          Egyéb költségek
   tdb:=Feldolgozas(4);
   WriteLogFile(koltseglogfile,' EK: '+IntToStr(tdb)+' db '+DateTimeToStr(now));
 Except
  on E: Exception do begin
     WriteLogFile(koltseglogfile,' ERROR '+DateTimeToStr(now)+ ' '+E.Message);
     end;  // on E
 End;

 if F5 then
 Try
   //Feldolgozas5;          EuroToll
   tdb:=Feldolgozas(5);
   WriteLogFile(koltseglogfile,' ET: '+IntToStr(tdb)+' db '+DateTimeToStr(now));
 Except
  on E: Exception do begin
     WriteLogFile(koltseglogfile,' ERROR '+DateTimeToStr(now)+ ' '+E.Message);
     end;  // on E
 End;

 if F6 then
 Try
   //Feldolgozas5;          EuroToll_DE
   tdb:=Feldolgozas(6);
   WriteLogFile(koltseglogfile,' ED: '+IntToStr(tdb)+' db '+DateTimeToStr(now));
 Except
  on E: Exception do begin
     WriteLogFile(koltseglogfile,' ERROR '+DateTimeToStr(now)+ ' '+E.Message);
     end;  // on E
 End;

 if F7 then
 Try
   //Feldolgozas5;          HU-GO
   tdb:=Feldolgozas(7);
   WriteLogFile(koltseglogfile,' HU: '+IntToStr(tdb)+' db '+DateTimeToStr(now));
 Except
  on E: Exception do begin
     WriteLogFile(koltseglogfile,' ERROR '+DateTimeToStr(now)+ ' '+E.Message);
     end;  // on E
 End;

 WriteLogFile(koltseglogfile,'END   '+DateTimeToStr(now));
 WriteLogFile(koltseglogfile,' ');
 sleep(2000);
 // close;
end;

function TForm1.WriteLogFile(FName, Text: string): Boolean;
var
  LogFile: TextFile;
begin
  // prepares log file
  AssignFile (LogFile, Fname);
  if FileExists (FName) then
    Append (LogFile) // open existing file
  else
    Rewrite (LogFile); // create a new one

  // write to the file and show error
  Writeln (LogFile,Text);

  // close the file
  CloseFile (LogFile);
  Result:=True;
end;

function TForm1.WriteAppLog(Text:string):Boolean;
var
   idopt: string;
begin
   idopt   := formatdatetime('yyyy.mm.dd. HH:mm:ss:zzz', Now);
   Result:= WriteLogFile(logfile, idopt+' '+Text);
end;



procedure TForm1.FormClose(Sender: TObject; var Action: TCloseAction);
begin
   if CSVReader <> nil then
      CSVReader.Free;
end;

procedure TForm1.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
//  ADOConnectionKozos.Close;
//  ADOConnectionAlap.Close;
end;

function TForm1.HutosGk(rendsz: string): Boolean;
var
    HUTOTIP,gkkat: string;
begin
  Result:=False;
  Szotar.Close;
  Szotar.Parameters[0].Value:='999';
  Szotar.Parameters[1].Value:='922';
  Szotar.Open;
  Szotar.First;
	HUTOTIP:=Szotarsz_menev.Value;

  Gepkocsi.Close;
  Gepkocsi.Parameters[0].Value:=rendsz;
  Gepkocsi.Open;
  Gepkocsi.First;

 	gkkat	:= ','+Gepkocsigk_gkkat.Value+',';
  if pos(gkkat,HUTOTIP)<>0 then
  begin
    Result:=True;
  end;

end;

function TForm1.Feldolgozas(KTIP: integer): integer;
var
  ism: boolean;
  rendsz,prendsz,datum,ido, jakod: string;
  jkdat,jvdat,jkido,jvido: string;
  udatum,uido,valnem,megj,leiras,tipus,fimod,teles,jarat,orsz,skod,snev,jaratkod: string;
  arfolyam,osszeg,ertek,menny,km,ekm, ear: double;
  ujkod, koltsegkm: integer;
  dbbe, dbfel, db, pluszkm: integer;
  elljarat,hutotankolas, ttank,htank: boolean;
  sertek,sosszeg,sarfolyam,smenny,skm, hiba,sjkkm,sjvkm,tele,sss, S: string;
  rs: _Recordset;
	alsoft			: double;
	felsoft			: double;
  hozzaadni: boolean;
  navc_datum, navc_ido,snavc_uza_lit,snavc_uza_szint, snavc_km: string;
  navc_uza_lit,navc_uza_szint, navc_km: double;
  ma: string;
begin
   if ADOConnectionAlap.InTransaction then
    ADOConnectionAlap.CommitTrans;

   ma:=DateToStr(date); 
   Result:=0;
   dbbe:=0;
   dbfel:=0;
 {  Edit1.Text:=IntToStr(dbbe);
   Edit1.Update;
   Edit2.Text:=IntToStr(dbfel);
   Edit2.Update; }
   T_GYUJTOMIND.Close;
   T_GYUJTOMIND.SQL.Clear;
   T_GYUJTOMIND.SQL.Add('select * from KOLTSEG_GYUJTO where ((KS_JAKOD is null)or(KS_JAKOD='+#39+#39+')) and '+' KS_KTIP= :KTIP order by ks_rendsz,ks_datum,ks_ido');
   T_GYUJTOMIND.Parameters[0].Value:=KTIP;
   T_GYUJTOMIND.Open;
   T_GYUJTOMIND.First;
   while not T_GYUJTOMIND.Eof do
   begin
      if ReszletesNaplo=1 then WriteLogFile(koltseglogfile,' KTKOD feldolgozás alatt: '+T_GYUJTOMINDKS_KTKOD.AsString);
      navc_datum:='';
      navc_ido:='';
      navc_uza_lit:=0;
      navc_uza_szint:=0;
      navc_km:=0;
      snavc_uza_lit:='0';
      snavc_uza_szint:='0';
      snavc_km:='0';
      hutotankolas:=False;
      hozzaadni:=False;
      rendsz:=T_GYUJTOMINDKS_RENDSZ.Value;
      if rendsz='' then // nincs rendszám
      begin
        if ReszletesNaplo=1 then WriteLogFile(koltseglogfile,' nincs rendszám');
        T_GYUJTOMIND.Next;
        Continue;
      end;

      datum:=Trim(T_GYUJTOMINDKS_DATUM.Value);
      if Length(datum)=10 then
        datum:=datum+'.';
      ido:=T_GYUJTOMINDKS_IDO.Value;
      Try   // Dátum, idő ell.
        StrToDateTime(datum+' '+ido);
      Except
        hiba:='Hibás dátum-idő! '+datum+' - '+ido;
        jaratkod:='!HIBA!';
        if ReszletesNaplo=1 then WriteLogFile(koltseglogfile,' '+hiba);
        ADOCommand1.CommandText:='UPDATE KOLTSEG_GYUJTO set KS_JARAT='+#39+jaratkod+#39+',KS_HMEGJ='+#39+hiba+#39+',KS_FELDAT=getdate() where KS_KTKOD='+T_GYUJTOMINDKS_KTKOD.AsString  ;
        ADOCommand1.Execute;
        if ReszletesNaplo=1 then WriteLogFile(koltseglogfile,' SQL exec: '+ADOCommand1.CommandText);
        T_GYUJTOMIND.Next;
        Continue;
      End;

      if (T_GYUJTOMINDKS_TIPUS.Value=0)and(HutosGk(rendsz)) then   // ez egy hűtős rendszám
      begin
        ekm:=T_GYUJTOMINDKS_KMORA.Value;
        T_JARPOTOS.Close;
        T_JARPOTOS.SQL.Clear;
        T_JARPOTOS.SQL.Add('select * from JARPOTOS , JARAT where (jp_poren= :KOD)and(jp_uzem1 <= :ORA1)and(jp_uzem2 >= :ORA2)and(ja_kod=jp_jakod)and((ja_jkezd + ja_keido )<= :DAT1)and((ja_jvege + ja_veido)>= :DAT2)');
        T_JARPOTOS.Parameters[0].Value:=rendsz;
        T_JARPOTOS.Parameters[1].Value:=ekm;
        T_JARPOTOS.Parameters[2].Value:=ekm;
        T_JARPOTOS.Parameters[3].Value:=datum+ido;
        T_JARPOTOS.Parameters[4].Value:=datum+ido;
        T_JARPOTOS.Open;
        T_JARPOTOS.First;

        if (T_JARPOTOS.RecordCount=0)and(ekm>0) then   // NEM talált hűtős pótot
        begin
          hiba:='Nincs járat ilyen hűtős pótkocsi üzemóraállással!';
          jaratkod:='!HIBA!';
          if ReszletesNaplo=1 then WriteLogFile(koltseglogfile,' '+hiba);
          ADOCommand1.CommandText:='UPDATE KOLTSEG_GYUJTO set KS_JARAT='+#39+jaratkod+#39+',KS_HMEGJ='+#39+hiba+#39+',KS_FELDAT=getdate() where KS_KTKOD='+T_GYUJTOMINDKS_KTKOD.AsString  ;
          ADOCommand1.Execute;
          if ReszletesNaplo=1 then WriteLogFile(koltseglogfile,' SQL exec: '+ADOCommand1.CommandText);
          T_GYUJTOMIND.Next;
          Continue;
        end;
        if (T_JARPOTOS.RecordCount>1)and(ekm>0) then   // talált hűtős pótot
        begin
          //prendsz:=rendsz;
          hiba:='Több járat ilyen hűtős pótkocsi üzemóraállással!';
          T_JARPOTOS.First;
          while not T_JARPOTOS.Eof do
          begin
            hiba:=hiba+T_JARPOTOSJP_JAKOD.Value+',';
            T_JARPOTOS.Next;
          end;
          jaratkod:='!HIBA!';
          if ReszletesNaplo=1 then WriteLogFile(koltseglogfile,' '+hiba);
          ADOCommand1.CommandText:='UPDATE KOLTSEG_GYUJTO set KS_JARAT='+#39+jaratkod+#39+',KS_HMEGJ='+#39+hiba+#39+',KS_FELDAT=getdate() where KS_KTKOD='+T_GYUJTOMINDKS_KTKOD.AsString  ;
          ADOCommand1.Execute;
          if ReszletesNaplo=1 then WriteLogFile(koltseglogfile,' SQL exec: '+ADOCommand1.CommandText);
          T_GYUJTOMIND.Next;
          Continue;
        end;

        if (T_JARPOTOS.RecordCount=1)and(ekm>0) then   // talált hűtős pótot
        begin
          jakod:=T_JARPOTOSJP_JAKOD.Value;
          hutotankolas:=True;
          //rendsz:= Query_Select('JARAT','JA_KOD',jakod,'JA_RENDSZ');
          ADOCommand1.CommandText:='select JA_RENDSZ from JARAT where JA_KOD='+#39+jakod+#39 ;
          rs:= ADOCommand1.Execute;
          rendsz:=rs.Fields[0].Value;
        end;
      end;
      datum:=Trim(T_GYUJTOMINDKS_DATUM.Value);
      if Length(datum)=10 then
        datum:=datum+'.';
      ido:=T_GYUJTOMINDKS_IDO.Value;
      Try   // Dátum, idő ell.
        StrToDateTime(datum+' '+ido);
      Except
        hiba:='Hibás dátum-idő! '+datum+' - '+ido;
        jaratkod:='!HIBA!';
        if ReszletesNaplo=1 then WriteLogFile(koltseglogfile,' '+hiba);
        ADOCommand1.CommandText:='UPDATE KOLTSEG_GYUJTO set KS_JARAT='+#39+jaratkod+#39+',KS_HMEGJ='+#39+hiba+#39+',KS_FELDAT=getdate() where KS_KTKOD='+T_GYUJTOMINDKS_KTKOD.AsString  ;
        ADOCommand1.Execute;
        if ReszletesNaplo=1 then WriteLogFile(koltseglogfile,' SQL exec: '+ADOCommand1.CommandText);
        T_GYUJTOMIND.Next;
        Continue;
      End;
      if ReszletesNaplo=1 then WriteLogFile(koltseglogfile,' járat keresés...');
      T_JARAT.Close;
      T_JARAT.SQL.Clear;
      koltsegkm:= T_GYUJTOMINDKS_KMORA.AsInteger;
      if ido='' then begin
        // S:='select * from jarat where JA_FAZIS<>'+'0'+' and ja_rendsz= :RSZ and (ja_jkezd)<= :DAT1 and (ja_jvege)>= :DAT2 order by ja_jkezd';
        S:='select * from jarat where JA_FAZIS<>'+'0'+' and ja_rendsz= '''+rendsz+''' and (ja_jkezd)<= '''+datum+''' and (ja_jvege)>= '''+datum+'''';
        if (KTIP=1) and EgyebDlg.TANKOLAS_KM_FIGYELEMBE_VETEL then begin  // Telepi tankolásnál
           S:= S+' and (('+IntToStr(koltsegkm)+' between JA_NYITKM and JA_ZAROKM )';
           S:= S+' or ('+IntToStr(koltsegkm)+'+(select GK_HOZZA from GEPKOCSI where GK_RESZ='''+rendsz+''') between JA_NYITKM and JA_ZAROKM ))';
           end;
        S:= S+' order by ja_jkezd';
        T_JARAT.SQL.Add(S);
        // T_JARAT.Parameters[0].Value:=rendsz;
        // T_JARAT.Parameters[1].Value:=datum;
        // T_JARAT.Parameters[2].Value:=datum;
        end
      else begin
        // T_JARAT.SQL.Add('select * from jarat where JA_FAZIS<>'+'0'+' and ja_rendsz= :RSZ and (ja_jkezd + ja_keido )<= :DAT1 and (ja_jvege + ja_veido)>= :DAT2 order by ja_jkezd, ja_keido');
        S:= 'select * from jarat where JA_FAZIS<>'+'0'+' and ja_rendsz= '''+rendsz+''' and (ja_jkezd + ja_keido )<= '''+datum+ido+''''+
          ' and (ja_jvege + ja_veido)>= '''+datum+ido+'''';
        if (KTIP=1) and EgyebDlg.TANKOLAS_KM_FIGYELEMBE_VETEL then begin   // Telepi tankolásnál
           S:= S+' and (('+IntToStr(koltsegkm)+' between JA_NYITKM and JA_ZAROKM )';
           S:= S+' or ('+IntToStr(koltsegkm)+'+(select GK_HOZZA from GEPKOCSI where GK_RESZ='''+rendsz+''') between JA_NYITKM and JA_ZAROKM ))';
           end;
        S:= S+' order by ja_jkezd, ja_keido';

        T_JARAT.SQL.Add(S);
        // T_JARAT.Parameters[0].Value:=rendsz;
        // T_JARAT.Parameters[1].Value:=datum+ido;
        // T_JARAT.Parameters[2].Value:=datum+ido;
        end;
      if ReszletesNaplo=1 then  WriteLogFile(koltseglogfile,' járat keresés, SQL='+S);
      T_JARAT.Open;
      T_JARAT.First;
      db:=T_JARAT.RecordCount;
      if ReszletesNaplo=1 then WriteLogFile(koltseglogfile,' járat keresés, darab='+IntToStr(db));
      if db=0 then // nincs járat
      begin
        T_GYUJTOMIND.Next;
        if ReszletesNaplo=1 then WriteLogFile(koltseglogfile,' nincs járat');
        Continue;
      end;
      if db>1 then
      begin
        hiba:='Több járat!';
        jaratkod:='!HIBA!';
        if ReszletesNaplo=1 then WriteLogFile(koltseglogfile,' '+hiba);
        ADOCommand1.CommandText:='UPDATE KOLTSEG_GYUJTO set KS_JARAT='+#39+jaratkod+#39+',KS_HMEGJ='+#39+hiba+#39+',KS_FELDAT=getdate() where KS_KTKOD='+T_GYUJTOMINDKS_KTKOD.AsString  ;
        ADOCommand1.Execute;
        if ReszletesNaplo=1 then WriteLogFile(koltseglogfile,' SQL exec: '+ADOCommand1.CommandText);
        T_GYUJTOMIND.Next;
        Continue;
      end;

      jakod:=T_JARATJA_KOD.Value;
  		jaratkod:= T_JARATJA_ORSZ.AsString+'-'+Format('%5.5d',[T_JARATJA_ALKOD.AsInteger]);
      sjkkm:=FloatToStr( T_JARATJA_NYITKM.Value );
      sjvkm:=FloatToStr( T_JARATJA_ZAROKM.Value );
      jkdat:=T_JARATJA_JKEZD.Value;
      jvdat:=T_JARATJA_JVEGE.Value;
      jkido:=T_JARATJA_KEIDO.Value;
      jvido:=T_JARATJA_VEIDO.Value;
      skod:=T_JARATJA_SOFK1.Value;
      snev:=T_JARATJA_SNEV1.Value;

     	elljarat:= EllenorzottJarat(jakod,False) > 0 ;
{
      if elljarat then       // a járat már ellenőrzött
      begin
        hiba:='Ellenőrzött járat!';
        //jaratkod:='!HIBA!';
        ADOCommand1.CommandText:='UPDATE KOLTSEG_GYUJTO set KS_JARAT='+#39+jaratkod+#39+',KS_HMEGJ='+#39+hiba+#39+',KS_FELDAT=getdate() where KS_KTKOD='+T_GYUJTOMINDKS_KTKOD.AsString  ;
        ADOCommand1.Execute;
        T_GYUJTOMIND.Next;
        Continue;
      end;
 }
 {
      if EllenorzottJarat(jakod,False) <> -1 then       // NEM elszámolt járat
      begin
        hiba:='NEM elszámolt járat!';
        //jaratkod:='!HIBA!';
        ADOCommand1.CommandText:='UPDATE KOLTSEG_GYUJTO set KS_JARAT='+#39+jaratkod+#39+',KS_HMEGJ='+#39+hiba+#39+',KS_FELDAT=getdate() where KS_KTKOD='+T_GYUJTOMINDKS_KTKOD.AsString  ;
        ADOCommand1.Execute;
        T_GYUJTOMIND.Next;
        Continue;
      end;
       }
      // Eddig OK. Feldolgozás
      // ASFINAGnál lehet hogy gyűjteni kell majd a költségeket !!!!!!!!!!!!!!!!!!!!!!

      valnem:=T_GYUJTOMINDKS_VALNEM.Value;
      arfolyam:=1;
      if valnem<>'HUF' then
        arfolyam:= ArfolyamErtek(valnem,datum,false);
      sarfolyam:=StringReplace( FloatToStr(arfolyam),',','.',[rfReplaceAll]) ;
      megj:=T_GYUJTOMINDKS_MEGJ.Value;
      leiras:=T_GYUJTOMINDKS_LEIRAS.Value;
      tipus:=IntToStr( T_GYUJTOMINDKS_TIPUS.Value);
      orsz:=T_GYUJTOMINDKS_ORSZA.Value;
      fimod:=T_GYUJTOMINDKS_FIMOD.Value;
      menny:= T_GYUJTOMINDKS_UZMENY.Value;
      ekm:=T_GYUJTOMINDKS_KMORA.Value;
      km:=T_GYUJTOMINDKS_KMORA.Value;
      teles:='N';
      osszeg:=T_GYUJTOMINDKS_OSSZEG.Value;
      sosszeg:=StringReplace( FloatToStr(osszeg),',','.',[rfReplaceAll]) ;
      ertek:=RoundTo(osszeg*arfolyam,0);
      sertek:=StringReplace( FloatToStr(ertek),',','.',[rfReplaceAll]) ;
      smenny:=StringReplace( FloatToStr(menny),',','.',[rfReplaceAll]) ;
      skm:=StringReplace( FloatToStr(km),',','.',[rfReplaceAll]) ;

      //if menny=0 then       // nincs mennyiség
      if (KTIP<>0)and(KTIP<>2)and(KTIP<>5)and(KTIP<>6)and(KTIP<>7)and(menny=0) then       // nincs mennyiség      // NEM DE ,AT
      begin
        hiba:='Nulla (0) mennyiség!';
        if ReszletesNaplo=1 then WriteLogFile(koltseglogfile,' '+hiba);
        ADOCommand1.CommandText:='UPDATE KOLTSEG_GYUJTO set KS_JARAT='+#39+jaratkod+#39+',KS_HMEGJ='+#39+hiba+#39+',KS_FELDAT=getdate() where KS_KTKOD='+T_GYUJTOMINDKS_KTKOD.AsString  ;
        ADOCommand1.Execute;
        if ReszletesNaplo=1 then WriteLogFile(koltseglogfile,' SQL exec: '+ADOCommand1.CommandText);
        T_GYUJTOMIND.Next;
        Continue;
      end;

      if (KTIP<>1)and(ertek=0) then       // nincs érték        NEM Telepi tankolás
      begin
        hiba:='Nulla (0) érték. Lehet hogy nincs árfolyam.';
        if ReszletesNaplo=1 then WriteLogFile(koltseglogfile,' '+hiba);
        ADOCommand1.CommandText:='UPDATE KOLTSEG_GYUJTO set KS_JARAT='+#39+jaratkod+#39+',KS_HMEGJ='+#39+hiba+#39+',KS_FELDAT=getdate() where KS_KTKOD='+T_GYUJTOMINDKS_KTKOD.AsString  ;
        ADOCommand1.Execute;
        if ReszletesNaplo=1 then WriteLogFile(koltseglogfile,' SQL exec: '+ADOCommand1.CommandText);
        T_GYUJTOMIND.Next;
        Continue;
      end;

      if tipus='2' then // AD Blue
      begin
       pluszkm:=Oraatfordulas(rendsz);
       if (pluszkm>km) then
       begin
  			 km:= km + pluszkm;   // óraátfordulás vizsgálata
         skm:=StringReplace( FloatToStr(km),',','.',[rfReplaceAll]) ;
       end;
       if ((T_JARATJA_NYITKM.Value>km)or(T_JARATJA_ZAROKM.Value<km)) then   // nem esik bele a járat km intervallumba
       begin
          if ReszletesNaplo=1 then WriteLogFile(koltseglogfile,' Rossz km! Járat: '+sjkkm+'-'+sjvkm);
          ADOCommand1.CommandText:='UPDATE KOLTSEG_GYUJTO set KS_JARAT='+#39+jaratkod+#39+',KS_HMEGJ='+#39+'Rossz km! Járat: '+sjkkm+'-'+sjvkm+#39+',KS_FELDAT=getdate() where KS_KTKOD='+T_GYUJTOMINDKS_KTKOD.AsString  ;
          ADOCommand1.Execute;
          if ReszletesNaplo=1 then WriteLogFile(koltseglogfile,' SQL exec: '+ADOCommand1.CommandText);
          T_GYUJTOMIND.Next;
          Continue;
       end;
      end;

      if tipus='0' then             // Üzemanyag; vizsgálni a km-t és a hűtőtankolást
      begin
       if (km=0)and((KTIP=3)or(KTIP=1)) then       //    KM=0, Telepi, SHELL
       begin
        hiba:='0 km óra állás.';
        jaratkod:='!HIBA!';
        if ReszletesNaplo=1 then WriteLogFile(koltseglogfile,' '+hiba);
        ADOCommand1.CommandText:='UPDATE KOLTSEG_GYUJTO set KS_JARAT='+#39+jaratkod+#39+',KS_HMEGJ='+#39+hiba+#39+',KS_FELDAT=getdate() where KS_KTKOD='+T_GYUJTOMINDKS_KTKOD.AsString  ;
        ADOCommand1.Execute;
        if ReszletesNaplo=1 then WriteLogFile(koltseglogfile,' SQL exec: '+ADOCommand1.CommandText);
        T_GYUJTOMIND.Next;
        Continue;
       end;

  		 //pluszkm:=StrToIntDef(Query_Select('GEPKOCSI', 'GK_RESZ', rendsz, 'GK_HOZZA'), 0);   // óraátfordulás vizsgálata
       pluszkm:= Oraatfordulas(rendsz);
       if (not hutotankolas) and (pluszkm>km) then
  			 km:= km + pluszkm;   // óraátfordulás vizsgálata

       skm:=StringReplace( FloatToStr(km),',','.',[rfReplaceAll]) ;

       //////////////////////////////////////////////////////////////////
       // vizsgálat; hátha jó a km(óra) a vontatóhoz és a póthoz is
       ttank:=not ((T_JARATJA_NYITKM.Value>km)or(T_JARATJA_ZAROKM.Value<km)) ;   // a vontatóhoz? ;    bele esik a járat km intervallumba ?

       T_JARPOTOS.Close;
       T_JARPOTOS.SQL.Clear;
       T_JARPOTOS.SQL.Add('select * from jarpotos where jp_jakod= :KOD and jp_uzem1 <= :ORA1 and  jp_uzem2 >= :ORA2');
       T_JARPOTOS.Parameters[0].Value:=jakod;
       T_JARPOTOS.Parameters[1].Value:=ekm;
       T_JARPOTOS.Parameters[2].Value:=ekm;
       T_JARPOTOS.Open;
       T_JARPOTOS.First;
       htank:=(T_JARPOTOS.RecordCount>0)and(ekm>0) ;   // póthoz?  ; talált hűtős pótot a járatban ?
       T_JARPOTOS.Close;

       if ttank and htank then   // az óraállás jó a vontatóhoz és a hűtőhöz is
       begin
          if ReszletesNaplo=1 then WriteLogFile(koltseglogfile,' '+'Az óraállás jó a vontatóhoz és a póthoz is');
          ADOCommand1.CommandText:='UPDATE KOLTSEG_GYUJTO set KS_JARAT='+#39+jaratkod+#39+',KS_HMEGJ='+#39+'Az óraállás jó a vontatóhoz és a póthoz is!'+#39+',KS_FELDAT=getdate() where KS_KTKOD='+T_GYUJTOMINDKS_KTKOD.AsString  ;
          ADOCommand1.Execute;
          if ReszletesNaplo=1 then WriteLogFile(koltseglogfile,' SQL exec: '+ADOCommand1.CommandText);
          T_GYUJTOMIND.Next;
          Continue;
       end;
       /////////////////////////////////////////

       if ((T_JARATJA_NYITKM.Value>km)or(T_JARATJA_ZAROKM.Value<km)) then   // nem esik bele a járat km intervallumba
       begin
        // Meg kell vizsgálni hogy nem hűtőtankolás e.
        hutotankolas:=False;
        T_JARPOTOS.Close;
        T_JARPOTOS.SQL.Clear;
        T_JARPOTOS.SQL.Add('select * from jarpotos where jp_jakod= :KOD and jp_uzem1 <= :ORA1 and  jp_uzem2 >= :ORA2');
        T_JARPOTOS.Parameters[0].Value:=jakod;
        T_JARPOTOS.Parameters[1].Value:=ekm;
        T_JARPOTOS.Parameters[2].Value:=ekm;
        T_JARPOTOS.Open;
        T_JARPOTOS.First;
        if (T_JARPOTOS.RecordCount=1)and(ekm>0) then   // talált hűtős pótot a járatban
        begin
          prendsz:=T_JARPOTOSJP_POREN.Value;
          //EgyebDlg.HutosGk(prendsz) then
          hutotankolas:=True;
          rendsz:=prendsz;
          tipus:='3';
          leiras:='Hűtő tankolás';
          km:=ekm;
          skm:=StringReplace( FloatToStr(km),',','.',[rfReplaceAll]) ;
        end;
        if (T_JARPOTOS.RecordCount=0) then   // NEM talált hűtős pótot a járatban
        begin
          if ReszletesNaplo=1 then WriteLogFile(koltseglogfile,' '+'Rossz km! Járat: '+sjkkm+'-'+sjvkm);
          ADOCommand1.CommandText:='UPDATE KOLTSEG_GYUJTO set KS_JARAT='+#39+jaratkod+#39+',KS_HMEGJ='+#39+'Rossz km! Járat: '+sjkkm+'-'+sjvkm+#39+',KS_FELDAT=getdate() where KS_KTKOD='+T_GYUJTOMINDKS_KTKOD.AsString  ;
          ADOCommand1.Execute;
          if ReszletesNaplo=1 then WriteLogFile(koltseglogfile,' SQL exec: '+ADOCommand1.CommandText);
          T_GYUJTOMIND.Next;
          Continue;
        end;
        if (T_JARPOTOS.RecordCount>1)and(ekm>0) then   // TÖBB talált hűtős pótot a járatban
        begin
          if ReszletesNaplo=1 then WriteLogFile(koltseglogfile,' '+'Több hűtős pót a járatban! Járat: '+sjkkm+'-'+sjvkm);
          ADOCommand1.CommandText:='UPDATE KOLTSEG_GYUJTO set KS_JARAT='+#39+jaratkod+#39+',KS_HMEGJ='+#39+'Több hűtős pót a járatban! Járat: '+sjkkm+'-'+sjvkm+#39+',KS_FELDAT=getdate() where KS_KTKOD='+T_GYUJTOMINDKS_KTKOD.AsString  ;
          ADOCommand1.Execute;
          if ReszletesNaplo=1 then WriteLogFile(koltseglogfile,' SQL exec: '+ADOCommand1.CommandText);
          T_GYUJTOMIND.Next;
          Continue;
        end;
       end;
      end;
      if tipus='1' then    // Egyéb költség
      begin
            km:=0;
            skm:='0';
      end;
      if hutotankolas then
        tele:='1'
      else
        tele:='0';
      // --------------------------------------------------------------------//
      //              Üzemanyag Telepi
      // --------------------------------------------------------------------//
      if KTIP=1 then
      begin
        if ReszletesNaplo=1 then WriteLogFile(koltseglogfile,' telepi tank feldolgozás 1');
        tele:='1';
        if tipus='2' then
     				ear		:= GetTelepiAr(datum,1);
        if (tipus='0')or(tipus='3') then          // Üzemanyag, hűtő
     				ear		:= GetTelepiAr(datum,0);
        if tipus='0' then
        begin
          T_KOLTSEG.Close;
          T_KOLTSEG.SQL.Clear;
          T_KOLTSEG.SQL.Add('SELECT KS_DATUM FROM KOLTSEG_GYUJTO WHERE KS_RENDSZ = '''+rendsz+''' '+
  					' AND KS_KTIP  = '+IntToStr(KTIP)+
	  				' AND KS_KMORA  = '+FloatToStr(km)+
		  			' AND KS_DATUM+KS_IDO  > '''+datum+ido+''' '+
			  		' AND KS_TIPUS  = '+tipus );
          T_KOLTSEG.Open;
          T_KOLTSEG.First;
          //////////////////////////////////////
				  if T_KOLTSEG.RecordCount > 0 then      // van tankolás ugyanolya km-rel. Csak a későbbi teletank.
            tele:='0';
          if tele='1' then                     // ha Telepi tank. és üzemanyag, és teletank, akkor a NavCenter adatai is
          begin
            navc_datum:=T_GYUJTOMINDKS_NC_DAT.Value;
            navc_ido:=T_GYUJTOMINDKS_NC_IDO.Value;
            navc_uza_lit:=T_GYUJTOMINDKS_NC_UME.Value;
            navc_uza_szint:=T_GYUJTOMINDKS_NC_USZ.Value;
            navc_km:=T_GYUJTOMINDKS_NC_KM.Value;
            snavc_uza_lit:=StringReplace( FloatToStr(T_GYUJTOMINDKS_NC_UME.Value),',','.',[rfReplaceAll]) ;
            snavc_uza_szint:=StringReplace( FloatToStr(T_GYUJTOMINDKS_NC_USZ.Value),',','.',[rfReplaceAll]) ;
            snavc_km:=StringReplace( FloatToStr(T_GYUJTOMINDKS_NC_KM.Value),',','.',[rfReplaceAll]) ;
          end;
          T_KOLTSEG.Close;
        end;

        ertek:=RoundTo( menny * ear,0);
        if ReszletesNaplo=1 then WriteLogFile(koltseglogfile,' érték: '+FormatFloat('0.00', ertek));
        sertek:=StringReplace( FloatToStr(ertek),',','.',[rfReplaceAll]) ;
        sosszeg:=sertek;
        if ertek=0 then
        begin
          hiba:='Nulla (0) érték. Lehet hogy nincs egységár!';
          if ReszletesNaplo=1 then WriteLogFile(koltseglogfile,' '+hiba);
          ADOCommand1.CommandText:='UPDATE KOLTSEG_GYUJTO set KS_JARAT='+#39+jaratkod+#39+',KS_HMEGJ='+#39+hiba+#39+',KS_FELDAT=getdate() where KS_KTKOD='+T_GYUJTOMINDKS_KTKOD.AsString  ;
          ADOCommand1.Execute;
          if ReszletesNaplo=1 then WriteLogFile(koltseglogfile,' SQL exec: '+ADOCommand1.CommandText);
          T_GYUJTOMIND.Next;
          Continue;
        end;
        if ear <> 0 then begin
               alsoft	:= StrToInt(Szotar_olvaso('999','714'));
				       felsoft:= StrToInt(Szotar_olvaso('999','715'));
               if alsoft > 0 then begin
               	if (alsoft > ear)and(tipus<>'2') then begin
                  hiba:='Az üzemanyag egységára kisebb mint a megadott intervallum alsó határa : /'+Format('%.0f Ft/l < %.0f Ft/l',[ear, alsoft]);
                  if ReszletesNaplo=1 then WriteLogFile(koltseglogfile,' '+hiba);
                  ADOCommand1.CommandText:='UPDATE KOLTSEG_GYUJTO set KS_JARAT='+#39+jaratkod+#39+',KS_HMEGJ='+#39+hiba+#39+',KS_FELDAT=getdate() where KS_KTKOD='+T_GYUJTOMINDKS_KTKOD.AsString  ;
                  ADOCommand1.Execute;
                  if ReszletesNaplo=1 then WriteLogFile(koltseglogfile,' SQL exec: '+ADOCommand1.CommandText);
                  T_GYUJTOMIND.Next;
                  Continue;
					      end;
               end;
               if felsoft > 0 then begin
               	if ear > felsoft then begin
                  hiba:='Az üzemanyag egységára nagyobb mint a megadott intervallum felső határa : /'+Format('%.0f Ft/l > %.0f Ft/l',[ear, felsoft]);
                  if ReszletesNaplo=1 then WriteLogFile(koltseglogfile,' '+hiba);
                  ADOCommand1.CommandText:='UPDATE KOLTSEG_GYUJTO set KS_JARAT='+#39+jaratkod+#39+',KS_HMEGJ='+#39+hiba+#39+',KS_FELDAT=getdate() where KS_KTKOD='+T_GYUJTOMINDKS_KTKOD.AsString  ;
                  ADOCommand1.Execute;
                  if ReszletesNaplo=1 then WriteLogFile(koltseglogfile,' SQL exec: '+ADOCommand1.CommandText);
                  T_GYUJTOMIND.Next;
                  Continue;
					      end;
               end;
			  end;
      end;

      if (KTIP<>5)and(KTIP<>7) then     // nem EUROTOLL vagy HUGO
      begin
  			// Ellenőrizzük a költség meglétét
       if ReszletesNaplo=1 then WriteLogFile(koltseglogfile,' feldolg. 2.');
       T_KOLTSEG.Close;
       T_KOLTSEG.SQL.Clear;
       T_KOLTSEG.SQL.Add('SELECT * FROM KOLTSEG WHERE KS_RENDSZ = '''+rendsz+''' '+
					' AND KS_DATUM  = '''+datum+''' '+
					' AND KS_IDO    = '''+ido+''' '+
					' AND KS_KMORA  = '+FloatToStr(km)+
					' AND KS_OSSZEG = '+sosszeg+
					' AND KS_TIPUS  = '+tipus );
       T_KOLTSEG.Open;
       T_KOLTSEG.First;
	  	 if T_KOLTSEG.RecordCount > 0 then      // Van már ilyen költség
       begin
          hiba:='Van már ilyen költség a járatban!';
          if ReszletesNaplo=1 then WriteLogFile(koltseglogfile,' '+hiba);
          ADOCommand1.CommandText:='UPDATE KOLTSEG_GYUJTO set KS_JARAT='+#39+jaratkod+#39+',KS_HMEGJ='+#39+hiba+#39+' ,KS_FELDAT=getdate() where KS_KTKOD='+T_GYUJTOMINDKS_KTKOD.AsString  ;
          ADOCommand1.Execute;
          if ReszletesNaplo=1 then WriteLogFile(koltseglogfile,' SQL exec: '+ADOCommand1.CommandText);
          T_GYUJTOMIND.Next;
          Continue;
       end;
      end
      else     // EUROTOLL(5); HUGO(7) gyűjteni kell a költségeket
      begin
  			// Ellenőrizzük a költség meglétét
       T_KOLTSEG.Close;
       T_KOLTSEG.SQL.Clear;
       T_KOLTSEG.SQL.Add('SELECT * FROM KOLTSEG WHERE KS_RENDSZ = '''+rendsz+''' '+
					' AND KS_DATUM  = '''+datum+''' '+
					' AND KS_ORSZA  = '''+orsz+''' '+
					' AND KS_JAKOD  = '''+jakod+''' '+
					' AND KS_KTIP   = '''+IntToStr(KTIP)+''' '+
					' AND KS_TIPUS  = '+tipus );
       T_KOLTSEG.Open;
       T_KOLTSEG.First;
  		 if T_KOLTSEG.RecordCount > 0 then      // Van már ilyen költség
       begin
        hozzaadni:=True;
        sosszeg:=StringReplace( FloatToStr(T_KOLTSEG.FieldByname('KS_OSSZEG').Value+ osszeg),',','.',[rfReplaceAll]) ;
        sertek :=StringReplace( FloatToStr(T_KOLTSEG.FieldByname('KS_ERTEK').Value +ertek),',','.',[rfReplaceAll]) ;
       end;
      end;

      if ertek=0 then
      begin
          hiba:='Nulla (0) érték. Lehet hogy nincs egységár!';
          ADOCommand1.CommandText:='UPDATE KOLTSEG_GYUJTO set KS_JARAT='+#39+jaratkod+#39+',KS_HMEGJ='+#39+hiba+#39+',KS_FELDAT=getdate() where KS_KTKOD='+T_GYUJTOMINDKS_KTKOD.AsString  ;
          ADOCommand1.Execute;
          if ReszletesNaplo=1 then WriteLogFile(koltseglogfile,' SQL exec: '+ADOCommand1.CommandText);
          T_GYUJTOMIND.Next;
          Continue;
      end;

      /////////////////////////////////////////////////////////
      if ADOConnectionAlap.InTransaction then
       ADOConnectionAlap.CommitTrans;

      Try      // Adatok tárolása
        if  leiras='Hűtő tankolás' then
          rendsz:=rendsz;
        ADOConnectionAlap.BeginTrans;
        if ReszletesNaplo=1 then WriteLogFile(koltseglogfile,' feldolg. 3.');
        if hozzaadni then
        begin
          ujkod:=T_KOLTSEG.FieldByname('KS_KTKOD').AsInteger;
				  ADOCommand1.CommandText:='update koltseg set '+
          'KS_OSSZEG='+sosszeg+','+
          'KS_ERTEK='+sertek+
          ' WHERE KS_KTKOD = '+IntToStr( ujkod)  ;
          ADOCommand1.Execute;
          if ReszletesNaplo=1 then WriteLogFile(koltseglogfile,' SQL exec: '+ADOCommand1.CommandText);
        end
        else
        begin
      	 ujkod	:= GetNextCode('koltseg');
         ADOCommand1.CommandText:='INSERT INTO KOLTSEG (KS_KTKOD,KS_RENDSZ,KS_JAKOD,KS_DATUM,KS_IDO,KS_LEIRAS,KS_MEGJ,KS_JARAT,KS_KMORA,KS_UZMENY,'+
          'KS_VALNEM,KS_ARFOLY,KS_OSSZEG,KS_ERTEK,KS_AFASZ,KS_AFAERT,KS_AFAKOD,KS_FIMOD,KS_ORSZA,KS_TIPUS,'+
          'KS_ATLAG,KS_TELE,KS_TETAN,KS_TELES,KS_SOKOD,KS_SONEV,KS_KTIP,KS_NC_KM,KS_NC_USZ,KS_NC_UME,KS_NC_DAT,KS_NC_IDO,KS_BEODAT ) '+
          ' VALUES ('+IntToStr(ujkod)+ ','+#39+rendsz+#39+','+#39+jakod+#39+','+#39+datum+#39+','+#39+ido+#39+','+#39+leiras+#39+','+#39+megj+#39+','+#39+jaratkod+#39+','+skm+','+smenny+
          ','+#39+valnem+#39+','+sarfolyam+','+sosszeg+','+sertek+','+'0'+','+'0'+','+#39+'103'+#39+','+#39+fimod+#39+','+#39+orsz+#39+','+tipus +
          ','+'0'+','+tele+','+'0'+','+#39+'N'+#39+','+#39+skod+#39+','+#39+snev+#39+','+IntToStr(KTIP)+','+snavc_km+','+snavc_uza_szint+','+snavc_uza_lit+','+#39+navc_datum+#39+','+#39+navc_ido+#39+','+#39+ma+#39 +' )' ;

         ADOCommand1.Execute;
         if ReszletesNaplo=1 then WriteLogFile(koltseglogfile,' SQL exec: '+ADOCommand1.CommandText);
        end;
        if tipus='0' then
        begin
				  ADOCommand1.CommandText:='update koltseg set KS_TELE=0 WHERE KS_RENDSZ = '''+rendsz+''' '+
					' AND KS_KTIP  = '+IntToStr(KTIP)+
					' AND KS_KMORA  = '+FloatToStr(km)+
					' AND KS_DATUM+KS_IDO  < '''+datum+ido+''' '+
					' AND KS_TIPUS  = '+tipus ;
          ADOCommand1.Execute;
        end;

        ADOCommand1.CommandText:='UPDATE KOLTSEG_GYUJTO set KS_HMEGJ='+#39+#39+',KS_JARAT='+#39+jaratkod+#39+',KS_JAKOD='+#39+jakod+#39+',KS_SOKOD='+#39+skod+#39+',KS_SONEV='+#39+snev+#39+',KS_LEIRAS='+#39+leiras+#39+
          ' ,KS_FELDAT=getdate(),KS_KTKOD2='+IntToStr(ujkod)+' where KS_KTKOD='+T_GYUJTOMINDKS_KTKOD.AsString  ;
        ADOCommand1.Execute;
        if ReszletesNaplo=1 then WriteLogFile(koltseglogfile,' SQL exec: '+ADOCommand1.CommandText);

        if ReszletesNaplo=1 then WriteLogFile(koltseglogfile,' feldolg. 4.');
        inc(dbfel);
        Edit2.Text:=IntToStr(dbfel);
        Edit2.Update;
        Application.ProcessMessages;

        if ADOConnectionAlap.InTransaction then
         ADOConnectionAlap.CommitTrans;
      Except
        on E: exception do begin
          if ADOConnectionAlap.InTransaction then begin
            WriteLogFile(koltseglogfile,' feldolg. rollback: '+E.Message);
            ADOConnectionAlap.RollbackTrans;
            end;
          end;
      End;
      T_GYUJTOMIND.Next;
   end;
   T_GYUJTOMIND.Close;
   //ShowMessage('Kész!');
   Result:=dbfel;
end;

procedure TForm1.Button3Click(Sender: TObject);
begin
   MegbizasFigyel;
end;

procedure TForm1.MegbizasFigyel;
var
  ido, fn, sor,sor0,sor1,sor2,sor20,sor21,sor3,smido,ssido,sor01,sor02, sor4: string;
  ftido,ltido, aido, mido,sido : TdateTime;
  nemteljesult,NTFel,NTLe, kiemelt, ujrakuldott : boolean;
  gkkat, fkod,fnev,femail,aemail,hemail,snev1,snev2,semail1,semail2,stel1,stel2, emailcim, skiemelt, targya, rendsz, alnev, masolat : string;
  IdMessage1: TIdMessage;
  p_kiemelt, p_sofor, p_ujra, p_ujra_norm, p_ujra_kiem, p_keses, gkpoz_elavul: integer;
  l_kiemelt, l_mir, l_sof, alvallalkozo, hutos, felfuggesztve, sofornekcsak1, vanerkezesido : boolean;
  l_k_ido, l_m_ido, l_s_ido : TDateTime;
  torzs_k, torzs_ks, torzs_m, torzs_s, torzs_a , torzs_: TStringList;
  sftido, sltido : string;
  cc_kiemelt,cc_mir,cc_sof,cc_alvallalkozo, cc_hutos, alv_telefon, poz_dat,poz_hely,link,poz_sx,poz_sy,poz_seb, cim : string;
  kiem_k, kiem_v, dolt_k,dolt_v, LogS: string;
  s_mbko,s_cim,s_fel_le,s_fel,s_le,s_sofor,s_gkpoz,s_link,s_lmegj,s_alv,s_megj1,s_megj2,s_init,s_keses,s_vevo : string;
  megbizaslogfile, Res: string;
begin
  if INI.ReadString('NEMTELJESULT','OK','I')<>'I' then
    exit;

	p_kiemelt:=StrToIntDef(Szotar_('376','001','M'),0);
	p_sofor:=StrToIntDef( Szotar_('376','002','M'),0);
	p_ujra_norm:=StrToIntDef( Szotar_('376','003','M'),0);
	p_ujra_kiem:=StrToIntDef( Szotar_('376','005','M'),0);
	gkpoz_elavul:=StrToIntDef( Szotar_('376','004','M'),120);
	aemail:=Szotar_('376','010','M');
	hemail:=Szotar_('376','012','M');
  cc_kiemelt:=Szotar_('376','020','M');
  cc_mir:=Szotar_('376','021','M');
  cc_sof:=Szotar_('376','022','M');
  cc_alvallalkozo:=Szotar_('376','023','M');
  cc_hutos:=Szotar_('376','024','M');
  sofornekcsak1:=Szotar_('376','009','M')<>'';

  kiem_k:='<strong>';
  kiem_v:='</strong>';
  dolt_k:='<em>';
  dolt_v:='</em>';
  s_init:='<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-2">' ;

  sor01:='Ez egy automatikus levél. Ne válaszolj rá!';
  sor02:='------------------------------------------';

  //p_kiemelt:=30;
  //p_sofor:=10;
  //p_ujra:=60;
  // Timer1.Enabled:=False;
  torzs_k :=TStringList.Create;
  torzs_ks:=TStringList.Create;
  torzs_m :=TStringList.Create;
  torzs_s :=TStringList.Create;
  torzs_a :=TStringList.Create;
  torzs_  :=TStringList.Create;

  fn:= INI.ReadString('NEMTELJESULT','FILE','AKI_MB.log');
  megbizaslogfile		:= EXEPath+'LOG\'+fn ;
  WriteLogFile(megbizaslogfile,'START '+DateTimeToStr(now)+'  '+ADOConnectionAlap.DefaultDatabase);

  T_MEGSEGED.Close;
  T_MEGSEGED.Parameters[0].Value:=MaiDatum;
  T_MEGSEGED.Open;
  T_MEGSEGED.First;

  // S:= 'select s.*,m.mb_venev from MEGSEGED s, MEGBIZAS m '+
  //      ' where (MB_MBKOD=MS_MBKOD)and (MS_DATUM= '''+MaiDatum+''') and '+
  //      ' ((MS_FETIDO='''' and MS_FERKIDO='''' and MS_TIPUS=0) or '+
  //      ' (MS_LETIDO='''' and MS_LERKIDO='''' and MS_TIPUS=1) ) '+
  //      ' and m.MB_STATU=100 '+  // csak az érvényes megbízások rakodásai
  //      ' order by MS_MBKOD, MS_SORSZ ';

  while not T_MEGSEGED.Eof do begin
   try
       torzs_k.Clear;
       torzs_ks.Clear;
       torzs_m.Clear;
       torzs_s.Clear;
       torzs_a.Clear;
       torzs_.Clear;
       skiemelt:='';
       nemteljesult:=False;
       kiemelt:=False;
       NTFel:=False;
       NTLe:=False;
       l_kiemelt:=False;
       l_mir:=False;
       s_lmegj:='';

       mido:=T_MEGSEGEDMS_M_MAIL.Value;
       sido:=T_MEGSEGEDMS_S_MAIL.Value;
       rendsz:= T_MEGSEGEDMS_RENSZ.Value ;
       alvallalkozo:= T_MEGSEGEDALNEV.Value<>'';
       hutos:= T_MEGSEGEDHUTOS.Value=1;
       felfuggesztve:=T_MEGSEGEDFELFUG.Value=1;
       if felfuggesztve then
       begin
        T_MEGSEGED.Next;
        Continue;
       end;
       alnev:=T_MEGSEGEDALNEV.Value;
       if (not alvallalkozo)and(rendsz<>'') then
       begin
        Poziciok.Close;
        Poziciok.Parameters[0].Value:=rendsz;
        Poziciok.Open;
        Poziciok.First;
        poz_dat:=PoziciokPO_DAT.AsString;
        poz_hely:=PoziciokPO_GEKOD.Value;
        poz_sx:=PoziciokPO_SZELE.AsString;
        poz_sy:=PoziciokPO_HOSZA.AsString;
        poz_sx:=StringReplace(poz_sx,',','.',[rfReplaceAll]);
        poz_sy:=StringReplace(poz_sy,',','.',[rfReplaceAll]);
        poz_seb:='  '+PoziciokPO_SEBES.AsString+' km/h';
        Poziciok.Close;
       end;
       if alvallalkozo then
       begin
         Partner.Close;
         Partner.Parameters[0].Value:=T_MEGSEGEDALVKOD.Value;
         Partner.Open;
         Partner.First;
         alv_telefon:=PartnerV_TEL.Value;
         Partner.Close;
       end;
       ////////////////////////////
       // FELRAKÓ
       if  (T_MEGSEGEDMS_FETIDO.AsString='')and(T_MEGSEGEDMS_FERKIDO.AsString='')and(T_MEGSEGEDMS_TIPUS.AsString='0') then     // nincs kitöltve a tény idő
       //if  (T_MEGSEGEDMS_FETIDO.AsString='')and(T_MEGSEGEDMS_TIPUS.AsString='0') then     // nincs kitöltve a tény idő
       begin
        vanerkezesido:= T_MEGSEGEDMS_FERKIDO.AsString<>'';
    {    ido:= T_MEGSEGEDMS_FELIDO.AsString;
        if T_MEGSEGEDMS_FELIDO2.AsString<>'' then   // -ig ki van töltve
          if  T_MEGSEGEDMS_FELIDO2.AsString < T_MEGSEGEDMS_FELIDO.AsString   then   // 20:00 - 06:00
            ido:= '23:59'
          else
            ido:= T_MEGSEGEDMS_FELIDO2.AsString;
     }
        if T_MEGSEGEDMS_FELIDO2.AsString<>'' then   // -ig ki van töltve
         if T_MEGSEGEDMS_FELDAT2.AsString<>'' then
          ftido:= StrToDateTimeDef(T_MEGSEGEDMS_FELDAT2.AsString+' '+T_MEGSEGEDMS_FELIDO2.AsString+':00',now)
         else
          ftido:= StrToDateTimeDef(T_MEGSEGEDMS_FELDAT.AsString+' '+T_MEGSEGEDMS_FELIDO2.AsString+':00',now)
        else
          ftido:= StrToDateTimeDef(T_MEGSEGEDMS_FELDAT.AsString+' '+T_MEGSEGEDMS_FELIDO.AsString+':00',now);

        //ftido:= StrToDateTimeDef(T_MEGSEGEDMS_FELDAT.AsString+' '+ido+':00',now);

        aido:= Now;
        //aido:=StrToDateTime( maidatum);
        if (not vanerkezesido)and(p_kiemelt>0)and(mido=0)and((T_MEGSEGEDMS_KIFON.Value=1)or(T_MEGSEGEDMS_FDARU.Value=1)) then  // kiemelt fontosságú, v. daru szükséges  és a menetirányító még nem kapott levelet
        begin
          kiemelt:=True;
          l_kiemelt:=True;
          if T_MEGSEGEDMS_KIFON.Value=1 then
            skiemelt:=' KIEMELT FONTOSSÁGÚ';
          if T_MEGSEGEDMS_FDARU.Value=1 then
            skiemelt:=skiemelt+' DARUS';
          //aido:=DateTimeMinusz(aido,p_kiemelt);
          ftido:=DateTimeMinusz(ftido,p_kiemelt);
        end;
        NTFel:=(aido > ftido) ;
        nemteljesult:= nemteljesult OR (aido > ftido) ;
        if kiemelt then
          ftido:=DateTimeMinusz(ftido,-p_kiemelt);
        sftido:=DateTimeToStr(ftido);
        sftido:=copy(sftido,1,length(sftido)-3);

        ido:= T_MEGSEGEDMS_LERIDO.AsString;
        if T_MEGSEGEDMS_LERIDO2.AsString<>'' then   // -ig ki van töltve
            ido:= T_MEGSEGEDMS_LERIDO2.AsString;
        sltido:= T_MEGSEGEDMS_LERDAT.AsString+' '+ido;
        //end;
       end;

       // ELRAKÓ
       /////////////////////////////////////////
       if  (T_MEGSEGEDMS_LETIDO.AsString='')and(T_MEGSEGEDMS_LERKIDO.AsString='')and(T_MEGSEGEDMS_TIPUS.AsString='1') then     // nincs kitöltve a tény idő
       //if  (T_MEGSEGEDMS_LETIDO.AsString='')and(T_MEGSEGEDMS_TIPUS.AsString='1') then     // nincs kitöltve a tény idő
       begin
        vanerkezesido:= T_MEGSEGEDMS_LERKIDO.AsString<>'';
    {    ido:= T_MEGSEGEDMS_LERIDO.AsString;
        if T_MEGSEGEDMS_LERIDO2.AsString<>'' then   // -ig ki van töltve
          if  T_MEGSEGEDMS_LERIDO2.AsString < T_MEGSEGEDMS_LERIDO.AsString   then   // 20:00 - 06:00
            ido:= '23:59'
          else
            ido:= T_MEGSEGEDMS_LERIDO2.AsString;
        ltido:= StrToDateTimeDef(T_MEGSEGEDMS_LERDAT.AsString+' '+ido+':00',now);
        }
        if T_MEGSEGEDMS_LERIDO2.AsString<>'' then   // -ig ki van töltve
         if T_MEGSEGEDMS_LERDAT2.AsString<>'' then
          ltido:= StrToDateTimeDef(T_MEGSEGEDMS_LERDAT2.AsString+' '+T_MEGSEGEDMS_LERIDO2.AsString+':00',now)
         else
          ltido:= StrToDateTimeDef(T_MEGSEGEDMS_LERDAT.AsString+' '+T_MEGSEGEDMS_LERIDO2.AsString+':00',now)
        else
          ltido:= StrToDateTimeDef(T_MEGSEGEDMS_LERDAT.AsString+' '+T_MEGSEGEDMS_LERIDO.AsString+':00',now);


        aido:= Now;
        //aido:=StrToDateTime( maidatum);
        if (not vanerkezesido)and(p_kiemelt>0)and(mido=0)and((T_MEGSEGEDMS_KIFON.Value=1)or(T_MEGSEGEDMS_LDARU.Value=1)) then  // kiemelt fontosságú, v. daru szükséges
        begin
          kiemelt:=True;            // kiemelt megbízás
          l_kiemelt:=True;          // kiemelt levelet kell küldeni
          if T_MEGSEGEDMS_KIFON.Value=1 then
            skiemelt:=' KIEMELT FONTOSSÁGÚ';
          if T_MEGSEGEDMS_LDARU.Value=1 then
            skiemelt:=skiemelt+' DARUS';
          //aido:=DateTimeMinusz(aido,p_kiemelt);
          ltido:=DateTimeMinusz(ltido,p_kiemelt);
        end;
        NTLe:=(aido > ltido) ;
        nemteljesult:= nemteljesult OR (aido > ltido) ;
        if kiemelt then
          ltido:=DateTimeMinusz(ltido,-p_kiemelt);

        sltido:=DateTimeToStr(ltido);

        sltido:=copy(sltido,1,length(sltido)-3);

        ido:= T_MEGSEGEDMS_FELIDO.AsString;
        if T_MEGSEGEDMS_FELIDO2.AsString<>'' then   // -ig ki van töltve
            ido:= T_MEGSEGEDMS_FELIDO2.AsString;
        sftido:= T_MEGSEGEDMS_FELDAT.AsString+' '+ido;
        //end;
       end;

       //////////////////////////////////////
       if nemteljesult then
       begin
         l_mir:=not l_kiemelt;
         l_sof:=False ;  // not l_kiemelt;
         ujrakuldott:=False;
         //felfuggesztve:=False;
         if not l_kiemelt then
         begin
           l_m_ido:=now;
           if mido<>0 then       // már volt küldés a menetirányítónak
           begin
            l_m_ido:=mido;
            //if (not felfuggesztve) and (mido < DateTimeMinusz(now,p_ujra_norm)) then  // letelt az újraküldési idő; megint el kell küldeni
            if kiemelt then
              p_ujra:=p_ujra_kiem
            else
              p_ujra:=p_ujra_norm;
            if (not felfuggesztve) and (mido < DateTimeMinusz(now,p_ujra)) then  // letelt az újraküldési idő; megint el kell küldeni
            begin
              l_mir:=True;     // menetiráyítónak kell küldeni    újra
              l_m_ido:=now;
              ujrakuldott:=True;
            end
            else
            begin
              l_mir:=False;
            end;
           end;

           if sido<>0 then       // már volt küldés a sofőrnek
           begin
            l_s_ido:=sido;
            if kiemelt then
              p_ujra:=p_ujra_kiem
            else
              p_ujra:=p_ujra_norm;
            if (not sofornekcsak1)and(not felfuggesztve)and(p_sofor>0)and((semail1+semail2)<>'')and(sido < DateTimeMinusz(now,p_ujra)) then  // letelt az újraküldési idő; megint el kell küldeni
            begin
              l_sof:=True;          // sofőrnek kell küldeni  újra
              l_s_ido:=now;
            end
            else
            begin
              l_sof:=False;
            end;
           end
           else // még nem küldtünk a sofőrnek
           begin
            if (not felfuggesztve)and(p_sofor>0)and((semail1+semail2)<>'')and(l_m_ido < DateTimeMinusz(now,p_sofor)) then  // már a sofőrnek is el kell küldeni
            //if l_m_ido < DateTimeMinusz(l_m_ido,p_sofor) then  // már a sofőrnek is el kell küldeni
            begin
               l_sof:=True;    // sofőrnek kell küldeni
            end;
           end;
         end;
         snev1:=T_MEGSEGEDMS_SNEV1.Value;
         snev2:=T_MEGSEGEDMS_SNEV2.Value;
         gkkat:=T_MEGSEGEDMS_GKKAT.Value;
        // if rendsz='' then gkkat:='';
         {Szotar.Close;
         Szotar.Parameters[0].Value:='340';
         Szotar.Parameters[1].Value:=gkkat;
         Szotar.Open;
         Szotar.First;   }
         fnev:=Szotar_('340',gkkat,'L');  // Szotarsz_leiro.Value;
         /////////////////
         Dolgozo.Close;
         Dolgozo.Parameters[0].Value:= fnev;
         Dolgozo.Open;
         Dolgozo.First;
         fkod:=Dolgozodo_kod.Value;
         femail:=Dolgozodo_email.Value;
         ////////////////
         Dolgozo.Close;
         Dolgozo.Parameters[0].Value:= snev1;
         Dolgozo.Open;
         Dolgozo.First;
         semail1:=Dolgozodo_email.Value;
         stel1:=Dolgozodo_telef.Value;
         ////////////////
         Dolgozo.Close;
         Dolgozo.Parameters[0].Value:= snev2;
         Dolgozo.Open;
         Dolgozo.First;
         semail2:=Dolgozodo_email.Value;
         stel2:=Dolgozodo_telef.Value;
         ////////////////
         Dolgozo.Close;
         if NTFel then
         begin
            s_mbko:=T_MEGSEGEDMS_MBKOD.AsString+'/'+T_MEGSEGEDMS_SORSZ.AsString ;
            s_cim:=skiemelt+kiem_k+ '  FELRAKÁS  '+kiem_v+'('+rendsz+') ('+gkkat+')'+IfThen(hutos,' (HŰTŐS)','');
            s_vevo:='Megbízó: '+T_MEGSEGEDmb_venev.Value;
            s_fel := 'Felrakó: ('+sftido+') '+T_MEGSEGEDMS_TEFNEV.Value+', '+T_MEGSEGEDMS_TEFOR.Value+' '+T_MEGSEGEDMS_TEFIR.Value +' '+T_MEGSEGEDMS_TEFTEL.Value+' '+T_MEGSEGEDMS_TEFCIM.Value ;
            s_le:= 'Lerakó : ('+sltido+') '+T_MEGSEGEDMS_TENNEV.Value+', '+T_MEGSEGEDMS_TENOR.Value+' '+T_MEGSEGEDMS_TENIR.Value +' '+T_MEGSEGEDMS_TENTEL.Value+' '+T_MEGSEGEDMS_TENCIM.Value ;
            sor21:='Idő: '+sftido ;
            sor:=s_mbko+s_fel;
            //p_keses:= MinutesBetween(StrToDateTime(MaiDatum),StrToDateTime(sftido));
            p_keses:= MinutesBetween(now,StrToDateTime(sftido));
            //s_keses:=' (Késedelem: '+ IntToStr( p_keses div 60 )+':'+IntToStr(p_keses mod 60)+')';
            s_keses:=' (Késedelem: '+Feltolt(IntToStr( p_keses div 60 ),'0',2)+':'+Feltolt(IntToStr(p_keses mod 60),'0',2)+IfThen(ujrakuldott,' !)',')');
            s_sofor:= 'Sofőr: '+snev1+' ('+stel1+')('+semail1+') ' ;
            s_gkpoz:='Gépkocsi pozíció: ('+copy(poz_dat,1,length(poz_dat)-3)+poz_seb+') '+poz_hely;

            s_lmegj:='';
            //if MinutesBetween(StrToDateTime(MaiDatum),StrToDateTime(poz_dat))> gkpoz_elavul then
            if (poz_dat<>'')and(MinutesBetween(now,StrToDateTime(poz_dat))> gkpoz_elavul) then
              s_lmegj:=' !!! Régi gépkocsi pozíció !!!';
            cim :=T_MEGSEGEDMS_TEFOR.Value+' '+T_MEGSEGEDMS_TEFIR.Value +' '+T_MEGSEGEDMS_TEFTEL.Value+' '+T_MEGSEGEDMS_TEFCIM.Value ;
            // s_link:='http://maps.google.com/maps?saddr='+poz_sx+','+poz_sy+'+to:'+cim+'&via=1&t=m';
            s_link:='http://maps.google.com/maps?saddr='+poz_sx+','+poz_sy+'&daddr='+cim+'&t=m';
            s_link:= '<a href="'+s_link+'" title="Google térkép">  (Térkép)</a><br>';
            if (p_sofor>0)and((semail1+semail2)<>'') then
              s_megj1:=dolt_k+ 'Amennyiben nem történik változás, a sofőr '+IntToStr(p_sofor)+' perc múlva értesítést kap.'+dolt_v
            else
              s_megj1:='';

            s_alv:='Alvállalkozó: '+alnev;
            if snev2<>'' then  s_sofor:=sor3+snev2+' ('+stel2+')('+semail2+')' ;
            if (Trim(snev1)+Trim(snev2))='' then
              s_sofor:='NINCS sofőr megadva!';

            WriteLogFile(megbizaslogfile,sor);
         end;
         if NTLe then
         begin
            s_mbko:=T_MEGSEGEDMS_MBKOD.AsString+'/'+T_MEGSEGEDMS_SORSZ.AsString ;
            //s_cim:=skiemelt+ ' LERAKÁS '+rendsz+' ('+gkkat+')';
           // s_cim:=skiemelt+ ' LERAKÁS ('+rendsz+') ('+gkkat+')';
            s_cim:=skiemelt+kiem_k+ '  LERAKÁS  '+kiem_v+'('+rendsz+') ('+gkkat+')'+IfThen(hutos,' (HŰTŐS)','');
            s_vevo:='Megbízó: '+T_MEGSEGEDmb_venev.Value;
            s_fel := 'Felrakó: ('+sftido+') '+T_MEGSEGEDMS_TEFNEV.Value+', '+T_MEGSEGEDMS_TEFOR.Value+' '+T_MEGSEGEDMS_TEFIR.Value +' '+T_MEGSEGEDMS_TEFTEL.Value+' '+T_MEGSEGEDMS_TEFCIM.Value ;
            s_le:= 'Lerakó : ('+sltido+') '+T_MEGSEGEDMS_TENNEV.Value+', '+T_MEGSEGEDMS_TENOR.Value+' '+T_MEGSEGEDMS_TENIR.Value +' '+T_MEGSEGEDMS_TENTEL.Value+' '+T_MEGSEGEDMS_TENCIM.Value ;
            sor21:='Idő: '+sftido ;
            sor:=s_mbko+s_le;
            //p_keses:= MinutesBetween(StrToDateTime(MaiDatum),StrToDateTime(sltido));
            p_keses:= MinutesBetween(now,StrToDateTime(sltido));
            //s_keses:=' (Késedelem: '+ IntToStr( p_keses div 60 )+':'+IntToStr(p_keses mod 60)+IfThen(ujrakuldott,' !)',')');
            s_keses:=' (Késedelem: '+Feltolt(IntToStr( p_keses div 60 ),'0',2)+':'+Feltolt(IntToStr(p_keses mod 60),'0',2)+IfThen(ujrakuldott,' !)',')');
            s_sofor:= 'Sofőr: '+snev1+' ('+stel1+')('+semail1+') ' ;
            //s_gkpoz:='Gépkocsi pozíció: ('+poz_dat+') '+poz_hely;
            s_gkpoz:='Gépkocsi pozíció: ('+copy(poz_dat,1,length(poz_dat)-3)+poz_seb+') '+poz_hely;
            s_lmegj:='';
            if (poz_dat<>'')and(MinutesBetween(now,StrToDateTime(poz_dat))> gkpoz_elavul) then
            //if MinutesBetween(StrToDateTime(MaiDatum),StrToDateTime(poz_dat))> gkpoz_elavul then
              s_lmegj:=' !!! Régi gépkocsi pozíció !!!';
            cim:= T_MEGSEGEDMS_TENOR.Value+' '+T_MEGSEGEDMS_TENIR.Value +' '+T_MEGSEGEDMS_TENTEL.Value+' '+T_MEGSEGEDMS_TENCIM.Value ;
            // s_link:='http://maps.google.com/maps?saddr='+poz_sx+','+poz_sy+'+to:'+cim+'&via=1&t=m';
            s_link:='http://maps.google.com/maps?saddr='+poz_sx+','+poz_sy+'&daddr='+cim+'&t=m';
            s_link:= '<a href="'+s_link+'" title="Google térkép">  (Térkép)</a><br>';
            //s_megj1:='Amennyiben nem történik változás, a sofőr '+IntToStr(p_sofor)+' perc múlva értesítést kap.';
            if (p_sofor>0)and((semail1+semail2)<>'') then
              s_megj1:=dolt_k+ 'Amennyiben nem történik változás, a sofőr '+IntToStr(p_sofor)+' perc múlva értesítést kap.'+dolt_v
            else
              s_megj1:='';

            s_alv:='Alvállalkozó: '+alnev;
            if snev2<>'' then  s_sofor:=sor3+snev2+' ('+stel2+')('+semail2+')' ;
            if (Trim(snev1)+Trim(snev2))='' then
              s_sofor:='NINCS sofőr megadva!';

            WriteLogFile(megbizaslogfile,sor);
         end;
         ///////////////////////////
            /////////// kiemelt
            torzs_k.Add(s_init);
            torzs_k.Add(s_mbko+s_cim);
            torzs_k.Add(sortores);
            torzs_k.Add(sortores);
            torzs_k.Add(s_vevo);
            torzs_k.Add(sortores);
            torzs_k.Add(s_fel);
            torzs_k.Add(sortores);
            torzs_k.Add(s_le);
            torzs_k.Add(sortores);
            torzs_k.Add(sortores);
            torzs_k.Add(s_sofor);
            torzs_k.Add(sortores);
            torzs_k.Add(sortores);
            if (rendsz<>'')and(poz_hely<>'') then
            begin
              torzs_k.Add(s_gkpoz) ;
              //torzs_k.Add(sortores);
              torzs_k.Add(s_link+s_lmegj) ;
            end;
            // SMS
            torzs_ks.Add('Késedelmes'+StringReplace(StringReplace(s_cim,kiem_k,'',[]),kiem_v,'',[]));
            if NTFel then
              torzs_ks.Add(s_fel)
            else
              torzs_ks.Add(s_le);
            torzs_ks.Add(s_sofor);

            ///////////////////  késés
            torzs_m.Add(s_init);
            torzs_m.Add(s_mbko+s_cim+s_keses);
            torzs_m.Add(sortores);
            torzs_m.Add(sortores);
            torzs_m.Add(s_vevo);
            torzs_m.Add(sortores);
            torzs_m.Add(s_fel);
            torzs_m.Add(sortores);
            torzs_m.Add(s_le);
            torzs_m.Add(sortores);
            torzs_m.Add(sortores);
            torzs_m.Add(s_sofor);
            torzs_m.Add(sortores);
            torzs_m.Add(sortores);
            if (rendsz<>'')and(poz_hely<>'') then
            begin
              torzs_m.Add(s_gkpoz) ;
              //torzs_m.Add(sortores);
              torzs_m.Add(s_link+s_lmegj) ;
            end;
            if copy(s_sofor,1,5)<>'NINCS' then
            begin
              torzs_m.Add(sortores);
              torzs_m.Add(s_megj1);
            end;

            /////////////////// alválallkozó
            torzs_a.Add(s_init);
            torzs_a.Add(s_mbko+s_cim+s_keses);
            torzs_a.Add(sortores);
            torzs_a.Add(sortores);
            torzs_a.Add(s_vevo);
            torzs_a.Add(sortores);
            torzs_a.Add(s_fel);
            torzs_a.Add(sortores);
            torzs_a.Add(s_le);
            torzs_a.Add(sortores);
            torzs_a.Add(sortores);
            torzs_a.Add(s_alv);

            ///////////////// sofőr
            torzs_s.Add('Késedelmes'+StringReplace(StringReplace(s_cim,kiem_k,'',[]),kiem_v,'',[]));
            if NTFel then
              torzs_s.Add(s_fel)
            else
              torzs_s.Add(s_le);

         ///////////////////////////
         IdMessage1:=TIdMessage.Create(Self);
         IdMessage1.ContentType:='text/html';
         // --- 20151124 NagyP -------
         IdMessage1.CharSet:= 'UTF-8';
         // --------------------------
         masolat:='';
         semail1:='';
         semail2:='';

         ///////////////////
         // LEVÉL KÜLDÉSE //
         ///////////////////
         emailcim:=femail;
         masolat:=cc_mir;
         targya:='';
         if l_kiemelt then  ////////// Kiemelt, darus
         begin
            targya:='Kiemelt fontosságú, vagy darus megbízás. '+DateTimeToStr(now);
            torzs_:=torzs_k;
         end;
         if l_mir then       // normál késés
         begin
            targya:='Megbízás késedelem. '+DateTimeToStr(now);
            torzs_:=torzs_m;
         end;
         if alvallalkozo then
         begin
            torzs_:=torzs_a;
            emailcim:=aemail;
            masolat:=cc_alvallalkozo;
         end;
         if hutos then
         begin
            emailcim:=hemail;
            masolat:=cc_hutos;
         end;

         if targya<>'' then begin

           // Ha az utolsó küldés munkaidőn kívüli és most is munkaidőn kívül vagyunk, akkor nem küldünk levelet.
           // vagyis akkor küldünk, ha most munkaidő van, vagy az utolsó küldés munkaidőben volt, vagy még nem volt küldés.
           // Ezzel elkerüljük az esti késésről reggelre felgyűlő levélkupacot

           LogS:='  ?Time ['+IntToStr(T_MEGSEGEDMS_MBKOD.Value)+'/'+IntToStr(T_MEGSEGEDMS_SORSZ.Value)+']';
           if (mido <> 0) then begin  // volt már küldés   mido = T_MEGSEGEDMS_M_MAIL.value
             LogS:= LogS +  ' Utolsó küldés: '+formatdatetime('yyyy-mm-dd HH:mm', mido);
             if Munkaido(mido) then
                LogS:= LogS + ' (munkaidő) '
             else LogS:= LogS + ' (NEM munkaidő) ';
             end
           else begin
             LogS:= LogS + ' Még nem volt küldés. ';
             end;
           LogS:= LogS + ' Most: '+formatdatetime('yyyy-mm-dd HH:mm', now);
           if Munkaido(now) then
              LogS:= LogS + ' (munkaidő) '
           else LogS:= LogS + ' (NEM munkaidő) ';
           WriteLogFile(megbizaslogfile, LogS);

           If Munkaido(now) or (mido = 0) or Munkaido(mido) then begin
            Try

             EmailDlg.Mail(IdMessage1,emailcim,masolat,targya,torzs_,True,'Megbízás ellenőrző');
             Res:= EmailDlg.SendMail(IdMessage1);
             if Res = '' then begin         // ha elküldte, akkor bejegyezni az időt
                WriteLogFile(megbizaslogfile,'  '+IdMessage1.Recipients.EMailAddresses+';'+IdMessage1.CCList.EMailAddresses);
                T_MEGSEGED.Edit;
                T_MEGSEGEDMS_M_MAIL.Value:=now;
                T_MEGSEGED.Post;
                end; // if
             Except
               on E: exception do
                  WriteLogFile(megbizaslogfile,'!! EXCEPTION: '+E.Message);
                End;  // try-except
             end  // if munkaidő
           else begin
             WriteLogFile(megbizaslogfile, '  Munkaidőn kívül nem küldjük el.');
             end;  // else
           end;  // if targya

         /////////////////////////////////// Sofőrnek
         if l_sof then
         begin
          //targya:='Késedelem';
          targya:='';
          emailcim:=semail1+';'+semail2;
          masolat:=cc_sof;
          // teszteléshez kivéve NagyP
          Try
           EmailDlg.Mail(IdMessage1,emailcim,masolat,targya,torzs_s,False,'Megbízás ellenőrző');
           Res:= EmailDlg.SendMail(IdMessage1);
           if  Res = '' then          // ha elküldte, akkor bejegyezni az időt
           begin
            WriteLogFile(megbizaslogfile,'  '+IdMessage1.Recipients.EMailAddresses+';'+IdMessage1.CCList.EMailAddresses);
            T_MEGSEGED.Edit;
            T_MEGSEGEDMS_S_MAIL.Value:=now;
            T_MEGSEGED.Post;
           end;
          Except
          End;
            // }
         end;
         ///////////////////////////
       end;
   except
      on E: Exception do begin
        WriteLogFile(megbizaslogfile, formatdatetime('yyyy.mm.dd. HH:mm:ss:zzz', Now)+' Hiba: '+E.Message);
        end;
      end;  // try-except
   T_MEGSEGED.Next;
  end; // while
  T_MEGSEGED.Close;
  WriteLogFile(megbizaslogfile,'END   '+DateTimeToStr(now));
  WriteLogFile(megbizaslogfile,' ');
  sleep(2000);
end;

procedure TForm1.Button4Click(Sender: TObject);
var
  darab: integer;
  XMLResult, Res: string;
  IdMessage1: TIdMessage;
  MyBody: TStringList;
begin
 // PozicioKuldesFeltolt;
  // XMLResult:= GetMNBArfolyamServiceSoap(False, '', nil).GetCurrentExchangeRates;
  // Memo1.Lines.Assign(ExchangeRatesFromXML(XMLResult));
  {
  frmXMLViewer:=TfrmXMLViewer.Create(Self);
  with frmXMLViewer do begin
      ShowModal;
      Free;
      end;  // with
      }
  MyBody:= TStringList.Create;
  MyBody.Add('Kőkút cipőfűző Sofőr: Veres Károly');
  IdMessage1:=TIdMessage.Create(Self);
  IdMessage1.CharSet:= 'UTF-8';
  EmailDlg.Mail(IdMessage1,'nagyp69@gmail.com','','Kőkút cipőfűző', MyBody, True, 'NAGYP ellenőrző');
  Res:= EmailDlg.SendMail(IdMessage1);
  MyBody.Free;
  NoticeKi('Küldés eredmény: '+Res);

 //  Res:= SendLevel('+36209588785@jssms.hu', '', 'TESZT SMS', 'TESZT5 - Kőkút cipőfűző, ÁRVÍZTŰRŐ TÜKÖRFÚRÓ.', True, 'Szerviz üzenetközpont');
  // Res:= SendLevel('nagyp69@gmail.com', '', 'TESZT SMS', 'TESZT2 - Kőkút cipőfűző, ÁRVÍZTŰRŐ TÜKÖRFÚRÓ.', True, 'Szerviz üzenetközpont');

  // TIdSMTP.QuickSend('sbs.jsspeed.local', 'TESZT SMS', 'nagyp69@gmail.com', 'Szerviz üzenetközpont', 'Kőkút cipőfűző, ÁRVÍZTŰRŐ TÜKÖRFÚRÓ.', 'text', '', '');
  // NoticeKi('Küldés eredmény: '+Res);



end;

procedure TForm1.Button5Click(Sender: TObject);
begin
   GyorshajtasSMS;
end;

procedure TForm1.GyorshajtasSMS;
const
  Elavulas_percben = '10';
var
  RENDSZAM, MIKOR, SEBES, LOCAT, S, cimzett, cimzett_igazi, Szoveg, Res, DOKOD: string;
  i: integer;
  Lezarhato: boolean;
  cimzettlist: TStringList;
begin
  cimzettlist:= TStringList.Create;
  try
    if not GyorshajtasSMS_Enabled then begin
        WriteLogFile(gyorshajtaslogfn,'Feldolgozás nincs engedélyezve.'+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
        Exit;  // procedure
        end;
    if EgyebDlg.SMS_KULDES_FORMA <> 'SMSEAGLE' then begin
        WriteLogFile(gyorshajtaslogfn,'Feldolgozás csak SMSEagle beállítással!'+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
        Exit;  // procedure
        end;
    WriteLogFile(gyorshajtaslogfn,'Feldolgozás kezdete: '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
    S:= 'select GY_RENSZ, GY_SEBES, GY_DATUM, GY_IDOPT, GY_LOCAT from NVC_GYORS_SZURT '+
        ' where '+
        // ' GY_DATUM>='''+EgyebDlg.GYORSHAJTAS_KULDES_KEZDETE+''' and ' +  // felesleges, a lenti elavulás szűrés miatt
        ' GY_RENSZ+''|''+GY_DATUM+'' ''+GY_IDOPT not in (select ST_DIM1+''|''+ST_DIM2 from SMS_TECH where ST_KATEG=''GYORSHAJTAS'') '+ // ami még nincs elküldve
        // --- a 30 percnél régebbieket már semmiképp nem küldi ki ---
        ' and GY_DATUM+'' ''+case when DATALENGTH(rtrim(GY_IDOPT))<8 then ''0''+ rtrim(GY_IDOPT) else rtrim(GY_IDOPT) end >= '+
        ' CONVERT(varchar(11), DATEADD(mi, -'+Elavulas_percben+', GETDATE()),102)+''. ''+convert(varchar(5),DATEADD(mi, -'+Elavulas_percben+', GETDATE()),108) '+
        // -------------
        ' and GY_RENSZ in (select RENDSZ from rendszam_sofor) '+  // csak amik ennél a cégnél vannak és lesz kinek küldeni az SMS-t
        ' order by 1 ';
    Query_run(QueryMain_long, S, False);
    // i:= QueryMain_long.RecordCount;
    while not QueryMain_long.eof do begin
        RENDSZAM:= QueryMain_long.FieldByName('GY_RENSZ').AsString;
        MIKOR:= QueryMain_long.FieldByName('GY_DATUM').AsString+' '+QueryMain_long.FieldByName('GY_IDOPT').AsString;
        SEBES:= QueryMain_long.FieldByName('GY_SEBES').AsString;
        LOCAT:= QueryMain_long.FieldByName('GY_LOCAT').AsString;
        WriteLogFile(gyorshajtaslogfn,'Feldolgozás alatt: ['+RENDSZAM+ ', '+SEBES+' km/h, '+ LOCAT+ ', időpont:'+MIKOR+'] '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
        cimzett:= GetSoforEagleSMS(RENDSZAM);
        // cimzett:='nagy.peter@js.hu';
        if cimzett <> '' then begin  // ha nem üres a lista
           // Szoveg:= '['+cimzett_igazi+'] '+RENDSZAM+ ' sebessége '+SEBES+' km/h, kérlek lassíts! Hely:'+ LOCAT+ ', időpont:'+MIKOR;
           Szoveg:= RENDSZAM+ ' sebessége '+SEBES+' km/h, kérlek lassíts! Hely:'+ LOCAT+ ', időpont:'+MIKOR;
           cimzettlist.Clear;
           cimzettlist.Add(cimzett);
           SetLength(LevelMellekletLista, 0);
           Res:= EmailDlg.SendLevel(cimzettlist, '', 'Fuvaros SMS', szoveg, True, 'Fuvaros üzenetközpont',
                        LevelMellekletLista, LevelMellekletNevek, LevelMellekletTipusok, 'multipart/mixed');
           if Res = '' then begin  // sikeres volt a küldés
              WriteLogFile(gyorshajtaslogfn,'Üzenet elküldve ['+cimzett+'] '+Szoveg+' '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
              S:= 'select DO_KOD from rendszam_sofor where RENDSZ='''+RENDSZAM+'''';
              Query2.Close; Query2.SQL.Clear;
              Query2.SQL.Add(S);
              Query2.Open;
              while not Query2.eof do begin
                Query_Insert (qUNI, 'SMS_TECH',
                    [
                    'ST_KATEG', ''''+'GYORSHAJTAS'+'''',
                    'ST_DIM1', ''''+RENDSZAM+'''',
                    'ST_DIM2', ''''+MIKOR+'''',
                    // 'ST_DIM3', ''''+AposztrofCsere(LOCAT)+'''',
                    'ST_DIM3', ''''+LOCAT+'''',   // bekerült a Query_insert-be
                    'ST_CIMZETT', ''''+cimzett+'''',
                    'ST_DOKOD', ''''+Query2.Fields[0].AsString+'''',
                    'ST_KULDVE', ''''+DateTimeToStr(now)+''''
                    ], False );  // ne legyen NoticeKi, ha hiba van
                 Query2.Next;
                 end;  // while
              end  // sikeres küldés
           else begin
              WriteLogFile(gyorshajtaslogfn,'Sikertelen küldés ('+Res+')! Rendszám: '+rendszam+', üzenet: '+Szoveg+' '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
              Res:= 'Sikertelen küldés!';
              end;
           end  // van címzett
         else begin  // nincs sofőr telefonszám
            WriteLogFile(gyorshajtaslogfn,'ISMERETLEN telefonszám! Rendszám: '+rendszam+', üzenet: '+Szoveg+' '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
            Res:= 'Ismeretlen telefonszám.';
            Query_Insert (qUNI, 'SMS_TECH',
                  [
                  'ST_KATEG', ''''+'GYORSHAJTAS'+'''',
                  'ST_DIM1', ''''+RENDSZAM+'''',
                  'ST_DIM2', ''''+MIKOR+'''',
                  // 'ST_DIM3', ''''+AposztrofCsere(LOCAT)+'''',
                  'ST_DIM3', ''''+LOCAT+'''',  // bekerült a Query_insert-be
                  'ST_CIMZETT', ''''+''+'''', // nincs címzett
                  'ST_DOKOD', ''''+''+'''',  // nincs DOKOD
                  'ST_KULDVE', ''''+DateTimeToStr(now)+''''
                  ], False );  // ne legyen NoticeKi, ha hiba van
            end; // else
        QueryMain_long.Next;
        end;  // while
   finally
      if cimzettlist <> nil then cimzettlist.Free;
      end;  // try-finally
  WriteLogFile(gyorshajtaslogfn,'Feldolgozás vége. '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
end;

procedure TForm1.Button6Click(Sender: TObject);
begin
   AutoService_GKTorzs_export;
   AutoService_Munkalap_import;
end;

procedure TForm1.Button7Click(Sender: TObject);
begin
   RendkivuliEsemenyErtesites;
end;

procedure TForm1.Button8Click(Sender: TObject);
begin
   SzakaszFrissites;
   OdaeresBejegyzes;
end;

procedure TForm1.RendkivuliEsemenyErtesites;
var
  S, RKID, EsemenyLeiro, FEKOD, RENDSZAM, MIKOR, TERULET, ESEMENY, TERULETKOD, JELENTETTE, cimzett: string;
  Szoveg, Res, TOVABBITERULET: string;
  cimzettlist, reszlista: TStringList;
  i: integer;
begin
  cimzettlist:= TStringList.Create;
  // cimzettlist.Duplicates:= dupIgnore;  // 2018.01.04.
  reszlista:= TStringList.Create;
  try
   try
    WriteLogFile(rendkivesemlogfn,'Feldolgozás kezdete: '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
    S:= 'select RK_ID, RK_FEKOD, RK_RENDSZAM, SZ1.SZ_MENEV TERULET, RK_DATUM, RK_IDOPT, RK_ESEMENY, '+
        ' RK_TERULETKOD, RK_TOVABBITERULET, JE_FENEV  '+
        ' from rendkivuli_esemeny, szotar sz1, JELSZO '+
        ' where sz1.SZ_FOKOD=''148'' and sz1.SZ_ALKOD=RK_TERULETKOD '+
        ' AND RK_FEKOD = JE_FEKOD '+
        ' and RK_JELENTI is null';
    Query_run(QueryMain, S, False);
    // i:= QueryMain.RecordCount;
    while not QueryMain.eof do begin
        RKID:= QueryMain.FieldByName('RK_ID').AsString;
        FEKOD:= QueryMain.FieldByName('RK_FEKOD').AsString;
        RENDSZAM:= QueryMain.FieldByName('RK_RENDSZAM').AsString;
        MIKOR:= QueryMain.FieldByName('RK_DATUM').AsString+' '+QueryMain.FieldByName('RK_IDOPT').AsString;
        TERULET:= QueryMain.FieldByName('TERULET').AsString;
        TERULETKOD:= QueryMain.FieldByName('RK_TERULETKOD').AsString;
        TOVABBITERULET:= QueryMain.FieldByName('RK_TOVABBITERULET').AsString;
        ESEMENY:= QueryMain.FieldByName('RK_ESEMENY').AsString;
        JELENTETTE:= QueryMain.FieldByName('JE_FENEV').AsString;
        EsemenyLeiro:= '['+TERULET+'] [' +JELENTETTE+ '] ['+MIKOR+'] ';
        if RENDSZAM <> '' then begin
          EsemenyLeiro:= EsemenyLeiro+ '[Rendszám: '+RENDSZAM+'] ';
          end;
        EsemenyLeiro:= EsemenyLeiro+ ESEMENY;

        WriteLogFile(rendkivesemlogfn, 'Feldolgozás alatt: '+ EsemenyLeiro +' ('+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now)+')');
        // cimzett:= EgyebDlg.GetFelettesEmail(TERULETKOD);
        cimzettlist:= EgyebDlg.GetFelettesEmail(TERULETKOD);
        if cimzettlist.Count > 0 then begin  // ha nem üres a lista
           cimzettlist:= EgyediElemek(cimzettlist);  // ez az igazi duplikátum szűrés
           Szoveg:= EsemenyLeiro;
           // ----- teszt
           // cimzettlist.Clear;
           // cimzettlist.Add(cimzett);
           for i:=1 to EgyebDlg.SepListLength(',', TOVABBITERULET) do begin
              reszlista:= EgyebDlg.GetFelettesEmail(EgyebDlg.GetSepListItem(',', TOVABBITERULET, i));
              cimzettlist.AddStrings(reszlista);
              end;
           // Seplist2TStringList(TOVABBITERULET, ',', cimzettlist, False);

           SetLength(LevelMellekletLista, 0);
           Res:= EmailDlg.SendLevel(cimzettlist, '', 'Rendkívüli esemény!', szoveg, True, 'Fuvaros üzenetközpont',
                          LevelMellekletLista, LevelMellekletNevek, LevelMellekletTipusok, 'multipart/mixed');
           // Res:= SendLevel('nagy.peter@js.hu', '', 'Rendkívüli esemény!', cimzett+' - '+szoveg, True, 'Fuvaros üzenetközpont');
           // -----------------------------
           if Res = '' then begin  // sikeres volt a küldés
              WriteLogFile(rendkivesemlogfn,'Üzenet elküldve ['+cimzett+'] '+Szoveg+' '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
              Query_Update (qUNI, 'RENDKIVULI_ESEMENY',
                    [
                    // 'RK_JELENTC', ''''+cimzett+'''',
                    'RK_JELENTC', ''''+cimzettlist.CommaText+'''',
                    'RK_JELENTI', ''''+DateTimeToStr(now)+''''
                    ], 'where RK_ID='+ RKID, False);  // ne legyen NoticeKi, ha hiba van
              end  // sikeres küldés
           else begin
              WriteLogFile(rendkivesemlogfn,'Sikertelen küldés ('+Res+')! ['+cimzett+'] '+Szoveg+' '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
              Res:= 'Sikertelen küldés!';
              end;
           end  // van címzett
         else begin  // nincs sofőr telefonszám
            WriteLogFile(rendkivesemlogfn,'Nincs címzett megadva! FEKOD='+FEKOD+' '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
            Res:= 'Nincs megadva címzett!';
            Query_Update (qUNI, 'RENDKIVULI_ESEMENY',
                    [
                    'RK_JELENTC', ''''+cimzett+'''',
                    'RK_JELENTI', ''''+DateTimeToStr(now)+''''
                    ], 'where RK_ID='+ RKID, False);  // ne legyen NoticeKi, ha hiba van
            end; // else
        QueryMain.Next;
        end;  // while
      except
        on E: exception do
          WriteLogFile(rendkivesemlogfn,'!! EXCEPTION: '+E.Message);
          end;  // try-except
   finally
      if cimzettlist <> nil then cimzettlist.Free;
      if reszlista <> nil then reszlista.Free;
      end;  // try-finally
  WriteLogFile(rendkivesemlogfn,'Feldolgozás vége. '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
end;

procedure TForm1.SzakaszFrissites;
var
  S, Mes: string;
begin
  WriteLogFile(szakaszlogfn,'Feldolgozás kezdete: '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
  S:= 'exec spRefresh_GKSZAKASZ_v4';  // v4: figyelembe veszi az MB_STATU értékét
  // Query_run(QueryMain, S, False);
  if Command_run(AdoCommand1, S, False) then  Mes:= 'Feldolgozás vége.'
  else Mes:= 'Sikertelen feldolgozás!!';
  WriteLogFile(szakaszlogfn, Mes+ ' '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now))
end;

procedure TForm1.ATJELENT_Frissit;
var
  S: string;
begin
  if INI.ReadString('OSZTRAK_BEJELENTES','ATJELENT_BEKAPCSOLVA','1') <> '1' then begin
    WriteLogFile(atjelentlogfn,'Feldolgozás kikapcsolva. '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
    Exit;  // procedure
    end;
  WriteLogFile(atjelentlogfn,'Feldolgozás kezdete: '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
  S:= 'exec spRefresh_ATJELENT';
  Query_run(QueryMain, S, False);
  WriteLogFile(atjelentlogfn,'ATJELENT FRISSÍTÉS kész'+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
 end;

procedure TForm1.ATJELENT_Levelek;
var
  S, LevelSzoveg, Res: string;
  cimzettlist: TStringList;
  AT_MSID: integer;
begin
  cimzettlist:= TStringList.Create;
  try
    WriteLogFile(atjelentlogfn,'Feldolgozás kezdete: '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
    S:= 'select AT_MSID, V_NEV, AT_VAROS, AT_CIM, AT_RAKDAT, AT_RAKIDO '+
        ' from ATJELENT join VEVO on AT_VKOD = V_KOD'+
        ' where isnull(AT_ERTESIT, 0) = 0 order by V_NEV, AT_VAROS, AT_CIM ';
    Query_run(QueryMain, S, False);
    cimzettlist:= GetFelelosEmail('ATJELENT');
    LevelSzoveg:= '';
    while not QueryMain.eof do begin
        AT_MSID:= QueryMain.FieldByName('AT_MSID').AsInteger;
        LevelSzoveg:= LevelSzoveg + QueryMain.FieldByName('AT_RAKDAT').AsString+' '+QueryMain.FieldByName('AT_RAKIDO').AsString+':';
        LevelSzoveg:= LevelSzoveg + QueryMain.FieldByName('AT_VAROS').AsString+', '+QueryMain.FieldByName('AT_CIM').AsString;
        LevelSzoveg:= LevelSzoveg + ' (megbízó: '+ QueryMain.FieldByName('V_NEV').AsString+')';
        LevelSzoveg:= LevelSzoveg + const_CRLF;
        QueryMain.Next;
        end;  // while
    if cimzettlist.Count > 0 then begin  // ha nem üres a lista
        SetLength(LevelMellekletLista, 0);
        Res:= EmailDlg.SendLevel(cimzettlist, '', 'Kitöltésre váró osztrák rakodások', LevelSzoveg, True, 'Fuvaros üzenetközpont',
                        LevelMellekletLista, LevelMellekletNevek, LevelMellekletTipusok, 'multipart/mixed');
        if Res = '' then begin  // sikeres volt a küldés
             WriteLogFile(atjelentlogfn,'Üzenet elküldve ['+cimzettlist.CommaText+'] '+LevelSzoveg+' '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
             Query_Update (qUNI, 'ATJELENT',
                    [
                    'AT_ERTESIT', '1'
                    ],  'isnull(AT_ERTESIT, 0) = 0');
              end  // sikeres küldés
         else begin
            WriteLogFile(atjelentlogfn,'Sikertelen küldés ('+Res+')! ['+cimzettlist.CommaText+'], üzenet: '+LevelSzoveg+' '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
            Res:= 'Sikertelen küldés!';
            end;
           end  // van címzett
     else begin  // nincs megadva címzett
            WriteLogFile(atjelentlogfn,'Sikertelen küldés, nincs megadva címzett! Adatszótár(152) / ATJELENT '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
          end;
   finally
      if cimzettlist <> nil then cimzettlist.Free;
      end;  // try-finally

  WriteLogFile(atjelentlogfn,'Feldolgozás vége. '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
end;


procedure TForm1.OdaeresBejegyzes;
var
  S, MSID, GKDIST, RENDSZAM, HOVA, EsemenyLeiro, cimzett, MBKOD, MBSORSZ, FELLE, Szoveg, Res: string;
  KisebbLista, KisebbSugar: string;
begin
  WriteLogFile(odaereslogfn,'Odaérés ellenőrzés kezdete: '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
  S:= 'insert into ODAERES_HIST '+
	  'select convert(varchar, getdate(),120) OD_MIKOR, GS_HOVA_MSID, GS_GKDIST, GS_HOVA_FELLE, MS_TEFNEV, MS_TENNEV, MS_FPOSOK, MS_LPOSOK '+
	  ' from GKSZAKASZ, MEGSEGED where GS_HOVA_MSID = MS_ID ';
  Query_run(QueryMain, S, False);
  S:= 'delete from ODAERES_HIST where OD_MIKOR <= convert(varchar(10),DATEADD(d, -30, GETDATE()),20) ';  // ELŐZMÉNYEK KORDÁBAN TARTÁSA
  Query_run(QueryMain, S, False);
  WriteLogFile(odaereslogfn,'  Archiválás OK. '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
  KisebbLista:= Szotar_olvaso('700','ODAER_KOZEL_NEV');
  KisebbSugar:= Szotar_olvaso('700','ODAER_KOZEL_TAV');
  if KisebbLista='' then KisebbLista:= '''';   // üres lista
  if KisebbSugar='' then KisebbSugar:= IntToStr(ODAERES_HATAR_METERBEN);

  S:= 'select GS_HOVA_MSID, GS_HOVA_FELLE, GS_GKDIST, GS_RENDSZAM, rtrim(GS_HOVA)+'' ''+rtrim(GS_HOVA_CIM) HOVA, MS_MBKOD, MS_SORSZ '+
      ' from GKSZAKASZ, MEGSEGED where GS_HOVA_MSID = MS_ID and '+
      // ' GS_GKDIST*1000<='+IntToStr(ODAERES_HATAR_METERBEN);
      ' GS_GKDIST*1000<=case when case when GS_HOVA_FELLE=''fel'' then MS_TEFNEV else MS_TENNEV end '+
      ' in ('+KisebbLista+') then '+KisebbSugar+' else '+IntToStr(ODAERES_HATAR_METERBEN)+' end '+
      ' and case when GS_HOVA_FELLE=''fel'' then MS_FPOSOK else MS_LPOSOK end is null ';  // csak ahol még nincs beírva
  WriteLogFile(odaereslogfn,'Erkezes SQL: '+S);
  Query_run(QueryMain, S, False);
  while not QueryMain.eof do begin
      MSID:= QueryMain.FieldByName('GS_HOVA_MSID').AsString;
      FELLE:= QueryMain.FieldByName('GS_HOVA_FELLE').AsString;
      MBKOD:= QueryMain.FieldByName('MS_MBKOD').AsString;
      MBSORSZ:= QueryMain.FieldByName('MS_SORSZ').AsString;
      // GKDIST:= QueryMain.FieldByName('GS_GKDIST').AsString; // km-ben
      GKDIST:= IntToStr(Floor(QueryMain.FieldByName('GS_GKDIST').AsFloat*1000));  // méterben
      RENDSZAM:= QueryMain.FieldByName('GS_RENDSZAM').AsString;
      HOVA:= QueryMain.FieldByName('HOVA').AsString;
      EsemenyLeiro:= RENDSZAM+ ' '+GKDIST+' méterre megközelítette a '+FELLE+'rakót ('+HOVA+'). '+
          'Az érkezés időpontját bejegyeztük a '+MBKOD+' megbízás '+MBSORSZ+'. sorában.';

      WriteLogFile(odaereslogfn, 'Feldolgozás alatt: '+ EsemenyLeiro +' ('+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now)+')');
      if FELLE = 'fel' then begin
            S:= 'update MEGSEGED set MS_FPOSOK='''+EgyebDlg.Maidatum+' '+formatDatetime('HH:mm', Now)+''' where MS_ID='+ MSID+
              ' and MS_FPOSOK is null';
            Query_Run (qUNI, S);
            WriteLogFile(odaereslogfn, 'SQL: '+ S);
            end; // id
      if FELLE = 'le' then begin
            S:= 'update MEGSEGED set MS_LPOSOK='''+EgyebDlg.Maidatum+' '+formatDatetime('HH:mm', Now)+''' where MS_ID='+ MSID+
                ' and MS_LPOSOK is null';
            Query_Run (qUNI, S);
            WriteLogFile(odaereslogfn, 'SQL: '+ S);
            end; // if

      // nem lesz levelezés a témában
      {
      cimzett:= GetMenetiranyitoEmail(RENDSZAM);
      if not Odaeres_update_enabled then begin // "teszt mód"
        cimzett:='nagy.peter@js.hu';
        end;
      if cimzett <> '' then begin  // ha nem üres a lista
         Szoveg:= EsemenyLeiro;
         Res:= SendLevel(cimzett, '', 'Gépkocsi a célnál', szoveg, True, 'Fuvaros üzenetközpont');
         if Res = '' then begin  // sikeres volt a küldés
            WriteLogFile(odaereslogfn,'Üzenet elküldve ['+cimzett+'] '+Szoveg+' '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
            end  // sikeres küldés
         else begin
            WriteLogFile(odaereslogfn,'Sikertelen küldés! ['+cimzett+'] '+Szoveg+' '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
            Res:= 'Sikertelen küldés!';
            end;
         end  // van címzett
       else begin  // nincs sofőr telefonszám
          WriteLogFile(odaereslogfn,'Nincs címzett megadva! Rendszám: '+RENDSZAM+' '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
          Res:= 'Nincs megadva címzett!';
          end; // else
          }
      QueryMain.Next;
      end;  // while
  WriteLogFile(odaereslogfn,'Odaérés ellenőrzés vége. '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
end;

procedure TForm1.IndulasBejegyzes;
var
  S, MSID, GKDIST, RENDSZAM, HONNAN, EsemenyLeiro, cimzett, MBKOD, MBSORSZ, FELLE, Szoveg, Res: string;
  KisebbLista, KisebbSugar: string;
begin
  WriteLogFile(odaereslogfn,'Indulás ellenőrzés kezdete: '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
  KisebbLista:= Szotar_olvaso('700','ODAER_KOZEL_NEV');
  KisebbSugar:= Szotar_olvaso('700','ODAER_KOZEL_TAV');
  if KisebbLista='' then KisebbLista:= '''';   // üres lista
  if KisebbSugar='' then KisebbSugar:= IntToStr(ODAERES_HATAR_METERBEN);

  S:= 'select GS_HONNAN_MSID, GS_SZAKASZKM-GS_GKDIST TAVOLSAG, GS_RENDSZAM, rtrim(GS_HONNAN)+'' ''+rtrim(GS_HONNAN_CIM) HONNAN, MS_MBKOD, MS_SORSZ '+
      ' from GKSZAKASZ, MEGSEGED where GS_HONNAN_MSID = MS_ID and '+
      ' (GS_SZAKASZKM-GS_GKDIST) *1000 > case when case when MS_TIPUS = 0  then MS_TEFNEV else MS_TENNEV end '+
      ' in ('+KisebbLista+') then '+KisebbSugar+' else '+IntToStr(ODAERES_HATAR_METERBEN)+' end '+
      ' and MS_INDULOK is null '; // csak ahol még nincs beírva
  WriteLogFile(odaereslogfn,'Indulás SQL: '+S);
  Query_run(QueryMain, S, False);
  while not QueryMain.eof do begin
      MSID:= QueryMain.FieldByName('GS_HONNAN_MSID').AsString;
      MBKOD:= QueryMain.FieldByName('MS_MBKOD').AsString;
      MBSORSZ:= QueryMain.FieldByName('MS_SORSZ').AsString;
      GKDIST:= IntToStr(Floor(QueryMain.FieldByName('TAVOLSAG').AsFloat*1000));  // méterben
      RENDSZAM:= QueryMain.FieldByName('GS_RENDSZAM').AsString;
      HONNAN:= QueryMain.FieldByName('HONNAN').AsString;
      EsemenyLeiro:= RENDSZAM+ ' '+GKDIST+' méterre eltávolodott a rakodóhelytől ('+HONNAN+'). '+
          'Az indulás időpontját bejegyeztük a '+MBKOD+' megbízás '+MBSORSZ+'. sorában.';

      WriteLogFile(odaereslogfn, 'Feldolgozás alatt: '+ EsemenyLeiro +' ('+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now)+')');
      S:= 'update MEGSEGED set MS_INDULOK='''+EgyebDlg.Maidatum+' '+formatDatetime('HH:mm', Now)+''' where MS_ID='+ MSID+
              ' and MS_INDULOK is null';
      Query_Run (qUNI, S);
      WriteLogFile(odaereslogfn, 'SQL: '+ S);
      QueryMain.Next;
      end;  // while
  WriteLogFile(odaereslogfn,'Indulás ellenőrzés vége. '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
end;


procedure TForm1.Button9Click(Sender: TObject);
begin
   KesesElorejelzes;
end;

procedure TForm1.KesesElorejelzes;
var
  S, MSID, GKDIST, RENDSZAM, HOVA, EsemenyLeiro, cimzett, MBKOD, MBSORSZ, FELLE, Szoveg, Res: string;
  ErkezD, ErkezI, MostD, MostI, RendelkezesreAlloIdo, CelInfo, KesesInfo, MBInfo, HelyzetInfo, Hova_felle: string;
  CegNeve, MegbizoNeve: string;
  SzakaszKm, Dist, AUTO_SZ, AUTO_HO: double;
  GaugePercent, TotalDiffSec, TotalDiffPerc, DiffOra, DiffPerc, SzuksegesSebesseg, i: integer;
  KellKuldes: boolean;
begin
  WriteLogFile(keseslogfn,'Késés előrejelzés kezdete: '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
  S:= 'select GK_KOD, GS_RENDSZAM, GS_HONNAN, GS_HOVA, GS_HOVA_FELLE, GS_SZAKASZKM, GS_INDULI, GS_ERKEZI, GS_GKDIST, GS_GKSEBES, '+
      ' GS_GKGEKOD, GS_HONNAN_MSID, GS_HOVA_MSID, GS_GKGEKOD, GS_GKPODAT, GS_GKSEBES, GS_GKIRANY, GS_HOVASZ, GS_HOVAHO '+
      ' from GKSZAKASZ, GEPKOCSI where GK_RESZ= GS_RENDSZAM '+
      ' and GS_HOVA_MSID not in (select ST_DIM2 from SMS_TECH where ST_KATEG=''KESES_ELOREJELZES'') ';
  // WriteLogFile(keseslogfn,'SQL: '+S);
  Query_run(QueryMain, S, False);
  with QueryMain do begin
    while not eof do begin
        KellKuldes:= False;
        RENDSZAM:= FieldByName('GS_RENDSZAM').AsString;
        // WriteLogFile(keseslogfn,'-- processing '+RENDSZAM+'...');
        SzakaszKm:= FieldByName('GS_SZAKASZKM').AsFloat;
        // WriteLogFile(keseslogfn,'-- SzakaszKm: '+FormatFloat('0.00', SzakaszKm));
        if SzakaszKm>0 then begin  // érvényes adatok vannak
          Dist:= FieldByName('GS_GKDIST').AsFloat;
          GaugePercent := Floor(((SzakaszKm-Dist)/SzakaszKm)*100.00);  // ide csak akkor kerül, ha SzakaszKm>0
          ErkezD:= EgyebDlg.SepFrontToken(' ', FieldByName('GS_ERKEZI').AsString);  // pl. 2016.10.12.
          ErkezI:= EgyebDlg.SepRest(' ', FieldByName('GS_ERKEZI').AsString);   // pl. 08:45
          MostD:= FormatDateTime('YYYY.MM.DD', Now);
          MostI:= FormatDateTime('HH:mm', Now);
          // WriteLogFile(keseslogfn,'-- Idők: '+ErkezD+' '+ErkezI);
          if (Trim(ErkezD) <> '') and (Trim(ErkezI) <> '') then begin // van idő egyáltalán
            TotalDiffSec:= DateTimeDifferenceSec(ErkezD, ErkezI, MostD, MostI); // másodpercekben
            // WriteLogFile(keseslogfn,'-- TotalDiffSec: '+IntToStr(TotalDiffSec));
            if TotalDiffSec > 0 then begin
              TotalDiffPerc:= Floor( TotalDiffSec / 60.0);  // könnyebb percekben átlátni
              DiffOra:= Floor( TotalDiffPerc / 60.0);
              DiffPerc:= TotalDiffPerc- 60*DiffOra;
              RendelkezesreAlloIdo:=IntToStr(DiffOra)+ ' óra '+IntToStr(DiffPerc)+ ' perc';
              SzuksegesSebesseg:= Floor(Dist/(TotalDiffPerc/60.0));
              // WriteLogFile(keseslogfn,'-- SzuksegesSebesseg: '+IntToStr(SzuksegesSebesseg));
              if SzuksegesSebesseg >= EgyebDlg.ELKESETT_SEBESSEG then begin
                KellKuldes:= True;
                end;  // if sebesség nem elég
              end // totaldiffsec > 0
            else begin  //  TotalDiffSec nulla vagy negatív
                // elvileg ilyen nem lehet, ha folyamatos a figyelés, hacsak át nem írják az érkezési időket menet közben...
                RendelkezesreAlloIdo:= 'ELKÉSETT!';
                KellKuldes:= True;
               end;
            // Debug KellKuldes:= True;
            if KellKuldes then begin
                MSID:= FieldByName('GS_HOVA_MSID').AsString;
                Hova_felle:=FieldByName('GS_HOVA_FELLE').AsString;
                // S:= 'select MS_MBKOD, MS_SORSZ, MS_FELNEV, MS_LERNEV from MEGSEGED where MS_ID='+MSID;
                S:= 'select MS_MBKOD, MS_SORSZ, MS_FELNEV, MS_LERNEV, MB_VENEV '+
                    ' from MEGSEGED left join MEGBIZAS on MS_MBKOD = MB_MBKOD '+
                    ' where MS_ID='+MSID;
                Query_Run(QUni, S);
                MBInfo:= QUni.FieldByName('MS_MBKOD').AsString+'/'+QUni.FieldByName('MS_SORSZ').AsString;
                MegbizoNeve:= QUni.FieldByName('MB_VENEV').AsString;
                if Hova_felle='fel' then CegNeve:= QUni.FieldByName('MS_FELNEV').AsString
                else CegNeve:= QUni.FieldByName('MS_LERNEV').AsString;
                CelInfo:= 'Cél: '+ FieldByName('GS_HOVA').AsString + ' ('+CegNeve+'), tervezett érkezési idő: '+FieldByName('GS_ERKEZI').AsString+'-ig '+
                         '(megbízás: '+MBInfo+'). Megbízó: '+MegbizoNeve;
                CelInfo:= CelInfo +const_LF + 'Cél a térképen: '+GeoRoutines.GetGooglePlaceLink(FieldByName('GS_HOVASZ').AsFloat, FieldByName('GS_HOVAHO').AsFloat)+ const_LF;
                KesesInfo:= 'Hátralévő távolság légvonalban: '+ FormatFloat('0.00', Dist)+' km, rendelkezésre álló idő: '+RendelkezesreAlloIdo+'.';
                HelyzetInfo:= 'Gépkocsi helyzete: '+FieldByName('GS_GKGEKOD').AsString+' ('+FieldByName('GS_GKPODAT').AsString+') ';
                HelyzetInfo:= HelyzetInfo + 'Sebesség: '+ IntToStr(FieldByName('GS_GKSEBES').AsInteger)+' km/h';
                if FieldByName('GS_GKSEBES').AsInteger <> 0 then  // csak ha mozog
                    HelyzetInfo:= HelyzetInfo +' "'+FieldByName('GS_GKIRANY').AsString+'" irányban';
                HelyzetInfo:= HelyzetInfo +'.';
                S:= 'select PO_SZELE, PO_HOSZA from POZICIOK where PO_RENSZ = '''+RENDSZAM+'''';
                Query_Run(QUni, S);
                AUTO_SZ:= QUni.FieldByName('PO_SZELE').AsFloat;
                AUTO_HO:= QUni.FieldByName('PO_HOSZA').AsFloat;
                HelyzetInfo:= HelyzetInfo + const_LF +'Gépkocsi helyzete a térképen: '+GeoRoutines.GetGooglePlaceLink(AUTO_SZ, AUTO_HO);

                HelyzetInfo:= HelyzetInfo + const_LF+ const_LF +'Útvonal a célig: '+GeoRoutines.GetGoogleRouteLink(AUTO_SZ, AUTO_HO, FieldByName('GS_HOVASZ').AsFloat, FieldByName('GS_HOVAHO').AsFloat);
                SendKesesGyanu(RENDSZAM, CelInfo, KesesInfo, HelyzetInfo, MSID);
              end;
            end; // if van érkezési idő
          end;  // if SzakaszKm>0
        Next;
        end;  // while
    end;  // with
  WriteLogFile(keseslogfn,'Késés előrejelzés vége. '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
end;

procedure TForm1.SendKesesGyanu(RENDSZAM, CelInfo, KesesInfo, HelyzetInfo, MSID: string);
var
  cimzett1, cimzett, Szoveg, Res, S, igazi_cimzett, Title: string;
  cimzettlista: TStringList;
begin
  // WriteLogFile(keseslogfn,'Ki a menetirányító? MSID= '+MSID);
  cimzettlista:= TStringList.Create;
  try
    cimzett1:= GetMenetiranyitoEmail(RENDSZAM);
    cimzettlista.Add(cimzett1);
    WriteLogFile(keseslogfn,'Címzett hozzáadva (menetirányító): '+cimzett1);
    cimzett1:= GetMegbizasFelelosEmail(MSID);
    cimzettlista.Add(cimzett1);
    WriteLogFile(keseslogfn,'Címzett hozzáadva (felelős): '+cimzett1);
    cimzett1:= GetCsoportFelelosEmail(MSID);
    cimzettlista.Add(cimzett1);
    WriteLogFile(keseslogfn,'Címzett hozzáadva (csoport felelős): '+cimzett1);
    // igazi_cimzett:= StringListToSepString(cimzettlista, ',', False);   // van benne duplikáció eliminálás
    WriteLogFile(keseslogfn,'-- SendKesesGyanu1: '+igazi_cimzett);
    // WriteLogFile(keseslogfn,'A menetirányító címe: '+igazi_cimzett);
    // ************************************
    // debug
    // cimzettlista.Clear;
    // cimzettlista.Add('nagy.peter@js.hu');
    // ************************************
    Title:= RENDSZAM+': elmaradás az útitervtől';
    if cimzettlista.Count > 0 then begin  // ha nem üres a lista
       Szoveg:= CelInfo + const_LF + KesesInfo + const_LF + HelyzetInfo;
       SetLength(LevelMellekletLista, 0);
       Res:= EmailDlg.SendLevel(cimzettlista, '', Title, szoveg, True, 'Fuvaros üzenetközpont',
                        LevelMellekletLista, LevelMellekletNevek, LevelMellekletTipusok, 'multipart/mixed');
       if Res = '' then begin  // sikeres volt a küldés
          WriteLogFile(keseslogfn,'Üzenet elküldve ['+cimzett+'] '+Title+' '+Szoveg+' '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
          // bejegyezzük elküldöttnek
          Query_Insert (qUNI, 'SMS_TECH',
                    [
                    'ST_KATEG', ''''+'KESES_ELOREJELZES'+'''',
                    'ST_DIM1', ''''+RENDSZAM+'''',
                    'ST_DIM2', ''''+MSID+'''',
                    'ST_CIMZETT', ''''+cimzett+'''',
                    'ST_UZENET', ''''+Szoveg+'''',
                    'ST_KULDVE', ''''+DateTimeToStr(now)+''''
                    ], False );  // ne legyen NoticeKi, ha hiba van
          end  // sikeres küldés
       else begin
          WriteLogFile(keseslogfn,'Sikertelen küldés! ['+cimzett+'] '+Title+' '+Szoveg+' '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
          Res:= 'Sikertelen küldés!';
          end;
       end  // van címzett
     else begin  // nincs sofőr telefonszám
        WriteLogFile(keseslogfn,'Menetirányító nem azonosítható! Rendszám: '+RENDSZAM+' '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
        Res:= 'Nincs megadva címzett!';
        end; // else
  finally
     if cimzettlista <> nil then cimzettlista.Free;
     end;  // try-finally

end;

function TForm1.GetMenetiranyitoEmail(RENDSZAM: string): string;
var
  GKKATEG, S: string;
begin
  GKKateg:= Query_Select_local('GEPKOCSI', 'GK_RESZ', Rendszam, 'GK_GKKAT');
  Result:= Trim(Szotar_olvaso('378',GKKateg));
end;

// ez az "utolsó módosító" alapú megoldás
{function TForm1.GetMenetiranyitoEmail(MSID: string): string;
var
  DOKOD, cimzett, S: string;
begin
  S:= 'select JE_DOKOD from MEGBIZAS, JELSZO where JE_FEKOD = MB_MSMOD and MB_MBKOD = (select MS_MBKOD from MEGSEGED where MS_ID='+MSID+')';
  // WriteLogFile(keseslogfn,'Menetirányító keresés SQL= '+S);
  Query_run(qUni, S);
  if not qUni.eof then begin
    DOKOD:= qUni.FieldByName('JE_DOKOD').AsString;
    // WriteLogFile(keseslogfn,'Menetirányító DOKOD= '+DOKOD);
    Result:= Trim(EgyebDlg.GetDolgozoSMSEagleCimzett(DOKOD, false));
    end;  // if not eof
end;
}

function TForm1.GetMegbizasFelelosEmail(MSID: string): string;
var
  DOKOD, cimzett, S: string;
begin
  S:= 'select JE_DOKOD from MEGBIZAS, JELSZO where JE_FEKOD = MB_FEKOD and MB_MBKOD = (select MS_MBKOD from MEGSEGED where MS_ID='+MSID+')';
  Query_run(qUni, S);
  if not qUni.eof then begin
    DOKOD:= qUni.FieldByName('JE_DOKOD').AsString;
    S:= Trim(EgyebDlg.GetDolgozoSMSEagleCimzett(DOKOD, false));
    Result:= GetKopaszEmail(S);  // leveszi a nevet és a < > "burkolatot"
    end;  // if not eof
end;

function TForm1.GetCsoportFelelosEmail(MSID: string): string;
var
  HUTOS, EmailLista, dokodlista, DOKOD, S: string;
begin
  S:= 'select MB_HUTOS from MEGBIZAS where MB_MBKOD = (select MS_MBKOD from MEGSEGED where MS_ID='+MSID+')';
  Query_run(qUni, S);
  if not qUni.eof then begin
    HUTOS:= qUni.Fields[0].AsString;
    dokodlista:= EgyebDlg.Read_SZGrid('150', HUTOS);
    EmailLista:= '';
    while dokodlista <> '' do begin
      DOKOD:= Trim(EgyebDlg.SepFrontToken(',', dokodlista));
      dokodlista:= Trim(EgyebDlg.SepRest(',', dokodlista));
      S:= trim(EgyebDlg.GetDolgozoSMSEagleCimzett(DOKOD, false));
      S:= GetKopaszEmail(S);  // leveszi a nevet és a < > "burkolatot"
      EmailLista:= Felsorolashoz(EmailLista, S, ',', False);
      end; // while
    Result:= EmailLista;
    end;  // if not eof
end;

procedure TForm1.MySMSSzalFrissit;
var
   fn, logfile: string;
begin
  SMSKuldDlg.RefreshAllConversations;
  fn:= INI.ReadString('SMS_SZALAK','LOGFILE','FUVAR_TIMER_SMS_SZALAK.log');
  logfile:= EXEPath+'LOG\'+fn ;
  WriteLogFile(logfile,'SMS szálak frissítve: '+DateTimeToStr(now)+'  '+ADOConnectionAlap.DefaultDatabase);
end;

procedure TForm1.AutoService_GKTorzs_export;
var
  S, GKTorzs_filename: string;
  SL: TStringList;
begin
  // GKTorzs_filename:= Szotar_olvaso('999','758');
  GKTorzs_filename:= INI.ReadString('GEPKOCSI_TORZS','KIMENET','');

  S:= 'select GK_RESZ+'';''+ '+
  	'GK_ALVAZ+'';''+ '+
		'GK_TIPUS+'';''+ '+
		'GK_MOTOR+'';''+ '+
		'convert(varchar,convert(int,GK_OSULY))+'';''+ '+
		'convert(varchar,GK_EVJAR)+'';''+ '+
		'case when GK_FORHE is null or GK_FORHE='''' then '''' else convert(varchar, szami.FuvarosDatumToDatetime(GK_FORHE, ''''), 112) end+'';''+ '+
		'case when GK_UT_KM is null then ''0'' else convert(varchar,convert(int,GK_UT_KM)) end+'';''+ '+
		'case when g1.GA_DATUM is null or g1.GA_DATUM='''' then '''' else convert(varchar, szami.FuvarosDatumToDatetime(g1.GA_DATUM, ''''), 112) end+'';''+ '+
		'case when g2.GA_DATUM is null or g2.GA_DATUM='''' then '''' else convert(varchar, szami.FuvarosDatumToDatetime(g2.GA_DATUM, ''''), 112) end+'';'' '+
    'from GEPKOCSI '+
    'left outer join GARANCIA g1 on g1.GA_RENSZ = GK_RESZ and g1.GA_ALKOD=''110'' '+
    'left outer join GARANCIA g2 on g2.GA_RENSZ = GK_RESZ and g2.GA_ALKOD=''210'' '+
    // 'where GK_KULSO=0 and GK_ARHIV=0 and GK_POTOS=0 ';
    'where GK_KULSO=0 and GK_ARHIV=0 ';   // valamiért a pótkocsikat eddig kihagytuk
  SL:= TStringList.Create;
  try
    with Query1 do begin
      Close; SQL.Clear;
      SQL.Add(S);
      Open;
      while not eof do begin
        SL.Add(Fields[0].AsString);  // az eredményhalmazt soronként hozzáadjuk
        Next;
        end;
      Close;
      SL.SaveToFile(GKTorzs_filename);
      end;  // with
  finally
    SL.Free;
    end;  // try-finally
end;

procedure TForm1.Fizetesi_jegyzek_feldolgozas;
const
  const_fizjegy_kiterjesztes = '.pdf';
var
  File_input_folder, File_archive_folder, NASRootMappa, TempDrive: string;
  NAS_user, NAS_pwd, FN_from, FN_to, Res, SoforNeve: string;
  talalat: integer;
  SearchRec: TSearchRec;
begin
  if INI.ReadString('FIZETESI_JEGYZEK','BEKAPCSOLVA','1') <> '1' then begin
    WriteLogFile(osztraklogfn,'Feldolgozás kikapcsolva. '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
    Exit;  // procedure
    end;
  WriteLogFile(fizetesilogfn,'Feldolgozás kezdete: '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
  try  // except
    File_input_folder:= INI.ReadString('FIZETESI_JEGYZEK','BEMENET','.\FIZJEGY');
    File_archive_folder:= INI.ReadString('FIZETESI_JEGYZEK','SOFOR_OK','.\FIZJEGY\SOFOR_OK');
    NASRootMappa:= INI.ReadString('DOLGOZO_NAS','NAS_ROOT','.\FIZJEGY\NAS');
    NAS_user:= Trim(DecodeString(INI.ReadString( 'DOLGOZO_NAS', 'NAS_USER',  '')));
	  NAS_pwd:= Trim(DecodeString(INI.ReadString( 'DOLGOZO_NAS', 'NAS_PWD',  '')));

    talalat:= FindFirst(File_input_folder+'\*'+const_fizjegy_kiterjesztes, faAnyFile, SearchRec);
    WriteLogFile(fizetesilogfn, 'Állomány: '+SearchRec.Name+' '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
    while talalat = 0 do begin
       SoforNeve:= EgyebDlg.SepFronttoken('_Fizetési', trim(SearchRec.Name)); // NAGY PÉTER
       // QFILE megoldás
       // Res:= KuldesSoforMappaba (File_input_folder+'\'+SearchRec.Name, NASRootMappa, SoforNeve, NAS_user, NAS_pwd);
       // Email megoldás
       Res:= PDFKuldesSofornekICloud(File_input_folder+'\'+SearchRec.Name, 'Fizetési jegyzék', 'Mellékelve.', SoforNeve);

       if Res = '' then begin
         WriteLogFile(fizetesilogfn,'Feldolgozva: '+SearchRec.Name+' '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
         FN_from:= File_input_folder+'\'+SearchRec.Name;
         FN_to:= File_archive_folder+'\'+SearchRec.Name;
         if MoveFile(PChar(FN_from), PChar(FN_to)) then
            WriteLogFile(fizetesilogfn,'Elmozgatva: '+FN_from+' -> '+FN_to)
         else
            WriteLogFile(fizetesilogfn,'!!! Sikertelen file mozgatás: '+FN_from+' -> '+FN_to)
         end  // if
       else
         WriteLogFile(fizetesilogfn,'!!! Sikertelen feldolgozás ('+Res+'): '+SearchRec.Name+' '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
       talalat:= FindNext(SearchRec);
       end;  // while
    FindClose(SearchRec);
    WriteLogFile(fizetesilogfn,'Feldolgozás vége: '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
  except
      on E: exception do
         WriteLogFile(fizetesilogfn,'!! EXCEPTION: '+E.Message);
      end;  // try-except
end;

procedure TForm1.Osztrak_bejelentes_feldolgozas;
const
  const_doc_kiterjesztes = '.pdf';
  const_fix_resz = 'ZKO3-Neumeldung';
var
  File_input_folder, File_archive_folder, NASRootMappa: string;
  NAS_user, NAS_pwd, FN_from, FN_to, Res, SoforNeve: string;
  talalat: integer;
  SearchRec: TSearchRec;
begin
  if INI.ReadString('OSZTRAK_BEJELENTES','BEKAPCSOLVA','1') <> '1' then begin
    WriteLogFile(osztraklogfn,'Feldolgozás kikapcsolva. '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
    Exit;  // procedure
    end;
  WriteLogFile(osztraklogfn,'Feldolgozás kezdete: '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
  try  // except
    File_input_folder:= INI.ReadString('OSZTRAK_BEJELENTES','BEMENET','.\OSZTRAK');
    File_archive_folder:= INI.ReadString('OSZTRAK_BEJELENTES','SOFOR_OK','.\OSZTRAK\SOFOR_OK');
    NASRootMappa:= INI.ReadString('DOLGOZO_NAS','NAS_ROOT','.\FIZJEGY\NAS');
    NAS_user:= Trim(DecodeString(INI.ReadString( 'DOLGOZO_NAS', 'NAS_USER',  '')));
	  NAS_pwd:= Trim(DecodeString(INI.ReadString( 'DOLGOZO_NAS', 'NAS_PWD',  '')));

    talalat:= FindFirst(File_input_folder+'\*'+const_doc_kiterjesztes, faAnyFile, SearchRec);
    WriteLogFile(osztraklogfn, 'Állomány: '+SearchRec.Name+' '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
    while talalat = 0 do begin
        // Kocsis Roland 20170301 ZKO3-Neumeldung-Formular.pdf
       SoforNeve:= trim(EgyebDlg.SepFronttoken(const_fix_resz, trim(SearchRec.Name))); // "Kocsis Roland 20170301"
       if (copy(SoforNeve, Length(SoforNeve)-8, 1)= ' ') and JoEgesz(copy(SoforNeve, Length(SoforNeve)-7, 8)) then
           SoforNeve:= copy(SoforNeve, 1, Length(SoforNeve)-9);  // levesszük a dátumot -> "Kocsis Roland"
       // QFILE megoldás
       // Res:= KuldesSoforMappaba (File_input_folder+'\'+SearchRec.Name, NASRootMappa, SoforNeve, NAS_user, NAS_pwd);
       // Email megoldás
       Res:= PDFKuldesSofornekICloud(File_input_folder+'\'+SearchRec.Name, 'Osztrák bejelentés', 'Mellékelve.', SoforNeve);
       if Res = '' then begin
         WriteLogFile(osztraklogfn,'Feldolgozva: '+SearchRec.Name+' '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
         FN_from:= File_input_folder+'\'+SearchRec.Name;
         FN_to:= File_archive_folder+'\'+SearchRec.Name;
         if MoveFile(PChar(FN_from), PChar(FN_to)) then
            WriteLogFile(osztraklogfn,'Elmozgatva: '+FN_from+' -> '+FN_to)
         else
            WriteLogFile(osztraklogfn,'!!! Sikertelen file mozgatás: '+FN_from+' -> '+FN_to)
         end  // if
       else
         WriteLogFile(osztraklogfn,'!!! Sikertelen feldolgozás ('+Res+'): '+SearchRec.Name+' '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
       talalat:= FindNext(SearchRec);
       end;  // while
    FindClose(SearchRec);
    WriteLogFile(osztraklogfn,'Feldolgozás vége: '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
  except
      on E: exception do
         WriteLogFile(osztraklogfn,'!! EXCEPTION: '+E.Message);
      end;  // try-except
end;


procedure TForm1.Probakuldes;
begin
  PDFKuldesSofornekICloud('C:\KutasDB\proba.pdf', 'próba fej', 'Szöveg', 'Auer György');
end;

function TForm1.KuldesSoforMappaba(FN, CelMappa, SoforNeve, login, password: string): string;
var
  CsakFN: string;
begin
  // FN szerkezete:  VARGA NORBERT_Fizetési jegyzék_2017_február_153_20170306113422840.pdf
  CsakFN:= ExtractFileName(FN);
  // if MapNetworkDrive(CelMappa, Drive_betujel, login, password) then begin
  if NetUseAddWithoutDriveMapping(CelMappa, login, password) then begin
    if CopyFile(FN, CelMappa+'\'+SoforNeve+'\'+CsakFN) then
       Result:= ''
    else Result:= 'A másolás nem sikerült!';
    end  // if
  else begin
    Result:= 'A NAS kapcsolódás nem sikerült!';
    end;
end;

function TForm1.PDFKuldesSofornekICloud(const FN, LevelFejlec, LevelBody, SoforNeve: string): string;
var
  iCloudEmail, TelSzam, DOKOD, S, Title, Uzenet, Res: string;
  cimzettlista: TStringList;
  Stream1: TMemoryStream;
begin
  S:= 'select DO_KOD from dolgozo where DO_NAME='''+SoforNeve+'''';  // nem case szenzitív
  Query_run(Query2, S);
  DOKOD:= Query2.Fields[0].AsString;
  TelSzam:= trim(EgyebDlg.GetDolgozoKontakt(DOKOD, True));  // ForceSMS = True
  if copy(TelSzam, 1, 3) <> '+36' then begin
      S:='SIKERTELEN KÜLDÉS! '+SoforNeve+' részére (kontakt: '+TelSzam+'): '+LevelFejlec +' '+ LevelBody +' [melléklet: '+FN+']';
      S:= S+ FormatDateTime('YYYY.MM.DD. HH:mm:ss', now);
      WriteLogFile(icloudlogfn, S);
      Exit;  // procedure
      end;
  TelSzam:= copy(TelSzam, 4, 999);
  iCloudEmail:= 'js'+TelSzam + ICLOUD_NEV;
  cimzettlista:=TStringlist.Create;
  Stream1:= TMemoryStream.Create;
  try
    cimzettlista.Add(iCloudEmail);
    SetLength(LevelMellekletLista, 1);
    Stream1.LoadFromFile(FN);
    LevelMellekletLista[0]:= @Stream1;
    LevelMellekletNevek.Clear;
    // LevelMellekletNevek.Add('Dokumentum');
    LevelMellekletNevek.Add(ExtractFileName(FN));
    LevelMellekletTipusok.Clear;
    LevelMellekletTipusok.Add(const_PDFMIME);
    Res:= EmailDlg.SendLevel(cimzettlista, '', LevelFejlec, LevelBody, True, 'Fuvaros üzenetközpont',
              LevelMellekletLista, LevelMellekletNevek, LevelMellekletTipusok, 'multipart/mixed');
    S:='Levél elküldve '+SoforNeve+'('+iCloudEmail+') részére: '+LevelFejlec +' '+ LevelBody +' [melléklet: '+FN+'] ';
    S:= S+ FormatDateTime('YYYY.MM.DD. HH:mm:ss', now);
    WriteLogFile(icloudlogfn, S);
  finally
    if cimzettlista <> nil then cimzettlista.Free;
    if Stream1 <> nil then Stream1.Free;
    end;  // try-finally

end;

procedure TForm1.AutoService_Munkalap_import;
const
  Igenyek_1_minta = 'Igények, hibaleírás: ';
  Fennmaradt_2_minta = 'Fennmaradt munkák: ';
  Megjegyzes_3_minta = 'Megjegyzés: ';
  MLP_kiterjesztes = '.mlp';
var
  S, Munkalap_input_folder, Munkalap_archive_folder, Kinek: string;
  sor, rendszam, munkalapszam, datum, idopont, netto, UjFN: string;
  InfoArray: array [1..3] of string;     // Igenyek, Fennmaradt, Megjegyzes
  FN_from, FN_to, SoforSMS, Szoveg, ActSor, Res, cimzett: string;
  SearchRec: TSearchRec;
  talalat, i, ActSzovegMezo, alkod, soforkod: integer;
  F1: textfile;
	filelista, torzs, cimzettlist: TStringList;
  // IdMessage1: TIdMessage;
begin
  WriteLogFile(munkalaplogfn,'Feldolgozás kezdete: '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));

  filelista:= TStringList.Create;
  torzs  :=TStringList.Create;
  cimzettlist:= TStringList.Create;
  try  // finally
  try  // except
    // IdMessage1:=TIdMessage.Create(Self);
    // IdMessage1.ContentType:='text/html';
    // IdMessage1.CharSet:= 'UTF-8';
    Munkalap_input_folder:= INI.ReadString('MUNKALAP','BEMENET','.\MLP');
    Munkalap_archive_folder:= INI.ReadString('MUNKALAP','ARCHIVUM','.\MLP\ARCH');
    talalat		:= FindFirst(Munkalap_input_folder+'\*'+MLP_kiterjesztes, faAnyFile, SearchRec);
    WriteLogFile(munkalaplogfn,'Állomány: '+SearchRec.Name+' '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
    while talalat = 0 do begin
       filelista.Add(SearchRec.Name);  // a későbbi elmozgatáshoz
       AssignFile(F1, Munkalap_input_folder+'\'+SearchRec.Name );
       Reset(F1);
       // WriteLogFile(munkalaplogfn,'-- 1'+' '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
       if not eof(F1) then begin
         ReadLn(F1, sor);
         OpenCSVSor(sor);    // pl. XZM-331;16000781;20160416;20:00;95250
         // WriteLogFile(munkalaplogfn,'-- 1.1'+' '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
         rendszam:= GetCSVElem(1);
         munkalapszam:= GetCSVElem(2);
         datum:= GetCSVElem(3);
         if (Length(datum)=8) then begin
            datum:=copy(datum,1,4)+'.'+copy(datum,5,2)+'.'+copy(datum,7,2)+'.';
            end;
         idopont:= GetCSVElem(4);
         netto:= GetCSVElem(5);
         for i:=1 to 3 do
           InfoArray[i]:='';
         while not eof(F1) do begin
            ReadLn(F1, sor);
            sor:= Trim(sor);
            // WriteLogFile(munkalaplogfn,'-- 1.2'+' '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
            // ActSzovegMezo:=3; // ha nem talál mintát, akkor menjen a Megjegyzésbe
            // --> nem jó, mert van hogy több soros pl. az "Igények, hibaleírás".
            // Amíg nem talál más mintát, addig a korábbi blokkba teszi.
            if copy(sor, 1, Length(Igenyek_1_minta))=Igenyek_1_minta then begin
                ActSzovegMezo:= 1;
                ActSor:= copy(sor, Length(Igenyek_1_minta)+1, 99999);  // a fennmaradó szöveg
                end
            else if copy(sor, 1, Length(Fennmaradt_2_minta))=Fennmaradt_2_minta then begin
                ActSzovegMezo:= 2;
                ActSor:= copy(sor, Length(Fennmaradt_2_minta)+1, 99999);  // a fennmaradó szöveg
                end
            else if copy(sor, 1, Length(Megjegyzes_3_minta))=Megjegyzes_3_minta then begin
                ActSzovegMezo:= 3;
                ActSor:= copy(sor, Length(Megjegyzes_3_minta)+1, 99999);  // a fennmaradó szöveg
                end
            else begin // ha nem blokk kezdő sor: az aktuális mezőbe mehet az egész szöveg
                ActSor:= sor;
                end;
            if trim(ActSor) <>'' then begin
              if InfoArray[ActSzovegMezo]='' then
                 InfoArray[ActSzovegMezo]:= ActSor
              else InfoArray[ActSzovegMezo]:= InfoArray[ActSzovegMezo]+ '. '+ ActSor;
              end;  // if trim
            end;  // while
        // WriteLogFile(munkalaplogfn,'-- 2'+' '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
        Res:= Query_SelectString_local('SZERVIZMUNKALAP', 'select count(*) from SZERVIZMUNKALAP where MU_MSZAM='''+munkalapszam+'''');
        // WriteLogFile(munkalaplogfn,'-- 3='+' '+Res+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
        if (Res='0') then begin
            Query_Insert (qUNI, 'SZERVIZMUNKALAP',
                [
                'MU_MSZAM', ''''+munkalapszam+'''',
                'MU_RENSZ', ''''+rendszam+'''',
                'MU_DATUM', ''''+datum+'''',
                'MU_IDO', ''''+idopont+'''',
                'MU_NETTO', netto,
                'MU_IGENY', ''''+InfoArray[1]+'''',
                'MU_FENNM', ''''+InfoArray[2]+'''',
                'MU_MEGJE', ''''+InfoArray[3]+''''
                ], False );
           end // if
        else begin
             Query_Update(qUNI, 'SZERVIZMUNKALAP',
                [
                'MU_RENSZ', ''''+rendszam+'''',
                'MU_DATUM', ''''+datum+'''',
                'MU_IDO', ''''+idopont+'''',
                'MU_NETTO', netto,
                'MU_IGENY', ''''+InfoArray[1]+'''',
                'MU_FENNM', ''''+InfoArray[2]+'''',
                'MU_MEGJE', ''''+InfoArray[3]+''''
                ], ' WHERE MU_MSZAM = '''+munkalapszam+'''', False );
            end;  // else
         WriteLogFile(munkalaplogfn,'Feldolgozva: '+munkalapszam+' '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
         if SoforSMS_Enabled then begin
             try
                Res:= '';
                if EgyebDlg.SMS_KULDES_FORMA='MYSMS' then begin
                  SoforSMS:= GetSoforSMS_JSONArray(rendszam);
                  if SoforSMS <> '[]' then begin  // ha nem üres a lista
                    Szoveg:= rendszam+' elkészült ('+ datum+ ' '+idopont+'). Megjegyzések: '+InfoArray[3];
                    torzs.Clear;
                    torzs.Add(Szoveg);
                    Res:= SMSKuldDlg.SendMySms(SoforSMS, Szoveg);
                    end  // if van kinek küldeni
                  else begin  // nincs sofőr telefonszám
                    WriteLogFile(munkalaplogfn,'ISMERETLEN telefonszám! Rendszám: '+rendszam+', munkalap: '+munkalapszam+' '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
                    end;
                 end;  // MYSMS
                if EgyebDlg.SMS_KULDES_FORMA='SMSEAGLE' then begin
                  cimzett:= GetSoforEagleSMS(rendszam);
                  cimzettlist.Clear;
                  cimzettlist.Add(cimzett);
                  // cimzett:='0036209588785@jssms.hu';
                  // cimzett:='nagyp69@gmail.com';
                  if cimzettlist.count > 0 then begin  // ha nem üres a lista
                    Szoveg:= rendszam+' elkészült ('+ datum+ ' '+idopont+'). Megjegyzések: '+InfoArray[3];
                    // torzs.Clear;
                    // torzs.Add(Szoveg);
                    // Mail(IdMessage1, SoforSMS, '', rendszam+' javítása elkészült', torzs, True,'J&S Szerviz Üzenetközpont');
                    // ------ teszt küldés nekem ----------
                    // EmailDlg.Mail(IdMessage1, 'nagyp@jsspeed.hu' , '', SoforSMS+': '+rendszam+' javítása elkészült', torzs, True,'J&S Szerviz Üzenetközpont');
                    // if EmailDlg.SendMail(IdMessage1) then begin         // ha elküldte, akkor bejegyezni az időt
                    // *********************************************** //
                     SetLength(LevelMellekletLista, 0);
                     Res:= EmailDlg.SendLevel(cimzettlist, '', 'Szerviz SMS', szoveg, True, 'Szerviz üzenetközpont',
                                    LevelMellekletLista, LevelMellekletNevek, LevelMellekletTipusok, 'multipart/mixed');
                    end  // if van kinek küldeni
                  else begin  // nincs sofőr telefonszám
                    WriteLogFile(munkalaplogfn,'ISMERETLEN telefonszám! Rendszám: '+rendszam+', munkalap: '+munkalapszam+' '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
                    Res:= 'Ismeretlen telefonszám.';
                    end;  // else
                  end;  // SMSEAGLE
                if (Res='') then begin
                    if EgyebDlg.SMS_KULDES_FORMA='MYSMS' then Kinek:= SoforSMS;
                    if EgyebDlg.SMS_KULDES_FORMA='SMSEAGLE' then Kinek:= cimzett;
                    Query_update (qUNI, 'SZERVIZMUNKALAP',
                       [
                       'MU_KULDC', 	''''+Kinek	+'''',
                       'MU_KULDD',	''''+	FormatDateTime('YYYY.MM.DD.', now)+'''',
                       'MU_KULDI', 	''''+ FormatDateTime('HH:mm:ss', now)	+''''
                       ], ' WHERE MU_MSZAM = '''+munkalapszam+'''', False );
                   WriteLogFile(munkalaplogfn,'Üzenet elküldve ['+Kinek+'] '+munkalapszam+' '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
                   // SendIrodaiLevel(rendszam, Szoveg, Kinek);  // --- az irodának is küldünk másolatot -----
                   EmailDlg.SendIrodaiLevel(rendszam, Szoveg, munkalaplogfn);  // --- az irodának is küldünk másolatot -----
                   end // if
                else begin
                   WriteLogFile(munkalaplogfn,'SIKERTELEN üzenetküldés! ['+cimzett+']. Hiba: '+Res+'. Munkalap: '+munkalapszam+' '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
                   end; // else
              except
                 on E: exception do
                   WriteLogFile(logfile,'!! EXCEPTION (munkalap SMS): '+E.Message);
                 end;  // try-except
              end;  // if SoforSMS_Enabled
         end; // if not eof
       CloseFile(F1);
       talalat:= FindNext(SearchRec);
       end;  // while
    FindClose(SearchRec);
    for i:=0 to filelista.Count - 1 do begin
      FN_from:= Munkalap_input_folder+'\'+filelista[i];
      FN_to:= Munkalap_archive_folder+'\'+filelista[i];
      WriteLogFile(munkalaplogfn,'File archiválás: '+FN_from);
      alkod:=1;
      while FileExists(FN_to) and (alkod<=999) do begin
        UjFN:= EgyebDlg.SepFrontToken(MLP_kiterjesztes, filelista[i])+ '_'+formatFloat('000', alkod)+MLP_kiterjesztes;
        FN_to:= Munkalap_archive_folder+'\'+UjFN;
        Inc(alkod);
        end; // while
      if alkod=1000 then begin
        WriteLogFile(munkalaplogfn,'File archiválás - túlcsordulás! ['+FN_to+'] ');
        end;  // if
      if MoveFile(PChar(FN_from), PChar(FN_to)) then
        WriteLogFile(munkalaplogfn,'Elmozgatva: ['+munkalapszam+'] '+FN_from+' -> '+FN_to)
      else
        WriteLogFile(munkalaplogfn,'!!! Sikertelen file mozgatás: ['+munkalapszam+'] '+FN_from+' -> '+FN_to)
      end;  // for
      WriteLogFile(munkalaplogfn,'Feldolgozás vége: '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
  except
      on E: exception do
         WriteLogFile(munkalaplogfn,'!! EXCEPTION: '+E.Message);
      end;  // try-except
  finally
    if filelista <> nil then filelista.Free;
    if torzs <> nil then torzs.Free;
    if cimzettlist <> nil then cimzettlist.Free;
    // if IdMessage1 <> nil then IdMessage1.Free;
    end;  // try-finally
  end;



function TForm1.GetSoforSMS_JSONArray(rendszam: string): string;
var
   S, DOKOD, Kontakt: string;
begin
   S:= 'select DO_KOD from rendszam_sofor where RENDSZ='''+rendszam+'''';
   with Query1 do begin
      Close; SQL.Clear;
      SQL.Add(S);
      Open;
      S:='';
      while not eof do begin
        DOKOD:=Fields[0].AsString;
        Kontakt:= Trim(EgyebDlg.GetDolgozoSMSKontakt(DOKOD));  // DOKOD-ból, "-eket is tesz köré
        S:= Felsorolashoz(S, Kontakt, ',', False);
        Next;
        end;  // if
      Result:= '['+S+']';
      end;  // with
end;

{function TForm1.GetSoforSMS(rendszam: string): string;
var
   S: string;
begin
   S:= 'select DO_EMAIL from rendszam_sofor where RENDSZ='''+rendszam+'''';
   with Query1 do begin
      Close; SQL.Clear;
      SQL.Add(S);
      Open;
      if not eof then begin
        Result:=Fields[0].AsString;
        end  // if
      else begin
        Result:='';
        end;
      end;  // with
end;
}
procedure TForm1.OpenCSVSor(CSVSor: string);
const
   FieldSeparator = 59; // ';'
   QuoteChar = 34; // '"'
begin
    with CSVReader do begin
       FieldCount_AutoDetect:= True;
       HeaderPresent:= False;
       UseFieldQuoting:= True;
       FieldSeparatorCharCode:= FieldSeparator;
       QuoteCharCode := QuoteChar;
       Close;
       DataString:= CSVSor;
       Open;
       end;  // with
end;

function TForm1.GetCSVElem(i: integer): string;
begin
    Result:= Trim(CSVReader.Fields[i-1].Value);  // index starts with 0
end;

procedure TForm1.btnArfolyamBeClick(Sender: TObject);
begin
  ArfolyamBeolvas;
end;

procedure TForm1.ArfolyamBeolvas;
const
  FejlecSorokSzama = 2;   // az "ArfolyamJelentesSzoveg" fejléc sorainak száma
var
  XMLResult, ActDB, TimeStamp, ErrorString, HibaVoltS: string;
  DBLista: TStringList;
  i, probalkozas: integer;
  hibavolt, siker: boolean;

  function GetXML: boolean;
    begin
     try
        XMLResult:= GetMNBArfolyamServiceSoap(ErrorString, arflogfn, False, '', nil).GetCurrentExchangeRates;
        Result:= True;
     except
       on E: Exception do begin
          WriteLogFile(arflogfn,'-- "GetCurrentExchangeRates" HIBA: '+E.Message+' '+DateTimeToStr(now));
          Result:= False;
          end;  // on E
        end;  // try-except
     end;

begin
  hibavolt:= False;
  WriteLogFile(arflogfn,'-- XMLResult? '+DateTimeToStr(now));
  probalkozas:= 1;
  siker:= False;
  while (probalkozas<=3) and not siker do begin
     WriteLogFile(arflogfn,'-- Próbálkozás no. '+IntToStr(probalkozas)+'... '+DateTimeToStr(now));
     siker:= GetXML;
     inc(probalkozas);
     end;
  if siker then begin
    WriteLogFile(arflogfn,'-- XMLResult OK: '+XMLResult);
    end
  else begin
    hibavolt:= True;
    end;
  if ErrorString <> '' then begin
      WriteLogFile(arflogfn,'-- XMLResult HIBA: '+ErrorString+' '+DateTimeToStr(now));
      hibavolt:= True;
      end;
  if hibavolt then HibaVoltS:='True' else HibaVoltS:='False';
  WriteLogFile(arflogfn,'-- XMLResult over, '+HibaVoltS+' '+DateTimeToStr(now));
  if not hibavolt then begin
      DBLista:= TStringList.Create;
      TimeStamp:= DateTimeToStr(now);
      try
        WriteLogFile(arflogfn,'-- Arfolyam_DBLista  = '+Arfolyam_DBLista+' '+DateTimeToStr(now));
        StringParse(Arfolyam_DBLista, ',', DBLista);
        for i:=0 to DBLista.Count-1 do begin
          if i=0 then begin
              ArfolyamJelentesSzoveg.Clear;  // nullázzuk a globális változót
              ArfolyamJelentesSzoveg.Add(TimeStamp+sortores);
              ArfolyamJelentesSzoveg.Add(sortores);
              end;  // if
          WriteLogFile(arflogfn,'START '+DateTimeToStr(now)+'  '+DBLista[i]+' Self='+PeldanyAzonosito);
          ADOConnectionArfolyam.DefaultDatabase:=DBLista[i];
          // WriteLogFile(arflogfn,'-- 1:'+DateTimeToStr(now));
          ExchangeRatesFromXML (DBLista[i], kuldendo_devizak, XMLResult);  // a qUniArfolyam-ot használja mindenhez, ami az ADOConnectionArfolyam-on át kapcsolódik
          // WriteLogFile(arflogfn,'-- 2:'+DateTimeToStr(now));
          if (i=0) and (ArfolyamJelentesSzoveg.Count > FejlecSorokSzama) then begin  // több adatbázis esetén se küldje el többször
              // WriteLogFile(arflogfn,'-- 3:'+DateTimeToStr(now));
              ArfolyamJelent(TimeStamp);
              end; // if
          end;  // for
        WriteLogFile(arflogfn,'FINISHED '+DateTimeToStr(now)+' Self='+PeldanyAzonosito);
      finally
        if DBLista<>nil then DBLista.Free;
        end;  // try-finally
      end;  // if not hibavolt
end;

procedure TForm1.ArfolyamJelent(TimeStamp: string);
var
  S, masolat, targy, Res: string;
  IdMessage1: TIdMessage;
begin
  IdMessage1:=TIdMessage.Create(Self);
  // --- 20151124 NagyP -------
  IdMessage1.CharSet:= 'UTF-8';
  // --------------------------
  try
    IdMessage1.ContentType:='text/html';
    targy:= 'Árfolyam jelentés '+TimeStamp;
    masolat:= '';  // nem kell
    EmailDlg.Mail(IdMessage1, arfolyam_mail_cim, masolat, targy, ArfolyamJelentesSzoveg, True, 'Árfolyam betöltő');
    Res:= EmailDlg.SendMail(IdMessage1);
    if Res = '' then begin
        WriteLogFile(arflogfn,'Árfolyam jelentés elküldve ('+arfolyam_mail_cim+')  '+DateTimeToStr(now));
        end
    else begin
        WriteLogFile(arflogfn,'SIKERTELEN árfolyam jelentés küldés! ('+Res+') '+DateTimeToStr(now));
        end;  // else
  finally
     IdMessage1.Free;
     end;  // finally
end;

function TForm1.LegutobbiArfolyamNap(ActCurr: string): string;  // visszaadja a dátumot
var
  S: string;
begin
  with qUniArfolyam do begin
      Close; SQL.Clear;
      S:='select max(ar_datum) from arfolyam where ar_valnem='''+ActCurr+'''';
      SQL.Add(S);
      Open;
      Result:=Fields[0].AsString;
      end;  // with qUniArfolyam
end;

function TForm1.LegutobbiArfolyam(ActCurr, ActDate: string): TArfolyamRec;  // visszaadja az árfolyamot
var
  S: string;
begin
  with qUniArfolyam do begin
      Close; SQL.Clear;
      S:='select ar_ertek, ar_egyseg from arfolyam where ar_valnem='''+ActCurr+''' and ar_datum = '''+ActDate+'''';
      SQL.Add(S);
      Open;
      Result.ertek:=StringReplace(Fields[0].AsString,',','.',[rfReplaceAll]); // a tizedes vesszőt pontra cseréljük
      Result.egyseg:=Fields[1].AsString;
      end;  // with qUniArfolyam
end;

procedure TForm1.StoreArfolyam(const DBNev: string; const Devizalista: TStringList; ActDate, ActCurr, ActUnit, ActRate: string);
var
  LegutobbiDatum, ActDatum, ElozoErtek, ElozoEgyseg: string;
  ElozoArf: TArfolyamRec;
  NemKell: integer;
begin
  LegutobbiDatum:= LegutobbiArfolyamNap (ActCurr);
  if LegutobbiDatum < ActDate then begin  // tárolni kell
    ActDatum:= DatumHozNap(LegutobbiDatum, 1, True);
    // kitöltjük a hiányzó napokat is, ha vannak.
    // Kellene viszont majd jelezni, ha munkanaphoz tartozó árfolyam hiányzik! Forrás: NAPOK adattábla.
    if ActDatum < ActDate then begin
      ElozoArf:= LegutobbiArfolyam(ActCurr, LegutobbiDatum); // ezt kell beírni a hiányzó napokra
      while ActDatum < ActDate do begin
         StoreArfolyam_core(DBNev, ActDatum, ActCurr, ElozoArf.egyseg, ElozoArf.ertek, 'MNB WEB Service (szünnap)');
         ActDatum:= DatumHozNap(ActDatum, 1, True);
         end; // while
      end;  // if
    StoreArfolyam_core(DBNev, ActDate, ActCurr, ActUnit, ActRate, 'MNB WEB Service');
    if Devizalista.Find(ActCurr, NemKell) then begin
        ArfolyamJelentesSzoveg.Add('   '+ActUnit+ ' '+ ActCurr +' = '+ ActRate + ' Ft'+ sortores);   //  forma: 1 EUR = 303.37 Ft
        end;
    end;  // if
end;

procedure TForm1.StoreArfolyam_core(const DBNev: string; ActDate, ActCurr, ActUnit, ActRate, Megje: string);
var
  LegutobbiDatum: string;
  kod: integer;
begin
	 kod:= GetNextArfolyamCode;
   Query_Insert (qUniArfolyam, 'ARFOLYAM',
						[
						'AR_KOD', 	 IntToStr(kod),
						'AR_EGYSEG', ActUnit,
						'AR_VALNEM', ''''+ActCurr+'''',
						'AR_ERTEK', ActRate,
						'AR_DATUM', ''''+ActDate+'''',
						'AR_MEGJE', ''''+Megje+''''
						], False );
    WriteLogFile(arflogfn,' '+DBNev+': '+ActCurr+'('+ActUnit+')= '+ActRate+' ['+ActDate+']'+' '+FormatDateTime('HH:mm:ss', now)+' Self='+PeldanyAzonosito);
end;

function TForm1.Query_Select_local( tabla, mezo, keres, output : string) : string;
var
	Query1	: TADOQuery;
begin
	Result	:= '';
	Query1	:= TADOQuery.Create(nil);
  Query1.Connection:= ADOCOnnectionAlap;
	EgyebDlg.SeTADOQueryDatabase(Query1);
	if Query_Run(Query1, 'SELECT '+output+' FROM '+tabla+' WHERE '+mezo+'='''+keres+'''') then begin
	 	Result	:= Query1.Fields[0].AsString;    // így megenged kifejezést is, pl. count(KOD)
		Query1.Close;
	end;
	Query1.Destroy;
end;


function TForm1.Query_SelectString_local( tabla, SQL: string) : string;
var
	Query1	: TADOQuery;
begin
	Result	:= '';
  Query1	:= TADOQuery.Create(nil);
  Query1.Connection:= ADOCOnnectionAlap;
	if Query_Run(Query1, SQL) then begin
	 	Result	:= Query1.Fields[0].AsString;    // így megenged kifejezést is, pl. count(KOD)
		Query1.Close;
	end;
	Query1.Free;
end;

{function TForm1.Query_Insert (sourcequery : TADOQuery; maintable : string;
						mezok : array of string) : boolean;
var
	i: integer;
	str, str2, ex_str, EgeszSQL: string;
const
   repeat_nr       : integer = 1;
begin
	Result		:= true;
 	if High(mezok) < 1 then begin
 		  Result := false;
    	Exit;
	    end;
 	sourcequery.Close;
	sourcequery.SQL.Clear;
 	i	:= 0;
  	str	:= '';
  	str2	:= '';
 	while i <= High(mezok) do begin
 		str	:= str + mezok[i] ;
  	str2	:= str2 + mezok[i+1] ;
    if i < ( High(mezok) -1 ) then begin
    		str 	:= str + ', ';
    		str2 	:= str2 + ', ';
    	  end;  // if
    i := i + 2;
   	end;  // while
  EgeszSQL:='INSERT INTO ' + maintable+ '(' + str +' ) VALUES ( ' + str2 + ' ) ';
 	sourcequery.Sql.Add(EgeszSQL);
 	try
  		sourcequery.ExecSQL;
 	except
      on E : Exception do begin
          ex_str := E.Message;
          WriteLogFile(logfile, 'Hibás SQL: '+sourcequery.SQL[0]+ '('+ex_str+')');
	  	    Result	:= false;
          end;
       end; // try-except
 	sourcequery.Close;
end;

function TForm1.Query_Update (sourcequery : TADOQuery; maintable : string;
						mezok : array of string; wherestr : string) : boolean;
var
	i: integer;
	str, logstr, ex_str: string;
const
   repeat_nr       : integer = 1;
begin
	Result		:= true;
 	if High(mezok) < 1 then begin
 		Result := false;
    	Exit;
 	  end;
	sourcequery.Close;
	sourcequery.SQL.Clear;
 	sourcequery.Sql.Add('UPDATE '+maintable+' SET ');
  logstr	:= 'UPDATE '+maintable+' SET ';
 	i	:= 0;
 	while i <= High(mezok) do begin
 		str	:= mezok[i] + '='+mezok[i+1];
    	if i < ( High(mezok) -1 ) then begin
    		str := str + ', ';
    	end;
 		sourcequery.Sql.Add(str);
	   	logstr	:= logstr + str;
    	i := i+2;
 	  end; // while
	if wherestr <> '' then begin
  		sourcequery.Sql.Add(wherestr);
	   	logstr	:= logstr +' '+ wherestr;
    	end;

  	try
  		sourcequery.ExecSQL;
  	except
      on E : Exception do begin
          ex_str := E.Message;
          WriteLogFile(logfile, 'Hibás SQL: '+sourcequery.SQL[0]+ '('+ex_str+')');
	  	    Result	:= false;
          end; // on E
    	end; // try - except
  	sourcequery.Close;
end;
       }

procedure TForm1.ExchangeRatesFromXML(const DBNev: string; const Devizalista: TStringList; const aXML:AnsiString);
var
    RootNode, DayNode, RateNode: TTreeNode;
    ActDate, ActCurr, TempFileName, FuvarosDate, ActRate, ActUnit: string;

begin
  TempFileName:= ExtractFilePath( Application.ExeName)+'LegutobbiArfolyam.xml';
  TFile.WriteAllText(TempFileName, aXML);
  frmXMLViewer:=TfrmXMLViewer.Create(Self);
  // WriteLogFile(arflogfn,'-- 1.1:'+DateTimeToStr(now));
  with frmXMLViewer do begin
    LoadDoc(TempFileName);
    // WriteLogFile(arflogfn,'-- 1.2:'+DateTimeToStr(now));
    RootNode:=GetRootNode;
    RootNode:=GotoChild(RootNode, 'MNBCurrentExchangeRates');
    DayNode:=GotoChild(RootNode, 'Day');
    ActDate:=GetNodeAttrib(DayNode, 'date');
    RateNode:=GotoChild(DayNode, 'Rate');
    // WriteLogFile(arflogfn,'-- 1.3:'+DateTimeToStr(now));
    while RateNode<>nil do begin
       ActCurr:=GetNodeAttrib(RateNode, 'curr');
       ActUnit:=GetNodeAttrib(RateNode, 'unit');
       ActRate:=RateNode.getFirstChild.Text;
       // "2015-06-08" --> "2015.06.08."
       FuvarosDate:=copy(ActDate,1,4)+'.'+copy(ActDate,6,2)+'.'+copy(ActDate,9,2)+'.';
       // "199,12" --> "199.2"
       ActRate:=StringReplace( ActRate,',','.',[rfReplaceAll]);
       // WriteLogFile(arflogfn,'-- 1.5:'+DateTimeToStr(now));
       StoreArfolyam(DBNev, Devizalista, FuvarosDate, ActCurr, ActUnit, ActRate);
       // WriteLogFile(arflogfn,'-- 1.6:'+DateTimeToStr(now));
       RateNode:=GetNextChild(DayNode, RateNode);
       end;  // while
     Release;  //  frmXMLViewer
     end;  // with
end;

function TForm1.DateTimeMinusz(dati: TDateTime; perc: integer): TDateTime;
var
  SDT: TTimeStamp;
begin
      SDT:= DateTimeToTimeStamp(dati);
      SDT.Time:=SDT.Time-(1000*60*perc);
      Result:=TimeStampToDateTime(SDT);
end;


function TForm1.Szotar_(fo, al, mezo: string): string;
begin
     Szotar.Close;
     Szotar.Parameters[0].Value:=fo;
     Szotar.Parameters[1].Value:=al;
     Szotar.Open;
     Szotar.First;
     if mezo='M' then
       Result:=Szotarsz_menev.Value
     else
       Result:=Szotarsz_leiro.Value;
     Szotar.Close;
end;

function TForm1.Feltolt(mit: string; mivel: char; hossz: integer): string;
var
  h: integer;
  s:string;
begin
  s:=Trim(mit);
  h:=hossz - Length(s);
  if h>0 then
    Result:= StringOfChar(mivel,h)+s
  else
    Result:=s;
end;

procedure TForm1.Button10Click(Sender: TObject);
begin
   KULCSKiegyenlitesImport;
end;


procedure TForm1.Button11Click(Sender: TObject);
begin
   MozdulatlanAutok;
end;

procedure TForm1.Button12Click(Sender: TObject);
begin
   // HecPollImport;
   KutasFigyelmeztetes;
   KutasNapiJelentes;
end;

procedure TForm1.Button13Click(Sender: TObject);
begin
  Fizetesi_jegyzek_feldolgozas;
  Osztrak_bejelentes_feldolgozas;
  ATJELENT_Frissit;
  // Probakuldes;
end;


procedure TForm1.HecPollImport;
var
  S: string;
begin
  Application.CreateForm(TTankTranOlvasoDlg, TankTranOlvasoDlg);
  try
   S:= TankTranOlvasoDlg.TankolasAtad (kutaslogfn);
    if S <> '' then begin
       WriteLogFile(kutaslogfn, 'Hiba a HecPoll tankolás feldolgozásban: '+S+' '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
       end
    else begin
       WriteLogFile(kutaslogfn, 'Sikeres HecPoll tankolás feldolgozás.'+' '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
       end;
    S:= TankTranOlvasoDlg.TankSzintAtad;
    if S <> '' then begin
       WriteLogFile(kutaslogfn, 'Hiba a HecPoll tartály szint feldolgozásban: '+S+' '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
       end
    else begin
       WriteLogFile(kutaslogfn, 'Sikeres HecPoll tartály szint feldolgozás.'+' '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
       end;
    S:= TankTranOlvasoDlg.UzemanyagArAtad;
    if S <> '' then begin
       WriteLogFile(kutaslogfn, 'Hiba a HecPoll üzemanyag ár átadásban: '+S+' '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
       end
    else begin
       WriteLogFile(kutaslogfn, 'Sikeres HecPoll üzemanyag ár átadás.'+' '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
       end;
  finally
    if TankTranOlvasoDlg <> nil then TankTranOlvasoDlg.Free;  // a Release dobott egy Access Vioalation-t
    end;  // try-finally
end;

procedure TForm1.Button15Click(Sender: TObject);
begin
   SMSEagle_outbox_check;
end;

procedure TForm1.Button16Click(Sender: TObject);
begin
   VCARDExport_All;
end;

procedure TForm1.Button17Click(Sender: TObject);
begin
   EDI_ALL_Export;
end;

procedure TForm1.Button18Click(Sender: TObject);
begin
  RESTAPIKozosDlg.Show;
  RESTAPIKozosDlg.Fuvaros_backend_szinkron;
end;


procedure TForm1.EDI_ALL_Export;
var
  SzamlaCount, i, FTPPORT: integer;
  Szamlaszam, NewEDIFileName, INISzekcioLista, S, EDIFACTdir, SzekcioName, HibaHely, FelelosCimzett: string;
  FTPHOST, FTPUSER, FTPPASSWORD, FTPDIR, SzekcioVevokodLista, SzekcioKezdoDatum, LogS, Res: string;
  SzekcioLista: TStringList;
begin
  WriteLogFile(ediexportlogfn,'EDI export kezdete: '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
  INISzekcioLista:= INI.ReadString('GENERAL','EDIGROUPS', '');  // pl. EDI_SANMINA,EDI_TYCO
  EDIFACTdir		:= EgyebDlg.Read_SZGrid('480', 'SANMINA_EDI_MAPPA');
  if EDIFACTdir = '' then begin
     EDIFACTdir  := EgyebDlg.TempPath;
     end;
  EDIFACTdir		:= IncludeTrailingPathDelimiter(EDIFACTdir);
  FelelosCimzett:= GetFelelosEmail('EDIFACT_KULDES').CommaText;
  SzekcioLista:= TStringList.Create;
  Application.CreateForm(TEDIFACT_ExportDlg, EDIFACT_ExportDlg);
  try
   try
    StringParse(INISzekcioLista, ',', SzekcioLista);
    for i:=0 to SzekcioLista.Count-1 do begin
      HibaHely:= 'P1';
      SzekcioName:= SzekcioLista[i];
      SzekcioVevokodLista:= INI.ReadString(SzekcioName,'VEVOKOD','');
      SzekcioKezdoDatum:= INI.ReadString(SzekcioName,'KEZDODATUM','');  // ennél korábbi számlákat nem exportálunk
      FTPHOST:= INI.ReadString(SzekcioName,'FTPHOST','');
      FTPUSER:= DecodeString(INI.ReadString(SzekcioName,'FTPUSER',''));
      FTPPASSWORD:= DecodeString(INI.ReadString(SzekcioName,'FTPPASSWORD',''));
      FTPDIR:= INI.ReadString(SzekcioName,'FTPDIR','');
      FTPPORT:= StrToInt(INI.ReadString(SzekcioName,'FTPPORT','21'));
      S:= 'select distinct SA_KOD from SZFEJ where SA_VEVOKOD in ('+SzekcioVevokodLista+') '+
          ' and SA_DATUM>='''+SzekcioKezdoDatum+''' and SA_RESZE = 0 '+  // csak egyedi számla
          ' and isnull(SA_EDISENT, '''')='''' and SA_KOD <> '''' ';
      HibaHely:= 'P2';
      Query_run(QueryMain, S, False);
      HibaHely:= 'P3';
      while not QueryMain.eof do begin
        Szamlaszam:= QueryMain.FieldByName('SA_KOD').AsString;
        HibaHely:= 'P4';
        WriteLogFile(ediexportlogfn,' Feldolgozás: '+Szamlaszam+' '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
        Res:= EDIFACT_ExportDlg.FileIro(Szamlaszam, EDIFACTdir, nil, ediexportlogfn, NewEDIFileName);
        if Res='' then begin
            HibaHely:= 'P5';
            WriteLogFile(ediexportlogfn,' Exportálva: '+Szamlaszam+' '+NewEDIFileName+' '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
            if FTPHOST <> '' then begin
              Res:= EgyebDlg.FTP_Upload(NewEDIFileName, FTPHOST, FTPUSER, FTPPASSWORD, FTPDIR, FTPPORT);
              if Res='' then begin
                  S:= 'Számlaszám: '+Szamlaszam+' ('+FTPHOST+' '+FTPDIR+')';
                  WriteLogFile(ediexportlogfn,' Felküldve - '+S+' '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
                  EmailDlg.SendSimaIrodaiLevel(FelelosCimzett, 'EDIFACT elküldve ('+SzekcioName+')', S, ediexportlogfn);
                  end
              else begin
                  S:= ' HIBA a feltöltés során: '+Res;
                  WriteLogFile(ediexportlogfn, S+' '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
                  EmailDlg.SendSimaIrodaiLevel(FelelosCimzett, 'EDIFACT hiba ('+SzekcioName+')', S, ediexportlogfn);
                  end;
              end
            else begin
              WriteLogFile(ediexportlogfn,' FTP küldés kikapcsolva (FTPHOST üres) '+Szamlaszam+' '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
              end;
            HibaHely:= 'P6';
            LogS:= FormatDateTime('YYYY.MM.DD. HH:mm:ss', now)+' '+FTPHOST+' '+FTPDIR+' '+NewEDIFileName;
            Query_Update (qUNI, 'SZFEJ',
                        [
                        'SA_EDISENT', ''''+LogS+ ''''
                        ], 'where SA_KOD='''+ Szamlaszam+'''', False);  // ne legyen NoticeKi, ha hiba van
            Inc(SzamlaCount);
            end
        else begin  // sikertelen generálás
            S:= ' EDI generálás ('+Szamlaszam+'): '+Res;
            WriteLogFile(ediexportlogfn, S+' '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
            EmailDlg.SendSimaIrodaiLevel(FelelosCimzett, 'EDIFACT hiba ('+SzekcioName+')', S, ediexportlogfn);
            end;
        QueryMain.Next;
        end;  // while
    end;  // for
   except
     on E: Exception do begin
        S:= ' HIBA az exportálás során ('+HibaHely+'): '+E.Message;
        WriteLogFile(ediexportlogfn, S+' '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
        EmailDlg.SendSimaIrodaiLevel(FelelosCimzett, 'EDIFACT hiba ('+SzekcioName+')', S, ediexportlogfn);
        end;  // on E
     end;  // try-except
  finally
    EDIFACT_ExportDlg.Free;
    end;  // try-finally
  WriteLogFile(ediexportlogfn,'EDI export vége. Exportálva: '+IntToStr(SzamlaCount)+' számla. '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
end;

function TForm1.VCARDExport_All: string;
const
  DB_darabszam = 3;  // ennyi mező van az adatbázisban
  Kozos_lista_neve = 'KOZOS';
  Iroda_lista_neve = 'IRODA';
  Gkv_lista_neve = 'GKV';
var
  Datum, lgstr, Res, S, Ceg1, Ceg2, Ceg3, VCARDExport_Dir, FileNev, KodLista, DBCEG, IRODACSOPLISTA, FN, CegNev, RefFN: string;
  Tipus, KMOraAllas, i, j: integer;
  Siker: boolean;
  VCARDExport_DBLista, timerinifile, DatabaseType, DatabaseName, Database_Ugyfellista, Database_user, Database_pwd: string;
  HovaKellMasolni, Cel1, VCARD_ROOT, CelFolder, Masolo_batch: string;
  DBLista, KoztesCel, HelyiCelTukor: TStringList;
  TimerIni: TIniFile;

     procedure Almappa_init(const FullAlmappa: string);
       begin
         if KoztesCel.IndexOf(FullAlmappa) = -1 then begin  // törölni, ha még nem volt
            if DirectoryExists(FullAlmappa) then
              EgyebDlg.DelFilesFromDir(FullAlmappa, '*.*', False)
            else ForceDirectories (FullAlmappa);
            KoztesCel.Add(FullAlmappa);
            end;
       end;

     function GetKodSQLLista(Q: TAdoQuery): string;
       var
          S: string;
       begin
          S:= '';
          while not Q.eof do begin
            S:= Felsorolashoz(S, Q.Fields[0].AsString, ',', False);
            Q.Next;
            end;
          Result:= S;
       end;

       function FurcsaKarakterekCsere(const UID: string): string;
           var
              S: string;
           begin
              S:= UID;
              S := StringReplace(S, '@', '_', [rfReplaceAll]);
              S := StringReplace(S, '.', '_', [rfReplaceAll]);
              S := StringReplace(S, '+', '_', [rfReplaceAll]);
              S := StringReplace(S, '/', '_', [rfReplaceAll]);
              Result:= S;
           end;

        procedure WriteOneVCARD(const HovaKellMasolni, MyFileName, FileContent: string);
          var
            Cel1, Cel1Fizikai, CelLista: string;
            F2: TextFile;
          begin
            CelLista:= HovaKellMasolni;
            while CelLista <> '' do begin
               Cel1:= EgyebDlg.SepFrontToken(',', CelLista);
               Cel1Fizikai:= VCARD_ROOT + TimerIni.ReadString('VCARD', Cel1, '');
               Almappa_init(Cel1Fizikai);
               AssignFile(F2, Cel1Fizikai+'\'+MyFileName);
               Rewrite(F2);
               Writeln(F2, FileContent);
               CloseFile(F2);
               CelLista:= EgyebDlg.SepRest(',', CelLista);
               end;  // while
          end;

        procedure WriteVCARD_OneSet(const CelFolder, Cegnev, DOKODList, VEVOKODList: string);
        var
          S, Vezeteknev, Keresztnev, Mobilszam, IrodaiSzam, Email, Beosztas, Szervezet, UID: string;
          EvSzama, HonapSzama, NapSzama, MyFileName: string;
          DolgozoMegy, KozosFile: boolean;
        begin
           if (DOKODList = '') and (VEVOKODList = '') then begin
              Exit;  // ne írjon üres állományt
              end;
           // ---------------------  D O L G O Z O ------------------- //
           if DOKODList <> '' then begin
               EvSzama:= copy(EgyebDlg.MaiDatum, 1,4);
               HonapSzama:= copy(EgyebDlg.MaiDatum, 6,2);
               NapSzama:= copy(EgyebDlg.MaiDatum, 9,2);
               S:= ' select NEV, NEVPLUSZ, EMAIL, IRODAISZAM, MOBILSZAM, BEOSZTAS, SZABI from '+
                ' (select DO_NAME NEV, DO_VCDADD NEVPLUSZ, isnull(DO_EMAIL,'''') EMAIL, isnull(DO_IRODAITEL,'''') IRODAISZAM, isnull(TE_TELSZAM, '''') MOBILSZAM, DO_BEOSZTAS beosztas, '+
                ' isnull(TA_NAP'+NapSzama+', '''') SZABI,'+
                '		 DOELS, RANK() OVER (PARTITION BY DO_NAME ORDER BY DOELS desc) TELSZAM_SORREND '+
                '         from dolgozo '+
                '         left outer join '+
                '         (select TK_DOKOD, TE_TELSZAM, isnull(TE_DOELS,0) DOELS from TELELOFIZETES, TELKESZULEK where TE_TKID=TK_ID and TE_TECH=0 '+
                '          union '+
                '          select TE_DOKOD, TE_TELSZAM, isnull(TE_DOELS,0) DOELS from TELELOFIZETES where TE_TECH=0 ) dotel on TK_DOKOD = DO_KOD '+
                '         left outer join TAVOLLET on TA_DOKOD = DO_KOD and TA_EV='+EvSzama+' and TA_HONAP='+HonapSzama+' '+
                '         where DO_ARHIV=0 '+
                '         and DO_KOD in ('+DOKODList+') '+
                '       ) a     '+
                ' where TELSZAM_SORREND = 1 '+  // lehetőleg az elsődleges szám kerüljön be
                ' order by NEV ';
              DolgozoMegy:= True;
              end;
           // ---------------------  V E V O   ------------------- //
           if VEVOKODList <> '' then begin
             S:= 'select V_NEV nev, V_TEL irodaiszam, VE_MOBIL mobilszam, VE_EMAIL email, '''' beosztas '+
                ' from vevo '+
                ' where VE_ARHIV=0 and V_KOD in ('+VEVOKODList+') '+
                ' order by V_NEV  ';
              DolgozoMegy:= False;
              end;
           Query_Run(QueryMulti, S, False);
           with QueryMulti do begin
             while not EOF do begin
               if DolgozoMegy then begin
                  Vezeteknev:= EgyebDlg.SepFronttoken(' ', FieldByName('NEV').AsString);
                  Keresztnev:= EgyebDlg.SepRest(' ', FieldByName('NEV').AsString);
                  if (FieldByName('NEVPLUSZ').AsString <> '') then  // a név után szerepeltetendő extra infó, pl. "négykézben Kovács Lajossal"
                    // Keresztnev:= Keresztnev+ ' '+FieldByName('NEVPLUSZ').AsString;
                    Keresztnev:= Keresztnev+ FieldByName('NEVPLUSZ').AsString;
                  if (FieldByName('SZABI').AsString = 'SZ') or (FieldByName('SZABI').AsString = 'B') then  // a betegséget nem jelezzük külön
                    Keresztnev:= Keresztnev+ ' (szabadságon)';
                  Szervezet:= Cegnev;
                  end
               else begin  // cég nevet ne daraboljon
                  Vezeteknev:= FieldByName('NEV').AsString;
                  Keresztnev:= '';
                  Szervezet:= '';
                  end;
               Mobilszam:= FieldByName('MOBILSZAM').AsString;
               IrodaiSzam:= FieldByName('IRODAISZAM').AsString;
               Email:= FieldByName('EMAIL').AsString;
               Beosztas:= FieldByName('beosztas').AsString;
               if (Trim(Mobilszam) <> '') or (Trim(IrodaiSzam) <> '') or (Trim(Email) <> '') then begin  // csak ha van értelmes kontakt infó
                 UID := Mobilszam+'_'+Email;
                 // filenév-kompatibilissá alakítjuk
                 UID := FurcsaKarakterekCsere(UID)+ '.vcf';
                 S:= GetVCARD(Vezeteknev, Keresztnev, Mobilszam, IrodaiSzam, Email, Beosztas, Szervezet, UID);
                 HovaKellMasolni:= TimerIni.ReadString('VCARD','VCARD_EXPORT_DIR_'+CelFolder, '');
                 WriteOneVCARD(HovaKellMasolni, UID, S);
                 end;  // if
               Next;
               end;  // while
              Close;
              end;  // with
        end;

        procedure WriteVCARD_Egyeb(const CelFolder, FileName: string);
        var
          S, Vezeteknev, Keresztnev, Mobilszam, IrodaiSzam, Email, Beosztas: string;
          EvSzama, HonapSzama, NapSzama, UID, MyFileName: string;
          DolgozoMegy, KozosFile: boolean;
        begin
           S:= 'select EK_NEV NEV, EK_MOBILTEL MOBILSZAM, EK_EMAIL EMAIL, EK_IRODAITEL IRODAISZAM, EK_BEOSZTAS BEOSZTAS from EKONTAKT where EK_VALID = 1';
           Query_Run(QueryMulti, S, False);
           if QueryMulti.Eof then begin
              Exit;  // ne írjon üres állományt
              end;
           with QueryMulti do begin
             while not EOF do begin
               Vezeteknev:= FieldByName('NEV').AsString;
               Keresztnev:= '';
               Mobilszam:= FieldByName('MOBILSZAM').AsString;
               IrodaiSzam:= FieldByName('IRODAISZAM').AsString;
               Email:= FieldByName('EMAIL').AsString;
               Beosztas:= FieldByName('BEOSZTAS').AsString;
               UID := Mobilszam+'_'+Email;
               // filenév-kompatibilissá alakítjuk
               UID := FurcsaKarakterekCsere(UID)+ '.vcf';
               S:= GetVCARD(Vezeteknev, Keresztnev, Mobilszam, IrodaiSzam, Email, Beosztas, '', UID);
               HovaKellMasolni:= TimerIni.ReadString('VCARD','VCARD_EXPORT_DIR_'+CelFolder, '');
               WriteOneVCARD(HovaKellMasolni, UID, S);
               Next;
               end;  // while
              Close;
              end;  // with
        end;

     procedure ExportEgyCeg(const DBCeg, CegNev, listanev, IrodaCsoplista: string);
       var
          S, FileNev, Almappa: string;
       begin
         CelFolder:= EgyebDlg.TempList;
         if listanev = Kozos_lista_neve then begin
            Almappa:= listanev;  // külön állományok ide készülnek
            end
         else begin
           Almappa:= DBCeg+'_'+listanev;  // külön állományok ide készülnek
           end;
         if listanev = Kozos_lista_neve then begin
           S:= 'select DO_KOD from DOLGOZO where DO_ARHIV=0 and isnull(DO_KOZOSLISTA, 0) = 1';
           end;
         if (listanev = Iroda_lista_neve) or (listanev = Gkv_lista_neve) then begin
           S:= 'select DO_KOD from DOLGOZO '+
               ' left outer join DOLGOZOCSOP on DC_DOKOD = DO_KOD '+
               ' where '+
               ' DC_DATTOL= (select max(cs2.DC_DATTOL) from DOLGOZOCSOP cs2 where cs2.DC_DOKOD=DO_KOD and DC_DATTOL<='''+EgyebDlg.MaiDatum+''') '+
               ' and DO_ARHIV=0 and isnull(DO_KOZOSLISTA, 0) = 0 ';
           if listanev = Iroda_lista_neve then begin
               if  IrodaCsoplista <>'' then
                 S:= S + ' and DC_CSKOD in ('+IrodaCsoplista+') '
               else S:= S+ '';  // ha nincs megadva csoport lista, akkor mindenki iroda lesz
               end; // if
           if listanev = Gkv_lista_neve then begin
               if  IrodaCsoplista <> '' then
                 S:= S + ' and DC_CSKOD not in ('+IrodaCsoplista+') '
               else S:= S+ ' and 1=2 ';  // ha nincs megadva csoport lista, akkor itt nem lesz seni
               end; // if
           S:= S + ' order by 1';
           end;
         Query_Run(QueryMulti, S, false);
         KodLista:= GetKodSQLLista(QueryMulti);
         WriteVCARD_OneSet(Almappa, CegNev, KodLista, '');  // DOLGOZO-ból exportál
       end;

       procedure ExportEgyebKontakt;
       var
          S, Almappa, FileNev: string;
       begin
         CelFolder:= EgyebDlg.TempList;
         FileNev:= Kozos_lista_neve+'.vcf';  // minden cégből ugyanide tesszük
         Almappa:= Kozos_lista_neve;  // külön állományok ide készülnek

         // New style
         WriteVCARD_Egyeb(Almappa, '');
        end;

begin
  timerinifile:= ExtractFilePath(Application.ExeName)+'INI\'+ChangeFileExt(ExtractFileName(Application.ExeName), '.INI' );
	TimerIni:= TIniFile.Create(timerinifile);
  VCARD_ROOT:= TimerIni.ReadString('VCARD','VCARD_EXPORT_LOCAL_ROOT', EgyebDlg.TempList);
  VCARDExport_DBLista:= TimerIni.ReadString('DATABASE','VCARDEXPORT_DB_LISTA', '');  // pl. DAT_DEV,DKDAT_TESZT
  Masolo_batch:=TimerIni.ReadString('VCARD','VCARD_SYNC_BATCH', '');
  DBLista:= TStringList.Create;
  KoztesCel:= TStringList.Create;
  HelyiCelTukor:= TStringList.Create;
  WriteLogFile(vcardlogfn, '============================= '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now)+' ===============================');
  try
   try
    StringParse(VCARDExport_DBLista, ',', DBLista);
    for i:=0 to DBLista.Count-1 do begin
       DatabaseName:= DBLista[i];
       WriteLogFile(vcardlogfn, '===== '+DatabaseName+' =====');
       Database_user:= Trim(DecodeString(TimerIni.ReadString( 'DATABASE-'+DatabaseName, 'USER',  '')));
       Database_pwd:= Trim(DecodeString(TimerIni.ReadString( 'DATABASE-'+DatabaseName, 'PSWD',  '')));
       lgstr:= TimerIni.ReadString( 'DATABASE', 'TANKOLAS_LOGIN_STR',  '');
       ADOConnectionMulti.Close;
       ADOConnectionMulti.ConnectionString:= lgstr+';User ID= '+Database_user+';Password='+Database_pwd+';Initial Catalog='+DatabaseName+';';
       ADOConnectionMulti.DefaultDatabase:= DatabaseName;
       ADOConnectionMulti.Open;
       S:= 'select SZ_MENEV CEG1, SZ_LEIRO from szotar where SZ_FOKOD=''470'' and SZ_ALKOD=''CEG1'' ';
       Query_Run(QueryMulti2, S, false);
       DBCEG:= QueryMulti2.FieldByName('CEG1').AsString; // akinek az adatbázisában vagyunk
       CegNev:= QueryMulti2.FieldByName('SZ_LEIRO').AsString; // ez fog szervezetként megjelenni a VCARD-ban
       S:= 'select SZ_MENEV from szotar where SZ_FOKOD=''470'' and SZ_ALKOD=''IRODACSOPLISTA'' ';
       Query_Run(QueryMulti2, S, false);
       IRODACSOPLISTA:= QueryMulti2.Fields[0].AsString;
       ExportEgyCeg(DBCEG, CegNev, Kozos_lista_neve, ''); // itt nem kell majd a csoport lista
       ExportEgyCeg(DBCEG, CegNev, Iroda_lista_neve, IRODACSOPLISTA);
       ExportEgyCeg(DBCEG, CegNev, Gkv_lista_neve, IRODACSOPLISTA);
       ExportEgyebKontakt;
       end;  // for
    // -------------------------------------
    // ---- elmozgatás a végleges helyre ---
    // -------------------------------------
   if VCARDExport_DBLista <> '' then begin  // ha van mit másolni
     WriteLogFile(vcardlogfn, 'Szinkronizálás:... ');
     ShellExecute(Application.Handle,'open',PChar(Masolo_batch), '', '', SW_SHOWNORMAL );
     WriteLogFile(vcardlogfn, 'Executed: '+Masolo_batch);
     end;
    except
      on E: exception do begin
        Result:= 'Sikertelen VCARD export (['+DatabaseName+'] '+E.Message;
        end;  // on E
      end;  // try-except
  finally
      if DBLista <> nil then DBLista.Free;
      if KoztesCel <> nil then KoztesCel.Free;
      if HelyiCelTukor <> nil then HelyiCelTukor.Free;
      end;  // try-finally
end;

procedure TForm1.Button19Click(Sender: TObject);
begin
   KilepBelepErtesitoAll;
end;

procedure TForm1.KilepBelepErtesitoAll;
var
  S: string;
begin
  S:= 'select dolgozo.DO_KOD, DO_NAME, DO_BELEP, DO_DFEOR, DO_FOTO from dolgozo left outer join DOLGOZOFOTO on dolgozo.DO_KOD = DOLGOZOFOTO.DO_KOD '+
      ' where isnull(DO_ERTBE, '''') ='''' '+
      ' and DO_BELEP>='''+ KILEPBELEP_KEZDONAP+'''';  // funkció indulás határnap
  KilepBelepErtesito(S, 'be');
  S:= 'select dolgozo.DO_KOD, DO_NAME, DO_KITERV, DO_FOTO from dolgozo left outer join DOLGOZOFOTO on dolgozo.DO_KOD = DOLGOZOFOTO.DO_KOD '+
      ' where isnull(DO_KITERV, '''')  <> '''' '+
      ' and  isnull(DO_ERTKI1, '''') = '''' and DO_KITERV>='''+ KILEPBELEP_KEZDONAP+'''';  // funkció indulás határnap
  KilepBelepErtesito(S, 'ki1');
  S:= 'select dolgozo.DO_KOD, DO_NAME, DO_KILEP, DO_FOTO from dolgozo left outer join DOLGOZOFOTO on dolgozo.DO_KOD = DOLGOZOFOTO.DO_KOD '+
      ' where isnull(DO_KILEP, '''')  <> '''' '+
      ' and  isnull(DO_ERTKI2, '''') = '''' and DO_KILEP>='''+ KILEPBELEP_KEZDONAP+'''';  // funkció indulás határnap
  KilepBelepErtesito(S, 'ki2');
end;

procedure TForm1.KilepBelepErtesito (const SQL, Mode: string);
const
  Title1 = 'Új dolgozó';
  Title2 = 'Hamarosan távozó dolgozó';
  Title3 = 'Kilépett dolgozó';
  SorTores = const_HTML_Ujsor;
var
  S, Uzenet, Title, ActSor, Res, SentMezoNev, FN, DOKODLista, DolgozoNevsor, DO_NAME, DO_NAME_NOSPACE: string;
  cimzettlista, Kuldve_DOKOD: TStringlist;
  // Stream1: TMemoryStream;
  StreamArray: TMemoryStreamArray;
  // MyStream: TStream;
  i, j, DolgozoCount: integer;
  MyBLOBField: TBLOBField;
begin
  WriteLogFile(kilepbeleplogfn,'Feldolgozás kezdete: '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
  Query_run(QueryMain, SQL, False);
  if Mode='be' then begin
    Title:= Title1;
    SentMezoNev:= 'DO_ERTBE';
    end;
  if Mode='ki1' then begin
    Title:= Title2;
    SentMezoNev:= 'DO_ERTKI1';
    end;
  if Mode='ki2' then begin
    Title:= Title3;
    SentMezoNev:= 'DO_ERTKI2';
    end;
  Title := Title + ' ('+EgyebDlg.Read_SZGrid('999', 'EDI_CEG')+')';
  Uzenet:= '<b>'+Title+':'+'</b>' + SorTores;
  // Uzenet:= Uzenet+ '-------------------------------------------------------------' + SorTores;
  SetLength(LevelMellekletLista, 0);
  LevelMellekletNevek.Clear;
  LevelMellekletTipusok.Clear;
  i:= 0;
  DolgozoCount:= 0;
  DolgozoNevsor:= '';
  cimzettlista:=TStringlist.Create;
  Kuldve_DOKOD:=TStringlist.Create;
  try
    cimzettlista:= GetFelelosEmail('DOLGOZO_KIBELEPES');
    // ---------- teszt -----------
    // cimzettlista.Add('nagyp69@gmail.com');
    // ----------------------------
    while not QueryMain.Eof do begin
      Inc(DolgozoCount);
      DO_NAME:= QueryMain.FieldByName('DO_NAME').AsString;
      DO_NAME_NOSPACE:= StringReplace(DO_NAME,' ','_',[rfReplaceAll]) ;
      ActSor:= DO_NAME;
      DolgozoNevsor:= Felsorolashoz(DolgozoNevsor, DO_NAME, ', ', false);
      // if i>0 then LevelMellekletLista[0]^.SaveToFile('D:\NagyP\Temp\xx1_'+FormatDateTime('HHMMSSZZZ', now)+'.jpg');  // debug
      if Mode='be' then begin
         ActSor:= ActSor+ ' - Belépés dátuma: ' +QueryMain.FieldByName('DO_BELEP').AsString;
         if QueryMain.FieldByName('DO_DFEOR').AsString <> '' then
            ActSor:= ActSor+ ', munkakör: '+QueryMain.FieldByName('DO_DFEOR').AsString;
         end;
      if Mode='ki1' then begin
         ActSor:= ActSor+ ' - Tervezett utolsó munkanap: ' +QueryMain.FieldByName('DO_KITERV').AsString;
         end;
      if Mode='ki2' then begin
         ActSor:= ActSor+ ' - Kilépés dátuma: ' +QueryMain.FieldByName('DO_KILEP').AsString;
         end;
      Uzenet:= Uzenet+ActSor + SorTores;
      // if i>0 then LevelMellekletLista[0]^.SaveToFile('D:\NagyP\Temp\xx2_'+FormatDateTime('HHMMSSZZZ', now)+'.jpg');  // debug
      if (QueryMain.FieldByName('DO_FOTO') as TBlobField).BlobSize >0 then begin
        inc(i);
        SetLength(LevelMellekletLista, i);
        SetLength(StreamArray, i);
        StreamArray[i-1]:= TMemoryStream.Create;
        LevelMellekletLista[i-1]:= @StreamArray[i-1];

        (QueryMain.FieldByName('DO_FOTO') as TBlobField).SaveToStream(LevelMellekletLista[i-1]^);
        // for j:= 1 to i do
        //  LevelMellekletLista[j-1]^.SaveToFile('D:\NagyP\Temp\debug'+IntToStr(j)+'_mikor='+IntToStr(i)+'.jpg');  // debug
        // LevelMellekletNevek.Add(DO_NAME_NOSPACE+'.jpg');
        LevelMellekletNevek.Add(DO_NAME);
        LevelMellekletTipusok.Add(const_JPGMIME);
        end;  // fotó csatolása
      // if i>0 then LevelMellekletLista[0]^.SaveToFile('D:\NagyP\Temp\xx3_'+FormatDateTime('HHMMSSZZZ', now)+'.jpg');  // debug
      FN:= KILEPBELEP_CSATOLTPDFDIR+'\'+QueryMain.FieldByName('DO_NAME').AsString+'\'+KILEPBELEP_CSATOLTPDFNEV;
      if FileExists(FN) then begin
        inc(i);
        SetLength(LevelMellekletLista, i);
        SetLength(StreamArray, i);
        StreamArray[i-1]:= TMemoryStream.Create;
        LevelMellekletLista[i-1]:= @StreamArray[i-1];
        LevelMellekletLista[i-1]^.LoadFromFile(FN);
        LevelMellekletTipusok.Add(const_PDFMIME);
        LevelMellekletNevek.Add(DO_NAME+' '+KILEPBELEP_CSATOLTPDFNEV);
        end; // felmondási dokumentum csatolása
      Kuldve_DOKOD.Add(''''+QueryMain.FieldByName('DO_KOD').AsString+'''');
      QueryMain.Next;
      // if i>0 then LevelMellekletLista[0]^.SaveToFile('D:\NagyP\Temp\xx4_'+FormatDateTime('HHMMSSZZZ', now)+'.jpg');  // debug
      end;  // while
    // WriteLogFile(kilepbeleplogfn,'  Szöveg OK '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
    // if i>0 then LevelMellekletLista[0]^.SaveToFile('D:\NagyP\Temp\xx5_'+FormatDateTime('HHMMSSZZZ', now)+'.jpg');  // debug
    if DolgozoCount > 0 then begin
      // if i>0 then LevelMellekletLista[0]^.SaveToFile('D:\NagyP\Temp\xx6_'+FormatDateTime('HHMMSSZZZ', now)+'.jpg');  // debug
      // for j:= 1 to i do
      //    LevelMellekletLista[j-1]^.SaveToFile('D:\NagyP\Temp\debug'+IntToStr(j)+'_SendLevelElott.jpg');  // debug
      Res:= EmailDlg.SendLevel(cimzettlista, '', Title, Uzenet, True, 'Fuvaros üzenetközpont',
                      LevelMellekletLista, LevelMellekletNevek, LevelMellekletTipusok, 'multipart/related', 'text/html'); // 'multipart/mixed'  beágyazott: 'multipart/related'
      DOKODLista:= '';
      for i:= 0 to Kuldve_DOKOD.Count-1 do begin
        DOKODLista:= Felsorolashoz(DOKODLista, Kuldve_DOKOD[i], ',', False);
        end;
      S:= 'update DOLGOZO set '+SentMezoNev+'='''+ FormatDateTime('yyyy.mm.dd. HH:mm:ss', Now) +''' where DO_KOD in ('+DOKODLista+')';
      Query_Run (qUNI, S, False);  // ne legyen NoticeKi, ha hiba van
      WriteLogFile(kilepbeleplogfn,' Értesítés küldve '+IntToStr(DolgozoCount)+' dolgozóról. '+Title+': '+DolgozoNevsor+ ' '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
      end;  // if volt valaki
    WriteLogFile(kilepbeleplogfn,'Feldolgozás vége. '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
  finally
    if cimzettlista <> nil then cimzettlista.Free;
    if Kuldve_DOKOD <> nil then Kuldve_DOKOD.Free;
    end;  // try-finally
end;

// fotó mellékletként
{procedure TForm1.KilepBelepErtesito (const SQL, Mode: string);
const
  Title1 = 'Új dolgozó';
  Title2 = 'Hamarosan távozó dolgozó';
  Title3 = 'Kilépett dolgozó';
var
  S, Uzenet, Title, ActSor, Res, SentMezoNev, FN, DOKODLista, DolgozoNevsor: string;
  cimzettlista, Kuldve_DOKOD: TStringlist;
  // Stream1: TMemoryStream;
  StreamArray: TMemoryStreamArray;
  // MyStream: TStream;
  i, j, DolgozoCount: integer;
  MyBLOBField: TBLOBField;
begin
  WriteLogFile(kilepbeleplogfn,'Feldolgozás kezdete: '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
  Query_run(QueryMain, SQL, False);
  if Mode='be' then begin
    Title:= Title1;
    SentMezoNev:= 'DO_ERTBE';
    end;
  if Mode='ki1' then begin
    Title:= Title2;
    SentMezoNev:= 'DO_ERTKI1';
    end;
  if Mode='ki2' then begin
    Title:= Title3;
    SentMezoNev:= 'DO_ERTKI2';
    end;
  Uzenet:= Title+':' + const_CRLF;
  // Uzenet:= Uzenet+ '-------------------------------------------------------------' + const_CRLF;
  SetLength(LevelMellekletLista, 0);
  LevelMellekletNevek.Clear;
  LevelMellekletTipusok.Clear;
  i:= 0;
  DolgozoCount:= 0;
  DolgozoNevsor:= '';
  cimzettlista:=TStringlist.Create;
  Kuldve_DOKOD:=TStringlist.Create;
  // Stream:= TStream.Create;
  try
    while not QueryMain.Eof do begin
      Inc(DolgozoCount);
      ActSor:= QueryMain.FieldByName('DO_NAME').AsString;
      DolgozoNevsor:= Felsorolashoz(DolgozoNevsor, QueryMain.FieldByName('DO_NAME').AsString, ', ', false);
      if Mode='be' then begin
         ActSor:= ActSor+ ' - Belépés dátuma: ' +QueryMain.FieldByName('DO_BELEP').AsString;
         if QueryMain.FieldByName('DO_DFEOR').AsString <> '' then
            ActSor:= ActSor+ ', munkakör: '+QueryMain.FieldByName('DO_DFEOR').AsString;
         end;
      if Mode='ki1' then begin
         ActSor:= ActSor+ ' - Tervezett utolsó munkanap: ' +QueryMain.FieldByName('DO_KITERV').AsString;
         end;
      if Mode='ki2' then begin
         ActSor:= ActSor+ ' - Kilépés dátuma: ' +QueryMain.FieldByName('DO_KILEP').AsString;
         end;
      Uzenet:= Uzenet+ActSor + const_CRLF;
      // MyBLOBField:= QueryMain.FieldByName('DO_FOTO') as TBlobField;
      // MyBLOBField.SaveToStream(Stream1);
      // Stream1:= TMemoryStream.Create;
      inc(i);
      SetLength(LevelMellekletLista, i);
      SetLength(StreamArray, i);
      StreamArray[i-1]:= TMemoryStream.Create;
      LevelMellekletLista[i-1]:= @StreamArray[i-1];
      // LevelMellekletLista[i-1]:= @Stream1;
      // LevelMellekletLista[i-1]:= TMemoryStream.Create;
      (QueryMain.FieldByName('DO_FOTO') as TBlobField).SaveToStream(LevelMellekletLista[i-1]^);
      // (QueryMain.FieldByName('DO_FOTO') as TBlobField).SaveToStream(Stream1);
      // Stream1.SaveToFile('D:\NagyP\Temp\debug'+IntToStr(i)+'.jpg');  // debug
      for j:= 1 to i do
        LevelMellekletLista[j-1]^.SaveToFile('D:\NagyP\Temp\debug'+IntToStr(j)+'_mikor='+IntToStr(i)+'.jpg');  // debug
      LevelMellekletNevek.Add(QueryMain.FieldByName('DO_NAME').AsString+'.jpg');
      LevelMellekletTipusok.Add(const_JPGMIME);
      FN:= KILEPBELEP_CSATOLTPDFDIR+'\'+QueryMain.FieldByName('DO_NAME').AsString+'\'+KILEPBELEP_CSATOLTPDFNEV;
      if FileExists(FN) then begin
        inc(i);
        SetLength(LevelMellekletLista, i);
        SetLength(StreamArray, i);
        StreamArray[i-1]:= TMemoryStream.Create;
        LevelMellekletLista[i-1]:= @StreamArray[i-1];
        LevelMellekletLista[i-1]^.LoadFromFile(FN);
        LevelMellekletTipusok.Add(const_PDFMIME);
        LevelMellekletNevek.Add(QueryMain.FieldByName('DO_NAME').AsString+' '+KILEPBELEP_CSATOLTPDFNEV);
        end;
      Kuldve_DOKOD.Add(''''+QueryMain.FieldByName('DO_KOD').AsString+'''');
      QueryMain.Next;
      end;  // while
    // WriteLogFile(kilepbeleplogfn,'  Szöveg OK '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
    if DolgozoCount > 0 then begin
      cimzettlista:= GetFelelosEmail('DOLGOZO_KIBELEPES');
      Res:= SendLevel(cimzettlista, '', Title, Uzenet, True, 'Fuvaros üzenetközpont',
                      LevelMellekletLista, LevelMellekletNevek, LevelMellekletTipusok, 'multipart/mixed'); // 'multipart/mixed'  beágyazott: 'multipart/related'
      DOKODLista:= '';
      for i:= 0 to Kuldve_DOKOD.Count-1 do begin
        DOKODLista:= Felsorolashoz(DOKODLista, Kuldve_DOKOD[i], ',', False);
        end;
      S:= 'update DOLGOZO set '+SentMezoNev+'='''+ FormatDateTime('yyyy.mm.dd. HH:mm:ss', Now) +''' where DO_KOD in ('+DOKODLista+')';
      Query_Run (qUNI, S, False);  // ne legyen NoticeKi, ha hiba van
      WriteLogFile(kilepbeleplogfn,' Értesítés küldve '+IntToStr(DolgozoCount)+' dolgozóról. '+Title+': '+DolgozoNevsor+ ' '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
      end;  // if volt valaki
    WriteLogFile(kilepbeleplogfn,'Feldolgozás vége. '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
  finally
    if cimzettlista <> nil then cimzettlista.Free;
    if Kuldve_DOKOD <> nil then Kuldve_DOKOD.Free;
    // if Stream1 <> nil then Stream1.Free;
    // MyStream <> nil then MyStream.Free;
    end;  // try-finally
end;
}


procedure TForm1.SMSEagle_outbox_check;
const
  ToleranciaPerc = 15;
  ToleranciaDarab = 200;
var
  S, Uzenet, Title, Lista, Res, LimitStamp: string;
  cimzettlista: TStringlist;
  i: integer;
  EagleOutboxData: TEagleOutboxData;
begin
  Title:='';
  WriteLogFile(smseaglelogfn,'Feldolgozás kezdete: '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
  EagleOutboxData:= QuerySMSEagleOutbox;
  if not(EagleOutboxData.elerheto) then begin
      WriteLogFile(smseaglelogfn,' SMSEagle nem elérhető!'+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
      Title:='SMSEagle nem elérhető!';
      Uzenet:= 'HTTP hívás nem válaszol.'+ const_CRLF;
      end
  else begin // elérhető
      WriteLogFile(smseaglelogfn,' Outbox: '+IntToStr(EagleOutboxData.darabszam)+', legrégebbi:  '+EagleOutboxData.legregebbi);
      LimitStamp:= FormatDateTime('yyyy.mm.dd.', IncMinute(now, -ToleranciaPerc))+' '+FormatDateTime('HH:mm', IncMinute(now, -ToleranciaPerc));
      WriteLogFile(smseaglelogfn,' LimitStamp: ** '+LimitStamp+' **');
      if (EagleOutboxData.darabszam > 0) and (trim(EagleOutboxData.legregebbi) <> '') then begin
        if (EagleOutboxData.legregebbi < LimitStamp) or (EagleOutboxData.darabszam > ToleranciaDarab) then begin
          WriteLogFile(smseaglelogfn,' Határérték felett!! Lista: '+EagleOutboxData.Lista+' '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
          Title:='SMSEagle kimenő levelek: torlódás!';
          Uzenet:= 'A várakozó üzenetek száma:' +IntToStr(EagleOutboxData.darabszam)+', legrégebbi: '+EagleOutboxData.legregebbi + const_CRLF;
          end; // if gubanc
        end;  // if 1
      end;  // else = elérhető
  if Title <> '' then begin
      cimzettlista:=TStringlist.Create;
      try
        cimzettlista:= GetFelelosEmail('SMSEAGLE_DUGO');
        SetLength(LevelMellekletLista, 0);
        Res:= EmailDlg.SendLevel(cimzettlista, '', Title, Uzenet, True, 'Fuvaros üzenetközpont',
                        LevelMellekletLista, LevelMellekletNevek, LevelMellekletTipusok, 'multipart/mixed');
        WriteLogFile(smseaglelogfn,' Figyelmeztetés elküldve ('+cimzettlista.delimitedtext +') '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
      finally
        if cimzettlista <> nil then cimzettlista.Free;
        end;  // try-finally
      end; // if küldés
  WriteLogFile(smseaglelogfn,'Feldolgozás vége. '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
end;

procedure TForm1.Button14Click(Sender: TObject);
begin
  SelectHosszuAtallas;
end;

procedure TForm1.SelectHosszuAtallas;
var
  S, Uzenet, Title, ActSor, Res: string;
  cimzettlista, Kuldve_MSID: TStringlist;
begin
  WriteLogFile(atallaslogfn,'Feldolgozás kezdete: '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
  // S:= 'exec spSelectHosszuAtallas '+Atallas_KMLimit+', '+Atallas_NapLimit;
  S:= 'exec spSelectHosszuAtallas2 '+Atallas_KMLimit+', '+Atallas_NapLimit;
  Query_run(QueryMain, S, False);
  WriteLogFile(atallaslogfn,'  SQL OK '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
  Title:='Hosszú ('+Atallas_KMLimit+' km feletti) export - import átállások listája az elmúlt '+Atallas_NapLimit+' napban';
  Uzenet:= Title+':' + const_CRLF;
  Uzenet:= Uzenet+ '-------------------------------------------------------------' + const_CRLF;
  Kuldve_MSID:=TStringlist.Create;
  while not QueryMain.Eof do begin
    if Kuldve_MSID.Indexof(QueryMain.FieldByName('HONNAN_MSID').AsString) < 0  then begin  // -1, ha nem találta
        ActSor:= QueryMain.FieldByName('RENDSZAM').AsString+ const_CRLF;
        ActSor:= ActSor+ 'Export lerakó: ' +QueryMain.FieldByName('EXPORT_LERAKO').AsString;
        ActSor:= ActSor+ ', indulás lerakóról: ' +QueryMain.FieldByName('INDULAS_LERAKOROL').AsString+ ' '+const_CRLF;
        ActSor:= ActSor+ 'Import felrakó: ' +QueryMain.FieldByName('IMPORT_FELRAKO').AsString;
        ActSor:= ActSor+ ', átállás hossza: ' +QueryMain.FieldByName('ATALLAS_HOSSZA').AsString+' km.'+ ' '+const_CRLF;
        ActSor:= ActSor+ 'Megbízások: export ' +QueryMain.FieldByName('EXPORT_MEGBIZAS').AsString;
        // if QueryMain.FieldByName('EXPORT_JARAT').AsString <> '' then
        //  ActSor:= ActSor+ ' (' +QueryMain.FieldByName('EXPORT_JARAT').AsString+')';
        ActSor:= ActSor+ ', import: ' +QueryMain.FieldByName('IMPORT_MEGBIZAS').AsString;
        // if QueryMain.FieldByName('IMPORT_JARAT').AsString <> '' then
        //   ActSor:= ActSor+ ' (' +QueryMain.FieldByName('IMPORT_JARAT').AsString+')';
        Uzenet:= Uzenet+ActSor + const_CRLF+ const_CRLF;
        Kuldve_MSID.Add(QueryMain.FieldByName('HONNAN_MSID').AsString);
        end;  // if
    QueryMain.Next;
    end;  // while
  WriteLogFile(atallaslogfn,'  Szöveg OK '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
  cimzettlista:=TStringlist.Create;

  try
    cimzettlista:= GetFelelosEmail('HOSSZU_ATALLAS');
    WriteLogFile(atallaslogfn,'  cimzettlista OK '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
    SetLength(LevelMellekletLista, 0);
    Res:= EmailDlg.SendLevel(cimzettlista, '', Title, Uzenet, True, 'Fuvaros üzenetközpont',
                    LevelMellekletLista, LevelMellekletNevek, LevelMellekletTipusok, 'multipart/mixed');
    Query_Insert (qUNI, 'SMS_TECH',
          [
          'ST_KATEG', ''''+'HOSSZU_ATALLAS'+'''',
          'ST_DIM1', ''''+QueryMain.FieldByName('RENDSZAM').AsString+'''',
          'ST_DIM2', ''''+QueryMain.FieldByName('HONNAN_MSID').AsString+'''',
          'ST_CIMZETT', ''''+copy(cimzettlista.Delimitedtext,1,100)+'''',
          'ST_UZENET', ''''+Uzenet+'''',
          'ST_DOKOD', ''''+''+'''',
          'ST_KULDVE', ''''+DateTimeToStr(now)+''''
          ], False );  // ne legyen NoticeKi, ha hiba van
    WriteLogFile(atallaslogfn,'Feldolgozás vége. '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
  finally
    if cimzettlista <> nil then cimzettlista.Free;
    if Kuldve_MSID <> nil then Kuldve_MSID.Free;
    end;  // try-finally
end;


procedure TForm1.SzamlaszamEllenor;
var
  S, Uzenet, Title, Res: string;
  cimzettlista: TStringlist;
begin
  WriteLogFile(szamlakodlogfn,'Feldolgozás kezdete: '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
  S:= 'select szami.SzamlaEllenor()';
  Query_run(QueryMain, S, False);
  WriteLogFile(szamlakodlogfn,'  SQL OK '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
  Title:='Számlaszám ellenőr';
  Uzenet:= QueryMain.Fields[0].AsString;
  WriteLogFile(szamlakodlogfn,'  Üzenet: '+Uzenet+' '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
  if Uzenet <> '' then begin
      cimzettlista:=TStringlist.Create;
      try
        cimzettlista:= GetFelelosEmail('SZAMLA_ELLENOR');
        WriteLogFile(szamlakodlogfn,'  cimzettlista OK '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
        SetLength(LevelMellekletLista, 0);
        Res:= EmailDlg.SendLevel(cimzettlista, '', Title, Uzenet, True, 'Fuvaros üzenetközpont',
                        LevelMellekletLista, LevelMellekletNevek, LevelMellekletTipusok, 'multipart/mixed');
        if Res = '' then
          WriteLogFile(szamlakodlogfn,'  Küldés OK! '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now))
        else WriteLogFile(szamlakodlogfn,'  Küldés gubanc: '+Res+' '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now))
      finally
        if cimzettlista <> nil then cimzettlista.Free;
        end;  // try-finally
      end;  // if
 WriteLogFile(szamlakodlogfn,'Feldolgozás vége. '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
end;

procedure TForm1.MegbizasDebug;
var
  S: string;
begin
  WriteLogFile(megbizasdebuglogfn,'Feldolgozás kezdete: '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
  // S:= 'exec spSelectHosszuAtallas '+Atallas_KMLimit+', '+Atallas_NapLimit;
  S:= 'insert into debug (d1, d2, d3, d4, d5, d6) '+
       '	select ''MEGBIZAS DEBUG'', CONVERT(varchar(11), getdate(),102)+''. ''+convert(varchar(5),getdate(),108), '+
       ' MB_MBKOD, MB_JAKOD, MB_JASZA, MB_VIKOD from MEGBIZAS '+
       ' where MB_DATUM>= CONVERT(varchar(11), dateadd(d, -10, getdate()),102)+''.'' ';
  Command_run(ADOCommand1, S, False);
end;

procedure TForm1.KutasNapiJelentes;
var
  S, Uzenet, Title, Napi_mikor, Napi_kuldve, Res, Tank2Nev, Tank3Nev: string;
  cimzettlista: TStringlist;
  SzamitottIdopont, MertIdopont: string;
begin
  Napi_mikor:= EgyebDlg.Read_Szotar('154', 'NAPI_IDOPONT');
  Napi_kuldve:= EgyebDlg.Read_Szotar('154', 'NAPI_KULDVE');
  Tank2Nev:= EgyebDlg.Read_Szotar('154', 'NEV2');
  Tank3Nev:= EgyebDlg.Read_Szotar('154', 'NEV3');
  // Kutasdat_mikor:= EgyebDlg.Read_Szotar('154', 'GET_IDOPONT');  // hány órai lekérdezés eredményét küldjük el? 00:10 körül, így szinkronban lesz a mért és a számított érték

  if (EgyebDlg.MaiDatum > Napi_kuldve) and (FormatDateTime('HH:mm', now)>= Napi_mikor) then begin   // ma még nem küldtünk és ma kell már küldeni
  // if true then begin
    cimzettlista:=TStringlist.Create;
    try
        cimzettlista:= GetFelelosEmail('TANK_NAPI');

        // debug
        // cimzettlista.Clear;
        // cimzettlista.Add('nagy.peter@js.hu');

        SzamitottIdopont:= GetKutasTartalySzamitottIdopont(1);
        MertIdopont:= GetKozeliMertIdopont(1, SzamitottIdopont);

        Title:= 'Üzemanyag szintek a telepi kútban';
        Uzenet:= Tank2Nev+' <b>mért</b> (szonda) <b>szint: '+ FormatFloat('0', GetKutasTartalyMertLiter(1, MertIdopont))+'</b>';  // a szonda az 1. tartályban van
        Uzenet:= Uzenet +' (lekérdezés: '+MertIdopont+')'+sortores;
        Uzenet:= Uzenet +'Készlet alapján <b>számított szint: ' + FormatFloat('0', GetKutasTartalySzamitottLiter(1))+'</b>'; // most már a számítás is
        Uzenet:= Uzenet +' (lekérdezés: '+SzamitottIdopont+')'+sortores;
        Uzenet:= Uzenet +'<b>'+ Tank3Nev+' szint: '+ FormatFloat('0', GetKutasTartalySzamitottLiter(3))+'</b>';
        Uzenet:= Uzenet +' (lekérdezés: '+SzamitottIdopont+') ';
        SetLength(LevelMellekletLista, 0);
        Res:= EmailDlg.SendLevel(cimzettlista, '', Title, Uzenet, True, 'Fuvaros üzenetközpont',
                      LevelMellekletLista, LevelMellekletNevek, LevelMellekletTipusok, 'multipart/mixed', 'text/html');
        if (Res = '') then begin
          WriteLogFile(kutaslogfn, 'NapiJelentes elküldve. Szöveg: '+Uzenet+' '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
          EgyebDlg.Write_Szotar('154', 'NAPI_KULDVE', EgyebDlg.MaiDatum); // bejegyezzük, hogy a mai jelentést kiküldtük
          end
        else begin
          WriteLogFile(kutaslogfn, 'Sikertelen napijelentes küldés! Szöveg: '+Uzenet+' '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
          end;
    finally
      if cimzettlista <> nil then cimzettlista.Free;
      end;  // try-finally
    end;
 end;


{ embedded graph image version
procedure TForm1.KutasNapiJelentes;
var
  S, Uzenet, Title, ActSor, Res: string;
  cimzettlista: TStringlist;
  Stream1, Stream2: TMemoryStream;
begin
  Application.CreateForm(TKutAllapotDlg, KutAllapotDlg);
  cimzettlista:=TStringlist.Create;
  // WriteLogFile(kutaslogfn, 'NapiJelentes 01 '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
  try
    with KutAllapotDlg do begin
      SetLength(LevelMellekletLista, 2);
      // WriteLogFile(kutaslogfn, 'NapiJelentes 02 '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
      RefreshGraph(2, 'napi', Chart2);
      // WriteLogFile(kutaslogfn, 'NapiJelentes 03 '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
      // Chart2.SaveToMetafileEnh('d:\NagyP\Temp\X\diesel.wmf');  // file megoldás
      Chart2.CopyToClipboardMetafile(True);
      // WriteLogFile(kutaslogfn, 'NapiJelentes 04 '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
      Stream1:= ClipboardMetafileToMemoryStream(kutaslogfn);
      // Stream1.SaveToFile('d:\NagyP\Temp\X\AttachBefore0.wmf');
      // WriteLogFile(kutaslogfn, 'NapiJelentes 05 '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
      LevelMellekletLista[0]:= @Stream1;
      // WriteLogFile(kutaslogfn, 'NapiJelentes 06 '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));

      RefreshGraph(3, 'heti', Chart3);
      // Chart3.SaveToMetafileEnh('d:\NagyP\Temp\X\adblue.wmf');
      Chart3.CopyToClipboardMetafile(True);
      // CldipboardMetafileToMemoryStream(Stream, kutaslogfn);
      Stream2:= ClipboardMetafileToMemoryStream(kutaslogfn);
      // Stream2.SaveToFile('d:\NagyP\Temp\X\AttachBefore1.wmf');
      LevelMellekletLista[1]:= @Stream2;

      // file megoldás
      // LevelMellekletLista.Add('d:\NagyP\Temp\X\diesel.wmf');
      // LevelMellekletLista.Add('d:\NagyP\Temp\X\adblue.wmf');

      // cimzettlista:= GetFelelosEmail('TANK_NAPI');
      cimzettlista.Clear;
      cimzettlista.Add('nagy.peter@js.hu');
      Title:= 'Üzemanyag szintek a telepi kútban';
      Uzenet:= 'Diesel és AdBlue szintek - '+FormatDateTime('yyyy.mm.dd. HH:mm', Now);
      // WriteLogFile(kutaslogfn, 'NapiJelentes 07 '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
      Res:= SendLevel(cimzettlista, '', Title, Uzenet, True, 'Fuvaros üzenetközpont', LevelMellekletLista);
      // WriteLogFile(kutaslogfn, 'NapiJelentes 08 '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
      EgyebDlg.Write_Szotar('154', 'NAPI_KULDVE', EgyebDlg.MaiDatum); // bejegyezzük, hogy milyen szintű figyelmeztetést küldtünk ki
      end;  // with
  finally
    KutAllapotDlg.Release;
    if cimzettlista <> nil then cimzettlista.Free;
    if Stream1 <> nil then Stream1.Free;
    if Stream2 <> nil then Stream2.Free;
    end;  // try-finally
 end;
}

function TForm1.GetKutasTartalySzamitottLiter(Tartaly: integer): double;
var
  S: string;
begin
    S:= 'select KU_LITER from KUTALLAPOT where KU_TARTALY = '+IntToStr(Tartaly)+' and KU_IDOPONT= '+
        ' (select MAX(KU_IDOPONT) from KUTALLAPOT where KU_TARTALY = '+IntToStr(Tartaly)+')';
    Query_run(Query2, S, False);
    Result:= Query2.Fields[0].AsFloat;
end;

function TForm1.GetKutasTartalyMertLiter(const Tartaly: integer; const KutasDat: string): double;
var
  S: string;
begin
    S:= 'select MIN(KM_LITER) from KUTSZINTMERO where KM_TARTALY = '+IntToStr(Tartaly)+' and KM_KUTASDAT= ';
    if KutasDat <> '' then begin
       S:= S+ ''''+KutasDat+'''';
       end
    else begin
       S:= S+ ' (select MAX(KM_KUTASDAT) from KUTSZINTMERO where KM_TARTALY = '+IntToStr(Tartaly)+')';
       end;
    Query_run(Query2, S, False);
    Result:= Query2.Fields[0].AsFloat;
end;

function TForm1.GetKutasTartalySzamitottIdopont(Tartaly: integer): string;
var
  S: string;
begin
    S:= 'select MAX(KU_KUTASDAT) from KUTALLAPOT where KU_TARTALY = '+IntToStr(Tartaly);
    Query_run(Query2, S, False);
    Result:= Query2.Fields[0].AsString;
end;

function TForm1.GetKutasTartalyMertIdopont(Tartaly: integer): string;
var
  S: string;
begin
    S:= 'select MAX(KM_KUTASDAT) from KUTSZINTMERO where KM_TARTALY = '+IntToStr(Tartaly);
    Query_run(Query2, S, False);
    Result:= Query2.Fields[0].AsString;
end;


function TForm1.GetKozeliMertIdopont(const Tartaly: integer; RefIdopont: string): string;
var
  S, Ido1, Ido2: string;
  RefDT, RefDT1, RefDT2: TDateTime;
  FuvarosDatum: TFuvarosDatum;
begin
    RefDT:= TFuvarosDatum.FuvarosDatumToDateTime(RefIdopont);
    RefDT1:= IncMinute(RefDT, -10);
    RefDT2:= IncMinute(RefDT, 180);
    S:= 'select min(KM_KUTASDAT) from KUTSZINTMERO where KM_TARTALY = '+IntToStr(Tartaly)+
        ' and KM_KUTASDAT between '''+TFuvarosDatum.DateTimeToFuvarosDatum(RefDT1)+''' '+
        ' and '''+TFuvarosDatum.DateTimeToFuvarosDatum(RefDT2)+''' ';
    Query_run(Query2, S, False);
    Result:= Query2.Fields[0].AsString;
end;

procedure TForm1.KutasFigyelmeztetes;
var
  S, Uzenet, Title, ActSor, Res: string;
  cimzettlista: TStringlist;


   procedure ProcessTartaly(Tartaly: integer);
     var
        ActLiter, WarningLiter1, WarningLiter2, WarningLiter3: double;
        S, TartalyNev: string;
        WarningKuldve, WarningSzint: integer;
        KellKuldes: boolean;
     begin
        ActLiter:= GetKutasTartalySzamitottLiter(Tartaly);
        WarningLiter1:= StrToFloat(EgyebDlg.Read_SZGrid('154', 'FIGY'+IntToStr(Tartaly)+'_1'));
        WarningLiter2:= StrToFloat(EgyebDlg.Read_SZGrid('154', 'FIGY'+IntToStr(Tartaly)+'_2'));
        WarningLiter3:= StrToFloat(EgyebDlg.Read_SZGrid('154', 'FIGY'+IntToStr(Tartaly)+'_3'));
        WarningKuldve:= Floor(StringSzam(EgyebDlg.Read_SZGrid('154', 'KULD_SZINT'+IntToStr(Tartaly))));
        WarningSzint:= 0;
        if ActLiter <= WarningLiter1 then WarningSzint:= 1;
        if ActLiter <= WarningLiter2 then WarningSzint:= 2;
        if ActLiter <= WarningLiter3 then WarningSzint:= 3;
        if WarningSzint = 0 then begin
          EgyebDlg.Write_Szotar('154', 'KULD_SZINT'+IntToStr(Tartaly), '0');  // megtöltöttük, reset
          end;

        KellKuldes:= (WarningSzint > WarningKuldve);
        if KellKuldes then begin
            TartalyNev:= EgyebDlg.Read_SZGrid('154', 'NEV'+IntToStr(Tartaly));
            Uzenet:= 'A '+TartalyNev+' tartály szintje alacsony! Jelenlegi mennyiség: '+ FormatFloat('0.00', ActLiter);
            WriteLogFile(kutaslogfn, Uzenet+' '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
            cimzettlista:=TStringlist.Create;
            try
              cimzettlista:= GetFelelosEmail('TANK_SZINT');
              Title:='Alacsony '+TartalyNev+' tartályszint!';
              SetLength(LevelMellekletLista, 0);
              Res:= EmailDlg.SendLevel(cimzettlista, '', Title, Uzenet, True, 'Fuvaros üzenetközpont',
                              LevelMellekletLista, LevelMellekletNevek, LevelMellekletTipusok, 'multipart/mixed');
              EgyebDlg.Write_Szotar('154', 'KULD_SZINT'+IntToStr(Tartaly), IntToStr(WarningSzint)); // bejegyezzük, hogy milyen szintű figyelmeztetést küldtünk ki
            finally
              if cimzettlista <> nil then cimzettlista.Free;
              end;  // try-finally
            end; // if
      end;

   procedure IdopontFigyeles(Tartaly: integer);
     var
        S, ActIdopont, D1, D2, T1, T2, TartalyNev, UtolsoKuldesS: string;
        WarningKuldve, WarningLimitOra, VarakozasOra: integer;
        KellKuldes: boolean;
        UtolsoMikor, UtolsoKuldes, LimitDateTime, KuldesVarakozasLimit: TDateTime;
     begin
        ActIdopont:= GetKutasTartalySzamitottIdopont(Tartaly);
        WriteLogFile(kutaslogfn, 'ActIdopont="'+ActIdopont+'", Tartály="'+IntToStr(Tartaly)+'"');
        WarningLimitOra:= StrToIntDef(EgyebDlg.Read_SZGrid('154', 'NINCSTOLTES_LIMIT'), 30);  // órában
        VarakozasOra:= StrToIntDef(EgyebDlg.Read_SZGrid('154', 'NINCSTOLTES_VARNI'), 24);  // órában
        UtolsoKuldesS:= EgyebDlg.Read_SZGrid('154', 'NINCSTOLTES_KULDVE');
        D1:= GetListOp(ActIdopont, 0, ' '); // '2017.05.14. 23:52' --> '2017.05.14.'
        T1:= GetListOp(ActIdopont, 1, ' '); // '2017.05.14. 23:52' --> '23:52'
        // WriteLogFile(kutaslogfn, 'D1="'+D1+'", T1="'+T1+'"');
        UtolsoMikor:= DateTimeStrToDateTime(D1, T1);

        D1:= GetListOp(UtolsoKuldesS, 0, ' '); // '2017.05.14. 23:52' --> '2017.05.14.'
        T1:= GetListOp(UtolsoKuldesS, 1, ' '); // '2017.05.14. 23:52' --> '23:52'
        UtolsoKuldes:= DateTimeStrToDateTime(D1, T1);

        // WriteLogFile(kutaslogfn, 'UtolsoMikor='+FormatDateTime('yyyymmdd HH:mm:ss', UtolsoMikor));
        LimitDateTime:= IncHour(UtolsoMikor, WarningLimitOra);
        WriteLogFile(kutaslogfn, 'LimitDateTime (+'+IntToStr(WarningLimitOra)+' óra): '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', LimitDateTime));
        KuldesVarakozasLimit:= IncHour(UtolsoKuldes, VarakozasOra);
        WriteLogFile(kutaslogfn, 'KuldesVarakozasLimit (+'+IntToStr(VarakozasOra)+' óra): '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', KuldesVarakozasLimit));
        KellKuldes:= (LimitDateTime < now) and (WarningLimitOra > 0)   // legalább "WarningLimitOra" eltelt a frissítés óta
            and (KuldesVarakozasLimit < now); // az utolsó küldés óta eltelt "VarakozasOra" óra
        if KellKuldes then begin
            TartalyNev:= EgyebDlg.Read_SZGrid('154', 'NEV'+IntToStr(Tartaly));
            Uzenet:= 'Hecpoll kút lekérdezés az utóbbi '+IntToStr(WarningLimitOra)+' órában sikertelen volt! Legutóbbi lekérdezés: '+ ActIdopont;
            WriteLogFile(kutaslogfn, Uzenet+' '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
            cimzettlista:=TStringlist.Create;
            try
              cimzettlista:= GetFelelosEmail('TANK_SZINT');
              Title:='Kút lekérdezés - lehetséges fennakadás!';
              SetLength(LevelMellekletLista, 0);
              Res:= EmailDlg.SendLevel(cimzettlista, '', Title, Uzenet, True, 'Fuvaros üzenetközpont',
                              LevelMellekletLista, LevelMellekletNevek, LevelMellekletTipusok, 'multipart/mixed');
              if Res='' then begin
                EgyebDlg.Write_Szotar('154', 'NINCSTOLTES_KULDVE', EgyebDlg.FuvarosMost); // bejegyezzük, hogy a mai jelentést kiküldtük
                end;
            finally
              if cimzettlista <> nil then cimzettlista.Free;
              end;  // try-finally
            end; // if
      end;


begin
  WriteLogFile(kutaslogfn,'Figyelmeztetés feldolgozás kezdete: '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
  ProcessTartaly(1);
  ProcessTartaly(3);
  IdopontFigyeles(1);
  // IdopontFigyeles(3); // elég az egyik, a lekérdezés időpontja ugyanaz
  WriteLogFile(kutaslogfn,'Figyelmeztetés feldolgozás vége: '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
end;

procedure TForm1.MozdulatlanAutok;
var
  S, Uzenet, Title, ActSor, Res: string;
  cimzettlista: TStringlist;
begin
  WriteLogFile(mozdulatlanlogfn,'Feldolgozás kezdete: '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
  S:= 'exec SelectMozdulatlanAutok';
  Query_run(QueryMain, S, False);
  WriteLogFile(mozdulatlanlogfn,'  SQL OK '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
  Title:='24 órája álló autók';
  Uzenet:= Title+':' + const_CRLF;
  Uzenet:= Uzenet+ '--------------------' + const_CRLF;
  while not QueryMain.Eof do begin
    ActSor:= QueryMain.FieldByName('GS_RENDSZAM').AsString+' (';
    ActSor:= ActSor+ QueryMain.FieldByName('GS_GKGEKOD').AsString+')';
    Uzenet:= Uzenet+ActSor + const_CRLF;
    QueryMain.Next;
    end;  // while
  WriteLogFile(mozdulatlanlogfn,'  Szöveg OK '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
  cimzettlista:=TStringlist.Create;
  try
    cimzettlista:= GetFelelosEmail('ALLO_GK');
    WriteLogFile(mozdulatlanlogfn,'  cimzettlista OK '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
    SetLength(LevelMellekletLista, 0);
    Res:= EmailDlg.SendLevel(cimzettlista, '', Title, Uzenet, True, 'Fuvaros üzenetközpont',
                    LevelMellekletLista, LevelMellekletNevek, LevelMellekletTipusok, 'multipart/mixed');
    WriteLogFile(mozdulatlanlogfn,'Feldolgozás vége. '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
  finally
    if cimzettlista <> nil then cimzettlista.Free;
    end;  // try-finally
end;

function TForm1.GetFelelosEmail(Funkcio: string): TStringList;
var
  EmailLista, dokodlista, DOKOD, S: string;
  ForceSMS: boolean;
begin
  Result:= TStringList.Create;
  dokodlista:= EgyebDlg.Read_SZGrid('152', Funkcio);
  ForceSMS:= (EgyebDlg.Read_SzotarTechnikaiErtek('152', Funkcio) = 1);
  // EmailLista:= '';
  while dokodlista <> '' do begin
    DOKOD:= Trim(EgyebDlg.SepFrontToken(',', dokodlista));
    dokodlista:= Trim(EgyebDlg.SepRest(',', dokodlista));
    S:= trim(EgyebDlg.GetDolgozoSMSEagleCimzett(DOKOD, ForceSMS));
    S:= GetKopaszEmail(S);  // leveszi a nevet és a < > "burkolatot"
    Result.Add(S);
    // EmailLista:= Felsorolashoz(EmailLista, S, ',', False);
    end; // while
  // Result:= EmailLista;
end;

procedure TForm1.KULCSKiegyenlitesImport;
var
  UpdatedCount: integer;
begin
  WriteLogFile(kulcslogfn,'KULCS számla kiegyenlítés import kezdete: '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
  Application.CreateForm(TKulcsAnalitikaOlvasoDlg, KulcsAnalitikaOlvasoDlg);
  UpdatedCount:= KulcsAnalitikaOlvasoDlg.Feldolgozas_DirectDB(kulcslogfn);
  WriteLogFile(kulcslogfn,'frissítve: '+IntToStr(UpdatedCount)+' számla.');
  KulcsAnalitikaOlvasoDlg.Free;
  WriteLogFile(kulcslogfn,'KULCS számla kiegyenlítés import vége. '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
end;


procedure TForm1.Button1Click(Sender: TObject);
var
  ido, fn, sor,sor0,sor1,sor2,sor20,sor21,sor3,smido,ssido,sor01,sor02, sor4: string;
  ftido,ltido, aido, mido,sido : TdateTime;
  nemteljesult,NTFel,NTLe, kiemelt, ujrakuldott : boolean;
  gkkat, fkod,fnev,femail,aemail,hemail,snev1,snev2,semail1,semail2,stel1,stel2, emailcim, skiemelt, targya, rendsz, alnev, masolat : string;
  IdMessage1: TIdMessage;
  p_kiemelt, p_sofor, p_ujra, p_ujra_norm, p_ujra_kiem, p_keses, gkpoz_elavul: integer;
  l_kiemelt, l_mir, l_sof, alvallalkozo, hutos, felfuggesztve, sofornekcsak1, vanerkezesido : boolean;
  l_k_ido, l_m_ido, l_s_ido : TDateTime;
  torzs_k, torzs_ks, torzs_m, torzs_s, torzs_a , torzs_: TStringList;
  sftido, sltido : string;
  cc_kiemelt,cc_mir,cc_sof,cc_alvallalkozo, cc_hutos, alv_telefon, poz_dat,poz_hely,link,poz_sx,poz_sy,poz_seb, cim : string;
  kiem_k, kiem_v, dolt_k,dolt_v : string;
  s_mbko,s_cim,s_fel_le,s_fel,s_le,s_sofor,s_gkpoz,s_link,s_lmegj,s_alv,s_megj1,s_megj2,s_init,s_keses : string;
begin
{
  if INI.ReadString('NEMTELJESULT','OK','I')<>'I' then
    exit;

	p_kiemelt:=StrToIntDef(Szotar_('376','001','M'),0);
	p_sofor:=StrToIntDef( Szotar_('376','002','M'),0);
	p_ujra_norm:=StrToIntDef( Szotar_('376','003','M'),0);
	p_ujra_kiem:=StrToIntDef( Szotar_('376','005','M'),0);
	gkpoz_elavul:=StrToIntDef( Szotar_('376','004','M'),120);
	aemail:=Szotar_('376','010','M');
	hemail:=Szotar_('376','012','M');
  cc_kiemelt:=Szotar_('376','020','M');
  cc_mir:=Szotar_('376','021','M');
  cc_sof:=Szotar_('376','022','M');
  cc_alvallalkozo:=Szotar_('376','023','M');
  cc_hutos:=Szotar_('376','024','M');
  sofornekcsak1:=Szotar_('376','009','M')<>'';

  kiem_k:='<strong>';
  kiem_v:='</strong>';
  dolt_k:='<em>';
  dolt_v:='</em>';
  s_init:='<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-2">' ;

  sor01:='Ez egy automatikus levél. Ne válaszolj rá!';
  sor02:='------------------------------------------';

  //p_kiemelt:=30;
  //p_sofor:=10;
  //p_ujra:=60;
  // Timer1.Enabled:=False;
  torzs_k :=TStringList.Create;
  torzs_ks:=TStringList.Create;
  torzs_m :=TStringList.Create;
  torzs_s :=TStringList.Create;
  torzs_a :=TStringList.Create;
  torzs_  :=TStringList.Create;

  fn:= INI.ReadString('NEMTELJESULT','FILE','AKI_MB.log');
  logfile		:= ExtractFilePath( Application.ExeName)+fn ;
  WriteLogFile(logfile,'START '+DateTimeToStr(now)+'  '+ADOConnectionAlap.DefaultDatabase);

  T_MEGSEGED.Close;
  T_MEGSEGED.Parameters[0].Value:=MaiDatum;
  T_MEGSEGED.Open;
  T_MEGSEGED.First;
  while not T_MEGSEGED.Eof do
  begin
   ///////////////////////////////////////////
   torzs_k.Clear;
   torzs_ks.Clear;
   torzs_m.Clear;
   torzs_s.Clear;
   torzs_a.Clear;
   torzs_.Clear;
   skiemelt:='';
   nemteljesult:=False;
   kiemelt:=False;
   NTFel:=False;
   NTLe:=False;
   l_kiemelt:=False;
   l_mir:=False;
   l_sof:=False;
   s_lmegj:='';

   mido:=T_MEGSEGEDMS_M_MAIL.Value;
   sido:=T_MEGSEGEDMS_S_MAIL.Value;
   rendsz:= T_MEGSEGEDMS_RENSZ.Value ;
   alvallalkozo:= T_MEGSEGEDALNEV.Value<>'';
   hutos:= T_MEGSEGEDHUTOS.Value=1;
   felfuggesztve:=T_MEGSEGEDFELFUG.Value=1;
   alnev:=T_MEGSEGEDALNEV.Value;
   if (not alvallalkozo)and(rendsz<>'') then
   begin
    Poziciok.Close;
    Poziciok.Parameters[0].Value:=rendsz;
    Poziciok.Open;
    Poziciok.First;
    poz_dat:=PoziciokPO_DAT.AsString;
    poz_hely:=PoziciokPO_GEKOD.Value;
    poz_sx:=PoziciokPO_SZELE.AsString;
    poz_sy:=PoziciokPO_HOSZA.AsString;
    poz_sx:=StringReplace(poz_sx,',','.',[rfReplaceAll]);
    poz_sy:=StringReplace(poz_sy,',','.',[rfReplaceAll]);
    poz_seb:='  '+PoziciokPO_SEBES.AsString+' km/h';
    Poziciok.Close;
   end;
   if alvallalkozo then
   begin
     Partner.Close;
     Partner.Parameters[0].Value:=T_MEGSEGEDALVKOD.Value;
     Partner.Open;
     Partner.First;
     alv_telefon:=PartnerV_TEL.Value;
     Partner.Close;
   end;
   ////////////////////////////
   // FELRAKÓ
   if  (T_MEGSEGEDMS_FETIDO.AsString='')and(T_MEGSEGEDMS_FERKIDO.AsString='')and(T_MEGSEGEDMS_TIPUS.AsString='0') then     // nincs kitöltve a tény idő
   //if  (T_MEGSEGEDMS_FETIDO.AsString='')and(T_MEGSEGEDMS_TIPUS.AsString='0') then     // nincs kitöltve a tény idő
   begin
    vanerkezesido:= T_MEGSEGEDMS_FERKIDO.AsString<>'';
    //if  (T_MEGSEGEDMS_FERKIDO.AsString='') then     // nincs kitöltve az érkezési idő
    //begin
    ido:= T_MEGSEGEDMS_FELIDO.AsString;
    if T_MEGSEGEDMS_FELIDO2.AsString<>'' then   // -ig ki van töltve
      if  T_MEGSEGEDMS_FELIDO2.AsString < T_MEGSEGEDMS_FELIDO.AsString   then   // 20:00 - 06:00
        ido:= '23:59'
      else
        ido:= T_MEGSEGEDMS_FELIDO2.AsString;
    ftido:= StrToDateTimeDef(T_MEGSEGEDMS_FELDAT.AsString+' '+ido+':00',now);

    aido:= Now;
    //aido:=StrToDateTime( maidatum);
    if (not vanerkezesido)and(p_kiemelt>0)and(mido=0)and((T_MEGSEGEDMS_KIFON.Value=1)or(T_MEGSEGEDMS_FDARU.Value=1)) then  // kiemelt fontosságú, v. daru szükséges  és a menetirányító még nem kapott levelet
    begin
      kiemelt:=True;
      l_kiemelt:=True;
      if T_MEGSEGEDMS_KIFON.Value=1 then
        skiemelt:=' KIEMELT FONTOSSÁGÚ';
      if T_MEGSEGEDMS_FDARU.Value=1 then
        skiemelt:=skiemelt+' DARUS';
      //aido:=DateTimeMinusz(aido,p_kiemelt);
      ftido:=DateTimeMinusz(ftido,p_kiemelt);
    end;
    NTFel:=(aido > ftido) ;
    nemteljesult:= nemteljesult OR (aido > ftido) ;
    if kiemelt then
      ftido:=DateTimeMinusz(ftido,-p_kiemelt);
    sftido:=DateTimeToStr(ftido);
    sftido:=copy(sftido,1,length(sftido)-3);

    ido:= T_MEGSEGEDMS_LERIDO.AsString;
    if T_MEGSEGEDMS_LERIDO2.AsString<>'' then   // -ig ki van töltve
        ido:= T_MEGSEGEDMS_LERIDO2.AsString;
    sltido:= T_MEGSEGEDMS_LERDAT.AsString+' '+ido;
    //end;
   end;

   // ELRAKÓ
   /////////////////////////////////////////
   if  (T_MEGSEGEDMS_LETIDO.AsString='')and(T_MEGSEGEDMS_LERKIDO.AsString='')and(T_MEGSEGEDMS_TIPUS.AsString='1') then     // nincs kitöltve a tény idő
   //if  (T_MEGSEGEDMS_LETIDO.AsString='')and(T_MEGSEGEDMS_TIPUS.AsString='1') then     // nincs kitöltve a tény idő
   begin
    vanerkezesido:= T_MEGSEGEDMS_LERKIDO.AsString<>'';
    //if  (T_MEGSEGEDMS_LERKIDO.AsString='') then     // nincs kitöltve az érkezési idő
    //begin
    ido:= T_MEGSEGEDMS_LERIDO.AsString;
    if T_MEGSEGEDMS_LERIDO2.AsString<>'' then   // -ig ki van töltve
      if  T_MEGSEGEDMS_LERIDO2.AsString < T_MEGSEGEDMS_LERIDO.AsString   then   // 20:00 - 06:00
        ido:= '23:59'
      else
        ido:= T_MEGSEGEDMS_LERIDO2.AsString;
    ltido:= StrToDateTimeDef(T_MEGSEGEDMS_LERDAT.AsString+' '+ido+':00',now);
    aido:= Now;
    //aido:=StrToDateTime( maidatum);
    if (not vanerkezesido)and(p_kiemelt>0)and(mido=0)and((T_MEGSEGEDMS_KIFON.Value=1)or(T_MEGSEGEDMS_LDARU.Value=1)) then  // kiemelt fontosságú, v. daru szükséges
    begin
      kiemelt:=True;            // kiemelt megbízás
      l_kiemelt:=True;          // kiemelt levelet kell küldeni
      if T_MEGSEGEDMS_KIFON.Value=1 then
        skiemelt:=' KIEMELT FONTOSSÁGÚ';
      if T_MEGSEGEDMS_LDARU.Value=1 then
        skiemelt:=skiemelt+' DARUS';
      //aido:=DateTimeMinusz(aido,p_kiemelt);
      ltido:=DateTimeMinusz(ltido,p_kiemelt);
    end;
    NTLe:=(aido > ltido) ;
    nemteljesult:= nemteljesult OR (aido > ltido) ;
    if kiemelt then
      ltido:=DateTimeMinusz(ltido,-p_kiemelt);

    sltido:=DateTimeToStr(ltido);

    sltido:=copy(sltido,1,length(sltido)-3);

    ido:= T_MEGSEGEDMS_FELIDO.AsString;
    if T_MEGSEGEDMS_FELIDO2.AsString<>'' then   // -ig ki van töltve
        ido:= T_MEGSEGEDMS_FELIDO2.AsString;
    sftido:= T_MEGSEGEDMS_FELDAT.AsString+' '+ido;
    //end;
   end;

   //////////////////////////////////////
   if nemteljesult then
   begin
     l_mir:=not l_kiemelt;
     l_sof:=False ;  // not l_kiemelt;
     ujrakuldott:=False;
     felfuggesztve:=False;
     if not l_kiemelt then
     begin
       l_m_ido:=now;
       if mido<>0 then       // már volt küldés a menetirányítónak
       begin
        l_m_ido:=mido;
        //if (not felfuggesztve) and (mido < DateTimeMinusz(now,p_ujra_norm)) then  // letelt az újraküldési idő; megint el kell küldeni
        if kiemelt then
          p_ujra:=p_ujra_kiem
        else
          p_ujra:=p_ujra_norm;
        if (not felfuggesztve) and (mido < DateTimeMinusz(now,p_ujra)) then  // letelt az újraküldési idő; megint el kell küldeni
        begin
          l_mir:=True;     // menetiráyítónak kell küldeni    újra
          l_m_ido:=now;
          ujrakuldott:=True;
        end
        else
        begin
          l_mir:=False;
        end;
       end;

       if sido<>0 then       // már volt küldés a sofőrnek
       begin
        l_s_ido:=sido;
        if kiemelt then
          p_ujra:=p_ujra_kiem
        else
          p_ujra:=p_ujra_norm;
        if (not sofornekcsak1)and(not felfuggesztve)and(p_sofor>0)and((semail1+semail2)<>'')and(sido < DateTimeMinusz(now,p_ujra)) then  // letelt az újraküldési idő; megint el kell küldeni
        begin
          l_sof:=True;          // sofőrnek kell küldeni  újra
          l_s_ido:=now;
        end
        else
        begin
          l_sof:=False;
        end;
       end
       else // még nem küldtünk a sofőrnek
       begin
        if (not felfuggesztve)and(p_sofor>0)and((semail1+semail2)<>'')and(l_m_ido < DateTimeMinusz(now,p_sofor)) then  // már a sofőrnek is el kell küldeni
        //if l_m_ido < DateTimeMinusz(l_m_ido,p_sofor) then  // már a sofőrnek is el kell küldeni
        begin
           l_sof:=True;    // sofőrnek kell küldeni
        end;
       end;
     end;
     snev1:=T_MEGSEGEDMS_SNEV1.Value;
     snev2:=T_MEGSEGEDMS_SNEV2.Value;
     gkkat:=T_MEGSEGEDMS_GKKAT.Value;
    // if rendsz='' then gkkat:='';

     fnev:=Szotar_('340',gkkat,'L');  // Szotarsz_leiro.Value;
     /////////////////
     Dolgozo.Close;
     Dolgozo.Parameters[0].Value:= fnev;
     Dolgozo.Open;
     Dolgozo.First;
     fkod:=Dolgozodo_kod.Value;
     femail:=Dolgozodo_email.Value;
     ////////////////
     Dolgozo.Close;
     Dolgozo.Parameters[0].Value:= snev1;
     Dolgozo.Open;
     Dolgozo.First;
     semail1:=Dolgozodo_email.Value;
     stel1:=Dolgozodo_telef.Value;
     ////////////////
     Dolgozo.Close;
     Dolgozo.Parameters[0].Value:= snev2;
     Dolgozo.Open;
     Dolgozo.First;
     semail2:=Dolgozodo_email.Value;
     stel2:=Dolgozodo_telef.Value;
     ////////////////
     Dolgozo.Close;
     if NTFel then
     begin
        s_mbko:=T_MEGSEGEDMS_MBKOD.AsString+'/'+T_MEGSEGEDMS_SORSZ.AsString ;
        s_cim:=skiemelt+kiem_k+ '  FELRAKÁS  '+kiem_v+'('+rendsz+') ('+gkkat+')'+IfThen(hutos,' (HŰTŐS)','');
        s_fel := 'Felrakó: ('+sftido+') '+T_MEGSEGEDMS_TEFNEV.Value+', '+T_MEGSEGEDMS_TEFOR.Value+' '+T_MEGSEGEDMS_TEFIR.Value +' '+T_MEGSEGEDMS_TEFTEL.Value+' '+T_MEGSEGEDMS_TEFCIM.Value ;
        s_le:= 'Lerakó : ('+sltido+') '+T_MEGSEGEDMS_TENNEV.Value+', '+T_MEGSEGEDMS_TENOR.Value+' '+T_MEGSEGEDMS_TENIR.Value +' '+T_MEGSEGEDMS_TENTEL.Value+' '+T_MEGSEGEDMS_TENCIM.Value ;
        sor21:='Idő: '+sftido ;
        sor:=s_mbko+s_fel;
        //p_keses:= MinutesBetween(StrToDateTime(MaiDatum),StrToDateTime(sftido));
        p_keses:= MinutesBetween(now,StrToDateTime(sftido));
        //s_keses:=' (Késedelem: '+ IntToStr( p_keses div 60 )+':'+IntToStr(p_keses mod 60)+')';
        s_keses:=' (Késedelem: '+Feltolt(IntToStr( p_keses div 60 ),'0',2)+':'+Feltolt(IntToStr(p_keses mod 60),'0',2)+IfThen(ujrakuldott,' !)',')');
        s_sofor:= 'Sofőr: '+snev1+' ('+stel1+')('+semail1+') ' ;
        s_gkpoz:='Gépkocsi pozíció: ('+copy(poz_dat,1,length(poz_dat)-3)+poz_seb+') '+poz_hely;

        s_lmegj:='';
        //if MinutesBetween(StrToDateTime(MaiDatum),StrToDateTime(poz_dat))> gkpoz_elavul then
        if (poz_dat<>'')and(MinutesBetween(now,StrToDateTime(poz_dat))> gkpoz_elavul) then
          s_lmegj:=' !!! Régi gépkocsi pozíció !!!';
        cim :=T_MEGSEGEDMS_TEFOR.Value+' '+T_MEGSEGEDMS_TEFIR.Value +' '+T_MEGSEGEDMS_TEFTEL.Value+' '+T_MEGSEGEDMS_TEFCIM.Value ;
        // s_link:='http://maps.google.com/maps?saddr='+poz_sx+','+poz_sy+'+to:'+cim+'&via=1&t=m';
        s_link:='http://maps.google.com/maps?saddr='+poz_sx+','+poz_sy+'&daddr='+cim+'&t=m';
        s_link:= '<a href="'+s_link+'" title="Google térkép">  (Térkép)</a><br>';
        if (p_sofor>0)and((semail1+semail2)<>'') then
          s_megj1:=dolt_k+ 'Amennyiben nem történik változás, a sofőr '+IntToStr(p_sofor)+' perc múlva értesítést kap.'+dolt_v
        else
          s_megj1:='';

        s_alv:='Alvállalkozó: '+alnev;
        if snev2<>'' then  s_sofor:=sor3+snev2+' ('+stel2+')('+semail2+')' ;
        if (Trim(snev1)+Trim(snev2))='' then
          s_sofor:='NINCS sofőr megadva!';

        WriteLogFile(logfile,sor);
     end;
     if NTLe then
     begin
        s_mbko:=T_MEGSEGEDMS_MBKOD.AsString+'/'+T_MEGSEGEDMS_SORSZ.AsString ;
        //s_cim:=skiemelt+ ' LERAKÁS '+rendsz+' ('+gkkat+')';
       // s_cim:=skiemelt+ ' LERAKÁS ('+rendsz+') ('+gkkat+')';
        s_cim:=skiemelt+kiem_k+ '  LERAKÁS  '+kiem_v+'('+rendsz+') ('+gkkat+')'+IfThen(hutos,' (HŰTŐS)','');
        s_fel := 'Felrakó: ('+sftido+') '+T_MEGSEGEDMS_TEFNEV.Value+', '+T_MEGSEGEDMS_TEFOR.Value+' '+T_MEGSEGEDMS_TEFIR.Value +' '+T_MEGSEGEDMS_TEFTEL.Value+' '+T_MEGSEGEDMS_TEFCIM.Value ;
        s_le:= 'Lerakó : ('+sltido+') '+T_MEGSEGEDMS_TENNEV.Value+', '+T_MEGSEGEDMS_TENOR.Value+' '+T_MEGSEGEDMS_TENIR.Value +' '+T_MEGSEGEDMS_TENTEL.Value+' '+T_MEGSEGEDMS_TENCIM.Value ;
        sor21:='Idő: '+sftido ;
        sor:=s_mbko+s_le;
        //p_keses:= MinutesBetween(StrToDateTime(MaiDatum),StrToDateTime(sltido));
        p_keses:= MinutesBetween(now,StrToDateTime(sltido));
        //s_keses:=' (Késedelem: '+ IntToStr( p_keses div 60 )+':'+IntToStr(p_keses mod 60)+IfThen(ujrakuldott,' !)',')');
        s_keses:=' (Késedelem: '+Feltolt(IntToStr( p_keses div 60 ),'0',2)+':'+Feltolt(IntToStr(p_keses mod 60),'0',2)+IfThen(ujrakuldott,' !)',')');
        s_sofor:= 'Sofőr: '+snev1+' ('+stel1+')('+semail1+') ' ;
        //s_gkpoz:='Gépkocsi pozíció: ('+poz_dat+') '+poz_hely;
        s_gkpoz:='Gépkocsi pozíció: ('+copy(poz_dat,1,length(poz_dat)-3)+poz_seb+') '+poz_hely;
        s_lmegj:='';
        if (poz_dat<>'')and(MinutesBetween(now,StrToDateTime(poz_dat))> gkpoz_elavul) then
        //if MinutesBetween(StrToDateTime(MaiDatum),StrToDateTime(poz_dat))> gkpoz_elavul then
          s_lmegj:=' !!! Régi gépkocsi pozíció !!!';
        cim:= T_MEGSEGEDMS_TENOR.Value+' '+T_MEGSEGEDMS_TENIR.Value +' '+T_MEGSEGEDMS_TENTEL.Value+' '+T_MEGSEGEDMS_TENCIM.Value ;
        // s_link:='http://maps.google.com/maps?saddr='+poz_sx+','+poz_sy+'+to:'+cim+'&via=1&t=m';
        s_link:='http://maps.google.com/maps?saddr='+poz_sx+','+poz_sy+'&daddr='+cim+'&t=m';
        s_link:= '<a href="'+s_link+'" title="Google térkép">  (Térkép)</a><br>';
        //s_megj1:='Amennyiben nem történik változás, a sofőr '+IntToStr(p_sofor)+' perc múlva értesítést kap.';
        if (p_sofor>0)and((semail1+semail2)<>'') then
          s_megj1:=dolt_k+ 'Amennyiben nem történik változás, a sofőr '+IntToStr(p_sofor)+' perc múlva értesítést kap.'+dolt_v
        else
          s_megj1:='';

        s_alv:='Alvállalkozó: '+alnev;
        if snev2<>'' then  s_sofor:=sor3+snev2+' ('+stel2+')('+semail2+')' ;
        if (Trim(snev1)+Trim(snev2))='' then
          s_sofor:='NINCS sofőr megadva!';

        WriteLogFile(logfile,sor);
     end;
     ///////////////////////////
        /////////// kiemelt
        torzs_k.Add(s_init);
        torzs_k.Add(s_mbko+s_cim);
        torzs_k.Add(sortores);
        torzs_k.Add(sortores);
        torzs_k.Add(s_fel);
        torzs_k.Add(sortores);
        torzs_k.Add(s_le);
        torzs_k.Add(sortores);
        torzs_k.Add(sortores);
        torzs_k.Add(s_sofor);
        torzs_k.Add(sortores);
        torzs_k.Add(sortores);
        if (rendsz<>'')and(poz_hely<>'') then
        begin
          torzs_k.Add(s_gkpoz) ;
          //torzs_k.Add(sortores);
          torzs_k.Add(s_link+s_lmegj) ;
        end;
        // SMS
        torzs_ks.Add('Késedelmes'+StringReplace(StringReplace(s_cim,kiem_k,'',[]),kiem_v,'',[]));
        if NTFel then
          torzs_ks.Add(s_fel)
        else
          torzs_ks.Add(s_le);
        torzs_ks.Add(s_sofor);

        ///////////////////  késés
        torzs_m.Add(s_init);
        torzs_m.Add(s_mbko+s_cim+s_keses);
        torzs_m.Add(sortores);
        torzs_m.Add(sortores);
        torzs_m.Add(s_fel);
        torzs_m.Add(sortores);
        torzs_m.Add(s_le);
        torzs_m.Add(sortores);
        torzs_m.Add(sortores);
        torzs_m.Add(s_sofor);
        torzs_m.Add(sortores);
        torzs_m.Add(sortores);
        if (rendsz<>'')and(poz_hely<>'') then
        begin
          torzs_m.Add(s_gkpoz) ;
          //torzs_m.Add(sortores);
          torzs_m.Add(s_link+s_lmegj) ;
        end;
        if copy(s_sofor,1,5)<>'NINCS' then
        begin
          torzs_m.Add(sortores);
          torzs_m.Add(s_megj1);
        end;

        /////////////////// alválallkozó
        torzs_a.Add(s_init);
        torzs_a.Add(s_mbko+s_cim+s_keses);
        torzs_a.Add(sortores);
        torzs_a.Add(sortores);
        torzs_a.Add(s_fel);
        torzs_a.Add(sortores);
        torzs_a.Add(s_le);
        torzs_a.Add(sortores);
        torzs_a.Add(sortores);
        torzs_a.Add(s_alv);

        ///////////////// sofőr
        torzs_s.Add('Késedelmes'+StringReplace(StringReplace(s_cim,kiem_k,'',[]),kiem_v,'',[]));
        if NTFel then
          torzs_s.Add(s_fel)
        else
          torzs_s.Add(s_le);

     ///////////////////////////
     IdMessage1:=TIdMessage.Create(Self);
     IdMessage1.ContentType:='text/html';
     // --- 20151124 NagyP -------
     IdMessage1.CharSet:= 'UTF-8';
     // --------------------------
     masolat:='';
     semail1:='';
     semail2:='';

     ///////////////////
     // LEVÉL KÜLDÉSE //
     ///////////////////
     emailcim:=femail;
     masolat:=cc_mir;
     targya:='';
     if l_kiemelt then  ////////// Kiemelt, darus
     begin
        targya:='Kiemelt fontosságú, vagy darus megbízás. '+DateTimeToStr(now);
        torzs_:=torzs_k;
     end;
     if l_mir then       // normál késés
     begin
        targya:='Megbízás késedelem. '+DateTimeToStr(now);
        torzs_:=torzs_m;
     end;
     if alvallalkozo then
     begin
        torzs_:=torzs_a;
        emailcim:=aemail;
        masolat:=cc_alvallalkozo;
     end;
     if hutos then
     begin
        emailcim:=hemail;
        masolat:=cc_hutos;
     end;

     if targya<>'' then
     begin
       Mail(IdMessage1,emailcim,masolat,targya,torzs_,True,'Megbízás ellenőrző');
       if SendMail(IdMessage1) then          // ha elküldte, akkor bejegyezni az időt
       begin
          WriteLogFile(logfile,'  '+IdMessage1.Recipients.EMailAddresses+';'+IdMessage1.CCList.EMailAddresses);
          T_MEGSEGED.Edit;
          T_MEGSEGEDMS_M_MAIL.Value:=now;
          T_MEGSEGED.Post;
       end;
     end;
     /////////////////////////////////// Sofőrnek
     if l_sof then
     begin
      //targya:='Késedelem';
      targya:='';
      emailcim:=semail1+';'+semail2;
      masolat:=cc_sof;
      Mail(IdMessage1,emailcim,masolat,targya,torzs_s,False,'Megbízás ellenőrző');
      if SendMail(IdMessage1) then          // ha elküldte, akkor bejegyezni az időt
      begin
        WriteLogFile(logfile,'  '+IdMessage1.Recipients.EMailAddresses+';'+IdMessage1.CCList.EMailAddresses);
        T_MEGSEGED.Edit;
        T_MEGSEGEDMS_S_MAIL.Value:=now;
        T_MEGSEGED.Post;
      end;
     end;
     ///////////////////////////
   end;
   T_MEGSEGED.Next;
  end;
  T_MEGSEGED.Close;
  WriteLogFile(logfile,'END   '+DateTimeToStr(now));
  WriteLogFile(logfile,' ');
  sleep(2000);
     }
end;

function TForm1.Szotar_olvaso(fokod, alkod: string): string;
begin
  Szotar.Close;
  Szotar.Parameters[0].Value:=fokod;
  Szotar.Parameters[1].Value:=alkod;
  Szotar.Open;
  Szotar.First;
	Result:=Szotarsz_menev.Value;
  Szotar.Close;
end;

function TForm1.DatumHozNap(dat : string; napok : integer; kellhetvege : boolean) : string;
{**********************************************************************************}
var
  ev, ho, nap : integer;
  tmpDate  : TDateTime;
begin
  DatumHozNap := '';
  if ( ( dat = '' ) or (Length(dat) < 10 ) ) then begin
     Exit;
  end;
  ev    := StrToIntDef(copy(dat,1,4),0);
  ho    := StrToIntDef(copy(dat,6,2),0);
  nap   := StrToIntDef(copy(dat,9,2),0);
  if ( ( ev = 0 ) or ( ho = 0 ) or ( nap = 0 ) ) then begin
     Exit;
  end;
  tmpDate := EncodeDate(ev, ho, nap) + napok;
  if not kellhetvege then begin
     if DayOfWeek(tmpDate) = 1 then begin      // vasárnap
        tmpDate := tmpDate + 1;
     end else begin
        if DayOfWeek(tmpDate) = 7 then begin   // szombat
           tmpDate := tmpDate + 2;
        end;
     end;
  end;
  DatumHozNap := FormatDateTime('yyyy.mm.dd.',tmpDate);
end;

procedure TForm1.StringParse(forras : string; elvalaszto : string; var mezok : TStringLIst);
var
	tempstr	: string;
begin
	mezok.Clear;
   tempstr	:= forras;
   while Pos(elvalaszto, tempstr) > 0 do begin
   	mezok.Add(copy(tempstr, 1, Pos(elvalaszto, tempstr) - 1));
       tempstr	:= copy(tempstr, Pos(elvalaszto, tempstr) + Length(elvalaszto) , 999);
   end;
	if tempstr <> '' then begin
		mezok.Add(tempstr);
	end;
end;

function TForm1.Munkaido(D: TDateTime): boolean;
var
  Hour, Min, Sec, MSec : Word;
  S, DateS: string;
begin
  DecodeTime(D, Hour, Min, Sec, MSec);
  DateS:= FormatDateTime('yyyymmdd', D);
  // debug WriteLogFile(logfile, 'Időpont óra = '+IntToStr(Hour)+ ', MunkaIdoKezdete='+IntToStr(MunkaIdoKezdete)+ ', MunkaIdoVege='+IntToStr(MunkaIdoVege));
  if (Hour < MunkaIdoKezdete) or (Hour > MunkaIdoVege) then
    Result:= False  // nem kell a naptárt nézni
  else begin // az időpont szerint lehet munkaidő
    // ünnepnap, ha szerepel NA_UNNEP=1-el a NAPOK-ban, vagy ha szombat-vasárnap és nem szerepel NA_UNNEP=0-val a NAPOK-ban (ez utóbbi a dolgozós szombat).
    // egyszerűbben: ha szerepel a NAPOK-ban, akkor az ottani érték, egyébként szombat-vasárnap alapon.
    with qUniArfolyam do begin
      Close; SQL.Clear;
      S:='select NA_UNNEP from napok where convert(varchar, NA_DATUM, 112) = '''+DateS+'''';
      SQL.Add(S);
      Open;
      if not eof then begin
        // debug WriteLogFile(logfile, 'Not eof, érték: '+IntToStr(Fields[0].AsInteger));
        Result:= (Fields[0].AsInteger = 0);
        end
      else begin
        // debug WriteLogFile(logfile, 'Eof volt, DayOfWeek: '+IntToStr(DayOfWeek(D)));
        Result:= not ((DayOfWeek(D) = 7) or (DayOfWeek(D) = 1));  // nem szombat vagy vasárnap
        end;
      Close;
      end;  // with
    end; // else
end;

procedure TForm1.PozicioKuldesFigyelo;
var
  S, UpdateSQLWhere, UpdateSQL, Res, UzenetSzoveg, MBLista, MBKOD, KivetelLista: string;
  megbizaslista: TStringlist;
begin

  WriteLogFile(pozikuldeslogfn,'Feldolgozás kezdete: '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
  S:= 'exec SelectPozicioKuldendo';
  Query_run(QueryMain, S, False);  // recordset: PO_ID, PO_MBKOD, PO_TRIGGER_MSID, PO_TRIGGER_ERK_IND, PO_TRIGGER_IDOPONT
  WriteLogFile(pozikuldeslogfn,'  SQL OK '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
  UpdateSQLWhere:= '';
  MBLista:= '';
  megbizaslista:=TStringlist.Create;
  megbizaslista.Sorted:=True;
  megbizaslista.Duplicates:=dupIgnore;
  KivetelLista:= Egyebdlg.Read_SZGrid('157', 'POZKULD_KIVETEL'); // pl. módosított struktúrájú megbízások, ezekről ne küldjünk butaságot
  try
    while not QueryMain.Eof do begin
      MBKOD:= QueryMain.FieldByName('PO_MBKOD').AsString;
      if not ListabanBenne(MBKOD, KivetelLista, ',') then begin  // csak ha nincs benne
        megbizaslista.add(MBKOD);
        UpdateSQLWhere:= UpdateSQLWhere + ' or (PO_ID='+QueryMain.FieldByName('PO_ID').AsString+')';
        MBLista:= Felsorolashoz(MBLista, QueryMain.FieldByName('PO_MBKOD').AsString, ', ', False);
        end;
      QueryMain.Next;
      end;  // while
    if megbizaslista.Count>=1 then begin
      WriteLogFile(pozikuldeslogfn,'Küldendő megbízások: '+MBLista+' '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
      Res:= EgyebDlg.MerciPozicioJelentesKuld (megbizaslista, '', UzenetSzoveg);  // címzettek az adatszótárból
      if Res = '' then begin  // sikeres volt a küldés
        if length(UzenetSzoveg)> 2000 then
            UzenetSzoveg:= copy(UzenetSzoveg,1,1990)+'...';
        UpdateSQL:= 'update POZKULD set PO_KULDVE_TIMESTAMP= CONVERT(varchar(11), getdate(),102)+''. ''+ '+
          ' convert(varchar(5),getdate(),108), PO_KULDVE_SZOVEG='''+UzenetSzoveg+''' where (1=2) '+ UpdateSQLWhere;
        Query_Run (qUNI, UpdateSQL);
        WriteLogFile(pozikuldeslogfn,'Sikeres küldés. '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
        end  // if
      else begin
        WriteLogFile(pozikuldeslogfn,'SIKERTELEN küldés! Res='+Res+' '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
        end;  // else
      end;
    WriteLogFile(pozikuldeslogfn,'Feldolgozás vége. '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
  finally
    if megbizaslista <> nil then megbizaslista.Free;
    end;  // try-finally

end;

procedure TForm1.PozicioKuldesFeltolt;
const
  MercedesBordero = '8530';
  MennyivelKorabban = 4;  // hány órával az első felrakó előtt kerül be a jelentésbe
var
  S, MBKOD, Felrak, LimitString: string;
  LimitTimeStamp: TDateTime;
  Siker: boolean;

  procedure IdozitettSorBeszur(MB, IDOPONT, LIMIT_IDO: string);
    var
      TeljesIdo, SS: string;
    begin
      TeljesIdo:= FormatDateTime('yyyy.mm.dd. ', Now) + IDOPONT;
      if TeljesIdo >= LIMIT_IDO then begin  // nincs már messze az indulás
        SS:= 'if not exists(select PO_ID from POZKULD where PO_MBKOD='+MB+' and PO_TRIGGER_IDOPONT='''+ TeljesIdo+'''' +') '+
          '	insert into POZKULD (PO_MBKOD, PO_TRIGGER_IDOPONT) values ('+MB+', '''+ TeljesIdo+''')';
        Query_Run(qUNI, SS);
        end;
    end;

  procedure TriggerSorBeszur(MB: string; MS_SORSZ, MS_TIPUS, ERK_IND: integer);
    var
      MSID, SS: string;
    begin
      SS:= 'select MS_ID from MEGSEGED where MS_MBKOD='+MB+' and MS_SORSZ='+IntToStr(MS_SORSZ)+' and MS_TIPUS='+IntToStr(MS_TIPUS);
      Query_Run(qUNI, SS);
      MSID:= qUNI.Fields[0].AsString;
      if (MSID <> '') then begin
        SS:= 'if not exists(select PO_ID from POZKULD where PO_MBKOD='+MB+' and PO_TRIGGER_MSID='+ MSID+' and PO_TRIGGER_ERK_IND=' +IntToStr(ERK_IND)+') '+
            '	insert into POZKULD (PO_MBKOD, PO_TRIGGER_MSID, PO_TRIGGER_ERK_IND) values ('+MB+', '+ MSID+', '+IntToStr(ERK_IND)+')';
        Query_Run(qUNI, SS);
        end;  // if
    end;

begin

  WriteLogFile(pozikuldeslogfn,'--- Feltöltés kezdete: '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
  S:= 'select MB_MBKOD, min(MS_FELDAT+'' ''+MS_FELIDO) FELRAK from MEGBIZAS left join MEGSEGED on MS_MBKOD = MB_MBKOD '+
    '	where mb_bordno1= '''+MercedesBordero+''' '+
    // ' and MS_FELDAT+MS_FELIDO <= (CONVERT(varchar(11), DATEADD(hh, '+MennyivelKorabban+', getdate()), 102)+''.''+convert(varchar(5),DATEADD(hh, '+MennyivelKorabban+', getdate()) ,108))'+
    ' and MS_FELDAT <= (CONVERT(varchar(11), getdate(), 102)+''.'')'+  // csak a napot nézzük (aki még ma elindul)
		' and isnull(MS_LETIDO, '''')=''''  '+  // csak a még lerakatlan sorok
    ' group by MB_MBKOD	order by 1 ';
  Query_run(QueryMain, S, False);  // recordset: MB_MBKOD, FELRAK
  while not QueryMain.Eof do begin
      MBKOD:= QueryMain.FieldByName('MB_MBKOD').AsString;
      Felrak:= QueryMain.FieldByName('FELRAK').AsString;
      LimitTimeStamp:= DateTimeStrToDateTime(copy(Felrak, 1, 11), copy(Felrak, 13, 5));
      LimitString:= FormatDateTime('yyyy.mm.dd. HH:MM', IncHour(LimitTimeStamp, -1*MennyivelKorabban)); // az első felrakó előtt X órával
      try
        IdozitettSorBeszur(MBKOD, '04:00', LimitString);
        IdozitettSorBeszur(MBKOD, '08:00', LimitString);
        IdozitettSorBeszur(MBKOD, '12:00', LimitString);
        IdozitettSorBeszur(MBKOD, '16:00', LimitString);
        IdozitettSorBeszur(MBKOD, '20:00', LimitString);
        IdozitettSorBeszur(MBKOD, '23:59', LimitString);

        TriggerSorBeszur(MBKOD, 1, 0, 0);  //   MOVAR_ERKEZES
        TriggerSorBeszur(MBKOD, 1, 0, 1);  //   MOVAR_INDULAS
        TriggerSorBeszur(MBKOD, 1, 1, 0);  //   BRECLAV_ERKEZES
        TriggerSorBeszur(MBKOD, 3, 0, 1);  //   BRECLAV_INDULAS
        TriggerSorBeszur(MBKOD, 2, 1, 0);  //   WORTH_ERKEZES
      except
        on E: exception do begin
           S:= '--- HIBA A POZÍCIÓ TRIGGER FELTÖLTÉS SORÁN! ('+E.Message+') ';
           WriteLogFile(pozikuldeslogfn, S+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));
           EmailDlg.SendSimaIrodaiLevel('nagy.peter@js.hu', 'FUVAR_TIMER gubanc', S, '');
           end;
        end;  // try-except
      QueryMain.Next;
      end;  // while
   //---------------- lezárult megbízásokhoz tartozó időzített sorok törlése ---------
    S:= 'delete from POZKULD where PO_TRIGGER_IDOPONT is not null '+ // időzített sor
            ' and PO_KULDVE_TIMESTAMP is null '+  // még nincs kiküldve
            ' and not exists (select p2.PO_ID from POZKULD p2 '+  // nincs már elküldetlen ...
                    ' where p2.PO_MBKOD = POZKULD.PO_MBKOD and PO_TRIGGER_MSID is not null '+ // ... odaérés-elindulás sor
                    ' and PO_KULDVE_TIMESTAMP is null) ';
    SQLFuttat('POZKULD', S);
    // -------------------
    WriteLogFile(pozikuldeslogfn,'--- Feltöltés vége. '+FormatDateTime('YYYY.MM.DD. HH:mm:ss', now));

end;

end.
