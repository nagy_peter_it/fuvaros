unit CsatolmanyFm;

interface

uses
	SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
	StdCtrls, Forms, DBCtrls, DB, DBGrids, DBTables, Grids, ExtCtrls, Buttons,
	ShellApi, Dialogs, J_FOFORMUJ, ADODB, Mask, Menus;

type
	TCsatolmanyFmDlg = class(TJ_FOFORMUJDLG)
    BitBtn7: TBitBtn;
	 procedure FormCreate(Sender: TObject);override;
	 procedure Adatlap(cim, kod : string);override;
    procedure ButtonKuldClick(Sender: TObject);override;
    procedure BitBtn7Click(Sender: TObject);
	 private
	 public
	end;

var
	CsatolmanyFmDlg : TCsatolmanyFmDlg;

implementation

uses
	Egyeb, J_SQL, Alvallalbe, Kozos;

{$R *.DFM}

procedure TCsatolmanyFmDlg.FormCreate(Sender: TObject);
begin
	Inherited FormCreate(Sender);
//	AddSzuromezoRovid( 'TT_TIPUS', 'TETANAR' );
	alapstr		:= 'Select * from CSATOLMANY ';
	if EgyebDlg.parameter_str <> '' then begin
	   alapstr	:= alapstr + 'WHERE CS_JAKOD = '''+EgyebDlg.parameter_str+''' ';
	end;
	FelTolto('Új csatolmány felvitele ', 'CSatolmány módosítása ',
			'Csatolmányok listája', 'CS_CSKOD', alapstr, 'CSATOLMANY', QUERY_ADAT_TAG );
	kulcsmezo			:= 'CS_CSKOD';
	nevmezo				:= 'CS_CSPDF';
	width				:= DLG_WIDTH;
	height				:= DLG_HEIGHT;
	ButtonUj.Hide;
	ButtonNez.Hide;
	ButtonTor.Hide;
	ButtonMod.Hide;
	BitBtn7.Parent		:= PanelBottom;
end;

procedure TCsatolmanyFmDlg.ButtonKuldClick(Sender: TObject);
begin
	Inherited ButtonKuldClick(Sender);
	ret_vnev	:= Query1.FieldByName('CS_CSPDF').AsString;
end;

procedure TCsatolmanyFmDlg.Adatlap(cim, kod : string);
begin
//	Application.CreateForm(TAlvallalbeDlg, AlForm);
	Inherited Adatlap(cim, kod );
end;

procedure TCsatolmanyFmDlg.BitBtn7Click(Sender: TObject);
var
	dir0	: string;
begin
	// A PDF megtekintése
	dir0	:= EgyebDlg.Read_SZGrid('999', '723');
	if ( ( copy(dir0, Length(dir0), 0) <> '\' ) and ( copy(dir0, Length(dir0), 0) <> '/' ) ) then begin
		dir0	:= dir0 + '\';
	end;
	if not FileExists(dir0+Query1.FieldByName('CS_CSPDF').AsString) then begin
		NoticeKi('Figyelem! A PDF állomány nem létezik!');
		Exit;
	end;
	ShellExecute(Application.Handle,'open',PChar(dir0+Query1.FieldByName('CS_CSPDF').AsString), '', '', SW_SHOWNORMAL );
end;

end.


