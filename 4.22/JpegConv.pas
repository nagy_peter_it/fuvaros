unit JpegConv;

interface

uses Windows, Graphics, SysUtils, Classes;

procedure CreateThumbnail(InStream, OutStream: TStream; MimeType: string;
  Width, Height: Integer; FillColor: TColor=clWhite); overload;
procedure CreateThumbnail(const InFileName, OutFileName, MimeType: string;
  Width, Height: Integer; FillColor: TColor=clWhite); overload;

implementation

uses Jpeg, Vcl.Imaging.pngimage;

procedure CreateThumbnail(InStream, OutStream: TStream; MimeType: string;
  Width, Height: Integer; FillColor: TColor=clWhite);
var
  JpegImage: TJpegImage;
  PngImage: TPngImage;
  Bitmap: TBitmap;
  Ratio: Double;
  ARect: TRect;
  AHeight, AHeightOffset, InputHeight, InputWidth: Integer;
  AWidth, AWidthOffset: Integer;
begin
//  Check for invalid parameters
  if Width<1 then
    raise Exception.Create('Invalid Width');
  if Height<1 then
    raise Exception.Create('Invalid Height');
  JpegImage:=TJpegImage.Create;
  // if (MimeType = 'image/jpg') then //// "JpegImage" is always created
  if (MimeType = 'image/png') then
     PngImage:=TPngImage.Create;
  try

//  Load the image
  if (MimeType = 'image/jpg') then begin
      JpegImage.LoadFromStream(InStream);
      InputHeight:= JpegImage.Height;
      InputWidth:= JpegImage.Width;
      end;
  if (MimeType = 'image/png') then begin
      PngImage.LoadFromStream(InStream);
      InputHeight:= PngImage.Height;
      InputWidth:= PngImage.Width;
      end;
    if (InputWidth <= Width) and (InputHeight <= Height) then begin
      InStream.Position:= 0;
      OutStream.CopyFrom(InStream, InStream.Size);
      end
    else begin
  // Create bitmap, and calculate parameters
      Bitmap:=TBitmap.Create;
      try
        Ratio:=InputWidth / InputHeight;
        if Ratio>1 then
        begin
          AHeight:=Round(Width/Ratio);
          AHeightOffset:=(Height-AHeight) div 2;
          AWidth:=Width;
          AWidthOffset:=0;
        end
        else
        begin
          AWidth:=Round(Height*Ratio);
          AWidthOffset:=(Width-AWidth) div 2;
          AHeight:=Height;
          AHeightOffset:=0;
        end;
        Bitmap.Width:=Width;
        Bitmap.Height:=Height;
        Bitmap.Canvas.Brush.Color:=FillColor;
        Bitmap.Canvas.FillRect(Rect(0,0,Width,Height));
        // StretchDraw original image
        ARect:=Rect(AWidthOffset,AHeightOffset,AWidth+AWidthOffset,AHeight+AHeightOffset);
        if (MimeType = 'image/jpg') then begin
            Bitmap.Canvas.StretchDraw(ARect,JpegImage);
            JpegImage.Assign(Bitmap);
            JpegImage.SaveToStream(OutStream);
            end;
        if (MimeType = 'image/png') then begin
            Bitmap.Canvas.StretchDraw(ARect,PngImage); //
            JpegImage.Assign(Bitmap); //
            JpegImage.SaveToStream(OutStream);
            end;
      // Assign back to the Jpeg, and save to the file
      finally
        if Bitmap <> nil then Bitmap.Free;
      end;
    end;  // else
  finally
    if JpegImage <> nil then JpegImage.Free;
    if PngImage <> nil then PngImage.Free;
  end;
end;

procedure CreateThumbnail(const InFileName, OutFileName, MimeType: string;
  Width, Height: Integer; FillColor: TColor=clWhite); overload;
var
  InStream, OutStream: TFileStream;
begin
  InStream:=TFileStream.Create(InFileName,fmOpenRead);
  try
    OutStream:=TFileStream.Create(OutFileName,fmOpenWrite or fmCreate);
    try
      CreateThumbnail(InStream, OutStream, MimeType, Width, Height, FillColor);
    finally
      OutStream.Free;
    end;
  finally
    InStream.Free;
  end;
end;

end.
