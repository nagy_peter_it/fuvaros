unit UzemalisHavi2;

interface

uses
  WinTypes, WinProcs, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, QuickRpt, QRExpr, Qrctrls, ExtCtrls, printers, DB, DBTables, Egyeb, J_SQL,
  Grids, Kozos, ADODB ;

type
  TUzemaLisHavi2Dlg = class(TForm)
    QRLabel1: TQRLabel;
	 QRLabel13: TQRLabel;
    QRLabel14: TQRLabel;
    Query1: TADOQuery;
    Query2: TADOQuery;
    Query3: TADOQuery;
    RepUzemHavi2: TQuickRep;
    QRBand3: TQRBand;
    QRShape8: TQRShape;
    QRShape7: TQRShape;
    QRShape6: TQRShape;
	 QRShape5: TQRShape;
    QRShape3: TQRShape;
    QRShape1: TQRShape;
    QRLSzamla: TQRLabel;
    QRLabel15: TQRLabel;
    QRLabel17: TQRLabel;
    QRLabel6: TQRLabel;
    QRSysData1: TQRSysData;
    QRLabel7: TQRLabel;
    QRSysData4: TQRSysData;
    QRLabel3: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel12: TQRLabel;
    QRLabel22: TQRLabel;
    QRLabel24: TQRLabel;
    QRLabel9: TQRLabel;
    QRLabel10: TQRLabel;
    QRLabel20: TQRLabel;
    QRLabel23: TQRLabel;
    QRShape18: TQRShape;
    QRLabel26: TQRLabel;
    QRShape19: TQRShape;
    QRLabel27: TQRLabel;
    QRLabel19: TQRLabel;
    QRBand1: TQRBand;
    QL4: TQRLabel;
    QL6: TQRLabel;
    QL2: TQRLabel;
    QL0: TQRLabel;
    QL1: TQRLabel;
    QL3: TQRLabel;
    QL5: TQRLabel;
    QL7: TQRLabel;
    QRBand2: TQRBand;
    QRLabel21: TQRLabel;
    QL40: TQRLabel;
    QL50: TQRLabel;
    QL80: TQRLabel;
    QRShape29: TQRShape;
    QRLabel42: TQRLabel;
    QRLabel43: TQRLabel;
    QL9: TQRLabel;
    QRBand4: TQRBand;
    QRLabel2: TQRLabel;
    QRLabel5: TQRLabel;
    QRShape2: TQRShape;
    QL8: TQRLabel;
    QRGroup1: TQRGroup;
    QRBand5: TQRBand;
    QC1: TQRLabel;
    QC4: TQRLabel;
    QC5: TQRLabel;
    QC6: TQRLabel;
    QC7: TQRLabel;
    QC8: TQRLabel;
    QC9: TQRLabel;
    procedure FormCreate(Sender: TObject);
    procedure	Tolt(rsz, datumtol, datumig, apnorma, telep : string );
    procedure QRBand1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure RepUzemHavi2BeforePrint(Sender: TCustomQuickRep;
      var PrintReport: Boolean);
    procedure QRBand2BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure FormDestroy(Sender: TObject);
    procedure QRBand5BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBand5AfterPrint(Sender: TQRCustomBand; BandPrinted: Boolean);
  private
       d1          	: string;
       d2          	: string;
     	rekordszam		: integer;
       // szumma a teljes riportra
       osszegek        : array [1..7] of double;    // 1: megtett km 2: tankolt liter 3: �tlag liter 4: �tlag t/km  5: megtakar�t�s liter 6: �a.m l/100km
                                                    // 7: megtett km minden sorra
       // szumma a csoportra
       qcossz          : array [1..7] of double;
       gkszam          : integer;
       gkszam_all      : integer;
       gkszamossz      : integer;
  public
	  vanadat			: boolean;
     kategoriak        : string;
  end;

var
  UzemaLisHavi2Dlg: TUzemaLisHavi2Dlg;

implementation

uses
	J_Fogyaszt, StrUtils;
{$R *.DFM}

procedure TUzemaLisHavi2Dlg.FormCreate(Sender: TObject);
begin
   Application.CreateForm(TFogyasztasDlg, FogyasztasDlg);
	EgyebDlg.SetADOQueryDatabase(Query1);
	EgyebDlg.SetADOQueryDatabase(Query2);
	EgyebDlg.SetADOQueryDatabase(Query3);
end;

procedure	TUzemaLisHavi2Dlg.Tolt(rsz, datumtol, datumig, apnorma, telep  : string );
var
  sqlstr	: string;
  str, S		: string;
  tkmatlag : double;
  sqlkateg : string;
begin
  	EllenListTorol;
	d1		    := copy(datumtol,1,11);
	d2		    := copy(datumig,1,11);
   QRLabel17.Caption 	:= datumtol + ' - ' + datumig;
   sqlkateg    := '';
   if kategoriak <> '' then begin
       sqlkateg := ' AND GK_GKKAT IN ('+kategoriak+') ';
   end;

   if rsz <> '' then begin
      S:='SELECT GK_RESZ, GK_GKKAT, SZ_EGYEB1 FROM GEPKOCSI, SZOTAR WHERE ( GK_POTOS = 2 OR GK_POTOS = 0) ';
      // S:=S+' AND GK_ARHIV = 0 AND GK_KIVON = 0 ';
      // a kimutat�si id�szakban �l� aut� volt
      S:=S+' and GK_SFORH <= '''+d2+''' and ((GK_FORKI >= '''+d1+''') or (GK_FORKI = '''')) and ((GK_IFORK >= '''+d1+''') or (GK_IFORK = ''''))';
      S:=S+' AND SZ_FOKOD=''340'' AND SZ_ALKOD=GK_GKKAT AND GK_RESZ = '''+rsz+''' ';
	    Query_Run(Query3, S, true);
      QRBand2.Enabled := false;
   end else begin
	    // Query_Run(Query3, 'SELECT GK_RESZ, GK_GKKAT FROM GEPKOCSI WHERE ( GK_POTOS = 2 OR GK_POTOS = 0) AND GK_ARHIV = 0 AND GK_KIVON = 0 '+sqlkateg+' ORDER BY GK_GKKAT, GK_RESZ ', true);
      S:='SELECT GK_RESZ, GK_GKKAT, SZ_EGYEB1 FROM GEPKOCSI, SZOTAR WHERE ( GK_POTOS = 2 OR GK_POTOS = 0) ';
      // S:=S+' AND GK_ARHIV = 0 AND GK_KIVON = 0 ';
      // a kimutat�si id�szakban �l� aut� volt
      S:=S+' and GK_SFORH <= '''+d2+''' and ((GK_FORKI >= '''+d1+''') or (GK_FORKI = '''')) and ((GK_IFORK >= '''+d1+''') or (GK_IFORK = ''''))';
      S:=S+' AND SZ_FOKOD=''340'' AND SZ_ALKOD=GK_GKKAT AND GK_NOKIM = 0 '+sqlkateg+' ORDER BY GK_GKKAT, GK_RESZ ';
      Query_Run(Query3, S, true);
   end;
   vanadat := Query3.RecordCount > 0;
end;

procedure TUzemaLisHavi2Dlg.QRBand1BeforePrint(Sender: TQRCustomBand;
	   var PrintBand: Boolean);
var
   rsz         : string;
   megtliter   : double;
   notank: boolean;  // a kateg�ri�hoz nincsenek tankol�si adatok
begin
	QL1.Caption	    := Query3.FieldByName('GK_RESZ').AsString;
	QL0.Caption	    := IntToStr(rekordszam);
   Inc(rekordszam);
   QL2.Caption     := '';
   QL3.Caption     := '';
   QL4.Caption     := '';
   QL5.Caption     := '';
   QL6.Caption     := '';
   QL7.Caption     := '';
   QL8.Caption     := '';
   QL9.Caption     := '';
   rsz             := Query3.FieldByName('GK_RESZ').AsString;
   if Query3.FieldByName('SZ_EGYEB1').AsInteger = 1 then notank:=True else notank:=False;
   Query_Run(Query1, 'SELECT * FROM ZAROKM WHERE ZK_RENSZ = '''+rsz+''' AND ZK_DATUM = '''+d2+''' ');
   QL2.Alignment   := taCenter;
   QL2.AutoSize    := false;
   QL2.Width       := 90;
   if Query1.RecordCount < 1 then begin
       QL2.Alignment   := taLeftJustify;
       QL2.AutoSize    := true;
       QL2.Width       := 1500;
       QL2.Caption     := 'Hi�nyz� z�r� km ('+d2+')';
       QL0.Caption	    := '* '+QL0.Caption ;
       Exit;
   end;
   // Van nyit�/z�r� km
   QL2.Caption    := Query1.FieldByName('ZK_NYITOKM').AsString;
   QL3.Caption    := Query1.FieldByName('ZK_KMORA').AsString;
   if QL2.Caption =  QL3.Caption then begin
      QL2.Color:=cl3DLight;
      QL3.Color:=cl3DLight;
      end
   else begin
      QL2.Color:=clWhite;
      QL3.Color:=clWhite;
      end;
//   HibaKiiro(rsz + Query1.FieldByName('ZK_NYITOKM').AsString+Query1.FieldByName('ZK_KMORA').AsString);
   FogyasztasDlg.ClearAllList;
   FogyasztasDlg.GeneralBetoltes(rsz, StrToIntdef(Query1.FieldByName('ZK_NYITOKM').AsString, 0), StrToIntDef(Query1.FieldByName('ZK_KMORA').AsString, 0) );
   FogyasztasDlg.CreateTankLista;
   FogyasztasDlg.CreateAdBlueLista;
   FogyasztasDlg.Szakaszolo;
   FogyasztasDlg.TeljesLista;
   FogyasztasDlg.Memo1.Lines.SaveToFile(EgyebDlg.TempLIst + 'UZMEGHAVI.TXT');
   QL4.Caption     := Format('%.0f', [FogyasztasDlg.GetOsszKm]);
   if not(notank) then begin
     QL5.Caption     := Format('%.2f', [FogyasztasDlg.GetTenyFogyasztas]);
     QL6.Caption     := Format('%.2f', [FogyasztasDlg.GetAtlag]);
     QL7.Caption     := Format('%.2f', [FogyasztasDlg.GetSulyAtlag]);
     QL8.Caption     := Format('%.2f', [FogyasztasDlg.GetMegtakaritas]);
     if FogyasztasDlg.GetOsszKm > 0 then begin
       QL9.Caption     := Format('%.2f', [FogyasztasDlg.GetMegtakaritas / (FogyasztasDlg.GetOsszKm / 100)]);
     end else begin
       QL9.Caption     := '0.00';
     end;
     end;  // if NOTANK
   // �szes km-t mindenk�pp sz�molunk
   qcossz[7] := qcossz[7] + StringSzam(QL4.Caption);
   Inc(gkszam_all);
   if FogyasztasDlg.GetAtlag = 0 then begin
       // Nincs k�vetkez� tankol�s
       if (QL2.Caption <> QL3.Caption) and not(notank) then // csak ha mozgott, �s ha nem tankol�s n�lk�li kateg�ria
           QL0.Caption	    := '* '+QL0.Caption ;
   end else begin
       // Csak akkor k�pz�nk �sszegeket, ha nem 0 a tankolt me.
       osszegek[1] := osszegek[1] + StringSzam(QL4.Caption);
       osszegek[2] := osszegek[2] + StringSzam(QL5.Caption);
       osszegek[3] := osszegek[3] + StringSzam(QL6.Caption);
       osszegek[4] := osszegek[4] + StringSzam(QL7.Caption);
       osszegek[5] := osszegek[5] + StringSzam(QL8.Caption);
       osszegek[6] := osszegek[6] + StringSzam(QL9.Caption);
       // A csoportonk�nti �sszegek k�pz�se
       qcossz[1] := qcossz[1] + StringSzam(QL4.Caption);
       qcossz[2] := qcossz[2] + StringSzam(QL5.Caption);
       qcossz[3] := 0;
       qcossz[4] := qcossz[4] + StringSzam(QL7.Caption) * StringSzam(QL4.Caption); // Itt a summa tonna fog kij�nni
       qcossz[5] := qcossz[5] + StringSzam(QL8.Caption);
       qcossz[6] := 0;
       Inc(gkszam);
       Inc(gkszamossz);
   end;
end;

procedure TUzemaLisHavi2Dlg.RepUzemHavi2BeforePrint(Sender: TCustomQuickRep;
      var PrintReport: Boolean);
var
   i : integer;
begin
   rekordszam	:= 1;
   gkszam      := 0;
   gkszam_all  := 0;
   gkszamossz  := 0;
   for i := 1 to 7 do begin
       osszegek[i] := 0;
       qcossz[i]   := 0;
   end;
end;

procedure TUzemaLisHavi2Dlg.QRBand2BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
   if gkszamossz = 0 then begin
       QL40.Caption     := '0';
   end else begin
       QL40.Caption     := Format('%.0f', [osszegek[1]/gkszamossz]);
   end;
   QL50.Caption     := Format('%.2f', [osszegek[2]]);
   QL80.Caption     := Format('%.2f', [osszegek[5]]);
end;

procedure TUzemaLisHavi2Dlg.FormDestroy(Sender: TObject);
begin
   FogyasztasDlg.Destroy;
end;

procedure TUzemaLisHavi2Dlg.QRBand5AfterPrint(Sender: TQRCustomBand;
  BandPrinted: Boolean);
var
   i : integer;
begin
   // csak a csoportra vonatkoz� �rt�keket null�zzuk
   gkszam      := 0;
   gkszam_all  := 0;
   for i := 1 to 7 do begin
       qcossz[i]   := 0;
   end;
end;

procedure TUzemaLisHavi2Dlg.QRBand5BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
   i   : integer;
   notank: boolean;  // a kateg�ri�hoz nincsenek tankol�si adatok
begin
   QC1.Caption     := Query3.FieldByName('GK_GKKAT').AsString + ' gk. kateg�ria �sszesen : ';
   if Query3.FieldByName('SZ_EGYEB1').AsInteger = 1 then notank:=True else notank:=False;
   if not(notank) then begin
     if gkszam > 0 then begin
       QC4.Caption     := Format('%.0f', [qcossz[1] / gkszam]);
     end else begin
       QC4.Caption     := '0';
       end;
     QC5.Caption     := Format('%.2f', [qcossz[2]]);
     if qcossz[1] = 0 then begin
       QC6.Caption     := '0.00';
       QC7.Caption     := '0.00';
       QC9.Caption     := '0.00';
     end else begin
       QC6.Caption     := Format('%.2f', [qcossz[2]*100/qcossz[1]]);
       QC7.Caption     := Format('%.2f', [qcossz[4]/qcossz[1]]);
       QC9.Caption     := Format('%.2f', [qcossz[5]*100/qcossz[1]]);
       end;
     QC8.Caption     := Format('%.2f', [qcossz[5]]);
     end  // if Not NOTANK
   else begin
     if gkszam_all > 0 then begin
       QC4.Caption     := Format('%.0f', [qcossz[7] / gkszam_all]);
     end else begin
       QC4.Caption     := '0';
       end;
      QC5.Caption:='';
      QC6.Caption:='';
      QC7.Caption:='';
      QC8.Caption:='';
      QC9.Caption:='';
      end;  // else
   for i := 1 to 6 do begin
       qcossz[i]   := 0;
   end;
   gkszam      := 0;
end;

end.

