unit MegJaratAl1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Mask, Buttons, ExtCtrls, J_ALFORM, DB, ADODB;

type
  TMegjaratAl1Dlg = class(TJ_AlformDlg)
    GroupBox4: TGroupBox;
    Label3: TLabel;
    Label33: TLabel;
    Label34: TLabel;
    Label35: TLabel;
    Label36: TLabel;
    CB4: TComboBox;
    BitBtn9: TBitBtn;
    CB41: TComboBox;
    M2000A: TMaskEdit;
    M3000: TMaskEdit;
    M4000: TMaskEdit;
    M3100: TMaskEdit;
    M3200: TMaskEdit;
    M3300: TMaskEdit;
    M3400: TMaskEdit;
    M3500: TMaskEdit;
    GroupBox3: TGroupBox;
    Label28: TLabel;
    Label29: TLabel;
    Label30: TLabel;
    Label31: TLabel;
    Label32: TLabel;
    CB3: TComboBox;
    BitBtn7: TBitBtn;
    CB31: TComboBox;
    M200A: TMaskEdit;
    M300: TMaskEdit;
    M400: TMaskEdit;
    M310: TMaskEdit;
    M320: TMaskEdit;
    M330: TMaskEdit;
    M340: TMaskEdit;
    M350: TMaskEdit;
    BitElkuld: TBitBtn;
    BitKilep: TBitBtn;
    GroupBox1: TGroupBox;
    Label6: TLabel;
    Label13: TLabel;
    M5: TMaskEdit;
    BitBtn10: TBitBtn;
    M6: TMaskEdit;
    M6__: TMaskEdit;
    M5__: TMaskEdit;
    BitBtn36: TBitBtn;
    GroupBox2: TGroupBox;
    Label1: TLabel;
    Label14: TLabel;
    M50: TMaskEdit;
    BitBtn1: TBitBtn;
    M60: TMaskEdit;
    M60__: TMaskEdit;
    M50__: TMaskEdit;
    BitBtn5: TBitBtn;
    Query2: TADOQuery;
    Query1: TADOQuery;
    QueryKoz: TADOQuery;
    procedure BitKilepClick(Sender: TObject);
    procedure Tolto(cim : string; teko : string); override;
    procedure FormCreate(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn5Click(Sender: TObject);
    procedure BitBtn10Click(Sender: TObject);
    procedure BitBtn36Click(Sender: TObject);
    procedure M50Exit(Sender: TObject);
    procedure M60Exit(Sender: TObject);
    procedure BitElkuldClick(Sender: TObject);

        procedure MezoSzinezo(tipus:integer);
    procedure BitBtn9Click(Sender: TObject);
    procedure BitBtn7Click(Sender: TObject);
    procedure CB4Change(Sender: TObject);
    procedure CB3Change(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure M5Exit(Sender: TObject);
    procedure M6Exit(Sender: TObject);
    procedure M5__Exit(Sender: TObject);
    procedure M6__Exit(Sender: TObject);
  private
		listaorsz	: TStringList;
  public
       tipus   : integer;
  end;

var
  MegjaratAl1Dlg: TMegjaratAl1Dlg;

implementation

uses
   Egyeb, J_SQL, Kozos, FelrakoFm;

{$R *.dfm}

procedure TMegjaratAl1Dlg.FormCreate(Sender: TObject);
begin
	EgyebDlg.SetADOQueryDatabase(Query1);
	EgyebDlg.SetADOQueryDatabase(Query2);
	EgyebDlg.SetADOQueryDatabase(QueryKoz);
	listaorsz	:= TStringList.Create;
	SzotarTolt(CB31, '300' , listaorsz, true, true );
	SzotarTolt(CB41, '300' , listaorsz, true, true );
end;

procedure TMegjaratAl1Dlg.BitKilepClick(Sender: TObject);
begin
   Close;
end;

procedure TMegjaratAl1Dlg.Tolto(cim : string; teko:string);
begin
   // Ide j�n a bet�lt�s
   Caption := cim;
   Query_Run(Query1, 'SELECT * FROM MEGSEGED WHERE MS_MSKOD = '+teko);
   // Query_Run(Query1, 'SELECT * FROM MEGSEGED WHERE MS_ID = '+teko);
   if Query1.RecordCount < 1 then begin
       NoticeKi('Hi�nyz� megb�z�s rekord!');
       Exit;
   end;
   case tipus of

       0 : // El�rak�s
       begin
			CB4.Text	:= Query1.FieldByName('MS_TEFNEV').AsString;
			ComboSet (CB41, Query1.FieldByName('MS_TEFOR').AsString, listaorsz);
			M2000A.Text	:= Query1.FieldByName('MS_TEFIR').AsString;
			M3000.Text	:= Query1.FieldByName('MS_TEFTEL').AsString;
			M4000.Text	:= Query1.FieldByName('MS_TEFCIM').AsString;
			M3100.Text	:= Query1.FieldByName('MS_TEFL1').AsString;
			M3200.Text	:= Query1.FieldByName('MS_TEFL2').AsString;
			M3300.Text	:= Query1.FieldByName('MS_TEFL3').AsString;
			M3400.Text	:= Query1.FieldByName('MS_TEFL4').AsString;
			M3500.Text	:= Query1.FieldByName('MS_TEFL5').AsString;

           // A t�ny lerak� be�ll�t�sa a telephelynek
           Query_Run(QueryKoz, 'SELECT * FROM FELRAKO WHERE FF_FEKOD = '+EgyebDlg.TELEPFELKOD);
           CB3.Text 	:= QueryKoz.FieldByName('FF_FELNEV').AsString;
			ComboSet (CB31, QueryKoz.FieldByName('FF_ORSZA').AsString, listaorsz);
           M200A.Text 	:= QueryKoz.FieldByName('FF_FELIR').AsString;
           M300.Text 	:= QueryKoz.FieldByName('FF_FELTEL').AsString;
           M400.Text 	:= QueryKoz.FieldByName('FF_FELCIM').AsString;
           M310.Text 	:= QueryKoz.FieldByName('FF_FELSE1').AsString;
           M320.Text 	:= QueryKoz.FieldByName('FF_FELSE2').AsString;
           M330.Text 	:= QueryKoz.FieldByName('FF_FELSE3').AsString;
           M340.Text 	:= QueryKoz.FieldByName('FF_FELSE4').AsString;
           M350.Text 	:= QueryKoz.FieldByName('FF_FELSE5').AsString;

           // A nem enged�lyezett elemek
           Self.ActiveControl  := M50;
           // a k�s�bbiekben �gyis meg tudja v�ltoztatni, ez�rt nem indokolt a tilt�s NP 2015-08-24
           GroupBox4.Enabled   := True;  // false;
           GroupBox3.Enabled   := True;  // false;
           // ---------------------------------------
           GroupBox1.Enabled   := false;
           MezoSzinezo(tipus);

       end;

       1 : // Lerak�
       begin
           // A t�ny felrak� be�ll�t�sa a telephelynek
           Query_Run(QueryKoz, 'SELECT * FROM FELRAKO WHERE FF_FEKOD = '+EgyebDlg.TELEPFELKOD);
           CB4.Text 	:= QueryKoz.FieldByName('FF_FELNEV').AsString;
			ComboSet (CB41, QueryKoz.FieldByName('FF_ORSZA').AsString, listaorsz);
           M2000A.Text 	:= QueryKoz.FieldByName('FF_FELIR').AsString;
           M3000.Text 	:= QueryKoz.FieldByName('FF_FELTEL').AsString;
           M4000.Text 	:= QueryKoz.FieldByName('FF_FELCIM').AsString;
           M3100.Text 	:= QueryKoz.FieldByName('FF_FELSE1').AsString;
           M3200.Text 	:= QueryKoz.FieldByName('FF_FELSE2').AsString;
           M3300.Text 	:= QueryKoz.FieldByName('FF_FELSE3').AsString;
           M3400.Text 	:= QueryKoz.FieldByName('FF_FELSE4').AsString;
           M3500.Text 	:= QueryKoz.FieldByName('FF_FELSE5').AsString;

			CB3.Text	:= Query1.FieldByName('MS_TENNEV').AsString;
			ComboSet (CB31, Query1.FieldByName('MS_TENOR').AsString, listaorsz);
			M200A.Text	:= Query1.FieldByName('MS_TENIR').AsString;
			M300.Text	:= Query1.FieldByName('MS_TENTEL').AsString;
			M400.Text	:= Query1.FieldByName('MS_TENCIM').AsString;
			M310.Text	:= Query1.FieldByName('MS_TENYL1').AsString;
			M320.Text	:= Query1.FieldByName('MS_TENYL2').AsString;
			M330.Text	:= Query1.FieldByName('MS_TENYL3').AsString;
			M340.Text	:= Query1.FieldByName('MS_TENYL4').AsString;
			M350.Text	:= Query1.FieldByName('MS_TENYL5').AsString;

           // A nem enged�lyezett elemek
           Self.ActiveControl  := M5;
           // a k�s�bbiekben �gyis meg tudja v�ltoztatni, ez�rt nem indokolt a tilt�s NP 2015-08-24
           GroupBox4.Enabled   := True;  // false;
           GroupBox3.Enabled   := True;  // false;
           // ---------------------------------------
//           GroupBox2.Enabled   := false;
           MezoSzinezo(tipus);
       end;
   end;
end;


procedure TMegjaratAl1Dlg.BitBtn1Click(Sender: TObject);
begin
	EgyebDlg.Calendarbe(M50);
   if M50__.Text = '' then begin
       M50__.Text := M50.Text;
   end;
   M60.SetFocus;
end;

procedure TMegjaratAl1Dlg.BitBtn5Click(Sender: TObject);
begin
	EgyebDlg.Calendarbe(M50__, True);
end;

procedure TMegjaratAl1Dlg.BitBtn10Click(Sender: TObject);
begin
	EgyebDlg.Calendarbe(M5);
   if M5__.Text = '' then begin
       M5__.Text := M5.Text;
   end;
   M6.SetFocus;
end;

procedure TMegjaratAl1Dlg.BitBtn36Click(Sender: TObject);
begin
	EgyebDlg.Calendarbe(M5__, True);
end;

procedure TMegjaratAl1Dlg.M50Exit(Sender: TObject);
begin
   DatumExit(Sender as TMAskEdit, false);
   if M50__.Text = '' then begin
       M50__.Text := M50.Text;
   end;
end;

procedure TMegjaratAl1Dlg.M60Exit(Sender: TObject);
begin
	IdoExit(Sender as TMaskEdit, false);
   if M60__.Text = '' then begin
       M60__.Text := M60.Text;
   end;
end;

procedure TMegjaratAl1Dlg.BitElkuldClick(Sender: TObject);
begin
   if ( ( tipus = 0 ) and ( M50.Text = '' ) ) then begin
       NoticeKi('A d�tum megad�sa k�telez�!');
       M50.SetFocus;
       Exit;
   end;
   if ( ( tipus = 1 ) and ( M5.Text = '' ) ) then begin
       NoticeKi('A d�tum megad�sa k�telez�!');
       M5.SetFocus;
       Exit;
   end;
   if ( (M5__.Text <> '') and (M5__.Text < M5.Text) ) then begin
       NoticeKi('�rv�nytelen d�tum!');
       M5__.SetFocus;
       Exit;
   end;
   if ( (M50__.Text <> '') and (M50__.Text < M50.Text) ) then begin
       NoticeKi('�rv�nytelen d�tum!');
       M50__.SetFocus;
       Exit;
   end;
   ret_kod := '1';
   Close;
end;

procedure TMegjaratAl1Dlg.MezoSzinezo(tipus:integer);
begin
   CB3.Color   := TILTOTTSZIN;
   CB31.Color  := TILTOTTSZIN;
   CB4.Color   := TILTOTTSZIN;
   CB41.Color  := TILTOTTSZIN;
   SetMaskEdits([M2000A, M3000, M3100, M3200, M3300, M3400, M3500, M4000, M200A, M300, M310, M320, M330, M340, M350, M400]);
   case tipus of
       0 : SetMaskEdits([M5, M6, M5__, M6__]);
//       1 : SetMaskEdits([M50, M60, M50__, M60__]);
   end;
end;

procedure TMegjaratAl1Dlg.BitBtn9Click(Sender: TObject);
begin
	// Kiv�lasztjuk a k�v�nt felrak�t / lerak�t
	Application.CreateForm(TFelrakoFmDlg, FelrakoFmDlg);
	FelrakoFmDlg.valaszt	:= true;
	FelrakoFmDlg.ShowModal;
	if FelrakoFmDlg.ret_vkod <> '' then begin
       Query_Run(QueryKoz, 'SELECT * FROM FELRAKO WHERE FF_FEKOD = '+FelrakoFmDlg.ret_vkod);
       CB4.Text	:= QueryKoz.FieldByName('FF_FELNEV').AsString;
       ComboSet (CB41, QueryKoz.FieldByName('FF_ORSZA').AsString, listaorsz);
       M2000A.Text := QueryKoz.FieldByName('FF_FELIR').AsString;
       M3000.Text 	:= QueryKoz.FieldByName('FF_FELTEL').AsString;
       M4000.Text 	:= QueryKoz.FieldByName('FF_FELCIM').AsString;
       M3100.Text 	:= QueryKoz.FieldByName('FF_FELSE1').AsString;
       M3200.Text 	:= QueryKoz.FieldByName('FF_FELSE2').AsString;
       M3300.Text 	:= QueryKoz.FieldByName('FF_FELSE3').AsString;
       M3400.Text 	:= QueryKoz.FieldByName('FF_FELSE4').AsString;
       M3500.Text 	:= QueryKoz.FieldByName('FF_FELSE5').AsString;
	end;
	FelrakoFmDlg.Destroy;
end;

procedure TMegjaratAl1Dlg.BitBtn7Click(Sender: TObject);
begin
	// Kiv�lasztjuk a k�v�nt felrak�t / lerak�t
	Application.CreateForm(TFelrakoFmDlg, FelrakoFmDlg);
	FelrakoFmDlg.valaszt	:= true;
	FelrakoFmDlg.ShowModal;
	if FelrakoFmDlg.ret_vkod <> '' then begin
       Query_Run(QueryKoz, 'SELECT * FROM FELRAKO WHERE FF_FEKOD = '+FelrakoFmDlg.ret_vkod);
       CB3.Text	:= QueryKoz.FieldByName('FF_FELNEV').AsString;
       ComboSet (CB31, QueryKoz.FieldByName('FF_ORSZA').AsString, listaorsz);
       M200A.Text  := QueryKoz.FieldByName('FF_FELIR').AsString;
       M300.Text 	:= QueryKoz.FieldByName('FF_FELTEL').AsString;
       M400.Text 	:= QueryKoz.FieldByName('FF_FELCIM').AsString;
       M310.Text 	:= QueryKoz.FieldByName('FF_FELSE1').AsString;
       M320.Text 	:= QueryKoz.FieldByName('FF_FELSE2').AsString;
       M330.Text 	:= QueryKoz.FieldByName('FF_FELSE3').AsString;
       M340.Text 	:= QueryKoz.FieldByName('FF_FELSE4').AsString;
       M350.Text 	:= QueryKoz.FieldByName('FF_FELSE5').AsString;
	end;
	FelrakoFmDlg.Destroy;
end;

procedure TMegjaratAl1Dlg.CB4Change(Sender: TObject);
begin
   Query_Run(QueryKoz, 'SELECT * FROM FELRAKO WHERE FF_FELNEV = '''+CB4.Text+''' ');
   ComboSet (CB41, QueryKoz.FieldByName('FF_ORSZA').AsString, listaorsz);
   M2000A.Text := QueryKoz.FieldByName('FF_FELIR').AsString;
   M3000.Text 	:= QueryKoz.FieldByName('FF_FELTEL').AsString;
   M4000.Text 	:= QueryKoz.FieldByName('FF_FELCIM').AsString;
   M3100.Text 	:= QueryKoz.FieldByName('FF_FELSE1').AsString;
   M3200.Text 	:= QueryKoz.FieldByName('FF_FELSE2').AsString;
   M3300.Text 	:= QueryKoz.FieldByName('FF_FELSE3').AsString;
   M3400.Text 	:= QueryKoz.FieldByName('FF_FELSE4').AsString;
   M3500.Text 	:= QueryKoz.FieldByName('FF_FELSE5').AsString;
end;

procedure TMegjaratAl1Dlg.CB3Change(Sender: TObject);
begin
   Query_Run(QueryKoz, 'SELECT * FROM FELRAKO WHERE FF_FELNEV = '''+CB3.Text+''' ');
   ComboSet (CB31, QueryKoz.FieldByName('FF_ORSZA').AsString, listaorsz);
   M200A.Text  := QueryKoz.FieldByName('FF_FELIR').AsString;
   M300.Text 	:= QueryKoz.FieldByName('FF_FELTEL').AsString;
   M400.Text 	:= QueryKoz.FieldByName('FF_FELCIM').AsString;
   M310.Text 	:= QueryKoz.FieldByName('FF_FELSE1').AsString;
   M320.Text 	:= QueryKoz.FieldByName('FF_FELSE2').AsString;
   M330.Text 	:= QueryKoz.FieldByName('FF_FELSE3').AsString;
   M340.Text 	:= QueryKoz.FieldByName('FF_FELSE4').AsString;
   M350.Text 	:= QueryKoz.FieldByName('FF_FELSE5').AsString;
end;

procedure TMegjaratAl1Dlg.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
	EgyebDlg.FormReturn(Key);
end;

procedure TMegjaratAl1Dlg.M5Exit(Sender: TObject);
begin
   DatumExit(Sender as TMAskEdit, false);
   if M5__.Text = '' then begin
       M5__.Text := M5.Text;
   end;
end;

procedure TMegjaratAl1Dlg.M6Exit(Sender: TObject);
begin
	IdoExit(Sender as TMaskEdit, false);
   if M6__.Text = '' then begin
       M6__.Text := M6.Text;
   end;
end;

procedure TMegjaratAl1Dlg.M5__Exit(Sender: TObject);
begin
   DatumExit(Sender as TMAskEdit, false);
end;

procedure TMegjaratAl1Dlg.M6__Exit(Sender: TObject);
begin
	IdoExit(Sender as TMaskEdit, false);
end;

end.
