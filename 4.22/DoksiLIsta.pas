unit DoksiLIsta;

interface

uses
	SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
	Forms, Dialogs, StdCtrls, Buttons, DB, ADODB, ShellApi;

type
	TDoksiLIstaDlg = class(TForm)
	BitElkuld: TBitBtn;
	BitKilep: TBitBtn;
	Label2: TLabel;
	Label12: TLabel;
    Query1: TADOQuery;
    LB1: TListBox;
    Label1: TLabel;
    Label3: TLabel;
	procedure BitKilepClick(Sender: TObject);
	procedure Tolto(dir0:string; lista : TStringLIst);
	procedure BitElkuldClick(Sender: TObject);
	private
	public
	end;

var
	DoksiLIstaDlg: TDoksiLIstaDlg;

implementation

uses
	Egyeb, J_SQL, Kozos;
{$R *.DFM}

procedure TDoksiLIstaDlg.BitKilepClick(Sender: TObject);
begin
	Close;
end;

procedure TDoksiLIstaDlg.Tolto(dir0:string; lista : TStringLIst);
begin
	Label3.Caption	:= dir0;
	LB1.Items.AddStrings(lista);
	LB1.ItemIndex	:= 0;
end;

procedure TDoksiLIstaDlg.BitElkuldClick(Sender: TObject);
begin
	ShellExecute(Application.Handle,'open',PChar(Label3.Caption+LB1.Items[LB1.ItemIndex]), '', '', SW_SHOWNORMAL );
end;

end.
