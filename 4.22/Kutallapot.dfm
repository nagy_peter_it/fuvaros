object KutAllapotDlg: TKutAllapotDlg
  Left = 0
  Top = 0
  Caption = #220'zemanyag k'#250't '#225'llapota'
  ClientHeight = 692
  ClientWidth = 914
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter
    Left = 0
    Top = 361
    Width = 914
    Height = 3
    Cursor = crVSplit
    Align = alTop
    ExplicitWidth = 290
  end
  object PageControl1: TPageControl
    Left = 0
    Top = 0
    Width = 914
    Height = 361
    ActivePage = TabSheet1
    Align = alTop
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    OnChange = PageControl1Change
    object TabSheet1: TTabSheet
      Caption = 'D'#237'zel heti'
      object Chart1: TChart
        Left = 0
        Top = 0
        Width = 906
        Height = 328
        Gradient.EndColor = 12910591
        Gradient.Visible = True
        Legend.Visible = False
        Title.Text.Strings = (
          'Diesel szint')
        BottomAxis.LabelsFormat.TextAlignment = taCenter
        DepthAxis.LabelsFormat.TextAlignment = taCenter
        DepthTopAxis.LabelsFormat.TextAlignment = taCenter
        LeftAxis.Automatic = False
        LeftAxis.AutomaticMaximum = False
        LeftAxis.LabelsFormat.TextAlignment = taCenter
        LeftAxis.Maximum = 100000.000000000000000000
        LeftAxis.Title.Caption = #220'zemanyag szint'
        RightAxis.LabelsFormat.TextAlignment = taCenter
        RightAxis.Title.Caption = 'Id'#337
        TopAxis.LabelsFormat.TextAlignment = taCenter
        Zoom.Pen.Mode = pmNotXor
        Align = alClient
        TabOrder = 0
        DefaultCanvas = 'TGDIPlusCanvas'
        PrintMargins = (
          15
          32
          15
          32)
        ColorPaletteIndex = 13
        object Series1: TBarSeries
          Marks.Visible = True
          Marks.Style = smsValue
          Marks.BackColor = clAqua
          Marks.Color = clAqua
          PercentFormat = '##0.##,%'
          XValues.Name = 'X'
          XValues.Order = loAscending
          YValues.Name = 'Bar'
          YValues.Order = loNone
          Data = {
            040600000000000000806AEA40FF060000004665622D303100000000407DF040
            FF060000004665622D303200000000506CF240FF060000004665622D30330000
            00000009D540FF060000004665622D3034000000000050E140FF060000004665
            622D303500000000C09AE540FF060000004665622D3036}
        end
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'D'#237'zel elm'#250'lt 12 '#243'ra'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'Tahoma'
      Font.Style = []
      ImageIndex = 1
      ParentFont = False
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Chart2: TChart
        Left = 0
        Top = 0
        Width = 906
        Height = 328
        Gradient.EndColor = 12910591
        Gradient.Visible = True
        Legend.LegendStyle = lsValues
        Legend.Visible = False
        Title.Text.Strings = (
          'Diesel szint')
        BottomAxis.LabelsFormat.TextAlignment = taCenter
        DepthAxis.LabelsFormat.TextAlignment = taCenter
        DepthTopAxis.LabelsFormat.TextAlignment = taCenter
        LeftAxis.Automatic = False
        LeftAxis.AutomaticMaximum = False
        LeftAxis.LabelsFormat.TextAlignment = taCenter
        LeftAxis.Maximum = 100000.000000000000000000
        LeftAxis.Title.Caption = #220'zemanyag szint'
        RightAxis.LabelsFormat.TextAlignment = taCenter
        RightAxis.Title.Caption = 'Id'#337
        TopAxis.LabelsFormat.TextAlignment = taCenter
        Zoom.Pen.Mode = pmNotXor
        Align = alClient
        TabOrder = 0
        DefaultCanvas = 'TGDIPlusCanvas'
        PrintMargins = (
          15
          32
          15
          32)
        ColorPaletteIndex = 13
        object BarSeries1: TBarSeries
          Marks.Visible = True
          Marks.Style = smsValue
          Marks.BackColor = 16777088
          Marks.Color = 16777088
          PercentFormat = '##0.##,%'
          Title = 'Series1'
          XValues.Name = 'X'
          XValues.Order = loAscending
          YValues.Name = 'Bar'
          YValues.Order = loNone
          Data = {
            041700000000000000806AEA40FF0500000031303A303000000000803DEA40FF
            0500000031313A303000000000803DEA40FF0500000031323A30300000000040
            F4E940FF0500000031333A303000000000A0E0E940FF0500000031343A303000
            0000006062E940FF0500000031353A303000000000402CE940FF050000003136
            3A303000000000E02BE940FF0500000031373A30300000000080F3E840FF0500
            000031383A30300000000040D7E840FF0500000031393A303000000000E066E8
            40FF0500000032303A303000000000802BE840FF0500000032313A3030000000
            00802BE840FF0500000032323A303000000000C0C8E740FF0500000032333A30
            3000000000C0C8E740FF0500000030303A303000000000C0C8E740FF05000000
            30313A303000000000C0C8E740FF0500000030323A303000000000C081E740FF
            0500000030333A303000000000C081E740FF0500000030343A303000000000C0
            81E740FF0500000030353A303000000000C081E740FF0500000030363A303000
            00000080F5E640FF0500000030373A3030000000006074E640FF050000003038
            3A3030}
        end
      end
    end
  end
  object PageControl2: TPageControl
    Left = 0
    Top = 364
    Width = 914
    Height = 328
    ActivePage = TabSheet3
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    object TabSheet3: TTabSheet
      Caption = 'ADBlue heti'
      object Chart3: TChart
        Left = 0
        Top = 0
        Width = 906
        Height = 295
        Gradient.EndColor = 16120835
        Gradient.Visible = True
        Legend.Visible = False
        Title.Text.Strings = (
          'AdBlue szint')
        BottomAxis.LabelsFormat.TextAlignment = taCenter
        DepthAxis.LabelsFormat.TextAlignment = taCenter
        DepthTopAxis.LabelsFormat.TextAlignment = taCenter
        LeftAxis.Automatic = False
        LeftAxis.AutomaticMaximum = False
        LeftAxis.LabelsFormat.TextAlignment = taCenter
        LeftAxis.Maximum = 10000.000000000000000000
        LeftAxis.Title.Caption = #220'zemanyag szint'
        RightAxis.LabelsFormat.TextAlignment = taCenter
        RightAxis.Title.Caption = 'Id'#337
        TopAxis.LabelsFormat.TextAlignment = taCenter
        Zoom.Pen.Mode = pmNotXor
        Align = alClient
        TabOrder = 0
        DefaultCanvas = 'TGDIPlusCanvas'
        ColorPaletteIndex = 13
        object BarSeries3: TBarSeries
          Marks.Visible = True
          Marks.Style = smsValue
          PercentFormat = '##0.##,%'
          Title = 'Series1'
          XValues.Name = 'X'
          XValues.Order = loAscending
          YValues.Name = 'Bar'
          YValues.Order = loNone
          Data = {
            0406000000000000000018B540FF060000004665622D303600000000005EBA40
            FF060000004665622D303500000000005EB540FF060000004665622D30340000
            00000006B340FF060000004665622D3033000000000085C140FF060000004665
            622D303200000000006DBF40FF060000004665622D3031}
        end
      end
    end
  end
  object Query1: TADOQuery
    Parameters = <>
    Left = 464
    Top = 8
  end
end
