unit Valvevo;

interface

uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, DBTables, DB, StdCtrls, Buttons, Mask, DBCtrls,
  Printers, ExtCtrls, CheckLst, ADODB;

type
  TValvevoDlg = class(TForm)
    BitElkuld: TBitBtn;
    BitKilep: TBitBtn;
    CheckListBox1: TCheckListBox;
    Memo1: TMemo;
    SpeedButton3: TSpeedButton;
    Query1: TADOQuery;
    procedure BitBtn6Click(Sender: TObject);
    procedure BitElkuldClick(Sender: TObject);
    procedure BitKilepClick(Sender: TObject);
    procedure Tolt(kodok : string);
    procedure FormShow(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
  private
	kodlist	: TStringList;
	  nevlist	: TStringList;
  public
	  kilepes		: boolean;
	  kodkilist   	: TStringList;
	  nevkilist   	: TStringList;
	  kodlista      : string;
	  nevlista		: string;
  end;

var
  ValvevoDlg: TValvevoDlg;

implementation

uses
	Egyeb, Kozos, J_SQL, Vevobe;
{$R *.DFM}

//***************************************
// NEM KELL V�LTOZTATNI!!!
//***************************************

procedure TValvevoDlg.BitBtn6Click(Sender: TObject);
begin
	Close;
end;

procedure TValvevoDlg.BitElkuldClick(Sender: TObject);
var
	i : integer;
begin
	// Itt �ll�tjuk �ssze a visszaadand� stringeket
	kilepes	:= false;
	kodlista	:= '';
	nevlista	:= '';
	kodkilist.Clear;
	nevkilist.Clear;
	for i := 0 to CheckListBox1.Items.Count - 1 do begin
		if CheckListBox1.Checked[i] = true then begin
			kodkilist.Add(kodlist[i]);
			kodlista	:= kodlista + kodlist[i] + ',';
			nevkilist.Add(nevlist[i]);
			nevlista	:= nevlista + nevlist[i] + ',';
		end;
	end;
	if nevlista <> '' then begin
		nevlista	:= copy(nevlista, 1, Length(nevlista)-1);
	end;
	if kodlista <> '' then begin
		kodlista	:= copy(kodlista, 1, Length(kodlista)-1);
	end;
	Close;
end;

procedure TValvevoDlg.BitKilepClick(Sender: TObject);
begin
	kodkilist.Clear;
  kilepes	:= true;
	Close;
end;

procedure TValvevoDlg.FormShow(Sender: TObject);
begin
	CheckListbox1.SetFocus;
end;

procedure TValvevoDlg.FormDestroy(Sender: TObject);
begin
	kodlist.Free;
  kodkilist.Free;
  nevlist.Free;
  nevkilist.Free;
end;

procedure TValvevoDlg.FormCreate(Sender: TObject);
begin
  kodlist		:= TStringList.Create;
  nevlist		:= TStringList.Create;
  kodkilist	:= TStringList.Create;
  nevkilist	:= TStringList.Create;
end;

//***************************************
// EZT KELL MEGV�LTOZTATNI!!!
//***************************************

procedure TValvevoDlg.Tolt(kodok : string);
var
	i : integer;
begin
	kilepes	:= false;
	CheckListBox1.Clear;
  // A tanfolyamok beolvas�sa
	EgyebDlg.SetADOQueryDatabase(Query1);
  Query_Run(Query1, 'SELECT * FROM VEVO WHERE V_NEV <> '''' ORDER BY V_NEV ');
  if Query1.RecordCount < 1 then begin
  	Exit;
  end;
  // Az elemek beolvas�sa
  Query1.First;
  while not Query1.EOF do begin
    	CheckListBox1.Items.Add(
//     	copy(Query1.FieldByName('V_KOD').AsString+URESSTRING,1,6) +
     	copy(Query1.FieldByName('V_NEV').AsString+URESSTRING,1,40)
        );
    	nevlist.Add( Query1.FieldByName('V_NEV').AsString);
     kodlist.Add(Query1.FieldByName('V_KOD').AsString);
     Query1.Next;
  end;
  Query1.Close;
  CheckListBox1.ItemIndex	:= 0;
  if kodok <> '' then begin
  	// A k�dokban tal�lhat� elemek kijel�l�se
     for i := 0 to CheckListBox1.Items.Count - 1 do begin
     	if Pos(','+kodlist[i]+',',','+kodok+',') >  0 then begin
        	CheckListBox1.Checked[i] := true;
        end;
     end;
  end;
end;

procedure TValvevoDlg.SpeedButton3Click(Sender: TObject);
begin
	// A dolgoz� adatainak m�dos�t�sa
	Application.CreateForm(TVevobeDlg, VevobeDlg);
	VevobeDlg.Tolto('Vev� m�dos�t�sa',kodlist[CheckListbox1.ItemIndex]);
	VevobeDlg.ShowModal;
  if VevobeDlg.ret_kod <> '' then begin
  	// Az adatok friss�t�se
  	Query_Run(Query1, 'SELECT * FROM VEVO WHERE V_KOD = '''+kodlist[CheckListbox1.ItemIndex]+''' ');
		CheckListBox1.Items[CheckListbox1.ItemIndex] :=
//    		copy(Query1.FieldByName('V_KOD').AsString+URESSTRING,1,6) +
     	copy(Query1.FieldByName('V_NEV').AsString+URESSTRING,1,40
        );
  end;
	VevobeDlg.Destroy;
end;

end.


