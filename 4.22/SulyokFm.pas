unit SulyokFm;

interface

uses
	SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
	StdCtrls, Forms, ExtCtrls, Dialogs, J_FOFORMUJ;

type
	TSulyokFmDlg = class(TJ_FOFORMUJDLG)
	  procedure FormCreate(Sender: TObject);override;
	 procedure Adatlap(cim, kod : string);override;
	end;

var
	SulyokFmDlg : TSulyokFmDlg;

implementation

uses
	Egyeb, J_SQL, Sulybe;

{$R *.DFM}

procedure TSulyokFmDlg.FormCreate(Sender: TObject);
begin
	Inherited FormCreate(Sender);
	FelTolto('�j s�ly adat felvitele ', 'S�ly adat m�dos�t�sa ',
			'S�ly adatok list�ja', 'SU_SUKOD', 'Select * from SULYOK ','SULYOK', QUERY_ADAT_TAG );
	width	:= DLG_WIDTH;
	height	:= DLG_HEIGHT;
end;

procedure TSulyokFmDlg.Adatlap(cim, kod : string);
begin
	Application.CreateForm(TSulybeDlg, AlForm);
	Inherited Adatlap(cim, kod );
end;

end.

