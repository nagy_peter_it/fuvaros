object FTerkep2: TFTerkep2
  Left = 597
  Top = 316
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = 'Google Maps'
  ClientHeight = 586
  ClientWidth = 1199
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poOwnerFormCenter
  WindowState = wsMaximized
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Button1: TButton
    Left = -100
    Top = 64
    Width = 75
    Height = 25
    Cancel = True
    Caption = 'Kil'#233'p'#233's'
    TabOrder = 0
    OnClick = Button1Click
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 1199
    Height = 32
    Align = alTop
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    object Label1: TLabel
      Left = 8
      Top = 6
      Width = 53
      Height = 16
      Caption = 'Id'#337'pont'
      FocusControl = DBEdit1
    end
    object Label2: TLabel
      Left = 240
      Top = 6
      Width = 72
      Height = 16
      Caption = 'Sebess'#233'g'
      FocusControl = DBEdit2
    end
    object Label3: TLabel
      Left = 521
      Top = 6
      Width = 33
      Height = 16
      Caption = 'Hely'
      FocusControl = DBEdit3
    end
    object DBText1: TDBText
      Left = 984
      Top = 1
      Width = 121
      Height = 29
      DataField = 'PO_RENSZ'
      DataSource = FTerkep.DataSource1
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -27
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object DBEdit1: TDBEdit
      Left = 72
      Top = 4
      Width = 153
      Height = 24
      DataField = 'PO_DAT'
      DataSource = FTerkep.DataSource1
      TabOrder = 0
    end
    object DBEdit2: TDBEdit
      Left = 320
      Top = 4
      Width = 73
      Height = 24
      DataField = 'PO_SEBES'
      DataSource = FTerkep.DataSource1
      TabOrder = 1
    end
    object DBEdit3: TDBEdit
      Left = 560
      Top = 4
      Width = 417
      Height = 24
      DataField = 'PO_GEKOD'
      DataSource = FTerkep.DataSource1
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
    end
    object DBEdit4: TDBEdit
      Left = 392
      Top = 4
      Width = 105
      Height = 24
      DataField = 'PO_GYUJTAS'
      DataSource = FTerkep.DataSource1
      TabOrder = 3
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 32
    Width = 1199
    Height = 32
    Align = alTop
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 2
    object Label4: TLabel
      Left = 506
      Top = 5
      Width = 49
      Height = 16
      Caption = 'Lerak'#243
      FocusControl = DBEdit3
    end
    object Label5: TLabel
      Left = 8
      Top = 5
      Width = 54
      Height = 16
      Caption = 'Felrak'#243
      FocusControl = DBEdit3
    end
    object Label6: TLabel
      Left = 984
      Top = 7
      Width = 57
      Height = 20
      Caption = 'Label6'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Edit2: TEdit
      Left = 560
      Top = 3
      Width = 417
      Height = 24
      TabOrder = 0
      Text = 'Edit1'
    end
    object Edit1: TEdit
      Left = 72
      Top = 3
      Width = 425
      Height = 24
      TabOrder = 1
      Text = 'Edit1'
    end
  end
  object Chromium1: TChromium
    Left = 0
    Top = 64
    Width = 1199
    Height = 507
    Color = clWhite
    Align = alClient
    DefaultUrl = 'about:blank'
    TabOrder = 3
    OnConsoleMessage = Chromium1ConsoleMessage
    ExplicitTop = 65
  end
  object pnlStatus: TPanel
    Left = 0
    Top = 571
    Width = 1199
    Height = 15
    Align = alBottom
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -8
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 4
  end
end
