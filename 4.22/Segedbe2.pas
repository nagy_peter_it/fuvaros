unit Segedbe2;

interface

uses
	SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls, DateUtils,
	Forms, Dialogs, StdCtrls, Buttons, Mask, DB, DBTables, Egyeb, Kozos, J_SQL,
	ADODB, ComCtrls, ExtCtrls, TabNotBk, MegbizasujFm, Grids, DBGrids, DBCtrls;

const
	FELRAKO_TIPUS_KOD	= 1;
  FELRAKO_TIPUS_NEV	= 2;

type                                          
	TSegedbe2Dlg = class(TForm)
	BitElkuld: TBitBtn;
	BitKilep: TBitBtn;
	Label1: TLabel;
	Label6: TLabel;
	Label7: TLabel;
    M1: TMaskEdit;
    M5: TMaskEdit;
    M100: TMaskEdit;
    M50: TMaskEdit;
	 Label4: TLabel;
	 BitBtn10: TBitBtn;
	 BitBtn1: TBitBtn;
	 Query1: TADOQuery;
	 M6: TMaskEdit;
	 M60: TMaskEdit;
	 Label9: TLabel;
	 Label11: TLabel;
	 M7: TMaskEdit;
    M70: TMaskEdit;
    BitBtn2: TBitBtn;
    BitBtn3: TBitBtn;
    M8: TMaskEdit;
    M80: TMaskEdit;
    Label10: TLabel;
    M9: TMaskEdit;
    Label12: TLabel;
    M16: TMaskEdit;
    Label16: TLabel;
    Label15: TLabel;
    M160: TMaskEdit;
    M90: TMaskEdit;
    Label18: TLabel;
    QueryKoz: TADOQuery;
	 GroupBox1: TGroupBox;
	 CB1: TComboBox;
    BitBtn4: TBitBtn;
    CB11: TComboBox;
	 M2A: TMaskEdit;
    M3: TMaskEdit;
    BitBtn18: TBitBtn;
    M4: TMaskEdit;
	 M11: TMaskEdit;
	 M12: TMaskEdit;
	 M13: TMaskEdit;
	 M14: TMaskEdit;
	 M15: TMaskEdit;
	 Label2: TLabel;
    Label19: TLabel;
	 Label20: TLabel;
    Label21: TLabel;
    Label22: TLabel;
    GroupBox2: TGroupBox;
    CB2: TComboBox;
	 BitBtn5: TBitBtn;
    CB21: TComboBox;
    M20A: TMaskEdit;
    M30: TMaskEdit;
    BitBtn6: TBitBtn;
    M40: TMaskEdit;
	 M110: TMaskEdit;
    M120: TMaskEdit;
	 M130: TMaskEdit;
    M140: TMaskEdit;
    M150: TMaskEdit;
    Label23: TLabel;
	 Label24: TLabel;
    Label25: TLabel;
    Label26: TLabel;
	 Label27: TLabel;
    GroupBox3: TGroupBox;
    Label28: TLabel;
    Label29: TLabel;
    Label30: TLabel;
	 Label31: TLabel;
	 Label32: TLabel;
	 CB3: TComboBox;
	 BitBtn7: TBitBtn;
	 CB31: TComboBox;
    M200A: TMaskEdit;
    M300: TMaskEdit;
    BitBtn8: TBitBtn;
    M400: TMaskEdit;
	 M310: TMaskEdit;
    M320: TMaskEdit;
    M330: TMaskEdit;
	 M340: TMaskEdit;
    M350: TMaskEdit;
    RadioGroup1: TRadioGroup;
	 GroupBox4: TGroupBox;
    Label3: TLabel;
    Label33: TLabel;
    Label34: TLabel;
    Label35: TLabel;
	 Label36: TLabel;
    CB4: TComboBox;
	 BitBtn9: TBitBtn;
    CB41: TComboBox;
    M2000A: TMaskEdit;
    M3000: TMaskEdit;
    BitBtn11: TBitBtn;
    M4000: TMaskEdit;
	 M3100: TMaskEdit;
	 M3200: TMaskEdit;
	 M3300: TMaskEdit;
    M3400: TMaskEdit;
	 M3500: TMaskEdit;
    GroupBox5: TGroupBox;
    Label46: TLabel;
	 Label47: TLabel;
	 Label48: TLabel;
    Label50: TLabel;
	 M31: TMaskEdit;
    BitBtn13: TBitBtn;
    M32: TMaskEdit;
    BitBtn20: TBitBtn;
	 M33: TMaskEdit;
    M34: TMaskEdit;
    BitBtn26: TBitBtn;
	 BitBtn28: TBitBtn;
    M35: TMaskEdit;
    M36: TMaskEdit;
	 BitBtn31: TBitBtn;
	 BitBtn32: TBitBtn;
    CBGkat: TComboBox;
    GroupBox6: TGroupBox;
	 RadioGroup2: TRadioGroup;
    Label38: TLabel;
    GroupBox7: TGroupBox;
    Label43: TLabel;
	 M61: TMaskEdit;
    BitBtn25: TBitBtn;
    Label44: TLabel;
    M62: TMaskEdit;
    BitBtn27: TBitBtn;
    BitBtn30: TBitBtn;
    BitBtn29: TBitBtn;
	 M64: TMaskEdit;
	 M63: TMaskEdit;
    Label45: TLabel;
    Label37: TLabel;
    MaskEdit1: TMaskEdit;
	 MaskEdit2: TMaskEdit;
	 BitBtn12: TBitBtn;
    BitBtn14: TBitBtn;
    MaskEdit3: TMaskEdit;
    Query2: TADOQuery;
    Label40: TLabel;
    MaskEdit4: TMaskEdit;
	 MaskEdit5: TMaskEdit;
    Label41: TLabel;
    MaskEdit6: TMaskEdit;
    MaskEdit7: TMaskEdit;
    CheckBox1: TCheckBox;
	 BitBtn15: TBitBtn;
    BitBtn16: TBitBtn;
    Label42: TLabel;
    M17: TMaskEdit;
    BitBtn17: TBitBtn;
    M18: TMaskEdit;
    Label51: TLabel;
    M170: TMaskEdit;
	 BitBtn19: TBitBtn;
    M180: TMaskEdit;
    MaskEdit8: TMaskEdit;
    MaskEdit9: TMaskEdit;
    CheckBox3: TCheckBox;
    CheckBox2: TCheckBox;
    Querykoz1: TADOQuery;
    BitBtn21: TBitBtn;
    CheckBox4: TCheckBox;
    CheckBox5: TCheckBox;
    M181: TMaskEdit;
    M1801: TMaskEdit;
    Label54: TLabel;
    Label55: TLabel;
    M5_: TMaskEdit;
    M50_: TMaskEdit;
    BitBtn22: TBitBtn;
    BitBtn23: TBitBtn;
    M6_: TMaskEdit;
    M60_: TMaskEdit;
    M6__: TMaskEdit;
    M60__: TMaskEdit;
    CB11A: TComboBox;
    CB21A: TComboBox;
    CB41A: TComboBox;
    CB31A: TComboBox;
    M15A: TMaskEdit;
    Label60: TLabel;
    Panel1: TPanel;
    Panel2: TPanel;
    DBGrid1: TDBGrid;
    Panel3: TPanel;
    BitBtn24: TBitBtn;
    BitBtn33: TBitBtn;
    TMEGTETEL: TADODataSet;
    DSMEGTETEL: TDataSource;
    TMEGTETELMT_MBKOD: TIntegerField;
    TMEGTETELMT_SORSZ: TIntegerField;
    TMEGTETELMT_FEPAL: TBCDField;
    TMEGTETELMT_FSULY: TBCDField;
    TMEGTETELMT_POZIC: TStringField;
    TMEGTETELMT_LOADID: TStringField;
    TMEGTETELMT_MEGJE: TStringField;
    TMEGTETELMT_ATADVA: TIntegerField;
    DBNavigator1: TDBNavigator;
    Label61: TLabel;
    M171: TMaskEdit;
    BitBtn34: TBitBtn;
    M1701: TMaskEdit;
    BitBtn35: TBitBtn;
    Label8: TLabel;
    Label49: TLabel;
    M5__: TMaskEdit;
    BitBtn36: TBitBtn;
    Label5: TLabel;
    M50__: TMaskEdit;
    BitBtn37: TBitBtn;
    Label13: TLabel;
    Label14: TLabel;
    Panel4: TPanel;
    Label17: TLabel;
    M210: TMaskEdit;
    Label39: TLabel;
    M211: TMaskEdit;
    M212: TMaskEdit;
    Panel5: TPanel;
    Label53: TLabel;
    M214: TMaskEdit;
    Label52: TLabel;
    M213: TMaskEdit;
    M215: TMaskEdit;
    CheckBox9: TCheckBox;
    BitJarat: TBitBtn;
    jaratkod: TMaskEdit;
    jaratszam: TMaskEdit;
    Panel6: TPanel;
    M101: TMaskEdit;
    M102: TMaskEdit;
    Label56: TLabel;
    Label57: TLabel;
    M1010: TMaskEdit;
    M1020: TMaskEdit;
    Label58: TLabel;
    Label59: TLabel;
    Label62: TLabel;
    M16C: TMaskEdit;
    M160C: TMaskEdit;
    M105: TMaskEdit;
    Label63: TLabel;
    Label64: TLabel;
    Ma10: TMaskEdit;
    Label65: TLabel;
    Ma11: TMaskEdit;
    QueryFelrako: TADOQuery;
    M216: TMaskEdit;
    Label66: TLabel;
    M_EKAER: TMaskEdit;
    Label67: TLabel;
    CB_HIOKA: TComboBox;
    Label68: TLabel;
    Button1: TButton;
    Label69: TLabel;
    Label70: TLabel;
	procedure BitKilepClick(Sender: TObject);
	procedure FormCreate(Sender: TObject);
	procedure Tolt(cim : string; kod, sorsz, mskod:string);
	procedure Tolt2(cim : string; kod, sorsz, mskod:string);
	procedure BitElkuldClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure BitBtn10Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
	 procedure M9Exit(Sender: TObject);
    procedure M9KeyPress(Sender: TObject; var Key: Char);
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
	 procedure M6Exit(Sender: TObject);
    procedure BitBtn4Click(Sender: TObject);
    procedure BitBtn18Click(Sender: TObject);
    procedure BitBtn6Click(Sender: TObject);
    procedure BitBtn5Click(Sender: TObject);
	 procedure CB1KeyDown(Sender: TObject; var Key: Word;
	   Shift: TShiftState);
	 procedure CB2KeyDown(Sender: TObject; var Key: Word;
	   Shift: TShiftState);
	 procedure FelrakoBetolto(sorsz, tipus : integer; fko : string);
	 procedure CB1Change(Sender: TObject);
	 procedure CB3KeyDown(Sender: TObject; var Key: Word;
	   Shift: TShiftState);
	 procedure BitBtn7Click(Sender: TObject);
	 procedure BitBtn8Click(Sender: TObject);
	 procedure CB2Change(Sender: TObject);
	 procedure CB3Change(Sender: TObject);
	 procedure CB2Exit(Sender: TObject);
	 procedure M70Exit(Sender: TObject);
	 procedure Jellegszamolo;
	 procedure CB11Change(Sender: TObject);
	 procedure CB21Change(Sender: TObject);
	 procedure CB31Change(Sender: TObject);
	 procedure BitBtn9Click(Sender: TObject);
	 procedure BitBtn11Click(Sender: TObject);
	 procedure CB4KeyDown(Sender: TObject; var Key: Word;
	   Shift: TShiftState);
	 procedure GepkocsiOlvaso(traki, dolk, doln, pot : TMaskEdit);
	 procedure BitBtn25Click(Sender: TObject);
	 procedure BitBtn27Click(Sender: TObject);
	 procedure BitBtn30Click(Sender: TObject);
	 procedure BitBtn29Click(Sender: TObject);
	 procedure Urito;
	 procedure RadioGroup2Click(Sender: TObject);
	 procedure GepkocsiTiltas(flag : boolean);
	 procedure BitBtn13Click(Sender: TObject);
	 procedure BitBtn20Click(Sender: TObject);
	 procedure BitBtn26Click(Sender: TObject);
	 procedure BitBtn28Click(Sender: TObject);
	 procedure BitBtn32Click(Sender: TObject);
	 procedure BitBtn31Click(Sender: TObject);
	 procedure BitBtn12Click(Sender: TObject);
	 procedure BitBtn14Click(Sender: TObject);
	 procedure CB4Change(Sender: TObject);
	 procedure M61Change(Sender: TObject);
	 procedure M31Change(Sender: TObject);
	 procedure BitBtn15Click(Sender: TObject);
	 function  Elkuldes : boolean;
    procedure BitBtn16Click(Sender: TObject);
	 procedure BitBtn17Click(Sender: TObject);
    procedure BitBtn19Click(Sender: TObject);
	 procedure FormDestroy(Sender: TObject);
    procedure M17Exit(Sender: TObject);
	 procedure M18Exit(Sender: TObject);
    procedure M180Exit(Sender: TObject);
    procedure M170Exit(Sender: TObject);
    procedure M2AChange(Sender: TObject);
    procedure M3Change(Sender: TObject);
    procedure M4Change(Sender: TObject);
    procedure M11Change(Sender: TObject);
	 procedure M12Change(Sender: TObject);
    procedure M13Change(Sender: TObject);
    procedure M14Change(Sender: TObject);
    procedure M15Change(Sender: TObject);
    procedure M20AChange(Sender: TObject);
    procedure M30Change(Sender: TObject);
    procedure M40Change(Sender: TObject);
    procedure M110Change(Sender: TObject);
    procedure M120Change(Sender: TObject);
    procedure M130Change(Sender: TObject);
    procedure M140Change(Sender: TObject);
    procedure M150Change(Sender: TObject);
    procedure M17KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure M170KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure M18KeyDown(Sender: TObject; var Key: Word;
	   Shift: TShiftState);
    procedure M180KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure M9KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure M16KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
	 procedure M160KeyDown(Sender: TObject; var Key: Word;
	   Shift: TShiftState);
	 procedure M90KeyDown(Sender: TObject; var Key: Word;
	   Shift: TShiftState);
    procedure BitBtn21Click(Sender: TObject);
    procedure M50Exit(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure BitBtn22Click(Sender: TObject);
    procedure BitBtn23Click(Sender: TObject);
    procedure M181KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure M1801KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CB11AChange(Sender: TObject);
    procedure CB41Change(Sender: TObject);
    procedure CB21AChange(Sender: TObject);
    procedure CB41AChange(Sender: TObject);
    procedure CB31AChange(Sender: TObject);
    procedure BitBtn24Click(Sender: TObject);
    procedure DBGrid1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBGrid1KeyPress(Sender: TObject; var Key: Char);
    procedure Panel1Exit(Sender: TObject);
    procedure BitBtn33Click(Sender: TObject);
    procedure DBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure TMEGTETELAfterInsert(DataSet: TDataSet);
    procedure TMEGTETELAfterOpen(DataSet: TDataSet);
    procedure TMEGTETELAfterPost(DataSet: TDataSet);
    procedure TMEGTETELAfterDelete(DataSet: TDataSet);
    procedure M6__Enter(Sender: TObject);
    procedure M60__Enter(Sender: TObject);
    procedure M181Exit(Sender: TObject);
    procedure M1801Exit(Sender: TObject);
    procedure M6__Exit(Sender: TObject);
    procedure M60__Exit(Sender: TObject);
    procedure M60Exit(Sender: TObject);
    procedure M171Exit(Sender: TObject);
    procedure M1701Exit(Sender: TObject);
    procedure M5__Exit(Sender: TObject);
    procedure M50__Exit(Sender: TObject);
    procedure BitBtn34Click(Sender: TObject);
    procedure BitBtn35Click(Sender: TObject);
    procedure M5Enter(Sender: TObject);
    procedure M50Enter(Sender: TObject);
    procedure BitBtn36Click(Sender: TObject);
    procedure M5Exit(Sender: TObject);
    procedure BitBtn37Click(Sender: TObject);
    procedure M210DblClick(Sender: TObject);
    procedure M214DblClick(Sender: TObject);
    procedure CheckBox9Click(Sender: TObject);
    procedure BitJaratClick(Sender: TObject);
    procedure M101KeyPress(Sender: TObject; var Key: Char);
    procedure M16CExit(Sender: TObject);
    procedure M160CExit(Sender: TObject);
    procedure M216DblClick(Sender: TObject);
    procedure M216Change(Sender: TObject);
    procedure HIOKA_Latszik(IgenVagyNem: boolean);
    procedure RakoCimEllenorzes(cbRakoNeve: TComboBox; Orszag, ZIP, Varos, UtcaHazszam: string;
                      meTovabbi1, meTovabbi2, meTovabbi3, meTovabbi4, meTovabbi5: TMaskEdit);
    procedure CB1Exit(Sender: TObject);
    procedure CB4Exit(Sender: TObject);
    procedure CB3Exit(Sender: TObject);
    function HasonloFelrakoEllenor(CB: TComboBox): boolean;
    procedure Button1Click(Sender: TObject);
	 private
		felrako_kod	: string;
		lerako_kod	: string;
		listaorsz	: TStringList;
		listaorszA: TStringList;
		kellexit	: boolean;
		alvalkod	: string;
    f_igazolt,l_igazolt: boolean;
    f_igazolo,l_igazolo: string;
    ID,ID2,SORSZAM: integer;
    IDFel,IDLe: integer;
    aztip: string;
    EredetiRendszam, EredetiPotszam: string;
	public
		tipusflag	: integer;
		ret_kod  	: string;
		ret_sorsz	: string;
		vevokod		: string;
		voltenter	: boolean;
		listagkat	: TStringList;
		ujmssorsz	: string;
		MegForm		: TMegbizasUjFmDlg;
		kellelore, kellpoz, hutos	: boolean;
	end;

var
	Segedbe2Dlg: TSegedbe2Dlg;
    FTDESC: string;

implementation

uses
	Felrakofm, J_Valaszto, Kozos_Local, GKErvenyesseg,MegbizasBe, Windows,Jelszo,ViszonyBe,
  ComboForm, FelrakoMentes, FelrakoValaszt, UniLista;
{$R *.DFM}

procedure TSegedbe2Dlg.BitKilepClick(Sender: TObject);
begin
	ret_kod 	:= '';
	ret_sorsz	:= '0';
	voltenter	:= false;
	Close;
end;

procedure TSegedbe2Dlg.Button1Click(Sender: TObject);
begin
   M_EKAER.Text:= 'nem EKAER k�teles';
end;

procedure TSegedbe2Dlg.FormCreate(Sender: TObject);
var
	sorsz,obe	: integer;
begin
	obe:=StrToIntDef( EgyebDlg.Read_SZGrid('999','924'),1);
  CB11.Visible:= (obe=0);
  CB21.Visible:= (obe=0);
  CB31.Visible:= (obe=0);
  CB41.Visible:= (obe=0);
  CB11A.Visible:= not CB11.Visible;
  CB21A.Visible:= not CB21.Visible;
  CB31A.Visible:= not CB31.Visible;
  CB41A.Visible:= not CB41.Visible;

	tipusflag		:= 0;
	ret_kod 		:= '';
	ret_sorsz		:= '0';
	voltenter		:= false;
	kellelore		:= false;
	kellexit		:= true;
	ujmssorsz	   	:= '';
	EgyebDlg.SetADOQueryDatabase(Query1);
	EgyebDlg.SetADOQueryDatabase(Query2);
	EgyebDlg.SetADOQueryDatabase(QueryKoz);
	EgyebDlg.SetADOQueryDatabase(QueryFelrako);
	EgyebDlg.SetADOQueryDatabase(QueryKoz1);
	SetMaskEdits([M1, M34, M36, MaskEdit6, MaskEdit7, MaskEdit8, MaskEdit4, MaskEdit5, MaskEdit9, MaskEdit2, M64]);
	// A combo-k felt�lt�se
   if EgyebDlg.MBujtetel then begin
	    Query_Run(QueryFelrako, 'SELECT FF_FELNEV FROM FELRAKO WHERE FF_FELNEV <> '''' ORDER BY FF_FELNEV');
       CB1.Clear;
       CB2.Clear;
       CB3.Clear;
       CB4.Clear;
       QueryFelrako.DisableControls;
       while not QueryFelrako.Eof do begin
           CB1.Items.Add(QueryFelrako.FieldByName('FF_FELNEV').AsString);
           QueryFelrako.Next;
	    end;
       QueryFelrako.EnableControls;
       CB2.Items.Assign(CB1.Items);
       CB3.Items.Assign(CB1.Items);
       CB4.Items.Assign(CB1.Items);
   end;
	listaorsz	:= TStringList.Create;
	listaorszA	:= TStringList.Create;
	SzotarTolt(CB11, '300' , listaorsz, true, true );
	SzotarTolt(CB21, '300' , listaorsz, true, true );
	SzotarTolt(CB31, '300' , listaorsz, true, true );
	SzotarTolt(CB41, '300' , listaorsz, true, true );
	SzotarTolt(CB11A,'300' , listaorszA,false, false );
	SzotarTolt(CB21A,'300' , listaorszA,false, false );
	SzotarTolt(CB31A,'300' , listaorszA,false, false );
	SzotarTolt(CB41A,'300' , listaorszA,false, false );

  DistinctTolt(CB_HIOKA, 'SZOTAR', 'SELECT SZ_MENEV FROM SZOTAR WHERE SZ_FOKOD=''372'' AND SZ_ALKOD BETWEEN ''200'' AND ''299'' '+
  		' AND SZ_ERVNY = 1 ORDER BY SZ_ALKOD', nil, True);
  HIOKA_Latszik(False);  // alaphelyzetben ne l�tsszon

	listagkat	:= TStringList.Create;
	SzotarTolt(CBGkat, '340' , listagkat, false, true );
	for sorsz := 0 to listagkat.Count - 1 do begin
		CBGkat.Items[sorsz]	:= listagkat[sorsz];
	end;
	CBGkat.ItemIndex	:= 0;
	RadioGroup2Click(Sender);
   CheckBox9Click(Self);
end;

procedure TSegedbe2Dlg.Tolt(cim : string; kod, sorsz, mskod:string);
var
  jasza    : string;
  kobkod   : string;
begin
	// A nyom�gombok enged�lyez�se
	BitBtn15.Hide;
	BitBtn16.Hide;
	if kellelore then begin
		BitBtn15.Show;
		BitBtn16.Show;
	end;
	ret_kod 	:= IntToStr(StrToIntDef(kod,0));
	Caption 	:= cim;
	M1.Text		:= sorsz;
	ret_sorsz	:= IntToStr(StrToIntDef(sorsz,0));
	ujmssorsz  	:= '';
	GepkocsiTiltas(true);
	Label38.Caption	:= 'norm�l';
	Label38.Update;
	MaskEdit3.Text	:= '0';
	felrako_kod		:= '';
	lerako_kod		:= '';
	jasza		    := Query_Select('MEGBIZAS', 'MB_MBKOD', IntToStr(StrToIntDef(kod,0)), 'MB_JAKOD');
   // A k�bs�ly haszn�lat�nak kider�t�se
   kobkod              := Query_Select('MEGBIZAS', 'MB_MBKOD', IntToStr(StrToIntDef(kod,0)), 'MB_KOBOS');
   Label64.Visible     := false;
   Label65.Visible     := false;
   Ma10.Visible        := false;
   Ma11.Visible        := false;
   if StrToIntDef(kobkod, 0) > 0 then begin
       // Kellenek a k�bs�lyos adatok
       Label64.Visible     := true;
       Label65.Visible     := true;
       Ma10.Visible        := true;
       Ma11.Visible        := true;
   end;

//  BitElkuld.Enabled:= not ((jasza<>'')and (EllenorzottJarat2( jasza)>0));
// BitElkuld.Enabled:= not ((jasza<>'')and (EllenorzottJarat( jasza)>0)) or EgyebDlg.user_super;
   BitElkuld.Enabled:= not ((jasza<>'')and (EllenorzottJarat( jasza)>0)) or EgyebDlg.user_super
    or  (GetRightTag(598, False) = RG_NORIGHT);  // megb�z�s admin

	alvalkod		:= Query_Select('MEGBIZAS', 'MB_MBKOD', IntToStr(StrToIntDef(kod,0)), 'MB_ALVAL');
	if ret_sorsz <> '0' then begin		{M�dos�t�s}
    if tipusflag= 1 then // azonos felrak�
 		 if Query_Run (Query1, 'SELECT * FROM MEGSEGED WHERE MS_MBKOD = '+IntToStr(StrToIntDef(kod,0))+
		 	' AND MS_SORSZ = '+IntToStr(StrTointDef(M1.Text,0))+ ' AND MS_TIPUS = 0 ',true) then begin
      IDFel:=0;
      if not Query1.Eof then
   			IDFel		:=StrToIntDef( Inttostr( Query1.FieldByName('MS_ID').Value),0);
		 end;
    if tipusflag= -1 then // azonos lerak�
		 if Query_Run (Query1, 'SELECT * FROM MEGSEGED WHERE MS_MBKOD = '+IntToStr(StrToIntDef(kod,0))+
			' AND MS_SORSZ = '+IntToStr(StrTointDef(M1.Text,0))+ ' AND MS_TIPUS = 1 ',true) then begin
      IDLe:=0;
      if not Query1.Eof then
  			IDLe		:=StrToIntDef( Inttostr( Query1.FieldByName('MS_ID').Value),0);
		 end;

		if Query_Run (Query1, 'SELECT * FROM MEGSEGED WHERE MS_MBKOD = '+IntToStr(StrToIntDef(kod,0))+
			' AND MS_SORSZ = '+IntToStr(StrTointDef(M1.Text,0))+ ' AND MS_TIPUS = 0 ',true) then begin
			felrako_kod		:= Query1.FieldByName('MS_MSKOD').AsString;
		end;
		if Query_Run (Query1, 'SELECT * FROM MEGSEGED WHERE MS_MBKOD = '+IntToStr(StrToIntDef(kod,0))+
			' AND MS_SORSZ = '+IntToStr(StrTointDef(M1.Text,0))+ ' AND MS_TIPUS = 1 ',true) then begin
			lerako_kod		:= Query1.FieldByName('MS_MSKOD').AsString;
		end;
		if Query_Run (Query1, 'SELECT * FROM MEGSEGED WHERE MS_MBKOD = '+IntToStr(StrToIntDef(kod,0))+
			' AND MS_SORSZ = '+IntToStr(StrTointDef(M1.Text,0)),true) then begin
			Label38.Caption	:= Query1.FieldByName('MS_FAJTA').AsString;
			Label38.Update;
			MaskEdit3.Text	:= Query1.FieldByName('MS_FAJKO').AsString;
    //  BitJarat.Visible:=Query1.FieldByName('MS_FAJKO').AsInteger<>0;
			if StrToIntDef(Query1.FieldByName('MS_FAJKO').AsString,0) > 0 then begin
				GroupBox6.Hide;
				GepkocsiTiltas(false);
			end;
			CB1.Text	:= Query1.FieldByName('MS_FELNEV').AsString;
			ComboSet (CB11, Query1.FieldByName('MS_FORSZ').AsString, listaorsz);
      CB11.OnChange(self);
			ID 	      := Query1.FieldByName('MS_ID').AsInteger;
			ID2	      := Query1.FieldByName('MS_ID2').AsInteger;
			SORSZAM   := Query1.FieldByName('MS_SORSZ').AsInteger;
			M2A.Text 	:= Query1.FieldByName('MS_FELIR').AsString;
			M3.Text 	:= Query1.FieldByName('MS_FELTEL').AsString;
			M4.Text 	:= Query1.FieldByName('MS_FELCIM').AsString;
			M11.Text	:= Query1.FieldByName('MS_FELSE1').AsString;
			M12.Text	:= Query1.FieldByName('MS_FELSE2').AsString;
			M13.Text	:= Query1.FieldByName('MS_FELSE3').AsString;
			M14.Text	:= Query1.FieldByName('MS_FELSE4').AsString;
			M15.Text	:= Query1.FieldByName('MS_FELSE5').AsString;
			M15A.Text	:= Query1.FieldByName('MS_LOADID').AsString;
			M17.Text	:= Query1.FieldByName('MS_FETED').AsString;
			M171.Text	:= Query1.FieldByName('MS_FETED2').AsString;
			M18.Text	:= Query1.FieldByName('MS_FETEI').AsString;
			M181.Text	:= Query1.FieldByName('MS_FETEI2').AsString;
			M6__.Text	:= Query1.FieldByName('MS_FELIDO2').AsString;
			CB2.Text	:= Query1.FieldByName('MS_LERNEV').AsString;
			ComboSet (CB21, Query1.FieldByName('MS_ORSZA').AsString, listaorsz);
      CB21.OnChange(self);
			M20A.Text	:= Query1.FieldByName('MS_LELIR').AsString;
			M30.Text	:= Query1.FieldByName('MS_LERTEL').AsString;
			M40.Text	:= Query1.FieldByName('MS_LERCIM').AsString;
			M110.Text	:= Query1.FieldByName('MS_LERSE1').AsString;
			M120.Text	:= Query1.FieldByName('MS_LERSE2').AsString;
			M130.Text	:= Query1.FieldByName('MS_LERSE3').AsString;
			M140.Text	:= Query1.FieldByName('MS_LERSE4').AsString;
			M150.Text	:= Query1.FieldByName('MS_LERSE5').AsString;
			M170.Text	:= Query1.FieldByName('MS_LETED').AsString;
			M1701.Text:= Query1.FieldByName('MS_LETED2').AsString;
			M180.Text	:= Query1.FieldByName('MS_LETEI').AsString;
			M1801.Text	:= Query1.FieldByName('MS_LETEI2').AsString;
			M60__.Text	:= Query1.FieldByName('MS_LERIDO2').AsString;
			CB3.Text	:= Query1.FieldByName('MS_TENNEV').AsString;
			ComboSet (CB31, Query1.FieldByName('MS_TENOR').AsString, listaorsz);
      CB31.OnChange(self);
			M200A.Text	:= Query1.FieldByName('MS_TENIR').AsString;
			M300.Text	:= Query1.FieldByName('MS_TENTEL').AsString;
			M400.Text	:= Query1.FieldByName('MS_TENCIM').AsString;
			M310.Text	:= Query1.FieldByName('MS_TENYL1').AsString;
			M320.Text	:= Query1.FieldByName('MS_TENYL2').AsString;
			M330.Text	:= Query1.FieldByName('MS_TENYL3').AsString;
			M340.Text	:= Query1.FieldByName('MS_TENYL4').AsString;
			M350.Text	:= Query1.FieldByName('MS_TENYL5').AsString;

			CB4.Text	:= Query1.FieldByName('MS_TEFNEV').AsString;
			ComboSet (CB41, Query1.FieldByName('MS_TEFOR').AsString, listaorsz);
           CB41.OnChange(self);
			M2000A.Text	:= Query1.FieldByName('MS_TEFIR').AsString;
			M3000.Text	:= Query1.FieldByName('MS_TEFTEL').AsString;
			M4000.Text	:= Query1.FieldByName('MS_TEFCIM').AsString;
			M3100.Text	:= Query1.FieldByName('MS_TEFL1').AsString;
			M3200.Text	:= Query1.FieldByName('MS_TEFL2').AsString;
			M3300.Text	:= Query1.FieldByName('MS_TEFL3').AsString;
			M3400.Text	:= Query1.FieldByName('MS_TEFL4').AsString;
			M3500.Text	:= Query1.FieldByName('MS_TEFL5').AsString;

			M5.Text		:= Query1.FieldByName('MS_FELDAT').AsString;
			M5__.Text	:= Query1.FieldByName('MS_FELDAT2').AsString;
			M6.Text 	:= Query1.FieldByName('MS_FELIDO').AsString;
			M6__.Text	:= Query1.FieldByName('MS_FELIDO2').AsString;
			M5_.Text	:= Query1.FieldByName('MS_FERKDAT').AsString;
			M6_.Text 	:= Query1.FieldByName('MS_FERKIDO').AsString;
			M7.Text 	:= Query1.FieldByName('MS_FETDAT').AsString;
			M8.Text 	:= Query1.FieldByName('MS_FETIDO').AsString;
			M9.Text 	:= Query1.FieldByName('MS_FSULY').AsString;
           Ma10.Text	:= Query1.FieldByName('MS_FEKOB').AsString;
			M16.Text 	:= Query1.FieldByName('MS_FEPAL').AsString;
			M16C.Text   := Query1.FieldByName('MS_FCPAL').AsString;
			M50.Text 	:= Query1.FieldByName('MS_LERDAT').AsString;
			M50__.Text  := Query1.FieldByName('MS_LERDAT2').AsString;
			M60.Text 	:= Query1.FieldByName('MS_LERIDO').AsString;
			M60__.Text  := Query1.FieldByName('MS_LERIDO2').AsString;
			M50_.Text 	:= Query1.FieldByName('MS_LERKDAT').AsString;
			M60_.Text 	:= Query1.FieldByName('MS_LERKIDO').AsString;
			M70.Text 	:= Query1.FieldByName('MS_LETDAT').AsString;
			M80.Text 	:= Query1.FieldByName('MS_LETIDO').AsString;
			M90.Text 	:= Query1.FieldByName('MS_LSULY').AsString;
           Ma11.Text	:= Query1.FieldByName('MS_LEKOB').AsString;
			M160.Text 	:= Query1.FieldByName('MS_LEPAL').AsString;
			M160C.Text 	:= Query1.FieldByName('MS_LCPAL').AsString;
			M100.Text 	:= Query1.FieldByName('MS_FELARU').AsString;
      M_EKAER.Text 	:= Query1.FieldByName('MS_EKAER').AsString;
			M105.Text 	:= Query1.FieldByName('MS_KESES').AsString;
			M101.Text 	:= Query1.FieldByName('MS_HFOK1').AsString;
			M102.Text 	:= Query1.FieldByName('MS_HFOK2').AsString;
			RadioGroup1.ItemIndex	:= StrToIntdef(Query1.FieldByName('MS_EXPOR').AsString,0);
      aztip:=Query1.FieldByName('MS_AZTIP').AsString;

      M210.Text:=Query1.FieldByName('MS_FPAFEL' ).AsString;
      //if  not Query1.FieldByName('MS_FPALER' ).IsNull then
      //  M211.Text:=FloatToStr(abs( Query1.FieldByName('MS_FPALER' ).Value)) ;
      M211.Text:=Query1.FieldByName('MS_FPALER' ).AsString;
      M212.Text:=Query1.FieldByName('MS_FPAMEGJ').AsString;
      f_igazolt:= Query1.FieldByName('MS_FPAIGAZT').AsInteger=1 ;
      f_igazolo:= Query1.FieldByName('MS_FPAIGAZL').AsString ;
      if Query1.FieldByName('MS_FPAIGAZT').AsInteger=1 then
      begin
        M210.Font.Color:=clBlack;
        M211.Font.Color:=clBlack;
        M212.Font.Color:=clBlack;
      end;

      M213.Text:=Query1.FieldByName('MS_LPAFEL' ).AsString;
      //if  not Query1.FieldByName('MS_LPALER' ).IsNull then
      //  M214.Text:=FloatToStr(abs( Query1.FieldByName('MS_LPALER' ).Value)) ;
      M214.Text:=Query1.FieldByName('MS_LPALER' ).AsString;
      M216.Text:=Query1.FieldByName('MS_LEPALHI' ).AsString;
      CB_HIOKA.ItemIndex:= CB_HIOKA.Items.IndexOf(Query1.FieldByName('MS_HIOKA').AsString);
      if (StringSzam(M216.Text)>0) or (CB_HIOKA.ItemIndex>0) then
          HIOKA_Latszik(True)
      else HIOKA_Latszik(False);
      M215.Text:=Query1.FieldByName('MS_LPAMEGJ').AsString;
      l_igazolt:= Query1.FieldByName('MS_LPAIGAZT').AsInteger=1 ;
      l_igazolo:= Query1.FieldByName('MS_LPAIGAZL').AsString ;
      if Query1.FieldByName('MS_LPAIGAZT').AsInteger=1 then
      begin
        M213.Font.Color:=clBlack;
        M214.Font.Color:=clBlack;
        M215.Font.Color:=clBlack;
      end;

			M31.Text	:= Query1.FieldByName('MS_RENSZ').AsString;
//			M31Change(Self);
			M32.Text	:= Query1.FieldByName('MS_POTSZ').AsString;
      EredetiRendszam:= M31.Text;
      EredetiPotszam:= M32.Text;
			M33.Text	:= Query1.FieldByName('MS_DOKOD').AsString;
			M34.Text	:= Query1.FieldByName('MS_SNEV1').AsString;
			M35.Text	:= Query1.FieldByName('MS_DOKO2').AsString;
			M36.Text	:= Query1.FieldByName('MS_SNEV2').AsString;
			ComboSet (CBGkat, Query1.FieldByName('MS_GKKAT').AsString, listagkat);
           CheckBox9.Checked:= Query1.FieldByName('MS_CSPAL').AsInteger=1;
           CheckBox9.OnClick(self);
           CheckBox1.Checked	:= false;
           if StrToIntdef(Query1.FieldByName('MS_RRAKO').AsString,0) > 0 then begin
               CheckBox1.Checked	:= true;
           end;
			// A kiemelt checkboxok be�ll�t�sa
			CheckBox2.Checked	:= false;
			CheckBox3.Checked	:= false;
			CheckBox4.Checked	:= false;
			CheckBox5.Checked	:= false;
			if StrToIntdef(Query1.FieldByName('MS_TIPUS').AsString,0) > 0 then begin
				// lerak�r�l van sz�
				if StrToIntdef(Query1.FieldByName('MS_KIFON').AsString,0) > 0 then begin
					CheckBox3.Checked	:= true;
				end;
				//if StrToIntdef(Query1.FieldByName('MS_LDARU').AsString,0) > 0 then begin
				if StrToIntdef(Query1.FieldByName('MS_DARU').AsString,0) > 0 then begin
					CheckBox4.Checked	:= true;
				end;
				//if StrToIntdef(Query1.FieldByName('MS_FDARU').AsString,0) > 0 then begin
				//	CheckBox5.Checked	:= true;
				//end;
				// Megkeress�k a felrak�j�t �s annak is megn�zz�k a kiemelts�g�t
				Query_Run (Query1, 'SELECT MS_KIFON,MS_DARU FROM MEGSEGED WHERE MS_MBKOD = '+IntToStr(StrToIntDef(kod,0))+
			 //	Query_Run (Query1, 'SELECT MS_KIFON FROM MEGSEGED WHERE MS_MBKOD = '+IntToStr(StrToIntDef(kod,0))+
					' AND MS_SORSZ = '+IntToStr(StrTointDef(M1.Text,0))+ ' AND MS_TIPUS = 0 ',true);
				if Query1.RecordCount > 0 then begin
					if StrToIntdef(Query1.FieldByName('MS_KIFON').AsString,0) > 0 then begin
						CheckBox2.Checked	:= true;
					end;
  				if StrToIntdef(Query1.FieldByName('MS_DARU').AsString,0) > 0 then begin
	  				CheckBox5.Checked	:= true;
		  		end;
				end;
			end else begin
				// felrak�r�l van sz�
				if StrToIntdef(Query1.FieldByName('MS_KIFON').AsString,0) > 0 then begin
					CheckBox2.Checked	:= true;
				end;
				if StrToIntdef(Query1.FieldByName('MS_DARU').AsString,0) > 0 then begin
					CheckBox5.Checked	:= true;
				end;
				// Megkeress�k a lerak�j�t �s annak is megn�zz�k a kiemelts�g�t
				//Query_Run (Query1, 'SELECT MS_KIFON,MS_LDARU,MS_FDARU FROM MEGSEGED WHERE MS_MBKOD = '+IntToStr(StrToIntDef(kod,0))+
				Query_Run (Query1, 'SELECT MS_KIFON,MS_DARU FROM MEGSEGED WHERE MS_MBKOD = '+IntToStr(StrToIntDef(kod,0))+
					' AND MS_SORSZ = '+IntToStr(StrTointDef(M1.Text,0))+ ' AND MS_TIPUS = 1 ',true);
				if Query1.RecordCount > 0 then begin
					if StrToIntdef(Query1.FieldByName('MS_KIFON').AsString,0) > 0 then begin
						CheckBox3.Checked	:= true;
					end;
					//if StrToIntdef(Query1.FieldByName('MS_LDARU').AsString,0) > 0 then begin
					if StrToIntdef(Query1.FieldByName('MS_DARU').AsString,0) > 0 then begin
						CheckBox4.Checked	:= true;
					end;
				end;
			end;
		end;
		// HA �res a fels� tervezett fel/le d�tum, t�lts�k fel az als�val
		if M17.Text = '' then begin
			M17.Text 	:= M5.Text;
			M18.Text    := M6.Text;
		end;
		if M170.Text = '' then begin
			M170.Text 	:= M50.Text;
			M180.Text   := M60.Text;
		end;

	end else begin
		// �j felviteln�l a g�pkocsi adatait felt�ltj�k
		Query_Run (Query1, 'SELECT * FROM MEGBIZAS WHERE MB_MBKOD = '+IntToStr(StrToIntDef(kod,0)),true);
		if Query1.RecordCount > 0 then begin
			M31.Text	:= Query1.FieldByName('MB_RENSZ').AsString;
//			M31Change(Self);
			M32.Text	:= Query1.FieldByName('MB_POTSZ').AsString;
      EredetiRendszam:= M31.Text;
      EredetiPotszam:= M32.Text;
			M33.Text	:= Query1.FieldByName('MB_DOKOD').AsString;
			M34.Text	:= Query1.FieldByName('MB_SNEV1').AsString;
			M35.Text	:= Query1.FieldByName('MB_DOKO2').AsString;
			M36.Text	:= Query1.FieldByName('MB_SNEV2').AsString;
			ComboSet (CBGkat, Query1.FieldByName('MB_GKKAT').AsString, listagkat);
		end;
	end;
	if tipusflag <> 0 then begin
		// Azonos felrak� vagy lerak� -> nem lehet el�h�zni vagy el�rakni
		RadioGroup2.ItemIndex	:= 0;
		RadioGroup2.Enabled		:= false;
	end;
end;

procedure TSegedbe2Dlg.BitElkuldClick(Sender: TObject);
var
  fel_id,le_id, s, UjRendszam:string;
  Siker: boolean;
begin
  if FTDESC<>'' then
  begin
    fel_id:=CB11.Text+'('+copy(M2A.Text,1,2);
    le_id:=CB21.Text+'('+copy(M20A.Text,1,2);
    if (pos(fel_id,FTDESC)=0) or (pos(le_id,FTDESC)=0) then
    begin
      s:=#13+#10+'Fuvart�bla kat: '+FTDESC+#13+#10+'Felrak�: '+CB11.Text+' '+M2A.Text+#13+#10+'Lerak�: '+CB21.Text+' '+M20A.Text+#13+#10;

			if NoticeKi('K�rem ellen�rizze az adatokat!'+#13+#10+s+#13+#10+'Folytatja?', NOT_QUESTION) <> 0 then
//      if MessageBox(0, Pchar('K�rem ellen�rizze az adatokat!'+#13+#10+s+#13+#10+'Folytatja?'), 'Fuvart�bla ellen�rz�s', MB_ICONWARNING or MB_YESNO or MB_DEFBUTTON2)=IDNO then
        exit;
    end;
  end;

  UjRendszam:= '';

  if M31.Text <> EredetiRendszam then
    UjRendszam:= Felsorolashoz(UjRendszam, M31.Text, ',', False);
  if M32.Text <> EredetiPotszam then
    UjRendszam:= Felsorolashoz(UjRendszam, M32.Text, ',', False);
  if UjRendszam <> '' then begin
      EKAERFigyelmeztetes(UjRendszam, IntToStr(StrToIntDef(ret_kod,0)));
      end;

 	Screen.Cursor	:= crHourGlass;
  Siker:= Elkuldes;
  Screen.Cursor	:= crDefault;

	if Siker then begin

    if FGKErvenyesseg.Ervenyesseg(M31.Text,Trunc(StrToDate(M170.Text)-date),PLUSZNAP) then
      FGKErvenyesseg.ShowModal;
    if FGKErvenyesseg.Ervenyesseg(M32.Text,Trunc(StrToDate(M170.Text)-date),PLUSZNAP) then
      FGKErvenyesseg.ShowModal;

		// A g�pkocsi/p�tkocsi statisztik�k friss�t�se
		if M31.Text <> '' then begin
			GepStatTolto(M31.Text);
		end;
		if M32.Text <> '' then begin
			PotStatTolto(M32.Text);
		end;
		if M61.Text <> '' then begin
			GepStatTolto(M61.Text);
		end;
		if M62.Text <> '' then begin
			PotStatTolto(M62.Text);
		end;

    ///////////

    if not TMEGTETEL.Active  then
    begin
     TMEGTETEL.Parameters[0].Value:=StrToInt(ret_kod);
     TMEGTETEL.Parameters[1].Value:=StrToInt(M1.Text);
     TMEGTETEL.Open;
    end;
    if kellpoz and (TMEGTETEL.RecordCount=0) then
    begin
      if MessageBox(0, 'Nincs megadva poz�ci�sz�m!'+#13+#10+'Folytatja akil�p�st a poz�ci�sz�m megad�sa n�lk�lt?', '', MB_ICONWARNING or MB_YESNO or MB_DEFBUTTON2)=IDNO then
          exit;
    end;

		// Ha van �j el�rak�s/lerak�s, akkor megnyitjuk az elemet
		if ujmssorsz <> '' then begin
			RadioGroup2.ItemIndex	:= 0;
			Tolt('Felrak�s/lerak�s m�dos�t�sa', ret_kod, ujmssorsz, '0');
		end else begin
			Close;
		end;
	end;
end;

procedure TSegedbe2Dlg.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
	EgyebDlg.FormReturn(Key);
end;

procedure TSegedbe2Dlg.BitBtn10Click(Sender: TObject);
begin
	EgyebDlg.Calendarbe(M5);
  M5.OnExit(self);
end;

procedure TSegedbe2Dlg.BitBtn1Click(Sender: TObject);
begin
	EgyebDlg.Calendarbe(M50);
  M50.OnExit(self);
end;

procedure TSegedbe2Dlg.M9Exit(Sender: TObject);
begin
	SzamExit(Sender);
end;

procedure TSegedbe2Dlg.M9KeyPress(Sender: TObject; var Key: Char);
begin
	with Sender as TMaskEdit do begin
		Key := EgyebDlg.Jochar(Text,Key,8,2);
	end;
end;

procedure TSegedbe2Dlg.BitBtn2Click(Sender: TObject);
begin
	EgyebDlg.Calendarbe(M7);
end;

procedure TSegedbe2Dlg.BitBtn3Click(Sender: TObject);
begin
	EgyebDlg.Calendarbe(M70);
end;

procedure TSegedbe2Dlg.M6Exit(Sender: TObject);
begin
	IdoExit(Sender, false);
 {   if (M6__.Text<>'')and(M5__.Text+ M6__.Text<M5.Text+ M6.Text)  then // HIBA
    begin
  		NoticeKi('A v�gs� id�pontnak nagyobbnak kell lenni, mint a kezd�nek!');
    end;      }
end;

procedure TSegedbe2Dlg.BitBtn4Click(Sender: TObject);
begin
	// Kiv�lasztjuk a k�v�nt felrak�t / lerak�t
	Application.CreateForm(TFelrakoFmDlg, FelrakoFmDlg);
   Application.ProcessMessages;
	FelrakoFmDlg.valaszt	:= true;
	FelrakoFmDlg.ShowModal;
	if FelrakoFmDlg.ret_vkod <> '' then begin
		FelrakoBetolto(1, FELRAKO_TIPUS_KOD, FelrakoFmDlg.ret_vkod);
	end;
	FelrakoFmDlg.Destroy;
end;

procedure TSegedbe2Dlg.BitBtn18Click(Sender: TObject);
begin
	FelrakoElment(CB1.Text, CB11.Text, M2A.Text, M3.Text, M4.Text, M11.Text, M12.Text, M13.Text, M14.Text, M15.Text, '', False);
end;

procedure TSegedbe2Dlg.BitBtn6Click(Sender: TObject);
begin
	FelrakoElment(CB2.Text, CB21.Text, M20A.Text, M30.Text, M40.Text, M110.Text, M120.Text, M130.Text, M140.Text, M150.Text, '', False);
end;

procedure TSegedbe2Dlg.BitBtn5Click(Sender: TObject);
begin
	Application.CreateForm(TFelrakoFmDlg, FelrakoFmDlg);
  Application.ProcessMessages;
	FelrakoFmDlg.valaszt	:= true;
	FelrakoFmDlg.ShowModal;
	if FelrakoFmDlg.ret_vkod <> '' then begin
		FelrakoBetolto(2, FELRAKO_TIPUS_KOD, FelrakoFmDlg.ret_vkod);
	end;
	FelrakoFmDlg.Destroy;
end;

procedure TSegedbe2Dlg.Tolt2(cim : string; kod, sorsz, mskod:string);
begin
	// A nyom�gombok enged�lyez�se
	BitBtn15.Hide;
	BitBtn16.Hide;
	if kellelore then begin
		BitBtn15.Show;
		BitBtn16.Show;
	end;
	// Ez h�v�dik meg k�rfuvar eset�n
	GepkocsiTiltas(true);
	ret_kod 	:= IntToStr(StrToIntDef(kod,0));
	Caption 	:= cim;
	ujmssorsz  	:= '';
	M1.Text		:= '';
	ret_sorsz	:= '0';
	if sorsz <> '0' then begin		{Felt�ltj�k a mezoket keresztbe! }
	  if Query_Run (Query1, 'SELECT * FROM MEGSEGED WHERE MS_MBKOD = '+IntToStr(StrToIntDef(kod,0))+
		' AND MS_SORSZ = '+IntToStr(StrTointDef(sorsz,0)),true) then begin

			M31.Text 	:= Query1.FieldByName('MS_RENSZ').AsString;
			M32.Text	:= Query1.FieldByName('MS_POTSZ').AsString;
			M33.Text	:= Query1.FieldByName('MS_DOKOD').AsString;
			M34.Text	:= Query1.FieldByName('MS_SNEV1').AsString;
			M35.Text	:= Query1.FieldByName('MS_DOKO2').AsString;
			M36.Text	:= Query1.FieldByName('MS_SNEV2').AsString;
			ComboSet (CBGkat, Query1.FieldByName('MS_GKKAT').AsString, listagkat);


			CB2.Text 	:= Query1.FieldByName('MS_FELNEV').AsString;
			ComboSet (CB21, Query1.FieldByName('MS_FORSZ').AsString, listaorsz);
      CB21.OnChange(self);
			M20A.Text 	:= Query1.FieldByName('MS_FELIR').AsString;
			M30.Text 	:= Query1.FieldByName('MS_FELTEL').AsString;
			M40.Text 	:= Query1.FieldByName('MS_FELCIM').AsString;
			M110.Text	:= Query1.FieldByName('MS_FELSE1').AsString;
			M120.Text	:= Query1.FieldByName('MS_FELSE2').AsString;
			M130.Text	:= Query1.FieldByName('MS_FELSE3').AsString;
			M140.Text	:= Query1.FieldByName('MS_FELSE4').AsString;
			M150.Text	:= Query1.FieldByName('MS_FELSE5').AsString;

			CB1.Text 	:= Query1.FieldByName('MS_LERNEV').AsString;
			ComboSet (CB11, Query1.FieldByName('MS_ORSZA').AsString, listaorsz);
      CB11.OnChange(self);
			M2A.Text 	:= Query1.FieldByName('MS_LELIR').AsString;
			M3.Text		:= Query1.FieldByName('MS_LERTEL').AsString;
			M4.Text		:= Query1.FieldByName('MS_LERCIM').AsString;
			M11.Text	:= Query1.FieldByName('MS_LERSE1').AsString;
			M12.Text	:= Query1.FieldByName('MS_LERSE2').AsString;
			M13.Text	:= Query1.FieldByName('MS_LERSE3').AsString;
			M14.Text	:= Query1.FieldByName('MS_LERSE4').AsString;
			M15.Text	:= Query1.FieldByName('MS_LERSE5').AsString;
			//M15A.Text	:= Query1.FieldByName('MS_LOADID').AsString;
			M17.Text	:= Query1.FieldByName('MS_LETED').AsString;
			M171.Text	:= Query1.FieldByName('MS_LETED2').AsString;

			CB4.Text	:= Query1.FieldByName('MS_TENNEV').AsString;
			ComboSet (CB41, Query1.FieldByName('MS_TENOR').AsString, listaorsz);
      CB41.OnChange(self);
			M2000A.Text	:= Query1.FieldByName('MS_TENIR').AsString;
			M3000.Text	:= Query1.FieldByName('MS_TENTEL').AsString;
			M4000.Text	:= Query1.FieldByName('MS_TENCIM').AsString;
			M3100.Text	:= Query1.FieldByName('MS_TENYL1').AsString;
			M3200.Text	:= Query1.FieldByName('MS_TENYL2').AsString;
			M3300.Text	:= Query1.FieldByName('MS_TENYL3').AsString;
			M3400.Text	:= Query1.FieldByName('MS_TENYL4').AsString;
			M3500.Text	:= Query1.FieldByName('MS_TENYL5').AsString;
			M5.Text	:= Query1.FieldByName('MS_LETED').AsString;
			M5__.Text	:= Query1.FieldByName('MS_LETED2').AsString;
			M7.Text	:= Query1.FieldByName('MS_LETED').AsString;
          {
			CB3.Text	:= Query1.FieldByName('MS_TEFNEV').AsString;
			ComboSet (CB31, Query1.FieldByName('MS_TEFOR').AsString, listaorsz);
			M200A.Text	:= Query1.FieldByName('MS_TEFIR').AsString;
			M300.Text	:= Query1.FieldByName('MS_TEFTEL').AsString;
			M400.Text	:= Query1.FieldByName('MS_TEFCIM').AsString;
			M310.Text	:= Query1.FieldByName('MS_TEFL1').AsString;
			M320.Text	:= Query1.FieldByName('MS_TEFL2').AsString;
			M330.Text	:= Query1.FieldByName('MS_TEFL3').AsString;
			M340.Text	:= Query1.FieldByName('MS_TEFL4').AsString;
			M350.Text	:= Query1.FieldByName('MS_TEFL5').AsString;
           }
      CB3.Text 	:= Query1.FieldByName('MS_FELNEV').AsString;
			ComboSet (CB31, Query1.FieldByName('MS_FORSZ').AsString, listaorsz);
      CB31.OnChange(self);
			M200A.Text 	:= Query1.FieldByName('MS_FELIR').AsString;
			M300.Text 	:= Query1.FieldByName('MS_FELTEL').AsString;
			M400.Text 	:= Query1.FieldByName('MS_FELCIM').AsString;
			M310.Text	:= Query1.FieldByName('MS_FELSE1').AsString;
			M320.Text	:= Query1.FieldByName('MS_FELSE2').AsString;
			M330.Text	:= Query1.FieldByName('MS_FELSE3').AsString;
			M340.Text	:= Query1.FieldByName('MS_FELSE4').AsString;
			M350.Text	:= Query1.FieldByName('MS_FELSE5').AsString;

		end;
	end else begin
		// �j felviteln�l vev�i adatok bet�lt�se
	end;
end;

procedure TSegedbe2Dlg.CB1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
	if Key = VK_RETURN then begin
		FelrakoBetolto(1, FELRAKO_TIPUS_NEV, CB1.Text);
		Application.ProcessMessages;
//		CB2.SetFocus;
		M17.SetFocus;
	end;
end;

procedure TSegedbe2Dlg.CB2KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
	if Key = VK_RETURN then begin
		FelrakoBetolto(2, FELRAKO_TIPUS_NEV, CB2.Text);
		Application.ProcessMessages;
		M170.SetFocus;
	end;
end;

procedure TSegedbe2Dlg.FelrakoBetolto(sorsz, tipus : integer; fko : string);
begin
	// Bet�ltj�k a kiv�lasztott elemet
	case tipus of
		FELRAKO_TIPUS_KOD :
			Query_Run(QueryKoz, 'SELECT * FROM FELRAKO WHERE FF_FEKOD = '+fko);
		FELRAKO_TIPUS_NEV :
			Query_Run(QueryKoz, 'SELECT * FROM FELRAKO WHERE FF_FELNEV = '''+fko+''' ');
	end;
	if QueryKoz.RecordCount > 0 then begin
		case sorsz of
			1 :
				begin
				if tipus = FELRAKO_TIPUS_KOD then begin
					CB1.Text 	:= QueryKoz.FieldByName('FF_FELNEV').AsString;
				end;
				ComboSet (CB11, QueryKoz.FieldByName('FF_ORSZA').AsString, listaorsz);
        CB11.OnChange(self);
				M2A.Text 	:= QueryKoz.FieldByName('FF_FELIR').AsString;
				M3.Text 	:= QueryKoz.FieldByName('FF_FELTEL').AsString;
				M4.Text 	:= QueryKoz.FieldByName('FF_FELCIM').AsString;
				M11.Text 	:= QueryKoz.FieldByName('FF_FELSE1').AsString;
				M12.Text 	:= QueryKoz.FieldByName('FF_FELSE2').AsString;
				M13.Text 	:= QueryKoz.FieldByName('FF_FELSE3').AsString;
				M14.Text 	:= QueryKoz.FieldByName('FF_FELSE4').AsString;
				M15.Text 	:= QueryKoz.FieldByName('FF_FELSE5').AsString;
				// A t�ny adatok t�lt�se
//				CB4.Text	:= CB1.Text;
				CB4.Text	:= QueryKoz.FieldByName('FF_FELNEV').AsString;
				ComboSet (CB41, QueryKoz.FieldByName('FF_ORSZA').AsString, listaorsz);
        CB41.OnChange(self);
				M2000A.Text := QueryKoz.FieldByName('FF_FELIR').AsString;
				M3000.Text 	:= QueryKoz.FieldByName('FF_FELTEL').AsString;
				M4000.Text 	:= QueryKoz.FieldByName('FF_FELCIM').AsString;
				M3100.Text 	:= QueryKoz.FieldByName('FF_FELSE1').AsString;
				M3200.Text 	:= QueryKoz.FieldByName('FF_FELSE2').AsString;
				M3300.Text 	:= QueryKoz.FieldByName('FF_FELSE3').AsString;
				M3400.Text 	:= QueryKoz.FieldByName('FF_FELSE4').AsString;
				M3500.Text 	:= QueryKoz.FieldByName('FF_FELSE5').AsString;
				end;
			2 :
				begin
				if tipus = FELRAKO_TIPUS_KOD then begin
					CB2.Text 	:= QueryKoz.FieldByName('FF_FELNEV').AsString;
				end;
				ComboSet (CB21, QueryKoz.FieldByName('FF_ORSZA').AsString, listaorsz);
        CB21.OnChange(self);
				M20A.Text 	:= QueryKoz.FieldByName('FF_FELIR').AsString;
				M30.Text 	:= QueryKoz.FieldByName('FF_FELTEL').AsString;
				M40.Text 	:= QueryKoz.FieldByName('FF_FELCIM').AsString;
				M110.Text 	:= QueryKoz.FieldByName('FF_FELSE1').AsString;
				M120.Text 	:= QueryKoz.FieldByName('FF_FELSE2').AsString;
				M130.Text 	:= QueryKoz.FieldByName('FF_FELSE3').AsString;
				M140.Text 	:= QueryKoz.FieldByName('FF_FELSE4').AsString;
				M150.Text 	:= QueryKoz.FieldByName('FF_FELSE5').AsString;
				// A t�ny lerak� t�lt�se
//				CB3.Text		:= CB2.Text;
				CB3.Text		:= QueryKoz.FieldByName('FF_FELNEV').AsString;
				ComboSet (CB31, QueryKoz.FieldByName('FF_ORSZA').AsString, listaorsz);
        CB31.OnChange(self);
				M200A.Text 	:= QueryKoz.FieldByName('FF_FELIR').AsString;
				M300.Text 	:= QueryKoz.FieldByName('FF_FELTEL').AsString;
				M400.Text 	:= QueryKoz.FieldByName('FF_FELCIM').AsString;
				M310.Text 	:= QueryKoz.FieldByName('FF_FELSE1').AsString;
				M320.Text 	:= QueryKoz.FieldByName('FF_FELSE2').AsString;
				M330.Text 	:= QueryKoz.FieldByName('FF_FELSE3').AsString;
				M340.Text 	:= QueryKoz.FieldByName('FF_FELSE4').AsString;
				M350.Text 	:= QueryKoz.FieldByName('FF_FELSE5').AsString;
				end;
			3 : // A t�ny lerak� bet�lt�se
				begin
				if tipus = FELRAKO_TIPUS_KOD then begin
					CB3.Text 	:= QueryKoz.FieldByName('FF_FELNEV').AsString;
				end;
				ComboSet (CB31, QueryKoz.FieldByName('FF_ORSZA').AsString, listaorsz);
        CB31.OnChange(self);
				M200A.Text 	:= QueryKoz.FieldByName('FF_FELIR').AsString;
				M300.Text 	:= QueryKoz.FieldByName('FF_FELTEL').AsString;
				M400.Text 	:= QueryKoz.FieldByName('FF_FELCIM').AsString;
				M310.Text 	:= QueryKoz.FieldByName('FF_FELSE1').AsString;
				M320.Text 	:= QueryKoz.FieldByName('FF_FELSE2').AsString;
				M330.Text 	:= QueryKoz.FieldByName('FF_FELSE3').AsString;
				M340.Text 	:= QueryKoz.FieldByName('FF_FELSE4').AsString;
				M350.Text 	:= QueryKoz.FieldByName('FF_FELSE5').AsString;
				end;
			4 : // A t�ny fel/lerak� bet�lt�se
				begin
				if tipus = FELRAKO_TIPUS_KOD then begin
					CB4.Text 	:= QueryKoz.FieldByName('FF_FELNEV').AsString;
				end;
				ComboSet (CB41, Querykoz.FieldByName('FF_ORSZA').AsString, listaorsz);
        CB41.OnChange(self);
				M2000A.Text := QueryKoz.FieldByName('FF_FELIR').AsString;
				M3000.Text 	:= QueryKoz.FieldByName('FF_FELTEL').AsString;
				M4000.Text 	:= QueryKoz.FieldByName('FF_FELCIM').AsString;
				M3100.Text 	:= QueryKoz.FieldByName('FF_FELSE1').AsString;
				M3200.Text 	:= QueryKoz.FieldByName('FF_FELSE2').AsString;
				M3300.Text 	:= QueryKoz.FieldByName('FF_FELSE3').AsString;
				M3400.Text 	:= QueryKoz.FieldByName('FF_FELSE4').AsString;
				M3500.Text 	:= QueryKoz.FieldByName('FF_FELSE5').AsString;
				end;
		end;
	end else begin
		// NEm tal�ltuk meg a rekordot, ki kell �r�teni a mez�ket
		case sorsz of
			1 :
				begin
				if tipus = FELRAKO_TIPUS_KOD then begin
					CB1.Text 	:= '';
				end;
				CB11.ItemIndex	:= 0;
				CB11A.ItemIndex	:= 0;
				M2A.Text 	:= '';
				M3.Text 	:= '';
				M4.Text 	:= '';
				M11.Text 	:= '';
				M12.Text 	:= '';
				M13.Text 	:= '';
				M14.Text 	:= '';
				M15.Text 	:= '';
				M15A.Text 	:= '';
				// A t�ny adatok t�lt�se
				CB4.Text	:= CB1.Text;
				CB41.ItemIndex	:= 0;
				CB41A.ItemIndex	:= 0;
				M2000A.Text := '';
				M3000.Text 	:= '';
				M4000.Text 	:= '';
				M3100.Text 	:= '';
				M3200.Text 	:= '';
				M3300.Text 	:= '';
				M3400.Text 	:= '';
				M3500.Text 	:= '';
				end;
			2 :
				begin
				if tipus = FELRAKO_TIPUS_KOD then begin
					CB2.Text 	:= '';
				end;
				CB21.ItemIndex	:= 0;
				CB21A.ItemIndex	:= 0;
				M20A.Text 	:= '';
				M30.Text 	:= '';
				M40.Text 	:= '';
				M110.Text 	:= '';
				M120.Text 	:= '';
				M130.Text 	:= '';
				M140.Text 	:= '';
				M150.Text 	:= '';
				// A t�ny lerak� t�lt�se
				CB3.Text	:= CB2.Text;
				CB31.ItemIndex	:= 0;
				CB31A.ItemIndex	:= 0;
				M200A.Text 	:= '';
				M300.Text 	:= '';
				M400.Text 	:= '';
				M310.Text 	:= '';
				M320.Text 	:= '';
				M330.Text 	:= '';
				M340.Text 	:= '';
				M350.Text 	:= '';
				end;
			3 : // A t�ny lerak� bet�lt�se
				begin
				if tipus = FELRAKO_TIPUS_KOD then begin
					CB3.Text 	:= '';
				end;
				CB31.ItemIndex	:= 0;
				CB31A.ItemIndex	:= 0;
				M200A.Text 	:= '';
				M300.Text 	:= '';
				M400.Text 	:= '';
				M310.Text 	:= '';
				M320.Text 	:= '';
				M330.Text 	:= '';
				M340.Text 	:= '';
				M350.Text 	:= '';
				end;
			4 : // A t�ny fellerak� bet�lt�se
				begin
				if tipus = FELRAKO_TIPUS_KOD then begin
					CB4.Text 	:= '';
				end;
				CB41.ItemIndex	:= 0;
				CB41A.ItemIndex	:= 0;
				M2000A.Text := '';
				M3000.Text 	:= '';
				M4000.Text 	:= '';
				M3100.Text 	:= '';
				M3200.Text 	:= '';
				M3300.Text 	:= '';
				M3400.Text 	:= '';
				M3500.Text 	:= '';
				end;
		end;
	end;
	// A jelleg be�ll�t�sa
	Jellegszamolo;
end;
procedure TSegedbe2Dlg.CB1Change(Sender: TObject);
begin
	FelrakoBetolto(1, FELRAKO_TIPUS_NEV, CB1.Text);
end;

procedure TSegedbe2Dlg.CB1Exit(Sender: TObject);
begin
   HasonloFelrakoEllenor(Sender as TComboBox);
end;

procedure TSegedbe2Dlg.CB3KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
	if Key = VK_RETURN then begin
		FelrakoBetolto(3, FELRAKO_TIPUS_NEV, CB3.Text);
		Application.ProcessMessages;
		M5.SetFocus;
	end;
end;

procedure TSegedbe2Dlg.BitBtn7Click(Sender: TObject);
begin
	Application.CreateForm(TFelrakoFmDlg, FelrakoFmDlg);
  Application.ProcessMessages;
	FelrakoFmDlg.valaszt	:= true;
  Application.ProcessMessages;
	FelrakoFmDlg.ShowModal;
	if FelrakoFmDlg.ret_vkod <> '' then begin
		FelrakoBetolto(3, FELRAKO_TIPUS_KOD, FelrakoFmDlg.ret_vkod);
	end;
	FelrakoFmDlg.Destroy;
end;

function TSegedbe2Dlg.HasonloFelrakoEllenor(CB: TComboBox): boolean;
var
  FelrakoNeve, FelrakoKod: string;
  APoint: TPoint;
begin
  Result:= True; // ha nem lesz megjelen�tve a form, akkor legyen True a v�lasz
  FelrakoNeve:= CB.Text;
  // if CB.Items.IndexOf(CB.Text) = -1 then begin  // nem list�b�l v�lasztott, hanem be�rt egy nevet (vagy v�letlen�l beleg�pelt)
  FelrakoKod:= Query_Select('FELRAKO', 'FF_FELNEV', FelrakoNeve, 'FF_FEKOD'); // ha l�tezik m�r ilyen n�ven
  if FelrakoKod=''  then begin  // nem list�b�l v�lasztott, hanem be�rt egy nevet (vagy v�letlen�l beleg�pelt)
    Screen.Cursor:= crHourGlass;
    Query_Run(EgyebDlg.Query3, 'exec HasonloFelrako '''+FelrakoNeve+'''', True);
    Screen.Cursor:= crDefault;
    if not EgyebDlg.Query3.Eof then begin
       Result:= False;
       Application.CreateForm(TUniListaDlg, UniListaDlg);
       try
         APoint := CB.ClientToScreen(Point(0, CB.ClientHeight));
         UniListaDlg.Left:= APoint.X;
         UniListaDlg.Top:= APoint.Y;
         UniListaDlg.ListBox1.Items.Clear;
         UniListaDlg.DisplayLabel.Caption:= 'Hasonl� nev� elemeket tal�ltam. Ha itt megtal�lhat� a k�v�nt elem, k�rlek v�laszd ki; ha nem, z�rd be az ablakot.';
         with EgyebDlg.Query3 do begin
            while not Eof do begin
              UniListaDlg.ListBox1.Items.Add(Fields[0].AsString);
              Next;
              end;  // while
            end; // with
         UniListaDlg.ShowModal;
         if (UniListaDlg.ModalResult= mrOK) and (UniListaDlg.ret_string <> '') then begin
            CB.Text:= UniListaDlg.ret_string;
            CB.OnChange(CB); // a c�m bet�lt�se
            Result:= True;  // t�rt�nt kiv�laszt�s
            end;
       finally
         UniListaDlg.Release;
         end;  // try-finally
       end; // if
    end;

end;

procedure TSegedbe2Dlg.BitBtn8Click(Sender: TObject);
begin
	FelrakoElment(CB3.Text, CB31.Text, M200A.Text, M300.Text, M400.Text, M310.Text, M320.Text, M330.Text, M340.Text, M350.Text, '', False);
end;

procedure TSegedbe2Dlg.CB2Change(Sender: TObject);
begin
	CB3.Text := CB2.Text;
	FelrakoBetolto(2, FELRAKO_TIPUS_NEV, CB2.Text);
end;

procedure TSegedbe2Dlg.CB3Change(Sender: TObject);
begin
	FelrakoBetolto(3, FELRAKO_TIPUS_NEV, CB3.Text);
end;

procedure TSegedbe2Dlg.CB3Exit(Sender: TObject);
begin
  HasonloFelrakoEllenor(Sender as TComboBox);
end;

procedure TSegedbe2Dlg.CB2Exit(Sender: TObject);
begin
  if HasonloFelrakoEllenor(Sender as TComboBox) then begin
  	if CB3.Text = '' then begin
  		CB3.Text	:= CB2.Text;
    	end;
    end;
end;

procedure TSegedbe2Dlg.M70Exit(Sender: TObject);
begin
	DatumExit(Sender, false);
end;

procedure TSegedbe2Dlg.Jellegszamolo;
begin
	RadioGroup1.ItemIndex := GetExportKod(CB11.Text, CB21.Text);
end;

procedure TSegedbe2Dlg.CB11Change(Sender: TObject);
var
  orsz: string;
begin
	CB41.ItemIndex	:= CB11.ItemIndex;
  orsz:=Query_Select2( 'SZOTAR','SZ_FOKOD','SZ_ALKOD','300',CB11.Text,'SZ_MENEV')  ;
  CB11A.ItemIndex:= CB11A.Items.IndexOf(orsz);
  CB41A.ItemIndex:=CB11A.ItemIndex;

	Jellegszamolo;
end;

procedure TSegedbe2Dlg.CB21Change(Sender: TObject);
var
  orsz: string;
begin
	CB31.ItemIndex	:= CB21.ItemIndex;
  orsz:=Query_Select2( 'SZOTAR','SZ_FOKOD','SZ_ALKOD','300',CB21.Text,'SZ_MENEV')  ;
  CB21A.ItemIndex:= CB21A.Items.IndexOf(orsz);
  CB31A.ItemIndex:=CB21A.ItemIndex;

	Jellegszamolo;
end;

procedure TSegedbe2Dlg.CB31Change(Sender: TObject);
var
  orsz: string;
begin
  orsz:=Query_Select2( 'SZOTAR','SZ_FOKOD','SZ_ALKOD','300',CB31.Text,'SZ_MENEV')  ;
  CB31A.ItemIndex:= CB31A.Items.IndexOf(orsz);
	Jellegszamolo;
end;

procedure TSegedbe2Dlg.BitBtn9Click(Sender: TObject);
begin
	// Kiv�lasztjuk a k�v�nt felrak�t / lerak�t
	Application.CreateForm(TFelrakoFmDlg, FelrakoFmDlg);
  Application.ProcessMessages;
	FelrakoFmDlg.valaszt	:= true;
	FelrakoFmDlg.ShowModal;
	if FelrakoFmDlg.ret_vkod <> '' then begin
		FelrakoBetolto(4, FELRAKO_TIPUS_KOD, FelrakoFmDlg.ret_vkod);
	end;
	FelrakoFmDlg.Destroy;
end;

procedure TSegedbe2Dlg.BitBtn11Click(Sender: TObject);
begin
	FelrakoElment(CB4.Text, CB41.Text, M2000A.Text, M3000.Text, M4000.Text, M3100.Text, M3200.Text, M3300.Text, M3400.Text, M3500.Text, '', False);
end;

procedure TSegedbe2Dlg.CB4KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
	if Key = VK_RETURN then begin
		FelrakoBetolto(4, FELRAKO_TIPUS_NEV, CB4.Text);
		Application.ProcessMessages;
		M5.SetFocus;
	end;
end;

procedure TSegedbe2Dlg.GepkocsiOlvaso(traki, dolk, doln, pot : TMaskEdit);
var
	potk	: string;
begin
	if traki.Text <> '' then begin
		{A sof�r beolvas�sa}
		Query_Run(QueryKoz,'SELECT * FROM DOLGOZO WHERE DO_RENDSZ = '''+traki.Text+''' AND DO_ARHIV < 1 ',true);
		if QueryKoz.RecordCount > 0 then begin
			// A sof�r adatok felvitele
			dolk.Text	:= Querykoz.FieldByName('DO_KOD').AsString;
			doln.Text	:= Querykoz.FieldByName('DO_NAME').AsString;
		end;

		// A kapcsol�d� p�tkocsik beolvas�sa
		Query_Run(Querykoz,'SELECT * FROM GEPKOCSI WHERE GK_RESZ = '''+traki.Text+''' ',true);
		if Querykoz.RecordCount > 0 then begin
			potk        := Query_Select('GEPKOCSI', 'GK_KOD', Querykoz.FieldByName('GK_POKOD').AsString, 'GK_RESZ');
			if (pot.Text <> '') and (potk<>'') then begin
				if pot.Text <> potk then begin
					if NoticeKi('Figyelem! A g�pkocsihoz tartoz� p�tkocsi nem egyezik meg a jelenlegivel. �t�rjuk a mostanit?', NOT_QUESTION) <> 0 then begin
						Exit;
					end;
    			pot.Text	:= potk;
				end;
			end;
		end;
	end;
end;

procedure TSegedbe2Dlg.BitBtn25Click(Sender: TObject);
begin
	GepkocsiValaszto(M61, GK_TRAKTOR);
	GepkocsiOlvaso(M61, M63, M64, M62);
end;

procedure TSegedbe2Dlg.BitBtn27Click(Sender: TObject);
begin
	GepkocsiValaszto(M62, GK_POTKOCSI);
end;

procedure TSegedbe2Dlg.BitBtn30Click(Sender: TObject);
begin
	M63.Text	:= '';
	M64.Text	:= '';
end;

procedure TSegedbe2Dlg.BitBtn29Click(Sender: TObject);
begin
	DolgozoValaszto(M63, M64);
end;

procedure TSegedbe2Dlg.Urito;
begin
	// A tipusflag-nek megfelel�en kiur�ti �s letiltja a mez�ket
	ret_sorsz		:= '0';
  M1.Text:='';
	case tipusflag of
		1 : 	// Azonos felrak�, csak a lerak�t lehet v�ltoztatni
		begin
			GroupBox1.Enabled	:= false;
			GroupBox4.Enabled	:= false;
			M5.Enabled			:= false;
			M6.Enabled			:= false;
			M6__.Enabled			:= false;
			M5_.Enabled			:= false;
			M5__.Enabled			:= false;
			M6_.Enabled			:= false;
			M7.Enabled			:= false;
			M8.Enabled			:= false;
			M9.Enabled			:= false;
			Ma10.Enabled  		:= false;
			M16.Enabled			:= false;
			M16C.Enabled			:= false;
			M210.Enabled			:= false;
			M211.Enabled			:= false;
			M212.Enabled			:= false;
			BitBtn10.Enabled	:= false;
			BitBtn2.Enabled		:= false;
		end;
		-1 : 	// Azonos lerak�, csak a felrak�t lehet v�ltoztatni
		begin
			GroupBox2.Enabled	:= false;
			GroupBox3.Enabled	:= false;
			M50.Enabled			:= false;
			M60.Enabled			:= false;
			M60__.Enabled			:= false;
			M50__.Enabled			:= false;
			M50_.Enabled			:= false;
			M60_.Enabled			:= false;
			M70.Enabled			:= false;
			M80.Enabled			:= false;
			M90.Enabled			:= false;
           Ma10.Enabled			:= false;
           Ma11.Enabled			:= false;
			M160.Enabled 		:= false;
			M160C.Enabled 		:= false;
			M213.Enabled			:= false;
			M214.Enabled			:= false;
			M215.Enabled			:= false;
      M216.Enabled			:= false;
			BitBtn1.Enabled		:= false;
			BitBtn3.Enabled		:= false;
		end;
	end;
end;

procedure TSegedbe2Dlg.RadioGroup2Click(Sender: TObject);
begin
	Label43.Caption	:= 'El�rak�s :';
	Label43.Update;
	GroupBox7.Show;
  Panel1.Hide;
	if M62.Text = '' then begin
		M62.Text	:= M32.Text;
	end;
	case RadioGroup2.ItemIndex of
		0:
		begin
		GroupBox7.Hide;
    Panel1.Visible:=kellpoz;
		end;
		1:
		begin
		Label43.Caption	:= 'El�rak�s :';
		Label43.Update;
		end;
		2:
		begin
		Label43.Caption	:= 'El�h�z�s :';
		Label43.Update;
		end;
		3:
		begin
		Label43.Caption	:= 'Lerak�s :';
		Label43.Update;
		end;
	end;
end;

procedure TSegedbe2Dlg.GepkocsiTiltas(flag : boolean);
begin
	if flag then begin
		M31.Enabled			:= false;
		M32.Enabled			:= false;
		CBGKat.Enabled		:= false;
		BitBtn13.Enabled	:= false;
		BitBtn20.Enabled	:= false;
		BitBtn26.Enabled	:= false;
		BitBtn28.Enabled	:= false;
		BitBtn31.Enabled	:= false;
		BitBtn32.Enabled	:= false;
	end else begin
		M31.Enabled			:= true;
		M32.Enabled			:= true;
		CBGKat.Enabled		:= true;
		BitBtn13.Enabled	:= true;
		BitBtn20.Enabled	:= true;
		BitBtn26.Enabled	:= true;
		BitBtn28.Enabled	:= true;
		BitBtn31.Enabled	:= true;
		BitBtn32.Enabled	:= true;
	end;
end;
procedure TSegedbe2Dlg.BitBtn13Click(Sender: TObject);
begin
	if alvalkod = '' then begin
		GepkocsiValaszto(M31, GK_TRAKTOR);
		GepkocsiOlvaso(M31, M33, M34, M32);
	end else begin
		// Ha van megadva alv�llalkoz�, akkor az alv��llalkoz�i g�pkocsikb�l v�lasztunk
		AlGepkocsiValaszto(M31, alvalkod, GK_TRAKTOR);
		// A kateg�ria kit�lt�se
		if M31.Text <> '' then begin
			Query_Run(Querykoz1,'SELECT * FROM ALGEPKOCSI WHERE AG_RENSZ = '''+M31.Text+''' ',true);
			if Querykoz1.RecordCount > 0 then begin
				ComboSet (CBGkat, QueryKoz1.FieldByName('AG_GKKAT').AsString, listagkat);
			end;
		end;
	end;
end;

procedure TSegedbe2Dlg.BitBtn20Click(Sender: TObject);
begin
	if alvalkod = '' then begin
		GepkocsiValaszto(M32, GK_POTKOCSI);
	end else begin
		// Ha van megadva alv�llalkoz�, akkor az alv��llalkoz�i g�pkocsikb�l v�lasztunk
		AlGepkocsiValaszto(M32, alvalkod, GK_POTKOCSI);
	end;
end;

procedure TSegedbe2Dlg.BitBtn26Click(Sender: TObject);
begin
	DolgozoValaszto(M33, M34);
end;

procedure TSegedbe2Dlg.BitBtn28Click(Sender: TObject);
begin
	M33.Text	:= '';
	M34.Text	:= '';
end;

procedure TSegedbe2Dlg.BitBtn32Click(Sender: TObject);
begin
	M35.Text	:= '';
	M36.Text	:= '';
end;

procedure TSegedbe2Dlg.BitBtn31Click(Sender: TObject);
begin
	DolgozoValaszto(M35, M36);
end;

procedure TSegedbe2Dlg.BitBtn12Click(Sender: TObject);
begin
	DolgozoValaszto(MaskEdit1, MaskEdit2);
end;

procedure TSegedbe2Dlg.BitBtn14Click(Sender: TObject);
begin
	MaskEdit1.Text	:= '';
	MaskEdit2.Text	:= '';
end;

procedure TSegedbe2Dlg.CB4Change(Sender: TObject);
begin
	FelrakoBetolto(4, FELRAKO_TIPUS_NEV, CB4.Text);
end;

procedure TSegedbe2Dlg.CB4Exit(Sender: TObject);
begin
   HasonloFelrakoEllenor(Sender as TComboBox);
end;

procedure TSegedbe2Dlg.M61Change(Sender: TObject);
var
	pozic 	: RPozicio;
begin
	// A poz�ci� ki�r�sa
	GetLocation(M61.Text, pozic);
	MaskEdit5.Text	:= pozic.location;
	MaskEdit4.Text	:= pozic.datum;
	MaskEdit9.Text	:= 'Seb:'+IntToStr(pozic.sebesseg)+' '+pozic.digit;
end;

procedure TSegedbe2Dlg.M31Change(Sender: TObject);
var
	pozic 	: RPozicio;
begin
	// A poz�ci� ki�r�sa
	GetLocation(M31.Text, pozic);
	MaskEdit7.Text	:= pozic.location;
	MaskEdit6.Text	:= pozic.datum;
	MaskEdit8.Text	:= 'S:'+IntToStr(pozic.sebesseg)+' '+pozic.digit;
end;

procedure TSegedbe2Dlg.BitBtn15Click(Sender: TObject);
var
  Siker: boolean;
begin
	// Elk�ldj�k a jelenlegi rekordot
 	Screen.Cursor	:= crHourGlass;
  Siker:= Elkuldes;
  Screen.Cursor	:= crDefault;
	if not Siker then begin
		Exit;
	end;
	// R�ugrunk a k�vetkez� elemre
	if not Megform.Query1.Eof then begin
		Megform.StatuszTorles;
		Megform.Query1.Next;
		MegForm.StatuszBeiras(Megform.Query1.FieldByName('MB_MBKOD').AsString);
		Tolt('Felrak�s/lerak�s m�dos�t�sa', Megform.Query1.FieldByName('MB_MBKOD').AsString,
			Megform.Query1.FieldByname('MS_SORSZ').AsString, Megform.Query1.FieldByName('MS_MSKOD').AsString);
	end;
end;

function TSegedbe2Dlg.Elkuldes : boolean;
var
	ujkodszam, ujkodszam2	: integer;
	akdat		: string;
	akido		: string;
	statusz		: integer;
	aknev		: string;
	aktel		: string;
	akcim		: string;
	aktor		: string;
	aktir		: string;
	gkkat		: string;
	fajko		: string;
	fajta		: string;
  msid0,msid1:integer;
  erkezes, rakas: string;
begin
	Result	:= false;
	if RadioGroup2.ItemIndex > 0 then begin
		if M61.Text	= '' then begin
			NoticeKi('A rendsz�m kit�lt�se k�telez�!');
			M61.SetFocus;
			Exit;
		end;
	end;
	if not DatumExit(M17, true) then begin
		Exit;
	end;
	if not DatumExit(M170, true) then begin
		Exit;
	end;
	if not Idoexit(M18, true) then begin
		Exit;
	end;
	if not Idoexit(M180, true) then begin
		Exit;
	end;
  // orsz�g megad�sa
	if (CB11.Text='')or(CB21.Text='')or(CB31.Text='')or(CB41.Text='') then begin
			NoticeKi('Az orsz�g kit�lt�se k�telez�!');
			M61.SetFocus;
			Exit;
	end;

	// A t�nyd�tumok felt�lt�se ha nincsenek kit�ltve
	if M5.Text = '' then begin
		M5.Text	:= M17.Text;
	end;
	if M5_.Text = '' then begin
		M5_.Text	:= M17.Text;
	end;
	if M6.Text = '' then begin
		M6.Text	:= M18.Text;
	end;
	if M50.Text = '' then begin
		M50.Text	:= M170.Text;
	end;
	if M50_.Text = '' then begin
		M50_.Text	:= M170.Text;
	end;
	if M60.Text = '' then begin
		M60.Text	:= M180.Text;
	end;
	if M7.Text = '' then begin
		M7.Text	:= M5.Text;
	end;
	if M70.Text = '' then begin     
		M70.Text := M50.Text;
	end;
	if M80.Text <> '' then begin
		// Ellen�rizz�k a t�nyd�tumokat
		if ( M70.Text + ' ' + M80.Text ) > FormatDateTime('yyyy.mm.dd. HH:NN', IncMinute(now, 5)) then begin
			NoticeKi('�rv�nytelen t�ny d�tum, id�pont!');
			M80.SetFocus;
			Exit;
		end;
	end;
	if M60_.Text <> '' then begin
		// Ellen�rizz�k a t�nyd�tumokat
		if ( M50_.Text + ' ' + M60_.Text ) > FormatDateTime('yyyy.mm.dd. HH:NN', IncMinute(now, 5)) then begin
			NoticeKi('�rv�nytelen �rkez�si d�tum, id�pont!');
			M60_.SetFocus;
			Exit;
		end;
	end;
	if M6_.Text <> '' then begin
		// Ellen�rizz�k a t�nyd�tumokat
		if ( M5_.Text + ' ' + M6_.Text ) > FormatDateTime('yyyy.mm.dd. HH:NN', IncMinute(now, 5)) then begin
			NoticeKi('�rv�nytelen �rkez�si d�tum, id�pont!');
			M6_.SetFocus;
			Exit;
		end;
	end;
	if M8.Text <> '' then begin
		// Ellen�rizz�k a t�nyd�tumokat
		if ( M7.Text + ' ' + M8.Text ) > FormatDateTime('yyyy.mm.dd. HH:NN', IncMinute(now, 5)) then begin
			NoticeKi('�rv�nytelen t�nyd�tum, id�pont!');
			M8.SetFocus;
			Exit;
		end;
	end;
	// A tervezett lerak�s nem lehet kisebb mint a felrak�s
	akdat	:= M17.Text + M18.Text;
	if 	akdat < M5.Text + M6.Text then begin
		akdat	:= M5.Text + M6.Text;
	end;
	if ( ( akdat <> '' ) and (akdat > M170.Text + M180.Text ) and (M170.Text + M180.Text <> '') ) then begin
		NoticeKi('A tervezett lerak�s d�tuma nem lehet kisebb mint a felrak�s d�tuma!');
		M170.SetFocus;
		Exit;
	end;
	if ( ( akdat <> '' ) and (akdat > M50.Text + M60.Text ) ) then begin
		NoticeKi('A tervezett lerak�s d�tuma nem lehet kisebb mint a felrak�s d�tuma!');
		M50.SetFocus;
		Exit;
	end;
	// A t�ny lerak�s sem lehet kisebb mint a t�ny felrak�s
	akdat	:= M7.Text + M8.Text;
	if ( ( akdat <> '' ) and (M80.Text <> '' ) and (akdat > M70.Text + M80.Text ) and (M70.Text + M80.Text <> '') ) then begin
		NoticeKi('A t�ny lerak�s d�tuma nem lehet kisebb mint a felrak�s d�tuma!');
		M70.SetFocus;
		Exit;
	end;

  // A t�ny felrak�s nem lehet kisebb mint az �rkez�si id�
	erkezes	:= M5_.Text + M6_.Text;
  rakas :=  M7.Text + M8.Text;
	if (M6_.Text <> '' ) and (M8.Text <> '' ) and ( erkezes <> '' ) and (rakas <> '' ) and (erkezes > rakas ) then begin
		NoticeKi('A t�ny felrak�s id�pontja nem lehet kor�bban mint az �rkez�si id�!');
		M8.SetFocus;
		Exit;
    end; // if

  // A t�ny lerak�s nem lehet kisebb mint az �rkez�si id�
	erkezes	:= M50_.Text + M60_.Text;
  rakas :=  M70.Text + M80.Text;
	if (M60_.Text <> '' ) and (M80.Text <> '' ) and  ( erkezes <> '' ) and (rakas <> '' ) and (erkezes > rakas ) then begin
		NoticeKi('A t�ny lerak�s id�pontja nem lehet kor�bban mint az �rkez�si id�!');
		M8.SetFocus;
		Exit;
    end; // if

  if (M8.Text<>'') then  begin
    if (M6_.Text='') then begin
  		NoticeKi('Az �rkez�si id�t meg kell adni!');
	  	M6_.SetFocus;
		  Exit;
    end;
  end;
  if (M80.Text<>'') then begin
    if (M60_.Text='') then begin
  		NoticeKi('Az �rkez�si id�t meg kell adni!');
	  	M60_.SetFocus;
		  Exit;
    end;
    if (StringSzam(M160.Text)=0)and(StringSzam(M16.Text)<>0) then begin
  		NoticeKi('A palett�t meg kell adni!');
	  	M160.SetFocus;
		  Exit;
    end;
    if (StringSzam(M90.Text)=0)and(StringSzam(M9.Text)<>0) then begin
  		NoticeKi('A s�lyt meg kell adni!');
	  	M90.SetFocus;
		  Exit;
    end;

  end;
  // hiba, hi�ny oka k�telez�, ha nem 0 a hib�s mennyis�g
  if CB_HIOKA.Visible and (trim(CB_HIOKA.Text)='') then begin
		NoticeKi('Hi�ny ok�nak kit�lt�se k�telez�!');
		CB_HIOKA.SetFocus;
		Exit;
  	end;
  // -------- c�m t�rol�sa a c�m sz�t�rba (FELRAKO) ------- //
  if EgyebDlg.GPS_KALKULACIOK_HASZNALATA then begin
    RakoCimEllenorzes(CB1, CB11.Text, M2A.Text, M3.Text, M4.Text, M11, M12, M13, M14, M15);
    RakoCimEllenorzes(CB2, CB21.Text, M20A.Text, M30.Text, M40.Text, M110, M120, M130, M140, M150);
    RakoCimEllenorzes(CB4, CB41.Text, M2000A.Text, M3000.Text, M4000.Text, M3100, M3200, M3300, M3400, M3500);
    RakoCimEllenorzes(CB3, CB31.Text, M200A.Text, M300.Text, M400.Text, M310, M320, M330, M340, M350);
    end;
  // ------------------------------------------------------- //

	akdat	:= '';

	if ret_sorsz = '0' then begin        // �j t�tel
		ujkodszam	:= 1;
		if Query_Run (Query1, 'SELECT MAX(MS_SORSZ) MAXKOD FROM MEGSEGED WHERE MS_MBKOD = '+IntToStr(StrToIntDef(ret_kod,0)),true) then begin
			ujkodszam	:= StrToIntDef(Query1.FieldByName('MAXKOD').AsString,0) + 1;
		end;
		ret_sorsz 	:= IntToStr(ujkodszam);
		ujkodszam 	:= GetNextCode('MEGSEGED', 'MS_MSKOD', 1, 0);
    aztip:='';
    if tipusflag<0 then aztip:='AL';
    if tipusflag>0 then aztip:='AF';
		if tipusflag <= 0 then begin            // azonos Lerak�
			Query_Run ( Query1, 'INSERT INTO MEGSEGED (MS_MSKOD, MS_MBKOD, MS_SORSZ, MS_TIPUS, MS_ID2,MS_AZTIP) VALUES ('
        + IntToStr(ujkodszam) + ',' + IntToStr(StrToIntDef(ret_kod,0)) + ',' + ret_sorsz + ','+ '0'+','+IntToStr(IDLe)+','''+aztip+''''+' )',true);
			felrako_kod	:= IntToStr(ujkodszam);
     // msid0:= Query1.FieldByName('MS_ID').value;
		end;
		if tipusflag >= 0 then begin            // azonos Felrak�
      ujkodszam2 	:= GetNextCode('MEGSEGED', 'MS_MSKOD', 1, 0);
			Query_Run ( Query1, 'INSERT INTO MEGSEGED (MS_MSKOD, MS_MBKOD, MS_SORSZ, MS_TIPUS, MS_ID2,MS_AZTIP) VALUES ('
        + IntToStr(ujkodszam2) + ',' + IntToStr(StrToIntDef(ret_kod,0)) + ',' + ret_sorsz + ','+ '1'+','+IntToStr(IDFel)+','''+aztip+''''+' )',true);
			lerako_kod	:= IntToStr(ujkodszam);
    //  msid1:= Query1.FieldByName('MS_ID').value;
		end;
		M1.Text 	:= ret_sorsz;
	end
  else begin     // m�dos�t�s
    tipusflag:=0;
    if  aztip='AF' then tipusflag:= 1;
    if  aztip='AL' then tipusflag:= -1;
  end;
  ////////////////////////
  // Ellen�rzi hogy a NEM AL,AF sor k�t t�telt tartalmaz e
  if tipusflag=0 then
  begin
    Query_Run (Query1, 'SELECT COUNT(MS_SORSZ) DB FROM MEGSEGED WHERE MS_MBKOD = '+IntToStr(StrToIntDef(ret_kod, 0))+' and MS_SORSZ='+ret_sorsz,true)  ;
    if Query1.FieldByName('DB').value <>2 then
    begin
      NoticeKi('!!!FIGYELEM!!!  A(z) '+ret_kod+' / '+ret_sorsz+' megrendel�s t�tel hi�nyos, vagy hib�s. K�rem �rtes�tse a Rendszergazd�t!');
    end;
  end;
  ///////////////
  // Ellen�rzi hogy az AL,AF sor egy t�telt tartalmaz e
  if tipusflag<>0 then
  begin
    Query_Run (Query1, 'SELECT COUNT(MS_SORSZ) DB FROM MEGSEGED WHERE MS_MBKOD = '+IntToStr(StrToIntDef(ret_kod, 0))+' and MS_SORSZ='+ret_sorsz,true)  ;
    if Query1.FieldByName('DB').value <>1 then
    begin
      NoticeKi('!!!FIGYELEM!!!  A(z) '+ret_kod+' / '+ret_sorsz+' megrendel�s t�tel hib�s (t�bb sort tartalmaz)! K�rem �rtes�tse a Rendszergazd�t!');
    end;
  end;
  ///////////////
	akdat	:= M5.Text;
	akido	:= M6.Text;
	if (M7.Text <> '')and(M8.Text <> '') then begin      // ha a t�ny id� ki van t�ltve, akkor az
		akdat	:= M7.Text;
		akido	:= M8.Text;
		if akido = '' then begin
			akido	:= M6.Text;
		end;
	end;
	aknev		:= CB1.Text;
	aktel		:= M3.Text;
	akcim		:= M4.Text;
	aktor		:= CB11.Text;
	aktir		:= M2A.Text;

	gkkat	:= listagkat[CBGkat.ItemIndex];
	if M31.Text <> '' then begin
		gkkat	:= Query_Select('GEPKOCSI', 'GK_RESZ', M31.Text, 'GK_GKKAT');
	end;
	if gkkat = '' then begin
		gkkat	:= listagkat[CBGkat.ItemIndex];
	end;

	if tipusflag <= 0 then begin
		// A felrak�s t�rol�sa
		Query_Update (Query1, 'MEGSEGED',
		[
		'MS_FELNEV', 	''''+CB1.Text+'''',
		'MS_FELIR', 	''''+M2A.Text+'''',
		'MS_FELTEL', 	''''+M3.Text+'''',
		'MS_FELCIM', 	''''+M4.Text+'''',
		'MS_FELSE1',	''''+M11.Text+'''',
		'MS_FELSE2',	''''+M12.Text+'''',
		'MS_FELSE3',	''''+M13.Text+'''',
		'MS_FELSE4',	''''+M14.Text+'''',
		'MS_FELSE5',	''''+M15.Text+'''',
		'MS_LOADID',	''''+M15A.Text+'''',
		'MS_FETED',		''''+M17.Text+'''',
		'MS_FETED2',		''''+M171.Text+'''',
		'MS_FETEI',		''''+M18.Text+'''',
		'MS_FETEI2',		''''+M181.Text+'''',
		'MS_FELDAT', 	''''+M5.Text+'''',
		'MS_FELDAT2', 	''''+M5__.Text+'''',
		'MS_FELIDO', 	''''+M6.Text+'''',
		'MS_FELIDO2', 	''''+M6__.Text+'''',
		'MS_FERKDAT', 	''''+M5_.Text+'''',
		'MS_FERKIDO', 	''''+M6_.Text+'''',
		'MS_FETDAT', 	''''+M7.Text+'''',
		'MS_FETIDO', 	''''+M8.Text+'''',
		'MS_TEIDO', 	''''+M8.Text+'''',
		'MS_FSULY', 	SqlSzamString(StringSzam(M9.Text),8,2),
		'MS_FEKOB', 	SqlSzamString(StringSzam(Ma10.Text),8,2),
		'MS_FEPAL', 	SqlSzamString(StringSzam(M16.Text),8,2),
		'MS_FCPAL', 	SqlSzamString(StringSzam(M16C.Text),8,2),
		'MS_LERNEV', 	''''+CB2.Text+'''',
		'MS_LELIR', 	''''+M20A.Text+'''',
		'MS_LERTEL', 	''''+M30.Text+'''',
		'MS_LERCIM', 	''''+M40.Text+'''',
		'MS_LERSE1',	''''+M110.Text+'''',
		'MS_LERSE2',	''''+M120.Text+'''',
		'MS_LERSE3',	''''+M130.Text+'''',
		'MS_LERSE4',	''''+M140.Text+'''',
		'MS_LERSE5',	''''+M150.Text+'''',
		'MS_LETED',		''''+M170.Text+'''',
		'MS_LETED2',		''''+M1701.Text+'''',
		'MS_LETEI',		''''+M180.Text+'''',
		'MS_LETEI2',		''''+M1801.Text+'''',
		'MS_TENNEV', 	''''+CB3.Text+'''',
		'MS_TENOR',		''''+CB31.Text+'''',
		'MS_TENIR', 	''''+M200A.Text+'''',
		'MS_TENTEL', 	''''+M300.Text+'''',
		'MS_TENCIM', 	''''+M400.Text+'''',
		'MS_TENYL1',	''''+M310.Text+'''',
		'MS_TENYL2',	''''+M320.Text+'''',
		'MS_TENYL3',	''''+M330.Text+'''',
		'MS_TENYL4',	''''+M340.Text+'''',
		'MS_TENYL5',	''''+M350.Text+'''',

		'MS_TEFNEV', 	''''+CB4.Text+'''',
		'MS_TEFOR',		''''+CB41.Text+'''',
		'MS_TEFIR', 	''''+M2000A.Text+'''',
		'MS_TEFTEL', 	''''+M3000.Text+'''',
		'MS_TEFCIM', 	''''+M4000.Text+'''',
		'MS_TEFL1',		''''+M3100.Text+'''',
		'MS_TEFL2',		''''+M3200.Text+'''',
		'MS_TEFL3',		''''+M3300.Text+'''',
		'MS_TEFL4',		''''+M3400.Text+'''',
		'MS_TEFL5',		''''+M3500.Text+'''',

		'MS_LERDAT', 	''''+M50.Text+'''',
		'MS_LERDAT2', 	''''+M50__.Text+'''',
		'MS_LERIDO2', 	''''+M60__.Text+'''',
		'MS_LERIDO', 	''''+M60.Text+'''',
		'MS_LERKDAT', 	''''+M50_.Text+'''',
		'MS_LERKIDO', 	''''+M60_.Text+'''',
		'MS_LETDAT', 	''''+M70.Text+'''',
		'MS_LETIDO', 	''''+M80.Text+'''',
		'MS_FORSZ',		''''+CB11.Text+'''',
		'MS_ORSZA',		''''+CB21.Text+'''',
		'MS_EXPOR',		IntToStr(RadioGroup1.ItemIndex),
		'MS_EXSTR', 	''''+GetExportStr(RadioGroup1.ItemIndex)+'''',
		'MS_LSULY', 	SqlSzamString(StringSzam(M90.Text),8,2),
		'MS_LEKOB', 	SqlSzamString(StringSzam(Ma11.Text),8,2),
		'MS_LEPAL', 	SqlSzamString(StringSzam(M160.Text),8,2),
		'MS_LCPAL', 	SqlSzamString(StringSzam(M160C.Text),8,2),
		'MS_DATUM',     ''''+akdat+'''',
		'MS_IDOPT',     ''''+akido+'''',
		'MS_TINEV', 	''''+EgyebDlg.felrakas_str+'''',
		'MS_AKTNEV', 	''''+aknev+'''',
		'MS_AKTTEL', 	''''+aktel+'''',
		'MS_AKTCIM', 	''''+akcim+'''',
		'MS_AKTOR', 	''''+aktor+'''',
		'MS_AKTIR', 	''''+aktir+'''',
		'MS_REGIK',		'0',

    'MS_FPAFEL',  SqlSzamString(StringSzam(M210.Text),3,0),
    'MS_FPALER',  SqlSzamString(StringSzam(M211.Text),3,0),
		'MS_FPAMEGJ',	''''+M212.Text+'''',
		'MS_FPAIGAZT',BoolToStr01(f_igazolt),
		'MS_FPAIGAZL',	''''+f_igazolo+'''',
    'MS_LPAFEL',  SqlSzamString(StringSzam(M213.Text),3,0),
    'MS_LPALER',  SqlSzamString(StringSzam(M214.Text),3,0),
    'MS_LEPALHI',  SqlSzamString(StringSzam(M216.Text),3,0),
    'MS_HIOKA',   ''''+CB_HIOKA.Text+'''',
		'MS_LPAMEGJ',	''''+M215.Text+'''',
		'MS_LPAIGAZT',BoolToStr01(l_igazolt),
		'MS_LPAIGAZL',	''''+l_igazolo+'''',

		'MS_RENSZ', 	''''+M31.Text+'''',
		'MS_POTSZ', 	''''+M32.Text+'''',
		'MS_DOKOD', 	''''+M33.Text+'''',
		'MS_DOKO2', 	''''+M35.Text+'''',
		'MS_GKKAT', 	''''+CBGKat.Text+'''',
		'MS_SNEV1', 	''''+M34.Text+'''',
		'MS_SNEV2', 	''''+M36.Text+'''',
		'MS_KIFON',		BoolToStr01(CheckBox2.Checked),
		'MS_DARU',		BoolToStr01(CheckBox5.Checked),
		//'MS_LDARU',		BoolToStr01(CheckBox4.Checked),
		//'MS_FDARU',		BoolToStr01(CheckBox5.Checked),
		'MS_RRAKO',		BoolToStr01(CheckBox1.Checked),
		'MS_CSPAL',		BoolToStr01(CheckBox9.Checked),
		'MS_FAJKO', 	IntToStr(StrToIntDef(MaskEdit3.Text,0)),
		'MS_FAJTA',		''''+Label38.Caption+'''',
		'MS_FELARU', 	''''+M100.Text+'''' ,
		'MS_EKAER', 	''''+M_EKAER.Text+'''' ,
		'MS_KESES', 	''''+M105.Text+'''' ,
		'MS_HFOK1', 	''''+M101.Text+'''' ,
		'MS_HFOK2', 	''''+M102.Text+''''
		],
		' WHERE MS_MBKOD = '+ret_kod+' AND MS_SORSZ = '+ret_sorsz+' AND MS_TIPUS = 0' );
	end;
  Query_Run (Query1, 'SELECT MS_ID FROM MEGSEGED WHERE MS_MBKOD = '+ret_kod+' AND MS_SORSZ = '+ret_sorsz+' AND MS_TIPUS = 0',true);
  if  not Query1.FieldByName('MS_ID').IsNull then
    msid0:= Query1.FieldByName('MS_ID').value;
  ///////////////////////////////////
	Query_Update (Query1, 'MEGSEGED',
		[
		'MS_FELDAT', 	''''+M5.Text+'''',
		'MS_FELDAT2', 	''''+M5__.Text+'''',
		'MS_FELIDO', 	''''+M6.Text+'''',
		'MS_FELIDO2', 	''''+M6__.Text+'''',
		'MS_FERKDAT', 	''''+M5_.Text+'''',
		'MS_FERKIDO', 	''''+M6_.Text+'''',
		'MS_FETDAT', 	''''+M7.Text+'''',
		'MS_FETIDO', 	''''+M8.Text+'''',
		'MS_FSULY', 	SqlSzamString(StringSzam(M9.Text),8,2),
		'MS_FEKOB', 	SqlSzamString(StringSzam(Ma10.Text),8,2),
		'MS_FEPAL', 	SqlSzamString(StringSzam(M16.Text),8,2),
		'MS_FCPAL', 	SqlSzamString(StringSzam(M16C.Text),8,2),
    'MS_FPAFEL',  SqlSzamString(StringSzam(M210.Text),3,0),
    'MS_FPALER',  SqlSzamString(StringSzam(M211.Text),3,0),
		'MS_FPAMEGJ',	''''+M212.Text+''''
		],
		' WHERE MS_AZTIP='''+'AF'''+' AND MS_ID<>'+IntToStr(msid0)+' AND MS_MBKOD = '+ret_kod+' AND MS_ID2 = '+IntToStr(msid0) );

  ///////////////////////////////////
	akdat	:= M50.Text;
	akido	:= M60.Text;
	if (M70.Text <> '')and(M80.Text <> '') then begin         // !!!!!!!!!!!!!
		akdat	:= M70.Text;
		akido	:= M80.Text;
		if akido = '' then begin
			akido	:= M60.Text;
		end;
	end;
	aknev		:= CB2.Text;
	aktel		:= M30.Text;
	akcim		:= M40.Text;
	aktor		:= CB21.Text;
	aktir		:= M20A.Text;
	if tipusflag >= 0 then begin
		// A lerak�s t�rol�sa
		Query_Update (Query1, 'MEGSEGED',
		[
		'MS_FELNEV', 	''''+CB1.Text+'''',
		'MS_FELIR', 	''''+M2A.Text+'''',
		'MS_FELTEL', 	''''+M3.Text+'''',
		'MS_FELCIM', 	''''+M4.Text+'''',
		'MS_FELSE1',	''''+M11.Text+'''',
		'MS_FELSE2',	''''+M12.Text+'''',
		'MS_FELSE3',	''''+M13.Text+'''',
		'MS_FELSE4',	''''+M14.Text+'''',
		'MS_FELSE5',	''''+M15.Text+'''',
		'MS_LOADID',	''''+M15A.Text+'''',
		'MS_FETED',		''''+M17.Text+'''',
		'MS_FETED2',		''''+M171.Text+'''',
		'MS_FETEI',		''''+M18.Text+'''',
		'MS_FETEI2',		''''+M181.Text+'''',
		'MS_FELDAT', 	''''+M5.Text+'''',
		'MS_FELDAT2', 	''''+M5__.Text+'''',
		'MS_FELIDO', 	''''+M6.Text+'''',
		'MS_FELIDO2', 	''''+M6__.Text+'''',
		'MS_FERKDAT', 	''''+M5_.Text+'''',
		'MS_FERKIDO', 	''''+M6_.Text+'''',
		'MS_FETDAT', 	''''+M7.Text+'''',
		'MS_FETIDO', 	''''+M8.Text+'''',
		'MS_TEIDO', 	''''+M80.Text+'''',
		'MS_FSULY', 	SqlSzamString(StringSzam(M9.Text),8,2),
		'MS_FEKOB', 	SqlSzamString(StringSzam(Ma10.Text),8,2),
		'MS_FEPAL', 	SqlSzamString(StringSzam(M16.Text),8,2),
		'MS_FCPAL', 	SqlSzamString(StringSzam(M16C.Text),8,2),
		'MS_LERNEV', 	''''+CB2.Text+'''',
		'MS_LELIR', 	''''+M20A.Text+'''',
		'MS_LERTEL', 	''''+M30.Text+'''',
		'MS_LERCIM', 	''''+M40.Text+'''',
		'MS_LERSE1',	''''+M110.Text+'''',
		'MS_LERSE2',	''''+M120.Text+'''',
		'MS_LERSE3',	''''+M130.Text+'''',
		'MS_LERSE4',	''''+M140.Text+'''',
		'MS_LERSE5',	''''+M150.Text+'''',
		'MS_LETED',		''''+M170.Text+'''',
		'MS_LETED2',		''''+M1701.Text+'''',
		'MS_LETEI',		''''+M180.Text+'''',
		'MS_LETEI2',		''''+M1801.Text+'''',
		'MS_TENNEV', 	''''+CB3.Text+'''',
		'MS_TENOR',		''''+CB31.Text+'''',
		'MS_TENIR', 	''''+M200A.Text+'''',
		'MS_TENTEL', 	''''+M300.Text+'''',
		'MS_TENCIM', 	''''+M400.Text+'''',
		'MS_TENYL1',	''''+M310.Text+'''',
		'MS_TENYL2',	''''+M320.Text+'''',
		'MS_TENYL3',	''''+M330.Text+'''',
		'MS_TENYL4',	''''+M340.Text+'''',
		'MS_TENYL5',	''''+M350.Text+'''',
		'MS_TEFNEV', 	''''+CB4.Text+'''',
		'MS_TEFOR',		''''+CB41.Text+'''',
		'MS_TEFIR', 	''''+M2000A.Text+'''',
		'MS_TEFTEL', 	''''+M3000.Text+'''',
		'MS_TEFCIM', 	''''+M4000.Text+'''',
		'MS_TEFL1',		''''+M3100.Text+'''',
		'MS_TEFL2',		''''+M3200.Text+'''',
		'MS_TEFL3',		''''+M3300.Text+'''',
		'MS_TEFL4',		''''+M3400.Text+'''',
		'MS_TEFL5',		''''+M3500.Text+'''',
		'MS_LERDAT', 	''''+M50.Text+'''',
		'MS_LERDAT2', 	''''+M50__.Text+'''',
		'MS_LERIDO', 	''''+M60.Text+'''',
		'MS_LERIDO2', 	''''+M60__.Text+'''',
		'MS_LERKDAT', 	''''+M50_.Text+'''',
		'MS_LERKIDO', 	''''+M60_.Text+'''',
		'MS_LETDAT', 	''''+M70.Text+'''',
		'MS_LETIDO', 	''''+M80.Text+'''',
		'MS_FORSZ',		''''+CB11.Text+'''',
		'MS_ORSZA',		''''+CB21.Text+'''',
		'MS_EXPOR',		IntToStr(RadioGroup1.ItemIndex),
		'MS_EXSTR', 	''''+GetExportStr(RadioGroup1.ItemIndex)+'''',
		'MS_LSULY', 	SqlSzamString(StringSzam(M90.Text),8,2),
		'MS_LEKOB', 	SqlSzamString(StringSzam(Ma11.Text),8,2),
		'MS_LEPAL', 	SqlSzamString(StringSzam(M160.Text),8,2),
		'MS_LCPAL', 	SqlSzamString(StringSzam(M160C.Text),8,2),
		'MS_DATUM',     ''''+akdat+'''',
		'MS_IDOPT',     ''''+akido+'''',
		'MS_TINEV', 	''''+EgyebDlg.lerakas_str+'''',
		'MS_AKTNEV', 	''''+aknev+'''',
		'MS_AKTTEL', 	''''+aktel+'''',
		'MS_AKTCIM', 	''''+akcim+'''',
		'MS_AKTOR', 	''''+aktor+'''',
		'MS_AKTIR', 	''''+aktir+'''',
		'MS_REGIK',		'0',

    'MS_FPAFEL',  SqlSzamString(StringSzam(M210.Text),3,0),
    'MS_FPALER',  SqlSzamString(StringSzam(M211.Text),3,0),
		'MS_FPAMEGJ',	''''+M212.Text+'''',
		'MS_FPAIGAZT',BoolToStr01(f_igazolt),
		'MS_FPAIGAZL',	''''+f_igazolo+'''',
    'MS_LPAFEL',  SqlSzamString(StringSzam(M213.Text),3,0),
    'MS_LPALER',  SqlSzamString(StringSzam(M214.Text),3,0),
    'MS_LEPALHI',  SqlSzamString(StringSzam(M216.Text),3,0),
    'MS_HIOKA',   ''''+CB_HIOKA.Text+'''',
		'MS_LPAMEGJ',	''''+M215.Text+'''',
		'MS_LPAIGAZT',BoolToStr01(l_igazolt),
		'MS_LPAIGAZL',	''''+l_igazolo+'''',

		'MS_RENSZ', 	''''+M31.Text+'''',
		'MS_POTSZ', 	''''+M32.Text+'''',
		'MS_DOKOD', 	''''+M33.Text+'''',
		'MS_DOKO2', 	''''+M35.Text+'''',
		'MS_GKKAT', 	''''+CBGKat.Text+'''',
		'MS_SNEV1', 	''''+M34.Text+'''',
		'MS_SNEV2', 	''''+M36.Text+'''',
		'MS_KIFON',		BoolToStr01(CheckBox3.Checked),
		'MS_DARU',		BoolToStr01(CheckBox4.Checked),
		//'MS_LDARU',		BoolToStr01(CheckBox4.Checked),
		//'MS_FDARU',		BoolToStr01(CheckBox5.Checked),
		'MS_RRAKO',		BoolToStr01(CheckBox1.Checked),
		'MS_CSPAL',		BoolToStr01(CheckBox9.Checked),
		'MS_FAJKO', 	IntToStr(StrToIntDef(MaskEdit3.Text,0)),
		'MS_FAJTA',		''''+Label38.Caption+'''',
		'MS_FELARU', 	''''+M100.Text+'''',
		'MS_EKAER', 	''''+M_EKAER.Text+'''',
		'MS_KESES', 	''''+M105.Text+'''',
		'MS_HFOK1', 	''''+M101.Text+'''' ,
		'MS_HFOK2', 	''''+M102.Text+''''
		],
		' WHERE MS_MBKOD = '+ret_kod+' AND MS_SORSZ = '+ret_sorsz+' AND MS_TIPUS = 1' );
	end;
  Query_Run (Query1, 'SELECT MS_ID FROM MEGSEGED WHERE MS_MBKOD = '+ret_kod+' AND MS_SORSZ = '+ret_sorsz+' AND MS_TIPUS = 1',true);
  if  not Query1.FieldByName('MS_ID').IsNull then
    msid1:= Query1.FieldByName('MS_ID').value;
  ///////////////////////////////////
	Query_Update (Query1, 'MEGSEGED',
		[
		'MS_LERDAT', 	''''+M50.Text+'''',
		'MS_LERDAT2', 	''''+M50__.Text+'''',
		'MS_LERIDO', 	''''+M60.Text+'''',
		'MS_LERIDO2', 	''''+M60__.Text+'''',
		'MS_LERKDAT', 	''''+M50_.Text+'''',
		'MS_LERKIDO', 	''''+M60_.Text+'''',
		'MS_LETDAT', 	''''+M70.Text+'''',
		'MS_LETIDO', 	''''+M80.Text+'''',
		'MS_LSULY', 	SqlSzamString(StringSzam(M90.Text),8,2),
		'MS_LEKOB', 	SqlSzamString(StringSzam(Ma11.Text),8,2),
		'MS_LEPAL', 	SqlSzamString(StringSzam(M160.Text),8,2),
		'MS_LCPAL', 	SqlSzamString(StringSzam(M160C.Text),8,2),
    'MS_LPAFEL',  SqlSzamString(StringSzam(M213.Text),3,0),
    'MS_LPALER',  SqlSzamString(StringSzam(M214.Text),3,0),
    'MS_LEPALHI',  SqlSzamString(StringSzam(M216.Text),3,0),
    'MS_HIOKA',   ''''+CB_HIOKA.Text+'''',
		'MS_LPAMEGJ',	''''+M215.Text+''''
		],
		' WHERE MS_AZTIP='''+'AL'''+' AND MS_ID<>'+IntToStr(msid1)+' AND MS_MBKOD = '+ret_kod+' AND MS_ID2 = '+IntToStr(msid1) );

  ///////////////////////////////////

   voltenter		:= true;
	// A kieg�sz�t�sek lekezel�se
	if RadioGroup2.ItemIndex > 0 then begin     // NEM norm�l
   Try
		// Lerak� kezel�se
		if RadioGroup2.ItemIndex = 3 then begin
			// Lerak�n�l ellen�rizz�k az eddigi lerak�kat
(*
			Query_Run(Query1, 'SELECT * FROM MEGSEGED WHERE MS_MBKOD = '+ret_kod+' AND MS_FAJKO = 3 ');
			if Query1.RecordCount > 0 then begin
				if NoticeKi('Figyelem! A megb�z�shoz m�r tartozik lerak�! Ha folytatja, az el�z� lerak� t�rl�sre ker�l! Folytatja?', NOT_QUESTION) <> 0 then begin
					Exit;
				end;
				// A l�tez� el�rak� t�rl�se
				Query_Run(Query1, 'DELETE FROM MEGSEGED WHERE MS_MBKOD = '+ret_kod+' AND MS_FAJKO = 3 ');
			end;
*)
			ujkodszam	:= 1;
			if Query_Run (Query1, 'SELECT MAX(MS_SORSZ) MAXKOD FROM MEGSEGED WHERE MS_MBKOD = '+IntToStr(StrToIntDef(ret_kod,0)),true) then begin
				ujkodszam	:= StrToIntDef(Query1.FieldByName('MAXKOD').AsString,0) + 1;
			end;
			ret_sorsz 	:= IntToStr(ujkodszam);
			ujkodszam 	:= GetNextCode('MEGSEGED', 'MS_MSKOD', 1, 0);
			if not Query_Run ( Query1, 'INSERT INTO MEGSEGED (MS_MSKOD, MS_MBKOD, MS_SORSZ, MS_TIPUS,MS_ID2) VALUES (' + IntToStr(ujkodszam) + ',' + IntToStr(StrToIntDef(ret_kod,0)) + ',' + ret_sorsz + ', 0 '+','+IntToStr(msid0)+')',true) then Raise Exception.Create('');
      ujkodszam2 	:= GetNextCode('MEGSEGED', 'MS_MSKOD', 1, 0);
			if not Query_Run ( Query1, 'INSERT INTO MEGSEGED (MS_MSKOD, MS_MBKOD, MS_SORSZ, MS_TIPUS,MS_ID2) VALUES (' + IntToStr(ujkodszam2) + ',' + IntToStr(StrToIntDef(ret_kod,0)) + ',' + ret_sorsz + ', 1 '+','+IntToStr(msid1)+')',true) then Raise Exception.Create('');
			M1.Text 	:= ret_sorsz;

			gkkat	:= listagkat[CBGkat.ItemIndex];
			if M61.Text <> '' then begin
				gkkat	:= Query_Select('GEPKOCSI', 'GK_RESZ', M61.Text, 'GK_GKKAT');
			end;
			if gkkat = '' then begin
				gkkat	:= listagkat[CBGkat.ItemIndex];
			end;

			akdat	:= M50.Text;
			akido	:= M60.Text;
			if (M70.Text <> '')and(M80.Text <> '') then begin    // !!!!!!!!!!!!!!
				akdat	:= M70.Text;
				akido	:= M80.Text;
				if akido = '' then begin
					akido	:= M60.Text;
				end;
			end;
			aknev		:= CB2.Text;
			aktel		:= M30.Text;
			akcim		:= M40.Text;
			aktor		:= CB21.Text;
			aktir		:= M20A.Text;
			// A felrak�s bejegyz�se
			if not Query_Update (Query1, 'MEGSEGED',
				[
				'MS_FELNEV', 	''''+CB1.Text+'''',
				'MS_FELIR', 	''''+M2A.Text+'''',
				'MS_FELTEL', 	''''+M3.Text+'''',
				'MS_FELCIM', 	''''+M4.Text+'''',
				'MS_FELSE1',	''''+M11.Text+'''',
				'MS_FELSE2',	''''+M12.Text+'''',
				'MS_FELSE3',	''''+M13.Text+'''',
				'MS_FELSE4',	''''+M14.Text+'''',
				'MS_FELSE5',	''''+M15.Text+'''',
				'MS_LOADID',	''''+M15A.Text+'''',
				'MS_FELDAT', 	''''+M50.Text+'''',
				'MS_FELDAT2', 	''''+M50__.Text+'''',
				'MS_FELIDO', 	''''+M60.Text+'''',
    		'MS_FELIDO2', 	''''+M60__.Text+'''',
				'MS_FERKDAT', 	''''+M50_.Text+'''',
				'MS_FERKIDO', 	''''+M60_.Text+'''',
				'MS_FETED',		''''+M17.Text+'''',
				'MS_FETED2',		''''+M171.Text+'''',
				'MS_FETEI',		''''+M18.Text+'''',
				'MS_FETEI2',		''''+M181.Text+'''',
				'MS_TEIDO', 	''''+M80.Text+'''',
				'MS_FSULY', 	SqlSzamString(StringSzam(M9.Text),8,2),
				'MS_FEKOB', 	SqlSzamString(StringSzam(Ma10.Text),8,2),
				'MS_FEPAL', 	SqlSzamString(StringSzam(M16.Text),8,2),
				'MS_FCPAL', 	SqlSzamString(StringSzam(M16C.Text),8,2),
				'MS_LERNEV', 	''''+CB2.Text+'''',
				'MS_LELIR', 	''''+M20A.Text+'''',
				'MS_LERTEL', 	''''+M30.Text+'''',
				'MS_LERCIM', 	''''+M40.Text+'''',
				'MS_LERSE1',	''''+M110.Text+'''',
				'MS_LERSE2',	''''+M120.Text+'''',
				'MS_LERSE3',	''''+M130.Text+'''',
				'MS_LERSE4',	''''+M140.Text+'''',
				'MS_LERSE5',	''''+M150.Text+'''',
				'MS_LETED',		''''+M170.Text+'''',
    		'MS_LETED2',		''''+M1701.Text+'''',
				'MS_LETEI',		''''+M180.Text+'''',
				'MS_LETEI2',		''''+M1801.Text+'''',

				// A t�ny lerak�hoz ker�lnek a tervezett lerak� adatai
				'MS_TENNEV', 	''''+CB2.Text+'''',
				'MS_TENOR',		''''+CB21.Text+'''',
				'MS_TENIR', 	''''+M20A.Text+'''',
				'MS_TENTEL', 	''''+M30.Text+'''',
				'MS_TENCIM', 	''''+M40.Text+'''',
				'MS_TENYL1',	''''+M110.Text+'''',
				'MS_TENYL2',	''''+M120.Text+'''',
				'MS_TENYL3',	''''+M130.Text+'''',
				'MS_TENYL4',	''''+M140.Text+'''',
				'MS_TENYL5',	''''+M150.Text+'''',
				'MS_LETDAT', 	''''+M170.Text+'''',
				'MS_LETIDO', 	''''+''+'''',

				// A t�ny felrak�hoz ker�lnek a mostani t�ny lerak� adatai
				'MS_TEFNEV', 	''''+CB3.Text+'''',
				'MS_TEFOR',		''''+CB31.Text+'''',
				'MS_TEFIR', 	''''+M200A.Text+'''',
				'MS_TEFTEL', 	''''+M300.Text+'''',
				'MS_TEFCIM', 	''''+M400.Text+'''',
				'MS_TEFL1',		''''+M310.Text+'''',
				'MS_TEFL2',		''''+M320.Text+'''',
				'MS_TEFL3',		''''+M330.Text+'''',
				'MS_TEFL4',		''''+M340.Text+'''',
				'MS_TEFL5',		''''+M350.Text+'''',
				'MS_FETDAT', 	''''+M70.Text+'''',
//				'MS_FETIDO', 	''''+M80.Text+'''',
				'MS_FETIDO', 	''''+''+'''',

				'MS_LERDAT', 	''''+M170.Text+'''',
				'MS_LERDAT2', 	''''+M1701.Text+'''',
				'MS_LERIDO', 	''''+M180.Text+'''',
    		'MS_LERIDO2', 	''''+M1801.Text+'''',
				'MS_FORSZ',		''''+CB11.Text+'''',
				'MS_ORSZA',		''''+CB21.Text+'''',
				'MS_EXPOR',		IntToStr(RadioGroup1.ItemIndex),
				'MS_EXSTR', 	''''+GetExportStr(RadioGroup1.ItemIndex)+'''',
				'MS_LSULY', 	SqlSzamString(StringSzam(M90.Text),8,2),
				'MS_LEKOB', 	SqlSzamString(StringSzam(Ma11.Text),8,2),
				'MS_LEPAL', 	SqlSzamString(StringSzam(M160.Text),8,2),
				'MS_LCPAL', 	SqlSzamString(StringSzam(M160C.Text),8,2),
				'MS_DATUM',     ''''+akdat+'''',
				'MS_IDOPT',     ''''+akido+'''',
				'MS_TINEV', 	''''+EgyebDlg.felrakas_str+'''',
				'MS_AKTNEV', 	''''+aknev+'''',
				'MS_AKTTEL', 	''''+aktel+'''',
				'MS_AKTCIM', 	''''+akcim+'''',
				'MS_AKTOR', 	''''+aktor+'''',
				'MS_AKTIR', 	''''+aktir+'''',
				'MS_REGIK',		IntToStr(StrToIntDef(lerako_kod,0)),

				'MS_RENSZ', 	''''+M61.Text+'''',
				'MS_POTSZ', 	''''+M62.Text+'''',
				'MS_DOKOD', 	''''+M63.Text+'''',
				'MS_DOKO2', 	''''+MaskEdit1.Text+'''',
				'MS_GKKAT', 	''''+gkkat+'''',
				'MS_SNEV1', 	''''+M64.Text+'''',
				'MS_SNEV2', 	''''+MaskEdit2.Text+'''',
				'MS_KIFON',		BoolToStr01(CheckBox2.Checked),
				'MS_CSPAL',		BoolToStr01(CheckBox9.Checked),
				'MS_RRAKO',		BoolToStr01(CheckBox1.Checked),
				'MS_FAJKO', 	'3',
				'MS_FAJTA',		''''+'lerak�'+'''',
				'MS_FELARU', 	''''+M100.Text+'''',
		    'MS_EKAER', 	''''+M_EKAER.Text+'''',
				'MS_KESES', 	''''+M105.Text+'''',
    		'MS_HFOK1', 	''''+M101.Text+'''' ,
		    'MS_HFOK2', 	''''+M102.Text+''''
				],
				' WHERE MS_MBKOD = '+ret_kod+' AND MS_SORSZ = '+ret_sorsz+' AND MS_TIPUS = 0' ) then Raise Exception.Create('');        // fel

			if not Query_Update (Query1, 'MEGSEGED',
				[
				'MS_FELNEV', 	''''+CB1.Text+'''',
				'MS_FELIR', 	''''+M2A.Text+'''',
				'MS_FELTEL', 	''''+M3.Text+'''',
				'MS_FELCIM', 	''''+M4.Text+'''',
				'MS_FELSE1',	''''+M11.Text+'''',
				'MS_FELSE2',	''''+M12.Text+'''',
				'MS_FELSE3',	''''+M13.Text+'''',
				'MS_FELSE4',	''''+M14.Text+'''',
				'MS_FELSE5',	''''+M15.Text+'''',
				'MS_LOADID',	''''+M15A.Text+'''',
				'MS_FELDAT', 	''''+M50.Text+'''',
				'MS_FELDAT2', 	''''+M50__.Text+'''',
				'MS_FELIDO', 	''''+M60.Text+'''',
    		'MS_FELIDO2', 	''''+M60__.Text+'''',
				'MS_FERKDAT', 	''''+M50_.Text+'''',
				'MS_FERKIDO', 	''''+M60_.Text+'''',
				'MS_TEIDO', 	''''+M80.Text+'''',
				'MS_FETED',		''''+M17.Text+'''',
				'MS_FETED2',		''''+M171.Text+'''',
				'MS_FETEI',		''''+M18.Text+'''',
				'MS_FETEI2',		''''+M181.Text+'''',
				'MS_FSULY', 	SqlSzamString(StringSzam(M9.Text),8,2),
				'MS_FEKOB', 	SqlSzamString(StringSzam(Ma10.Text),8,2),
				'MS_FEPAL', 	SqlSzamString(StringSzam(M16.Text),8,2),
				'MS_FCPAL', 	SqlSzamString(StringSzam(M16C.Text),8,2),
				'MS_LERNEV', 	''''+CB2.Text+'''',
				'MS_LELIR', 	''''+M20A.Text+'''',
				'MS_LERTEL', 	''''+M30.Text+'''',
				'MS_LERCIM', 	''''+M40.Text+'''',
				'MS_LERSE1',	''''+M110.Text+'''',
				'MS_LERSE2',	''''+M120.Text+'''',
				'MS_LERSE3',	''''+M130.Text+'''',
				'MS_LERSE4',	''''+M140.Text+'''',
				'MS_LERSE5',	''''+M150.Text+'''',
				'MS_LETED',		''''+M170.Text+'''',
				'MS_LETED2',		''''+M1701.Text+'''',
				'MS_LETEI',		''''+M180.Text+'''',
				'MS_LETEI2',		''''+M1801.Text+'''',

				// A t�ny lerak�hoz ker�lnek a tervezett lerak� adatai
				'MS_TENNEV', 	''''+CB2.Text+'''',
				'MS_TENOR',		''''+CB21.Text+'''',
				'MS_TENIR', 	''''+M20A.Text+'''',
				'MS_TENTEL', 	''''+M30.Text+'''',
				'MS_TENCIM', 	''''+M40.Text+'''',
				'MS_TENYL1',	''''+M110.Text+'''',
				'MS_TENYL2',	''''+M120.Text+'''',
				'MS_TENYL3',	''''+M130.Text+'''',
				'MS_TENYL4',	''''+M140.Text+'''',
				'MS_TENYL5',	''''+M150.Text+'''',
				'MS_LETDAT', 	''''+M170.Text+'''',
//				'MS_LETIDO', 	''''+M60.Text+'''',
				'MS_LETIDO', 	''''+''+'''',

				// A t�ny felrak�hoz ker�lnek a mostani t�ny lerak� adatai
				'MS_TEFNEV', 	''''+CB3.Text+'''',
				'MS_TEFOR',		''''+CB31.Text+'''',
				'MS_TEFIR', 	''''+M200A.Text+'''',
				'MS_TEFTEL', 	''''+M300.Text+'''',
				'MS_TEFCIM', 	''''+M400.Text+'''',
				'MS_TEFL1',		''''+M310.Text+'''',
				'MS_TEFL2',		''''+M320.Text+'''',
				'MS_TEFL3',		''''+M330.Text+'''',
				'MS_TEFL4',		''''+M340.Text+'''',
				'MS_TEFL5',		''''+M350.Text+'''',
				'MS_FETDAT', 	''''+M70.Text+'''',
				'MS_FETIDO', 	''''+''+'''',

				'MS_LERDAT', 	''''+M170.Text+'''',
				'MS_LERDAT2', 	''''+M1701.Text+'''',
				'MS_LERIDO', 	''''+M180.Text+'''',
				'MS_LERIDO2', 	''''+M1801.Text+'''',
				'MS_FORSZ',		''''+CB11.Text+'''',
				'MS_ORSZA',		''''+CB21.Text+'''',
				'MS_EXPOR',		IntToStr(RadioGroup1.ItemIndex),
				'MS_EXSTR', 	''''+GetExportStr(RadioGroup1.ItemIndex)+'''',
				'MS_LSULY', 	SqlSzamString(StringSzam(M90.Text),8,2),
				'MS_LEKOB', 	SqlSzamString(StringSzam(Ma11.Text),8,2),
				'MS_LEPAL', 	SqlSzamString(StringSzam(M160.Text),8,2),
				'MS_LCPAL', 	SqlSzamString(StringSzam(M160C.Text),8,2),
				'MS_DATUM',     ''''+akdat+'''',
				'MS_IDOPT',     ''''+akido+'''',
				'MS_TINEV', 	''''+EgyebDlg.lerakas_str+'''',
				'MS_AKTNEV', 	''''+aknev+'''',
				'MS_AKTTEL', 	''''+aktel+'''',
				'MS_AKTCIM', 	''''+akcim+'''',
				'MS_AKTOR', 	''''+aktor+'''',
				'MS_AKTIR', 	''''+aktir+'''',
				'MS_REGIK',		IntToStr(StrToIntDef(lerako_kod,0)),

				'MS_RENSZ', 	''''+M61.Text+'''',
				'MS_POTSZ', 	''''+M62.Text+'''',
				'MS_DOKOD', 	''''+M63.Text+'''',
				'MS_DOKO2', 	''''+MaskEdit1.Text+'''',
				'MS_GKKAT', 	''''+gkkat+'''',
				'MS_SNEV1', 	''''+M64.Text+'''',
				'MS_SNEV2', 	''''+MaskEdit2.Text+'''',
				'MS_KIFON',		BoolToStr01(CheckBox3.Checked),
    		    'MS_DARU',		BoolToStr01(CheckBox4.Checked),
    		    //'MS_LDARU',		BoolToStr01(CheckBox4.Checked),
    		    //'MS_FDARU',		BoolToStr01(CheckBox5.Checked),
				'MS_RRAKO',	BoolToStr01(CheckBox1.Checked),
				'MS_CSPAL',		BoolToStr01(CheckBox9.Checked),
				'MS_FAJKO', 	'3',
				'MS_FAJTA',		''''+'lerak�'+'''',
				'MS_FELARU', 	''''+M100.Text+'''',
		    'MS_EKAER', 	''''+M_EKAER.Text+'''',
				'MS_KESES', 	''''+M105.Text+'''',
    		    'MS_HFOK1', 	''''+M101.Text+'''' ,
               'MS_HFOK2', 	''''+M102.Text+''''
				],
				' WHERE MS_MBKOD = '+ret_kod+' AND MS_SORSZ = '+ret_sorsz+' AND MS_TIPUS = 1' ) then Raise Exception.Create('');        // le
			ujmssorsz	:= ret_sorsz;
		end;
		// El�rak�s vagy el�h�z�s
		if ( ( RadioGroup2.ItemIndex = 1 ) or ( RadioGroup2.ItemIndex = 2 ) ) then begin
			ujkodszam	:= 1;
			if RadioGroup2.ItemIndex = 1 then begin
				fajko	:= '1';
				fajta	:= 'el�rak�s';
			end else begin
				fajko	:= '2';
				fajta	:= 'el�h�z�s';
			end;
			if Query_Run (Query1, 'SELECT MAX(MS_SORSZ) MAXKOD FROM MEGSEGED WHERE MS_MBKOD = '+IntToStr(StrToIntDef(ret_kod,0)),true) then begin
				ujkodszam	:= StrToIntDef(Query1.FieldByName('MAXKOD').AsString,0) + 1;
			end;
			ret_sorsz 	:= IntToStr(ujkodszam);
			ujkodszam 	:= GetNextCode('MEGSEGED', 'MS_MSKOD', 1, 0);
			if not Query_Run ( Query1, 'INSERT INTO MEGSEGED (MS_MSKOD, MS_MBKOD, MS_SORSZ, MS_TIPUS,MS_ID2) VALUES (' + IntToStr(ujkodszam) + ',' + IntToStr(StrToIntDef(ret_kod,0)) + ',' + ret_sorsz + ', 0'+','+IntToStr(msid0)+' )',true) then Raise Exception.Create('');
      ujkodszam2 	:= GetNextCode('MEGSEGED', 'MS_MSKOD', 1, 0);
			if not Query_Run ( Query1, 'INSERT INTO MEGSEGED (MS_MSKOD, MS_MBKOD, MS_SORSZ, MS_TIPUS,MS_ID2) VALUES (' + IntToStr(ujkodszam2) + ',' + IntToStr(StrToIntDef(ret_kod,0)) + ',' + ret_sorsz + ', 1'+','+IntToStr(msid1)+' )',true) then Raise Exception.Create('');
			M1.Text 	:= ret_sorsz;

			gkkat	:= listagkat[CBGkat.ItemIndex];
			if M61.Text <> '' then begin
				gkkat	:= Query_Select('GEPKOCSI', 'GK_RESZ', M61.Text, 'GK_GKKAT');
			end;
			if gkkat = '' then begin
				gkkat	:= listagkat[CBGkat.ItemIndex];
			end;

			akdat	:= M50.Text;
			akido	:= M60.Text;
    	if (M70.Text <> '')and(M80.Text <> '') then begin         // !!!!!!!!!!!!!
				akdat	:= M70.Text;
				akido	:= M80.Text;
				if akido = '' then begin
					akido	:= M60.Text;
				end;
			end;
			aknev		:= CB2.Text;
			aktel		:= M30.Text;
			akcim		:= M40.Text;
			aktor		:= CB21.Text;
			aktir		:= M20A.Text;
			// A felrak�s rekord besz�r�sa
			if not Query_Update (Query1, 'MEGSEGED',
				[
				'MS_FELNEV', 	''''+CB1.Text+'''',
				'MS_FELIR', 	''''+M2A.Text+'''',
				'MS_FELTEL', 	''''+M3.Text+'''',
				'MS_FELCIM', 	''''+M4.Text+'''',
				'MS_FELSE1',	''''+M11.Text+'''',
				'MS_FELSE2',	''''+M12.Text+'''',
				'MS_FELSE3',	''''+M13.Text+'''',
				'MS_FELSE4',	''''+M14.Text+'''',
				'MS_FELSE5',	''''+M15.Text+'''',
				'MS_LOADID',	''''+M15A.Text+'''',
//				'MS_FELDAT', 	''''+M5.Text+'''',
//				'MS_FELIDO', 	''''+M6.Text+'''',
				'MS_FELDAT', 	''''+M17.Text+'''',
				'MS_FELDAT2', 	''''+M171.Text+'''',
				'MS_FELIDO', 	''''+M18.Text+'''',
				'MS_FELIDO2', 	''''+M181.Text+'''',
				'MS_FETED',		''''+M17.Text+'''',
				'MS_FETED2',		''''+M171.Text+'''',
				'MS_FETEI',		''''+M18.Text+'''',
				'MS_FETEI2',		''''+M181.Text+'''',
				'MS_TEIDO', 	''''+M80.Text+'''',
				'MS_FSULY', 	SqlSzamString(StringSzam(M9.Text),8,2),
				'MS_FEKOB', 	SqlSzamString(StringSzam(Ma10.Text),8,2),
				'MS_FEPAL', 	SqlSzamString(StringSzam(M16.Text),8,2),
				'MS_FCPAL', 	SqlSzamString(StringSzam(M16C.Text),8,2),
				'MS_LERNEV', 	''''+CB2.Text+'''',
				'MS_LELIR', 	''''+M20A.Text+'''',
				'MS_LERTEL', 	''''+M30.Text+'''',
				'MS_LERCIM', 	''''+M40.Text+'''',
				'MS_LERSE1',	''''+M110.Text+'''',
				'MS_LERSE2',	''''+M120.Text+'''',
				'MS_LERSE3',	''''+M130.Text+'''',
				'MS_LERSE4',	''''+M140.Text+'''',
				'MS_LERSE5',	''''+M150.Text+'''',
				'MS_LETED',		''''+M170.Text+'''',
				'MS_LETED2',		''''+M1701.Text+'''',
				'MS_LETEI',		''''+M180.Text+'''',
				'MS_LETEI2',		''''+M1801.Text+'''',

				// A t�ny lerak�hoz ker�lnek a mostani t�ny felrak� adatai
				'MS_TENNEV', 	''''+CB4.Text+'''',
				'MS_TENOR',		''''+CB41.Text+'''',
				'MS_TENIR', 	''''+M2000A.Text+'''',
				'MS_TENTEL', 	''''+M3000.Text+'''',
				'MS_TENCIM', 	''''+M4000.Text+'''',
				'MS_TENYL1',	''''+M3100.Text+'''',
				'MS_TENYL2',	''''+M3200.Text+'''',
				'MS_TENYL3',	''''+M3300.Text+'''',
				'MS_TENYL4',	''''+M3400.Text+'''',
				'MS_TENYL5',	''''+M3500.Text+'''',
				'MS_LETDAT', 	''''+M7.Text+'''',
				'MS_LETIDO', 	''''+M8.Text+'''',
//				'MS_LETIDO', 	''''+''+'''',

				// A t�ny felrak�hoz ker�lnek a mostani tervezett felrak� adatai
				'MS_TEFNEV', 	''''+CB1.Text+'''',
				'MS_TEFOR',		''''+CB11.Text+'''',
				'MS_TEFIR', 	''''+M2A.Text+'''',
				'MS_TEFTEL', 	''''+M3.Text+'''',
				'MS_TEFCIM', 	''''+M4.Text+'''',
				'MS_TEFL1',		''''+M11.Text+'''',
				'MS_TEFL2',		''''+M12.Text+'''',
				'MS_TEFL3',		''''+M13.Text+'''',
				'MS_TEFL4',		''''+M14.Text+'''',
				'MS_TEFL5',		''''+M15.Text+'''',
//				'MS_FETDAT', 	''''+M5.Text+'''',
				'MS_FETDAT', 	''''+M17.Text+'''',
//				'MS_FETIDO', 	''''+M6.Text+'''',
				'MS_FETIDO', 	''''+''+'''',

				// A t�ny lerak�hoz ker�lnek a r�gebbi felrak� adatai
				'MS_LERDAT', 	''''+M5.Text+'''',
				'MS_LERDAT2', 	''''+M5__.Text+'''',
				'MS_LERIDO', 	''''+M6.Text+'''',
				'MS_LERIDO2', 	''''+M6__.Text+'''',
				'MS_LERKDAT', 	''''+M5_.Text+'''',
				'MS_LERKIDO', 	''''+M6_.Text+'''',
				'MS_FORSZ',		''''+CB11.Text+'''',
				'MS_ORSZA',		''''+CB21.Text+'''',
				'MS_EXPOR',		IntToStr(RadioGroup1.ItemIndex),
				'MS_EXSTR', 	''''+GetExportStr(RadioGroup1.ItemIndex)+'''',
				'MS_LSULY', 	SqlSzamString(StringSzam(M90.Text),8,2),
				'MS_LEKOB', 	SqlSzamString(StringSzam(Ma11.Text),8,2),
				'MS_LEPAL', 	SqlSzamString(StringSzam(M160.Text),8,2),
				'MS_LCPAL', 	SqlSzamString(StringSzam(M160C.Text),8,2),
				'MS_DATUM',     ''''+akdat+'''',
				'MS_IDOPT',     ''''+akido+'''',
				'MS_TINEV', 	''''+EgyebDlg.felrakas_str+'''',
				'MS_AKTNEV', 	''''+aknev+'''',
				'MS_AKTTEL', 	''''+aktel+'''',
				'MS_AKTCIM', 	''''+akcim+'''',
				'MS_AKTOR', 	''''+aktor+'''',
				'MS_AKTIR', 	''''+aktir+'''',
				'MS_REGIK',		IntToStr(StrToIntDef(felrako_kod,0)),

				'MS_RENSZ', 	''''+M61.Text+'''',
				'MS_POTSZ', 	''''+M62.Text+'''',
				'MS_DOKOD', 	''''+M63.Text+'''',
				'MS_DOKO2', 	''''+MaskEdit1.Text+'''',
				'MS_GKKAT', 	''''+gkkat+'''',
				'MS_SNEV1', 	''''+M64.Text+'''',
				'MS_SNEV2', 	''''+MaskEdit2.Text+'''',
				'MS_KIFON',		BoolToStr01(CheckBox2.Checked),
				'MS_RRAKO',		BoolToStr01(CheckBox1.Checked),
				'MS_CSPAL',		BoolToStr01(CheckBox9.Checked),
				'MS_FAJKO', 	fajko,
				'MS_FAJTA',		''''+fajta+'''',
				'MS_FELARU', 	''''+M100.Text+'''',
		    'MS_EKAER', 	''''+M_EKAER.Text+'''',
				'MS_KESES', 	''''+M105.Text+'''',
       		'MS_HFOK1', 	''''+M101.Text+'''' ,
	    	    'MS_HFOK2', 	''''+M102.Text+''''
				],
				' WHERE MS_MBKOD = '+ret_kod+' AND MS_SORSZ = '+ret_sorsz+' AND MS_TIPUS = 0' ) then Raise Exception.Create('');

			if not Query_Update (Query1, 'MEGSEGED',
				[
				'MS_FELNEV', 	''''+CB1.Text+'''',
				'MS_FELIR', 	''''+M2A.Text+'''',
				'MS_FELTEL', 	''''+M3.Text+'''',
				'MS_FELCIM', 	''''+M4.Text+'''',
				'MS_FELSE1',	''''+M11.Text+'''',
				'MS_FELSE2',	''''+M12.Text+'''',
				'MS_FELSE3',	''''+M13.Text+'''',
				'MS_FELSE4',	''''+M14.Text+'''',
				'MS_FELSE5',	''''+M15.Text+'''',
				'MS_LOADID',	''''+M15A.Text+'''',
				'MS_FELDAT', 	''''+M17.Text+'''',
				'MS_FELDAT2',	''''+M171.Text+'''',
				'MS_FELIDO', 	''''+M18.Text+'''',
				'MS_FELIDO2', 	''''+M181.Text+'''',
				'MS_FETED',		''''+M17.Text+'''',
				'MS_FETED2',		''''+M171.Text+'''',
				'MS_FETEI',		''''+M18.Text+'''',
				'MS_FETEI2',		''''+M181.Text+'''',
				'MS_TEIDO', 	''''+M80.Text+'''',
				'MS_FSULY', 	SqlSzamString(StringSzam(M9.Text),8,2),
				'MS_FEKOB', 	SqlSzamString(StringSzam(Ma10.Text),8,2),
				'MS_FEPAL', 	SqlSzamString(StringSzam(M16.Text),8,2),
				'MS_FCPAL', 	SqlSzamString(StringSzam(M16C.Text),8,2),
				'MS_LERNEV', 	''''+CB2.Text+'''',
				'MS_LELIR', 	''''+M20A.Text+'''',
				'MS_LERTEL', 	''''+M30.Text+'''',
				'MS_LERCIM', 	''''+M40.Text+'''',
				'MS_LERSE1',	''''+M110.Text+'''',
				'MS_LERSE2',	''''+M120.Text+'''',
				'MS_LERSE3',	''''+M130.Text+'''',
				'MS_LERSE4',	''''+M140.Text+'''',
				'MS_LERSE5',	''''+M150.Text+'''',
				'MS_LETED',		''''+M170.Text+'''',
				'MS_LETED2',		''''+M1701.Text+'''',
				'MS_LETEI',		''''+M180.Text+'''',
				'MS_LETEI2',		''''+M1801.Text+'''',

				// A t�ny lerak�hoz ker�lnek a mostani t�ny felrak� adatai
				'MS_TENNEV', 	''''+CB4.Text+'''',
				'MS_TENOR',		''''+CB41.Text+'''',
				'MS_TENIR', 	''''+M2000A.Text+'''',
				'MS_TENTEL', 	''''+M3000.Text+'''',
				'MS_TENCIM', 	''''+M4000.Text+'''',
				'MS_TENYL1',	''''+M3100.Text+'''',
				'MS_TENYL2',	''''+M3200.Text+'''',
				'MS_TENYL3',	''''+M3300.Text+'''',
				'MS_TENYL4',	''''+M3400.Text+'''',
				'MS_TENYL5',	''''+M3500.Text+'''',
				'MS_LETDAT', 	''''+M7.Text+'''',
				'MS_LETIDO', 	''''+M8.Text+'''',
//				'MS_LETIDO', 	''''+''+'''',

				// A t�ny felrak�hoz ker�lnek a mostani tervezett felrak� adatai
				'MS_TEFNEV', 	''''+CB1.Text+'''',
				'MS_TEFOR',		''''+CB11.Text+'''',
				'MS_TEFIR', 	''''+M2A.Text+'''',
				'MS_TEFTEL', 	''''+M3.Text+'''',
				'MS_TEFCIM', 	''''+M4.Text+'''',
				'MS_TEFL1',		''''+M11.Text+'''',
				'MS_TEFL2',		''''+M12.Text+'''',
				'MS_TEFL3',		''''+M13.Text+'''',
				'MS_TEFL4',		''''+M14.Text+'''',
				'MS_TEFL5',		''''+M15.Text+'''',
				'MS_FETDAT', 	''''+M17.Text+'''',
//				'MS_FETIDO', 	''''+M6.Text+'''',
				'MS_FETIDO', 	''''+''+'''',

				'MS_LERDAT', 	''''+M5.Text+'''',
				'MS_LERDAT2', 	''''+M5__.Text+'''',
				'MS_LERIDO', 	''''+M6.Text+'''',
				'MS_LERIDO2', 	''''+M6__.Text+'''',
				'MS_LERKDAT', 	''''+M5_.Text+'''',
				'MS_LERKIDO', 	''''+M6_.Text+'''',
				'MS_FORSZ',		''''+CB11.Text+'''',
				'MS_ORSZA',		''''+CB21.Text+'''',
				'MS_EXPOR',		IntToStr(RadioGroup1.ItemIndex),
				'MS_EXSTR', 	''''+GetExportStr(RadioGroup1.ItemIndex)+'''',
				'MS_LSULY', 	SqlSzamString(StringSzam(M90.Text),8,2),
				'MS_LEKOB', 	SqlSzamString(StringSzam(Ma11.Text),8,2),
				'MS_LEPAL', 	SqlSzamString(StringSzam(M160.Text),8,2),
				'MS_LCPAL', 	SqlSzamString(StringSzam(M160C.Text),8,2),
				'MS_DATUM',     ''''+akdat+'''',
				'MS_IDOPT',     ''''+akido+'''',
				'MS_TINEV', 	''''+EgyebDlg.lerakas_str+'''',
				'MS_AKTNEV', 	''''+aknev+'''',
				'MS_AKTTEL', 	''''+aktel+'''',
				'MS_AKTCIM', 	''''+akcim+'''',
				'MS_AKTOR', 	''''+aktor+'''',
				'MS_AKTIR', 	''''+aktir+'''',
				'MS_REGIK',		IntToStr(StrToIntDef(felrako_kod,0)),

				'MS_RENSZ', 	''''+M61.Text+'''',
				'MS_POTSZ', 	''''+M62.Text+'''',
				'MS_DOKOD', 	''''+M63.Text+'''',
				'MS_DOKO2', 	''''+MaskEdit1.Text+'''',
				'MS_GKKAT', 	''''+gkkat+'''',
				'MS_SNEV1', 	''''+M64.Text+'''',
				'MS_SNEV2', 	''''+MaskEdit2.Text+'''',
				'MS_KIFON',		BoolToStr01(CheckBox3.Checked),
    		'MS_DARU',		BoolToStr01(CheckBox4.Checked),
    		//'MS_LDARU',		BoolToStr01(CheckBox4.Checked),
    		//'MS_FDARU',		BoolToStr01(CheckBox5.Checked),
				'MS_RRAKO',		BoolToStr01(CheckBox1.Checked),
				'MS_CSPAL',		BoolToStr01(CheckBox9.Checked),
				'MS_FAJKO', 	fajko,
				'MS_FAJTA',		''''+fajta+'''',
				'MS_FELARU', 	''''+M100.Text+'''',
		    'MS_EKAER', 	''''+M_EKAER.Text+'''',
				'MS_KESES', 	''''+M105.Text+'''',
    		    'MS_HFOK1', 	''''+M101.Text+'''' ,
		        'MS_HFOK2', 	''''+M102.Text+''''
				],
				' WHERE MS_MBKOD = '+ret_kod+' AND MS_SORSZ = '+ret_sorsz+' AND MS_TIPUS = 1' ) then Raise Exception.Create('');

			ujmssorsz	:= ret_sorsz;

			// Megkeress�k az el�z� el�h�z�st/el�rak�st
			Query_Run(Query1, 'SELECT * FROM MEGSEGED WHERE MS_REGIK = '+IntToStr(StrToIntDef(felrako_kod,0))+' AND MS_SORSZ <> '+IntToStr(StrToIntDef(ret_sorsz,0))+
				' AND MS_FAJKO IN (1,2) ORDER BY MS_MSKOD');
//			if Query1.RecordCount > 1 then begin
			if False then begin
				// M�r van megel�z� rekord, annak a lerak�j�t t�ltj�k a felrak�ba!!!
				Query1.Last;
				Query1.Prior;
				if not Query_Update (Query2, 'MEGSEGED',
					[
					// A t�ny felrak�hoz ker�lnek a mostani t�ny lerak� adatai
					'MS_TEFNEV', 	''''+Query1.FieldByName('MS_TENNEV').AsString+'''',
					'MS_TEFOR',		''''+Query1.FieldByName('MS_TENOR').AsString+'''',
					'MS_TEFIR', 	''''+Query1.FieldByName('MS_TENIR').AsString+'''',
					'MS_TEFTEL', 	''''+Query1.FieldByName('MS_TENTEL').AsString+'''',
					'MS_TEFCIM', 	''''+Query1.FieldByName('MS_TENCIM').AsString+'''',
					'MS_TEFL1',		''''+Query1.FieldByName('MS_TENYL1').AsString+'''',
					'MS_TEFL2',		''''+Query1.FieldByName('MS_TENYL2').AsString+'''',
					'MS_TEFL3',		''''+Query1.FieldByName('MS_TENYL3').AsString+'''',
					'MS_TEFL4',		''''+Query1.FieldByName('MS_TENYL4').AsString+'''',
					'MS_TEFL5',		''''+Query1.FieldByName('MS_TENYL5').AsString+'''',
					'MS_FETDAT', 	''''+Query1.FieldByName('MS_LETDAT').AsString+'''',
//					'MS_FETIDO', 	''''+Query1.FieldByName('MS_LETIDO').AsString+'''',
					'MS_FETIDO', 	''''+''+'''',
				    'MS_KESES', 	''''+''+'''',
 		      'MS_EKAER', 	''''+M_EKAER.Text+'''',
					'MS_FELARU', 	''''+M100.Text+''''
					],
					' WHERE MS_MBKOD = '+ret_kod+' AND MS_SORSZ = '+ret_sorsz+' AND MS_TIPUS = 0' ) then Raise Exception.Create('');
				if not Query_Update (Query2, 'MEGSEGED',
					[
					// A t�ny felrak�hoz ker�lnek a mostani t�ny lerak� adatai
					'MS_TEFNEV', 	''''+Query1.FieldByName('MS_TENNEV').AsString+'''',
					'MS_TEFOR',		''''+Query1.FieldByName('MS_TENOR').AsString+'''',
					'MS_TEFIR', 	''''+Query1.FieldByName('MS_TENIR').AsString+'''',
					'MS_TEFTEL', 	''''+Query1.FieldByName('MS_TENTEL').AsString+'''',
					'MS_TEFCIM', 	''''+Query1.FieldByName('MS_TENCIM').AsString+'''',
					'MS_TEFL1',		''''+Query1.FieldByName('MS_TENYL1').AsString+'''',
					'MS_TEFL2',		''''+Query1.FieldByName('MS_TENYL2').AsString+'''',
					'MS_TEFL3',		''''+Query1.FieldByName('MS_TENYL3').AsString+'''',
					'MS_TEFL4',		''''+Query1.FieldByName('MS_TENYL4').AsString+'''',
					'MS_TEFL5',		''''+Query1.FieldByName('MS_TENYL5').AsString+'''',
					'MS_FETDAT', 	''''+Query1.FieldByName('MS_LETDAT').AsString+'''',
//					'MS_FETIDO', 	''''+Query1.FieldByName('MS_LETIDO').AsString+'''',
					'MS_FETIDO', 	''''+''+'''',
				    'MS_KESES', 	''''+''+'''',
  		    'MS_EKAER', 	''''+M_EKAER.Text+'''',
					'MS_FELARU', 	''''+M100.Text+''''
					],
					' WHERE MS_MBKOD = '+ret_kod+' AND MS_SORSZ = '+ret_sorsz+' AND MS_TIPUS = 1' ) then Raise Exception.Create('');
			end;
		end;
   Except
   END;
	end;
	// A st�tuszok friss�t�se
//	Query_Run(Query1, 'SELECT * FROM MEGSEGED WHERE MS_MBKOD = '+ret_kod+' AND MS_SORSZ = '+ret_sorsz);
	Query_Run(Query1, 'SELECT * FROM MEGSEGED WHERE MS_MBKOD = '+ret_kod);
	if Query1.RecordCount > 0 then begin
		while not Query1.Eof do begin
			statusz	:= GetFelrakoStatus(Query1.FieldByName('MS_ID').AsString);
			SetFelrakoStatus(Query1.FieldByName('MS_ID').AsString, statusz);
			Query1.Next;
		end;
	end;
   MegbizasKieg(ret_kod);
	Result	:= true;
end;

procedure TSegedbe2Dlg.BitBtn16Click(Sender: TObject);
var
  Siker: boolean;
begin
	// Elk�ldj�k a jelenlegi rekordot
 	Screen.Cursor	:= crHourGlass;
  Siker:= Elkuldes;
  Screen.Cursor	:= crDefault;
	if not Siker then begin
		Exit;
	end;
	// R�ugrunk a k�vetkez� elemre
	Megform.StatuszTorles;
	Megform.Query1.Prior;
	MegForm.StatuszBeiras(Megform.Query1.FieldByName('MB_MBKOD').AsString);
	Tolt('Felrak�s/lerak�s m�dos�t�sa', Megform.Query1.FieldByName('MB_MBKOD').AsString,
		Megform.Query1.FieldByname('MS_SORSZ').AsString, Megform.Query1.FieldByName('MS_MSKOD').AsString);
end;

procedure TSegedbe2Dlg.BitBtn17Click(Sender: TObject);
begin
	EgyebDlg.Calendarbe(M17);
	// Csak akkor �rjuk be az adatokat, ha a felrak�hely = t�ny felrak�hely
{	if ( CB1.Text + CB11.Text + M2A.Text + M3.Text = CB4.Text + CB41.Text + M2000A.Text + M3000.Text ) then begin
		M5.Text	:= M17.Text;
		M7.Text	:= M17.Text;
	end;   }
  M17.OnExit(self);
end;

procedure TSegedbe2Dlg.BitBtn19Click(Sender: TObject);
begin
	EgyebDlg.Calendarbe(M170);
	// Csak akkor �rjuk be az adatokat, ha a felrak�hely = t�ny felrak�hely
{	if ( CB2.Text + CB21.Text + M20A.Text + M30.Text = CB3.Text + CB31.Text + M200A.Text + M300.Text ) then begin
		M50.Text	:= M170.Text;
		M70.Text	:= M170.Text;
	end;    }
  M170.OnExit(self);
end;

procedure TSegedbe2Dlg.FormDestroy(Sender: TObject);
begin
	listaorsz.Free;
	listagkat.Free;
end;

procedure TSegedbe2Dlg.M17Exit(Sender: TObject);
begin
	if kellexit then begin
		DatumExit(M17, false);
    if M171.Text='' then
    begin
		  M171.Text:= M17.Text;
    end;
		// Csak akkor �rjuk be az adatokat, ha a felrak�hely = t�ny felrak�hely
		if ( CB1.Text + CB11.Text + M2A.Text + M3.Text = CB4.Text + CB41.Text + M2000A.Text + M3000.Text ) then begin
			M5.Text	 := M17.Text;
			M7.Text	 := M17.Text;
			//M17.Text := M17.Text;
			M5_.Text:= M17.Text;
      if M5__.Text='' then
			  M5__.Text:= M17.Text;
		end;
	end;
	kellexit	:= true;
  if M17.Text<>'' then
  M18.SetFocus;
end;

procedure TSegedbe2Dlg.M18Exit(Sender: TObject);
begin
//	if kellexit then begin
  	IdoExit(Sender, false);
{    if (M181.Text<>'')and(M171.Text+ M181.Text<M17.Text+ M18.Text)  then // HIBA
    begin
  		NoticeKi('A v�gs� id�pontnak nagyobbnak kell lenni, mint a kezd�nek!');
	  	//M181.SetFocus;
		  //Exit;
    end;  }
    if M181.Text='' then
      M181.Text:=M18.Text;

		// Csak akkor �rjuk be az adatokat, ha a felrak�hely = t�ny felrak�hely
		if ( CB1.Text + CB11.Text + M2A.Text + M3.Text = CB4.Text + CB41.Text + M2000A.Text + M3000.Text ) then begin
			M6.Text	:= M18.Text;
		end;
//	end;
	kellexit	:= true;
end;

procedure TSegedbe2Dlg.M180Exit(Sender: TObject);
begin
//	if kellexit then begin
  	IdoExit(Sender, false);
{    if (M1801.Text<>'')and(M1701.Text+ M1801.Text<M170.Text+ M180.Text)  then // HIBA
    begin
  		NoticeKi('A v�gs� id�pontnak nagyobbnak kell lenni, mint a kezd�nek!');
    end;  }
    if M1801.Text='' then
      M1801.Text:=M180.Text;
		// Csak akkor �rjuk be az adatokat, ha a felrak�hely = t�ny felrak�hely
		if ( CB2.Text + CB21.Text + M20A.Text + M30.Text = CB3.Text + CB31.Text + M200A.Text + M300.Text ) then begin
			M60.Text	:= M180.Text;
		end;
//	end;
	kellexit	:= true;
end;

procedure TSegedbe2Dlg.M170Exit(Sender: TObject);
begin
	if kellexit  then begin
		if DatumExit(M170, false) then
      if StrToDateDef(M170.Text,date) - StrToDateDef(M17.Text,date) > 7 then
      //if StrToDate(M170.Text) - StrToDate(M17.Text) > 7 then
        NoticeKi('7 napn�l hosszabb sz�ll�t�si id�szak. Ellen�rizze!');
    if M1701.Text='' then
    begin
		  M1701.Text:= M170.Text;
    end;
		// Csak akkor �rjuk be az adatokat, ha a felrak�hely = t�ny felrak�hely
		if ( CB2.Text + CB21.Text + M20A.Text + M30.Text = CB3.Text + CB31.Text + M200A.Text + M300.Text ) then begin
			M50.Text	:= M170.Text;
			M70.Text	:= M170.Text;
			//M1701.Text	:= M170.Text;
			M50_.Text	:= M170.Text;
      if M50__.Text='' then
			  M50__.Text	:= M170.Text;
		end;
    if StrToDateDef(M170.Text,date) - StrToDateDef(M17.Text,date) > 7 then
      NoticeKi('7 napn�l hosszabb sz�ll�t�si id�szak. Ellen�rizze!');
	end;
	kellexit	:= true;
  if M170.Text<>'' then
  M180.SetFocus;
end;

procedure TSegedbe2Dlg.M2AChange(Sender: TObject);
begin
	M2000A.Text	:= M2A.Text;
end;

procedure TSegedbe2Dlg.M3Change(Sender: TObject);
begin
	M3000.Text	:= M3.Text;
end;

procedure TSegedbe2Dlg.M4Change(Sender: TObject);
begin
	M4000.Text	:= M4.Text;
end;

procedure TSegedbe2Dlg.M11Change(Sender: TObject);
begin
	M3100.Text	:= M11.Text;
end;

procedure TSegedbe2Dlg.M12Change(Sender: TObject);
begin
	M3200.Text	:= M12.Text;
end;

procedure TSegedbe2Dlg.M13Change(Sender: TObject);
begin
	M3300.Text	:= M13.Text;
end;

procedure TSegedbe2Dlg.M14Change(Sender: TObject);
begin
	M3400.Text	:= M14.Text;
end;

procedure TSegedbe2Dlg.M15Change(Sender: TObject);
begin
	M3500.Text	:= M15.Text;
end;

procedure TSegedbe2Dlg.M20AChange(Sender: TObject);
begin
	M200A.Text	:= M20A.Text;
end;

procedure TSegedbe2Dlg.M30Change(Sender: TObject);
begin
	M300.Text	:= M30.Text;
end;

procedure TSegedbe2Dlg.M40Change(Sender: TObject);
begin
	M400.Text	:= M40.Text;
end;

procedure TSegedbe2Dlg.M110Change(Sender: TObject);
begin
	M310.Text	:= M110.Text;
end;

procedure TSegedbe2Dlg.M120Change(Sender: TObject);
begin
	M320.Text	:= M120.Text;
end;

procedure TSegedbe2Dlg.M130Change(Sender: TObject);
begin
	M330.Text	:= M130.Text;
end;

procedure TSegedbe2Dlg.M140Change(Sender: TObject);
begin
	M340.Text	:= M140.Text;
end;

procedure TSegedbe2Dlg.M150Change(Sender: TObject);
begin
	M350.Text	:= M150.Text;
end;

procedure TSegedbe2Dlg.M17KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
{	if Key = VK_RETURN then begin
		kellexit	:= false;
		if DatumExit(Sender, false) then begin
			// Csak akkor �rjuk be az adatokat, ha a felrak�hely = t�ny felrak�hely
			if ( CB1.Text + CB11.Text + M2A.Text + M3.Text = CB4.Text + CB41.Text + M2000A.Text + M3000.Text ) then begin
				M5.Text	:= M17.Text;
				M7.Text	:= M17.Text;
	  		M17.Text := M17.Text;
  			M5__.Text:= M17.Text;
			end;
			M17.SetFocus;
		end;
	end;  }
end;

procedure TSegedbe2Dlg.M170KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
{	if Key = VK_RETURN then begin
		kellexit	:= false;
		DatumExit(Sender, false);
		// Csak akkor �rjuk be az adatokat, ha a felrak�hely = t�ny felrak�hely
		if ( CB2.Text + CB21.Text + M20A.Text + M30.Text = CB3.Text + CB31.Text + M200A.Text + M300.Text ) then begin
			M50.Text	:= M170.Text;
			M70.Text	:= M170.Text;
		end;
		M170.SetFocus;
	end; }
end;

procedure TSegedbe2Dlg.M18KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
{	if Key = VK_RETURN then begin
		kellexit	:= false;
		if IdoExit(Sender, false) then begin
			// Csak akkor �rjuk be az adatokat, ha a felrak�hely = t�ny felrak�hely
			if ( CB1.Text + CB11.Text + M2A.Text + M3.Text = CB4.Text + CB41.Text + M2000A.Text + M3000.Text ) then begin
				M6.Text	:= M18.Text;
			end;
		 //	CheckBox2.SetFocus;
		end;
	end;  }
end;

procedure TSegedbe2Dlg.M180KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
{	if Key = VK_RETURN then begin
		kellexit	:= false;
		if IdoExit(Sender, false) then begin
			// Csak akkor �rjuk be az adatokat, ha a felrak�hely = t�ny felrak�hely
			if ( CB2.Text + CB21.Text + M20A.Text + M30.Text = CB3.Text + CB31.Text + M200A.Text + M300.Text ) then begin
				M60.Text	:= M180.Text;
			end;
		//	M80.SetFocus;
		end;
	end;       }
end;

procedure TSegedbe2Dlg.M9KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
	if Key = VK_RETURN then begin
		M9.SetFocus;
	end;
end;

procedure TSegedbe2Dlg.M16KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
	if Key = VK_RETURN then begin
		M16.SetFocus;
    Key:=0;
	end;
end;

procedure TSegedbe2Dlg.M160KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
	if Key = VK_RETURN then begin
		M160.SetFocus;
		Key	:= 0;
	end;
end;

procedure TSegedbe2Dlg.M90KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
	if Key = VK_RETURN then begin
		M100.SetFocus;
	end;
end;

procedure TSegedbe2Dlg.BitBtn21Click(Sender: TObject);
var
  rendsz,rajt,cel,rc:string;
begin
 rendsz:=M31.Text;
 rajt:=CB11.Text+', '+M2A.Text +', '+M3.Text +', '+M4.Text ;
 cel:= CB21.Text+', '+M20A.Text+', '+M30.Text+', '+M40.Text ;
 rc:='0';
 if M8.Text<>'' then rc:='1';
 if M80.Text<>'' then rc:='2';
// if MaskEdit6.Text<>'' then
  EgyebDlg.GoogleMap(rendsz,rajt,cel,rc,False)     ;

end;

procedure TSegedbe2Dlg.M50Exit(Sender: TObject);
begin
	if DatumExit(M50, false) then
   if StrToDate(M50.Text) - StrToDate(M5.Text) > 7 then
    NoticeKi('7 napn�l hosszabb sz�ll�t�si id�szak. Ellen�rizze!');
  M60.SetFocus;
end;

procedure TSegedbe2Dlg.FormShow(Sender: TObject);
begin
{    SetWindowPos(
    Self.Handle,
    HWND_TOPMOST,
    0,
    0,
    0,
    0,
    SWP_NOACTIVATE or SWP_NOMOVE or SWP_NOSIZE);
 }
 Try
 if CB1.Text='' then
   CB1.SetFocus
 else
  if M8.Text='' then
    M8.SetFocus
  else
   if M80.Text='' then
    M80.SetFocus;
 Except
 End;
 if hutos then
 begin
  Panel6.Visible:=True;
  RadioGroup2.Height:=45;
 end
 else
 begin
  Panel6.Visible:=False;
  RadioGroup2.Height:=45+53;
 end;

 Label60.Enabled:=kellpoz;
 M15A.Enabled:=kellpoz;
 Panel1.Visible:=kellpoz and (RadioGroup2.ItemIndex=0);
 if kellpoz and (M1.Text<>'') then
 begin
   TMEGTETEL.Close;
   TMEGTETEL.Parameters[0].Value:=StrToInt(ret_kod);
   TMEGTETEL.Parameters[1].Value:=StrToInt(M1.Text);
   TMEGTETEL.Open;
 end;
end;

procedure TSegedbe2Dlg.BitBtn22Click(Sender: TObject);
begin
	EgyebDlg.Calendarbe(M5_);
end;

procedure TSegedbe2Dlg.BitBtn23Click(Sender: TObject);
begin
	EgyebDlg.Calendarbe(M50_);
end;

procedure TSegedbe2Dlg.M181KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
{	if Key = VK_RETURN then begin
		kellexit	:= false;
		if IdoExit(Sender, false) then begin
			// Csak akkor �rjuk be az adatokat, ha a felrak�hely = t�ny felrak�hely
			if ( CB1.Text + CB11.Text + M2A.Text + M3.Text = CB4.Text + CB41.Text + M2000A.Text + M3000.Text ) then begin
			 	M6__.Text	:= M181.Text;
			end;
			CheckBox2.SetFocus;
		end;
	end;
     }
end;

procedure TSegedbe2Dlg.M1801KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
{	if Key = VK_RETURN then begin
		kellexit	:= false;
		if IdoExit(Sender, false) then begin
			// Csak akkor �rjuk be az adatokat, ha a felrak�hely = t�ny felrak�hely
			if ( CB2.Text + CB21.Text + M20A.Text + M30.Text = CB3.Text + CB31.Text + M200A.Text + M300.Text ) then begin
			 	M60__.Text	:= M1801.Text;
			end;
			M80.SetFocus;
		end;
	end;
  }
end;

procedure TSegedbe2Dlg.CB11AChange(Sender: TObject);
var
  orsz: string;
begin
  orsz:=Query_Select2( 'SZOTAR','SZ_FOKOD','SZ_MENEV','300',CB11A.Text,'SZ_ALKOD')  ;
	ComboSet (CB11, orsz, listaorsz);
  CB11.OnChange(self);
end;

procedure TSegedbe2Dlg.CB41Change(Sender: TObject);
var
  orsz: string;
begin
  orsz:=Query_Select2( 'SZOTAR','SZ_FOKOD','SZ_ALKOD','300',CB41.Text,'SZ_MENEV')  ;
  CB41A.ItemIndex:= CB41A.Items.IndexOf(orsz);
	Jellegszamolo;
end;

procedure TSegedbe2Dlg.CB21AChange(Sender: TObject);
var
  orsz: string;
begin
  orsz:=Query_Select2( 'SZOTAR','SZ_FOKOD','SZ_MENEV','300',CB21A.Text,'SZ_ALKOD')  ;
	ComboSet (CB21, orsz, listaorsz);
  CB21.OnChange(self);
end;

procedure TSegedbe2Dlg.CB41AChange(Sender: TObject);
var
  orsz: string;
begin
  orsz:=Query_Select2( 'SZOTAR','SZ_FOKOD','SZ_MENEV','300',CB41A.Text,'SZ_ALKOD')  ;
	ComboSet (CB41, orsz, listaorsz);
  CB41.OnChange(self);
end;

procedure TSegedbe2Dlg.CB31AChange(Sender: TObject);
var
  orsz: string;
begin
  orsz:=Query_Select2( 'SZOTAR','SZ_FOKOD','SZ_MENEV','300',CB31A.Text,'SZ_ALKOD')  ;
	ComboSet (CB31, orsz, listaorsz);
  CB31.OnChange(self);
end;

procedure TSegedbe2Dlg.BitBtn24Click(Sender: TObject);
var
  Siker: boolean;
begin
 if (M1.Text='') then
  Screen.Cursor	:= crHourGlass;
  Siker:= Elkuldes;
  Screen.Cursor	:= crDefault;

  if Siker and not TMEGTETEL.Active then begin
   TMEGTETEL.Close;
   TMEGTETEL.Parameters[0].Value:=StrToInt(ret_kod);
   TMEGTETEL.Parameters[1].Value:=StrToInt(M1.Text);
   TMEGTETEL.Open;
   end;

 if TMEGTETEL.Active then begin
  if TMEGTETEL.Modified then TMEGTETEL.Post;
  TMEGTETEL.Append;
  DBGrid1.SetFocus;
 end; 
end;

procedure TSegedbe2Dlg.DBGrid1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (ssCtrl in Shift) and (Key=VK_DELETE) then begin
    Key:=0;
  end;
  if Key=VK_DOWN then
  begin
    TMEGTETEL.Next;
    if not TMEGTETEL.Eof then
    begin
      TMEGTETEL.Prior;
    end
    else
    begin
      TMEGTETEL.last;
      Key:=0;
    end;
  end;
{
  if Key=VK_ESCAPE then
    if TMEGTETEL.State in [dsInsert,dsEdit] then
      TMEGTETEL.Cancel ;
 }
end;

procedure TSegedbe2Dlg.DBGrid1KeyPress(Sender: TObject; var Key: Char);
begin
  if (Key=chr(13)) then
  begin
    if DBGrid1.SelectedIndex<DBGrid1.Columns.Count-1 then
      DBGrid1.SelectedIndex:=DBGrid1.SelectedIndex+1
    else
    begin
      if DSMEGTETEL.State  in [dsEdit, dsInsert] then
      begin
        TMEGTETEL.Post;
      end;
      if DBGrid1.SelectedIndex>=DBGrid1.Columns.Count-1 then
        DBGrid1.SelectedIndex:=0;
    end;
  end;

end;

procedure TSegedbe2Dlg.Panel1Exit(Sender: TObject);
begin
  if TMEGTETEL.Modified then TMEGTETEL.Post;
end;

procedure TSegedbe2Dlg.BitBtn33Click(Sender: TObject);
begin
  if TMEGTETEL.Eof then exit;
	if NoticeKi('Biztosan k�ri a t�tel t�rl�s�t?', NOT_QUESTION) <> 0 then
    exit ;
 // if MessageBox(0, 'Biztosan k�ri a t�tel t�rl�s�t?', '', MB_ICONQUESTION or MB_YESNO or MB_DEFBUTTON2)= IDNO then exit;
  TMEGTETEL.Delete;
  DBGrid1.SetFocus;
end;

procedure TSegedbe2Dlg.DBGrid1DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
  if TMEGTETELMT_ATADVA.Value>0 then
    DBGrid1.Canvas.Font.Color := clRed;

  DBGrid1.DefaultDrawColumnCell(Rect,DataCol,Column , State);

end;

procedure TSegedbe2Dlg.TMEGTETELAfterInsert(DataSet: TDataSet);
begin
  TMEGTETELMT_MBKOD.Value:=StrToInt(ret_kod);
  TMEGTETELMT_SORSZ.Value:=StrToInt(M1.Text);
  TMEGTETELMT_ATADVA.Value:=0;
  DBGrid1.SelectedIndex:=0;
end;

procedure TSegedbe2Dlg.TMEGTETELAfterOpen(DataSet: TDataSet);
begin
  Label61.Caption:=IntToStr(TMEGTETEL.RecordCount);

end;

procedure TSegedbe2Dlg.TMEGTETELAfterPost(DataSet: TDataSet);
begin
  Label61.Caption:=IntToStr(TMEGTETEL.RecordCount);
  if TMEGTETELMT_LOADID.Value=EmptyStr then
  begin
    if MessageBox(0, 'Biztos hogy nincs Load ID??', '', MB_ICONWARNING or MB_YESNO) = IDYES     then
    begin
      TMEGTETEL.Edit;
      TMEGTETELMT_LOADID.Value:=TMEGTETELMT_POZIC.Value;
      TMEGTETEL.Post;
    end;
  end;
end;

procedure TSegedbe2Dlg.TMEGTETELAfterDelete(DataSet: TDataSet);
begin
  Label61.Caption:=IntToStr(TMEGTETEL.RecordCount);

end;

procedure TSegedbe2Dlg.M6__Enter(Sender: TObject);
begin
  if M6__.Text='' then
			if ( CB1.Text + CB11.Text + M2A.Text + M3.Text = CB4.Text + CB41.Text + M2000A.Text + M3000.Text ) then
			 	M6__.Text	:= M181.Text;
end;

procedure TSegedbe2Dlg.M60__Enter(Sender: TObject);
begin
  if M60__.Text='' then
			if ( CB2.Text + CB21.Text + M20A.Text + M30.Text = CB3.Text + CB31.Text + M200A.Text + M300.Text ) then
			 	M60__.Text	:= M1801.Text;
end;

procedure TSegedbe2Dlg.M181Exit(Sender: TObject);
begin
//	if kellexit then begin
  	IdoExit(Sender, false);
    //if (M181.Text<>'')and(M181.Text<M18.Text)  then // HIBA pl.: 20:00 - 06:30
    if (M181.Text<>'')and(M171.Text+ M181.Text<M17.Text+ M18.Text)  then // HIBA
    begin
  		NoticeKi('A v�gs� id�pontnak nagyobbnak kell lenni, mint a kezd�nek!');
			//if NoticeKi('Figyelem! A v�gs� id�pont kisebb, mint a kezd�! Folytatja?', NOT_QUESTION) <> 0 then begin
  		//NoticeKi('A v�gs� id�pontnak nagyobbnak kell lenni, mint a kezd�nek!');
      if M181.Text='' then
  	  	M181.SetFocus
      else
  	  	M18.SetFocus;
	  	Exit;
      //end;
    end;
		// Csak akkor �rjuk be az adatokat, ha a felrak�hely = t�ny felrak�hely
		if ( CB1.Text + CB11.Text + M2A.Text + M3.Text = CB4.Text + CB41.Text + M2000A.Text + M3000.Text ) then begin
			 	M6__.Text	:= M181.Text;
		end;
//	end;
	kellexit	:= true;

end;

procedure TSegedbe2Dlg.M1801Exit(Sender: TObject);
begin
//	if kellexit then begin
  	IdoExit(Sender, false);
    if (M1801.Text<>'')and(M1701.Text+ M1801.Text<M170.Text+ M180.Text)  then // HIBA
    begin
			if NoticeKi('Figyelem! A v�gs� id�pont kisebb, mint a kezd�! Folytatja?', NOT_QUESTION) <> 0 then begin
  		//NoticeKi('A v�gs� id�pontnak nagyobbnak kell lenni, mint a kezd�nek!');
        if M1801.Text='' then
    	  	M1801.SetFocus
        else
  	    	M180.SetFocus;
	  	  Exit;
      end;
    end;
		// Csak akkor �rjuk be az adatokat, ha a felrak�hely = t�ny felrak�hely
		if ( CB2.Text + CB21.Text + M20A.Text + M30.Text = CB3.Text + CB31.Text + M200A.Text + M300.Text ) then begin
			 	M60__.Text	:= M1801.Text;
		end;
//	end;
	kellexit	:= true;

end;

procedure TSegedbe2Dlg.M6__Exit(Sender: TObject);
begin
	IdoExit(Sender, false);
    if (M6__.Text<>'')and(M5__.Text+ M6__.Text<M5.Text+ M6.Text)  then // HIBA
    begin
			//if NoticeKi('Figyelem! A v�gs� id�pont kisebb, mint a kezd�! Folytatja?', NOT_QUESTION) <> 0 then begin
  		NoticeKi('A v�gs� id�pontnak nagyobbnak kell lenni, mint a kezd�nek!');
  		//NoticeKi('A v�gs� id�pontnak nagyobbnak kell lenni, mint a kezd�nek!');
      // Csapdahelyzet lett, kiveszem. NagyP-2017-01-26
      //   if M5__.Text='' then
      //    M5__.SetFocus
      //  else
  	  //	  M6__.SetFocus;
      // --- end NagyP-2017-01-26
	  	  //Exit;
      //end;
    end;
end;

procedure TSegedbe2Dlg.M60__Exit(Sender: TObject);
begin
	IdoExit(Sender, false);
    if (M60__.Text<>'')and(M50__.Text+ M60__.Text<M50.Text+ M60.Text)  then // HIBA pl.: 20:00 - 06:30
    begin
			if NoticeKi('Figyelem! A v�gs� id�pont kisebb, mint a kezd�! Folytatja?', NOT_QUESTION) <> 0 then begin
  		//NoticeKi('A v�gs� id�pontnak nagyobbnak kell lenni, mint a kezd�nek!');
      // csapda. NagyP-2017-01-26
      //  if M50__.Text='' then
      //    M50__.SetFocus
      //  else
  	  //	  M60__.SetFocus;
      // ---- end NagyP-2017-01-26
	  	//  Exit;
      end;
    end;
    
end;

procedure TSegedbe2Dlg.M60Exit(Sender: TObject);
begin
	IdoExit(Sender, false);
    if (M60__.Text<>'')and(M50__.Text+ M60__.Text<M50.Text+ M60.Text)  then // HIBA 
    begin
  		NoticeKi('A v�gs� id�pontnak nagyobbnak kell lenni, mint a kezd�nek!');
    end;
end;

procedure TSegedbe2Dlg.M171Exit(Sender: TObject);
begin
		DatumExit(M171, false);
    if (M171.Text<M17.Text)  then //
    begin
  		  NoticeKi('A v�gs� d�tum nem lehet kisebb a kezd�n�l!');
        if M181.Text='' then
    	  	M181.SetFocus
        else
  	    	M171.SetFocus;
	  	  Exit;
    end;
		// Csak akkor �rjuk be az adatokat, ha a felrak�hely = t�ny felrak�hely
		if ( CB1.Text + CB11.Text + M2A.Text + M3.Text = CB4.Text + CB41.Text + M2000A.Text + M3000.Text ) then begin
			M5__.Text:= M171.Text;
		end;
    M181.SetFocus;
end;

procedure TSegedbe2Dlg.M1701Exit(Sender: TObject);
begin
		DatumExit(M1701, false);
    if (M1701.Text<M170.Text)  then //
    begin
  		  NoticeKi('A v�gs� d�tum nem lehet kisebb a kezd�n�l!');
        if M1801.Text='' then
    	  	M1801.SetFocus
        else
  	    	M1701.SetFocus;
	  	  Exit;
    end;
		// Csak akkor �rjuk be az adatokat, ha a felrak�hely = t�ny felrak�hely
		if ( CB2.Text + CB21.Text + M20A.Text + M30.Text = CB3.Text + CB31.Text + M200A.Text + M300.Text ) then begin
			M50__.Text:= M1701.Text;
		end;
    M1801.SetFocus;
end;

procedure TSegedbe2Dlg.M5__Exit(Sender: TObject);
begin
		DatumExit(M5__, false);
    if (M5__.Text<M5.Text)  then //
    begin
  		  NoticeKi('A v�gs� d�tum nem lehet kisebb a kezd�n�l!');
        if M5__.Text='' then
          M5__.SetFocus
        else
  	  	  M6__.SetFocus;
	  	  Exit;
    end;
    M6__.SetFocus;
end;

procedure TSegedbe2Dlg.M50__Exit(Sender: TObject);
begin
		DatumExit(M50__, false);
    if (M50__.Text<M50.Text)  then //
    begin
  		  NoticeKi('A v�gs� d�tum nem lehet kisebb a kezd�n�l!');
        if M50__.Text='' then
          M50__.SetFocus
        else
  	  	  M60__.SetFocus;
    end;
end;

procedure TSegedbe2Dlg.BitBtn34Click(Sender: TObject);
begin
	EgyebDlg.Calendarbe(M171);
	// Csak akkor �rjuk be az adatokat, ha a felrak�hely = t�ny felrak�hely
{	if ( CB1.Text + CB11.Text + M2A.Text + M3.Text = CB4.Text + CB41.Text + M2000A.Text + M3000.Text ) then begin
		M5.Text	:= M17.Text;
		M7.Text	:= M17.Text;
	end;   }
  M171.OnExit(self);
end;

procedure TSegedbe2Dlg.BitBtn35Click(Sender: TObject);
begin
	EgyebDlg.Calendarbe(M1701);
	// Csak akkor �rjuk be az adatokat, ha a felrak�hely = t�ny felrak�hely
{	if ( CB2.Text + CB21.Text + M20A.Text + M30.Text = CB3.Text + CB31.Text + M200A.Text + M300.Text ) then begin
		M50.Text	:= M170.Text;
		M70.Text	:= M170.Text;
	end;    }
  M1701.OnExit(self);

end;

procedure TSegedbe2Dlg.M5Enter(Sender: TObject);
begin
		//  felrak�hely = t�ny felrak�hely
		if ( CB1.Text + CB11.Text + M2A.Text + M3.Text = CB4.Text + CB41.Text + M2000A.Text + M3000.Text ) and (M5.Text<>'') then
     M5_.SetFocus ;
end;

procedure TSegedbe2Dlg.M50Enter(Sender: TObject);
begin
		//  felrak�hely = t�ny felrak�hely
		if ( CB2.Text + CB21.Text + M20A.Text + M30.Text = CB3.Text + CB31.Text + M200A.Text + M300.Text ) and (M50.Text<>'') then begin
     M50_.SetFocus ;
end;

end;
procedure TSegedbe2Dlg.BitBtn36Click(Sender: TObject);
begin
	EgyebDlg.Calendarbe(M5__);
  M5__.OnExit(self);

end;

procedure TSegedbe2Dlg.M5Exit(Sender: TObject);
begin
	DatumExit(M5, false);
  M6.SetFocus;
end;

procedure TSegedbe2Dlg.BitBtn37Click(Sender: TObject);
begin
	EgyebDlg.Calendarbe(M50__);
  M50__.OnExit(self);

end;

procedure TSegedbe2Dlg.M210DblClick(Sender: TObject);
var
  ename,ecode :string;
  esuper : boolean;
begin
  if f_igazolt then exit;

  ename:=   EgyebDlg.user_name 	;
	ecode:=		EgyebDlg.user_code  ;
	esuper:=	EgyebDlg.user_super ;

  Application.CreateForm(TJelszoDlg, JelszoDlg);
  JelszoDlg.ShowModal;
  if ecode<>EgyebDlg.user_code then
  begin
    NoticeKi('Hib�s jelsz�!');
    EgyebDlg.user_name :=ename;
    EgyebDlg.user_code :=ecode;
    EgyebDlg.user_super:=esuper;
    exit;
  end;
  f_igazolt:=True;
  f_igazolo:=ename;
  M210.Font.Color:=clBlack;
  M211.Font.Color:=clBlack;
  M212.Font.Color:=clBlack;
end;

procedure TSegedbe2Dlg.M214DblClick(Sender: TObject);
var
  ename,ecode :string;
  esuper : boolean;
begin
  if l_igazolt then exit;

  ename:=   EgyebDlg.user_name 	;
	ecode:=		EgyebDlg.user_code  ;
	esuper:=	EgyebDlg.user_super ;

  Application.CreateForm(TJelszoDlg, JelszoDlg);
  JelszoDlg.ShowModal;
  if ecode<>EgyebDlg.user_code then
  begin
    NoticeKi('Hib�s jelsz�!');
    EgyebDlg.user_name :=ename;
    EgyebDlg.user_code :=ecode;
    EgyebDlg.user_super:=esuper;
    exit;
  end;
  l_igazolt:=True;
  l_igazolo:=ename;
  M213.Font.Color:=clBlack;
  M214.Font.Color:=clBlack;
  M215.Font.Color:=clBlack;
end;

procedure TSegedbe2Dlg.M216Change(Sender: TObject);
begin
   if StringSzam(M216.Text)>0 then HIOKA_Latszik(True)
   else HIOKA_Latszik(False);
end;

procedure TSegedbe2Dlg.HIOKA_Latszik(IgenVagyNem: boolean);
begin
  CB_HIOKA.Visible:=IgenVagyNem;
  Label68.Visible:=IgenVagyNem;
end;

procedure TSegedbe2Dlg.M216DblClick(Sender: TObject);
var
  pt1, pt2	: TPoint;
begin
  // lebeg� combobox-os megold�s, v�g�l nem �gy lett.
  {ComboFormDlg:= TComboFormDlg.Create(Segedbe2Dlg);
  // Application.CreateForm(TComboFormDlg, ComboFormDlg);
  try
    ComboFormDlg.Caption:='A hi�ny oka';
    if Query_Run(QueryKoz, 'SELECT SZ_MENEV FROM SZOTAR WHERE SZ_FOKOD=''372'' AND SZ_ALKOD BETWEEN ''200'' AND ''299'' '+
  		' AND SZ_ERVNY = 1 ORDER BY SZ_ALKOD') then begin
		  while not QueryKoz.EOF do begin
  				ComboFormDlg.cbCombo.Items.Add(QueryKoz.FieldByName('SZ_MENEV').AsString);
	    		QueryKoz.Next;
          end;  // while
      end;  // if
    // pt1.x	:= M216.Left;
		// pt1.y	:= M216.Top;
    // pt2:= ClientToScreen(pt1);
    // pt2:= Mouse.CursorPos;
    ComboFormDlg.Left:= ClientOrigin.X + 640;  // nagyj�b�l rajta legyen
    ComboFormDlg.Top:= ClientOrigin.Y + 520;  // nagyj�b�l rajta legyen
    ComboFormDlg.ShowModal;
    M216.Hint:=ComboFormDlg.cbCombo.Text;  // copy(ComboFormDlg.cbCombo.Text,1,1);  // csak az els� karakter kell, a k�d
  except
    ComboFormDlg.Release;
    end;  // try-except
    }
end;

procedure TSegedbe2Dlg.CheckBox9Click(Sender: TObject);
begin
  Panel4.Visible:=CheckBox9.Checked;
  Panel5.Visible:=CheckBox9.Checked;
  M16C.Visible:=CheckBox9.Checked;
  M160C.Visible:=CheckBox9.Checked;
end;

procedure TSegedbe2Dlg.BitJaratClick(Sender: TObject);
var
	szurosor	: integer;
  FETDAT,FETIDO,LETDAT,LETIDO: string;
  MBKOD: string;
begin

    FETDAT:=M7.Text;;
    FETIDO:=M8.Text;
    LETDAT:=M70.Text;
    LETIDO:=M80.Text;

    if MaskEdit3.Text='1' then
    begin
      LETDAT:=M7.Text; // MEGSEGEDMS_FETDAT.Value;
      LETIDO:=M8.Text; // MEGSEGEDMS_FETIDO.Value;
    end;
    if MaskEdit3.Text='2' then
    begin
      FETDAT:=M70.Text; // MEGSEGEDMS_LETDAT.Value;
      FETIDO:=M80.Text; // MEGSEGEDMS_LETIDO.Value;
    end;
    if MaskEdit3.Text='3' then
    begin
      FETDAT:=M70.Text; // MEGSEGEDMS_LETDAT.Value;
      FETIDO:=M80.Text; // MEGSEGEDMS_LETIDO.Value;
    end;

	// A j�rat sz�r�s be�ll�t�sa
	EgyebDlg.SzuroGrid.RowCount 			:= EgyebDlg.SzuroGrid.RowCount + 1;
	szurosor								:= EgyebDlg.SzuroGrid.RowCount - 1 ;
	EgyebDlg.SzuroGrid.Cells[0,szurosor] 	:= '190';
 //	EgyebDlg.SzuroGrid.Cells[1,szurosor] 	:= 'JA_RENDSZ = '''+M31.Text+''' ';
	EgyebDlg.SzuroGrid.Cells[1,szurosor] 	:= 'JA_RENDSZ = '''+M31.Text+''' and JA_JKEZD<='''+ FETDAT+''' and JA_JVEGE>='''+ LETDAT+''' ';
 {
(((ja_jkezd+ja_keido)<= :KEZD) and ((ja_jvege+ja_veido)>= :KEZD2) and
((ja_jkezd+ja_keido)<= :VEG) and ((ja_jvege+ja_veido)>= :VEG2))
  }

  //	EgyebDlg.megbizaskod					:= Query1.FieldByName('MS_MBKOD').AsString;
	EgyebDlg.megbizaskod					:= IntToStr(StrToIntDef(ret_kod,0)) ;
  MBKOD:=EgyebDlg.megbizaskod;
	JaratValaszto(jaratkod, jaratszam);

	EgyebDlg.megbizaskod					:= '';
	// A sz�r�s t�rl�se
	GridTorles(EgyebDlg.SzuroGrid, szurosor);
	if jaratkod.Text = '' then begin
		Exit;
	end;

  Application.CreateForm(TViszonybeDlg, ViszonybeDlg);
	ViszonybeDlg.ToltMegbizas(MBKOD ,jaratkod.Text);
  ViszonybeDlg.BitElkuld.OnClick(self);
	if ViszonybeDlg.ret_kod <> '' then begin
		// L�trej�tt az �j viszonylat, bejegyezz�k a megb�z�sba is
                   
		Query_Update( Query2, 'MEGSEGED', [
			'MS_JAKOD', ''''+jaratkod.Text+'''',
			'MS_JASZA', ''''+jaratszam.Text+''''
		], ' WHERE MS_MBKOD='+MBKOD+' and MS_SORSZ = '+IntToStr(SORSZAM));
  end;
  voltenter	:= true;
 	ViszonybeDlg.Destroy;
end;

procedure TSegedbe2Dlg.M101KeyPress(Sender: TObject; var Key: Char);
begin
	with Sender as TMaskEdit do begin
	 	Key := EgyebDlg.Jochar(Text,Key,3,0);
	end;

end;

procedure TSegedbe2Dlg.M16CExit(Sender: TObject);
begin
	SzamExit(Sender);
  if StrToIntDef(M16C.Text,0) > StrToIntDef(M16.Text,0)  then
  begin
    NoticeKi('Nem lehet t�bb a csere paletta, mint az �sszes!');
    M16C.SetFocus;
  end;
end;

procedure TSegedbe2Dlg.M160CExit(Sender: TObject);
begin
	SzamExit(Sender);
  if StrToIntDef(M160C.Text,0) > StrToIntDef(M160.Text,0)  then
  begin
    NoticeKi('Nem lehet t�bb a csere paletta, mint az �sszes!');
    M160C.SetFocus;
  end;
end;

procedure TSegedbe2Dlg.RakoCimEllenorzes(cbRakoNeve: TComboBox; Orszag, ZIP, Varos, UtcaHazszam: string;
                      meTovabbi1, meTovabbi2, meTovabbi3, meTovabbi4, meTovabbi5: TMaskEdit);
var
  TaroltOrszag, TaroltZIP, TaroltVaros, TaroltUtcaHazszam, S, TaroltSzelesseg, TaroltHosszusag: string;
  TaroltE1, TaroltE2, TaroltE3, TaroltE4, TaroltE5: string;
  RakoNeve, Tovabbi1, Tovabbi2, Tovabbi3, Tovabbi4, Tovabbi5: string;
  sor, i: integer;
  ModRes: TModalResult;
  procedure Tarolas(RakoNeve, Orszag, ZIP, Varos, UtcaHazszam, Tovabbi1, Tovabbi2, Tovabbi3, Tovabbi4, Tovabbi5: string);
    begin
      Application.CreateForm(TFelrakoMentesDlg, FelrakoMentesDlg);
      FelrakoMentesDlg.Tolto(RakoNeve, Orszag, ZIP, Varos, UtcaHazszam, Tovabbi1, Tovabbi2, Tovabbi3, Tovabbi4, Tovabbi5);
	    ModRes:=FelrakoMentesDlg.ShowModal;
      if ModRes= mrYes then begin
          cbRakoNeve.Text:= FelrakoMentesDlg.meUjnev.text;
          end;
    	FelrakoMentesDlg.Free;
    end;

   procedure SetRakonev(cbRakoNeve: TComboBox; UjNev: string);
     begin
       cbRakoNeve.Text:= UjNev;
     end;

begin
  RakoNeve:= cbRakoNeve.Text;
  Tovabbi1:= meTovabbi1.Text;
  Tovabbi2:= meTovabbi2.Text;
  Tovabbi3:= meTovabbi3.Text;
  Tovabbi4:= meTovabbi4.Text;
  Tovabbi5:= meTovabbi5.Text;
  RakoNeve:= Trim(RakoNeve);
  if RakoNeve <> '' then begin    // rak� neve alapj�n
    S:= 'select FF_ORSZA, FF_FELIR, FF_FELTEL, FF_FELCIM, FF_SZELE, FF_HOSZA, FF_FELSE1, FF_FELSE2, '+
        'FF_FELSE3, FF_FELSE4, FF_FELSE5 from FELRAKO where FF_FELNEV= '''+AposztrofCsere(RakoNeve)+'''';
    Query_run(Query2, S);
    if not Query2.Eof then begin
      TaroltOrszag:= Query2.FieldByName('FF_ORSZA').AsString;
      TaroltZIP:= Query2.FieldByName('FF_FELIR').AsString;
      TaroltVaros:= Query2.FieldByName('FF_FELTEL').AsString;
      TaroltUtcaHazszam:= Query2.FieldByName('FF_FELCIM').AsString;
      TaroltSzelesseg:= Trim(Query2.FieldByName('FF_SZELE').AsString);
      TaroltHosszusag:= Trim(Query2.FieldByName('FF_HOSZA').AsString);
      TaroltE1:= Trim(Query2.FieldByName('FF_FELSE1').AsString);
      TaroltE2:= Trim(Query2.FieldByName('FF_FELSE2').AsString);
      TaroltE3:= Trim(Query2.FieldByName('FF_FELSE3').AsString);
      TaroltE4:= Trim(Query2.FieldByName('FF_FELSE4').AsString);
      TaroltE5:= Trim(Query2.FieldByName('FF_FELSE5').AsString);
      if (Orszag <> TaroltOrszag) or (ZIP <> TaroltZIP) or (Varos <> TaroltVaros) or (UtcaHazszam <> TaroltUtcaHazszam)
        or (TaroltE1 <> Tovabbi1) or (TaroltE2 <> Tovabbi2) or (TaroltE3 <> Tovabbi3) or (TaroltE4 <> Tovabbi4) or (TaroltE5 <> Tovabbi5) then begin
          S:= 'select FF_FELNEV from FELRAKO '+
             ' where FF_ORSZA='''+Orszag+''' and FF_FELIR='''+ZIP+''' and FF_FELTEL='''+AposztrofCsere(Varos)+''' and FF_FELCIM='''+AposztrofCsere(UtcaHazszam)+'''' +
             ' and FF_FELSE1='''+AposztrofCsere(Tovabbi1)+''' and FF_FELSE2='''+AposztrofCsere(Tovabbi2)+''' and FF_FELSE3='''+AposztrofCsere(Tovabbi3)+''''+
             ' and FF_FELSE4='''+AposztrofCsere(Tovabbi4)+''' and FF_FELSE5='''+AposztrofCsere(Tovabbi5)+'''';
          Query_run(Query2, S);
          if not Query2.Eof then begin  // pontos egyez�s (pl. az el�bb mentett�k el ugyanezt...
              SetRakonev(cbRakoNeve, Query2.FieldByName('FF_FELNEV').AsString);
              end
          else begin // m�s n�vvel nincs pontos egyez�s, majd a felhaszn�l� eld�nti hogy mi legyen.
              Application.CreateForm(TFelrakoMentesDlg, FelrakoMentesDlg);
              try
                FelrakoMentesDlg.Tolto(RakoNeve, Orszag, ZIP, Varos, UtcaHazszam, Tovabbi1, Tovabbi2, Tovabbi3, Tovabbi4, Tovabbi5);
                ModRes:=FelrakoMentesDlg.ShowModal;
                if ModRes= mrYes then begin
                  SetRakonev(cbRakoNeve, FelrakoMentesDlg.meUjnev.text);
                  end;
              finally
                FelrakoMentesDlg.Free;
                end;  // try-finally
              end;  // else
          end // volt elt�r�s a t�rolt �rt�kekt�l
      else begin // nincs elt�r�s, ...
          if EgyebDlg.GPS_KALKULACIOK_HASZNALATA and ((TaroltSzelesseg='') or (TaroltHosszusag='')) then begin // ...de m�g nincsenek koordin�t�k
            FelrakoElment (RakoNeve, Orszag, ZIP, Varos, UtcaHazszam, Tovabbi1, Tovabbi2, Tovabbi3, Tovabbi4, Tovabbi5, '', False); // ablak n�lk�l kalkul�lja a koordin�t�kat
            end;
          end;
      end // if
    else begin  // nincs ilyen nev� rak�: egyb�l r�gz�tj�k �j rak�k�nt
      // Tarolas(RakoNeve, Orszag, ZIP, Varos, UtcaHazszam, Tovabbi1, Tovabbi2, Tovabbi3, Tovabbi4, Tovabbi5);
      // **** itt lehetne m�g hasonl� nev� felrak�kat keresni
      FelrakoElment (RakoNeve, Orszag, ZIP, Varos, UtcaHazszam, Tovabbi1, Tovabbi2, Tovabbi3, Tovabbi4, Tovabbi5, '', False); // ablak n�lk�l kalkul�lja a koordin�t�kat
      end;
    end

  // nincs megadva rak� n�v: lek�rdezz�k c�m alapj�n. Ha tal�lunk, felaj�nljuk egy list�ban az
  // azonos postai c�m� rak�kat (felt�ntetve az 5 addit�v mez�t is). Innen v�laszthat vagy megadhat �j nevet �s azzal r�gz�theti.
  else begin
    S:= 'select FF_FELNEV from FELRAKO '+
        ' where FF_ORSZA='''+Orszag+''' and FF_FELIR='''+ZIP+''' and FF_FELTEL='''+AposztrofCsere(Varos)+''' and FF_FELCIM='''+AposztrofCsere(UtcaHazszam)+'''' +
        ' and FF_FELSE1='''+AposztrofCsere(Tovabbi1)+''' and FF_FELSE2='''+AposztrofCsere(Tovabbi2)+''' and FF_FELSE3='''+AposztrofCsere(Tovabbi3)+''''+
        ' and FF_FELSE4='''+AposztrofCsere(Tovabbi4)+''' and FF_FELSE5='''+AposztrofCsere(Tovabbi5)+'''';
   Query_run(Query2, S);
   //  if not Query2.Eof then begin  // pontos egyez�s (pl. az el�bb mentett�k el ugyanezt...
    if Query2.RecordCount = 1 then begin  // pontos egyez�s (pl. az el�bb mentett�k el ugyanezt...
        SetRakonev(cbRakoNeve, Query2.FieldByName('FF_FELNEV').AsString);
        end
    else begin  // nincs pontos egyez�s, vagy t�bb pontos egyez�s van
        S:= 'select FF_FELNEV, FF_ORSZA, FF_FELIR, FF_FELTEL, FF_FELCIM, FF_SZELE, FF_HOSZA, FF_FELSE1, FF_FELSE2, '+
            ' FF_FELSE3, FF_FELSE4, FF_FELSE5 from FELRAKO '+
            ' where FF_ORSZA='''+Orszag+''' and FF_FELIR='''+ZIP+''' and FF_FELTEL='''+AposztrofCsere(Varos)+''' and FF_FELCIM='''+AposztrofCsere(UtcaHazszam)+'''';
        Query_run(Query2, S);
        if not Query2.Eof then begin
          Application.CreateForm(TFelrakoValasztDlg, FelrakoValasztDlg);
          FelrakoValasztDlg.Tolto('', Orszag, ZIP, Varos, UtcaHazszam, Tovabbi1, Tovabbi2, Tovabbi3, Tovabbi4, Tovabbi5);
          FelrakoValasztDlg.GridFormazas;
          i:=1;
          while not Query2.Eof do begin
             FelrakoValasztDlg.SG1.RowCount:= i+1;
             FelrakoValasztDlg.SG1.Cells[0,i]:= Query2.FieldByName('FF_FELNEV').AsString;
             FelrakoValasztDlg.SG1.Cells[1,i]:= Query2.FieldByName('FF_ORSZA').AsString;
             FelrakoValasztDlg.SG1.Cells[2,i]:= Query2.FieldByName('FF_FELIR').AsString;
             FelrakoValasztDlg.SG1.Cells[3,i]:= Query2.FieldByName('FF_FELTEL').AsString;
             FelrakoValasztDlg.SG1.Cells[4,i]:= Query2.FieldByName('FF_FELCIM').AsString;
             FelrakoValasztDlg.SG1.Cells[5,i]:= Trim(Query2.FieldByName('FF_FELSE1').AsString);
             FelrakoValasztDlg.SG1.Cells[6,i]:= Trim(Query2.FieldByName('FF_FELSE2').AsString);
             FelrakoValasztDlg.SG1.Cells[7,i]:= Trim(Query2.FieldByName('FF_FELSE3').AsString);
             FelrakoValasztDlg.SG1.Cells[8,i]:= Trim(Query2.FieldByName('FF_FELSE4').AsString);
             FelrakoValasztDlg.SG1.Cells[9,i]:= Trim(Query2.FieldByName('FF_FELSE5').AsString);
             Query2.Next;
             Inc(i);
             end;  // while
          ModRes:=FelrakoValasztDlg.ShowModal;
          if ModRes=mrOK then begin  // v�lasztott a t�bl�zatb�l, a v�lasztott rak� adatait kell a formra tenni
             sor:= FelrakoValasztDlg.SG1.Row;
             cbRakoNeve.Text:= FelrakoValasztDlg.SG1.Cells[0, sor];
             meTovabbi1.Text:= FelrakoValasztDlg.SG1.Cells[5, sor];
             meTovabbi2.Text:= FelrakoValasztDlg.SG1.Cells[6, sor];
             meTovabbi3.Text:= FelrakoValasztDlg.SG1.Cells[7, sor];
             meTovabbi4.Text:= FelrakoValasztDlg.SG1.Cells[8, sor];
             meTovabbi5.Text:= FelrakoValasztDlg.SG1.Cells[9, sor];
             end;  // if
          if ModRes=mrYes then begin  // elmentette �j rak�k�nt, a nev�t vissza�rjuk a formra
             SetRakonev(cbRakoNeve, FelrakoValasztDlg.meUjnev.text);
             end;  // if
          FelrakoValasztDlg.Free;
          end // if
        else begin  // c�m alapj�n nincs rak�: adjon nevet �s mentse el
          Tarolas(RakoNeve, Orszag, ZIP, Varos, UtcaHazszam, Tovabbi1, Tovabbi2, Tovabbi3, Tovabbi4, Tovabbi5);
          end;
      end;  // nem volt pontos egyez�s
    end;  // nincs megadva rak� n�v
end;

end.

