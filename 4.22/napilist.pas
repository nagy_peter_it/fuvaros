unit napilist;

interface

uses
  WinTypes, WinProcs, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, QuickRpt, QRExpr, Qrctrls, ExtCtrls, printers, DB, DBTables, Grids, ADODB,clipbrd ;

type
  TNapiListDlg = class(TForm)
    Rep: TQuickRep;
    QRBand3: TQRBand;
    QRLSzamla: TQRLabel;
    QRBand1: TQRBand;
    QRBand2: TQRBand;
    QRLabel15: TQRLabel;
    QRLabel16: TQRLabel;
    QRLabel18: TQRLabel;
    QRLabel38: TQRLabel;
    QRLabel6: TQRLabel;
    QRSysData1: TQRSysData;
	 QRLabel7: TQRLabel;
    QRSysData4: TQRSysData;
    QRShape1: TQRShape;
    QRLabel1: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel9: TQRLabel;
	 QLO2: TQRLabel;
	 Q3: TQRLabel;
    Q2: TQRLabel;
    Q5: TQRLabel;
	 Q7: TQRLabel;
    QRLabel31: TQRLabel;
    QRLabel35: TQRLabel;
    QLO1: TQRLabel;
    QRLabel21: TQRLabel;
    QRLabel3: TQRLabel;
    Q1: TQRLabel;
    ArfolyamGrid: TStringGrid;
    Osszesito: TStringGrid;
    L1: TQRLabel;
    L2: TQRLabel;
    L3: TQRLabel;
	 L4: TQRLabel;
    QRBand4: TQRBand;
    QRLab: TQRLabel;
    QRLabel2: TQRLabel;
    Q6: TQRLabel;
    QLNAP: TQRLabel;
    QRLabel13: TQRLabel;
    QRLabel14: TQRLabel;
    QRLabel17: TQRLabel;
    Query2: TADOQuery;
	 QueryJarat: TADOQuery;
	 Query3: TADOQuery;
	 SGBeKi: TStringGrid;
	 SGRekordok: TStringGrid;
	 ChildBand1: TQRChildBand;
	 O1: TQRLabel;
	 O2: TQRLabel;
	 O4: TQRLabel;
	 O5: TQRLabel;
	 procedure FormCreate(Sender: TObject);
	 procedure	Tolt(dat1, dat2	: string; reszletes : boolean );
	 procedure QRBand1BeforePrint(Sender: TQRCustomBand;
	   var PrintBand: Boolean);
	 procedure QRBand2BeforePrint(Sender: TQRCustomBand;
	   var PrintBand: Boolean);
	 procedure RepBeforePrint(Sender: TCustomQuickRep;
	   var PrintReport: Boolean);
	 procedure RepNeedData(Sender: TObject; var MoreData: Boolean);
	 procedure ChildBand1BeforePrint(Sender: TQRCustomBand;
	   var PrintBand: Boolean);
	 procedure QRBand1AfterPrint(Sender: TQRCustomBand;
	   BandPrinted: Boolean);
	 function	GetLocalArf(datu : string) : double;
  private
		osusd			: double;
		osfrt			: double;
		sorszam			: integer;
		valnem			: string;
		napidij			: double;
		osnap			: integer;
		donap			: integer;
		dousd			: double;
		doft			: double;
		reszlet			: boolean;
		dattol			: string;
		datig			: string;
		rekordszam		: integer;
  public
	  vanadat			: boolean;
  end;

var
  NapiListDlg: TNapiListDlg;

implementation

uses
	J_SQL, Egyeb, KOzos;

{$R *.DFM}

procedure TNapiListDlg.FormCreate(Sender: TObject);
begin
	EgyebDlg.SetADOQueryDatabase(Query2);
	EgyebDlg.SetADOQueryDatabase(Query3);
	EgyebDlg.SetADOQueryDatabase(QueryJarat);
	vanadat		:= true;
	sorszam		:= 1;
end;

procedure	TNapiListDlg.Tolt(dat1, dat2	: string; reszletes : boolean );
var
	sqlstr		: string;
	napok		: integer;
	orak			: integer;
	aktdat		: string;
	aktsor		: integer;
	i			: integer;
	arf			: double;
	joardat		: string;
	st			: string;
	ev			: integer;
	ho			: integer;
	rekszam		: integer;
	soforkod	: string;
	sofornev	: string;
	soforvege	: boolean;
begin
	vanadat 	:= false;
	dattol		:= dat1;
	datig		:= dat2;
	reszlet		:= reszletes;
	ArfolyamGrid.RowCount	:= 1;
	ArfolyamGrid.Rows[0].Clear;
	EgyebDlg.DateTimeDifference( dat1, '00:00', dat2, '00:00', napok, orak);
	ArfolyamGrid.RowCount	:= napok + 1;
	aktdat	:= dat1;
	aktsor	:= 0;
	while aktdat <= dat2 do begin
		ArfolyamGrid.Cells[0, aktsor]	:= aktdat;
		Inc(aktsor);
		aktdat	:= DatumHozNap(aktdat,1,true);
  	end;

	napidij		:= StringSzam(EgyebDlg.Read_SZGrid( '999', '744'));
	valnem		:= EgyebDlg.Read_SZGrid( '999', '745');

	if valnem = '' then begin
		NoticeKi('Hi�nyz� napid�jas bejegyz�sek az adatsz�t�rban (999 / 744, 745)!');
		Exit;
	end;

	{�rfolyamok ellen�rz�se -> mindig az el�z� h�nap 15.-e a j� �rfolyam}
	joardat	:= '';
	arf		:= 0;
	for i := 0 to ArfolyamGrid.RowCount -1 do begin
		ev		:= StrToIntDef(copy(ArfolyamGrid.Cells[0,i],1,4),0);
		ho		:= StrToIntDef(copy(ArfolyamGrid.Cells[0,i],6,2),0);
		if ((ev=0) or (ho=0)) then begin
			Continue;
		end;
(*
		Dec(ho);
		if ho = 0 then begin {Janu�rban az el�z� decembert kell n�zni}
			Dec(ev);
			ho	:= 12;
		end;
*)
		// Nem cs�kkent�k a h�napot, az aktu�lis h� 15.�nek vessz�k az �rfolyam�t
		st	:= Format('%4.4d.%2.2d.15.',[ev,ho]);	{El�z� h�nap 15.-e}
		if st <> joardat then begin
			joardat	:= st;
			arf		:= EgyebDlg.ArfolyamErtek( valnem, st, true);
			if arf = 0 then begin
				Exit;
			end;
		end;
		ArfolyamGrid.Cells[1, i]	:= SzamString(arf, 12,4);
	end;

	// A j�rat ellen�rz�tts�g ellen�rz�se
	sqlstr	:= 'SELECT DISTINCT JA_KOD, JA_ALKOD, JA_ORSZ FROM JARAT, JARSOFOR WHERE JA_KOD = JS_JAKOD AND JA_FAZIS < 1 ' +
	{Sz�r�s az id�szakra}
		' AND (((JS_KIDAT >= '''+dat1+ ''') AND (JS_KIDAT <= '''+dat2+''')) ' +
		' OR ((JS_BEDAT >= '''+dat1+ ''') AND (JS_BEDAT <= '''+dat2+''')) ' +
		' OR ((JS_KIDAT <= '''+dat1+ ''') AND (JS_BEDAT >= '''+dat2+''')))';
	Query_Run(Query2, sqlstr);
	EllenListTorol;
	while not Query2.Eof do begin
		EllenListAppend('Nem ellen�rz�tt j�rat : ('+Query2.FieldByName('JA_KOD').AsString + ') ' + GetJaratszamFromDb(Query2), true);
		Query2.Next;
	end;
	EllenListMutat('Nem ellen�rz�tt j�ratok', true);

	// Legy�jtj�k az �sszes JARSOFOR rekordot �s betessz�k ezeket a t�bl�ba
	SGBeKi.RowCount	:= 1;
	SGBeKi.Rows[0].Clear;
  {
	sqlstr	:= 'SELECT * FROM JARSOFOR WHERE ' +
		' (((JS_KIDAT >= '''+dat1+ ''') AND (JS_KIDAT <= '''+dat2+''')) ' +
		' OR ((JS_BEDAT >= '''+dat1+ ''') AND (JS_BEDAT <= '''+dat2+''')) ' +
		' OR ((JS_KIDAT <= '''+dat1+ ''') AND (JS_BEDAT >= '''+dat2+'''))) '+
    ' order by js_sofor,js_jakod';
      }
	sqlstr	:= 'SELECT * FROM JARSOFOR WHERE ' +
		' (((JS_KIDAT >= '''+dat1+ ''') AND (JS_KIDAT <= '''+dat2+''')) ' +
		' OR ((JS_BEDAT >= '''+dat1+ ''') AND (JS_BEDAT <= '''+dat2+''')) ' +
    // ki volt kommentezve, de a h�napon �tny�l� visszafel� j�rat miatt kell. Visszatettem NP 2017-08-11
    // ' OR ((JS_BEDAT >= '''+dat1+ ''') AND (JS_BEDAT <= '''+dat2+''')) ' +
		' OR ((JS_KIDAT <= '''+dat1+ ''') AND (JS_BEDAT >= '''+dat2+''')) '+
    // ------------------------------------------------------------------------
    ' ) '+
    ' order by js_sofor,js_jakod';

   //   Clipboard.Clear;
   //   Clipboard.AsText:=sqlstr;

	Query_Run(Query2, sqlstr);
	if Query2.RecordCount < 1 then begin
		Exit;
	end;
	SGBeKi.RowCount := Query2.RecordCount * 2;
	rekszam	:= 0;
	Query_Run(QueryJarat, 'SELECT JA_KOD, JA_ORSZ, JA_ALKOD FROM JARAT');
	Query_Run(Query3, 'SELECT DO_KOD, DO_NAME FROM DOLGOZO');
	while not Query2.Eof do begin
		if Query2.FieldByName('JS_KIDAT').AsString <> '' then begin
			SGBeKi.Cells[0,rekszam]	:= Query2.FieldByName('JS_SOFOR').AsString;
			SGBeKi.Cells[1,rekszam]	:= Query2.FieldByName('JS_JAKOD').AsString;
			SGBeKi.Cells[2,rekszam]	:= Query2.FieldByName('JS_KIDAT').AsString;
			SGBeKi.Cells[3,rekszam]	:= Query2.FieldByName('JS_KIIDO').AsString;
			SGBeKi.Cells[4,rekszam]	:= '1';
			Inc(rekszam);
		end;
		if Query2.FieldByName('JS_BEDAT').AsString <> '' then begin
			SGBeKi.Cells[0,rekszam]	:= Query2.FieldByName('JS_SOFOR').AsString;
			SGBeKi.Cells[1,rekszam]	:= Query2.FieldByName('JS_JAKOD').AsString;
			SGBeKi.Cells[2,rekszam]	:= Query2.FieldByName('JS_BEDAT').AsString;
			SGBeKi.Cells[3,rekszam]	:= Query2.FieldByName('JS_BEIDO').AsString;
			SGBeKi.Cells[4,rekszam]	:= '0';
			Inc(rekszam);
		end;
		Query2.Next;
	end;
	SGBeKi.RowCount := rekszam;
	// A t�bl�zat ment�se!!!
//	GridExportToFile (SGBeKi, EgyebDlg.TempList + 'BEKITABLA.XLS');
	// A t�bla rendez�se
	TablaRendezo( SGBeKi, [0,2,3], [0,0,0], 0, true);
//	GridExportToFile (SGBeKi, EgyebDlg.TempList + 'BEKITABLA_REN.XLS');
	// A t�bl�zat ellen�rz�se a k�vetkez� szab�lyok alapj�n :
	//   - minden dolgoz�n�l az els� elemnek kil�p�snek kell lennie
	//   - minden dolgoz�n�l az utols� elemnek bel�p�snek kell lennie
	//   - ha k�t kil�p�s vagy �t bel�p�s k�veti egym�st azonos dolgoz�n�l -> besz�r�s
	soforkod	:= '';
	EllenListTorol;
	for i := 0 to SGBeKi.RowCount - 1 do begin
		if soforkod <> SGBeKi.Cells[0,i] then begin
			// Sof�rv�lt�s van, itt kil�p�s kell
			soforkod := SGBeKi.Cells[0,i];
			if SGBeKi.Cells[4,i] <> '1' then begin
				// Meg kell keresni az el�z� kil�p�st
				Query_Run(Query2, 'SELECT * FROM JARSOFOR WHERE  JS_KIDAT + JS_KIIDO <= '''+SGBeKi.Cells[2,i]+SGBeKi.Cells[3,i]+''' AND JS_KIDAT > '''' '+
				 ' AND JS_SOFOR = '''+SGBeKi.Cells[0,i]+''' ORDER BY JS_KIDAT, JS_KIIDO');
				SGBeKi.RowCount := SGBeKi.RowCount +1;
				rekszam	:= SGBeKi.RowCount - 1;
				if Query2.RecordCount > 0 then begin
					// Megtal�ltuk a j� elemet
					Query2.Last;
					SGBeKi.Cells[0,rekszam]	:= Query2.FieldByName('JS_SOFOR').AsString;
					SGBeKi.Cells[1,rekszam]	:= Query2.FieldByName('JS_JAKOD').AsString;
					SGBeKi.Cells[2,rekszam]	:= Query2.FieldByName('JS_KIDAT').AsString;
					SGBeKi.Cells[3,rekszam]	:= Query2.FieldByName('JS_KIIDO').AsString;
					SGBeKi.Cells[4,rekszam]	:= '1';
				end else begin
					// Nincs kisebb kil�p�s, j�het a dummy
					sofornev	:= '';
					if Query3.Locate('DO_KOD',SGBeKi.Cells[0,i],[ ]) then begin
						sofornev	:= Query3.FieldByName('DO_NAME').AsString;
					end;
					EllenListAppend('Hi�nyz� els� kil�p�s : '+SGBeKi.Cells[0,i]+' '+sofornev+'; '+GetJaratszam(SGBeKi.Cells[1,i])+
						'; '+SGBeKi.Cells[2,i]+' '+SGBeKi.Cells[3,i], true);
					SGBeKi.Cells[0,rekszam]	:= SGBeKi.Cells[0,i];
					SGBeKi.Cells[1,rekszam]	:= 'HI�NY';
					SGBeKi.Cells[2,rekszam]	:= SGBeKi.Cells[2,i];
					SGBeKi.Cells[3,rekszam]	:= copy(SGBeKi.Cells[3,i], 1, 3);
					SGBeKi.Cells[4,rekszam]	:= '1';
				end;
			end;
		end;
		// N�zz�k a k�t egym�s m�g�tti elemet
		if i < SGBeKi.RowCount - 1 then begin
			if ( ( SGBeKi.Cells[0,i] = SGBeKi.Cells[0,i+1] ) and			// Megegyezik a sof�rk�d
				 ( SGBeKi.Cells[4,i] = SGBeKi.Cells[4,i+1] ) )  then begin  // Megegyezik a m�velet
				sofornev	:= '';
				if Query3.Locate('DO_KOD',SGBeKi.Cells[0,i],[ ]) then begin
					sofornev	:= Query3.FieldByName('DO_NAME').AsString;
				end;
				SGBeKi.RowCount := SGBeKi.RowCount +1;
				rekszam			:= SGBeKi.RowCount - 1;
				if SGBeKi.Cells[4,i] = '0' then begin
					// Duplik�lt bel�p�s
					EllenListAppend('Duplik�lt bel�p�s : '+SGBeKi.Cells[0,i]+' '+sofornev+'; '+
					GetJaratszam(SGBeKi.Cells[1,i])+'; '+SGBeKi.Cells[2,i]+' '+SGBeKi.Cells[3,i]+'<->'+
					GetJaratszam(SGBeKi.Cells[1,i+1])+'; '+SGBeKi.Cells[2,i+1]+' '+SGBeKi.Cells[3,i+1]+')', true);
					SGBeKi.Cells[0,rekszam]	:= SGBeKi.Cells[0,i];
					SGBeKi.Cells[1,rekszam]	:= 'P�TKI';
					SGBeKi.Cells[2,rekszam]	:= SGBeKi.Cells[2,i+1];
					SGBeKi.Cells[3,rekszam]	:= copy(SGBeKi.Cells[3,i+1], 1, 3);
					SGBeKi.Cells[4,rekszam]	:= '1';
				end else begin
					// Duplik�lt kil�p�s
					EllenListAppend('Duplik�lt kil�p�s : '+SGBeKi.Cells[0,i]+' '+sofornev+'; '+
					GetJaratszam(SGBeKi.Cells[1,i])+'; '+SGBeKi.Cells[2,i]+' '+SGBeKi.Cells[3,i]+'<->'+
					GetJaratszam(SGBeKi.Cells[1,i+1])+'; '+SGBeKi.Cells[2,i+1]+' '+SGBeKi.Cells[3,i+1]+')', true);
					SGBeKi.Cells[0,rekszam]	:= SGBeKi.Cells[0,i];
					SGBeKi.Cells[1,rekszam]	:= 'P�TBE';
					SGBeKi.Cells[2,rekszam]	:= SGBeKi.Cells[2,i];
					SGBeKi.Cells[3,rekszam]	:= copy(SGBeKi.Cells[3,i], 1, 3)+'59';
					SGBeKi.Cells[4,rekszam]	:= '0';
				end;
			end;
		end;
		// Megn�zz�k a dolgoz� utols� elem�t
		soforvege := false;
		if i = SGBeKi.RowCount - 1 then begin
			soforvege := true;
		end else begin
			if ( SGBeKi.Cells[0,i] <> SGBeKi.Cells[0,i+1] ) then begin
				soforvege := true;
			end;
		end;
		if soforvege then begin
			if SGBeKi.Cells[4,i] = '1' then begin
				// Meg kell keresni a k�vetkez� bel�p�st
				Query_Run(Query2, 'SELECT * FROM JARSOFOR WHERE  JS_BEDAT + JS_BEIDO >= '''+SGBeKi.Cells[2,i]+SGBeKi.Cells[3,i]+''' AND JS_BEDAT > '''' '+
				 ' AND JS_SOFOR = '''+SGBeKi.Cells[0,i]+''' ORDER BY JS_BEDAT, JS_BEIDO');
				SGBeKi.RowCount := SGBeKi.RowCount +1;
				rekszam			:= SGBeKi.RowCount - 1;
				if Query2.RecordCount > 0 then begin
					// Megtal�ltuk a j� elemet
					Query2.First;
					SGBeKi.Cells[0,rekszam]	:= Query2.FieldByName('JS_SOFOR').AsString;
					SGBeKi.Cells[1,rekszam]	:= Query2.FieldByName('JS_JAKOD').AsString;
					SGBeKi.Cells[2,rekszam]	:= Query2.FieldByName('JS_KIDAT').AsString;
					SGBeKi.Cells[3,rekszam]	:= Query2.FieldByName('JS_KIIDO').AsString;
					SGBeKi.Cells[4,rekszam]	:= '0';
				end else begin
					// Nincs kisebb kil�p�s, j�het a dummy
					sofornev	:= '';
					if Query3.Locate('DO_KOD',SGBeKi.Cells[0,i],[ ]) then begin
						sofornev	:= Query3.FieldByName('DO_NAME').AsString;
					end;
					EllenListAppend('Hi�nyz� utols� bel�p�s : '+SGBeKi.Cells[0,i]+' '+sofornev+'; '+GetJaratszam(SGBeKi.Cells[1,i])+
						'; '+SGBeKi.Cells[2,i]+' '+SGBeKi.Cells[3,i], true);
					SGBeKi.Cells[0,rekszam]	:= SGBeKi.Cells[0,i];
					SGBeKi.Cells[1,rekszam]	:= 'HI�NY';
					SGBeKi.Cells[2,rekszam]	:= SGBeKi.Cells[2,i];
					SGBeKi.Cells[3,rekszam]	:= copy(SGBeKi.Cells[3,i], 1, 3)+'59';
					SGBeKi.Cells[4,rekszam]	:= '0';
				end;
			end;
		end;
	end;
	EllenListMutat('Napid�jas probl�m�k', true);
	// �jra lerendezz�k a t�bl�t az �j sorok miatt
//	GridExportToFile (SGBeKi, EgyebDlg.TempList + 'BEKITABLA2.XLS');
	TablaRendezo( SGBeKi, [0,2,3], [0,0,0], 0, true);
//	GridExportToFile (SGBeKi, EgyebDlg.TempList + 'BEKITABLA3.XLS');

	// Besz�rjuk a rekordokat a t�bl�ba
	SGRekordok.RowCount	:= SGBeKi.RowCount;
	rekordszam			:= 0;
	soforkod			:= '';
	sofornev			:= '';
	for i := 0 to SGBeKi.RowCount - 2 do begin
		if ( ( SGBeKi.Cells[0,i] = SGBeKi.Cells[0,i+1] )
			and (SGBeKi.Cells[4,i] = '1' )			// Kil�p�s
			and (SGBeKi.Cells[4,i+1] = '0' )		// Bel�p�s
			) then begin
			if soforkod <> SGBeKi.Cells[0,i] then begin
				soforkod 	:= SGBeKi.Cells[0,i];
				sofornev	:= '';
				if Query3.Locate('DO_KOD',soforkod,[ ]) then begin
					sofornev	:= Query3.FieldByName('DO_NAME').AsString;
				end;
			end;
			// A rekordok t�bla felt�lt�se
			SGRekordok.Cells[ 0,rekordszam] := SGBeKi.Cells[0,i];   	// Sof�rk�d
			SGRekordok.Cells[ 1,rekordszam] := sofornev;                // Sof�rn�v
			SGRekordok.Cells[ 2,rekordszam] := SGBeKi.Cells[2,i];		// Kil�p�s d�tuma
			SGRekordok.Cells[ 3,rekordszam] := SGBeKi.Cells[3,i];		// Kil�p�s ideje
			SGRekordok.Cells[ 4,rekordszam] := SGBeKi.Cells[1,i];		// Kil�p�s j�ratk�d
			SGRekordok.Cells[ 5,rekordszam] := SGBeKi.Cells[2,i+1];		// Bel�p�s d�tuma
			SGRekordok.Cells[ 6,rekordszam] := SGBeKi.Cells[3,i+1];		// Bel�p�s id�ponta
			SGRekordok.Cells[ 7,rekordszam] := SGBeKi.Cells[1,i+1];		// Bel�p�s j�rata
			Inc(rekordszam);
		end;
	end;
	SGRekordok.RowCount	:= rekordszam;
	// Rendez�s n�vre �s d�tumra
	TablaRendezo( SGRekordok, [1,2,3], [0,0,0], 0, true);
	vanadat 			:= SGRekordok.Cells[ 0, 0] <> '';
	// Az id�szaki cimke megjelen�t�se
	QrLabel21.Caption := dat1 + ' - ' + dat2;
	QrLabel16.Caption := valnem;
	// A napid�j ki�r�sa a lap alj�n
	QRLab.Caption := 'Napid�j : '+ Format('%.2f',[napidij]) + ' '+valnem+'/nap    ';
	if not reszletes then begin
		// A nem r�szletes list�n�l elt�ntet�nk feliratokat
		QrLabel2.Enabled	:= false;
		QrLabel9.Enabled	:= false;
		QrLabel31.Enabled	:= false;
		QrLabel35.Enabled	:= false;
	end;
end;

procedure TNapiListDlg.QRBand1BeforePrint(Sender: TQRCustomBand;
	   var PrintBand: Boolean);
var
	jaratstr	: string;

	dat			: string;
	ido			: integer;
	ora			: integer;
	perc		: integer;
	maradek		: integer;
	elokezdes	: boolean;
	kesobefej	: boolean;
	bedat, beido, kidat, kiido : string;

	arf			: double;
	nap			: integer;
	usd, ft		: double;
	kellnap		: boolean;
begin
	PrintBand	:= reszlet;
	nap			:= 0;
	usd			:= 0;
	ft			:= 0;
	Q1.Caption 	:= IntToStr(sorszam);
	Inc(sorszam);
	Q2.Caption 	:= SGRekordok.Cells[ 0,rekordszam] + ' ' + SGRekordok.Cells[ 1,rekordszam];
	if QueryJarat.Locate('JA_KOD',SGRekordok.Cells[ 4,rekordszam],[loCaseInsensitive, loPartialKey]) then begin
		jaratstr 	:= GetJaratszamFromDb(QueryJarat);
	end else begin
		jaratstr	:= '! '+SGRekordok.Cells[ 4,rekordszam];
	end;
	if SGRekordok.Cells[ 4,rekordszam] <> SGRekordok.Cells[ 7,rekordszam] then begin
		 if QueryJarat.Locate('JA_KOD', SGRekordok.Cells[ 7,rekordszam],[loCaseInsensitive, loPartialKey]) then begin
			jaratstr	:= jaratstr + '; '+GetJaratszamFromDb(QueryJarat);
		 end else begin
			jaratstr	:= jaratstr + '; '+'! '+SGRekordok.Cells[ 7,rekordszam];
		 end;
	end;
	Q6.Caption 	:= jaratstr;

	// D�tumok + napszam meghat�roz�sa
	bedat		:= SGRekordok.Cells[ 2,rekordszam];
	beido		:= SGRekordok.Cells[ 3,rekordszam];
	kidat		:= SGRekordok.Cells[ 5,rekordszam];
	kiido		:= SGRekordok.Cells[ 6,rekordszam];
	Jodatum(bedat);
	Jodatum(kidat);
	Joido(beido);
	Joido(kiido);
	elokezdes	:= false;
	kesobefej	:= false;
	maradek		:= 0;
	if dattol > bedat then begin
		bedat 		:=  dattol;
		elokezdes	:= true;
		// Ki kell sz�molni a megel�z� id�szak �r�it!!!
		ora		:= StrToIntDef(copy(beido, 1, Pos(':', beido)-1) ,0);
		perc	:= StrToIntDef(copy(beido, Pos(':', beido)+1, 10),0);
		maradek	:= 24 * 60 - ora * 60 - perc;
		beido 	:= '00:00';
	end;
	if datig < kidat then begin
		kidat 		:=  datig;
		kiido 		:= '24:00';
		kesobefej	:= true;
	end;
	dat			:= bedat;
	if dat = '' then begin
		dat		:= DatumHozNap(kidat, 1, true);
	end;
	while dat <= kidat do begin
		kellnap	:= false;
		if dat = bedat then begin
			// Megegyezik a kezd�nappal, csak a beid� ut�ni id� sz�m�t
			if elokezdes then begin
				// A d�tum el�tt kezdt�k el a kintl�tet
				if dat = kidat then begin
					ora		:= StrToIntDef(copy(kiido, 1, Pos(':', kiido)-1) ,0);
					perc	:= StrToIntDef(copy(kiido, Pos(':', kiido)+1, 10),0);
					ido		:= ora * 60 + perc;
					if ( maradek + ido ) >= 32*60  then begin
						kellnap	:= true;
					end else begin
						if ( ( maradek < 480 ) and ( maradek + ido >= 480 ) ) then begin
							kellnap	:= true;
						end;
					end;
				end else begin
					kellnap	:= true;
				end;
			end else begin
				ora		:= StrToIntDef(copy(beido, 1, Pos(':', beido)-1) ,0);
				perc	:= StrToIntDef(copy(beido, Pos(':', beido)+1, 10),0);
				ido		:= 24 * 60 - ora * 60 - perc;
				if dat = kidat then begin
					// A befejez� nappal is megegyezik, csak a k�t d�tum k�z�tti k�l�nbs�g sz�m�t
					ora		:= StrToIntDef(copy(kiido, 1, Pos(':', kiido)-1) ,0);
					perc	:= StrToIntDef(copy(kiido, Pos(':', kiido)+1, 10),0);
					ido		:= ido - ( 24 * 60 - ora * 60 - perc );
					if ido >= 480 then begin
						kellnap	:= true;
					end;
				end else begin
					maradek	:= ido;
					if ido >= 480 then begin
						kellnap	:= true;
					end;
			   end;
			end;
		end else begin
			if dat = kidat then begin
				// Megegyezik a befejez� nappal, csak a kiid� el�tti id� sz�m�t
				if kesobefej then begin
					// A befejez�s k�s�bb t�rt�nik meg
					kellnap	:= true;
				end else begin
					ora		:= StrToIntDef(copy(kiido, 1, Pos(':', kiido)-1) ,0);
					perc	:= StrToIntDef(copy(kiido, Pos(':', kiido)+1, 10),0);
					ido		:= ora * 60 + perc;
					// Ha a benapon kevesebb id�t t�lt�tt�nk el, mint 8 �ra, akkor hozz�adjuk a kiid�h�z
					if ( ido + maradek ) >= 32 * 60 then begin
						kellnap	:= true;
					end else begin
						if ( ( maradek < 480 ) and ( maradek + ido >= 480 ) ) then begin
							kellnap	:= true;
						end;
					end;
				end;
			end else begin
				kellnap	:= true;
			end;
		end;
		if kellnap then begin
			arf	:= GetLocalArf(dat);
			usd	:= usd + napidij;
			ft	:= ft  + napidij * arf;
			nap	:= nap + 1;
		end;
		dat	:= DatumHozNap(dat, 1, true);
	end;

	Q3.Caption	:= Format('%4d',  [nap]);;
	Q5.Caption 	:= Format('%.2f', [usd]);
	Q7.Caption	:= Format('%.0f', [ft]);
	L1.Caption	:= bedat;
	if elokezdes then begin
		L1.Caption	:= '* '+bedat;
	end;
	L2.Caption	:= beido;
	L3.Caption	:= kidat;
	if kesobefej then begin
		L3.Caption	:= '* '+kidat;
	end;
	L4.Caption	:= kiido;
	donap		:= donap + nap;
	dousd		:= dousd + StringSzam(Q5.Caption);
	doft		:= doft  + StringSzam(Q7.Caption);
	PrintBand	:= reszlet;
end;

procedure TNapiListDlg.QRBand2BeforePrint(Sender: TQRCustomBand;
	   var PrintBand: Boolean);
begin
	QLO1.Caption 	:= Format('%12.2f', [osusd]);
	QLO2.Caption 	:= Format('%10.0f', [osfrt]);
	QLNAP.Caption 	:= Format('%4d', 	[osnap]);
end;

procedure TNapiListDlg.RepBeforePrint(Sender: TCustomQuickRep;
	   var PrintReport: Boolean);
begin
	osusd			:= 0;
	osfrt			:= 0;
	sorszam			:= 1;
	osnap			:= 0;
	donap			:= 0;
	dousd			:= 0;
	doft			:= 0;
	rekordszam		:= 0;
end;

procedure TNapiListDlg.RepNeedData(Sender: TObject; var MoreData: Boolean);
begin
	MoreData	:= SGRekordok.RowCount > rekordszam;
end;

function	TNapiListDlg.GetLocalArf(datu : string) : double;
var
	arfsor	: integer;
begin
	Result	:= 0;
	arfsor		:= ArfolyamGrid.Cols[0].IndexOf(datu);
	if arfsor > -1 then begin
		Result	:= StringSzam(ArfolyamGrid.Cells[1,arfsor]);
	end;
end;


procedure TNapiListDlg.ChildBand1BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
	PrintBand := false;
	// Csak akkor mutatjuk meg, ha a dolgoz�n�v v�ltozik
	if rekordszam	>= SGRekordok.RowCount then begin
		PrintBand	:= true;
	end else begin
		if SGRekordok.Cells[ 0,rekordszam-1] <> SGRekordok.Cells[ 0,rekordszam] then begin
			PrintBand	:= true;
		end;
	end;
	if PrintBand then begin
		O1.Caption 	:= SGRekordok.Cells[ 0,rekordszam-1] + ' ' + SGRekordok.Cells[ 1,rekordszam-1];
		osnap		:= osnap + donap;
		osusd		:= osusd + dousd;
		osfrt		:= osfrt + doft;
		O2.Caption 	:= Format('%4d', [donap]);
		O4.Caption 	:= Format('%12.2f', [dousd]);
		O5.Caption 	:= Format('%10.0f', [doft]);
		donap		:= 0;
		dousd		:= 0;
		doft		:= 0;
		sorszam		:= 1;
	end;
end;

procedure TNapiListDlg.QRBand1AfterPrint(Sender: TQRCustomBand;
  BandPrinted: Boolean);
begin
	Inc(rekordszam);
end;

end.
