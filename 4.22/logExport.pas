unit LogExport;

interface

uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls, Egyeb,
  Forms, Dialogs, DBTables, DB, StdCtrls, Buttons, Mask, DBCtrls,
  ExtCtrls, ADODB;

type
  TLogExportDlg = class(TForm)
	 BitBtn6: TBitBtn;
	 Label3: TLabel;
	 M0: TMaskEdit;
	 M1: TMaskEdit;
	 Label1: TLabel;
	 BitBtn2: TBitBtn;
	 Label2: TLabel;
	 M2: TMaskEdit;
	 BitBtn3: TBitBtn;
	 Label4: TLabel;
	 Query1: TADOQuery;
	 M3: TMaskEdit;
	 BitBtn1: TBitBtn;
	 Label5: TLabel;
	 M4: TMaskEdit;
	 BitElkuld: TBitBtn;
	 procedure FormCreate(Sender: TObject);
	 procedure BitBtn6Click(Sender: TObject);
	 procedure M1Exit(Sender: TObject);
	 procedure BitElkuldClick(Sender: TObject);
	 procedure BitBtn1Click(Sender: TObject);
	 procedure M3Change(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
  private
		kilepes	: boolean;
  public
  end;

var
  LogExportDlg: TLogExportDlg;

implementation

uses
	J_VALASZTO, KilizingList, Kozos, J_SQL, Lgauge;
{$R *.DFM}

procedure TLogExportDlg.FormCreate(Sender: TObject);
begin
	EgyebDlg.SetADOQueryDatabase(Query1);
	// Az adatok felt�lt�se
	kilepes	:= true;
	Query_Run(Query1, 'SELECT MAX(KI_DATUM) MAXDAT, MIN(KI_DATUM) MINDAT, COUNT(*) DARAB FROM UPDATEDB');
	M0.Text	:= Query1.FieldByName('DARAB').AsString;
	M1.Text	:= Query1.FieldByName('MINDAT').AsString;
	M2.Text	:= Query1.FieldByName('MAXDAT').AsString;
//	SetMaskEdit([M0, M1, M2, M4]);
	// A d�tum meghat�roz�sa
	M3.Text	:= copy(M2.Text, 1, 8)+'01.';
	if M1.Text > M3.text then begin
		M3.Text	:= M1.Text;
	end;
	// Az �llom�nyn�v meghat�roz�sa
	M4.Text := 'LOG'+copy(M3.Text,1,4)+copy(M3.Text,6,2)+copy(M3.Text,9,2)+'.txt';
	SetMaskEdits([M0, M1, M2, M4]);
	BitBtn2.Enabled	:= false;
	BitBtn3.Enabled	:= false;
end;

procedure TLogExportDlg.BitBtn2Click(Sender: TObject);
begin
   EgyebDlg.CalendarBe_idoszak(M1, M2);
end;

procedure TLogExportDlg.BitBtn3Click(Sender: TObject);
begin
   EgyebDlg.CalendarBe(M2);
end;

procedure TLogExportDlg.BitBtn6Click(Sender: TObject);
begin
	if not kilepes then begin
		kilepes	:= true;
	end else begin
		Close;
	end;
end;

procedure TLogExportDlg.M1Exit(Sender: TObject);
begin
	DatumExit(Sender);
end;

procedure TLogExportDlg.BitElkuldClick(Sender: TObject);
var
	nr		: integer;
	F       : TextFile;
	fn		: string;
	recsz	: integer;
	tordat	: string;
begin
	if not Jodatum2(M3) then begin
		Exit;
	end;
	Screen.Cursor	:= crHourGlass;
	// Az export�l�s v�grehajt�sa
	BitElkuld.Hide;
	fn	  		:= 'LOG'+copy(M3.Text,1,4)+copy(M3.Text,6,2)+copy(M3.Text,9,2);
	M4.Text 	:= fn+'.txt';
	nr			:= 1;
	kilepes		:= false;
	while FileExists(fn+'.txt') do begin
		fn 	   	:= 'LOG'+copy(M3.Text,1,4)+copy(M3.Text,6,2)+copy(M3.Text,9,2)+'_'+IntToStr(nr);
		M4.Text := fn+'.txt';
		Inc(nr);
	end;
	fn		:= EgyebDlg.TempList+M4.Text;
	try
	if not Query_Run(Query1, 'SELECT * FROM UPDATEDB WHERE KI_DATUM < '''+M3.Text+''' ORDER BY KI_DATUM, KI_IDOPT ', false) then begin
		NoticeKi('Nem siker�lt az �llom�ny lek�rdez�se!');
	end;
	except
	end;
	{$I-}
	AssignFile(F, fn);
	if FileExists(fn) then begin
		Append(F)
	end else begin
		Rewrite(F);
	end;
	{$I+}
	if IOResult = 0 then begin
		// A mez�nevek ki�r�sa
		for nr:= 0 to Query1.FieldCount -1 do begin
			Write(F, Query1.Fields[nr].FieldName + Chr(9));
		end;
		Writeln(F, '');
		recsz	:= 0;
		Application.CreateForm(TFormGaugeDlg, FormGaugeDlg);
		FormGaugeDlg.GaugeNyit('LOG adatok ment�se ... ' , IntToStr(Query1.RecordCount)+' db LOG rekord ki�r�sa ', false);
		Query1.DisableControls;
		while not Query1.Eof do begin
			if recsz mod 100 = 0 then begin
				Application.ProcessMessages;
				FormGaugeDlg.Pozicio ( ( recsz * 100 ) div Query1.RecordCount ) ;
			end;
			Inc(recsz);
			for nr:= 0 to Query1.FieldCount -1 do begin
				Write(F, Query1.FieldByName(Query1.Fields[nr].FieldName).AsString+ Chr(9) );
			end;
			Writeln(F, '');
			if kilepes then begin
				Query1.Last;
			end;
			Query1.Next;
		end;
		Query1.EnableControls;
		FormGaugeDlg.Destroy;
	end;
	Flush(f);
	CloseFile(f);
	// Az elemek t�rl�se
	if kilepes then begin
		NoticeKi('Felhaszn�l�i megszak�t�s!');
		DeleteFile(PChar(fn));
	end else begin
		tordat	:= M1.Text;
		while tordat < M3.Text do begin
			tordat	:= DatumHozNap(tordat, 7, false) ;
			if tordat > M3.Text then begin
				tordat	:= M3.Text;
			end;
			Query_Run(Query1, 'DELETE FROM UPDATEDB WHERE KI_DATUM < '''+tordat+''' ')
//		if not then begin
//			DeleteFile(PChar(fn));
		end;
	end;
	BitElkuld.Show;
	kilepes	:= true;
	Screen.Cursor	:= crDefault;
	Close;
end;

procedure TLogExportDlg.BitBtn1Click(Sender: TObject);
begin
	EgyebDlg.Calendarbe(M3);
	M4.Text := 'LOG'+copy(M3.Text,1,4)+copy(M3.Text,6,2)+copy(M3.Text,9,2)+'.txt';
end;

procedure TLogExportDlg.M3Change(Sender: TObject);
begin
	if Jodatum2(M3) then begin
		M4.Text := 'LOG'+copy(M3.Text,1,4)+copy(M3.Text,6,2)+copy(M3.Text,9,2)+'.txt';
	end;
end;

end.


