unit fizlis2;

interface

uses
  WinTypes, WinProcs, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, QuickRpt, QRExpr, Qrctrls, ExtCtrls, printers, DB, DBTables, Egyeb, Forgalom, J_SQL,
  Grids, Kozos, ADODB ;

type
  TFizlisDlg = class(TForm)
    Rep: TQuickRep;
    QRBand3: TQRBand;
    QRLSzamla: TQRLabel;
    QRBand1: TQRBand;
    QRLabel15: TQRLabel;
    QRLabel16: TQRLabel;
    QRLabel17: TQRLabel;
    QRLabel6: TQRLabel;
    QRSysData1: TQRSysData;
    QRLabel7: TQRLabel;
    QRSysData4: TQRSysData;
    QRLabel8: TQRLabel;
    QRLabel3: TQRLabel;
    QRShape1: TQRShape;
    QRShape3: TQRShape;
    QRShape4: TQRShape;
    QRShape5: TQRShape;
    QRShape6: TQRShape;
    QRShape7: TQRShape;
    QRShape8: TQRShape;
    QRLabel4: TQRLabel;
    QRLabel9: TQRLabel;
    QRLabel12: TQRLabel;
    QRLabel22: TQRLabel;
    QRLabel24: TQRLabel;
    QRShape2: TQRShape;
    QRShape9: TQRShape;
    QRShape10: TQRShape;
    QRShape11: TQRShape;
    QRShape12: TQRShape;
    QRShape13: TQRShape;
    QRShape14: TQRShape;
    SL8: TQRLabel;
    QRBand2: TQRBand;
    QRLabel21: TQRLabel;
    QRShape15: TQRShape;
    QL3: TQRLabel;
    QRLabel10: TQRLabel;
    QRLabel11: TQRLabel;
    SL6: TQRLabel;
    QL4: TQRLabel;
    QRShape16: TQRShape;
    SG1: TStringGrid;
    SL1: TQRLabel;
    SL2: TQRLabel;
    SL3: TQRLabel;
    SL4: TQRLabel;
    SL5: TQRLabel;
    QRShape17: TQRShape;
    QRLabel1: TQRLabel;
    QRShape18: TQRShape;
    SL7A: TQRLabel;
    QL2A: TQRLabel;
    QRShape19: TQRShape;
    QRShape20: TQRShape;
    QL5: TQRLabel;
    QRShape21: TQRShape;
    QL2B: TQRLabel;
	 QRShape25: TQRShape;
    QRShape26: TQRShape;
    SL7b: TQRLabel;
    QRLabel13: TQRLabel;
    Query1: TADOQuery;
    Query2: TADOQuery;
    Query3: TADOQuery;
    Query4: TADOQuery;
    Query5: TADOQuery;
    QRShape22: TQRShape;
    QRLabel2: TQRLabel;
    QL5A: TQRLabel;
    QRShape23: TQRShape;
    SL5A: TQRLabel;
    QRShape27: TQRShape;
    SL7: TQRLabel;
    QJarat: TADOQuery;
    QViszony: TADOQuery;
    procedure FormCreate(Sender: TObject);
    procedure	Tolt(sofor, datol, datig, sofnev: string; mozgoftkm, jarbontas, fizlist_csehtran : boolean );
    procedure QRBand1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure RepBeforePrint(Sender: TCustomQuickRep;
      var PrintReport: Boolean);
    procedure QRBand2BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure RepNeedData(Sender: TObject; var MoreData: Boolean);
    procedure Sajat_JaratEllenor( kod : string );
    procedure 	GetMozgoBer(sof, dat1, dat2 : string);
  private
     befdat			: string;
     sorszam			: integer;
     kifkm				: longint;
     tenykm			: longint;
  public
	  vanadat			: boolean;
	  kelluzenet		: boolean;
  end;

var
  FizlisDlg: TFizlisDlg;

implementation

uses
   J_Fogyaszt, Math;

{$R *.DFM}

procedure TFizlisDlg.FormCreate(Sender: TObject);
begin
	EgyebDlg.SetADOQueryDatabase(Query1);
	EgyebDlg.SetADOQueryDatabase(Query2);
	EgyebDlg.SetADOQueryDatabase(Query3);
	EgyebDlg.SetADOQueryDatabase(Query4);
	EgyebDlg.SetADOQueryDatabase(Query5);
	EgyebDlg.SetADOQueryDatabase(QJarat);
	EgyebDlg.SetADOQueryDatabase(QViszony);
	befdat		:= '';
	kelluzenet	:= true;
end;

procedure	TFizlisDlg.Tolt(sofor, datol, datig, sofnev: string; mozgoftkm, jarbontas, fizlist_csehtran: boolean );
var
	ert 		: double;
	ert2 		: double;
  	elszkm   	: double;
  	sqlstr		: string;
  	ssz			: integer;
	viszony		: string;
  	kovnap		: string;
  	elteltev 	: integer;
  	belepdat	: string;
  	mozgober	: double;
	extra		: double;
  	felra		: double;
   csehtr      : double;
   elszkmo     : double;   // �sszes elsz�molhat� km
   mozkmft     : double;   // Mozg�b�rhez tartoz� Ft/km �rt�k
   soforkm     : double;   // A sof�r �ltal megtett km
   FogyasztasDlg: TFogyasztasDlg;
   kezdokm	    : integer;
   vegzokm	    : integer;
   k1		    : integer;
   k2		    : integer;
   str         : string;
   CsehTranOsszeg: integer;
begin
   CsehTranOsszeg := Floor(StringSzam(EgyebDlg.Read_SZGrid ('999','927')));
	{A fejl�c kit�lt�se}
  	befdat	:= datig;
	if sofor <> '' then begin
		QRLabel16.Caption	:= sofnev;
	end else begin
		QRLabel16.Caption	:= 'Minden sof�r';
	end;
	QRLabel17.Caption	:= datol + ' - ' + datig;
	Application.CreateForm(TFogyasztasDlg, FogyasztasDlg);
	{A grid felt�lt�se}
	ssz				:= 0;
   SG1.ColCount    := 11;
	SG1.RowCount 	:= 1;
	SG1.Rows[0].Clear;
	EllenListTorol;
	Query_Run(QViszony, 'SELECT * FROM VISZONY ORDER BY VI_JAKOD ');
   sqlstr  := 'SELECT * FROM JARAT WHERE JA_JKEZD >= '''+datol+''' '+' AND JA_JKEZD <= '''+datig+''' '+' ORDER BY JA_JKEZD ';
   if jarbontas then begin
       sqlstr  :=  'SELECT * FROM JARAT '+
                   ' WHERE ( ( JA_JKEZD >= '''+datol+''' AND JA_JKEZD <= '''+datig+''' ) OR '+
			        ' ( JA_JVEGE >= '''+datol+''' AND JA_JVEGE <= '''+datig+''' ) OR '+
				    ' ( JA_JKEZD <  '''+datol+''' AND JA_JVEGE >  '''+datig+''' ) ) '+
                   ' ORDER BY JA_JKEZD ';
   end;
	if Query_Run(Query1, sqlstr ) then begin
		{Ezek a j�rat �sszegek}
		SG1.RowCount 	:= Query1.RecordCount;
		ssz				:= 0;
		kifkm			:= 0;
		tenykm   		:= 0;
       elszkmo         := 0;
       if sofor = '' then begin
           Query_Run(QJarat, 'SELECT JS_JAKOD FROM JARSOFOR ORDER BY JS_JAKOD ');
       end else begin
           Query_Run(QJarat, 'SELECT JS_JAKOD FROM JARSOFOR WHERE JS_SOFOR = '''+sofor+''' ORDER BY JS_JAKOD');
       end;
		while not Query1.EOF do begin
			// R�poz�cion�lunk a j�ratra �s ellen�rizz�k
			if StrToIntDef(Query1.FieldByname('JA_FAZIS').AsString,0) < 1 then begin
				{M�g nem ellen�rz�tt j�rat}
				EllenListAppend('Nem ellen�rz�tt j�rat : ('+ Query1.FieldByName('JA_KOD').AsString + ' ) ' + GetJaratszamFromDb(Query1), true);
			end;
			if not QJarat.Locate('JS_JAKOD', Query1.FieldByname('JA_KOD').AsString, []) then begin
				// Az adott j�ratban nem szerepelt a sof�r
				Query1.Next;
				Continue;
			end;
			if sofor = '' then begin
				Query_Run(Query5, 'SELECT SUM(JS_SZAKM) OSSZKM FROM JARSOFOR WHERE JS_JAKOD = '''+Query1.FieldByname('JA_KOD').AsString +	''' ');
			end else begin
				Query_Run(Query5, 'SELECT SUM(JS_SZAKM) OSSZKM FROM JARSOFOR WHERE JS_JAKOD = '''+Query1.FieldByname('JA_KOD').AsString+''' AND JS_SOFOR = '''+sofor+''' ');
			end;
			{A t�nyleges �s a kifizetett km -ek meghat�roz�sa}
			kifkm	  			:= kifkm  + Round(StringSZam(Query5.FieldByname('OSSZKM').AsString));
			tenykm   			:= tenykm + Round(StringSzam(Query1.FieldByname('JA_OSSZKM').AsString));
			SG1.Cells[0,ssz]	:= IntToStr(ssz+1);
			SG1.Cells[1,ssz]	:= Query1.FieldByName('JA_JKEZD').AsString;
			SG1.Cells[2,ssz]	:= GetJaratszam(Query1.FieldByName('JA_KOD').AsString);
			// SOS j�rat eset�n berakunk egy # -ot a j�ratsz�m el�
			Query_Run(Query5, 'SELECT MAX(MB_SOSFL) MAXFL FROM MEGBIZAS WHERE MB_JAKOD = '''+Query1.FieldByName('JA_KOD').AsString+''' ');
			if StrToIntdef(Query5.FieldByName('MAXFL').AsString, 0) > 0 then begin
				SG1.Cells[2,ssz]	:= '# '+SG1.Cells[2,ssz];
			end;
			SG1.Cells[3,ssz]	:= Query1.FieldByName('JA_RENDSZ').AsString;
			viszony				:= '';
			if QViszony.Locate('VI_JAKOD',Query1.FieldByName('JA_KOD').AsString,[loCaseInsensitive, loPartialKey]) then begin
				while ( ( not QViszony.Eof ) and ( QViszony.FieldByName('VI_JAKOD').AsString = Query1.FieldByName('JA_KOD').AsString ) ) do begin
					viszony	:=	viszony + QViszony.FieldByName('VI_HONNAN').AsString+ ' - '+QViszony.FieldByName('VI_HOVA').AsString+'; ';
					QViszony.Next;
				end;
			end;
			SG1.Cells[4,ssz]	:= viszony;
			if sofor = '' then begin
				{Minden sof�r sz�m�t}
				Query_Run(Query5, 'SELECT SUM(JS_SZAKM*JS_JADIJ) AS DIJ1, SUM(JS_EXDIJ) AS DIJ2, SUM(JS_FELRA) AS DIJ3, SUM(JS_SZAKM) AS ELKM, SUM(JS_CSEHTRAN) AS CSEHT FROM JARSOFOR '+
					' WHERE JS_JAKOD = '''+Query1.FieldByname('JA_KOD').AsString+ ''' ');
				ert  				:= 	StringSZam(Query5.FieldByname('DIJ1').AsString);
				extra				:= 	StringSZam(Query5.FieldByname('DIJ2').AsString);
				elszkm				:=  StringSZam(Query5.FieldByname('ELKM').AsString);
				felra				:=  StringSZam(Query5.FieldByname('DIJ3').AsString);
               csehtr:=  StringSZam(Query5.FieldByname('CSEHT').AsString);
  			    SG1.Cells[5,ssz]	:= Format('%10.0f',[elszkm]);
				if elszkm = 0 then begin
					SG1.Cells[6,ssz]	:= '0';	{d�j}
				end else begin
					SG1.Cells[6,ssz]	:= Format('%10.2f',[ert / elszkm]);	{d�j}
				end;
			end else begin
				Query_Run(Query5, 'SELECT SUM(JS_SZAKM*JS_JADIJ) AS DIJ1, SUM(JS_EXDIJ) AS DIJ2, SUM(JS_FELRA) AS DIJ3, SUM(JS_SZAKM) AS ELKM, SUM(JS_CSEHTRAN) AS CSEHT FROM JARSOFOR '+
					' WHERE JS_JAKOD = '''+Query1.FieldByname('JA_KOD').AsString+ ''' '+
					' AND JS_SOFOR = '''+sofor+''' ');
				ert  				:= 	StringSZam(Query5.FieldByname('DIJ1').AsString);
				extra				:= 	StringSZam(Query5.FieldByname('DIJ2').AsString);
				elszkm				:=  StringSZam(Query5.FieldByname('ELKM').AsString);
				felra				:=  StringSZam(Query5.FieldByname('DIJ3').AsString);
               csehtr              :=  StringSZam(Query5.FieldByname('CSEHT').AsString);
               // Megn�zz�k az elsz�molhat� km-t �s korrig�ljuk ha van jarbontas
               if jarbontas then begin
                   str	:= DatumHozNap(datol, -1, true);
                   Query_Run(Query2, 'SELECT ZK_KMORA FROM ZAROKM WHERE ZK_RENSZ = '''+Query1.FieldByName('JA_RENDSZ').AsString+''' AND ZK_DATUM = '''+str+''' ', true);
                   kezdokm	:= StrToIntDef(Query2.FieldByName('ZK_KMORA').AsString,0);
                   Query_Run(Query2, 'SELECT ZK_KMORA FROM ZAROKM WHERE ZK_RENSZ = '''+Query1.FieldByName('JA_RENDSZ').AsString+'''  AND ZK_DATUM = '''+datig+''' ', true);
                   vegzokm	:= StrToIntDef(Query2.FieldByName('ZK_KMORA').AsString,0);
                   FogyasztasDlg.ClearAllList;
                   k1	:=  StrToIntDef(Query1.FieldByName('JA_NYITKM').AsString, 0);
                   if k1 < kezdokm then begin
                       k1 := kezdokm;
                   end;
                   k2	:=  StrToIntDef(Query1.FieldByName('JA_ZAROKM').AsString, 0);
                   if k2 > vegzokm then begin
                       k2 := vegzokm;
                   end;
                   FogyasztasDlg.GeneralBetoltes(Query1.FieldByName('JA_RENDSZ').AsString, k1, k2);
                   FogyasztasDlg.CreateTankLista;
                   FogyasztasDlg.CreateAdBlueLista;
                   FogyasztasDlg.Szakaszolo;
                   soforkm := FogyasztasDlg.GetSoforKm(sofor);
                   if StrToIntDef(Query1.FieldByName('JA_ZAROKM').AsString, 0) <> StrToIntDef(Query1.FieldByName('JA_NYITKM').AsString, 0) then begin
                       elszkm  := ( elszkm * soforkm ) / (StrToIntDef(Query1.FieldByName('JA_ZAROKM').AsString, 0) - StrToIntDef(Query1.FieldByName('JA_NYITKM').AsString, 0)) ;
                       ert     := ( ert * soforkm ) / (StrToIntDef(Query1.FieldByName('JA_ZAROKM').AsString, 0) - StrToIntDef(Query1.FieldByName('JA_NYITKM').AsString, 0)) ;
				        SG1.Cells[5,ssz]	:= Format('%10.0f',[elszkm]);
                   end;
               end;
               SG1.Cells[5,ssz]	:= Format('%10.0f',[elszkm]);
               if elszkm = 0 then begin
                   SG1.Cells[6,ssz]	:= '0';	{d�j}
               end else begin
                   SG1.Cells[6,ssz]	:= Format('%10.2f',[ert / elszkm]);	{d�j}
               end;
			end;
           elszkmo := elszkmo + elszkm;
			SG1.Cells[9,ssz]		:= '0';
			ert2					:= 0;
           if jarbontas then begin
               if StrToIntDef(Query1.FieldByName('JA_NYITKM').AsString, 0) < k1 then begin
                   extra := 0;
                   felra := 0;
               end;
           end;
			if felra > 0 then begin
				// A felrak�sok sz�ma nem 0 -> az extra d�jat meg kell n�velni
				ert2 := StringSzam(EgyebDlg.Read_SZGrid('CEG', '301')) * felra;
				SG1.Cells[9,ssz]	:= Format('%10.2f',[ert2] );
			end;
			SG1.Cells[8,ssz]	:= Format('%10.0f',[extra]);
           if not fizlist_csehtran then
               csehtr:=0;
           // SG1.Cells[7,ssz]	:= Format('%10.0f',[ert+extra+ert2+csehtr*4000]);
			// SG1.Cells[10,ssz]	:= Format('%10.0f',[csehtr*4000]);
           SG1.Cells[7,ssz]	:= Format('%10.0f',[ert+extra+ert2+csehtr*CsehTranOsszeg]);
			SG1.Cells[10,ssz]	:= Format('%10.0f',[csehtr*CsehTranOsszeg]);
			Inc(ssz);
			Query1.Next;
		end;
	end;
	SG1.RowCount := ssz;
	if kelluzenet then begin
		EllenListMutat('Nem ellen�rz�tt j�ratok', true);
	end;
	{A mozg�b�r megjelen�t�se a kifizet�si list�n /csak ha teljes h�napr�l van sz�/ }
   if EgyebDlg.v_formatum <> 3 then begin
       // J&S -es sz�mol�si m�dszer
       GetMozgoBer(sofor, datol, datig);
       Inc(ssz);
   end else begin
       // A DK-n�l marad a r�gi
       kovnap	:= DatumHozNap(datig, 1, false);
       if (  ( copy(datol,9,2) = '01' ) and ( copy(datol,1,7) = copy(datig,1,7) ) and
           ( copy(kovnap,1,7) > copy(datig,1,7) ) and (sofor <> '') ) then begin
           {Egy eg�sz h�napr�l van sz�}
           if Query_Run(Query4, 'SELECT DO_CHMOZ, DO_BELEP FROM DOLGOZO WHERE DO_KOD = '''+sofor + ''' ',true) then begin
               if Query4.Fieldbyname('DO_CHMOZ').AsInteger > 0 then begin
                   {A dolgoz�nak j�r a mozg�b�r -> az �vek kisz�m�t�sa}
                   belepdat	:= Query4.Fieldbyname('DO_BELEP').AsString;
                   if 	belepdat = '' then begin
                       belepdat := datol;
                   end;
                   if EgyebDlg.MOZGOBER_NAPTARI_EVRE then begin
                       if copy(belepdat,6,5) = '01.01' then begin
                           elteltev := StrToIntDef(copy(datol,1,4),0) - StrToIntDef(copy(belepdat,1,4),0) + 1;
                       end else begin
                           elteltev := StrToIntDef(copy(datol,1,4),0) - StrToIntDef(copy(belepdat,1,4),0);
                       end;
                       if elteltev < 0 then begin
                           elteltev := 0;
                       end;
                   end else begin  // �j m�di 2017-02-13
                       elteltev:= floor (DatumDiffHonap (belepdat, datol) / 12);
                   end;
                   if not mozgoftkm then begin
                       // Sima mozg�b�rt sz�molunk, nem ft/km szerint
                       mozgober := StringSzam(EgyebDlg.Read_SZGrid( '120', IntToStr(elteltev)));
                       if mozgober > 0 then begin
                           {A havi b�r besz�r�sa}
                           SG1.RowCount 		:= ssz + 1;
                           SG1.Cells[0,ssz]	:= IntToStr(ssz+1);
                           SG1.Cells[1,ssz]	:= copy(datol,1,7);
                           SG1.Cells[2,ssz]	:= '';
                           SG1.Cells[3,ssz]	:= '';
                           SG1.Cells[4,ssz]	:= 'Havi mozg�b�r';
                           SG1.Cells[5,ssz]	:= '0';
                           SG1.Cells[6,ssz]	:= '0';
                           SG1.Cells[7,ssz]	:= Format('%10.1f',[mozgober]);
                           SG1.Cells[8,ssz]	:= '0';
                           SG1.Cells[9,ssz]	:= '0';
                           Inc(ssz);
                       end;
                   end else begin
                       // Mozg�b�r sz�mol�sa, ft/km szerint
                       mozkmft := StringSzam(EgyebDlg.Read_SZGrid( '122', IntToStr(elteltev)));
                       {A havi b�r besz�r�sa}
                       SG1.RowCount 		:= ssz + 1;
                       SG1.Cells[0,ssz]	:= IntToStr(ssz+1);
                       SG1.Cells[1,ssz]	:= copy(datol,1,7);
                       SG1.Cells[2,ssz]	:= '';
                       SG1.Cells[3,ssz]	:= '';
                       SG1.Cells[4,ssz]	:= 'Mozg�b�r Ft/km alapj�n ('+IntToStr(elteltev)+')';
                       SG1.Cells[5,ssz]	:= '0';
                       SG1.Cells[6,ssz]	:= Format('%10.2f',[mozkmft]);
                       SG1.Cells[7,ssz]	:= Format('%10.1f',[mozkmft*elszkmo]);
                       SG1.Cells[8,ssz]	:= '0';
                       SG1.Cells[9,ssz]	:= '0';
                       Inc(ssz);
                   end;
               end;
           end;
       end else begin
           // Ha nem teljes h�nap volt, akkor is van km szerinti �sszeg
           if Query_Run(Query4, 'SELECT DO_CHMOZ, DO_BELEP FROM DOLGOZO WHERE DO_KOD = '''+sofor + ''' ',true) then begin
               if Query4.Fieldbyname('DO_CHMOZ').AsInteger > 0 then begin
                   {A dolgoz�nak j�r a mozg�b�r -> az �vek kisz�m�t�sa}
                   belepdat	:= Query4.Fieldbyname('DO_BELEP').AsString;
                   if 	belepdat = '' then begin
                       belepdat := datol;
                   end;
                   if copy(belepdat,6,5) = '01.01' then begin
                       elteltev := StrToIntDef(copy(datol,1,4),0) - StrToIntDef(copy(belepdat,1,4),0) + 1;
                   end else begin
                       elteltev := StrToIntDef(copy(datol,1,4),0) - StrToIntDef(copy(belepdat,1,4),0);
                   end;
                   if elteltev < 0 then begin
                       elteltev := 0;
                   end;
                   // Mozg�b�r sz�mol�sa, ft/km szerint
                   mozkmft := StringSzam(EgyebDlg.Read_SZGrid( '122', IntToStr(elteltev)));
                   {A havi b�r besz�r�sa}
                   SG1.RowCount 		:= ssz + 1;
                   SG1.Cells[0,ssz]	:= IntToStr(ssz+1);
                   SG1.Cells[1,ssz]	:= copy(datol,1,7);
                   SG1.Cells[2,ssz]	:= '';
                   SG1.Cells[3,ssz]	:= '';
                   SG1.Cells[4,ssz]	:= 'Mozg�b�r Ft/km alapj�n ('+IntToStr(elteltev)+')';
                   SG1.Cells[5,ssz]	:= '0';
                   SG1.Cells[6,ssz]	:= Format('%10.2f',[mozkmft]);
                   SG1.Cells[7,ssz]	:= Format('%10.1f',[mozkmft*elszkmo]);
                   SG1.Cells[8,ssz]	:= '0';
                   SG1.Cells[9,ssz]	:= '0';
                   Inc(ssz);
               end;
           end;
       end;
   end;

  	{Egyedi k�lts�gek �sszes�t�se}
  	sqlstr := 'SELECT * FROM TEHER WHERE ( TH_DATUM >= '''+datol+''' AND TH_DATUM <= '''+datig+''' ) ';
	if sofor <> '' then begin
  		sqlstr	:= sqlstr + ' AND TH_DOKOD = '''+sofor+''' ';
  	end;
	sqlstr	:= sqlstr + ' ORDER BY TH_DATUM';
  	if Query_Run(Query1, sqlstr,true) then begin
  		while not Query1.EOF do begin
     		SG1.RowCount 		:= ssz + 1;
    		SG1.Cells[0,ssz]	:= IntToStr(ssz+1);
			SG1.Cells[1,ssz]	:= Query1.FieldByName('TH_DATUM').AsString;
			SG1.Cells[2,ssz]	:= '';
			SG1.Cells[3,ssz]	:= '';
			SG1.Cells[4,ssz]	:= Query1.FieldByName('TH_MEGJ').AsString;
			SG1.Cells[5,ssz]	:= '0';
			SG1.Cells[6,ssz]	:= '0';
			SG1.Cells[7,ssz]	:= Format('%10.1f',[Query1.FieldByName('TH_OSSZEG').AsFloat]);
			SG1.Cells[8,ssz]	:= '0';
			SG1.Cells[9,ssz]	:= '0';
			Inc(ssz);
			Query1.Next;
		end;
	end;
	vanadat	:= false;
	if SG1.Cells[0,0]	<> '' then begin
		vanadat	:= true;
	end;
   FogyasztasDlg.Destroy;
end;

procedure TFizlisDlg.QRBand1BeforePrint(Sender: TQRCustomBand;
	   var PrintBand: Boolean);
begin
	SL1.Caption	:=	SG1.Cells[0,sorszam];       // Sorsz�m
	SL2.Caption	:=	SG1.Cells[1,sorszam];       // Kelt
	SL3.Caption	:=	SG1.Cells[2,sorszam];       // J�ratsz�m
	SL4.Caption	:=	SG1.Cells[3,sorszam];       // Rendsz�m
	SL5.Caption	:=	SG1.Cells[4,sorszam];       // Viszonylat
	SL5A.Caption :=	Format('%10.0f',	[StringSzam(SG1.Cells[10,sorszam])]);    // Cseh transzport
	SL6.Caption	:=	Format('%10.0f',	[StringSzam(SG1.Cells[5,sorszam])]);    // Elsz.km
	SL7.Caption	:=	Format('%.2f',	[StringSzam(SG1.Cells[6,sorszam])]);        // Ft/km
	// SL7A.Caption:=	Format('%9.2f',	[StringSzam(SG1.Cells[8,sorszam])]);        // Felrak�
  SL7A.Caption:=	Format('%9.0f',	[StringSzam(SG1.Cells[8,sorszam])]);        // Felrak�
	// SL7B.Caption:=	Format('%9.2f',	[StringSzam(SG1.Cells[9,sorszam])]);        // Extra d�j
  SL7B.Caption:=	Format('%9.0f',	[StringSzam(SG1.Cells[9,sorszam])]);        // Extra d�j
	// SL8.Caption	:=	Format('%12.1f',	[StringSzam(SG1.Cells[7,sorszam])]);    // �sszeg
  SL8.Caption	:=	Format('%12.0f',	[StringSzam(SG1.Cells[7,sorszam])]);    // �sszeg
   Inc(sorszam);
end;

procedure TFizlisDlg.RepBeforePrint(Sender: TCustomQuickRep;
      var PrintReport: Boolean);
begin
  sorszam	:= 0;
end;

procedure TFizlisDlg.QRBand2BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
var
    ossz	: double;
    osszkm	: double;
    i		: integer;
    osszex	: double;
    osszfr	: double;
    osszcs : double;
begin
	osszkm	:= 0;
	ossz		:= 0;
  	osszex	:= 0;
  	osszfr	:= 0;
  	for i := 0 to SG1.RowCount - 1 do begin
		osszkm	:= osszkm 	+ StringSzam(SG1.Cells[5,i]);
		ossz   	:= ossz	 	+ StringSzam(SG1.Cells[7,i]);
     	osszex	:= osszex 	+ StringSzam(SG1.Cells[8,i]);
     	osszfr	:= osszfr 	+ StringSzam(SG1.Cells[9,i]);
     	osszcs	:= osszcs 	+ StringSzam(SG1.Cells[10,i]);
  	end;
	QL4.Caption 	:= Format('%9.0f',	[osszkm]);
	QL2A.Caption 	:= Format('%12.1f',	[osszex]);
	QL2B.Caption 	:= Format('%12.1f',	[osszfr]);
	QL3.Caption 	:= Format('%12.1f',	[ossz]);
	QL5A.Caption 	:= Format('%12.0f',	[osszcs]);
  	if osszkm = 0 then begin
		QL5.Caption 	:= '';
	end else begin
		QL5.Caption 	:= Format('%.1f',[ossz/osszkm]);
  	end;
end;

procedure TFizlisDlg.RepNeedData(Sender: TObject; var MoreData: Boolean);
begin
 	MoreData	:= true;
	if sorszam	>= SG1.RowCount then begin
  		MoreData	:= false;
  	end;
end;

procedure 	TFizlisDlg.Sajat_JaratEllenor( kod : string );
begin
	if Query1.Locate('JA_KOD',kod,[loCaseInsensitive, loPartialKey]) then begin
		if StrToIntDef(Query1.FieldByname('JA_FAZIS').AsString,0) < 1 then begin
			{M�g nem ellen�rz�tt j�rat}
			EllenListAppend('Nem ellen�rz�tt j�rat : ('+kod + ' ) ' + GetJaratszamFromDb(Query1), true);
		end;
	end;
end;

procedure 	TFizlisDlg.GetMozgoBer(sof, dat1, dat2 : string);
var
   mozgoftkm   : double;
   mozgober    : double;
   sorsz       : integer;
   elteltev    : integer;
   osszkm      : double;
   regiev      : double;
   mostev      : double;
   ii          : integer;
begin
   mozgober    := 0;
   mozgoftkm   := 0;

	osszkm	:= 0;
  	for ii := 0 to SG1.RowCount - 1 do begin
		osszkm	:= osszkm 	+ StringSzam(SG1.Cells[5,ii]);
  	end;

   Query_Run(Query4, 'SELECT DO_CHMOZ, DO_BELEP FROM DOLGOZO WHERE DO_KOD = '''+sof + ''' ',true);
   if StrToIntDef(Query4.Fieldbyname('DO_CHMOZ').AsString, 0) > 0 then begin
       // Adhat� mzog�b�r, csak ilyenkor sz�molunk
       elteltev  := StrToIntDef(copy(dat1, 1, 4), 0) - StrToIntDef(copy(Query4.Fieldbyname('DO_BELEP').AsString, 1, 4), 0);
       if ( ( elteltev > 100 ) or (elteltev < 0) ) then begin
           elteltev    := 0;
       end;
       // Ha az eltelt �v > 10 �s teljes a h�nap akkor + 50.000 DE CSAK J&S -n�l
       if EgyebDlg.v_formatum <> 3 then begin
           if ( ( elteltev > 10 ) and (GetFirstDay(dat1) = dat1 ) and (GetLastDay(dat2)= dat2) )  then begin
               mozgober    := mozgober + 50000;
           end;
       end;
(*
       // KM alapj�n megkeress�k a Ft/km �rt�ket
       Query_Run(Query4, 'SELECT * FROM SZOTAR WHERE SZ_FOKOD = ''123'' ',true);
       regikm      := 0;
       while not Query4.Eof do begin
           mostkm  := StringSzam(Query4.Fieldbyname('SZ_ALKOD').AsString);
           if ( ( mostkm >= regikm ) and ( mostkm <= osszkm) ) then begin
               mozgoftkm   := StringSzam(Query4.Fieldbyname('SZ_MENEV').AsString);
           end;
           Query4.Next;
       end;
*)
       // �V alapj�n megkeress�k a Ft/km �rt�ket
       Query_Run(Query4, 'SELECT * FROM SZOTAR WHERE SZ_FOKOD = ''123'' ',true);
       regiev      := -1;
       while not Query4.Eof do begin
           mostev  := StringSzam(Query4.Fieldbyname('SZ_ALKOD').AsString);
           if ( ( mostev >= regiev ) and ( mostev <= elteltev) ) then begin
               mozgoftkm   := StringSzam(Query4.Fieldbyname('SZ_MENEV').AsString);
               regiev  := mostev;
           end;
           Query4.Next;
       end;
       if mozgoftkm > 0 then begin
           mozgober    := mozgober + mozgoftkm*osszkm;
       end;
   end;

   {A havi b�r besz�r�sa}
   SG1.RowCount 		            := SG1.RowCount + 1;
   SG1.Cells[0,SG1.RowCount - 1]	:= IntToStr(SG1.RowCount);
   SG1.Cells[1,SG1.RowCount - 1]	:= copy(dat1,1,7);
   SG1.Cells[2,SG1.RowCount - 1]	:= '';
   SG1.Cells[3,SG1.RowCount - 1]	:= '';
   SG1.Cells[4,SG1.RowCount - 1]	:= 'Mozg�b�r ('+IntToStr(elteltev)+' �v; '+Format('%.0f',[osszkm])+' km )';
   SG1.Cells[5,SG1.RowCount - 1]	:= '0';
   SG1.Cells[6,SG1.RowCount - 1]	:= Format('%10.2f',[mozgoftkm]);
   SG1.Cells[7,SG1.RowCount - 1]	:= Format('%10.1f',[mozgober]);
   SG1.Cells[8,SG1.RowCount - 1]	:= '0';
   SG1.Cells[9,SG1.RowCount - 1]	:= '0';
end;


end.
