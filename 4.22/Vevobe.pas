unit Vevobe;

interface

uses
	SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
	Forms, Dialogs, StdCtrls, Buttons, Mask, DB, DBTables, J_ALFORM, ExtCtrls,
  ADODB, Grids, DBGrids, ComCtrls,ShellAPI, DBCtrls,StrUtils;
	
type
	TVevobeDlg = class(TJ_AlformDlg)
	BitElkuld: TBitBtn;
	BitKilep: TBitBtn;
    PageControl2: TPageControl;
    TabSheet6: TTabSheet;
    TabSheet7: TTabSheet;
    TabSheet9: TTabSheet;
    ScrollBox1: TScrollBox;
    GroupBox3: TGroupBox;
    Label42: TLabel;
    Label43: TLabel;
    Label44: TLabel;
    Label45: TLabel;
    Label46: TLabel;
    Label47: TLabel;
    Label48: TLabel;
    Label49: TLabel;
    M1: TMaskEdit;
    M2: TMaskEdit;
    M4: TMaskEdit;
    M5: TMaskEdit;
    M6: TMaskEdit;
    M8: TMaskEdit;
    M7: TMaskEdit;
    M3: TMaskEdit;
    ScrollBox2: TScrollBox;
    ScrollBox3: TScrollBox;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label9: TLabel;
    Label12: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label13: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    Label8: TLabel;
    Label18: TLabel;
    Label21: TLabel;
    Label14: TLabel;
    Label29: TLabel;
    Label37: TLabel;
    Label38: TLabel;
    Label40: TLabel;
    Label41: TLabel;
    Label50: TLabel;
    MaskEdit1: TMaskEdit;
    MaskEdit2: TMaskEdit;
    MaskEdit3: TMaskEdit;
    MaskEdit4: TMaskEdit;
    MaskEdit5: TMaskEdit;
    MaskEdit6: TMaskEdit;
    MaskEdit8: TMaskEdit;
    MaskEdit7: TMaskEdit;
    MaskEdit9: TMaskEdit;
    MaskEdit10: TMaskEdit;
    MaskEdit11: TMaskEdit;
    MaskEdit12: TMaskEdit;
    MaskEdit13: TMaskEdit;
    OrszagCombo: TComboBox;
    MaskEdit80: TMaskEdit;
    AFACombo: TComboBox;
    CheckBox4: TCheckBox;
    CheckBox2: TCheckBox;
    MaskEdit15: TMaskEdit;
    MaskEdit22: TMaskEdit;
    CheckBox7: TCheckBox;
    MaskEdit23: TMaskEdit;
    MaskEdit25: TMaskEdit;
    MaskEdit111: TMaskEdit;
    cbNYELV: TComboBox;
    CheckBox1: TCheckBox;
    cbUGYNYELV: TComboBox;
    meMobil: TMaskEdit;
    AdoOrszagkodCombo: TComboBox;
    Query1: TADOQuery;
    Label30: TLabel;
    CheckBox5: TCheckBox;
    CheckBox6: TCheckBox;
    MaskEdit24: TMaskEdit;
    Label39: TLabel;
    RadioGroup1: TRadioGroup;
    RadioGroup2: TRadioGroup;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    RadioGroup3: TRadioGroup;
    BitBtn1: TBitBtn;
    MaskEdit14: TMaskEdit;
    Panel3: TPanel;
    DBGrid3: TDBGrid;
    DBNavigator2: TDBNavigator;
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    Button4: TButton;
    TabSheet2: TTabSheet;
    Panel1: TPanel;
    BitBtn30: TBitBtn;
    BitBtn31: TBitBtn;
    BitBtn32: TBitBtn;
    DBGrid1: TDBGrid;
    TabSheet3: TTabSheet;
    Label22: TLabel;
    Label23: TLabel;
    Label24: TLabel;
    Label25: TLabel;
    Label26: TLabel;
    Label27: TLabel;
    Label28: TLabel;
    MaskEdit16: TMaskEdit;
    OrszagCombo2: TComboBox;
    MaskEdit17: TMaskEdit;
    MaskEdit18: TMaskEdit;
    CheckBox3: TCheckBox;
    MaskEdit19: TMaskEdit;
    MaskEdit20: TMaskEdit;
    MaskEdit21: TMaskEdit;
    TabSheet4: TTabSheet;
    DBGrid2: TDBGrid;
    Panel2: TPanel;
    BitBtn2: TBitBtn;
    BitBtn3: TBitBtn;
    BitBtn4: TBitBtn;
    DBNavigator1: TDBNavigator;
    TabSheet5: TTabSheet;
    Panel4: TPanel;
    BitBtn5: TBitBtn;
    BitBtn6: TBitBtn;
    BitBtn7: TBitBtn;
    DBGrid4: TDBGrid;
    GroupBox1: TGroupBox;
    Label31: TLabel;
    Label32: TLabel;
    Label33: TLabel;
    MaskEdit26: TMaskEdit;
    MaskEdit27: TMaskEdit;
    CBBank: TComboBox;
    GroupBox2: TGroupBox;
    Label34: TLabel;
    Label35: TLabel;
    Label36: TLabel;
    MaskEdit29: TMaskEdit;
    MaskEdit30: TMaskEdit;
    MaskEdit28: TMaskEdit;
    SzPldCombo: TComboBox;
    DataSource1: TDataSource;
    QueryDij: TADOQuery;
    QueryDijVD_VEKOD: TStringField;
    QueryDijVD_SORSZ: TIntegerField;
    QueryDijVD_TEKOD: TStringField;
    QueryDijT_TERNEV: TStringField;
    QueryDijVD_EGYAR: TBCDField;
    QueryDijVD_VANEM: TStringField;
    VevoDoc: TADODataSet;
    VevoDocVI_VEKOD: TStringField;
    VevoDocVI_SORSZ: TAutoIncField;
    VevoDocVI_DONEV: TStringField;
    VevoDocVI_DFILE: TStringField;
    VevoDocVI_DATUM: TDateTimeField;
    VevoDocVI_LEDAT: TDateTimeField;
    VevoDocVI_LEELL: TStringField;
    VevoDocVI_LEDATS: TStringField;
    DataSource2: TDataSource;
    OpenDialog1: TOpenDialog;
    UZEMFIX: TADODataSet;
    UZEMFIXUX_VEKOD: TStringField;
    UZEMFIXUX_EV: TStringField;
    UZEMFIXUX_H01: TBCDField;
    UZEMFIXUX_H02: TBCDField;
    UZEMFIXUX_H03: TBCDField;
    UZEMFIXUX_H04: TBCDField;
    UZEMFIXUX_H05: TBCDField;
    UZEMFIXUX_H06: TBCDField;
    UZEMFIXUX_H07: TBCDField;
    UZEMFIXUX_H08: TBCDField;
    UZEMFIXUX_H09: TBCDField;
    UZEMFIXUX_H10: TBCDField;
    UZEMFIXUX_H11: TBCDField;
    UZEMFIXUX_H12: TBCDField;
    DataSource3: TDataSource;
    DataSource4: TDataSource;
    QueryGk: TADOQuery;
    QueryGkAG_RENSZ: TStringField;
    QueryGkAG_TIPUS: TStringField;
    QueryGkAG_AGKOD: TIntegerField;
    TabSheet8: TTabSheet;
    ScrollBox4: TScrollBox;
    Label52: TLabel;
    Label51: TLabel;
    meRogzitette: TMaskEdit;
    meRogzitetteTimestamp: TMaskEdit;
    meModositottaTimestamp: TMaskEdit;
    meModositotta: TMaskEdit;
    BitBtn17: TBitBtn;
	procedure BitKilepClick(Sender: TObject);
	procedure FormCreate(Sender: TObject);
	procedure Tolto(cim : string; teko : string);override;
	procedure BitElkuldClick(Sender: TObject);
	procedure FormDestroy(Sender: TObject);
	procedure MaskEdit10KeyPress(Sender: TObject; var Key: Char);
	procedure MaskEdit10Exit(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit13KeyPress(Sender: TObject; var Key: Char);
    procedure MaskEdit13Exit(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure RadioGroup3Click(Sender: TObject);
    procedure MaskEdit14Exit(Sender: TObject);
    procedure MaskEdit14KeyPress(Sender: TObject; var Key: Char);
    procedure BitBtn30Click(Sender: TObject);
	 function  Elkuldes : boolean;
    procedure BitBtn31Click(Sender: TObject);
	 procedure BitBtn32Click(Sender: TObject);
    procedure CheckBox3Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure VevoDocAfterInsert(DataSet: TDataSet);
    procedure DBGrid2DblClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure DBGrid2KeyPress(Sender: TObject; var Key: Char);
    procedure BitBtn4Click(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure DBGrid2ColEnter(Sender: TObject);
    procedure DBGrid2DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure VevoDocVI_LEDATSetText(Sender: TField; const Text: String);
    procedure UZEMFIXAfterInsert(DataSet: TDataSet);
    procedure Button1Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure OrszagComboChange(Sender: TObject);
    procedure MaskEdit80Exit(Sender: TObject);
    procedure BitBtn5Click(Sender: TObject);
    procedure BitBtn6Click(Sender: TObject);
    procedure BitBtn7Click(Sender: TObject);
    procedure PageControl1Changing(Sender: TObject;
      var AllowChange: Boolean);
    procedure CBBankChange(Sender: TObject);
    procedure VevoDocBeforePost(DataSet: TDataSet);
    procedure SendVevo(muv : string);
    procedure MaskEdit5Change(Sender: TObject);
    function DuplikatumElfogadva(VevoNev, VevoVaros: string): boolean;
    procedure BitBtn17Click(Sender: TObject);
	private
		afalist 		: TStringList;
		orszaglist 	: TStringList;
		banklist 		: TStringList;
		orszaglist2	: TStringList;
    orszagkodlist	: TStringList;
    nyelvlist		: TStringList;
    vbank       : string;
	public
	end;

var
	VevobeDlg: TVevobeDlg;

implementation

uses
	J_SQL, Kozos, Potlekbe, vevoDijbe, Egyeb, DateUtils, AlGepbe, VCARDParams;
{$R *.DFM}


{************************************************
* Ad�sz�m ellen�rz�se
* Visszat�r�si �rt�k:
* -  0: J� ad�sz�m
* - -1: Rossz a kapott �rt�k hossza (csak 11 /elv�laszt�s n�lk�l/ vagy 13 /elv�laszt�ssal/ karakter lehet)
* - -2: A kapott �rt�k nem csak sz�mjegyet tartalmaz (kiv�ve: elv�laszt�s)
* - -3: A 9. helyen nem 1,2 vagy 3 szerepel (ad�mentes, ad�k�teles,EVA)
* - -4: Az utols� k�t sz�mjegy nem a k�vetkez�k egyike: 02-20, 22-44, 41
* - -5: A kapott �rt�k CDV hib�s
* K�rnyezet: Delphi 7
************************************************}
function CheckAdoszam(cAdo: string):integer;
const
  aCDV:array[1..4] of integer = (9,7,3,1);
var i: int64;
    j: integer;
    nCDV: integer;
    cTemp: string;
begin
  if not (length(cAdo) in [11,13]) then
  begin
    Result := -1;
    exit;
  end;

  if Length(cAdo)=11 then
  begin
    if not TryStrToInt64(cAdo,i) then
    begin
      Result := -2;
      exit;
    end;
    cTemp := cAdo;
  end
  else
  begin
    cTemp := copy(cAdo,1,8) + copy(cAdo,10,1) + copy(cAdo,12,2);
    if not TryStrToInt64(cTemp,i) then
    begin
      Result := -2;
      exit;
    end;
  end;

  if not (cTemp[9] in ['1','2','3','4','5']) then // b�v�tve 2016-02-29: csoportos ad�alanyis�got v�laszt� ad�alanyok = 4-es, 5-�s.
  begin
    Result := -3;
    exit;
  end;

  nCDV := StrToInt(copy(cTemp,10,2));
  if not(((nCDV>1) and (nCDV<21)) or ((nCDV>21) and (nCDV<45)) or (nCDV=51)) then
  begin
    Result := -4;
    exit;
  end;

  nCDV := 0;
  for j:=1 to 7 do
  begin
    nCDV := nCDV + StrToInt(cTemp[j])*aCDV[((j-1) mod 4)+1];
  end;

  if StrToInt(cTemp[8]) <> ((10-(nCDV mod 10)) mod 10) then
  begin
    Result := -5;
    exit;
  end;

  Result := 0;
end;



procedure TVevobeDlg.BitKilepClick(Sender: TObject);
begin
	ret_kod := '';
	Close;
end;

procedure TVevobeDlg.FormCreate(Sender: TObject);
var
	bakod	: string;
begin
	ret_kod := '';
	EgyebDlg.SetADOQueryDatabase(Query1);
	EgyebDlg.SetADOQueryDatabase(QueryDij);
	EgyebDlg.SetADOQueryDatabase(QueryGK);
  VevoDoc.Connection:=EgyebDlg.ADOConnectionKozos;
  UZEMFIX.Connection:=EgyebDlg.ADOConnectionKozos;
	SetMaskEdits([MaskEdit1]);
	afalist := TStringList.Create;
	SzotarTolt(AFACombo, '101' , afalist, false );
	orszaglist 	:= TStringList.Create;
	orszaglist2 := TStringList.Create;
  orszagkodlist := TStringList.Create;
  nyelvlist 	:= TStringList.Create;
	SzotarTolt(OrszagCombo,  '300' , orszaglist, false );
	SzotarTolt(OrszagCombo2, '300' , orszaglist2, false );
	SzotarTolt(AdoOrszagkodCombo,  '300' , orszagkodlist, true );  // a k�dokat tessz�k bele
	OrszagCombo.ItemIndex  	:= 0;
	OrszagCombo2.ItemIndex 	:= 0;
  AdoOrszagkodCombo.ItemIndex 	:= 0;
 	SzotarTolt(cbNyelv, '430', nyelvlist, false );
  cbNyelv.ItemIndex 	:= 0;
 	SzotarTolt(cbUgyNyelv, '430', nyelvlist, false );
  cbUgyNyelv.ItemIndex 	:= 0;
	RadioGroup1.ItemIndex	:= 0;
	RadioGroup2.ItemIndex	:= 0;
	// A bank adatok bet�lt�se
	banklist := TStringList.Create;
	CBBank.Items.Clear;
	Query_Run(Query1, 'SELECT * FROM SZOTAR WHERE SZ_FOKOD = ''320'' ORDER BY SZ_ALKOD ');
  CBBank.Items.Add('');
  banklist.Add('');
	while not Query1.Eof do begin
		bakod	:= Trim(Query1.FieldByName('SZ_ALKOD').AsString);
		if copy(bakod, Length(bakod), 1 ) = '1' then begin
       	bakod := IntToStr(StrToIntDef(bakod,0) + 1);
        //CBBank.Items.Add(Query1.FieldByName('SZ_MENEV').AsString + ', ' + EgyebDlg.Read_SZGrid('320', bakod) );
        CBBank.Items.Add(Query1.FieldByName('SZ_MENEV').AsString);
        banklist.Add(Query1.FieldByName('SZ_ALKOD').AsString);
    end;
		Query1.Next;
   end;
   CBBank.ItemIndex	:= 0;
   PageControl1.ActivePageIndex	:= 0;
end;

procedure TVevobeDlg.Tolto(cim : string; teko:string);
var
  bank: integer;
  mFEKOD: string;
begin
	ret_kod 	:= teko;
	Caption := cim;
	MaskEdit1.Text := '';
	MaskEdit2.Text := '';
	MaskEdit3.Text := '';
	MaskEdit4.Text := '';
	MaskEdit5.Text := '';
	MaskEdit6.Text := '';
	MaskEdit7.Text := '';
	MaskEdit8.Text := '';
	MaskEdit9.Text := '';
	MaskEdit10.Text := '';
	MaskEdit11.Text := '';
	MaskEdit111.Text:= '';
	MaskEdit12.Text := '';
	MaskEdit14.Text := '';
	MaskEdit23.Text := '';
	MaskEdit25.Text := '';
  meRogzitette.Text := '';
  meRogzitetteTimestamp.Text := '';
  meModositottaTimestamp.Text := '';
  meModositotta.Text := '';

	BitBtn31.Enabled	:= false;
	BitBtn32.Enabled	:= false;
	SzPldCombo.ItemIndex:=SzPldCombo.Items.IndexOf('0');  // alap�rtelmezett �rt�k
	if ret_kod <> '' then begin		{M�dos�t�s}
		if Query_Run(Query1, 'SELECT * FROM VEVO WHERE V_KOD = '''+ret_kod+''' ') then begin
			if Query1.RecordCount > 0 then begin
				MaskEdit1.Text 			:= Query1.FieldByName('V_KOD').AsString;
				MaskEdit2.Text 			:= Query1.FieldByName('V_NEV').AsString;
				MaskEdit3.Text 			:= Query1.FieldByName('V_IRSZ').AsString;
				MaskEdit4.Text 			:= Query1.FieldByName('V_VAROS').AsString;
				MaskEdit5.Text 			:= Query1.FieldByName('V_CIM').AsString;
				MaskEdit6.Text 			:= Query1.FieldByName('V_TEL').AsString;
				MaskEdit7.Text 			:= Query1.FieldByName('V_FAX').AsString;
				MaskEdit8.Text 			:= Query1.FieldByName('V_ADO').AsString;
				MaskEdit80.Text 		:= Query1.FieldByName('VE_EUADO').AsString;
				MaskEdit9.Text 			:= Query1.FieldByName('V_BANK').AsString;
				MaskEdit10.Text 		:= Query1.FieldByName('V_KEDV').AsString;
				MaskEdit11.Text 		:= Query1.FieldByName('V_UGYINT').AsString;
				MaskEdit111.Text 		:= Query1.FieldByName('V_PUGYES').AsString;
				MaskEdit12.Text 		:= Query1.FieldByName('V_MEGJ').AsString;
				MaskEdit22.Text 		:= Query1.FieldByName('VE_VEFEJ').AsString;
				MaskEdit23.Text 		:= Query1.FieldByName('VE_TIMO').AsString;
				MaskEdit13.Text 		:= Query1.FieldByName('V_LEJAR').AsString;
				MaskEdit14.Text 		:= Query1.FieldByName('V_UPERT').AsString;
				MaskEdit15.Text 		:= Query1.FieldByName('VE_EMAIL').AsString;
        meMobil.Text 		    := Query1.FieldByName('VE_MOBIL').AsString;
				MaskEdit24.Text 		:= Query1.FieldByName('VE_SKOD').AsString;
				MaskEdit25.Text 		:= Query1.FieldByName('VE_CSOPAZ').AsString;
               M1.Text             	:= Query1.FieldByName('VE_KERULET').AsString;
               M2.Text             	:= Query1.FieldByName('VE_KOZTER').AsString;
               M3.Text             	:= Query1.FieldByName('VE_JELLEG').AsString;
               M4.Text             	:= Query1.FieldByName('VE_HAZSZAM').AsString;
               M5.Text             	:= Query1.FieldByName('VE_EPULET').AsString;
               M6.Text             	:= Query1.FieldByName('VE_LEPCSO').AsString;
               M7.Text             	:= Query1.FieldByName('VE_SZINT').AsString;
               M8.Text             	:= Query1.FieldByName('VE_AJTO').AsString;
			  CheckBox1.Checked 		:= StrToIntDef(Query1.FieldByName('V_NEMET').AsString, -1) > 0;
				CheckBox5.Checked 		:= StrToIntDef(Query1.FieldByName('VE_XMLSZ').AsString, -1) > 0;
				CheckBox6.Checked 		:= StrToIntDef(Query1.FieldByName('VE_PKELL').AsString, -1) > 0;
				CheckBox4.Checked 		:= StrToIntDef(Query1.FieldByName('VE_ARHIV').AsString, -1) > 0;
				CheckBox2.Checked 		:= StrToIntDef(Query1.FieldByName('VE_FELIS').AsString, -1) > 0;
				CheckBox7.Checked 		:= StrToIntDef(Query1.FieldByName('VE_FTELJ').AsString, -1) > 0;
				RadioGroup1.ItemIndex	:= Query1.FieldByName('V_TENAP').AsInteger;
				RadioGroup2.ItemIndex	:= Query1.FieldByName('V_ARNAP').AsInteger;
				ComboSet (AfaCombo, 	Query1.FieldByName('V_AFAKO').AsString, afalist);
				ComboSet (OrszagCombo, 	Query1.FieldByName('V_ORSZ').AsString, orszaglist);
				ComboSet (AdoOrszagkodCombo, Query1.FieldByName('VE_AORSZ').AsString, orszagkodlist);
        ComboSet (cbNyelv, 	Query1.FieldByName('VE_NYELV').AsString, nyelvlist);
        ComboSet (cbUgyNyelv, Query1.FieldByName('VE_UNYELV').AsString, nyelvlist);
        // SzPldCombo.ItemIndex:= Query1.FieldByName('VE_SZPLD').AsInteger;
        SzPldCombo.ItemIndex:=SzPldCombo.Items.IndexOf(Query1.FieldByName('VE_SZPLD').AsString);
        ///////////////////////bank
        bank:=StrToIntDef( Trim(Query1.FieldByName('VE_VBANK').AsString),0);

				ComboSet(CBBank, 	 	Trim(Query1.FieldByName('VE_VBANK').AsString), banklist,-1);
				MaskEdit26.Text 		:= EgyebDlg.Read_SZGrid('320',inttostr(bank+1));
				MaskEdit27.Text 		:= EgyebDlg.Read_SZGrid('320',inttostr(bank+2));

				MaskEdit28.Text 		:= EgyebDlg.Read_SZGrid('320',inttostr(bank+4));
				MaskEdit29.Text 		:= EgyebDlg.Read_SZGrid('320',inttostr(bank+5));
				MaskEdit30.Text 		:= EgyebDlg.Read_SZGrid('320',inttostr(bank+6));
        ///////////////////////////
				RadioGroup3.ItemIndex	:= StrToIntDef(Query1.FieldByName('V_UPTIP').AsString,0);
				RadioGroup3Click(Self);
				Query_Run(QueryDij, 'SELECT * FROM VEVODIJ, TERMEK WHERE T_TERKOD = VD_TEKOD AND VD_VEKOD = '''+ret_kod+''' '+
					' ORDER BY VD_SORSZ');
				if QueryDij.RecordCount > 0 then begin
					BitBtn31.Enabled	:= true;
					BitBtn32.Enabled	:= true;
				end;
        // napl� elemek
        mFEKOD:= Query1.FieldByName('VE_LETRE').AsString;
        meRogzitette.Text:= query_Select('JELSZO', 'JE_FEKOD', mFEKOD, 'JE_FENEV');
        mFEKOD:= Query1.FieldByName('VE_MODOS').AsString;
        meModositotta.Text:= query_Select('JELSZO', 'JE_FEKOD', mFEKOD, 'JE_FENEV');
        meRogzitetteTimestamp.Text:= Query1.FieldByName('VE_LETREI').AsString;
        meModositottaTimestamp.Text:= Query1.FieldByName('VE_MODOSI').AsString;
				// A postai c�m bet�lt�se
				ComboSet (OrszagCombo2, Query1.FieldByName('VE_PORSZ').AsString, orszaglist2);
				MaskEdit16.Text 		:= Query1.FieldByName('VE_PIRSZ').AsString;
				MaskEdit17.Text 		:= Query1.FieldByName('VE_POVAR').AsString;
				MaskEdit18.Text 		:= Query1.FieldByName('VE_POCIM').AsString;
				MaskEdit19.Text 		:= Query1.FieldByName('VE_PNEV1').AsString;
				MaskEdit20.Text 		:= Query1.FieldByName('VE_PNEV2').AsString;
				MaskEdit21.Text 		:= Query1.FieldByName('VE_PNEV3').AsString;
				CheckBox3.Checked 		:= StrToIntDef(Query1.FieldByName('VE_PFLAG').AsString, -1) > 0;
				CheckBox3Click(Self);
        ///////////////
				Query_Run(QueryGK, 'SELECT * FROM ALGEPKOCSI WHERE AG_AVKOD = '+ret_kod+' ORDER BY AG_RENSZ ');
				if QueryGK.RecordCount > 0 then begin
					BitBtn6.Enabled	:= true;
					BitBtn7.Enabled	:= true;
				end;
			end;
		end;
	end;
end;

procedure TVevobeDlg.BitElkuldClick(Sender: TObject);
begin
	if Elkuldes then begin
		Close;
	end;
end;

procedure TVevobeDlg.FormDestroy(Sender: TObject);
begin
	 afalist.Free;
   banklist.Free;
   orszaglist.Free;
   orszagkodlist.Free;
   nyelvlist.Free;
end;

procedure TVevobeDlg.MaskEdit10KeyPress(Sender: TObject; var Key: Char);
begin
	Key := EgyebDlg.Jochar(MaskEdit10.Text,Key,6,2);
end;

procedure TVevobeDlg.MaskEdit10Exit(Sender: TObject);
begin
	EgyebDlg.Joszazalek(MaskEdit10);
end;

procedure TVevobeDlg.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 if not DBGrid2.Focused then
	EgyebDlg.FormReturn(Key);
end;

procedure TVevobeDlg.MaskEdit13KeyPress(Sender: TObject; var Key: Char);
begin
	Key := EgyebDlg.Jochar(MaskEdit10.Text,Key,3,0);
end;

procedure TVevobeDlg.MaskEdit13Exit(Sender: TObject);
begin
	MaskEdit13.Text := StrSzamStr(MaskEdit13.Text, 3,0);
end;

procedure TVevobeDlg.BitBtn17Click(Sender: TObject);
begin
  Application.CreateForm(TVCARDParamsDlg, VCARDParamsDlg);
  VCARDParamsDlg.VCardTolto('VEVO', MaskEdit1.Text, MaskEdit2.Text);
  // VCARDParamsDlg.Tolto(MaskEdit2.Text, 'VEVO|'+MaskEdit1.Text);
  VCARDParamsDlg.ShowModal;
  VCARDParamsDlg.Free;
end;

procedure TVevobeDlg.BitBtn1Click(Sender: TObject);
begin
	// Az �zemanyag p�tl�k t�bl�zat�nak megjelen�t�se
	if Trim(MaskEdit1.Text) <> '' then begin
       Application.CreateForm(TPotlekbeDlg, PotlekbeDlg);
       PotlekbeDlg.Tolto('�zemanyag p�tl�k karbantart�sa', MaskEdit1.Text);
       PotlekbeDlg.ShowModal;
       PotlekbeDlg.Destroy;
   end else begin
   	// �j vev� felvitele
		NoticeKi('El�sz�r r�gz�teni kell a vev�t, csak ut�na lehet �zemanyag p�tl�kot felvinni!!!');
   end;
end;

procedure TVevobeDlg.RadioGroup3Click(Sender: TObject);
begin
	case RadioGroup3.ItemIndex of
   	0 :
       begin
       	BitBtn1.Enabled		:= false;
			MaskEdit14.Text		:= '0';
			SetMaskEdits([MaskEdit14]);
		end;
		1 :
		begin
			BitBtn1.Enabled		:= true;
			MaskEdit14.Text		:= '0';
			SetMaskEdits([MaskEdit14]);
		end;
		2 :
		begin
			BitBtn1.Enabled		:= false;
			SetMaskEdits([MaskEdit14], 1);
      UZEMFIX.Close;
      UZEMFIX.Parameters[0].Value:=MaskEdit1.Text;
      UZEMFIX.Open;
      UZEMFIX.First;
		end;
   end;
   Panel3.Visible:=RadioGroup3.ItemIndex=2;
end;

procedure TVevobeDlg.MaskEdit14Exit(Sender: TObject);
begin
   SzamExit(Sender);
end;

procedure TVevobeDlg.MaskEdit14KeyPress(Sender: TObject; var Key: Char);
begin
	Key := EgyebDlg.Jochar(MaskEdit14.Text,Key,6,2);
end;

procedure TVevobeDlg.MaskEdit5Change(Sender: TObject);
begin
   M2.Text := MaskEdit5.Text;
end;

procedure TVevobeDlg.BitBtn30Click(Sender: TObject);
begin
	if Elkuldes then begin
   	Application.CreateForm(TVevodijbeDlg, VevodijbeDlg);
   	VevodijbeDlg.Tolt(MaskEdit1.Text, '');
   	VevodijbeDlg.ShowModal;
		if VevodijbeDlg.ret_sorsz <> '' then begin
       	// R�ugrunk a megfelel� rekordra
			QueryDij.Close;
			Query_Run(QueryDij, 'SELECT * FROM VEVODIJ, TERMEK WHERE T_TERKOD = VD_TEKOD AND VD_VEKOD = '''+ret_kod+''' ORDER BY VD_SORSZ');
			QueryDij.Locate('VD_SORSZ', VevodijbeDlg.ret_sorsz, [] );
           BitBtn31.Enabled	:= true;
           BitBtn32.Enabled	:= true;
       end;
   	VevodijbeDlg.Destroy;
	end;
end;

function TVevobeDlg.Elkuldes : boolean;
var
	st			: string;
  res: integer;
begin
	Result	:= false;
	if MaskEdit2.Text = '' then begin
		NoticeKi('Hi�nyz� vev�n�v!');
		MaskEdit2.SetFocus;
		Exit;
	end;
	if M2.Text = '' then begin
		NoticeKi('Hi�nyz� k�zter�let!');
		M2.SetFocus;
		Exit;
	end;
	if MaskEdit3.Text = '' then begin
		NoticeKi('Hi�nyz� ir�ny�t�sz�m!');
		MaskEdit3.SetFocus;
		Exit;
	end;
	if MaskEdit4.Text = '' then begin
		NoticeKi('Hi�nyz� telep�l�s!');
		MaskEdit4.SetFocus;
		Exit;
	end;
	if not EgyebDlg.Joszazalek(MaskEdit10) then begin
		NoticeKi('Rossz %!');
		MaskEdit10.SetFocus;
		Exit;
	end;

  /////// KG 20111122 Ellen�rz�s
	if (MaskEdit8.Text = '') and (OrszagCombo.Text='Magyarorsz�g') then
			if NoticeKi('Nincs ad�sz�m! Folytatja?', NOT_QUESTION) <> 0 then
      begin
        MaskEdit8.SetFocus;
				Exit;
      end;
	// A l�tez� ad�sz�mok vizsg�lata
	if MaskEdit8.Text <> '' then begin
    /////// KG 20111122 Ellen�rz�s
    res := CheckAdoszam(MaskEdit8.Text);
    if res <>0 then
			if NoticeKi('Hib�s ad�sz�m (hibak�d: '+IntToStr(res)+ ')! Folytatja?', NOT_QUESTION) <> 0 then
      begin
        MaskEdit8.SetFocus;
				Exit;
      end;
    ///////////
		Query_Run(Query1, 'SELECT * FROM VEVO WHERE V_ADO = '''+MaskEdit8.Text+''' AND V_KOD <> '''+ret_kod+''' ');
		if Query1.RecordCount >= 1 then begin
			if NoticeKi('T�bbsz�r�s ad�sz�m! Folytatja?', NOT_QUESTION) <> 0 then begin
				Exit;
			end;
		end;
	end;
  /////// KG 20111122 Ellen�rz�s
	if (MaskEdit80.Text = '') and (OrszagCombo.Text<>'Magyarorsz�g') then
			if NoticeKi('Nincs EU ad�sz�m! Folytatja?', NOT_QUESTION) <> 0 then
      begin
        MaskEdit80.SetFocus;
				Exit;
      end;
	if MaskEdit80.Text <> '' then begin
		Query_Run(Query1, 'SELECT * FROM VEVO WHERE VE_EUADO = '''+MaskEdit80.Text+''' AND V_KOD <> '''+ret_kod+''' ');
		if Query1.RecordCount >= 1 then begin
			if NoticeKi('T�bbsz�r�s EU ad�sz�m! Folytatja?', NOT_QUESTION) <> 0 then begin
				Exit;
			end;
		end;
	end;
	// A postai c�m ellen�rz�se
	if CheckBox3.Checked then begin
		CheckBox3Click(Self);
	end;

	{�j vev� felvitele}
   st  := '';
	if ret_kod = '' then begin
     if not DuplikatumElfogadva(MaskEdit2.text, MaskEdit4.text) then begin
        Exit;  // function;
        end;
		{�j k�d gener�l�sa}
		st:= Format('%5.5d',[GetNextCode('VEVO', 'V_KOD', 0, 5)]);

    if not Query_Insert(Query1, 'VEVO', [
       'V_KOD', ''''+ st+'''',
       'VE_LETRE', ''''+EgyebDlg.user_code+'''',
       'VE_LETREI', ''''+formatdatetime('yyyy.mm.dd. HH:mm:ss', Now)+''''
       ], True) then begin
       NoticeKi('SIKERTELEN r�gz�t�s!');
       Exit;  // function;
       end;
		MaskEdit1.Text  := st;
		ret_kod			:= st;
	end;

   vbank   := '';
   if CBBank.ItemIndex > -1 then begin
       vbank   := banklist[CBBank.ItemIndex];
   end;

   // Lev�l k�ld�se a m�dos�t�sr�l
   if st <> '' then begin
       // �j vev� keletkezett
       SendVevo('�j partner l�trehoz�sa. Azonos�t� : '+ret_kod);
   end else begin
       // Vev� m�dos�t�s volt
       SendVevo('Partner adatok m�dos�t�sa. Azonos�t� : '+ret_kod);
   end;

	if Query_Update( Query1, 'VEVO', [
		'V_NEV', 		''''+MaskEdit2.Text+'''',
		'V_IRSZ',		''''+MaskEdit3.Text+'''',
		'V_VAROS',		''''+MaskEdit4.Text+'''',
		'V_CIM',		''''+MaskEdit5.Text+'''',
       'VE_KERULET',	''''+M1.Text+'''',
       'VE_KOZTER',	''''+M2.Text+'''',
       'VE_JELLEG',	''''+M3.Text+'''',
       'VE_HAZSZAM',	''''+M4.Text+'''',
       'VE_EPULET',	''''+M5.Text+'''',
       'VE_LEPCSO',	''''+M6.Text+'''',
       'VE_SZINT',		''''+M7.Text+'''',
       'VE_AJTO',		''''+M8.Text+'''',
		'V_TEL',		''''+MaskEdit6.Text+'''',
		'V_FAX',		''''+MaskEdit7.Text+'''',
		'V_ADO',		''''+MaskEdit8.Text+'''',
		'VE_EUADO',		''''+MaskEdit80.Text+'''',
		'V_BANK',		''''+MaskEdit9.Text+'''',
	 	'VE_VBANK',		''''+vbank+'''',
		'V_KEDV',		SqlSzamString(StringSzam(MaskEdit10.Text),6,2),
		'V_UGYINT',		''''+MaskEdit11.Text+'''',
		'V_PUGYES',		''''+MaskEdit111.Text+'''',
		'VE_TIMO',		''''+MaskEdit23.Text+'''',
		'V_LEJAR',		IntToStr(StrToIntDef(MaskEdit13.Text,0)),
		'VE_EMAIL',		''''+MaskEdit15.Text+'''',
    'VE_MOBIL',		''''+meMobil.Text+'''',
		'V_MEGJ',		''''+MaskEdit12.Text+'''',
		'VE_SKOD',		''''+MaskEdit24.Text+'''',
		'VE_CSOPAZ',	''''+MaskEdit25.Text+'''',
		'V_NEMET',		''''+BoolToStr01(CheckBox1.Checked)+'''',
		'VE_XMLSZ',		''''+BoolToStr01(CheckBox5.Checked)+'''',
		'VE_PKELL',		''''+BoolToStr01(CheckBox6.Checked)+'''',
		'VE_FTELJ',		''''+BoolToStr01(CheckBox7.Checked)+'''',
		'V_UPTIP',     	IntToStr(RadioGroup3.ItemIndex),
		'V_UPERT',		SqlSzamString(StringSzam(MaskEdit14.Text),6,2),
		'V_TENAP',   	IntToStr(RadioGroup1.ItemIndex),
		'V_ARNAP',   	IntToStr(RadioGroup2.ItemIndex),
		'V_ORSZ',		''''+orszaglist[OrszagCombo.ItemIndex]+'''',
		'VE_AORSZ',		''''+orszagkodlist[AdoOrszagkodCombo.ItemIndex]+'''',
    'VE_NYELV',		''''+nyelvlist[cbNyelv.ItemIndex]+'''',
    'VE_UNYELV',		''''+nyelvlist[cbUgyNyelv.ItemIndex]+'''',
		'VE_ARHIV', 	BoolToStr01(CheckBox4.Checked),
		'VE_FELIS',		BoolToStr01(CheckBox2.Checked),
		'VE_PFLAG',		BoolToStr01(CheckBox3.Checked),
		'VE_PIRSZ',		''''+MaskEdit16.Text+'''',
		'VE_POVAR',		''''+MaskEdit17.Text+'''',
		'VE_POCIM',		''''+MaskEdit18.Text+'''',
		'VE_PNEV1',		''''+MaskEdit19.Text+'''',
		'VE_PNEV2',		''''+MaskEdit20.Text+'''',
		'VE_PNEV3',		''''+MaskEdit21.Text+'''',
		'VE_VEFEJ',		''''+MaskEdit22.Text+'''',
		//'VE_SBANK1',	''''+MaskEdit25.Text+'''',
		//'VE_SBANK2',	''''+MaskEdit26.Text+'''',
		//'VE_SBANK3',	''''+MaskEdit27.Text+'''',
		'VE_PORSZ',		''''+orszaglist2[OrszagCombo2.ItemIndex]+'''',
		'V_AFAKO',		''''+afalist[AfaCombo.ItemIndex]+'''',
    'VE_SZPLD',   SzPldCombo.Text,
    'VE_MODOS',  ''''+EgyebDlg.user_code+'''',
    'VE_MODOSI', ''''+formatdatetime('yyyy.mm.dd. HH:mm:ss', Now)+''''
		] , ' WHERE V_KOD = '''+ret_kod+''' ' ) then
    	Result	:= true
  else Result	:= False;
end;

procedure TVevobeDlg.BitBtn31Click(Sender: TObject);
begin
	if Elkuldes then begin
		Application.CreateForm(TVevodijbeDlg, VevodijbeDlg);
		VevodijbeDlg.Tolt(MaskEdit1.Text, QueryDij.FieldByName('VD_SORSZ').AsString);
		VevodijbeDlg.ShowModal;
		if VevodijbeDlg.ret_sorsz <> '' then begin
			// R�ugrunk a megfelel� rekordra
			QueryDij.Close;
			QueryDij.Open;
			QueryDij.Locate('VD_SORSZ', VevodijbeDlg.ret_sorsz, [] );
		end;
		VevodijbeDlg.Destroy;
	end;
end;

function TVevobeDlg.DuplikatumElfogadva(VevoNev, VevoVaros: string): boolean;
var
  S, lista: string;
begin
  S:= 'select V_NEV, V_VAROS from vevo where V_NEV like ''%'+VevoNev+'%''';
  Query_Run(Query1, S);
  lista := '';
  while not Query1.Eof do begin
    lista := Felsorolashoz(lista, Query1.FieldByName('V_NEV').AsString+' ('+Query1.FieldByName('V_VAROS').AsString+')', ',', True);
    Query1.Next;
    end;
  Result:= True; // ha nincs duplik�tum, akkor helyb�l OK
  if lista <> '' then begin
     if NoticeKi('A partner m�r szerepel! Folytatja? ('+lista+')', NOT_QUESTION) <> 0 then
	  		Result:= False;
     end;
end;

procedure TVevobeDlg.BitBtn32Click(Sender: TObject);
begin
	if NoticeKi('Val�ban t�r�lni akarja a kiv�lasztott elemet?', NOT_QUESTION) <> 0 then begin
		Exit;
	end;
	Query_Run(Query1, 'DELETE FROM VEVODIJ WHERE VD_VEKOD = '''+MaskEdit1.Text+''' AND VD_SORSZ = '+
		QueryDij.FieldByName('VD_SORSZ').AsString);
	QueryDij.Close;
	QueryDij.Open;
	QueryDij.First;
	BitBtn31.Enabled	:= QueryDij.RecordCount > 0;
	BitBtn32.Enabled	:= QueryDij.RecordCount > 0;
end;

procedure TVevobeDlg.CheckBox3Click(Sender: TObject);
begin
	if CheckBox3.Checked then begin
		MaskEdit16.Text			:= MaskEdit3.Text;
		MaskEdit17.Text			:= MaskEdit4.Text;
		MaskEdit18.Text			:= MaskEdit5.Text;
		MaskEdit19.Text			:= MaskEdit2.Text;
		MaskEdit20.Text			:= '';
		MaskEdit21.Text			:= '';
		OrszagCombo2.ItemIndex	:= OrszagCombo.ItemIndex;
		MaskEdit16.Enabled		:= false;
		MaskEdit17.Enabled		:= false;
		MaskEdit18.Enabled		:= false;
		MaskEdit19.Enabled		:= false;
		MaskEdit20.Enabled		:= false;
		MaskEdit21.Enabled		:= false;
		OrszagCombo2.Enabled	:= false;
	end else begin
		MaskEdit16.Enabled		:= true;
		MaskEdit17.Enabled		:= true;
		MaskEdit18.Enabled		:= true;
		MaskEdit19.Enabled		:= true;
		MaskEdit20.Enabled		:= true;
		MaskEdit21.Enabled		:= true;
		OrszagCombo2.Enabled	:= true;
	end;
end;

procedure TVevobeDlg.BitBtn2Click(Sender: TObject);
var
  PartnerKod, PartnerPath: string;
begin
  if (VevoDoc.State in [dsInsert,dsEdit]) then
    VevoDoc.Post;
  // --- 2018.03.22. ----
  PartnerKod:= MaskEdit1.Text;
  PartnerPath:= EgyebDlg.DocumentPath+ EgyebDlg.PartnerFolder+'\'+PartnerKod;
  ForceDirectories(PartnerPath);  // ha nem l�tezne m�g
  OpenDialog1.InitialDir:= PartnerPath;
  // --------------------
  if OpenDialog1.Execute then
  begin
    VevoDoc.Append;
    VevoDocVI_DFILE.Value:=OpenDialog1.FileName;
    VevoDocVI_DONEV.Value:=ExtractFileName(OpenDialog1.FileName);
    // -- 2018.03.22. --
    VevoDoc.Post;
    // ---------------
    DBGrid2.SelectedIndex:=0;
    DBGrid2.SetFocus;
  end;
end;

procedure TVevobeDlg.VevoDocAfterInsert(DataSet: TDataSet);
begin
  VevoDocVI_VEKOD.Value:=MaskEdit1.Text;
  VevoDocVI_DATUM.Value:=date;
  VevoDocVI_LEELL.Value:='N';

end;

procedure TVevobeDlg.VevoDocBeforePost(DataSet: TDataSet);
begin
  if StringReplace( StringReplace(VevoDocVI_LEDATS.Value,'.','',[rfReplaceAll]),' ','',[rfReplaceAll])='' then
  begin
    VevoDocVI_LEDATS.Clear;
    VevoDocVI_LEDAT.Clear;
  end
  else
    VevoDocVI_LEDAT.Value:=StrToDate(VevoDocVI_LEDATS.Value);
end;

procedure TVevobeDlg.DBGrid2DblClick(Sender: TObject);
var
  fn:string;
begin
      fn:=VevoDocVI_DFILE.Value;
      if FileExists( fn) then
      begin
        SetFileAttributes(PChar(fn),FILE_ATTRIBUTE_READONLY);
        ShellExecute(Handle, 'open', PChar(fn), nil, PChar(ExtractFilePath(fn)), SW_SHOWDEFAULT);
      end
      else
      begin
        Application.MessageBox('A dokumentum nem tal�lhat�!','Hiba !!!',MB_OK + MB_ICONWARNING + MB_DEFBUTTON1);
      end;

end;

procedure TVevobeDlg.FormShow(Sender: TObject);
begin
  if VevoDoc.Active then VevoDoc.Close;
  VevoDoc.Parameters[0].Value:=MaskEdit1.Text;
  VevoDoc.Open;
  VevoDoc.First;
  //DBGrid2.SetFocus;
end;

procedure TVevobeDlg.DBGrid2KeyPress(Sender: TObject; var Key: Char);
begin
  if (Key=chr(13)) then
  begin
    if DBGrid2.SelectedIndex<2 then
    begin
      DBGrid2.SelectedIndex:=DBGrid2.SelectedIndex+1;
      key:=char(0);

    //  keybd_event(VK_TAB,0,0,0);
    //  keybd_event(VK_TAB,0,KEYEVENTF_KEYUP,0);
    //  keybd_event(VK_MENU,0,0,0);
    end
    else
    begin
      if DataSource2.State  in [dsEdit, dsInsert] then
      begin
        VevoDoc.Post;
      end;
      //if DBGrid2.SelectedIndex>=2 then
      //  DBGrid2.SelectedIndex:=0;
    end;
  end;
  DBGrid2.SetFocus;
end;

procedure TVevobeDlg.BitBtn4Click(Sender: TObject);
begin
	if NoticeKi('Val�ban t�r�lni akarja a kiv�lasztott elemet?', NOT_QUESTION) <> 0 then begin
		Exit;
	end;
  VevoDoc.Delete;
end;

procedure TVevobeDlg.BitBtn3Click(Sender: TObject);
begin
  DBGrid2.OnDblClick(self);
end;

procedure TVevobeDlg.DBGrid2ColEnter(Sender: TObject);
begin
  if (DBGrid2.Columns[DBGrid2.SelectedIndex].PickList.Count > 0)and(DataSource2.State in [dsInsert,dsEdit]) then
  begin
    keybd_event(VK_F2,0,0,0);
    keybd_event(VK_F2,0,KEYEVENTF_KEYUP,0);
    keybd_event(VK_MENU,0,0,0);
    keybd_event(VK_DOWN,0,0,0);
    keybd_event(VK_DOWN,0,KEYEVENTF_KEYUP,0);
    keybd_event(VK_MENU,0,KEYEVENTF_KEYUP,0);
    //Application.ProcessMessages;
  end;

end;

procedure TVevobeDlg.DBGrid2DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
    if (VevoDocVI_LEELL.Value='I')and(VevoDocVI_LEDAT.Value<date) then
      DBGrid2.Canvas.Font.Color:=clRed;

  DBGrid2.DefaultDrawColumnCell(Rect,DataCol,Column , State);

end;

procedure TVevobeDlg.VevoDocVI_LEDATSetText(Sender: TField;
  const Text: String);
var DText:string;
begin
  DText:=Text;
  if pos(' ',DText)>0 then
  begin
    DText:=StringReplace(DText,' ','',[rfReplaceAll	]);
    DText:=StringReplace(DText,'.','',[rfReplaceAll	]);
    if Length(DText)=2 then
    begin
      DText:=IntToStr(YearOf(date))+'.'+IntToStr(MonthOf(date))+'.'+DText;
    end;
    if Length(DText)=4 then
    begin
      DText:=IntToStr(YearOf(date))+'.'+copy(DText,1,2)+'.'+copy(DText,3,2);
    end;
    if Length(DText)=6 then
    begin
      DText:=copy(DText,1,4)+'.'+copy(DText,5,2)+'.01';
    end;
    VevoDocVI_LEDAT.AsString:=DText;
  end;
end;

procedure TVevobeDlg.UZEMFIXAfterInsert(DataSet: TDataSet);
begin
  UZEMFIXUX_VEKOD.Value:=UZEMFIX.Parameters[0].Value;
end;

procedure TVevobeDlg.Button1Click(Sender: TObject);
begin
  UZEMFIX.Append;
  DBGrid3.SelectedIndex:=0;
  DBGrid3.SetFocus;
end;

procedure TVevobeDlg.Button3Click(Sender: TObject);
begin
  if UZEMFIX.Modified then
  UZEMFIX.Post;
end;

procedure TVevobeDlg.Button4Click(Sender: TObject);
begin
  if UZEMFIX.Modified then
  UZEMFIX.Cancel;

end;

procedure TVevobeDlg.OrszagComboChange(Sender: TObject);
begin
  if ret_kod=''  then
      CheckBox3.Checked:=OrszagCombo.Text<>'Magyarorsz�g'  ;
end;

procedure TVevobeDlg.MaskEdit80Exit(Sender: TObject);
begin
  if POS(' ',Trim(MaskEdit80.Text))>0 then
    NoticeKi('Sz�k�zt nem tartalmazhat. Jav�tson!') ;
end;

procedure TVevobeDlg.BitBtn5Click(Sender: TObject);
begin
	if Elkuldes then begin
		Application.CreateForm(TAlgepbeDlg, AlgepbeDlg);
		AlgepbeDlg.Tolto('�j alv�llalkoz�i g�pkocsi felvitele', '');
		// Az alv�llalkoz� felt�lt�se
		AlgepbeDlg.M1A.Text	:= MaskEdit1.Text;
		AlgepbeDlg.M1B.Text	:= MaskEdit2.Text;
		AlgepbeDlg.ButValVevo.Enabled	:= false;
		AlgepbeDlg.ShowModal;
		if AlgepbeDlg.ret_kod <> '' then begin
			// R�ugrunk a megfelel� rekordra
			QueryGK.Close;
			Query_Run(QueryGK, 'SELECT * FROM ALGEPKOCSI WHERE AG_AVKOD = '+ret_kod+' ORDER BY AG_RENSZ ');
			QueryGK.Locate('AG_AGKOD', AlgepbeDlg.ret_kod, [] );
			BitBtn6.Enabled	:= true;
			BitBtn7.Enabled	:= true;
		end;
		AlgepbeDlg.Destroy;
	end;

end;

procedure TVevobeDlg.BitBtn6Click(Sender: TObject);
begin
	if Elkuldes then begin
		Application.CreateForm(TAlgepbeDlg, AlgepbeDlg);
		AlgepbeDlg.Tolto('Alv�llalkoz�i g�pkocsi m�dos�t�sa', QueryGK.FieldByName('AG_AGKOD').AsString);
		// Az alv�llalkoz� felt�lt�se
//		AlgepbeDlg.M1A.Text	:= MaskEdit1.Text;
//		AlgepbeDlg.M1A.Text	:= MaskEdit2.Text;
		AlgepbeDlg.ButValVevo.Enabled	:= false;
		AlgepbeDlg.ShowModal;
		if AlgepbeDlg.ret_kod <> '' then begin
			// R�ugrunk a megfelel� rekordra
			QueryGK.Close;
			Query_Run(QueryGK, 'SELECT * FROM ALGEPKOCSI WHERE AG_AVKOD = '+ret_kod+' ORDER BY AG_RENSZ ');
			QueryGK.Locate('AG_AGKOD', AlgepbeDlg.ret_kod, [] );
			BitBtn6.Enabled	:= true;
			BitBtn7.Enabled	:= true;
		end;
		AlgepbeDlg.Destroy;
	end;

end;

procedure TVevobeDlg.BitBtn7Click(Sender: TObject);
var
	regikod	: string;
begin
	if NoticeKi('Val�ban t�r�lni akarja a kiv�lasztott elemet?', NOT_QUESTION) <> 0 then begin
		Exit;
	end;
	regikod				:= QueryGK.FieldByName('AG_AGKOD').AsString;
	QueryGK.Close;
	Query_Run(QueryGK, 'DELETE FROM ALGEPKOCSI WHERE AG_AGKOD = '+regikod);
	Query_Run(QueryGK, 'SELECT * FROM ALGEPKOCSI WHERE AG_AVKOD = '+ret_kod+' ORDER BY AG_RENSZ ');
	BitBtn6.Enabled	:= QueryGK.RecordCount > 0;
	BitBtn7.Enabled	:= QueryGK.RecordCount > 0;

end;

procedure TVevobeDlg.PageControl1Changing(Sender: TObject;
  var AllowChange: Boolean);
var
  P : TPoint;
  New_Tab: integer;
begin
  P := PageControl1.ScreenToClient(Mouse.CursorPos);
  New_Tab := PageControl1.IndexOfTabAt(P.X,P.Y);
  if New_Tab=4 then
    AllowChange:= EgyebDlg.EvSzam>'2012';
end;

procedure TVevobeDlg.CBBankChange(Sender: TObject);
var
  bank: integer;
begin
  bank:=StrToIntDef( banklist[CBBank.ItemIndex],0);
	//ComboSet (CBBank, 	 	Trim(Query1.FieldByName('VE_VBANK').AsString), banklist);
	MaskEdit26.Text 		:= EgyebDlg.Read_SZGrid('320',inttostr(bank+1));
	MaskEdit27.Text 		:= EgyebDlg.Read_SZGrid('320',inttostr(bank+2));

	MaskEdit28.Text 		:= EgyebDlg.Read_SZGrid('320',inttostr(bank+4));
	MaskEdit29.Text 		:= EgyebDlg.Read_SZGrid('320',inttostr(bank+5));
	MaskEdit30.Text 		:= EgyebDlg.Read_SZGrid('320',inttostr(bank+6));
end;

procedure TVevobeDlg.SendVevo(muv : string);
var
   cimzett     : string;
begin
   // Feketelist�s vev� ker�lt elk�ld�sre
   Screen.Cursor   := crHourGlass;
   cimzett := EgyebDlg.Read_SZGrid('376','110');
   if cimzett <> '' then begin
       EgyebDlg.ellenlista.Clear;
       EgyebDlg.ellenlista.Add('Figyelem!');
       EgyebDlg.ellenlista.Add('');
       EgyebDlg.ellenlista.Add(muv);
       EgyebDlg.ellenlista.Add('Felhaszn�l� : '+EgyebDlg.user_code+' '+EgyebDlg.user_name);
       EgyebDlg.ellenlista.Add('M�velet id�pontja : '+FormatDateTime('YYYY.MM.DD hh:nn:ss', now));
       EgyebDlg.ellenlista.Add('');
       // A megv�ltozott adatok elk�ld�se
       Query_Run(Query1, 'SELECT * from VEVO WHERE V_KOD = '''+ret_kod+'''');
		if Query1.FieldByName('V_NEV').AsString     <> MaskEdit2.Text then begin
           EgyebDlg.ellenlista.Add('N�v v�ltoz�s : ' + Query1.FieldByName('V_NEV').AsString +' ->' + MaskEdit2.Text );
       end;
		if Query1.FieldByName('V_IRSZ').AsString    <> MaskEdit3.Text then begin
           EgyebDlg.ellenlista.Add('Ir�ny�t�sz�m v�ltoz�s : ' + Query1.FieldByName('V_IRSZ').AsString   +' ->' +  MaskEdit3.Text );
       end;
		if Query1.FieldByName('V_VAROS').AsString   <> MaskEdit4.Text then begin
           EgyebDlg.ellenlista.Add('Telep�l�s v�ltoz�s : ' + Query1.FieldByName('V_VAROS').AsString    +' ->' +  MaskEdit4.Text  );
       end;
		if Query1.FieldByName('V_CIM').AsString     <> MaskEdit5.Text then begin
           EgyebDlg.ellenlista.Add('C�m v�ltoz�s : ' + Query1.FieldByName('V_CIM').AsString      +' ->' +  MaskEdit5.Text  );
       end;
		if Query1.FieldByName('V_TEL').AsString     <> MaskEdit6.Text then begin
           EgyebDlg.ellenlista.Add('Telefon v�ltoz�s : ' + Query1.FieldByName('V_TEL').AsString      +' ->' +  MaskEdit6.Text  );
       end;
		if Query1.FieldByName('V_FAX').AsString     <> MaskEdit7.Text then begin
           EgyebDlg.ellenlista.Add('Fax v�ltoz�s : ' + Query1.FieldByName('V_FAX').AsString      +' ->' +  MaskEdit7.Text  );
       end;
		if Query1.FieldByName('V_ADO').AsString     <> MaskEdit8.Text then begin
           EgyebDlg.ellenlista.Add('Ad�sz�m v�ltoz�s : ' + Query1.FieldByName('V_ADO').AsString      +' ->' +  MaskEdit8.Text  );
       end;
		if Query1.FieldByName('VE_EUADO').AsString  <> MaskEdit80.Text then begin
		    EgyebDlg.ellenlista.Add('EU ad�sz�m v�ltoz�s : ' + Query1.FieldByName('VE_EUADO').AsString   +' ->' +  MaskEdit80.Text  );
       end;
		if Query1.FieldByName('V_BANK').AsString    <> MaskEdit9.Text then begin
		    EgyebDlg.ellenlista.Add('Banksz�mla v�ltoz�s : ' + Query1.FieldByName('V_BANK').AsString     +' ->' +  MaskEdit9.Text  );
       end;
	 	if Query1.FieldByName('VE_VBANK').AsString  <> vbank then begin
	 	    EgyebDlg.ellenlista.Add('C�ges bank v�ltoz�s : ' + Query1.FieldByName('VE_VBANK').AsString   +' ->' +  vbank  );
       end;
		if StringSzam(Query1.FieldByName('V_KEDV').AsString )   <> StringSzam(MaskEdit10.Text) then begin
		    EgyebDlg.ellenlista.Add('Kedvezm�ny v�ltoz�s : ' + Query1.FieldByName('V_KEDV').AsString     +' ->' +  SqlSzamString(StringSzam(MaskEdit10.Text),6,2)  );
       end;
		if Query1.FieldByName('V_UGYINT').AsString  <> MaskEdit11.Text then begin
		    EgyebDlg.ellenlista.Add('�gyint�z� v�ltoz�s : ' + Query1.FieldByName('V_UGYINT').AsString   +' ->' +  MaskEdit11.Text  );
       end;
		if Query1.FieldByName('V_PUGYES').AsString  <> MaskEdit111.Text then begin
		    EgyebDlg.ellenlista.Add('P�nz�gyes v�ltoz�s : ' + Query1.FieldByName('V_PUGYES').AsString   +' ->' +  MaskEdit111.Text  );
       end;
		if Query1.FieldByName('VE_TIMO').AsString   <> MaskEdit23.Text then begin
		    EgyebDlg.ellenlista.Add('TIMO v�ltoz�s : ' + Query1.FieldByName('VE_TIMO').AsString    +' ->' +  MaskEdit23.Text  );
       end;
		if Query1.FieldByName('V_LEJAR').AsString   <> IntToStr(StrToIntDef(MaskEdit13.Text,0)) then begin
		    EgyebDlg.ellenlista.Add('Lej�rat v�ltoz�s : ' + Query1.FieldByName('V_LEJAR').AsString    +' ->' +  IntToStr(StrToIntDef(MaskEdit13.Text,0))  );
       end;
		if Query1.FieldByName('VE_EMAIL').AsString  <> MaskEdit15.Text then begin
		    EgyebDlg.ellenlista.Add('Email v�ltoz�s : ' + Query1.FieldByName('VE_EMAIL').AsString   +' ->' +  MaskEdit15.Text  );
       end;
		if Query1.FieldByName('VE_MOBIL').AsString  <> meMobil.Text then begin
		    EgyebDlg.ellenlista.Add('Mobilsz�m v�ltoz�s : ' + Query1.FieldByName('VE_MOBIL').AsString   +' ->' +  meMobil.Text  );
       end;
		if Query1.FieldByName('V_MEGJ').AsString    <> MaskEdit12.Text then begin
		    EgyebDlg.ellenlista.Add('Megjegyz�s v�ltoz�s : ' + Query1.FieldByName('V_MEGJ').AsString     +' ->' +  MaskEdit12.Text  );
       end;
		if Query1.FieldByName('VE_SKOD').AsString   <> MaskEdit24.Text then begin
		    EgyebDlg.ellenlista.Add('JS azonos�t� v�ltoz�s : ' + Query1.FieldByName('VE_SKOD').AsString    +' ->' +  MaskEdit24.Text  );
       end;
		if Query1.FieldByName('VE_CSOPAZ').AsString <> MaskEdit25.Text then begin
		    EgyebDlg.ellenlista.Add('Csoport az. v�ltoz�s : ' + Query1.FieldByName('VE_CSOPAZ').AsString  +' ->' +  MaskEdit25.Text  );
       end;
		if Query1.FieldByName('V_NEMET').AsString   <> BoolToStr01(CheckBox1.Checked) then begin
		    EgyebDlg.ellenlista.Add('EUR�s sz�mla v�ltoz�s : ' + Query1.FieldByName('V_NEMET').AsString    +' ->' +  BoolToStr01(CheckBox1.Checked)  );
       end;
		if Query1.FieldByName('VE_XMLSZ').AsString  <> BoolToStr01(CheckBox5.Checked) then begin
		    EgyebDlg.ellenlista.Add('XML sz�mla v�ltoz�s : ' + Query1.FieldByName('VE_XMLSZ').AsString   +' ->' +  BoolToStr01(CheckBox5.Checked)  );
       end;
		if Query1.FieldByName('VE_PKELL').AsString  <> BoolToStr01(CheckBox6.Checked) then begin
		    EgyebDlg.ellenlista.Add('Poz�ci�sz�m jelz�s v�ltoz�s : ' + Query1.FieldByName('VE_PKELL').AsString   +' ->' +  BoolToStr01(CheckBox6.Checked)  );
       end;
		if Query1.FieldByName('VE_FTELJ').AsString  <> BoolToStr01(CheckBox7.Checked) then begin
		    EgyebDlg.ellenlista.Add('Foly. teljes�t�s v�ltoz�s : ' + Query1.FieldByName('VE_FTELJ').AsString   +' ->' +  BoolToStr01(CheckBox7.Checked)  );
       end;
		if Query1.FieldByName('V_UPTIP').AsString   <> IntToStr(RadioGroup3.ItemIndex) then begin
		    EgyebDlg.ellenlista.Add('�zemanyag p�tl�k v�ltoz�s : ' + Query1.FieldByName('V_UPTIP').AsString    +' ->' +  IntToStr(RadioGroup3.ItemIndex)  );
       end;
		if StringSzam(Query1.FieldByName('V_UPERT').AsString )  <> StringSzam(MaskEdit14.Text) then begin
		    EgyebDlg.ellenlista.Add('�z.p�tl�k �rt�k v�ltoz�s : ' + Query1.FieldByName('V_UPERT').AsString    +' ->' +  SqlSzamString(StringSzam(MaskEdit14.Text),6,2)  );
       end;
		if Query1.FieldByName('V_TENAP').AsString   <> IntToStr(RadioGroup1.ItemIndex) then begin
		    EgyebDlg.ellenlista.Add('Teljes�t�s nap v�ltoz�s : ' + Query1.FieldByName('V_TENAP').AsString    +' ->' +  IntToStr(RadioGroup1.ItemIndex)  );
       end;
		if Query1.FieldByName('V_ARNAP').AsString   <> IntToStr(RadioGroup2.ItemIndex) then begin
		    EgyebDlg.ellenlista.Add('�rfolyam jelz�s v�ltoz�s : ' + Query1.FieldByName('V_ARNAP').AsString    +' ->' +  IntToStr(RadioGroup2.ItemIndex)  );
       end;
		if Query1.FieldByName('V_ORSZ').AsString    <> orszaglist[OrszagCombo.ItemIndex] then begin
		    EgyebDlg.ellenlista.Add('Orsz�g v�ltoz�s : ' + Query1.FieldByName('V_ORSZ').AsString     +' ->' +  orszaglist[OrszagCombo.ItemIndex]  );
       end;
		if Query1.FieldByName('VE_AORSZ').AsString    <> orszagkodlist[AdoOrszagkodCombo.ItemIndex] then begin
		    EgyebDlg.ellenlista.Add('Ad�sz�m orsz�g v�ltoz�s : ' + Query1.FieldByName('VE_AORSZ').AsString     +' ->' +  orszagkodlist[AdoOrszagkodCombo.ItemIndex]  );
       end;
		if Query1.FieldByName('VE_NYELV').AsString  <> nyelvlist[cbNyelv.ItemIndex] then begin
		    EgyebDlg.ellenlista.Add('Sz�ml�z�si nyelv v�ltoz�s : ' + Query1.FieldByName('VE_NYELV').AsString     +' ->' +  nyelvlist[cbNyelv.ItemIndex]  );
       end;
		if Query1.FieldByName('VE_UNYELV').AsString  <> nyelvlist[cbUgyNyelv.ItemIndex] then begin
		    EgyebDlg.ellenlista.Add('�gyint�z�si nyelv v�ltoz�s : ' + Query1.FieldByName('VE_UNYELV').AsString     +' ->' +  nyelvlist[cbUgyNyelv.ItemIndex]  );
       end;
		if Query1.FieldByName('VE_ARHIV').AsString  <> BoolToStr01(CheckBox4.Checked) then begin
		    EgyebDlg.ellenlista.Add('Arh�v flag v�ltoz�s : ' + Query1.FieldByName('VE_ARHIV').AsString   +' ->' +  BoolToStr01(CheckBox4.Checked)  );
       end;
		if Query1.FieldByName('VE_FELIS').AsString  <> BoolToStr01(CheckBox2.Checked) then begin
		    EgyebDlg.ellenlista.Add('Feketelista jelz�s v�ltoz�s : ' + Query1.FieldByName('VE_FELIS').AsString   +' ->' +  BoolToStr01(CheckBox2.Checked)  );
       end;
		if Query1.FieldByName('VE_PFLAG').AsString  <> BoolToStr01(CheckBox3.Checked) then begin
		    EgyebDlg.ellenlista.Add('Post�z�si c�m v�ltoz�s : ' + Query1.FieldByName('VE_PFLAG').AsString   +' ->' +  BoolToStr01(CheckBox3.Checked)  );
       end;
		if Query1.FieldByName('VE_PIRSZ').AsString  <> MaskEdit16.Text then begin
		    EgyebDlg.ellenlista.Add('Postai irsz. v�ltoz�s : ' + Query1.FieldByName('VE_PIRSZ').AsString   +' ->' +  MaskEdit16.Text  );
       end;
		if Query1.FieldByName('VE_POVAR').AsString  <> MaskEdit17.Text then begin
		    EgyebDlg.ellenlista.Add('Postai v�ros v�ltoz�s : ' + Query1.FieldByName('VE_POVAR').AsString   +' ->' +  MaskEdit17.Text  );
       end;
		if Query1.FieldByName('VE_POCIM').AsString  <> MaskEdit18.Text then begin
		    EgyebDlg.ellenlista.Add('Postai c�m v�ltoz�s : ' + Query1.FieldByName('VE_POCIM').AsString   +' ->' +  MaskEdit18.Text  );
       end;
		if Query1.FieldByName('VE_PNEV1').AsString  <> MaskEdit19.Text then begin
		    EgyebDlg.ellenlista.Add('Postai n�v v�ltoz�s : ' + Query1.FieldByName('VE_PNEV1').AsString   +' ->' +  MaskEdit19.Text  );
       end;
		if Query1.FieldByName('VE_PNEV2').AsString  <> MaskEdit20.Text then begin
		    EgyebDlg.ellenlista.Add('Postai n�v 2 v�ltoz�s : ' + Query1.FieldByName('VE_PNEV2').AsString   +' ->' +  MaskEdit20.Text  );
       end;
		if Query1.FieldByName('VE_PNEV3').AsString  <> MaskEdit21.Text then begin
		    EgyebDlg.ellenlista.Add('Postai n�v 3 v�ltoz�s : ' + Query1.FieldByName('VE_PNEV3').AsString   +' ->' +  MaskEdit21.Text  );
       end;
		if Query1.FieldByName('VE_PORSZ').AsString  <> orszaglist2[OrszagCombo2.ItemIndex] then begin
		    EgyebDlg.ellenlista.Add('Postai orsz�g v�ltoz�s : ' + Query1.FieldByName('VE_PORSZ').AsString   +' ->' +  orszaglist2[OrszagCombo2.ItemIndex]  );
       end;
		if Query1.FieldByName('VE_VEFEJ').AsString  <> MaskEdit22.Text then begin
		    EgyebDlg.ellenlista.Add('Extra fejsor v�ltoz�s : ' + Query1.FieldByName('VE_VEFEJ').AsString   +' ->' +  MaskEdit22.Text  );
       end;
		if Query1.FieldByName('V_AFAKO').AsString   <> afalist[AfaCombo.ItemIndex] then begin
		    EgyebDlg.ellenlista.Add('�FA % v�ltoz�s : ' + Query1.FieldByName('V_AFAKO').AsString    +' ->' +  afalist[AfaCombo.ItemIndex]  );
       end;
       if Query1.FieldByName('VE_SZPLD').AsString  <> SzPldCombo.Text then begin
           EgyebDlg.ellenlista.Add('Sz�mla p�ld�ny v�ltoz�s : ' + Query1.FieldByName('VE_SZPLD').AsString   +' ->' +  SzPldCombo.Text  );
       end;
       EgyebDlg.ellenlista.Add('');
       EgyebDlg.ellenlista.Add('FUVAROS rendszer');
       if not SendJSEmail (cimzett, '', 'Partner v�ltoz�s',  EgyebDlg.ellenlista ) then begin
           HibaKiiro('Partner v�ltoz�s �zenet elk�ld�se nem siker�lt! A partner k�dja : '+ret_kod);
       end;
   end;
   Screen.Cursor   := crDefault;
end;


end.
