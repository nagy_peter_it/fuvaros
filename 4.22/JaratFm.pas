unit JaratFm;

interface

uses
	SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
	StdCtrls, Forms, ExtCtrls, Dialogs, J_FOFORMUJ, DB, ADODB, Buttons, Vcl.Mask;

type
	TJaratFormDlg = class(TJ_FOFORMUJDLG)
	 QueryJarat: TADOQuery;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    BitBtn3: TBitBtn;
    BitBtn4: TBitBtn;
    QueryAljarat: TADOQuery;
    Query3: TADOQuery;
    BitBtn5: TBitBtn;
    QSeged: TADOQuery;
    M2: TMaskEdit;
    BitBtn6: TBitBtn;
    M3: TMaskEdit;
    BitBtn7: TBitBtn;
    BitBtn8: TBitBtn;
    BitBtn9: TBitBtn;
    BitBtn12: TBitBtn;
	  procedure FormCreate(Sender: TObject);override;
	 procedure Adatlap(cim, kod : string);override;
	 procedure ButtonKuldClick(Sender: TObject);override;
	 procedure FormKeyDown(Sender: TObject; var Key: Word;
	   Shift: TShiftState);override;
	 function 	KeresChange(mezo, szoveg : string) : string; override;
	 procedure DataSource1DataChange(Sender: TObject; Field: TField);override;
	 procedure ButtonTorClick(Sender: TObject); override;
	 procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure BitBtn4Click(Sender: TObject);
    procedure BitBtn5Click(Sender: TObject);
    procedure SegedEllenor;
    procedure BitBtn12Click(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BitBtn6Click(Sender: TObject);
    procedure BitBtn7Click(Sender: TObject);
	private
		vanellenor	: boolean;
		vanstorno	: boolean;
    kell_datum_szures: boolean;
  public
		ret_orsz	: string;
		ret_alkod   : string;
	end;

var
	JaratFormDlg : TJaratFormDlg;

implementation

uses
	Egyeb, Jaratbe, J_SQL, KOZOS, Kozos_Local, CsatolmanyFm, Jarat_ell,
   Lgauge, Ossznyil;

{$R *.DFM}

procedure TJaratFormDlg.FormCreate(Sender: TObject);
begin
	Inherited FormCreate(Sender);
	EgyebDLg.SetADOQueryDatabase(QueryAlJarat);
	EgyebDLg.SetADOQueryDatabase(QSeged);
	Query_Run(QueryAlJarat, 'SELECT AJ_JAKOD, AJ_ALNEV FROM ALJARAT ORDER BY AJ_JAKOD ');
	AddSzuromezoRovid('JA_RENDSZ', 'JARAT');
	AddSzuromezoRovid('JA_ORSZ',   'JARAT');
	AddSzuromezoRovid('JA_SOFK1',  'JARAT');
	AddSzuromezoRovid('JA_SOFK2',  'JARAT');
	AddSzuromezoRovid('JA_SNEV1',  'JARAT');
	AddSzuromezoRovid('JA_SNEV2',  'JARAT');
	// AddSzuromezoRovid('JA_POTK',   'JARAT');
	AddSzuromezoRovid('JA_FAZST',  'JARAT');
	AddSzuromezoRovid('JA_FAZIS',  'JARAT');
	AddSzuromezoRovid('JA_JKEZD',  'JARAT');
	AddSzuromezoRovid('JA_FAJKO',  'JARAT');
	AddSzuromezoRovid('JA_MSDB',  'JARAT');
	AddSzuromezoRovid('JA_MSFAJ',  'JARAT');
	ButtonNez.Visible	:= true;
  kellujures:=False;
   kell_datum_szures:=False;
   M2.Text             := DatumhozNap(EgyebDlg.MaiDatum, -7, true );
   M3.Text             := EgyebDlg.MaiDatum;
   // M2.Text             := '2000.01.01.';           // m�nusz v�gtelen
   // M3.Text             := DatumhozNap(EgyebDlg.MaiDatum, 365, true );  // ma + 1 �v
	alapstr	:= 'SELECT * FROM JARAT';
  if kell_datum_szures then begin
    alapstr	:=alapstr	+ ' WHERE JA_JKEZD >= '''+M2.Text+''' AND JA_JKEZD <= '''+M3.Text+''' ';
    end;
  FelTolto('�j J�rat felvitele ', 'J�rat adatok m�dos�t�sa ',
			'J�ratok list�ja', 'JA_KOD', alapstr,'JARAT', QUERY_ADAT_TAG );
	width				:= DLG_WIDTH;
	height				:= DLG_HEIGHT;
	ret_orsz			:= '';
	ret_alkod   		:= '';
   EgyebDlg.DATUMIDOELL:=True;
	BitBtn1.Parent		:= PanelBottom;
	BitBtn2.Parent		:= PanelBottom;
	BitBtn3.Parent		:= PanelBottom;
	BitBtn4.Parent		:= PanelBottom;
	BitBtn5.Parent		:= PanelBottom;
	EgyebDLg.SetADOQueryDatabase(QueryJarat);
	EgyebDLg.SetADOQueryDatabase(Query3);
	vanellenor	:= 	GetRightTag(562) <> RG_NORIGHT;
	vanstorno	:= 	GetRightTag(564) <> RG_NORIGHT;
   SegedEllenor;
	M2.Parent			:= PanelBottom;
	M3.Parent			:= PanelBottom;
	BitBtn8.Parent		:= PanelBottom;
	BitBtn9.Parent		:= PanelBottom;
	BitBtn6.Parent		:= PanelBottom;
	BitBtn7.Parent		:= PanelBottom;
	BitBtn12.Parent		:= PanelBottom;
end;

procedure TJaratFormDlg.Adatlap(cim, kod : string);
begin
	Application.CreateForm(TJaratbeDlg, AlForm);
	Inherited Adatlap(cim, kod );
	if EgyebDlg.torles_jakod <> '' then begin
		Query_Run(QueryJarat,  'DELETE FROM ALSEGED WHERE AS_JAKOD = '''+EgyebDlg.torles_jakod+''' ', false);
		Query_Run(QueryJarat,  'DELETE FROM JARAT WHERE JA_KOD = '''+EgyebDlg.torles_jakod+''' ', false);
		Query_Run(QueryJarat,  'DELETE FROM ALJARAT WHERE AJ_JAKOD = '''+EgyebDlg.torles_jakod+''' ', false);
		ModLocate (Query1, FOMEZO, Query1.FieldByName(FOMEZO).AsString );
	end;
end;

procedure TJaratFormDlg.ButtonKuldClick(Sender: TObject);
begin
	Inherited ButtonKuldClick(Sender);
	ret_orsz	:= Query1.FieldByName('JA_ORSZ').AsString;
	ret_alkod   := Query1.FieldByName('JA_ALKOD').AsString;
end;

procedure TJaratFormDlg.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
	Inherited FormKeyDown(Sender, Key, Shift);
	if ( EgyebDlg.CtrlGomb and ( StrToIntDef(Query1.FieldByName('JA_FAZIS').AsString,0) = 1 ) and EgyebDlg.user_super ) then begin
		// A j�rat felold�sa
		voltalt	:= true;
		BitBtn2Click(Sender);
	end;
	if ( ( ssAlt in Shift ) and (key = VK_F11) ) then begin
		// A j�rat ellen�rz�se
       if Query1.FieldByName('JA_FAZIS').AsString = '1' then begin
           Exit;
       end;
       if GetRightTag(340) = RG_NORIGHT then begin
           Exit;
       end;
       Screen.Cursor				:= crHourGlass;
       Application.CreateForm(TOssznyilDlg, OssznyilDlg);
       Screen.Cursor				:= crDefault;
       OssznyilDlg.M0.Text			:= Query1.FieldByName('JA_KOD').AsString ;
       OssznyilDlg.M1.Text     	:= Query1.FieldByName('JA_ORSZ').AsString + '-'+ Query1.FieldByName('JA_ALKOD').AsString ;
       OssznyilDlg.M1.Enabled		:= false;
       OssznyilDlg.BitBtn2.Enabled	:= false;
       OssznyilDlg.BitBtn3.Enabled	:= false;
       OssznyilDlg.BitBtn5.Enabled	:= false;
       OssznyilDlg.BitBtn8.Enabled	:= false;
       OssznyilDlg.BitBtn7.Enabled	:= false;
       OssznyilDlg.BitBtn32.Enabled	:= false;
       OssznyilDlg.MD1.Enabled		:= false;
       OssznyilDlg.MD2.Enabled		:= false;
       OssznyilDlg.BitBtn4.OnClick(self);
       OssznyilDlg.Destroy;
		ModLocate (Query1, FOMEZO, Query1.FieldByName(FOMEZO).AsString );
	end;
end;

procedure TJaratFormDlg.FormResize(Sender: TObject);
begin
	Inherited FormResize(Sender);
	M2.Top			:= BitBtn8.Top+2;
	M2.Left			:= BitBtn8.Left;
	BitBtn6.Top		:= BitBtn8.Top+2;
	BitBtn6.Left	:= BitBtn8.Left + M2.Width;
	BitBtn6.Width	:= BitBtn5.Height;

	M3.Top			:= BitBtn9.Top+2;
	M3.Left			:= BitBtn6.Left + BitBtn6.Width + 10;
	BitBtn7.Top		:= M3.Top;
	BitBtn7.Left	:= M3.Left + M3.Width;
	BitBtn7.Width	:= BitBtn7.Height;

end;

procedure TJaratFormDlg.DataSource1DataChange(Sender: TObject; Field: TField);
begin
	// A nyom�gombok szab�lyoz�sa
	case Query1.FieldByName('JA_FAZIS').AsInteger  of
	  1 :
		begin
			{A sz�mla nem m�dos�that�}
			ButtonMod.Enabled 	:= false;
			ButtonMod.Hide;
			ButtonTor.Enabled 	:= false;
			ButtonTor.Hide;
			BitBtn2.Enabled 	:= true;
			BitBtn2.Show;
			//BitBtn3.Enabled 	:= true;
			//BitBtn3.Show;
		end;
	  0, -1, -2 :
		begin
			{A sz�mla m�dos�that�}
			ButtonMod.Enabled 	:= true;
			ButtonMod.Show;
			ButtonTor.Enabled 	:= true;
			ButtonTor.Show;
			BitBtn2.Enabled 	:= false;
			BitBtn2.Hide;
			//BitBtn3.Enabled 	:= true;
			//BitBtn3.Show;
		end;
	  2 :	// Storn�zott sz�mla
		begin
			{A sz�mla nem m�dos�that�}
			ButtonMod.Enabled := false;
			ButtonMod.Hide;
			ButtonTor.Enabled := false;
			ButtonTor.Hide;
			BitBtn2.Enabled := false;
			BitBtn2.Hide;
			//BitBtn3.Enabled := false;
			//BitBtn3.Hide;
		end;
	end;
	ShapeTorles.Visible	:= ButtonTor.Visible;
	// A jogosults�gok ellen�rz�se
	if not vanellenor then begin
		BitBtn2.Enabled := false;
		BitBtn2.Hide;
	end;
	if not vanstorno then begin
		//BitBtn3.Enabled := false;
		//BitBtn3.Hide;
	end;
	// Alv�llalkoz�i j�rat eset�n lehet k�rni az elk�ld�tt pdf-eket
	BitBtn4.Hide;
	if QueryAlJarat.Locate('AJ_JAKOD', Query1.FieldByName('JA_KOD').AsString, []) then begin
		if QueryAlJarat.FieldByName('AJ_ALNEV').AsString <> '' then begin
			// Alv�llalkoz�i j�ratr�l van sz�!!!
			BitBtn4.Show;
		end;
	end;
	inherited DataSource1DataChange(Sender, Field);
end;

function TJaratFormDlg.KeresChange(mezo, szoveg : string) : string;
begin
	Result	:= szoveg;
  exit;
	if UpperCase(mezo) = 'JA_KOD' then begin
		Result := Format('%6.6d',[StrToIntDef(szoveg,0)]);
	end;
end;

procedure TJaratFormDlg.ButtonTorClick(Sender: TObject);
var
  jaratszam: string;
begin
	if not voltalt then begin
		voltalt 	:= true;
		Exit;
	end;
	if NoticeKi( TORLOTEXT, NOT_QUESTION ) = 0 then begin

    if Query_Select('KOLTSEG','KS_JAKOD',Query1.FieldByName(FOMEZO).AsString,'KS_JAKOD') <> '' then
    begin
      NoticeKi('FIGYELEM ! A j�rathoz k�lts�g tartozik, ez�rt nem t�r�lhet�!');
      exit;
    end;

    if Query_Select('VISZONY','VI_JAKOD',Query1.FieldByName(FOMEZO).AsString,'VI_JAKOD') <> '' then
    begin
      NoticeKi('FIGYELEM ! A j�rathoz viszony tartozik, ez�rt nem t�r�lhet�!');
      exit;
    end;

(*
    jaratszam:=Trim(Query1.FieldByName('JA_ALKOD').AsString);
    jaratszam:=stringofchar('0',5-length(jaratszam))+jaratszam;
    jaratszam:=Query1.FieldByName('JA_ORSZ').AsString +'-'+ jaratszam;
*)
   jaratszam   := GetJaratszam(Query1.FieldByName('JA_KOD').AsString);
    if (Query_Select('MEGBIZAS','MB_JAKOD',Query1.FieldByName(FOMEZO).AsString,'MB_JAKOD') <> '')and(Query_Select('MEGBIZAS','MB_JASZA',jaratszam,'MB_JASZA')<>'' ) then
//    if Query_Select2('MEGBIZAS','MB_JAKOD','MB_JASZA',Query1.FieldByName(FOMEZO).AsString, jaratszam,'MB_JAKOD') <>'' then
    begin
      NoticeKi('FIGYELEM ! A j�rathoz megb�z�s tartozik, ez�rt nem t�r�lhet�!');
      exit;
    end;
   Try
		if not Query_Run(Query2, 'DELETE from JARAT where '+FOMEZO+'  ='''+Query1.FieldByName(FOMEZO).AsString+''' ') then Raise Exception.Create('');
		if not Query_Run(Query2, 'DELETE from VISZONY where  VI_JAKOD ='''+Query1.FieldByName(FOMEZO).AsString+''' ') then Raise Exception.Create('');
		if not Query_Run(Query2, 'DELETE from KOLTSEG where  KS_JAKOD ='''+Query1.FieldByName(FOMEZO).AsString+''' ') then Raise Exception.Create('');
		if not Query_Run(Query2, 'DELETE from SULYOK where   SU_JAKOD ='''+Query1.FieldByName(FOMEZO).AsString+''' ') then Raise Exception.Create('');
		if not Query_Run(Query2, 'DELETE from JARPOTOS where JP_JAKOD ='''+Query1.FieldByName(FOMEZO).AsString+''' ') then Raise Exception.Create('');
		if not Query_Run(Query2, 'DELETE from JARSOFOR where JS_JAKOD ='''+Query1.FieldByName(FOMEZO).AsString+''' ') then Raise Exception.Create('');
		if not Query_Run(Query2, 'DELETE from ALJARAT  where AJ_JAKOD ='''+Query1.FieldByName(FOMEZO).AsString+''' ') then Raise Exception.Create('');
		if not Query_Update( Query2, 'MEGBIZAS', [
			'MB_JAKOD', ''''+''+'''',
			'MB_JASZA', ''''+''+'''',
			// 'MB_SZKOD', ''''+''+'''',
      'MB_SAKO2', ''''+''+'''',
			'MB_VIKOD', ''''+''+''''
		], ' WHERE MB_JAKOD = '''+Query1.FieldByName(FOMEZO).AsString+''' ') then Raise Exception.Create('') ;
//		], ' WHERE MB_JAKOD = '''+Query1.FieldByName(FOMEZO).AsString+''' AND MB_DATUM > '''+EgyebDlg.MaiDatum+''' ');
   Except
      NoticeKi('A t�rl�s nem siker�lt!');
   End;
		ModLocate (Query1, FOMEZO, Query1.FieldByName(FOMEZO).AsString );
	end;
end;

procedure TJaratFormDlg.BitBtn12Click(Sender: TObject);
begin
	// Let�r�lj�k a felt�teleket
  kell_datum_szures:=False;
	alapstr	:= 'SELECT * FROM JARAT ';
	SQL_ToltoFo(GetOrderBy(true));
end;

procedure TJaratFormDlg.BitBtn1Click(Sender: TObject);
var
	str	: string;
begin
	if not voltalt then begin
		voltalt 	:= true;
		Exit;
	end;
	// A le�r�s �t�r�sa
	str := InputBox('Le�r�s cser�je', 'Az �j le�r�s : ', Query1.FieldByName('JA_JARAT').AsString);
	if str <> Query1.FieldByName('JA_JARAT').AsString then begin
		Query_Run(Query2, 'UPDATE JARAT SET JA_JARAT = '''+str+''' where '+FOMEZO+' ='''+Query1.FieldByName(FOMEZO).AsString+''' ');
		ModLocate (Query1, FOMEZO, Query1.FieldByName(FOMEZO).AsString );
	end;
end;

procedure TJaratFormDlg.BitBtn2Click(Sender: TObject);
begin
	// Csak ellen�rz�tt j�ratot lehet feloldani
	if StrToIntDef(Query1.FieldByName('JA_FAZIS').AsString,0) <> 1 then begin
		Exit;
	end;
	if NoticeKi('Val�ban feloldja az ellen�rz�tts�get?', NOT_QUESTION) <> 0 then begin
		Exit;
	end;
	Query_Run (Query2, 'UPDATE JARAT SET JA_FAZIS = -2, JA_FAZST = '''+GetJaratFazis( -2)+''' '+
		' WHERE '+FOMEZO+' = '''+Query1.Fieldbyname(FOMEZO).AsString  + ''' ', true);
	ModLocate (Query1, FOMEZO, Query1.FieldByName(FOMEZO).AsString );
end;

procedure TJaratFormDlg.BitBtn3Click(Sender: TObject);
begin
    if Query_Select('KOLTSEG','KS_JAKOD',Query1.FieldByName(FOMEZO).AsString,'KS_JAKOD') <> '' then
    begin
      NoticeKi('FIGYELEM ! A j�rathoz k�lts�g tartozik, ez�rt nem stornozhat�!');
      exit;
    end;
    if Query_Select('MEGBIZAS','MB_JAKOD',Query1.FieldByName(FOMEZO).AsString,'MB_JAKOD') = '' then
    begin
      NoticeKi('FIGYELEM ! A j�rathoz nem tartozik megb�z�s, ez�rt nem stornozhat�!');
      exit;
    end;


	// J�rat storn�z�sa
	if NoticeKi('Val�ban Storn�zni k�v�nja a j�ratot?', NOT_QUESTION) <> 0 then begin
		Exit;
	end;
	if NoticeKi('A storn�zott j�ratot a k�s�bbiek sor�n m�r nem m�dos�thatja. Folytatja?', NOT_QUESTION) <> 0 then begin
		Exit;
	end;

	Query_Run (Query2, 'UPDATE JARAT SET JA_FAZIS = 2, JA_FAZST = '''+GetJaratFazis( 2)+''' '+
		' WHERE '+FOMEZO+' = '''+Query1.Fieldbyname(FOMEZO).AsString  + ''' ', true);
	ModLocate (Query1, FOMEZO, Query1.FieldByName(FOMEZO).AsString );
end;

procedure TJaratFormDlg.BitBtn4Click(Sender: TObject);
begin
	if not voltalt then begin
		voltalt 	:= true;
		Exit;
	end;
	// Az eddig elk�ld�tt pdf-ek megjelen�t�se
	Query_Run (Query3, 'SELECT * FROM CSATOLMANY WHERE CS_JAKOD = '''+Query1.FieldByName('JA_KOD').AsString+''' ',true);
	if Query3.RecordCount > 0 then begin
		// Megnyitjuk a csatolm�nyokat
		EgyebDlg.parameter_str	:= Query1.FieldByName('JA_KOD').AsString;
		Application.CreateForm(TCsatolmanyFmDlg, CsatolmanyFmDlg);
		CsatolmanyFmDlg.ShowModal;;
		CsatolmanyFmDlg.Destroy;
		EgyebDlg.parameter_str	:= '';
	end else begin
		NoticeKi('M�g nem k�ldt�k el az alv�llalkoz�i megb�z�st! ');
	end;
end;

procedure TJaratFormDlg.BitBtn5Click(Sender: TObject);
begin
  Application.CreateForm(TFJarat_ell, FJarat_ell);
  FJarat_ell.ShowModal;
  FJarat_ell.Free;
end;

procedure TJaratFormDlg.BitBtn6Click(Sender: TObject);
begin
	// EgyebDlg.Calendarbe(M2);
	// M3.Text	:= M2.Text;
  // EgyebDlg.ElsoNapEllenor(M2, M3);

  EgyebDlg.CalendarBe_idoszak(M2, M3);
  kell_datum_szures:=True;

	alapstr	:= 'SELECT * FROM JARAT WHERE JA_JKEZD >= '''+M2.Text+''' AND JA_JKEZD <= '''+M3.Text+''' ';
	SQL_ToltoFo(GetOrderBy(true));
end;

procedure TJaratFormDlg.BitBtn7Click(Sender: TObject);
begin
	EgyebDlg.Calendarbe(M3, True);
  kell_datum_szures:=True;
	alapstr	:= 'SELECT * FROM JARAT WHERE JA_JKEZD >= '''+M2.Text+''' AND JA_JKEZD <= '''+M3.Text+''' ';
	SQL_ToltoFo(GetOrderBy(true));
end;

procedure TJaratFormDlg.SegedEllenor;
var
   ssz     : integer;
begin
   Query_Run ( QSeged, 'SELECT JG_JAKOD FROM JARATMEGSEGED');
   if QSeged.RecordCount > 0 then begin
       Exit;
   end;
   // A JARATMEGSEGED ellen�rz�se
   Screen.Cursor   := crHourGlass;
   Application.CreateForm(TFormGaugeDlg, FormGaugeDlg);
   FormGaugeDlg.GaugeNyit('Seg�dsorok �sszegy�jt�se 1 ... ' , '', false);
//   Query_Run(EgyebDlg.QueryAlap,'SELECT MS_JAKOD, MS_MSKOD, MS_MBKOD FROM MEGSEGED WHERE MS_JAKOD = '''' AND MS_FAJKO <> 0 AND MS_RENSZ = ''FJJ-367'' ORDER BY MS_MBKOD ');
   Query_Run(EgyebDlg.QueryAlap,'SELECT MS_JAKOD, MS_MSKOD, MS_MBKOD FROM MEGSEGED WHERE MS_JAKOD = '''' AND MS_FAJKO <> 0 ORDER BY MS_MBKOD ');
   EgyebDlg.QueryAlap.DisableControls;
   ssz     := 1;
   while not EgyebDlg.QueryAlap.Eof  do begin
       Application.ProcessMessages;
       FormGaugeDlg.Pozicio ( ( ssz * 100 ) div EgyebDlg.QueryAlap.RecordCount ) ;
       Inc(ssz);
       MegSegedJarat(EgyebDlg.QueryAlap.FieldByName('MS_MBKOD').AsString, EgyebDlg.QueryAlap.FieldByName('MS_MSKOD').AsString);
       EgyebDlg.QueryAlap.Next;
   end;
   EgyebDlg.QueryAlap.EnableControls;
   FormGaugeDlg.GaugeNyit('Seg�dsorok �sszegy�jt�se 2 ... ' , '', false);
   Query_Run(EgyebDlg.QueryAlap,'SELECT MS_JAKOD, MS_MSKOD, MS_MBKOD FROM MEGSEGED WHERE MS_JAKOD <> '''' AND MS_FAJKO <> 0 ORDER BY MS_MBKOD ');
   ssz     := 1;
   EgyebDlg.QueryAlap.DisableControls;
   while not EgyebDlg.QueryAlap.Eof do begin
       Application.ProcessMessages;
       FormGaugeDlg.Pozicio ( ( ssz * 100 ) div EgyebDlg.QueryAlap.RecordCount ) ;
       Inc(ssz);
       // A megl�v� elemek berak�sa a t�bl�ba
       Query_Run ( QSeged, 'INSERT INTO JARATMEGSEGED ( JG_JAKOD, JG_MBKOD, JG_MSKOD, JG_FOJAR ) VALUES ( '+
               ''''+ EgyebDlg.QueryAlap.FieldByName('MS_JAKOD').AsString + ''', '+EgyebDlg.QueryAlap.FieldByName('MS_MBKOD').AsString +','+EgyebDlg.QueryAlap.FieldByName('MS_MSKOD').AsString +','+
               ''''+ Query_Select('MEGBIZAS', 'MB_MBKOD', EgyebDlg.QueryAlap.FieldByName('MS_MBKOD').AsString, 'MB_JAKOD')+''' )');
       EgyebDlg.QueryAlap.Next;
   end;
   EgyebDlg.QueryAlap.EnableControls;
   FormGaugeDlg.Destroy;
   Screen.Cursor   := crDefault;
end;

end.





