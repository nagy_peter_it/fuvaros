unit TelSzamlaTetelekFm;

interface

uses
	SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
	StdCtrls, Forms, DBCtrls, DB, DBGrids, DBTables, Grids, ExtCtrls, Buttons,
	ShellApi, Dialogs, ADODB, J_FOFORMUJ, Vcl.Menus;

type
	TTelSzamlaTetelekFmDlg = class(TJ_FOFORMUJDLG)
    Query3: TADOQuery;
    PopupMenu1: TPopupMenu;
    miTelefonszamlaMegjegyzes: TMenuItem;
    BitBtn2: TBitBtn;
    BitBtn1: TBitBtn;
	  procedure FormCreate(Sender: TObject);override;
    procedure DBGrid2DrawColumnCell(Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);override;
    // procedure Query1AfterScroll(DataSet: TDataSet);override;
    procedure miTelefonszamlaMegjegyzesClick(Sender: TObject);
    procedure Adatlap(cim, kod : string);override;
    procedure MegjegyzesMutat;
    // procedure miAnomaliaJovahagyasaClick(Sender: TObject);
    // procedure miAnomaliaJovahagyasVisszavonasaClick(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
	end;

var
	TelSzamlaTetelekFmDlg : TTelSzamlaTetelekFmDlg;

implementation

uses
	Egyeb, J_SQL, Kozos, UniSzovegForm, TelElofizetBe, UniGraphInfoForm;

{$R *.DFM}

procedure TTelSzamlaTetelekFmDlg.FormCreate(Sender: TObject);
var
  EgyediLimitIroda, EgyediLimitSofor, SajatAtlagSzorzo, CsoportAtlagSzorzo, MobilnetSzolgLista: string;
begin
	EgyebDLg.SetADOQueryDatabase(Query3);
	Inherited FormCreate(Sender);
  EgyediLimitIroda:= EgyebDlg.Read_SZGrid('383', '1');
  EgyediLimitSofor:= EgyebDlg.Read_SZGrid('383', '2');
  SajatAtlagSzorzo:= EgyebDlg.Read_SZGrid('383', '11');
  CsoportAtlagSzorzo:= EgyebDlg.Read_SZGrid('383', '12');
  MobilnetSzolgLista:= EgyebDlg.Read_SZGrid('383', '30');

	AddSzuromezoRovid('El�fizet�s', '');
	AddSzuromezoRovid('Id�szak', '');
	AddSzuromezoRovid('Sz�mlacsoport', '', '1');
	AddSzuromezoRovid('Kin�l', '', '1');
	AddSzuromezoRovid('Term�k', '');
  AddSzuromezoRovid('T�telOszt�ly', '', '1');
  AddSzuromezoRovid('FuvarosOK', '', '1');
  // AddSzuromezoRovid('Igazolva', '', '1');
  AddSzummamezo('Brutt�');
  AddSzummamezo('Mennyis�g');
  AddSzummamezo('Nett�');
  Alapstr := 'select * from '+ // a mez�k azonos�t�s�hoz az els� "from"-t�l haszn�lja, ez�rt kell ez a plusz "h�j"
        ' (select TT_ID, TT_TELSZAM El�fizet�s, TS_IDOSZAK_TOL+'' - ''+TS_IDOSZAK_IG Id�szak, sz1.SZ_MENEV Term�k, TT_MENNYISEG Mennyis�g, TT_MENNYEGYS Egys�g, TT_NETTOAR Nett�, '+
        ' TT_BRUTTOAR Brutt�, SZAMLACSOPORT Sz�mlacsoport, '+
        // ' ltrim(rtrim(isnull(DO_NAME,'''') + isnull(TT_HINFO,''''))) Kin�l, '+
        // ' case when (isnull(DO_NAME,'''') <> '''') then ltrim(rtrim(isnull(DO_NAME,''''))) else ltrim(rtrim(isnull(TT_HINFO,''''))) end Kin�l, '+
        ' case when (isnull(TZ_DONEVLIST,'''') <> '''') then ltrim(rtrim(isnull(TZ_DONEVLIST,''''))) else ltrim(rtrim(isnull(TZ_HINFO,''''))) end Kin�l, '+
        ' TT_MEGJE Megjegyz�s, '+
        // ' case when isnull(TT_IGAZOL,'''')=''1'' then ''Igen'' else '''' end Igazolva, '+
        // ' szami.TelefonszamlaTetelEllenor3 (TS_IDOSZAK_TOL+'' - ''+TS_IDOSZAK_IG, TT_TELSZAM, SZ_MENEV, TT_BRUTTOAR, case when TE_SZAMLACSOPKOD = 1 then '+EgyediLimitIroda+' else '+EgyediLimitSofor+' end, '+
        // '   '+SajatAtlagSzorzo+', '+CsoportAtlagSzorzo+') T�telOszt�ly, '+
        ' TT_OSZTALY T�telOszt�ly, '+
        // ' case when isnull(sz1.SZ_EGYEB1,0)=1 then szami.TelefonszamlaTetelSzolgaltatasEllenor (TT_TELSZAM, sz1.SZ_MENEV) else '''' end FuvarosOK '+
        // csak akkor ellen�rzi, ha mobilnet szolg�ltat�s
        ' case when sz1.SZ_ALKOD in ('+MobilnetSzolgLista+') then szami.TelefonszamlaTetelSzolgaltatasEllenor (TT_TELSZAM, sz1.SZ_MENEV) else '''' end FuvarosOK '+
        ' from TELSZAMLAK '+
        ' join TELSZAMLATETELEK on TT_TSID = TS_ID '+
        ' left outer join SZOTAR sz1 on sz1.SZ_FOKOD=''155'' and sz1.SZ_ALKOD = TT_TERMEKID '+
        ' left outer join TELSZAMLASZAMOK on TZ_TSID = TS_ID and TZ_TELSZAM = TT_TELSZAM '+
        // ' left outer join DOLGOZO on TT_DOKOD = DO_KOD '+
        ' left outer join (select TE_TELSZAM, substring(SZ_MENEV,1,20) SZAMLACSOPORT, TE_SZAMLACSOPKOD from TELELOFIZETES, SZOTAR '+
        '     where SZ_FOKOD=''145'' and TE_SZAMLACSOPKOD = SZ_ALKOD) x1 '+
        '     on TT_TELSZAM = TE_TELSZAM '+
        ' ) a  where 1=1 ';  // a k�s�bbi sz�r�shez kell
   kellujures:= false;
   FelTolto('', 'Sz�mla t�telek',
			'Telefonsz�mla t�telek', 'TT_ID',
           alapstr,
           'TELSZAMLAK',
            QUERY_KOZOS_TAG);
	width	:= DLG_WIDTH;
	height	:= DLG_HEIGHT;
  ButtonUj.Visible:= False;
  ButtonMod.Visible:= False;
  ButtonNez.Visible:= True; // ebb�l lesz a megjegyz�s szerkeszt�s
  DBGrid2.PopupMenu:= PopupMenu1;
  BitBtn2.Parent:= PanelBottom;
  BitBtn1.Parent:= PanelBottom;
end;

{
procedure TTelSzamlaTetelekFmDlg.miAnomaliaJovahagyasaClick(Sender: TObject);
var
  Szoveg, TTID, KOD: string;
begin
    TTID:= Query1.FieldByName('TT_ID').AsString;
    KOD:= Query1.FieldByName(FOMEZO).AsString;
    Query_Update (Query3, 'TELSZAMLATETELEK', [
      'TT_IGAZOL',  '1'
        ], ' WHERE TT_ID = '+TTID );
    SQL_ToltoFo(GetOrderBy(true));   // t�bl�zat friss�t�s
    ModLocate (Query1, FOMEZO, KOD);
end;

procedure TTelSzamlaTetelekFmDlg.miAnomaliaJovahagyasVisszavonasaClick(Sender: TObject);
var
  Szoveg, TTID, KOD: string;
begin
    TTID:= Query1.FieldByName('TT_ID').AsString;
    KOD:= Query1.FieldByName(FOMEZO).AsString;
    Query_Update (Query3, 'TELSZAMLATETELEK', [
      'TT_IGAZOL',  '0'
        ], ' WHERE TT_ID = '+TTID );
    SQL_ToltoFo(GetOrderBy(true));   // t�bl�zat friss�t�s
    ModLocate (Query1, FOMEZO, KOD);
end;
 }
procedure TTelSzamlaTetelekFmDlg.miTelefonszamlaMegjegyzesClick(Sender: TObject);
begin
  MegjegyzesMutat;
end;

procedure TTelSzamlaTetelekFmDlg.MegjegyzesMutat;
var
  Szoveg, KOD, TTID: string;
begin
    Szoveg:= Query1.FieldByName('Megjegyz�s').AsString;
    TTID:= Query1.FieldByName('TT_ID').AsString;
    KOD:= Query1.FieldByName(FOMEZO).AsString;
		Application.CreateForm(TUniSzovegFormDlg, UniSzovegFormDlg);
    with UniSzovegFormDlg do begin
      Caption:='Megjegyz�s m�dos�t�sa';
      lblSzovegelemNeve.Caption:='Megjegyz�s:';
      meSzoveg.Text:= Szoveg;
      M1.Text:= TTID;
      UpdateSQLString:= 'update TELSZAMLATETELEK set TT_MEGJE=''%s'' where TT_ID='+TTID;
      ShowModal;
      Release;
      end; // with
    SQL_ToltoFo(GetOrderBy(true));   // t�bl�zat friss�t�s
    ModLocate (Query1, FOMEZO, KOD);
end;

procedure TTelSzamlaTetelekFmDlg.BitBtn1Click(Sender: TObject);
var
  APoint: TPoint;
begin
  Application.CreateForm(TUniGraphInfoFormDlg, UniGraphInfoFormDlg);
  UniGraphInfoFormDlg.SetPicture('Telefonsz�mla t�telek sz�nmagyar�zat', 1);
  APoint:= BitBtn1.ClientToScreen(Point(0, 0));
  UniGraphInfoFormDlg.Left:= TelSzamlaTetelekFmDlg.ScreenToClient(APoint).X + BitBtn1.Width - UniGraphInfoFormDlg.Width;
  UniGraphInfoFormDlg.Top:= TelSzamlaTetelekFmDlg.ScreenToClient(APoint).Y - UniGraphInfoFormDlg.Height;
  UniGraphInfoFormDlg.ShowModal;
  UniGraphInfoFormDlg.Free;
end;

procedure TTelSzamlaTetelekFmDlg.BitBtn2Click(Sender: TObject);
begin
	// El�fizet�s adatlap megjelen�t�se
	if Query1.FieldByName('El�fizet�s').AsString <> '' then begin
		Query_Run (Query3, 'SELECT TE_ID FROM TELELOFIZETES WHERE TE_TELSZAM = '''+Query1.FieldByName('El�fizet�s').AsString+''' ',true);
		if Query3.RecordCount < 1 then begin
			NoticeKi('Az el�fizet�s nem tal�lhat�!');
			Exit;
		  end;  // end
		Application.CreateForm(TTelElofizetBeDlg, TelElofizetBeDlg);
    TelElofizetBeDlg.megtekint:= true;
		TelElofizetBeDlg.Tolto('El�fizet�s megtekint�se',Query3.FieldByName('TE_ID').AsString);
		TelElofizetBeDlg.ShowModal;
		TelElofizetBeDlg.Release;
    ModLocate (Query1, FOMEZO, Query1.FieldByName(FOMEZO).AsString );
   end;
end;

procedure TTelSzamlaTetelekFmDlg.DBGrid2DrawColumnCell(Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
var
   c: integer;
begin
   if Column.FieldName='T�telOszt�ly' then begin  // csak ezt az oszlopot sz�nezz�k, gyorsabb
     DBGrid2.Canvas.Brush.Color:= clWindow;  // alap�rtelmezett h�tt�rsz�n
     if Query1.FieldByName('T�telOszt�ly').AsString = 's�rga' then
          DBGrid2.Canvas.Brush.Color:= EgyebDlg.GetFuvarosSzin('1');
     if Query1.FieldByName('T�telOszt�ly').AsString = 'piros' then
          DBGrid2.Canvas.Brush.Color:= EgyebDlg.GetFuvarosSzin('2');
     if Query1.FieldByName('T�telOszt�ly').AsString = 'narancs' then
          DBGrid2.Canvas.Brush.Color:= EgyebDlg.GetFuvarosSzin('3');
     if Query1.FieldByName('T�telOszt�ly').AsString = 'halv�nys�rga' then
          DBGrid2.Canvas.Brush.Color:= EgyebDlg.GetFuvarosSzin('4');
     if Query1.FieldByName('T�telOszt�ly').AsString = 'halv�nynarancs' then
          DBGrid2.Canvas.Brush.Color:= EgyebDlg.GetFuvarosSzin('5');
     if Query1.FieldByName('T�telOszt�ly').AsString = 'halv�nylila' then
          DBGrid2.Canvas.Brush.Color:= EgyebDlg.GetFuvarosSzin('6');
     if Query1.FieldByName('T�telOszt�ly').AsString = 'lila' then
          DBGrid2.Canvas.Brush.Color:= EgyebDlg.GetFuvarosSzin('7');

    DBGrid2.DefaultDrawDataCell(Rect, Column.Field, State);
    end;
	inherited DBGrid2DrawColumnCell(Sender, Rect, DataCol, Column, State);

end;

procedure TTelSzamlaTetelekFmDlg.Adatlap(cim, kod : string);
begin
  MegjegyzesMutat;  // az aktu�lis sorral dolgozik
end;


{procedure TTelSzamlaTetelekFmDlg.Query1AfterScroll(DataSet: TDataSet);
begin
  if (Query1.FieldByName('Igazolva').AsString <> '') then begin
       miAnomaliaJovahagyasa.Visible:= False;
       miAnomaliaJovahagyasVisszavonasa.Visible:= True;
       end
  else begin
       miAnomaliaJovahagyasa.Visible:= True;
       miAnomaliaJovahagyasVisszavonasa.Visible:= False;
       end;  // else

  inherited Query1AfterScroll(DataSet);
end;}

end.

