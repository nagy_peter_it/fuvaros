unit Valgepk;

interface

uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, DBTables, DB, StdCtrls, Buttons, Mask, DBCtrls,
  Printers, ExtCtrls, CheckLst, ADODB;

type
  TValgepkDlg = class(TForm)
    BitElkuld: TBitBtn;
    BitKilep: TBitBtn;
    CheckListBox1: TCheckListBox;
    Memo1: TMemo;
    SpeedButton3: TSpeedButton;
    Query1: TADOQuery;
    procedure BitBtn6Click(Sender: TObject);
    procedure BitElkuldClick(Sender: TObject);
    procedure BitKilepClick(Sender: TObject);
    procedure Tolt(kodok : string);
    procedure FormShow(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
  private
  		kodlist	: TStringList;
     	nevlist	: TStringList;
  public
     	kilepes		: boolean;
     	kodkilist   : TStringList;		// A rendsz�mok list�ja
     	nevkilist   : TStringList;		// A t�pusok list�ja
      csakpotos, ARHIVNEM	: boolean;
  end;

var
  ValgepkDlg: TValgepkDlg;

implementation

uses
	Egyeb, Kozos, J_SQL, GepkocsiBe;
{$R *.DFM}

procedure TValgepkDlg.BitBtn6Click(Sender: TObject);
begin
	Close;
end;

procedure TValgepkDlg.BitElkuldClick(Sender: TObject);
var
	i : integer;
begin
	// Itt �ll�tjuk �ssze a visszaadand� stringeket
  kilepes	:= false;
  kodkilist.Clear;
  nevkilist.Clear;
  for i := 0 to CheckListBox1.Items.Count - 1 do begin
     if CheckListBox1.Checked[i] = true then begin
        kodkilist.Add(kodlist[i]);
        nevkilist.Add(nevlist[i]);
     end;
  end;
  Close;
end;

procedure TValgepkDlg.BitKilepClick(Sender: TObject);
begin
	kodkilist.Clear;
  kilepes	:= true;
	Close;
end;

procedure TValgepkDlg.Tolt(kodok : string);
var
	i : integer;
  arc:string;
begin
	kilepes	:= false;
	CheckListBox1.Clear;
  arc:='';
  if ARHIVNEM then        // Arch�vak nem jelennek meg
  begin
    if csakpotos then
      arc:=' and GK_ARHIV<>1 '
    else
      arc:='  where GK_ARHIV<>1 ';
  end;
  // A tanfolyamok beolvas�sa
	EgyebDlg.SetADOQueryDatabase(Query1);
   if not csakpotos then begin
   	Caption := 'G�pkocsik / p�tkocsik kiv�laszt�sa';
  		Query_Run(Query1, 'SELECT * FROM GEPKOCSI '+arc+ ' ORDER BY GK_RESZ ');
   end else begin
   	Caption := 'P�tkocsik kiv�laszt�sa';
  		Query_Run(Query1, 'SELECT * FROM GEPKOCSI WHERE GK_POTOS = 1 '+arc+' ORDER BY GK_RESZ ');
   end;
  	if Query1.RecordCount < 1 then begin
  		Exit;
  	end;
  	// Az elemek beolvas�sa
  	Query1.First;
  	while not Query1.EOF do begin
    	CheckListBox1.Items.Add( copy(Query1.FieldByName('GK_RESZ').AsString+URESSTRING,1,10) +
     	copy(Query1.FieldByName('GK_TIPUS').AsString+URESSTRING,1,25)+
     	copy(Query1.FieldByName('GK_MEGJ').AsString+URESSTRING,1,10)
        );
    	nevlist.Add( Query1.FieldByName('GK_RESZ').AsString);
     	kodlist.Add(Query1.FieldByName('GK_KOD').AsString);
     	Query1.Next;
  	end;
  	Query1.Close;
  	CheckListBox1.ItemIndex	:= 0;
  	if kodok <> '' then begin
  		// A k�dokban tal�lhat� elemek kijel�l�se
     	for i := 0 to CheckListBox1.Items.Count - 1 do begin
     		if Pos(','+kodlist[i]+',',','+kodok+',') >  0 then begin
        		CheckListBox1.Checked[i] := true;
        	end;
     	end;
  	end;
end;

procedure TValgepkDlg.FormShow(Sender: TObject);
begin
	CheckListbox1.SetFocus;
end;

procedure TValgepkDlg.FormDestroy(Sender: TObject);
begin
	kodlist.Free;
  	kodkilist.Free;
  	nevlist.Free;
  	nevkilist.Free;
end;

procedure TValgepkDlg.FormCreate(Sender: TObject);
begin
  	kodlist		:= TStringList.Create;
  	nevlist		:= TStringList.Create;
  	kodkilist	:= TStringList.Create;
  	nevkilist	:= TStringList.Create;
  	csakpotos	:= false;
end;

procedure TValgepkDlg.SpeedButton3Click(Sender: TObject);
begin
	// A dolgoz� adatainak m�dos�t�sa
	Application.CreateForm(TGepkocsibeDlg, GepkocsibeDlg);
	GepkocsibeDlg.Tolto('G�pkocsi m�dos�t�sa',kodlist[CheckListbox1.ItemIndex]);
	GepkocsibeDlg.ShowModal;
  	if GepkocsibeDlg.ret_kod <> '' then begin
  		// Az adatok friss�t�se
  		Query_Run(Query1, 'SELECT * FROM GEPKOCSI WHERE GK_KOD = '''+kodlist[CheckListbox1.ItemIndex]+''' ');
		CheckListBox1.Items[CheckListbox1.ItemIndex] :=
    		copy(Query1.FieldByName('GK_RESZ').AsString+URESSTRING,1,10) +
     		copy(Query1.FieldByName('GK_TIPUS').AsString+URESSTRING,1,25)+
     		copy(Query1.FieldByName('GK_MEGJ').AsString+URESSTRING,1,10
        	);
  	end;
	GepkocsibeDlg.Destroy;
end;

end.


